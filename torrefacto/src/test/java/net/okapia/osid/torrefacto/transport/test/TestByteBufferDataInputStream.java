//
// TestByteBufferDataInputStream.java
//
//     A unit test for a ByteBufferDataInputStream
//
//
// Tom Coppeto
// Okapia
// 23 September 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.torrefacto.transport.test;

import org.testng.Assert;
import org.testng.annotations.*;

import java.nio.ByteBuffer;
import java.util.Random;

import org.osid.transport.DataInputStream;
import net.okapia.osid.torrefacto.transport.ByteBufferDataInputStream;


@Test
public class TestByteBufferDataInputStream
    extends AbstractTestDataInputStream {


    @Override
    protected DataInputStream makeStream(byte[] data) {
        ByteBuffer buffer = ByteBuffer.allocate(data.length);
        buffer.put(data);
        buffer.flip();
        return (new ByteBufferDataInputStream(buffer));
    }


    public static int[] SIZES = {0, 1, 2, 3, 8, 32, 99, 127, 1024, 100000, 1234567};

    @DataProvider(name = "data")
    public Object[][] getData() {
        Object[][] ret = new Object[SIZES.length][1];

        for (int i = 0; i < SIZES.length; i++) {
            byte[] buffer = new byte[SIZES[i]];
            new Random().nextBytes(buffer);
            ret[i][0] = buffer;
        }

        return (ret);
    }
}
