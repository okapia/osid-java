//
// AbstractTestDataInoputStream.java
//
//     A unit test for a DataInputStream.
//
//
// Tom Coppeto
// Okapia
// 23 September 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.torrefacto.transport.test;

import org.testng.Assert;
import org.testng.annotations.*;

import java.nio.ByteBuffer;

import org.osid.transport.DataInputStream;


public abstract class AbstractTestDataInputStream {

    protected abstract DataInputStream makeStream(byte[] data);


    @Test(dataProvider="data")
    public void testValidity(byte[] data) {
        DataInputStream stream = makeStream(data);
        
        Assert.assertNotNull(stream);
        if (data.length == 0) {
            Assert.assertEquals(stream.atEndOfStream(), true);
        } else {
            Assert.assertEquals(stream.atEndOfStream(), false);
        }

        stream.close();
        return;
    }


    @Test(dataProvider="data")
    public void testSkip(byte[] data) {
        DataInputStream stream = makeStream(data);
        
        long total = 0;
        byte[] input = new byte[data.length];

        while (!stream.atEndOfStream()) {
            long available = stream.available();
            Assert.assertEquals(available >= 0, true);

            long skipped = stream.skip(available);
            Assert.assertEquals(available, skipped);
            
            total += skipped;
        }

        Assert.assertEquals(stream.available(), 0);
        stream.close();

        Assert.assertEquals(data.length, total);
        return;
    }


    @Test(dataProvider="data")
    public void testSkipOne(byte[] data) {
        DataInputStream stream = makeStream(data);
        
        long total = 0;
        byte[] input = new byte[data.length];

        while (!stream.atEndOfStream()) {
            Assert.assertEquals(stream.skip(1), 1);
            ++total;
        }

        stream.close();

        Assert.assertEquals(data.length, total);
        return;
    }


    @Test(dataProvider="data")
    public void testSkipInvalid(byte[] data) {
        DataInputStream stream = makeStream(data);
        
        long total = 0;
        byte[] input = new byte[data.length];

        while (!stream.atEndOfStream()) {
            long available = stream.available();
            Assert.assertEquals(available >= 0, true);

            long skipped = stream.skip(available * 100000);
            Assert.assertEquals(available, skipped);
            
            total += skipped;
        }

        Assert.assertEquals(stream.available(), 0);
        stream.close();

        Assert.assertEquals(data.length, total);
        return;
    }


    @Test(dataProvider="data")
    public void testRead(byte[] data)
        throws org.osid.OperationFailedException {

        DataInputStream stream = makeStream(data);
        
        long total = 0;
        ByteBuffer input = ByteBuffer.allocate(data.length);

        while (!stream.atEndOfStream()) {
            long available = stream.available();
            byte[] d = new byte[(int) available];
            long read = stream.read(d, available);
            Assert.assertEquals(available, read);

            input.put(d);
            
            total += read;
        }

        Assert.assertEquals(stream.available(), 0);
        stream.close();

        input.flip();
        Assert.assertEquals(data, input.array());
        Assert.assertEquals(data.length, total);

        return;
    }


    @Test(dataProvider="data")
    public void testReadOne(byte[] data) 
        throws org.osid.OperationFailedException {

        DataInputStream stream = makeStream(data);
        
        long total = 0;
        ByteBuffer input = ByteBuffer.allocate(data.length);

        while (!stream.atEndOfStream()) {
            byte[] d = new byte[1];
            Assert.assertEquals(stream.read(d, 1), 1);

            input.put(d);
            ++total;
        }

        Assert.assertEquals(stream.available(), 0);
        stream.close();

        input.flip();
        Assert.assertEquals(data, input.array());
        Assert.assertEquals(data.length, total);
        return;
    }


    @Test(dataProvider="data")
    public void testReadInvalid(byte[] data)
        throws org.osid.OperationFailedException {

        DataInputStream stream = makeStream(data);
        
        long total = 0;
        ByteBuffer input = ByteBuffer.allocate(data.length);

        while (!stream.atEndOfStream()) {
            long available = stream.available();
            byte[] d = new byte[(int) available * 10];
            long read = stream.read(d, available * 10);

            Assert.assertEquals(available, read);
            input.put(d, (int) total, (int) read);            
            total += read;
        }

        Assert.assertEquals(stream.available(), 0);
        stream.close();

        input.flip();
        Assert.assertEquals(data, input.array());
        Assert.assertEquals(data.length, total);

        return;
    }


    @Test(dataProvider="data")
    public void testReadSkip(byte[] data) 
        throws org.osid.OperationFailedException {

        DataInputStream stream = makeStream(data);
        
        long total = 0;

        boolean skip = true;
        while (!stream.atEndOfStream()) {
            byte[] d = new byte[1];

            if (skip) {
                Assert.assertEquals(stream.skip(1), 1);
                skip = false;
            } else {
                Assert.assertEquals(stream.read(d, 1), 1);
                skip = true;
            }

            ++total;
        }

        Assert.assertEquals(stream.available(), 0);
        stream.close();

        Assert.assertEquals(data.length, total);
        return;
    }
}
