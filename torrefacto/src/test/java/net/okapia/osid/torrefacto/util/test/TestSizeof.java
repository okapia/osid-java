//
// TestSizeof.java
//
//     A unit test for the utilities.
//
//
// Tom Coppeto
// Okapia
// 14 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.torrefacto.util.test;

import org.testng.Assert;
import org.testng.annotations.*;

import net.okapia.osid.torrefacto.util.Sizeof;


@Test
public class TestSizeof {

    public static Object STUFF[][] = {
        {'c', 1},
        {"hello", 5},
        {Integer.valueOf(1000), 4},
        {Long.valueOf(1000L), 8},
        {Boolean.valueOf(true), 1}
    };


    @DataProvider(name = "stuff")
    public Object[][] getStuff() {
        return (STUFF);
    }


    @Test(dataProvider="stuff")
    public void testSize(Object obj, int bytes) {
        Assert.assertEquals(Sizeof.bytes(obj), bytes);       
        return;
    }
}
