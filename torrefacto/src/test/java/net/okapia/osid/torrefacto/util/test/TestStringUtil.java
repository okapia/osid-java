//
// TestStringUtil.java
//
//     A unit test for the utilities.
//
//
// Tom Coppeto
// Okapia
// 23 September 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.torrefacto.util.test;

import org.testng.Assert;
import org.testng.annotations.*;

import net.okapia.osid.torrefacto.util.StringUtil;


@Test
public class TestStringUtil {

    public static Object ARTICLES[][] = {
        {"apple", "An", "an",},
        {"quick brown fox", "A", "a"},
        {"Yo-Yo", "A", "a"},
        {"history book", "A", "a"},
        {"island", "An", "an"},
        {"effort", "An", "an"},
        {"platypus", "A", "a"}
    };


    @DataProvider(name = "articles")
    public Object[][] getArticles() {
        return (ARTICLES);
    }


    @Test(dataProvider="articles")
    public void testArticle(String s, String upper, String lower) {
        Assert.assertEquals(StringUtil.getUpperArticle(s), upper);
        Assert.assertEquals(StringUtil.getLowerArticle(s), lower);
        
        return;
    }


    public static Object PHRASES[][] = {
        {"apple", "Apple", "apple",},
        {"the quick brown Fox", "The Quick Brown Fox", "the quick brown fox"},
        {"turn left. turn right.", "Turn Left. Turn Right.", "turn left. turn right."},
        {"i", "I", "i"}
    };


    @DataProvider(name = "phrases")
    public Object[][] getPhrases() {
        return (PHRASES);
    }


    @Test(dataProvider="phrases")
    public void testPhrase(String s, String upper, String lower) {
        Assert.assertEquals(StringUtil.capitalize(s), upper);
        Assert.assertEquals(StringUtil.uncapitalize(s), lower);
        
        return;
    }


    public static Object ORDINALS[][] = {
        {0, "0"},
        {1, "1st"},
        {2, "2nd"},
        {3, "3rd"},
        {4, "4th"},
        {5, "5th"},
        {6, "6th"},
        {7, "7th"},
        {8, "8th"},
        {9, "9th"},
        {10, "10th"},
        {11, "11th"},
        {12, "12th"},
        {13, "13th"},
        {14, "14th"},
        {15, "15th"},
        {20, "20th"},
        {21, "21st"},
        {22, "22nd"},
        {23, "23rd"},
        {24, "24th"},
        {171, "171st"},
        {172, "172nd"},
        {173, "173rd"},
        {174, "174th"}
    };


    @DataProvider(name = "ordinals")
    public Object[][] getOrdinals() {
        return (ORDINALS);
    }


    @Test(dataProvider="ordinals")
    public void testOrdinal(int number, String ordinal) {
        Assert.assertEquals(StringUtil.ordinalize(number), ordinal);
        return;
    }


    public static Object VOWELS[][] = {
        {'a', true},   {'b', false}, {'c', false},  {'d', false}, {'e', true},  {'f', false},
        {'g', false},  {'h', false}, {'i', true},   {'j', false}, {'k', false}, {'l', false},
        {'m', false},  {'n', false}, {'o', true},   {'p', false}, {'q', false}, {'r', false},
        {'s', false},  {'t', false}, {'u', true},   {'v', false}, {'w', false}, {'x', false},
        {'y', false},  {'x', false},
        {'A', true},   {'B', false}, {'C', false},  {'D', false}, {'E', true},  {'F', false},
        {'G', false},  {'H', false}, {'I', true},   {'J', false}, {'K', false}, {'L', false},
        {'M', false},  {'N', false}, {'O', true},   {'P', false}, {'Q', false}, {'R', false},
        {'S', false},  {'T', false}, {'U', true},   {'V', false}, {'W', false}, {'X', false},
        {'Y', false},  {'X', false},
        {'!', false},  {'@', false}, {'$', false},  {'%', false}, {'$', false}, {'%', false},
        {'^', false},  {'&', false}, {'*', false},  {'(', false}, {')', false}, {'-', false},
        {'+', false},  {'-', false}, {'=', false},  {'{', false}, {'}', false}, {'|', false},
        {'[', false},  {']', false}, {'\\', false}, {':', false}, {'"', false}, {';', false},
        {'\'', false}, {'<', false}, {'>', false},  {'?', false}, {',', false}, {'.', false},
        {'/', false},  {'`', false}, {'©', false},  {'™', false}, {'¢', false}, {'∞', false},
        {'0', false},  {'1', false}, {'2', false},  {'3', false}, {'4', false}, {'5', false},
        {'6', false},  {'7', false}, {'8', false},  {'9', false}
    };


    @DataProvider(name = "vowels")
    public Object[][] getVowels() {
        return (VOWELS);
    }


    @Test(dataProvider="vowels")
    public void testVowel(char c, boolean isVowel) {
        Assert.assertEquals(StringUtil.isVowel(c), isVowel);
        return;
    }
}
