//
// TypeSet.java
//
//     Maintains a unique collection of types.
//
//
// Tom Coppeto
// OnTapSolutions
// 20 September 200
//
//
// Copyright (c) 2009 Massachusetts Institute of Technology. All Rights 
// Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.torrefacto.collect;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Maintains a unique collection of types. Each type is uniquely
 *  identified by a namespace, authority, and identifier. 
 *
 *  This utility is designed to assist a provider in managing type
 *  support. 
 */

public class TypeSet
    implements Types {

    private final java.util.Collection<org.osid.type.Type> types = java.util.Collections.synchronizedSet(new java.util.LinkedHashSet<org.osid.type.Type>());


    /**
     *  Creates a new TypeSet.
     */

    public TypeSet() {
	super();
	return;
    }


    /**
     *  Creates a new TypeSet for a given collection of types.
     * 
     *  @param types
     *  @throws org.osid.NullArgumentException <code>types</code> is
     *          <code>null</code>
     */

    public TypeSet(java.util.Collection<org.osid.type.Type> types) {
	nullarg(types, "types");
	this.types.addAll(types);
	return;
    }


    /**
     *  Creates a new TypeSet for a given array of types.
     * 
     *  @param types
     *  @throws org.osid.NullArgumentException <code>types</code> is
     *          <code>null</code>
     */

    public TypeSet(org.osid.type.Type[] types) {
	nullarg(types, "types");

	for (org.osid.type.Type type : types) {
	    this.types.add(type);
	}

	return;
    }


    /**
     *  Tracks a supported type.
     *
     *  @param type
     *  @throws org.osid.NullArgumentException
     *          <code>type</code> is <code>null</code>
     */

    public void add(org.osid.type.Type type) {
	nullarg(type, "type");
	this.types.add(type);
	return;
    }


    /**
     *  Removes a type.
     *
     *  @param type
     *  @throws org.osid.NullArgumentException
     *          <code>type</code> is <code>null</code>
     */

    public void remove(org.osid.type.Type type) {
	nullarg(type, "type");
	this.types.remove(type);
	return;
    }


    /**
     *  Tests for the existience of a type.
     *
     *  @param type
     *  @return <code>true</code> if the type exists,
     *          <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>type</code> is <code>null</code>
     */

    public boolean contains(org.osid.type.Type type) {
	nullarg(type, "type");
	return (this.types.contains(type));
    }


    /**
     *  Gets an iterable collection of the types.
     *
     *  @return a collection
     */

    public java.util.Collection<org.osid.type.Type> toCollection() {
	return (java.util.Collections.unmodifiableCollection(this.types));
    }


    /**
     *  Gets an array of the types.
     *
     *  @return an array of types
     */

    public org.osid.type.Type[] toArray() {
	return (this.types.toArray(new org.osid.type.Type[this.types.size()]));
    }
												   

    /**
     *  Removes all types.
     */

    public void clear() {
	this.types.clear();
	return;
    }


    /**
     *  Returns an iterator of Types.
     *
     *  @return an iterator
     */

    public java.util.Iterator<org.osid.type.Type> iterator() {
	return (toCollection().iterator());
    }
}
