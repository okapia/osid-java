//
// XMLUtil.java
//
//     Fun with XML.
//
//
// Tom Coppeto
// Okapia
// 28 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.torrefacto.xml;

import java.io.File;
import java.io.InputStream;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.Attribute;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;
import org.dom4j.xpath.DefaultXPath;

import org.jaxen.SimpleNamespaceContext; 
import org.jaxen.dom4j.Dom4jXPath; 


/**
 *  Some handy XML tools based on dom4j.
 */

public class XMLUtil {
    private final Document document;
    private final SimpleNamespaceContext ns = new SimpleNamespaceContext();


    /*
     *  Constructs a new <code>XMLUtil</code> and opens the specified
     *  file.
     *
     *  @param path the path to the xml file
     *  @throws org.osid.NotFoundException file not found
     *  @throws org.osid.NullArgumentException <code>path</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException could not read file
     */

    public XMLUtil(String path)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {    

        SAXReader reader = new SAXReader();

        File file = new File(path);
        if (!file.exists()) {
            throw new org.osid.NotFoundException (path + " does not exist");
        }

        if (!file.isFile()) {
            throw new org.osid.OperationFailedException(path + " is not a file");
        }

        try {
            this.document = reader.read(file);
          } catch (Exception e) {
            throw new org.osid.OperationFailedException(e);
        }
    }


    /**
     *  Adds a namespace to this document.
     *
     *  @param prefix
     *  @param uri
     */

    public void addNamespace(String prefix, String uri) {
        this.ns.addNamespace(prefix, uri);
        return;
    }


    /**
     *  Gets the Document element.
     *
     *  @return the document
     */

    public Document getDocument() {
        return (this.document);
    }

    
    /**
     *  Gets an element specified by the xpath.
     *
     *  @param  xpath
     *  @param  required <code>true</code> if an error should be thrown
     *          if not found, <code>false</code> otherwise
     *  @throws org.osid.OperationFailedException if element not found
     *          and is required
     */

    public Element getElement(String xpath, boolean required)
        throws org.osid.OperationFailedException {

        return (getElement(this.document, xpath, required));
    }


    /**
     *  Gets an element specified by the xpath.
     *
     *  @param node
     *  @param  xpath
     *  @param  required <code>true</code> if an error should be thrown
     *          if not found, <code>false</code> otherwise
     *  @throws org.osid.OperationFailedException if element not found
     *          and is required
     */

    public Element getElement(Node node, String xpath, boolean required)
        throws org.osid.OperationFailedException {

        try {
            Dom4jXPath dxpath = new Dom4jXPath(xpath);
            dxpath.setNamespaceContext(this.ns);
            
            Node ret = (Node) dxpath.selectSingleNode(node);
            if ((ret == null) && required) {
                throw new org.osid.OperationFailedException (xpath + " not found in " 
                                                             + this.document.getName());
            }
            
            if (!(ret instanceof Element)) {
                if (!required) {
                    return (null);
                }

                throw new org.osid.OperationFailedException(xpath + " is not an element in " 
                                                            + this.document.getName());
            }

            return ((Element) ret);     
        } catch (Exception e) {
            throw new org.osid.OperationFailedException(e);
        }
    }


    /**
     *  Gets a list of nodes.
     *
     *  @param xpath
     */

    public java.util.Collection<Node> getNodes(String xpath) {
        return (getNodes(this.document, xpath));
    }


    /**
     *  Gets a list of nodes from the given node.
     *
     *  @param node
     *  @param xpath
     */

    public java.util.Collection<Node> getNodes(Node node, String xpath) {
        java.util.Collection<Node> ret = new java.util.ArrayList<Node>();

        try {
            Dom4jXPath dxpath = new Dom4jXPath(xpath);
            dxpath.setNamespaceContext(this.ns);        
            java.util.Collection<?> set = dxpath.selectNodes(node);        
            
            for (Object o : set) {
                ret.add((Node) o);
            }
        
            return (ret);
        } catch (Exception e) {
            return (ret);
        }
    }


    /**
     *  Gets the text content under the given xpath.
     *
     *  @return the text content
     *  @throws org.osid.OperationFailedException cannot access node
     */

    public String getContent(String xpath)
        throws org.osid.OperationFailedException {

        Element e = getElement(xpath, true);
        if (e == null) {
            return("");
        }

        return (trim(e.getText()));
    }


    /**
     *  Gets the text content for the given xpath under the given node.
     *
     *  @return the text content
     *  @throws org.osid.OperationFailedException cannot access node
     */

    public String getContent(Node node, String xpath)
        throws org.osid.OperationFailedException {

        Element e = getElement(node, xpath, true);
        if (e == null) {
            return("");
        }

        return (trim(e.getText()));
    }


    /**
     *  Gets the boolean attribute.
     *
     *  @param node
     *  @param attr
     *  @return boolean
     */

    public boolean getBooleanAttribute(Node node, String attr) {
        try {
            return (Boolean.parseBoolean(node.valueOf("@" + attr)));
        } catch (Exception e) {
            return (false);
        }
    }


    static char[] ws_chars = {' ', '\n', '\t', '\r', '\b'};


    /**
     *  Trims the <code>asXML()</code> content and removes the outer
     *  tags.
     *
     *  @return trimmed string
     */

    public String trim(String s) {
        if (s.indexOf('<') < 0) {
            return (s);
        }

        int i = s.indexOf('>');
        int j = s.lastIndexOf('<');
        
        if (i > j) {
            return ("");
        }

        String inner = s.substring(i+1, j);
        return (removeExtraWS(inner));
    }


    /**
     *  Removes extra whitespace.
     *
     *  @return string without extra whitespace
     */

    public String removeExtraWS(String s) {
        StringBuffer sb = new StringBuffer(s.length());
        boolean ws = true;

        for (int i = 0; i < s.length(); i++) {
            int j;
            for (j = 0; j < ws_chars.length; j++) {
                if (s.charAt(i) == ws_chars[j]) {
                    if (!ws) {
                        sb.append(" ");
                        ws = true;
                    }
                    
                    break;
                }
            }

            if (j == ws_chars.length) {
                ws = false;
            }

            if (!ws) {
                sb.append(s.charAt(i));
            }
        }

        for (int i = sb.length() - 1; i > 0; i--) {
            int j;
            for (j = 0; j < ws_chars.length; j++) {
                if (sb.charAt(i) == ws_chars[j]) {
                    sb.deleteCharAt(i);
                    break;
                }
            }
            
            if (j == ws_chars.length) {
                break;
            }
        }

        return (sb.toString());
    }
}



