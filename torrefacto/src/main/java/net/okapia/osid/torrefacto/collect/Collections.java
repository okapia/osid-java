//
// Collections.java
//
//     Collections utils.
//
//
// Tom Coppeto
// Okapia
// 20 September 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.torrefacto.collect;


/**
 *  Adds MultiMaps to Java Collections.
 */

public class Collections {


    /*
     * java.util.Collections has a private constructor so it can't be
     * extended.
     */

    protected Collections() {
        return;
    }


    /**
     *  Returns a synchronized (thread-safe) map backed by the
     *  specified map. In order to guarantee serial access, it is
     *  critical that all access to the backing map is accomplished
     *  through the returned map.
     *
     *  @param m the map to be "wrapped" in a synchronized map
     *  @return a synchronized view of the specified map
     */

    public static <K,V> MultiMap<K,V> synchronizedMultiMap(MultiMap<K,V> m) {
        return (new SynchronizedMultiMap<>(m));
    }


    /** 
     *  Returns an unmodifiable view of the specified map. This method
     *  allows modules to provide users with "read-only" access to
     *  internal maps. Query operations on the returned map "read
     *  through" to the specified map, and attempts to modify the
     *  returned map, whether direct or via its collection views,
     *  result in an UnsupportedOperationException.
     *
     *  @param m the map for which an unmodifiable view is to be returned
     *  @return an unmodifiable view of the specified map
     */

    public static <K,V> MultiMap<K,V> unmodifiableMultiMap(MultiMap<K,V> m) {
        return (new UnmodifiableMultiMap<>(m));
    }
}
