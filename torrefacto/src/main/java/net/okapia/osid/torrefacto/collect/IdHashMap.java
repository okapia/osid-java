//
// IdHashMap.java
//
//     Maintains a HashMap based on Id.
//
//
// Tom Coppeto
// Okapia
// 20 September 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.torrefacto.collect;


/**
 *  A HashMap based on an Id key.
 */

public class IdHashMap<V>
    extends java.util.LinkedHashMap<org.osid.id.Id, V>
    implements java.util.Map<org.osid.id.Id, V> {

    private static final long serialVersionUID = 996517901L;


    /**
     *  Constructs an empty <code>HashMap</code> with the specified
     *  initial capacity and load factor.
     *
     *  @param  initialCapacity the initial capacity
     *  @param  loadFactor      the load factor
     *  @throws IllegalArgumentException if the initial capacity is
     *          negative or the load factor is nonpositive
     */
  
    public IdHashMap(int initialCapacity, float loadFactor) {
        super(initialCapacity, loadFactor);
        return;
    }


    /**
     *  Constructs an empty <code>HashMap</code> with the specified
     *  initial capacity and the default load factor (0.75).
     *
     *  @param  initialCapacity the initial capacity.
     *  @throws IllegalArgumentException if the initial capacity is negative.
     */
    
   public IdHashMap(int initialCapacity) {
       super(initialCapacity);
       return;
   }

   
   /**
    *  Constructs an empty <code>HashMap</code> with the default initial capacity
    *  (16) and the default load factor (0.75).
    */

   public IdHashMap() {
       super();
       return;
   }


   /**
    *  Returns the value to which the specified key is mapped,
    *  or {@code null} if this map contains no mapping for the key.
    *
    *  More formally, if this map contains a mapping from a key {@code
    *  k} to a value {@code v} such that {@code (key==null ? k==null :
    *  key.equals(k))}, then this method returns {@code v}; otherwise
    *  it returns {@code null}.  (There can be at most one such
    *  mapping.)
    *
    *  A return value of {@code null} does not <i>necessarily</i>
    *  indicate that the map contains no mapping for the key; it's
    *  also possible that the map explicitly maps the key to {@code
    *  null}.  The {@link #containsKey containsKey} operation may be
    *  used to distinguish these two cases.
    *
    *  @see #put(Object, Object)
    */

   public V get(org.osid.id.Id key) {
       return (super.get(key));
   }


   /**
    *  Returns <code>true</code> if this map contains a mapping for the
    *  specified key.
    *
    *  @param key The key whose presence in this map is to be tested
    *  @return <code>true</code> if this map contains a mapping for
    *          the specified key.
    */

   public boolean containsKey(org.osid.id.Id key) {
       return (super.containsKey(key));
   }


   /**
    *  Associates the specified value with the specified key in this map.
    *  If the map previously contained a mapping for the key, the old
    *  value is replaced.
    *
    *  @param key key with which the specified value is to be associated
    *  @param value value to be associated with the specified key
    *  @return the previous value associated with <code>key</code>, or
    *          <code>null</code> if there was no mapping for
    *          <code>key</code>.  (A <code>null</code> return can also
    *          indicate that the map previously associated
    *          <code>null</code> with <code>key</code>.)
    */

   public V put(org.osid.id.Id key, V value) {
       return (super.put(key, value));
   }
}
