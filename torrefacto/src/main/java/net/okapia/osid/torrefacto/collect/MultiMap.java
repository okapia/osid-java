//
// MultiMap.java
//
//     Interface for MultiMaps.
//
//
// Tom Coppeto
// Okapia
// 20 September 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.torrefacto.collect;


/**
 *  The MultiMap interface maps keys to multiple values.
 */

public interface MultiMap<K,V> {

    /** 
     *  Tests if this map can be modified.
     *
     *  @return <code>true</code> if the map can be modified, 
     *          <code>false</code. if read-only
     */
    
    public boolean isModifiable();


    /** 
     *  Returns the number of key-value mappings in this map.
     *
     *  @return the number of key-value mappings in this map
     */
    
    public long size();
    
    
    /**
     *  Tests if this map contains no key-value mappings.
     *
     *  @return <code>true</code> if this map contains no key-value
     *          mappings, <code>false</code> otherwise
     */
    
    public boolean isEmpty();
    
    
    /**
     *  Tests if this map contains a mapping for the specified key.
     *
     *  @param key the key whose presence in this map is to be tested
     *  @return <code>true</code> if this map contains a mapping for
     *          the specified key, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException <code>key</code> is
     *          <code>null</code>
     */
    
    public boolean containsKey(K key);
    
    
    /**
     *  Tests the map contains the value specified.
     *
     *  @param value the value to search for
     *  @return <code>true</code> if the map contains the value,
     *          <code>false otherwise
     *  @throws org.osid.NullArgumentException <code>value</code> is
     *          <code>null</code>
     */
    
    public boolean containsValue(V value);
    
    
    /**
     *  Tests the map contains the key and value specified.
     *
     *  @param key the key
     *  @param value the value
     *  @return <code>true</code> if the map contains the key/value
     *          pair, <code>false otherwise
     *  @throws org.osid.NullArgumentException <code>key</code> or
     *          <code>value</code> is <code>null</code>
     */
    
    public boolean containsKeyValue(K key, V value);
    
    
    /**
     *  Gets the values to which the specified key is mapped. If the
     *  key is not defined, this method returns an empty list.
     *
     *  @param key the key
     *  @return a collection of the values for the given key or an
     *          empty collection if the key is not found
     *  @throws org.osid.NullArgumentException <code>key</code>
     *          is <code>null</code>
     */
    
    public java.util.Collection<V> get(K key);
    
    
    /**
     *  Associates the specified value with the specified key in this
     *  map.  If the map previously contained a mapping for the key,
     *  the old value is replaced.
     *
     *  @param key key with which the specified value is to be associated
     *  @param value value to be associated with the specified key
     *  @throws org.osid.NullArgumentException <code>key</code> or
     *          <code>value</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException map is not modifiable
     */
    
    public void put(K key, V value);


    /**
     *  Adds a collection of values to to a key in this map
     *
     *  @param key the key
     *  @param values the values
     *  @throws org.osid.NullArgumentException <code>key</code> or
     *          <code>values</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException map is not modifiable
     */

    public void putAll(K key, java.util.Collection<V> values);


    /**
     *  Stores a map of keys and values inside this map.
     *
     *  @param map the map to store
     *  @throws org.osid.NullArgumentException <code>map</code> is
     *          <code>null</code>
     *  @throws org.osid.UnsupportedException map is not modifiable
     */
    
    public void putAll(java.util.Map<K, java.util.Collection<V>> map);


    /**
     *  Stores a Multimap inside this map.
     *
     *  @param map the map to store
     *  @throws org.osid.NullArgumentException <code>map</code> is
     *          <code>null</code>
     *  @throws org.osid.UnsupportedException map is not modifiable
     */

    public void putAll(MultiMap<K, V> map);


    /**
     *  Removes the mapping for a key and all its related values.
     *
     *  @param key whose mapping is to be removed from the map
     *  @throws org.osid.NullArgumentException <code>key</code>
     *          is <code>null</code>
     *  @throws org.osid.UnsupportedException map is not modifiable
     */
    
    public void remove(K key);
    
    
    /**
     *  Removes a specific key/value pair from the map.
     * 
     *  The item is removed from the collection mapped to the specified
     *  key.  Other values attached to that key are unaffected.
     *
     *  @param key the key 
     *  @param value the value to remove
     *  @throws org.osid.NullArgumentException <code>key</code> or
     *         <code>value</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException map is not modifiable
     */
    
    public void remove(K key, V value);
    

    /**
     *  Removes a value.
     *
     *  @param value to be removed from the map
     *  @throws org.osid.NullArgumentException <code>value</code>
     *          is <code>null</code>
     */

    public void removeValue(V value);

    
    /**
     *  Removes all of the mappings from this map. The map will be
     *  empty after this call returns.
     *
     *  @throws org.osid.UnsupportedException map is not modifiable
     */
    
    public void clear();


    /**
     *  Returns an immutable Collection view of the keys contained in
     *  this map.
     *
     *  @return a set view of the keys contained in this map
     */

    public java.util.Collection<K> keySet();

   
    /**
     *  Returns a Collection view of the values contained in this
     *  map. 
     *
     *  @return a collection of the values
     */

    public java.util.Collection<V> values();


    /**
     *  Gets a Map of this MultiMap.
     *
     *  @return a map view of this MultiMap
     */

    public java.util.Map<K, java.util.Collection<V>> asMap();
}