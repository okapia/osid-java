package net.okapia.osid.torrefacto.util;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;


public class Sizeof {


    /**
     *  Gets the size of an Object.
     *
     *  @param obj the object
     *  @return the approximate memory size
     */

    public static <T> int object(Object obj) {
        if (obj == null) {
            return (0);
        }

        Class<?> c = obj.getClass();
        
        if (c.isArray()) {
            return (sizeofArray(obj, c, false));
        } else {
            return (sizeofClass(c));
        }
    }


    /**
     *  Gets the number of bytes of an Object. This method is used to
     *  estimate the number of bytes "transmitted" and includes Strings.
     *
     *  @param obj the object
     *  @return the approximate number of bytes
     */

    public static int bytes(Object obj) {
        if (obj == null) {
            return (0);
        }

        if (obj instanceof String) {
            return (((String) obj).length());
        }
            
        Class<?> c = obj.getClass();
        
        if (c.isArray()) {
            return (sizeofArray(obj, c, true));
        } else {
            if (c.equals(Void.class) ||        
                c.equals(Boolean.class) ||
                c.equals(Byte.class) ||
                c.equals(Character.class) ||
                c.equals(Double.class) ||
                c.equals(Float.class) ||
                c.equals(Integer.class) ||
                c.equals(Long.class) ||
                c.equals(Short.class)) {

                return (sizeofPrimitive(c));
            }

            return (sizeofClass(c));
        }
    }


    private static int sizeofArray(Object obj, Class<?> c, boolean bytes) {
        Class<?> ct = c.getComponentType();
        
        if (ct.isPrimitive()) {
            return (Array.getLength(obj) * sizeofPrimitive(ct));
        } else {
            int size = 0;
            for (int i = 0; i <  Array.getLength(obj); i++) {
                if (!bytes) { 
                    size += 4;
                }
                Object obj2 = Array.get(obj, i);
                if (bytes) {
                    size += bytes(obj2);
                } else {
                    size += object(obj2);
                }
            }
            
            return (size);
        }
    }


    private static int sizeofClass(Class<?> c) {
        int size = 0;
        
        for (Field field : c.getDeclaredFields()) {
            if (!c.isInterface() &&
                (field.getModifiers() & Modifier.STATIC) != 0)
                continue;
            size += sizeofPrimitive(field.getType());
        }
        
        if (c.getSuperclass() != null)
            size += sizeofClass(c.getSuperclass());
        
        for (Class<?> ci : c.getInterfaces()) {
            size += sizeofClass(ci);
        }
        
        return (size);
    }


    private static int sizeofPrimitive(Class<?> c) {
        if (c.equals(Void.class)) {
            return (0);
        }
        
        if (c.equals(Boolean.class)) {            
            return (1);
        }

        if (c.equals(Byte.class)) {
            return (1);
        }

        if (c.equals(Character.class)) {
            return (1);
        }

        if (c.equals(Double.class)) {
            return (8);
        }

        if (c.equals(Float.class)) {
            return (4);
        }

        if (c.equals(Integer.class)) {
            return (4);
        }

        if (c.equals(Long.class)) {
            return (8);
        }

        if (c.equals(Short.class)) {
            return (2);
        }

        return (4);
    }


    public static void main (String... args) {
        if (args.length != 1) {
            System.err.println(Sizeof.class.getName() + " <string>");
            System.exit(1);
        }

        System.out.println(bytes(new java.math.BigDecimal(args[0])));
    }
}