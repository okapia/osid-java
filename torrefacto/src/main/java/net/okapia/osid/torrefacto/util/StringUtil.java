//
// StringUtil.java
//
//     Fun with strings.
//
//
// Tom Coppeto
// Okapia
// 28 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.torrefacto.util;

import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Some string utilities.
 */

public class StringUtil {


    /**
     *  Gets an upper case article for the given word.
     *
     *  @param s a work or phrase
     *  @return the article
     *  @throws org.osid.NullArgumentException <code>s</code> is
     *          <code>null</code>
     */

    public static String getUpperArticle(String s) {
        nullarg(s, "s");

        if (isVowel(s.charAt(0))) {
            return ("An");
        } else {
            return ("A");
        }
    }


    /**
     *  Gets a lower case article for the given word.
     *
     *  @param s a work or phrase
     *  @return the article
     *  @throws org.osid.NullArgumentException <code>s</code> is
     *          <code>null</code>
     */

    public static String getLowerArticle(String s) {
        nullarg(s, "s");

        if (isVowel(s.charAt(0))) {
            return ("an");
        } else {
            return ("a");
        }
    }


    /**
     *  Capitalizes a string.
     *
     *  @param str a string
     *  @return the capitalized string
     *  @throws org.osid.NullArgumentException <code>str</code> is
     *          <code>null</code>
     */
    
    public static String capitalize(String str) {
        nullarg(str, "str");

        StringBuffer sb = new StringBuffer(str.length());
        boolean first = true;
        for (String s : str.split(" ")) {
            if (!first) {
                sb.append(' ');
            } else {
                first = false;
            }

            sb.append(s.substring(0, 1).toUpperCase());
            sb.append(s.substring(1));
        }

        return (sb.toString());
    }


    /**
     *  UnCapitalizes a String.
     *
     *  @param str a string
     *  @throws org.osid.NullArgumentException <code>str</code> is
     *          <code>
     */
    
    public static String uncapitalize(String str) {
        nullarg(str, "str");

        StringBuffer sb = new StringBuffer(str.length());
        boolean first = true;
        for (String s : str.split(" ")) {
            if (!first) {
                sb.append(' ');
            } else {
                first = false;
            }

            sb.append(s.substring(0, 1).toLowerCase());
            sb.append(s.substring(1));
        }

        return (sb.toString());
    }


    /**
     *  Turns a non-negative number into an ordinal string used to
     *  denote the position in an ordered sequence, such as 1st, 2nd,
     *  3rd, 4th.
     * 
     *  @param number the non-negative number
     *  @return the string with the number and ordinal suffix
     *  @throws org.osid.InvalidArgumentException <code>number</code>
     *          is negative
     */

    public static String ordinalize(long number) {
        cardinalarg(number, "number");

        String nstr = Long.toString(number);
        if (number == 0) {
            return (nstr);
        }

        nstr = java.text.NumberFormat.getNumberInstance().format(number);

        long remainder = number % 100;

        if ((number >= 11) && (number <= 13)) {
            return nstr + "th";
        }

        remainder = number % 10;

        if (remainder == 1) {
            return (nstr + "st");
        }

        if (remainder == 2) {
            return (nstr + "nd");
        }

        if (remainder == 3) {
            return (nstr + "rd");
        }

        return (nstr + "th");
    }


    /**
     *  Tests if a letter is a vowel.
     *
     *  @param c a character
     *  @return <code>true</code> if <code>c</code> is a vowelm
     *          <code>false</code> otherwise
     */

    public static boolean isVowel(char c) {
        if ((c == 'a') || (c == 'A') || (c == 'e') || (c == 'E') || (c == 'i') || (c == 'I') ||
            (c == 'o') || (c == 'O') || (c == 'u') || (c == 'U')) {
            return (true);
        } else {
            return (false);
        }
    }
}



