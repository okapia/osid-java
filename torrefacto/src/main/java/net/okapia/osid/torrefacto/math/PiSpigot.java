/*****************************************************************
  Copyright 2006 Rick Miller, Raffi Kasparian - Pulp Free Press 
     
   This source code accompanies the text Java For Artists and is
   provided for instructional purposes only. No warranty concerning
   the quality of this code is expressed or implied. You are free to 
   use this code in your programs so long as this copyright notice
   is included in its entirety.
*****************************************************************/

package net.okapia.osid.torrefacto.math;

import java.math.BigInteger;

public class PiSpigot {
    private static final BigInteger ZERO  = BigInteger.ZERO;
    private static final BigInteger ONE   = BigInteger.ONE;
    private static final BigInteger TWO   = BigInteger.valueOf(2);
    private static final BigInteger THREE = BigInteger.valueOf(3);
    private static final BigInteger FOUR  = BigInteger.valueOf(4);
    private static final BigInteger TEN   = BigInteger.valueOf(10);

    private BigInteger q = ONE;
    private BigInteger r = ZERO;
    private BigInteger t = ONE;
    private BigInteger k = ONE;
    private BigInteger n = ZERO;


    public int nextDigit() {
        while (true) {
            n = THREE.multiply(q).add(r).divide(t);
            if (FOUR.multiply(q).add(r).divide(t).equals(n)) {
                q = TEN.multiply(q);
                r = TEN.multiply(r.subtract(n.multiply(t)));
                return n.intValue();
            } else {
                BigInteger temp_q = q.multiply(k);
                BigInteger temp_2k_plus_1 = TWO.multiply(k).add(ONE);
                BigInteger temp_r = TWO.multiply(q).add(r).multiply(temp_2k_plus_1);
                BigInteger temp_t = t.multiply(temp_2k_plus_1);
                BigInteger temp_k = k.add(ONE);
                q = temp_q;
                r = temp_r;
                t = temp_t;
                k = temp_k;
            }
        }
    }


    public static void main (String... args) {
        PiSpigot pi = new PiSpigot();
        while (true) {
            System.out.print(pi.nextDigit());
        }
    }
}