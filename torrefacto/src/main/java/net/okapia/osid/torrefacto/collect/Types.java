//
// Types.java
//
//     Interface for type collections.
//
//
// Tom Coppeto
// Okapia
// 20 September 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.torrefacto.collect;


/**
 *  An interface for type collections.
 *
 *  This utility is designed to assist a provider in managing type
 *  support. 
 */

public interface Types
    extends Iterable<org.osid.type.Type> {

    /**
     *  Tracks a supported type.
     *
     *  @param type
     *  @throws org.osid.NullArgumentException
     *          <code>type</code> is <code>null</code>
     */

    public void add(org.osid.type.Type type);


    /**
     *  Removes a type.
     *
     *  @param type
     *  @throws org.osid.NullArgumentException
     *          <code>type</code> is <code>null</code>
     */

    public void remove(org.osid.type.Type type);


    /**
     *  Tests for the existience of a type.
     *
     *  @param type
     *  @return <code>true</code> if the type exists,
     *          <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>type</code> is <code>null</code>
     */

    public boolean contains(org.osid.type.Type type);


    /**
     *  Gets an iterable collection of the types.
     *
     *  @return a collection
     */

    public java.util.Collection<org.osid.type.Type> toCollection();


    /**
     *  Gets an array of the types.
     *
     *  @return an array of types
     */

    public org.osid.type.Type[] toArray();


    /**
     *  Removes all types.
     */
    
    public void clear();
}
