//
// TypeEnumSet.java
//
//     Manages a set of Types in an enumeration.
//
//
// Tom Coppeto
// Okapia
// 21 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.torrefacto.collect;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Manages a set of Types in an enumeration.
 */

public class TypeEnumSet <E extends Enum<E> & TypeEnum>
    extends TypeSet
    implements Types {
    
    private final java.util.Map<org.osid.type.Type, E> types = new java.util.HashMap<>();


    /**
     *  Constructs a new <code>TypeEnumSet</code>.
     *
     *  @param enumClass an enumeration of Types
     *  @throws org.osid.NullArgumentException <code>enum</code> 
     *          is <cdoe>null</code>
     */

    public TypeEnumSet(Class<E> enumClass) {
	nullarg(enumClass, "enumClass");

	for (E type : java.util.EnumSet.allOf(enumClass)) {
	    add(type.getType());
	    this.types.put(type.getType(), type);
	}

	return;
    }


    /**
     *  Gets the enumerated value for a type.
     *
     *  @param type
     *  @return enumerated value
     *  @throws org.osid.NullArgumentException <code>type</code> 
     *          is <cdoe>null</code>     
     *  @throws org.osid.UnsupportedException <code>type</code> 
     *          is not in enumeration
     */

    public E getEnum(org.osid.type.Type type) {
	nullarg(type, "type");

	E e = this.types.get(type);
	if (e != null) {
	    return (e);
	} else {
	    throw new org.osid.UnsupportedException(type + " is not supported");
	}
    }
}
