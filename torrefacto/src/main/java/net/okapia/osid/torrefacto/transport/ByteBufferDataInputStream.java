//
// ByteBufferDataInputStream.java
//
//     A DataINputStream backed by a ByteBuffer.
//
//
// Tom Coppeto
// Okapia
// 20 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.torrefacto.transport;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A DataInputStream backed by a Java ByteBuffer. 
 */

public class ByteBufferDataInputStream 
    implements org.osid.transport.DataInputStream {

    private final java.nio.ByteBuffer buffer;
    private boolean closed = false;

    
    /**
     *  Constructs a new <code>byteBufferDataInputStream</code>.
     *
     *  @param buffer a flipped byte buffer
     *  @throws org.osid.NullArgumentException <code>buffer</code> is
     *          <code>null</code>
     */

    public ByteBufferDataInputStream(java.nio.ByteBuffer buffer) {
        nullarg(buffer, "byte buffer");
        this.buffer = buffer;
        return;
    }


    /**
     *  Tests if the end of this stream has been reached. This may not
     *  be a permanent condition as more data may be available at a
     *  later time as in the case of tailing a file.
     *
     *  @return <code> true </code> if the end of this stream has been
     *          reached, <code> false </code> otherwise
     *  @throws org.osid.IllegalStateException this stream has been closed 
     */

    @OSID @Override
    public boolean atEndOfStream() {
        if (this.closed) {
            throw new org.osid.IllegalStateException("stream is closed");
        }

        if (this.buffer.remaining() == 0) {
            return (true);
        } else {
            return (false);
        }
    }


    /**
     *  Gets the number of <code> bytes </code> available for
     *  retrieval. The number returned by this method may be less than
     *  or equal to the total number of <code> bytes </code> in this
     *  stream.
     *
     *  @return the number of <code> bytes </code> available for retrieval 
     *  @throws org.osid.IllegalStateException this stream has been closed 
     */

    @OSID @Override
    public long available() {
        if (this.closed) {
            throw new org.osid.IllegalStateException("stream is closed");
        }

        return (this.buffer.remaining());
    }


    /**
     *  Skips a specified number of <code> bytes </code> in the stream. 
     *
     *  @param  n the number of <code> bytes </code> to skip 
     *  @return the actual number of <code> bytes </code> skipped 
     *  @throws org.osid.IllegalStateException this stream has been closed or 
     *          <code> atEndOfStream() </code> is <code> true </code> 
     */

    @OSID @Override
    public long skip(long n) {
        if (this.closed) {
            throw new org.osid.IllegalStateException("stream is closed");
        }

        if (atEndOfStream()) {
            throw new org.osid.IllegalStateException("stream is complete");
        }

        if (n > available()) {
            n = available(); // should constrain to maxint
        }
        
        this.buffer.position(this.buffer.position() + (int) n);
        return (n);
    }


    /**
     *  Reads a specified number of <code> bytes </code> from this stream. 
     *
     *  @param  buf the buffer in which the data is read 
     *  @param  n the number of <code> bytes </code> to read 
     *  @return the actual number of <code> bytes </code> read 
     *  @throws org.osid.IllegalStateException this stream has been closed or 
     *          <code> atEndOfStream() </code> is <code> true </code> 
     *  @throws org.osid.InvalidArgumentException the size of <code> buf 
     *          </code> is less than <code> n </code> 
     *  @throws org.osid.NullArgumentException <code> buf </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public long read(byte[] buf, long n)
        throws org.osid.OperationFailedException {
        
        nullarg(buf, "buf");
        if (buf.length < n) {
            throw new org.osid.InvalidArgumentException("buffer size too small");
        }

        if (this.closed) {
            throw new org.osid.IllegalStateException("stream is closed");
        }

        if (atEndOfStream()) {
            throw new org.osid.IllegalStateException("stream is complete");
        }

        if (n > available()) {
            n = available();
        }

        int position = this.buffer.position();
        this.buffer.get(buf, 0, (int) n);
        return (this.buffer.position() - position);
    }


    /**
     *  Closes this stream and frees up any allocated resources. Methods in 
     *  this object may not be invoked after this method is called. 
     *
     *  @throws org.osid.IllegalStateException this stream has been closed 
     */

    @OSID @Override
    public void close() {
        if (this.closed) {
            throw new org.osid.IllegalStateException("stream already closed");
        }

        this.closed = true;
        try {
            this.buffer.clear();
            this.buffer.compact();
        } catch (java.nio.ReadOnlyBufferException robe) {}

        return;
    }
}
