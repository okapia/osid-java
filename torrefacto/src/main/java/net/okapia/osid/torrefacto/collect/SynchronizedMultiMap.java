//
// SynchronizedMultiMap.java
//
//     A synchronized adapter for MultiMaps.
//
//
// Tom Coppeto
// Okapia
// 20 September 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.torrefacto.collect;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A synchronized adapter for MultiMap.
 */

public class SynchronizedMultiMap<K,V>
    implements MultiMap<K, V> {

    private final MultiMap<K,V> map;


    /**
     *  Constructs a new SynchronizedMultiMap.
     *
     *  @param map the multimap tp synchronize
     *  @throws org.osid.NullArgumentException <code>map</code> is
     *          <code>null</code>
     */

    public SynchronizedMultiMap(MultiMap<K,V> map) {
        nullarg(map, "map");
        this.map = map;
        return;
    }


    /** 
     *  Tests if this map can be modified.
     *
     *  @return <code>true</code> if the map can be modified, 
     *          <code>false</code. if read-only
     */
    
    @Override
    public boolean isModifiable() {
        return (this.map.isModifiable());
    }


    /** 
     *  Returns the number of key-value mappings in this map.
     *
     *  @return the number of key-value mappings in this map
     */

    @Override    
    public long size() {
        synchronized (this.map) {
            return (this.map.size());
        }
    }
    
    
    /**
     *  Tests if this map contains no key-value mappings.
     *
     *  @return <code>true</code> if this map contains no key-value
     *          mappings, <code>false</code> otherwise
     */
    
    @Override
    public boolean isEmpty() {
        synchronized (this.map) {
            return (this.isEmpty());
        }
    }
    
    
    /**
     *  Tests if this map contains a mapping for the specified key.
     *
     *  @param key the key whose presence in this map is to be tested
     *  @return <code>true</code> if this map contains a mapping for
     *          the specified key, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException <code>key</code> is
     *          <code>null</code>
     */

    @Override    
    public boolean containsKey(K key) {
        synchronized (this.map) {
            return (this.map.containsKey(key));
        }
    }
    
    
    /**
     *  Tests the map contains the value specified.
     *
     *  @param value the value to search for
     *  @return <code>true</code> if the map contains the value,
     *          <code>false otherwise
     *  @throws org.osid.NullArgumentException <code>value</code> is
     *          <code>null</code>
     */
    
    @Override
    public boolean containsValue(V value) {
        synchronized (this.map) {
            return (this.map.containsValue(value));
        }
    }
    
    
    /**
     *  Tests the map contains the key and value specified.
     *
     *  @param key the key
     *  @param value the value
     *  @return <code>true</code> if the map contains the key/value
     *          pair, <code>false otherwise
     *  @throws org.osid.NullArgumentException <code>key</code> or
     *          <code>value</code> is <code>null</code>
     */
    
    @Override
    public boolean containsKeyValue(K key, V value) {
        synchronized (this.map) {
            return (this.map.containsKeyValue(key, value));
        }
    }
    
    
    /**
     *  Gets the values to which the specified key is mapped. If the
     *  key is not defined, this method returns an empty list.
     *
     *  @param key the key
     *  @return a collection of the values for the given key or an
     *          empty collection if the key is not found
     *  @throws org.osid.NullArgumentException <code>key</code>
     *          is <code>null</code>
     */

    @Override    
    public java.util.Collection<V> get(K key) {
        synchronized (this.map) {
            return (this.map.get(key));
        }
    }
    
    
    /**
     *  Associates the specified value with the specified key in this
     *  map.  If the map previously contained a mapping for the key,
     *  the old value is replaced.
     *
     *  @param key key with which the specified value is to be associated
     *  @param value value to be associated with the specified key
     *  @throws org.osid.NullArgumentException <code>key</code> or
     *          <code>value</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException map is not modifiable
     */
    
    @Override
    public void put(K key, V value) {
        synchronized (this.map) {
            this.map.put(key, value);
        }

        return;
    }


    /**
     *  Adds a collection of values to to a key in this map
     *
     *  @param key the key
     *  @param values the values
     *  @throws org.osid.NullArgumentException <code>key</code> or
     *          <code>values</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException map is not modifiable
     */

    @Override
    public void putAll(K key, java.util.Collection<V> values) {
        synchronized (this.map) {
            this.map.putAll(key, values);
        }

        return;
    }


    /**
     *  Stores a map of keys and values inside this map.
     *
     *  @param map the map to store
     *  @throws org.osid.NullArgumentException <code>map</code> is
     *          <code>null</code>
     *  @throws org.osid.UnsupportedException map is not modifiable
     */

    @Override    
    public void putAll(java.util.Map<K, java.util.Collection<V>> map) {
        synchronized (this.map) {
            this.map.putAll(map);
        }

        return;
    }


    /**
     *  Stores a Multimap inside this map.
     *
     *  @param map the map to store
     *  @throws org.osid.NullArgumentException <code>map</code> is
     *          <code>null</code>
     *  @throws org.osid.UnsupportedException map is not modifiable
     */

    @Override
    public void putAll(MultiMap<K, V> map) {
        synchronized (this.map) {
            this.map.putAll(map);
        }

        return;
    }


    /**
     *  Removes the mapping for a key and all its related values.
     *
     *  @param key whose mapping is to be removed from the map
     *  @throws org.osid.NullArgumentException <code>key</code>
     *          is <code>null</code>
     *  @throws org.osid.UnsupportedException map is not modifiable
     */
    
    @Override
    public void remove(K key) {
        synchronized (this.map) {
            this.map.remove(key);
        }

        return;
    }
    
    
    /**
     *  Removes a value.
     *
     *  @param value to be removed from the map
     *  @throws org.osid.NullArgumentException <code>value</code>
     *          is <code>null</code>
     */

    @Override
    public void removeValue(V value) {
        synchronized (this.map) {
            this.map.removeValue(value);
        }

        return;
    }
        

    /**
     *  Removes a specific key/value pair from the map.
     * 
     *  The item is removed from the collection mapped to the specified
     *  key.  Other values attached to that key are unaffected.
     *
     *  @param key the key 
     *  @param value the value to remove
     *  @throws org.osid.NullArgumentException <code>key</code> or
     *         <code>value</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException map is not modifiable
     */
    
    @Override
    public void remove(K key, V value) {
        synchronized (this.map) {
            this.map.remove(key, value);
        }

        return;
    }
    
    
    /**
     *  Removes all of the mappings from this map. The map will be
     *  empty after this call returns.
     *
     *  @throws org.osid.UnsupportedException map is not modifiable
     */

    @Override    
    public void clear() {
        synchronized (this.map) {
            this.map.clear();
        }

        return;
    }


    /**
     *  Returns an immutable Collection view of the keys contained in
     *  this map.
     *
     *  @return a set view of the keys contained in this map
     */

    @Override
    public java.util.Collection<K> keySet() {
        synchronized (this.map) {
            return (this.map.keySet());
        }
    }

   
    /**
     *  Returns a Collection view of the values contained in this
     *  map. 
     *
     *  @return a collection of the values
     */

    @Override
    public java.util.Collection<V> values() {
        synchronized (this.map) {
            return (this.map.values());
        }
    }


    /**
     *  Gets a Map of this MultiMap.
     *
     *  @return a map view of this MultiMap
     */

    @Override
    public java.util.Map<K, java.util.Collection<V>> asMap() {
        synchronized (this.map) {
            return (this.map.asMap());
        }
    }
}