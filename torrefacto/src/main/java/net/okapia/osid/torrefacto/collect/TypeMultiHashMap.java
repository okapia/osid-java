//
// TypeMultiHashMap.java
//
//     Maintains a MultiHashMap based on Type.
//
//
// Tom Coppeto
// Okapia
// 20 September 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.torrefacto.collect;


/**
 *  A HashMap based on an Type key.
 */

public class TypeMultiHashMap<V>
    extends MultiHashMap<org.osid.type.Type, V>
    implements MultiMap<org.osid.type.Type, V> {

    private static final long serialVersionUID = 1177038478L;


    /**
     *  Constructs an empty <code>MultiHashMap</code> with the default
     *  initial capacity (16) and the default load factor (0.75).
     */
    
    public TypeMultiHashMap() {
        super();
        return;
    }
    

    /**
     *  Constructs an empty <code>MultiHashMap</code> with the
     *  specified initial capacity and the default load factor (0.75).
     *
     *  @param  initialCapacity the initial capacity.
     *  @throws IllegalArgumentException if the initial capacity is negative.
     */
    
    public TypeMultiHashMap(int initialCapacity) {
        super(initialCapacity);
        return;
    }
    
    
    /**
     *  Constructs an empty <code>MultiHashMap</code> with the
     *  specified initial capacity and load factor.
     *
     *  @param  initialCapacity the initial capacity
     *  @param  loadFactor      the load factor
     *  @throws IllegalArgumentException if the initial capacity is
     *          negative or the load factor is nonpositive
     */
    
    public TypeMultiHashMap(int initialCapacity, float loadFactor) {
	super(initialCapacity, loadFactor);
	return;
    }
    
    
    /**
     *  Returns <code>true</code> if this map contains a mapping for the
     *  specified key.
     *
     *  @param key The key whose presence in this map is to be tested
     *  @return <code>true</code> if this map contains a mapping for
     *          the specified key.
     */
    
    @Override
    public boolean containsKey(org.osid.type.Type key) {
        return (super.containsKey(key));
    }
    

    /**
     *  Tests the map contains the key and value specified.
     *
     *  @param key the key
     *  @param value the value
     *  @return <code>true</code> if the map contains the key/value
     *          pair, <code>false otherwise
     *  @throws org.osid.NullArgumentException <code>key</code> or
     *          <code>value</code> is <code>null</code>
     */
    
    @Override
    public boolean containsKeyValue(org.osid.type.Type key, V value) {
        return (super.containsKeyValue(key, value));
    }    


    /**
     *  Gets the values to which the specified key is mapped. If the
     *  key is not defined, this method returns an empty list.
     *
     *  @param key the key
     *  @return a collection of the values for the given key or an
     *          empty collection if the key is not found
     *  @throws org.osid.NullArgumentException <code>key</code>
     *          is <code>null</code>
     */
    
    @Override
    public java.util.Collection<V> get(org.osid.type.Type key) {
        return (super.get(key));
    }
    
    
    /**
     *  Associates the specified value with the specified key in this
     *  map.  If the map previously contained a mapping for the key,
     *  the old value is replaced.
     *
     *  @param key key with which the specified value is to be associated
     *  @param value value to be associated with the specified key
     *  @throws org.osid.NullArgumentException <code>key</code> or
     *          <code>value</code> is <code>null</code>
     */

    @Override
    public void put(org.osid.type.Type key, V value) {
        super.put(key, value);
    }


    /**
     *  Adds a collection of values to to a key in this map
     *
     *  @param key the key
     *  @param values the values
     *  @throws org.osid.NullArgumentException <code>key</code> or
     *          <code>values</code> is <code>null</code>
     */

    @Override
    public void putAll(org.osid.type.Type key, java.util.Collection<V> values) {
        super.putAll(key, values);
        return;
    }        


    /**
     *  Stores a map of keys and values inside this map.
     *
     *  @param map the map to store
     *  @throws org.osid.NullArgumentException <code>map</code> is
     *          <code>null</code>
     */
    
    @Override
    public void putAll(java.util.Map<org.osid.type.Type, java.util.Collection<V>> map) {
        super.putAll(map);
        return;
    }


    /**
     *  Stores a Multimap inside this map.
     *
     *  @param map the map to store
     *  @throws org.osid.NullArgumentException <code>map</code> is
     *          <code>null</code>
     */

    @Override
    public void putAll(MultiMap<org.osid.type.Type, V> map) {
        super.putAll(map);
        return;
    }


    /**
     *  Removes the mapping for a key and all its related values.
     *
     *  @param key whose mapping is to be removed from the map
     *  @throws org.osid.NullArgumentException <code>key</code>
     *          is <code>null</code>
     */
    
    @Override
    public void remove(org.osid.type.Type key) {
        super.remove(key);
    }
    
    
    /**
     *  Removes a specific key/value pair from the map.
     * 
     *  The item is removed from the collection mapped to the specified
     *  key.  Other values attached to that key are unaffected.
     *
     *  @param key the key 
     *  @param value the value to remove
     *  @throws org.osid.NullArgumentException <code>key</code> or
     *          <code>value</code> is <code>null</code>
     */
    
    @Override
    public void remove(org.osid.type.Type key, V value) {
        super.remove(key, value);
        return;
    }


    /**
     *  Returns an immutable Collection view of the keys contained in
     *  this map.
     *
     *  @return a set view of the keys contained in this map
     */

    @Override
    public java.util.Collection<org.osid.type.Type> keySet() {
        return (super.keySet());
    }


    /**
     *  Gets a Map of this MultiMap.
     *
     *  @return a map view of this MultiMap
     */

    @Override
    public java.util.Map<org.osid.type.Type, java.util.Collection<V>> asMap() {
        return (super.asMap());
    }
}
