//
// MethodCheck.java
//
//     Helps reduce method clutter.
//
//
// Tom Coppeto
// OnTapSolutions
// 20 September 2008
//
//
// Copyright (c) 2008 Massachusetts Institute of Technology. All Rights 
// Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.torrefacto.util;


/**
 *  Utility to help reduce method clutter in checking state
 *  and arguments.
 */

public class MethodCheck {

    private static final java.math.BigDecimal ZERO    = new java.math.BigDecimal(0);
    private static final java.math.BigDecimal HUNDRED = new java.math.BigDecimal(100);


    /**
     *  Throws an error if the given object is <code>null</code>.
     *
     *  @param o an object
     *  @param name the name of the parameter to include in the exception
     *  @throws org.osid.NullArgumentException <code>o</code> is 
     *  <code>null</code>
     */

    public final static void nullarg(Object o, String name) {
	if (o == null) {
	    if (name == null) {
		throw new org.osid.NullArgumentException("null everywhere");
	    } else {
		throw new org.osid.NullArgumentException(name + " is null");
	    }
	}
	
	return;
    }


    /**
     *  Checks that the supplied number is a cardinal value.
     *
     *  @param number the value to check
     *  @param name the name of the parameter to include in the exception
     *  @throws org.osid.InvalidArgumentException if
     *          <code>number</code> is less than zero
     *  @throws org.osid.NullArgumentException <code>name</code> is
     *          null
     */

    public final static void cardinalarg(long number, String name) {
	nullarg(name, "name");

	if (number < 0) {
	    throw new org.osid.InvalidArgumentException(name + " is not a cardinal number");
	}

	return;
    }


    /**
     *  Checks that the supplied number is a cardinal value.
     *
     *  @param number the value to check
     *  @param name the name of the parameter to include in the exception
     *  @throws org.osid.InvalidArgumentException if <code>number</code>
     *          is less than zero
     *  @throws org.osid.NullArgumentException <code>number</code> or
     *          <code>name</code> is null
     */

    public final static void cardinalarg(java.math.BigDecimal number, String name) {
	nullarg(number, "number");
	nullarg(name, "name");

	if (number.compareTo(ZERO) < 0) {
	    throw new org.osid.InvalidArgumentException(name + " is not a cardinal number");
	}

	return;
    }


    /**
     *  Checks the supplied cardinal range.
     *
     *  @param low the low end of range
     *  @param high the high end of range
     *  @param lowName the name of the parameter to include in the exception
     *  @param highName the name of the parameter to include in the exception
     *  @throws org.osid.InvalidArgumentException if <code>low</code>
     *          or <code>high</code> is less than zero, or
     *          <code>low</code> is greater than <code>high</code>
     *  @throws org.osid.NullArgumentException <code>lowName</code> or
     *          <code>highName</code> is null
     */

    public final static void cardinalrangearg(long low, long high, String lowName, String highName) {
	nullarg(lowName, "low name");
	nullarg(highName, "high name");

        cardinalarg(low, lowName);
        cardinalarg(high, highName);

	if (low > high) {
	    throw new org.osid.InvalidArgumentException(low + " is greater than " + high);
	}

	return;
    }


    /**
     *  Checks the supplied decimal range.
     *
     *  @param low the low end of range
     *  @param high the high end of range
     *  @param lowName the name of the parameter to include in the exception
     *  @param highName the name of the parameter to include in the exception
     *  @throws org.osid.InvalidArgumentException if <code>low</code>
     *          or <code>high</code> is less than zero, or
     *          <code>low</code> is greater than <code>high</code>
     *  @throws org.osid.NullArgumentException <code>low</code>,
     *          <code>lowName</code>, <codehigh</code>, or
     *          <code>highName</code> is null
     */

    public final static void decimalrangearg(java.math.BigDecimal low, java.math.BigDecimal high, 
                                             String lowName, String highName) {
        nullarg(low, "low");
	nullarg(lowName, "low name");
        nullarg(high, "high");
	nullarg(highName, "high name");

        cardinalarg(low, lowName);
        cardinalarg(high, highName);

	if (low.compareTo(high) > 0) {
	    throw new org.osid.InvalidArgumentException(low + " is greater than " + high);
	}

	return;
    }


    /**
     *  Checks the supplied integer range.
     *
     *  @param low the low end of range
     *  @param high the high end of range
     *  @param lowName the name of the parameter to include in the exception
     *  @param highName the name of the parameter to include in the exception
     *  @throws org.osid.InvalidArgumentException if <code>low</code>
     *          is greater than <code>high</code>
     *  @throws org.osid.NullArgumentException <code>lowName</code> or
     *          <code>highName</code> is null
     */

    public final static void integerrangearg(long low, long high, String lowName, String highName) {
	nullarg(lowName, "low name");
	nullarg(highName, "high name");

	if (low > high) {
	    throw new org.osid.InvalidArgumentException(low + " is greater than " + high);
	}

	return;
    }


    /**
     *  Checks that the supplied number is between 0-100.
     *
     *  @param number the value to check
     *  @param name the name of the parameter to include in the exception
     *  @throws org.osid.InvalidArgumentException if <code>number</code>
     *          is out of range
     *  @throws org.osid.NullArgumentException <code>name</code> is
     *          null
     */

    public final static void percentarg(long number, String name) {
	nullarg(name, "name");

	if ((number < 0) || (number > 100)) {
	    throw new org.osid.InvalidArgumentException(name + " is out of range");
	}

	return;
    }


    /**
     *  Checks that the supplied number is between 0-100.
     *
     *  @param number the value to check
     *  @param name the name of the parameter to include in the exception
     *  @throws org.osid.InvalidArgumentException if <code>number</code>
     *          is <code>null</code> or less than zero
     *  @throws org.osid.NullArgumentException <code>number</code> or
     *          <code>name</code> is null
     */

    public final static void percentarg(java.math.BigDecimal number, String name) {
	nullarg(number, "number");
	nullarg(name, "name");

	if ((number.compareTo(ZERO) < 0) || (number.compareTo(HUNDRED) > 0)) {
	    throw new org.osid.InvalidArgumentException(name + " is out of range");
	}

	return;
    }


    /**
     *  Checks that a value is not null.
     *
     *  @param var the value to check
     *  @param name the name of the parameter to include in the exception
     *  @throws org.osid.BadLogicException if <code>var</code> has not
     *  been set
     *  @throws org.osid.NullArgumentException <code>var</code> or
     *          <code>name</code> is null
     */

    public final static void unsetvar(Object var, String name) {
	nullarg(name, "name");

	if (var == null) {
	    throw new org.osid.BadLogicException(name + " has not been set");
	}

	return;
    }
}