#!/bin/sh
#
# Validates XML files.
#

usage() {
    echo "$0 <schema file> <xml file>"
    exit 1
}

schema=$1
xml=$2

if [ ! $# -eq 2 -o ! "${schema}" -o ! "${xml}" ]; then
    usage
fi

java -jar `dirname $0`/../lib/jing.jar -c ${schema} ${xml}



   