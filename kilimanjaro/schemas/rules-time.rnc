#
# rules-time.rnc
#
#    The schema for the org.osid.runtime.providers.rules.time
#    Rules OSID Provider.
#
#
# Tom Coppeto
# 29 January 2010
#
# Copyright (c) 2010 Massachusetts Institute of Technology. All Rights 
# Reserved.
#
#      Permission is hereby granted, free of charge, to any person
#      obtaining a copy of this software and associated documentation
#      files (the "Software"), to deal in the Software without
#      restriction, including without limitation the rights to use,
#      copy, modify, merge, publish, distribute, sublicesne, and/or
#      sell copies of the Software, and to permit the persons to whom the
#      Software is furnished to do so, subject the following conditions:
#
#      The above copyright notice and this permission notice shall be
#      included in all copies or substantial portions of the Software.
#
#      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
#      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
#      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
#      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
#      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
#      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
#      DEALINGS IN THE SOFTWARE.
#

default namespace = "urn:inet:osid.org:schemas:providers/rules/time/1"
start = engineType

include "xhtml.rnc" 

engineType = element engine {
    attribute lastmod { xsd:date | xsd:dateTime },
    element displayName { inline-markup },
    element description { block-markup },
    (ruleType)*
}


ruleType = element rule {
    attribute id { token },
    element displayName { inline-markup },
    element description { block-markup },
    (termType)*
}


termType = element term {
    attribute op { "and" | "or" }?,
    attribute negate { xsd:boolean }?,
    (timeRangeType | dateRangeType | dayofWeekType)?
}


timeRangeType = element timeRange {
    element start { xsd:time },
    element end { xsd:time}
}


dateRangeType = element dateRange {
    element start { xsd:date },
    element end { xsd:end }
}


dayOfWeekType = element weekday {
    attribute day { weekdayType }
}


weekdayType = ("Monday" | "Mon" | "M" | "Tuesday" | "Tues" | "T" | 
               "Wednesday" | "Wed" | "W" | "Thursday" | "Thu" | "R" | 
               "Friday" | "Fri" | "F" | "Saturday" | "Sat" | "A" |
               "Sunday" | "Sun" | "S")