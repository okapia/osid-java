//
// TeamResourceQueryRecord.java
//
//     A test resource service.
//
//
// Tom Coppeto
// Okapia
// 4 December 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.test.providers.resource.baseball;


public interface TeamResourceQueryRecord 
    extends org.osid.resource.records.ResourceQueryRecord {

    public static final org.osid.type.Type TYPE = TeamResourceRecord.TYPE;


    /**
     *  Matches teams in a league.
     *
     *  @param league the league
     *  @param stringMatchType a string match type
     *  @param match {@code true} for a positive match, {@code false}
     *         for a negative match
     *  @throws org.osid.NullArgumentException {@code league} or
     *          {@code stringMatchType} is {@code null}
     *  @throws org.osid.UnsupportedException {@code
     *          supportsStringMatchType(stringMatchType)} is {@code
     *          false}
     */


    public void matchLeague(String league, 
                            org.osid.type.Type stringMatchType,
                            boolean match);

    /**
     *  Matches teams as part of any league.
     *
     *  @param match {@code true} for a positive match, {@code false}
     *         for a negative match
     */

    public void matchAnyLeague(boolean match);


    /**
     *  Clears the league terms.
     */

    public void clearLeagueTerms();


    /**
     *  Matches teams in a division.
     *
     *  @param division the division
     *  @param stringMatchType a string match type
     *  @param match {@code true} for a positive match, {@code false}
     *         for a negative match
     *  @throws org.osid.NullArgumentException {@code division} or
     *          {@code stringMatchType} is {@code null}
     *  @throws org.osid.UnsupportedException {@code
     *          supportsStringMatchType(stringMatchType)} is {@code
     *          false}
     */

    public void matchDivision(String division, 
                              org.osid.type.Type stringMatchType,
                              boolean match);

    /**
     *  Matches teams as part of any league.
     *
     *  @param match {@code true} for a positive match, {@code false}
     *         for a negative match
     */

    public void matchAnyDivision(boolean match);


    /**
     *  Clears the division terms.
     */

    public void clearDivisionTerms();


    /**
     *  Matches teams founded within the given date range inclusive.
     *
     *  @param start start of date range
     *  @param end end of date range
     *  @param match {@code true} for a positive match, {@code false}
     *         for a negative match
     *  @throws org.osid.InvalidArgumentException {@code start} is
     *          greater than {@code end}
     *  @throws org.osid.NullArgumentException {@code start} or {@code
     *          end} is {@code null}
     */

    public void matchFounded(org.osid.calendaring.DateTime start,
                             org.osid.calendaring.DateTime end,
                             boolean match);

    /**
     *  Matches teams with any founded date.
     *
     *  @param match {@code true} for a positive match, {@code false}
     *         for a negative match
     */

    public void matchAnyFounded(boolean match);

    
    /**
     *  Clears the founded terms.
     */

    public void clearFoundedTerms();


    /**
     *  Matches teams who won championshops within the given range
     *  inclusive.
     *
     *  @param low start of range
     *  @param high end of range
     *  @param match {@code true} for a positive match, {@code false}
     *         for a negative match
     *  @throws org.osid.InvalidArgumentException {@code low} is
     *          greater than {@code high}
     */

    public void matchChampionships(long low, long high, boolean match);


    /**
     *  Matches teams with any championships set.
     *
     *  @param match {@code true} for a positive match, {@code false}
     *         for a negative match
     */

    public void matchAnyChampionships(boolean match);

    
    /**
     *  Clears the championship terms.
     */

    public void clearChampionshipsTerms();


    /**
     *  for testing
     */

    public String getNull();
}
