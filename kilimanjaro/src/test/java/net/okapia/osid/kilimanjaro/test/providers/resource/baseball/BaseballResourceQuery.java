//
// BaseballResourceQuery.java
//
//     A test resource service.
//
//
// Tom Coppeto
// Okapia
// 4 December 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.test.providers.resource.baseball;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDRecord;


/**
 *  no abstract class exists for ResourceQueries....
 */

public class BaseballResourceQuery
    extends net.okapia.osid.jamocha.query.spi.AbstractOsidObjectQuery
    implements org.osid.resource.ResourceQuery,
               TeamResourceQueryRecord,
               net.okapia.osid.jamocha.inline.filter.resource.resource.ResourceFilter {

    private final java.util.Collection<org.osid.search.terms.StringTerm> leagueTerms = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.search.terms.StringTerm> divisionTerms = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.search.terms.DateTimeRangeTerm> foundedTerms = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.search.terms.IntegerRangeTerm> championshipTerms = new java.util.LinkedHashSet<>();


    /**
     *  Constructs a new {@code BaseballResourceQuery}.
     */

    BaseballResourceQuery() {
        super(new net.okapia.osid.jamocha.query.BasicTermFactory());
        addRecordType(TeamResourceQueryRecord.TYPE);
        return;
    }


    protected boolean doIBelongHere() {
        return (true);
    }


    /**
     *  Tests if the given type is implemented by this record. Other
     *  types than that directly indicated by <code> getType() </code>
     *  may be supported through an inheritance scheme where the given
     *  type specifies a record that is a parent interface of the
     *  interface specified by <code> getType(). </code>
     *
     *  @param  recordType a type
     *  @return <code> true </code> if the given record <code> Type
     *          </code> is implemented by this record, <code> false
     *          </code> otherwise
     *  @throws org.osid.NullArgumentException <code> recordType </code> is
     *          <code> null </code>
     */

    @OSID @Override
    public boolean implementsRecordType(org.osid.type.Type recordType) {
        return (TeamResourceQueryRecord.TYPE.equals(recordType));
    }

    
    /**
     *  Gets the record corresponding to the given resource query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a resource implementing the requested record.
     *
     *  @param recordType a resource record type
     *  @return the resource query record
     *  @throws org.osid.NullArgumentException
     *          <code>resourceRecordType</code> is
     *  <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(resourceRecordType)</code>
     *          is <code>false</code>
     */

    public org.osid.resource.records.ResourceQueryRecord getResourceQueryRecord(org.osid.type.Type recordType)
        throws org.osid.OperationFailedException {

        if (implementsRecordType(recordType)) {
            return (this);
        }

        throw new org.osid.UnsupportedException(recordType + " not supported");
    }

    
    /**
     *  Matches resources that are also groups. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchGroup(boolean match) {
        return;
    }


    /**
     *  Clears the group terms. 
     */

    @OSID @Override
    public void clearGroupTerms() {
        return;
    }


    /**
     *  Matches resources that are also demographics. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchDemographic(boolean match) {
        return;
    }


    /**
     *  Clears the demographic terms. 
     */

    @OSID @Override
    public void clearDemographicTerms() {
        return;
    }


    /**
     *  Sets the group <code> Id </code> for this query to match resources 
     *  within the given group. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchContainingGroupId(org.osid.id.Id resourceId, 
                                       boolean match) {
        return;
    }


    /**
     *  Clears the group <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearContainingGroupIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available for querying 
     *  containing groups. 
     *
     *  @return <code> true </code> if a group resource query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContainingGroupQuery() {
        return (false);
    }


    /**
     *  Gets the query for a a containing group. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContainingGroupQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getContainingGroupQuery() {
        throw new org.osid.UnimplementedException("supportsContainingGroupQuery() is false");
    }


    /**
     *  Matches resources inside any group. 
     *
     *  @param  match <code> true </code> to match any containing group, 
     *          <code> false </code> to match resources part of no groups 
     */

    @OSID @Override
    public void matchAnyContainingGroup(boolean match) {
        return;
    }


    /**
     *  Clears the containing group terms. 
     */

    @OSID @Override
    public void clearContainingGroupTerms() {
        return;
    }


    /**
     *  Sets the asset <code> Id </code> for this query. 
     *
     *  @param  assetId the asset <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assetId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAvatarId(org.osid.id.Id assetId, boolean match) {
        return;
    }


    /**
     *  Clears the asset <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAvatarIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AssetQuery </code> is available. 
     *
     *  @return <code> true </code> if an asset query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAvatarQuery() {
        return (false);
    }


    /**
     *  Gets the query for an asset. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the asset query 
     *  @throws org.osid.UnimplementedException <code> supportsAvatarQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetQuery getAvatarQuery() {
        throw new org.osid.UnimplementedException("supportsAvatarQuery() is false");
    }


    /**
     *  Matches resources with any asset. 
     *
     *  @param  match <code> true </code> to match any asset, <code> false 
     *          </code> to match resources with no asset 
     */

    @OSID @Override
    public void matchAnyAvatar(boolean match) {
        return;
    }


    /**
     *  Clears the asset terms. 
     */

    @OSID @Override
    public void clearAvatarTerms() {
        return;
    }


    /**
     *  Sets the agent <code> Id </code> for this query. 
     *
     *  @param  agentId the agent <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAgentId(org.osid.id.Id agentId, boolean match) {
        return;
    }


    /**
     *  Clears the agent <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAgentIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgentQuery() {
        return (false);
    }


    /**
     *  Gets the query for an agent. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> supportsAgentQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getAgentQuery() {
        throw new org.osid.UnimplementedException("supportsAgentQuery() is false");
    }


    /**
     *  Matches resources with any agent. 
     *
     *  @param  match <code> true </code> to match any agent, <code> false 
     *          </code> to match resources with no agent 
     */

    @OSID @Override
    public void matchAnyAgent(boolean match) {
        return;
    }


    /**
     *  Clears the agent terms. 
     */

    @OSID @Override
    public void clearAgentTerms() {
        return;
    }


    /**
     *  Sets the resource relationship <code> Id </code> for this query. 
     *
     *  @param  resourceRelationshipId the resource relationship <code> Id 
     *          </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceRelationshipId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchResourceRelationshipId(org.osid.id.Id resourceRelationshipId, 
                                            boolean match) {
        return;
    }


    /**
     *  Clears the resource relationship <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearResourceRelationshipIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceRelationshipQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource relationship query is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceRelationshipQuery() {
        return (false);
    }


    /**
     *  Gets the query for aa resource relationship. Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the resource relationship query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRelationshipQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipQuery getResourceRelationshipQuery() {
        throw new org.osid.UnimplementedException("supportsResourceRelationshipQuery() is false");
    }


    /**
     *  Matches resources with any resource relationship. 
     *
     *  @param  match <code> true </code> to match any resource relationship, 
     *          <code> false </code> to match resources with no relationship 
     */

    @OSID @Override
    public void matchAnyResourceRelationship(boolean match) {
        return;
    }


    /**
     *  Clears the resource relationship terms. 
     */

    @OSID @Override
    public void clearResourceRelationshipTerms() {
        return;
    }


    /**
     *  Sets the bin <code> Id </code> for this query. 
     *
     *  @param  binId the bin <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchBinId(org.osid.id.Id binId, boolean match) {
        return;
    }


    /**
     *  Clears the bin <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBinIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BinQuery </code> is available. 
     *
     *  @return <code> true </code> if a bin query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBinQuery() {
        return (false);
    }


    /**
     *  Gets the query for a bin. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the bin query 
     *  @throws org.osid.UnimplementedException <code> supportsBinQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.BinQuery getBinQuery() {
        throw new org.osid.UnimplementedException("supportsBinQuery() is false");
    }


    /**
     *  Clears the bin terms. 
     */

    @OSID @Override
    public void clearBinTerms() {
        return;
    }


    /**
     *  Matches teams in a league.
     *
     *  @param league the league
     *  @param stringMatchType a string match type
     *  @param match {@code true} for a positive match, {@code false}
     *         for a negative match
     *  @throws org.osid.NullArgumentException {@code league} or
     *          {@code stringMatchType} is {@code null}
     *  @throws org.osid.UnsupportedException {@code
     *          supportsStringMatchType(stringMatchType)} is {@code
     *          false}
     */

    @OSIDRecord @Override
    public void matchLeague(String league, 
                            org.osid.type.Type stringMatchType,
                            boolean match) {
        this.leagueTerms.add(getTermFactory().createStringTerm(league, stringMatchType,
                                                               match));
        return;
    }


    /**
     *  Matches teams as part of any league.
     *
     *  @param match {@code true} for a positive match, {@code false}
     *         for a negative match
     */

    @OSIDRecord @Override
    public void matchAnyLeague(boolean match) {
        this.leagueTerms.add(getTermFactory().createStringWildcardTerm(match));
        return;
    }


    /**
     *  Clears the league terms.
     */

    @OSIDRecord @Override
    public void clearLeagueTerms() {
        this.leagueTerms.clear();
        return;
    }


    /**
     *  Gets all the league query terms.
     *
     *  @return a collection of the league query terms
     */

    protected java.util.Collection<org.osid.search.terms.StringTerm> getLeagueTerms() {
        return (java.util.Collections.unmodifiableCollection(this.leagueTerms));
    }


    /**
     *  Matches teams in a division.
     *
     *  @param division the division
     *  @param stringMatchType a string match type
     *  @param match {@code true} for a positive match, {@code false}
     *         for a negative match
     *  @throws org.osid.NullArgumentException {@code division} or
     *          {@code stringMatchType} is {@code null}
     *  @throws org.osid.UnsupportedException {@code
     *          supportsStringMatchType(stringMatchType)} is {@code
     *          false}
     */

    @OSIDRecord @Override
    public void matchDivision(String division, 
                              org.osid.type.Type stringMatchType,
                              boolean match) {
        this.divisionTerms.add(getTermFactory().createStringTerm(division, 
                                                                 stringMatchType,
                                                                 match));
        return;
    }


    /**
     *  Matches teams as part of any division.
     *
     *  @param match {@code true} for a positive match, {@code false}
     *         for a negative match
     */

    @OSIDRecord @Override
    public void matchAnyDivision(boolean match) {
        this.divisionTerms.add(getTermFactory().createStringWildcardTerm(match));
        return;
    }


    /**
     *  Clears the division terms.
     */

    @OSIDRecord @Override
    public void clearDivisionTerms() {
        this.divisionTerms.clear();
        return;
    }


    /**
     *  Gets all the division query terms.
     *
     *  @return a collection of the division query terms
     */

    protected java.util.Collection<org.osid.search.terms.StringTerm> getDivisionTerms() {
        return (java.util.Collections.unmodifiableCollection(this.divisionTerms));
    }


    /**
     *  Matches teams founded within the given date range inclusive.
     *
     *  @param start start of date range
     *  @param end end of date range
     *  @param match {@code true} for a positive match, {@code false}
     *         for a negative match
     *  @throws org.osid.InvalidArgumentException {@code start} is
     *          greater than {@code end}
     *  @throws org.osid.NullArgumentException {@code start} or {@code
     *          end} is {@code null}
     */

    @OSIDRecord @Override
    public void matchFounded(org.osid.calendaring.DateTime start,
                             org.osid.calendaring.DateTime end,
                             boolean match) {
        this.foundedTerms.add(getTermFactory().createDateTimeRangeTerm(start, 
                                                                       end, 
                                                                       match));
        return;
    }


    /**
     *  Matches teams with any founded date.
     *
     *  @param match {@code true} for a positive match, {@code false}
     *         for a negative match
     */

    @OSIDRecord @Override
    public void matchAnyFounded(boolean match) {
        this.foundedTerms.add(getTermFactory().createDateTimeRangeWildcardTerm(match));
        return;
    }

    
    /**
     *  Clears the founded terms.
     */

    @OSIDRecord @Override
    public void clearFoundedTerms() {
        this.foundedTerms.clear();
        return;
    }


    /**
     *  Gets all the founded query terms.
     *
     *  @return a collection of the founded query terms
     */

    protected java.util.Collection<org.osid.search.terms.DateTimeRangeTerm> getFoundedTerms() {
        return (java.util.Collections.unmodifiableCollection(this.foundedTerms));
    }


    /**
     *  Matches teams who won championshops within the given range
     *  inclusive.
     *
     *  @param low start of range
     *  @param high end of range
     *  @param match {@code true} for a positive match, {@code false}
     *         for a negative match
     *  @throws org.osid.InvalidArgumentException {@code low} is
     *          greater than {@code high}
     */

    @OSIDRecord @Override
    public void matchChampionships(long low, long high, boolean match) {
        this.championshipTerms.add(getTermFactory().createIntegerRangeTerm(low,
                                                                           high,
                                                                           match));
        return;
    }


    /**
     *  Matches teams with any championships set.
     *
     *  @param match {@code true} for a positive match, {@code false}
     *         for a negative match
     */

    @OSIDRecord @Override
    public void matchAnyChampionships(boolean match) {
        this.championshipTerms.add(getTermFactory().createIntegerRangeWildcardTerm(match));
        return;
    }

    
    /**
     *  Clears the championship terms.
     */

    @OSIDRecord @Override
    public void clearChampionshipsTerms() {
        this.championshipTerms.clear();
        return;
    }


    /**
     *  Gets all the championship query terms.
     *
     *  @return a collection of the championship query terms
     */

    protected java.util.Collection<org.osid.search.terms.IntegerRangeTerm> getChampionshipTerms() {
        return (java.util.Collections.unmodifiableCollection(this.championshipTerms));
    }


    
    /**
     *  for testing
     */

    @OSIDRecord @Override
    public String getNull() {
        return (null);
    }


    /**
     *  Resource Filter.
     *
     *  @param resource the resource to filter
     *  @return <code>true</code> if the resource passes the filter,
     *          <code>false</code> if the resource should be filtered
     */

    @Override
    public boolean pass(org.osid.resource.Resource resource) {
        boolean and = false;

        if (hasAnyTerm()) {
            return (true);
        }

        if (hasNoneTerm()) {
            return (false);
        }

        if (getKeywordTerms().size() > 0) {
            for (org.osid.search.terms.StringTerm term : getKeywordTerms()) {
                
            }
        }

        if (getRecordTypeTerms().size() > 0) {
            for (org.osid.search.terms.TypeTerm term : getRecordTypeTerms()) {
                
            }
        }

        if (getDisplayNameTerms().size() > 0) {
            for (org.osid.search.terms.StringTerm term : getDisplayNameTerms()) {
                
            }
        }        

        if (getDescriptionTerms().size() > 0) {
            for (org.osid.search.terms.StringTerm term : getDescriptionTerms()) {
                
            }
        }

        if (getGenusTypeTerms().size() > 0) {
            for (org.osid.search.terms.TypeTerm term : getGenusTypeTerms()) {
                
            }
        }        

        if (getParentGenusTypeTerms().size() > 0) {
            for (org.osid.search.terms.TypeTerm term : getParentGenusTypeTerms()) {
                
            }
        }        

        if (getLeagueTerms().size() > 0) {
            for (org.osid.search.terms.StringTerm term : getLeagueTerms()) {
                
            }
        }        

        if (getDivisionTerms().size() > 0) {
            for (org.osid.search.terms.StringTerm term : getDivisionTerms()) {
                
            }
        }        

        if (getFoundedTerms().size() > 0) {
            for (org.osid.search.terms.DateTimeRangeTerm term : getFoundedTerms()) {
                
            }
        }        

        if (getChampionshipTerms().size() > 0) {
            for (org.osid.search.terms.IntegerRangeTerm term : getChampionshipTerms()) {
                
            }
        }        

        return (true);
    }
}
