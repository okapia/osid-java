//
// BaseballResourceQuerySession.java
//
//     A test resource service.
//
//
// Tom Coppeto
// Okapia
// 4 December 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.test.providers.resource.baseball;

import org.osid.binding.java.annotation.OSID;

/**
 *  
 */

public class BaseballResourceQuerySession
    extends net.okapia.osid.jamocha.resource.spi.AbstractResourceQuerySession
    implements org.osid.resource.ResourceQuerySession {

    private final Data data;


    /**
     *  Constructs a new <code>BaseballResourceQuerySession</code>.
     */

    public BaseballResourceQuerySession(Data data) {
        this.data = data;
        setBin(this.data.getBin());
        return;
    }


    public String getSomething() {
        return ("something");
    }


    /**
     *  Gets a resource query. The returned query will not have an extension 
     *  query. 
     *
     *  @return the resource query 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getResourceQuery() {
        return (new BaseballResourceQuery());
    }


    /**
     *  Gets a list of <code> Resources </code> matching the given resource 
     *  query. 
     *
     *  @param  resourceQuery the resource query 
     *  @return the returned <code> ResourceList </code> 
     *  @throws org.osid.NullArgumentException <code> resourceQuery </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException <code> resourceQuery </code> is 
     *          not of this service 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getResourcesByQuery(org.osid.resource.ResourceQuery resourceQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.resource.Resource> hits = new java.util.LinkedHashSet<>();

        BaseballResourceQuery query = (BaseballResourceQuery) resourceQuery;
        if (!query.doIBelongHere()) {
            throw new org.osid.UnsupportedException("not my query");
        }

        for (org.osid.resource.Resource resource : this.data.getResources().values()) {
        }

        return (new net.okapia.osid.jamocha.resource.resource.ArrayResourceList(hits));
    }        
}
