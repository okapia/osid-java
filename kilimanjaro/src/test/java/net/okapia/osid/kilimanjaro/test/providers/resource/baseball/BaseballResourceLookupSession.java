//
// BaseballResourceLookupSession.java
//
//     A test resource service.
//
//
// Tom Coppeto
// Okapia
// 4 December 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.test.providers.resource.baseball;

import org.osid.binding.java.annotation.OSID;

/**
 *  
 */

public class BaseballResourceLookupSession
    extends net.okapia.osid.jamocha.resource.spi.AbstractResourceLookupSession
    implements org.osid.resource.ResourceLookupSession {

    private final Data data;


    /**
     *  Constructs a new <code>BaseballResourceLookupSession</code>.
     */

    public BaseballResourceLookupSession(Data data) {
        this.data = data;
        setBin(this.data.getBin());
        return;
    }


    public String getSomething() {
        return ("something");
    }


    /**
     *  Gets the <code>Resource</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Resource</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Resource</code> and
     *  retained for compatibility.
     *
     *  @param resourceId <code>Id</code> of the <code>Resource</code>
     *  @return the resource
     *  @throws org.osid.NotFoundException <code>resourceId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>resourceId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed
     */

    @OSID @Override
    public org.osid.resource.Resource getResource(org.osid.id.Id resourceId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resource.Resource resource = this.data.getResources().get(resourceId);
        if (resource == null) {
            throw new org.osid.NotFoundException(resourceId + " not found");
        } else {
            return (resource);
        }
    }


    /**
     *  Gets all <code>Resources</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  resources or an error results. Otherwise, the returned list
     *  may contain only those resources that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Resources</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     *  @throws org.osid.IllegalStateException this session has been closed
     */

    @OSID @Override
    public org.osid.resource.ResourceList getResources()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.resource.resource.ArrayResourceList(this.data.getResources().values()));
    }
}
