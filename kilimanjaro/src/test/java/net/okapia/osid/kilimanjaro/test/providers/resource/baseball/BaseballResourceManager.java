//
// BaseballResourceManager.java
//
//     A test resource service.
//
//
// Tom Coppeto
// Okapia
// 4 December 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.test.providers.resource.baseball;

import org.osid.binding.java.annotation.OSID;

/**
 *  
 */

public class BaseballResourceManager
    extends net.okapia.osid.jamocha.resource.spi.AbstractResourceManager
    implements org.osid.resource.ResourceManager {

    private final Data data;


    /**
     *  Constructs a new <code>BaseballResourceManager</code>.
     */

    public BaseballResourceManager() {
        super(new ServiceProvider());
        this.data = new Data();
        return;
    }
    

    public String getSomething() {
        return ("something");
    }


    /**
     *  Tests for the availability of a resource lookup service. 
     *
     *  @return <code> true </code> if lookup is available, <code>
     *          false </code> otherwise
     */

    @OSID @Override
    public boolean supportsResourceLookup() {
        return (true);
    }


    /**
     *  Gets a resource lookup session. 
     *
     *  @return a <code>ResourceLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException
     *          <code>supportsResourceLookup()</code> is
     *          <code>false</code>
     *  @throws org.osid.IllegalStateException this manager has been shut down 
     */

    @OSID @Override
    public org.osid.resource.ResourceLookupSession getResourceLookupSession()
        throws org.osid.OperationFailedException {

        return (new BaseballResourceLookupSession(this.data));
    }


    /**
     *  Tests for the availability of a resource query service. 
     *
     *  @return <code> true </code> if query is available, <code>
     *          false </code> otherwise
     */

    @OSID @Override
    public boolean supportsResourceQuery() {
        return (true);
    }


    /**
     *  Gets a resource query session. 
     *
     *  @return a <code>ResourceQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException
     *          <code>supportsResourceQuery()</code> is
     *          <code>false</code>
     *  @throws org.osid.IllegalStateException this manager has been shut down 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuerySession getResourceQuerySession()
        throws org.osid.OperationFailedException {

        return (new BaseballResourceQuerySession(this.data));
    }


    /**
     *  Tests for the availability of a resource admin service. 
     *
     *  @return <code> true </code> if admin is available, <code>
     *          false </code> otherwise
     */

    @OSID @Override
    public boolean supportsResourceAdmin() {
        return (true);
    }


    /**
     *  Gets a resource admin session. 
     *
     *  @return a <code>ResourceAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException
     *          <code>supportsResourceAdmin()</code> is
     *          <code>false</code>
     *  @throws org.osid.IllegalStateException this manager has been shut down 
     */

    @OSID @Override
    public org.osid.resource.ResourceAdminSession getResourceAdminSession()
        throws org.osid.OperationFailedException {

        return (new BaseballResourceAdminSession(this.data));
    }
}
