//
// Data.java
//
//     A test resource service.
//
//
// Tom Coppeto
// Okapia
// 4 December 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.test.providers.resource.baseball;

import static net.okapia.osid.primordium.locale.text.eng.us.Plain.text;


class Data {
    private static final org.osid.resource.Resource[] TEAMS = {
        makeTeam("Baltimore Orioles", "American", "East", 1901, 3),
        makeTeam("Boston Red Sox", "American", "East", 1901, 8),
        makeTeam("New York Yankees", "American", "East", 1901, 27),
        makeTeam("Tampa Bay Rays", "American", "East", 1998, 0),
        makeTeam("Toronto Blue Jays", "American", "North", 1977, 2)
    };

    private static final org.osid.resource.Bin BIN = makeBin();

    private final java.util.Map<org.osid.id.Id, org.osid.resource.Resource> resources = java.util.Collections.synchronizedMap(new net.okapia.osid.torrefacto.collect.IdHashMap<org.osid.resource.Resource>());
    
    private static int ID = 1;


    Data() {
        for (org.osid.resource.Resource resource : TEAMS) {
            this.resources.put(resource.getId(), resource);
        }

        return;
    }


    org.osid.resource.Bin getBin() {
        return (BIN);
    }


    java.util.Map<org.osid.id.Id, org.osid.resource.Resource> getResources() {
        return (this.resources);
    }


    private static org.osid.resource.Bin makeBin() {
        try {
            return (new net.okapia.osid.jamocha.builder.resource.bin.BinBuilder()
                    .id(net.okapia.osid.primordium.id.URNId.valueOf("urn:osid:okapia.net:identifiers:bin:baseball:teams"))
                    .displayName(text("Baseball Teams"))
                    .description(text("Major league baseball teams."))
                    .provider(net.okapia.osid.provider.Providers.OKAPIA.getProvider())
                    .build());
        } catch (Exception e) {
            e.printStackTrace();
            return (null);
        }
    }


    private static org.osid.resource.Resource makeTeam(String name, String league, String division, int year, int wins) {
        
        try {
        return (new net.okapia.osid.jamocha.builder.resource.resource.ResourceBuilder()
                .id(net.okapia.osid.primordium.id.URNId.valueOf("urn:osid:okapia.net:identifiers:resource:baseball:team:" + ID++))
                .displayName(text(name))
                .description(text("The " + name + " major league baseball team."))
                .genus(net.okapia.osid.primordium.type.URNType.valueOf("urn:osid:okapia.net:types:genera:resource:Resource:MajorLeagueBasballTeam"))
                .record(new TeamResourceRecordImpl(league, division, year, wins), TeamResourceRecord.TYPE)
                .build());
        } catch (Exception e) {
            e.printStackTrace();
            throw (e);
        }
    }
}
