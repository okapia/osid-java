//
// UselessAdapterResourceManager.java
//
//     A test resource service.
//
//
// Tom Coppeto
// Okapia
// 4 December 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.test.providers.resource.useless;

import org.osid.binding.java.annotation.OSID;

/**
 *  
 */

public class UselessAdapterResourceManager
    extends net.okapia.osid.jamocha.adapter.resource.spi.AbstractDynamicResourceManager
    implements java.lang.reflect.InvocationHandler {


    /**
     *  Constructs a new <code>UselessAdapterResourceManager</code>.
     */

    public UselessAdapterResourceManager() {
        super(new ServiceProvider());
        return;
    }


    /**
     *  Initializes this manager. A manager is initialized once at the time of
     *  creation.
     *
     *  @param  runtime the runtime environment
     *  @throws org.osid.ConfigurationErrorException an error with
     *          implementation configuration
     *  @throws org.osid.IllegalStateException this manager has already been
     *          initialized by the <code> OsidLoader </code> or this manager
     *          has been shut down
     *  @throws org.osid.NullArgumentException <code> runtime </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     */

    public void initialize(org.osid.OsidRuntimeManager runtime)
        throws org.osid.ConfigurationErrorException,
               org.osid.OperationFailedException {

        super.initialize(runtime);
        try {
            start("net.okapia.osid.kilimanjaro.test.providers.resource.baseball.BaseballResourceManager");
        } catch (org.osid.NotFoundException nfe) {
            throw new org.osid.ConfigurationErrorException(nfe);
        }

        return;
    }
}
