//
// BaseballResourceForm.java
//
//     A test resource service.
//
//
// Tom Coppeto
// Okapia
// 4 December 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.test.providers.resource.baseball;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDRecord;



/**
 *  no abstract class exists for ResourceForms...
 */

public class BaseballResourceForm
    implements org.osid.resource.ResourceForm {

    private static int id = 1;


    /**
     *  Constructs a new {@code BaseballResourceForm}.
     */

    BaseballResourceForm() {
        return;
    }


    protected boolean doIBelongHere() {
        return (true);
    }


    /**
     *  Gets the Id associated with this instance of this OSID object.
     *  Persisting any reference to this object is done by persisting
     *  the Id returned from this method. The Id returned may be
     *  different than the Id used to query this object. In this case,
     *  the new Id should be preferred over the old one for future
     *  queries.
     *
     *  @return the <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getId() {
        return (new net.okapia.osid.primordium.id.BasicId("test", "form", String.valueOf(BaseballResourceForm.id++)));
    }


    /**
     *  Tests to see if the last method invoked retrieved up-to-date
     *  data.  Simple retrieval methods do not specify errors as,
     *  generally, the data is retrieved once at the time this object
     *  is instantiated. Some implementations may provide real-time
     *  data though the application may not always care. An
     *  implementation providing a real-time service may fall back to
     *  a previous snapshot in case of error. This method returns
     *  false if the data last retrieved was stale.
     *
     *  @return <code> true </code> if the last data retrieval was up
     *          to date, <code> false </code> otherwise
     */

    @OSID @Override
    public boolean isCurrent() {
        return (false);
    }


    /**
     *  Gets the record types available in this object. A record
     *  <code> Type </code> explicitly indicates the specification of
     *  an interface to the record. A record may or may not inherit
     *  other record interfaces through interface inheritance in which
     *  case support of a record type may not be explicit in the
     *  returned list. Interoperability with the typed interface to
     *  this object should be performed through <code>
     *  hasRecordType(). </code>
     *
     *  @return the record types available 
     */

    @OSID @Override
    public org.osid.type.TypeList getRecordTypes() {
        return (new net.okapia.osid.jamocha.nil.type.type.EmptyTypeList());
    }


    /**
     *  Tests if this object supports the given record <code>
     *  Type. </code> The given record type may be supported by the
     *  object through interface/type inheritence. This method should
     *  be checked before retrieving the record interface.
     *
     *  @param  recordType a type 
     *  @return <code> true </code> if a record of the given record <code> 
     *          Type </code> is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type recordType) {
        return (false);
    }


    /**
     *  Gets the required record types for this form. The required
     *  records may change as a result of other data in this form and
     *  should be checked before submission.
     *
     *  @return a list of required record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRequiredRecordTypes() {
        return (new net.okapia.osid.jamocha.nil.type.type.EmptyTypeList());
    }


    /**
     *  Tests if this form is for an update operation. 
     *
     *  @return <code> true </code> if this form is for an update operation, 
     *          <code> false </code> if for a create operation 
     */

    @OSID @Override
    public boolean isForUpdate() {
        return (false);
    }


    /**
     *  Gets a default locale for <code> DisplayTexts </code> when a
     *  locale is not specified.
     *
     *  @return the default locale 
     */

    @OSID @Override
    public org.osid.locale.Locale getDefaultLocale() {
        return (new Locale());
    }


    /**
     *  Gets a list of locales for available <code> DisplayText </code> 
     *  translations that can be performed using this form. 
     *
     *  @return a list of available locales or an empty list if no translation 
     *          operations are available 
     */

    @OSID @Override
    public org.osid.locale.LocaleList getLocales() {
        return (new net.okapia.osid.jamocha.locale.locale.SingleLocaleList(new Locale()));
    }


    /**
     *  Specifies a language and script type for <code> DisplayText
     *  </code> fields in this form. Setting a locale to something
     *  other than the default locale may affect the <code> Metadata
     *  </code> in this form.
     *  
     *  If multiple locales are available for managing translations,
     *  the <code> Metadata </code> indicates the fields are unset as
     *  they may be returning a defeult value based on the default
     *  locale.
     *
     *  @param  languageType the language type 
     *  @param  scriptType the script type 
     *  @throws org.osid.NullArgumentException <code> languageType </code> or 
     *          <code> scriptType </code> is null 
     *  @throws org.osid.UnsupportedException <code> languageType </code> and 
     *          <code> scriptType </code> not available from <code> 
     *          getLocales() </code> 
     */

    @OSID @Override
    public void setLocale(org.osid.type.Type languageType, 
                          org.osid.type.Type scriptType) {
        return;
    }


    /**
     *  Gets the metadata for the comment corresponding to this form
     *  submission. The comment is used for describing the nature of
     *  the change to the corresponding object for the purposes of
     *  logging and auditing.
     *
     *  @return metadata for the comment 
     */

    @OSID @Override
    public org.osid.Metadata getJournalCommentMetadata() {
        return (new net.okapia.osid.jamocha.metadata.ReadOnlyStringMetadata(new net.okapia.osid.primordium.id.BasicId("test", "resource", "comment"),
                                                                            net.okapia.osid.primordium.locale.text.eng.us.Plain.valueOf("comment"),
                                                                            net.okapia.osid.primordium.locale.text.eng.us.Plain.valueOf("do nothing"),
                                                                            false));
    }


    /**
     *  Sets a comment. 
     *
     *  @param  comment the new comment 
     *  @throws org.osid.InvalidArgumentException <code> comment </code> is 
     *          invalid 
     *  @throws org.osid.NoAccessException <code> Metadata.isReadonly() 
     *          </code> is <code> true </code> 
     *  @throws org.osid.NullArgumentException <code> comment </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void setJournalComment(String comment) {
        return;
    }


    /**
     *  Tests if ths form is in a valid state for submission. A form
     *  is valid if all required data has been supplied compliant with
     *  any constraints.
     *
     *  @return <code> false </code> if there is a known error in this
     *          form, <code> true </code> otherwise
     *  @throws org.osid.OperationFailedException attempt to perform 
     *          validation failed 
     */
    
    @OSID @Override
    public boolean isValid()
        throws org.osid.OperationFailedException {

        return (true);
    }


    /**
     *  Gets text messages corresponding to additional instructions to
     *  pass form validation.
     *
     *  @return a list of messages 
     */

    @OSID @Override
    public org.osid.locale.DisplayText[] getValidationMessages() {
        return (new org.osid.locale.DisplayText[0]);
    }


    /**
     *  Gets a list of metadata for the elements in this form which
     *  are not valid.
     *
     *  @return invalid metadata 
     */

    @OSID @Override
    public org.osid.Metadata[] getInvalidMetadata()  {
        return (new org.osid.Metadata[0]);
    }


    /**
     *  Gets the metadata for a display name. 
     *
     *  @return metadata for the display name 
     */

    @OSID @Override
    public org.osid.Metadata getDisplayNameMetadata() {
        return (new net.okapia.osid.jamocha.metadata.ReadOnlyStringMetadata(new net.okapia.osid.primordium.id.BasicId("test", "resource", "name"),
                                                                            net.okapia.osid.primordium.locale.text.eng.us.Plain.valueOf("name"),
                                                                            net.okapia.osid.primordium.locale.text.eng.us.Plain.valueOf("do nothing"),
                                                                            false));
    }


    /**
     *  Sets a display name. A display name is required and if not set, will 
     *  be set by the provider. 
     *
     *  @param  displayName the new display name 
     *  @throws org.osid.InvalidArgumentException <code> displayName </code> 
     *          is invalid 
     *  @throws org.osid.NoAccessException <code> Metadata.isReadonly() 
     *          </code> is <code> true </code> 
     *  @throws org.osid.NullArgumentException <code> displayName </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void setDisplayName(String displayName) {
        return;
    }


    /**
     *  Clears the display name. 
     *
     *  @throws org.osid.NoAccessException <code> Metadata.isRequired() 
     *          </code> or <code> Metadata.isReadOnly() </code> is <code> true 
     *          </code> 
     */

    @OSID @Override
    public void clearDisplayName() {
        return;
    }


    /**
     *  Gets the metadata for a description. 
     *
     *  @return metadata for the description 
     */

    @OSID @Override
    public org.osid.Metadata getDescriptionMetadata() {
        return (new net.okapia.osid.jamocha.metadata.ReadOnlyStringMetadata(new net.okapia.osid.primordium.id.BasicId("test", "resource", "description"),
                                                                            net.okapia.osid.primordium.locale.text.eng.us.Plain.valueOf("description"),
                                                                             net.okapia.osid.primordium.locale.text.eng.us.Plain.valueOf("do nothing"),
                                                                             false));
    }


    /**
     *  Sets a description. 
     *
     *  @param  description the new description 
     *  @throws org.osid.InvalidArgumentException <code> description </code> 
     *          is invalid 
     *  @throws org.osid.NoAccessException <code> Metadata.isReadonly() 
     *          </code> is <code> true </code> 
     *  @throws org.osid.NullArgumentException <code> description </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void setDescription(String description) {
        return;
    }


    /**
     *  Clears the description. 
     *
     *  @throws org.osid.NoAccessException <code> Metadata.isRequired() 
     *          </code> or <code> Metadata.isReadOnly() </code> is <code> true 
     *          </code> 
     */

    @OSID @Override
    public void clearDescription() {
        return;
    }


    /**
     *  Gets the metadata for a genus type. 
     *
     *  @return metadata for the genus 
     */

    @OSID @Override
    public org.osid.Metadata getGenusTypeMetadata() {
        return (new net.okapia.osid.jamocha.metadata.ReadOnlyTypeMetadata(new net.okapia.osid.primordium.id.BasicId("test", "resource", "type"),
                                                                             net.okapia.osid.primordium.locale.text.eng.us.Plain.valueOf("type"),
                                                                             net.okapia.osid.primordium.locale.text.eng.us.Plain.valueOf("do nothing"),
                                                                             false));
    }


    /**
     *  Sets a genus. A genus cannot be cleared because all objects have at 
     *  minimum a root genus. 
     *
     *  @param  genusType the new genus 
     *  @throws org.osid.InvalidArgumentException <code> genusType </code> is 
     *          invalid 
     *  @throws org.osid.NoAccessException <code> Metadata.isReadonly() 
     *          </code> is <code> true </code> 
     *  @throws org.osid.NullArgumentException <code> genusType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void setGenusType(org.osid.type.Type genusType) {
        return;
    }


    /**
     *  Clears the genus type. 
     *
     *  @throws org.osid.NoAccessException <code> Metadata.isRequired() 
     *          </code> or <code> Metadata.isReadOnly() </code> is <code> true 
     *          </code> 
     */

    @OSID @Override
    public void clearGenusType() {
        return;
    }


    /**
     *  Gets the metadata for a group. 
     *
     *  @return metadata for the group 
     */

    @OSID @Override
    public org.osid.Metadata getGroupMetadata() {
        return (new net.okapia.osid.jamocha.metadata.ReadOnlyBooleanMetadata(new net.okapia.osid.primordium.id.BasicId("test", "resource", "group"),
                                                                             net.okapia.osid.primordium.locale.text.eng.us.Plain.valueOf("group"),
                                                                             net.okapia.osid.primordium.locale.text.eng.us.Plain.valueOf("do nothing"),
                                                                             false));
    }


    /**
     *  Sets the resource as a group. 
     *
     *  @param  group <code> true </code> if this resource is a group, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.InvalidArgumentException <code> group </code> is 
     *          invalid 
     *  @throws org.osid.NoAccessException <code> Metadata.isReadOnly() 
     *          </code> is <code> true </code> 
     */

    @OSID @Override
    public void setGroup(boolean group) {
        return;
    }


    /**
     *  Clears the group designation. 
     *
     *  @throws org.osid.NoAccessException <code> Metadata.isRequired() 
     *          </code> or <code> Metadata.isReadOnly() </code> is <code> true 
     *          </code> 
     */

    @OSID @Override
    public void clearGroup() {
        return;
    }


    /**
     *  Gets the metadata for an asset. 
     *
     *  @return metadata for the asset 
     */

    @OSID @Override
    public org.osid.Metadata getAvatarMetadata() {
        return (new net.okapia.osid.jamocha.metadata.ReadOnlyBooleanMetadata(new net.okapia.osid.primordium.id.BasicId("test", "resource", "avatar"),
                                                                             net.okapia.osid.primordium.locale.text.eng.us.Plain.valueOf("avatar"),
                                                                             net.okapia.osid.primordium.locale.text.eng.us.Plain.valueOf("do nothing"),
                                                                             false));
    }


    /**
     *  Sets the avatar asset. 
     *
     *  @param  assetId an asset <code> Id </code> 
     *  @throws org.osid.InvalidArgumentException <code> assetId </code> is 
     *          invalid 
     *  @throws org.osid.NoAccessException <code> Metadata.isReadOnly() 
     *          </code> is <code> true </code> 
     */

    @OSID @Override
    public void setAvatar(org.osid.id.Id assetId) {
        return;
    }


    /**
     *  Clears the asset. 
     *
     *  @throws org.osid.NoAccessException <code> Metadata.isRequired() 
     *          </code> or <code> Metadata.isReadOnly() </code> is <code> true 
     *          </code> 
     */

    @OSID @Override
    public void clearAvatar() {
        return;
    }


    /**
     *  Gets the <code> ResourceFormRecord </code> corresponding to the given 
     *  <code> Resource </code> record <code> Type. </code> 
     *
     *  @param  resourceRecordType the resource record type 
     *  @return the resource form record 
     *  @throws org.osid.NullArgumentException <code> resourceRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(resourceRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.records.ResourceFormRecord getResourceFormRecord(org.osid.type.Type resourceRecordType)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnsupportedException ("no form record.. oops");
    }
}
