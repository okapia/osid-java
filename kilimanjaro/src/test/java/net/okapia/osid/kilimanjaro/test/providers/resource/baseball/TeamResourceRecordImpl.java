//
// TeamResourceRecordImpl.java
//
//     A test resource service.
//
//
// Tom Coppeto
// Okapia
// 4 December 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.test.providers.resource.baseball;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDRecord;

import static net.okapia.osid.primordium.locale.text.eng.us.Plain.text;


public class TeamResourceRecordImpl
    implements TeamResourceRecord,
               org.osid.resource.records.ResourceRecord {
    
    private final org.osid.locale.DisplayText league;
    private final org.osid.locale.DisplayText division;
    private final org.osid.calendaring.DateTime founded;
    private final long championships;
    
    TeamResourceRecordImpl(String league, String division, int year, int wins) {
        this.league   = text(league);
        this.division = text(division);
        this.founded  = net.okapia.osid.primordium.calendaring.GregorianUTCDateTime.valueOf(Integer.valueOf(year).toString());
        this.championships = wins;
        
        return;
    }
    

    /**
     *  Tests if the given type is implemented by this record. Other types
     *  than that directly indicated by <code> getType() </code> may be
     *  supported through an inheritance scheme where the given type specifies
     *  a record that is a parent interface of the interface specified by
     *  <code> getType(). </code>
     *
     *  @param  recordType a type
     *  @return <code> true </code> if the given record <code> Type </code> is
     *          implemented by this record, <code> false </code> otherwise
     *  @throws org.osid.NullArgumentException <code> recordType </code> is
     *          <code> null </code>
     */

    @OSID @Override
    public boolean implementsRecordType(org.osid.type.Type recordType) {
        return (TeamResourceRecord.TYPE.equals(recordType));
    }
    
    
    /**
     *  Gets the league.
     *
     *  @return the league
     */

    @OSIDRecord @Override
    public org.osid.locale.DisplayText getLeague() {
        return (this.league);
    }
    
    
    /**
     *  Gets the division in the league.
     *
     *  @return the division
     */

    @OSIDRecord @Override
    public org.osid.locale.DisplayText getDivision() {
        return (this.division);
    }
    
    
    /**
     *  Gets the date founded.
     *
     *  @return the founding date
     */

    @OSIDRecord @Override
    public org.osid.calendaring.DateTime getFounded() {
        return (this.founded);
    }
    
    
    /**
     *  Gets the number of world series championships won.
     *
     *  @return the number of championships
     */

    @OSIDRecord @Override
    public long getChampionships() {
        return (this.championships);
    }


    /**
     *  for testing
     */

    @OSIDRecord @Override
    public String getNull() {
        return (null);
    }
}            
