//
// TeamResourceRecord.java
//
//     A test resource service.
//
//
// Tom Coppeto
// Okapia
// 4 December 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.test.providers.resource.baseball;


public interface TeamResourceRecord
    extends org.osid.resource.records.ResourceRecord {

    public static final org.osid.type.Type TYPE = net.okapia.osid.primordium.type.URNType.valueOf("urn:osid:okapia.net:types:records:resource:Resource:baseball:TeamRecord");


    /**
     *  Gets the league.
     *
     *  @return the league
     */

    public org.osid.locale.DisplayText getLeague();


    /**
     *  Gets the division in the league.
     *
     *  @return the division
     */

    public org.osid.locale.DisplayText getDivision();

    
    /**
     *  Gets the date founded.
     *
     *  @return the founding date
     */

    public org.osid.calendaring.DateTime getFounded();


    /**
     *  Gets the number of world series championships won.
     *
     *  @return the number of championships
     */

    public long getChampionships();


    /**
     *  for testing
     */

    public String getNull();
}
