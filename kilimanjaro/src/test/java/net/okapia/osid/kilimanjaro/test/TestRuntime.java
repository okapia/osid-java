//
// TestRuntime.java
//
//     A unit test for the Runtime.
//
//
// Tom Coppeto
// Okapia
// 12 December 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.test;

import org.testng.annotations.*;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;

import java.lang.reflect.Method;

import org.osid.OSID;
import org.osid.OsidManager;
import org.osid.OsidRuntimeManager;

import org.osid.calendaring.DateTime;
import org.osid.id.Id;
import org.osid.locale.DisplayText;
import org.osid.resource.ResourceManager;
import org.osid.resource.ResourceLookupSession;
import org.osid.resource.ResourceQuerySession;
import org.osid.resource.ResourceAdminSession;
import org.osid.resource.Resource;
import org.osid.resource.ResourceQuery;
import org.osid.resource.ResourceForm;
import org.osid.resource.ResourceList;
import org.osid.resource.records.ResourceRecord;
import org.osid.type.Type;

import org.osid.ConfigurationErrorException;
import org.osid.IllegalStateException;
import org.osid.NotFoundException;
import org.osid.NullReturnException;
import org.osid.OperationFailedException;
import org.osid.PermissionDeniedException;

import net.okapia.osid.jamocha.resource.resource.ArrayResourceList;
import net.okapia.osid.jamocha.builder.resource.resource.ImmutableResource;
import net.okapia.osid.jamocha.builder.resource.resource.MutableResource;

import net.okapia.osid.kilimanjaro.test.providers.resource.baseball.BaseballResourceManager;
import net.okapia.osid.kilimanjaro.test.providers.resource.baseball.BaseballResourceLookupSession;
import net.okapia.osid.kilimanjaro.test.providers.resource.baseball.BaseballResourceQuerySession;
import net.okapia.osid.kilimanjaro.test.providers.resource.baseball.TeamResourceRecord;
import net.okapia.osid.kilimanjaro.test.providers.resource.baseball.TeamResourceRecordImpl;


@Test
public class TestRuntime
    extends AbstractTestRuntime {


    public TestRuntime() 
        throws ConfigurationErrorException,
               OperationFailedException {

        super();
        return;
    }


    protected Object[][] OSIDS = {
        {
            "net.okapia.osid.kilimanjaro.test.providers.resource.baseball.BaseballResourceManager", 
            "net.okapia.osid.kilimanjaro.test.providers.resource.baseball.BaseballResourceManager", true,
            "net.okapia.osid.kilimanjaro.test.providers.resource.baseball.BaseballResourceLookupSession", true, 
            "net.okapia.osid.kilimanjaro.test.providers.resource.baseball.BaseballResourceQuerySession", true,
            "net.okapia.osid.kilimanjaro.test.providers.resource.baseball.BaseballResourceAdminSession", true,
            "net.okapia.osid.jamocha.resource.resource.ArrayResourceList", false,
            "net.okapia.osid.jamocha.builder.resource.resource.ImmutableResource", false
        },
        {
            "net.okapia.osid.kilimanjaro.test.providers.resource.useless.UselessAdapterResourceManager", 
            "net.okapia.osid.kilimanjaro.test.providers.resource.useless.UselessAdapterResourceManager", false, 
            "net.okapia.osid.kilimanjaro.test.providers.resource.baseball.BaseballResourceLookupSession", true,
            "net.okapia.osid.kilimanjaro.test.providers.resource.baseball.BaseballResourceQuerySession", true,
            "net.okapia.osid.kilimanjaro.test.providers.resource.baseball.BaseballResourceAdminSession", true,
            "net.okapia.osid.jamocha.resource.resource.ArrayResourceList", false,
            "net.okapia.osid.jamocha.builder.resource.resource.ImmutableResource", false
        }
    };
    
    
    @DataProvider(name="osids")
    public Object[][] getOSIDs() {
        return (OSIDS);
    }
        
    
    @Test(dataProvider="osids")
    public void testOsid(String osidImpl, 
                         String managerImpl, boolean managerHasSomething,
                         String lookupSessionImpl, boolean lookupSessionHasSomething,
                         String querySessionImpl, boolean querySessionHasSomething,
                         String adminSessionImpl, boolean adminSessionHasSomething,
                         String listImpl, boolean listHasSomething,
                         String objectImpl, boolean objectHasSomething)
        throws ConfigurationErrorException,
               NotFoundException,
               OperationFailedException,
               PermissionDeniedException {

        try {
            ResourceManager manager = (ResourceManager) loadOsidManager(OSID.RESOURCE, osidImpl);
            assertNotNull(manager);
            testOsidManager(manager, 
                            managerImpl, managerHasSomething,
                            lookupSessionImpl, lookupSessionHasSomething,
                            querySessionImpl, querySessionHasSomething,
                            adminSessionImpl, adminSessionHasSomething,
                            listImpl, listHasSomething,
                            objectImpl, objectHasSomething);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }


    protected void testOsidManager(ResourceManager manager, 
                         String impl, boolean hasSomething,
                         String lookupSessionImpl, boolean lookupSessionHasSomething,
                         String querySessionImpl, boolean querySessionHasSomething,
                         String adminSessionImpl, boolean adminSessionHasSomething,
                         String listImpl, boolean listHasSomething,
                         String objectImpl, boolean objectHasSomething)
        throws ConfigurationErrorException,
               NotFoundException,
               OperationFailedException,
               PermissionDeniedException {

        testEncapsulation(manager, impl, hasSomething);
        ResourceLookupSession lookupSession = manager.getResourceLookupSession();
        testOsidLookupSession(lookupSession, lookupSessionImpl, lookupSessionHasSomething, 
                              listImpl, listHasSomething,
                              objectImpl, objectHasSomething);

        ResourceQuerySession querySession = manager.getResourceQuerySession();
        testOsidQuerySession(querySession, querySessionImpl, querySessionHasSomething);

        ResourceAdminSession adminSession = manager.getResourceAdminSession();
        testOsidAdminSession(adminSession, adminSessionImpl, adminSessionHasSomething);

        return;
    }

    
    protected void testOsidLookupSession(ResourceLookupSession session, String impl, boolean hasSomething,
                                         String listImpl, boolean listHasSomething,
                                         String objectImpl, boolean objectHasSomething)
        throws NotFoundException,
               OperationFailedException,
               PermissionDeniedException {


        testEncapsulation(session, impl, hasSomething);

        ResourceList list = session.getResources();
        testOsidList(list, listImpl, listHasSomething, objectImpl, objectHasSomething);

        list = session.getResources();
        testListClose(list);
        
        testSessionClose(session);

        return;
    }


    protected void testOsidList(ResourceList list, String impl, boolean hasSomething,
                                String objectImpl, boolean objectHasSomething)
        throws NotFoundException,
               OperationFailedException,
               PermissionDeniedException {

        testEncapsulation(list, impl, hasSomething);

        while (list.hasNext()) {
            testOsidObject(list.getNextResource(), objectImpl, objectHasSomething);
        }

        try {
            list.getNextResource();
            fail("list is exhausted");
        } catch (IllegalStateException ise) {}
        
        list.close();
        return;
    }


    protected void testOsidObject(Resource resource, String impl, boolean hasSomething) 
        throws NotFoundException,
               OperationFailedException {

        testEncapsulation(resource, impl, hasSomething);

        Id id = resource.getId();
        DisplayText name = resource.getDisplayName();
        DisplayText desc = resource.getDescription();
        Type genus       = resource.getGenusType();

        assertEquals(resource.equals(resource), true);
        
        testOsidRecord(resource);
        return;
    }


    protected void testOsidRecord(Resource resource) 
        throws NotFoundException,
               OperationFailedException {

        ResourceRecord record = resource.getResourceRecord(TeamResourceRecord.TYPE);
        TeamResourceRecord teamRecord = (TeamResourceRecord) record;
        
        try {
            TeamResourceRecordImpl impl = (TeamResourceRecordImpl) teamRecord;
            fail("record impl cast");
        } catch (ClassCastException cce) {}

        DisplayText league = teamRecord.getLeague();
        DisplayText division = teamRecord.getDivision();
        DateTime founded = teamRecord.getFounded();
        long wins = teamRecord.getChampionships();

        try {
            String s = teamRecord.getNull();
            fail("null not detected");
        } catch (NullReturnException nre) {}

        return;
    }


    protected void testOsidQuerySession(ResourceQuerySession session, String impl, boolean hasSomething)
        throws NotFoundException,
               OperationFailedException,
               PermissionDeniedException {

        testEncapsulation(session, impl, hasSomething);

        ResourceQuery query = session.getResourceQuery();
        query.matchAny(true);
        ResourceList resources = session.getResourcesByQuery(query);
        /////
        while (resources.hasNext()) {
            System.out.println("name: " + resources.getNextResource().getDisplayName());
        }

        testSessionClose(session);
        return;
    }


    protected void testOsidAdminSession(ResourceAdminSession session, String impl, boolean hasSomething)
        throws NotFoundException,
               OperationFailedException,
               PermissionDeniedException {

        testEncapsulation(session, impl, hasSomething);

        ResourceForm form = session.getResourceFormForCreate(new org.osid.type.Type[0]);
        form.setJournalComment("Boston Braves");
        form.setDisplayName("Boston Braves");
        Resource resource = session.createResource(form);

        testSessionClose(session);
        return;
    }
}
