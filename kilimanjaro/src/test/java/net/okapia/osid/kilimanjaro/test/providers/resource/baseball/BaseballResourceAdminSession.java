//
// BaseballResourceAdminSession.java
//
//     A test resource service.
//
//
// Tom Coppeto
// Okapia
// 4 December 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.test.providers.resource.baseball;

import org.osid.binding.java.annotation.OSID;

/**
 *  
 */

public class BaseballResourceAdminSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.resource.ResourceAdminSession {

    private final Data data;


    /**
     *  Constructs a new <code>BaseballResourceAdminSession</code>.
     */

    public BaseballResourceAdminSession(Data data) {
        this.data = data;
        return;
    }


    public String getSomething() {
        return ("something");
    }


        /**
     *  Gets the <code> Bin </code> <code> Id </code> associated with this 
     *  session. 
     *
     *  @return the <code> Bin Id </code> associated with this session 
     */

    @OSID @Override
    public org.osid.id.Id getBinId() {
        return (this.data.getBin().getId());
    }


    /**
     *  Gets the <code> Bin </code> associated with this session. 
     *
     *  @return the <code> Bin </code> associated with this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resource.Bin getBin()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.data.getBin());
    }


    /**
     *  Tests if this user can create <code> Resources. </code> A
     *  return of true does not guarantee successful authorization. A
     *  return of false indicates that it is known creating a <code>
     *  Resource </code> will result in a <code>
     *  PERMISSION_DENIED. </code> This is intended as a hint to an
     *  application that may opt not to offer create operations to an
     *  unauthorized user.
     *
     *  @return <code> false </code> if <code> Resource </code> creation is 
     *          not authorized, <code> true </code> otherwise 
     */

    @OSID @Override
    public boolean canCreateResources() {
        return (true);
    }


    /**
     *  Tests if this user can create a single <code> Resource </code>
     *  using the desired record types. While <code>
     *  ResourceManager.getResourceRecordTypes() </code> can be used
     *  to examine which records are supported, this method tests
     *  which record(s) are required for creating a specific <code>
     *  Resource. </code> Providing an empty array tests if a <code>
     *  Resource </code> can be created with no records.
     *
     *  @param  resourceRecordTypes array of resource record types 
     *  @return <code> true </code> if <code> Resource </code> creation using 
     *          the specified <code> Types </code> is supported, <code> false 
     *          </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> resourceRecordTypes 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean canCreateResourceWithRecordTypes(org.osid.type.Type[] resourceRecordTypes) {
        if (resourceRecordTypes.length == 0) {
            return (true);
        }

        return (false);
    }


    /**
     *  Gets the resource form for creating new resources. A new form
     *  should be requested for each create transaction.
     *
     *  @param  resourceRecordTypes array of resource record types 
     *  @return the resource form 
     *  @throws org.osid.NullArgumentException <code> resourceRecordTypes 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException unable to get form with 
     *          requested record types 
     */

    @OSID @Override
    public org.osid.resource.ResourceForm getResourceFormForCreate(org.osid.type.Type[] resourceRecordTypes)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (!canCreateResourceWithRecordTypes(resourceRecordTypes)) {
            throw new org.osid.UnsupportedException("cannot create resource with record types");
        }

        return (new BaseballResourceForm());
    }


    /**
     *  Creates a new <code> Resource. </code> 
     *
     *  @param  resourceForm the form for this <code> Resource </code> 
     *  @return the new <code> Resource </code> 
     *  @throws org.osid.IllegalStateException <code> resourceForm </code> 
     *          already used in a create transaction 
     *  @throws org.osid.InvalidArgumentException one or more of the form 
     *          elements is invalid 
     *  @throws org.osid.NullArgumentException <code> resourceForm </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException <code> resourceForm </code> did 
     *          not originate from <code> getResourceFormForCreate() </code> 
     */

    @OSID @Override
    public org.osid.resource.Resource createResource(org.osid.resource.ResourceForm resourceForm)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        BaseballResourceForm form = (BaseballResourceForm) resourceForm;
        if (!form.doIBelongHere()) {
            throw new org.osid.UnsupportedException("not my form");
        }

        return (new net.okapia.osid.jamocha.nil.resource.resource.UnknownResource());
    }


    /**
     *  Tests if this user can update <code> Resources. </code> A
     *  return of true does not guarantee successful authorization. A
     *  return of false indicates that it is known updating a <code>
     *  Resource </code> will result in a <code>
     *  PERMISSION_DENIED. </code> This is intended as a hint to an
     *  application that may opt not to offer update operations to an
     *  unauthorized user.
     *
     *  @return <code> false </code> if <code> Resource </code> modification 
     *          is not authorized, <code> true </code> otherwise 
     */

    @OSID @Override
    public boolean canUpdateResources() {
        return (false);
    }


    /**
     *  Gets the resource form for updating an existing resource. A
     *  new resource form should be requested for each update
     *  transaction.
     *
     *  @param  resourceId the <code> Id </code> of the <code> Resource 
     *          </code> 
     *  @return the resource form 
     *  @throws org.osid.NotFoundException <code> resourceId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resource.ResourceForm getResourceFormForUpdate(org.osid.id.Id resourceId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        throw new org.osid.PermissionDeniedException("not authorized to invoke a method that has no implementation");
    }


    /**
     *  Updates an existing resource. 
     *
     *  @param  resourceForm the form containing the elements to be updated 
     *  @throws org.osid.IllegalStateException <code> resourceForm </code> 
     *          already used in an update transaction 
     *  @throws org.osid.InvalidArgumentException the form contains an invalid 
     *          value 
     *  @throws org.osid.NullArgumentException <code> resourceForm </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException <code> resourceForm </code> did 
     *          not originate from <code> getResourceFormForUpdate() </code> 
     */

    @OSID @Override
    public void updateResource(org.osid.resource.ResourceForm resourceForm)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        throw new org.osid.PermissionDeniedException("not authorized to invoke a method that has no implementation");
    }


    /**
     *  Tests if this user can delete <code> Resources. </code> A return of 
     *  true does not guarantee successful authorization. A return of false 
     *  indicates that it is known deleting a <code> Resource </code> will 
     *  result in a <code> PERMISSION_DENIED. </code> This is intended as a 
     *  hint to an application that may opt not to offer delete operations to 
     *  an unauthorized user. 
     *
     *  @return <code> false </code> if <code> Resource </code> deletion is 
     *          not authorized, <code> true </code> otherwise 
     */

    @OSID @Override
    public boolean canDeleteResources() {
        return (false);
    }


    /**
     *  Deletes a <code> Resource. </code> 
     *
     *  @param  resourceId the <code> Id </code> of the <code> Resource 
     *          </code> to remove 
     *  @throws org.osid.NotFoundException <code> resourceId </code> not found 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void deleteResource(org.osid.id.Id resourceId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        throw new org.osid.PermissionDeniedException("not authorized to invoke a method that has no implementation");
    }


    /**
     *  Tests if this user can manage <code> Id </code> aliases for <code> 
     *  Resources. </code> A return of true does not guarantee successful 
     *  authorization. A return of false indicates that it is known changing 
     *  an alias will result in a <code> PERMISSION_DENIED. </code> This is 
     *  intended as a hint to an application that may opt not to offer alias 
     *  operations to an unauthorized user. 
     *
     *  @return <code> false </code> if <code> Resource </code> aliasing is 
     *          not authorized, <code> true </code> otherwise 
     */

    @OSID @Override
    public boolean canManageResourceAliases() {
        return (false);
    }


    /**
     *  Adds an <code> Id </code> to a <code> Resource </code> for the purpose 
     *  of creating compatibility. The primary <code> Id </code> of the <code> 
     *  Resource </code> is determined by the provider. The new <code> Id 
     *  </code> performs as an alias to the primary <code> Id. </code> If the 
     *  alias is a pointer to another resource it is reassigned to the given 
     *  resource <code> Id. </code> 
     *
     *  @param  resourceId the <code> Id </code> of a <code> Resource </code> 
     *  @param  aliasId the alias <code> Id </code> 
     *  @throws org.osid.AlreadyExistsException <code> aliasId </code> is 
     *          already assigned 
     *  @throws org.osid.NotFoundException <code> resourceId </code> not found 
     *  @throws org.osid.NullArgumentException <code> aliasId </code> or 
     *          <code> resourceId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void aliasResource(org.osid.id.Id resourceId, 
                              org.osid.id.Id aliasId)
        throws org.osid.AlreadyExistsException,
               org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        throw new org.osid.PermissionDeniedException("not authorized to invoke a method that has no implementation");
    }
}

