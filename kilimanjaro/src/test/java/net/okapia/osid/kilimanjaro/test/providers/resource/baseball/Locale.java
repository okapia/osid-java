package net.okapia.osid.kilimanjaro.test.providers.resource.baseball;

import net.okapia.osid.primordium.type.BasicType;


public class Locale
    implements org.osid.locale.Locale {


    /**
     *  Gets the language <code> Type. </code> 
     *
     *  @return the language type 
     */

    @Override
    public org.osid.type.Type getLanguageType() {
        return (new BasicType("test", "language", "readable"));
    }


    /**
     *  Gets the script <code> Type. </code> 
     *
     *  @return the script type 
     */

    @Override
    public org.osid.type.Type getScriptType() {
        return (new BasicType("test", "script", "cursive"));
    }


    /**
     *  Gets the calendar <code> Type. </code> 
     *
     *  @return the calendar type 
     */

    @Override
    public org.osid.type.Type getCalendarType() {
        return (new BasicType("test", "calendar", "photos"));
    }


    /**
     *  Gets the time <code> Type. </code> 
     *
     *  @return the time type 
     */

    @Override
    public org.osid.type.Type getTimeType() {
        return (new BasicType("test", "time", "fast"));
    }


    /**
     *  Gets the currency <code> Type. </code> 
     *
     *  @return the currency type 
     */

    @Override
    public org.osid.type.Type getCurrencyType() {
        return (new BasicType("test", "currency", "lots"));
    }


    /**
     *  Gets the unit system <code> Type. </code> 
     *
     *  @return the unit system type 
     */

    @Override
    public org.osid.type.Type getUnitSystemType() {
        return (new BasicType("test", "unit system", "lots"));
    }


    /**
     *  Gets the numeric format <code> Type. </code> 
     *
     *  @return the numeric format type 
     */

    @Override
    public org.osid.type.Type getNumericFormatType() {
        return (new BasicType("test", "numeric", "nice"));
    }


    /**
     *  Gets the calendar format <code> Type. </code> 
     *
     *  @return the calendar format type 
     */

    @Override
    public org.osid.type.Type getCalendarFormatType() {
        return (new BasicType("test", "calendar", "nice"));
    }


    /**
     *  Gets the time format <code> Type. </code> 
     *
     *  @return the time format type 
     */

    @Override
    public org.osid.type.Type getTimeFormatType() {
        return (new BasicType("test", "time", "nice"));
    }


    /**
     *  Gets the currency format <code> Type. </code> 
     *
     *  @return the currency format type 
     */

    @Override
    public org.osid.type.Type getCurrencyFormatType() {
        return (new BasicType("test", "currencyformat", "nice"));
    }


    /**
     *  Gets the coordinate format <code> Type. </code> 
     *
     *  @return the coordinate format type 
     */

    @Override
    public org.osid.type.Type getCoordinateFormatType() {
        return (new BasicType("test", "coordinateformat", "nice"));
    }
}
