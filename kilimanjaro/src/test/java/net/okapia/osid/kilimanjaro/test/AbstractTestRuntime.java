//
// AbstractTestRuntime.java
//
//     A unit test for the Runtime.
//
//
// Tom Coppeto
// Okapia
// 12 December 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.test;

import org.testng.annotations.*;
import static org.testng.Assert.fail;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.lang.reflect.InvocationTargetException;

import org.osid.OSID;
import org.osid.OsidManager;
import org.osid.OsidRuntimeManager;

import org.osid.ConfigurationErrorException;
import org.osid.IllegalStateException;
import org.osid.NotFoundException;
import org.osid.OperationFailedException;

import net.okapia.osid.kilimanjaro.BootLoader;
import net.okapia.osid.kilimanjaro.OsidVersions;
import net.okapia.osid.kilimanjaro.executor.Contract;


public abstract class AbstractTestRuntime {
    private final org.osid.OsidRuntimeManager runtime;


    public AbstractTestRuntime()
        throws ConfigurationErrorException,
               OperationFailedException {

        BootLoader loader = new BootLoader();
        this.runtime = loader.getRuntimeManager("test");
        assertNotNull(runtime);

        return;
    }

    
    protected org.osid.OsidManager loadOsidManager(OSID osid, String impl)
        throws ConfigurationErrorException, 
               NotFoundException,
               OperationFailedException {

        return (getRuntime().getManager(osid, impl, OsidVersions.V3_0_0.getVersion()));
    }


    protected org.osid.OsidRuntimeManager getRuntime() {
        return (this.runtime);
    }



    protected void testEncapsulation(Object object, String impl, boolean hasSomething) {
        boolean isProxy; // might be proxied twice

        if (Proxy.isProxyClass(object.getClass())) {
            isProxy = true;
        } else {
            isProxy = false;
        }

        if (!isProxy) {
            assertEquals(((Contract) object).getProviderOfContract(), impl, impl + " vs. " + 
                         ((Contract) object).getProviderOfContract());
        } 

        Object cobject = ((Contract) object).violateContract();
        Class<?> c = null;
        
        try {
            c = Class.forName(impl);
        } catch (ClassNotFoundException cnfe) {
            fail("cannot find class " + impl, cnfe);
        }

        Method method = null;
        try {
            method = c.getMethod("getSomething");
        } catch (NoSuchMethodException iae) {
            if (hasSomething) {
                fail("does not have something when it should");
            }
        }

        if (method != null) {
            try {
                method.invoke(object);
                fail("private method visible");
            } catch (IllegalArgumentException | IllegalAccessException iae) {}
            catch (InvocationTargetException ite) {
                fail("something threw an exception", ite);
            }
            
            try {
                method.invoke(cobject);
                if (!hasSomething) {
                    fail("has something when it shouldn't");
                }
            } catch (IllegalAccessException iae) {
                if (hasSomething) {
                    fail("does not have domething when it should", iae);
                }
            } catch (InvocationTargetException ite) {
                fail("something threw an exception", ite);
            }
        }
            
        if (c.isAssignableFrom(object.getClass())) {
            fail("cast");
        }

        if (!isProxy) {
            if (!c.isAssignableFrom(cobject.getClass())) {
                fail("cast");
            }
        }

        return;
    }


    protected void testSessionClose(org.osid.OsidSession session) {
        session.close();

        try {
            session.isAuthenticated();
            fail("session closed");
        } catch (IllegalStateException ie) {}

        return;
    }

    
    protected void testListClose(org.osid.OsidList list) {
        list.close();

        try {
            list.hasNext();
            fail("list is closed");
        } catch (IllegalStateException ise) {}
        
        return;
    }
}
