//
// Parser.java
//
//     Parses an XML based type file.
//
//
// Tom Coppeto
// Okapia
// 21 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.runtime.providers.type.xml;

import net.okapia.osid.primordium.type.BasicType;
import net.okapia.osid.primordium.locale.text.eng.us.Plain;
import net.okapia.osid.torrefacto.xml.XMLUtil;


/**
 *  Parses an XML based type file.
 */

public final class Parser {

    private final java.util.Collection<org.osid.type.Type> types = new java.util.HashSet<>();
    private final java.util.Map<org.osid.type.Type, org.osid.type.Type> aliases = new java.util.HashMap<>();
    private final java.util.Map<org.osid.type.Type, java.util.Collection<org.osid.type.Type>> parents= new java.util.HashMap<>();


    /**
     *  Constructs a new parser for type files.
     *
     *  @param  file path to type file
     *  @throws org.osid.NotFoundException file not found
     *  @throws org.osid.NullArgumentException
     *          <code>file</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException error reading file
     */

    public Parser(String file)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        java.net.URL url = getClass().getResource(file);
        if (url != null) {
            file = url.getPath();
        } 

        XMLUtil xml = new XMLUtil(file);
        xml.addNamespace("c", "urn:inet:osid.org:schemas:providers/type/1");
        
        org.dom4j.Element root = xml.getElement("/c:types", true);

        for (org.dom4j.Node np : xml.getNodes(root, "c:type")) {
            String id = np.valueOf("@id");
            if (id == null) {
                throw new org.osid.OperationFailedException("type has no identifier");
            }

            org.osid.type.Type type = BasicType.valueOf(id);

            String alias  = np.valueOf("@alias");
            if ((alias != null) && (alias.length() > 0)) {
                org.osid.type.Type aliasType = BasicType.valueOf(alias);
                addAlias(type, aliasType);
                continue;
            }

            String parent = np.valueOf("@parent");
            if ((parent != null) && (parent.length() > 0)) {
                org.osid.type.Type parentType = BasicType.valueOf(parent);
                addParent(type, parentType);
                continue;
            }

            String domain = np.valueOf("@domain");
            String name   = xml.getContent(np, "c:displayName");
            String label  = xml.getContent(np, "c:displayLabel");
            String desc   = xml.getContent(np, "c:description");
            
            if ((name != null) && (name.length() > 0)) {
                if ((label == null) && (label.length() == 0)) {
                    label = name;
                }

                if ((desc == null) && (desc.length() == 0)) {
                    desc = "Type: " + name;
                }

                type = new BasicType(type, Plain.valueOf(domain), Plain.valueOf(name), 
                                     Plain.valueOf(label), Plain.valueOf(desc));
            }

            addType(type);
        }

        return;
    }

    
    /**
     *  Gets the list of parsed types.
     *
     *  @return types
     */

    public java.util.Collection<org.osid.type.Type> getTypes() {
        return (this.types);
    }


    /**
     *  Gets the map of aliases.
     *
     *  @return alias map
     */

    public java.util.Map<org.osid.type.Type, org.osid.type.Type> getAliases() {
        return (this.aliases);
    }


    /**
     *  Gets the map of parents.
     *
     *  @return type hierarchy map
     */

    public java.util.Map<org.osid.type.Type, java.util.Collection<org.osid.type.Type>> getParents() {
        return (this.parents);
    }


    private void addType(org.osid.type.Type type) {
        this.types.add(type);
    }


    private void addAlias(org.osid.type.Type type, org.osid.type.Type alias) {
        this.aliases.put(type, alias);
        return;
    }


    private void addParent(org.osid.type.Type type, org.osid.type.Type parent) {
        java.util.Collection<org.osid.type.Type> set;
        set = this.parents.get(type);
        if (set == null) {
            set = new java.util.HashSet<org.osid.type.Type>();
        }
        
        set.add(parent);
        this.parents.put(type, set);
        return;
    }
}