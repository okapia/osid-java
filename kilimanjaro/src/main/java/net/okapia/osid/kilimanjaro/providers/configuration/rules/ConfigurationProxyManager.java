//
// ConfigurationProxyManager.java
//
//     A Configuration adapter to filter configuration values based on
//     rules.
//
//
// Tom Coppeto
// Okapia
// 30 January 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.providers.configuration.rules;


/**
 *  Applies rules to returned configuration values. The OSID stack is
 *  passed through a Proxy.
 */

public final class ConfigurationProxyManager
    extends net.okapia.osid.jamocha.configuration.spi.AbstractConfigurationManager
    implements org.osid.configuration.ConfigurationProxyManager {

    private org.osid.rules.RulesManager rulesManager;
    private org.osid.configuration.ConfigurationManager configurationManager;
    private org.osid.rules.Rule rule;


    /**
     *  Constructs a new <code>ConfigurationProxyManager</code>.
     */

    public ConfigurationProxyManager() {
        super(new ConfigurationServiceProvider());
        return;
    }

    
    /**
     *  Initializes this manager. A manager is initialized once at the time of 
     *  creation. 
     *
     *  @param  runtime the runtime environment 
     *  @throws org.osid.ConfigurationErrorException an error with 
     *          implementation configuration 
     *  @throws org.osid.IllegalStateException this manager has already been 
     *          initialized by the <code> OsidLoader </code>
     *  @throws org.osid.NullArgumentException <code> runtime </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    public void initialize(org.osid.OsidRuntimeManager runtime)
        throws org.osid.ConfigurationErrorException,
               org.osid.OperationFailedException {

        super.initialize(runtime);
        
        this.configurationManager = (org.osid.configuration.ConfigurationManager) getOsidManager(org.osid.OSID.CONFIGURATION, "net.okapia.osid.kilimanjaro.providers.configuration.base.ConfigurationManager");
        this.rulesManager = (org.osid.rules.RulesManager) getOsidManager(org.osid.OSID.RULES, "net.okapia.osid.kilimanjaro.providers.rules.configuration.RulesManager");

        /*
         * check out the configuration service
         */

        if (!this.configurationManager.supportsValueLookup()) {
            throw new org.osid.ConfigurationErrorException("configuration service does not support value lookups");
        }
            
        /*
         * check out the rules service
         */

        if (!this.rulesManager.supportsRules() || !this.rulesManager.supportsRuleLookup()) {
            throw new org.osid.ConfigurationErrorException("rules service does not support rules sessions");
        }
            
        try {
            org.osid.rules.RuleList rules = this.rulesManager.getRuleLookupSession().getRules();
            if (!rules.hasNext()) {
                throw new org.osid.ConfigurationErrorException("no rules in rules service");
            }
            
            this.rule = rules.getNextRule();
            if (rules.hasNext()) {
                throw new org.osid.ConfigurationErrorException("expecting a single rule in rules service");
            }
        } catch (org.osid.PermissionDeniedException pde) {
            throw new org.osid.OperationFailedException("cannot access rules service", pde);
        }

        return;
    }


    /**
     *  Tests if federation is visible for this service. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    public boolean supportsVisibleFederation() {
        return (true);
    }


    /**
     *  Tests for the availability of a configuration value lookup service. 
     *
     *  @return <code> true </code> if value lookup is available, <code> false 
     *          </code> otherwise 
     */

    public boolean supportsValueLookup() {
        return (true);
    }


    /**
     *  Gets a configuration value lookup session. 
     *
     *  @return a <code> ValueLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsValueLookup() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.IllegalStateException this manager has been shut down 
     */

    public org.osid.configuration.ValueLookupSession getValueLookupSession()
        throws org.osid.OperationFailedException {

        return (ValueLookupSession.createValueLookupSession(this.configurationManager.getValueLookupSession(), 
                                                            this.rulesManager.getRulesSession(), this.rule));
    }


    /**
     *  Gets a configuration value lookup session.
     *
     *  @param  proxy a proxy 
     *  @return a <code> ValueLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException
     *          <code>supportsValueLookup()</code> is <code> False
     *          </code>
     *  @throws org.osid.IllegalStateException this manager has been shut down 
     */

    public org.osid.configuration.ValueLookupSession getValueLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (ValueLookupSession.createValueLookupSession(this.configurationManager.getValueLookupSession(),
                                                            this.rulesManager.getRulesSession(), this.rule,
                                                            proxy));
    }


    /**
     *  Gets a configuration value lookup session using the supplied 
     *  configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> to use 
     *  @return a <code> ValueLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> configurationId </code> is 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsValueLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.IllegalStateException this manager has been shut down 
     */

    public org.osid.configuration.ValueLookupSession getValueLookupSessionForConfiguration(org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        org.osid.configuration.ValueLookupSession valueSession = this.configurationManager.getValueLookupSessionForConfiguration(configurationId);

        org.osid.rules.RulesSession rulesSession;
        try {
            rulesSession = this.rulesManager.getRulesSessionForEngine(configurationId);
        } catch (org.osid.NotFoundException nfe) {
            return (valueSession);
        } 

        return (ValueLookupSession.createValueLookupSession(valueSession, rulesSession, this.rule));
    }


    /**
     *  Gets a configuration value lookup session using the supplied 
     *  configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> to use 
     *  @param  proxy a proxy 
     *  @return a <code> ValueLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> configurationId </code> is 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsValueLookup()      
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> False </code>      
     *  @throws org.osid.IllegalStateException this manager has been shut down 
     */

    public org.osid.configuration.ValueLookupSession getValueLookupSessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        org.osid.configuration.ValueLookupSession valueSession = this.configurationManager.getValueLookupSessionForConfiguration(configurationId);

        org.osid.rules.RulesSession rulesSession;

        try {
            rulesSession = this.rulesManager.getRulesSessionForEngine(configurationId);
        } catch (org.osid.NotFoundException nfe) {
            return (valueSession);
        } 

        return (ValueLookupSession.createValueLookupSession(valueSession, rulesSession, this.rule, proxy));
    }
}
