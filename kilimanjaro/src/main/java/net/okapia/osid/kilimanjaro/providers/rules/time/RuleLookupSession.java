//
// RuleLookupSession.java
//
//     Supplies basic information in common throughout the managers and
//     profiles.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.providers.rules.time;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;

import net.okapia.osid.primordium.id.BasicId;
import net.okapia.osid.primordium.locale.text.eng.us.Html;
import net.okapia.osid.torrefacto.xml.XMLUtil;


/**
 *  This session provides methods for retrieving <code> Rule </code>
 *  objects. The <code> Rule </code> represents a collection of rule
 *  entries.
 *  
 *  This session defines views that offer differing behaviors when
 *  retrieving multiple objects.
 *  
 *  <ul>
 *      <li> comparative view: elements may be silently omitted or re-ordered 
 *      </li> 
 *      <li> plenary view: provides a complete set or is an error condition 
 *      </li> 
 *  </ul>
 *
 *  Generally, the comparative view should be used for most
 *  applications as it permits operation even if there is data that
 *  cannot be accessed.. For example, a browsing application may only
 *  need to examine the <code> Rules </code> it can access, without
 *  breaking execution. However, an assessment may only be useful if
 *  all <code> Rules </code> referenced by it are available, and a
 *  test-taking applicationmay sacrifice some interoperability for the
 *  sake of precision.
 */

public final class RuleLookupSession
    extends net.okapia.osid.jamocha.core.rules.spi.AbstractIndexedMapRuleLookupSession
    implements org.osid.rules.RuleLookupSession {


    /**
     *  Constructs a new <code>RuleLookupSession</code>.
     *
     *  @param engineId
     *  @throws org.osid.NotFoundException
     *  @throws org.osid.NullArgumentException <code>engineId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException
     */
  
    protected RuleLookupSession(org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        load(engineId);
        return;
    }


    protected void load(org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(engineId, "engine Id");

        try {
            String file = getEngineFile(engineId);
            XMLUtil xml = new XMLUtil(file + "-time.xml");
            xml.addNamespace("c", "urn:inet:osid.org:schemas:providers/rules/time/1");
            org.dom4j.Element root = xml.getElement("/c:engine", true);

            String name = file;
            String description = "A rules file.";

            org.dom4j.Element element = xml.getElement(root, "c:displayName", false);
            if (element != null) {
                name = xml.trim(element.asXML());
            }

            element = xml.getElement(root, "c:description", false);
            if (element != null) {
                description = xml.trim(element.asXML());
            }
            
            setEngine(new net.okapia.osid.jamocha.builder.rules.engine.EngineBuilder()
                      .id(engineId)
                      .displayName(Html.valueOf(name))
                      .description(Html.valueOf(description))
                      .provider(net.okapia.osid.provider.Providers.OKAPIA.getProvider())
                      .build());

            loadRules(root, xml);
        } catch (org.osid.NotFoundException nfe) {
                throw nfe;
        } catch (Exception e) {
            throw new org.osid.OperationFailedException("unable to read configuration", e);
        }

        return;
    }


    private void loadRules(org.dom4j.Element root, XMLUtil xml)
        throws org.osid.OperationFailedException {       

        for (org.dom4j.Node node : xml.getNodes(root, "c:rule")) {
            String name = "rule";
            String description = "A rules.";
            
            org.dom4j.Element element = xml.getElement(node, "c:displayName", false);
            if (element != null) {
                name = xml.trim(element.asXML());
            }

            element = xml.getElement(node, "c:description", false);
            if (element != null) {
                description = xml.trim(element.asXML());
            }

            net.okapia.osid.jamocha.assembly.rules.evaluation.spi.AbstractTerm term = new net.okapia.osid.jamocha.assembly.rules.evaluation.RootTerm();
            for (org.dom4j.Node t : xml.getNodes(node, "c:term")) {
                loadRuleTerms(xml, t, term);
            }

            try {
                org.osid.id.Id id = new BasicId(getEngine().getId().getAuthority(), "rule", 
                                                getEngine().getId().getIdentifier() + "#" + 
                                                node.valueOf("@id"));

                putRule(new net.okapia.osid.jamocha.assembly.rules.evaluation.RuleBuilder()
                        .id(id)
                        .displayName(Html.valueOf(name))
                        .description(Html.valueOf(description))
                        .term(term)
                        .build());

            } catch (Exception e) {
                System.err.println("unable to get rule Id in " + name);
                continue;
            }
        }

        return;
    }


    private void loadRuleTerms(XMLUtil xml, org.dom4j.Node node, net.okapia.osid.jamocha.assembly.rules.evaluation.spi.AbstractTerm term) 
        throws org.osid.OperationFailedException {

        net.okapia.osid.jamocha.assembly.rules.evaluation.NodeTerm t = new net.okapia.osid.jamocha.assembly.rules.evaluation.NodeTerm(node.valueOf("@op"),
                                                                                                                                      xml.getBooleanAttribute(node, "negate"));
        
        term.addTerm(t);
        
        java.util.Iterator<?> ei = ((org.dom4j.Element) node).elementIterator();
        
        while (ei.hasNext()) {
            org.dom4j.Element e = (org.dom4j.Element) ei.next();
            
            if (e.getName().equals("timeRange")) {
                t.addTerm(new TimeRangeTerm(xml.getContent(e, "c:start"), xml.getContent(e, "c:end"),
                                            xml.getBooleanAttribute(e, "negate")));
            } else if (e.getName().equals("dateRange")) {
                t.addTerm(new DateRangeTerm(xml.getContent(e, "c:start"), xml.getContent(e, "c:end"),
                                            xml.getBooleanAttribute(e, "negate")));
            } else if (e.getName().equals("dayOfWeek")) {
                t.addTerm(new WeekdayTerm(e.valueOf("@day"), xml.getBooleanAttribute(e, "negate")));
            } else if (e.getName().equals("term")) {
                loadRuleTerms(xml, e, t);
            }
        }

        return;
    }


    /*
     * The format for these engine Ids is: file://path
     */

    private String getEngineFile(org.osid.id.Id engineId)
        throws org.osid.NotFoundException {

        if (!engineId.getIdentifierNamespace().equals("file") ||
            engineId.getAuthority().equals("local")) {
            throw new org.osid.NotFoundException("identifier not a local path");
        }

        return (engineId.getIdentifier());
    }
}
