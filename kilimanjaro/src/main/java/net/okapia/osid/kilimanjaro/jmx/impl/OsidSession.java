//
// OsidSession.java
//
//     A JMX Bean for an OSID Session.
//
//
// Tom Coppeto
// Okapia
// 27 June 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.jmx.impl;


public class OsidSession
    implements net.okapia.osid.kilimanjaro.jmx.OsidSessionMXBean {

    private final java.lang.ref.WeakReference<org.osid.OsidSession> session;

    private final OsidProvider provider;

    private final String name;
    private final String implementation;    
    private final boolean proxy;

    private final OsidStats stats;
    private final OsidStats cumulativeStats;

    private final javax.management.ObjectName objectName;
    private final javax.management.MBeanServer server;
    private static volatile long index = 0;


    protected OsidSession(OsidProvider provider, org.osid.OsidSession session, 
                          String name, String implementation, boolean proxy) 
        throws javax.management.JMException {

        this.session = new java.lang.ref.WeakReference<org.osid.OsidSession>(session);

        this.provider        = provider;
        this.name            = name;
        this.implementation  = implementation;
        this.proxy           = proxy;

        this.server          = java.lang.management.ManagementFactory.getPlatformMBeanServer();
        this.objectName      = new javax.management.ObjectName("net.okapia.osid.kilimanjaro.jmx:type=OsidSession,name=" + provider.getContext() + ++OsidSession.index);
        this.server.registerMBean(this, this.objectName);

        this.cumulativeStats = new OsidStats(provider.getContext(), provider.getCumulativeStatsCollector());
        this.stats           = new OsidStats(provider.getContext(), this.cumulativeStats);
        
        return;
    }


    public void destroy() {
        this.provider.removeSession(this);

        this.stats.destroy();
        this.cumulativeStats.destroy();

        try {
            this.server.unregisterMBean(this.objectName);
        } catch (javax.management.JMException e) {
            // ?
        }
    }


    public OsidStats getStatsCollector() {
        return (this.stats);
    }


    protected org.osid.OsidSession getOsidSession() {
        org.osid.OsidSession session = this.session.get();
        if (session == null) {
            destroy();
        }

        return (session);
    }


    /**
     *  Gets the package of the session.
     * 
     *  @return the session package
     */

    @Override
    public String getOsidPackage() {
        return (this.provider.getOsidPackage());
    }


    /**
     *  Gets the name of the session.
     * 
     *  @return the session name
     */

    @Override
    public String getName() {
        return (this.name);
    }


    /**
     *  Gets the implementation class name of the session.
     * 
     *  @return the class name
     */

    @Override
    public String getImplementationName() {
        return (this.implementation);
    }


    /**
     *  Gets the language type.
     *
     *  @return the language type
     */

    @Override
    public String getLanguageType() {
        org.osid.OsidSession session = getOsidSession();
        if (session == null) {
            return ("[not accessible]");
        }

        return (session.getLocale().getLanguageType().toString());
    }


    /**
     *  Gets the script type.
     *
     *  @return the script type
     */

    @Override
    public String getScriptType() {
        org.osid.OsidSession session = getOsidSession();
        if (session == null) {
            return ("[not accessible]");
        }

        return (session.getLocale().getScriptType().toString());
    }

    
    /**
     *  Gets the calendar type.
     *
     *  @return the calendar type
     */

    @Override
    public String getCalendarType() {
        org.osid.OsidSession session = getOsidSession();
        if (session == null) {
            return ("[not accessible]");
        }

        return (session.getLocale().getCalendarType().toString());
    }


    /**
     *  Gets the time type.
     *
     *  @return the time type
     */

    @Override
    public String getTimeType() {
        org.osid.OsidSession session = getOsidSession();
        if (session == null) {
            return ("[not accessible]");
        }

        return (session.getLocale().getTimeType().toString());
    }


    /**
     *  Gets the currency type.
     *
     *  @return the currency type
     */

    @Override
    public String getCurrencyType() {
        org.osid.OsidSession session = getOsidSession();
        if (session == null) {
            return ("[not accessible]");
        }

        return (session.getLocale().getCurrencyType().toString());
    }


    /**
     *  Gets the unit system type.
     *
     *  @return the unit system type
     */

    @Override
    public String getUnitSystemType() {
        org.osid.OsidSession session = getOsidSession();
        if (session == null) {
            return ("[not accessible]");
        }

        return (session.getLocale().getUnitSystemType().toString());
    }


    /**
     *  Gets the numeric format type.
     *
     *  @return the numeric format type
     */

    @Override
    public String getNumericFormatType() {
        org.osid.OsidSession session = getOsidSession();
        if (session == null) {
            return ("[not accessible]");
        }

        return (session.getLocale().getNumericFormatType().toString());
    }


    /**
     *  Gets the calendar format type.
     *
     *  @return the calendar format type
     */

    @Override
    public String getCalendarFormatType() {
        org.osid.OsidSession session = getOsidSession();
        if (session == null) {
            return ("[not accessible]");
        }

        return (session.getLocale().getCalendarFormatType().toString());
    }


    /**
     *  Gets the time format type.
     *
     *  @return the time format type
     */

    @Override
    public String getTimeFormatType() {
        org.osid.OsidSession session = getOsidSession();
        if (session == null) {
            return ("[not accessible]");
        }

        return (session.getLocale().getTimeFormatType().toString());
    }


    /**
     *  Gets the currency format type.
     *
     *  @return the currency format type
     */

    @Override
    public String getCurrencyFormatType() {
        org.osid.OsidSession session = getOsidSession();
        if (session == null) {
            return ("[not accessible]");
        }

        return (session.getLocale().getCurrencyFormatType().toString());
    }


    /**
     *  Gets the coordinate format type.
     *
     *  @return the coordinate format type
     */

    @Override
    public String getCoordinateFormatType() {
        org.osid.OsidSession session = getOsidSession();
        if (session == null) {
            return ("[not accessible]");
        }

        return (session.getLocale().getCoordinateFormatType().toString());
    }


    /**
     *  Gets the authenticated agent.
     *
     *  @return the authenticated agent
     */

    @Override
    public Agent getAuthenticatedAgent() {
        org.osid.OsidSession session = getOsidSession();
        if (session == null) {
            return (null);
        }

        if (session.isAuthenticated()) {
            try {
                return (new Agent(session.getAuthenticatedAgent()));
            } catch (org.osid.OperationFailedException oe) {
                return (new Agent(session.getAuthenticatedAgentId()));
            }
        } else {
            return (null);
        }
    }


    /**
     *  Gets the effective agent.
     *
     *  @return the effective agent
     */

    @Override
    public Agent getEffectiveAgent() {
        org.osid.OsidSession session = getOsidSession();
        if (session == null) {
            return (null);
        }

        try {
            return (new Agent(session.getEffectiveAgent()));
        } catch (org.osid.OperationFailedException oe) {
            return (new Agent(session.getEffectiveAgentId()));
        }
    }
     
   
    /**
     *  Gets the effective date for this session.
     *
     *  @return the effective date
     */

    @Override
    public java.util.Date getEffectiveDate() {
        org.osid.OsidSession session = getOsidSession();
        if (session == null) {
            return (null);
        }

        return (session.getDate());
    }


    /**
     *  Gets the stats for this OsidSession.
     *
     *  @return the stats
     */

    @Override
    public net.okapia.osid.kilimanjaro.jmx.OsidStatsMXBean getStats() {
        return (this.stats);
    }
}
