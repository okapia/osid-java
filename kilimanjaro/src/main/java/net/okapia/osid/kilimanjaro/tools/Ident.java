//
// Ident.java
//
//     Identifies an OSID.
//
//
// Tom Coppeto
// 30 September 2008
//
//
// Copyright (c) 2008 Massachusetts Institute of Technology. All Rights 
// Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.tools;

import org.osid.*;
import org.osid.type.*;
import org.osid.resource.*;
import org.osid.repository.*;

import net.okapia.osid.kilimanjaro.BootLoader;
import net.okapia.osid.kilimanjaro.OsidVersions;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.jar.*;


public class Ident {

    public void showRuntime(String impl, boolean verbose)
        throws Exception {

        BootLoader loader = new BootLoader();
        loader.setRuntimeImpl(impl);
        OsidRuntimeManager runtime = loader.getRuntimeManager("Ident");

        System.out.println("Ident for " + impl + ":");
        System.out.println();
        print("OSID", "runtime", 0);

        showManager(runtime, verbose);

        return; 
    }


    public void showOSID(String impl, boolean verbose)
        throws Exception {

        BootLoader loader = new BootLoader();
        OsidRuntimeManager runtime = loader.getRuntimeManager("Ident");

        OSID osid = getOSIDService(runtime, impl);
        OsidManager manager = runtime.getManager(osid, impl, OsidVersions.V3_0_0.getVersion());

        System.out.println("Ident for " + impl + ":");
        System.out.println();
        print("OSID", osid.getOSIDServiceName(), 0);

        showManager(manager, verbose);

        manager.close();
        runtime.close();

        return; 
    }


    private void showManager(OsidManager manager, boolean verbose)
        throws org.osid.OsidException {

        showProfile(manager);

        int [] numbers = getNumbers(manager);
        print("Lotto Numbers",  numbers[0] + "-" + numbers[1] + "-" + numbers[2] + "-" + 
                           numbers[3] + "-" + numbers[4] + " " + numbers[5], 5);

        System.out.println();   
        System.out.println("PROVIDER:");

        showObject(manager.getProvider(), 5);
        System.out.println();

        if (verbose) {
            try (org.osid.repository.AssetList assets = manager.getBranding()) {
                showAssets(assets, 0);
            }
        }

        return;
    }


    private void showManager(OsidProxyManager manager, boolean verbose)
        throws org.osid.OsidException {

        showProfile(manager);

        int [] numbers = getNumbers(manager);
        print("Lotto Numbers",  numbers[0] + "-" + numbers[1] + "-" + numbers[2] + "-" + 
                           numbers[3] + "-" + numbers[4] + " " + numbers[5], 5);

        System.out.println();   
        System.out.println("PROVIDER:");

        showObject(manager.getProvider(), 5);
        System.out.println();

        if (verbose) {
            showAssets(manager.getBranding(), 0);
        }

        return;
    }


    private void showProfile(org.osid.OsidProfile profile)
        throws org.osid.OsidException {

        print("Id", profile.getId().toString(), 5);
        print("Name", profile.getDisplayName().getText(), 5);
        print("Description", profile.getDescription().getText(), 5);
        print("Version",  profile.getVersion().toString(), 5);
        print("Release Date", profile.getReleaseDate().toString(), 5);

        System.out.println();
        print("License", profile.getLicense().getText(), 5);
        return;
    }


    private void showObject(org.osid.OsidObject object, int indent)
        throws org.osid.OsidException {

        print("Id", object.getId().toString(), indent);
        print("Name", object.getDisplayName().getText(), indent);
        print("Description", object.getDescription().getText(), indent);
        print("Genus", object.getGenusType().toString(), indent);
 
        try (TypeList types = object.getRecordTypes()) {
            int r = 0;
            while (types.hasNext()) {
                Type type = types.getNextType();
                System.out.println();
                print("Record (" + ++r + ")", type.getDisplayName().getText() + "(" + type + ")", indent);
                
                PropertyList properties = object.getPropertiesByRecordType(type);
                print("Properties", "", indent+5);
                while (properties.hasNext()) {
                    Property p = properties.getNextProperty();
                    print(p.getDisplayName().getText(), p.getValue(), indent + 10);
                }
            }
        }
              
        return;
    }


    private void showAssets(AssetList assets, int indent)
        throws org.osid.OsidException {

        while (assets.hasNext()) {
            print("ASSET", "", indent);
            Asset asset = assets.getNextAsset();
            showObject(asset, indent + 5);
            
            try (AssetContentList contents = asset.getAssetContents()) {
                System.out.println();
                print("Asset Contents", "", indent+5);
                
                int c = 0;
                while (contents.hasNext()) {
                    print("Asset Content (" + ++c + ")", "", indent+10);
                    AssetContent content = contents.getNextAssetContent();
                    if (content.hasDataLength()) {
                        print("Data Length", content.getDataLength() + " bytes", indent+15);
                    }
                    if (content.hasURL()) {
                        print("URL", content.getURL(), indent+15);
                    }

                    showObject(content, indent + 15);
                    System.out.println();
                }
            }
        }

        return;
    }


    private OSID getOSIDService(OsidRuntimeManager runtime, String name)
        throws Exception {

        Class<?> c = Class.forName(name);

        for (OSID osid : OSID.values()) {
            try {
                Class<? extends org.osid.OsidManager> osidClass = Class.forName(osid.getManager()).asSubclass(org.osid.OsidManager.class);
                if (osidClass.isAssignableFrom(c)) {
                    return (osid);
                }
            } catch (Exception e) {}

            try {
                Class<? extends org.osid.OsidProxyManager> osidProxyClass = Class.forName(osid.getProxyManager()).asSubclass(org.osid.OsidProxyManager.class);
                if (osidProxyClass.isAssignableFrom(c)) {
                    return (osid);
                }
            } catch (Exception e) {}
        }

        return (null);
    }


    private int[] getNumbers(Object manager) {
        int[] ret = new int[6];
        
        Random r = new Random(manager.getClass().getName().hashCode());
        
        for (int i = 0; i < 5; i++) {
            ret[i] = r.nextInt(56) + 1;
        }
        ret[5] = 100;
        Arrays.sort(ret);
        ret[5] = r.nextInt(46);
        
        return (ret);
    }


    private void print(String name, String value, int indent) {
        System.out.print(getMargin(indent));
        System.out.print(name + ":");
        System.out.print(getMargin(50-name.length()-1-indent));
        System.out.println(format(value, 50));
        return;
    }

        
    private String getMargin(int indent) {

        if (indent < 0) {
            return ("");
        }

        StringBuffer sb = new StringBuffer(indent);

        for (int i = 0; i < indent; i++) {
            sb.append(' ');
        }

        return (sb.toString());
    }


    private String format(String text, int amount) {
        StringBuffer buf = new StringBuffer(text.length());
        StringBuffer margin = new StringBuffer(amount);
        String[] tokens = text.split(" ");

        for (int i = 0; i < amount; i++) {
            margin.append(' ');
        }

        int col = 0;    
        for (String token: tokens) {
            String[] words = token.split("\n");
            boolean nl = false;

            for (String word : words) {
                if (nl) {
                    buf.append('\n');
                    buf.append(margin);
                    col = 0;
                }

                if ((col > 0) && ((col + word.length()) > (115 - amount))) {
                    buf.append('\n');
                    buf.append(margin);
                    col = 0;
                } else if (col > 0) {
                    buf.append(' ');
                    ++col;
                }
            
                buf.append(word);
                col += word.length();
                nl = true;
            }
        }

        return (buf.toString());
    }


    public void showJAR(String path)
        throws Exception {

        File file = new File(path);
        if (!file.exists()) {
            System.err.println(path + " does not exist");
        }
        
        if (!file.isFile()) {
            System.err.println(path + " is not a file");
        }

        try {
            JarFile jar = new JarFile(file);
            Manifest manifest = jar.getManifest();
            System.out.println("MANIFEST for " + path + ":");

            Attributes attrs = manifest.getMainAttributes();
            for (Object key : attrs.keySet()) {
                Attributes.Name attrName = (Attributes.Name) key;
                String attrValue = attrs.getValue(attrName);
                print(attrName.toString(), attrValue, 5);
            }

            System.out.println();
            System.out.println("OSIDs in " + path + ":");
            for (Enumeration<JarEntry> e = jar.entries(); e.hasMoreElements(); ) {
                managerInfo(jar, e.nextElement());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void managerInfo(JarFile jar, JarEntry entry)
        throws Exception {

        String className = entry.getName().replace("/", ".");
        if (!className.endsWith(".class")) {
            return;
        }

        className = className.replace(".class", "");

        URL[] urls = { new URL("jar:file:" + jar.getName() + "!/") };
        URLClassLoader loader = URLClassLoader.newInstance(urls, getClass().getClassLoader());

        Class<?> c = Class.forName(className, true, loader);
        if (c == null) {
            System.out.println("cannot create class");
        }

        for (OSID osid : OSID.values()) {
            try {
                Class<? extends org.osid.OsidManager> osidClass = Class.forName(osid.getManager()).asSubclass(org.osid.OsidManager.class);
                if (osidClass.isAssignableFrom(c)) {
                    print (osid.getOSIDServiceName(), className, 5);
                }

                Class<? extends org.osid.OsidProxyManager> osidProxyClass = Class.forName(osid.getProxyManager()).asSubclass(org.osid.OsidProxyManager.class);
                if (osidClass.isAssignableFrom(c)) {
                    print (osid.getOSIDServiceName() + " (proxy)", className, 5);
                }
            } catch (Exception e) { }
        }

        return;
    }


    public static void main(String[] args)
        throws Exception {

        String implName    = null;
        String jarName     = null;
        boolean jar        = false;
        boolean osid       = false;
        boolean verbose    = false;
        boolean runtime    = false;

        for (String arg : args) {
            if (arg.equals("-jar")) {
                jar = true;
                continue;
            }

            if (arg.equals("-osid")) {
                osid = true;
                continue;
            }

            if (arg.equals("-v")) {
                verbose = true;
                continue;
            }

            if (arg.equals("-r")) {
                runtime = true;
                continue;
            }

            if (jar && (jarName == null)) {
                jarName = arg;
                continue;
            }

            if (osid && (implName == null)) {
                implName = arg;
                continue;
            }

            System.err.println("usage: Ident [-v] [-r] [-jar <jar>] [-osid <impl>]");
            if (arg.equals("-h")) {
                System.exit(0);
            }

            System.exit(1);
        }


        Ident ident = new Ident();
        if (runtime && (implName != null)) {
            ident.showRuntime(implName, verbose);
        } else if (osid && (implName != null)) {
            ident.showOSID(implName, verbose);
        } else if (jar && (jarName != null)) {
            ident.showJAR(jarName);
        } else {
            System.err.println("usage: Ident [-v] [-r] [-jar <jar>] [-osid <impl>]");
            System.exit(1);
        }

        return;
    }
}

