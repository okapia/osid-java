//
// RulesSession.java
//
//    Simple implementation framework for providing a Rules
//    service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.providers.rules.time;


/**
 *  This session provides methods to evaluate and execute rules.
 */

public final class RulesSession
    extends net.okapia.osid.jamocha.assembly.rules.evaluation.spi.AbstractRulesSession
    implements org.osid.rules.RulesSession {

   
    /**
     *  Constructs a new <code>RulesSession</code>.
     *
     *  @param engineId
     *  @throws org.osid.NotFoundException
     *  @throws org.osid.NullArgumentException <code>engineId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException
     */
  
    protected RulesSession(org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        try {
            org.osid.rules.RuleLookupSession session = new RuleLookupSession(engineId);
            setEngine(session.getEngine());

            org.osid.rules.RuleList rules = session.getRules();

            while(rules.hasNext()) {
                org.osid.rules.Rule rule = rules.getNextRule();
                if (rule instanceof net.okapia.osid.jamocha.assembly.rules.evaluation.EvaluatingRule) {
                    addRule((net.okapia.osid.jamocha.assembly.rules.evaluation.EvaluatingRule) rule);
                }
            }
        } catch (org.osid.PermissionDeniedException pde) {
            throw new org.osid.OperationFailedException(pde);
        }

        return;
    }
}
