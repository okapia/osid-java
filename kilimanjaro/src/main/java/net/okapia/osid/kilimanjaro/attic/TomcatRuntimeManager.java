//
// TomcatRuntimeManager.java
//
//     Specifies the OSID definition for osid.OsidRuntimeManager.
//
//
// Tom Coppeto
// 28 June 2008
//
// Copyright (c) 2008 Massachusetts Institute of Technology. All Rights 
// Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;

import net.okapia.osid.runtime.types.records.proxy.proxycondition.PathProxyConditionRecord;


/**
 *  The <code> OsidRuntimeManager </code> represents and OSID platform
 *  and contains the information required for running OSID
 *  implementations such as search paths and configurations.
 *  
 *  The <code> OsidRuntimeManager </code> is defined as an interface
 *  to provide flexibility for managing an OSID environment. The
 *  instantiation of a <code> OsidRuntimeManager </code>
 *  implementation is defined by the OSID platform.
 *  
 *  The <code> OsidRuntimeManager </code> should be instantiated with
 *  a string that identifies the application or environment current at
 *  the time of instantiation. This key is used soley for the purpose
 *  of seeding the configuration service as a means to enable lower
 *  level OSIDs to tune their configuration in response to this key,
 *  or, it can be used by the application to retrieve configuration
 *  data for itself.
 */

public class TomcatRuntimeManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.OsidRuntimeManager {

    private static final org.osid.id.Id ID   = net.okapia.osid.primordium.id.BasicId.valueOf("OSID Runtime:tomcat@okapia.net");
    private static final String DISPLAY_NAME = "Tomcat Runtime Manager";
    private static final String DESCRIPTION  = "A Tomcat OSID runtime manager.";
    private static final org.osid.installation.Version VERSION  = net.okapia.osid.primordium.installation.SoftwareVersion.valueOf("0.0.3");
    private static final org.osid.calendaring.DateTime RELEASE_DATE = net.okapia.osid.primordium.calendaring.GregorianDateTime.valueOf("2009-1-5");

    private TomcatRuntimeManager parent;
    protected org.osid.configuration.ConfigurationProxyManager configMgr;
    protected org.osid.proxy.ProxyManager proxyMgr;
    private String context;
    private boolean init = false;


    /**
     *  Creates a new runtime manager. The BootLoader uses 
     *  this constructor.
     *
     *  @param context an application string
     *  @throws org.osid.NullArgumentException <code>context</code>
     *          is <code>null</code>
     */

    public TomcatRuntimeManager(String context) {
        super(ID);
        setMetadata(DISPLAY_NAME, DESCRIPTION, VERSION, RELEASE_DATE, 
                    net.okapia.osid.provider.Providers.OKI.getProvider());

        nullarg(context, "context");
        this.context = context;
        return;
    }


    /**
     *  Creates a new runtime manager. The BootLoader uses 
     *  this constructor.
     *
     *  @param context an application string
     *  @param parent the parent runtime
     *  @throws org.osid.NullArgumentException <code>context</code>
     *          is <code>null</code>
     */

    public TomcatRuntimeManager(String context, TomcatRuntimeManager parent) {
        this(context);
        this.parent = parent;
        return;
    }


    /**
     *  Initializes this manager. A manager is initialized once at the time of 
     *  creation. 
     *
     *  @param  runtime the runtime environment 
     *  @throws org.osid.ConfigurationErrorException an error with 
     *          implementation configuration 
     *  @throws org.osid.IllegalStateException this manager has already been 
     *          initialized by the <code> OsidLoader </code> 
     *  @throws org.osid.NullArgumentException <code> runtime </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public void initialize(org.osid.OsidRuntimeManager runtime)
        throws org.osid.OperationFailedException {

        super.initialize(runtime);
        // loadConfiguration("osid.properties");
    }


    void initializeLocal()
        throws org.osid.OperationFailedException {

        if (this.init) {
            return;
        }

        this.init = true;

        if (this.parent == null) {
            try {
                org.osid.orchestration.OrchestrationManager mgr = (org.osid.orchestration.OrchestrationManager) getRuntime().getManager(org.osid.OSID.ORCHESTRATION, "net.okapia.osid.runtime.providers.configuration.tomcat.OrchestrationManager", "3.0.0");
                this.configMgr = mgr.getConfigurationProxyManager();
                this.proxyMgr  = mgr.getProxyManager();
            } catch (org.osid.NotFoundException nfe) {
                throw new org.osid.ConfigurationErrorException("orchestration service", nfe);
            }
        } else {
            this.configMgr = this.parent.configMgr;
            this.proxyMgr  = this.parent.proxyMgr;
        }

        return;
    }


    /**
     *  Tests if a configuration service is provided within this runtime 
     *  environment. 
     *
     *  @return <code> false </code>
     */

    @OSID @Override
    public boolean supportsConfiguration() {
        return (false);
    }


    /**
     *  Finds, loads and instantiates providers of OSID managers. Providers 
     *  must conform to an OsidManager interface. The interfaces are defined 
     *  in the OSID enumeration. For all OSID requests, an instance of <code> 
     *  OsidManager </code> that implements the <code> OsidManager </code> 
     *  interface is returned.
     *
     *  @param  osid represents the OSID 
     *  @param  implClassName the name of the implementation 
     *  @param  version the minimum required interface version 
     *  @return the manager of the service 
     *  @throws org.osid.NotFoundException the implementation class name was 
     *          not found 
     *  @throws org.osid.NullArgumentException <code>null</code> argument
     *          provided
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> implClassName </code> 
     *          does not support the requested OSID 
     */

    @OSID @Override
    public org.osid.OsidManager getManager(org.osid.OSID osid, 
                                           String implClassName, 
                                           String version)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(osid, "osid");  
        nullarg(implClassName, "implementation");       
        nullarg(version, "version");    

        initializeLocal();

        /*
         * get the class corresponding to the requested OSID interface
         */

        Class<? extends org.osid.OsidManager> osidInterface;
        
        try {
            osidInterface = getClass().forName(osid.getManager()).asSubclass(org.osid.OsidManager.class);
        } catch (Throwable t) {
            throw new org.osid.NotFoundException(osid.getManager() + " was not found. Check for availability of OSID package.", t);
        }

        return (instantiateManager(osidInterface, implClassName, version));
    }


    /**
     *  Finds, loads and instantiates providers of OSID managers. Providers 
     *  must conform to an <code> OsidManager </code> interface. The 
     *  interfaces are defined in the OSID enumeration. For all OSID requests, 
     *  an instance of <code> OsidManager </code> that implements the <code> 
     *  OsidManager </code> interface is returned.
     *
     *  @param  osid represents the OSID 
     *  @param  implementation the name of the implementation 
     *  @param  version the minimum required interface version 
     *  @return the manager of the service 
     *  @throws org.osid.NotFoundException the implementation package was not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>null</code> argument
     *          provided
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> implementation </code> 
     *          does not support the requested OSID 
     */

    @OSID @Override
    public org.osid.OsidProxyManager getProxyManager(org.osid.OSID osid, 
                                                     String implementation, 
                                                     String version)
        throws org.osid.NotFoundException,
               org.osid.NullArgumentException,
               org.osid.OperationFailedException,
               org.osid.UnsupportedException {

        nullarg(osid, "osid");  
        nullarg(implementation, "implementation");      
        nullarg(version, "version");    

        initializeLocal();

        /*
         * get the class corresponding to the requested OSID interface
         */

        Class<? extends org.osid.OsidProxyManager>  osidInterface;
        
        try {
            osidInterface = getClass().forName(osid.getProxyManager()).asSubclass(org.osid.OsidProxyManager.class);
        } catch (Throwable t) {
            throw new org.osid.NotFoundException(osid.getProxyManager() + " was not found. Check for availability of OSID package.", t);
        }

        return (instantiateProxyManager(osidInterface, implementation, version));
    }


    /**
     *  Gets the current configuration in the runtime environment. 
     *
     *  @return a configuration 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException an authorization failure 
     *          occured 
     *  @throws org.osid.UnimplementedException a configuration service is not 
     *          supported 
     */

    @OSID @Override
    public org.osid.configuration.ValueLookupSession getConfiguration()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        initializeLocal();
        org.osid.proxy.ProxySession session = this.proxyMgr.getProxySession();

        /*
         * Set the OSID path in a Proxy.
         */

        org.osid.proxy.ProxyCondition specifier = session.getProxyCondition();
        if (!specifier.hasRecordType(PathProxyConditionRecord.TYPE)) {
            throw new org.osid.OperationFailedException("configuration record not available");
        }

        PathProxyConditionRecord ppcr = (PathProxyConditionRecord) specifier.getProxyConditionRecord(PathProxyConditionRecord.TYPE);
        ppcr.setPath(getPath());
        org.osid.proxy.Proxy proxy = session.getProxy(specifier);

        return (this.configMgr.getValueLookupSession(proxy));
    }


    private org.osid.OsidManager instantiateManager(Class <? extends org.osid.OsidManager> osid, 
                                                    String implClassName, String version)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {
        

        /*
         * get the implementation class
         */

        Class<? extends org.osid.OsidManager> managerClass;
        try {
            managerClass = getClass().forName(implClassName).asSubclass(org.osid.OsidManager.class);
        } catch (Throwable t) {
            throw new org.osid.NotFoundException(implClassName + " was not found", t);
        }

        if (!org.osid.binding.java.DynamicManager.class.isAssignableFrom(managerClass) &&
            !osid.isAssignableFrom(managerClass)) {
            throw new org.osid.UnsupportedException(implClassName + " does not implement " + 
                                                    osid.getClass().getName());
        } 


        /*
         * create a new instance of the manager
         */

        org.osid.OsidManager manager;
        try {
            manager = managerClass.newInstance();
        } catch (Throwable t) {
            throw new org.osid.OperationFailedException("unable to create instance of " + implClassName, t);
        }


        /*
         *  If the manager is a dynamic java adapter, start it up.
         */

        Class[] interfaces = new Class[] { osid };
        if (org.osid.binding.java.DynamicManager.class.isAssignableFrom(managerClass)) {
            manager = (org.osid.OsidManager) java.lang.reflect.Proxy.newProxyInstance(manager.getClass().getClassLoader(),
                                                                                           interfaces,
                                                                                           (org.osid.binding.java.DynamicManager) manager);
        }


        /*
         * Bind the nw manager to the contract.
         */

        manager = net.okapia.osid.runtime.executor.BoilerPlate.bind(manager, osid);


        /*
         *  Spin off an instance of this runtime.
         */

        org.osid.OsidRuntimeManager runtime = net.okapia.osid.runtime.executor.BoilerPlate.accept(new TomcatRuntimeManager(implClassName, this));
        runtime.initialize(this);


        /*
         *  Initialize the manager through the contract.
         */

        try {
            manager.initialize(runtime);
        } catch (Throwable t) {
            throw new org.osid.OperationFailedException("unable to initialize " + implClassName, t);
        }


        /* 
         * check for version compatibility
         */

        if (!manager.supportsOSIDVersion(version)) {
            throw new org.osid.UnsupportedException(implClassName + " does not support version " + version);
        }

        return (manager);
    }



    private org.osid.OsidProxyManager instantiateProxyManager(Class<? extends org.osid.OsidProxyManager> osid, 
                                                              String implClassName, String version)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {
        
        /*
         * get the implementation class
         */

        Class<? extends org.osid.OsidProxyManager> managerClass;
        try {
            managerClass = getClass().forName(implClassName).asSubclass(org.osid.OsidProxyManager.class);
        } catch (Throwable t) {
            throw new org.osid.NotFoundException(implClassName + " was not found", t);
        }

        if (!org.osid.binding.java.DynamicManager.class.isAssignableFrom(managerClass) &&
            !osid.isAssignableFrom(managerClass)) {
            throw new org.osid.UnsupportedException(implClassName + " does not implement " + 
                                                    osid.getClass().getName());
        }

        /*
         * create a new instance of the manager
         */

        org.osid.OsidProxyManager manager;
        try {
            manager = managerClass.newInstance();
        } catch (Throwable t) {
            throw new org.osid.OperationFailedException("unable to create instance of " + implClassName, t);
        }


        /*
         *  If the manager is a dynamic java adapter, start it up.
         */

        Class[] interfaces = new Class[] { osid };
        if (org.osid.binding.java.DynamicManager.class.isAssignableFrom(managerClass)) {
            manager = (org.osid.OsidProxyManager) java.lang.reflect.Proxy.newProxyInstance(manager.getClass().getClassLoader(),
                                                                                           interfaces,
                                                                                           (org.osid.binding.java.DynamicManager) manager);
        }


        /*
         * Bind the nw manager to the contract.
         */

        manager = net.okapia.osid.runtime.executor.BoilerPlate.bind(manager, osid);


        /*
         *  Spin off an instance of this runtime.
         */

        org.osid.OsidRuntimeManager runtime = net.okapia.osid.runtime.executor.BoilerPlate.accept(new TomcatRuntimeManager(implClassName, this));
        runtime.initialize(this);


        /*
         *  Initialize the manager through the contract.
         */

        try {
            manager.initialize(runtime);
        } catch (Throwable t) {
            throw new org.osid.OperationFailedException("unable to initialize " + implClassName, t);
        }


        /* 
         * check for version compatibility
         */

        if (!manager.supportsOSIDVersion(version)) {
            throw new org.osid.UnsupportedException(implClassName + " does not support version " + version);
        }

        return (manager);
    }


    /**
     *  Shuts down this manager.
     */

    @OSID @Override
    public void close() {
        
        /* shutdown managers if this is the root */
        if (this.init && (this.parent == null)) {
            this.configMgr.close();
            this.proxyMgr.close();
        }

        super.close();
        return;
    }


    protected String getContext() {
        return (this.context);
    }

    
    protected TomcatRuntimeManager getParentRuntime() {
        return (this.parent);
    }


    protected String[] getPath() {
        java.util.Collection<String> path = new java.util.ArrayList<String>();
        getPath(this, path);
        return (path.toArray(new String[path.size()]));
    }

    
    protected void getPath(TomcatRuntimeManager runtime, java.util.Collection<String> path) {
        if (runtime.getParentRuntime() != null) {
            getPath(runtime.getParentRuntime(), path);
        }

        path.add(runtime.getContext()); 
        return;
    }
        
    
    private String getPackageName(String name) {
        int index = name.lastIndexOf(".");
        if (index > 0) {
            name = name.substring(0, index);
        }

        return (name);
    }
}
