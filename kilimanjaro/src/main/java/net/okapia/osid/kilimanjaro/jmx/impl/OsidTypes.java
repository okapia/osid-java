//
// OsidTypes.java
//
//     A JMX Bean for an OSID Types.
//
//
// Tom Coppeto
// Okapia
// 27 June 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.jmx.impl;


public class OsidTypes 
    implements net.okapia.osid.kilimanjaro.jmx.OsidTypesMXBean {

    private final String id;
    private final java.util.Map<String, java.util.List<String>> types = new java.util.HashMap<>();


    public OsidTypes(String id) {
        this.id = id;
        return;
    }


    /**
     *  Gets an identifier for this record.
     *
     *  @return an identifier
     */

    @Override
    public String getId() {
        return (this.id);
    }


    /**
     *  Gets the types supported by this provider.
     *
     *  @return supported types
     */

    @Override
    public java.util.Map<String, java.util.List<String>> getTypes() {
        return (java.util.Collections.unmodifiableMap(this.types));
    }

    
    public void addType(String key, String type) {
        java.util.List<String> types = this.types.get(key);
        if (types == null) {
            types = new java.util.ArrayList<>();
            this.types.put(key, types);
        }

        types.add(type);
        return;
    }

    
    public void addTypes(String key, java.util.List<String> types) {
        for (String type : types) {
            addType(key, type);
        }

        return;
    }
}
