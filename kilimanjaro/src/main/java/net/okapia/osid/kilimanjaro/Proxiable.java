package net.okapia.osid.kilimanjaro;

public interface Proxiable {

    /**
     *  Starts a proxy. Should only be used by BootLoader.
     */

    public org.osid.OsidRuntimeManager startProxy();
}

