//
// Condition.java
//
//     A Condition for the configuration rules.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.providers.rules.path;

import net.okapia.osid.kilimanjaro.types.records.rules.condition.PathConditionRecord;
import net.okapia.osid.kilimanjaro.types.records.rules.condition.ValueConditionRecord;


/**
 *  Defines a simple Condition to draw from.
 */

public class Condition
    extends net.okapia.osid.jamocha.rules.condition.spi.AbstractCondition
    implements org.osid.rules.Condition,
	       org.osid.rules.records.ConditionRecord,
	       PathConditionRecord,
	       ValueConditionRecord {

    private String[] path;
    private java.util.Collection<org.osid.configuration.Value> values = new java.util.ArrayList<>();
    private java.util.Collection<org.osid.configuration.ValueList> lists = new java.util.ArrayList<>();
    

    /**
     *  Creates a new <code>Condition</code>.
     */

    public Condition() {
        addConditionRecord(this, PathConditionRecord.TYPE);
        addConditionRecord(this, ValueConditionRecord.TYPE);
        return;
    }
        
    
    /**
     *  Gets the <code> Condition </code> from which this record originated. 
     *
     *  @return the condition 
     */

    public org.osid.rules.Condition getCondition() {
        return (this);
    }


    /**
     *  Tests if the given type is implemented by this record. Other
     *  types than that directly indicated by <code> getType() </code>
     *  may be supported through an inheritance scheme where the given
     *  type specifies a record that is a parent interface of the
     *  interface specified by <code> getType(). </code>
     *
     *  @param  recordType a type 
     *  @return <code> true </code> if the given record <code> Type
     *          </code> is implemented by this record, <code> false
     *          </code> otherwise
     *  @throws org.osid.NullArgumentException <code> recordType
     *          </code> is <code> null </code>
     */

    public boolean implementsRecordType(org.osid.type.Type recordType) {
        return (hasRecordType(recordType));
    }


    /**
     *  Adds a configuration value to this condition.
     *
     *  @param value an OSID Configuration Value
     *  @throws org.osid.NullArgumentException <code>value</code> is
     *          <code>null</code>
     */

    public void addValue(org.osid.configuration.Value value)  {
        this.values.add(value);
        return;
    }


    /**
     *  Adds configuration values to this condition.
     *
     *  @param values OSID Configuration Values
     *  @throws org.osid.NullArgumentException <code>values</code> is
     *          <code>null</code>
     */

    public void addValueList(org.osid.configuration.ValueList values) {
        this.lists.add(values);
        return;
    }


    /**
     *  Gets the values in this condiiton.
     *
     *  @return the values
     */

    protected org.osid.configuration.ValueList getValues() {
        net.okapia.osid.jamocha.adapter.federator.configuration.value.CompositeValueList ret = new net.okapia.osid.jamocha.adapter.federator.configuration.value.CompositeValueList();
        ret.addValueList(new net.okapia.osid.jamocha.configuration.value.ArrayValueList(this.values));

        for (org.osid.configuration.ValueList list : this.lists) {
            ret.addValueList(list);
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Sets the path in this condition.
     *
     *  @param path a path represented by an array of strings
     *  @throws org.osid.NullArgumentException <code>path</code> is
     *          <code>null</code>
     */

    public void setPath(String[] path) {
        this.path = path;
        return;
    }


    /**
     *  Gets the path in this condiiton.
     *
     *  @return the path
     */

    protected String[] getPath() {
        return (this.path);
    }
}
