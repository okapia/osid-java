//
// ObjectTerm.java
//
//     Validates the state of various objects based on the OSID
//     specifications.
//
//
// Tom Coppeto
// Okapia
// 4 January 2009
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.executor;

import net.okapia.osid.primordium.locale.text.eng.us.Plain;
import net.okapia.osid.torrefacto.util.StringUtil;


/**
 *  Validates the state of various objects based on the OSID
 *  specifications. This class is only used by the BoilerPlate
 *  contract.
 */

public final class ObjectTerm
    extends AbstractIdentifiableTerm
    implements FinePrint {


    /**
     *  Constructs a new <code>ObjectTerm</code>.
     *  
     *  @param parent
     *  @param spec interface specification
     */

    protected ObjectTerm(FinePrint parent, org.osid.meta.OsidInterface spec) {
        super(parent, spec, parent.getStatsCollector());
        return;
    }

    
    /**
     *  Applies a little consideration upon the return values.
     *
     *  @param method the OSID spec
     *  @param delegate the delegate object
     *  @param ret the return object
     */

    protected Object consideration(org.osid.meta.OsidMethod method, Object delegate, Object ret) {

        /* supply default names & descriptions if not set */
        /*
        if (method.getName().equals("getDisplayName")) {
            if (((org.osid.locale.DisplayText) ret).getText().equals("")) {
                return (processDisplayName(delegate));
            } else {
                return (ret);
            }
        } 

        if (method.getName().equals("getDescription")) {
            if (((org.osid.locale.DisplayText) ret).getText().equals("")) {
                return (processDescription(delegate));
            } else {
                return (ret);
            }
        }
        */
        return (super.consideration(method, delegate, ret));
    }


    /**
     *  Handle the getDisplayName() method.
     *  
     *  @param delegate
     *  @return string
     */

    protected org.osid.locale.DisplayText processDisplayName(Object delegate) {
        org.osid.OsidObject obj = (org.osid.OsidObject) delegate;
        return (Plain.valueOf(getClassName() + " (" + obj.getId() + ")"));
    } 


    /**
     *  Handle the getDescription() method.
     *  
     *  @param delegate
     *  @return string
     */

    protected org.osid.locale.DisplayText processDescription(Object delegate) {
        org.osid.OsidObject obj = (org.osid.OsidObject) delegate;
        String dn = obj.getDisplayName().getText();
        String cn = getClassName();

        if (dn.equals("")) {
            return (Plain.valueOf(StringUtil.getUpperArticle(cn) + " " + cn + "."));
        } else {
            return (Plain.valueOf(StringUtil.getUpperArticle(dn) + " " + dn + " " + cn + "."));
        }
    } 


    /**
     *  Handle the toString() method.
     *  
     *  @param delegate
     *  @return string
     */

    @Override
    public String processToString(Object delegate) {
        String s = delegate.toString();

        if (s.startsWith(delegate.getClass().getName())) {
            org.osid.OsidObject obj = (org.osid.OsidObject) delegate;
            return (obj.getDisplayName() + " [" + getClassName() + ":" + obj.getId() + "]");
        } else {
            return (s);
        }
    } 


    /**
     *  Creates a fine print term.
     *
     *  @param parent the parent's fine print
     *  @param spec specification interface
     *  @param object the delegate object
     *  @throws org.osid.OsidRuntimeException interfaceName not defined
     */

    protected static FinePrint createTerm(FinePrint parent, org.osid.meta.OsidInterface spec, Object object) {
        return (new ObjectTerm(parent, spec));
    }
}
