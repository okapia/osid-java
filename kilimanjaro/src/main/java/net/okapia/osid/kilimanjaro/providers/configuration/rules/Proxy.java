//
// Proxy.java
//
//    A proxy interface.
//
//
// Tom Coppeto
// Okapia
// 30 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.providers.configuration.rules;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDRecord;

import net.okapia.osid.kilimanjaro.types.records.proxy.proxy.PathProxyRecord;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A proxy to retrieve the OSID path.
 */

public final class Proxy 
    extends net.okapia.osid.jamocha.proxy.proxy.spi.AbstractProxy
    implements org.osid.proxy.Proxy,
               org.osid.proxy.records.ProxyRecord,
               PathProxyRecord {

    private String[] path = new String[0];


    /**
     *  Constructs a new <code>Proxy</code>.
     *
     *  @param path
     *  @throws org.osid.NullArgumentException <code>path</code> is
     *          <code>null</code>
     */

    protected Proxy(String[] path) {
        nullarg(path, "path");
        this.path = path;
        addProxyRecord(this, PathProxyRecord.TYPE);
        return;
    }


    /**
     *  Tests if the given type is implemented by this record. Other
     *  types than that directly indicated by <code> getType() </code>
     *  may be supported through an inheritance scheme where the given
     *  type specifies a record that is a parent interface of the
     *  interface specified by <code> getType(). </code>
     *
     *  @param  recordType a type 
     *  @return <code> true </code> if the given record <code> Type
     *          </code> is implemented by this record, <code> false
     *          </code> otherwise
     *  @throws org.osid.NullArgumentException <code> recordType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean implementsRecordType(org.osid.type.Type recordType) {
        nullarg(recordType, "record type");
        return (PathProxyRecord.TYPE.equals(recordType));
    }


    /*
     *  Gets the path.
     *
     *  @return the path
     */

    @OSIDRecord @Override
    public String[] getPath() {
        return (this.path);
    }
}
