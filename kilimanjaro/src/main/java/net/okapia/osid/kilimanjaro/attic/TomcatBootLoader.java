//
// TomcatBootLoader.java
//
//     Gets the OSID Runtime environment.
//
//
// Tom Coppeto
// OnTapSolutions
// 27 September 2008
//
// Copyright (c) 2008 Massachusetts Institute of Technology. All Rights
// Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//


package net.okapia.osid.kilimanjaro;


/**
 *  Bootstraps the OsidRuntime environment.
 *
 *  The name of the runtime implementation is first searched for in
 *  the environment variable <code>OSID_RUNTIME_IMPL</code>. This can
 *  be set to override any default behavior of the implementation
 *  search.
 *
 *  If that variable isn't set, a Java-esque properties file called
 *  <code>osid.conf</code> is checked for the property
 *  "runtime_impl". The location of this properties file may be either
 *  be set in an environment variable, or be located in one of
 *  <code>/usr/osid</code>,
 *  <code>[~]/Library/OSID/Configuration</code> or
 *  <code>C:\OSID\config</code>. The location of this file may also be
 *  overridden through the <code>${OSID_CONFIG}</code> environment
 *  variable.
 *
 *  If the implementation name still cannot be found, 
 *  "org.osid.impl.runtime.RuntimeManager" is used.
 *
 *  @see org.osid.OsidRuntimeManager
 */

public class TomcatBootLoader
    extends BootLoader {

    private String impl = "net.okapia.osid.runtime.TomcatRuntimeManager";
    protected boolean debug = false;


    /**
     *  Constructs a new <code>TomcatBootLoader</code>.
     *
     *  @throws org.osid.OperationFailedException a bootloader
     *          properties was found but an error was encountered
     *          reading it
     */

    public TomcatBootLoader()
        throws org.osid.OperationFailedException {

        setRuntimeImpl(this.impl);
        return;
    }


    /**
     *  Instantiates and returns the OSID runtime manager. 
     *
     *  @param sc servlet config
     *  @return the runtime manager
     *  @throws org.osid.NotFoundException the OSID interface
     *          definition or the runtime manager implementation was
     *          not found
     *  @throws org.osid.NullArgumentException <code>context</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException the runtime manager
     *          implementation could not be instantiated or initialized
     *  @throws org.osid.UnsupportedException the runtime manager
     *          does not implement the org.osid.OsidRuntimeManager
     *          interface
     */

    public org.osid.OsidRuntimeManager getRuntimeManager(javax.servlet.ServletConfig sc)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        if (sc == null) {
            throw new org.osid.NullArgumentException("servlet config is null");
        } 

        return (super.getRuntimeManager(sc.getServletName()));
    }
}
