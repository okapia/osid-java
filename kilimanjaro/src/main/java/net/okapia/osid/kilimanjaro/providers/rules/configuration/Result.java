//
// Result.java
//
//     Defines a Result.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.providers.rules.configuration;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDRecord;

import net.okapia.osid.primordium.id.BasicId;

import net.okapia.osid.kilimanjaro.types.records.rules.condition.PathConditionRecord;
import net.okapia.osid.kilimanjaro.types.records.rules.condition.ValueConditionRecord;
import net.okapia.osid.kilimanjaro.types.records.rules.result.ValueResultRecord;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a Result. Since the evaluation results in a
 *  <code>ValueList</code>, processing needs to occur here so that
 *  multiple invocations of <code>getValues()</code> through the
 *  result record return distinct <code>ValueLists</code>.
 */

public final class Result
    extends net.okapia.osid.jamocha.rules.result.spi.AbstractResult
    implements org.osid.rules.Result,
               org.osid.rules.records.ResultRecord,
               ValueResultRecord {

    private Condition condition;
    private RulesSession session;
    private boolean result = false;
    private org.osid.configuration.ValueList values;


    /**
     *  Creates a new <code>Result</code>. The values are buffered
     *  inside this object.
     *
     *  @param session
     *  @throws org.osid.NullArgumentException
     *  @throws org.osid.OperationFailedException
     */

    public Result(Condition condition, RulesSession session)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        nullarg(condition, "condition");
        nullarg(session, "session");

        this.session = session;
        this.condition = condition;

        addResultRecord(this, ValueResultRecord.TYPE);

        execute();
        return;
    }


    /**
     *  Gets a boolean result from the rule evaluation. Based on the
     *  definition of the Rule, the result may be a simple boolean value or
     *  something more complex defined in the record type.
     *
     *  @return <code> true </code> or <code> false </code>
     */

    @OSID @Override
    public boolean getBooleanValue() {
        if (this.values == null) {
            try {
                execute();
            } catch (Exception e) {
                return (false);
            }
        }

        return (this.result);
    }

    
    /**
     *  Tests if the given type is implemented by this record. Other types 
     *  than that directly indicated by <code> getType() </code> may be 
     *  supported through an inheritance scheme where the given type specifies 
     *  a record that is a parent interface of the interface specified by 
     *  <code> getType(). </code> 
     *
     *  @param  recordType a type 
     *  @return <code> true </code> if the given record <code> Type </code> is 
     *          implemented by this record, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> recordType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean implementsRecordType(org.osid.type.Type recordType) {
        nullarg(recordType, "record type");
        return (TYPE.equals(recordType));
    }


    /**
     *  Gets all the valid configuration values in order of preference. 
     *
     *  @return valid configuration values
     *  @throws org.osid.OperationFailedException 
     */

    @OSIDRecord @Override
    public org.osid.configuration.ValueList getValues()
        throws org.osid.OperationFailedException {

        if (this.values == null) {
            try {
                execute();
            } catch (Exception e) {
                throw new org.osid.OperationFailedException("unable to get values", e);
            }
        }

        org.osid.configuration.ValueList ret = this.values;
        this.values = null;
        return (ret);
    }


    private void execute()
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        /*
         *  Get the path and input value list
         */

        String[] path = this.condition.getPathRecord().getPath();
        org.osid.configuration.ValueList values = this.condition.getValueRecord().getValues();

        /*
         * Launch the processing stream for the time filter.
         */

        if (this.session.time != null) {
            values = new net.okapia.osid.jamocha.inline.processing.configuration.value.FilteringValueList(values, this.session.time, 
                                                                                                          this.session.parser.getTimeRules(),
                                                                                                          true);
        }
        
        if (this.session.path != null) {
            
            /*
             * Fill in the condition record for the path filter. It has
             * the same structure as this one.
             */
            
            /* one fixed Id for the path provider */
            org.osid.id.Id pruleId = BasicId.valueOf("osid.rules.Rule:path@okapia.net");
            
            /* get and sets the path condition record */
            org.osid.rules.Condition pc = this.session.path.getConditionForRule(pruleId);
            
            if (!pc.hasRecordType(PathConditionRecord.TYPE)) {
                throw new org.osid.OperationFailedException("path provider does not support paths");
            }
            
            PathConditionRecord pcr = (PathConditionRecord) pc.getConditionRecord(PathConditionRecord.TYPE);
            
            pcr.setPath(path);
            
            /* get and sets the value condition record */
            if (!pc.hasRecordType(ValueConditionRecord.TYPE)) {
                throw new org.osid.OperationFailedException("path provider does not support values");
            }

            ValueConditionRecord cvcr = (ValueConditionRecord) pc.getConditionRecord(ValueConditionRecord.TYPE);
            cvcr.addValueList(values);
            
            /*
             * execute the path rule
             */
            
            values = new net.okapia.osid.jamocha.inline.processing.configuration.value.ProcessingValueList(values, this.session.path, pruleId, pc);
        }

        /*
         * stash the result
         */

        this.values = values;
        return;
    }
}
