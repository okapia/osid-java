//
// OrchestrationManager
//
//    An orchestration manager for the Proxy and Configuration services.
//
//
// Tom Coppeto
// OnTapSolutions
// 30 March 2010
//
//
// Copyright (c) 2010 Massachusetts Institute of Technology. All Rights 
// Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.providers.configuration.tomcat;

import org.osid.binding.java.annotation.OSID;

import net.okapia.osid.primordium.id.BasicId;
import net.okapia.osid.primordium.calendaring.GregorianUTCDateTime;
import net.okapia.osid.primordium.installation.SoftwareVersion;
import net.okapia.osid.primordium.locale.text.eng.us.Plain;


/**
 *  Orchestrates the Proxy and Configuration services.
 */

public final class OrchestrationManager
    extends net.okapia.osid.jamocha.orchestration.spi.AbstractOrchestrationManager
    implements org.osid.orchestration.OrchestrationManager {

    private static final org.osid.id.Id ID                          = BasicId.valueOf("OSID Runtime Providers:orchestration.configuration.tomcat@okapia.net");
    private static final org.osid.locale.DisplayText DISPLAY_NAME   = Plain.valueOf("Orchestration Provider");
    private static final org.osid.locale.DisplayText DESCRIPTION    = Plain.valueOf("An OSID Orchestration Provider for the Tomcat Configuration and Proxy Providers.");
    private static final org.osid.installation.Version VERSION      = SoftwareVersion.valueOf("0.1.0");
    private static final org.osid.calendaring.DateTime RELEASE_DATE = GregorianUTCDateTime.valueOf("2010-3-31T12:30");

    
    /**
     *  Constructs a new <code>OrchestrationManager</code>.
     */

    public OrchestrationManager() {
        super(ID);
	setMetadata(DISPLAY_NAME, DESCRIPTION, VERSION, RELEASE_DATE,
		    net.okapia.osid.provider.Providers.OKAPIA.getProvider());
	return;
    }


    /**
     *  Tests if a configuration provider is supported.
     *
     *  @return <code> true </code> if a configuration provider is available,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean supportsConfigurationProvider() {
        return (true);
    }


    /**
     * Tests if a proxy provider is supported.
     *
     *  @return <code> true </code> if a proxy provider is available, <code>
     *          false </code> otherwise
     */

    @OSID @Override
    public boolean supportsProxyProvider() {
        return (true);
    }


    /**
     *  Gets the manager associated with the Configuration service.
     *
     *  @return the manager
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.UnimplementedException <code>
     *          supportsConfigurationProvider() </code> is <code> false
     *         </code>
     *  @throws org.osid.IllegalStateException this manager has been shut down
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationProxyManager getConfigurationProxyManager()
	throws org.osid.OperationFailedException {

	try {
	    return ((org.osid.configuration.ConfigurationProxyManager) getRuntime().getProxyManager(org.osid.OSID.CONFIGURATION,
												    "net.okapia.osid.kilimanjaro.providers.configuration.nativeapp.ConfigurationProxyManager",
												    "3.0.0"));
	} catch (org.osid.NotFoundException nfe) {
	    throw new org.osid.OperationFailedException("cannot get configuration manager", nfe);
	}
    }	



    /**
     *  Gets the manager associated with the Proxy service.
     *
     *  @return the manager
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.UnimplementedException <code> supportsProxyProvider()
     *          </code> is <code> false </code>
     *  @throws org.osid.IllegalStateException this manager has been shut down
     */

    @OSID @Override
    public org.osid.proxy.ProxyManager getProxyManager()
	throws org.osid.OperationFailedException {

	try {
	    return ((org.osid.proxy.ProxyManager) getRuntime().getManager(org.osid.OSID.PROXY,
									  "net.okapia.osid.kilimanjaro.providers.configuration.rules.ProxyManager",
									  "3.0.0"));
	} catch (org.osid.NotFoundException nfe) {
	    throw new org.osid.OperationFailedException("cannot get proxy manager", nfe);
	}
    }
}
