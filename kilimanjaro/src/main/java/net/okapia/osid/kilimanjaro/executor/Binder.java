//
// Binder.java
//
//     Tools to bind OSIDs to a Java proxy.
//
//
// Tom Coppeto
// Okapia
// 4 December 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.executor;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


public class Binder {

    /* looks up the FP for a manager instance */
    public static final java.util.Map<org.osid.OsidProfile, FinePrint> map = java.util.Collections.synchronizedMap(new java.util.HashMap<org.osid.OsidProfile, FinePrint>());


    /**
     *  Binds a child runtime.
     *
     *  @param parent
     *  @param runtime an OSID Runtime
     *  @param context the runtime context
     *  @throws org.osid.NullArgumentException
     *  @throws org.osid.OsidRuntimeException cannot start proxy
     */

    public static org.osid.OsidRuntimeManager launchChildRuntime(org.osid.OsidProfile parent,
                                                                 org.osid.OsidRuntimeManager runtime,
                                                                 String context) {

        Class<?> runtimeInterface;
        try {
            runtimeInterface = Class.forName("org.osid.OsidRuntimeManager");
        } catch (Exception e) {
            throw new org.osid.OsidRuntimeException("cannot get class for org.osid.OsidRuntimeManager");
        }

        java.util.Collection<Class<?>> c = new java.util.ArrayList<>();
        c.add(Contract.class);
        c.add(runtimeInterface);

        /* not found because manager is not proxied yet */
        FinePrint parentFp = Binder.map.get(parent);
        if (parentFp == null) {
            throw new org.osid.OsidRuntimeException("not spawned from a managed manager");
        }

        FinePrint fp = createTerm(parentFp, runtimeInterface, runtime, context);
        Binder.map.put(runtime, fp);
        return (createProviderProxy(runtime, runtime, c, fp));
    }


    /**
     *  Binds a manager to a runtime.
     *
     *  @param runtime the runtime
     *  @param object an object
     *  @param osidInterface the name of the interface
     *  @return proxy for the manager
     *  @throws org.osid.NullArgumentException
     *  @throws org.osid.OsidRuntimeException cannot start proxy
     */

    public static <T extends org.osid.OsidProfile> T bind(org.osid.OsidRuntimeManager runtime, T object, String osidInterface) {
        nullarg(object, "object");
        nullarg(osidInterface, "OSID interface");

        java.util.Collection<Class<?>> c = new java.util.ArrayList<>();
        c.add(Contract.class);
        try {
            c.add(Class.forName(osidInterface));
        } catch (Exception e) {
            throw new org.osid.OsidRuntimeException("cannot get class for " + osidInterface);
        }

        FinePrint parentFp = Binder.map.get(runtime);
        if (parentFp == null) {
            throw new org.osid.OsidRuntimeException("not spawned from a managed manager");
        }

        FinePrint fp = createTerm(parentFp, osidInterface, object);
        Binder.map.put(object, fp);
        return (createProviderProxy(object, runtime, c, fp));
    }


    /**
     *  Starts the contract. This method is used by a bootstrapper to
     *  launch the OSID Runtime inside proxy. 
     *
     *  @param runtime an OSID Runtime
     *  @param context the context of the runtime
     *  @return proxy for the runtime
     *  @throws org.osid.NullArgumentException
     *  @throws org.osid.OsidRuntimeException cannot start proxy
     */

    public static org.osid.OsidRuntimeManager accept(org.osid.OsidRuntimeManager runtime, String context) {
        nullarg(runtime, "runtime");

        Class<?> runtimeInterface;
        try {
            runtimeInterface = Class.forName("org.osid.OsidRuntimeManager");
        } catch (Exception e) {
            throw new org.osid.OsidRuntimeException("cannot get class for org.osid.OsidRuntimeManager");
        }

        java.util.Collection<Class<?>> c = new java.util.ArrayList<>();
        c.add(Contract.class);
        c.add(runtimeInterface);

        FinePrint fp = createTerm(null, runtimeInterface, runtime, context);
        Binder.map.put(runtime, fp);
        return (createProviderProxy(runtime, runtime, c, fp));
    }


    /**
     *  Removes a runtime binding. Called when runtime manager is shutdown.
     */

    protected static void unbind(FinePrint fp) {
        Binder.map.values().remove(fp);
        return;
    }
        
        
    /**
     *  Creates a fine print term.
     *
     *  @param parent the parent's fine print
     *  @param interfaceName the name of the child specification interface
     *  @param object the delegate object
     *  @throws org.osid.OsidRuntimeException interfaceName not defined
     */

    protected static FinePrint createTerm(FinePrint parent, String interfaceName, Object object) {
        Class<?> c;

        try {
            c = Class.forName(interfaceName);
        } catch (Exception e) {
            throw new org.osid.OsidRuntimeException("cannot get class for " + interfaceName);
        }

        return (createTerm(parent, c, object));
    }


    /**
     *  Creates a fine print term.
     *
     *  @param parent the parent's fine print
     *  @param interfaceClass the class of the child specification interface
     *  @param object the delegate object
     *  @throws org.osid.OsidRuntimeException interfaceName not defined
     */

    protected static FinePrint createTerm(FinePrint parent, Class<?> interfaceClass, Object object) {
        return (createTerm(parent, interfaceClass, object, ""));
    }
        

    /**
     *  Creates a fine print term.
     *
     *  @param parent the parent's fine print
     *  @param interfaceClass the class of the child specification interface
     *  @param object the delegate object
     *  @param context for runtime
     *  @throws org.osid.OsidRuntimeException interfaceName not defined
     */

    protected static FinePrint createTerm(FinePrint parent, Class<?> interfaceClass, Object object, String context) {
        org.osid.meta.OsidInterface spec = getInterface(interfaceClass.getName());
        if (spec == null) {
            throw new org.osid.OsidRuntimeException("cannot get OSID interface for " + interfaceClass.getName());
        }

        if (org.osid.OsidRuntimeManager.class.isAssignableFrom(interfaceClass)) {
            if (parent == null) {
                return (RuntimeTerm.createRoot(spec, object, context));
            } else {
                return (RuntimeTerm.createTerm(parent, spec, object, context));
            }
        }

        if (org.osid.OsidManager.class.isAssignableFrom(interfaceClass) ||
            org.osid.OsidProxyManager.class.isAssignableFrom(interfaceClass)) {
            return (ManagerTerm.createTerm(parent, spec, object));
        }

        if (org.osid.OsidSession.class.isAssignableFrom(interfaceClass)) {
            return (SessionTerm.createTerm(parent, spec, object));
        }

        if (org.osid.OsidList.class.isAssignableFrom(interfaceClass)) {
            return (ListTerm.createTerm(parent, spec, object));
        }

        if (org.osid.OsidRecord.class.isAssignableFrom(interfaceClass)) {
            return (RecordTerm.createTerm(parent, spec, object));
        }

        if (org.osid.OsidObject.class.isAssignableFrom(interfaceClass)) {
            return (ObjectTerm.createTerm(parent, spec, object));
        }

        if (org.osid.Identifiable.class.isAssignableFrom(interfaceClass)) {
            return (ObjectTerm.createTerm(parent, spec, object));
        }

        if (org.osid.transport.DataInputStream.class.isAssignableFrom(interfaceClass) ||
            org.osid.transport.DataOutputStream.class.isAssignableFrom(interfaceClass)) {
            return (StreamTerm.createTerm(parent, spec, object));
        }

        return (Term.createTerm(parent, spec, object));
    }


    /**
     *  Creates a proxy for a consumer.
     *
     *  @param object the object to proxy
     *  @param interfaces a list of interfaces to include with proxy
     *  @param fp a fine print term for the object
     */

    @SuppressWarnings("unchecked")
    protected static <T extends Object> T createConsumerProxy(T object, java.util.Collection<Class<?>> interfaces, 
                                                              FinePrint fp) {

        return ((T) java.lang.reflect.Proxy.newProxyInstance(object.getClass().getClassLoader(), 
                                                             interfaces.toArray(new Class<?>[interfaces.size()]),
                                                             new BoilerPlate(object, null, fp, false)));
    } 


    /**
     *  Creates a proxy for a provider.
     *
     *  @param object the object to proxy
     *  @param interfaces a list of interfaces to include with proxy
     *  @param fp a fine print term for the object
     */

    @SuppressWarnings("unchecked")
    protected static <T extends Object> T createProviderProxy(T object, Object provider, 
                                                              java.util.Collection<Class<?>> interfaces, 
                                                              FinePrint fp) {

        return ((T) java.lang.reflect.Proxy.newProxyInstance(object.getClass().getClassLoader(), 
                                                             interfaces.toArray(new Class<?>[interfaces.size()]),
                                                             new BoilerPlate(object, provider, fp, true)));
    } 


    private static org.osid.meta.OsidInterface getInterface(String interfaceName) {
        org.osid.meta.OsidInterface spec = org.osid.meta.OsidInterfaces.getInterface(interfaceName);
        if (spec == null) {
            try {
                org.osid.meta.loader.OsidLoader.loadInterface(interfaceName);
            } catch (Exception e) {
                /* need to compensate for bug and tickle the load */
                
                throw new org.osid.OsidRuntimeException("error in loading meta data", e);
            }
        }

        return (org.osid.meta.OsidInterfaces.getInterface(interfaceName));
    }


    private static void getLoaderMethod(String name) {
        String className = "org.osid.meta.loader.";
        String methodName = "load";
        String[] s = name.split("\\.");
        int pos = 2;

        if (s.length > 3) {
            className += s[2] + ".";
            ++pos;
        }

        while(pos < s.length - 1) {
            className += s[pos] + ".";
            ++pos;
        }

        className += "Loader";
        methodName += s[pos];
        
        
    }
}