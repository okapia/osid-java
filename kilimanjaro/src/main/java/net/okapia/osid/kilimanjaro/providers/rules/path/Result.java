//
// Result.java
//
//     Defines a Result.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.providers.rules.path;

import net.okapia.osid.kilimanjaro.types.records.rules.result.ValueResultRecord;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a Result.
 */

public final class Result
    extends net.okapia.osid.jamocha.rules.result.spi.AbstractResult
    implements org.osid.rules.Result,
               org.osid.rules.records.ResultRecord,
               ValueResultRecord {

    private java.util.Collection<org.osid.configuration.Value> values;


    /**
     *  Creates a new <code>Result</code>. The values are buffered
     *  inside this object.
     *
     *  @param values
     *  @throws org.osid.NullArgumentException <code>values</code> is
     *          <code>nul</code>
     */

    public Result(java.util.Collection<org.osid.configuration.Value> values) {
        nullarg(values, "values");
        this.values = values;

        addResultRecord(this, ValueResultRecord.TYPE);
        return;
    }
        

    /**
     *  Gets a boolean result from the rule evaluation. Based on the
     *  definition of the Rule, the result may be a simple boolean value or
     *  something more complex defined in the record type.
     *
     *  @return <code> true </code> or <code> false </code>
     */

    public boolean getBooleanValue() {
        if (this.values.size() > 0) {
            return (true);
        } else {
            return (false);
        }
    }

    
    /**
     *  Gets the <code> Result </code> from which this record originated. 
     *
     *  @return the rule
     */

    public org.osid.rules.Result getResult() {
        return (this);
    }


    /**
     *  Tests if the given type is implemented by this record. Other types 
     *  than that directly indicated by <code> getType() </code> may be 
     *  supported through an inheritance scheme where the given type specifies 
     *  a record that is a parent interface of the interface specified by 
     *  <code> getType(). </code> 
     *
     *  @param  recordType a type 
     *  @return <code> true </code> if the given record <code> Type </code> is 
     *          implemented by this record, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> recordType </code> is 
     *          <code> null </code> 
     */

    public boolean implementsRecordType(org.osid.type.Type recordType) {
        return (hasRecordType(recordType));
    }


    /**
     *  Gets all the valid configuration values in order of preference. 
     *
     *  @return valid configuration values
     */

    public org.osid.configuration.ValueList getValues() {
        return (new net.okapia.osid.jamocha.configuration.value.ArrayValueList(this.values));
    }
}
