//
// OsidSessionMXBean.java
//
//     A JMX Bean for an OSID Session.
//
//
// Tom Coppeto
// Okapia
// 27 June 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.jmx;


public interface OsidSessionMXBean {


    /**
     *  Gets the OSID package of the session.
     * 
     *  @return the session package
     */

    public String getOsidPackage();


    /**
     *  Gets the name of the session.
     * 
     *  @return the session name
     */

    public String getName();


    /**
     *  Gets the implementation class name of the session.
     * 
     *  @return the class name
     */

    public String getImplementationName();


    /**
     *  Gets the language type.
     *
     *  @return the language type
     */

    public String getLanguageType();


    /**
     *  Gets the script type.
     *
     *  @return the script type
     */

    public String getScriptType();

    
    /**
     *  Gets the calendar type.
     *
     *  @return the calendar type
     */

    public String getCalendarType();


    /**
     *  Gets the time type.
     *
     *  @return the time type
     */

    public String getTimeType();


    /**
     *  Gets the currency type.
     *
     *  @return the currency type
     */

    public String getCurrencyType();


    /**
     *  Gets the unit system type.
     *
     *  @return the unit system type
     */

    public String getUnitSystemType();


    /**
     *  Gets the numeric format type.
     *
     *  @return the numeric format type
     */

    public String getNumericFormatType();


    /**
     *  Gets the calendar format type.
     *
     *  @return the calendar format type
     */

    public String getCalendarFormatType();


    /**
     *  Gets the time format type.
     *
     *  @return the time format type
     */

    public String getTimeFormatType();


    /**
     *  Gets the currency format type.
     *
     *  @return the currency format type
     */

    public String getCurrencyFormatType();


    /**
     *  Gets the coordinate format type.
     *
     *  @return the coordinate format type
     */

    public String getCoordinateFormatType();


    /**
     *  Gets the authenticated agent.
     *
     *  @return the authenticated agent
     */

    public AgentMXBean getAuthenticatedAgent();


    /**
     *  Gets the effective agent.
     *
     *  @return the effective agent
     */

    public AgentMXBean getEffectiveAgent();

    
    /**
     *  Gets the effective date for this session.
     *
     *  @return the effective date
     */

    public java.util.Date getEffectiveDate();


    /**
     *  Gets the stats for this OsidSession.
     *
     *  @return the stats
     */

    public OsidStatsMXBean getStats();
}
