//
// Meta.java
//
//     Utility for OSID Meta Data
//
//
// Tom Coppeto
// Okapia
// 21 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.tools;

import org.osid.meta.*;
import org.osid.meta.loader.OsidLoader;


public class Meta {
    

    public void primeAllOsids() {
        OsidLoader.preloadInterfaces();
        return;
    }


    public void loadOsid(String osid)
        throws Exception {
        //        org.osid.meta.loader.OsidLoader.loadOsid(osid);
        return;        
    }


    public void loadInterface(String iface)
        throws Exception {

        org.osid.meta.loader.OsidLoader.loadInterface(iface);
        return;        
    }


    public void outputOsids()
        throws Exception {

        Osids osids = Osids.getInstance();        
        for (Osid osid : osids.getOsids()) {
            System.out.println(osid.getName());
        }

        return; 
    }


    public void outputOsid(String osidName)
        throws Exception {
        
        Osids osids = Osids.getInstance(); 
        Osid osid = osids.getOsid(osidName);

        for (OsidInterface oi : osid.getInterfaces()) {
            System.out.println(oi.getName());
        }
    }


    public void outputInterface(String name)
        throws Exception {
        
        OsidInterface oi = OsidInterfaces.getInterface(name);
        System.out.println("Interface: " + oi.getName());
        System.out.println();
        
        for (OsidMethod method : oi.getMethods()) {
            System.out.print("\t");
            if (method.getReturn() != null) {
                System.out.print(method.getReturn().getType());
                if (method.getReturn().isArray()) {
                    System.out.print("[]");
                }
            } else {
                System.out.print("void");
            }

            System.out.print(" ");
            System.out.print(method.getName());
            System.out.print("(");

            boolean first = true;
            for (OsidParameter parameter : method.getParameters()) {
                if (!first) {
                    System.out.println(", ");
                } else {
                    first = false;
                }

                System.out.print(parameter.getType());
                System.out.print(" ");
                System.out.print(parameter.getName());
                if (parameter.isArray()) {
                    System.out.print("[]");
                }
            }

            System.out.print(")");

            if (method.getErrors().size() > 0) {
                System.out.print("\n\t    throws ");

                first = true;
                for (String error : method.getErrors()) {
                    
                    if (!first) {
                        System.out.print(",\n\t           ");
                    } else {
                        first = false;
                    }
                    
                    System.out.print(error);
                }
            }

            System.out.println(";\n");
        }
    }

        
    public static void main(String[] args)
        throws Exception {

        boolean prime = false;
        boolean list = false;
        boolean stats = false;
        boolean osid  = false;
        boolean iface = false;
        String pkg    = null;
        String iname  = null;

        for (String arg : args) {
            if (arg.equals("-p")) {
                prime = true;
                continue;
            }

            if (arg.equals("-s")) {
                stats = true;
                continue;
            }

            if (arg.equals("-l")) {
                list = true;
                continue;
            }

            if (arg.equals("-osid")) {
                osid = true;
                continue;
            }

            if (arg.equals("-int")) {
                iface = true;
                continue;
            }

            if (osid && (pkg == null)) {
                pkg = arg;
                continue;
            }

            if (iface && (iname == null)) {
                iname = arg;
                continue;
            }

            System.err.println("usage: Meta [-h] [-l] [-p] [-s] [-osid <pkg>] [-int <interface>]");
            if (arg.equals("-h")) {
                System.exit(0);
            }

            System.exit(1);
        }            
        
        Meta meta = new Meta();
        Runtime runtime = Runtime.getRuntime();
        long mem  = runtime.totalMemory() - runtime.freeMemory();
        long nano = System.nanoTime();

        if (prime) {
            meta.primeAllOsids();
        } else if (osid) {
            meta.loadOsid(pkg);
        } else if (iface) {
            meta.loadInterface(iname);
        }

        if (stats) {
            long duration = (System.nanoTime() - nano)/1000;
            long usage = (runtime.totalMemory() - runtime.freeMemory() - mem) / 1024;
            System.out.println("meta loading time: " + duration + "\u00B5s   memory: " + usage + "KB");
            mem  = runtime.totalMemory() - runtime.freeMemory();
            nano = System.nanoTime();
        }

        if (list && osid) {
            meta.outputOsid(pkg);
        } else if (list && iface) {
            meta.outputInterface(iname);
        } else if (list) {
            meta.outputOsids();
        }

        if (stats) {
            long duration = (System.nanoTime() - nano)/1000;
            long usage = (runtime.totalMemory() - runtime.freeMemory() - mem) / 1024;
            System.out.println("meta access time: " + duration + "\u00B5s   memory: " + usage + "KB");
        }

        return;
    }
}

