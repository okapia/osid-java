//
// RulesManager.java
//
//     Supplies basic information in common throughout the managers and
//     profiles.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.providers.rules.time;

import org.osid.binding.java.annotation.OSID;


/**
 */

public final class RulesManager
    extends net.okapia.osid.jamocha.rules.spi.AbstractRulesManager
    implements org.osid.rules.RulesManager,
               org.osid.rules.RulesProxyManager {
   

    /**
     *  Constructs a new <code>RulesManager</code>.
     */

    public RulesManager() {
        super(new ServiceProvider());
        return;
    }


    /**
     *  Tests if any engine federation is exposed. Federation is exposed when
     *  a specific engine may be identified, selected and used to create a
     *  lookup or admin session. Federation is not exposed when a set of
     *  engines appears as a single engine.
     *
     *  @return <code> true </code> if visible federation is supproted, <code>
     *          false </code> otherwise
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (true);
    }


    /**
     *  Tests if rule evaluation is supported.
     *
     *  @return <code> true </code> if rule evaluation is supported, <code>
     *          false </code> otherwise
     */

    @OSID @Override
    public boolean supportsRules() {
        return (true);
    }


    /**
     *  Tests for the availability of a rule lookup service.
     *
     *  @return <code> true </code> if rule lookup is available, <code> false
     *          </code> otherwise
     */

    @OSID @Override
    public boolean supportsRuleLookup() {
        return (true);
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the rule 
     *  evaluation service. 
     *
     *  @return a <code> RulesSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRules() </code> 
     *          is <code> false </code> 
     *  @throws org.osid.IllegalStateException this manager has been shut down 
     */

    @OSID @Override
    public org.osid.rules.RulesSession getRulesSession()
        throws org.osid.OperationFailedException {
        
        throw new org.osid.OperationFailedException("no default engine");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the rule 
     *  evaluation service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of the <code> Rule </code> 
     *  @return a <code> RulesSession </code> 
     *  @throws org.osid.NotFoundException no <code> Engine </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRules() </code> 
     *          or <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     *  @throws org.osid.IllegalStateException this manager has been shut down 
     */

    @OSID @Override
    public org.osid.rules.RulesSession getRulesSessionForEngine(org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (new RulesSession(engineId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the rule lookup 
     *  service. 
     *
     *  @return a <code> RuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRuleLookup() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.IllegalStateException this manager has been shut down 
     */

    @OSID @Override
    public org.osid.rules.RuleLookupSession getRuleLookupSession()
        throws org.osid.OperationFailedException {
        
        throw new org.osid.OperationFailedException("no default engine");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the rule lookup 
     *  service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of the <code> Rule </code> 
     *  @return a <code> RuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Engine </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRuleLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.IllegalStateException this manager has been shut down 
     */

    @OSID @Override
    public org.osid.rules.RuleLookupSession getRuleLookupSessionForEngine(org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (new RuleLookupSession(engineId));
    }
}
