//
// Package.java
//
//     Lists OSID pckages.
//
//
// Tom Coppeto
// Okapia
// 2 May 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.tools;

import org.osid.*;
import org.osid.type.Type;
import org.osid.installation.*;

import net.okapia.osid.kilimanjaro.BootLoader;
import net.okapia.osid.kilimanjaro.OsidVersions;


public class Package {
    private org.osid.installation.InstallationManager manager;
    private org.osid.OsidRuntimeManager runtime;


    public Package()
        throws Exception {
        
        this("org.osid.runtime.providers.installation.runtime.InstallationManager");
        return;
    }   

    public Package(String impl)
        throws Exception {

        BootLoader loader = new BootLoader();
        this.runtime = loader.getRuntimeManager("Config");
        this.manager = (InstallationManager) runtime.getManager(OSID.INSTALLATION, impl, 
                                                                 OsidVersions.V3_0_0.getVersion());
        return;
    }   


    public void listSites()
        throws Exception {

        SiteLookupSession session = this.manager.getSiteLookupSession();
        session.useComparativeSiteView();
        try (SiteList sites = session.getSites()) {
            while (sites.hasNext()) {
                Site site = sites.getNextSite();
                System.out.println("name:        " + site.getDisplayName());
                System.out.println("id:          " + site.getId());
                System.out.println("path:        " + site.getPath());
                System.out.println("description: " + site.getDescription());
                if (sites.hasNext()) {
                    System.out.println();
                }
            }
        }

        session.close();
        return; 
    }


    public void listInstalls(String siteId) 
        throws Exception {

        InstallationLookupSession session = this.manager.getInstallationLookupSessionForSite(net.okapia.osid.primordium.id.BasicId.valueOf(siteId));
        session.useComparativeInstallationView();
        session.useDenormalizedVersionView();

        try (InstallationList installs = session.getInstallations()) {        
            while (installs.hasNext()) {
                Installation install = installs.getNextInstallation();
                System.out.println("name:         " + install.getDisplayName());
                System.out.println("id:           " + install.getId());
                System.out.println("description:  " + install.getDescription());
                System.out.println("version:      " + install.getPackage().getVersion());
                System.out.println("installed:    " + install.getInstallDate());
                System.out.println("installed by: " + install.getAgent().getDisplayName());
                if (installs.hasNext()) {
                    System.out.println();
                }
            }
        }

        session.close();
        return; 
    }


    public static void main(String[] args)
        throws Exception {

        String implName    = null;
        boolean osid       = false;
        String siteName    = null;
        boolean site       = false;

        for (String arg : args) {
            if (arg.equals("-osid")) {
                osid = true;
                continue;
            }

            if (arg.equals("-site")) {
                site = true;
                continue;
            }

            if (osid && (implName == null)) {
                implName = arg;
                continue;
            }

            if (site && (siteName == null)) {
                siteName = arg;
                continue;
            }

            System.err.println("usage: Package [-osid <impl>] [-site <site>]");
            System.exit(1);
        }

        Package pkg;

        if (osid) {
            pkg = new Package(implName);
        } else {
            pkg = new Package();
        }

        if (site) {
            pkg.listInstalls(siteName);
        } else {
            pkg.listSites();
        }

        return;
    }
}

