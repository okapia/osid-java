//
// AbstractTerm.java
//
//     Validates the state of various objects based on the OSID
//     specifications. 
//
//
// Tom Coppeto
// Okapia
// 4 January 2009
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.executor;


/**
 *  Validate sthe state of various objects based on the OSID
 *  specifications. This class is only used by the BoilerPlate
 *  contract.
 */

public abstract class AbstractTerm
    implements FinePrint {

    private final org.osid.meta.OsidInterface spec;
    private final net.okapia.osid.kilimanjaro.jmx.impl.OsidStats stats;
    private FinePrint parent;

    private final java.util.Map<FinePrint, Boolean> contracts = java.util.Collections.synchronizedMap(new java.util.WeakHashMap<FinePrint, Boolean>());
    

    /**
     *  Constructs a new <code>AbstractTerm</code>.
     *  
     *  @param parent parent's fine print
     *  @param spec the OSID interface
     */

    protected AbstractTerm(FinePrint parent, org.osid.meta.OsidInterface spec,
                           net.okapia.osid.kilimanjaro.jmx.impl.OsidStats stats) {

        this.parent = parent;
        this.spec   = spec;
        this.stats  = stats;

        if (parent != null) {
            parent.addAddendum(this);
        }

        return;
    }


    /**
     *  Constructs a new <code>AbstractTerm</code>.
     *  
     *  @param spec the OSID interface
     */

    protected AbstractTerm(org.osid.meta.OsidInterface spec,
                           net.okapia.osid.kilimanjaro.jmx.impl.OsidStats stats) {

        this.spec  = spec;
        this.stats = stats;

        return;
    }


    /**
     *  Gets the spec for this term.
     *
     *  @return the spec
     */

    @Override
    public org.osid.meta.OsidInterface getInterfaceSpec() {
        return (this.spec);
    }


    /**
     *  Shuts down this contract.
     */

    @Override
    public synchronized void close() {

        /* 
         * iterate over a fixed array, else we'll get a concurrent
         * modification exception when the child checks out 
         */

        for (FinePrint ref : this.contracts.keySet().toArray(new FinePrint[this.contracts.size()])) {
            /* getting a null in the array because the list closed in the interim */
            if (ref != null) {
                ref.close();
            }
        }
        
        if (getParent() != null) {
            getParent().cancel(this);
        }

        this.parent = null;
        return;
    }


    /**
     *  Registers a child from this interface.
     *
     *  @param fp FinePrint
     */
    
    @Override
    public void addAddendum(FinePrint fp) {
        this.contracts.put(fp, true);
        return;
    }


    /**
     *  Unregisters a managed contract.
     *
     *  @param fp FinePrint
     */

    @Override
    public void cancel(FinePrint fp) {
        this.contracts.remove(fp);
        return;
    }


    /**
     *  Gets the OSID interface.
     *
     *  @return the OSID interface
     */

    protected org.osid.meta.OsidInterface getInterface() {
        return (this.spec);
    }

    
    /**
     *  Gets the class name of the OSID interface.
     *
     *  @return the class name
     */

    protected String getClassName() {
        String[] tok = getInterface().getName().split("\\.");

        if (tok.length > 0) {
            return (tok[tok.length - 1]);
        } else {
            return (getInterface().getName());
        }           
    }


    /**
     *  Gets the parent.
     *
     *  @return the parent
     */

    protected FinePrint getParent() {
        return (this.parent);
    }


    /**
     *  Gets an OsidMethod based on the method name. The state of
     *  the interface is validated.
     *
     *  @param method
     *  @param args
     *  @return the OSID method
     *  @throws org.osid.InvalidMethodException method not found
     */

    @Override
    public Object perform(java.lang.reflect.Method method, Object delegate, Object[] args)
        throws Throwable {

        org.osid.meta.OsidMethod osidMethod = getInterface().getMethod(method.getName());
        if (osidMethod == null) {
            throw new org.osid.InvalidMethodException(method.getName() + " not an OSID method");
        }

        approve(osidMethod, args);
        return (consideration(osidMethod, delegate, execute(osidMethod, method, delegate, args)));
    }


    /**
     *  Approve the invocation of a method in this interface. This
     *  method throws a runtime exception to abort the transaction.
     *
     *  @param method the OSID spec
     *  @param args method arguments
     */

    protected void approve(org.osid.meta.OsidMethod method, Object[] args) {
        return;
    }


    /**
     *  Applies a little consideration to the return values. 
     *
     *  @param method the OSID spec
     *  @param delegate the delegate object
     *  @param ret the return object
     */

    protected Object consideration(org.osid.meta.OsidMethod method, Object delegate, Object ret) {
        return (ret);
    }


    /**
     *  Handle the toString() method.
     *  
     *  @param delegate
     *  @return string
     */

    @Override
    public String processToString(Object delegate) {
        String s = delegate.toString();

        if (s.startsWith(delegate.getClass().getName())) {
            return ("[" + getClassName() + "]");
        } else {
            return (s);
        }
    }


    /**
     *  gets the jmx stats collector
     */

    @Override
    public net.okapia.osid.kilimanjaro.jmx.impl.OsidStats getStatsCollector() {
        return (this.stats);
    }


    /*
     *  Validates and prepares arguments.
     *
     *  @param method
     *  @param args
     *  @return new arguments
     *  @throws org.osid.InvalidArgumentException
     *  @throws org.osid.NullArgumentException
     */

    protected Object[] processArguments(org.osid.meta.OsidMethod osidMethod, java.lang.reflect.Method method, 
                                        Object delegate, Object[] args) {

        java.util.Collection<org.osid.meta.OsidParameter> parameters = osidMethod.getParameters();

        /*
         *  Check for the same number of arguments.
         */

        if (args == null) {
            if (parameters.size() > 0) {
                throw new org.osid.NullArgumentException("missing arguments for " + osidMethod.getName() + "()");
            }

            return (null);
        }

        if (parameters.size() == 0) {
            throw new org.osid.InvalidArgumentException("undefined arguments given to " + osidMethod.getName());
        }

        if (args.length < parameters.size()) {
            throw new org.osid.InvalidArgumentException("too few arguments given to " + osidMethod.getName());
        }

        if (args.length > parameters.size()) {
            throw new org.osid.InvalidArgumentException("too many arguments given to " + osidMethod.getName());
        }

        Object[] proxies = new Object[args.length];
        
        int i = 0;
        for (org.osid.meta.OsidParameter parameter : parameters) {
            
            /*
             * OSIDs do not permit NULL arguments
             */

            if (args[i] == null) {
                throw new org.osid.NullArgumentException(parameter.getName() + " is null");
            }
            
            if (parameter.isPrimitive()) {

                /*
                 * An OSID Primitive is checked for validity, and
                 * passed unwrapped.
                 */

                checkPrimitiveParameter(osidMethod, parameter, args[i]);
                proxies[i] = args[i];

            } else if (parameter.isArray()) {
                proxies[i] = processInterfaceParameterArray(osidMethod, method, parameter, 
                                                            delegate, args[i]);
            } else {
                proxies[i] = processInterfaceParameter(osidMethod, method, parameter, 
                                                       delegate, args[i]);
            }

            ++i;
        }

        return (proxies);
    }


    /*
     * Checks for a valid OSID primitive argument. 
     *
     * @throws org.osid.InvalidArgumentException
     */

    private void checkPrimitiveParameter(org.osid.meta.OsidMethod method, 
                                         org.osid.meta.OsidParameter parameter, 
                                         Object primitive) {

        if (isDefinedPrimitive(parameter.getSpecType(), parameter.isArray(), primitive)) {
            return;
        }

        throw new org.osid.InvalidArgumentException("invalid argument: expected " 
                                                    + parameter.getType()
                                                    + " for " + parameter.getName()
                                                    + " in " + method.getName() + "()");
    }


    /*
     *  Validates OSID interface arguments and proxies as
     *  necessary. In the core OSID spec, arguments can only be an
     *  OSID primitive or defined OSID interface.
     *
     *  @throws org.osid.InvalidArgumentException
     */

    protected Object processInterfaceParameter(org.osid.meta.OsidMethod osidMethod, 
                                               java.lang.reflect.Method method, 
                                               org.osid.meta.OsidParameter parameter, 
                                               Object delegate, Object argument) {

        String parameterType = parameter.getType().replace("[]", "");
        Class<? extends Object> iclass;

        try {
            iclass = Class.forName(parameterType);
        } catch (Exception e) {
            throw new org.osid.InvalidArgumentException(e);
        }

        Class<? extends Object> argClass = argument.getClass();
        if (iclass.isAssignableFrom(argClass.asSubclass(argClass))) {
            if (argument instanceof Contract) {

                /*
                 * The argument has already been proxied.
                 */

                /*
                 * If a Suppliable interface, unwrap it when it gets
                 * to the origin provider.
                 */
                
                if (argument instanceof org.osid.Suppliable) {
                    if (delegate.getClass().getName().equals(((Contract) argument).getSupplierOfContract())) {
                        return (((Contract) argument).violateContract()); 
                    }
                }
            
                return (argument);

            } else if (parameter.isEnumeration()) {

                /*
                 * Pass the enumerations through (for now)
                 */

                return (argument);

            } else {
                
                /*
                 * Wrap the argument inside a proxy. This should
                 * only be performed once for OSID defined
                 * interfaces.
                 */

                return (Binder.createConsumerProxy(argument, getArgumentInterfaces(parameter, argument), 
                                                   Binder.createTerm(this, parameterType, argument)));
            }
        }

        throw new org.osid.InvalidArgumentException("invalid argument expected " 
                                                    + parameterType + (parameter.isArray() ? "[]" : "") 
                                                    + " for " + parameter.getName()
                                                    + " in " + osidMethod.getName() + "()");
    }


    /*
     *  Handles an argument array. Not widely used in
     *  the OSIDs. Each element in the array may need
     *  to be proxied.
     *
     *  @throws org.osid.InvalidArgumentException
     */

    private Object processInterfaceParameterArray(org.osid.meta.OsidMethod osidMethod, 
                                                  java.lang.reflect.Method method,
                                                  org.osid.meta.OsidParameter parameter, 
                                                  Object delegate,
                                                  Object argument) {
        
        Object[] array;

        try {
            array = (Object[]) argument;
        } catch (ClassCastException cce) {
            throw new org.osid.InvalidArgumentException(parameter.getName() + " is not an array");
        }

        for (int i = 0; i < array.length; i++) {
            array[i] = processInterfaceParameter(osidMethod, method, parameter, delegate, array[i]);
        }

        return (array);
    }


    /*
     * Gets the list of interfaces the OSID argument should
     * implement.
     */

    protected java.util.Collection<Class<?>> getArgumentInterfaces(org.osid.meta.OsidParameter parameter, Object object) {
        java.util.Collection<Class<?>> imps = new java.util.ArrayList<>();

        imps.add(Contract.class);

        try {
            imps.add(Class.forName(parameter.getType().replace("[]", "")));
        } catch (Exception e) {
            throw new org.osid.InvalidArgumentException(e);
        }

        return (imps);
    }


    /*
     * Validates and prepares the return object.
     */

    protected Object processReturn(org.osid.meta.OsidMethod method, Object delegate, Object ret) {
        org.osid.meta.OsidReturn r = method.getReturn();

        /*
         * Null returns never permitted in the OSIDs.
         */

        if ((ret == null) && (r != null)) {
            throw new org.osid.NullReturnException("null return from " + method.getName());
        }

        if ((ret != null) && (r == null)) {
            throw new org.osid.ProviderContractException("no return defined in " + method.getName() + "()");
        }

        if (r == null) {
            return (null);
        }

        
        if (isPrimitiveReturn(r)) {

            /*
             * return OSID primitives untouched
             */

            checkPrimitiveReturn(method, r, ret);
            return (ret);
        } else if (r.isArray()) {
            ret = processInterfaceReturnArray(method, delegate, ret);
        } else {
            ret = processInterfaceReturn(method, delegate, ret);
        }

        return (ret);
    }


    /* workaround for meta db, please fix me there */
    private boolean isPrimitiveReturn(org.osid.meta.OsidReturn r) {
        if (r.getType().equals("boolean")) {
            return (true);
        }

        return (r.isPrimitive());
    }


    /*
     *  Validates OSID primitive return value.
     */

    private void checkPrimitiveReturn(org.osid.meta.OsidMethod method, 
                                      org.osid.meta.OsidReturn ret, 
                                      Object primitive) {

        if (isDefinedPrimitive(ret.getSpecType(), ret.isArray(), primitive)) {
            return;
        }

        throw new org.osid.InvalidReturnException("invalid return: expected " 
                                                  + ret.getType()
                                                  + " in " + method.getName() + "() but got " +
                                                  primitive.getClass().getName());
    }


    /*
     * Validates OSID interface return value and proxies if necessary.
     */

    private Object processInterfaceReturn(org.osid.meta.OsidMethod method, Object delegate, Object object) {
        
        String returnType = method.getReturn().getType().replace("[]", "");       
        Class<? extends Object> iclass;
        
        try {
            iclass = Class.forName(returnType);
        } catch (Exception e) {
            throw new org.osid.InvalidReturnException(e);
        }

        if (iclass.isAssignableFrom(object.getClass().asSubclass(object.getClass()))) {
            if (object instanceof Contract) {
                
                /*
                 * The return has already been proxied.
                 */
                
                return (object);

            } else if (method.getReturn().isEnumeration()) {

                /*
                 * Pass the enumerations through (for now)
                 */

                return (object);

            } else {

                /*
                 * Check to see an Id was set (the toolkit defaults to
                 * an unknown Id).
                 */


                Class<? extends Object> spec;
                try {
                    spec = Class.forName(returnType);
                } catch (Exception e) {
                    throw new org.osid.InvalidReturnException(e);
                }
            

                /*
                 * Wrap the returned object inside a proxy. This
                 * should only be performed once for OSID defined
                 * interfaces.
                 */
                
                FinePrint rfp = Binder.createTerm(this, returnType, object);
                return (Binder.createProviderProxy(object, delegate, getReturnInterfaces(method.getReturn(), object, rfp), rfp));
            }
        }

        throw new org.osid.InvalidReturnException("invalid return: expected " 
                                                  + returnType + (method.getReturn().isArray() ? "[]" : "")
                                                  + " in " + method.getName() + "()");
    }


    /*
     *  Handles a return array. Not widely used in
     *  the OSIDs. Each element in the array may need
     *  to be proxied.
     *
     *  @throws org.osid.InvalidArgumentException
     */

    private Object processInterfaceReturnArray(org.osid.meta.OsidMethod method, Object delegate, Object obj) {        
        Object[] array;

        try {
            array = (Object[]) obj;
        } catch (ClassCastException cce) {
            throw new org.osid.InvalidArgumentException(method.getName() + " return value is not an array");
        }

        for (int i = 0; i < array.length; i++) {
            array[i] = processInterfaceReturn(method, delegate, array[i]);
        }

        return (array);
    }


    /*
     * Gets the interfaces to be implemented by the proxy for the return.
     */

    protected java.util.Collection<Class<?>> getReturnInterfaces(org.osid.meta.OsidReturn ret, Object object, FinePrint rfp) {
        
        java.util.Collection<Class<?>> imps = new java.util.ArrayList<>();
        imps.add(Contract.class);

        /*
         * OsidRecord returns can be cast into any other
         * interface that extends OsidRecord.
         */

        if (rfp instanceof RecordTerm) {
            for (Class<?> c : object.getClass().getInterfaces()) {
                if (org.osid.OsidRecord.class.isAssignableFrom(c)) {
                    imps.add(c);
                }
            }

            return (imps);
        }

        String returnType = ret.getType().replace("[]", "");
        Class<? extends Object> spec;

        try {
            spec = Class.forName(returnType);
        } catch (Exception e) {
            throw new org.osid.InvalidReturnException(e);
        }
            
        Class<? extends Object> objClass = object.getClass();

        if (spec.isAssignableFrom(objClass.asSubclass(objClass))) {
            imps.add(spec);
            return (imps);
        }

        throw new org.osid.InvalidReturnException("unknown return type " + returnType + 
                                                  ", why didn't we see that before?");
    }


    /* 
     * Checks if primitive is a defined OSID Primitive.
     */

    protected boolean isDefinedPrimitive(String type, boolean isArray, Object primitive) {

        String primitiveType = primitive.getClass().getName();

        if (type.equals("boolean")) {
            if (isArray) {
                if (primitiveType.equals("[Z") || primitiveType.equals("[Ljava.lang.Boolean;")) {
                    return (true);
                }
            } else if (primitiveType.equals("boolean") || (primitiveType.equals("java.lang.Boolean"))) {
                return (true);
            }
        } else if (type.equals("byte")) {
            if (isArray) {
                if (primitiveType.equals("[B") || primitiveType.equals("[Ljava.lang.Byte;")) {
                    return (true);
                }
            } else if (primitiveType.equals("byte") || primitiveType.equals("[Ljava.lang.Byte;")) {
                return (true);
            }
        } else if (type.equals("cardinal")) {
            if (isArray) {
                if (primitiveType.equals("[J") || primitiveType.equals("[Ljava.lang.Long;")) {
                    return (true);
                }
                for (long l : (Long[]) primitive) {
                    if (l < 0) {
                        throw new org.osid.InvalidReturnException("invalid cardinal: less than zero");
                    }
                }                   
            } else if (primitiveType.equals("long") || (primitiveType.equals("java.lang.Long"))) {
                if ((Long) primitive < 0) {
                    throw new org.osid.InvalidReturnException("invalid cardinal: less than zero");
                }

                return (true);
            }
        } else if (type.equals("decimal")) {
            if (isArray) {
                if (primitiveType.equals("[Ljava.math.BigDecimal;")) {
                    return (true);
                }
            } else if (primitiveType.equals("java.math.BigDecimal")) {
                return (true);
            }
        } else if (type.equals("integer")) {
            if (isArray) {
                if (primitiveType.equals("[J") || primitiveType.equals("[Ljava.lang.Long;")) {
                    return (true);
                }
            } else if (primitiveType.equals("long") || (primitiveType.equals("java.lang.Long"))) {
                return (true);
            }
        } else if (type.equals("string")) {
            if (isArray) {
                if (primitiveType.equals("[Ljava.lang.String;")) {
                    return (true);
                }
            } else if (primitiveType.equals("java.lang.String")) {
                return (true);
            }
        } else if (type.equals("timestamp")) {
            if (isArray) {
                if (primitiveType.equals("[Ljava.util.Date;")) {
                    return (true);
                }
            } else if (primitiveType.equals("java.util.Date")) {
                return (true);
            }
        } else if (type.equals("object")) {
            if (isArray) {
                if (primitiveType.startsWith("[L")) {
                    return (true);
                }
            } else {
                return (true);
            }
        } 

        return (false);
    }


    /*
     * Invokes the method and checks for valid exceptions. 
     * JVM and Thread errors are allowed to pass. The rest
     * must be caught and translated by the OSID provider.
     */

    protected Object execute(org.osid.meta.OsidMethod osidMethod, 
                             java.lang.reflect.Method method, 
                             Object delegate, Object[] args)
        throws Throwable {
        
        try {
            return (processReturn(osidMethod, delegate, method.invoke(delegate, 
                                                                      processArguments(osidMethod, method, 
                                                                                       delegate, args))));
        } catch (Throwable t) {
            Throwable cause = t.getCause();

            if (cause == null) {
                throw t;
            }

            /* 
             * always pass these through 
             */

            if (cause instanceof ThreadDeath) {
                throw cause;
            }

            if (cause instanceof VirtualMachineError) {
                throw cause;
            }

            if (cause instanceof LinkageError) {
                throw new org.osid.MissingMethodException(osidMethod.getName() + " is not implemented", cause);
            }

            /*
             * these exceptions can originate from this proxy
             */

            if (cause instanceof org.osid.ProviderContractException) {
                throw cause;
            }

            if (cause instanceof org.osid.InvalidArgumentException) {
                throw cause;
            }

            if (cause instanceof org.osid.InvalidMethodException) {
                throw cause;
            }

            if (cause instanceof org.osid.NullArgumentException) {
                throw cause;
            }

            /*
             * check if the error is defined in the OSID
             */

            if (osidMethod.getErrors() != null) {
                for (String error : osidMethod.getErrors()) {
                    if (error.equals(cause.getClass().getName())) {
                        throw cause;
                    }
                }
            }

            throw new org.osid.InvalidErrorException(cause.getClass().getName()
                                                     + " not permitted here, but got one anyway", cause);
        }
    }
}

