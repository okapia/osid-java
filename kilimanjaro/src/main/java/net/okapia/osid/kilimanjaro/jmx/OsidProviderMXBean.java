//
// OsidProviderMXBean.java
//
//     A JMX Bean for an OSID Provider.
//
//
// Tom Coppeto
// Okapia
// 27 June 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.jmx;


public interface OsidProviderMXBean {


    /**
     *  Gets the identifier for this bean.
     *
     *  @return the identifier
     */
    
    public String getIdentifier();
        

    /**
     *  Gets the context. The context exists for providers
     *  of OsidRuntimeManager.
     *
     *  @return the context of the runtime
     */

    public String getContext();


    /**
     *  Gets the OSID Package name.
     * 
     *  @return the OSID Package name
     */

    public String getOsidPackage();


    /**
     *  Gets the implementation class.
     * 
     *  @return the implementation class
     */

    public String getImplementationName();


    /**
     *  Gets the service Id for the OSID Provider.
     *
     *  @return the service Id
     */

    public String getId();


    /**
     *  Gets the display name for this provider.
     *
     *  @return the display name
     */

    public String getName();


    /**
     *  Gets the description for this provider.
     *
     *  @return the description
     */

    public String getDescription();


    /**
     *  Gets the version for this provider.
     *
     *  @return the version
     */

    public String getVersion();


    /**
     *  Gets the release date for this provider.
     *
     *  @return the release date
     */

    public java.util.Date getReleaseDate();


    /**
     *  Gets the boot time.
     *
     *  @return the time this OSID Provider started
     */

    public java.util.Date getBootTime();


    /**
     *  Gets the uptime in milliseconds.
     *
     *  @return the uptime
     */

    public long getUptime();


    /**
     *  Gets the OSID Providers requested through this OSID Provider. 
     *
     *  @return the spawned OSID Providers
     */

    public java.util.List<OsidProviderMXBean> getServices();


    /**
     *  Gets the active OsidSessions from this OSID Provider.
     *
     *  @return the OsidSessions
     */

    public java.util.List<OsidSessionMXBean> getSessions();


    /**
     *  Gets the stats for this provider.
     *
     *  @return the stats
     */

    public OsidStatsMXBean getStats();


    /**
     *  Gets the stats for this provider and all of its services.
     *
     *  @return the stats
     */

    public OsidStatsMXBean getCumulativeStats();
}
