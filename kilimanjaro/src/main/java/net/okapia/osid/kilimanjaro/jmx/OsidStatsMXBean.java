//
// OsidStatsMXBean.java
//
//     A JMX Bean for an OSID Stats.
//
//
// Tom Coppeto
// Okapia
// 27 June 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.jmx;


public interface OsidStatsMXBean {


    /**
     *  Gets the number of calls into the OSID Provider from OSID
     *  Consumers.
     *
     *  @return number of calls
     */

    public long getCallsIn();


    /**
     *  Gets the number of calls into OSID Consumers from the OSID
     *  Provider.
     *
     *  @return number of calls
     */

    public long getCallsOut();


    /**
     *  Gets the number of objects into the OSID Provider from OSID
     *  Consumers.
     *
     *  @return number of objects
     */

    public long getObjectsIn();


    /**
     *  Gets the number of objects into OSID Consumers from the OSID
     *  Provider.
     *
     *  @return number of objects
     */

    public long getObjectsOut();


    /**
     *  Gets the number of bytes transferred into the OSID Provider
     *  from OSID Consumers.
     *
     *  @return number of bytes transferred
     */

    public long getBytesIn();


    /**
     *  Gets the number of bytes transferred from the OSID Provider to
     *  OSID Consumers.
     *
     *  @return number of bytes transferred
     */

    public long getBytesOut();


    /**
     *  Gets the number of contract errors resulting from OSID
     *  Consumer actions.
     *
     *  @return number of contract errors
     */

    public long getContractErrorsIn();    


    /**
     *  Gets the number of contract errors from OSID Provider actions.
     *
     *  @return number of contract errors
     */

    public long getContractErrorsOut();


    /**
     *  Gets the number of user errors thrown from the OSID
     *  Provider.
     *
     *  @return number of user errors
     */

    public long getUserErrors();


    /**
     *  Gets the number of operational errors thrown from the OSID
     *  Provider.
     *
     *  @return number of operational errors
     */

    public long getOperationalErrors();


    /**
     *  Gets the number of java system errors thrown from the OSID
     *  Provider.
     *
     *  @return number of system errors
     */

    public long getSystemErrors();


    /**
     *  Gets the number of unknown errors thrown from the OSID
     *  Provider.
     *
     *  @return number of unknown errors
     */

    public long getUnknownErrors();
        

    /**
     *  Gets a breakdown of all errors.
     *
     *  @return the list errors by type
     */

    public java.util.List<OsidErrorStatsMXBean> getErrors();
}
