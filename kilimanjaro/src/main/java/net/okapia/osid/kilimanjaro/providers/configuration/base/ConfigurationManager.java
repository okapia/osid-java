//
// ConfigurationManager.java
//
//     A Configuration manager for the OSID Runtime.
//
//
// Tom Coppeto
// Okapia
// 4 January 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.providers.configuration.base;

import org.osid.binding.java.annotation.OSID;


/**
 *  An OSID Configuration Provider for the OSID Runtime. 
 */

public final class ConfigurationManager
    extends net.okapia.osid.jamocha.configuration.spi.AbstractConfigurationManager
    implements org.osid.configuration.ConfigurationManager,
               org.osid.configuration.ConfigurationProxyManager {


    /**
     *  Constructs a new <code>ConfigurationManager</code>.
     */

    public ConfigurationManager() {
        super(new ServiceProvider());
        return;
    }


    /**
     *  Tests for the availability of a configuration value lookup service. 
     *
     *  @return <code> true </code> if value lookup is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsValueLookup() {
        return (true);
    }


    /**
     *  Gets a configuration value lookup session. 
     *
     *  @return a <code> ValueLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsValueLookup() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.IllegalStateException this manager has been shut down 
     */

    @OSID @Override
    public org.osid.configuration.ValueLookupSession getValueLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.OperationFailedException("no default configuration available");
    }


    /**
     *  Gets a configuration value lookup session using the supplied 
     *  configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> to use 
     *  @return a <code> ValueLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> configurationId </code> is 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsValueLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.IllegalStateException this manager has been shut down 
     */

    @OSID @Override
    public org.osid.configuration.ValueLookupSession getValueLookupSessionForConfiguration(org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (new ValueLookupSession(configurationId));
    }
}
