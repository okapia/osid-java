//
// ManagerTerm.java
//
//     Validates the state of various objects based on the OSID
//     specifications.
//
//
// Tom Coppeto
// Okapia
// 4 January 2009
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.executor;


/**
 *  Validates the state of various objects based on the OSID
 *  specifications. This class is only used by the BoilerPlate
 *  contract.
 */

public class ManagerTerm
    extends AbstractClosableTerm
    implements FinePrint {

    private final net.okapia.osid.kilimanjaro.jmx.impl.OsidProvider jmx;
    private boolean initialized = false;


    /**
     *  Constructs a new <code>ManagerTerm</code>.
     *  
     *  @param parent
     *  @param spec interface specification
     *  @param jmx the management bean
     */

    protected ManagerTerm(FinePrint parent, org.osid.meta.OsidInterface spec,
                          net.okapia.osid.kilimanjaro.jmx.impl.OsidProvider jmx) {
        super(parent, spec, jmx.getStatsCollector());
        this.jmx = jmx;
        return;
    }


    /**
     *  Approve the invocation of a method in this interface. This
     *  method throws a runtime exception to abort the transaction.
     *
     *  @param method the OSID spec
     *  @param args method arguments
     */

    @Override
    protected void approve(org.osid.meta.OsidMethod method, Object[] args) {
        super.approve(method, args);

        String name = method.getName();
        if (name.equals("initialize")) {
            if (this.initialized) {
                throw new org.osid.IllegalStateException(getInterface().getName() + " already initialized");
            }

            this.initialized = true;
        } 

        /* 2017: me thinks this is some debugging. If this runtime
         * initializes, then why check for it?

        else if (!this.initialized) {
            throw new org.osid.IllegalStateException(getInterface().getName() + " is not initialized");
        }
          */
        return;
    }


    /**
     *  Applies a little consideration to the return values. 
     *
     *  @param method the OSID spec
     *  @param delegate the delegate object
     *  @param ret the return object
     */

    @Override
    protected Object consideration(org.osid.meta.OsidMethod method, Object delegate, Object ret) {
        String name = method.getName();
        if (name.equals("close")) {
            close();
        }

        return (ret);
    }

    
    /**
     *  Handle the toString() method.
     *  
     *  @param delegate
     *  @return string
     */

    @Override
    public String processToString(Object delegate) {
        String s = delegate.toString();
        if (s.startsWith(delegate.getClass().getName())) {
            org.osid.OsidProfile mgr = (org.osid.OsidProfile) delegate;
            return (mgr.getDisplayName() + " [" + getClassName() + ":" + mgr.getId() + "]");
        } else {
            return (s);
        }
    } 

    
    /**
     *  Gets the jmx provider.
     */

    protected net.okapia.osid.kilimanjaro.jmx.impl.OsidProvider getJMXProvider() {
        return (this.jmx);
    }


    /**
     *  Creates a fine print term.
     *
     *  @param parent the parent's fine print
     *  @param spec interface specification
     *  @param object the delegate object
     *  @return new term
     *  @throws org.osid.OsidRuntimeException interfaceName not defined
     */

    protected static FinePrint createTerm(FinePrint parent, org.osid.meta.OsidInterface spec, Object object) {

        /* the manager came from the runtime or another manager */
        if (!(parent instanceof ManagerTerm)) {
            throw new org.osid.OsidRuntimeException("manager not created from a provider");
        }

        if (!(object instanceof org.osid.OsidProfile)) {
            throw new org.osid.OsidRuntimeException("object not an OsidProfile");
        }

        org.osid.OsidProfile profile = (org.osid.OsidProfile) object;

        if (object instanceof Contract) {
            System.out.println(">>>> " + profile.getDisplayName() + " is a proxy");
        }

        int ch = spec.getName().lastIndexOf(".");
        String pkg = spec.getName().substring(0, ch);

        ManagerTerm manager = (ManagerTerm) parent;
        net.okapia.osid.kilimanjaro.jmx.impl.OsidProvider provider;
        try {
            provider = manager.getJMXProvider().createProvider(profile, pkg, profile.getClass().getName());
        } catch (javax.management.JMException jme) {
            throw new org.osid.OsidRuntimeException(jme);
        }

        return (new ManagerTerm(parent, spec, provider));        
    }


    /**
     *  Shuts down this contract. ManagerTerms are tracked in Binder.
     */

    @Override
    public synchronized void close() {
        Binder.unbind(this);
        super.close();
        return;
    }
}
