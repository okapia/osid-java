//
// ValueLookupSession.java
//
//    An adapter to inject rules into a ValueLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.providers.configuration.rules;

import org.osid.binding.java.annotation.OSID;

import net.okapia.osid.kilimanjaro.types.records.proxy.proxy.PathProxyRecord;
import net.okapia.osid.kilimanjaro.types.records.rules.condition.PathConditionRecord;
import net.okapia.osid.kilimanjaro.types.records.rules.condition.ValueConditionRecord;
import net.okapia.osid.kilimanjaro.types.records.rules.result.ValueResultRecord;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An adapter to inject rules into a ValueLookupSession.
 */

public final class ValueLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.Adapter<org.osid.configuration.ValueLookupSession> {

    private org.osid.configuration.ValueLookupSession valueSession;
    private org.osid.rules.RulesSession ruleSession;
    private org.osid.rules.Rule rule;
    private boolean conditional = false;
    private String[] path;

    
    /**
     *  Creates a <code>ValueLookupSession</code> adapter.
     *
     *  @param valueSession a ValueLookupSession
     *  @param rulesSession a RulesSession
     *  @param rule a rule to evaluate values
     *  @throws org.osid.NullArgumentException
     *          <code>valueSession</code>, <code>ruleSession</code>,
     *          or <code>rule</code> is <code>null</code>
     */

    private ValueLookupSession(org.osid.configuration.ValueLookupSession valueSession, 
                               org.osid.rules.RulesSession rulesSession, org.osid.rules.Rule rule) {
        
        super(valueSession);
        nullarg(rulesSession, "rule session");
        nullarg(rule, "rule");

        this.valueSession = valueSession;
        this.ruleSession  = rulesSession;
        this.rule         = rule;

        return;
    }


    /**
     *  Creates a <code>ValueLookupSession</code> adapter.
     *
     *  @param valueSession a ValueLookupSession
     *  @param rulesSession a RulesSession
     *  @param rule a rule to evaluate values
     *  @param proxy an OSID proxy
     *  @throws org.osid.NullArgumentException
     *          <code>valueSession</code>, <code>ruleSession</code>,
     *          <code>rule</code>, or <code>proxy</code> is
     *          <code>null</code>
     */

    private ValueLookupSession(org.osid.configuration.ValueLookupSession values, 
                               org.osid.rules.RulesSession rules, org.osid.rules.Rule rule,
                               org.osid.proxy.Proxy proxy) {
        
        this(values, rules, rule);

        try {
            if (proxy.hasRecordType(PathProxyRecord.TYPE)) {
                PathProxyRecord record = (PathProxyRecord) proxy.getProxyRecord(PathProxyRecord.TYPE);
                this.path = record.getPath();
            }
        } catch (org.osid.OsidException oe) {
            System.err.println(oe.getMessage());
        }
        
        return;
    }


    /**
     *  Creates this ValueLookupSession by creating the adapter for
     *  the delegate.
     *
     *  @param valueSession a ValueLookupSession
     *  @param rulesSession a RulesSession
     *  @param rule a rule to evaluate values
     *  @return the session adapter
     *  @throws org.osid.NullArgumentException
     *          <code>valueSession</code>, <code>ruleSession</code>,
     *          or <code>rule</code> is <code>null</code>
     */

    protected static org.osid.configuration.ValueLookupSession createValueLookupSession(org.osid.configuration.ValueLookupSession valueSession, 
                                                                                        org.osid.rules.RulesSession rulesSession,
                                                                                        org.osid.rules.Rule rule) {

        return ((new ValueLookupSession(valueSession, rulesSession, rule)).create());
    }


    /**
     *  Creates this ValueLookupSession by creating the adapter for
     *  the delegate.
     *
     *  @param valueSession a ValueLookupSession
     *  @param rulesSession a RulesSession
     *  @param rule a rule to evaluate values
     *  @param proxy an OSID proxy
     *  @return the session adapter
     *  @throws org.osid.NullArgumentException
     *          <code>valueSession</code>, <code>ruleSession</code>,
     *          <code>rule</code>, or <code>proxy</code> is
     *          <code>null</code>
     */

    protected static org.osid.configuration.ValueLookupSession createValueLookupSession(org.osid.configuration.ValueLookupSession valueSession, 
                                                                                        org.osid.rules.RulesSession rulesSession,
                                                                                        org.osid.rules.Rule rule,
                                                                                        org.osid.proxy.Proxy proxy) {

        return ((new ValueLookupSession(valueSession, rulesSession, rule, proxy)).create());
    }


    /**
     *  Returns only values that pass the defined parameter condition. Some 
     *  parameter conditions do not require explicit conditional data to be 
     *  passed and the <code> Values </code> returned from any method in this 
     *  session are filtered on an implicit condition. 
     */

    @OSID
    public void useConditionalView() {
        this.conditional = true;
        this.valueSession.useConditionalView();
        return;
    }


    /**
     *  Values that are filtered based on an implicit condition are not 
     *  filtered out from methods in this session. Methods that take an 
     *  explicit condition as a parameter are filtered on only those 
     *  conditions that are specified. 
     */

    @OSID
    public void useUnconditionalView() {
        this.conditional = false;
        this.valueSession.useUnconditionalView();
        return;
    }


    /**
     *  Gets the <code> Values </code> for the given parameter <code> Id. 
     *  </code> 
     *
     *  @param  parameterId the <code> Id </code> of the <code> Parameter 
     *          </code> to retrieve 
     *  @return the value list 
     *  @throws org.osid.NotFoundException the <code> parameterId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException the <code> parameterId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID
    public org.osid.configuration.ValueList getValuesByParameter(org.osid.id.Id parameterId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.configuration.ValueList values = this.valueSession.getValuesByParameter(parameterId);

        if (this.conditional) {
            return (processRule(values));
        } else {
            return (values);
        }
    }


    /**
     *  Gets the <code> Values </code> for the given parameter <code>
     *  Ids.  </code> In pleneary mode, the values for all parameters
     *  are returned in the order requested or an error results. In
     *  comparative mode, inaccessible values may be omitted or the
     *  values reordered.
     *
     *  @param  parameterIds the <code> Id </code> of the <code> Parameter 
     *          </code> to retrieve 
     *  @return the value list 
     *  @throws org.osid.NotFoundException a parameter <code> Id </code> is 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> parameterIds </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID
    public org.osid.configuration.ValueList getValuesByParameters(org.osid.id.IdList parameterIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.configuration.ValueList values = this.valueSession.getValuesByParameters(parameterIds);
        if (this.conditional) {
            return (processRule(values));
        } else {
            return (values);
        }
    }


    /**
     *  Gets all <code>Values</code>. In plenary mode, the returned 
     *  list contains all known values or an error results. Otherwise, 
     *  the returned list may contain only those values that are 
     *  accessible through this session. In both cases, the order of the set 
     *  is not specified. 
     *
     *  @return a list of <code>Values</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID
    public org.osid.configuration.ValueList getValues() 
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.configuration.ValueList values = this.valueSession.getValues();

        if (this.conditional) {
            return (processRule(values));
        } else {
            return (values);
        }
    }


    /**
     *  Gets a value condition for the given parameter. The conditions
     *  used by the underlying rules provider are not exposed.
     *
     *  @param  parameterId the <code> Id </code> of a <code> Parameter 
     *          </code> 
     *  @return a value condition 
     *  @throws org.osid.NullArgumentException <code> parameterId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID
    public org.osid.configuration.ValueCondition getValueCondition(org.osid.id.Id parameterId) {
        return (new net.okapia.osid.jamocha.nil.configuration.valuecondition.UnValueCondition());
    }


    /**
     *  Gets the values in this configuration based on a condition. <code> 
     *  </code> The condition specified is applied to any or all parameters in 
     *  this configuration as applicable. In pleneary mode, all values are 
     *  returned or an error results. In comparative mode, inaccessible values 
     *  may be omitted. 
     *
     *  @param  valueCondition condition interface 
     *  @return the value list 
     *  @throws org.osid.NullArgumentException <code> valueCondition </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException <code> valueCondition </code> 
     *          not of this service 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID
    public org.osid.configuration.ValueList getValuesOnCondition(org.osid.configuration.ValueCondition valueCondition)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        if (!(valueCondition instanceof net.okapia.osid.jamocha.nil.configuration.valuecondition.UnValueCondition)) {
            throw new org.osid.UnsupportedException("condition not of this service");
        }

        return (getValues());
    }


    /**
     *  Gets the values for a parameter based on a condition. In pleneary 
     *  mode, all values are returned or an error results. In comparative 
     *  mode, inaccessible values may be omitted. 
     *
     *  @param  parameterId the <code> Id </code> of a <code> Parameter 
     *          </code> 
     *  @param  valueCondition the condition 
     *  @return the value list 
     *  @throws org.osid.NotFoundException parameter <code> Id </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> parameterId </code> or 
     *          <code> valueCondition </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException <code> valueCondition </code> is 
     *          not of this service 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID
    public org.osid.configuration.ValueList getValuesByParameterOnCondition(org.osid.id.Id parameterId, 
                                                                            org.osid.configuration.ValueCondition valueCondition)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        if (!(valueCondition instanceof net.okapia.osid.jamocha.nil.configuration.valuecondition.UnValueCondition)) {
            throw new org.osid.UnsupportedException("condition not of this service");
        }

        return (getValuesByParameter(parameterId));
    }


    /**
     *  Gets the values for parameters based on a condition. The specified 
     *  condition is applied to any or all of the parameters as applicable. In 
     *  pleneary mode, all values are returned or an error results. In 
     *  comparative mode, inaccessible values may be omitted. 
     *
     *  @param  parameterIds the <code> Id </code> of a <code> Parameter 
     *          </code> 
     *  @param  valueCondition the condition 
     *  @return the value list 
     *  @throws org.osid.NotFoundException a parameter <code> Id </code> is 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> parameterIds </code> or 
     *          <code> valueCondition </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException <code> valueCondition </code> 
     *          not of this service 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID
    public org.osid.configuration.ValueList getValuesByParametersOnCondition(org.osid.id.IdList parameterIds,
                                                                             org.osid.configuration.ValueCondition valueCondition)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (!(valueCondition instanceof net.okapia.osid.jamocha.nil.configuration.valuecondition.UnValueCondition)) {
            throw new org.osid.UnsupportedException("condition not of this service");
        }

        return (getValuesByParameters(parameterIds));    
    }


    private org.osid.configuration.ValueList processRule(org.osid.configuration.ValueList values)
        throws org.osid.OperationFailedException {

        return (processRule(values, this.path));
    }


    private org.osid.configuration.ValueList processRule(org.osid.configuration.ValueList values, String[] path)
        throws org.osid.OperationFailedException {

        try {

            /*
             * Get the condition of the underlying rules provider and set
             * the input value list.
             */

            org.osid.rules.Condition condition = this.ruleSession.getConditionForRule(this.rule.getId());
            ValueConditionRecord valuesConditionRecord = (ValueConditionRecord) condition.getConditionRecord(ValueConditionRecord.TYPE);            
            valuesConditionRecord.addValueList(values);     

            /*
             * Set the input path if exists.
             */

            if (path.length > 0) {
                PathConditionRecord pathConditionRecord = (PathConditionRecord) condition.getConditionRecord(PathConditionRecord.TYPE);
            
                pathConditionRecord.setPath(path);
            }

            /*
             * Execute the rule.
             */

            org.osid.rules.Result result = this.ruleSession.executeRule(this.rule.getId(), condition);      

            /*
             * Return the ValueList from the Result record.
             */

            return (((ValueResultRecord) result.getResultRecord(ValueResultRecord.TYPE)).getValues());
        } catch (Exception e) {
            throw new org.osid.OperationFailedException("unable to execute rule", e);
        }       
    }
}
