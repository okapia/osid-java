//
// Parser.java
//
//     Parses an XML based configuration file.
//
//
// Tom Coppeto
// Okapia
// 22 March 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.providers.rules.configuration;

import net.okapia.osid.primordium.id.BasicId;
import net.okapia.osid.primordium.locale.text.eng.us.Plain;

import net.okapia.osid.torrefacto.xml.XMLUtil;


/**
 *  Parses an XML based configuration file.
 */

public final class Parser {

    private final java.util.Map<org.osid.id.Id, java.util.Collection<org.osid.id.Id>> timeRules = new java.util.HashMap<>();
    private final java.util.Map<org.osid.id.Id, java.util.Collection<org.osid.id.Id>> pathRules = new java.util.HashMap<>();
    private org.osid.rules.Engine engine;


    /**
     *  Constructs a new parser for configuration files.
     *
     *  @param  engineId path to configuration file
     *  @throws org.osid.NotFoundException file not found
     *  @throws org.osid.NullArgumentException <code>file</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException error reading file
     */

    public Parser(org.osid.id.Id engineId) 
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        String file = getFileFromId(engineId);
        XMLUtil xml = new XMLUtil(file + "-rules.xml");
        xml.addNamespace("c", "urn:inet:osid.org:schemas:providers/rules/configuration/1");
        
        org.dom4j.Element root = xml.getElement("/c:engine", true);
        String name = file;
        String description = "A rules mapping file forconfiguration values.";
        
        org.dom4j.Element element = xml.getElement(root, "c:displayName", false);
        if (element != null) {
            name = xml.trim(element.asXML());
        }
        
        element = xml.getElement(root, "c:description", false);
        if (element != null) {
            description = xml.trim(element.asXML());
        }
        
        this.engine = new net.okapia.osid.jamocha.builder.rules.engine.EngineBuilder()
            .id(engineId)
            .displayName(Plain.valueOf(name))
            .description(Plain.valueOf(description))
            .provider(net.okapia.osid.provider.Providers.OKAPIA.getProvider())
            .build();

        for (org.dom4j.Node valueNode : xml.getNodes(root, "c:value")) {
            org.osid.id.Id valueId;
            try {
                valueId = BasicId.valueOf(valueNode.valueOf("@id"));
            } catch (Exception e) {
                System.err.println("bad value id in " + file);
                continue;
            }

            for (org.dom4j.Node ruleNode : xml.getNodes(valueNode, "c:rule")) {
                org.osid.id.Id ruleId;
                try {
                    ruleId = BasicId.valueOf(ruleNode.valueOf("@id"));
                } catch (Exception e) {
                    System.err.println("bad rule id in " + file);
                    continue;
                }

                /*
                 * peek at the rule Id to see what kind of rule it is
                 */

                java.util.Map<org.osid.id.Id, java.util.Collection<org.osid.id.Id>> map;                

                if (ruleId.getIdentifier().equals("identifiers:rules/Rule/path")) {
                    map = this.pathRules;
                } else {
                    map = this.timeRules;
                }

                java.util.Collection<org.osid.id.Id> rules = map.get(valueId);
                if (rules == null) {
                    rules = new java.util.HashSet<>();
                    map.put(valueId, rules);
                }

                rules.add(ruleId);
            }
        }

        return;     
    }


    /**
     *  Gets the engine.
     *
     *  @return the engine
     */

    public org.osid.rules.Engine getEngine() {
        return (this.engine);
    }


    /**
     *  Gets the rules associated with a value id.
     *
     *  @param valueId <code>Id</code> of the <code>Value</code>
     *  @return the rules or <code>null</code> if not found
     */

    public java.util.Collection<org.osid.id.Id> getPathRules(org.osid.id.Id valueId) {
        return (this.pathRules.get(valueId));
    }


    /**
     *  Gets all the rules.
     *
     *  @return the rules map
     */

    public java.util.Map<org.osid.id.Id, java.util.Collection<org.osid.id.Id>> getTimeRules() {
        return (this.timeRules);
    }


    private static String getFileFromId(org.osid.id.Id id)
        throws org.osid.NotFoundException {

        if (!id.getIdentifierNamespace().equals("file") || !id.getAuthority().equals("localhost")) {
            throw new org.osid.NotFoundException("identifier not a local path");
        }

        return (id.getIdentifier());
    }
}
