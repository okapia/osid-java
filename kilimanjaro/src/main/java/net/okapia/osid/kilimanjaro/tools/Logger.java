//
// Logger.java
//
//     Logs a message. 
//
//
// Tom Coppeto
// 30 September 2008
//
//
// Copyright (c) 2008 Massachusetts Institute of Technology. All Rights 
// Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.tools;

import org.osid.*;
import org.osid.type.Type;
import org.osid.logging.*;

import net.okapia.osid.kilimanjaro.BootLoader;
import net.okapia.osid.kilimanjaro.OsidVersions;

import net.okapia.osid.primordium.type.BasicType;


public class Logger {
    private final org.osid.logging.LoggingManager manager;
    private static final Type stringContentType = BasicType.valueOf("logging.LogEntry.content:string@okapia.net");


    public Logger(String impl)
        throws Exception {

        BootLoader loader = new BootLoader();
        OsidRuntimeManager runtime = loader.getRuntimeManager("Logger");

        this.manager = (LoggingManager) runtime.getManager(OSID.LOGGING, impl, 
                                                           OsidVersions.V3_0_0.getVersion());
        return;
    }   


    public void log(org.osid.type.Type priority, String message)
        throws Exception {

        LoggingSession session = this.manager.getLoggingSession();
        session.logAtPriority(priority, message, stringContentType);

        session.close();
        return; 
    }


    public void log(String message)
        throws Exception {

        log (SyslogPriority.INFO.getType(), message);
        return;
    }


    public static void main(String[] args)
        throws Exception {

        String implName    = null;
        boolean osid       = false;
        boolean priority   = false;
        String message     = "";
        org.osid.type.Type priorityType = null;

        for (String arg : args) {
            if (arg.equals("-osid")) {
                osid = true;
                continue;
            }

            if (arg.equals("-p")) {
                priority = true;
                continue;
            }

            if (arg.equals("-h")) {
                System.err.println("usage: Logger [-p <priority>] [-osid <impl>] <message>");
                System.exit(0);
            }

            if (priority && (priorityType == null)) {
                if (arg.equals("fatal")) {
                    priorityType = SyslogPriority.FATAL.getType();
                } else if (arg.equals("critical")) {
                    priorityType = SyslogPriority.CRITICAL.getType();
                } else if (arg.equals("error")) {
                    priorityType = SyslogPriority.ERROR.getType();
                } else if (arg.equals("warning")) {
                    priorityType = SyslogPriority.WARNING.getType();
                } else if (arg.equals("info")) {
                    priorityType = SyslogPriority.INFO.getType();
                } else if (arg.equals("debug")) {
                    priorityType = SyslogPriority.DEBUG.getType();
                } else if (arg.equals("trace")) {
                    priorityType = SyslogPriority.TRACE.getType();
                } else {
                    System.err.println("priority is one of fatal, critical, error, warning, info, debug, trace");
                    System.exit(1);
                }

                continue;
            }

            if (osid && (implName == null)) {
                implName = arg;
                continue;
            }

            if (message.length() > 0) {
                message += " ";
            }
            
            message += arg;
        }

        if (message == null) {
            return;
        }

        Logger logger = new Logger(implName);
        if (priorityType != null) {
            logger.log(priorityType, message);
        } else {
            logger.log(message);
        }

        return;
    }


    enum SyslogPriority {
        TRACE ("trace"),
        DEBUG("debug"),
        INFO("info"),
        NOTICE("notice"),
        WARNING("warning"),
        ERROR("error"),
        CRITICAL("critical"),
        FATAL("fatal");

        private final org.osid.type.Type type;


        private SyslogPriority(String id) {
            this.type = BasicType.valueOf("logging.priority:" + id + "@okapia.net");
            return;
        }


        /**
         *  Gets the associated type.
         *
         *  @return the type
         */

        public org.osid.type.Type getType() {
            return (this.type);
        }
    }
}

