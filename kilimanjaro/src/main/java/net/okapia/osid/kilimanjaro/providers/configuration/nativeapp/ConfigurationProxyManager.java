//
// ConfigurationProxyManager.java
//
//     A Configuration adapter to load a Configuration for the native
//     runtime.
//
//
// Tom Coppeto
// Okapia
// 2 April 2010
//
// Copyright (c) Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.providers.configuration.nativeapp;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  A Configuration adapter to load a Configuration for the native
 *  runtime.
 */

public final class ConfigurationProxyManager
    extends net.okapia.osid.jamocha.configuration.spi.AbstractConfigurationManager
    implements org.osid.configuration.ConfigurationProxyManager {

    private org.osid.configuration.ConfigurationProxyManager configurationManager;
    private org.osid.orchestration.OrchestrationManager orchestration;


    /**
     *  Constructs a new <code>ConfigurationProxyManager</code>.
     */

    public ConfigurationProxyManager() {
        super(new ConfigurationServiceProvider());
        return;
    }

    
    /**
     *  Initializes this manager. A manager is initialized once at the time of 
     *  creation. 
     *
     *  @param  runtime the runtime environment 
     *  @throws org.osid.ConfigurationErrorException an error with 
     *          implementation configuration 
     *  @throws org.osid.IllegalStateException this manager has already been 
     *          initialized by the <code> OsidLoader </code>
     *  @throws org.osid.NullArgumentException <code> runtime </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public void initialize(org.osid.OsidRuntimeManager runtime)
        throws org.osid.ConfigurationErrorException,
               org.osid.OperationFailedException {

        super.initialize(runtime);

        this.orchestration = (org.osid.orchestration.OrchestrationManager) getOsidManager(org.osid.OSID.ORCHESTRATION, "net.okapia.osid.kilimanjaro.providers.configuration.rules.OrchestrationManager");
        this.configurationManager = this.orchestration.getConfigurationProxyManager();


        /*
         * check out the configuration service
         */

        if (!this.configurationManager.supportsValueLookup() || !this.configurationManager.supportsVisibleFederation()) {
            throw new org.osid.ConfigurationErrorException("configuration service does not support federated value lookups");
        }
            
        return;
    }


    /**
     *  Tests for the availability of a configuration value lookup service. 
     *
     *  @return <code> true </code> if value lookup is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsValueLookup() {
        return (true);
    }


    /**
     *  Gets a configuration value lookup session. 
     *
     *  @return a <code> ValueLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsValueLookup() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.IllegalStateException this manager has been shut down 
     */

    @OSID @Override
    public org.osid.configuration.ValueLookupSession getValueLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.OperationFailedException("proxy required");
    }


    /**
     *  Gets a configuration value lookup session.
     *
     *  @param  proxy a proxy 
     *  @return a <code> ValueLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException
     *          <code>supportsValueLookup()</code> is <code> False
     *          </code>
     *  @throws org.osid.IllegalStateException this manager has been shut down 
     */

    @OSID @Override
    public org.osid.configuration.ValueLookupSession getValueLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (new ValueLookupSession(this.configurationManager, proxy));
    }


    /**
     *  Shuts down this manager.
     */

    @OSIDBinding @Override
    public void close() {
        this.configurationManager.close();
        this.orchestration.close();
        super.close();
        return;
    }
}
