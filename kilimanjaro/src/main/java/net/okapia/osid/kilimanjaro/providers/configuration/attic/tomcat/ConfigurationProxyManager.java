//
// ConfigurationProxyManager.java
//
//     A Configuration adapter to load a Configuration for the native
//     runtime.
//
//
// Tom Coppeto
// Okapia
// 2 April 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.providers.configuration.tomcat;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.primordium.id.BasicId;
import net.okapia.osid.primordium.calendaring.GregorianUTCDateTime;
import net.okapia.osid.primordium.installation.SoftwareVersion;
import net.okapia.osid.primordium.locale.text.eng.us.Plain;


/**
 *  A Configuration adapter to load a Configuration for the native
 *  runtime.
 */

public final class ConfigurationProxyManager
    extends net.okapia.osid.jamocha.configuration.spi.AbstractConfigurationManager
    implements org.osid.configuration.ConfigurationProxyManager {

    private static final org.osid.id.Id ID                          = BasicId.valueOf("OSID Runtime Providers:configuration.tomcat@okapia.net");
    private static final org.osid.locale.DisplayText DISPLAY_NAME   = Plain.valueOf("Tomcat Configuration Provider");
    private static final org.osid.locale.DisplayText DESCRIPTION    = Plain.valueOf("An OSID Configuration Provider for the Tomcat Runtime.");
    private static final org.osid.installation.Version VERSION      = SoftwareVersion.valueOf("0.1.0");
    private static final org.osid.calendaring.DateTime RELEASE_DATE = GregorianUTCDateTime.valueOf("2010-1-4T12:30");

    private org.osid.configuration.ConfigurationProxyManager configurationManager;


    /**
     *  Constructs a new <code>ConfigurationProxyManager</code>.
     */

    public ConfigurationProxyManager() {
        super(ID);
        setMetadata(DISPLAY_NAME, DESCRIPTION, VERSION, RELEASE_DATE,
                    net.okapia.osid.provider.Providers.OKAPIA.getProvider());
        return;
    }

    
    /**
     *  Initializes this manager. A manager is initialized once at the time of 
     *  creation. 
     *
     *  @param  runtime the runtime environment 
     *  @throws org.osid.ConfigurationErrorException an error with 
     *          implementation configuration 
     *  @throws org.osid.IllegalStateException this manager has already been 
     *          initialized by the <code> OsidLoader </code>
     *  @throws org.osid.NullArgumentException <code> runtime </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public void initialize(org.osid.OsidRuntimeManager runtime)
        throws org.osid.OperationFailedException {

        super.initialize(runtime);

        try {
            org.osid.orchestration.OrchestrationManager mgr = (org.osid.orchestration.OrchestrationManager) runtime.getManager(org.osid.OSID.ORCHESTRATION, "org.osid.kilimanjaro.providers.configuration.rules.OrchestrationManager", "3.0.0");
            this.configurationManager = mgr.getConfigurationProxyManager();
        } catch (org.osid.NotFoundException nfe) {
            throw new org.osid.ConfigurationErrorException(nfe);
        }

        /*
         * check out the configuration service
         */

        if (!configurationManager.supportsValueLookup() || !configurationManager.supportsVisibleFederation()) {
            throw new org.osid.ConfigurationErrorException("configuration service does not support federated value lookups");
        }
            
        return;
    }


    /**
     *  Tests for the availability of a configuration value lookup service. 
     *
     *  @return <code> true </code> if value lookup is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsValueLookup() {
        return (true);
    }


    /**
     *  Gets a configuration value lookup session. 
     *
     *  @return a <code> ValueLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsValueLookup() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.IllegalStateException this manager has been shut down 
     */

    @OSID @Override
    public org.osid.configuration.ValueLookupSession getValueLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.OperationFailedException("proxy required");
    }


    /**
     *  Gets a configuration value lookup session.
     *
     *  @param  proxy a proxy 
     *  @return a <code> ValueLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException
     *          <code>supportsValueLookup()</code> is <code> False
     *          </code>
     *  @throws org.osid.IllegalStateException this manager has been shut down 
     */

    @OSID @Override
    public org.osid.configuration.ValueLookupSession getValueLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (new ValueLookupSession(this.configurationManager, proxy));
    }


    /**
     *  Shuts down this manager.
     */

    @OSIDBinding @Override
    public void close() {
        this.configurationManager.close();
        super.close();
    }
}
