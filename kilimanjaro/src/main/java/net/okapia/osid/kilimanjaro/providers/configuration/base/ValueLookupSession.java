//
// ValueLookupSession.java
//
//    Loads configuration data from an XML file.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.providers.configuration.base;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Loads configuration data from an XML file.
 */

public final class ValueLookupSession
    extends net.okapia.osid.jamocha.core.configuration.spi.AbstractIndexedMapValueLookupSession
    implements org.osid.configuration.ValueLookupSession {


    /**
     *  Constructs a new <code>ValueLookupSession</code>.
     *
     *  @param configurationId Id of the Configuration as a File URI
     *  @throws org.osid.NotFoundException dictionary not found
     *  @throws org.osid.NullArgumentException
     *         <code>configurationId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException something broke
     */

    ValueLookupSession(org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(configurationId, "configuration Id");
        load(configurationId);
        return;
    }


    private void load(org.osid.id.Id id)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        Parser parser = new Parser(id);
        setConfiguration(parser.getConfiguration());
        putValues(parser.getValues());
        return;
    }
}
