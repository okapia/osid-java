//
// SessionAdapter.java
//
//     A session adapter.
//
//
// Tom Coppeto
// Okapia
// 30 March 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.providers.configuration.cache;


/**
 *  An OSID Configuration Adapter to translate OSID Provider names
 *  to configuration file names for use with a file-based OSID
 *  Configuration Provider.
 *
 *  All Configuration Ids are intercepted.
 */

public final class SessionAdapter
    extends net.okapia.osid.jamocha.adapter.spi.Adapter<org.osid.OsidSession> {


    /**
     *  Constructs a new adapter.
     *
     *  @param delegate the underlying object
     *  @throws org.osid.NullArgumentException <code>delegate</code>
     *          id <code>null</code>
     */

    public SessionAdapter(org.osid.OsidSession delegate) {
        super(delegate);
        return;
    }


    /**
     *  Invokes a method. Part of the InvocationHandler interface.
     *  This method uses the manager identity methods of this adapter
     *  including Id, displayName, description, version, release date,
     *  license and provider. This behavior may be used by calling
     *  this method or replaced by overriding it completely.
     *
     *  @param proxy the instance of the object on which the method
     *         was invoked
     *  @param method the method invoked
     *  @param args the arguments to the method
     *  @return result of the method
     *  @throws Throwable
     */

    @Override
    public Object invoke(Object proxy, java.lang.reflect.Method method, Object[] args)
        throws Throwable {

        /*
         * intercept the close emethods so the underlying sessions stay open
         * until we shutdown
         */

        if (method.getName().equals("close")) {
            return (null);
        }

        return (super.invoke(proxy, method, args));
    }


    /**
     *  Closes this session for real.
     *
     *  @throws org.osid.IllegalStateException This manager has been
     *         closed.
     */

    public void reallyclose() {
        getDelegate().close();
        return;
    }
}
