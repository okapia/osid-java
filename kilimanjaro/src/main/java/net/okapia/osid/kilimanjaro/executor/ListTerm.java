//
// ListTerm.java
//
//     Validates the state of various objects based on the OSID
//     specifications.
//
//
// Tom Coppeto
// Okapia
// 4 January 2009
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.executor;


/**
 *  Validates the state of various objects based on the OSID
 *  specifications. This class is only used by the BoilerPlate
 *  contract.
 */

public final class ListTerm
    extends AbstractClosableTerm
    implements FinePrint {


    /**
     *  Constructs a new <code>ListTerm</code>.
     *  
     *  @param parent
     *  @param spec interface specification
     */

    protected ListTerm(FinePrint parent, org.osid.meta.OsidInterface spec) {
        super(parent, spec, parent.getStatsCollector());    
        return;
    }


    /**
     *  Applies a little consideration to the return values. 
     *
     *  @param method the OSID spec
     *  @param delegate the delegate object
     *  @param ret the return object
     */

    @Override
    protected Object consideration(org.osid.meta.OsidMethod method, Object delegate, Object ret) {
        String name = method.getName();
        if (name.equals("close")) {
            close();
        }

        return (ret);
    }


    /**
     *  Creates a fine print term.
     *
     *  @param parent the parent's fine print
     *  @param spec interface specification
     *  @param object the delegate object
     *  @return new term
     *  @throws org.osid.OsidRuntimeException interfaceName not defined
     */

    protected static FinePrint createTerm(FinePrint parent, org.osid.meta.OsidInterface spec, Object object) {
        return (new ListTerm(parent, spec));
    }
}
