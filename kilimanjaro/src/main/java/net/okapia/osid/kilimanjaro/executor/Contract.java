//
// Contract.java
//
//     Interface marker for an OSID contract proxy.
//
//
// Tom Coppeto
// Okapia
// 4 January 2009
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.executor;


/**
 *  A marker interface for an OSID proxy. A proxy is employed by a
 *  runtime environment to enforce contract rules and strengthen
 *  encapsulation.
 *
 *  This interface provides the means for provider implementations to
 *  access their own underlying objects. Provider implementations
 *  should first check that this interface exists as it is optional.
 * 
 *  Consumers should not attempt through cast or direct handling of
 *  this interface break the encapsulation of underlying
 *  objects. Agreements between consumers and providers should be
 *  handled through OSID Types.
 */

public interface Contract {

    /**
     *  Retrieve the underlying OSID object from this proxy.  This
     *  method allows providers to access the objects previously sent
     *  to their consumers.
     *
     *  @return the underlying object
     */

    public java.lang.Object violateContract();


    /**
     *  Retrieve the name of the provider owning the underlying
     *  object.
     *
     *  @return the provider
     */

    public String getProviderOfContract();


    /**
     *  Retrieve the name of the supplier of the object.
     *
     *  @return the supplier
     */

    public String getSupplierOfContract();
}

