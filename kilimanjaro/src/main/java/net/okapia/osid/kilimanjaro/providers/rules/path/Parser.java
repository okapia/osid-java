//
// Parser.java
//
//     Parses an XML based configuration file.
//
//
// Tom Coppeto
// Okapia
// 22 September 2009
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.providers.rules.path;

import net.okapia.osid.primordium.locale.text.eng.us.Plain;
import net.okapia.osid.torrefacto.xml.XMLUtil;


/**
 *  Parses an XML based configuration file.
 */

public final class Parser {

    private final java.util.Map<org.osid.id.Id, java.util.Collection<String>> paths = new java.util.HashMap<>();
    private org.osid.rules.Engine engine;


    /**
     *  Constructs a new parser for configuration files.
     *
     *  @param  engineId path to configuration file
     *  @param  ruleId the rule to execute
     *  @throws org.osid.NotFoundException file not found
     *  @throws org.osid.NullArgumentException <code>engineId</code>
     *          or <code>ruleId</code>is <code>null</code>
     *  @throws org.osid.OperationFailedException error reading file
     */

    public Parser(org.osid.id.Id engineId, org.osid.id.Id ruleId) 
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        String file = getFileFromId(engineId);
        XMLUtil xml = new XMLUtil(file + "-path.xml");
        xml.addNamespace("c", "urn:inet:osid.org:schemas:providers/rules/path/1");

        String name = file;
        String description = "A rules file.";
        
        org.dom4j.Element root = xml.getElement("/c:engine", true);
        org.dom4j.Element element = xml.getElement(root, "c:displayName", false);
        if (element != null) {
            name = xml.trim(element.asXML());
        }
        
        element = xml.getElement(root, "c:description", false);
        if (element != null) {
            description = xml.trim(element.asXML());
        }

        this.engine = new net.okapia.osid.jamocha.builder.rules.engine.EngineBuilder()
            .id(engineId)
            .displayName(Plain.valueOf(name))
            .description(Plain.valueOf(description))
            .provider(net.okapia.osid.provider.Providers.OKAPIA.getProvider())
            .build();

        for (org.dom4j.Node ruleNode : xml.getNodes(root, "c:rule")) {
            org.osid.id.Id id;
            try {
                id = net.okapia.osid.primordium.id.BasicId.valueOf(ruleNode.valueOf("@id"));
            } catch (Exception e) {
                System.err.println("bad rule id in " + file);
                continue;
            }

            if (!ruleId.equals(id)) {
                continue;
            }

            for (org.dom4j.Node np : xml.getNodes(ruleNode, "c:value")) {
                try {
                    org.osid.id.Id valueId = net.okapia.osid.primordium.id.BasicId.valueOf(np.valueOf("@id"));
                    org.dom4j.Element pathNode = xml.getElement(np, "c:path", false);
                    if (pathNode == null) {
                        continue;
                    }

                    java.util.Collection<String> path = new java.util.ArrayList<String>();
                    for (org.dom4j.Node node : xml.getNodes(pathNode, "c:component")) {
                        path.add(node.valueOf("@name"));
                    }

                    this.paths.put(valueId, path);
                } catch (Exception e) { 
                    System.err.println("unable to parse path node in rule " + id);
                    continue;
                }
            }
        }

        return;     
    }


    /**
     *  Gets the engine.
     *
     *  @return the engine
     */

    public org.osid.rules.Engine getEngine() {
        return (this.engine);
    }


    /**
     *  Gets the path associated with a value id.
     *
     *  @param id <code>Id</code> of the <code>Value</code>
     *  @return the path or <code>null</code> if not found
     */

    public java.util.Collection<String> getPath(org.osid.id.Id id) {
        return (this.paths.get(id));
    }


    private static String getFileFromId(org.osid.id.Id id)
        throws org.osid.NotFoundException {

        if (!id.getIdentifierNamespace().equals("file") || !id.getAuthority().equals("localhost")) {
            throw new org.osid.NotFoundException("identifier not a local path");
        }

        return (id.getIdentifier());
    }
}
