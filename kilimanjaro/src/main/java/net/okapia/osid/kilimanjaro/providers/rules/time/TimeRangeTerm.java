//
// TimeRangeTerm.java
//
//     A rule Term for a time.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.providers.rules.time;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A rule term for a time.
 */

public class TimeRangeTerm
    extends net.okapia.osid.jamocha.assembly.rules.evaluation.spi.AbstractTerm
    implements net.okapia.osid.jamocha.assembly.rules.evaluation.Term {

    private double start;
    private double end;
    private boolean negate;

    
    public TimeRangeTerm(String start, String end, boolean negate) {
        this(parseTime(start), parseTime(end), negate);
        return;
    }


    public TimeRangeTerm(double start, double end, boolean negate) {
        this.start  = start;
        this.end    = end;
        this.negate = negate;
        return;
    }


    public boolean eval(org.osid.rules.Condition condition) {
        java.util.Calendar cal = java.util.Calendar.getInstance(java.util.TimeZone.getTimeZone("GMT"));
        return (eval(((double) (cal.get(java.util.Calendar.HOUR_OF_DAY) * 3600 
                                + cal.get(java.util.Calendar.MINUTE) * 60 
                                + cal.get(java.util.Calendar.SECOND)))));
    }


    protected boolean eval(double time) {
        boolean result;

        if ((time < this.end) && (time >= this.start)) {
            result = true;
        } else {
            result = false;
        }

        if (negate) {
            return (!result);
        } else {
            return (result);
        }
    }

    
    protected static double parseTime(String time)
        throws org.osid.InvalidArgumentException {

        nullarg(time, "time");

        double ret;
        try {
            String[] a = time.split(":");
            ret = Double.parseDouble(a[0]) * 3600;
            if (a.length > 1) {
                ret += Double.parseDouble(a[1]) * 60;
            }

            if (a.length > 2) {
                ret += Double.parseDouble(a[2]);
            }
        } catch (NumberFormatException nfe) {
            throw new org.osid.InvalidArgumentException("time is malformed", nfe);
        }

        return (ret);
    }
}
