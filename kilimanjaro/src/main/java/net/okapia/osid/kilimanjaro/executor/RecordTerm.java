//
// RecordTerm.java
//
//     Validates the state of various objects based on the OSID
//     specifications.
//
//
// Tom Coppeto
// Okapia
// 4 January 2009
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.executor;


/**
 *  Validate sthe state of various objects based on the OSID
 *  specifications. This class is only used by the BoilerPlate
 *  contract.
 */

public final class RecordTerm
    extends AbstractTerm
    implements FinePrint {


    /**
     *  Constructs a new <code>RecordTerm</code>.
     *  
     *  @param parent
     *  @param spec interface specification
     */

    protected RecordTerm(FinePrint parent, org.osid.meta.OsidInterface spec) {
        super(parent, spec, parent.getStatsCollector());    
        return;
    }


    /**
     *  Gets an OsidMethod based on the method name. The state of
     *  the interface is validated.
     *
     *  @param method
     *  @param delegate
     *  @param args
     *  @return the OSID method
     *  @throws org.osid.InvalidMethodException method not found
     */

    @Override
    public Object perform(java.lang.reflect.Method method, Object delegate, Object[] args)
        throws Throwable {

        org.osid.meta.OsidMethod osidMethod = getInterface().getMethod(method.getName());
        if (osidMethod == null) {
            
            if (args != null) {
                for (Object arg : args) {
                    if (arg == null) {
                        throw new org.osid.NullArgumentException("null argument to " + method.getName());
                    }
                }
            }

            Object ret = method.invoke(delegate, args);
            if (!method.getReturnType().equals(Void.TYPE) && (ret == null)) {
                throw new org.osid.NullReturnException("null return from " + method.getName());
            }
            
            return (ret);
        }

        return (consideration(osidMethod, delegate, execute(osidMethod, method, delegate, args)));
    }


    /**
     *  Creates a fine print term.
     *
     *  @param parent the parent's fine print
     *  @param spec specification interface
     *  @param object the delegate object
     *  @return new term
     *  @throws org.osid.OsidRuntimeException interfaceName not defined
     */

    protected static FinePrint createTerm(FinePrint parent, org.osid.meta.OsidInterface spec, Object object) {
        return (new RecordTerm(parent, spec));
    }
}
