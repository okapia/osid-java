//
// Repose.java
//
//     Tools for repository OSIDs.
//
//
// Tom Coppeto
// 17 October 2008
//
//
// Copyright (c) 2008 Massachusetts Institute of Technology. All Rights 
// Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//


package net.okapia.osid.kilimanjaro.tools;

import org.osid.*;
import org.osid.type.*;
import org.osid.resource.*;
import org.osid.repository.*;
import net.okapia.osid.kilimanjaro.BootLoader;
import net.okapia.osid.kilimanjaro.OsidVersions;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.jar.*;


public class Repose {

    private OsidRuntimeManager runtime;
    private RepositoryManager manager;

    public Repose(String impl)
        throws Exception {

        BootLoader loader = new BootLoader();
        this.runtime = loader.getRuntimeManager("Repose");
        this.manager = (RepositoryManager) this.runtime.getManager(OSID.REPOSITORY, impl,
                                                                   OsidVersions.V3_0_0.getVersion());
        return; 
    }


    private void getAsset(String id, boolean verbose)
        throws org.osid.OsidException {

        AssetLookupSession session = this.manager.getAssetLookupSession();      
        session.useFederatedRepositoryView();

        if (verbose) {
            printAssetDetail(session.getAsset(net.okapia.osid.primordium.id.BasicId.valueOf(id)));
        } else {
            printAssetSummary(session.getAsset(net.okapia.osid.primordium.id.BasicId.valueOf(id)));
        }
        session.close();
        return;
    }


    public void listAssets()
        throws org.osid.OsidException {

        AssetLookupSession session = this.manager.getAssetLookupSession();
        session.useFederatedRepositoryView();
        session.useComparativeAssetView();

        AssetList assets = session.getAssets();
        while (assets.hasNext()) {
            printAssetSummary(assets.getNextAsset());
            System.out.println();
        }

        session.close();
        return;
    }


    public void searchAssets(String queryString)
        throws org.osid.OsidException {

        AssetSearchSession session = this.manager.getAssetSearchSession();
        session.useFederatedRepositoryView();

        AssetQuery query = session.getAssetQuery();
        query.matchKeyword(queryString, net.okapia.osid.primordium.type.BasicType.valueOf("string match:wildcard@okapia.net"), true);
        AssetList assets = session.getAssetsByQuery(query);
        while (assets.hasNext()) {
            printAssetSummary(assets.getNextAsset());
            System.out.println();
        }

        session.close();
        return;
    }


    private void printAssetSummary(Asset asset) {
        print("Asset Id", asset.getId().toString(), 0);
        print("Display Name", asset.getDisplayName().getText(), 0);
        return;
    }


    private void printAssetDetail(org.osid.repository.Asset asset)
        throws org.osid.OsidException {

        printObject(asset, 0);
        AssetContentList contents = asset.getAssetContents();
        System.out.println();
        int indent = 0;

        int c = 0;
        while (contents.hasNext()) {
            print("Asset Content (" + ++c + ")", "", indent+0);
            AssetContent content = contents.getNextAssetContent();
            if (content.hasDataLength()) {
                print("Data Length", content.getDataLength() + " bytes", indent+5);
            }
            if (content.hasURL()) {
                print("URL", content.getURL(), indent+5);
            }
            printObject(content, indent + 5);
            System.out.println();
        }
        
        return;
    }


    private void printObject(org.osid.OsidObject object, int indent)
        throws org.osid.OsidException {

        print("Id", object.getId().toString(), indent);
        print("Name", object.getDisplayName().getText(), indent);
        print("Description", object.getDescription().getText(), indent);
        print("Genus", object.getGenusType() + ")", indent);
 
        TypeList types = object.getRecordTypes();
        int r = 0;
        while (types.hasNext()) {
            Type type = types.getNextType();
            System.out.println();
            print("Record (" + ++r + ")", type.getDisplayName().getText() + "(" + type + ")", indent);
            
            PropertyList properties = object.getPropertiesByRecordType(type);
            while (properties.hasNext()) {
                Property p = properties.getNextProperty();
                print(p.getDisplayName().getText(), p.getValue(), indent + 5);
            }
        }
              
        return;
    }


        
    private void print(String name, String value, int indent) {
        System.out.print(getMargin(indent));
        System.out.print(name + ":");
        System.out.print(getMargin(30-name.length()-1-indent));
        System.out.println(format(value, 30));
        return;
    }

        
    private String getMargin(int indent) {

        if (indent < 0) {
            return ("");
        }

        StringBuffer sb = new StringBuffer(indent);

        for (int i = 0; i < indent; i++) {
            sb.append(' ');
        }

        return (sb.toString());
    }


    private String format(String text, int amount) {
        StringBuffer buf = new StringBuffer(text.length());
        StringBuffer margin = new StringBuffer(amount);
        String[] tokens = text.split(" ");

        for (int i = 0; i < amount; i++) {
            margin.append(' ');
        }

        int col = 0;    
        for (String token: tokens) {
            String[] words = token.trim().split("\n");
            boolean nl = false;

            for (String word : words) {
                if (word.length() == 0) {
                    continue;
                }

                if (nl) {
                    buf.append('\n');
                    buf.append(margin);
                    col = 0;
                }

                if ((col > 0) && ((col + word.length()) > (100 - amount))) {
                    buf.append('\n');
                    buf.append(margin);
                    col = 0;
                } else if (col > 0) {
                    buf.append(' ');
                    ++col;
                }

                buf.append(word.trim());
                col += word.length();
                nl = true;
            }
        }

        return (buf.toString());
    }


    public void close() {
        this.manager.close();
        this.runtime.close();
        return;
    }


    public static void main(String[] args)
        throws Exception {

        String implName    = null;
        String id          = null;
        String query       = null;
        boolean verbose    = false;
        boolean osid       = false;
        boolean list       = false;
        boolean get        = false;
        boolean search     = false;

        for (String arg : args) {
            if (arg.equals("-v")) {
                verbose = true;
                continue;
            }

            if (arg.equals("-osid")) {
                osid = true;
                continue;
            }

            if (arg.equals("-list")) {
                list = true;
                continue;
            }

            if (arg.equals("-get")) {
                get = true;
                continue;
            }

            if (arg.equals("-search")) {
                search = true;
                continue;
            }

            if ((osid == true) && (implName == null)) {
                implName = arg;
                continue;
            }

            if ((get == true) && (id == null)) {
                id = arg;
                continue;
            }

            if ((search == true) && (query == null)) {
                query = arg;
                continue;
            }

            System.err.println("usage: Repose [-v] [-list] [-get <id>] -osid <impl>");
            System.exit(1);
        }

        if ((implName == null) || (get && list)) {
            System.err.println("usage: Repose [-v] [-list] [-get <id>] -osid <impl>");
            System.exit(1);
        }

        Repose r = new Repose(implName);

        if (search) {
            r.searchAssets(query);
        } else if (list) {
            r.listAssets();
        } else if (get) {
            r.getAsset(id, verbose);
        }

        r.close();

        return;
    }
}

