//
// ValueLookupSession
//
//    An adapter to federate multiple OSID configurations.
//
//
// Tom Coppeto
// OnTapSolutions
// 30 March 2010
//
//
// Copyright (c) 2010 Massachusetts Institute of Technology. All Rights 
// Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.providers.configuration.tomcat;

import org.osid.binding.java.annotation.OSID;
import net.okapia.osid.kilimanjaro.types.records.proxy.proxy.PathProxyRecord;

import net.okapia.osid.primordium.id.BasicId;
import net.okapia.osid.primordium.locale.text.eng.us.Plain;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An adapter to federate multiple OSID configurations. The parameter
 *  in the first session defining the parameter is referenced.
 */

public final class ValueLookupSession 
    extends net.okapia.osid.jamocha.adapter.federator.configuration.AbstractValueLookupSession
    implements org.osid.configuration.ValueLookupSession {

    private static final org.osid.configuration.Configuration CONFIGURATION = new net.okapia.osid.jamocha.builder.configuration.configuration.ConfigurationBuilder()
	.id(BasicId.valueOf("org.osid.configuration.Configuration:configuration/nativeapp@okapia.net"))
	.displayName(Plain.valueOf("Native Runtime OSID Configuration"))
	.description(Plain.valueOf("An OSID Configuration for the native application runtime environment."))
	.provider(net.okapia.osid.provider.Providers.OKAPIA.getProvider())
	.build();


    /**
     *  Creates a <code>ValueLookupSession</code> adapter.
     *
     *  @param manager a ConfigurationProxyManager
     *  @param proxy a Proxy
     *  @throws org.osid.NullArgumentException <code>manager</code> or
     *          <code>proxy</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException
     */

    ValueLookupSession(org.osid.configuration.ConfigurationProxyManager manager, org.osid.proxy.Proxy proxy)
	throws org.osid.OperationFailedException {

	nullarg(manager, "configuration manager");
	nullarg(proxy, "proxy");

	setConfiguration(CONFIGURATION);
	selectFirst();

	for (org.osid.id.Id configurationId : getConfigurations(proxy)) {
	    try {
		addSession(manager.getValueLookupSessionForConfiguration(configurationId, proxy));
	    } catch (org.osid.NotFoundException nfe) {
		continue;
	    }
	}

	try {
	    org.osid.id.Id mainId = getMainConfiguration();
	    addSession(manager.getValueLookupSessionForConfiguration(mainId, proxy));
	} catch (org.osid.NotFoundException nfe) {}

	return;
    }


    /**
     *  Gets a value condition for the given parameter. 
     *
     *  @param  parameterId the <code> Id </code> of a <code> Parameter 
     *          </code> 
     *  @return a value condition 
     *  @throws org.osid.NullArgumentException <code> parameterId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ValueCondition getValueCondition(org.osid.id.Id parameterId) {
	return (new net.okapia.osid.jamocha.nil.configuration.valuecondition.UnValueCondition());
    }

    
    private java.util.Collection<org.osid.id.Id> getConfigurations(org.osid.proxy.Proxy proxy) {
	java.util.Collection<org.osid.id.Id> ret = new java.util.LinkedHashSet<org.osid.id.Id>();

	if (!proxy.hasRecordType(PathProxyRecord.TYPE)) {
	    return (ret);
	}

	try {
	    PathProxyRecord record = (PathProxyRecord) proxy.getProxyRecord(PathProxyRecord.TYPE);
	    for (String path : record.getPath()) {
		ret.add(new BasicId("localhost", "file", path));	      
	    }
	} catch (Exception e) {}

	return (ret);
    }


    private org.osid.id.Id getMainConfiguration() {
	return(new BasicId("localhost", "file", "osid"));
    }
}
