//
// OsidProvider.java
//
//     A JMX Bean for an OSID Provider.
//
//
// Tom Coppeto
// Okapia
// 27 June 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.jmx.impl;


public class OsidProvider
    implements net.okapia.osid.kilimanjaro.jmx.OsidProviderMXBean {

    private final java.lang.ref.WeakReference<org.osid.OsidProfile> profile;

    private final String context;
    private final OsidProvider parent;
    private final String osidPackage;
    private final String implementation;
    private final java.util.Date bootTime = new java.util.Date();

    private final java.util.List<OsidProvider> services = java.util.Collections.synchronizedList(new java.util.ArrayList<OsidProvider>());
    private final java.util.List<OsidSession> sessions  = java.util.Collections.synchronizedList(new java.util.ArrayList<OsidSession>());

    private final OsidStats stats;
    private final OsidStats cumulativeStats;

    private javax.management.ObjectName objectName;
    private javax.management.MBeanServer server;
    private final String uuid = java.util.UUID.randomUUID().toString();
    private static volatile long index = 0;


    protected OsidProvider(String context,
                           org.osid.OsidProfile profile, String osidPackage,
                           String implementation)
        throws javax.management.JMException {

        this.parent          = null;
        this.context         = context;
        this.profile         = new java.lang.ref.WeakReference<org.osid.OsidProfile>(profile);
        this.osidPackage     = osidPackage;
        this.implementation  = implementation;
        this.cumulativeStats = new OsidStats(context);
        this.stats           = new OsidStats(context, this.cumulativeStats);

        register();

        return;
    }


    protected OsidProvider(OsidProvider parent,
                           org.osid.OsidProfile profile, String osidPackage,
                           String implementation)
        throws javax.management.JMException {

        this.parent          = parent;
        this.context         = parent.getContext();
        this.profile         = new java.lang.ref.WeakReference<org.osid.OsidProfile>(profile);
        this.osidPackage     = osidPackage;
        this.implementation  = implementation;
        this.cumulativeStats = new OsidStats(context, this.parent.getCumulativeStatsCollector());
        this.stats           = new OsidStats(context, this.cumulativeStats);
        
        register();

        return;
    }

    
    private void register() 
        throws javax.management.JMException {

        this.server         = java.lang.management.ManagementFactory.getPlatformMBeanServer();
        this.objectName     = new javax.management.ObjectName("net.okapia.osid.kilimanjaro.jmx:type=OsidProvider,name=" + getContext() + "." + ++OsidProvider.index);
        this.server.registerMBean(this, this.objectName);

        return;
    }


    public static OsidProvider createRootRuntime(String context, 
                                                 org.osid.OsidProfile profile, 
                                                 String implementation) 
        throws javax.management.JMException {

        return (new OsidProvider(context, profile, "org.osid", implementation));
    }


    public OsidProvider createChildRuntime(org.osid.OsidProfile profile, 
                                           String implementation) 
        throws javax.management.JMException {

        OsidProvider service = new OsidProvider(this, profile, "org.osid", implementation);
        this.services.add(service);
        return (service);
    }


    public OsidProvider createProvider(org.osid.OsidProfile profile, 
                                       String osidPackage, String implementation) 
        throws javax.management.JMException {

        OsidProvider service = new OsidProvider(this, profile, osidPackage, implementation);
        this.services.add(service);
        return (service);
    }


    public void destroy() {
        if (this.parent != null) {
            this.parent.removeService(this);
        }

        for (OsidProvider p : this.services) {
            p.destroy();
        }

        for (OsidSession s : this.sessions) {
            s.destroy();
        }

        if (this.stats != null) {
            this.stats.destroy();
        }

        if (this.cumulativeStats != null) {
            this.cumulativeStats.destroy();
        }

        try {
            this.server.unregisterMBean(this.objectName);
        } catch (javax.management.JMException e) {
            // ?
        }
    }


    protected org.osid.OsidProfile getOsidProfile() {
        org.osid.OsidProfile profile = this.profile.get();
        if (profile == null) {
            destroy();
        }

        return (profile);
    }


    /**
     *  Gets the context.
     *
     *  @return the context of the runtime
     */
    
    @Override
    public String getContext() {
        return (this.context);
    }


    /**
     *  Gets the identifier for this bean.
     *
     *  @return the identifier
     */
    
    @Override
    public String getIdentifier() {
        return (this.uuid);
    }
        

    /**
     *  Gets the OSID Package name.
     * 
     *  @return the OSID Package name
     */

    @Override
    public String getOsidPackage() {
        return (this.osidPackage);
    }


    /**
     *  Gets the implementation class.
     * 
     *  @return the implementation class
     */

    public String getImplementationName() {
        return (this.implementation);
    }


    /**
     *  Gets the Service Id for the OSID Provider.
     *
     *  @return the service Id
     */

    @Override
    public String getId() {
        org.osid.OsidProfile profile = getOsidProfile();
        if (profile == null) {
            return ("[not accessible]");
        }

        return (profile.getId().toString());
    }


    /**
     *  Gets the display name for this provider.
     *
     *  @return the display name
     */

    @Override
    public String getName() {
        org.osid.OsidProfile profile = getOsidProfile();
        if (profile == null) {
            return ("[not accessible]");
        }

        return (profile.getDisplayName().getText());
    }


    /**
     *  Gets the description for this provider.
     *
     *  @return the description
     */

    @Override
    public String getDescription() {
        org.osid.OsidProfile profile = getOsidProfile();
        if (profile == null) {
            return ("[not accessible]");
        }
        
        return (profile.getDescription().getText());
    }


    /**
     *  Gets the version for this provider.
     *
     *  @return the version
     */

    @Override
    public String getVersion() {
        org.osid.OsidProfile profile = getOsidProfile();
        if (profile == null) {
            return ("[not accessible]");
        }
        
        return (profile.getVersion().toString());
    }


    /**
     *  Gets the release date for this provider.
     *
     *  @return the release date
     */

    @Override
    public java.util.Date getReleaseDate() {
        org.osid.OsidProfile profile = getOsidProfile();
        if (profile == null) {
            return (null);
        }

        return (makeDate(profile.getReleaseDate()));
    }


    /**
     *  Gets the boot time.
     *
     *  @return the time this provider started
     */

    @Override
    public java.util.Date getBootTime() {
        return (this.bootTime);
    }


    /**
     *  Gets the uptime in milliseconds.
     *
     *  @return the uptime
     */

    @Override
    public long getUptime() {
        return (System.currentTimeMillis() - getBootTime().getTime());
    }


    /**
     *  Gets the OSID Providers requested through this OSID Provider.
     *
     *  @return the spawned OSID Providers
     */

    @Override
    public java.util.List<net.okapia.osid.kilimanjaro.jmx.OsidProviderMXBean> getServices() {
        java.util.Collection<OsidProvider> copy = new java.util.ArrayList<>(this.services);

        for (OsidProvider provider : copy) {
            provider.checkService();
        }

        return (java.util.Collections.unmodifiableList(new java.util.ArrayList<net.okapia.osid.kilimanjaro.jmx.OsidProviderMXBean>(this.services)));
    }


    protected void checkService() {
        org.osid.OsidProfile profile = this.profile.get();
        if (profile == null) {
            destroy();
        }

        return;
    }


    protected void removeService(OsidProvider provider) {
        this.services.remove(provider);
        return;
    }


    /**
     *  Gets the active OsidSessions from this OSID Provider.
     *
     *  @return the OsidSessions
     */

    @Override
    public java.util.List<net.okapia.osid.kilimanjaro.jmx.OsidSessionMXBean> getSessions() {
        return (java.util.Collections.unmodifiableList(new java.util.ArrayList<net.okapia.osid.kilimanjaro.jmx.OsidSessionMXBean>(this.sessions)));
    }


    public OsidSession createSession(org.osid.OsidSession session, String name,
                                     String implementation, boolean proxy) 
        throws javax.management.JMException {

        OsidSession jsession = new OsidSession(this, session, name, implementation, proxy);
        this.sessions.add(jsession);
        return (jsession);
    }


    public void removeSession(OsidSession session) {
        this.sessions.remove(session);
        return;
    }


    /**
     *  Gets the stats for this provider.
     *
     *  @return the stats
     */

    @Override
    public net.okapia.osid.kilimanjaro.jmx.OsidStatsMXBean getStats() {
        return (this.stats);
    }


    /**
     *  Gets the stats for this provider and all of its services.
     *
     *  @return the stats
     */

    @Override
    public net.okapia.osid.kilimanjaro.jmx.OsidStatsMXBean getCumulativeStats() {
        return (this.cumulativeStats);
    }


    public OsidStats getStatsCollector() {
        return (this.stats);
    }


    protected OsidStats getCumulativeStatsCollector() {
        return (this.cumulativeStats);
    }


    /* slam it in there and hope for the best */
    private java.util.Date makeDate(org.osid.calendaring.DateTime date) {
        java.util.Calendar calendar = new java.util.GregorianCalendar();

        calendar.clear();
        if (date.getGranularity().ordinal() <= org.osid.calendaring.DateTimeResolution.MILLISECOND.ordinal()) {
            calendar.set(java.util.Calendar.MILLISECOND, (int) date.getMilliseconds());
        } 

        if (date.getGranularity().ordinal() <= org.osid.calendaring.DateTimeResolution.SECOND.ordinal()) {
            calendar.set((int) date.getYear(), (int) date.getMonth(), (int) date.getDay(),
                         (int) date.getHour(), (int) date.getMinute(), (int) date.getSecond());
            return (calendar.getTime());
        } 

        if (date.getGranularity().ordinal() <= org.osid.calendaring.DateTimeResolution.MINUTE.ordinal()) {
            calendar.set((int) date.getYear(), (int) date.getMonth(), (int) date.getDay(),
                         (int) date.getHour(), (int) date.getMinute());
            return (calendar.getTime());
        } 


        if (date.getGranularity().ordinal() <= org.osid.calendaring.DateTimeResolution.HOUR.ordinal()) {
            calendar.set(java.util.Calendar.HOUR, (int) date.getHour());
        } 

        if (date.getGranularity().ordinal() <= org.osid.calendaring.DateTimeResolution.DAY.ordinal()) {
            calendar.set((int) date.getYear(), (int) date.getMonth(), (int) date.getDay());
            return (calendar.getTime());
        } 

        if (date.getGranularity().ordinal() <= org.osid.calendaring.DateTimeResolution.MONTH.ordinal()) {
            calendar.set(java.util.Calendar.MONTH, (int) date.getMonth());
        }         


        if (date.getGranularity().ordinal() <= org.osid.calendaring.DateTimeResolution.YEAR.ordinal()) {
            calendar.set(java.util.Calendar.YEAR, (int) date.getYear());
        }

        return (calendar.getTime());
    }
}
