//
// OsidStats.java
//
//     A JMX Bean for an OSID Stats.
//
//
// Tom Coppeto
// Okapia
// 27 June 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.jmx.impl;


public class OsidStats
    implements net.okapia.osid.kilimanjaro.jmx.OsidStatsMXBean {
    
    private volatile long callsin;
    private volatile long callsout;
    private volatile long objectsin;
    private volatile long objectsout;
    private volatile long bytesin;
    private volatile long bytesout;
    private volatile long errorsin;
    private volatile long errorsout;
    private volatile long usererrors;
    private volatile long operrors;
    private volatile long syserrors;
    private volatile long unknownerrors;

    private final OsidStats parent;

    private final java.util.Map<String, OsidErrorStats> errors = java.util.Collections.synchronizedMap(new java.util.HashMap<String, OsidErrorStats>());

    private final javax.management.ObjectName objectName;
    private final javax.management.MBeanServer server;
    private static volatile long index = 0;

    private final String context;
    

    protected OsidStats(String context)
        throws javax.management.JMException {

        this(context, null);
        return;
    }


    public OsidStats(String context, OsidStats parent)
        throws javax.management.JMException {

        this.parent     = parent;
        this.context    = context;
        this.server     = java.lang.management.ManagementFactory.getPlatformMBeanServer();
        this.objectName = new javax.management.ObjectName("net.okapia.osid.kilimanjaro.jmx:type=OsidStats,name=" + this.context + "." + ++OsidStats.index);
        this.server.registerMBean(this, this.objectName);

        return;
    }


    public void destroy() {
        try {
            this.server.unregisterMBean(this.objectName);
        } catch (javax.management.JMException e) {
            // ?
        }
    }


    /**
     *  Gets the number of calls into the OSID Provider from OSID
     *  Consumers.
     *
     *  @return number of calls
     */

    @Override
    public long getCallsIn() {
        return (this.callsin);
    }


    public void addCallsIn() {
        addCallsIn(1);
        return;
    }


    public void addCallsIn(long count) {
        this.callsin += count;
        if (this.parent != null) {
            this.parent.addCallsIn(count);
        }

        return;
    }


    /**
     *  Gets the number of calls into OSID Consumers from OSID the
     *  Provider.
     *
     *  @return number of calls
     */

    @Override
    public long getCallsOut() {
        return (this.callsout);
    }


    public void addCallsOut() {
        addCallsOut(1);
        return;
    }


    public void addCallsOut(long count) {
        this.callsout += count;
        if (this.parent != null) {
            this.parent.addCallsOut(count);
        }

        return;
    }


    /**
     *  Gets the number of objects into the OSID Provider from OSID
     *  Consumers.
     *
     *  @return number of objects
     */

    @Override
    public long getObjectsIn() {
        return (this.objectsin);
    }


    public void addObjectsIn() {
        addObjectsIn(1);
        return;
    }


    public void addObjectsIn(long count) {
        this.objectsin += count;
        if (this.parent != null) {
            this.parent.addObjectsIn(count);
        }

        return;
    }


    /**
     *  Gets the number of objects into OSID Consumers from OSID the
     *  Provider.
     *
     *  @return number of objects
     */

    @Override
    public long getObjectsOut() {
        return (this.objectsout);
    }


    public void addObjectsOut() {
        addObjectsOut(1);
        return;
    }


    public void addObjectsOut(long count) {
        this.objectsout += count;
        if (this.parent != null) {
            this.parent.addObjectsOut(count);
        }

        return;
    }


    /**
     *  Gets the number of bytes transferred into the OSID Provider
     *  from OSID Consumers.
     *
     *  @return number of bytes transferred
     */

    @Override
    public long getBytesIn() {
        return (this.bytesin);
    }


    public void addBytesIn() {
        addBytesIn(1);
        return;
    }


    public void addBytesIn(long count) {
        this.bytesin += count;
        if (this.parent != null) {
            this.parent.addBytesIn(count);
        }

        return;
    }


    /**
     *  Gets the number of bytes transferred from the OSID Provider to
     *  OSID Consumers.
     *
     *  @return number of bytes transferred
     */

    @Override
    public long getBytesOut() {
        return (this.bytesout);
    }


    public void addBytesOut() {
        addBytesOut(1);
        return;
    }


    public void addBytesOut(long count) {
        this.bytesout += count;
        if (this.parent != null) {
            this.parent.addBytesOut(count);
        }

        return;
    }


    /**
     *  Gets the number of contract errors resulting from OSID
     *  Consumer actions.
     *
     *  @return number of contract errors
     */

    @Override
    public long getContractErrorsIn() {
        return (this.errorsin);
    }


    /**
     *  Gets the number of contract errors from OSID Provider actions.
     *
     *  @return number of contract errors
     */

    @Override
    public long getContractErrorsOut() {
        return (this.errorsout);
    }


    /**
     *  Gets the number of user errors thrown from an OSID Provider.
     *
     *  @return number of user errors
     */

    @Override
    public long getUserErrors() {
        return (this.usererrors);
    }


    /**
     *  Gets the number of non-contract errors thrown from an OSID
     *  Provider.
     *
     *  @return number of operational errors
     */

    @Override
    public long getOperationalErrors() {
        return (this.operrors);
    }


    /**
     *  Gets the number of Java system errors thrown from an OSID
     *  Provider.
     *
     *  @return number of system errors
     */

    @Override
    public long getSystemErrors() {
        return (this.syserrors);
    }


    /**
     *  Gets the number of unknown errors thrown from an OSID
     *  Provider.
     *
     *  @return number of unknown errors
     */

    @Override
    public long getUnknownErrors() {
        return (this.unknownerrors);
    }


    /**
     *  Gets a breakdown of all errors.
     *
     *  @return the list errors by type
     */

    @Override
    public java.util.List<net.okapia.osid.kilimanjaro.jmx.OsidErrorStatsMXBean> getErrors() {
        return (java.util.Collections.unmodifiableList(new java.util.ArrayList<net.okapia.osid.kilimanjaro.jmx.OsidErrorStatsMXBean>(this.errors.values())));
    }



    public void addError(Throwable error) {
        addError(error, 1);
        return;
    }


    public void addError(Throwable error, long count) {
        OsidErrorStats stats;

        synchronized (this.errors) {
            stats = this.errors.get(error);
            if (stats == null) {
                stats = new OsidErrorStats(error.getClass().getName());
                this.errors.put(error.getClass().getName(), stats);
            }
        }

        stats.addError(count);

        if (error instanceof org.osid.UserException) {
            this.usererrors++;
        } else if (error instanceof org.osid.OperationalException) {
            this.operrors++;
        } else if (error instanceof org.osid.ConsumerContractException) {
            this.errorsin++;
        } else if (error instanceof org.osid.ProviderContractException) {
            this.errorsout++;
        } else if (error instanceof java.lang.Error) {
            this.syserrors++;
        } else {
            this.unknownerrors++;
        }

        if (this.parent != null) {
            this.parent.addError(error, count);
        }

        return;
    }
}
