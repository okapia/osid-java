//
// BootLoader.java
//
//     Gets the OSID Runtime environment.
//
//
// Tom Coppeto
// OnTapSolutions
// 27 September 2008
//
// Copyright (c) 2008 Massachusetts Institute of Technology. All Rights
// Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Bootstraps the OsidRuntime environment.
 *
 *  The name of the runtime implementation is first searched for
 *  in the environment variable <code>OSID_RUNTIME_IMPL</code>. This
 *  can be set to override any default behavior of the implementation
 *  search.
 *
 *  If that variable isn't set, a Java-esque properties file called
 *  <code>osid.conf</code> is checked for the property
 *  "runtime_impl". The location of this properties file may be either
 *  be set in an environment variable, or be located in one of
 *  <code>/usr/osid</code>,
 *  <code>[~]/Library/OSID/Configuration</code> or
 *  <code>C:\OSID\config</code>. The location of this file may also be
 *  overridden through the <code>${OSID_CONFIG}</code> environment
 *  variable.
 *
 *  If the implementation name still cannot be found, 
 *  "net.okapia.osid..runtime.NativeRuntimeManager" is used.
 *
 *  @see org.osid.OsidRuntimeManager
 */

public class BootLoader {
    private String impl = "net.okapia.osid.kilimanjaro.NativeRuntimeManager";
    private boolean debug = false;


    /**
     *  Constructs a new <code>BootLoader</code>.
     *
     *  @throws org.osid.OperationFailedException a bootloader
     *          properties was found but an error was encountered
     *          reading it
     */

    public BootLoader()
        throws org.osid.OperationFailedException {

        this.loadConfiguration();
        setRuntimeImpl(this.impl);

        String env = System.getenv("OSID_RUNTIME_IMPL");
        if (env != null) {
            setRuntimeImpl(env);
            if (debug) {
                System.out.println("bootloader: using $OSID_RUNTIME_IMPL");
            }
        }

        return;
    }


    /**
     *  Sets the name of the runtime manager implementation 
     *  overriding the configuration.
     *
     *  @param impl the name of the runtime OSID implementation
     *  @throws org.osid.NullArgumentException <code>impl</code> is
     *          <code>null</code>
     */

    public void setRuntimeImpl(String impl) {
        nullarg(impl, "impl");
        this.impl = impl;
        return;
    }


    /**
     *  Instantiates and returns the OSID runtime manager. 
     *
     *  @param context application string
     *  @return the runtime manager
     *  @throws org.osid.ConfigurationErrorException problem in
     *          runtime configuration
     *  @throws org.osid.NullArgumentException <code>context</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException the runtime manager
     *          implementation could not be instantiated or initialized
     */

    public org.osid.OsidRuntimeManager getRuntimeManager(String context)
        throws org.osid.ConfigurationErrorException,
               org.osid.OperationFailedException {

        nullarg(context, "context");

        Class<? extends org.osid.OsidRuntimeManager> implClass;
        Class<? extends org.osid.OsidRuntimeManager> interfaceClass;
        
        try {
            implClass = Class.forName(this.impl).asSubclass(org.osid.OsidRuntimeManager.class);
        } catch (Throwable t) {
            throw new org.osid.ConfigurationErrorException(this.impl + " was not found", t);
        }

        try {
            interfaceClass = Class.forName("org.osid.OsidRuntimeManager").asSubclass(org.osid.OsidRuntimeManager.class);
        } catch (Throwable t) {
            throw new org.osid.ConfigurationErrorException("OSID interface package was not found", t);
        }

        if (!interfaceClass.isAssignableFrom(implClass)) {
            throw new org.osid.ConfigurationErrorException(this.impl + " does not implement org.osid.OsidRuntimeManager");
        }

        org.osid.OsidRuntimeManager manager;

        try {
            java.lang.reflect.Constructor<? extends org.osid.OsidRuntimeManager> cons = implClass.getConstructor(String.class);
            Object[] args = new String[1];
            args[0] = context;
            manager = cons.newInstance(args);
        } catch (Throwable t) {
            throw new org.osid.OperationFailedException("unable to create instance of " + this.impl, t);
        }
        
        if (manager instanceof Proxiable) {
            manager = ((Proxiable) manager).startProxy();
        }

        manager.initialize(manager);
        return (manager);
    }


    /**
     *  Gets the BootLoader properties from bootloader.properties.
     *
     *  @throws org.osid.OperationFailedException a problem processing
     *          configuration
     */

    protected void loadConfiguration()
        throws org.osid.OperationFailedException {
        
        String filename = getOsidConfigurationFile();
        if (filename == null) {
            if (debug) {
                System.out.println("bootloader: no config file found");
            }
            return;
        }

        java.io.File file = new java.io.File(filename);
        if (!file.exists() || !file.isFile()) {
            if (debug) {
                System.out.println("bootloader: no config file after it was found");
            }
            return;
        }

        this.readConfiguration(filename);
        return;
    }


    /**
     *  Reads the given configuration file.
     *
     *  @param path pathname of config file
     *  @throws org.osid.NullArgumentException <code>path</code> is 
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException a problem reading or
     *          processing config file
     */

    protected void readConfiguration(String path)
        throws org.osid.OperationFailedException {
        
        nullarg(path, "path");

        org.w3c.dom.Document doc;

        javax.xml.parsers.DocumentBuilderFactory factory = javax.xml.parsers.DocumentBuilderFactory.newInstance();
        //      factory.setNamespaceAware(true);
        factory.setIgnoringElementContentWhitespace(true);

        try {
            javax.xml.parsers.DocumentBuilder builder = factory.newDocumentBuilder();
            builder.setErrorHandler(new org.xml.sax.helpers.DefaultHandler());
            
            doc = builder.parse(new java.io.File(path));
            doc.getDocumentElement().normalize();
        } catch (Exception e) {
            throw new org.osid.OperationFailedException("cannot parse " + path, e);
        } 

        org.w3c.dom.NodeList nodes = doc.getElementsByTagName("runtime");
        if (nodes.getLength() > 0) {
            org.w3c.dom.Node node = nodes.item(0);
            if ((node.getFirstChild() != null) && 
                (node.getFirstChild().getNodeType() == org.w3c.dom.Node.TEXT_NODE)) {
                this.impl = node.getFirstChild().getNodeValue().trim();
            }
        }

        nodes = doc.getElementsByTagName("classpath");
        for (int i = 0; i < nodes.getLength(); i++) {
            org.w3c.dom.Node node = nodes.item(i);
            if ((node.getFirstChild() != null) 
                && (node.getFirstChild().getNodeType() == org.w3c.dom.Node.TEXT_NODE)) {
                addClassPath(node.getFirstChild().getNodeValue().trim());
            }       
        }
        

        if (debug) {
            System.out.println("read configuration");
        }

        return;
    }


    /**
     *  Gets the OSID config file by looking in several well-known
     *  locations and checking the <code>${OSID_CONFIG}</code>
     *  environment variable.
     *
     *  @return the pathname to the OSID configuration file
     */

    protected String getOsidConfigurationFile() {
        java.io.File file;
        String path;
        String env = null;

        java.net.URL url = getClass().getResource("osid.conf");
        if (url != null) {
            env = url.getPath();
        }

        if (env != null) {
            path = env;
            file = new java.io.File(path);
            if (file.exists() && file.isFile()) {
                return (file.getPath());
            }
        }

        env = System.getenv("OSID_HOME");
        if (env != null) {
            path = env + java.io.File.separator + "etc" + java.io.File.separator + "osid.conf";
            file = new java.io.File(path);
            if (file.exists() && file.isFile()) {
                return (file.getPath());
            }
        }

        if (debug) {
            System.out.println("bootloader: cannot find an OSID configurationfile");
        }

        return ("null");
    }


    /**
     *  Adds a class path to the system loader.
     *
     *  @param path the classpath component to add
     *  @throws org.osid.NullArgumentException <code>path</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException addition failed
     */

    public void addClassPath(String path)
        throws org.osid.OperationFailedException {

        nullarg(path, "path");

        java.net.URL url;

        try {
            url = new java.net.URL(path);
        } catch (Exception e) {
            throw new org.osid.OperationFailedException("bad path name: " + path, e);
        }
        
        java.net.URLClassLoader loader = (java.net.URLClassLoader) ClassLoader.getSystemClassLoader();
        Class<?> loaderClass = java.net.URLClassLoader.class;
        
        /* break into the system classloader */
        try {
            java.lang.reflect.Method method = loaderClass.getDeclaredMethod("addURL", java.net.URL.class);
            method.setAccessible(true);
            method.invoke(loader, new Object[] {url});
        } catch (Throwable t) {
            throw new org.osid.OperationFailedException("could not add " + path + " to system classloader");
        }

        if (debug) {
            System.out.println("added " + path);
        }

        return;
    }
}
