//
// ProxyManager.java
//
//    A proxy manager to deliver the proxy.
//
//
// Tom Coppeto
// Okapia
// 30 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.providers.configuration.rules;

import org.osid.binding.java.annotation.OSID;

import net.okapia.osid.primordium.id.BasicId;
import net.okapia.osid.primordium.calendaring.GregorianUTCDateTime;
import net.okapia.osid.primordium.installation.SoftwareVersion;
import net.okapia.osid.primordium.locale.text.eng.us.Plain;


/**
 *  A proxy to inject an OSID path from the runtime.
 */

public final class ProxyManager
    extends net.okapia.osid.jamocha.proxy.spi.AbstractProxyManager
    implements org.osid.proxy.ProxyManager {

    
    /**
     *  Constructs a new <code>ProxyManager</code>.
     */

    public ProxyManager() {
        super(new ProxyServiceProvider());
        return;
    }


    /**
     *  Tests if a proxy session is supported.
     *
     *  @return <code> true </code> if proxy is supported <code>, </code>
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean supportsProxy() {
        return (true);
    }


    /**
     *  Gets a <code> ProxySession </code> which is responsible for acquiring 
     *  authentication credentials on behalf of a service client. 
     *
     *  @return a proxy session for this service 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProxy() </code> 
     *          is <code> false </code> 
     *  @throws org.osid.IllegalStateException this manager has been shut down 
     */

    @OSID @Override
    public org.osid.proxy.ProxySession getProxySession()
        throws org.osid.OperationFailedException {
        
        return (new ProxySession());
    }    
}
