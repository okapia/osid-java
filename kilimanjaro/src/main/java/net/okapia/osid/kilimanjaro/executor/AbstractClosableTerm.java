//
// AbstractTerm.java
//
//     Validates the state of various objects based on the OSID
//     specifications. 
//
//
// Tom Coppeto
// Okapia
// 4 January 2009
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.executor;


/**
 *  Validate sthe state of various objects based on the OSID
 *  specifications. This class is only used by the BoilerPlate
 *  contract.
 */

public abstract class AbstractClosableTerm
    extends AbstractTerm
    implements FinePrint {

    private boolean closed = false;

    
    /**
     *  Constructs a new <code>AbstractTerm</code>.
     *  
     *  @param parent parent's fine print
     *  @param spec the OSID interface
     */

    protected AbstractClosableTerm(FinePrint parent, org.osid.meta.OsidInterface spec,
                                   net.okapia.osid.kilimanjaro.jmx.impl.OsidStats stats) {
        super(parent, spec, stats);
        return;
    }


    protected void approve(org.osid.meta.OsidMethod method, Object[] args) {
        if (this.closed) {
            throw new org.osid.IllegalStateException(getInterface().getName() + " is closed");
        }

        super.approve(method, args);
        return;
    }


    /**
     *  Shuts down this contract.
     */

    @Override
    public synchronized void close() {

        /*
         * the consumer may have already closed it
         */

        if (this.closed) {
            return;
        }
        
        super.close();
        this.closed = true;
        return;
    }
}

