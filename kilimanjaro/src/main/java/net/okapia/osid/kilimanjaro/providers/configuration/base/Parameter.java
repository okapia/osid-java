//
// Parameter.java
//
//     A parameter.
//
//
// Tom Coppeto
// Okapia
// 22 September 2009
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.providers.configuration.base;

import net.okapia.osid.torrefacto.xml.XMLUtil;
import net.okapia.osid.primordium.locale.text.eng.us.Plain;


/**
 *  A parameter.
 */

public final class Parameter 
    extends net.okapia.osid.jamocha.configuration.parameter.spi.AbstractParameter
    implements org.osid.configuration.Parameter {


    /**
     *  Constructs a new <code>Parameter</code>.
     *
     *  @param id the Id of the Parameter
     *  @param syntax value syntax
     *  @throws org.osid.NullArgumentException <code>id</code>,
     *          <code>displayName</code> or <code>syntax</code> is
     *          <code>null</code>
     */

    Parameter(org.osid.id.Id id, org.osid.Syntax syntax) {
        setId(id);
        setValueSyntax(syntax);
        return;
    }


    static org.osid.configuration.Parameter parse(XMLUtil xml, org.dom4j.Node node)
        throws org.osid.OperationFailedException {
        
        String sid = node.valueOf("@id");
        if (sid == null) {
            throw new org.osid.OperationFailedException("no parameter id");
        }

        org.osid.id.Id id = new net.okapia.osid.primordium.id.BasicId("local", "configuration", sid);
        org.osid.Syntax syntax = null;
        String sstr = node.valueOf("@syntax");
        for (org.osid.Syntax s : org.osid.Syntax.values()) {
            if (s.getDisplayName().equalsIgnoreCase(sstr)) {
                syntax = s;
            }
        }
        
        if (syntax == null) {
            throw new org.osid.OperationFailedException("no syntax");
        }

        Parameter parameter = new Parameter(id, syntax);

        if ("true".equalsIgnoreCase(node.valueOf("@shuffleValues"))) {
            parameter.setShuffled(true);
        } else {
            parameter.setShuffled(false);
        }

        org.dom4j.Element element = xml.getElement(node, "c:displayName", false);
        if (element != null) {
            parameter.setDisplayName(Plain.valueOf(xml.trim(element.asXML())));
        }

        element = xml.getElement(node, "c:description", false);
        if (element != null) {
            parameter.setDescription(Plain.valueOf(xml.trim(element.asXML())));
        }

        return (parameter);     
    }
}
