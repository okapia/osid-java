//
// TypeLookupSession.java
//
//     A type lookup session.
//
//
// Tom Coppeto
// Okapia
// 21 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.runtime.providers.type.xml;


/**
 *  Parses types from an XML file.
 */

public final class TypeLookupSession 
    extends net.okapia.osid.jamocha.core.type.spi.AbstractIndexedMapTypeLookupSession
    implements org.osid.type.TypeLookupSession {


    /**
     *  Constructs a new <code>TypeLookupSession</code>.
     *
     *  @param file the xml file specifying the types
     *  @throws org.osid.NotFoundException <code>file</code> not found
     *  @throws org.osid.NullArgumentException <code>file</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException cannot parse file
     */

    TypeLookupSession(String file)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        Parser parser = new Parser(file);
        for (org.osid.type.Type type : parser.getTypes()) {
            putType(type);
        }

        java.util.Map<org.osid.type.Type, org.osid.type.Type> aliases = parser.getAliases();
        for (org.osid.type.Type type : aliases.keySet()) {
            aliasType(type, aliases.get(type));
        }

        java.util.Map<org.osid.type.Type, java.util.Collection<org.osid.type.Type>> parents = parser.getParents();
        for (org.osid.type.Type type : parents.keySet()) {
            for (org.osid.type.Type parent : parents.get(type)) {
                extendType(parent, type);
            }
        }

        return;
    }
}
