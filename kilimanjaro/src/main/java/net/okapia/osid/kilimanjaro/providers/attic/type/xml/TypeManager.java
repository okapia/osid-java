//
// TypeManager.java
//
//     Implements a string-string based type.
//
//
// Tom Coppeto
// Okapia
// 21 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.runtime.providers.type.xml;

import org.osid.binding.java.annotation.OSID;
import net.okapia.osid.runtime.types.records.proxy.proxy.FileProxyRecord;

import net.okapia.osid.primordium.id.BasicId;
import net.okapia.osid.primordium.calendaring.GregorianUTCDateTime;
import net.okapia.osid.primordium.installation.SoftwareVersion;
import net.okapia.osid.primordium.locale.text.eng.us.Plain;


/**
 *  Implements a string-string type.
 */

public final class TypeManager
    extends net.okapia.osid.jamocha.type.spi.AbstractTypeManager
    implements org.osid.type.TypeManager,
               org.osid.type.TypeProxyManager {

    private static final org.osid.id.Id ID                          = BasicId.valueOf("OSID Runtime Providers:type.xml@okapia.net");
    private static final org.osid.locale.DisplayText DISPLAY_NAME   = Plain.valueOf("XML Type Provider");
    private static final org.osid.locale.DisplayText DESCRIPTION    = Plain.valueOf("A Type OSID Provider for using an XML file.");
    private static final org.osid.installation.Version VERSION      = SoftwareVersion.valueOf("0.0.1");
    private static final org.osid.calendaring.DateTime RELEASE_DATE = GregorianUTCDateTime.valueOf("2010-1-5T21:30");


    /**
     *  Constructs a new <code>TypeManager</code>.
     */

    public TypeManager() {
        super(ID);
        setMetadata(DISPLAY_NAME, DESCRIPTION, VERSION, RELEASE_DATE,
                    net.okapia.osid.provider.Providers.OKAPIA.getProvider());

        return;
    }


    /**
     *  Tests if looking up types is supported. 
     *
     *  @return <code> true </code> if type lookup is supported,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean supportsTypeLookup() {
        return (true);
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the type lookup 
     *  service. 
     *
     *  @return a <code> TypeLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTypeLookup() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.IllegalStateException this manager has been shut down 
     */

    @OSID @Override
    public org.osid.type.TypeLookupSession getTypeLookupSession()
        throws org.osid.OperationFailedException {

        try {
            return (new TypeLookupSession("types.xml"));
        } catch (org.osid.NotFoundException nfe) {
            throw new org.osid.OperationFailedException(nfe);
        }
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the type lookup 
     *  service. 
     *
     *  @return a <code> TypeLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.NullArgumentException <code>proxy</code> is
     *          <code>null</code>
     *  @throws org.osid.UnimplementedException <code> supportsTypeLookup() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.IllegalStateException this manager has been shut down 
     */

    @OSID @Override
    public org.osid.type.TypeLookupSession getTypeLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {
        
        if (!proxy.hasRecordType(FileProxyRecord.TYPE)) {
            return (getTypeLookupSession());
        }

        try {
            FileProxyRecord rec = (FileProxyRecord) proxy.getProxyRecord(FileProxyRecord.TYPE); 
            return (new TypeLookupSession(rec.getFile()));
        } catch (org.osid.OsidException oe) {
            throw new org.osid.OperationFailedException("cannot get proxy record ", oe);
        }
    }
}
