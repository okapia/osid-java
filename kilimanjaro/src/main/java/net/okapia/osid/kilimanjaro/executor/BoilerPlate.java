//
// BoilerPlate.java
//
//     Implements a Java proxy around the OSID interfaces and performs
//     some enforcement of the contracts.
//
//
// Tom Coppeto
// Okapia
// 4 January 2009
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.executor;

import static  net.okapia.osid.torrefacto.util.Sizeof.bytes;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  The OSID contract-enforcing proxy. Be sure to read the fine print.
 */

public class BoilerPlate
    implements java.lang.reflect.InvocationHandler {

    private final Object delegate;
    private final Object provider;
    private final FinePrint fp;
    private final boolean providerOwned;


    /**
     *  Constructs a new BoilerPlate contract.
     *
     *  @param delegate the object subject to the OSID contract
     *  @param fp the fine print
     *  @param providerOwned {@code true} if delegate is owned by an
     *         OSID Provider, {@code false} if owned by an OSID
     *         Consumer
     */
     
    BoilerPlate(Object delegate, Object provider, FinePrint fp, boolean providerOwned) {
        this.delegate      = delegate;
        this.fp            = fp;
        this.provider      = provider;
        this.providerOwned = providerOwned;

        return;
    }

    
    /**
     *  Invokes a method. Part of the InvocationHandler interface.
     *
     *  @param proxy the instance of the object on which the method
     *         was invoked
     *  @param method the method invoked
     *  @param args the arguments to the method
     *  @return result of the method
     *  @throws Throwable
     */

    @Override
    public Object invoke(Object proxy, java.lang.reflect.Method method, Object[] args)
        throws Throwable {

        String methodName = method.getName();
        
        try {
            if (!(proxy instanceof Contract)) {
                Exception e = new org.osid.ProviderContractException("where did you get this from?");
                addErrorCount(e);
                throw e;
            }

            addCall();
            /* back door methods */
            if (methodName.equals("violateContract")) {
                return (this.delegate);
            }

            // not sure why these are here.. they are either contract
            // methods or should be done in the respective fineprints

            if (methodName.equals("getProviderOfContract")) {
                return (this.delegate.getClass().getName());
            }

            if (methodName.equals("getSupplierOfContract")) {
                if (this.provider != null) {
                    return (this.provider.getClass().getName());
                } else {
                    return ("consumer");
                }
            }
            
            /* make sure we get the impls view of these methods */
            if (methodName.equals("hashCode")) {
                addBytesOutCount(4);
                return (method.invoke(this.delegate, args));
            }
            
            /* make sure we get the impls view of these methods */
            if (methodName.equals("equals")) {
                addBytesOutCount(1);
                return (method.invoke(this.delegate, args));
            }
            
            if (methodName.equals("toString")) {
                String ret = this.fp.processToString(this.delegate);
                addBytesOutCount(ret.length());
                return (ret);
            }

            /* primitives are serializable */
            if (proxy instanceof org.osid.OsidPrimitive) {
                if (methodName.equals("writeObject") || methodName.equals("readObject") ||
                    methodName.equals("writeReplace") || methodName.equals("readResolve") ||
                    methodName.equals("readObjectNoData")) {
                    // wrap in a proxy?
                    return (method.invoke(this.delegate, args));
                }
            }
            
            try {
                Object ret = this.fp.perform(method, this.delegate, args);
                addObjectsOutCount();
                addBytesOutCount(ret);

                if (args != null) {
                    for (Object arg : args) {
                        addObjectsInCount();
                        addBytesInCount(arg);
                    }
                }

                return (ret);                
            } catch (Exception e) {
                addErrorCount(e);
                throw e;
            }
            
        } catch (java.lang.reflect.InvocationTargetException ite) {
            throw ite.getTargetException();
        }
    }

    protected void addCall() {
        if (this.providerOwned) {
            this.fp.getStatsCollector().addCallsIn(1);
        } else {
            this.fp.getStatsCollector().addCallsOut();
        }

        return;
    }


    protected void addObjectsOutCount() {
        if (this.providerOwned) {
            this.fp.getStatsCollector().addObjectsOut();
        } else {
            this.fp.getStatsCollector().addObjectsIn();
        }

        return;
    }


    protected void addObjectsInCount() {
        if (this.providerOwned) {
            this.fp.getStatsCollector().addObjectsIn();
        } else {
            this.fp.getStatsCollector().addObjectsOut();
        }

        return;
    }

    
    protected void addBytesOutCount(int n) {
        if (this.providerOwned) {
            this.fp.getStatsCollector().addBytesOut(n);
        } else {
            this.fp.getStatsCollector().addBytesIn(n);
        }

        return;
    }


    protected void addBytesInCount(int n) {
        if (this.providerOwned) {
            this.fp.getStatsCollector().addBytesIn(n);
        } else {
            this.fp.getStatsCollector().addBytesOut(n);
        }

        return;
    }


    protected void addBytesOutCount(Object obj) {
        addBytesOutCount(bytes(obj));
        return;
    }


    protected void addBytesInCount(Object obj) {
        addBytesInCount(bytes(obj));
        return;
    }


    protected void addErrorCount(Throwable t) {
        this.fp.getStatsCollector().addError(t);
        return;
    }
}
