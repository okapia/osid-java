//
// Agent.java
//
//     A JMX Bean for an OSID Agent.
//
//
// Tom Coppeto
// Okapia
// 27 June 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.jmx.impl;


public class Agent
    implements net.okapia.osid.kilimanjaro.jmx.AgentMXBean {

    private final String id;
    private final String type;
    private final String name;
    private final String description;


    public Agent(org.osid.id.Id agentId) {
        this.id          = getId().toString();
        this.type        = "agent:unresolved@okapia.net";
        this.name        = "[" + getId().toString() + "]";
        this.description = "";

        return;
    }


    public Agent(org.osid.authentication.Agent agent) {
        this.id          = agent.getId().toString();
        this.type        = agent.getGenusType().toString();
        this.name        = agent.getDisplayName().getText();
        this.description = agent.getDescription().getText();

        return;
    }


    /**
     *  Gets an identifier for the agent.
     *
     *  @return the Agent Id
     */

    @Override
    public String getId() {
        return (this.id);
    }


    /**
     *  Gets the genus type.
     *
     *  @return the genus type
     */

    @Override
    public String getGenusType() {
        return (this.type);
    }


    /**
     *  Gets the name of the session.
     * 
     *  @return the session name
     */

    @Override
    public String getName() {
        return (this.name);
    }


    /**
     *  Gets the description.
     *
     *  @return the agent description
     */

    @Override
    public String getDescription() {
        return (this.description);
    }
}
