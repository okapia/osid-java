//
// ConfigurationManager.java
//
//     A Configuration adapter to cache configuration sessions.
//
//
// Tom Coppeto
// Okapia
// 30 March 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.providers.configuration.cache;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An OSID Configuration Adapter to translate OSID Provider names
 *  to configuration file names for use with a file-based OSID
 *  Configuration Provider.
 *
 *  All Configuration Ids are intercepted.
 */

public final class ConfigurationManager
    extends net.okapia.osid.jamocha.adapter.configuration.spi.AbstractDynamicConfigurationManager {

    private java.util.Map<org.osid.id.Id, org.osid.configuration.ValueLookupSession> cache = new java.util.HashMap<org.osid.id.Id, org.osid.configuration.ValueLookupSession>();
    private org.osid.configuration.ValueLookupSession defaultSession;


    /**
     *  Constructs a new <code>ConfigurationManager</code>.
     */

    public ConfigurationManager() {
        super(new ServiceProvider());
        return;
    }

    
    /**
     *  Initializes this manager. A manager is initialized once at the time of 
     *  creation. 
     *
     *  @param  runtime the runtime environment 
     *  @throws org.osid.ConfigurationErrorException an error with 
     *          implementation configuration 
     *  @throws org.osid.IllegalStateException this manager has already been 
     *          initialized by the <code> OsidLoader </code>
     *  @throws org.osid.NullArgumentException <code> runtime </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public void initialize(org.osid.OsidRuntimeManager runtime)
        throws org.osid.ConfigurationErrorException,
               org.osid.OperationFailedException {

        super.initialize(runtime);

        try {
            super.startProxy("net.okapia.osid.kilimanjaro.providers.configuration.rules.ConfigurationProxyManager");
        } catch (Exception e) {
            throw new org.osid.OperationFailedException(e);
        }

        return;
    }


    /**
     *  Invokes a method. Part of the InvocationHandler interface.
     *  This method uses the manager identity methods of this adapter
     *  including Id, displayName, description, version, release date,
     *  license and provider. This behavior may be used by calling
     *  this method or replaced by overriding it completely.
     *
     *  @param proxy the instance of the object on which the method
     *         was invoked
     *  @param method the method invoked
     *  @param args the arguments to the method
     *  @return result of the method
     *  @throws Throwable
     */

    @Override
    public Object invoke(Object proxy, java.lang.reflect.Method method, Object[] args)
        throws Throwable {

        if (method.getName().equals("close")) {
            close();
            return (null);
        }

        
        if (method.getName().equals("getValueLookupSession")) {
            if (this.defaultSession != null) {
                return (this.defaultSession);
            }
        }

        if (method.getName().equals("getValueLookupSessionForConfiguration")) {
            org.osid.id.Id configurationId = (org.osid.id.Id) args[0];
            org.osid.configuration.ValueLookupSession session = this.cache.get(configurationId);
            if (session != null) {
                return (session);
            }
        }

        Object ret = super.invoke(proxy, method, args);

        if (method.getName().equals("getValueLookupSession")) {
            this.defaultSession = (org.osid.configuration.ValueLookupSession) ret;
            ret = java.lang.reflect.Proxy.newProxyInstance(ret.getClass().getClassLoader(),
                                                           ret.getClass().getInterfaces(),
                                                           new SessionAdapter(this.defaultSession));
        }

        if (method.getName().equals("getValueLookupSessionForConfiguration")) {
            org.osid.id.Id configurationId = (org.osid.id.Id) args[0];
            this.cache.put(configurationId, (org.osid.configuration.ValueLookupSession) ret);
            ret = java.lang.reflect.Proxy.newProxyInstance(ret.getClass().getClassLoader(),
                                                           ret.getClass().getInterfaces(),
                                                           new SessionAdapter((org.osid.configuration.ValueLookupSession) ret));
        }

        return (ret);
    }


    /**
     *  Closes this <code>osid.OsidManager</code>
     *
     *  @throws org.osid.IllegalStateException This manager has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        for (org.osid.configuration.ValueLookupSession session : this.cache.values()) {
            session.close();
        }

        if (this.defaultSession != null) {
            this.defaultSession.close();
        }

        super.close();
        return;
    }
}
