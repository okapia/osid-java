//
// WeekdayTerm.java
//
//     A rule Term for a weekday.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.providers.rules.time;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A rule term for a weekday.
 */

public class WeekdayTerm
    extends net.okapia.osid.jamocha.assembly.rules.evaluation.spi.AbstractTerm
    implements net.okapia.osid.jamocha.assembly.rules.evaluation.Term {

    private final int weekday;
    private final boolean negate;
    

    public WeekdayTerm(String weekday, boolean negate) { 
        this(parseWeekday(weekday), negate);
        return;
    }


    public WeekdayTerm(int weekday, boolean negate) {
        this.weekday = weekday;
        this.negate  = negate;
        return;
    }


    public boolean eval(org.osid.rules.Condition condition) {
        java.util.Calendar cal = java.util.Calendar.getInstance(java.util.TimeZone.getTimeZone("GMT"));
        return (eval(cal.get(java.util.Calendar.DAY_OF_WEEK)));
    }


    protected boolean eval(int day) {
        boolean result;

        if (day == this.weekday) {
            result = true;
        } else {
            result = false;
        }

        if (negate) {
            return (!result);
        } else {
            return (result);
        }
    }

    
    protected static int parseWeekday(String day)
        throws org.osid.InvalidArgumentException {

        nullarg(day, "day");

        if (day.equalsIgnoreCase("Monday") || day.equalsIgnoreCase("Mon") || day.equalsIgnoreCase("M")) {
            return (java.util.Calendar.MONDAY);
        }

        if (day.equalsIgnoreCase("Tuesday") || day.equalsIgnoreCase("Tue") || day.equalsIgnoreCase("T")) {
            return (java.util.Calendar.TUESDAY);
        }

        if (day.equalsIgnoreCase("Wednesday") || day.equalsIgnoreCase("Wed") || day.equalsIgnoreCase("W")) {
            return (java.util.Calendar.WEDNESDAY);
        }

        if (day.equalsIgnoreCase("Thursday") || day.equalsIgnoreCase("Thu") || day.equalsIgnoreCase("R")) {
            return (java.util.Calendar.THURSDAY);
        }

        if (day.equalsIgnoreCase("Friday") || day.equalsIgnoreCase("Fri") || day.equalsIgnoreCase("F")) {
            return (java.util.Calendar.FRIDAY);
        }

        if (day.equalsIgnoreCase("Saturday") || day.equalsIgnoreCase("Sat") || day.equalsIgnoreCase("A")) {
            return (java.util.Calendar.SATURDAY);
        }

        if (day.equalsIgnoreCase("Sunday") || day.equalsIgnoreCase("Sun") || day.equalsIgnoreCase("S")) {
            return (java.util.Calendar.SUNDAY);
        }

        throw new org.osid.InvalidArgumentException(day + " not known");
    }
}
