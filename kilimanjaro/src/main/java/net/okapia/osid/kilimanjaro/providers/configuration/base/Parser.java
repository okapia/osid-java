//
// Parser.java
//
//     Parses an XML based configuration file.
//
//
// Tom Coppeto
// Okapia
// 22 September 2009
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.providers.configuration.base;

import net.okapia.osid.primordium.id.PathId;
import net.okapia.osid.primordium.locale.text.eng.us.Plain;
import net.okapia.osid.torrefacto.xml.XMLUtil;


/**
 *  Parses an XML based configuration file.
 */


public final class Parser {

    private final java.util.Map<org.osid.id.Id, org.osid.configuration.Parameter> parameters = new java.util.HashMap<>();
    private final java.util.Collection<org.osid.configuration.Value> values = new java.util.ArrayList<>();
        
    private org.osid.configuration.Configuration configuration;


    /**
     *  Constructs a new parser for configuration files.
     *
     *  @param  configurationId path to configuration file
     *  @throws org.osid.NotFoundException file not found
     *  @throws org.osid.NullArgumentException
     *          <code>configurationId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException error reading file
     */

    public Parser(org.osid.id.Id configurationId) 
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        String file = getFileFromId(configurationId) + ".xml";

        java.net.URL url = getClass().getResource(file);
        if (url == null) {
            url = Thread.currentThread().getContextClassLoader().getResource(file);
        }
        
        if (url != null) {
            file = url.getPath();
        } 

        XMLUtil xml = new XMLUtil(file);
        xml.addNamespace("c", "urn:inet:osid.org:schemas:providers/configuration/base/1");
        
        org.dom4j.Element root = xml.getElement("/c:configuration", true);
        String name = file;
        String description = "A configuration file.";

        org.dom4j.Element element = xml.getElement(root, "c:displayName", false);
        if (element != null) {
            name = xml.trim(element.asXML());
        }

        element = xml.getElement(root, "c:description", false);
        if (element != null) {
            description = xml.trim(element.asXML());
        }

        boolean registry = false;
        if ("true".equalsIgnoreCase(root.valueOf("@registry"))) {
            registry = true;
        }

        this.configuration = new net.okapia.osid.jamocha.builder.configuration.configuration.ConfigurationBuilder()
            .id(new PathId(file))
            .displayName(Plain.valueOf(name))
            .description(Plain.valueOf(description))
            .registry(registry)
            .provider(net.okapia.osid.provider.Providers.OKAPIA.getProvider())
            .build();

        for (org.dom4j.Node np : xml.getNodes(root, "c:parameter")) {
            org.osid.configuration.Parameter parameter = Parameter.parse(xml, np);
            addParameter(parameter);

            int i = 0;
            for (org.dom4j.Node nv : xml.getNodes(np, "c:value")) {
                org.osid.configuration.Value value = Value.parse(xml, parameter, ++i, nv);
                addValue(value);
            }
        }

        return;
    }


    /**
     *  Gets the configuration object.
     *
     *  @return the configuration
     */

    public org.osid.configuration.Configuration getConfiguration() {
        return (this.configuration);
    }

    
    /**
     *  Gets the list of parsed parameters.
     *
     *  @return parameters
     */

    public java.util.Collection<org.osid.configuration.Parameter> getParameters() {
        return (this.parameters.values());
    }


    /**
     *  Gets the list of parsed values.
     *
     *  @return values
     */

    public java.util.Collection<org.osid.configuration.Value> getValues() {
        return (this.values);
    }

    
    private void addParameter(org.osid.configuration.Parameter parameter) {
        this.parameters.put(parameter.getId(), parameter);
    }


    private void addValue(org.osid.configuration.Value value) {
        this.values.add(value);
        return;
    }


    private static String getFileFromId(org.osid.id.Id id)
        throws org.osid.NotFoundException {

        if (!id.getIdentifierNamespace().equals("path") || !id.getAuthority().equals("localhost")) {
            throw new org.osid.NotFoundException("identifier not a local path");
        }

        return (id.getIdentifier());
    }
}
