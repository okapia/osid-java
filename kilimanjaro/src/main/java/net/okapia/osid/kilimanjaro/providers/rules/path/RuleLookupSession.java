//
// RuleLookupSession.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.providers.rules.path;

import net.okapia.osid.primordium.id.BasicId;
import net.okapia.osid.primordium.locale.text.eng.us.Plain;


/**
 *  This session provides methods for retrieving <code> Rule </code>
 *  objects. The <code> Rule </code> represents a collection of rule
 *  entries.
 *  
 *  This session defines views that offer differing behaviors when
 *  retrieving multiple objects.
 *  
 *  <ul>
 *      <li> comparative view: elements may be silently omitted or re-ordered 
 *      </li> 
 *      <li> plenary view: provides a complete set or is an error condition 
 *      </li> 
 *  </ul>
 *
 *  Generally, the comparative view should be used for most
 *  applications as it permits operation even if there is data that
 *  cannot be accessed.. For example, a browsing application may only
 *  need to examine the <code> Rules </code> it can access, without
 *  breaking execution. However, an assessment may only be useful if
 *  all <code> Rules </code> referenced by it are available, and a
 *  test-taking applicationmay sacrifice some interoperability for the
 *  sake of precision.
 */

public final class RuleLookupSession
    extends net.okapia.osid.jamocha.core.rules.spi.AbstractIndexedMapRuleLookupSession
    implements org.osid.rules.RuleLookupSession,
               org.osid.rules.RulesSession {

    private Parser parser;
    private final static org.osid.rules.Rule RULE = new net.okapia.osid.jamocha.builder.rules.rule.RuleBuilder()
        .id(BasicId.valueOf("osid.rules.Rule:path@okapia.net"))
        .displayName(Plain.valueOf("Path Matching Rule"))
        .description(Plain.valueOf("A rule for matching path structures."))
        .build();


    /**
     *  Constructs a new <code>RuleLookupSession</code>.
     *
     *  @param engineId
     *  @throws org.osid.NotFoundException
     *  @throws org.osid.NullArgumentException <code>engineId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException
     */
  
    protected RuleLookupSession(org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        putRule(RULE);

        this.parser = new Parser(engineId, RULE.getId());
        setEngine(this.parser.getEngine());

        return;
    }


    /**
     *  Tests if this user can evaluate rules. A return of true does not
     *  guarantee successful authorization. A return of false indicates that
     *  it is known all methods in this session will result in a <code>
     *  PERMISSION_DENIED. </code> This is intended as a hint to an
     *  application that may opt not to offer these operations.
     *
     *  @return <code> false </code> if evaluation methods are not authorized,
     *          <code> true </code> otherwise
     *  @throws org.osid.IllegalStateException this session has been closed
     */

    public boolean canEvaluateRules() {
        return (true);
    }


    /**
     *  Gets a condition for the given rule.
     *
     *  @param  ruleId the <code> Id </code> of a <code> Rule </code>
     *  @return the returned <code> Condition </code>
     *  @throws org.osid.NotFoundException no <code> Rule </code> found with
     *          the given <code> Id </code>
     *  @throws org.osid.NullArgumentException <code> ruleId </code> is <code>
     *          null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     *  @throws org.osid.IllegalStateException this session has been closed
     */

    public org.osid.rules.Condition getConditionForRule(org.osid.id.Id ruleId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new Condition());
    }


    /**
     *  Evaluates a rule based on an input condition.
     *
     *  @param  ruleId a rule <code> Id </code>
     *  @param  condition input conditions
     *  @return result of the evaluation
     *  @throws org.osid.NotFoundException an <code> Id was </code> not found
     *  @throws org.osid.NullArgumentException <code> ruleId </code> or <code>
     *          condition </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     *  @throws org.osid.UnsupportedException <code> condition </code> not of
     *          this service
     *  @throws org.osid.IllegalStateException this session has been closed
     */

    public boolean evaluateRule(org.osid.id.Id ruleId, org.osid.rules.Condition condition)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.rules.Result result = executeRule(ruleId, condition);
        return (result.getBooleanValue());
    }


    /**
     *  Executes a rule based on an input condition and returns a result.
     *
     *  @param  ruleId a rule <code> Id </code>
     *  @param  condition input conditions
     *  @return result of the execution
     *  @throws org.osid.NotFoundException an <code> Id was </code> not found
     *  @throws org.osid.NullArgumentException <code> ruleId </code> or <code>
     *          condition </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     *  @throws org.osid.UnsupportedException <code> condition </code> not of
     *          this service
     *  @throws org.osid.IllegalStateException this session has been closed
     */

    public org.osid.rules.Result executeRule(org.osid.id.Id ruleId,
                                             org.osid.rules.Condition condition)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (RULE.getId().equals(ruleId)) {
            return (executePathRule(condition));
        }

        throw new org.osid.NotFoundException(ruleId + " not found");
    }


    private org.osid.rules.Result executePathRule(org.osid.rules.Condition condition)
        throws org.osid.OperationFailedException {

        Object obj = ((net.okapia.osid.kilimanjaro.executor.Contract) condition).violateContract();
        if (!(obj instanceof Condition)) {
            throw new org.osid.UnsupportedException("condition not of this service");
        }

        Condition c =  (Condition) obj;
        String[] path = c.getPath();
        java.util.Map<org.osid.configuration.Value,Integer> map = new java.util.LinkedHashMap<org.osid.configuration.Value,Integer>();
        
        org.osid.configuration.ValueList values = c.getValues();
        while (values.hasNext()) {
            org.osid.configuration.Value value = values.getNextValue();
            int match = matchPath(path, this.parser.getPath(value.getId()));
            if (match >= 0) {
                Integer stored = map.get(value);
                if ((stored != null) && (stored > match)) {
                    continue;
                }

                map.put(value, match);
            }
        }

        return (new Result(map.keySet()));
    }

    

    private int matchPath(String[] stack, java.util.Collection<String> value) {
        if ((stack == null) || (value == null)) {
            return (0);
        }

        return (matchPath(stack, value.toArray(new String[0])));
    }


    private int matchPath(String[] stack, String[] value) {
        int j = 0;
        int matches = 1;

        if (value.length == 0) {
            return (-1);
        }

        for (int i = 0; i < stack.length; i++) {
            if (stack[i].equals(value[j])) {
                ++matches;
                if (++j >= value.length) {
                    break;
                }
            } else if (!value[j].equals("*")) {
                return (-1);
            } else if (j < (value.length - 1)) {
                if (stack[i].equals(value[j+1])) {
                    ++j;
                }
            }
        }

        if (j < value.length) {
            if (!(value[j].equals("*") && (j == (value.length - 1)))) {
                return (-1);
            }
        }

        return (matches);
    }
}
