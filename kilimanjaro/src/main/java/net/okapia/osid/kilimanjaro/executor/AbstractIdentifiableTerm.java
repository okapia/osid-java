//
// AbstractIdentifiableTerm.java
//
//     Validates the state of various objects based on the OSID
//     specifications.
//
//
// Tom Coppeto
// Okapia
// 4 January 2009
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.executor;


/**
 *  Validate sthe state of various objects based on the OSID
 *  specifications. This class is only used by the BoilerPlate
 *  contract.
 */

public abstract class AbstractIdentifiableTerm
    extends AbstractTerm
    implements FinePrint {

    private boolean initialized = false;


    /**
     *  Constructs a new <code>AbstractIdentifiableTerm</code>.
     *  
     *  @param parent
     *  @param spec interface specification
     */

    protected AbstractIdentifiableTerm(FinePrint parent, org.osid.meta.OsidInterface spec,
                                       net.okapia.osid.kilimanjaro.jmx.impl.OsidStats stats) {
        super(parent, spec, stats);
        return;
    }


    /**
     *  Handle the toString() method.
     *  
     *  @param delegate
     *  @return string
     */

    @Override
    public String processToString(Object delegate) {
        String s = delegate.toString();

        if (s.startsWith(delegate.getClass().getName())) {
            org.osid.Identifiable obj = (org.osid.Identifiable) delegate;
            return ("[" + getClassName() + ":" + obj.getId() + "]");
        } else {
            return (s);
        }
    } 
}
