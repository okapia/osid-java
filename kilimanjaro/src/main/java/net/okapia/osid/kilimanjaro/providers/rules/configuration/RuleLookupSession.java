//
// RuleLookupSession.java
//
//     A lookup session for this one rule.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.providers.rules.configuration;

import net.okapia.osid.primordium.id.BasicId;
import net.okapia.osid.primordium.locale.text.eng.us.Plain;


/**
 *  Delivers a rule.
 */

public class RuleLookupSession
    extends net.okapia.osid.jamocha.core.rules.spi.AbstractIndexedMapRuleLookupSession
    implements org.osid.rules.RuleLookupSession {

    protected static final org.osid.rules.Rule RULE = (new net.okapia.osid.jamocha.builder.rules.rule.RuleBuilder())
        .id(BasicId.valueOf("org.osid.rules.Rule:runtime.configuration@okapia.net"))
        .displayName(Plain.valueOf("Configuration Rule"))
        .description(Plain.valueOf("A rule to select configuration values."))
        .build();

    protected static final org.osid.rules.Engine ENGINE = new net.okapia.osid.jamocha.builder.rules.engine.EngineBuilder()
        .id(BasicId.valueOf("org.osid.rules.Engine:rules.configuration@okapia.net"))
        .displayName(Plain.valueOf("Default Engine"))
        .description(Plain.valueOf("A default Engine for the configuration rules."))
        .provider(net.okapia.osid.provider.Providers.OKAPIA.getProvider())
        .build();


    /**
     *  Constructs a new <code>RuleLookupSession</code>.
     */
  
    protected RuleLookupSession() {
        setEngine(ENGINE);
        putRule(RULE);

        return;
    }
}
