//
// Config.java
//
//     Dumps a configuration.
//
//
// Tom Coppeto
// 21 October 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.tools;

import org.osid.*;
import org.osid.type.Type;
import org.osid.configuration.*;

import net.okapia.osid.kilimanjaro.BootLoader;
import net.okapia.osid.kilimanjaro.OsidVersions;


public class Config {
    private org.osid.configuration.ConfigurationManager manager;
    private org.osid.OsidRuntimeManager runtime;


    public Config(String context)
        throws Exception {

        BootLoader loader = new BootLoader();
        this.runtime = loader.getRuntimeManager(context);
        return;
    }   


    public Config(String context, String impl)
        throws Exception {

        BootLoader loader = new BootLoader();
        this.runtime = loader.getRuntimeManager("Config");
        this.manager = (ConfigurationManager) runtime.getManager(OSID.CONFIGURATION, impl, 
                                                                 OsidVersions.V3_0_0.getVersion());
        return;
    }   


    public void dump(String id) 
        throws Exception {

        ValueLookupSession session = this.manager.getValueLookupSessionForConfiguration(net.okapia.osid.primordium.id.BasicId.valueOf(id));
        session.useConditionalView();

        try (ValueList values = session.getValues()) {
            while (values.hasNext()) {
                Value value = values.getNextValue();
                dumpValue(value);
            }
        }

        session.close();
        return; 
    }


    public void dump() 
        throws Exception {

        ValueLookupSession session = this.runtime.getConfiguration();
        session = this.runtime.getConfiguration();
        session = this.runtime.getConfiguration();

        try (ValueList values = session.getValues()) {
            while (values.hasNext()) {
                Value value = values.getNextValue();
                dumpValue(value);
            }
        }

        session.close();
        return; 
    }


    private void dumpValue(org.osid.configuration.Value value)
        throws org.osid.OperationFailedException {

        System.out.print(value.getParameter().getDisplayName() + " = (" + 
                         value.getParameter().getValueSyntax() + ") ");
        
        switch (value.getParameter().getValueSyntax()) {
        case BOOLEAN:
            System.out.println(value.getBooleanValue());
            break;
        case BYTE:
            System.out.println(value.getBytesValue());
            break;
        case CARDINAL:
            System.out.println(value.getCardinalValue());
            break;
        case COORDINATE:
            System.out.println(value.getCoordinateValue());
            break;
        case DATETIME:
            System.out.println(value.getDateTimeValue());
            break;
        case DECIMAL:
            System.out.println(value.getDecimalValue());
            break;
        case ID:
            System.out.println(value.getIdValue());
            break;
        case INTEGER:
            System.out.println(value.getIntegerValue());
            break;
        case OBJECT:
            System.out.println(value.getParameter().getValueObjectType() + ":" + value.getObjectValue());
            break;
        case STRING:
            System.out.println(value.getStringValue());
            break;
        case TYPE:
            System.out.println(value.getTypeValue());
            break;
        default:
            System.out.println("unknown");
            break;
        }

        return;
    }


    public static void main(String[] args)
        throws Exception {

        String implName    = null;
        boolean osid       = false;
        String configName  = null;
        boolean config     = false;
        boolean runtime    = false;
        boolean context    = false;
        String contextName = null;


        for (String arg : args) {
            if (arg.equals("-osid")) {
                osid = true;
                continue;
            }

            if (arg.equals("-config")) {
                config = true;
                continue;
            }

            if (arg.equals("-context")) {
                context = true;
                continue;
            }

            if (arg.equals("-r") || arg.equals("-runtime")) {
                runtime = true;
                continue;
            }

            if (osid && (implName == null)) {
                implName = arg;
                continue;
            }

            if (config && (configName == null)) {
                configName = arg;
                continue;
            }

            if (context && (contextName == null)) {
                contextName = arg;
                continue;
            }

            System.err.println("usage: Config [-osid <impl>] [-context <context>] [-config <id>] [-runtime]");
            System.exit(1);
        }

        if (contextName == null) {
            contextName = "Config";
        }

        if ((configName == null) && (implName == null) && !runtime) {
            System.err.println("usage: Config [-osid <impl>] [-context <context>] [-config <id>] [-runtime]");
            System.exit(1);
        }
            
        if (runtime) {
            Config configuration = new Config(contextName);
            configuration.dump();
        } else {
            Config configuration = new Config(contextName, implName);
            configuration.dump(configName);
        }

        return;
    }
}

