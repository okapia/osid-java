//
// ProxySession.java
//
//    A proxy session to deliver the proxy.
//
//
// Tom Coppeto
// Okapia
// 30 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.providers.configuration.rules;

import org.osid.binding.java.annotation.OSID;
import net.okapia.osid.kilimanjaro.types.records.proxy.proxycondition.PathProxyConditionRecord;


/**
 *  A proxy to inject an OSID path from the runtime.
 */

public final class ProxySession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.proxy.ProxySession {


    /**
     *  Gets a proxy condition interface for acquiring a proxy. A new proxy 
     *  condition should be acquired for each proxy request. 
     *
     *  @return a proxy condiiton interface 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.proxy.ProxyCondition getProxyCondition() {
        return (new ProxyCondition());
    }


    /**
     *  Gets a proxy. 
     *
     *  @param  input a proxy specifier 
     *  @return a proxy interface 
     *  @throws org.osid.NullArgumentException <code> input </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException <code> input </code> not of this 
     *          service 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.proxy.Proxy getProxy(org.osid.proxy.ProxyCondition input)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
                
        if (!(input instanceof ProxyCondition)) {
            throw new org.osid.UnsupportedException("input condition not of this service");
        }

        return (new Proxy(((ProxyCondition) input).getPath()));
    }
}
