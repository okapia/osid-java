//
// AuthorizationManager
//
//     An authorization service stub that returns yes.
//
//
// Tom Coppeto
// OnTapSolutions
// 4 January 2010
//
// Copyright (c) 2010 Massachusetts Institute of Technology. All Rights
// Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package org.osid.runtime.providers.authorization.yes;

import org.osid.binding.java.annotation.OSID;


/**
 *  An OSID Authorization Provider for the OSID Runtime. 
 */

public final class AuthorizationManager
    extends org.osid.impl.authorization.AbstractAuthorizationManager
    implements org.osid.authorization.AuthorizationManager,
	       org.osid.authorization.AuthorizationProxyManager {

    private static final org.osid.id.Id ID                          = BasicId.valueOf("OSID Providers:authorization.yes@okapia.net");
    private static final org.osid.locale.DisplayText DISPLAY_NAME   = Plain.valueOf("Yes Authorization Provider");
    private static final org.osid.locale.DisplayText DESCRIPTION    = Plain.valueOf("An OSID Authorization Provider stub that performs no authorizations.");
    private static final org.osid.installation.Version IMPL_VERSION = SoftwareVersion.valueOf("0.1.0");
    private static final org.osid.calendaring.DateTime RELEASE_DATE = GregorianUTCDateTime.valueOf("210-1-4T12:30");

    private static final org.osid.authorization.Vault VAULT = new net.okapia.osid.jamocha.builder.authorization.VaultBuilder()
        BasicId.valueOf("osid.authorization.Vault:yes@okapia.net")
	.displayName(Plain.valueOf("Yes Vault"))
        .description(Plain.valueOf("Vault for the Yes authorization provider."))
	.setProvider(org.osid.impl.provider.Providers.Okapia.getProvider())
	.build();


    /**
     *  Constructs a new <code>AuthorizationManager</code>.
     */

    public AuthorizationManager() {
	super(ID);
	setMetadata(DISPLAY_NAME, DESCRIPTION, VERSION, RELEASE_DATE, 
		    org.osid.impl.provider.Providers.Okapia.getProvider());
	return;
    }


    /**
     *  Tests for the availability of an authorization service which is the
     *  basic service for checking authorizations.
     *
     *  @return <code> true </code> if authorization is supported, <code>
     *          false </code> otherwise
     */

    @OSID @Override
    public boolean supportsAuthorization() {
        return (true);
    }


    /**
     *  Gets an <code> AuthorizationSession </code> which is responsible for
     *  performing authorization checks.
     *
     *  @return an authorization session for this service
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.UnimplementedException <code> supportsAuthorization()
     *          </code> is <code> false </code>
     *  @throws org.osid.IllegalStateException this manager has been shut down
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationSession getAuthorizationSession()
        throws org.osid.OperationFailedException {

	return (new AuthorizationSession(VAULT));
    }
}
