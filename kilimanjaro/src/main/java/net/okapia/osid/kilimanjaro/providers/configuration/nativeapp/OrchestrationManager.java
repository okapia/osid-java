//
// OrchestrationManager.java
//
//    An orchestration manager for the Proxy and Configuration
//    services.
//
//
// Tom Coppeto
// Okapia
// 30 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.providers.configuration.nativeapp;

import org.osid.binding.java.annotation.OSID;


/**
 *  Orchestrates the Proxy and Configuration services.
 */

public final class OrchestrationManager
    extends net.okapia.osid.jamocha.orchestration.spi.AbstractOrchestrationManager
    implements org.osid.orchestration.OrchestrationManager {

    
    /**
     *  Constructs a new <code>OrchestrationManager</code>.
     */

    public OrchestrationManager() {
        super(new OrchestrationServiceProvider());
        return;
    }


    /**
     *  Tests if a configuration provider is supported.
     *
     *  @return <code> true </code> if a configuration provider is available,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean supportsConfigurationProvider() {
        return (true);
    }


    /**
     * Tests if a proxy provider is supported.
     *
     *  @return <code> true </code> if a proxy provider is available, <code>
     *          false </code> otherwise
     */

    @OSID @Override
    public boolean supportsProxyProvider() {
        return (true);
    }


    /**
     *  Gets the manager associated with the Configuration service.
     *
     *  @return the manager
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.UnimplementedException <code>
     *          supportsConfigurationProvider() </code> is <code> false
     *         </code>
     *  @throws org.osid.IllegalStateException this manager has been shut down
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationProxyManager getConfigurationProxyManager()
        throws org.osid.OperationFailedException {

        try {
            return ((org.osid.configuration.ConfigurationProxyManager) getOsidProxyManager(org.osid.OSID.CONFIGURATION,                                                                                           
                                                                                           "net.okapia.osid.kilimanjaro.providers.configuration.nativeapp.ConfigurationProxyManager"));
                    } catch (org.osid.ConfigurationErrorException cee) {
            throw new org.osid.OperationFailedException("problem configuring configuration manager", cee);
        }        
    }   


    /**
     *  Gets the manager associated with the Proxy service.
     *
     *  @return the manager
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.UnimplementedException <code> supportsProxyProvider()
     *          </code> is <code> false </code>
     *  @throws org.osid.IllegalStateException this manager has been shut down
     */

    @OSID @Override
    public org.osid.proxy.ProxyManager getProxyManager()
        throws org.osid.OperationFailedException {

        try {
            return ((org.osid.proxy.ProxyManager) getOsidManager(org.osid.OSID.PROXY,
                                                                 "net.okapia.osid.kilimanjaro.providers.configuration.rules.ProxyManager"));
        } catch (org.osid.ConfigurationErrorException cee) {
            throw new org.osid.OperationFailedException("problem configuring proxy manager", cee);
        }
    }
}
