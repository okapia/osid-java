//
// RuntimeTerm.java
//
//     Validates the state of various objects based on the OSID
//     specifications.
//
//
// Tom Coppeto
// Okapia
// 4 January 2009
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.executor;


/**
 *  Validates the state of various objects based on the OSID
 *  specifications. This class is only used by the BoilerPlate
 *  contract.
 */

public final class RuntimeTerm
    extends ManagerTerm
    implements FinePrint {


    /**
     *  Constructs a new <code>RuntimeTerm</code>.
     *  
     *  @param spec interface specification
     */

    protected RuntimeTerm(org.osid.meta.OsidInterface spec,
                          net.okapia.osid.kilimanjaro.jmx.impl.OsidProvider jmx) {
        super(null, spec, jmx);
        return;
    }


    /**
     *  Constructs a new <code>RuntimeTerm</code>.
     *  
     *  @param parent
     *  @param spec interface specification
     */

    protected RuntimeTerm(FinePrint parent, org.osid.meta.OsidInterface spec,
                          net.okapia.osid.kilimanjaro.jmx.impl.OsidProvider jmx) {
        super(parent, spec, jmx);
        return;
    }


    /**
     *  Creates a fine print term.
     *
     *  @param parent the parent's fine print
     *  @param spec interface specification
     *  @param object the delegate object
     *  @param context the context of the runtime
     *  @return new term
     *  @throws org.osid.OsidRuntimeException interfaceName not defined
     */

    protected static FinePrint createTerm(FinePrint parent, org.osid.meta.OsidInterface spec, Object object, String context) {

        /* the manager came from another runtime */
        if (!(parent instanceof ManagerTerm)) {
            throw new org.osid.OsidRuntimeException("runtime not created from a manager");
        }        

        if (!(object instanceof org.osid.OsidRuntimeManager)) {
            throw new org.osid.OsidRuntimeException("object not a Runtime");
        }        

        org.osid.OsidProfile profile = (org.osid.OsidProfile) object;

        if (context.equals("")) {
            throw new RuntimeException("runtime term no context");
        }

        net.okapia.osid.kilimanjaro.jmx.impl.OsidProvider jruntime;

        try {
            jruntime = ((ManagerTerm) parent).getJMXProvider().createChildRuntime(profile, profile.getClass().getName());
        } catch (javax.management.JMException jme) {
            throw new org.osid.OsidRuntimeException(jme);
        }

        return (new RuntimeTerm(parent, spec, jruntime));
    }


    /**
     *  Creates a fine print term for parent runtime.
     *
     *  @param spec interface specification
     *  @param object the delegate object
     *  @param context the context of the runtime
     *  @return new term
     *  @throws org.osid.OsidRuntimeException interfaceName not defined
     */

    protected static FinePrint createRoot(org.osid.meta.OsidInterface spec, Object object, String context) {
        if (!(object instanceof org.osid.OsidRuntimeManager)) {
            throw new org.osid.OsidRuntimeException("object not a Runtime");
        }        

        org.osid.OsidProfile profile = (org.osid.OsidProfile) object;

        net.okapia.osid.kilimanjaro.jmx.impl.OsidProvider jruntime;

        try {
            jruntime = net.okapia.osid.kilimanjaro.jmx.impl.OsidProvider.createRootRuntime(context, profile, profile.getClass().getName());
        } catch (javax.management.JMException jme) {
            throw new org.osid.OsidRuntimeException(jme);
        }

        return (new RuntimeTerm(spec, jruntime));
    }
}

