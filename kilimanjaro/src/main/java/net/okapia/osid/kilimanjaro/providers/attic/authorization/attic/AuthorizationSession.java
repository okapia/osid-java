//
// AuthorizationSession
//
//     An authorization service stub that returns yes.
//
//
// Tom Coppeto
// OnTapSolutions
// 4 January 2010
//
// Copyright (c) 2010 Massachusetts Institute of Technology. All Rights
// Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package org.osid.runtime.providers.authorization.yes;

import org.osid.binding.java.annotation.OSID;


/**
 *  An OSID Authorization session that says yes to everything.
 */

public final class AuthorizationSession 
    extends org.osid.impl.authorization.AbstractAuthorizationSession
    implements org.osid.authorization.AuthorizationSession {

    /*
     * Constructs a new <code>AuthorizationSession</code>.
     *
     * @param vault the session catalog
     * @throws org.osid.NullArgumentException <code>vault</code> is
     *         <code>null</code>
     */

    AuthorizationSession(org.osid.authorization.Vault vault) {
	setVault(vault);
	return;
    }


    /**
     *  Determines if the given agent is authorized.
     *
     *  @param  agentId the <code> Id </code> of an <code> Agent </code>
     *  @param  functionId the <code> Id </code> of a <code> Function </code>
     *  @param  qualifierId the <code> Id </code> of a <code> Qualifier
     *          </code>
     *  @return <code> true </code> if the user is authorized, <code> false
     *          </code> othersise
     *  @throws org.osid.NullArgumentException <code> agentId </code> , <code>
     *          functionId,</code> or <code>  qualifierId </code>
     *          is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     *          making request
     *  @throws org.osid.IllegalStateException this session has been closed
     */

    @OSID @Override
    public boolean isAuthorized(org.osid.id.Id agentId, org.osid.id.Id functionId,
				org.osid.id.Id qualifierId) 
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

	return (true);
    }
}
