//
// Value.java
//
//     A value parser.
//
//
// Tom Coppeto
// Okapia
// 22 September 2009
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.kilimanjaro.providers.configuration.base;

import net.okapia.osid.torrefacto.xml.XMLUtil;


/**
 *  A value parser.
 */

public final class Value {

    static org.osid.configuration.Value parse(XMLUtil xml, org.osid.configuration.Parameter parameter, 
                                              int sequence, org.dom4j.Node node)
        throws org.osid.OperationFailedException {

        String identifier = parameter.getId().getIdentifier() + "#" + sequence;
        org.osid.id.Id id = new net.okapia.osid.primordium.id.BasicId(parameter.getId().getAuthority(), 
                                                                      parameter.getId().getIdentifierNamespace(), 
                                                                      identifier);

        int priority = 0;
        String attr = node.valueOf("@priority");
        if ((attr != null) && !attr.equals("")) {
            try {
                priority = Integer.parseInt(attr);
            } catch (NumberFormatException nfe) {
                throw new org.osid.OperationFailedException("priority not an integer for " + parameter, nfe);
            }
        }

        org.dom4j.Element element = xml.getElement(node, "c:boolean", false);
        if (element != null) {
            try {
                return (new net.okapia.osid.jamocha.builder.configuration.value.ValueBuilder()
                        .id(id)
                        .parameter(parameter)
                        .booleanValue(Boolean.parseBoolean(element.getText()))
                        .priority(priority)
                        .build());
            } catch (Exception e) {
                throw new org.osid.OperationFailedException("cannot parse boolean for " + parameter, e);
            }
        }

        element = xml.getElement(node, "c:cardinal", false);
        if (element != null) {
            try {
                return (new net.okapia.osid.jamocha.builder.configuration.value.ValueBuilder()
                        .id(id)
                        .parameter(parameter)
                        .cardinalValue(Long.parseLong(element.getText()))
                        .priority(priority)
                        .build());
            } catch (Exception e) {
                throw new org.osid.OperationFailedException("cannot parse cardinal for " + parameter, e);
            }
        }

        element = xml.getElement(node, "c:datetime", false);
        if (element != null) {
            try {
                return (new net.okapia.osid.jamocha.builder.configuration.value.ValueBuilder()
                        .id(id)
                        .parameter(parameter)
                        .dateTimeValue(net.okapia.osid.primordium.calendaring.GregorianUTCDateTime.valueOf(element.getText()))
                        .priority(priority)
                        .build());
            } catch (Exception e) {
                throw new org.osid.OperationFailedException("cannot parse date for " + parameter, e);
            }
        }

        element = xml.getElement(node, "c:float", false);
        if (element != null) {
            try {
                return (new net.okapia.osid.jamocha.builder.configuration.value.ValueBuilder()
                        .id(id)
                        .parameter(parameter)
                        .decimalValue(new java.math.BigDecimal(element.getText()))
                        .priority(priority)
                        .build());
            } catch (Exception e) {
                throw new org.osid.OperationFailedException("cannot parse float " + parameter, e);
            }
        }

        element = xml.getElement(node, "c:id", false);
        if (element != null) {
            try {
                return (new net.okapia.osid.jamocha.builder.configuration.value.ValueBuilder()
                        .id(id)
                        .parameter(parameter)
                        .idValue(net.okapia.osid.primordium.id.BasicId.valueOf(element.getText()))
                        .priority(priority)
                        .build());
            } catch (Exception e) {
                throw new org.osid.OperationFailedException("cannot parse id for " + parameter, e);
            }
        }

        element = xml.getElement(node, "c:integer", false);
        if (element != null) {
            try {
                return (new net.okapia.osid.jamocha.builder.configuration.value.ValueBuilder()
                        .id(id)
                        .parameter(parameter)
                        .integerValue(Long.parseLong(element.getText()))
                        .priority(priority)
                        .build());
            } catch (Exception e) {
                throw new org.osid.OperationFailedException("cannot parse integer for " + parameter, e);
            }
        }

        element = xml.getElement(node, "c:string", false);
        if (element != null) {
            return (new net.okapia.osid.jamocha.builder.configuration.value.ValueBuilder()
                    .id(id)
                    .parameter(parameter)
                    .stringValue(xml.trim((element).asXML()))
                    .priority(priority)
                    .build());
        }

        element = xml.getElement(node, "c:type", false);
        if (element != null) {
            try {
                return (new net.okapia.osid.jamocha.builder.configuration.value.ValueBuilder()
                        .id(id)
                        .parameter(parameter)
                        .typeValue(net.okapia.osid.primordium.type.BasicType.valueOf(element.getText()))
                        .priority(priority)
                        .build());
            } catch (Exception e) {
                throw new org.osid.OperationFailedException("cannot parse type for " + parameter, e);
            }
        }

        throw new org.osid.OperationFailedException("no value defined for " + parameter);
    }
}
