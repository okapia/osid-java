//
// AbstractTestTime.java
//
//     A unit test for a Time.
//
//
// Tom Coppeto
// Okapia
// 2 May 2012
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.calendaring.test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;
import org.testng.annotations.*;

import org.osid.type.Type;
import org.osid.calendaring.Time;
import org.osid.calendaring.DateTimeResolution;
import net.okapia.osid.primordium.type.BasicType;

import net.okapia.osid.primordium.calendaring.spi.AbstractConfigurableTime;
import net.okapia.osid.primordium.calendaring.spi.AbstractTimeSpecifier;
import net.okapia.osid.primordium.calendaring.spi.AbstractConfigurableTimeSpecifier;
import net.okapia.osid.primordium.calendaring.TimeDefinition;
import net.okapia.osid.primordium.calendaring.spi.TimeFactory;



public abstract class AbstractTestTime {
    protected static final int PLANCKS_IN_UNDASECOND = 185_509_483;


    /**
     *  Override to run this test for a different implementation.
     */

    protected abstract Time makeTime(String timeStr);


    @DataProvider(name = "validity")
    public abstract Object[][] getValidData();


    @Test(dataProvider="validity")
    public void testValidity(String input, String output) {
        Time time = makeTime(input);

        assertNotNull(time.getTimeType());
        assertNotNull(time.getGranularity());
        assertEquals(time.getGranularityMultiplier() > 0, true);

        assertEquals(time.getHour() >= 0, true);
        assertEquals(time.getMinute() >= 0, true);
        assertEquals(time.getSecond() >= 0, true);

        assertEquals(time.getMilliseconds() >= 0, true);
        assertEquals(time.getMilliseconds() <= 999, true);
        assertEquals(time.getMicroseconds() >= 0, true);
        assertEquals(time.getMicroseconds() <= 999, true);
        assertEquals(time.getNanoseconds() >= 0, true);
        assertEquals(time.getNanoseconds() <= 999, true);        
        assertEquals(time.getPicoseconds() >= 0, true);
        assertEquals(time.getPicoseconds() <= 999, true);
        assertEquals(time.getFemtoseconds() >= 0, true);
        assertEquals(time.getFemtoseconds() <= 999, true);
        assertEquals(time.getAttoseconds() >= 0, true);
        assertEquals(time.getAttoseconds() <= 999, true);
        assertEquals(time.getZeptoseconds() >= 0, true);
        assertEquals(time.getZeptoseconds() <= 999, true);
        assertEquals(time.getYoctoseconds() >= 0, true);
        assertEquals(time.getYoctoseconds() <= 999, true);
        assertEquals(time.getXoxxoseconds() >= 0, true);
        assertEquals(time.getXoxxoseconds() <= 999, true);
        assertEquals(time.getWeebleseconds() >= 0, true);
        assertEquals(time.getWeebleseconds() <= 999, true);
        assertEquals(time.getVatoseconds() >= 0, true);
        assertEquals(time.getVatoseconds() <= 999, true);
        assertEquals(time.getUndaseconds() >= 0, true);
        assertEquals(time.getUndaseconds() <= 999, true);
        assertEquals(time.getPlanckSeconds() >= 0, true);
        assertEquals(time.getPlanckSeconds() <= PLANCKS_IN_UNDASECOND, true);

        assertEquals(time.toString(), output);

        return;
    }


    @DataProvider(name = "invalidity")
    public abstract Object[][] getInvalidData();


    @Test(dataProvider="invalidity")
    public void testInvalidity(String input) {
        try {
            Time time = makeTime(input);
            fail(input + " should be invalid");
        } catch (org.osid.InvalidArgumentException iae) {}

        return;
    }


    @DataProvider(name = "precision")
    public abstract Object[][] getPrecisionData();


    @Test(dataProvider="precision")
    public void testPrecision(String input, DateTimeResolution granularity, long multiplier) {
        Time time = makeTime(input);        
        return;
    }


    @DataProvider(name = "comparisons")
    public abstract Object[][] getComparisonData();


    @Test(dataProvider="comparisons")
    public void testComparisons(String oldTimeStr, String newTimeStr) {
        Time oldTime = makeTime(oldTimeStr);
        Time newTime = makeTime(newTimeStr);
        
        assertEquals(oldTime.equals(newTime), false);
        assertEquals(newTime.equals(oldTime), false);
        
        assertEquals(oldTime.isGreater(newTime), false);
        assertEquals(newTime.isGreater(oldTime), true);
        
        assertEquals(oldTime.isLess(newTime), true);
        assertEquals(newTime.isLess(oldTime), false);

        return;
    }


    @Test(dataProvider="comparisons")
    public void testComparisonsUnequalSchemes(String oldTimeStr, String newTimeStr) {
        Time oldTime = makeTime(oldTimeStr);
        Time newTime = makeTime(newTimeStr);

        Time random = new RandomTime();

        assertEquals(oldTime.equals(random), false);
        assertEquals(random.equals(oldTime), false);

        try {
            assertEquals(oldTime.isGreater(random), false);
            fail("unsupported type expected");
        } catch (org.osid.UnsupportedException ue) {}
        
        try {
            assertEquals(random.isGreater(oldTime), false);
            fail("unsupported type expected");
        } catch (org.osid.UnsupportedException ue) {}
        
        try {
            assertEquals(oldTime.isLess(random), false);
            fail("unsupported type expected");
        } catch (org.osid.UnsupportedException ue) {}
        
        try {
            assertEquals(random.isLess(oldTime), false);
            fail("unsupported type expected");
        } catch (org.osid.UnsupportedException ue) {}
        
        try {
            assertEquals(oldTime.isInclusive(random), false);
            fail("unsupported type expected");
        } catch (org.osid.UnsupportedException ue) {}
        
        try {
            assertEquals(random.isInclusive(oldTime), false);
            fail("unsupported type expected");
        } catch (org.osid.UnsupportedException ue) {}
        
        try {
            assertEquals(oldTime.isExclusive(random), false);
            fail("unsupported type expected");
        } catch (org.osid.UnsupportedException ue) {}
        
        try {
            assertEquals(random.isExclusive(oldTime), false);
            fail("unsupported type expected");
        } catch (org.osid.UnsupportedException ue) {}
    
        return;
    }


    @DataProvider(name = "inclusions")    
    public abstract Object[][] getInclusionData();


    @Test(dataProvider="inclusions")
    public void testInclusions(String outerStr, String innerStr, String otherStr) {
        Time outer = makeTime(outerStr);
        Time inner = makeTime(innerStr);
        Time other = makeTime(otherStr);

        assertEquals(inner.isGreater(outer), false);
        assertEquals(outer.isLess(inner), false);

        assertEquals(outer.isInclusive(inner), true);
        assertEquals(inner.isInclusive(other), false);

        assertEquals(inner.equals(other), false);
        assertEquals(outer.equals(other), false);
        assertEquals(other.equals(outer), false);
        assertEquals(other.equals(inner), false);

        return;
    }

    
    @DataProvider(name = "exclusions")
    public abstract Object[][] getExclusionData();


    @Test(dataProvider="exclusions")
    public void testExclusions(String refStr, String exclusiveStr, String nonexclusiveStr) {
        Time ref = makeTime(refStr);
        Time exclusive = makeTime(exclusiveStr);
        Time nonexclusive = makeTime(nonexclusiveStr);

        assertEquals(ref.equals(exclusive), false);
        assertEquals(exclusive.equals(ref), false);

        assertEquals(ref.isExclusive(exclusive), true);
        assertEquals(exclusive.isExclusive(ref), true);

        assertEquals(ref.isExclusive(nonexclusive), false);

        return;
    }


    @DataProvider(name = "sorting")
    public abstract Object[][] getSortingData();


    @Test(dataProvider="sorting")
    public void testSorting(String[] unsortedStrings, String[] sortedStrings) {
        java.util.List<Time> unsorted = new ArrayList<>(unsortedStrings.length);
        java.util.List<Time> sorted   = new ArrayList<>(sortedStrings.length);

        for (String s : unsortedStrings) {
            unsorted.add(makeTime(s));
        }

        for (String s : sortedStrings) {
            sorted.add(makeTime(s));
        }

        Collections.sort(unsorted);
        assertEquals(unsorted, sorted); 

        return;
    }


    protected static class RandomTime
        extends AbstractConfigurableTime
        implements org.osid.calendaring.Time {
        
        private static final Random r = new Random();
        private static final long serialVersionUID = 0L;


        protected RandomTime() {
            super(new RandomTimeFactory(), r.nextInt(1_000), r.nextInt(1_000_000), r.nextInt(1_000_000_000));
            return;
        }


        protected static class RandomTimeFactory
            implements TimeFactory {

            @Override
            public TimeDefinition getTimeDefinition() {
                return new RandomTimeDefinition();
            }


            @Override
            public RandomTimeSpecifier getTimeSpecifier() {
                return (new RandomTimeSpecifier());
            }

            
            @Override
            public org.osid.calendaring.Time makeTime(AbstractTimeSpecifier specifier) {
                return (new RandomTime());
            }
        }
        

        protected static class RandomTimeSpecifier
            extends AbstractConfigurableTimeSpecifier {

            
            public RandomTimeSpecifier() {
                super(new RandomTimeDefinition());
                return;
            }
        }


        protected static class RandomTimeDefinition
            implements TimeDefinition {

            protected static final org.osid.type.Type TYPE = new BasicType("test", "nonsense", "blort");
    

            @Override
            public org.osid.type.Type getTimeType() {
                return (TYPE);
            }


            @Override
            public long getHoursPerDay() {
                return (1_000);
            }

            
            @Override
            public long getStartingHour() {
                return (0);
            }


            @Override
            public long getMinutesPerHour() {
                return (1_000_000);
            }


            @Override
            public long getStartingMinute() {
                return (0);
            }


            @Override
            public long getSecondsPerMinute() {
                return (1_000_000_000);
            }


            @Override
            public long getStartingSecond() {
                return (0);
            }

            
            @Override
            public java.math.BigDecimal getPlancksPerSecond() {
                return (new java.math.BigDecimal("1e100"));
            }

            
            @Override
            public String getLabel(org.osid.calendaring.DateTimeResolution units) {
                return ("apple");
            }


            @Override
            public String getPluralLabel(org.osid.calendaring.DateTimeResolution units) {
                return ("planes");
            }


            @Override
            public org.osid.calendaring.DateTimeResolution getUnits(String units)
                throws org.osid.NotFoundException {

                return (org.osid.calendaring.DateTimeResolution.BLUEMOON);
            }                
        }
    }
}
