//
// AbstractTestDateTime.java
//
//     A unit test for a DateTime.
//
//
// Tom Coppeto
// Okapia
// 2 May 2012
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.calendaring.test;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;
import org.testng.annotations.*;

import org.osid.type.Type;
import org.osid.calendaring.DateTime;
import org.osid.calendaring.DateTimeResolution;
import net.okapia.osid.primordium.type.BasicType;

import net.okapia.osid.primordium.calendaring.spi.AbstractConfigurableDateTime;
import net.okapia.osid.primordium.calendaring.spi.AbstractTimeSpecifier;
import net.okapia.osid.primordium.calendaring.spi.AbstractDateTimeSpecifier;
import net.okapia.osid.primordium.calendaring.spi.AbstractConfigurableDateTimeSpecifier;
import net.okapia.osid.primordium.calendaring.CalendarDefinition;
import net.okapia.osid.primordium.calendaring.spi.DateTimeFactory;
import net.okapia.osid.primordium.calendaring.UTCTime;



public abstract class AbstractTestDateTime {
    protected static final int PLANCKS_IN_UNDASECOND = 185_509_483;


    /**
     *  Override to run this test for a different implementation.
     */

    protected abstract DateTime makeDateTime(String dateStr);


    @DataProvider(name = "validity")
    public abstract Object[][] getValidData();


    @Test(dataProvider="validity")
    public void testValidity(String input, String output) {
        DateTime datetime = makeDateTime(input);
        assertNotNull(datetime.getCalendarType());
        assertNotNull(datetime.getTimeType());
        assertNotNull(datetime.getGranularity());
        assertEquals(datetime.getGranularityMultiplier() > 0, true);

        assertEquals(datetime.toString(), output);

        assertEquals(datetime.getMonth() >= 1, true);
        assertEquals(datetime.getDay() >= 1, true);
        assertEquals(datetime.getHour() >= 0, true);
        assertEquals(datetime.getMinute() >= 0, true);
        assertEquals(datetime.getSecond() >= 0, true);

        assertEquals(datetime.getMilliseconds() >= 0, true);
        assertEquals(datetime.getMilliseconds() <= 999, true);
        assertEquals(datetime.getMicroseconds() >= 0, true);
        assertEquals(datetime.getMicroseconds() <= 999, true);
        assertEquals(datetime.getNanoseconds() >= 0, true);
        assertEquals(datetime.getNanoseconds() <= 999, true);        
        assertEquals(datetime.getPicoseconds() >= 0, true);
        assertEquals(datetime.getPicoseconds() <= 999, true);
        assertEquals(datetime.getFemtoseconds() >= 0, true);
        assertEquals(datetime.getFemtoseconds() <= 999, true);
        assertEquals(datetime.getAttoseconds() >= 0, true);
        assertEquals(datetime.getAttoseconds() <= 999, true);
        assertEquals(datetime.getZeptoseconds() >= 0, true);
        assertEquals(datetime.getZeptoseconds() <= 999, true);
        assertEquals(datetime.getYoctoseconds() >= 0, true);
        assertEquals(datetime.getYoctoseconds() <= 999, true);
        assertEquals(datetime.getXoxxoseconds() >= 0, true);
        assertEquals(datetime.getXoxxoseconds() <= 999, true);
        assertEquals(datetime.getWeebleseconds() >= 0, true);
        assertEquals(datetime.getWeebleseconds() <= 999, true);
        assertEquals(datetime.getVatoseconds() >= 0, true);
        assertEquals(datetime.getVatoseconds() <= 999, true);
        assertEquals(datetime.getUndaseconds() >= 0, true);
        assertEquals(datetime.getUndaseconds() <= 999, true);
        assertEquals(datetime.getPlanckSeconds() >= 0, true);
        assertEquals(datetime.getPlanckSeconds() <= PLANCKS_IN_UNDASECOND, true);

        return;
    }


    @DataProvider(name = "invalidity")
    public abstract Object[][] getInvalidData();


    @Test(dataProvider="invalidity")
    public void testInvalidity(String input) {
        try {
            DateTime datetime = makeDateTime(input);
            fail(input + " should be invalid");
        } catch (org.osid.InvalidArgumentException iae) {}

        return;
    }


    @DataProvider(name = "precision")
    public abstract Object[][] getPrecisionData();


    @Test(dataProvider="precision")
    public void testPrecision(String input, DateTimeResolution granularity, long multiplier) {
        DateTime datetime = makeDateTime(input);        
        return;
    }


    @DataProvider(name = "comparisons")
    public abstract Object[][] getComparisonData();


    @Test(dataProvider="comparisons")
    public void testComparisons(String oldDateTimeStr, String newDateTimeStr) {
        DateTime oldDateTime = makeDateTime(oldDateTimeStr);
        DateTime newDateTime = makeDateTime(newDateTimeStr);

        assertEquals(oldDateTime.equals(newDateTime), false);
        assertEquals(newDateTime.equals(oldDateTime), false);
        
        assertEquals(oldDateTime.isGreater(newDateTime), false);
        assertEquals(newDateTime.isGreater(oldDateTime), true);
        
        assertEquals(oldDateTime.isLess(newDateTime), true);
        assertEquals(newDateTime.isLess(oldDateTime), false);

        return;
    }


    @Test(dataProvider="comparisons")
    public void testComparisonsUnequalSchemes(String oldDateTimeStr, String newDateTimeStr) {
        DateTime oldDateTime = makeDateTime(oldDateTimeStr);
        DateTime newDateTime = makeDateTime(newDateTimeStr);

        DateTime random = new RandomDateTime();

        assertEquals(oldDateTime.equals(random), false);
        assertEquals(random.equals(oldDateTime), false);

        try {
            assertEquals(oldDateTime.isGreater(random), false);
            fail("unsupported type expected");
        } catch (org.osid.UnsupportedException ue) {}
        
        try {
            assertEquals(random.isGreater(oldDateTime), false);
            fail("unsupported type expected");
        } catch (org.osid.UnsupportedException ue) {}
        
        try {
            assertEquals(oldDateTime.isLess(random), false);
            fail("unsupported type expected");
        } catch (org.osid.UnsupportedException ue) {}
        
        try {
            assertEquals(random.isLess(oldDateTime), false);
            fail("unsupported type expected");
        } catch (org.osid.UnsupportedException ue) {}
        
        try {
            assertEquals(oldDateTime.isInclusive(random), false);
            fail("unsupported type expected");
        } catch (org.osid.UnsupportedException ue) {}
        
        try {
            assertEquals(random.isInclusive(oldDateTime), false);
            fail("unsupported type expected");
        } catch (org.osid.UnsupportedException ue) {}
        
        try {
            assertEquals(oldDateTime.isExclusive(random), false);
            fail("unsupported type expected");
        } catch (org.osid.UnsupportedException ue) {}
        
        try {
            assertEquals(random.isExclusive(oldDateTime), false);
            fail("unsupported type expected");
        } catch (org.osid.UnsupportedException ue) {}
    
        return;
    }


    @DataProvider(name = "inclusions")    
    public abstract Object[][] getInclusionData();


    @Test(dataProvider="inclusions")
    public void testInclusions(String outerStr, String innerStr, String otherStr) {
        DateTime outer = makeDateTime(outerStr);
        DateTime inner = makeDateTime(innerStr);
        DateTime other = makeDateTime(otherStr);

        assertEquals(inner.isGreater(outer), false);
        assertEquals(outer.isLess(inner), false);

        assertEquals(inner.isInclusive(outer), false);
        assertEquals(outer.isInclusive(inner), true);

        assertEquals(inner.isInclusive(other), false);

        return;
    }

    
    @DataProvider(name = "exclusions")
    public abstract Object[][] getExclusionData();


    @Test(dataProvider="exclusions")
    public void testExclusions(String refStr, String exclusiveStr, String nonexclusiveStr) {
        DateTime ref = makeDateTime(refStr);
        DateTime exclusive = makeDateTime(exclusiveStr);
        DateTime nonexclusive = makeDateTime(nonexclusiveStr);

        assertEquals(ref.equals(exclusive), false);
        assertEquals(exclusive.equals(ref), false);

        assertEquals(ref.isExclusive(exclusive), true);
        assertEquals(exclusive.isExclusive(ref), true);

        assertEquals(ref.isExclusive(nonexclusive), false);

        return;
    }


    protected static class RandomDateTime
        extends AbstractConfigurableDateTime
        implements org.osid.calendaring.DateTime {
        
        private static final Random r = new Random();
        private static final long serialVersionUID = 0L;


        protected RandomDateTime() {
            super(new RandomDateTimeFactory(), new RandomDateTimeSpecifier());
            return;
        }


        protected RandomDateTime(RandomDateTimeSpecifier specifier) {
            super(new RandomDateTimeFactory(), specifier);
            return;
        }


        protected static class RandomDateTimeFactory
            implements DateTimeFactory {
            
            @Override
            public RandomCalendarDefinition getCalendarDefinition() {
                return (new RandomCalendarDefinition());
            }
            
            
            @Override
            public RandomDateTimeSpecifier getDateTimeSpecifier() {
                return (new RandomDateTimeSpecifier());
            }
            
            
            @Override
            public org.osid.calendaring.DateTime makeDateTime(AbstractDateTimeSpecifier specifier) {
                if (!(specifier instanceof RandomDateTimeSpecifier)) {
                    throw new org.osid.UnsupportedException("specifier not supported");
                }
                
                return (new RandomDateTime((RandomDateTimeSpecifier) specifier));
            }
            
            
            @Override
            public UTCTime makeTime(AbstractTimeSpecifier specifier) {
                if (!(specifier instanceof UTCTime.UTCTimeSpecifier)) {
                    throw new org.osid.UnsupportedException("specifier not supported");
                }
                
                return (new UTCTime((UTCTime.UTCTimeSpecifier) specifier));
            }
            
            
            @Override
            public org.osid.calendaring.Time makeTime(org.osid.calendaring.DateTime datetime) {
                return (new UTCTime(new UTCTime.UTCTimeSpecifier(datetime)));
            }    
        }
        
        
        protected static class RandomDateTimeSpecifier
            extends AbstractConfigurableDateTimeSpecifier {
            
            private static final org.osid.type.Type TYPE = new BasicType("test", "nonsense", "stuff");
            
            protected RandomDateTimeSpecifier() {
                super(new RandomCalendarDefinition(), TYPE,
                      new UTCTime.UTCTimeSpecifier());
                return;
            }
            
            
            @Override
            public org.osid.calendaring.Time getTime() {
                return (new UTCTime((UTCTime.UTCTimeSpecifier) getTimeSpecifier()));
            }
        }
    }


    protected static class RandomCalendarDefinition 
        implements CalendarDefinition {

        private static final org.osid.type.Type TYPE = new BasicType("test", "nonsense", "stuff");

    
        @Override
        public org.osid.type.Type getCalendarType() {
            return (TYPE);
        }
        

        @Override
        public java.math.BigDecimal getYearsPerBluemoon() {
            return (new java.math.BigDecimal(42));
        }

    
        @Override
        public long getMonthsPerYear() {
            return (27);
        }

        @Override
        public java.math.BigDecimal getAverageWeeksPerYear() {
            return (new java.math.BigDecimal("99"));
        }


        @Override
        public java.math.BigDecimal getAverageDaysPerYear() {
            return (new java.math.BigDecimal("499"));
        }


        @Override
        public java.math.BigDecimal getAverageDaysPerMonth() {
            return (new java.math.BigDecimal("13"));
        }


        @Override
        public java.math.BigDecimal getAverageDaysPerWeek() {
            return (new java.math.BigDecimal("3"));
        }

        
        @Override
        public String getLabel(org.osid.calendaring.DateTimeResolution units) {
            return ("fire hydrant");
        }
        
        
        @Override
        public String getPluralLabel(org.osid.calendaring.DateTimeResolution units) {
            return ("rocks");
        }

        
        @Override
        public org.osid.calendaring.DateTimeResolution getUnits(String units)
            throws org.osid.NotFoundException {
            
            return (org.osid.calendaring.DateTimeResolution.BLUEMOON);
        }
    }
}
