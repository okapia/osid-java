//
// AbstractTestId
//
//     A unit test for an Id.
//
//
// Tom Coppeto
// Okapia
// 23 September 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.id.test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import org.testng.annotations.*;

import org.osid.id.Id;


public abstract class AbstractTestId {

    @Test(dataProvider="validity")
    public void testValidity(String authority, String namespace, String identifier, 
                             String input, Id id) {

        assertNotNull(id.getAuthority());
        assertNotNull(id.getIdentifierNamespace());
        assertNotNull(id.getIdentifier());
        assertNotNull(id.toString());
        
        assertEquals(id.getAuthority(), authority);
        assertEquals(id.getIdentifierNamespace(), namespace);
        assertEquals(id.getIdentifier(), identifier);
        assertEquals(id.toString(), input);
        
        Id simple = new SimpleId(authority, namespace, identifier);
        assertEquals(id.getAuthority(), simple.getAuthority());
        assertEquals(id.getIdentifierNamespace(), simple.getIdentifierNamespace());
        assertEquals(id.getIdentifier(), simple.getIdentifier());
        assertEquals(id, simple);

        return;
    }


    protected static class SimpleId
        extends net.okapia.osid.primordium.id.spi.AbstractId
        implements org.osid.id.Id {

        private static final long serialVersionUID = 1L;

        
        public SimpleId(String authority, String namespace, String identifier) {
            super(authority, namespace, identifier);
            return;
        }

        
        @Override
        protected String formatLabel() {
            return ("hello");
        }
    }        
}
