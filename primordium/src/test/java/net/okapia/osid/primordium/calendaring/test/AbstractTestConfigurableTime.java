//
// AbstractTestConfigurableTime
//
//     A unit test for a Time.
//
//
// Tom Coppeto
// Okapia
// 2 May 2012
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.calendaring.test;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.*;

import org.osid.type.Type;
import org.osid.calendaring.Time;

import net.okapia.osid.primordium.calendaring.TimeDefinition;


public abstract class AbstractTestConfigurableTime
    extends AbstractTestTime {


    protected abstract TimeDefinition getTimeDefinition();


    @Test(dataProvider="validity")
    public void testValidityAgainstDefinition(String input, String output) {
        Time time = makeTime(input);
        TimeDefinition def = getTimeDefinition();

        Assert.assertEquals(time.getHour() >= def.getStartingHour(), true);
        Assert.assertEquals(time.getMinute() >= def.getStartingMinute(), true);
        Assert.assertEquals(time.getSecond() >= def.getStartingSecond(), true);

        Assert.assertEquals(time.getHour() < (def.getStartingHour() + def.getHoursPerDay()), true);
        Assert.assertEquals(time.getMinute() < (def.getStartingMinute() + def.getMinutesPerHour()), true);
        Assert.assertEquals(time.getSecond() < (def.getStartingSecond() + def.getSecondsPerMinute()), true);

        return;
    }
}
