//
// TestURNId
//
//     A unit test for a URNId.
//
//
// Tom Coppeto
// Okapia
// 23 September 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.id.test;

import org.testng.annotations.*;

import org.osid.id.Id;
import net.okapia.osid.primordium.id.URNId;


@Test
public class TestURNId
    extends AbstractTestId {


    /**
     *  Override to run this test for a different implementation.
     */

    protected Id makeId(String id) {
        return (URNId.valueOf(id));
    }


    /**
     *  This data may be overidden to constrain to a specific Version
     *  scheme.
     */

    protected String[][] VALIDITY_DATA = {
        {"okapia.net", "here", "there", "urn:osid:okapia.net:identifiers:here:there"},
        {"okapia.net", "here", "there/elsewhere", "urn:osid:okapia.net:identifiers:here:there:elsewhere"},
        {"okapia.net", "here", "there:elsewhere", "urn:osid:okapia.net:identifiers:here:there%3aelsewhere"},
        {"okapia.net", "here", "there::elsewhere", "urn:osid:okapia.net:identifiers:here:there%3a%3aelsewhere"},
        {"okapia.net", "here", "there@elsewhere", "urn:osid:okapia.net:identifiers:here:there@elsewhere"},
        {"okapia.net", "here", "there?elsewhere", "urn:osid:okapia.net:identifiers:here:there%3felsewhere"},
        {"okapia.net", "right:here", "there", "urn:osid:okapia.net:identifiers:right%3ahere:there"},
        {"okapia.net", "right/here", "there/elsewhere", "urn:osid:okapia.net:identifiers:right%2fhere:there:elsewhere"},
        {"okapia.net", "right::here", "there::elsewhere", "urn:osid:okapia.net:identifiers:right%3a%3ahere:there%3a%3aelsewhere"},
        {"okapia.net", "right@here", "there@elsewhere", "urn:osid:okapia.net:identifiers:right@here:there@elsewhere"},
        {"okapia.net", "right?here", "there?elsewhere", "urn:osid:okapia.net:identifiers:right%3fhere:there%3felsewhere"}
    };


    @DataProvider(name = "validity")
    public Object[][] getValidity() {
        Object[][] ret = new Object[VALIDITY_DATA.length][5];

        int i = 0;
        for (String[] c : VALIDITY_DATA) {
            ret[i][0] = c[0];            
            ret[i][1] = c[1];            
            ret[i][2] = c[2];            
            ret[i][3] = c[3];            
            ret[i++][4] = makeId(c[3]);
        }

        return (ret);
    }
}
