//
// TestUTCTime.java
//
//     A unit test for a Time.
//
//
// Tom Coppeto
// Okapia
// 2 May 2012
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.calendaring.test;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;

import static org.testng.Assert.assertEquals;
import org.testng.annotations.*;

import org.osid.type.Type;
import org.osid.calendaring.Time;
import org.osid.calendaring.DateTimeResolution;

import net.okapia.osid.primordium.calendaring.UTCTime;
import net.okapia.osid.primordium.calendaring.TimeDefinition;
import net.okapia.osid.primordium.calendaring.UTCTimeDefinition;

import net.okapia.osid.primordium.types.time.EarthTimeTypes;


@Test
public class TestUTCTime
    extends AbstractTest246060Time {

    private static final Type TIME_TYPE = EarthTimeTypes.UTC.getType();


    @Override
    protected Time makeTime(String s) {
        return UTCTime.valueOf(s);
    }


    @Override
    protected TimeDefinition getTimeDefinition() {
        return (new UTCTimeDefinition());
    }

    @Test(dataProvider="validity")
    public void testType(String input, String output) {
        Time time = makeTime(input);
        assertEquals(time.getTimeType(), TIME_TYPE);
        return;
    }


    public void testUnknown() {
        Time time = UTCTime.unknown();
        assertEquals(time.getGranularity(), DateTimeResolution.INFINITY);        
        assertEquals(time.getTimeType(), TIME_TYPE);

        return;
    }
}
