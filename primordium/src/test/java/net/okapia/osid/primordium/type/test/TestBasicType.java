//
// TestBasicType
//
//     A unit test for a BasicType.
//
//
// Tom Coppeto
// Okapia
// 23 September 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.type.test;

import org.testng.annotations.*;

import org.osid.type.Type;
import net.okapia.osid.primordium.type.BasicType;


@Test
public class TestBasicType
    extends AbstractTestType {


    /**
     *  Override to run this test for a different implementation.
     */

    protected Type makeType(String type) {
        return (BasicType.valueOf(type));
    }


    /**
     *  This data may be overidden to constrain to a specific Version
     *  scheme.
     */

    protected String[][] VALIDITY_DATA = {
        {"okapia.net", "here", "there", "here:there@okapia.net"},
        {"okapia.net", "here", "there/somewhere", "here:there/somewhere@okapia.net"},
        {"okapia.net", "here", "there:somewhere", "here:there%3asomewhere@okapia.net"},
        {"okapia.net", "almost:here", "there:somewhere", "almost%3ahere:there%3asomewhere@okapia.net"},
        {"okapia.net", "almost:here", "there:somewhere", "almost%3ahere:there%3asomewhere@okapia.net"},
        {"okapia.net", "almost:here", "there:somewhere@else", "almost%3ahere:there%3asomewhere%40else@okapia.net"},
        {"site@okapia.net", "almost:here", "there:somewhere@else", "almost%3ahere:there%3asomewhere%40else@site%40okapia.net"},
        {"our:site@okapia.net", "almost:here", "there:somewhere@else", "almost%3ahere:there%3asomewhere%40else@our%3asite%40okapia.net"},
        {"our:site@ok!#$<>;^&*()_+|}{?//-=apia.net", "al!#$<>;^&*()_+|}{?//-=most:here", "there:some!#$<>;^&*()_+|}{?//-=where@else", "al!#$<>;^&*()_+|}{?//-=most%3ahere:there%3asome!#$<>;^&*()_+|}{?//-=where%40else@our%3asite%40ok!#$<>;^&*()_+|}{?//-=apia.net"}
    };


    @DataProvider(name = "validity")
    public Object[][] getValidity() {
        Object[][] ret = new Object[VALIDITY_DATA.length][5];

        int i = 0;
        for (String[] c : VALIDITY_DATA) {
            ret[i][0] = c[0];            
            ret[i][1] = c[1];            
            ret[i][2] = c[2];            
            ret[i][3] = c[3];            
            ret[i++][4] = makeType(c[3]);
        }

        return (ret);
    }
}
