//
// TestVersion
//
//     A unit test for an Version.
//
//
// Tom Coppeto
// Okapia
// 2 May 2012
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.installation.test;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;
import org.testng.annotations.*;

import org.osid.type.Type;
import org.osid.installation.Version;
import net.okapia.osid.primordium.installation.AnyVersion;
import net.okapia.osid.primordium.types.version.VersionSchemeTypes;
import net.okapia.osid.primordium.type.BasicType;


@Test
public class TestVersion {

    /**
     *  Override to run this test for a different implementation.
     */

    protected Version makeVersion(String[] components) {
        return (new AnyVersion(VersionSchemeTypes.SOFTWARE.getType(), components));
    }


    /**
     *  This data may be overidden to constrain to a specific Version
     *  scheme.
     */

    protected String[][] VALIDITY_DATA = {
        {"1"},
        {"1", "0"},
        {"1", "1"},
        {"1", "2", "3"},
        {"1", "2", "4rc"},
        {"1", "2", "3", "4", "5", "6", "7", "8", "9"},
        {"9", "1", "8", "2", "7", "3", "6", "4", "5"},
        {"9", "1", "8", "2", "7", "3", "6", "4", "5"}
    };


    @DataProvider(name = "validity")
    public Object[][] getValidity() {
        Object[][] ret = new Object[VALIDITY_DATA.length][2];

        int i = 0;
        for (String[] c : VALIDITY_DATA) {
            ret[i][0] = makeVersion(c);
            ret[i++][1] = c;            
        }

        return (ret);
    }


    @Test(dataProvider="validity")
    public void testValidity(Version version, String[] components) {

        assertNotNull(version.getScheme());
        assertNotNull(version.getComponents());
        assertNotNull(version.toString());
        
        assertEquals(version.getComponents().length, components.length);
        assertEquals(version.getComponents(), components);
        
        assertEquals(version.isNewer(version), false);
        assertEquals(version.isOlder(version), false);
        assertEquals(version.isInclusive(version), true);
        assertEquals(version.isExclusive(version), false);
        
        assertEquals(version.compareTo(version), 0);
        assertEquals(version.equals(version), true);
        assertNotEquals(version.hashCode(), 0);

        return;
    }


    /**
     *  This data may be overidden to constrain to a specific Version
     *  scheme. The LOW data and the HIGH data are aligned across the
     *  elements.
     */

    protected String[][] COMPARISON_LOW_DATA = {
        {"1"},
        {"1", "0"},
        {"1", "1"},
        {"1", "2", "3"},
        {"1", "2", "4rc"},
        {"1", "2", "3", "4", "5", "6", "7", "8", "9"},
        {"9", "1", "8", "2", "7", "3", "6", "4", "5"}
    };

    protected String[][] COMPARISON_HIGH_DATA = {
        {"2"},
        {"1", "1"},
        {"1", "1", "1"},
        {"1", "2", "3", "4", "5"},
        {"1", "2", "5rc"},
        {"2", "2", "3", "4", "5", "6", "7", "8"},
        {"9", "1", "8", "2", "8", "3", "6", "4", "5"}
    };


    @DataProvider(name = "comparisons")
    public Object[][] getComparisonData() {
        Object[][] ret = new Object[COMPARISON_LOW_DATA.length][2];

        int i = 0;
        for (String[] c : COMPARISON_LOW_DATA) {
            ret[i++][0] = makeVersion(c);
        }

        i = 0;
        for (String[] c : COMPARISON_HIGH_DATA) {
            ret[i++][1] = makeVersion(c);
        }

        return (ret);
    }


    @Test(dataProvider="comparisons")
    public void testComparisons(Version oldVersion, Version newVersion) {

        assertEquals(oldVersion.equals(newVersion), false);
        assertEquals(newVersion.equals(oldVersion), false);
        
        assertEquals(oldVersion.isNewer(newVersion), false);
        assertEquals(newVersion.isNewer(oldVersion), true);
        
        assertEquals(oldVersion.isOlder(newVersion), true);
        assertEquals(newVersion.isOlder(oldVersion), false);

        return;
    }


    @Test(dataProvider="comparisons")
    public void testComparisonsUnequalSchemes(Version version, Version newVersion) {

        Version random = new AnyVersion(new BasicType("foo9", "bar8", "baz7"), new String[] {"0"});

        assertEquals(version.equals(random), false);
        assertEquals(random.equals(version), false);

        try {
            assertEquals(version.isNewer(random), false);
            fail("unsupported type expected");
        } catch (org.osid.UnsupportedException ue) {}
        
        try {
            assertEquals(random.isNewer(version), false);
            fail("unsupported type expected");
        } catch (org.osid.UnsupportedException ue) {}
        
        try {
            assertEquals(version.isOlder(random), false);
            fail("unsupported type expected");
        } catch (org.osid.UnsupportedException ue) {}
        
        try {
            assertEquals(random.isOlder(version), false);
            fail("unsupported type expected");
        } catch (org.osid.UnsupportedException ue) {}
        
        try {
            assertEquals(version.isInclusive(random), false);
            fail("unsupported type expected");
        } catch (org.osid.UnsupportedException ue) {}
        
        try {
            assertEquals(random.isInclusive(version), false);
            fail("unsupported type expected");
        } catch (org.osid.UnsupportedException ue) {}
        
        try {
            assertEquals(version.isExclusive(random), false);
            fail("unsupported type expected");
        } catch (org.osid.UnsupportedException ue) {}
        
        try {
            assertEquals(random.isExclusive(version), false);
            fail("unsupported type expected");
        } catch (org.osid.UnsupportedException ue) {}
    
        return;
    }


    /**
     *  This data may be overidden to constrain to a specific Version
     *  scheme. The OUTER data and the INNER data are aligned across the
     *  elements.
     */

    protected String[][] INCLUSION_OUTER_DATA = {
        {"1"},
        {"1", "0"},
        {"1", "1"},
        {"1", "2", "4rc"},
        {"1", "2", "3", "4", "5", "6", "7", "8"},
        {"9", "1", "8", "2", "7ABC", "3", "6", "4"}
    };

    protected String[][] INCLUSION_INNER_DATA = {
        {"1", "1"},
        {"1", "0", "9"},
        {"1", "1", "XYZ"},
        {"1", "2", "4rc", "0"},
        {"1", "2", "3", "4", "5", "6", "7", "8", "9"},
        {"9", "1", "8", "2", "7ABC", "3", "6", "4", "5"}
    };

    protected String[][] INCLUSION_NOT_INNER_DATA = {
        {"2", "1"},
        {"1", "1"},
        {"1", "2", "0"},
        {"1", "2", "4rd"},
        {"1", "2", "3", "4", "5", "6", "7"},
        {"9", "1", "8", "2", "7ABC", "3"}
    };


    @DataProvider(name = "inclusions")
    public Object[][] getInclusionData() {
        Object[][] ret = new Object[INCLUSION_OUTER_DATA.length][3];

        int i = 0;
        for (String[] c : INCLUSION_OUTER_DATA) {
            ret[i++][0] = makeVersion(c);
        }

        i = 0;
        for (String[] c : INCLUSION_INNER_DATA) {
            ret[i++][1] = makeVersion(c);
        }

        i = 0;
        for (String[] c : INCLUSION_NOT_INNER_DATA) {
            ret[i++][2] = makeVersion(c);
        }

        return (ret);
    }


    @Test(dataProvider="inclusions")
    public void testInclusions(Version outer, Version inner, Version other) {
        assertEquals(inner.isNewer(outer), true);
        assertEquals(outer.isNewer(inner), false);

        assertEquals(inner.isOlder(outer), false);
        assertEquals(outer.isOlder(inner), true);

        assertEquals(inner.isInclusive(outer), false);
        assertEquals(outer.isInclusive(inner), true);

        assertEquals(inner.isInclusive(other), false);

        return;
    }


    /**
     *  This data may be overidden to constrain to a specific Version
     *  scheme. The OUTER data and the INNER data are aligned across the
     *  elements.
     */

    protected String[][] EXCLUSION_DATA = {
        {"1"},
        {"1", "0"},
        {"1", "1"},
        {"1", "2", "3"},
        {"1", "2", "4rc"},
        {"1", "2", "3", "4", "5", "6", "7", "8", "9"}
    };

    protected String[][] EXCLUSION_TRUE_DATA = {
        {"9"},
        {"1", "1"},
        {"2", "1"},
        {"918", "1"},
        {"1", "2", "4"},
        {"1", "2", "3", "4", "4", "6", "7", "8", "9"}
    };

    protected String[][] EXCLUSION_FALSE_DATA = {
        {"1", "1"},
        {"1"},
        {"1", "1"},
        {"1", "2", "3", "9", "9"},
        {"1", "2", "4rc"},
        {"1", "2", "3", "4", "5", "6", "7", "8", "9", "100000", "999999"}
    };


    @DataProvider(name = "exclusions")
    public Object[][] getExclusionData() {
        Object[][] ret = new Object[EXCLUSION_DATA.length][3];

        int i = 0;
        for (String[] c : EXCLUSION_DATA) {
            ret[i++][0] = makeVersion(c);
        }

        i = 0;
        for (String[] c : EXCLUSION_TRUE_DATA) {
            ret[i++][1] = makeVersion(c);
        }

        i = 0;
        for (String[] c : EXCLUSION_FALSE_DATA) {
            ret[i++][2] = makeVersion(c);
        }

        return (ret);
    }


    @Test(dataProvider="exclusions")
    public void testExclusions(Version ref, Version exclusive, Version nonexclusive) {
        assertEquals(ref.equals(exclusive), false);
        assertEquals(exclusive.equals(ref), false);

        assertEquals(ref.isExclusive(exclusive), true);
        assertEquals(exclusive.isExclusive(ref), true);

        assertEquals(ref.isExclusive(nonexclusive), false);
        assertEquals(nonexclusive.isExclusive(ref), false);

        return;
    }
}
