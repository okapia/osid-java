//
// AbstractTestDuration.java
//
//     A unit test for a Duration
//
//
// Tom Coppeto
// Okapia
// 2 May 2012
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.calendaring.test;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;
import org.testng.annotations.*;

import org.osid.type.Type;
import org.osid.calendaring.Duration;
import org.osid.calendaring.DateTimeResolution;
import net.okapia.osid.primordium.type.BasicType;

import net.okapia.osid.primordium.calendaring.NewEarthTimeDefinition;
import net.okapia.osid.primordium.calendaring.CalendarDefinition;
import net.okapia.osid.primordium.calendaring.spi.AbstractConfigurableDuration;


public abstract class AbstractTestDuration {

    /**
     *  Override to run this test for a different implementation.
     */

    protected abstract Duration makeDuration(String dateStr);


    @DataProvider(name = "validity")
    public abstract Object[][] getValidData();


    @Test(dataProvider="validity")
    public void testValidity(String input, String output) {
        Duration duration    = makeDuration(input);

        assertNotNull(duration.getCalendarType());
        assertNotNull(duration.getTimeType());
        assertNotNull(duration.getGranularity());
        assertEquals(duration.getGranularityMultiplier() > 0, true);

        assertNotNull(duration.getAeons());
        assertNotNull(duration.getEpochs());
        assertNotNull(duration.getMillennia());
        assertNotNull(duration.getCenturies());
        assertNotNull(duration.getBluemoons());
        assertNotNull(duration.getYears());

        assertNotNull(duration.getMonths());
        assertNotNull(duration.getWeeks());
        assertNotNull(duration.getDays());

        assertNotNull(duration.getHours());
        assertNotNull(duration.getMinutes());
        assertNotNull(duration.getSeconds());

        assertNotNull(duration.getMilliseconds());
        assertNotNull(duration.getMilliseconds());
        assertNotNull(duration.getMicroseconds());
        assertNotNull(duration.getMicroseconds());
        assertNotNull(duration.getNanoseconds());
        assertNotNull(duration.getNanoseconds());
        assertNotNull(duration.getPicoseconds());
        assertNotNull(duration.getPicoseconds());
        assertNotNull(duration.getFemtoseconds());
        assertNotNull(duration.getFemtoseconds());
        assertNotNull(duration.getAttoseconds());
        assertNotNull(duration.getAttoseconds());
        assertNotNull(duration.getZeptoseconds());
        assertNotNull(duration.getZeptoseconds());
        assertNotNull(duration.getYoctoseconds());
        assertNotNull(duration.getYoctoseconds());
        assertNotNull(duration.getXoxxoseconds());
        assertNotNull(duration.getXoxxoseconds());
        assertNotNull(duration.getWeebleseconds());
        assertNotNull(duration.getWeebleseconds());
        assertNotNull(duration.getVatoseconds());
        assertNotNull(duration.getVatoseconds());
        assertNotNull(duration.getUndaseconds());
        assertNotNull(duration.getUndaseconds());
        assertNotNull(duration.getPlanckSeconds());
        assertNotNull(duration.getPlanckSeconds());

        assertEquals(duration.toString().toLowerCase(), output.toLowerCase());
        return;
    }


    @DataProvider(name = "invalidity")
    public abstract Object[][] getInvalidData();


    @Test(dataProvider="invalidity")
    public void testInvalidity(String input) {
        try {
            Duration duration = makeDuration(input);
            fail(input + " should be invalid");
        } catch (org.osid.InvalidArgumentException iae) {}

        return;
    }


    @DataProvider(name = "precision")
    public abstract Object[][] getPrecisionData();


    @Test(dataProvider="precision")
    public void testPrecision(String input, DateTimeResolution granularity, long multiplier) {
        Duration duration = makeDuration(input);        
        return;
    }


    @DataProvider(name = "comparisons")
    public abstract Object[][] getComparisonData();


    @Test(dataProvider="comparisons")
    public void testComparisons(String oldDurationStr, String newDurationStr) {
        Duration oldDuration = makeDuration(oldDurationStr);
        Duration newDuration = makeDuration(newDurationStr);

        assertEquals(oldDuration.equals(newDuration), false);
        assertEquals(newDuration.equals(oldDuration), false);
        
        assertEquals(oldDuration.isGreater(newDuration), false);
        assertEquals(newDuration.isGreater(oldDuration), true);
        
        assertEquals(oldDuration.isLess(newDuration), true);
        assertEquals(newDuration.isLess(oldDuration), false);

        return;
    }


    @Test(dataProvider="comparisons")
    public void testComparisonsUnequalSchemes(String oldDurationStr, String newDurationStr) {
        Duration oldDuration = makeDuration(oldDurationStr);
        Duration newDuration = makeDuration(newDurationStr);

        Duration random = new RandomDuration();

        assertEquals(oldDuration.equals(random), false);
        assertEquals(random.equals(oldDuration), false);

        try {
            assertEquals(oldDuration.isGreater(random), false);
            fail("unsupported type expected");
        } catch (org.osid.UnsupportedException ue) {}
        
        try {
            assertEquals(random.isGreater(oldDuration), false);
            fail("unsupported type expected");
        } catch (org.osid.UnsupportedException ue) {}
        
        try {
            assertEquals(oldDuration.isLess(random), false);
            fail("unsupported type expected");
        } catch (org.osid.UnsupportedException ue) {}
        
        try {
            assertEquals(random.isLess(oldDuration), false);
            fail("unsupported type expected");
        } catch (org.osid.UnsupportedException ue) {}
        
        return;
    }


    @DataProvider(name = "inclusions")    
    public abstract Object[][] getInclusionData();


    @Test(dataProvider="inclusions")
    public void testInclusions(String outerStr, String innerStr, String otherStr) {
        Duration outer = makeDuration(outerStr);
        Duration inner = makeDuration(innerStr);
        Duration other = makeDuration(otherStr);

        assertEquals(inner.isGreater(outer), false);
        assertEquals(outer.isLess(inner), false);
        assertEquals(outer.isInclusive(inner), true);
        assertEquals(inner.isInclusive(other), false);

        return;
    }


    @DataProvider(name = "exclusions")
    public abstract Object[][] getExclusionData();


    @Test(dataProvider="exclusions")
    public void testExclusions(String refStr, String exclusiveStr, String nonexclusiveStr) {
        Duration ref = makeDuration(refStr);
        Duration exclusive = makeDuration(exclusiveStr);
        Duration nonexclusive = makeDuration(nonexclusiveStr);

        assertEquals(ref.equals(exclusive), false);
        assertEquals(exclusive.equals(ref), false);

        assertEquals(ref.isExclusive(exclusive), true);
        assertEquals(exclusive.isExclusive(ref), true);

        assertEquals(ref.isExclusive(nonexclusive), false);

        return;
    }


    protected static class RandomDuration
        extends AbstractConfigurableDuration
        implements org.osid.calendaring.Duration {
        
        private static final Random r = new Random();
        private static final long serialVersionUID = 0L;


        protected RandomDuration() {
            super(new RandomCalendarDefinition(), new NewEarthTimeDefinition(),
                  org.osid.calendaring.DateTimeResolution.DAY, 1, 
                  new java.math.BigInteger(Integer.toString(r.nextInt())));

            return;
        }

        
        @Override
        protected org.osid.calendaring.Duration makeDuration(org.osid.calendaring.DateTimeResolution granularity,
                                                             long granularityMultiplier, 
                                                             java.math.BigInteger amount) {
            return (new RandomDuration());
        }
    }


    protected static class RandomCalendarDefinition 
        implements CalendarDefinition {

        private static final org.osid.type.Type TYPE = new BasicType("test", "nonsense", "stuff");

    
        @Override
        public org.osid.type.Type getCalendarType() {
            return (TYPE);
        }
        

        @Override
        public java.math.BigDecimal getYearsPerBluemoon() {
            return (new java.math.BigDecimal(42));
        }

    
        @Override
        public long getMonthsPerYear() {
            return (27);
        }

        @Override
        public java.math.BigDecimal getAverageWeeksPerYear() {
            return (new java.math.BigDecimal("99"));
        }


        @Override
        public java.math.BigDecimal getAverageDaysPerYear() {
            return (new java.math.BigDecimal("499"));
        }


        @Override
        public java.math.BigDecimal getAverageDaysPerMonth() {
            return (new java.math.BigDecimal("13"));
        }


        @Override
        public java.math.BigDecimal getAverageDaysPerWeek() {
            return (new java.math.BigDecimal("3"));
        }


        @Override
        public String getLabel(org.osid.calendaring.DateTimeResolution units) {
            return ("car");
        }
        
        
        @Override
        public String getPluralLabel(org.osid.calendaring.DateTimeResolution units) {
            return ("trees");
        }

        
        @Override
        public org.osid.calendaring.DateTimeResolution getUnits(String units)
            throws org.osid.NotFoundException {
            
            return (org.osid.calendaring.DateTimeResolution.BLUEMOON);
        }
    }
}
