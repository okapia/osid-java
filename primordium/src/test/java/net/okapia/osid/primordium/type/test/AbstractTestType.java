//
// AbstractTestType
//
//     A unit test for a Type.
//
//
// Tom Coppeto
// Okapia
// 23 September 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.type.test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import org.testng.annotations.*;

import org.osid.type.Type;


public abstract class AbstractTestType {

    @Test(dataProvider="validity")
    public void testValidity(String authority, String namespace, String identifier, 
                             String input, Type type) {

        assertNotNull(type.getAuthority());
        assertNotNull(type.getIdentifierNamespace());
        assertNotNull(type.getIdentifier());
        assertNotNull(type.getDisplayName());
        assertNotNull(type.getDisplayLabel());
        assertNotNull(type.getDescription());
        assertNotNull(type.getDomain());
        assertNotNull(type.toString());
        
        assertEquals(type.getAuthority(), authority);
        assertEquals(type.getIdentifierNamespace(), namespace);
        assertEquals(type.getIdentifier(), identifier);
        assertEquals(type.toString(), input);
        
        Type simple = new SimpleType(authority, namespace, identifier);
        assertEquals(type.getAuthority(), simple.getAuthority());
        assertEquals(type.getIdentifierNamespace(), simple.getIdentifierNamespace());
        assertEquals(type.getIdentifier(), simple.getIdentifier());
        assertEquals(type, simple);

        return;
    }


    protected static class SimpleType
        extends net.okapia.osid.primordium.type.spi.AbstractType
        implements org.osid.type.Type {

        private static final long serialVersionUID = 1L;


        public SimpleType(String authority, String namespace, String identifier) {
            super(authority, namespace, identifier);
            return;
        }

        
        @Override
        protected String formatLabel() {
            return ("hello");
        }
    }        
}
