//
// TestText
//
//     A unit test for an Text.
//
//
// Tom Coppeto
// Okapia
// 2 May 2012
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.installation.test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;
import static org.testng.Assert.assertNotNull;
import org.testng.annotations.*;

import org.osid.type.Type;
import org.osid.locale.DisplayText;
import net.okapia.osid.primordium.locale.AnyDisplayText;
import net.okapia.osid.primordium.type.BasicType;


@Test
public class TestText {

    /**
     *  Override to run this test for a different implementation.
     */

    protected DisplayText makeText(Type language, Type script, Type format, String text) {
        return (new AnyDisplayText(language, script, format, text));
    }


    /**
     *  This data may be overidden to constrain to a specific Text
     *  scheme.
     */

    protected String[][] VALIDITY_DATA = {
        {"639-2:ENG@ISO", "15924:LATN@ISO", "text:plain@okapia.net", "hello world"},
        {"639-2:FRE@ISO", "15924:LATN@ISO", "text:html@okapia.net", "bonjour tout le monde"},
        {"639-2:FRE@ISO", "15924:LATN@ISO", "text:plain@okapia.net", "bonjour tout le monde"},
        {"639-2:ARM@ISO", "15924:ARMN@ISO", "text:plain@okapia.net", "բարեւ աշխարհը"},
        {"639-2:CAT@ISO", "15924:LATN@ISO", "text:plain@okapia.net", "hola món"},
        {"639-2:CHI@ISO", "15924:HANS@ISO", "text:plain@okapia.net", "äåäç"},
        {"639-2:GRE@ISO", "15924:HANS@ISO", "text:plain@okapia.net", "γεια σας κόσμο"}
    };


    @DataProvider(name = "validity")
    public Object[][] getValidity() {
        Object[][] ret = new Object[VALIDITY_DATA.length][4];

        int i = 0;
        for (String[] c : VALIDITY_DATA) {
            ret[i][0] = BasicType.valueOf(c[0]);
            ret[i][1] = BasicType.valueOf(c[1]);
            ret[i][2] = BasicType.valueOf(c[2]);
            ret[i++][3] = c[3];
        }

        return (ret);
    }


    @Test(dataProvider="validity")
    public void testValidity(Type language, Type script, Type format, String text) {

        DisplayText dt = makeText(language, script, format, "hello world");

        assertNotNull(dt.getLanguageType());
        assertNotNull(dt.getScriptType());
        assertNotNull(dt.getFormatType());
        assertNotNull(dt.getText());

        assertNotNull(dt.toString());
        
        assertEquals(language.equals(dt.getLanguageType()), true);
        assertEquals(script.equals(dt.getScriptType()), true);
        assertEquals(format.equals(dt.getFormatType()), true);

        assertNotEquals(dt.hashCode(), 0);

        DisplayText dt2 = new AnyDisplayText(BasicType.valueOf("foo:bar@baz"), script, format, "hello world");
        assertEquals(dt.equals(dt2), false);

        return;
    }
}
