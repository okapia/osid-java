//
// TestGregorianUTCDateTime.java
//
//     A unit test for a DateTime.
//
//
// Tom Coppeto
// Okapia
// 2 May 2012
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.calendaring.test;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import org.testng.annotations.*;

import org.osid.type.Type;
import org.osid.calendaring.DateTime;
import org.osid.calendaring.DateTimeResolution;

import net.okapia.osid.primordium.calendaring.GregorianUTCDateTime;
import net.okapia.osid.primordium.calendaring.CalendarDefinition;
import net.okapia.osid.primordium.calendaring.GregorianCalendarDefinition;
import net.okapia.osid.primordium.types.calendar.CalendarTypes;
import net.okapia.osid.primordium.types.time.EarthTimeTypes;


@Test
public class TestGregorianUTCDateTime
    extends AbstractTestConfigurableDateTime {

    private static final Type CALENDAR_TYPE = CalendarTypes.GREGORIAN.getType();
    private static final Type TIME_TYPE = EarthTimeTypes.UTC.getType();


    @Override
    protected DateTime makeDateTime(String s) {
        return (GregorianUTCDateTime.valueOf(s));
    }

    protected DateTime makeDateTime(Date date) {
        return (GregorianUTCDateTime.valueOf(date));
    }


    @Override
    protected CalendarDefinition getCalendarDefinition() {
        return (new GregorianCalendarDefinition());
    }


    protected String[][] VALID_DATA = {
        {"2012", "2012"},
        {"2012-07", "2012-07"},
        {"2012-07-07", "2012-07-07"},
        {"2012-07-07T12Z", "2012-07-07T12Z"},
        {"2012-07-07T12:52Z", "2012-07-07T12:52Z"},
        {"2012-07-07T12:52:00Z", "2012-07-07T12:52:00Z"},
        {"2012-07-07T12:52:12Z", "2012-07-07T12:52:12Z"},
        {"2012-07-07T12:52:12.345Z", "2012-07-07T12:52:12.345Z"},
        {"2012-07-07T12:52:12.345678Z", "2012-07-07T12:52:12.345678Z"},
        {"2012-07-07T12:52:12.345678901Z", "2012-07-07T12:52:12.345678901Z"},
        {"2012-07-07T12:52:12.345678901234Z", "2012-07-07T12:52:12.345678901234Z"},
        {"2012-07-07T12:52:12.345678901234567Z", "2012-07-07T12:52:12.345678901234567Z"},
        {"2012-07-07T12:52:12.345678901234567890Z", "2012-07-07T12:52:12.345678901234567890Z"},
        {"2012-07-07T12:52:12.345678901234567890123Z", "2012-07-07T12:52:12.345678901234567890123Z"},
        {"2012-07-07T12:52:12.345678901234567890123456Z", "2012-07-07T12:52:12.345678901234567890123456Z"},
        {"2012-07-07T12:52:12.345678901234567890123456789Z", "2012-07-07T12:52:12.345678901234567890123456789Z"},
        {"2012-07-07T12:52:12.345678901234567890123456789012Z", "2012-07-07T12:52:12.345678901234567890123456789012Z"},
        {"2012-07-07T12:52:12.345678901234567890123456789012345Z", "2012-07-07T12:52:12.345678901234567890123456789012345Z"},
        {"2012-07-07T12:52:12.345678901234567890123456789012345678Z", "2012-07-07T12:52:12.345678901234567890123456789012345678Z"},
        /*
        {"2012-07-07T12:52:12.345678901234567890123456789012345678901Z", "2012-07-07T12:52:12.345678901234567890123456789012345678901Z"},
        {"2012-07-07T12:52:12.345678901234567890123456789012345678901234Z", "2012-07-07T12:52:12.345678901234567890123456789012345678901234Z"},
        {"2012-07-07T12:52:12.345678901234567890123456789012345678901234567Z", "2012-07-07T12:52:12.345678901234567890123456789012345678901234567Z"},
        {"2012-07-07T12:52:12.345678901234567890123456789012345678901234567890Z", "2012-07-07T12:52:12.345678901234567890123456789012345678901234567890Z"},
        */
        {"-2012", "-2012"},
        {"-2012-07", "-2012-07"},
        {"-2012-07-07", "-2012-07-07"},
        {"-2012-07-07T12Z", "-2012-07-07T12Z"},
        {"-2012-07-07T12:52Z", "-2012-07-07T12:52Z"},
        {"-2012-07-07T12:52:00Z", "-2012-07-07T12:52:00Z"},
        {"-2012-07-07T12:52:12Z", "-2012-07-07T12:52:12Z"},
        {"-2012-07-07T12:52:12.345Z", "-2012-07-07T12:52:12.345Z"},
        {"2012-07-07T23Z", "2012-07-07T23Z"},
        {"2012-07-07T23:52Z", "2012-07-07T23:52Z"},
        {"2012-07-07T23:52:00Z", "2012-07-07T23:52:00Z"},
        {"2012-07-07T23:52:12Z", "2012-07-07T23:52:12Z"},
        {"2012-07-07T23:52:12.345Z", "2012-07-07T23:52:12.345Z"},
        {"1000000000000", "+1000000000000"},
        {"-1000000000000", "-1000000000000"}
    };


    @Override
    @DataProvider(name = "validity")
    public Object[][] getValidData() {
        return (VALID_DATA);
    }


    protected Object[][] VALID_JAVA_DATA = {
        {-1000000000000000L, "-29720-11-16T22:13:20.000Z"},
        {-9876543210L, "1969-09-08T16:30:56.790Z"},
        {-1234567890L, "1969-12-17T17:03:52.110Z"},
        {-1L, "1969-12-31T23:59:59.999Z"},
        {0L, "1970-01-01T00:00:00.000Z"},
        {1L, "1970-01-01T00:00:00.001Z"},
        {1234567890L, "1970-01-15T06:56:07.890Z"},
        {9876543210L, "1970-04-25T07:29:03.210Z"},
        {1000000000000000L, "+33658-09-27T01:46:40.000Z"}
    };


    @DataProvider(name = "validjava")
    public Object[][] getValidJavaData() {
        return (VALID_JAVA_DATA);
    }


    @Test(dataProvider="validjava")
    public void testGregorian(long time, String output) {
        DateTime datetime = makeDateTime(new Date(time));

        assertEquals(datetime.getCalendarType(), CALENDAR_TYPE);
        assertEquals(datetime.getTimeType(), TIME_TYPE);

        assertEquals(datetime.toString(), output);
        return;
    }


    protected String[][] INVALID_DATA = {
        {"at the sound of the beep"}
    };


    @Override
    @DataProvider(name = "invalidity")
    public Object[][] getInvalidData() {
        return (INVALID_DATA);
    }


    protected Object[][] PRECISION_DATA = {
        {"2012-08-01", DateTimeResolution.DAY, 1}
    };


    @Override
    @DataProvider(name = "precision")
    public Object[][] getPrecisionData() {
        return (PRECISION_DATA);
    }


    protected String[][] COMPARISON_DATA = {
        {"1999-12-31", "2000-01-01"}
    };


    @DataProvider(name = "comparisons")
    public Object[][] getComparisonData() {
        return (COMPARISON_DATA);
    }


    protected String[][] EXCLUSION_DATA = {
        {"2010-01-01", "2099-01-01", "2010-01-01T4:00"}
    };


    @Override
    @DataProvider(name = "exclusions")
    public Object[][] getExclusionData() {
        return (EXCLUSION_DATA);
    }


    protected String[][] INCLUSION_DATA = {
        {"2011", "2011-01-01", "2012-01-01"}
    };


    @Override
    @DataProvider(name = "inclusions")
    public Object[][] getInclusionData() {
        return (INCLUSION_DATA);
    }


    public void testUnknown() {
        DateTime datetime = GregorianUTCDateTime.unknown();
        assertEquals(datetime.getGranularity(), DateTimeResolution.INFINITY);
        
        assertEquals(datetime.getCalendarType(), CALENDAR_TYPE);
        assertEquals(datetime.getTimeType(), TIME_TYPE);

        return;
    }
}
