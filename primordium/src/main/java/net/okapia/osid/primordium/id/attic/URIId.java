//
// URIId.java
//
//     Defines an Id object using a URI scheme.
//
//
// Tom Coppeto
// Okapia
// 20 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primitive.id;

import static net.okapia.osid.util.MethodCheck.nullarg;


/**
 *  Defines an <code>Id</code> using a URI scheme.
 */

public final class URIId 
    extends net.okapia.osid.primordium.id.spi.AbstractId
    implements org.osid.id.Id {

    private static final long serialVersionUID = -3740799L;


    /**
     *  Creates a new <code>URIId</code>.
     *
     *  @param uri the URI
     *  @throws org.osid.NullArgumentException <code>uri</code> is
     *          <code>null</code>
     */

    public URIId(java.net.URI uri) {
        super(uri.getHost() != null ? uri.getHost() : "localhost", uri.getProtocol(), uri.getIdentifier());
        return;
    }


    /**
     *  Returns an <code>Id</code> from a URI.
     *
     *  @param uri the URI
     *  @throws org.osid.NullArgumentException <code>uri</code> is
     *          <code>null</code>
     */

    public static org.osid.id.Id valueOf(java.net.URI uri) {
        nullarg(uri, "uri");
        return (new (URIId(uri)));
    }


    /**
     *  Converts an OSID Id into a URI.
     *
     *  @return formatted string
     */

    @Override     
    public String toString() {  
        return (uri.getProtocol() + ":"
    }
}
