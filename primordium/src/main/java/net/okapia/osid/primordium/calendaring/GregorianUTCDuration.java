//
// GregorianUTCDuration.java
//
//     A duration for Gregorian clanedars and UTC times.
//
//
// Tom Coppeto
// Okapia
// 2 July 2012
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.calendaring;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A duration for Gregorian calendars and UTC times.
 */

public final class GregorianUTCDuration
    extends net.okapia.osid.primordium.calendaring.spi.AbstractConfigurableDuration
    implements org.osid.calendaring.Duration {

    private static final long serialVersionUID = -179855505L;
    

    /**
     *  Constructs a new unknown <code>GregorianUTCDuration</code>.
     */

    public GregorianUTCDuration() {        
        this(org.osid.calendaring.DateTimeResolution.INFINITY, 1, 
             new java.math.BigInteger("0"), org.osid.calendaring.DateTimeResolution.INFINITY, 
             1, 1); 
        return;
    }


    /**
     *  Constructs a new <code>GregorianUTCDuration</code>.
     * 
     *  @param granularity the granularity
     *  @param amount the duration amount
     *  @throws org.osid.InvalidArgumentException
     *          <code>granularityMultiplier</code> is negative
     *  @throws org.osid.NullArgumentException
     *          code>granularity</code>, or <code>amount</code>
     *          <code>null</code>
     */

    public GregorianUTCDuration(org.osid.calendaring.DateTimeResolution granularity, 
                                java.math.BigInteger amount) {
        
        this(granularity, 1, amount); 
        return;
    }


    /**
     *  Constructs a new <code>GregorianUTCDuration</code>.
     * 
     *  @param granularity the granularity
     *  @param granularityMultiplier the granularity multiplier
     *  @param amount the duration amount
     *  @throws org.osid.InvalidArgumentException
     *          <code>granularityMultiplier</code> is negative
     *  @throws org.osid.NullArgumentException
     *          code>granularity</code>, or <code>amount</code>
     *          <code>null</code>
     */

    public GregorianUTCDuration(org.osid.calendaring.DateTimeResolution granularity, 
                                long granularityMultiplier, java.math.BigInteger amount) {
        
        super(new GregorianCalendarDefinition(), new UTCTimeDefinition(),
              granularity, granularityMultiplier, amount);
 
        return;
    }


    /**
     *  Constructs a new <code>GregorianUTCDuration</code>.
     * 
     *  @param granularity the resolution
     *  @param granularityMultiplier the resolution multiplier
     *  @param amount the amount of the duration
     *  @param uncertaintyUnits the units of uncertainty
     *  @param uncertaintyPlus uncertainty in the positive direction
     *  @param uncertaintyMinus uncertainty in the negative direction
     *  @throws org.osid.InvalidArgumentException
     *          <code>granularityMultiplier</code,
     *          <code>uncertaintyPlus</code>,
     *          <code>uncertaintyMinus</code> is negative
     *  @throws org.osid.NullArgumentException
     *          <code>granularity</code>, or
     *          <code>uncertaintyUnits</code> is <code>null</code>
     */

    public GregorianUTCDuration(org.osid.calendaring.DateTimeResolution granularity, 
                                long granularityMultiplier, java.math.BigInteger amount,
                                org.osid.calendaring.DateTimeResolution uncertaintyUnits, 
                                long uncertaintyPlus, long uncertaintyMinus) {
        
        super(new GregorianCalendarDefinition(), new UTCTimeDefinition(),
              granularity, granularityMultiplier, amount, uncertaintyUnits, 
              uncertaintyPlus, uncertaintyMinus);
 
        return;
    }


    @Override
    protected org.osid.calendaring.Duration makeDuration(org.osid.calendaring.DateTimeResolution granularity,
                                                         long granularityMultiplier, 
                                                         java.math.BigInteger amount) {

        return (new GregorianUTCDuration(granularity, granularityMultiplier, amount));
    }


    /**
     *  Parses a string into a duration.
     *
     *  @param str a string to parse
     *  @return a duration
     *  @throws org.osid.InvalidArgumentException cannot parse string
     *  @throws org.osid.NullArgumentException <code>str</code> is
     *          <code>null</code>
     */

    public static org.osid.calendaring.Duration valueOf(String str) {
        return (new GregorianUTCDuration().parseDuration(str));
    }


    /**
     *  Creates an unknown duration.
     *
     *  @return an unknown duration
     */

    public static org.osid.calendaring.Duration unknown() {
        return (new GregorianUTCDuration());
    }
}
