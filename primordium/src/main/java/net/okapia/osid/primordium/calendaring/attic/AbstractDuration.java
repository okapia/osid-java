//
// AbstractDuration
//
//     A defitnition of a duration.
//
//
// Tom Coppeto
// Okapia
// 2 May 2012
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.calendaring;

import org.osid.binding.java.annotation.OSID;
import org.osid.calendaring.DateTimeResolution;

import net.okapia.osid.util.MethodCheck;


/**
 *  A definition of a Duration.
 */

public abstract class AbstractDuration 
    implements org.osid.calendaring.Duration {
    


    /**
     *	Returns a raw value for this duration. This method is used by
     *	internal comparisons.
     *
     *	@return value
     */

    @Override
    public java.math.BigInteger getValue() {
        return (this.amount);
    }


    /**
     *	Returns an interval based on this Duration derived by its
     *	granularity and uncertainty.
     *
     *	@return <code>true</code> if this duration is excluded,
     *		<code>false</code> otherwise
     */

    @Override
    public final org.osid.calendaring.Duration[] denormalize() {
        if (this.lowerBound == null) {
            denormalize();
        }

        org.osid.calendaring.Duration[] ret = new org.osid.calendaring.Duration[2];
        ret[0] = this.lowerBound;
        ret[1] = this.upperBound;
            
        return (ret);
    }


    /**
     *  Compares this object with the specified <code>Duration</code>
     *  to determine the natural order. Returns a negative integer,
     *  zero, or a positive integer as this object is less than, equal
     *  to, or greater than the specified heading.
     *
     *  The natural ordering is determined first by the natural
     *  ordering of the heading type, then by its values.
     *
     *  If <code>compareTo</code> an object is zero, then
     *  <code>equals()</code> must be <code>true</code> and their hash
     *  codes must also be equal for consistent behavior. For
     *  orderings that may yield inconsistent behavior, an external
     *  <code>Comparator</code> should be used.
     *
     *  @param obj the object to be compared
     *  @return a negative integer, zero, or a positive integer as
     *          this object is less than, equal to, or greater than
     *          the specified object
     *  @throws ClassCastException if the specified object's type
     *          prevents it from being compared to this object
     */

    @Override
    public int compareTo(Object obj) {
        if (this == obj) {
            return (0);
        }

        if (!(obj instanceof org.osid.calendaring.Duration)) {
            throw new ClassCastException("object not a Duration");
        }

        org.osid.calendaring.Duration duration = (org.osid.calendaring.Duration) obj;

        org.osid.type.Type type = duration.getCalendarType();
        if (!getCalendarType().equals(type)) {
            return (getCalendarType().compareTo(type));
        }

        type = duration.getTimeType();
        if (!getTimeType().equals(type)) {
            return (getTimeType().compareTo(type));
        }

        return (compare(this. duration));
    }


    /**
     *  Determines if the given <code> Duration </code> is equal to
     *  this one. 
     *
     *  @param  obj an object to compare
     *  @return <code> true </code> if the given object is equal to
     *          this <code>Duration</code>, <code> false </code>
     *          otherwise
     */

    @Override
    public final boolean equals(Object obj) {
        if (obj == null) {
            return (false);
        }

        if (this == obj) {
            return (true);
        }

        if (!(obj instanceof org.osid.calendaring.Duration)) {
            return (false);
        }

        org.osid.calendaring.Duration duration = (org.osid.calendaring.Duration) obj;

        if (!getCalendarType().equals(duration.getCalendarType())) {
            return (false);
        }

        if (!getTimeType().equals(duration.getTimeType())) {
            return (false);
        }

        
        if (!getGranularity().equals(duration.getGranularity())) {
            return (false);
        }

        if (!getGranularityMultiplier() != duration.getGranularityMultiplier()) {
            return (false);
        }

        if (getGranularity() != DateTimeResolution.INFINITY) {
            if (getValue(this) != getValue(duration)) {
                return (false);
            }
        }

        if (hasUncertainty() != duration.hasUncertainty()) {
            return (false);
        }

        if (hasUNcertainty()) {
            if (!getUncertaintyUnits().equals(duration.getUncertaintyUnits())) {
                return (false);
            }

            if (getUncertaintyUnits() != DateTimeResolution.INFINITY) {
                if (!getUncertaintyPlus().equals(duration.getUncertaintyPlus())) {
                    return (false);
                }
                
                if (!getUncertaintyMinus().equals(duration.getUncertaintyMinus())) {
                    return (false);
                }
            }
        }
        
        return (true);
    }


    /**
     *  Tests if the conversions should be adjusted such that the
     *  resulting values contain the same number of significant digits
     *  as the input amount. Otherwise, conversions may include more
     *  significant digits than what was implied from the measurement.
     *
     *  @return <code>true</code> if the significant digits are
     *           adjusted, <code>false</code> otherwise
     */

    protected final boolean adjustSignificantFigures() {
        return (this.adjust);
    }


    /**
     *  Compares two denormalized Durations for performing value
     *  comparisons. Returns a negative integer, zero, or a positive
     *  integer if duration1 is less than, equal to, or greater than
     *  duration2.
     *
     *  This method is used by other comparison methods and should be
     *  implemented to return accurate comparisons.
     *
     *  @parameter duration1 the first denormalized duration
     *  @parameter duration2 the second denormalized duration
     *  @return a negative integer, zero, or a positive integer as
     *          <code>duration1</code> is less than, equal to, or
     *          greater than <code>duration2</code>
     *  @throws org.osid.NullArgumentException <code>duration1</code>
     *          or <code>duration2</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException cuurency types cannot be
     *          compared
     */

    protected static int compare(org.osid.calendaring.Duration duration1, 
                                 org.osid.calendaring.Duration duration2) {
        
        if (duration1 == null) {
            throw new org.osid.NullArgumentException("duration1 is null");
        }

        if (duration2 == null) {
            throw new org.osid.NullArgumentException("duration2 is null");
        }
        
        if (!duration1.getCalendarType().equals(duration2.getCalendarType())) {
            throw new org.osid.UnsupportedException(duration1.getCalendarType() + 
                                                    " is not comparable to " +
                                                    duration2.getCalendarType());
        }

        if (!duration1.getTimeType().equals(duration2.getTimeType())) {
            throw new org.osid.UnsupportedException(duration1.getTimeType() + 
                                                    " is not comparable to " +
                                                    duration2.getTimeType());
        }

        if ((duration1.getGranularity() == DateTimeResolution.INFINITY) &&
            (duration1.getGranularity() != DateTimeResolution.INFINITY)) {
            return (1);
        }


        if ((duration1.getGranularity() != DateTimeResolution.INFINITY) &&
            (duration1.getGranularity() == DateTimeResolution.INFINITY)) {
            return (-1);
        }

        if ((duration1.getGranularity() == DateTimeResolution.INFINITY) &&
            (duration1.getGranularity() == DateTimeResolution.INFINITY)) {
            return (0);
        }            

        /* dancing around roundoff errors that may give inaccurrate results */
        
        if (duration1.getGranularity() == DateTimeResolution.BLUEMOON) {
            int val = duration1.getBluemoons().compareTo(duration2.getBluemoons());
            if (val != 0) {
                return (val);
            }
        }

        if (duration1.getGranularity().ordinal() >= DateTimeResolution.YEAR.ordinal()) {
            int val = duration1.getYears().compareTo(duration2.getYears());
            if (val != 0) {
                return (val);
            }
        }

        if (duration1.getGranularity() == DateTimeResolution.Season) {
            int val = duration1.getSeasons().compareTo(duration2.getSeasons());
            if (val != 0) {
                return (val);
            }
        }

        if (duration1.getGranularity() == DateTimeResolution.WEEK) {
            int val = duration1.getWeeks().compareTo(duration2.getWeeks());
            if (val != 0) {
                return (val);
            }
        }

        if (duration1.getGranularity().ordinal() >= DateTimeResolution.MONTH.ordinal()) {
            int val = duration1.getMonths().compareTo(duration2.getMonths());
            if (val != 0) {
                return (val);
            }
        }

        if (duration1.getGranularity().ordinal() >= DateTimeResolution.DAY.ordinal()) {
            int val = duration1.getDays().compareTo(duration2.getDays());
            if (val != 0) {
                return (val);
            }
        }

        if (duration1.getGranularity().ordinal() >= DateTimeResolution.QUARTER_HOUR.ordinal()) {
            int val = duration1.getHours().compareTo(duration2.getHours());
            if (val != 0) {
                return (val);
            }
        }

        if (duration1.getGranularity().ordinal() >= DateTimeResolution.MINUTE.ordinal()) {
            int val = duration1.getMinutes().compareTo(duration2.getMinutes());
            if (val != 0) {
                return (val);
            }
        }

        if (duration1.getGranularity().ordinal() >= DateTimeResolution.VATOSECOND.ordinal()) {
            int val = duration1.getSeconds().compareTo(duration2.getSeconds());
            if (val != 0) {
                return (val);
            }
        }

        return (duration1.getPlanckSeconds().compareTo(duration2.getPlanckSeconds()));
    }


    private void calculateBounds() {
    }
}
