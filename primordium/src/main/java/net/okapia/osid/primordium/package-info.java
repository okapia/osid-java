/**
 * This package contains the basic implementions and tools for {@link
 * org.osid.OsidPrimitive}s. 
 *
 * OsidPrimitives are constructed by both OSID Consumers and OSID
 * Providers and are accessed separately from other implementation
 * tools. OsidPrimitives are treated in a similar fashion as one would
 * treat a String or Boolean in Java. 
 *
 * OsidPrimitives have the following characteristics:
 *
 * <dl>
 *     <dt>Immutable</dt> <dd>Once an OsidPrimitive is constructed,
 *     its values should not change underneath the consumer of the
 *     OsidPrimitive interface.</dd>
 *
 *     <dt>Serializable</dt> <dd>OsidPrimitives can be persisted and
 *     used at a later time. OsidPrimitive implementations may also
 *     support serialization and deserialization using
 *     <code>toString()</code> and <code>valueOf()</code> methods.
 *
 *     <dt>Comparable</dt> <dd>OsidPrimitives may implement
 *     <code>compareTo</code> to provide a natural ordering that
 *     should not be confused with a value-based comparison. Other
 *     comparison methods provide a more specific means of value
 *     comparison. <code>equals()</code> is a literal equality check,
 *     however, it is possible for two OsidPrimitives to be equal in
 *     value whose data members differ.</dd>
 * </dl> 
 *
 * In addition to OsidPrimitive implemntations, this package also
 * defines a bunch of common (and not so common) Types to be used with
 * various OsidPrimitives such as Calendar, Coordinate, and
 * Currency. These Types can be accessed using the Java Enums in the
 * net.okapia.osid.primordium.types package.
 *
 * The net.okapia.osid.primordium.tools package contains various tools
 * to manage sets of OsidPrimitives.
 */

package net.okapia.osid.osid.primordium;
