//
// Distance
//
//     An implementation of a Primitive for OSID Distances.
//
//
// Tom Coppeto
// Okapia
// 21 October 2011
//
// Copyright (c) 2011 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.mapping;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;

import java.math.BigDecimal;


/**
 *  An implementation of Primitive for OSID Distances.
 */

public final class Distance 
    extends net.okapia.osid.primordium.mapping.spi.AbstractDistance
    implements org.osid.mapping.Distance {

    private static final long serialVersionUID = 117700136L;


    /**
     *  Constructs a new <code>Distance</code>.
     *
     *  @param meters the distance in meters
     */

    public Distance(long meters) {
        this (new java.math.BigDecimal(meters));
        return;
    }


    /**
     *  Constructs a new <code>Distance</code>.
     *
     *  @param meters the distance in meters
     *  @throws org.osid.NullArgumentException <code>meters</code>
     *          is <code>null</code>
     */

    public Distance(BigDecimal meters) {
        super(meters, org.osid.mapping.DistanceResolution.METER);
        return;
    }


    /**
     *  Constructs a new <code>Distance</code>.
     *
     *  @param amount the distance
     *  @param units the units
     *  @throws org.osid.NullArgumentException <code>units</code> is
     *          <code>null</code>
     */

    public Distance(long amount, org.osid.mapping.DistanceResolution units) {
        this (new java.math.BigDecimal(amount), units);
        return;
    }


    /**
     *  Constructs a new <code>Distance</code>.
     *
     *  @param amount the distance
     *  @param units the units
     *  @throws org.osid.NullArgumentException <code>amount</code> or
     *          <code>units</code> is <code>null</code>
     */

    public Distance(BigDecimal amount, org.osid.mapping.DistanceResolution units) {
        super(amount, units);
        return;
    }


    /** 
     *  Creates a new Distance based on meters.
     *
     *  @param meters the value in meters
     */

    public static org.osid.mapping.Distance valueOf(long meters) {
        return (new Distance(meters));
    }


    /** 
     *  Creates a new Distance based on meters.
     *
     *  @param meters the value in meters
     *  @throws org.osid.NullArgumentException <code>meters</code>
     *          is <code>null</code>
     */

    public static org.osid.mapping.Distance valueOf(java.math.BigDecimal meters) {
        return (new Distance(meters));
    }
}
