//
// AbstractCurrencyRangeTerm.java
//
//     Implements a CurrencyRangeTerm.
//
//
// Tom Coppeto
// Okapia
// 29 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.terms.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A basic implementation of CurrencyRangeTerm.
 */

public abstract class AbstractCurrencyRangeTerm
    extends AbstractQueryTerm
    implements org.osid.search.terms.CurrencyRangeTerm {

    private final org.osid.financials.Currency start;
    private final org.osid.financials.Currency end;


    /**
     *  Creates a new <code>AbstractCurrencyRangeTerm</code>.
     *
     *  @param start the currency start range
     *  @param end the currency end range
     *  @param match <code>true</code> if a positive term,
     *         <code>false</code> for a negative term
     *  @throws org.osid.InvalidArgumentException types are not
     *          compatible
     *  @throws org.osid.NullArgumentException <code>start</code> or
     *          <code>end</code> is <code>null</code>
     */

    public AbstractCurrencyRangeTerm(org.osid.financials.Currency start,
                                     org.osid.financials.Currency end, boolean match) {
	super(match);

        nullarg(start, "start currency range");
        nullarg(end, "end currency range");

        if (!start.getCurrencyType().equals(end.getCurrencyType())) {
            throw new org.osid.InvalidArgumentException("currencies have different types");
        }

        this.start = start;
        this.end   = end;

	return;
    }


    /**
     *  Gets the start of the currency range in the term.
     *
     *  @return the currency start
     *  @throws org.osid.IllegalStateException
     *          <code>isWildcard()</code> is <code>true</code>
     */

    @OSID @Override
    public org.osid.financials.Currency getCurrencyRangeStart() {
        return (this.start);
    }


    /**
     *  Gets the end of the currency range in the term.
     *
     *  @return the currency end
     *  @throws org.osid.IllegalStateException
     *          <code>isWildcard()</code> is <code>true</code>
     */

    @OSID @Override
    public org.osid.financials.Currency getCurrencyRangeEnd() {
        return (this.end);
    }
}
