//
// AbstractTimeRangeWildcardTerm.java
//
//     Implements a TimeRangeTerm.
//
//
// Tom Coppeto
// Okapia
// 29 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.terms.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A basic implementation of TimeRangeTerm.
 */

public abstract class AbstractTimeRangeWildcardTerm
    extends AbstractWildcardQueryTerm
    implements org.osid.search.terms.TimeRangeTerm {


    /**
     *  Creates a new <code>AbstractTimeRangeWildcardTerm</code>.
     *
     *  @param match <code>true</code> if a positive term,
     *         <code>false</code> for a negative term
     */

    public AbstractTimeRangeWildcardTerm(boolean match) {
	super(match);
	return;
    }


    /**
     *  Gets the start of the time range in the term.
     *
     *  @return the time start
     *  @throws org.osid.IllegalStateException
     *          <code>isWildcard()</code> is <code>true</code>
     */

    @OSID @Override
    public org.osid.calendaring.Time getTimeRangeStart() {
        throw new org.osid.IllegalStateException("isWildcard() is true");
    }


    /**
     *  Gets the end of the time range in the term.
     *
     *  @return the time end
     *  @throws org.osid.IllegalStateException
     *          <code>isWildcard()</code> is <code>true</code>
     */

    @OSID @Override
    public org.osid.calendaring.Time getTimeRangeEnd() {
        throw new org.osid.IllegalStateException("isWildcard() is true");
    }
}
