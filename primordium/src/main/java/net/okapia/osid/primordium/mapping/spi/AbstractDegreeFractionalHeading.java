//
// AbstractDegreeFractionalHeading.java
//
//     An implementation of Primitive for OSID Headings.
//
//
// Tom Coppeto
// Okapia
// 21 October 2011
//
// Copyright (c) 2011 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.mapping.spi;

import java.math.BigDecimal;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An implementation of Primitive for OSID Headings to provide
 *  consistent Java Object behavior across OSID implementations. All
 *  OSID Heading implementations should be a descendent of this class.
 */

public abstract class AbstractDegreeFractionalHeading 
    extends AbstractHeading
    implements org.osid.mapping.Heading {

    private String label;
    private org.osid.mapping.Heading[] outer = new org.osid.mapping.Heading[2];
    private static final org.osid.type.Type TYPE = net.okapia.osid.primordium.types.heading.HeadingTypes.DEGREE.getType();
    private static final BigDecimal ZERO         = new BigDecimal(0);
    private static final BigDecimal SIXTY       = new BigDecimal(60);
    private static final BigDecimal THREESIXTY  = new BigDecimal(360);

    private static final long serialVersionUID = 350903634L;


    /**
     *  Constructs a new <code>AbstractDegreeFractionalHeading</code>.
     *
     *  @param degrees
     *  @throws org.osid.InvalidArgumentException values are out of range
     *  @throws org.osid.NullArgumentException <code>seconds</code>
     *          <code>null</code>
     */

    protected AbstractDegreeFractionalHeading(BigDecimal degrees) {
        super(TYPE, new BigDecimal[] {degrees});

        if ((degrees.compareTo(new BigDecimal(0)) < 0) || (degrees.compareTo(new BigDecimal(360)) >= 0)) {
            throw new org.osid.InvalidArgumentException("degrees out of range");
        }

        return;
    }


    /**
     *  Constructs a new <code>AbstractDegreeFractionalHeading</code>.
     *
     *  @param degrees
     *  @param minutes
     *  @param seconds
     *  @throws org.osid.InvalidArgumentException values are out of range
     *  @throws org.osid.NullArgumentException <code>seconds</code>
     *          <code>null</code>
     */

    protected AbstractDegreeFractionalHeading(int degrees, int minutes, BigDecimal seconds) {
        super(TYPE, new BigDecimal[] {calculateHeading(degrees, minutes, seconds)});
        return;
    }


    /**
     *  Constructs a new <code>AbstractDegreeFractionalHeading</code>.
     *
     *  @param degrees
     *  @param minutes
     *  @param seconds
     *  @param uncertaintyPlus the uncertainty in the positive direction
     *  @param uncertaintyMinus the uncertainty in the other directio
     *  @throws org.osid.InvalidArgumentException values are out of range
     *  @throws org.osid.NullArgumentException <code>seconds</code>,
     *          <code>uncertaintyPlus</code>, or
     *          <code>uncertaintyMinus</code> is <code>null</code>
     */

    protected AbstractDegreeFractionalHeading(int degrees, int minutes, BigDecimal seconds, 
                                              BigDecimal uncertaintyPlus, BigDecimal uncertaintyMinus) {

        super(TYPE, new BigDecimal[] {calculateHeading(degrees, minutes, seconds)}, 
              new BigDecimal[] {uncertaintyPlus}, new BigDecimal[] {uncertaintyMinus});
        return;
    }


    /**
     *  Creates a degree Heading based on a decimal value in degrees.
     *
     *  @value the number of degrees in the heading (0 <= value < 360)
     *  @return a heading
     *  @throws org.osid.InvalidArgumentException <code>value</code> is
     *          out of range
     *  @throws org.osid.NullArgumentException <code>value</code> is
     *          <code>null</code>
     */

    protected abstract org.osid.mapping.Heading makeHeading(BigDecimal value);


    /**
     *	Tests if this <code>Heading</code>, as specified by its
     *	uncertainty, completely includes the given
     *	<code>Heading</code> ranged by its uncertainty.
     *
     *	If either heading has an uncertainty of infinity this
     *	method returns false.
     *
     *  It is possible for <code>isInclusive()</code> and
     *  <code>isExclsuive()</code> to be both <code>false</code> among
     *  two <code>Headings</code> due to uncertainties in the
     *  values.
     *
     *	@param	heading the heading to compare
     *	@return <code>true</code> if this heading includes the given
     *		heading, <code>false</code> otherwise
     *	@throws org.osid.NullArgumentException <code>heading</code>
     *		is <code>null</code>
     *	@throws org.osid.UnsupportedException cannot compare heading
     */

    public final boolean isInclusive(org.osid.mapping.Heading heading) {
        if (!getHeadingType().equals(heading.getHeadingType())) {
            throw new org.osid.UnsupportedException(heading.getHeadingType() + " is not supported");
        }

        BigDecimal[] ourOuter = new BigDecimal[2];
        ourOuter[0] = getOuterBound()[0].getValues()[0];
        ourOuter[1] = getOuterBound()[1].getValues()[0];

        BigDecimal[] theirOuter = new BigDecimal[2];
        theirOuter[0] = heading.getOuterBound()[0].getValues()[0];
        theirOuter[1] = heading.getOuterBound()[1].getValues()[0];

        if ((ourOuter[0].compareTo(theirOuter[0]) <= 0)  && (ourOuter[1].compareTo(theirOuter[1]) >= 0)) {
            return (true);
        } else {
            return (false);
        }
    }


    /**
     *	Tests if this <code>Heading</code>, as specified by its
     *	uncertainty, does not overlap the given
     *	<code>Heading</code>.
     *
     *	If either heading has an uncertainty of infinity this
     *	method returns false.
     *
     *  It is possible for <code>isInclusive()</code> and
     *  <code>isExclsuive()</code> to be both <code>false</code> among
     *  two <code>Headings</code> due to uncertainties in the
     *  values.
     *     
     *	@param	heading the heading to compare
     *	@return <code>true</code> if this heading is excluded,
     *		<code>false</code> otherwise
     *	@throws org.osid.NullArgumentException <code>heading</code>
     *		is <code>null</code>
     *	@throws org.osid.UnsupportedException cannot compare heading
     */

    public final boolean isExclusive(org.osid.mapping.Heading heading) {
        if (!getHeadingType().equals(heading.getHeadingType())) {
            throw new org.osid.UnsupportedException(heading.getHeadingType() + " is not supported");
        }

        BigDecimal[] ourOuter = new BigDecimal[2];
        ourOuter[0] = getOuterBound()[0].getValues()[0];
        ourOuter[1] = getOuterBound()[1].getValues()[0];

        BigDecimal[] theirOuter = new BigDecimal[2];
        theirOuter[0] = heading.getOuterBound()[0].getValues()[0];
        theirOuter[1] = heading.getOuterBound()[1].getValues()[0];

        if ((ourOuter[0].compareTo(theirOuter[1]) > 0)  || (ourOuter[1].compareTo(theirOuter[0]) < 0)) {
            return (true);
        } else {
            return (false);
        }
    }


    /**
     *  Gets the outer bounds of the heading. The outer bounds is the
     *  heading plus and minus the uncertainty. This method returns
     *  the Headings forming the largest radius allowed by its
     *  uncertainty. 
     *
     *  For this type, the size of the returned array is 2.
     *
     *  @return the heading outer bound
     */

    public final org.osid.mapping.Heading[] getOuterBound() {
        if (this.outer[0] == null) {
            calculateBounds();
        }

        return (this.outer);
    }


    /**
     *  Returns a string representation of this Heading.
     *
     *  All implementations are encouraged to supply this method to
     *  display the heading.
     *
     *  @return a string
     */

    @Override
    public String toString() {
        if (this.label == null) {
            this.label = formatLabel();
        }

        return (this.label);
    }


    private String formatLabel() {
        StringBuilder sb = new StringBuilder();
        BigDecimal degrees = getValues()[0];
        sb.append(degrees);

        if (definesUncertainty()) {
            BigDecimal uncertaintyPlus = getUncertaintyPlus()[0];
            BigDecimal uncertaintyMinus = getUncertaintyMinus()[0];

            boolean equals = true;
            if (!uncertaintyPlus.equals(uncertaintyMinus)) {
                equals = false;
            }

            if (equals) {
                sb.append(" +/-(");
            } else {
                sb.append(" +(");
            }

            sb.append(uncertaintyPlus);

            sb.append(")");
            if (!equals) {
                sb.append(" -(");
                sb.append(uncertaintyMinus);
            }
        }

        return (sb.toString());
    }


    
    private static BigDecimal calculateHeading(int degrees, int minutes, BigDecimal seconds) {
        nullarg(seconds, "seconds");
        cardinalarg(degrees, "degrees");
        cardinalarg(minutes, "minutes");

        if (degrees > 360) {
            throw new org.osid.InvalidArgumentException("degrees out of range");
        }

        if (minutes > 60) {
            throw new org.osid.InvalidArgumentException("minutes out of range");
        }

        if ((seconds.compareTo(ZERO) < 0) || (seconds.compareTo(SIXTY) > 0)) {
            throw new org.osid.InvalidArgumentException("seconds out of range");
        }
        
        
        BigDecimal s = seconds.divide(new BigDecimal(3600), seconds.scale() + 4, java.math.RoundingMode.HALF_UP);
        BigDecimal m = (new BigDecimal(minutes)).divide(SIXTY, seconds.scale() + 6, 
                                                        java.math.RoundingMode.HALF_UP);
        BigDecimal d = new BigDecimal(degrees);

        return (d.add(m).add(s));                                                  
    }


    private void calculateBounds() {
        if (!definesUncertainty()) {
            this.outer[0] = this;
            this.outer[1] = this;
            return;
        }

        java.math.BigDecimal low  = getValues()[0].subtract(getUncertaintyMinus()[0]);
        java.math.BigDecimal high = getValues()[0].add(getUncertaintyPlus()[0]);

        this.outer[0] = makeHeading(low);
        this.outer[1] = makeHeading(high);
        
        while (low.compareTo(new BigDecimal(0)) < 0) {
            this.outer[0] = makeHeading(this.outer[0].getValues()[0].add(THREESIXTY));
        }

        if (high.compareTo(new BigDecimal(360)) >= 0) {
            this.outer[1] = makeHeading(this.outer[1].getValues()[0].remainder(THREESIXTY));
        }

        return;
    }
}
