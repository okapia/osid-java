//
// BasicId.java
//
//     Defines a simple Id object implementing the Id interface.
//
//
// Tom Coppeto
// OnTapSolutions
// 20 June 2008
//
//
// Copyright (c) 2008 Massachusetts Institute of Technology. All Rights 
// Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.id;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a basic Id.
 */

public final class BasicId 
    extends net.okapia.osid.primordium.id.spi.AbstractId 
    implements org.osid.id.Id {

    private static final long serialVersionUID = -896824535L;


    /**
     *  Creates a new <code>BasicId</code>.
     *
     *  @param authority the authority for this Id
     *  @param namespace the namespace for this Id
     *  @param identifier the identifier for this Id
     *  @throws org.osid.NullArgumentException null argument provided
     */

    public BasicId(String authority, String namespace, String identifier) {
        super(authority, namespace, identifier);
        return;
    }

   
    /**
     *  Creates a new <code>Id</code>. This method can be used to wrap
     *  an <code>Id</code> of unknown origin that supports equality
     *  and hash codes based on the unique identifiers.
     *
     *  @param id an Id
     *  @throws org.osid.NullArgumentException <code>id</code> is
     *          <code>null</code>
     */

    public static org.osid.id.Id valueOf(org.osid.id.Id id) {
        nullarg(id, "id");
        return (new BasicId(id.getAuthority(), id.getIdentifierNamespace(), id.getIdentifier()));
    }


    /**
     *  Returns an <code>Id</code> from its string representation.
     *
     *  namespace:identifier@authority
     *
     *  @param str the string to be parsed
     *  @return an <code>Id</code> with the string components
     *  @throws org.osid.InvalidArgumentException string could not be
     *          parsed
     *  @throws org.osid.NullArgumentException <code>str</code> is
     *          <code>null</code>
     */

    public static org.osid.id.Id valueOf(String str) {
        nullarg(str, "str");

        String authority;
        String namespace;
        String identifier;

        int nindex = str.indexOf(':');
        if (nindex < 0) {
            throw new org.osid.InvalidArgumentException("cannot parse " + str);
        }
        
        namespace = unescape(str.substring(0, nindex));
        
        int aindex = str.lastIndexOf('@');
        if (aindex < 0) {
            throw new org.osid.InvalidArgumentException("cannot parse " + str);
        }
        
        authority = unescape(str.substring(aindex + 1));
        identifier = unescape(str.substring(nindex + 1, aindex));

        return (new BasicId(authority, namespace, identifier));
    }


    @Override
    protected String formatLabel() {
	return (escape(getIdentifierNamespace()) + ":" + escape(getIdentifier()) + "@" + escape(getAuthority()));
    }
   

    private static String escape(String s) {
        return (s.replaceAll("%", "%25").replaceAll(":", "%3a").replaceAll("@", "%40"));
    }


    private static String unescape(String s) {
        return (s.replaceAll("%25", "%").replaceAll("%3a", ":").replaceAll("%40", "@"));
    }        
}
