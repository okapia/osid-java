//
// UnitSystemTypes.java
//
//     An enumeration of Unit Systems.
//
//
// Tom Coppeto
// Okapia
// 12 January 2023
//
//
// Copyright (c) 2023 Okpaia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.types.locale;

import static net.okapia.osid.primordium.locale.text.eng.us.Plain.text;


/**
 *  An enumeration of Unit Systems Types, if anyone cared.
 */

public enum UnitSystemTypes
    implements net.okapia.osid.torrefacto.collect.TypeEnum {

    /** SI */
    SI ("si", "International System of Units"),

    /** UCS */
    UCS ("ucs", "United States Customary Units"),

    /** Imperial */
    IMPERIAL ("imperial", "Imperial");

    private final String identifier;
    private final String name;
    private org.osid.type.Type type;

    
    UnitSystemTypes(String name) {
        this(name, name);
        return;
    }

    
    UnitSystemTypes(String identifier, String name) {
        this.identifier = identifier;
        this.name       = name;
	return;
    }
    

    /**
     *  Gets the Type.
     *
     *  @return the Type
     */

    public org.osid.type.Type getType() {
        if (this.type == null) {
            this.type = new net.okapia.osid.primordium.type.BasicTypeBuilder()
                .authority("okapia.net")
                .namespace("unitsystem")
                .identifier(this.identifier)
                .domain(text("Unit Systems"))
                .name(text(this.name))
                .label(text(this.identifier))
                .description(text("The type for the " +  this.name + " unit system."))
                .build();
        }

	return (this.type);
    }
}
