//
// AbstractHeading
//
//     An implementation of a Primitive for OSID Headings.
//
//
// Tom Coppeto
// Okapia
// 21 October 2011
//
// Copyright (c) 2011 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.mapping.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import java.math.BigDecimal;


/**
 *  An implementation of Primitive for OSID Headings.
 */

public abstract class AbstractHeading 
    implements org.osid.mapping.Heading {

    private final org.osid.type.Type headingType;
    private final long dimensions;
    private final BigDecimal[] values;
    private final BigDecimal[] uncertaintyMinus;
    private final BigDecimal[] uncertaintyPlus;
    private String label;
    private int hash = 0;

    private static final long serialVersionUID = 572069675L;


    /**
     *  Constructs a new <code>AbstractHeading</code>.
     *
     *  @param headingType the type of coordinate system
     *  @param values the heading values where the array length is
     *         the number of dimensions in this coordinate system
     *  @throws org.osid.NullArgumentException
     *          <code>headingType</code> or <code>values</code> is
     *          <code>null</code>
     */

    protected AbstractHeading(org.osid.type.Type headingType, BigDecimal[] values) {
        this(headingType, values, new BigDecimal[0], new BigDecimal[0]);
        return;
    }


    /**
     *  Constructs a new <code>AbstractHeading</code>.
     *
     *  @param headingType the type of coordinate system
     *  @param values the heading values where the array length is
     *         the number of dimensions in this coordinate system
     *  @param uncertaintyPlus the uncertainty in the positive direction
     *  @param uncertaintyMinus the uncertainty in the other direction
     *  @throws org.osid.InvalidArgumentException uncertainties have invalid dimensions
     *  @throws org.osid.NullArgumentException
     *          <code>headingType</code>, <code>values</code>,
     *          <code>uncertaintyPlus</code>, or
     *          <code>uncertaintyMinus</code> is <code>null</code>
     */

    protected AbstractHeading(org.osid.type.Type headingType, BigDecimal[] values, 
                              BigDecimal[] uncertaintyPlus, BigDecimal[] uncertaintyMinus) {

        if (headingType == null) {
            throw new org.osid.NullArgumentException("headingType is null");
        }

        if (values == null) {
            throw new org.osid.NullArgumentException("values is null");
        }

        if (uncertaintyPlus == null) {
            throw new org.osid.NullArgumentException("plus uncertainty is null");
        }

        if (uncertaintyMinus == null) {
            throw new org.osid.NullArgumentException("minus uncertainty is null");
        }

        if (uncertaintyMinus.length != uncertaintyPlus.length) {
            throw new org.osid.InvalidArgumentException("uncertainty dimension mismatch");
        }

        if ((uncertaintyPlus.length > 0) && (uncertaintyPlus.length != values.length)) {
            throw new org.osid.InvalidArgumentException("uncertainty dimension mismatch");
        }

        this.headingType = headingType;
        this.values = values.clone();
        this.dimensions = this.values.length;
        this.uncertaintyPlus = uncertaintyPlus.clone();
        this.uncertaintyMinus = uncertaintyMinus.clone();

        return;
    }


    /**
     *  Gets the <code> Type </code> of this <code> Headng </code> which 
     *  indicates the format of the heading data. 
     *
     *  @return the coordinate type 
     */

    @OSID @Override
    public final org.osid.type.Type getHeadingType() {
        return (this.headingType);
    }


    /**
     *  Gets the number of dimensions of motion. 
     *
     *  @return the number of dimensions 
     */

    @OSID @Override
    public final long getDimensions() {
        return (this.dimensions);
    }


    /**
     *  Gets the values of this heading The size of the returned array is 
     *  typically one less than <code> getDimensions(). </code> 
     *
     *  @return the heading values 
     */

    @OSID @Override
    public final BigDecimal[] getValues() {
        return (this.values.clone());
    }


    /**
     *  Tests if uncertainty is defined for this heading. 
     *
     *  @return <code> true </code> if uncertainty is defined, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public final boolean definesUncertainty() {
        if (this.uncertaintyPlus.length > 0) {
            return (true);
        }

        if (this.uncertaintyMinus.length > 0) {
            return (true);
        }

        return (false);
    }


    /**
     *  Gets the uncertainty in the negtive direction for each value of this 
     *  heading. The size of the returned array is typically one less than 
     *  <code> getDimensions(). </code> 
     *
     *  @return the negative uncertainty values 
     */

    @OSID @Override
    public final BigDecimal[] getUncertaintyMinus() {
        return (this.uncertaintyMinus.clone());
    }


    /**
     *  Gets the uncertainty in the positive direction for each value of this 
     *  heading. The size of the returned array is typically one less than 
     *  <code> getDimensions(). </code> 
     *
     *  @return the positive uncertainty values 
     */
    
    @OSID @Override
    public final BigDecimal[] getUncertaintyPlus() {
        return (this.uncertaintyPlus.clone());
    }


    /**
     *  Compares this heading with the specified <code>Heading</code>
     *  to determine the natural order. Returns a negative integer,
     *  zero, or a positive integer as this heading is less than,
     *  equal to, or greater than the specified heading.
     *
     *  The natural ordering is determined first by the natural
     *  ordering of the heading type, then by its values. This method
     *  is not useful for numeric comparisons. The ranges implied by
     *  their granularities and uncertainties may overlap in such a
     *  way that two unequal Headings may be neither less than or
     *  greater than the other.
     *
     *  If <code>compareTo</code> a heading is zero, then
     *  <code>equals()</code> must be <code>true</code> and their hash
     *  codes must also be equal for consistent behavior. For
     *  orderings that may yield inconsistent behavior, an external
     *  <code>Comparator</code> should be used.
     *
     *  @param heading the heading to be compared
     *  @return a negative integer, zero, or a positive integer as
     *          this heading is less than, equal to, or greater than
     *          the specified heading
     *  @throws ClassCastException if the specified heading's type
     *          prevents it from being compared to this heading
     */

    @OSIDBinding @Override
    public int compareTo(org.osid.mapping.Heading heading) {
        if (this == heading) {
            return (0);
        }

        int c = getHeadingType().compareTo(heading.getHeadingType());
        if (c != 0) {
            return (c);
        }

        BigDecimal[] ourValues = getValues();
        BigDecimal[] theirValues = heading.getValues();

        for (int i = 0; i < ourValues.length; i++) {
            c = ourValues[i].compareTo(theirValues[i]);
            if (c != 0) {
                return (c);
            }
        }

        return (0);
    }


    /**
     *  Determines if the given <code> Heading </code> is equal to
     *  this one. Two <code>Headings</code> are equal if their heading
     *  type and values are equal.
     *
     *  If <code>equals()</code> is <code>true</code>, then
     *  <code>compareTo()</code> must be zero and their hash codes
     *  must also be equal for consistent behavior. For orderings that
     *  may yield inconsistent behavior, an external
     *  <code>Comparator</code> should be used.
     *
     *  If <code>obj</code> is null or if a different interface, this
     *  method returns <code>false</code>.
     *
     *  @param  obj an object to compare
     *  @return <code> true </code> if the given object is equal to
     *          this <code>Heading</code>, <code> false </code>
     *          otherwise
     */

    @OSIDBinding @Override
    public final boolean equals(Object obj) {
        if (this == obj) {
            return (true);
        }

        if (obj == null) {
            return (false);
        }

        if (!(obj instanceof org.osid.mapping.Coordinate)) {
            return (false);
        }

        org.osid.mapping.Heading heading = (org.osid.mapping.Heading) obj;
        if (!getHeadingType().equals(heading.getHeadingType())) {
            return (false);
        }

        if (getDimensions() != heading.getDimensions()) {
            return (false);
        }

        if (definesUncertainty() != heading.definesUncertainty()) {
            return (false);
        }

        BigDecimal[] ourValues = getValues();
        BigDecimal[] theirValues = heading.getValues();
        for (int i = 0; i < getDimensions(); i++) {
            if (!ourValues[i].equals(theirValues[i])) {
                return (false);
            }
        }

        BigDecimal[] ourUncertaintyPlusses = getUncertaintyPlus();
        BigDecimal[] theirUncertaintyPlusses = heading.getUncertaintyPlus();
        for (int i = 0; i < getDimensions(); i++) {
            if (!ourUncertaintyPlusses[i].equals(theirUncertaintyPlusses[i])) {
                return (false);
            }
        }

        BigDecimal[] ourUncertaintyMinuses = getUncertaintyMinus();
        BigDecimal[] theirUncertaintyMinuses = heading.getUncertaintyMinus();
        for (int i = 0; i < getDimensions(); i++) {
            if (!ourUncertaintyMinuses[i].equals(theirUncertaintyMinuses[i])) {
                return (false);
            }
        }

        return (true);        
    }


    /**
     *  Returns a hash code value for this <code>Heading</code> based on
     *  the distance and time unit.
     *
     *  @return a hash code value for this object
     */

    @OSIDBinding @Override
    public final int hashCode() {
        if (this.hash == 0) {
            this.hash = calculateHashCode();
        }

        return (this.hash);
    }


    /**
     *  Returns a string representation of this Heading.
     *
     *  All implementations are encouraged to supply this method to
     *  display the heading.
     *
     *  @return a string
     */

    @OSIDBinding @Override
    public String toString() {
        if (this.label == null) {
            this.label = formatLabel();
        }

        return (this.label);
    }


    private int calculateHashCode() {
        int result = getHeadingType().hashCode();

        BigDecimal[] values = getValues();
        for (int i = 0; i < getDimensions(); i++) {
            result = result * 31 + values[i].hashCode();
        }

        if (!definesUncertainty()) {
            return (result);
        }

        BigDecimal[] up = getUncertaintyPlus();
        for (int i = 0; i < getDimensions(); i++) {
            result = result * 31 + up[i].hashCode();
        }

        BigDecimal[] um = getUncertaintyMinus();
        for (int i = 0; i < getDimensions(); i++) {
            result = result * 31 + um[i].hashCode();
        }

        return (result);
    }


    private String formatLabel() {
        StringBuilder sb = new StringBuilder();
        sb.append(getHeadingType().getDisplayLabel());
        sb.append(": ");

        BigDecimal values[] = getValues();
        for (int i = 0; i < getDimensions(); i++) {
            if (i > 0) {
                sb.append(",");
            }

            sb.append(values[i].toString());
        }

        if (definesUncertainty()) {
            BigDecimal[] uncertaintyPlusses = getUncertaintyPlus();
            BigDecimal[] uncertaintyMinuses = getUncertaintyMinus();
            boolean equals = true;
            for (int i = 0; i < getDimensions(); i++) {
                if (!uncertaintyPlusses[i].equals(uncertaintyMinuses[i])) {
                    equals = false;
                }
            }

            if (equals) {
                sb.append(" +/-(");
            } else {
                sb.append(" +(");
            }

            for (int i = 0; i < getDimensions(); i++) {
                if (i > 0) {
                    sb.append(",");
                }

                sb.append(uncertaintyPlusses[i]);
            } 

            sb.append(")");
            if (!equals) {
                sb.append(" -(");
                for (int i = 0; i < getDimensions(); i++) {
                    if (i > 0) {
                        sb.append(",");
                    }
                    
                    sb.append(uncertaintyMinuses[i]);
                } 
            }
        }

        return (sb.toString());
    }
}
