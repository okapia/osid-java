//
// AbstractDateTime.java
//
//     Simple DateTime framework.
//
//
// Tom Coppeto
// Okapia
// 21 October 2009
//
// Copyright (c) 2009,2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;
import org.osid.calendaring.DateTimeResolution;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a basic immutable DateTime interface. This object is
 *  used to copy another DateTime object into an immutable form or to
 *  provide an "unknown" time value.
 *
 *  Checking is performed to see if values correspond to valid
 *  values on any calendar.
 */

public abstract class AbstractConfigurableDateTime
    extends AbstractDateTime
    implements org.osid.calendaring.DateTime {

    private final DateTimeFactory factory;
    private int hash;

    private org.osid.calendaring.DateTime low;
    private org.osid.calendaring.DateTime high;
    private org.osid.calendaring.Time time;

    private static final long serialVersionUID = 673606037L;


    /**
     *  Constructs a new <code>AbstractConfigurableDateTime</code>.
     *
     *  @param factory the datetime factory
     *  @param specifier a datetime specifier
     *  @throws org.osid.NullArgumentException <code>factory</code> or
     *          <code>specifier</code> is <code>null</code>
     */

    protected AbstractConfigurableDateTime(DateTimeFactory factory, AbstractDateTimeSpecifier specifier) {
        super(specifier);
        this.factory = factory;
        return;
    }


    /**
     *  Tests if this DateTime is greater than the given DateTime. A
     *  greater Time is one whose values minus granularity and
     *  uncertainty is greater than another plus its granularity and
     *  uncertainty. An uncertainty of infinity is undefined and
     *  returns false.
     *
     *
     *  @param  datetime the datetime to compare
     *  @return <code>true</code> if this datetime is larger,
     *          <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException <code>datetime</code>
     *          is <code>null</code>
     *  @throws org.osid.UnsupportedException cannot compare datetime
     */

    @OSIDBinding @Override
    public boolean isGreater(org.osid.calendaring.DateTime datetime) {        
        if (!getCalendarType().equals(datetime.getCalendarType())) {
            throw new org.osid.UnsupportedException("cannot compare " + getCalendarType() + 
                                                    " to " + datetime.getCalendarType());
        }

        if (!getTimeType().equals(datetime.getTimeType())) {
            throw new org.osid.UnsupportedException("cannot compare " + getTimeType() + 
                                                    " to " + datetime.getTimeType());
        }

        org.osid.calendaring.DateTimeInterval ours = denormalize();
        org.osid.calendaring.DateTimeInterval theirs = datetime.denormalize();

	return (compare(ours.getStart(), theirs.getEnd(), getDateTimeFactory()) > 0);
    }


    /**
     *  Tests if this DateTime is less than the given DateTime. A
     *  lesser DateTime is one whose values plus granularity and
     *  uncertainty is less than another minus its uncertainty. An
     *  uncertainty of infinity is undefined and returns false.
     *
     *  @param  datetime the datetime to compare
     *  @return <code>true</code> if this datetime is less,
     *          <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException <code>datetime</code>
     *          is <code>null</code>
     *  @throws org.osid.UnsupportedException cannot compare datetime
     */

    @OSIDBinding @Override
    public final boolean isLess(org.osid.calendaring.DateTime datetime) {
        if (!getCalendarType().equals(datetime.getCalendarType())) {
            throw new org.osid.UnsupportedException("cannot compare " + getCalendarType() + 
                                                    " to " + datetime.getCalendarType());
        }

        if (!getTimeType().equals(datetime.getTimeType())) {
            throw new org.osid.UnsupportedException("cannot compare " + getTimeType() + 
                                                    " to " + datetime.getTimeType());
        }

        org.osid.calendaring.DateTimeInterval ours = denormalize();
        org.osid.calendaring.DateTimeInterval theirs = datetime.denormalize();

	return (compare(ours.getEnd(), theirs.getStart(), getDateTimeFactory()) < 0);
    }


    /**
     *  Tests if the range of this DateTime, as specified by its
     *  granularity and uncertainty, includes the given DateTime
     *  ranged by its grabularity and uncertainty.
     *
     *  If either datetime has an uncertainty of infinity this method
     *  returns false.
     *
     *  @param  datetime the datetime to compare
     *  @return <code>true</code> if this datetime is included,
     *          <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException <code>datetime</code>
     *          is <code>null</code>
     *  @throws org.osid.UnsupportedException cannot compare datetime
     */

    @OSIDBinding @Override
    public final boolean isInclusive(org.osid.calendaring.DateTime datetime) {
        if (!getCalendarType().equals(datetime.getCalendarType())) {
            throw new org.osid.UnsupportedException("cannot compare " + getCalendarType() + 
                                                    " to " + datetime.getCalendarType());
        }

        if (!getTimeType().equals(datetime.getTimeType())) {
            throw new org.osid.UnsupportedException("cannot compare " + getTimeType() + 
                                                    " to " + datetime.getTimeType());
        }

        org.osid.calendaring.DateTimeInterval ours = denormalize();
        org.osid.calendaring.DateTimeInterval theirs = datetime.denormalize();

	return ((compare(ours.getStart(), theirs.getStart(), getDateTimeFactory()) <= 0) &&
                (compare(ours.getEnd(), theirs.getEnd(), getDateTimeFactory()) >= 0));
    }


    /**
     *  Tests if the range of this DateTime, as specified by its
     *  granularity and uncertainty, does not overlap with the given
     *  DateTime.
     *
     *  If either datetime has an uncertainty of infinity this method
     *  returns false.
     *
     *  @param  datetime the datetime to compare
     *  @return <code>true</code> if this datetime is excluded,
     *          <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException <code>datetime</code>
     *          is <code>null</code>
     *  @throws org.osid.UnsupportedException cannot compare datetime
     */

    @OSIDBinding @Override
    public final boolean isExclusive(org.osid.calendaring.DateTime datetime) {
	return (isLess(datetime) || isGreater(datetime));
    }


    /**
     *  Returns an interval based on this DateTime derived by its
     *  granularity and uncertainty.
     *
     *  @return <code>true</code> if this datetime is excluded,
     *          <code>false</code> otherwise
     */

    @OSIDBinding @Override
    public final org.osid.calendaring.DateTimeInterval denormalize() {
        if (this.low == null) {
            this.low  = calculateLowerBound();
        }

        if (this.high == null) {
            this.high = calculateUpperBound();
        }

        return (new net.okapia.osid.primordium.calendaring.DateTimeInterval(this.low, this.high));
    }


    /**
     *  Compares this DateTime with the specified DateTime for ordering.
     *
     *  @param datetime the DateTime to be compared
     *  @return a negative integer, zero, or a positive integer as
     *          this DateTime is less than, equal to, or greater than
     *          the specified DateTime
     *  @throws org.osid.UnsupportedException cannot compare calendar
     */

    @OSIDBinding @Override
    public int compareTo(org.osid.calendaring.DateTime datetime) {
        if (this == datetime) {
            return (0);
        }

        int c = getCalendarType().compareTo(datetime.getCalendarType());
        if (c != 0) {
            return (c);
        }

        if (getGranularity() == DateTimeResolution.INFINITY) {
            if (datetime.getGranularity() == DateTimeResolution.INFINITY) {
                return (0);
            } else {
                return (1);
            }
        } 

        if (datetime.getGranularity() == DateTimeResolution.INFINITY) {
            return (-1);
        } 
        
	if (getAeon() > datetime.getAeon()) {
            return (1);
        }

        if (getAeon() < datetime.getAeon()) {
            return (-1);
        }

        if (getGranularity() == DateTimeResolution.AEON) {
            if (datetime.getGranularity() == DateTimeResolution.AEON) {
                return (0);
            } else {
                return (-1);
            }
        } 

        if (datetime.getGranularity() == DateTimeResolution.AEON) {
            return (1);
        } 
        
	if (getEpoch() > datetime.getEpoch()) {
            return (1);
        }

        if (getEpoch() < datetime.getEpoch()) {
            return (-1);
        }

        if (getGranularity() == DateTimeResolution.EPOCH) {
            if (datetime.getGranularity() == DateTimeResolution.EPOCH) {
                return (0);
            } else {
                return (-1);
            }
        } 

        if (datetime.getGranularity() == DateTimeResolution.EPOCH) {
            return (1);
        } 

	if (getMillennium() > datetime.getMillennium()) {
            return (1);
        }

        if (getMillennium() < datetime.getMillennium()) {
            return (-1);
        }

        if (getGranularity() == DateTimeResolution.GLACIAL) {
            if (datetime.getGranularity() == DateTimeResolution.GLACIAL) {
                return (0);
            } else {
                return (-1);
            }
        } 

        if (datetime.getGranularity() == DateTimeResolution.GLACIAL) {
            return (1);
        } 

        if (getGranularity() == DateTimeResolution.MILLENNIA) {
            if (datetime.getGranularity() == DateTimeResolution.MILLENNIA) {
                return (0);
            } else {
                return (-1);
            }
        } 

        if (datetime.getGranularity() == DateTimeResolution.MILLENNIA) {
            return (1);
        } 

	if (getCentury() > datetime.getCentury()) {
            return (1);
        }

        if (getCentury() < datetime.getCentury()) {
            return (-1);
        }

        if (getGranularity() == DateTimeResolution.CENTURY) {
            if (datetime.getGranularity() == DateTimeResolution.CENTURY) {
                return (0);
            } else {
                return (-1);
            }
        } 

        if (datetime.getGranularity() == DateTimeResolution.CENTURY) {
            return (1);
        } 

	if (getYear() > datetime.getYear()) {
            return (1);
        }

        if (getYear() < datetime.getYear()) {
            return (-1);
        }

        if (getGranularity() == DateTimeResolution.DECADE) {
            if (datetime.getGranularity() == DateTimeResolution.DECADE) {
                return (0);
            } else {
                return (-1);
            }
        } 

        if (datetime.getGranularity() == DateTimeResolution.DECADE) {
            return (1);
        } 

        if (getGranularity() == DateTimeResolution.BLUEMOON) {
            if (datetime.getGranularity() == DateTimeResolution.BLUEMOON) {
                return (0);
            } else {
                return (-1);
            }
        } 

        if (datetime.getGranularity() == DateTimeResolution.BLUEMOON) {
            return (1);
        } 

        if (getGranularity() == DateTimeResolution.YEAR) {
            if (datetime.getGranularity() == DateTimeResolution.YEAR) {
                return (0);
            } else {
                return (-1);
            }
        } 

        if (datetime.getGranularity() == DateTimeResolution.YEAR) {
            return (1);
        } 
        
	if (getMonth() > datetime.getMonth()) {
            return (1);
        }

        if (getMonth() < datetime.getMonth()) {
            return (-1);
        }

        if (getGranularity() == DateTimeResolution.MONTH) {
            if (datetime.getGranularity() == DateTimeResolution.MONTH) {
                return (0);
            } else {
                return (-1);
            }
        } 

        if (datetime.getGranularity() == DateTimeResolution.MONTH) {
            return (1);
        } 

	if (getDay() > datetime.getDay()) {
            return (1);
        }

        if (getDay() < datetime.getDay()) {
            return (-1);
        }

        if (getGranularity() == DateTimeResolution.DAY) {
            if (datetime.getGranularity() == DateTimeResolution.DAY) {
                return (0);
            } else {
                return (-1);
            }
        } 

        if (datetime.getGranularity() == DateTimeResolution.DAY) {
            return (1);
        } 

        org.osid.calendaring.Time time = getDateTimeFactory().makeTime(datetime);
        return (getTime().compareTo(time));
    }


    /**
     *  Determines if the given <code> Time </code> is equal to this
     *  one.
     *
     *  @param  obj an object to compare
     *  @return <code> true </code> if the given object is equal to
     *          this <code>Time</code>, <code> false </code> otherwise
     */

    @OSIDBinding @Override
    public final boolean equals(Object obj) {
        if (obj == null) {
            return (false);
        }

        if (this == obj) {
            return (true);
        }

        if (!(obj instanceof org.osid.calendaring.DateTime)) {
            return (false);
        }

        org.osid.calendaring.DateTime datetime = (org.osid.calendaring.DateTime) obj;
        
	if (!getCalendarType().equals(datetime.getCalendarType())) {
	    return (false);
	}
        
	if (getGranularity() != datetime.getGranularity()) {
	    return (false);
	}

        /* if uncertainty is defined, but is zero, it doesn't count */
        if ((definesUncertainty() && ((getUncertaintyPlus() > 0) ||
                                      getUncertaintyMinus() > 0)) != 
            (datetime.definesUncertainty() && ((datetime.getUncertaintyPlus() > 0) ||
                                               datetime.getUncertaintyMinus() > 0))) {
            return (false);
        } else if (definesUncertainty() && datetime.definesUncertainty()) {
            if (getUncertaintyUnits().equals(datetime.getUncertaintyUnits())) {
                return (false);
            }

            if (getUncertaintyPlus() != datetime.getUncertaintyPlus()) {
                return (false);
            }

            if (getUncertaintyMinus() != datetime.getUncertaintyMinus()) {
                return (false);
            }
        }

	if (getGranularity().ordinal() ==  DateTimeResolution.INFINITY.ordinal()) {
	    return (true);
	}

	if (getAeon() != datetime.getAeon()) {
	    return (false);
	}

	if (getGranularity() == DateTimeResolution.AEON) {
	    return (true);
	}
        
	if (getEpoch() != datetime.getEpoch()) {
	    return (false);
	}

	if (getGranularity() == DateTimeResolution.EPOCH) {
	    return (true);
	}

        if (getGranularity() == DateTimeResolution.GLACIAL) {
            if ((int) getMillennium()/100 == (int) datetime.getMillennium()/100) {
                return (true);
            }
        }

        if (getMillennium() != datetime.getMillennium()) {
            return (false);
        }

        if (getGranularity() == DateTimeResolution.MILLENNIA) {
            return (true);
        }

        if (getCentury() != datetime.getCentury()) {
            return (false);
        }

        if (getGranularity() == DateTimeResolution.CENTURY) {
            return (true);
        }

        if (getGranularity() == DateTimeResolution.DECADE) {
            if ((int) getYear()/10 == (int) datetime.getYear()/10) {
                return (true);
            } else {
                return (false);
            }
        }

        if (getGranularity() == DateTimeResolution.BLUEMOON) {
            java.math.BigDecimal bm = getDateTimeFactory().getCalendarDefinition().getYearsPerBluemoon();
            java.math.BigDecimal years1 = new java.math.BigDecimal(getYear()).divide(bm, 0, 
                                                                                     java.math.RoundingMode.DOWN);
            java.math.BigDecimal years2 = new java.math.BigDecimal(datetime.getYear()).divide(bm, 0,
                                                                                              java.math.RoundingMode.DOWN);
            return (years1.equals(years2));
        }

        if (getYear() != datetime.getYear()) {
            return (false);
        }

        if (getGranularity() == DateTimeResolution.YEAR) {
            return (true);
        }

        if (getMonth() != datetime.getMonth()) {
            return (false);
        }

        if (getGranularity() == DateTimeResolution.MONTH) {
            return (true);
        }

        if (getDay() != datetime.getDay()) {
            return (false);
        }

        if (getGranularity() == DateTimeResolution.DAY) {
            return (true);
        }

        org.osid.calendaring.Time time = factory.makeTime(datetime);
	return (getTime().equals(time));
    }


    /**
     *  Returns a hash code value for this <code>Time</code> based
     *  on the type, granularity and values.
     *
     *  @return a hash code value for this object
     */

    @OSIDBinding @Override
    public final int hashCode() { 
        if (this.hash == 0) {
            this.hash = calculateHashCode();
        }

        return (this.hash);
    }


    /**
     *  Compares two normalized finite datetimes. If the granularities
     *  differ the comparison is performed using the least granular
     *  date, such that January 1985 equals 1985.
     *
     *  @param dt1 a <code>DateTime</code> to compare     
     *  @param dt2 a <code>DateTime</code> to compare
     *  @param factory the datetime factory
     *  @return <code> 1 </code> if this is greater than
     *          <code>dt</code>, -1 if this is less than
     *          <code>dt</code>, 0 if equal
     *  @throws org.osid.NullArgumentException <code>dt1</code>,
     *          <code>dt2</code> or <code>factory</code> is
     *          <code>null</code>
     *  @throws org.osid.UnsupportedException not the same calendar
     */

    protected static int compare(org.osid.calendaring.DateTime dt1, 
                                 org.osid.calendaring.DateTime dt2,
                                 DateTimeFactory factory) {

        nullarg(dt1, "datetime 1");
        nullarg(dt2, "datetime 2");
        nullarg(factory, "datetime factory");

        if (!dt1.getCalendarType().equals(dt2.getCalendarType())) {
            throw new org.osid.UnsupportedException("cannot compare " + dt1.getCalendarType()
                                                    + " and " + dt2.getCalendarType());
        }

        if (!dt1.getTimeType().equals(dt2.getTimeType())) {
            throw new org.osid.UnsupportedException("cannot compare " + dt1.getTimeType()
                                                    + " and " + dt2.getTimeType());
        }

        if (dt1.getAeon() > dt2.getAeon()) {
            return (1);
        }

        if (dt1.getAeon() < dt2.getAeon()) {
            return (-1);
        }

        if ((dt1.getGranularity() == DateTimeResolution.AEON) || 
            (dt2.getGranularity() == DateTimeResolution.AEON)) {
            return (0);
        }

        if (dt1.getEpoch() > dt2.getEpoch()) {
            return (1);
        }

        if (dt1.getEpoch() < dt2.getEpoch()) {
            return (-1);
        }

        if ((dt1.getGranularity() == DateTimeResolution.EPOCH) || 
            (dt2.getGranularity() == DateTimeResolution.EPOCH)) {
            return (0);
        }

        if (((int) dt1.getMillennium()/100) > ((int) dt2.getMillennium()/100)) {
            return (1);
        }

        if (((int) dt1.getMillennium()/100) < ((int) dt2.getMillennium()/100)) {
            return (-1);
        }

        if ((dt1.getGranularity() == DateTimeResolution.GLACIAL) || 
            (dt2.getGranularity() == DateTimeResolution.GLACIAL)) {
            return (0);
        }

        if (dt1.getCentury() > dt2.getCentury()) {
            return (1);
        }

        if (dt1.getCentury() < dt2.getCentury()) {
            return (-1);
        }

        if ((dt1.getGranularity() == DateTimeResolution.CENTURY) || 
            (dt2.getGranularity() == DateTimeResolution.CENTURY)) {
            return (0);
        }

        if (((int) dt1.getYear()/10) > ((int) dt2.getYear()/10)) {
            return (1);
        }

        if (((int) dt1.getYear()/10) < ((int) dt2.getYear()/10)) {
            return (-1);
        }

        if ((dt1.getGranularity() == DateTimeResolution.DECADE) || 
            (dt2.getGranularity() == DateTimeResolution.DECADE)) {
            return (0);
        }

        java.math.BigDecimal bm = factory.getCalendarDefinition().getYearsPerBluemoon();
        java.math.BigDecimal years1 = new java.math.BigDecimal(dt1.getYear()).divide(bm, 0, 
                                                                                     java.math.RoundingMode.DOWN);
        java.math.BigDecimal years2 = new java.math.BigDecimal(dt2.getYear()).divide(bm, 0,                   
                                                                                     java.math.RoundingMode.DOWN);

        int res = years1.compareTo(years2);
        if (res != 0) {
            return (res);
        }

        if ((dt1.getGranularity() == DateTimeResolution.BLUEMOON) ||
            (dt2.getGranularity() == DateTimeResolution.BLUEMOON)) {
            return (0);
        }

        if (dt1.getYear() > dt2.getYear()) {
            return (1);
        }

        if (dt1.getYear() < dt2.getYear()) {
            return (-1);
        }

        if ((dt1.getGranularity() == DateTimeResolution.YEAR) ||
            (dt2.getGranularity() == DateTimeResolution.YEAR)) {
            return (0);
        }

        if (dt1.getMonth() > dt2.getMonth()) {
            return (1);
        }

        if (dt1.getMonth() < dt2.getMonth()) {
            return (-1);
        }

        if ((dt1.getGranularity() == DateTimeResolution.MONTH) ||
            (dt2.getGranularity() == DateTimeResolution.MONTH)) {
            return (0);
        }

        if (dt1.getDay() > dt2.getDay()) {
            return (1);
        }

        if (dt1.getDay() < dt2.getDay()) {
            return (-1);
        }

        if ((dt1.getGranularity() == DateTimeResolution.DAY) ||
            (dt2.getGranularity() == DateTimeResolution.DAY)) {
            return (0);
        }

        org.osid.calendaring.Time time1 = factory.makeTime(dt1);
        org.osid.calendaring.Time time2 = factory.makeTime(dt2);
        return (time1.compareTo(time2));
    }


    private int calculateHashCode() {
        int hash = getCalendarType().hashCode();        
        hash = hash * 31 + getTimeType().hashCode();

        hash = hash * 31 + getGranularity().hashCode();
        hash = hash * 31 + (int) getGranularityMultiplier();
        
        if (definesUncertainty()) {
            hash = hash * 31 + getUncertaintyUnits().hashCode();
            hash = hash * 31 + (int) getUncertaintyPlus();
            hash = hash * 31 + (int) getUncertaintyMinus();
        }

	if (getGranularity() == DateTimeResolution.INFINITY) {
	    return (hash);
	}

	hash = hash * 31 + (int) getHour();
	if (getGranularity() == DateTimeResolution.HOUR) {
	    return (hash);
	}

	hash = hash * 31 + (int) getMinute();
	if (getGranularity() == DateTimeResolution.MINUTE) {
	    return (hash);
	}

	hash = hash * 31 + (int) getSecond();
	if (getGranularity() == DateTimeResolution.SECOND) {
	    return (hash);
	}

	hash = hash * 31 + (int) getMilliseconds();
	if (getGranularity() == DateTimeResolution.MILLISECOND) {
	    return (hash);
	}

	hash = hash * 31 + (int) getMicroseconds();
	if (getGranularity() == DateTimeResolution.MICROSECOND) {
	    return (hash);
	}

	hash = hash * 31 + (int) getNanoseconds();
	if (getGranularity() == DateTimeResolution.NANOSECOND) {
	    return (hash);
	}

	hash = hash * 31 + (int) getPicoseconds();
	if (getGranularity() == DateTimeResolution.PICOSECOND) {
	    return (hash);
	}

	hash = hash * 31 + (int) getFemtoseconds();
	if (getGranularity() == DateTimeResolution.FEMTOSECOND) {
	    return (hash);
	}

	hash = hash * 31 + (int) getAttoseconds();
	if (getGranularity() == DateTimeResolution.ATTOSECOND) {
	    return (hash);
	}

	hash = hash * 31 + (int) getZeptoseconds();
	if (getGranularity() == DateTimeResolution.ZEPTOSECOND) {
	    return (hash);
	}

	hash = hash * 31 + (int) getYoctoseconds();
	if (getGranularity() == DateTimeResolution.YOCTOSECOND) {
	    return (hash);
	}

	hash = hash * 31 + (int) getXoxxoseconds();
	if (getGranularity() == DateTimeResolution.XOXXOSECOND) {
	    return (hash);
	}

	hash = hash * 31 + (int) getWeebleseconds();
	if (getGranularity() == DateTimeResolution.WEEBLESECOND) {
	    return (hash);
	}

	hash = hash * 31 + (int) getVatoseconds();
	if (getGranularity() == DateTimeResolution.VATOSECOND) {
	    return (hash);
	}

	hash = hash * 31 + (int) getUndaseconds();
	if (getGranularity() == DateTimeResolution.UNDASECOND) {
	    return (hash);
	}

	hash = hash * 31 + (int) getPlanckSeconds();
	if (getGranularity() == DateTimeResolution.PLANCKSECOND) {
	    return (hash);
	}

        return (hash);
    }

    
    /**
     *  Returns a string representation of this DateTime.
     *
     *  @return a string
     */

    @Override
    public String toString() {
        if (getGranularity() == DateTimeResolution.INFINITY) {
            return ("undefined");
        }

        if (getGranularity() == DateTimeResolution.AEON) {
            return (formatNumber(getAeon()) + " aeon");
        }

        if (getGranularity() == DateTimeResolution.EPOCH) {
            return (formatNumber(getEpoch()) + " epoch");
        }

        if (getGranularity() == DateTimeResolution.GLACIAL) {
            return (formatNumber(getMillennium()/100) + " ice age");
        }

        if (getGranularity() == DateTimeResolution.MILLENNIA) {
            return (formatNumber(getMillennium()) + " millennium");
        }

        if (getGranularity() == DateTimeResolution.CENTURY) {
            return (formatNumber(getCentury()) + " century");
        }

        if (getGranularity() == DateTimeResolution.DECADE) {
            return (((int) getYear()/10) + "0s");
        }

        if (getGranularity() == DateTimeResolution.BLUEMOON) {
            java.math.BigDecimal bm = getDateTimeFactory().getCalendarDefinition().getYearsPerBluemoon();
            java.math.BigDecimal years = new java.math.BigDecimal(getYear()).divide(bm, 0, 
                                                                                    java.math.RoundingMode.DOWN);
            return (years + " blue moon");
        }

        if (getGranularity() == DateTimeResolution.YEAR) {
            return (formatYear(getYear()) + "");
        }

        StringBuilder s = new StringBuilder(16);
        s.append(formatYear(getYear()));
        s.append("-");
        s.append(formatDigits(getMonth(), 2));

        if (getGranularity() == DateTimeResolution.MONTH) {
            return (s.toString());
        }

        s.append("-");
        s.append(formatDigits(getDay(), 2));

        if (getGranularity() == DateTimeResolution.DAY) {
            return (s.toString());
        }

        s.append("T");
        s.append(getTime().toString());
        
        return (s.toString() + "Z");
    }


    /**
     *  Gets the datetime factory.
     *
     *  @return the factory
     */

    protected DateTimeFactory getDateTimeFactory() {
        return (this.factory);
    }


    /**
     *  Gets the time component.
     *
     *  @return the time
     */

    protected org.osid.calendaring.Time getTime() {
        if (this.time == null) {
            this.time = getDateTimeFactory().makeTime(this);
        }

        return (this.time);
    }



    protected static String formatYear(long year) {
        if ((year >= 0) && (year < 10000)) {
            return (formatDigits(year, 4));
        }

        if ((year >= -10000) && (year < 0)) {
            return (formatDigits(year, 4));
        }

        if (year > 0) {
            return ("+" + year);
        } else {
            return (Long.toString(year));
        }
    }


    protected static String formatDigits(long num, int n) {
        StringBuilder sb = new StringBuilder(n);
        sb.append(num);
        if (sb.length() < n) {
            for (int i = 0; i < n - sb.length(); i++) {
                sb.insert(0, "0");
            }
        }
        
        return (sb.toString());
    }


    protected static String formatNumber(long num) {
        String s = String.valueOf(num);
        switch (s.charAt(s.length() -1)) {
        case 1: return (s + "st");
        case 2: return (s + "nd");
        case 3: return (s + "rd");
        default: return (s + "th");
        }
    }


    /**
     *  Parses an ISO date string.
     *
     *  @param iso the iso formatted string
     *  @return a Gregorian date
     *  @throws org.osid.InvalidArgumentException could not 
     *          parse <code>iso</code>
     *  @throws org.osid.NullArgumentException <code>iso</code>
     *          is <code>null</code>
     */

    protected static org.osid.calendaring.DateTime parseISO(DateTimeFactory factory, String iso) {
        nullarg(iso, "iso string");

        org.osid.calendaring.DateTimeResolution res;
        AbstractDateTimeSpecifier specifier = factory.getDateTimeSpecifier();

        int ad = 1;
        if (iso.startsWith("-")) {
            iso = iso.substring(1);
            ad = -1;
        }

        String[] parts     = iso.split("T| ");
        String[] dateParts = parts[0].split("-");
        
        try {
            specifier.setYear(Long.parseLong(dateParts[0]) * ad);

            if (dateParts.length > 1) {
                specifier.setMonth(Long.parseLong(dateParts[1]));

                if (dateParts.length > 2) {
                    specifier.setDay(Long.parseLong(dateParts[2]));
                }
            }
        } catch (NumberFormatException nfe) {
            throw new org.osid.InvalidArgumentException("cannot parse date " + iso, nfe);
        }

        if (parts.length > 1) {
            String[] timeParts = parts[1].split("Z");
            specifier.setTime(net.okapia.osid.primordium.calendaring.UTCTime.valueOf(timeParts[0]));
        }

        return(factory.makeDateTime(specifier));
    }


    private org.osid.calendaring.DateTime calculateLowerBound() {
        AbstractDateTimeSpecifier spec = this.factory.getDateTimeSpecifier();
        spec.setTime(getTime().denormalizeTime()[0]);
        spec.setGranularity(DateTimeResolution.PLANCKSECOND);

        if (getGranularity().ordinal() <= DateTimeResolution.DAY.ordinal()) {
            spec.setDay(getDay());
        }

        if (getGranularity().ordinal() <= DateTimeResolution.MONTH.ordinal()) {
            spec.setMonth(getMonth());
        }
        
            
        if (getGranularity().ordinal() <= DateTimeResolution.DECADE.ordinal()) {
            spec.setYear(getYear());
        }

        switch (getGranularity()) {
        case INFINITY:
            spec.setAeon(Long.MIN_VALUE);
            break;
        case AEON:
            spec.setAeon(getAeon());
            break;
        case EPOCH:
            spec.setEpoch(getEpoch());
            break;
        case GLACIAL:
        case MILLENNIA:
            spec.setMillennium(getMillennium());
            break;
        case CENTURY:
            spec.setCentury(getCentury());
            break;
        }

        return (this.factory.makeDateTime(spec));
    }


    private org.osid.calendaring.DateTime calculateUpperBound() {
        AbstractDateTimeSpecifier spec = this.factory.getDateTimeSpecifier();
        spec.setTime(getTime().denormalizeTime()[1]);
        spec.setGranularity(DateTimeResolution.PLANCKSECOND);

        if (getGranularity().ordinal() > DateTimeResolution.DAY.ordinal()) {
            spec.setDay(30);
        } else {
            spec.setDay((int) getDay());
        }

        if (getGranularity().ordinal() > DateTimeResolution.MONTH.ordinal()) {
            spec.setMonth(getDateTimeFactory().getCalendarDefinition().getMonthsPerYear());
        } else {
            spec.setMonth(getMonth());
        }

        if (getGranularity().ordinal() <= DateTimeResolution.YEAR.ordinal()) {
            spec.setYear(getYear());
        }

        switch (getGranularity()) {
        case BLUEMOON:
            break;
        case DECADE:
            break;
        case CENTURY:
            break;
        case MILLENNIA:
            break;
        case GLACIAL:
            break;
        case EPOCH:
            break;
        case AEON:
            break;
        case INFINITY:
            spec.setAeon(Long.MAX_VALUE);
            break;
        }

        return (this.factory.makeDateTime(spec));
    }
}
