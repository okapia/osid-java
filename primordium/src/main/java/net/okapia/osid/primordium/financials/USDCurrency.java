//
// USDCurrency.java
//
//     USD Currency with arbitrary precision.
//
//
// Tom Coppeto
// OnTapSolutions
// 21 October 2011
//
// Copyright (c) 2011 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.financials;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;

import java.math.BigDecimal;
import java.math.RoundingMode;

import net.okapia.osid.primordium.types.currency.ISOCurrencyTypes;


/**
 *  USD Currency with arbitrary precision. The amounts can be any
 *  decimal number greater than or equal to zero. 
 *
 *  A USDCurrency may have a Precision where input amounts are rounded
 *  to DOLLAR, CENT, or MILL. ARBITRARY Precision will leave the
 *  input amounts alone.
 *
 *  A USDCurrency supports two output formats. One using the $ SYMBOL
 *  and the other using a USD suffix. 
 */

public final class USDCurrency
    extends net.okapia.osid.primordium.financials.spi.AbstractCurrency
    implements org.osid.financials.Currency {

    private final Precision precision;
    private final Format format;
    private String label;

    private static final long serialVersionUID = -650751985L;


    /**
     *  Constructs a new <code>USDCurrency</code>. The default
     *  precision is Precision.CENT and default output format is
     *  Format.SYMBOL.
     *
     *  @param amount the currency amount in cents
     *  @throws org.osid.InvalidArgumentException <code>amount</code> is
     *          negative 
     *  @throws org.osid.NullArgumentException <code>amount</code> is
     *          <code>null</code>
     */

    public USDCurrency(BigDecimal amount) {
        this(amount, Precision.CENT, Format.SYMBOL);
        return;
    }


    /**
     *  Constructs a new <code>USDCurrency</code> of the specified
     *  precision and format. 
     *
     *  @param amount the currency amount in decimal dollars
     *  @param format the output format
     *  @throws org.osid.InvalidArgumentException <code>amount</code> is
     *          negative
     *  @throws org.osid.NullArgumentException <code>amount</code>,
     *          <code>precision</code> or <code>format</code> is
     *          <code>null</code>
     */

    public USDCurrency(BigDecimal amount, Precision precision, Format format) {
        super(ISOCurrencyTypes.USD.getType(), round(amount, precision));

        nullarg(format, "currency output format");
        this.format = format;
        this.precision = precision;

        return;
    }


    private static BigDecimal round(BigDecimal amount, Precision precision) {
        nullarg(amount, "amount");
        nullarg(precision, "precision");

        if (amount.compareTo(new BigDecimal(0)) < 0) {
            throw new org.osid.InvalidArgumentException("amount is negative");
        }

        switch (precision) {
        case DOLLAR:
            return (amount.setScale(0, RoundingMode.HALF_UP));
        case CENT:
            return (amount.setScale(2, RoundingMode.HALF_UP));
        case MILL:
            return (amount.setScale(3, RoundingMode.HALF_UP));
        default:
            return (amount);
        }
    }


    /**
     *  Returns a string representation of this Currency.
     *
     *  @return a string
     */

    @OSIDBinding @Override
    public String toString() {
        if (this.label == null) {
            this.label = formatLabel();
        }

	return (this.label);
    }


    private String formatLabel() {
        String pattern;

        switch (this.precision) {
        case DOLLAR:
            pattern = "###,###";
            break;
        case CENT:
            pattern = "###,###.00";
            break;
        case MILL:
            pattern = "###,###.000";
            break;
        default:
            pattern = "###,###.#";
            break;
        }

        java.text.DecimalFormat formatter = new java.text.DecimalFormat(pattern);
        String output = formatter.format(getAmount());

        switch (this.format) {
        case SYMBOL:
            output = "$" + output;
            break;
        case USD:
            output = output + " USD";
            break;
        }
            
	return (output);
    }

    
    /**
     *  Makes a USDCurrency from a string. The Precision is based on
     *  the decimal places in the supplied string and the output
     *  format is determined by the symbols in the supplied string.
     *
     *  Formats are:
     *  <ul>
     *      <li>$D ($1,500)</li>
     *      <li>D USD (1,500 USD)</li>
     *      <li>$D.CC ($1,500.00)</li>
     *      <li>D.CC USD (1,500.00 USD)</li>
     *      <li>$D.CCCCC ($1,500.00000)</li>
     *      <li>D.CCCCC USD (1,500.00000 USD)</li>
     *  </ul>
     *
     *  @param str a string representation of the amount
     *  @return a Currency
     *  @throws org.osid.InvalidArgumentException cannot parse
     *          <code>str</code>
     *  @throws org.osid.NullArgumentException <code>str</code> is
     *          <code>null</code>
     */

    public static org.osid.financials.Currency valueOf(String str) {
        nullarg(str, "currency str");
        
        String number = str.trim();
        Format format = Format.NONE;
        Precision precision;

        if (number.startsWith("$")) {
            number = number.substring(1);
            format = Format.SYMBOL;
        } 

        if (number.endsWith("USD")) {
            number = number.substring(0, number.length() - 3).trim();
            format = Format.USD;
        }

        java.text.DecimalFormat formatter = new java.text.DecimalFormat("###,###.###");
        formatter.setParseBigDecimal(true);
        BigDecimal value = (BigDecimal) formatter.parse(number, new java.text.ParsePosition(0));
        
        if (value == null) {
            throw new org.osid.InvalidArgumentException(str + " parsing error");
        }

        return (valueOf(value, format));
    }


    /**
     *  Makes a USDCurrency from a BigDecimal. The Precision is based
     *  on the scale of the BigDecimal and the output format is
     *  Format.SYMBOL.
     *
     *  @param amount the currency amount
     *  @return a Currency
     *  @throws org.osid.InvalidArgumentException amount is negative
     *  @throws org.osid.NullArgumentException <code>amount</code> is
     *          <code>null</code>
     */

    public static org.osid.financials.Currency valueOf(BigDecimal amount) {
        return (valueOf(amount, Format.SYMBOL));
    }


    /**
     *  Makes a USDCurrency from a BigDecimal. The Precision is based
     *  on the scale of the BigDecimal.
     *
     *  @param amount the currency amount
     *  @param format the output format
     *  @return a Currency
     *  @throws org.osid.InvalidArgumentException amount is negative
     *  @throws org.osid.NullArgumentException <code>amount</code> or
     *          <code>format</code> is <code>null</code>
     */

    public static org.osid.financials.Currency valueOf(BigDecimal amount, Format format) {
        nullarg(amount, "amount");
        nullarg(format, "format");

        Precision precision;

        switch (amount.scale()) {
        case 0:
            precision = Precision.DOLLAR;                
            break;
        case 1:
        case 2: 
            precision = Precision.CENT;   
            break;
        case 3:
            precision = Precision.MILL;   
            break;
        default:
            precision = Precision.ARBITRARY;
        }
                
        return (new USDCurrency(amount, precision, format));        
    }


    /**
     *  Makes a USDCurrency from a number of cents. The Precision is
     *  set to Precision.CENT and the output format is Format.SYMBOL.
     *
     *  @param cents the number of cents
     *  @return a Currency
     *  @throws org.osid.InvalidArgumentException cents is negative
     */

    public static org.osid.financials.Currency valueOf(long cents) {
        return (valueOf(cents, Format.SYMBOL));
    }


    /**
     *  Makes a USDCurrency from a number of cents. The Precision is
     *  set to Precision.CENT.
     *
     *  @param cents the currency cents
     *  @param format the output format
     *  @return a Currency
     *  @throws org.osid.InvalidArgumentException cents is negative
     */

    public static org.osid.financials.Currency valueOf(long cents, Format format) {
        cardinalarg(cents, "cents");
        nullarg(format, "format");
        return (new USDCurrency((new BigDecimal(cents)).divide(new BigDecimal(100), 2,
                                                               RoundingMode.UNNECESSARY),
                                Precision.CENT, format));
    }


    /**
     *  Precision is used to specify the rounding of input values to
     *  a USD currency.
     */

    public enum Precision {
        /** Dollar precision with no decimal places. ($50) */
        DOLLAR,

        /** Cent precision with 2 decimal places. ($50.99) */
        CENT,

        /** Mill precision with 3 decimal places. ($50.995) */
        MILL,

        /** Any precision. */
        ARBITRARY;
    }


    /**
     *  Format is used to specify the output <code>toString()</code>
     *  format of a USD currency value.
     */

    public enum Format {
        /** USD currency values are prefixed with a $ symbol. ($10 or $10.00) */
        SYMBOL,

        /** USD currency values are suffixed with the currency code. (10 USD or 10.00 USD) */
        USD,

        /** USD currency values are formatted with no symbol or code. (10 10.00) */
        NONE;
    }            
}
