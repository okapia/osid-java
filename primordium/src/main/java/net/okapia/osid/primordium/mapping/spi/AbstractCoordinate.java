//
// AbstractCoordinate.java
//
//     An implementation of Primitive for OSID Coordinates.
//
//
// Tom Coppeto
// Okapia
// 21 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.mapping.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An implementation of Primitive for OSID Coordinatess to provide
 *  consistent Java Object behavior across OSID implementations. All
 *  OSID Coordinate implementations should be a descendant of this
 *  class.
 *
 *  This class is just a placeholder since the implementations will
 *  need to be responsible for implementing these methods based on
 *  their supported record Types.
 */

public abstract class AbstractCoordinate
    implements org.osid.mapping.Coordinate {

    private final org.osid.type.Type coordinateType;
    private final long dimensions;
    private final java.math.BigDecimal[] values;
    private final java.math.BigDecimal[] uncertaintyMinus;
    private final java.math.BigDecimal[] uncertaintyPlus;
    private String label;
    private int hash = 0;

    private static final long serialVersionUID = -1792417873L;


    /**
     *  Constructs a new <code>AbstractCoordinate</code>.
     *
     *  @param coordinateType the type of coordinate system
     *  @param values the coordinate values where the array length is
     *         the number of dimensions in this coordinate system
     *  @throws org.osid.NullArgumentException
     *          <code>coordinateType</code> or <code>values</code> is
     *          <code>null</code>
     */

    protected AbstractCoordinate(org.osid.type.Type coordinateType, java.math.BigDecimal[] values) {
        this(coordinateType, values, new java.math.BigDecimal[0], new java.math.BigDecimal[0]);
        return;
    }


    /**
     *  Constructs a new <code>AbstractCoordinate</code>.
     *
     *  @param coordinateType the type of coordinate system
     *  @param values the coordinate values where the array length is
     *         the number of dimensions in this coordinate system
     *  @param uncertaintyPlus the uncertainty in the positive direction
     *  @param uncertaintyMinus the uncertainty in the other direction
     *  @throws org.osid.InvalidArgumentException uncertainties have invalid dimensions
     *  @throws org.osid.NullArgumentException
     *          <code>coordinateType</code>, <code>values</code>,
     *          <code>uncertaintyPlus</code>, or
     *          <code>uncertaintyMinus</code> is <code>null</code>
     */

    protected AbstractCoordinate(org.osid.type.Type coordinateType, java.math.BigDecimal[] values, 
                                 java.math.BigDecimal[] uncertaintyPlus, java.math.BigDecimal[] uncertaintyMinus) {

        nullarg(coordinateType, "coordinateType");
        nullarg(values, "values");
        nullarg(uncertaintyPlus, "uncertainty plus");
        nullarg(uncertaintyMinus, "uncertainty minus");

        if (uncertaintyMinus.length != uncertaintyPlus.length) {
            throw new org.osid.InvalidArgumentException("uncertainty dimension mismatch");
        }

        if ((uncertaintyPlus.length > 0) && (uncertaintyPlus.length != values.length)) {
            throw new org.osid.InvalidArgumentException("uncertainty dimension mismatch");
        }

        this.coordinateType = coordinateType;
        this.values = values.clone();
        this.dimensions = this.values.length;
        this.uncertaintyPlus = uncertaintyPlus.clone();
        this.uncertaintyMinus = uncertaintyMinus.clone();

        return;
    }


    /**
     *  Gets the <code> Type </code> of this <code> Coordinate </code> which 
     *  indicates the format of the coordinate data. 
     *
     *  @return the coordinate type 
     */

    @OSID @Override
    public final org.osid.type.Type getCoordinateType() {
        return (this.coordinateType);
    }


    /**
     *  Gets the number of dimensions available in this coordinate. 
     *
     *  @return the number of dimensions 
     */

    @OSID @Override
    public final long getDimensions() {
        return (this.dimensions);
    }


    /**
     *  Gets the values of this coordinate. The size of the returned array 
     *  should equal <code> getDimensions(). </code> 
     *
     *  @return the coordinate values 
     */

    @OSID @Override
    public final java.math.BigDecimal[] getValues() {
        return (this.values);
    }


    /**
     *  Tests if uncertainty is defined for this heading. 
     *
     *  @return <code> true </code> if uncertainty is defined, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public final boolean definesUncertainty() {
        if (this.uncertaintyPlus.length > 0) {
            return (true);
        }

        if (this.uncertaintyMinus.length > 0) {
            return (true);
        }

        return (false);
    }


    /**
     *  Gets the uncertainty in the negtive direction for each value of this 
     *  coordinate. The size of the returned array is typically one less than 
     *  <code> getDimensions(). </code> 
     *
     *  @return the negative uncertainty values 
     */

    @OSID @Override
    public final java.math.BigDecimal[] getUncertaintyMinus() {
        return (this.uncertaintyMinus.clone());
    }


    /**
     *  Gets the uncertainty in the positive direction for each value of this 
     *  coordinate. The size of the returned array is typically one less than 
     *  <code> getDimensions(). </code> 
     *
     *  @return the positive uncertainty values 
     */

    @OSID @Override
    public final java.math.BigDecimal[] getUncertaintyPlus() {
        return (this.uncertaintyPlus.clone());
    }


    /**
     *  Compares this coordinate with the specified
     *  <code>Coordinate</code> to determine the natural
     *  order. Returns a negative integer, zero, or a positive integer
     *  as this coordinate is less than, equal to, or greater than the
     *  specified coordinate.
     *
     *  The natural ordering is determined first by the natural
     *  ordering of the coordinate type, then by its values. This
     *  method is not useful for numeric comparisons. The ranges
     *  implied by their granularities and uncertainties may overlap
     *  in such a way that two unequal Coordinates may be neither less
     *  than or greater than the other.
     *
     *  If <code>compareTo</code> a coordinate is zero, then
     *  <code>equals()</code> must be <code>true</code> and their hash
     *  codes must also be equal for consistent behavior. For
     *  orderings that may yield inconsistent behavior, an external
     *  <code>Comparator</code> should be used.
     *
     *  @param coordinate the coordinate to be compared
     *  @return a negative integer, zero, or a positive integer as
     *          this coordinate is less than, equal to, or greater than
     *          the specified coordinate
     *  @throws ClassCastException if the specified coordinate's type
     *          prevents it from being compared to this coordinate
     */

    @Override
    public int compareTo(org.osid.mapping.Coordinate coordinate) {
        if (this == coordinate) {
            return (0);
        }

        if (!getCoordinateType().equals(coordinate.getCoordinateType())) {
            return (getCoordinateType().compareTo(coordinate.getCoordinateType()));
        }

        if (getDimensions() < coordinate.getDimensions()) {
            return (-1);
        } else if (getDimensions() > coordinate.getDimensions()) {
            return (1);
        }

        java.math.BigDecimal[] ours = getValues();
        java.math.BigDecimal[] theirs = coordinate.getValues();
        
        for (int i = 0; i < getDimensions(); i++) {
            if (!ours[i].equals(theirs[i])) {
                return (ours[i].compareTo(theirs[i]));
            }
        }

        return (0);
    }


    /**
     *  Determines if the given <code> Coordinate </code> is equal to
     *  this one. Two <code>Coordinates</code> are equal if their
     *  coordinate types, values, and uncertainties are equal.
     *
     *  If <code>equals()</code> is <code>true</code>, then
     *  <code>compareTo()</code> must be zero and their hash codes
     *  must also be equal for consistent behavior. For orderings that
     *  may yield inconsistent behavior, an external
     *  <code>Comparator</code> should be used.
     *
     *  If <code>obj</code> is null or if a different interface, this
     *  method returns <code>false</code>.
     *
     *  @param  obj an object to compare
     *  @return <code> true </code> if the given object is equal to
     *          this <code>Coordinate</code>, <code> false </code>
     *          otherwise
     */

    @Override
    public final boolean equals(Object obj) {
        if (this == obj) {
            return (true);
        }

        if (obj == null) {
            return (false);
        }

        if (!(obj instanceof org.osid.mapping.Coordinate)) {
            return (false);
        }

        org.osid.mapping.Coordinate coordinate = (org.osid.mapping.Coordinate) obj;
        if (!getCoordinateType().equals(coordinate.getCoordinateType())) {
            return (false);
        }

        if (getDimensions() != coordinate.getDimensions()) {
            return (false);
        }

        if (definesUncertainty() != coordinate.definesUncertainty()) {
            return (false);
        }

        java.math.BigDecimal[] ourValues = getValues();
        java.math.BigDecimal[] theirValues = coordinate.getValues();
        for (int i = 0; i < getDimensions(); i++) {
            if (!ourValues[i].equals(theirValues[i])) {
                return (false);
            }
        }

        java.math.BigDecimal[] ourUncertaintyPlusses = getUncertaintyPlus();
        java.math.BigDecimal[] theirUncertaintyPlusses = coordinate.getUncertaintyPlus();
        for (int i = 0; i < getDimensions(); i++) {
            if (!ourUncertaintyPlusses[i].equals(theirUncertaintyPlusses[i])) {
                return (false);
            }
        }

        java.math.BigDecimal[] ourUncertaintyMinuses = getUncertaintyMinus();
        java.math.BigDecimal[] theirUncertaintyMinuses = coordinate.getUncertaintyMinus();
        for (int i = 0; i < getDimensions(); i++) {
            if (!ourUncertaintyMinuses[i].equals(theirUncertaintyMinuses[i])) {
                return (false);
            }
        }

        return (true);        
    }


    /**
     *  Returns a hash code value for this <code>Coordinate</code>.
     *  The hash code is determined by the Coordinate type, values,
     *  and uncertainty.
     *
     *  All implementations need to supply this method to determine
     *  the hash code among the Coordinate record Type and values.
     *
     *  @return a hash code value for this object
     */

    @Override
    public final int hashCode() {
        if (this.hash == 0) {
            this.hash = calculateHashCode();
        }

        return (this.hash);
    }


    /**
     *  Returns a string representation of this Coordinate.
     *
     *  All implementations are encouraged to supply this method to
     *  display the type and value.
     *
     *  @return a string
     */

    @Override
    public String toString() {
        if (this.label == null) {
            this.label = formatLabel();
        }

        return (this.label);
    }


    private int calculateHashCode() {
        int result = getCoordinateType().hashCode();

        java.math.BigDecimal[] values = getValues();
        for (int i = 0; i < getDimensions(); i++) {
            result = result * 31 + values[i].hashCode();
        }

        if (!definesUncertainty()) {
            return (result);
        }

        java.math.BigDecimal[] up = getUncertaintyPlus();
        for (int i = 0; i < getDimensions(); i++) {
            result = result * 31 + up[i].hashCode();
        }

        java.math.BigDecimal[] um = getUncertaintyMinus();
        for (int i = 0; i < getDimensions(); i++) {
            result = result * 31 + um[i].hashCode();
        }

        return (result);
    }


    private String formatLabel() {
        StringBuilder sb = new StringBuilder();
        sb.append(getCoordinateType().getDisplayLabel());
        sb.append(": ");

        java.math.BigDecimal[] values = getValues();
        for (int i = 0; i < getDimensions(); i++) {
            if (i > 0) {
                sb.append(",");
            }

            sb.append(values[i].toString());
        }

        if (definesUncertainty()) {
            java.math.BigDecimal[] uncertaintyPlusses = getUncertaintyPlus();
            java.math.BigDecimal[] uncertaintyMinuses = getUncertaintyMinus();
            boolean equals = true;
            for (int i = 0; i < getDimensions(); i++) {
                if (!uncertaintyPlusses[i].equals(uncertaintyMinuses[i])) {
                    equals = false;
                }
            }

            if (equals) {
                sb.append(" +/-(");
            } else {
                sb.append(" +(");
            }

            for (int i = 0; i < getDimensions(); i++) {
                if (i > 0) {
                    sb.append(",");
                }

                sb.append(uncertaintyPlusses[i]);
            } 

            sb.append(")");
            if (!equals) {
                sb.append(" -(");
                for (int i = 0; i < getDimensions(); i++) {
                    if (i > 0) {
                        sb.append(",");
                    }
                    
                    sb.append(uncertaintyMinuses[i]);
                } 
            }
        }

        return (sb.toString());
    }
}
