//
// FormatTypes.java
//
//     An enumeration of Display Text Format Types.
//
//
// Tom Coppeto
// Okapia
// 25 April 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.types.text;


/**
 *  An enumeration of DisplayText Format Types.
 */

public enum FormatTypes
    implements net.okapia.osid.torrefacto.collect.TypeEnum {

        ASCIIDOC ("AsciiDoc"),
        CREOLE ("Creole"),
        DOCBOOK ("DocBook"),
        GROFF ("Groff"),
        HTML ("HTML"),
        HTML_LATEX ("HTML+LaTeX"),
        LATEX ("LaTeX"),
        MARKDOWN ("Markdown"),
        MMD ("MultiMarkdown"),
        NROFF ("nroff"),
        PLAIN ("Plain"),
        REST ("reStructuredText"),
        RUNOFF ("RUNOFF"),
        SCRIBE ("Scribe"),
        TEX ("TeX"),
        TEXINFO ("Texinfo"),
        TEXTILE ("Textile"),
        TROFF ("troff"),
        Z ("ZFormat");
        
    private String name;
    private org.osid.type.Type type;

    FormatTypes(String name) {
        this.name = name;
	return;
    }
    

    /**
     *  Gets the Type.
     *
     *  @return the Type
     */

    public org.osid.type.Type getType() {
        if (this.type == null) {
            this.type = new net.okapia.osid.primordium.types.text.util.LazyTextTypeBuilder()
                .authority("okapia.net")
                .namespace("format.text")
                .identifier(this.name)
                .domain("DisplayText Formats")
                .name(this.name + " Text Format")
                .label(this.name)
                .description("The display text format type for the " +  this.name + " format.")
                .build();
        }

	return (this.type);
    }
}
