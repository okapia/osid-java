//
// AbstractIdSetTerm.java
//
//     Implements an IdSetTerm.
//
//
// Tom Coppeto
// Okapia
// 29 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.terms.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A basic implementation of IdSetTerm.
 */

public abstract class AbstractIdSetTerm
    extends AbstractQueryTerm
    implements org.osid.search.terms.IdSetTerm {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.LinkedHashSet<>();


    /**
     *  Creates a new <code>AbstractIdSetTerm</code>.
     *
     *  @param ids the Id values
     *  @param match <code>true</code> if a positive term,
     *         <code>false</code> for a negative term
     *  @throws org.osid.NullArgumentException <code>ids</code>
     *          is <code>null</code>
     */

    public AbstractIdSetTerm(org.osid.id.Id[] ids, boolean match) {
	super(match);

        nullarg(ids, "ids");

        for (org.osid.id.Id id : ids) {
            this.ids.add(id);
        }

	return;
    }


    /**
     *  Gets the Id values in the term.
     *
     *  @return the Id values
     *  @throws org.osid.IllegalStateException
     *          <code>isWildcard()</code> is <code>true</code>
     */

    @OSID @Override
    public org.osid.id.Id[] getIds() {
        return (this.ids.toArray(new org.osid.id.Id[this.ids.size()]));
    }
}
