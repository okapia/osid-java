//
// CalendarTypes.java
//
//     An enumeration of Calendar Types.
//
//
// Tom Coppeto
// Okapia
// 5 April 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.types.calendar;

import static net.okapia.osid.primordium.locale.text.eng.us.Plain.text;


/**
 *  An enumeration of Calendar Types.
 */

public enum CalendarTypes
    implements net.okapia.osid.torrefacto.collect.TypeEnum {
 
        /** Akan Calendar */
        AKAN ("Akan"),

        /** Bahai Calendar */
        BAHAI ("Bahai", "Bahá'í"),

        /** Bengali Calendar */
        BENGALI ("Bengali"),

        /** Berber Calendar */
        BERBER ("Berber"),

        /** Buddhist Calendar */
        BUDDHIST ("Buddhist"),

        /** Chinese Calendar */
        CHINESE ("Chinese"),

        /** Coptic Calendar */
        COPTIC ("Coptic"),

        /** Discordian Calendar */
        DISCORDIAN ("Discordian"),

        /** Ethiopian Calendar */
        ETHIOPIAN ("Ethiopian"),

        /** Gregorian Calendar */
        GREGORIAN ("Gregorian"),

        /** Hebrew Calendar */
        HEBREW ("Hebrew"),

        /** Hellenic Calendar */
        HELLENIC ("Hellenic"),

        /** Igbo Calendar */
        IGBO ("Igbo"),

        /** Indian Calendar */
        INDIAN ("Indian", "Indian National"),

        /** ISO8601 Calendar */
        ISO_8601 ("ISO8601", "ISO 8601"),

        /** ISOWeekdate Calendar */
        ISO_WEEKDATE ("ISOWeekdate", "ISO Week Date"),

        /** Islamic Calendar */
        ISLAMIC ("Islamic"),

        /** Japanese Calendar */
        JAPANESE ("Japanese"),

        /** Javanese Calendar */
        JAVANESE ("Javanese"),

        /** Juche Calendar */
        JUCHE ("Juche", "Juche Era"),

        /** Jualian Calendar */
        JULIAN ("Jualian"),

        /** Kurdish Calendar */
        KURDISH ("Kurdish"),

        /** Malayalam Calendar */
        MALAYALAM ("Malayalam"),

        /** Nanakshahi Calendar */
        NANAKSHAHI ("Nanakshahi"),

        /** Nepali Calendar */
        NEPALI ("Nepali"),

        /** NepalSambat Calendar */
        NEPAL_SAMBAT ("NepalSambat", "Nepal Sambat"),

        /** RevisedJulian Calendar */
        REVISED_JULIAN ("RevisedJulian", "Revised Julian"),

        /** Romanian Calendar */
        ROMANIAN ("Romanian"),

        /** Rumi Calendar */
        RUMI ("Rumi"),

        /** Runic Calendar */
        RUNIC ("Runic"),

        /** Tamil Calendar */
        TAMIL ("Tamil"),

        /** ThaiLunar Calendar */
        THAI_LUNAR ("ThaiLunar", "Thai Lunar"),

        /** ThaiSolar Calendar */
        THAI_SOLAR ("ThaiSolar", "Thai Solar"),

        /** Tibetan Calendar */
        TIBETAN ("Tibetan"),

        /** Xhosa Calendar */
        XHOSA ("Xhosa"),

        /** Yoruba Calendar */
        YORUBA ("Yoruba"),

        /** Zoroastrian Calendar */
        ZOROASTRIAN ("Zoroastrian");

        
    private final org.osid.type.Type type;
    

    CalendarTypes(String name) {
        this(name, name);
        return;
    }


    CalendarTypes(String identifier, String name) {
	this.type = new net.okapia.osid.primordium.type.BasicTypeBuilder()
            .authority("okapia.net")
            .namespace("calendar")
            .identifier(identifier)
            .domain(text("Calendar Types"))
            .name(text(name + " Calendar"))
            .label(text(name))
            .description(text("The calendar type for the " +  name + " calendar."))
            .build();

	return;
    }
    

    /**
     *  Gets the Type.
     *
     *  @return the Type
     */

    public org.osid.type.Type getType() {
	return (this.type);
    }
}
