//
// AbstractPolygonalSpatialUnit
//
//     An implementation of an OSID Elliptcal SpatialUnit.
//
//
// Tom Coppeto
// Okapia
// 13 May 2012
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.mapping;

import org.osid.binding.java.annotation.OSID;

import net.okapia.osid.util.MethodCheck;



    import java.math.BigDecimal;
    import java.math.BigInteger;
    import java.math.MathContext;
    import java.math.RoundingMode;
    import java.util.Deque;
    import java.util.LinkedList;
    

/**
 *  An implementation of an OSID Polygonal SpatialUnit.
 */

public abstract class AbstractPolygonalSpatialUnit
    implements org.osid.mapping.SpatialUnit {

    private final org.osid.mapping.Coordinate[] points;

    private org.osid.mapping.SpatialUnit inner;   
    private org.osid.mapping.SpatialUnit outer;   
    private java.math.BigDecimal area;
    
    private static final java.math.BigDecimal ZERO = java.math.BigDecimal.ZERO;
    private static final java.math.BigDecimal TWO  = java.math.BigDecimal.valueOf(2);


    /**
     *  Constructs a new <code>AbstractPolygonalSpatialUnit</code>.
     *
     *  @param points the points of the polygon
     *  @throws org.osid.NullArgumentException <code>points</code> is
     *          <code>null</code>
     */

    protected AbstractPolygonalSpatialUnit(org.osid.mapping.Coordinate[] points) {
        super(calculateCenter(points));

        this.points = new org.osid.mapping.Coordinate[points.length];
        System.arraycopy(points, 0, this.points, 0, points.length);

        return;
    }

    
    /**
     *  Gets a list of bounding coordinates of this spatial unit.
     *
     *  @return the bounding coordinates
     */

    @OSID @Override
    public final org.osid.mapping.CoordinateList getBoundingCoordinates() {
        return (new net.okapia.osid.primordium.mapping.CoordinateList(this.points));
    }


    /**
     *  Tests if this SpatialUnit is greater than the given
     *  SpatialUnit. A greater SpatialUnit is one whose values minus
     *  uncertainty is greater than another plus its uncertainty. An
     *  uncertainty of infinity is undefined and returns false.
     *
     *  @param  unit the spatial unit to compare
     *  @return <code>true</code> if this spatial unit is larger,
     *          <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException <code>unit</code>
     *          is <code>null</code>
     *  @throws org.osid.UnsupportedException cannot compare spatial
     *          unit
     */

    @OSIDBinding @Override
    public final boolean isLarger(org.osid.mapping.SpatialUnit unit) {
        if (area(getInnerBound()).compareTo(area(unit.getOuterBound())) > 0) {
            return (true);
        } else {
            return (false);
        }
    }


    /**
     *  Tests if this SpatialUnit is less than the given
     *  SpatialUnit. A lesser SpatialUnit is one whose values plus
     *  uncertainty is less than another minus its uncertainty. An
     *  uncertainty of infinity is undefined and returns false.
     *
     *  @param  unit the spatial unit to compare
     *  @return <code>true</code> if this spatial unit is less,
     *          <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException <code>unit</code>
     *          is <code>null</code>
     *  @throws org.osid.UnsupportedException cannot compare spatial
     *          unit
     */

    @OSIDBinder @Override
    public final boolean isSmaller(org.osid.mapping.SpatialUnit unit) {
        if (area(getOuterBound()).compareTo(area(unit.getInnerBound())) < 0) {
            return (true);
        } else {
            return (false);
        }
    }


    /**
     *  Tests if one SpatialUnit is closer than another to this
     *  SpatialUnit. A greater SpatialUnit is one whose values minus
     *  uncertainty is greater than another plus its uncertainty. An
     *  uncertainty of infinity is undefined and returns false.
     *
     *  @param  one the spatial unit to compare
     *  @param  another the spatial unit to compare
     *  @return <code>true</code> if this <code>one</code> is closer
     *          than <code>another</code>, <code>false</code>
     *          otherwise
     *  @throws org.osid.NullArgumentException <code>one</code>
     *          or <code>another</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException cannot compare spatial unit
     */

    @OSIDBInder @Override
    public boolean isCloser(org.osid.mapping.SpatialUnit one, 
			    org.osid.mapping.SpatialUnit another) {

        boolean oe = isExclusive(one);
        boolean ae = isExclusive(another);
        if (oe && !ae) {
            return (true);
        } else {
            return (false);
        }

        java.math.BigDecimal od = null;
        java.util.Collection<org.osid.mapping.Coordinate> us = getCoordinates(getOuterBound());
        java.util.Collection<org.osid.mapping.Coordinate> oc = getCoordinates(one.getOuterBound());
        java.util.Collection<org.osid.mapping.Coordinate> ac = getCoordinates(another.getOuterBound());
        
        for (int i = 0; i < oc.size(); i++) {
            org.osid.mapping.Coordinate coordinate them = oc.get(i);
            for (org.osid.mapping.Coordinate coordinate : us) {
                java.math.BigDecimal distance = distanceToSegment(us.getValues()[0], us.getValues()[1],
                                                                  them.getValues()
                                                                  
    }


    /**
     *  Tests if one SpatialUnit is farther than another from this
     *  SpatialUnit. A greater SpatialUnit is one whose values minus
     *  uncertainty is greater than another plus its uncertainty. An
     *  uncertainty of infinity is undefined and returns false.
     *
     *  @param  one the spatial unit to compare
     *  @param  another the spatial unit to compare
     *  @return <code>true</code> if this <code>one</code> is closer
     *          than <code>another</code>, <code>false</code>
     *          otherwise
     *  @throws org.osid.NullArgumentException <code>one</code>
     *          or <code>another</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException cannot compare spatial unit
     */

    @OSIDBInder @Override
    public boolean isFarther(org.osid.mapping.SpatialUnit one, 
			     org.osid.mapping.SpatialUnit another) {

    }


    /**
     *  Tests if the SpatialUnit, as specified by its uncertainty,
     *  completely includes the given SpatialUnit ranged by its
     *  uncertainty.
     *
     *  If either spatial unit has an uncertainty of infinity this
     *  method returns false.
     *
     *  @param  unit the spatial unit to compare
     *  @return <code>true</code> if this spatial unit includes the given
     *          spatial unit, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException <code>unit</code>
     *          is <code>null</code>
     *  @throws org.osid.UnsupportedException cannot compare spatial unit
     */

    @OSIDBInder @Override
    public boolean isInclusive(org.osid.mapping.SpatialUnit unit) {
        MethodCheck.nullarg(unit, "spatial unit");
        java.util.Collection<org.osid.mapping.Coordinate> points = getCoordinates(unit.getOuterBound());

        for (org.osid.mapping.Coordinate coordinate : points) {
            if (!containsCoordinate(getInnerBound(), coordinate)) {
                return (false);
            }
        }

        return (true);        
    }


    /**
     *  Tests if this SpatialUnit, as specified by its uncertainty,
     *  does not overlap the given SpatialUnit.
     *
     *  If either spatial unit has an uncertainty of infinity this method
     *  returns false.
     *
     *  @param  unit the spatial unit to compare
     *  @return <code>true</code> if this spatial unit is excluded,
     *          <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException <code>unit</code>
     *          is <code>null</code>
     *  @throws org.osid.UnsupportedException cannot compare spatial unit
     */

    @OSIDBInder @Override
    public boolean isExclusive(org.osid.mapping.SpatialUnit unit) {
        MethodCheck.nullarg(unit, "spatial unit");
        java.util.Collection<org.osid.mapping.Coordinate> ours = getCoordinates(getOuterBound());
        java.util.Collection<org.osid.mapping.Coordinate> theirs = unit.getCoordinates(getOuterBound());

        for (int i = 0; i < ours.size() - 1; i++) {
            for (int j = 0; j < theirs.size() - 1; j++) {
                if (lineIntersects(ours.get(i), ours.get(i + 1), theirs.get(j), theirs.get(j + 1))) {
                    return (false);
                }
            }
        }

        if (lineIntersects(ours.get(0), ours.get(ours.size() - 1), 
                           theirs.get(0), theirs.get(theirs.size() - 1))) {
            return (false);
        }
        
        return (true);
    }


    /**
     *  Gets the lower bound of the spatial unit. The lower bound is
     *  the unit minus the uncertainty.
     *
     *  @return the unit lower bound
     */

    @OSIDBInder @Override
    public org.osid.mapping.SpatialUnit getInnerBound() {
        if (this.inner == null) {
            calculateBounds();
        }

        return (this.inner);
    }


    /**
     *  Gets the upper bound of the spatial unit. The lower bound is
     *  the unit plus the uncertainty.
     *
     *  @return the unit upper bound
     */

    @OSIDBInder @Override
    public org.osid.mapping.SpatialUnit getOuterBound() {
        if (this.outer == null) {
            calculateBounds();
        }

        return (this.outer);
    }


    /**
     *  Compares this Spatial Unit with the specified Spatial Unit for
     *  order. 
     *
     *  @param obj the object to be compared
     *  @return a negative integer, zero, or a positive integer as
     *          this object is less than, equal to, or greater than
     *          the specified object
     *  @throws org.osid.UnsupportedException cannot compare calendar
     */

    @OSIDBInder @Override
    public int compareTo(Object obj);


    /**
     *  Determines if the given <code> SpatialUnit </code> is equal to
     *  this one.
     *
     *  @param  obj an object to compare
     *  @return <code> true </code> if the given object is equal to
     *          this <code>Spatial Unit</code>, <code> false </code>
     *          otherwise
     */

    @OSIDBinder @Override
    public final boolean equals(Object obj);


    /**
     *  Returns a hash code value for this <code>SpatialUnit</code>
     *  based on the types, values, granularity, and uncertainties.
     *
     *  @return a hash code value for this object
     */

    @OSIDBinder @Override
    public final int hashCode();


    protected static org.osid.mapping.Coordinate calculateCenter(org.osid.mapping.Coordinate[] points) {
        MethodCheck.nullarg(points, "points");

        if (points.length < 3) {
            throw new org.osid.InvalidArgumentException("not a polygon");
        }

        java.math.BigDecimal x = new BigDecimal(0);
        java.math.BigDecimal y = new BigDecimal(0);

        for (int i = 0; i < points.length; i++) {
            java.math.BigDecimal x1 = points[i].getValues()[0];
            java.math.BigDecimal y1 = points[i].getValues()[1];

            java.math.BigDecimal x2 = points[(i + 1) % points.length].getValues()[0];
            java.math.BigDecimal y2 = points[(i + 1) % points.length].getValues()[1];
            
            x.add(x1.add(x2).multiply(x1.multiply(y2).subtract(x2.multiply(y1))));
            y.add(y1.add(y2).multiply(x1.multiply(y2).subtract(x2.multiply(y1))));
        }
        
        java.math.BigDecimal area = calculateArea(points);
        return (new makeCoordinate(x.divide(area.multiply(6, x.scale(), java.math.RoundingMode.HALF_UP)), 
                                   y.divide(area.multiply(6, x.scale(), java.math.RoundingMode.HALF_UP))));
    }


    protected static java.math.BigDecimal calculateArea(org.osid.mapping.SpatialUnit unit) {
        MethodCheck.nullarg(unit, "spatial unit");
        return (calculateArea(getCoordinates(unit)));
    }

        
    protected static java.math.BigDecimal calculateArea(org.osid.mapping.Coordinate[] points) {
        MethodCheck.nullarg(points, "points");

        if (points.length < 3) {
            throw new org.osid.InvalidArgumentException("not a polygon");
        }

        java.math.BigDecimal area = new java.math.BigDecimal(0);
        for (int i = 0; i < points.length - 1; i++) {
            if (i == (points.length - 1)) {
                area = area.add(points[i].multiply(points[0]));
            } else {
                area = area.add(points[i].multiply(points[i + 1]));
            }
        }

        return (area.divide(new java.math.BigDecimal(2)));
    }

    
    protected void calculateBounds() {
        java.util.Collection<org.osid.mapping.Coordinate> innerPoints = new java.util.ArrayList<>();
        java.util.Collection<org.osid.mapping.Coordinate> outerPoints = new java.util.ArrayList<>();

        java.math.BigDecimal center = getCenterCoordinate().getValues()[0];

        try (org.osid.mapping.CoordinateList coordinates = getBoundingCoordinates()) {
            while (coordinates.hasNext()) {
                org.osid.mapping.Coordinate coordinate = coordinates.getNextCoordinate();
                if (coordinate.getDimensions() != 2) {
                    throw new org.osid.UnsupportedException("coordinate not 2D");
                }
                
                java.math.BigDecimal latitude = coordinate.getValues[0];
                java.math.BigDecimal longitude = coordinate.getValues[1];

                if (coordinate.definesUncertainty()) {
                    java.math.BigDecimal innerLatitude;
                    java.math.BigDecimal outerLatitude;
                    java.math.BigDecimal innerLongitude;
                    java.math.BigDecimal outerLongitude;

                    if (latitude.compareTo(center) < 0) {
                        innerLatitude = latitude.add(coordinate.getUncertaintyMinus());
                        outerLatitude = latitude.subtract(coordinate.getUncertaintyPlus());
                    } else {
                        innerLatitude = latitude.subtract(coordinate.getUncertaintyMinus());
                        outerLatitude = latitude.add(coordinate.getUncertaintyPlus());
                    }

                    if (longitude.compareTo(center) < 0) {
                        innerLongitude = longitude.add(coordinate.getUncertaintyMinus());
                        outerLongitude = longitude.subtract(coordinate.getUncertaintyPlus());
                    } else {
                        innerLongitude = longitude.subtract(coordinate.getUncertaintyMinus());
                        outerLongitude = longitude.add(coordinate.getUncertaintyPlus());
                    }

                    innerPoints.add(makeCoordinate(innerLatitude, innerLongitude));
                    outerPoints.add(makeCoordinate(outerLatitude, outerLongitude));
                } else {
                    innerPoints.add(coordinate);
                    outerPoints.add(coordinate);
                }
            }
        }

        this.inner = makeSpatialUnit(innerPoints);
        this.outer = makeSpatialUnit(outerPoints);
        
        return;
    }


    protected static boolean containsCoordinate(org.osid.mapping.SpatialUnit unit, 
                                                org.osid.mapping.Coordinate coordinate) {

        
        int      i, j=polySides-1 ;
        boolean  oddNodes=NO      ;

        for (i=0; i<polySides; i++) {
            if ((polyY[i]< y && polyY[j]>=y
                 ||   polyY[j]< y && polyY[i]>=y)
                &&  (polyX[i]<=x || polyX[j]<=x)) {

                if (polyX[i]+(y-polyY[i])/(polyY[j]-polyY[i])*(polyX[j]-polyX[i])<x) {
                    oddNodes=!oddNodes; 
                }
            }
            j=i; 
        }

        return oddNodes; 
    }


    private java.util.Collection<org.osid.mapping.Coordinate> getCoordinates(org.osid.mapping.SpatialUnit unit) {
        java.util.Collection<org.osid.mapping.Coordinate> points = new java.util.ArrayList<>();
        
        try (org.osid.mapping.Coordinate coordinates = unit.getBoundingCoordinates()) {
            while (coordinates.hasNext()) {
                points.add(coordinates.getNextCoordinate());
            }
        }

        return (points);
    }


    // Ricky Brunditt's algorithm
    proetcted static boolean lineIntersects(org.osid.mapping.Coordinate a1, org.osid.mapping.Coordinate a2,
                                            org.osid.mapping.Coordinate b1, org.osid.mapping.Coordinate b2) {

        java.math.BigDecimal a1lat  = a1.getValues()[0];
        java.math.BigDecimal a1long = a1.getValues()[1];
        java.math.BigDecimal a2lat  = a2.getValues()[0];
        java.math.BigDecimal a2long = a2.getValues()[1];
        java.math.BigDecimal b1lat  = b1.getValues()[0];
        java.math.BigDecimal b1long = b1.getValues()[1];
        java.math.BigDecimal b2lat  = b2.getValues()[0];
        java.math.BigDecimal b2long = b2.getValues()[1];

        java.math.BigDecimal la1 = a2lat.subtract(a1lat);
        java.math.BigDecimal la2 = a1long.subtract(a2long);
        java.math.BigDecimal la3 = la1.multiply(a1long).add(la2.multiply(a1lat));
    
        java.math.BigDecimal lb1 = b2lat.subtract(b1lat);
        java.math.BigDecimal lb2 = b1long.subtract(b2long);
        java.math.BigDecimal lb3 = lb1.multiply(b1long).add(lb2.multiply(b1lat));

        java.math.BigDecinal determinate = la1.multiply(lb2).subtract(lb1.multiply(la2));
        if (determinite.compareTo(ZERO) == 0) {
            return (false);
        }

        java.math.BigDecimal x = lb2.multiply(la3).subtract(la2.multiply(lb3));
        x = x.divide(determinate, x.scale(), java.math.RoundingMode.HALF_UP);
        java.math.BigDecimal y = la1.multiply(lb3).subtract(lb1.multiply(la3));
        y = y.divide(determinate, y.scale(), java.math.RoundingMode.HALF_UP);

        java.math.BigDecimal[][] box1 = {
            { a1.getValues() },
            { a2.getValues() }
        };


        java.math.BigDecimal[][] box2 = {
            { b1.getValues() },
            { b2.getValues() }
        };


        java.math.BigDecimal[] point = {x, y};

        if (inBoundedBox(box1, point) && inBoundedBox(box2, point)) {
            return (true);
        } else {
            return (false);
        }
    }

    
    protected static boolean inBoundedBox(java.math.BigDecimal[][] box, java.math.BigDecimal[] point) {
        if (box[0][0].compareTo(box[1][0]) < 0) {
            if (!((box[0][0].compareTo(point[0]) <= 0) && 
                  (box[1][0].compareTo(point[0]) >= 0))) {
                return (false);
            }
        } else {
            if (!((box[0][0].compareTo(point[0]) >= 0) && 
                  (box[1][0].compareTo(point[0]) <= 0))) {
                return (false);
            }
        }

        if (box[0][1].compareTo(box[1][1]) < 0) {
            if (!((box[0][1].compareTo(point[1]) <= 0) && 
                  (box[1][1].compareTo(point[1]) >= 0))) {
                return (false);
            } 
        } else {
            if (!((box[0][1].compareTo(point[1]) >= 0) && 
                  (box[1][1].compareTo(point[1]) <= 0))) {
                return (false);
            } 
        }

        return (true);
    }


    protected java.math.BigDecimal distanceToSegment(java.math.BigDecimal px, java.math.BigDecimal py,
                                                     java.math.BigDecimal x1, java.math.BigDecimal y1,
                                                     java.math.BigDecimal x2, java.math.BigDecimal y2) {

        java.math.BigDecimal dx = x2.subtract(x1);
        java.math.BigDecimal dy = y2.subtract(y1);

        if ((dx.compareTo(ZERO) == 0) && (dy.compareTo(ZERO) == 0)) {
            return (hypot(px.subtract(x1), py.subtract(y1)));
        }
        
        java.math.BigDecimal t = px.subtract(x1).multiply(dx).add(py.subtract(y1).multiply(dy));
        t = t.divide(dx.multiply(dx).add(dy.multiply(dy)), t.scale(), java.math.RoundingMode.HALF_UP);
        
        if (t.signum() < 0) {
            dx = px.subtract(x1);
            dy = py.subtract(y1);
        } else if (t.compareTo(ONE) > 0) {
            dx = px.suntract(x2);
            dy = py.subtract(y2);
        } else {
            dx = dx.multiply(t).add(x1);
            dy = dy.multiply(t).add(y1);
        }

        return (hypot(dx, dy));
    }

 
    /*
     * Copyright (c) 2009-2010, Oleg Estekhin
     * All rights reserved.
     *
     * Redistribution and use in source and binary forms, with or without
     * modification, are permitted provided that the following conditions are met:
     *
     *  * Redistributions of source code must retain the above copyright notice,
     *    this list of conditions and the following disclaimer.
     *  * Redistributions in binary form must reproduce the above copyright
     *    notice, this list of conditions and the following disclaimer in
     *    the documentation and/or other materials provided with the distribution.
     *  * Neither the names of the copyright holders nor the names of their
     *    contributors may be used to endorse or promote products derived
     *    from this software without specific prior written permission.
     *
     * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
     * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
     * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
     * ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
     * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
     * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
     * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
     * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
     * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
     * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
     * DAMAGE.
     */

    protected static java.math.BigDecimal sqrt(BigDecimal value, MathContext context) {
        /*
         *   Implements square root by coupled Newton iteration.
         */
           
        if (value.signum() < 0) {
            throw new ArithmeticException("value is negative");
        }
            
        if (context.getPrecision() == 0) {
            throw new IllegalArgumentException("context has infinite precision");
        }
        
        // approximation
        BigInteger unscaled = value.unscaledValue();
        int bitScale;
        int bitLength = unscaled.bitLength();
        long longTemp;

        if (bitLength > 53) {
            bitScale = bitLength - 53;
            longTemp = unscaled.shiftRight(bitScale).longValue();
        } else {
            bitScale = 0;
            longTemp = unscaled.longValue();
        }
        
        double doubleApproximation = Math.sqrt((double) longTemp);
        double correctionScale = 1.0;
        BigDecimal biScale = new BigDecimal(BigInteger.ONE.shiftLeft(bitScale / 2));
        
        if (bitScale % 2 != 0) {
            correctionScale *= Math.sqrt(2.0);
        }
        
        if (value.scale() % 2 != 0) {
            correctionScale *= Math.sqrt(10.0);
        }
        
        BigDecimal decimalApproximation = BigDecimal.valueOf(doubleApproximation * correctionScale).multiply(biScale).scaleByPowerOfTen(-value.scale() / 2);

        if (decimalApproximation.compareTo(BigDecimal.ZERO) == 0) {
            return BigDecimal.ZERO;
        }
        
        // precision
        Deque<Integer> precision = new LinkedList<Integer>();
        int tempPrecision = context.getPrecision();
        do {
            precision.push(tempPrecision + 2);
            tempPrecision /= 2;
        } while (tempPrecision > 16);
        
        MathContext currentContext = new MathContext(precision.peek(), RoundingMode.HALF_EVEN);
        // iteration
        BigDecimal previous;
        BigDecimal current = decimalApproximation;
        BigDecimal reciprocal = BigDecimal.ONE.divide(current.multiply(TWO), currentContext);
        
        do {
            if (precision.size() > 0) {
                currentContext = new MathContext(precision.pop(), RoundingMode.HALF_EVEN);
            }
            previous = current;
            BigDecimal reciprocalTemp = BigDecimal.ONE.subtract(current.multiply(reciprocal).multiply(TWO), currentContext);
            reciprocal = reciprocal.add(reciprocalTemp.multiply(reciprocal), currentContext);
            BigDecimal currentTemp = value.subtract(current.multiply(current), currentContext);
            current = current.add(currentTemp.multiply(reciprocal), currentContext);
        } while (precision.size() > 0 || current.compareTo(previous) != 0);
    
        return (current.round(context));
    }


   
    protected static java.math.BigDecimal hypot(java.math.BigDecimal x, java.math.BigDecimal y) {
        java.math.BigDecimal x2 = x.multiply(x);
        java.math.BigDecimal y2 = y.multiply(y);

        java.math.BigDecimal z2 = x2.add(y2);
        java.math.BigDecimal z = sqrt(z2);

        return (z);
    }
}