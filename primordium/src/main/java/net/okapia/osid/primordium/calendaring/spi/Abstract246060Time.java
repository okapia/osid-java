//
// Abstract246060Time.java
//
//     An implementation of a Abstract246060 based OSID Time.
//
//
// Tom Coppeto
// Okapia
// 2 May 2012
//
// Copyright (c) 2012 Ingenescus. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;
import org.osid.calendaring.DateTimeResolution;

import net.okapia.osid.primordium.types.time.EarthTimeTypes;


/**
 *  An implementation of a Time based on Abstract246060 using a
 *  Abstract246060TimeDefinition.
 */

public abstract class Abstract246060Time 
    extends AbstractConfigurableTime
    implements org.osid.calendaring.Time {

    private String label;
    private static final long serialVersionUID = -1445811996L;


    /**
     *  Constructs a new <code>Abstract246060Time</code> with second
     *  resolution.
     *
     *  @param factory the time factory
     *  @param hour the hour
     *  @param minute the minute
     *  @param second the second
     *  @throws org.osid.InvalidArgumentException <code>hour</code>,
     *          <code>minute</code>, or <code>second</code> is
     *          negative, or <code>hour</code> is greater than 23
     *          <code>minute</code> is greater than 59, or
     *          <code>second</code> is greater than 60
     */

    public Abstract246060Time(TimeFactory factory, long hour, long minute, long second) {
        super(factory, hour, minute, second);
        return;
    }

    
    /**
     *  Constructs a new <code>Abstract246060Time</code>.
     *
     *  @param factory the time factory
     *  @param specifier a time specifier
     *  @throws org.osid.NullArgumentException <code>specifier</code>
     *          is <code>null</code>
     */

    public Abstract246060Time(TimeFactory factory, AbstractConfigurableTimeSpecifier specifier) {
        super(factory, specifier);
        return;
    }


    /**
     *  Creates a Time based on a formatted string.
     *
     *  @param factory the time factory
     *  @param str the string to parse
     *  @return a Time
     *  @throws org.osid.InvalidArgumentException cannot parse string
     *  @throws org.osid.NullArgumentException <code>factory</code> or
     *          <code>str</code> is <code>null</code>
     */
    
    @SuppressWarnings("fallthrough")
    protected static org.osid.calendaring.Time parseTime(TimeFactory factory, String str) {
        DateTimeResolution granularity;
        String[] tok = str.split(":");
        String dec = "";
        
        switch (tok.length) {
        case 1: 
            granularity = DateTimeResolution.HOUR;
            break;
        case 2: 
            granularity = DateTimeResolution.MINUTE;
            break;
        case 3: 
            granularity = DateTimeResolution.SECOND;
            String[] dtok = tok[2].split("\\.");
            tok[2] = dtok[0];
            if (dtok.length == 2) {
                dec = dtok[1];
            } else if (dtok.length > 2) {
                throw new org.osid.InvalidArgumentException("parsing error too many dots");
            } 
            
            break;
        default:
            throw new org.osid.InvalidArgumentException("too many delimiters in " + str);
        }

        int multiplier = 1;
        org.osid.calendaring.Time decimal = null;
        if (dec.length() > 0) {
            decimal = parseDecimal(factory, dec);
            granularity = decimal.getGranularity();
            multiplier = (int) decimal.getGranularityMultiplier();
        }
        
        AbstractTimeSpecifier spec = factory.getTimeSpecifier();
        spec.setGranularity(granularity, multiplier);

        if (decimal != null) {
            switch (granularity) {
            case PLANCKSECOND:
                spec.setPlanckSeconds((int) decimal.getPlanckSeconds());  
            case UNDASECOND:
                spec.setUndaseconds((int) decimal.getUndaseconds());
            case VATOSECOND:
                spec.setVatoseconds((int) decimal.getVatoseconds());
            case WEEBLESECOND:
                spec.setWeebleseconds((int) decimal.getWeebleseconds());
            case XOXXOSECOND:
                spec.setXoxxoseconds((int) decimal.getXoxxoseconds());
            case YOCTOSECOND:
                spec.setYoctoseconds((int) decimal.getYoctoseconds());
            case ZEPTOSECOND:
                spec.setZeptoseconds((int) decimal.getZeptoseconds());
            case ATTOSECOND:
                spec.setAttoseconds((int) decimal.getAttoseconds());
            case FEMTOSECOND:
                spec.setFemtoseconds((int) decimal.getFemtoseconds());
            case PICOSECOND:
                spec.setPicoseconds((int) decimal.getPicoseconds());
            case NANOSECOND:
                spec.setNanoseconds((int) decimal.getNanoseconds());
            case MICROSECOND:
                spec.setMicroseconds((int) decimal.getMicroseconds());
            case MILLISECOND:
                spec.setMilliseconds((int) decimal.getMilliseconds());
            }
        }

        try {
            if (granularity.ordinal() < DateTimeResolution.SECOND.ordinal()) {
                    spec.setSecond(Long.parseLong(tok[2]));
                    spec.setMinute(Long.parseLong(tok[1]));
                    spec.setHour(Long.parseLong(tok[0]));
            } else {
                switch(granularity) {
                case SECOND:
                    spec.setSecond(Long.parseLong(tok[2]));
                case MINUTE:
                    spec.setMinute(Long.parseLong(tok[1]));
                case HOUR:
                    spec.setHour(Long.parseLong(tok[0]));
                }
            }
        } catch (NumberFormatException nfe) {
            throw new org.osid.InvalidArgumentException(nfe);
        }
        
        return (factory.makeTime(spec));
    }
    
    
    /**
     *  Returns a string representation of this DateTime. 
     *
     *  @return a string
     */

    @OSIDBinding @Override
    public String toString() {        
        if (this.label == null) {
            this.label = formatLabel();
        }

        return (this.label);
    }


    private String formatLabel() {
	if (getGranularity() == DateTimeResolution.INFINITY) {
	    return ("undefined");
	}

	StringBuilder s = new StringBuilder(16);
	s.append(formatSignificantDigits(getHour(), 2));

	if (getGranularity() == DateTimeResolution.HOUR) {
	    return (s.toString());
	}

	s.append(":");
	s.append(formatSignificantDigits(getMinute(), 2));
	if (getGranularity() == DateTimeResolution.MINUTE) {
	    return (s.toString());
	}

	s.append(":");
	s.append(formatSignificantDigits(getSecond(), 2));
	if (getGranularity() == DateTimeResolution.SECOND) {
	    return (s.toString());
	}

	s.append(".");
	s.append(formatDecimal(this));
	return (s.toString());
    }
}
