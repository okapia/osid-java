//
// AbstractSpatialUnit.java
//
//     An implementation of an OSID SpatialUnit.
//
//
// Tom Coppeto
// Okapia
// 13 May 2012
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.mapping.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An implementation of an OSID SpatialUnit.
 */

public abstract class AbstractSpatialUnit
    implements org.osid.mapping.SpatialUnit {

    private final org.osid.mapping.Coordinate center;

    private static final long serialVersionUID = 23176529L;


    /**
     *  Constructs a new <code>AbstractSpatialUnit</code>.
     *
     *  @param center the center coordinate
     *  @throws org.osid.NullArgumentException <code>center</code> is
     *          <code>null</code>
     */

    protected AbstractSpatialUnit(org.osid.mapping.Coordinate center) {
        nullarg(center, "center coordinate");

        this.center = center;

        return;
    }

    /**
     *  Gets a single coordinate to represent the center of this spatial unit.
     *
     *  @return the center coordinate
     */

    @OSID @Override
    public final org.osid.mapping.Coordinate getCenterCoordinate() {
        return (this.center);
    }
}
