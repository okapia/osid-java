//
// AbstractGeographicCoordinate.java
//
//     An implementation of a geographic coordinate with latitude and
//     longitude.
//
//
// Tom Coppeto
// Okapia
// 13 May 2012
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.mapping.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import java.math.BigDecimal;


/**
 *  An implementation of a geographic coordinate based on latitude and
 *  longitude. 
 */

public abstract class AbstractGeographicCoordinate
    extends AbstractCoordinate
    implements org.osid.mapping.Coordinate {

    private String label;
    private int hash = 0;
    private static final BigDecimal NEGATE     = new BigDecimal(-1);
    private static final BigDecimal ZERO       = new BigDecimal(0);
    private static final BigDecimal NINETY     = new BigDecimal(90);
    private static final BigDecimal ONEEIGHTY  = new BigDecimal(180);
    private static final BigDecimal THREESIXTY = new BigDecimal(360);
    
    private org.osid.mapping.SpatialUnit outer;

    private static final long serialVersionUID = -382095162L;


    /**
     *  Constructs a new <code>AbstractGeographicCoordinate</code>.
     *
     *  @param coordinateType the type of coordinate system
     *  @param latitude the latitude
     *  @param longitude the longitude
     *  @throws org.osid.InvalidArgumentException
     *          <code>latitude</code> or <code>longitude</code> is out
     *          of range
     *  @throws org.osid.NullArgumentException
     *          <code>coordinateType</code>, <code>latitude</code>, or
     *          <code>longitude</code> is <code>null</code>
     */

    protected AbstractGeographicCoordinate(org.osid.type.Type coordinateType, BigDecimal latitude, BigDecimal longitude) {
        super(coordinateType, new BigDecimal[] {latitude, longitude});

        if ((latitude.compareTo(NINETY) > 0) || (latitude.compareTo(NINETY.multiply(NEGATE)) < 0)) {
            throw new org.osid.InvalidArgumentException("latitude out of range");
        }
        
        if ((longitude.compareTo(ONEEIGHTY) > 0) || (longitude.compareTo(ONEEIGHTY.multiply(NEGATE)) < 0)) {
            throw new org.osid.InvalidArgumentException("longitude out of range");
        }

        return;
    }


    /**
     *  Constructs a new <code>AbstractGeographicCoordinate</code>.
     *
     *  @param coordinateType the specific geographic type
     *  @param latitude the latitude
     *  @param longitude the longitude
     *  @param latitudeUncertaintyPlus the latitude uncertainty in the
     *         positive direction
     *  @param latitudeUncertaintyMinus the latitude uncertainty in the
     *         other direction
     *  @param longitudeUncertaintyPlus the longitude uncertainty in the
     *         positive direction
     *  @param longitudeUncertaintyMinus the longitude uncertainty in
     *         the other direction
     *  @throws org.osid.InvalidArgumentException
     *          <code>latitude</code> or <code>longitude</code> is out
     *          of range
     *  @throws org.osid.NullArgumentException
     *          <code>coordinateType</code>, <code>latitude</code>,
     *          <code>longitude</code,
     *          <code>latitudeUncertaintyPlus</code>,
     *          <code>latitudeUncertaintyMinus</code>,
     *          <code>longitudeUncertaintyPlus</code>, or
     *          <code>longitudeUncertaintyMinus</code>, is
     *          <code>null</code>
     */

    protected AbstractGeographicCoordinate(org.osid.type.Type coordinateType, BigDecimal latitude, BigDecimal longitude,
                                           BigDecimal latitudeUncertaintyPlus, BigDecimal latitudeUncertaintyMinus,
                                           BigDecimal longitudeUncertaintyPlus, BigDecimal longitudeUncertaintyMinus) {

        super(coordinateType, new BigDecimal[] {latitude, longitude}, 
              new BigDecimal[] {latitudeUncertaintyPlus, longitudeUncertaintyPlus},
              new BigDecimal[] {latitudeUncertaintyMinus, longitudeUncertaintyMinus});

        if ((latitude.compareTo(NINETY) > 0) || (latitude.compareTo(NINETY.multiply(NEGATE)) < 0)) {
            throw new org.osid.InvalidArgumentException("latitude out of range");
        }
        
        if ((longitude.compareTo(ONEEIGHTY) > 0) || (longitude.compareTo(ONEEIGHTY.multiply(NEGATE)) < 0)) {
            throw new org.osid.InvalidArgumentException("longitude out of range");
        }

        return;
    }


    /**
     *	Tests if this <code>Coordinate</code> is greater in area than
     *	the given <code>Coordinate</code>. A greater
     *	<code>Coordinate</code> is one whose values minus uncertainty
     *	is greater than another plus its uncertainty. An uncertainty
     *	of infinity is undefined and returns false.
     *
     *  It is possible for <code>isLarger()</code>,
     *  <code>isSmaller()</code>, and equals() to be all
     *  <code>false</code> among two <code>Coordinates</code> due to
     *  uncertainties in the values. In this case,
     *  <code>compareTo()</code> may elect to return a value to
     *  provide a natural ordering but should not be used to determine
     *  equality. <code>equals()</code> tends to have a stricter
     *  definition of equality.
     *
     *	@param	coordinate the coordinate to compare
     *	@return <code>true</code> if this coordinate is larger,
     *		<code>false</code> otherwise
     *	@throws org.osid.NullArgumentException <code>coordinate</code>
     *		is <code>null</code>
     *	@throws org.osid.UnsupportedException cannot compare coordinate
     */

    @OSIDBinding @Override
    public boolean isLarger(org.osid.mapping.Coordinate coordinate) {    
        org.osid.mapping.SpatialUnit theirOuter = coordinate.getOuterBound();

        return false;
    }


    /**
     *	Tests if this <code>Coordinate</code> is less in area than the
     *	given <code>Coordinate</code>. A lesser
     *	<code>Coordinate</code> is one whose values plus uncertainty
     *	is less than another minus its uncertainty. An uncertainty of
     *	infinity is undefined and returns false.
     *
     *  It is possible for <code>isLarger()</code>,
     *  <code>isSmaller()</code>, and equals() to be all
     *  <code>false</code> among two <code>Coordinates</code> due to
     *  uncertainties in the values. In this case,
     *  <code>compareTo()</code> may elect to return a value to
     *  provide a natural ordering but should not be used to determine
     *  equality. <code>equals()</code> tends to have a stricter
     *  definition of equality.
     *
     *	@param	coordinate the coordinate to compare
     *	@return <code>true</code> if this coordinate is less,
     *		<code>false</code> otherwise
     *	@throws org.osid.NullArgumentException <code>coordinate</code>
     *		is <code>null</code>
     *	@throws org.osid.UnsupportedException cannot compare coordinate
     */

    @OSIDBinding @Override
    public boolean isSmaller(org.osid.mapping.Coordinate coordinate) {

        return false;
    }


    /**
     *	Tests if one <code>Coordinate</code> is closer to origin than
     *	another to this <code>Coordinate</code>. A greater
     *	<code>Coordinate</code> is one whose values minus uncertainty
     *	is greater than another plus its uncertainty. An uncertainty
     *	of infinity is undefined and returns false.
     *
     *  It is possible for <code>isCloser()</code> and
     *  <code>isFarther()</code> to be both <code>false</code> among
     *  two <code>Coordinates</code> due to uncertainties in the
     *  values.
     *
     *	@param	one the coordinate to compare
     *	@param	another the coordinate to compare
     *	@return <code>true</code> if this <code>one</code> is closer
     *		than <code>another</code>, <code>false</code>
     *		otherwise
     *	@throws org.osid.NullArgumentException <code>one</code> or
     *		<code>another</code> is <code>null</code>
     *	@throws org.osid.UnsupportedException cannot compare coordinate
     */

    @OSIDBinding @Override
    public boolean isCloser(org.osid.mapping.Coordinate one, 
			    org.osid.mapping.Coordinate another) {

        return false;
    }


    /**
     *	Tests if one <code>Coordinate</code> is farther from origin
     *	than another from this <code>Coordinate</code>. A greater
     *	<code>Coordinate</code> is one whose values minus uncertainty
     *	is greater than another plus its uncertainty. An uncertainty
     *	of infinity is undefined and returns false.
     *
     *  It is possible for <code>isCloser()</code> and
     *  <code>isFarther()</code> to be both <code>false</code> among
     *  two <code>Coordinates</code> due to uncertainties in the
     *  values.
     *
     *	@param	one the coordinate to compare
     *	@param	another the coordinate to compare
     *	@return <code>true</code> if this <code>one</code> is closer
     *		than <code>another</code>, <code>false</code>
     *		otherwise
     *	@throws org.osid.NullArgumentException <code>one</code> or
     *		<code>another</code> is <code>null</code>
     *	@throws org.osid.UnsupportedException cannot compare coordinate
     */

    @OSIDBinding @Override
    public boolean isFarther(org.osid.mapping.Coordinate one, 
			     org.osid.mapping.Coordinate another) {

        return false;
    }


    /**
     *	Tests if this <code>Coordinate</code>, as specified by its
     *	uncertainty, completely includes the given
     *	<code>Coordinate</code> ranged by its uncertainty.
     *
     *	If either coordinate has an uncertainty of infinity this
     *	method returns false.
     *
     *  It is possible for <code>isInclusive()</code> and
     *  <code>isExclsuive()</code> to be both <code>false</code> among
     *  two <code>Coordinates</code> due to uncertainties in the
     *  values.
     *
     *	@param	coordinate the coordinate to compare
     *	@return <code>true</code> if this coordinate includes the given
     *		coordinate, <code>false</code> otherwise
     *	@throws org.osid.NullArgumentException <code>coordinate</code>
     *		is <code>null</code>
     *	@throws org.osid.UnsupportedException cannot compare coordinate
     */

    @OSIDBinding @Override
    public boolean isInclusive(org.osid.mapping.Coordinate coordinate) {
        return false;
    }


    /**
     *	Tests if this <code>Coordinate</code>, as specified by its
     *	uncertainty, does not overlap the given
     *	<code>Coordinate</code>.
     *
     *	If either coordinate has an uncertainty of infinity this
     *	method returns false.
     *
     *  It is possible for <code>isInclusive()</code> and
     *  <code>isExclsuive()</code> to be both <code>false</code> among
     *  two <code>Coordinates</code> due to uncertainties in the
     *  values.
     *     
     *	@param	coordinate the coordinate to compare
     *	@return <code>true</code> if this coordinate is excluded,
     *		<code>false</code> otherwise
     *	@throws org.osid.NullArgumentException <code>coordinate</code>
     *		is <code>null</code>
     *	@throws org.osid.UnsupportedException cannot compare coordinate
     */

    @OSIDBinding @Override
    public boolean isExclusive(org.osid.mapping.Coordinate coordinate) {
        return false;
    }


    /**
     *	Gets the upper bound of the coordinate. The lower bound is the
     *	coordinate plus the uncertainty. This method returns the
     *	Coordinate with the largest radius allowed by its uncertainty.
     *
     *	@return the coordinate upper bound
     */

    @OSIDBinding @Override
    public org.osid.mapping.SpatialUnit getOuterBound() {
        if (this.outer == null) {
            calculateBounds();
        }
        
        return (this.outer);
    }


    /**
     *	Gets the closest bound of the coordinate. The closest bound is
     *	the coordinate of the same granularity in the uncertainty
     *	range closest to the origin. 
     *
     *	@return the coordinate lower bound
     */

    @OSIDBinding @Override
    public org.osid.mapping.Coordinate getClosestBound() {
        return null;
    }


    /**
     *	Gets the farthest bound of the coordinate. The farthest bound
     *	is the coordinate of the same granularity in the uncertainty
     *	range farthest from the origin.
     *
     *	@return the coordinate upper bound
     */

    @OSIDBinding @Override
    public org.osid.mapping.Coordinate getFarthestBound() {
        return null;
    }


    /**
     *  Returns a string representation of this Coordinate.
     *
     *  All implementations are encouraged to supply this method to
     *  display the type and value.
     *
     *  @return a string
     */

    @Override
    public String toString() {
        if (this.label == null) {
            this.label = formatLabel();
        }

        return (this.label);
    }


    /**
     *  Makes a simple coordinate.
     *
     *  @param latitude
     *  @param longitude
     *  @return a coordinate
     */

    protected abstract org.osid.mapping.Coordinate makeCoordinate(BigDecimal latitude, BigDecimal longitude);


    /**
     *  Makes a simple spatial unit.
     *
     *  @param bounds
     *  @return the bounding coordinates
     */

    protected abstract org.osid.mapping.SpatialUnit makeSpatialUnit(org.osid.mapping.Coordinate[] bounds);


    private void calculateBounds() {
        org.osid.mapping.Coordinate center = makeCoordinate(getValues()[0], getValues()[1]);
        org.osid.mapping.Coordinate[] bounds = new org.osid.mapping.Coordinate[4];

        if (!definesUncertainty()) {
            bounds[0] = center;
            bounds[1] = center;
            bounds[2] = center;
            bounds[3] = center;
        } else {
            java.math.BigDecimal latitude1  = fixLatitude(getValues()[0].subtract(getUncertaintyMinus()[0]));
            java.math.BigDecimal longitude1 = fixLongitude(getValues()[1].subtract(getUncertaintyMinus()[1]));
            bounds[0] = makeCoordinate(latitude1, longitude1);
            
            java.math.BigDecimal latitude2  = fixLatitude(getValues()[0].add(getUncertaintyPlus()[0]));
            bounds[1] = makeCoordinate(latitude2, longitude1);
            
            java.math.BigDecimal longitude2 = fixLatitude(getValues()[1].add(getUncertaintyPlus()[1]));
            bounds[2] = makeCoordinate(latitude2, longitude2);
            
            bounds[3] = makeCoordinate(latitude1, longitude2);
        }

        this.outer = makeSpatialUnit(bounds);
        return;
    }
    

    private BigDecimal fixLatitude(BigDecimal latitude) {
        if (latitude.compareTo(THREESIXTY) >= 0) {
            latitude = latitude.remainder(THREESIXTY);
        }

        if (latitude.compareTo(THREESIXTY.multiply(NEGATE)) <= 0) {
            latitude = latitude.remainder(THREESIXTY);
        }

        if (latitude.compareTo(NINETY) >= 0) {
            latitude = ONEEIGHTY.subtract(latitude);
        }

        if (latitude.compareTo(NINETY.multiply(NEGATE)) <= 0) {
            latitude = latitude.add(ONEEIGHTY).multiply(NEGATE);
        }

        return (latitude);
    }


    private BigDecimal fixLongitude(BigDecimal longitude) {
        if (longitude.compareTo(THREESIXTY) >= 0) {
            longitude = longitude.remainder(THREESIXTY);
        }

        if (longitude.compareTo(THREESIXTY.multiply(NEGATE)) <= 0) {
            longitude = longitude.remainder(THREESIXTY);
        }

        if (longitude.compareTo(ONEEIGHTY) > 0) {
            longitude = longitude.subtract(THREESIXTY);
        }

        if (longitude.compareTo(ONEEIGHTY.multiply(NEGATE)) < 0) {
            longitude = longitude.add(THREESIXTY);
        }

        return (longitude);
    }


    private String formatLabel() {
        StringBuilder sb = new StringBuilder();
        sb.append(getCoordinateType().getDisplayLabel());
        sb.append(": ");

        BigDecimal[] values = getValues();
        for (int i = 0; i < getDimensions(); i++) {
            if (i > 0) {
                sb.append(",");
            }

            sb.append(values[i].toString());
        }

        if (definesUncertainty()) {
            BigDecimal[] uncertaintyPlusses = getUncertaintyPlus();
            BigDecimal[] uncertaintyMinuses = getUncertaintyMinus();
            boolean equals = true;
            for (int i = 0; i < getDimensions(); i++) {
                if (!uncertaintyPlusses[i].equals(uncertaintyMinuses[i])) {
                    equals = false;
                }
            }

            if (equals) {
                sb.append(" +/-(");
            } else {
                sb.append(" +(");
            }

            for (int i = 0; i < getDimensions(); i++) {
                if (i > 0) {
                    sb.append(",");
                }

                sb.append(uncertaintyPlusses[i]);
            } 

            sb.append(")");
            if (!equals) {
                sb.append(" -(");
                for (int i = 0; i < getDimensions(); i++) {
                    if (i > 0) {
                        sb.append(",");
                    }
                    
                    sb.append(uncertaintyMinuses[i]);
                } 
            }
        }

        return (sb.toString());
    }
}
