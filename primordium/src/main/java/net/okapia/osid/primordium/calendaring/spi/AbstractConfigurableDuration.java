//
// AbstractConfigurableDuration.java
//
//     An abstract Duration.
//
//
// Tom Coppeto
// Okapia
// 2 July 2012
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;


/**
 *  An abstract Duration.
 */

public abstract class AbstractConfigurableDuration
    extends AbstractDuration
    implements org.osid.calendaring.Duration {
    
    private final net.okapia.osid.primordium.calendaring.CalendarDefinition calendar;
    private final net.okapia.osid.primordium.calendaring.TimeDefinition time;
    
    private BigDecimal bluemoons;
    private BigDecimal years;
    private BigDecimal months;
    private BigDecimal weeks;
    private BigDecimal days;
    private BigDecimal hours;
    private BigDecimal minutes;
    private BigDecimal seconds;
    private BigDecimal plancks;

    private org.osid.calendaring.Duration upper;
    private org.osid.calendaring.Duration lower;

    private String label;

    private static final long serialVersionUID = 1183811470L;


    /**
     *  Constructs a new <code>AbstractConfigurableDuration</code>.
     * 
     *  @param calendarDefinition the calendar definition
     *  @param timeDefinition the time definition
     *  @param granularity the granularity
     *  @param granularityMultiplier the granularity multiplier
     *  @param amount the duration amount
     *  @throws org.osid.InvalidArgumentException or
     *          <code>granularityMultiplier</code> 
     *          is negative
     *  @throws org.osid.NullArgumentException
     *          <code>calendarDefinition</code>,
     *          <code>timeDefinition</code>, <code>granularity</code>,
     *          or <code>amount</code> <code>null</code>
     */

    protected AbstractConfigurableDuration(net.okapia.osid.primordium.calendaring.CalendarDefinition calendarDefinition,
                                           net.okapia.osid.primordium.calendaring.TimeDefinition timeDefinition,
                                           org.osid.calendaring.DateTimeResolution granularity, 
                                           long granularityMultiplier, BigInteger amount) {
        
        super(calendarDefinition.getCalendarType(), timeDefinition.getTimeType(), 
              granularity, granularityMultiplier);
 
        nullarg(amount, "amount");
        
        this.calendar = calendarDefinition;
        this.time     = timeDefinition;

        normalizeAmount(new BigDecimal(amount));

        return;
    }


    /**
     *  Constructs a new <code>AbstractConfigurableDuration</code>.
     * 
     *  @param calendarDefinition the calendar definition
     *  @param timeDefinition the time definition
     *  @param granularity the resolution
     *  @param granularityMultiplier the resolution multiplier
     *  @param amount the amount of the duration
     *  @param uncertaintyUnits the units of uncertainty
     *  @param uncertaintyPlus uncertainty in the positive direction
     *  @param uncertaintyMinus uncertainty in the negative direction
     *  @throws org.osid.InvalidArgumentException
     *          <code>granularityMultiplier</code,
     *          <code>uncertaintyPlus</code>, or
     *          <code>uncertaintyMinus</code> 
     *          is negative
     *  @throws org.osid.NullArgumentException
     *          <code>calendarType</code>, <code>timeType</code>,
     *          <code>granularity</code>, or
     *          <code>uncertaintyUnits</code> is <code>null</code>
     */

    protected AbstractConfigurableDuration(net.okapia.osid.primordium.calendaring.CalendarDefinition calendarDefinition,
                                           net.okapia.osid.primordium.calendaring.TimeDefinition timeDefinition,
                                           org.osid.calendaring.DateTimeResolution granularity, 
                                           long granularityMultiplier, BigInteger amount,
                                           org.osid.calendaring.DateTimeResolution uncertaintyUnits, 
                                           long uncertaintyPlus, long uncertaintyMinus) {
        
        super(calendarDefinition.getCalendarType(), timeDefinition.getTimeType(), 
              granularity, granularityMultiplier, uncertaintyUnits, 
              uncertaintyPlus, uncertaintyMinus);
 
        nullarg(amount, "amount");
        
        this.calendar = calendarDefinition;
        this.time     = timeDefinition;

        normalizeAmount(new BigDecimal(amount));

        return;
    }


    /**
     *  Gets the number of blue moons. 
     *
     *  @return the number of blue moons 
     */

    @OSID @Override
    public final BigDecimal getBluemoons() {
        return (this.bluemoons);
    }


    /**
     *  Gets the number of years. 
     *
     *  @return the number of years 
     */

    @OSID @Override
    public final BigDecimal getYears() {
        return (this.years);
    }


    /**
     *  Gets the number of months. 
     *
     *  @return the number of months 
     */

    @OSID @Override
    public final BigDecimal getMonths() {
        return (this.months);
    }


    /**
     *  Gets the number of weeks. 
     *
     *  @return the number of weeks 
     */

    @OSID @Override
    public final BigDecimal getWeeks() {
        return (this.weeks);
    }


    /**
     *  Gets the number of days. 
     *
     *  @return the number of days 
     */

    @OSID @Override
    public final BigDecimal getDays() {
        return (this.days);
    }


    /**
     *  Gets the number of hours. 
     *
     *  @return the number of hours 
     */

    @OSID @Override
    public final BigDecimal getHours() {
        return (this.hours);
    }


    /**
     *  Gets the number of minutes. 
     *
     *  @return the number of minutes 
     */

    @OSID @Override
    public final BigDecimal getMinutes() {
        return (this.minutes);
    }


    /**
     *  Gets the number of seconds. 
     *
     *  @return the number of seconds 
     */

    @OSID @Override
    public final BigDecimal getSeconds() {
        return (this.seconds);
    }


    /**
     *  Gets the number of Planck sseconds. A Planck is 10 
     *  quattuordecillionths of a second. 
     *
     *  @return the number of planck seconds 
     */

    @OSID @Override
    public final BigDecimal getPlanckSeconds() {
        return (this.plancks);
    }


    /**
     *  Gets the lower bound of the duration. The lower bound is the
     *  duration minus the uncertainty.
     *
     *  @return the duration lower bound
     */

    @OSIDBinding @Override
    public final org.osid.calendaring.Duration getLowerBound() {        
        if (this.lower == null) {
            this.lower = makeDuration(org.osid.calendaring.DateTimeResolution.PLANCKSECOND,
                                      1, calculateLowerBound());
        }

        return (this.lower);
    }


    /**
     *  Gets the upper bound of the duration. The lower bound is the
     *  duration plus the uncertainty.
     *
     *  @return the duration upper bound
     */

    @OSIDBinding @Override
    public final org.osid.calendaring.Duration getUpperBound() {
        if (this.upper == null) {
            this.upper = makeDuration(org.osid.calendaring.DateTimeResolution.PLANCKSECOND,
                                      1, calculateUpperBound());
        }

        return (this.upper);
    }


    protected net.okapia.osid.primordium.calendaring.CalendarDefinition getCalendarDefinition() {
        return (this.calendar);
    }


    protected net.okapia.osid.primordium.calendaring.TimeDefinition getTimeDefinition() {
        return (this.time);
    }


    /**
     *  Returns a string representation of this Duration.
     *
     *  All implementations are encouraged to supply this method to
     *  display a serialized Duration.
     *
     *  @return a string
     */

    @Override
    public String toString() {
        if (this.label == null) {
            this.label = formatLabel();
        }

        return (this.label);
    }


    protected org.osid.calendaring.Duration parseDuration(String str) {
        nullarg(str, "string");
        String s = str.trim().replace(" ", "").toLowerCase();
        StringBuilder num = new StringBuilder(str.length());

        boolean negative = false;
        if (s.charAt(0) == '-') {
            num.append("-");
            s = s.substring(1);
        } else if (s.charAt(0) == '+') {
            s = s.substring(1);
        }

        int i = 0;
        boolean inNumber = true;
        int digits = 0;
        int len = s.length();

        while (i < len) {
            if (Character.isDigit(s.charAt(i))) {
                num.append(s.charAt(i));
                if (inNumber) {
                    ++digits;
                }
            } else if ((s.charAt(i) == 'e') && (i < (len - 1))) {
                inNumber = false;
                
                if (!Character.isDigit(s.charAt(i + 1))) {
                    if ((s.charAt(i+1) == '+') || (s.charAt(i+1) == '-')) {
                        num.append('e'); 
                        ++i;
                        num.append(s.charAt(i));
                    } else {
                        break;
                    }
                } else {
                    num.append('e'); 
                }
            } else if (s.charAt(i) == '.') {
                num.append('.');
            } else if (s.charAt(i) != ',') {
                break;
            } 
            
            ++i;
        }

        org.osid.calendaring.DateTimeResolution granularity;
        try {
            granularity = getCalendarDefinition().getUnits(s.substring(i));
        } catch (org.osid.NotFoundException nfe) {
            try {
                granularity = getTimeDefinition().getUnits(s.substring(i));
            } catch (org.osid.NotFoundException nfe2) {
                throw new org.osid.InvalidArgumentException("cannot get granularity for " + str);
            }
        } 

        org.osid.calendaring.Duration ret;
        if (granularity == org.osid.calendaring.DateTimeResolution.INFINITY) {
            ret = makeDuration(granularity, 1, new BigInteger("0"));
        } else {            
            try {
                ret = makeDuration(granularity, 1, new BigDecimal(num.toString()).toBigInteger());
            } catch (NumberFormatException nfe) {
                throw new org.osid.InvalidArgumentException("cannot parse " + str, nfe);
            }
        }

        return (ret);
    }

                
    private String formatLabel() {
	StringBuilder sb = new StringBuilder();
	
        switch (getGranularity()) {
        case INFINITY:
            return ("∞");
        case AEON:
            sb.append(formatOutputNumber(getAeons()));
            break;
        case EPOCH:
            sb.append(formatOutputNumber(getEpochs()));
            break;
        case GLACIAL:
            sb.append(formatOutputNumber(getMillennia().divide(new BigDecimal("100"))));
            break;            
        case MILLENNIA:
            sb.append(formatOutputNumber(getMillennia()));
            break;
        case CENTURY:
            sb.append(formatOutputNumber(getCenturies()));
            break;
        case DECADE:
            sb.append(formatOutputNumber(getYears().divide(new BigDecimal("10"))));
            break;
        case BLUEMOON:
            sb.append(formatOutputNumber(getBluemoons()));
            break;
        case YEAR:
            sb.append(formatOutputNumber(getYears()));
            break;
        case MONTH:
            sb.append(formatOutputNumber(getMonths()));
            break;
        case WEEK:
            sb.append(formatOutputNumber(getWeeks()));
            break;
        case DAY:
            sb.append(formatOutputNumber(getDays()));
            break;
        case HOUR:
            sb.append(formatOutputNumber(getHours()));
            break;
        case HALF_HOUR:
        case QUARTER_HOUR:
        case MINUTE:
            sb.append(formatOutputNumber(getMinutes()));
            break;
        case SECOND:
            sb.append(formatOutputNumber(getSeconds()));
            break;
        case MILLISECOND:
            sb.append(formatOutputNumber(getMilliseconds()));
            break;
        case MICROSECOND:
            sb.append(formatOutputNumber(getMicroseconds()));
            break;
        case NANOSECOND:
            sb.append(formatOutputNumber(getNanoseconds()));
            break;
        case PICOSECOND:
            sb.append(formatOutputNumber(getPicoseconds()));
            break;
        case FEMTOSECOND:
            sb.append(formatOutputNumber(getFemtoseconds()));
            break;
        case ATTOSECOND:
            sb.append(formatOutputNumber(getAttoseconds()));
            break;
        case ZEPTOSECOND:
            sb.append(formatOutputNumber(getZeptoseconds()));
            break;
        case YOCTOSECOND:
            sb.append(formatOutputNumber(getYoctoseconds()));
            break;
        case XOXXOSECOND:
            sb.append(formatOutputNumber(getXoxxoseconds()));
            break;
        case WEEBLESECOND:
            sb.append(formatOutputNumber(getWeebleseconds()));
            break;
        case VATOSECOND:
            sb.append(formatOutputNumber(getVatoseconds()));
            break;
        case UNDASECOND:
            sb.append(formatOutputNumber(getUndaseconds()));
            break;
        case PLANCKSECOND:
            sb.append(formatOutputNumber(getPlanckSeconds()));
            break;
        default:
            throw new org.osid.IllegalStateException("unknown units");
        }

        sb.append (" ");
        if (sb.toString().equals("1 ") || sb.toString().equals("-1 ")) {
            if (getGranularity().ordinal() >= org.osid.calendaring.DateTimeResolution.DAY.ordinal()) {
                sb.append(getCalendarDefinition().getLabel(getGranularity()));
            } else {
                sb.append(getTimeDefinition().getLabel(getGranularity()));
            }
        } else {
            if (getGranularity().ordinal() >= org.osid.calendaring.DateTimeResolution.DAY.ordinal()) {
                sb.append(getCalendarDefinition().getPluralLabel(getGranularity()));
            } else {
                sb.append(getTimeDefinition().getPluralLabel(getGranularity()));
            }
        }

	return (sb.toString());
    }


    protected String formatOutputNumber(BigDecimal number) {
        StringBuilder sb = new StringBuilder();
        String[] parts = number.toPlainString().split("\\.");

        int length = parts[0].length();
        int places = 0;

        for (int i = length - 1; i >= 0; i--) {
            if ((places > 0) && ((places % 3) == 0)) {
                sb.insert(0, ',');
            } 

            ++places;

            sb.insert(0, parts[0].charAt(i));
        }

        if (parts.length > 1) {
            sb.append('.');
            sb.append(parts[1]);
        }

        return (sb.toString());
    }


    private void normalizeAmount(BigDecimal amount) {
        org.osid.calendaring.DateTimeResolution granularity = getGranularity();

        if (granularity == org.osid.calendaring.DateTimeResolution.INFINITY) {
            normalizeByInfinity();
        } else if (granularity == org.osid.calendaring.DateTimeResolution.BLUEMOON) {
            normalizeByBluemoons(amount);
        } else if (granularity.ordinal() >= org.osid.calendaring.DateTimeResolution.YEAR.ordinal()) {
            normalizeByYears(amount);
        } else if (granularity.ordinal() >= org.osid.calendaring.DateTimeResolution.MONTH.ordinal()) {
            normalizeByMonths(amount);
        } else if (granularity.ordinal() >= org.osid.calendaring.DateTimeResolution.WEEK.ordinal()) {
            normalizeByWeeks(amount);
        } else if (granularity.ordinal() >= org.osid.calendaring.DateTimeResolution.DAY.ordinal()) {
            normalizeByDays(amount);
        } else if (granularity.ordinal() >= org.osid.calendaring.DateTimeResolution.QUARTER_HOUR.ordinal()) {
            normalizeByHours(amount);
        } else if (granularity.ordinal() >= org.osid.calendaring.DateTimeResolution.MINUTE.ordinal()) {
            normalizeByMinutes(amount);
        } else if (granularity.ordinal() >= org.osid.calendaring.DateTimeResolution.UNDASECOND.ordinal()) {
            normalizeBySeconds(amount);
        } else if (granularity.ordinal() >= org.osid.calendaring.DateTimeResolution.PLANCKSECOND.ordinal()) {
            normalizeByPlancks(amount);
        } else {
            throw new org.osid.IllegalStateException("unknown granularity: " + granularity.getDisplayName());
        }
    }


    private void normalizeByInfinity() {
        
        this.years     = new BigDecimal(0);
        this.bluemoons = new BigDecimal(0);
        this.months    = new BigDecimal(0);
        this.weeks     = new BigDecimal(0);
        this.days      = new BigDecimal(0);
        this.hours     = new BigDecimal(0);
        this.minutes   = new BigDecimal(0);
        this.seconds   = new BigDecimal(0);
        this.plancks   = new BigDecimal(0);

        return;
    }


    private void normalizeByYears(BigDecimal amount) {
        switch (getGranularity()) {
        case AEON:
            this.years  = amount.multiply(new BigDecimal("1e9"));
            break;
        case EPOCH:
            this.years  = amount.multiply(new BigDecimal("1e6"));
            break;
        case GLACIAL:
            this.years  = amount.multiply(new BigDecimal("1e5"));
            break;
        case MILLENNIA:
            this.years  = amount.multiply(new BigDecimal("1e3"));
            break;
        case CENTURY:
            this.years  = amount.multiply(new BigDecimal("1e2"));
            break;
        case DECADE:
            this.years  = amount.multiply(new BigDecimal("1e1"));
            break;
        case YEAR:
            this.years  = amount;
            break;
        default:
            throw new org.osid.IllegalStateException("unexpected " + getGranularity());
        }

        try {
            this.bluemoons = this.years.divide(getYearsPerBluemoon());
        } catch (ArithmeticException ae) {
            this.bluemoons = this.years.divide(getYearsPerBluemoon(),
                                               new MathContext(getYearsPerBluemoon().precision(),
                                                               RoundingMode.HALF_UP));
        }
 
        this.months    = this.years.multiply(getMonthsPerYear());
        this.weeks     = this.years.multiply(getWeeksPerYear());
        this.days      = this.years.multiply(getDaysPerYear());
        this.hours     = this.days.multiply(getHoursPerDay());
        this.minutes   = this.hours.multiply(getMinutesPerHour());
        this.seconds   = this.minutes.multiply(getSecondsPerMinute());
        this.plancks   = this.seconds.multiply(getPlancksPerSecond());

        return;
    }


    private void normalizeByBluemoons(BigDecimal amount) {

        switch (getGranularity()) {
        case BLUEMOON:
            this.bluemoons  = amount;
            break;
        default:
            throw new org.osid.IllegalStateException("unexpected " + getGranularity());
        }

        this.years   = this.bluemoons.multiply(getYearsPerBluemoon());
        this.months  = this.years.multiply(getMonthsPerYear());
        this.weeks   = this.years.multiply(getWeeksPerYear());
        this.days    = this.years.multiply(getDaysPerYear());
        this.hours   = this.days.multiply(getHoursPerDay());
        this.minutes = this.hours.multiply(getMinutesPerHour());
        this.seconds = this.minutes.multiply(getSecondsPerMinute());
        this.plancks = this.seconds.multiply(getPlancksPerSecond());
        
        return;
        
    }


    private void normalizeByMonths(BigDecimal amount) {
        switch (getGranularity()) {
        case MONTH:
            this.months = amount;
            break;
        default:
            throw new org.osid.IllegalStateException("unexpected " + getGranularity());
        }

        BigDecimal months = getMonthsPerYear();

        try {
            this.years     = this.months.divide(getMonthsPerYear());
        } catch (ArithmeticException ae) {
            this.years     = this.months.divide(getMonthsPerYear(),                   
                                                new MathContext(getMonthsPerYear().precision(),
                                                                RoundingMode.HALF_UP));
        }

        try {
            this.bluemoons = this.years.divide(getYearsPerBluemoon());
        } catch (ArithmeticException ae) {
            this.bluemoons = this.years.divide(getYearsPerBluemoon(),
                                               new MathContext(getYearsPerBluemoon().precision(),
                                                               RoundingMode.HALF_UP));
        }

        this.days      = this.months.multiply(getDaysPerMonth());
        this.weeks     = this.years.multiply(getWeeksPerYear());
        this.hours     = this.days.multiply(getHoursPerDay());
        this.minutes   = this.hours.multiply(getMinutesPerHour());
        this.seconds   = this.minutes.multiply(getSecondsPerMinute());
        this.plancks   = this.seconds.multiply(getPlancksPerSecond());
        
        return;
    }


    private void normalizeByWeeks(BigDecimal amount) {
        switch (getGranularity()) {
        case WEEK:
            this.weeks = amount;
            break;
        default:
            throw new org.osid.IllegalStateException("unexpected " + getGranularity());
        }

        try {
            this.years     = this.weeks.divide(getWeeksPerYear());
        } catch (ArithmeticException ae) {
            this.years     = this.weeks.divide(getMonthsPerYear(),                   
                                               new MathContext(getWeeksPerYear().precision(),
                                                               RoundingMode.HALF_UP));
        }

        try {
            this.bluemoons = this.years.divide(getYearsPerBluemoon());
        } catch (ArithmeticException ae) {
            this.bluemoons = this.years.divide(getYearsPerBluemoon(),
                                               new MathContext(getYearsPerBluemoon().precision(),
                                                               RoundingMode.HALF_UP));
        }

        this.months    = this.years.multiply(getMonthsPerYear());
        this.days      = this.weeks.multiply(getDaysPerWeek());
        this.hours     = this.days.multiply(getHoursPerDay());
        this.minutes   = this.hours.multiply(getMinutesPerHour());
        this.seconds   = this.minutes.multiply(getSecondsPerMinute());
        this.plancks   = this.seconds.multiply(getPlancksPerSecond());
        
        return;
    }


    private void normalizeByDays(BigDecimal amount) {
        switch (getGranularity()) {
        case DAY:
            this.days = amount;
            break;
        default:
            throw new org.osid.IllegalStateException("unexpected " + getGranularity());
        }

        try {
            this.years     = this.days.divide(getDaysPerYear());
        } catch (ArithmeticException ae) {
            this.years     = this.days.divide(getDaysPerYear(),                   
                                              new MathContext(getDaysPerYear().precision(),
                                                              RoundingMode.HALF_UP));
        }

        try {
            this.bluemoons = this.years.divide(getYearsPerBluemoon());
        } catch (ArithmeticException ae) {
            this.bluemoons = this.years.divide(getYearsPerBluemoon(),
                                               new MathContext(getYearsPerBluemoon().precision(),
                                                               RoundingMode.HALF_UP));
        }

        try {
            this.weeks     = this.days.divide(getDaysPerWeek());
        } catch (ArithmeticException ae) {
            this.weeks     = this.days.divide(getDaysPerWeek(),                   
                                              new MathContext(getDaysPerWeek().precision(),
                                                              RoundingMode.HALF_UP));
        }

        this.months    = this.years.multiply(getMonthsPerYear());
        this.hours     = this.days.multiply(getHoursPerDay());
        this.minutes   = this.hours.multiply(getMinutesPerHour());
        this.seconds   = this.minutes.multiply(getSecondsPerMinute());
        this.plancks   = this.seconds.multiply(getPlancksPerSecond());
        
        return;
    }


    private void normalizeByHours(BigDecimal amount) {
        switch (getGranularity()) {
        case HOUR:
            this.hours = amount;
            break;
        case HALF_HOUR:
            this.hours = amount.divide(new BigDecimal("2"));
            break;
        case QUARTER_HOUR:
            this.hours = amount.divide(new BigDecimal("4"));
            break;
        default:
            throw new org.osid.IllegalStateException("unexpected " + getGranularity());
        }

        BigDecimal hoursInDay   = getHoursPerDay();
        BigDecimal hoursInWeek  = hoursInDay.multiply(getDaysPerWeek());
        BigDecimal hoursInMonth = hoursInDay.multiply(getDaysPerMonth());
        BigDecimal hoursInYear  = hoursInDay.multiply(getDaysPerYear());
        
        try {
            this.years     = this.hours.divide(hoursInYear);
        } catch (ArithmeticException ae) {
            this.years     = this.hours.divide(hoursInYear,
                                              new MathContext(hoursInDay.precision(),
                                                              RoundingMode.HALF_UP));
        }

        try {
            this.bluemoons = this.years.divide(getYearsPerBluemoon());
        } catch (ArithmeticException ae) {
            this.bluemoons = this.years.divide(getYearsPerBluemoon(),
                                               new MathContext(getYearsPerBluemoon().precision(),
                                                               RoundingMode.HALF_UP));
        }

        try {
            this.months    = this.hours.divide(hoursInMonth);
        } catch (ArithmeticException ae) {
            this.months    = this.hours.divide(hoursInMonth,
                                              new MathContext(hoursInMonth.precision(),
                                                              RoundingMode.HALF_UP));
        }

        try {
            this.weeks     = this.hours.divide(hoursInWeek);
        } catch (ArithmeticException ae) {
            this.weeks     = this.hours.divide(hoursInWeek,
                                              new MathContext(hoursInWeek.precision(),
                                                              RoundingMode.HALF_UP));
        }

        try {
            this.days     = this.hours.divide(hoursInDay);
        } catch (ArithmeticException ae) {
            this.days     = this.hours.divide(hoursInDay,
                                              new MathContext(hoursInDay.precision(),
                                                              RoundingMode.HALF_UP));
        }

        this.minutes   = this.hours.multiply(getMinutesPerHour());
        this.seconds   = this.minutes.multiply(getSecondsPerMinute());
        this.plancks   = this.seconds.multiply(getPlancksPerSecond());

        return;
    }


    private void normalizeByMinutes(BigDecimal amount) {
        switch (getGranularity()) {
        case MINUTE:
            this.minutes = amount;
            break;
        default:
            throw new org.osid.IllegalStateException("unexpected " + getGranularity());
        }

        BigDecimal minutesInHour  = getMinutesPerHour();
        BigDecimal minutesInDay   = getMinutesPerHour().multiply(getHoursPerDay());
        BigDecimal minutesInWeek  = minutesInDay.multiply(getDaysPerWeek());
        BigDecimal minutesInMonth = minutesInDay.multiply(getDaysPerMonth());
        BigDecimal minutesInYear  = minutesInDay.multiply(getDaysPerYear());
        
        try {
            this.years     = this.minutes.divide(minutesInYear);
        } catch (ArithmeticException ae) {
            this.years     = this.minutes.divide(minutesInYear,
                                              new MathContext(minutesInDay.precision(),
                                                              RoundingMode.HALF_UP));
        }

        try {
            this.bluemoons = this.years.divide(getYearsPerBluemoon());
        } catch (ArithmeticException ae) {
            this.bluemoons = this.years.divide(getYearsPerBluemoon(),
                                               new MathContext(getYearsPerBluemoon().precision(),
                                                               RoundingMode.HALF_UP));
        }

        try {
            this.months    = this.minutes.divide(minutesInMonth);
        } catch (ArithmeticException ae) {
            this.months    = this.minutes.divide(minutesInMonth,
                                              new MathContext(minutesInMonth.precision(),
                                                              RoundingMode.HALF_UP));
        }

        try {
            this.weeks     = this.minutes.divide(minutesInWeek);
        } catch (ArithmeticException ae) {
            this.weeks     = this.minutes.divide(minutesInWeek,
                                              new MathContext(minutesInWeek.precision(),
                                                              RoundingMode.HALF_UP));
        }

        try {
            this.days     = this.minutes.divide(minutesInDay);
        } catch (ArithmeticException ae) {
            this.days     = this.minutes.divide(minutesInDay,
                                              new MathContext(minutesInDay.precision(),
                                                              RoundingMode.HALF_UP));
        }

        try {
            this.hours    = this.minutes.divide(minutesInHour);
        } catch (ArithmeticException ae) {
            this.hours    = this.minutes.divide(minutesInHour,
                                              new MathContext(minutesInHour.precision(),
                                                              RoundingMode.HALF_UP));
        }

        this.seconds   = this.minutes.multiply(getSecondsPerMinute());
        this.plancks   = this.seconds.multiply(getPlancksPerSecond());

        return;
    }


    private void normalizeBySeconds(BigDecimal amount) {
        switch (getGranularity()) {
        case SECOND:
            this.seconds = amount;
            break;
        case MILLISECOND:
            this.seconds = amount.divide(new BigDecimal("1e3"));
            break;
        case MICROSECOND:
            this.seconds = amount.divide(new BigDecimal("1e6"));
            break;
        case NANOSECOND:
            this.seconds = amount.divide(new BigDecimal("1e9"));
            break;
        case PICOSECOND:
            this.seconds = amount.divide(new BigDecimal("1e12"));
            break;
        case FEMTOSECOND:
            this.seconds = amount.divide(new BigDecimal("1e15"));
            break;
        case ATTOSECOND:
            this.seconds = amount.divide(new BigDecimal("1e18"));
            break;
        case ZEPTOSECOND:
            this.seconds = amount.divide(new BigDecimal("1e21"));
            break;
        case YOCTOSECOND:
            this.seconds = amount.divide(new BigDecimal("1e24"));
            break;
        case XOXXOSECOND:
            this.seconds = amount.divide(new BigDecimal("1e27"));
            break;
        case WEEBLESECOND:
            this.seconds = amount.divide(new BigDecimal("1e30"));
            break;
        case VATOSECOND:
            this.seconds = amount.divide(new BigDecimal("1e33"));
            break;
        case UNDASECOND:
            this.seconds = amount.divide(new BigDecimal("1e36"));
            break;
        default:
            throw new org.osid.IllegalStateException("unexpected " + getGranularity());
        }

        BigDecimal secondsInMinute = getSecondsPerMinute();
        BigDecimal secondsInHour   = getMinutesPerHour().multiply(getSecondsPerMinute());
        BigDecimal secondsInDay    = secondsInHour.multiply(getHoursPerDay());
        BigDecimal secondsInWeek   = secondsInDay.multiply(getDaysPerWeek());
        BigDecimal secondsInMonth  = secondsInDay.multiply(getDaysPerMonth());
        BigDecimal secondsInYear   = secondsInDay.multiply(getDaysPerYear());

        try {
            this.years     = this.seconds.divide(secondsInYear);
        } catch (ArithmeticException ae) {
            this.years     = this.seconds.divide(secondsInYear,
                                              new MathContext(secondsInDay.precision(),
                                                              RoundingMode.HALF_UP));
        }

        try {
            this.bluemoons = this.years.divide(getYearsPerBluemoon());
        } catch (ArithmeticException ae) {
            this.bluemoons = this.years.divide(getYearsPerBluemoon(),
                                               new MathContext(getYearsPerBluemoon().precision(),
                                                               RoundingMode.HALF_UP));
        }

        try {
            this.months    = this.seconds.divide(secondsInMonth);
        } catch (ArithmeticException ae) {
            this.months    = this.seconds.divide(secondsInMonth,
                                              new MathContext(secondsInMonth.precision(),
                                                              RoundingMode.HALF_UP));
        }

        try {
            this.weeks     = this.seconds.divide(secondsInWeek);
        } catch (ArithmeticException ae) {
            this.weeks     = this.seconds.divide(secondsInWeek,
                                              new MathContext(secondsInWeek.precision(),
                                                              RoundingMode.HALF_UP));
        }

        try {
            this.days     = this.seconds.divide(secondsInDay);
        } catch (ArithmeticException ae) {
            this.days     = this.seconds.divide(secondsInDay,
                                              new MathContext(secondsInDay.precision(),
                                                              RoundingMode.HALF_UP));
        }

        try {
            this.hours    = this.seconds.divide(secondsInHour);
        } catch (ArithmeticException ae) {
            this.hours    = this.seconds.divide(secondsInHour,
                                              new MathContext(secondsInHour.precision(),
                                                              RoundingMode.HALF_UP));
        }

        try {
            this.minutes  = this.seconds.divide(secondsInMinute);
        } catch (ArithmeticException ae) {
            this.minutes  = this.seconds.divide(secondsInMinute,
                                              new MathContext(secondsInMinute.precision(),
                                                              RoundingMode.HALF_UP));
        }

        this.plancks   = this.seconds.multiply(getPlancksPerSecond());

        return;
    }


    private void normalizeByPlancks(BigDecimal amount) {
        switch (getGranularity()) {
        case PLANCKSECOND:
            this.plancks = amount;
            break;
        default:
            throw new org.osid.IllegalStateException("unexpected " + getGranularity());
        }

        BigDecimal plancksInSecond  = getPlancksPerSecond();
        BigDecimal plancksInMinute  = plancksInSecond.multiply(getSecondsPerMinute());
        BigDecimal plancksInHour    = plancksInMinute.multiply(getMinutesPerHour());
        BigDecimal plancksInDay     = plancksInHour.multiply(getHoursPerDay());
        BigDecimal plancksInWeek    = plancksInDay.multiply(getDaysPerWeek());
        BigDecimal plancksInMonth   = plancksInDay.multiply(getDaysPerMonth());
        BigDecimal plancksInYear    = plancksInDay.multiply(getDaysPerYear());
        
        try {
            this.years     = this.plancks.divide(plancksInYear);
        } catch (ArithmeticException ae) {
            this.years     = this.plancks.divide(plancksInYear,
                                              new MathContext(plancksInDay.precision(),
                                                              RoundingMode.HALF_UP));
        }

        try {
            this.bluemoons = this.years.divide(getYearsPerBluemoon());
        } catch (ArithmeticException ae) {
            this.bluemoons = this.years.divide(getYearsPerBluemoon(),
                                               new MathContext(getYearsPerBluemoon().precision(),
                                                               RoundingMode.HALF_UP));
        }

        try {
            this.months    = this.plancks.divide(plancksInMonth);
        } catch (ArithmeticException ae) {
            this.months    = this.plancks.divide(plancksInMonth,
                                              new MathContext(plancksInMonth.precision(),
                                                              RoundingMode.HALF_UP));
        }

        try {
            this.weeks     = this.plancks.divide(plancksInWeek);
        } catch (ArithmeticException ae) {
            this.weeks     = this.plancks.divide(plancksInWeek,
                                              new MathContext(plancksInWeek.precision(),
                                                              RoundingMode.HALF_UP));
        }

        try {
            this.days     = this.plancks.divide(plancksInDay);
        } catch (ArithmeticException ae) {
            this.days     = this.plancks.divide(plancksInDay,
                                              new MathContext(plancksInDay.precision(),
                                                              RoundingMode.HALF_UP));
        }

        try {
            this.hours    = this.plancks.divide(plancksInHour);
        } catch (ArithmeticException ae) {
            this.hours    = this.plancks.divide(plancksInHour,
                                              new MathContext(plancksInHour.precision(),
                                                              RoundingMode.HALF_UP));
        }

        try {
            this.minutes  = this.plancks.divide(plancksInMinute);
        } catch (ArithmeticException ae) {
            this.minutes  = this.plancks.divide(plancksInMinute,
                                              new MathContext(plancksInMinute.precision(),
                                                              RoundingMode.HALF_UP));
        }

        try {
            this.seconds  = this.plancks.divide(plancksInSecond);
        } catch (ArithmeticException ae) {
            this.seconds  = this.plancks.divide(plancksInSecond,
                                              new MathContext(plancksInSecond.precision(),
                                                              RoundingMode.HALF_UP));
        }

        return;
    }


    protected abstract org.osid.calendaring.Duration makeDuration(org.osid.calendaring.DateTimeResolution granularity,
                                                                  long granularityMultiplier, 
                                                                  BigInteger amount);


    private BigInteger calculateLowerBound() {
        BigDecimal plancks = getPlanckSeconds();

        if (definesUncertainty()) {
            BigDecimal minus = getUncertaintyAmount(getUncertaintyUnits(), getUncertaintyMinus());
            plancks = plancks.subtract(minus);
        }

        return (plancks.toBigInteger());   
    }


    private BigInteger calculateUpperBound() {
        BigDecimal plancks = getPlanckSeconds();
        BigDecimal plus = new BigDecimal(0);

        switch (getGranularity()) {
        case INFINITY:
            break;
        case AEON:
            plus = getPlancksPerYear().multiply(new BigDecimal(1e9));
            break;
        case EPOCH:
            plus = getPlancksPerYear().multiply(new BigDecimal(1e6));
            break;
        case GLACIAL:
            plus = getPlancksPerYear().multiply(new BigDecimal(1e5));
            break;
        case MILLENNIA:
            plus = getPlancksPerYear().multiply(new BigDecimal(1e3));
            break;
        case CENTURY:
            plus = getPlancksPerYear().multiply(new BigDecimal(1e2));
            break;
        case DECADE:
            plus = getPlancksPerYear().multiply(new BigDecimal(1e1));
            break;
        case BLUEMOON:
            plus = getPlancksPerYear().multiply(getYearsPerBluemoon());
            break;
        case YEAR:
            plus = getPlancksPerYear();
            break;
        case MONTH:
            plus = getPlancksPerDay().multiply(getDaysPerMonth());
            break;
        case WEEK:
            plus = getPlancksPerDay().multiply(getDaysPerWeek());
            break;
        case DAY:
            plus = getPlancksPerDay();
            break;
        case HOUR:
            plus = getPlancksPerSecond().multiply(getSecondsPerMinute()).multiply(getMinutesPerHour());
            break;
        case HALF_HOUR:
            plus = getPlancksPerSecond().multiply(getSecondsPerMinute()).multiply(getMinutesPerHour().divide(new BigDecimal(2)));   
            break;
        case QUARTER_HOUR:
            plus = getPlancksPerSecond().multiply(getSecondsPerMinute()).multiply(getMinutesPerHour().divide(new BigDecimal(4)));    
            break;
        case MINUTE:
            plus = getPlancksPerSecond().multiply(getSecondsPerMinute());
            break;
        case SECOND:
            plus = getPlancksPerSecond();
            break;
        case MILLISECOND:
            plus = getPlancksPerSecond().divide(new BigDecimal("1e3"));
            break;
        case MICROSECOND:
            plus = getPlancksPerSecond().divide(new BigDecimal("1e6"));
            break;
        case NANOSECOND:
            plus = getPlancksPerSecond().divide(new BigDecimal("1e9"));
            break;
        case PICOSECOND:
            plus = getPlancksPerSecond().divide(new BigDecimal("1e12"));
            break;
        case FEMTOSECOND:
            plus = getPlancksPerSecond().divide(new BigDecimal("1e15"));
            break;
        case ATTOSECOND:
            plus = getPlancksPerSecond().divide(new BigDecimal("1e18"));
            break;
        case ZEPTOSECOND:
            plus = getPlancksPerSecond().divide(new BigDecimal("1e21"));
            break;
        case YOCTOSECOND:
            plus = getPlancksPerSecond().divide(new BigDecimal("1e24"));
            break;
        case XOXXOSECOND:
            plus = getPlancksPerSecond().divide(new BigDecimal("1e27"));
            break;
        case WEEBLESECOND:
            plus = getPlancksPerSecond().divide(new BigDecimal("1e30"));
            break;
        case VATOSECOND:
            plus = getPlancksPerSecond().divide(new BigDecimal("1e33"));
            break;
        case UNDASECOND:
            plus = getPlancksPerSecond().divide(new BigDecimal("1e36"));
            break;
        case PLANCKSECOND:
            plus = new BigDecimal(1);
            break;
        default:
            throw new org.osid.IllegalStateException("unknown resolution");
        }        

        plancks = plancks.add(plus.multiply(new BigDecimal(getGranularityMultiplier())));
        plancks = plancks.subtract(new BigDecimal(1));

        if (definesUncertainty()) {
            plancks = plancks.add(getUncertaintyAmount(getUncertaintyUnits(), getUncertaintyPlus()));
        }

        return (plancks.toBigInteger());
    }


    private BigDecimal getUncertaintyAmount(org.osid.calendaring.DateTimeResolution units,
                                            long uncertainty) {
        BigDecimal amount = new BigDecimal(0);

        switch (units) {
        case INFINITY:
            break;
        case AEON:
            amount = getPlancksPerYear().multiply(new BigDecimal(uncertainty)).multiply(new BigDecimal(uncertainty * 1e9));
            break;
        case EPOCH:
            amount = getPlancksPerYear().multiply(new BigDecimal(uncertainty)).multiply(new BigDecimal(uncertainty * 1e6));
            break;
        case GLACIAL:
            amount = getPlancksPerYear().multiply(new BigDecimal(uncertainty)).multiply(new BigDecimal(uncertainty * 1e5));
            break;
        case MILLENNIA:
            amount = getPlancksPerYear().multiply(new BigDecimal(uncertainty)).multiply(new BigDecimal(uncertainty * 1e3));
            break;
        case CENTURY:
            amount = getPlancksPerYear().multiply(new BigDecimal(uncertainty)).multiply(new BigDecimal(uncertainty * 1e2));
            break;
        case DECADE:
            amount = getPlancksPerYear().multiply(new BigDecimal(uncertainty)).multiply(new BigDecimal(uncertainty * 1e1));
            break;
        case BLUEMOON:
            amount = getPlancksPerYear().multiply(new BigDecimal(uncertainty)).multiply(new BigDecimal(uncertainty)).multiply(getYearsPerBluemoon());
            break;
        case YEAR:
            amount = getPlancksPerYear().multiply(new BigDecimal(uncertainty)).multiply(new BigDecimal(uncertainty));
            break;
        case MONTH:
            amount = getPlancksPerDay().multiply(new BigDecimal(uncertainty)).multiply(new BigDecimal(uncertainty)).multiply(getDaysPerMonth());
            break;
        case WEEK:
            amount = getPlancksPerDay().multiply(new BigDecimal(uncertainty)).multiply(new BigDecimal(uncertainty)).multiply(getDaysPerWeek());
            break;
        case DAY:
            amount = getPlancksPerDay().multiply(new BigDecimal(uncertainty)).multiply(new BigDecimal(uncertainty));
            break;
        case HOUR:
            amount = getPlancksPerSecond().multiply(new BigDecimal(uncertainty)).multiply(new BigDecimal(uncertainty)).multiply(getSecondsPerMinute()).multiply(getMinutesPerHour());
            break;
        case HALF_HOUR:
            amount = getPlancksPerSecond().multiply(new BigDecimal(uncertainty)).multiply(new BigDecimal(uncertainty)).multiply(getSecondsPerMinute()).multiply(getMinutesPerHour()).divide(new BigDecimal(2));
            break;
        case QUARTER_HOUR:
            amount = getPlancksPerSecond().multiply(new BigDecimal(uncertainty)).multiply(new BigDecimal(uncertainty)).multiply(getSecondsPerMinute()).multiply(getMinutesPerHour()).divide(new BigDecimal(4));
            break;
        case MINUTE:
            amount = getPlancksPerSecond().multiply(new BigDecimal(uncertainty)).multiply(new BigDecimal(uncertainty)).multiply(getSecondsPerMinute());
            break;
        case SECOND:
            amount = getPlancksPerSecond().multiply(new BigDecimal(uncertainty)).multiply(new BigDecimal(uncertainty));
            break;
        case MILLISECOND:
            amount = getPlancksPerSecond().multiply(new BigDecimal(uncertainty)).multiply(new BigDecimal(uncertainty)).divide(new BigDecimal("1e3"));
            break;
        case MICROSECOND:
            amount = getPlancksPerSecond().multiply(new BigDecimal(uncertainty)).multiply(new BigDecimal(uncertainty)).divide(new BigDecimal("1e6"));
            break;
        case NANOSECOND:
            amount = getPlancksPerSecond().multiply(new BigDecimal(uncertainty)).multiply(new BigDecimal(uncertainty)).divide(new BigDecimal("1e9"));
            break;
        case PICOSECOND:
            amount = getPlancksPerSecond().multiply(new BigDecimal(uncertainty)).multiply(new BigDecimal(uncertainty)).divide(new BigDecimal("1e12"));
            break;
        case FEMTOSECOND:
            amount = getPlancksPerSecond().multiply(new BigDecimal(uncertainty)).multiply(new BigDecimal(uncertainty)).divide(new BigDecimal("1e15"));
            break;
        case ATTOSECOND:
            amount = getPlancksPerSecond().multiply(new BigDecimal(uncertainty)).multiply(new BigDecimal(uncertainty)).divide(new BigDecimal("1e18"));
            break;
        case ZEPTOSECOND:
            amount = getPlancksPerSecond().multiply(new BigDecimal(uncertainty)).multiply(new BigDecimal(uncertainty)).divide(new BigDecimal("1e21"));
            break;
        case YOCTOSECOND:
            amount = getPlancksPerSecond().multiply(new BigDecimal(uncertainty)).multiply(new BigDecimal(uncertainty)).divide(new BigDecimal("1e24"));
            break;
        case XOXXOSECOND:
            amount = getPlancksPerSecond().multiply(new BigDecimal(uncertainty)).multiply(new BigDecimal(uncertainty)).divide(new BigDecimal("1e27"));
            break;
        case WEEBLESECOND:
            amount = getPlancksPerSecond().multiply(new BigDecimal(uncertainty)).multiply(new BigDecimal(uncertainty)).divide(new BigDecimal("1e30"));
            break;
        case VATOSECOND:
            amount = getPlancksPerSecond().multiply(new BigDecimal(uncertainty)).multiply(new BigDecimal(uncertainty)).divide(new BigDecimal("1e33"));
            break;
        case UNDASECOND:
            amount = getPlancksPerSecond().multiply(new BigDecimal(uncertainty)).multiply(new BigDecimal(uncertainty)).divide(new BigDecimal("1e36"));
            break;
        case PLANCKSECOND:
            amount = new BigDecimal(uncertainty).multiply(new BigDecimal(uncertainty));
            break;
        default:
            throw new org.osid.IllegalStateException("unknown resolution");
        }        

        return (amount);
    }


    private BigDecimal getYearsPerBluemoon() {
        return (getCalendarDefinition().getYearsPerBluemoon());
    }


    private BigDecimal getMonthsPerYear() {
        return (new BigDecimal(getCalendarDefinition().getMonthsPerYear()));
    }


    private BigDecimal getWeeksPerYear() {
        return (getCalendarDefinition().getAverageWeeksPerYear());
    }


    private BigDecimal getDaysPerYear() {
        return (getCalendarDefinition().getAverageDaysPerYear());
    }


    private BigDecimal getDaysPerMonth() {
        return (getCalendarDefinition().getAverageDaysPerMonth());
    }


    private BigDecimal getDaysPerWeek() {
        return (getCalendarDefinition().getAverageDaysPerWeek());
    }


    private BigDecimal getHoursPerDay() {
        return (new BigDecimal(getTimeDefinition().getHoursPerDay()));
    }


    private BigDecimal getMinutesPerHour() {
        return (new BigDecimal(getTimeDefinition().getMinutesPerHour()));
    }


    private BigDecimal getSecondsPerMinute() {
        return (new BigDecimal(getTimeDefinition().getSecondsPerMinute()));
    }


    private BigDecimal getSecondsPerYear() {
        return (getSecondsPerMinute().multiply(getMinutesPerHour()).multiply(getHoursPerDay()).multiply(getDaysPerYear()));
    }


    private BigDecimal getPlancksPerSecond() {
        return (getTimeDefinition().getPlancksPerSecond());
    }


    private BigDecimal getPlancksPerYear() {
        return (getPlancksPerSecond().multiply(getSecondsPerYear()));
    }


    private BigDecimal getPlancksPerDay() {
        return (getPlancksPerSecond().multiply(getSecondsPerMinute()).multiply(getMinutesPerHour()).multiply(getHoursPerDay()));
    }
}
