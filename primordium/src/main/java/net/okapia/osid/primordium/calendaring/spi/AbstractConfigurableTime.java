//
// AbstractConfigurableTime.java
//
//     An implementation of an OSID Time.
//
//
// Tom Coppeto
// Okapia
// 2 May 2012
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;
import org.osid.calendaring.DateTimeResolution;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract implementation of OSID Time that uses a TimeDefinition
 *  to understand the ranges of hours, minutes, and seconds in various
 *  time systems to implement comparison methods.
 *
 *  The ConfigurableTimeSpecifier uses a TimeDefinition to validate
 *  input values during the building of a Time.
 */

public abstract class AbstractConfigurableTime 
    extends AbstractTime
    implements org.osid.calendaring.Time {

    private final TimeFactory factory;
    private final net.okapia.osid.primordium.calendaring.TimeDefinition definition;
    private org.osid.calendaring.Time low;
    private org.osid.calendaring.Time high;

    private int hash;

    private static final long serialVersionUID = -1697539641L;


    /**
     *  Constructs a new <code>AbstractConfigruableTime</code> with second
     *  resolution.     
     *
     *  @param factory the time factory
     *  @param hour the hour
     *  @param minute the minute
     *  @param second the second
     *  @throws org.osid.InvalidArgumentException <code>hour</code>,
     *          <code>minutes</code>, or <code>second</code> is
     *          out of range
     *  @throws org.osid.NullArgumentException <code>factory</code> is
     *          <code>null</code>
     */
    
    protected AbstractConfigurableTime(TimeFactory factory, long hour, long minute, long second) {

        super(factory.getTimeDefinition().getTimeType(), hour, minute, second);

        this.factory    = factory;
        this.definition = factory.getTimeDefinition();

        if (hour > (this.definition.getHoursPerDay() + this.definition.getStartingHour() - 1)) {
            throw new org.osid.InvalidArgumentException("hour is out of range");
        }

        if (minute > (this.definition.getMinutesPerHour() + this.definition.getStartingMinute() - 1)) {
            throw new org.osid.InvalidArgumentException("minute is out of range");
        }

        if (second > (this.definition.getSecondsPerMinute() + this.definition.getStartingSecond() - 1)) {
            throw new org.osid.InvalidArgumentException("second is out of range");
        }

        if (hour < this.definition.getStartingHour()) {
            throw new org.osid.InvalidArgumentException("hour is out of range");
        }

        if (minute < this.definition.getStartingMinute()) {
            throw new org.osid.InvalidArgumentException("minute is out of range");
        }

        if (second < this.definition.getStartingSecond()) {
            throw new org.osid.InvalidArgumentException("second is out of range");
        }

        return;
    }
    
    
    /**
     *  Constructs a new <code>AbstractConfigurableTime</code>.
     *
     *  @param factory the time factory
     *  @param specifier a time specifier
     *  @throws org.osid.NullArgumentException <code>factory</code> or
     *          <code>specifier</code> is <code>null</code>
     */
    
    protected AbstractConfigurableTime(TimeFactory factory, AbstractConfigurableTimeSpecifier specifier) {
        super(specifier);

        nullarg(factory, "time factory");
        this.factory    = factory;
        this.definition = factory.getTimeDefinition();

        return;
    }
    

    /**
     *  Tests if this Time is greater than the given Time. A greater
     *  Time is one whose values minus granularity is greater than
     *  another plus its granularity.
     *
     *  @param  time the time to compare
     *  @return <code>true</code> if this time is larger,
     *          <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException <code>time</code>
     *          is <code>null</code>
     *  @throws org.osid.UnsupportedException cannot compare time
     */

    @OSIDBinding @Override
    public boolean isGreater(org.osid.calendaring.Time time) {
        if (!getTimeType().equals(time.getTimeType())) {
            throw new org.osid.UnsupportedException("cannot compare " + getTimeType() +
                                                    " to " + time.getTimeType());
        }

        return (compare(denormalizeTime()[0], time.denormalizeTime()[1], getTimeDefinition()) > 0);
    }


    /**
     * Tests if this Time is less than the given Time. A lesser Time
     * is one whose values plus granularity is less than another
     * minus its granularity.
     *
     *  @param  time the time to compare
     *  @return <code>true</code> if this time is less,
     *          <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException <code>time</code>
     *          is <code>null</code>
     *  @throws org.osid.UnsupportedException cannot compare time
     */

    @OSIDBinding @Override
    public final boolean isLess(org.osid.calendaring.Time time) {
        if (!getTimeType().equals(time.getTimeType())) {
            throw new org.osid.UnsupportedException("cannot compare " + getTimeType() +
                                                    " to " + time.getTimeType());
        }

        return ((compare(denormalizeTime()[1], time.denormalizeTime()[0], getTimeDefinition()) < 0));
    }


    /**
     *  Tests if the range of this Time, as specified by its
     *  granularity, includes the given Time ranged by its
     *  granularity.
     *
     *  @param  time the ime to compare
     *  @return <code>true</code> if this time is included,
     *          <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException <code>time</code>
     *          is <code>null</code>
     *  @throws org.osid.UnsupportedException cannot compare time
     */

    @OSIDBinding @Override
    public final boolean isInclusive(org.osid.calendaring.Time time) {
        if (!getTimeType().equals(time.getTimeType())) {
            throw new org.osid.UnsupportedException("cannot compare " + getTimeType() +
                                                    " to " + time.getTimeType());
        }

        org.osid.calendaring.Time[] tn  = denormalizeTime();
        org.osid.calendaring.Time[] tn1 = time.denormalizeTime();

        return ((compare(tn[0], tn1[0], getTimeDefinition()) <= 0) &&
                (compare(tn[1], tn1[1], getTimeDefinition()) >= 0));
    }


    /**
     *  Tests if the range of this Time, as specified by its
     *  granularity does not overlap the given Time.
     *
     *  @param  time the time to compare
     *  @return <code>true</code> if this time is excluded,
     *          <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException <code>time</code>
     *          is <code>null</code>
     *  @throws org.osid.UnsupportedException cannot compare time
     */

    @OSIDBinding @Override
    public final boolean isExclusive(org.osid.calendaring.Time time) {
        return (isLess(time) || isGreater(time));
    }


    /**
     *  Compares this Time with the specified Time for ordering.
     *
     *  @param time the time to be compared
     *  @return a negative integer, zero, or a positive integer as
     *          this time is less than, equal to, or greater than the
     *          specified time
     *  @throws org.osid.UnsupportedException cannot compare calendar
     */

    @OSIDBinding @Override
    public int compareTo(org.osid.calendaring.Time time) {
        if (this == time) {
            return (0);
        }

        int c = getTimeType().compareTo(time.getTimeType());
        if (c != 0) {
            return (c);
        }

	if ((getGranularity().ordinal() > DateTimeResolution.HOUR.ordinal())  &&
            (time.getGranularity().ordinal() > DateTimeResolution.HOUR.ordinal())) {
           return (0);
        }

	if ((getGranularity().ordinal() > DateTimeResolution.HOUR.ordinal())  &&
            (time.getGranularity().ordinal() <= DateTimeResolution.HOUR.ordinal())) {
            return (-1);
        }

	if ((getGranularity().ordinal() <= DateTimeResolution.HOUR.ordinal())  &&
            (time.getGranularity().ordinal() > DateTimeResolution.HOUR.ordinal())) {
            return (1);
        }

	if (getHour() > time.getHour()) {
            return (1);
        }

        if (getHour() < time.getHour()) {
            return (-1);
	}

        if (getGranularity() == DateTimeResolution.HOUR) {
            if (time.getGranularity() == DateTimeResolution.HOUR) {
                return (0);
            } else {
                return (-1);
            }
        } 

        if (time.getGranularity() == DateTimeResolution.HOUR) {
            return (1);
        } 
            
	if (getMinute() > time.getMinute()) {
            return (1);
        }

        if (getMinute() < time.getMinute()) {
            return (-1);
        }

        if (getGranularity() == DateTimeResolution.MINUTE) {
            if (time.getGranularity() == DateTimeResolution.MINUTE) {
                return (0);
            } else {
                return (-1);
            }
        } 

        if (time.getGranularity() == DateTimeResolution.MINUTE) {
            return (1);
        } 

	if (getSecond() > time.getSecond()) {
            return (1);
        }

        if (getSecond() < time.getSecond()) {
            return (-1);
        }

        if (getGranularity() == DateTimeResolution.SECOND) {
            if (time.getGranularity() == DateTimeResolution.SECOND) {
                return (0);
            } else {
                return (-1);
            }
        } 

        if (time.getGranularity() == DateTimeResolution.SECOND) {
            return (1);
        } 

	if (getMilliseconds() > time.getMilliseconds()) {
            return (1);
        }

        if (getMilliseconds() < time.getMilliseconds()) {
            return (-1);
        }

        if (getGranularity() == DateTimeResolution.MILLISECOND) {
            if (time.getGranularity() == DateTimeResolution.MILLISECOND) {
                return (0);
            } else {
                return (-1);
            }
        } 

        if (time.getGranularity() == DateTimeResolution.MILLISECOND) {
            return (1);
        } 

	if (getMicroseconds() > time.getMicroseconds()) {
            return (1);
        }

        if (getMicroseconds() < time.getMicroseconds()) {
            return (-1);
        }

        if (getGranularity() == DateTimeResolution.MICROSECOND) {
            if (time.getGranularity() == DateTimeResolution.MICROSECOND) {
                return (0);
            } else {
                return (-1);
            }
        } 

        if (time.getGranularity() == DateTimeResolution.MICROSECOND) {
            return (1);
        } 

	if (getNanoseconds() > time.getNanoseconds()) {
            return (1);
        }

        if (getNanoseconds() < time.getNanoseconds()) {
            return (-1);
        }

        if (getGranularity() == DateTimeResolution.NANOSECOND) {
            if (time.getGranularity() == DateTimeResolution.NANOSECOND) {
                return (0);
            } else {
                return (-1);
            }
        } 

        if (time.getGranularity() == DateTimeResolution.NANOSECOND) {
            return (1);
        } 

	if (getPicoseconds() > time.getPicoseconds()) {
            return (1);
        }

        if (getPicoseconds() < time.getPicoseconds()) {
            return (-1);
        }

        if (getGranularity() == DateTimeResolution.PICOSECOND) {
            if (time.getGranularity() == DateTimeResolution.PICOSECOND) {
                return (0);
            } else {
                return (-1);
            }
        } 

        if (time.getGranularity() == DateTimeResolution.PICOSECOND) {
            return (1);
        } 

	if (getFemtoseconds() > time.getFemtoseconds()) {
            return (1);
        }

        if (getFemtoseconds() < time.getFemtoseconds()) {
            return (-1);
        }

        if (getGranularity() == DateTimeResolution.FEMTOSECOND) {
            if (time.getGranularity() == DateTimeResolution.FEMTOSECOND) {
                return (0);
            } else {
                return (-1);
            }
        } 

        if (time.getGranularity() == DateTimeResolution.FEMTOSECOND) {
            return (1);
        } 

	if (getAttoseconds() > time.getAttoseconds()) {
            return (1);
        }

        if (getAttoseconds() < time.getAttoseconds()) {
            return (-1);
        }

        if (getGranularity() == DateTimeResolution.ATTOSECOND) {
            if (time.getGranularity() == DateTimeResolution.ATTOSECOND) {
                return (0);
            } else {
                return (-1);
            }
        } 

        if (time.getGranularity() == DateTimeResolution.ATTOSECOND) {
            return (1);
        } 

	if (getZeptoseconds() > time.getZeptoseconds()) {
            return (1);
        }

        if (getZeptoseconds() < time.getZeptoseconds()) {
            return (-1);
        }

        if (getGranularity() == DateTimeResolution.ZEPTOSECOND) {
            if (time.getGranularity() == DateTimeResolution.ZEPTOSECOND) {
                return (0);
            } else {
                return (-1);
            }
        } 

        if (time.getGranularity() == DateTimeResolution.ZEPTOSECOND) {
            return (1);
        } 

	if (getYoctoseconds() > time.getYoctoseconds()) {
            return (1);
        }

        if (getYoctoseconds() < time.getYoctoseconds()) {
            return (-1);
        }

        if (getGranularity() == DateTimeResolution.YOCTOSECOND) {
            if (time.getGranularity() == DateTimeResolution.YOCTOSECOND) {
                return (0);
            } else {
                return (-1);
            }
        } 

        if (time.getGranularity() == DateTimeResolution.YOCTOSECOND) {
            return (1);
        } 

	if (getXoxxoseconds() > time.getXoxxoseconds()) {
            return (1);
        }

        if (getXoxxoseconds() < time.getXoxxoseconds()) {
            return (-1);
        }

        if (getGranularity() == DateTimeResolution.XOXXOSECOND) {
            if (time.getGranularity() == DateTimeResolution.XOXXOSECOND) {
                return (0);
            } else {
                return (-1);
            }
        } 

        if (time.getGranularity() == DateTimeResolution.XOXXOSECOND) {
            return (1);
        } 

	if (getWeebleseconds() > time.getWeebleseconds()) {
            return (1);
        }

        if (getWeebleseconds() < time.getWeebleseconds()) {
            return (-1);
        }

        if (getGranularity() == DateTimeResolution.WEEBLESECOND) {
            if (time.getGranularity() == DateTimeResolution.WEEBLESECOND) {
                return (0);
            } else {
                return (-1);
            }
        } 

        if (time.getGranularity() == DateTimeResolution.WEEBLESECOND) {
            return (1);
        } 

	if (getVatoseconds() > time.getVatoseconds()) {
            return (1);
        }

        if (getVatoseconds() < time.getVatoseconds()) {
            return (-1);
        }

        if (getGranularity() == DateTimeResolution.VATOSECOND) {
            if (time.getGranularity() == DateTimeResolution.VATOSECOND) {
                return (0);
            } else {
                return (-1);
            }
        } 

        if (time.getGranularity() == DateTimeResolution.VATOSECOND) {
            return (1);
        } 

	if (getUndaseconds() > time.getUndaseconds()) {
            return (1);
        }

        if (getUndaseconds() < time.getUndaseconds()) {
            return (-1);
        }

        if (getGranularity() == DateTimeResolution.UNDASECOND) {
            if (time.getGranularity() == DateTimeResolution.UNDASECOND) {
                return (0);
            } else {
                return (-1);
            }
        } 

        if (time.getGranularity() == DateTimeResolution.UNDASECOND) {
            return (1);
        } 

	if (getPlanckSeconds() > time.getPlanckSeconds()) {
            return (1);
        }

        if (getPlanckSeconds() < time.getPlanckSeconds()) {
            return (-1);
        }

	return (0);
    }


    /**
     *  Determines if the given <code> Time </code> is equal to this
     *  one.
     *
     *  @param  obj an object to compare
     *  @return <code> true </code> if the given object is equal to
     *          this <code>Time</code>, <code> false </code> otherwise
     */

    @OSIDBinding @Override
    public final boolean equals(Object obj) {
        if (obj == null) {
            return (false);
        }

        if (this == obj) {
            return (true);
        }

        if (!(obj instanceof org.osid.calendaring.Time)) {
            return (false);
        }

        org.osid.calendaring.Time time = (org.osid.calendaring.Time) obj;

        if (getGranularity() != time.getGranularity()) {
            return (false);
        }

        if (getGranularity().ordinal() > DateTimeResolution.HOUR.ordinal()) {
            return (true);
        }

        if (!getTimeType().equals(time.getTimeType())) {
            return (false);
        }

        if (getHour() != time.getHour()) {
            return (false);
        }

        if (getGranularity() == DateTimeResolution.HOUR) {
            return (true);
        }

        long mph  = getTimeDefinition().getMinutesPerHour();
        long sm   = getTimeDefinition().getStartingMinute();

        /* this seems too precise for "half past 3" */
        if (getGranularity() == DateTimeResolution.HALF_HOUR) {	    
            long half = mph/2 + sm - 1;
	    if ((getMinute() < half) && (time.getMinute() < half)) {
		return (true);
	    } else if ((getMinute() >= half) && (time.getMinute() >= half)) {
		return (true);
	    } else {
		return (false);
	    }
	}

        if (getGranularity() == DateTimeResolution.QUARTER_HOUR) {
            long quarter = mph/4 + sm - 1;
	    if (getMinute() < quarter) {
		if (time.getMinute() < quarter) {
		    return (true);
		} else {
		    return (false);
		}
	    }

	    if (getMinute() < quarter * 2) {
		if ((time.getMinute() >= quarter) && (time.getMinute() < quarter * 2)) {
		    return (true);
		} else {
		    return (false);
		}
	    }

	    if (getMinute() < quarter * 3) {
		if ((time.getMinute() >= quarter * 2) && (time.getMinute() < quarter * 3)) {
		    return (true);
		} else {
		    return (false);
		}
	    }

	    if (getMinute() >= quarter * 3) {
		    return (true);
	    } else {
		return (false);
	    }
	}

        if (getMinute() != time.getMinute()) {
            return (false);
        }

        if (getGranularity() == DateTimeResolution.MINUTE) {
            return (true);
        }

        if (getSecond() != time.getSecond()) {
            return (false);
        }

        if (getGranularity() == DateTimeResolution.SECOND) {
            return (true);
        }

        if (getMilliseconds() != time.getMilliseconds()) {
            return (false);
        }

        if (getGranularity() == DateTimeResolution.MILLISECOND) {
            return (true);
        }

        if (getMicroseconds() != time.getMicroseconds()) {
            return (false);
        }

        if (getGranularity() == DateTimeResolution.MICROSECOND) {
            return (true);
        }

        if (getNanoseconds() != time.getNanoseconds()) {
            return (false);
        }

        if (getGranularity() == DateTimeResolution.NANOSECOND) {
            return (true);
        }

        if (getPicoseconds() != time.getPicoseconds()) {
            return (false);
        }

        if (getGranularity() == DateTimeResolution.PICOSECOND) {
            return (true);
        }

        if (getFemtoseconds() != time.getFemtoseconds()) {
            return (false);
        }

        if (getGranularity() == DateTimeResolution.FEMTOSECOND) {
            return (true);
        }

        if (getAttoseconds() != time.getAttoseconds()) {
            return (false);
        }

        if (getGranularity() == DateTimeResolution.ATTOSECOND) {
            return (true);
        }

        if (getZeptoseconds() != time.getZeptoseconds()) {
            return (false);
        }

        if (getGranularity() == DateTimeResolution.ZEPTOSECOND) {
            return (true);
        }

        if (getYoctoseconds() != time.getYoctoseconds()) {
            return (false);
        }

        if (getGranularity() == DateTimeResolution.YOCTOSECOND) {
            return (true);
        }

        if (getXoxxoseconds() != time.getXoxxoseconds()) {
            return (false);
        }

        if (getGranularity() == DateTimeResolution.XOXXOSECOND) {
            return (true);
        }

        if (getWeebleseconds() != time.getWeebleseconds()) {
            return (false);
        }

        if (getGranularity() == DateTimeResolution.WEEBLESECOND) {
            return (true);
        }

        if (getVatoseconds() != time.getVatoseconds()) {
            return (false);
        }

        if (getGranularity() == DateTimeResolution.VATOSECOND) {
            return (true);
        }

        if (getUndaseconds() != time.getUndaseconds()) {
            return (false);
        }

        if (getGranularity() == DateTimeResolution.UNDASECOND) {
            return (true);
        }

        if (getPlanckSeconds() != time.getPlanckSeconds()) {
            return (false);
        }

        if (getGranularity() == DateTimeResolution.PLANCKSECOND) {
            return (true);
        }
        
        return (false);
    }


    /**
     *  Returns a hash code value for this <code>Time</code> based
     *  on the type, granularity and values.
     *
     *  @return a hash code value for this object
     */

    @OSIDBinding @Override
    public final int hashCode() {
        if (this.hash == 0) {
            this.hash = calculateHashCode();
        }

        return (this.hash);
    }


    /**
     *  Returns an interval based on this Time derived by its
     *  granularity. This method depends on the time Type.
     *
     *  @return a low and a high time
     */

    @OSIDBinding @Override
    public final org.osid.calendaring.Time[] denormalizeTime() {
        if (this.low == null) {
            this.low = calculateLowerBound();
        }

        if (this.high == null) {
            this.high = calculateUpperBound();
        }

        org.osid.calendaring.Time[] ret = new org.osid.calendaring.Time[2];
        ret[0] = this.low;
        ret[1] = this.high;
            
        return (ret);
    }

    
    /**
     *  Gets the time factory.
     *
     *  @return the time factory
     */

    protected TimeFactory getTimeFactory() {
        return (this.factory);
    }


    /**
     *  Gets the time definition.
     *
     *  @return the time definition
     */

    protected net.okapia.osid.primordium.calendaring.TimeDefinition getTimeDefinition() {
        return (this.definition);
    }


    /**
     *  Compares two normalized finite times. If the granularities
     *  differ the comparison is performed using the least granular
     *  time, such that 8:15.01 equals 8:15.
     *
     *  @param  time1 a <code>Time</code> to compare
     *  @param  time2 a <code>Time</code> to compare
     *  @param definition  the time definition
     *  @throws org.osid.NullArgumentException <code>time1</code> or
     *          <code>time2</code> is <code>null</code>
     *  @return <code> 1 </code> if <code>time1</code> is greater than
     *          <code>time2</code>, -1 if <code>time1</code> is less
     *          than <code>time2</code>, 0 if equal
     *  @throws org.osid.NullArgumentException <code>time1</code>,
     *          <code>time2</code>, or <cod>definition</code> is
     *          <code>null</code>
     *  @throws org.osid.UnsupportedException not the same time type
     */

    protected int compare(org.osid.calendaring.Time time1, org.osid.calendaring.Time time2,
                          net.okapia.osid.primordium.calendaring.TimeDefinition definition) {

        nullarg(time1, "time 1");
        nullarg(time2, "time 2");
        nullarg(definition, "time definition");

	if (!time1.getTimeType().equals(time2.getTimeType())) {
	    throw new org.osid.UnsupportedException("cannot compare " + time1.getTimeType()
						    + " and " + time2.getTimeType());
	}

	if ((time1.getGranularity().ordinal() >  DateTimeResolution.HOUR.ordinal())  &&
            (time2.getGranularity().ordinal() >  DateTimeResolution.HOUR.ordinal())) {
            return (0);
        }

	if ((time1.getGranularity().ordinal() >  DateTimeResolution.HOUR.ordinal())  &&
            (time2.getGranularity().ordinal() <=  DateTimeResolution.HOUR.ordinal())) {
            return (-1);
        }

	if ((time1.getGranularity().ordinal() <=  DateTimeResolution.HOUR.ordinal())  &&
            (time2.getGranularity().ordinal() >  DateTimeResolution.HOUR.ordinal())) {
            return (1);
        }

	if (time1.getHour() > time2.getHour()) {
            return (1);
        }

        if (time1.getHour() < time2.getHour()) {
            return (-1);
	}

        if ((time1.getGranularity() == DateTimeResolution.HOUR) ||
            (time2.getGranularity() == DateTimeResolution.HOUR)) {
            return (0);
	}
        
        long mph  = definition.getMinutesPerHour();
        long sm   = definition.getStartingMinute();
        
        if ((time1.getGranularity() == DateTimeResolution.HALF_HOUR) ||
            (time2.getGranularity() == DateTimeResolution.HALF_HOUR)) {

            long half = mph/2 + sm - 1;
	    
	    if ((time1.getMinute() >= half) && (time2.getMinute() < half)) {
		return (1);
	    } else if ((time1.getMinute() < half) && (time2.getMinute() <= half)) {
		return (-1);
	    } else {
		return (0);
	    }
	}

        if ((time1.getGranularity() == DateTimeResolution.QUARTER_HOUR) ||
            (time2.getGranularity() == DateTimeResolution.QUARTER_HOUR)) {

            long quarter = mph/4 + sm - 1;	    
	    if ((time1.getMinute() >= quarter) && (time2.getMinute() < quarter)) {
		return (1);
	    } else if ((time1.getMinute() >= quarter * 2) && (time2.getMinute() < quarter * 2)) {
		return (1);
	    } else if ((time1.getMinute() >= quarter * 3) && (time2.getMinute() < quarter * 3)) {
		return (1);
	    } else if ((time1.getMinute() < quarter) && (time2.getMinute() >= quarter)) {
		return (-1);
	    } else if ((time1.getMinute() < quarter * 2) && (time2.getMinute() >= quarter * 2)) {
		return (-1);
	    } else if ((time1.getMinute() < quarter * 3) && (time2.getMinute() >= quarter * 3)) {
		return (-1);
	    } else {
		return (0);
	    }
	}

	if (time1.getMinute() > time2.getMinute()) {
            return (1);
        }

        if (time1.getMinute() < time2.getMinute()) {
            return (-1);
        }

        if ((time1.getGranularity() == DateTimeResolution.MINUTE) ||
            (time2.getGranularity() == DateTimeResolution.MINUTE)) {
            return (0);
        }

	if (time1.getSecond() > time2.getSecond()) {
            return (1);
        }

        if (time1.getSecond() < time2.getSecond()) {
            return (-1);
        }

        if ((time1.getGranularity() == DateTimeResolution.SECOND) ||
            (time2.getGranularity() == DateTimeResolution.SECOND)) {
            return (0);
        }

	if (time1.getMilliseconds() > time2.getMilliseconds()) {
            return (1);
        }

        if (time1.getMilliseconds() < time2.getMilliseconds()) {
            return (-1);
        }

        if ((time1.getGranularity() == DateTimeResolution.MILLISECOND) ||
            (time2.getGranularity() == DateTimeResolution.MILLISECOND)) {
            return (0);
        }

	if (time1.getMicroseconds() > time2.getMicroseconds()) {
            return (1);
        }

        if (time1.getMicroseconds() < time2.getMicroseconds()) {
            return (-1);
        }

        if ((time1.getGranularity() == DateTimeResolution.MICROSECOND) ||
            (time2.getGranularity() == DateTimeResolution.MICROSECOND)) {
            return (0);
        }

	if (time1.getNanoseconds() > time2.getNanoseconds()) {
            return (1);
        }

        if (time1.getNanoseconds() < time2.getNanoseconds()) {
            return (-1);
        }

        if ((time1.getGranularity() == DateTimeResolution.NANOSECOND) ||
            (time2.getGranularity() == DateTimeResolution.NANOSECOND)) {
            return (0);
        }

	if (time1.getPicoseconds() > time2.getPicoseconds()) {
            return (1);
        }

        if (time1.getPicoseconds() < time2.getPicoseconds()) {
            return (-1);
        }

        if ((time1.getGranularity() == DateTimeResolution.PICOSECOND) ||
            (time2.getGranularity() == DateTimeResolution.PICOSECOND)) {
            return (0);
        }

	if (time1.getFemtoseconds() > time2.getFemtoseconds()) {
            return (1);
        }

        if (time1.getFemtoseconds() < time2.getFemtoseconds()) {
            return (-1);
        }

        if ((time1.getGranularity() == DateTimeResolution.FEMTOSECOND) ||
            (time2.getGranularity() == DateTimeResolution.FEMTOSECOND)) {
            return (0);
        }

	if (time1.getAttoseconds() > time2.getAttoseconds()) {
            return (1);
        }

        if (time1.getAttoseconds() < time2.getAttoseconds()) {
            return (-1);
        }

        if ((time1.getGranularity() == DateTimeResolution.ATTOSECOND) ||
            (time2.getGranularity() == DateTimeResolution.ATTOSECOND)) {
            return (0);
        }

	if (time1.getZeptoseconds() > time2.getZeptoseconds()) {
            return (1);
        }

        if (time1.getZeptoseconds() < time2.getZeptoseconds()) {
            return (-1);
        }

        if ((time1.getGranularity() == DateTimeResolution.ZEPTOSECOND) ||
            (time2.getGranularity() == DateTimeResolution.ZEPTOSECOND)) {
            return (0);
        }

	if (time1.getYoctoseconds() > time2.getYoctoseconds()) {
            return (1);
        }

        if (time1.getYoctoseconds() < time2.getYoctoseconds()) {
            return (-1);
        }

        if ((time1.getGranularity() == DateTimeResolution.YOCTOSECOND) ||
            (time2.getGranularity() == DateTimeResolution.YOCTOSECOND)) {
            return (0);
        }

	if (time1.getXoxxoseconds() > time2.getXoxxoseconds()) {
            return (1);
        }

        if (time1.getXoxxoseconds() < time2.getXoxxoseconds()) {
            return (-1);
        }

        if ((time1.getGranularity() == DateTimeResolution.XOXXOSECOND) ||
            (time2.getGranularity() == DateTimeResolution.XOXXOSECOND)) {
            return (0);
        }

	if (time1.getWeebleseconds() > time2.getWeebleseconds()) {
            return (1);
        }

        if (time1.getWeebleseconds() < time2.getWeebleseconds()) {
            return (-1);
        }

        if ((time1.getGranularity() == DateTimeResolution.WEEBLESECOND) ||
            (time2.getGranularity() == DateTimeResolution.WEEBLESECOND)) {
            return (0);
        }

	if (time1.getVatoseconds() > time2.getVatoseconds()) {
            return (1);
        }

        if (time1.getVatoseconds() < time2.getVatoseconds()) {
            return (-1);
        }

        if ((time1.getGranularity() == DateTimeResolution.VATOSECOND) ||
            (time2.getGranularity() == DateTimeResolution.VATOSECOND)) {
            return (0);
        }

	if (time1.getUndaseconds() > time2.getUndaseconds()) {
            return (1);
        }

        if (time1.getUndaseconds() < time2.getUndaseconds()) {
            return (-1);
        }

        if ((time1.getGranularity() == DateTimeResolution.UNDASECOND) ||
            (time2.getGranularity() == DateTimeResolution.UNDASECOND)) {
            return (0);
        }

	if (time1.getPlanckSeconds() > time2.getPlanckSeconds()) {
            return (1);
        }

        if (time1.getPlanckSeconds() < time2.getPlanckSeconds()) {
            return (-1);
        }

	return (0);
    }


    private org.osid.calendaring.Time calculateLowerBound() {
        AbstractTimeSpecifier spec = this.factory.getTimeSpecifier();
        spec.setGranularity(DateTimeResolution.PLANCKSECOND);
        
        if (getGranularity().ordinal() == DateTimeResolution.PLANCKSECOND.ordinal()) {
            spec.setPlanckSeconds((int) getPlanckSeconds());
        }

        if (getGranularity().ordinal() <= DateTimeResolution.UNDASECOND.ordinal()) {
            spec.setUndaseconds((int) getUndaseconds());
        }

        if (getGranularity().ordinal() <= DateTimeResolution.VATOSECOND.ordinal()) {
            spec.setVatoseconds((int) getVatoseconds());
        }

        if (getGranularity().ordinal() <= DateTimeResolution.WEEBLESECOND.ordinal()) {
            spec.setWeebleseconds((int) getWeebleseconds());
        }

        if (getGranularity().ordinal() <= DateTimeResolution.XOXXOSECOND.ordinal()) {
            spec.setXoxxoseconds((int) getXoxxoseconds());
        }
            
        if (getGranularity().ordinal() <= DateTimeResolution.YOCTOSECOND.ordinal()) {
            spec.setYoctoseconds((int) getYoctoseconds());
        }

        if (getGranularity().ordinal() <= DateTimeResolution.ZEPTOSECOND.ordinal()) {
            spec.setZeptoseconds((int) getZeptoseconds());
        }

        if (getGranularity().ordinal() <= DateTimeResolution.ATTOSECOND.ordinal()) {
            spec.setAttoseconds((int) getAttoseconds());
        }

        if (getGranularity().ordinal() <= DateTimeResolution.FEMTOSECOND.ordinal()) {
            spec.setFemtoseconds((int) getFemtoseconds());
        }

        if (getGranularity().ordinal() <= DateTimeResolution.PICOSECOND.ordinal()) {
            spec.setPicoseconds((int) getPicoseconds());
        }

        if (getGranularity().ordinal() <= DateTimeResolution.NANOSECOND.ordinal()) {
            spec.setNanoseconds((int) getNanoseconds());
        }

        if (getGranularity().ordinal() <= DateTimeResolution.MICROSECOND.ordinal()) {
            spec.setMicroseconds((int) getMicroseconds());
        }

        if (getGranularity().ordinal() <= DateTimeResolution.MILLISECOND.ordinal()) {
            spec.setMilliseconds((int) getMilliseconds());
        }

        if (getGranularity().ordinal() <= DateTimeResolution.SECOND.ordinal()) {
            spec.setSecond(getSecond());
        }

        if (getGranularity().ordinal() <= DateTimeResolution.HALF_HOUR.ordinal()) {
            spec.setMinute(getMinute());
        }

        if (getGranularity().ordinal() <= DateTimeResolution.HOUR.ordinal()) {
            spec.setHour(getHour());
        }

        return (this.factory.makeTime(spec));
    }    


    private org.osid.calendaring.Time calculateUpperBound() {
        AbstractTimeSpecifier spec = this.factory.getTimeSpecifier();
        spec.setGranularity(DateTimeResolution.PLANCKSECOND);
        
        if (getGranularity().ordinal() > DateTimeResolution.PLANCKSECOND.ordinal()) {
            spec.setPlanckSeconds(PLANCKS_IN_UNDASECOND);
        } else {
            spec.setPlanckSeconds((int) getPlanckSeconds());
        }

        if (getGranularity().ordinal() > DateTimeResolution.UNDASECOND.ordinal()) {
            spec.setUndaseconds(999);
        } else {
            spec.setUndaseconds((int) getUndaseconds());
        }

        if (getGranularity().ordinal() > DateTimeResolution.VATOSECOND.ordinal()) {
            spec.setVatoseconds(999);
        } else {
            spec.setVatoseconds((int) getVatoseconds());
        }

        if (getGranularity().ordinal() > DateTimeResolution.WEEBLESECOND.ordinal()) {
            spec.setWeebleseconds(999);
        } else {
            spec.setWeebleseconds((int) getWeebleseconds());
        }

        if (getGranularity().ordinal() > DateTimeResolution.XOXXOSECOND.ordinal()) {
            spec.setXoxxoseconds(999);
        } else {
            spec.setXoxxoseconds((int) getXoxxoseconds());
        }
            
        if (getGranularity().ordinal() > DateTimeResolution.YOCTOSECOND.ordinal()) {
            spec.setYoctoseconds(999);
        } else {
            spec.setYoctoseconds((int) getYoctoseconds());
        }

        if (getGranularity().ordinal() > DateTimeResolution.ZEPTOSECOND.ordinal()) {
            spec.setZeptoseconds(999);
        } else {
            spec.setZeptoseconds((int) getZeptoseconds());
        }

        if (getGranularity().ordinal() > DateTimeResolution.ATTOSECOND.ordinal()) {
            spec.setAttoseconds(999);
        } else {
            spec.setAttoseconds((int) getAttoseconds());
        }

        if (getGranularity().ordinal() > DateTimeResolution.FEMTOSECOND.ordinal()) {
            spec.setFemtoseconds(999);
        } else {
            spec.setFemtoseconds((int) getFemtoseconds());
        }

        if (getGranularity().ordinal() > DateTimeResolution.PICOSECOND.ordinal()) {
            spec.setPicoseconds(999);
        } else {
            spec.setPicoseconds((int) getPicoseconds());
        }

        if (getGranularity().ordinal() > DateTimeResolution.NANOSECOND.ordinal()) {
            spec.setNanoseconds(999);
        } else {
            spec.setNanoseconds((int) getNanoseconds());
        }

        if (getGranularity().ordinal() > DateTimeResolution.MICROSECOND.ordinal()) {
            spec.setMicroseconds(999);
        } else {
            spec.setMicroseconds((int) getMicroseconds());
        }

        if (getGranularity().ordinal() > DateTimeResolution.MILLISECOND.ordinal()) {
            spec.setMilliseconds(999);
        } else {
            spec.setMilliseconds((int) getMilliseconds());
        }

        if (getGranularity().ordinal() > DateTimeResolution.SECOND.ordinal()) {
            spec.setSecond(this.definition.getSecondsPerMinute() + this.definition.getStartingSecond() - 1);
        } else {
            spec.setSecond(getSecond());
        }

        if (getGranularity().ordinal() > DateTimeResolution.MINUTE.ordinal()) {
            spec.setMinute(this.definition.getMinutesPerHour() + this.definition.getStartingMinute() - 1);
        } else {
            spec.setMinute(getMinute());
        }

        if (getGranularity().ordinal() > DateTimeResolution.HOUR.ordinal()) {
            spec.setHour(this.definition.getHoursPerDay() + this.definition.getStartingHour() - 1);
        } else {
            spec.setHour(getHour());
        }

        if (getGranularity() == DateTimeResolution.QUARTER_HOUR) {
            long slice = (long) Math.ceil(this.definition.getMinutesPerHour() / 4); 
            for (int i = 1; i <= 4; i++) {
                long min = this.definition.getStartingMinute() + (slice * i) - 1;
                spec.setMinute(min);
            }
        }

        if (getGranularity() == DateTimeResolution.HALF_HOUR) {
            long slice = (long) Math.ceil(this.definition.getMinutesPerHour() / 2); 
            for (int i = 1; i <= 2; i++) {
                long min = this.definition.getStartingMinute() + (slice * i) - 1;
                spec.setMinute(min);
            }
        }

        return (this.factory.makeTime(spec));
    }    


    private int calculateHashCode() {
        int hash = getTimeType().hashCode();

	if (getGranularity() == DateTimeResolution.INFINITY) {
	    return (hash);
	}

	hash = hash * 31 + (int) getHour();
	if (getGranularity() == DateTimeResolution.HOUR) {
	    return (hash);
	}

        long mph  = definition.getMinutesPerHour();
        long sm   = definition.getStartingMinute();
        
        long half = mph/2 + sm - 1;
	hash = hash * 31 + (int) half;

	if (getGranularity() == DateTimeResolution.HALF_HOUR) {
	    return (hash);
	}

        long quarter = mph/4 + sm - 1;	    
	hash = hash * 31 + (int) quarter;

	if (getGranularity() == DateTimeResolution.QUARTER_HOUR) {
	    return (hash);
	}

	hash = hash * 31 + (int) getMinute();
	if (getGranularity() == DateTimeResolution.MINUTE) {
	    return (hash);
	}

	hash = hash * 31 + (int) getSecond();
	if (getGranularity() == DateTimeResolution.SECOND) {
	    return (hash);
	}

	hash = hash * 31 + (int) getMilliseconds();
	if (getGranularity() == DateTimeResolution.MILLISECOND) {
	    return (hash);
	}

	hash = hash * 31 + (int) getMicroseconds();
	if (getGranularity() == DateTimeResolution.MICROSECOND) {
	    return (hash);
	}

	hash = hash * 31 + (int) getNanoseconds();
	if (getGranularity() == DateTimeResolution.NANOSECOND) {
	    return (hash);
	}

	hash = hash * 31 + (int) getPicoseconds();
	if (getGranularity() == DateTimeResolution.PICOSECOND) {
	    return (hash);
	}

	hash = hash * 31 + (int) getFemtoseconds();
	if (getGranularity() == DateTimeResolution.FEMTOSECOND) {
	    return (hash);
	}

	hash = hash * 31 + (int) getAttoseconds();
	if (getGranularity() == DateTimeResolution.ATTOSECOND) {
	    return (hash);
	}

	hash = hash * 31 + (int) getZeptoseconds();
	if (getGranularity() == DateTimeResolution.ZEPTOSECOND) {
	    return (hash);
	}

	hash = hash * 31 + (int) getYoctoseconds();
	if (getGranularity() == DateTimeResolution.YOCTOSECOND) {
	    return (hash);
	}

	hash = hash * 31 + (int) getXoxxoseconds();
	if (getGranularity() == DateTimeResolution.XOXXOSECOND) {
	    return (hash);
	}

	hash = hash * 31 + (int) getWeebleseconds();
	if (getGranularity() == DateTimeResolution.WEEBLESECOND) {
	    return (hash);
	}

	hash = hash * 31 + (int) getVatoseconds();
	if (getGranularity() == DateTimeResolution.VATOSECOND) {
	    return (hash);
	}

	hash = hash * 31 + (int) getUndaseconds();
	if (getGranularity() == DateTimeResolution.UNDASECOND) {
	    return (hash);
	}

	hash = hash * 31 + (int) getPlanckSeconds();
	if (getGranularity() == DateTimeResolution.PLANCKSECOND) {
	    return (hash);
	}

        return (hash);
    }


    /**
     *  Parses the decimal portion of the seconds. The supplied string
     *  is everything to the right of the decimal point.
     *
     *  @param factory the time factory
     *  @param str the string to parse
     *  @return a Time
     *  @throws org.osid.InvalidArgumentException cannot parse string
     *  @throws org.osid.NullArgumentException <code>factory</code> or
     *          <code>str</code> is <code>null</code>
     */
    
    @SuppressWarnings("fallthrough")
    protected static org.osid.calendaring.Time parseDecimal(TimeFactory factory, String str) {

        org.osid.calendaring.DateTimeResolution granularity;
        int multiplier;

        switch (str.length()) {
        case 0:
            granularity = DateTimeResolution.SECOND;
            multiplier = 1;
            break;
        case 1:
            multiplier = 100;
            granularity = DateTimeResolution.MILLISECOND;
            break;
        case 2:
            multiplier = 10;
            granularity = DateTimeResolution.MILLISECOND;
            break;
        case 3:
            multiplier = 1;
            granularity = DateTimeResolution.MILLISECOND;
            break;
        case 4:
            multiplier = 100;
            granularity = DateTimeResolution.MICROSECOND;
            break;
        case 5:
            multiplier = 10;
            granularity = DateTimeResolution.MICROSECOND;
            break;
        case 6:
            multiplier = 1;
            granularity = DateTimeResolution.MICROSECOND;
            break;
        case 7:
            multiplier = 100;
            granularity = DateTimeResolution.NANOSECOND;
            break;
        case 8:
            multiplier = 10;
            granularity = DateTimeResolution.NANOSECOND;
            break;
        case 9:
            multiplier = 1;
            granularity = DateTimeResolution.NANOSECOND;
            break;
        case 10:
            multiplier = 100;
            granularity = DateTimeResolution.PICOSECOND;
            break;
        case 11:
            multiplier = 10;
            granularity = DateTimeResolution.PICOSECOND;
            break;
        case 12:
            multiplier = 1;
            granularity = DateTimeResolution.PICOSECOND;
            break;
        case 13:
            multiplier = 100;
            granularity = DateTimeResolution.FEMTOSECOND;
            break;
        case 14:
            multiplier = 10;
            granularity = DateTimeResolution.FEMTOSECOND;
            break;
        case 15:
            multiplier = 1;
            granularity = DateTimeResolution.FEMTOSECOND;
            break;
        case 16:
            multiplier = 100;
            granularity = DateTimeResolution.ATTOSECOND;
            break;
        case 17:
            multiplier = 10;
            granularity = DateTimeResolution.ATTOSECOND;
            break;
        case 18:
            multiplier = 1;
            granularity = DateTimeResolution.ATTOSECOND;
            break;
        case 19:
            multiplier = 100;
            granularity = DateTimeResolution.ZEPTOSECOND;
            break;
        case 20:
            multiplier = 10;
            granularity = DateTimeResolution.ZEPTOSECOND;
            break;
        case 21:
            multiplier = 1;
            granularity = DateTimeResolution.ZEPTOSECOND;
            break;
        case 22:
            multiplier = 100;
            granularity = DateTimeResolution.YOCTOSECOND;
            break;
        case 23:
            multiplier = 10;
            granularity = DateTimeResolution.YOCTOSECOND;
            break;
        case 24:
            multiplier = 1;
            granularity = DateTimeResolution.YOCTOSECOND;
            break;
        case 25:
            multiplier = 100;
            granularity = DateTimeResolution.XOXXOSECOND;
            break;
        case 26:
            multiplier = 10;
            granularity = DateTimeResolution.XOXXOSECOND;
            break;
        case 27:
            multiplier = 1;
            granularity = DateTimeResolution.XOXXOSECOND;
            break;
        case 28:
            multiplier = 100;
            granularity = DateTimeResolution.WEEBLESECOND;
            break;
        case 29:
            multiplier = 10;
            granularity = DateTimeResolution.WEEBLESECOND;
            break;
        case 30:
            multiplier = 1;
            granularity = DateTimeResolution.WEEBLESECOND;
            break;
        case 31:
            multiplier = 100;
            granularity = DateTimeResolution.VATOSECOND;
            break;
        case 32:
            multiplier = 10;
            granularity = DateTimeResolution.VATOSECOND;
            break;
        case 33:
            multiplier = 1;
            granularity = DateTimeResolution.VATOSECOND;
            break;
        case 34:
            multiplier = 100;
            granularity = DateTimeResolution.UNDASECOND;
            break;
        case 35:
            multiplier = 10;
            granularity = DateTimeResolution.UNDASECOND;
            break;
        case 36:
            multiplier = 1;
            granularity = DateTimeResolution.UNDASECOND;
            break;
        default:
            multiplier = 1;
            granularity = DateTimeResolution.PLANCKSECOND;
            break;
        }
        
        AbstractTimeSpecifier spec = factory.getTimeSpecifier();
        spec.setGranularity(granularity, multiplier);
        
        try {
            switch (granularity) {
            case PLANCKSECOND:
                spec.setPlanckSeconds(parseDecPlanck(str.substring(36)));  
            case UNDASECOND:
                spec.setUndaseconds(parseDec(str.substring(33)));
            case VATOSECOND:
                spec.setVatoseconds(parseDec(str.substring(30)));
            case WEEBLESECOND:
                spec.setWeebleseconds(parseDec(str.substring(27)));
            case XOXXOSECOND:
                spec.setXoxxoseconds(parseDec(str.substring(24)));
            case YOCTOSECOND:
                spec.setYoctoseconds(parseDec(str.substring(21)));
            case ZEPTOSECOND:
                spec.setZeptoseconds(parseDec(str.substring(18)));
            case ATTOSECOND:
                spec.setAttoseconds(parseDec(str.substring(15)));
            case FEMTOSECOND:
                spec.setFemtoseconds(parseDec(str.substring(12)));
            case PICOSECOND:
                spec.setPicoseconds(parseDec(str.substring(9)));
            case NANOSECOND:
                spec.setNanoseconds(parseDec(str.substring(6)));
            case MICROSECOND:
                spec.setMicroseconds(parseDec(str.substring(3)));
            case MILLISECOND:
                spec.setMilliseconds(parseDec(str.substring(0)));
            }
        } catch (NumberFormatException nfe) {
            throw new org.osid.InvalidArgumentException(nfe);
        }
        
        return (factory.makeTime(spec));
    }
    

    
    private static int parseDec(String dec) {
        switch (dec.length()) {
        case 1:
            return (Integer.parseInt(dec) * 100);
        case 2:
            return (Integer.parseInt(dec) * 10);
        case 3:
            return (Integer.parseInt(dec));
        default:
            return (Integer.parseInt(dec.substring(0, 3)));
        }
    }
    
    
    private static int parseDecPlanck(String dec) {
        java.math.BigDecimal d = new java.math.BigDecimal("." + dec);
        d = d.multiply(new java.math.BigDecimal(PLANCKS_IN_UNDASECOND));
        
        return (d.intValue());


        /*
        if (dec.length() > 14) {
            dec = dec.substring(0, 14);
        }
        
        long d = Long.parseLong(dec);
        for (int i = dec.length(); i < 14; i++) {
            d = d * 10;
        }
        
        return ((int) (d / PLANCKS_IN_UNDASECOND));
        */
    }
}
