//
// SoftwareVersion.java
//
//     An implementation ofan OSID Version for software schemes.
//
//
// Tom Coppeto
// Okapia
// 2 May 2012
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.installation;

import org.osid.binding.java.annotation.OSID;

import net.okapia.osid.primordium.types.version.VersionSchemeTypes;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An implementation of an OSID Version for common software schemes.
 *
 *  Software schemes contain 1 or more numeric components separated by
 *  a period. 
 */

public final class SoftwareVersion 
    extends net.okapia.osid.primordium.installation.spi.AbstractVersion
    implements org.osid.installation.Version {

    /** the Type of this SoftwareVersion scheme */
    public static final org.osid.type.Type TYPE = VersionSchemeTypes.SOFTWARE.getType();

    private static final long serialVersionUID = 32704976L;


    /**
     *  Constructs a new <code>SoftwareVersion</code>.
     *
     *  @param components the version components
     *  @throws org.osid.NullArgumentException <code>components</code>
     *          is <code>null</code>
     */

    public SoftwareVersion(String[] components) {
        super(TYPE, components, Format.DOT);
        return;
    }


    /**
     *  Constructs a new <code>SoftwareVersion</code>.
     *
     *  @param components the version components
     *  @throws org.osid.NullArgumentException <code>components</code>
     *          or <code>format</code> is <code>null</code>
     */

    public SoftwareVersion(String[] components, Format format) {
        super(TYPE, components, format);
        return;
    }


    /**
     *  Parses a version string to create a new Version.
     *
     *  The version string may be delimited by dots, hyphens, or
     *  dashes. Otherwise, the string is treated as a single version
     *  component. If there is a mix of delimiters in the version
     *  string, a single delimiter will appear as an output string.
     *
     *  @param version the string to parse
     *  @throws org.osid.NullArgumentException <code>version</code> is
     *          <code>null</code>
     */

    public static org.osid.installation.Version valueOf(String version) {
        nullarg(version, "version");
        
        String[] components = version.split("[-/\\.]");
        Format format;
        if (version.indexOf('.') > 0) {
            format = Format.DOT;
        } else if (version.indexOf('-') > 0) {
            format = Format.DASH;
        } else if (version.indexOf('/') > 0) {
            format = Format.SLASH;
        } else {
            format = Format.NONE;
        }

        return (new SoftwareVersion(components, format));
    }
}
