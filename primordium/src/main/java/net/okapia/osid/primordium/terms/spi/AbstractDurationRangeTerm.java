//
// AbstractDurationRangeTerm.java
//
//     Implements a DurationRangeTerm.
//
//
// Tom Coppeto
// Okapia
// 29 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.terms.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A basic implementation of DurationRangeTerm.
 */

public abstract class AbstractDurationRangeTerm
    extends AbstractQueryTerm
    implements org.osid.search.terms.DurationRangeTerm {

    private final org.osid.calendaring.Duration start;
    private final org.osid.calendaring.Duration end;


    /**
     *  Creates a new <code>AbstractDurationRangeTerm</code>.
     *
     *  @param start the duration start range
     *  @param end the duration end range
     *  @param match <code>true</code> if a positive term,
     *         <code>false</code> for a negative term
     *  @throws org.osid.InvalidArgumentException <code>start</code>
     *          is greater than <code>end</code> or types not compatible
     *  @throws org.osid.NullArgumentException <code>start</code> or
     *          <code>end</code> is <code>null</code>
     */

    public AbstractDurationRangeTerm(org.osid.calendaring.Duration start,
                                     org.osid.calendaring.Duration end, boolean match) {
	super(match);

        nullarg(start, "start duration range");
        nullarg(end, "end duration range");

        if (start.isGreater(end)) {
            throw new org.osid.InvalidArgumentException("start is greater than end");
        }

        this.start = start;
        this.end   = end;

	return;
    }


    /**
     *  Gets the start of the duration range in the term.
     *
     *  @return the duration start
     *  @throws org.osid.IllegalStateException
     *          <code>isWildcard()</code> is <code>true</code>
     */

    @OSID @Override
    public org.osid.calendaring.Duration getDurationRangeStart() {
        return (this.start);
    }


    /**
     *  Gets the end of the duration range in the term.
     *
     *  @return the duration end
     *  @throws org.osid.IllegalStateException
     *          <code>isWildcard()</code> is <code>true</code>
     */

    @OSID @Override
    public org.osid.calendaring.Duration getDurationRangeEnd() {
        return (this.end);
    }
}
