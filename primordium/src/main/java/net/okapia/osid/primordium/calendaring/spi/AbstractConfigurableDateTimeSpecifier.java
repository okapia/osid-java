//
// AbstractConfigurableDateTimeSpecifier.java
//
//     Datetime specifier for configurable datetimes.
//
//
// Tom Coppeto
// Okapia
// 21 July 2012
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.calendaring.spi;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  The AbstractConfigurableDateTimeTimeSpecifier is used to construct
 *  <code>DateTimes</code>. 
 */

public abstract class AbstractConfigurableDateTimeSpecifier
    extends AbstractDateTimeSpecifier {
    
    private final net.okapia.osid.primordium.calendaring.CalendarDefinition definition;
    
    
    /**
     *  Constructs a new <code>AbstractConfigurableDateTimeSpecifier</code>.
     *
     *  @param definition the calendar definition
     *  @param calendarType the type of calendar
     *  @param timeSpecifier the time specifier
     *  @throws org.osid.NullArgumentException <code>definition</code>,
     *          <code>calendarType</code> or <code>timeSpecifier</code>
     *          is <code>null</code>
     */
    
    protected AbstractConfigurableDateTimeSpecifier(net.okapia.osid.primordium.calendaring.CalendarDefinition definition,
                                                    org.osid.type.Type calendarType, 
                                                    AbstractTimeSpecifier timeSpecifier) {
        
        super(calendarType, timeSpecifier);
        nullarg(definition, "calendar definition");
        this.definition = definition;
        return;
    }
    
    
    /**
     *  Constructs a new <code>AbstractConfigurableDateTimeSpecifier</code>
     *  from another DateTime.
     *
     *  @param definition the calendar definition
     *  @param datetime a DateTime to copy
     *  @param timeSpecifier the time specifier
     *  @throws org.osid.InvalidArgumentException cannot copy
     *          datetime
     *  @throws org.osid.NullArgumentException
     *          <code>definition</code>, <code>datetime</code>, or
     *          <code>timeSpecifier</code> is <code>null</code>
     */
    
    protected AbstractConfigurableDateTimeSpecifier(net.okapia.osid.primordium.calendaring.CalendarDefinition definition,
                                                    org.osid.calendaring.DateTime datetime,
                                                    AbstractTimeSpecifier timeSpecifier) {        
        super(datetime, timeSpecifier);
        
        nullarg(definition, "calendar definition");
        this.definition = definition;
        
        return;
    }
    
    
    /**
     *  Sets the month.
     *
     *  @param month the month
     *  @throws org.osid.IllegalStateException cannot set months at
     *          current resolution
     *  @throws org.osid.InvalidArgumentException
     *          <code>month</code> is negative
     */
    
    public void setMonth(long month) {
        if (month > this.definition.getMonthsPerYear()) {
            throw new org.osid.InvalidArgumentException("month is out of range");
        }
        
        super.setMonth(month);
        return;
    }
    
    
    /**
     *  Sets the day.
     *
     *  @param day the day
     *  @throws org.osid.IllegalStateException cannot set days at
     *          current resolution
     *  @throws org.osid.InvalidArgumentException
     *          <code>day</code> is negative
     */
    
    public void setDay(long day) {
        super.setDay(day);
        return;
    }
}        
