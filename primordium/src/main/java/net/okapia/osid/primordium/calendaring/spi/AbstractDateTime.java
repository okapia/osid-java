//
// AbstractDateTime.java
//
//     Simple DateTime framework.
//
//
// Tom Coppeto
// Okapia
// 21 October 2009
//
// Copyright (c) 2009,2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;
import org.osid.calendaring.DateTimeResolution;

import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a shell for a DateTime.
 */

public abstract class AbstractDateTime
    implements org.osid.calendaring.DateTime {

    private final org.osid.type.Type calendarType;
    private final long aeon;
    private final long epoch;
    private final long millennium;
    private final long century;
    private final long year;
    private final long month;
    private final long day;

    private final org.osid.calendaring.Time time;

    private final org.osid.calendaring.DateTimeResolution granularity;
    private final long multiplier;

    private final org.osid.calendaring.DateTimeResolution uncertaintyUnits;
    private final long uncertaintyPlus;
    private final long uncertaintyMinus;
    private final boolean uncertaintyDateInclusive;
    private final boolean uncertaintyTimeInclusive;

    private static final long serialVersionUID = 1395417276L;


    /**
     *  Constructs a new <code>AbstractDateTime</code>.
     *
     *  @param specifier a datetime specifier
     *  @throws org.osid.NullArgumentException <code>specifier</code>
     *          is <code>null</code>
     */

    protected AbstractDateTime(AbstractDateTimeSpecifier specifier) {
        nullarg(specifier, "datetime specifier");

        this.calendarType             = specifier.getCalendarType();

        this.granularity              = specifier.getGranularity();
        this.multiplier               = specifier.getGranularityMultiplier();
        this.uncertaintyUnits         = specifier.getUncertaintyUnits();
        this.uncertaintyPlus          = specifier.getUncertaintyPlus();
        this.uncertaintyMinus         = specifier.getUncertaintyMinus();
        this.uncertaintyDateInclusive = specifier.isUncertaintyDateInclusive();
        this.uncertaintyTimeInclusive = specifier.isUncertaintyTimeInclusive();

        this.aeon                     = specifier.getAeon();
        this.epoch                    = specifier.getEpoch();
        this.millennium               = specifier.getMillennium();
        this.century                  = specifier.getCentury();
        this.year                     = specifier.getYear();
        this.month                    = specifier.getMonth();
        this.day                      = specifier.getDay();

        this.time                     = specifier.getTime();

        return;
    }


    /**
     *  Gets the calendar type. 
     *
     *  @return the calendar type 
     */

    @OSID @Override
    public final org.osid.type.Type getCalendarType() {
	return (this.calendarType);
    }


    /**
     *  Gets the time type. 
     *
     *  @return the time type 
     */

    @OSID @Override
    public final org.osid.type.Type getTimeType() {
	return (this.time.getTimeType());
    }


    /**
     *  Gets the aeon. An aeon is 1,000,000,000 years.
     *
     *  @return the aeon 
     */

    @OSID @Override
    public final long getAeon() {
	return (this.aeon);
    }


    /**
     *  Gets the epoch. An epoch is 1,000,000 years.
     *
     *  @return the epoch
     */

    @OSID @Override
    public final long getEpoch() {
	return (this.epoch);
    }


    /**
     *  Gets the millennium. A millennium is 1,000 years.
     *
     *  @return the millennium 
     */

    @OSID @Override
    public final long getMillennium() {
	return (this.millennium);
    }


    /**
     *  Gets the century.
     *
     *  @return the century 
     */

    @OSID @Override
    public final long getCentury() {
	return (this.century);
    }


    /**
     *  Gets the year.
     *
     *  @return the year 
     */

    @OSID @Override
    public final long getYear() {
	return (this.year);
    }


    /**
     *  Gets the month.
     *
     *  @return the month 
     */

    @OSID @Override
    public final long getMonth() {
	return (this.month);
    }


    /**
     *  Gets the day of the month.
     *
     *  @return the day of the month 
     */

    @OSID @Override
    public final long getDay() {
	return (this.day);
    }


    /**
     *  Gets the hour of the day.
     *
     *  @return the hour of the day 
     */

    @OSID @Override
    public final long getHour() {
	return (this.time.getHour());
    }


    /**
     *  Gets the minute of the hour.
     *
     *  @return the minute of the hour 
     */

    @OSID @Override
    public final long getMinute() {
	return (this.time.getMinute());
    }


    /**
     *  Gets the second of the minute.
     *
     *  @return the second of the minute 
     */

    @OSID @Override
    public final long getSecond() {
	return (this.time.getSecond());
    }


    /**
     *  Gets the number of milliseconds in this second 0-999. A millisecond is 
     *  one thousandth of a second. 
     *
     *  @return the milliseconds of the second 
     */

    @OSID @Override
    public final long getMilliseconds() {
	return (this.time.getMilliseconds());
    }


    /**
     *  Gets the number of microseconds of the second 0-999. A microsecond is 
     *  one millionth of a second. 
     *
     *  @return the micrseconds of the millisecond 
     */

    @OSID @Override
    public final long getMicroseconds() {
	return (this.time.getMicroseconds());
    }


    /**
     *  Gets the number of nanoseconds of the microsecond 0-999. A nanosecond 
     *  is one billionth of a second. 
     *
     *  @return the nanoseconds of the microsecond 
     */

    @OSID @Override
    public final long getNanoseconds() {
	return (this.time.getNanoseconds());
    }


    /**
     *  Gets the number of picoseconds of the nanosecond 0-999. A picosecond 
     *  is one trillionth of a second. 
     *
     *  @return the picoseconds of the nanosecond 
     */

    @OSID @Override
    public final long getPicoseconds() {
	return (this.time.getPicoseconds());
    }


    /**
     *  Gets the number of femtoseconds of the picosecond 0-999. A femtosecond 
     *  is one quadrillionth of a second. 
     *
     *  @return the femtoseconds of the picosecond 
     */

    @OSID @Override
    public final long getFemtoseconds() {
	return (this.time.getFemtoseconds());
    }


    /**
     *  Gets the number of attoseconds of the femtoseconds 0-999. An 
     *  attosecond is one quintillionth of a second. 
     *
     *  @return the attoseconds of the femtosecond 
     */

    @OSID @Override
    public final long getAttoseconds() {
	return (this.time.getAttoseconds());
    }


    /**
     *  Gets the number of zeptoseconds of the attosecond 0-999. A zeptosecond 
     *  is one sextillionth of a second. 
     *
     *  @return the zeptoseconds of the attosecond 
     */

    @OSID @Override
    public final long getZeptoseconds() {
	return (this.time.getZeptoseconds());
    }


    /**
     *  Gets the number of yoctoseconds of the picosecond 0-999. A yoctosecond 
     *  is one septillionth of a second. This is getting quite small. 
     *
     *  @return the yoctoseconds of the yoctosecond 
     */

    @OSID @Override
    public final long getYoctoseconds() {
	return (this.time.getYoctoseconds());
    }


    /**
     *  Gets the number of xoxxoseconds of the yoctosecond 0-999. A 
     *  xoxxosecond is one octillionth of a second. We're going with Rudy 
     *  Rucker here. 
     *
     *  @return the xoxxoseconds of the yoctosecond 
     */

    @OSID @Override
    public final long getXoxxoseconds() {
	return (this.time.getXoxxoseconds());
    }


    /**
     *  Gets the number of weebleseconds of the xoxxosecond 0-999. A 
     *  weeblesecond is one nonillionth of a second. 
     *
     *  @return the weebleseconds of the xoxxoseconds 
     */

    @OSID @Override
    public final long getWeebleseconds() {
	return (this.time.getWeebleseconds());
    }


    /**
     *  Gets the number of vatoseconds of the xoxxosecond 0-999. A vatosecond 
     *  is one decillionth of a second. 
     *
     *  @return the vatoseconds of the weeblesecond 
     */

    @OSID @Override
    public final long getVatoseconds() {
	return (this.time.getVatoseconds());
    }


    /**
     *  Gets the number of undaseconds of the vatosecond 0-999. An undasecond 
     *  is one unadecillionth of a second. 
     *
     *  @return the undaseconds of the vatosecond 
     */

    @OSID @Override
    public final long getUndaseconds() {
	return (this.time.getUndaseconds());
    }


    /**
     *  Gets the number of Plancks of the vatoseconds. A Planck is 10 
     *  quattuordecillionths of a second. 
     *
     *  @return the plancks of the undasecond 
     */

    @OSID @Override
    public final long getPlanckSeconds() {
	return (this.time.getPlanckSeconds());
    }

	
    /**
     *  Gets the granularity of this time. The granularity indicates the 
     *  resolution of the clock. More precision that what is specified in this 
     *  method cannot be inferred from the available data. 
     *
     *  @return granularity 
     */

    @OSID @Override
    public final org.osid.calendaring.DateTimeResolution getGranularity() {
	return (this.granularity);
    }


    /**
     *  If the granularity of the time equals <code> getGranularity(),
     *  </code> then the multiplier is 1. This method may return a
     *  different number when the granularity differs from one of the
     *  defined resolutions.
     *
     *  @return granularity multiplier 
     */

    @OSID @Override
    public final long getGranularityMultiplier() {
        return (this.multiplier);
    }


    /**
     *  Tests if uncertainty is defined for this time. 
     *
     *  @return <code> true </code> if uncertainty is defined, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public final boolean definesUncertainty() {
        return (this.uncertaintyUnits != null);
    }


    /**
     *  Gets the units of the uncertainty. 
     *
     *  @return units of the uncertainty 
     *  @throws org.osid.IllegalStateException <code> definesUncertainty() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public final org.osid.calendaring.DateTimeResolution getUncertaintyUnits() {
        if (!definesUncertainty()) {
            throw new org.osid.IllegalStateException("no uncertainty");
        }

	return (this.uncertaintyUnits);
    }


    /**
     *  Gets the uncertainty of this time in the negative direction. 
     *
     *  @return the uncertainty under this value 
     *  @throws org.osid.IllegalStateException <code> definesUncertainty() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public final long getUncertaintyMinus() {
        if (!definesUncertainty()) {
            throw new org.osid.IllegalStateException("no uncertainty");
        }

	return (this.uncertaintyMinus);
    }	


    /**
     *  Gets the uncertainty of this time in the positive direction. 
     *
     *  @return the uncertainty over this value 
     *  @throws org.osid.IllegalStateException <code> definesUncertainty() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public final long getUncertaintyPlus() {
	if (!definesUncertainty()) {
	    throw new org.osid.IllegalStateException("uncertainty not defined");
	}

	return (this.uncertaintyPlus);
    }


    /**
     *  Tests if the uncertainty is inclusive of all dates. An
     *  inclusive uncertainty includes the entire range specified by
     *  the uncertainty units e.g. +/- 1 year includes all of the
     *  months and days within that interval. A non-inclusive
     *  uncertainty would mean the year is uncertain but the month and
     *  day is certain.
     *
     *  @return <code> true </code> if the uncertainty includes all dates,
     *          <code> false </code> otherwise
     *  @throws org.osid.IllegalStateException <code> definesUncertainty()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public boolean isUncertaintyDateInclusive() {
	if (!definesUncertainty()) {
	    throw new org.osid.IllegalStateException("uncertainty not defined");
	}

        return (this.uncertaintyDateInclusive);
    }


    /**
     *  Tests if the uncertainty is time inclusive. An inclusive uncertainty
     *  includes the entire range specified by the uncertainty units e.g. +/-
     *  1 year includes all of the seconds within that interval. A
     *  non-inclusive uncertainty would mean the year is uncertain but the
     *  time is certain.
     *
     *  @return <code> true </code> if the uncertainty includes all times,
     *          <code> false </code> otherwise
     *  @throws org.osid.IllegalStateException <code> definesUncertainty()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public boolean isUncertaintyTimeInclusive() {
	if (!definesUncertainty()) {
	    throw new org.osid.IllegalStateException("uncertainty not defined");
	}

        return (this.uncertaintyTimeInclusive);
    }
}
