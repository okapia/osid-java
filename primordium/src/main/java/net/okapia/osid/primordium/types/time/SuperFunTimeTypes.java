//
// SuperFunTimeTypes.java
//
//     An enumeration of Super Fun Time Types.
//
//
// Tom Coppeto
// Okapia
// 5 April 2012
//
//
// Copyright (c) 2012 Okapa. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.types.time;

import static net.okapia.osid.primordium.locale.text.eng.us.Plain.text;


/**
 *  An enumeration of Super Fun Time Types.
 */

public enum SuperFunTimeTypes
    implements net.okapia.osid.torrefacto.collect.TypeEnum {

    /** Frakkin' Centon Time */
    COLONIAL ("Colonial", "Battlestar Galactica"),

    /** New Earth Time  */
    NET ("NewEarth", "New Earth"),

    /** Swatch Internet Time */
    SWATCH ("Swatch", "Swatch Internet");

    private final String identifier;
    private final String name;
    private org.osid.type.Type type;
    

    SuperFunTimeTypes(String name) {
        this(name, name);
        return;
    }


    SuperFunTimeTypes(String identifier, String name) {
        this.identifier = identifier;
        this.name       = name;
	return;
    }
    

    /**
     *  Gets the Type.
     *
     *  @return the Type
     */

    public org.osid.type.Type getType() {
	if (this.type == null) {
            this.type = new net.okapia.osid.primordium.type.BasicTypeBuilder()
                .authority("okapia.net")
                .namespace("time")
                .identifier(identifier)
                .domain(text("Alternative Time Systems"))
                .name(text(name + " Time"))
                .label(text(identifier))
                .description(text("The time type for the "  +  name + " time."))
                .build();
        }

        return (this.type);
    }
}
