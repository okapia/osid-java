//
// AbstractDegreeHeading.java
//
//     An implementation of Primitive for OSID Headings.
//
//
// Tom Coppeto
// Okapia
// 21 October 2011
//
// Copyright (c) 2011 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.mapping.spi;

import java.math.BigDecimal;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An implementation of Primitive for OSID Headings to provide
 *  consistent Java Object behavior across OSID implementations. All
 *  OSID Heading implementations should be a descendent of this class.
 */

public abstract class AbstractDegreeHeading 
    extends AbstractDegreeFractionalHeading
    implements org.osid.mapping.Heading {

    private String label;

    private static final long serialVersionUID = 1856897023L;


    /**
     *  Constructs a new <code>AbstractDegreeHeading</code>.
     *
     *  @param degrees
     *  @throws org.osid.InvalidArgumentException values are out of range
     *  @throws org.osid.NullArgumentException <code>seconds</code>
     *          <code>null</code>
     */

    protected AbstractDegreeHeading(BigDecimal degrees) {
        super(degrees);
        return;
    }


    /**
     *  Constructs a new <code>AbstractDegreeHeading</code>.
     *
     *  @param degrees
     *  @param minutes
     *  @param seconds
     *  @throws org.osid.InvalidArgumentException values are out of range
     *  @throws org.osid.NullArgumentException <code>seconds</code>
     *          <code>null</code>
     */

    protected AbstractDegreeHeading(int degrees, int minutes, BigDecimal seconds) {
        super(degrees, minutes, seconds);
        return;
    }


    /**
     *  Constructs a new <code>AbstractDegreeHeading</code>.
     *
     *  @param degrees
     *  @param minutes
     *  @param seconds
     *  @param uncertaintyPlus the uncertainty in the positive direction
     *  @param uncertaintyMinus the uncertainty in the other directio
     *  @throws org.osid.InvalidArgumentException values are out of range
     *  @throws org.osid.NullArgumentException <code>seconds</code>,
     *          <code>uncertaintyPlus</code>, or
     *          <code>uncertaintyMinus</code> is <code>null</code>
     */

    protected AbstractDegreeHeading(int degrees, int minutes, BigDecimal seconds, 
                                    BigDecimal uncertaintyPlus, BigDecimal uncertaintyMinus) {

        super(degrees, minutes, seconds, uncertaintyPlus, uncertaintyMinus);
        return;
    }


    /**
     *  Returns a string representation of this Heading.
     *
     *  All implementations are encouraged to supply this method to
     *  display the heading.
     *
     *  @return a string
     */

    @Override
    public String toString() {
        if (this.label == null) {
            this.label = formatLabel();
        }

        return (this.label);
    }


    private String formatLabel() {
        StringBuilder sb = new StringBuilder();
        BigDecimal degrees = getValues()[0];
        sb.append(degrees);

        if (definesUncertainty()) {
            BigDecimal uncertaintyPlus = getUncertaintyPlus()[0];
            BigDecimal uncertaintyMinus = getUncertaintyMinus()[0];

            boolean equals = true;
            if (!uncertaintyPlus.equals(uncertaintyMinus)) {
                equals = false;
            }

            if (equals) {
                sb.append(" +/-(");
            } else {
                sb.append(" +(");
            }

            sb.append(uncertaintyPlus);

            sb.append(")");
            if (!equals) {
                sb.append(" -(");
                sb.append(uncertaintyMinus);
            }
        }

        return (sb.toString());
    }
}
