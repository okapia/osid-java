//
// BasicIdBuilder.java
//
//     A builder for a BasicId.
//
//
// Tom Coppeto
// OnTapSolutions
// 29 April 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.id;


/**
 *  A Basic Id Builder.
 */

public class BasicIdBuilder
    extends net.okapia.osid.primordium.id.spi.AbstractIdBuilder<BasicIdBuilder> {
    
    /**
     *  Constructs a new <code>BasicIdBuilder</code>.
     */
    
    public BasicIdBuilder() {
        return;
    }


    /**
     *  Constructs a new <code>BasicIdBuilder</code> from another
     *  Id.
     *
     *  @param id an Id to copy
     *  @throws org.osid.NullArgumentException <code>id</code>
     *          is <code>null</code>
     */
    
    public BasicIdBuilder(org.osid.id.Id id) {
        super(id);
        return;
    }
    

    /**
     *  Gets the reference to this instance of the builder.
     *
     *  @return ObjectBuilder
     */

    @Override
    protected BasicIdBuilder self() {
        return (this);
    }

    
    /**
     *  Builds the BasicId. The Id authority, namespace, and
     *  identifier must have been set.
     *
     *  @return a new Id
     *  @throws org.osid.IllegalStateException Id cannot be
     *          constructed
     */

    @Override
    public org.osid.id.Id build() {
        String authority   = getAuthority();
        String namespace   = getNamespace();
        String identifier  = getIdentifier();
        
        if (authority == null) {
            throw new org.osid.IllegalStateException("authority has not been set");
        }
        
        if (namespace == null) {
            throw new org.osid.IllegalStateException("namespace has not been set");
        }
        
        if (identifier == null) {
            throw new org.osid.IllegalStateException("identifier has not been set");
        }
                
        return new BasicId(authority, namespace, identifier);
    }
}        
