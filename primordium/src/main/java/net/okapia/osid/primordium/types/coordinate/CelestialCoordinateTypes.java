//
// CelestialCoordinateTypes.java
//
//     An enumeration of Celestial Coordinate Types.
//
//
// Tom Coppeto
// Okapia
// 5 April 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.types.coordinate;

import static net.okapia.osid.primordium.locale.text.eng.us.Plain.text;


/**
 *  An enumeration of Celestial Coordinate Types.
 */

public enum CelestialCoordinateTypes
    implements net.okapia.osid.torrefacto.collect.TypeEnum {

    /** Ecliptic Coordinate System */
    ECLIPTIC ("Ecliptic", "Ecliptic Coordinate System"),

    /** Equatorial Coordinate System */
    EQUATORIAL ("Equatorial", "Equatorial Coordinate System"),

    /** Galactic Coordinate System */
    GCS ("Galactic", "Galactic Coordinate System"),

    /** Horizontal Altitude-Azimuth Coordinate System */
    HORIZON ("Horizon", "Horizontal Altitude-Azimuth Coordinate System"),

    /** International Celestial Reference System */
    ICRS ("ICRS", "International Celestial Reference System"),

    /** Supergalactic Coordinate System */
    SUPERGALACTIC ("Supergalactic", "Supergalactic Coordinate System");

    private final String identifier;
    private final String name;
    private org.osid.type.Type type;


    CelestialCoordinateTypes(String identifier, String name) {
        this.identifier = identifier;
        this.name = name;
	return;
    }
    

    /**
     *  Gets the Type.
     *
     *  @return the Type
     */

    public org.osid.type.Type getType() {
        if (this.type == null) {
            this.type = new net.okapia.osid.primordium.type.BasicTypeBuilder()
                .authority("okapia.net")
                .namespace("coordinate")
                .identifier(this.identifier)
                .domain(text("Celestial Coordinate Systems"))
                .name(text(name))
                .label(text(identifier))
                .description(text("The " +  name + "."))
                .build();
        }

	return (this.type);
    }
}
