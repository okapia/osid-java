//
// UTCTime.java
//
//     An implementation of a UTC based OSID Time.
//
//
// Tom Coppeto
// Okapia
// 2 May 2012
//
// Copyright (c) 2012 Ingenescus. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.calendaring;

import org.osid.binding.java.annotation.OSID;
import org.osid.calendaring.DateTimeResolution;

import net.okapia.osid.primordium.types.time.EarthTimeTypes;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An implementation of a Time based on UTC using a
 *  UTCTimeDefinition.
 *
 *  Currently, leap seconds are not supported in this class.
 */

public final class UTCTime 
    extends net.okapia.osid.primordium.calendaring.spi.Abstract246060Time
    implements org.osid.calendaring.Time {

    private static final long serialVersionUID = 317675668L;


    /**
     *  Constructs a new <code>UTCTime</code> with second
     *  resolution.
     *
     *  @param hour the hour
     *  @param minute the minute
     *  @param second the second
     *  @throws org.osid.InvalidArgumentException <code>hour</code>,
     *          <code>minute</code>, or <code>second</code> is
     *          negative, or <code>hour</code> is greater than 23
     *          <code>minute</code> is greater than 59, or
     *          <code>second</code> is greater than 60
     */

    public UTCTime(long hour, long minute, long second) {
        super(new UTCTimeFactory(), hour, minute, second);
        return;
    }

    
    /**
     *  Constructs a new <code>UTCTime</code>.
     *
     *  @param specifier a time specifier
     *  @throws org.osid.NullArgumentException <code>specifier</code>
     *          is <code>null</code>
     */

    public UTCTime(UTCTimeSpecifier specifier) {
        super(new UTCTimeFactory(), specifier);
    }


    /**
     *  Returns a Time for a time format.
     *
     *  @return a string
     *  @throws org.osid.InvalidArgumentException cannot parse string
     *  @throws org.osid.NullArgumentException <code>s</code> is
     *          <code>null</code>
     */

    public static org.osid.calendaring.Time valueOf(String s) {        
        nullarg(s, "time string");
        return (parseTime(new UTCTimeFactory(), s));
    }


    /**
     *  Gets an unknown date.
     *
     *  @return an unknown date
     */

    public static org.osid.calendaring.Time unknown() {
        UTCTimeSpecifier specifier = new UTCTimeSpecifier();
        specifier.setGranularity(DateTimeResolution.INFINITY);
        return (new UTCTime(specifier));        
    }


    /**
     *  The TimeSpecifier is used to construct
     *  <code>UTCTimes</code>. 
     */

    public static class UTCTimeSpecifier
        extends net.okapia.osid.primordium.calendaring.spi.AbstractConfigurableTimeSpecifier {

        
        /**
         *  Constructs a new <code>TimeSpecifier</code>.
         */

        public UTCTimeSpecifier() {
            super(new UTCTimeDefinition());
            return;
        }


        /**
         *  Constructs a new <code>TimeSpecifier</code> from another Time.
         *
         *  @param time a Time to copy
         *  @throws org.osid.NullArgumentException <code>time</code>
         *          is <code>null</code>
         */
        
        public UTCTimeSpecifier(org.osid.calendaring.Time time) {            
            super(new UTCTimeDefinition());
            setTime(time);
            return;
        }


        /**
         *  Constructs a new <code>TimeSpecifier</code> from another DateTime.
         *
         *  @param datetime a DateTime to copy
         *  @throws org.osid.NullArgumentException <code>datetime</code>
         *          is <code>null</code>
         */
        
        public UTCTimeSpecifier(org.osid.calendaring.DateTime datetime) {            
            super(new UTCTimeDefinition());
            setDateTime(datetime);
            return;
        }
    }


    protected static class UTCTimeFactory
        implements net.okapia.osid.primordium.calendaring.spi.TimeFactory {

        private static final long serialVersionUID = 3176756681L;
        

        /**
         *  Gets a <code>TimeDefinition</code>.
         *
         *  @return the time definition
         */
        
        @Override
        public TimeDefinition getTimeDefinition() {
            return (new UTCTimeDefinition());
        }


        /**
         *  Gets a <code>AbstractTimeSpecifier</code>.
         */

        @Override
        public UTCTimeSpecifier getTimeSpecifier() {
            return (new UTCTimeSpecifier());
        }


        /**
         *  Makes a Time.
         *
         *  @param specifier the time specifier
         *  @return a Time
         *  @throws org.osid.InvalidArgumentException sepcifier is not
         *          for UTC
         *  @throws org.osid.NullArgumentException <code>specifier</code>
         *          is <code>null</code>
         */

        @Override
        public org.osid.calendaring.Time makeTime(net.okapia.osid.primordium.calendaring.spi.AbstractTimeSpecifier specifier) {
            if (!(specifier instanceof UTCTimeSpecifier)) {
                throw new org.osid.InvalidArgumentException("not a UTC specifier");
            }

            return (new UTCTime((UTCTimeSpecifier) specifier));
        }
    }
}
