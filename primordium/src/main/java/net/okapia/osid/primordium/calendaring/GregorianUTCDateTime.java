//
// GregorianUTCDateTime.java
//
//     A Gregorian Date.
//
//
// Tom Coppeto
// Okapia
// 21 July 2012
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.calendaring;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;
import org.osid.calendaring.DateTimeResolution;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A Gregorian Date.
 */

public final class GregorianUTCDateTime 
    extends net.okapia.osid.primordium.calendaring.spi.AbstractConfigurableDateTime
    implements org.osid.calendaring.DateTime {

    protected final static org.osid.type.Type CALENDAR_TYPE = net.okapia.osid.primordium.types.calendar.CalendarTypes.GREGORIAN.getType();
    private final static org.osid.type.Type TIME_TYPE = net.okapia.osid.primordium.types.time.EarthTimeTypes.UTC.getType();        
    private static final long serialVersionUID = -690060938L;

    
    /**
     *  Constructs a new <code>GregorianUTCDateTime</code>.
     *
     *  @param specifier a datetime specifier
     *  @throws org.osid.NullArgumentException <code>specifier</code>
     *          is <code>null</code>
     */

    public GregorianUTCDateTime(GregorianUTCDateTimeSpecifier specifier) {
        super(new GregorianUTCDateTimeFactory(), specifier);
        return;
    }


    /**
     *  Parses an ISO date string.
     *
     *  @param iso the iso formatted string
     *  @return a Gregorian/UTC DateTime
     *  @throws org.osid.InvalidArgumentException could not 
     *          parse <code>iso</code>
     *  @throws org.osid.NullArgumentException <code>iso</code>
     *          is <code>null</code>
     */

    public static org.osid.calendaring.DateTime valueOf(String iso) {
        return(parseISO(new GregorianUTCDateTimeFactory(), iso));
    }


    /**
     *  Converts a Java Date to a DateTime using millisecond granularity. 
     *
     *  @param date a Java Date
     *  @return a Gregorian/UTC DateTime
     *  @throws org.osid.NullArgumentException <code>date</code>
     *          is <code>null</code>
     */

    public static org.osid.calendaring.DateTime valueOf(java.util.Date date) {
        return (valueOf(date, DateTimeResolution.MILLISECOND));
    }


    /**
     *  Converts a Java Date to a DateTime. 
     *
     *  @param date a Java Date
     *  @param granularity the clock resolution
     *  @return a Gregorian/UTC DateTime
     *  @throws org.osid.NullArgumentException <code>date</code> or
     *          <code>granularity</code> is <code>null</code>
     */

    public static org.osid.calendaring.DateTime valueOf(java.util.Date date, DateTimeResolution granularity) {
        nullarg(date, "date");
        nullarg(granularity, "granularity");

        java.util.GregorianCalendar calendar = new java.util.GregorianCalendar(java.util.TimeZone.getTimeZone("GMT"));
        calendar.setTime(date);

        return (valueOf(calendar, granularity));
    }        


    /**
     *  Converts a Java Gregorian Calendar to a DateTime using
     *  millisecond granularity.
     *
     *  @param calendar a Java Gregorian Calendar
     *  @return a Gregorian/UTC DateTime
     *  @throws org.osid.NullArgumentException <code>calendar</code>
     *          is <code>null</code>
     */

    public static org.osid.calendaring.DateTime valueOf(java.util.GregorianCalendar calendar) {
        return (valueOf(calendar, DateTimeResolution.MILLISECOND));
    }


    /**
     *  Converts a Java Gregorian Calendar to a DateTime. 
     *
     *  @param calendar a Java Gregorian Calendar
     *  @param granularity the resolution
     *  @return a Gregorian/UTC DateTime
     *  @throws org.osid.NullArgumentException <code>calendar</code>
     *          or <code>granularity</code> is <code>null</code>
     */

    public static org.osid.calendaring.DateTime valueOf(java.util.GregorianCalendar calendar,
                                                        DateTimeResolution granularity) {
        nullarg(calendar, "calendar");
        nullarg(granularity, "granularity");

        java.util.Calendar c = calendar;
        if (!calendar.getTimeZone().getID().equals("GMT")) {
            c = java.util.GregorianCalendar.getInstance(java.util.TimeZone.getTimeZone("GMT"));
            c.setTimeInMillis(calendar.getTimeInMillis());
        }

        GregorianUTCDateTimeSpecifier specifier = new GregorianUTCDateTimeSpecifier();

        if (c.get(java.util.Calendar.ERA) == java.util.GregorianCalendar.AD) {
            specifier.setYear(c.get(java.util.Calendar.YEAR));
        } else {            
            specifier.setYear(c.get(java.util.Calendar.YEAR) * -1);
        }
            
        specifier.setMonth(c.get(java.util.Calendar.MONTH) + 1);
        specifier.setDay(c.get(java.util.Calendar.DAY_OF_MONTH));
        specifier.setHour(c.get(java.util.Calendar.HOUR_OF_DAY));
        specifier.setMinute(c.get(java.util.Calendar.MINUTE));
        specifier.setSecond(c.get(java.util.Calendar.SECOND));
        specifier.setMilliseconds(c.get(java.util.Calendar.MILLISECOND));
        specifier.setGranularity(granularity);

        return (new GregorianUTCDateTime(specifier));
    }


    /**
     *  Gets the datetime as of right now.
     *
     *  @return a current Gregorian/UTC DateTime
     */

    public static org.osid.calendaring.DateTime now() {
        java.util.GregorianCalendar calendar = new java.util.GregorianCalendar(java.util.TimeZone.getTimeZone("GMT"));
        return (valueOf(calendar));
    }

    
    /**
     *  Gets an unknown date.
     *
     *  @return an unknown date
     */

    public static org.osid.calendaring.DateTime unknown() {
        GregorianUTCDateTimeSpecifier specifier = new GregorianUTCDateTimeSpecifier();
        specifier.setGranularity(DateTimeResolution.INFINITY);
        specifier.setUncertainty(DateTimeResolution.INFINITY, 0);
        return (new GregorianUTCDateTime(specifier));        
    }

        
    /**
     *  The GregorianUTCDateTimeSpecifier is used to construct
     *  <code>GregorianUTCDateTimes</code>.
     */

    public static class GregorianUTCDateTimeFactory
        implements net.okapia.osid.primordium.calendaring.spi.DateTimeFactory {

        private static final long serialVersionUID = -6900609381L;
        
        
        /**
         *  Gets a <code>CalendarDefinition</code>.
         *
         *  @return the datetime definition
         */
        
        @Override
        public GregorianCalendarDefinition getCalendarDefinition() {
            return (new GregorianCalendarDefinition());
        }


        /**
         *  Gets a <code>GregorianDateTimeSpecifier</code>.
         */
        
        @Override
        public GregorianUTCDateTimeSpecifier getDateTimeSpecifier() {
            return (new GregorianUTCDateTimeSpecifier());
        }

        
        /**
         *  Makes a DateTime from a DateTimeSpecifier.
         *
         *  @param specifier the time specifier
         *  @return a DateTime
         *  @throws org.osid.NullArgumentException <code>specifier</code>
         *          is <code>null</code>
         *  @throws org.osid.UnsupportedException calendarType not supported
         */
        
        @Override
        public org.osid.calendaring.DateTime makeDateTime(net.okapia.osid.primordium.calendaring.spi.AbstractDateTimeSpecifier specifier) {
            if (!(specifier instanceof GregorianUTCDateTimeSpecifier)) {
                throw new org.osid.UnsupportedException("specifier not supported");
            }

            return (new GregorianUTCDateTime((GregorianUTCDateTimeSpecifier) specifier));
        }


        /**
         *  Makes a Time from a TimeSpecifier.
         *
         *  @param specifier the time specifier
         *  @return a Time
         *  @throws org.osid.NullArgumentException <code>specifier</code>
         *          is <code>null</code>
         *  @throws org.osid.UnsupportedException timeType not supported
         */
        
        @Override
        public UTCTime makeTime(net.okapia.osid.primordium.calendaring.spi.AbstractTimeSpecifier specifier) {
            if (!(specifier instanceof UTCTime.UTCTimeSpecifier)) {
                throw new org.osid.UnsupportedException("specifier not supported");
            }

            return (new UTCTime((UTCTime.UTCTimeSpecifier) specifier));
        }

        
        /**
         *  Makes a Time from a DateTime.
         *
         *  @param datetime a datetime
         *  @return a DateTime
         *  @throws org.osid.NullArgumentException <code>datetime</code>
         *          is <code>null</code>
         *  @throws org.osid.UnsupportedException timeType not supported
         */
        
        @Override
        public org.osid.calendaring.Time makeTime(org.osid.calendaring.DateTime datetime) {
            return (new UTCTime(new UTCTime.UTCTimeSpecifier(datetime)));
        }    
    }


    /**
     *  The GregorianUTCDateTimeSpecifier is used to construct
     *  <code>DateTimes</code>.
     */
    
    public static class GregorianUTCDateTimeSpecifier
        extends net.okapia.osid.primordium.calendaring.spi.AbstractConfigurableDateTimeSpecifier {
        

        /**
         *  Constructs a new <code>AbstractGregorianDateTimeSpecifier</code>.
         */
        
        public GregorianUTCDateTimeSpecifier() {
            super(new GregorianCalendarDefinition(), CALENDAR_TYPE, new UTCTime.UTCTimeSpecifier());
            return;
        }

    
        /**
         *  Constructs a new
         *  <code>GregorianUTCDateTimeSpecifier</code> from another
         *  DateTime.
         *
         *  @param datetime a DateTime to copy
         *  @throws org.osid.InvalidArgumentException cannot copy datetime
         *  @throws org.osid.NullArgumentException <code>datetime</code>
         *          is <code>null</code>
         */
        
        protected GregorianUTCDateTimeSpecifier(org.osid.calendaring.DateTime datetime) {
            super(new GregorianCalendarDefinition(), datetime, new UTCTime.UTCTimeSpecifier());        
            return;
        }

        
        /**
         *  Gets the time component of this specifier.
         *
         *  @return the time component
         */
        
        @Override
        public org.osid.calendaring.Time getTime() {
            return (new UTCTime((UTCTime.UTCTimeSpecifier) getTimeSpecifier()));
        }
    }
}
