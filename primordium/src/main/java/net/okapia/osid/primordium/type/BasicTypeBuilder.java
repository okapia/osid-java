//
// BasicTypeBuilder.java
//
//     A builder for a BasicType.
//
//
// Tom Coppeto
// OnTapSolutions
// 29 April 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.type;

import static net.okapia.osid.primordium.locale.text.eng.us.Plain.text;


/**
 *  A Basic Type Builder.
 */

public class BasicTypeBuilder
    extends net.okapia.osid.primordium.type.spi.AbstractTypeBuilder<BasicTypeBuilder> {
    
    /**
     *  Constructs a new <code>BasicTypeBuilder</code>.
     */
    
    public BasicTypeBuilder() {
        return;
    }


    /**
     *  Constructs a new <code>BasicTypeBuilder</code> from another
     *  Type.
     *
     *  @param type a Type to copy
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */
    
    public BasicTypeBuilder(org.osid.type.Type type) {
        super(type);
        return;
    }
    

    /**
     *  Gets the reference to this instance of the builder.
     *
     *  @return ObjectBuilder
     */

    @Override
    protected BasicTypeBuilder self() {
        return (this);
    }

    
    /**
     *  Builds the BasicType. The Type authority, namespace, and
     *  identifier must have been set.
     *
     *  @return a new Type
     *  @throws org.osid.IllegalStateException Type cannot be
     *          constructed
     */

    @Override
    public org.osid.type.Type build() {
        String authority   = getAuthority();
        String namespace   = getNamespace();
        String identifier  = getIdentifier();

        org.osid.locale.DisplayText domain      = getDomain();
        org.osid.locale.DisplayText name        = getDisplayName();
        org.osid.locale.DisplayText label       = getDisplayLabel();
        org.osid.locale.DisplayText description = getDescription();
        
        if (authority == null) {
            throw new org.osid.IllegalStateException("type authority has not been set");
        }
        
        if (namespace == null) {
            throw new org.osid.IllegalStateException("type namespace has not been set");
        }
        
        if (identifier == null) {
            throw new org.osid.IllegalStateException("type identifier has not been set");
        }
        
        if ((domain == null) && (authority == null) && (namespace == null) &&
            (identifier == null)) {
            
            return new BasicType(authority, namespace, identifier);
        }
        
        if (domain == null) {
            domain = text("");
        }
        
        if (name == null) {
            if (label != null) {
                name = label;
            } else {
                name = text(BasicType.formatDefaultName(authority, namespace, identifier));
                label = name;
            }
        }
        
        if (label == null) {
            label = text(BasicType.formatDefaultName(authority, namespace, identifier));
        }
        
        if (description == null) {
            description = text("");
        }
        
        return new BasicType(authority, namespace, identifier, domain, name, label, description);        
    }
}        
