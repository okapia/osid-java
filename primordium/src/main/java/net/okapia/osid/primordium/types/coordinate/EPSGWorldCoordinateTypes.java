//
// EPSGWorldCoordinateTypes.java
//
//     An enumeration of EPSG World Coordinate Reference Systems.
//
//
// Tom Coppeto
// Okapia
// 25 November 2013
//
//
// Copyright (c) 2013 Okpaia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.types.coordinate;

import static net.okapia.osid.primordium.locale.text.eng.us.Plain.text;


/**
 *  An enumeration of EPSG World Coordinate Reference Systems.
 */

public enum EPSGWorldCoordinateTypes
    implements net.okapia.osid.torrefacto.collect.TypeEnum {

    EPSG_3855 ("EGM2008 geoid height", "Vertical / Earth Gravitational Model 2008 geoid datum for the world."),
    EPSG_4276 ("NSWC 9Z-2", "Ellipsoidal 2D / Naval Surface Warfare Center 9Z-2 geodetic datum for the world."),
    EPSG_4322 ("WGS 72", "Ellipsoidal 2D / World Geodetic System 1972 geodetic datum for the world."),
    EPSG_4324 ("WGS 72BE", "Ellipsoidal 2D / WGS 72 Transit Broadcast Ephemeris geodetic datum for the world."),
    EPSG_4326 ("WGS 84", "Ellipsoidal 2D / World Geodetic System 1984 geodetic datum for the world."),
    EPSG_4740 ("PZ-90", "Ellipsoidal 2D / Parametrop Zemp 1990 geodetic datum for the world."),
    EPSG_4760 ("WGS 66", "Ellipsoidal 2D / World Geodetic System 1966 geodetic datum for the world."),
    EPSG_4890 ("WGS 66", "Cartesian 3D / World Geodetic System 1966 geodetic datum for the world."),
    EPSG_4891 ("WGS 66", "Ellipsoidal 3D / World Geodetic System 1966 geodetic datum for the world."),
    EPSG_4896 ("ITRF2005", "Cartesian 3D / International Terrestrial Reference Frame (ITRF) 2005 geodetic datum for the world."),
    EPSG_4910 ("ITRF88", "Cartesian 3D / International Terrestrial Reference Frame (ITRF) 1988 for the world."),
    EPSG_4911 ("ITRF89", "Cartesian 3D / International Terrestrial Reference Frame (ITRF) 1989 for the world."),
    EPSG_4912 ("ITRF90", "Cartesian 3D / International Terrestrial Reference Frame (ITRF) 1990 for the world."),
    EPSG_4913 ("ITRF91", "Cartesian 3D / International Terrestrial Reference Frame (ITRF) 1991 for the world."),
    EPSG_4914 ("ITRF92", "Cartesian 3D / International Terrestrial Reference Frame (ITRF) 1992 for the world."),
    EPSG_4915 ("ITRF93", "Cartesian 3D / International Terrestrial Reference Frame (ITRF) 1993 for the world."),
    EPSG_4916 ("ITRF94", "Cartesian 3D / International Terrestrial Reference Frame (ITRF) 1994 for the world."),
    EPSG_4917 ("ITRF96", "Cartesian 3D / International Terrestrial Reference Frame (ITRF) 1996 for the world."),
    EPSG_4918 ("ITRF97", "Cartesian 3D / International Terrestrial Reference Frame (ITRF) 1997 for the world."),
    EPSG_4919 ("ITRF2000", "Cartesian 3D / International Terrestrial Reference Frame (ITRF) 2000 for the world."),
    EPSG_4922 ("PZ-90", "Cartesian 3D / Parametrop Zemp 1990 geodetic datum for the world."),
    EPSG_4923 ("PZ-90", "Ellipsoidal 3D / Parametrop Zemp 1990 geodetic datum for the world."),
    EPSG_4978 ("WGS 84", "Cartesian 3D / World Geodetic System 1984 geodetic datum for the world."),
    EPSG_4979 ("WGS 84", "Ellipsoidal 3D / World Geodetic System 1984 geodetic datum for the world."),
    EPSG_4984 ("WGS 72", "Cartesian 3D / World Geodetic System 1972 geodetic datum for the world."),
    EPSG_4985 ("WGS 72", "Ellipsoidal 3D / World Geodetic System 1972 geodetic datum for the world."),
    EPSG_4986 ("WGS 72BE", "Cartesian 3D / WGS 72 Transit Broadcast Ephemeris geodetic datum for the world."),
    EPSG_4987 ("WGS 72BE", "Ellipsoidal 3D / WGS 72 Transit Broadcast Ephemeris geodetic datum for the world."),
    EPSG_5332 ("ITRF2008", "Cartesian 3D / International Terrestrial Reference Frame (ITRF) 2008 geodetic datum for the world."),
    EPSG_5714 ("MSL height", "Vertical / Mean Sea Level vertical datum for the world."),
    EPSG_5715 ("MSL depth", "Vertical / Mean Sea Level vertical datum for the world."),
    EPSG_5773 ("EGM96 geoid height", "Vertical / Earth Gravitational Model (EGM) geoid 1996 vertical datum for the world."),
    EPSG_5798 ("EGM84 geoid height", "Vertical / Earth Gravitational Moel (EGM84) geoid 1984 vertical datum for the world."),
    EPSG_5829 ("Instantaneous Water Level height", "Vertical / Instantaneous Water Level vertical datum for the world."),
    EPSG_5831 ("Instantaneous Water Level depth", "Vertical / Instantaneous Water Level vertical datum for the world."),
    EPSG_5861 ("LAT depth", "Vertical / Lowest Astronomic Tide vertical datum for the world."),
    EPSG_5862 ("LLWLT depth", "Vertical / Lower Low Water Large Tide vertical datum for the world."),
    EPSG_5863 ("ISLW depth", "Vertical / Indian Spring Low Water vertical datum for the world."),
    EPSG_5864 ("MLLWS depth", "Vertical / Mean Lower Low Water Spring Tides vertical datum for the world."),
    EPSG_5865 ("MLWS depth", "Vertical / Mean Low Water Spring Tides vertical datum for the world."),
    EPSG_5866 ("MLLW depth", "Vertical / Mean Lower Low Water vertical datum for the world."),
    EPSG_5867 ("MLW depth", "Vertical / Mean Low Water vertical datum for the world."),
    EPSG_5868 ("MHW height", "Vertical / Mean High Water vertical datum for the world."),
    EPSG_5869 ("MHHW height", "Vertical / Mean Higher High Water vertical datum for the world."),
    EPSG_5870 ("MHWS height", "Vertical / Mean High Water Spring Tides vertical datum for the world."),
    EPSG_5871 ("HHWLT height", "Vertical / Higher High Water Large Tide vertical datum for the world."),
    EPSG_5872 ("HAT height", "Vertical / Highest Astronomic Tide vertical datum for the world."),
    EPSG_5873 ("Low Water depth", "Vertical / Low Water vertical datum for the world."),
    EPSG_5874 ("High Water height", "Vertical / High Water vertical datum for the world.");
    
    private final String name;
    private final String description;               
    private org.osid.type.Type type;

    
    EPSGWorldCoordinateTypes(String name, String description) {
        this.name        = name;
        this.description = description;
	return;
    }
    

    /**
     *  Gets the Type.
     *
     *  @return the Type
     */

    public org.osid.type.Type getType() {
        if (this.type == null) {
            this.type = new net.okapia.osid.primordium.type.BasicTypeBuilder()
                .authority("ogp.org")
                .namespace("epsg")
                .identifier(name())
                .domain(text("Geographic Coordinate Reference Systems"))
                .name(text(this.name + " Coordinate Reference System"))
                .label(text(this.name))
                .description(text(this.description))
                .build();
        }

	return (this.type);
    }
}