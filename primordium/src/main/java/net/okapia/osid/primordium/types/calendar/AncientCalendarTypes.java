//
// AncientCalendarTypes.java
//
//     An enumeration of Ancient Calendar Types.
//
//
// Tom Coppeto
// Okapia
// 5 April 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.types.calendar;

import static net.okapia.osid.primordium.locale.text.eng.us.Plain.text;


/**
 *  An enumeration of very old Calendar Types.
 */

public enum AncientCalendarTypes
    implements net.okapia.osid.torrefacto.collect.TypeEnum {
 
        /** Armenian Calendar */
        ARMENIAN ("Armenian"),

        /** Assyrian Calendar */
        ASSYRIAN ("Assyrian"),
        
        /** Attic Calendar */
        ATTIC ("Attic"),

        /** Aztec Calendar */
        AZTEC ("Aztec"),

        /** Babylonian Calendar */
        BABYLONIAN ("Babylonian"),

        /** Boeotian Calendar */
        BOEOTIAN ("Boeotian"),

        /** Bulgar Calendar */
        BULGAR ("Bulgar"),

        /** Byzantine Calendar */
        BYZANTINE ("Byzantine"),

        /** Coligny Calendar */
        COLIGNY ("Coligny"),    

        /** Cretan Calendar */
        CRETAN ("Cretan"),

        /** Delphic Calendar */
        DELPHIC ("Delphic"),

        /** Egyptian Calendar */
        EGYPTIAN ("Egyptian"),

        /** Epirotic Calendar */
        EPIROTIC ("Epirotic"),

        /** Enoch Calendar */
        ENOCH ("Enoch"),       

        /** Florentine Calendar */
        FLORENTINE ("Florentine"),

        /** FrenchRepublican Calendar */
        FRENCH_REPUBLICAN ("FrenchRepublican", "French Republican"),

        /** Germanic Calendar */
        GERMANIC ("Germanic"),

        /** Gaelic Calendar */
        GAELIC ("Gaelic"),

        /** Laconian Calendar */
        LACONIAN ("Laconian"),

        /** Lithuanian Calendar */
        LITHUANIAN ("Lithuanian"),

        /** Macedonian Calendar */
        MACEDONIAN ("Macedonian", "Ancient Macedonian"),

        /** Maya Calendar */
        MAYA ("Maya"),

        /** Minguo Calendar */
        MINGUO ("Minguo"),

        /** Pentecontad Calendar */
        PENTECONTAD ("Pentecontad"),

        /** RapaNui Calendar */
        RAPA_NUI ("RapaNui", "Rapa Nui"),

        /** Rhodian Calendar */
        RHODIAN ("Rhodian"),

        /** Roman Calendar */
        ROMAN ("Roman"),

        /** Runic Calendar */
        RUNIC ("Runic"),

        /** Sicilian Calendar */
        SICILIAN ("Sicilian"),

        /** Soviet Calendar */
        SOVIET ("Soviet"),

        /** Swedish Calendar */
        SWEDISH ("Swedish");

    private final org.osid.type.Type type;
    

    AncientCalendarTypes(String name) {
        this(name, name);
        return;
    }


    AncientCalendarTypes(String identifier, String name) {
	this.type = new net.okapia.osid.primordium.type.BasicTypeBuilder()
            .authority("okapia.net")
            .namespace("calendar")
            .identifier(identifier)
            .domain(text("Ancient Calendar Types"))
            .name(text(name + " Calendar"))
            .label(text(name))
            .description(text("The calendar type for the " +  name + " calendar."))
            .build();

	return;
    }
    

    /**
     *  Gets the Type.
     *
     *  @return the Type
     */

    public org.osid.type.Type getType() {
	return (this.type);
    }
}
