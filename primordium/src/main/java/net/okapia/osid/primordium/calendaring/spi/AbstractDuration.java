//
// AbstractDuration.java
//
//     An abstract Duration.
//
//
// Tom Coppeto
// Okapia
// 2 July 2012
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;

import java.math.BigDecimal;


/**
 *  An abstract Duration.
 */

public abstract class AbstractDuration
    implements org.osid.calendaring.Duration {

    private final org.osid.type.Type calendarType;
    private final org.osid.type.Type timeType;

    private final org.osid.calendaring.DateTimeResolution granularity;
    private final long multiplier;

    private org.osid.calendaring.DateTimeResolution uncertaintyUnits;
    private long uncertaintyMinus;
    private long uncertaintyPlus;

    private int hash;

    private static final long serialVersionUID = 1905622709L;

    
    /**
     *  Constructs a new <code>AbstractDuration</code>.
     * 
     *  @param calendarType the type of calendar 
     *  @param timeType the time system
     *  @param granularity the resolution
     *  @param granularityMultiplier the resolution multiplier
     *  @throws org.osid.InvalidArgumentException
     *          <code>granularityMultiplier</code> is negative
     *  @throws org.osid.NullArgumentException
     *          <code>calendarType</code>, <code>timeType</code>, or
     *          <code>granularity</code> is <code>null</code>
     */

    protected AbstractDuration(org.osid.type.Type calendarType, org.osid.type.Type timeType, 
                               org.osid.calendaring.DateTimeResolution granularity, 
                               long granularityMultiplier) {

        nullarg(timeType, "time type");
        nullarg(calendarType, "calendar type");
        nullarg(granularity, "granularity");
        cardinalarg(granularityMultiplier, "granularity multiplier");

        this.timeType     = timeType;
        this.calendarType = calendarType;

        if (granularityMultiplier > 0) {
            this.granularity  = granularity;
        } else {
            this.granularity  = org.osid.calendaring.DateTimeResolution.INFINITY;
        }

        this.multiplier   = granularityMultiplier;

        return;
    }


    /**
     *  Constructs a new <code>AbstractDuration</code>.
     * 
     *  @param calendarType the type of calendar 
     *  @param timeType the time system
     *  @param granularity the resolution
     *  @param granularityMultiplier the resolution multiplier
     *  @param uncertaintyUnits the units of uncertainty
     *  @param uncertaintyPlus uncertainty in the positive direction
     *  @param uncertaintyMinus uncertainty in the negative direction
     *  @throws org.osid.InvalidArgumentException
     *          <code>granularityMultiplier</code,
     *          <code>uncertaintyPlus</code>, or
     *          <code>uncertaintyMinus</code>,
     *          is negative
     *  @throws org.osid.NullArgumentException
     *          <code>calendarType</code>, <code>timeType</code>,
     *          <code>granularity</code>, or
     *          <code>uncertaintyUnits</code> is <code>null</code>
     */

    protected AbstractDuration(org.osid.type.Type calendarType, org.osid.type.Type timeType, 
                               org.osid.calendaring.DateTimeResolution granularity, long granularityMultiplier,
                               org.osid.calendaring.DateTimeResolution uncertaintyUnits, 
                               long uncertaintyPlus, long uncertaintyMinus) {

        this(calendarType, timeType, granularity, granularityMultiplier);

        nullarg(uncertaintyUnits, "uncertainty units");
        cardinalarg(uncertaintyPlus, "uncertainty plus");
        cardinalarg(uncertaintyMinus, "uncertainty minus");

        this.uncertaintyUnits = uncertaintyUnits;
        this.uncertaintyPlus  = uncertaintyPlus;
        this.uncertaintyMinus = uncertaintyMinus;

        return;
    }


    /**
     *  Gets the calendar type. 
     *
     *  @return the calendar type 
     */

    @OSID @Override
    public final org.osid.type.Type getCalendarType() {
        return (this.calendarType);
    }


    /**
     *  Gets the time type. 
     *
     *  @return the time type 
     */

    @OSID @Override
    public final org.osid.type.Type getTimeType() {
        return (this.timeType);
    }


    /**
     *  Gets the number of aeons. An aeon is 1B years. 
     *
     *  @return the number of aeons 
     */

    @OSID @Override
    public final java.math.BigDecimal getAeons() {        
        return (getYears().divide(new BigDecimal("1e9")));
    }


    /**
     *  Gets the number of epochs. An epoch is 1M years. 
     *
     *  @return the number of epochs 
     */

    @OSID @Override
    public final java.math.BigDecimal getEpochs() {
        return (getYears().divide(new BigDecimal("1e6")));
    }


    /**
     *  Gets the number of millennia. A millennium is 1,000 years. 
     *
     *  @return the number of millennia 
     */

    @OSID @Override
    public final java.math.BigDecimal getMillennia() {
        return (getYears().divide(new BigDecimal("1e3")));
    }


    /**
     *  Gets the number of centuries. 
     *
     *  @return the number of centuries 
     */

    @OSID @Override
    public java.math.BigDecimal getCenturies() {
        return (getYears().divide(new BigDecimal("1e2")));
    }


    /**
     *  Gets the number of scores. 
     *
     *  @return the number of scores 
     */

    @OSID @Override
    public final java.math.BigDecimal getScores() {
        return (getYears().divide(new BigDecimal(20)));
    }


    /**
     *  Gets the number of blue moons. 
     *
     *  @return the number of blue moons 
     */

    @OSID @Override
    public abstract java.math.BigDecimal getBluemoons();


    /**
     *  Gets the number of years. 
     *
     *  @return the number of years 
     */

    @OSID @Override
    public abstract java.math.BigDecimal getYears();


    /**
     *  Gets the number of months. 
     *
     *  @return the number of months 
     */

    @OSID @Override
    public abstract java.math.BigDecimal getMonths();


    /**
     *  Gets the number of weeks. 
     *
     *  @return the number of weeks 
     */

    @OSID @Override
    public abstract java.math.BigDecimal getWeeks();


    /**
     *  Gets the number of days. 
     *
     *  @return the number of days 
     */

    @OSID @Override
    public abstract java.math.BigDecimal getDays();


    /**
     *  Gets the number of hours. 
     *
     *  @return the number of hours 
     */

    @OSID @Override
    public abstract java.math.BigDecimal getHours();


    /**
     *  Gets the number of minutes. 
     *
     *  @return the number of minutes 
     */

    @OSID @Override
    public abstract java.math.BigDecimal getMinutes();


    /**
     *  Gets the number of seconds. 
     *
     *  @return the number of seconds 
     */

    @OSID @Override
    public abstract java.math.BigDecimal getSeconds();


    /**
     *  Gets the number of milliseconds. A millisecond is one
     *  thousandth of a second.
     *
     *  @return the number of milliseconds 
     */

    @OSID @Override
    public final java.math.BigDecimal getMilliseconds() {
        return (getSeconds().multiply(new BigDecimal("1e3")));
    }


    /**
     *  Gets the number of microseconds. A microsecond is one millionth of a 
     *  second. 
     *
     *  @return the number of micrseconds 
     */

    @OSID @Override
    public final java.math.BigDecimal getMicroseconds() {
        return (getSeconds().multiply(new BigDecimal("1e6")));
    }


    /**
     *  Gets the number of nanoseconds. A nanosecond is one billionth of a 
     *  second. 
     *
     *  @return the number of nanoseconds 
     */

    @OSID @Override
    public final java.math.BigDecimal getNanoseconds() {
        return (getSeconds().multiply(new BigDecimal("1e9")));
    }


    /**
     *  Gets the number of picoseconds. A picosecond is one trillionth of a 
     *  second. 
     *
     *  @return the number of picoseconds 
     */

    @OSID @Override
    public final java.math.BigDecimal getPicoseconds() {
        return (getSeconds().multiply(new BigDecimal("1e12")));
    }


    /**
     *  Gets the number of femtoseconds. A femtosecond is one quadrillionth of 
     *  a second. 
     *
     *  @return the number of femtoseconds 
     */

    @OSID @Override
    public final java.math.BigDecimal getFemtoseconds() {
        return (getSeconds().multiply(new BigDecimal("1e15")));
    }


    /**
     *  Gets the number of attoseconds. An attosecond is one quintillionth of 
     *  a second. 
     *
     *  @return the number of attoseconds 
     */

    @OSID @Override
    public final java.math.BigDecimal getAttoseconds() {
        return (getSeconds().multiply(new BigDecimal("1e18")));
    }


    /**
     *  Gets the number of zeptoseconds. A zeptosecond is one sextillionth of 
     *  a second. 
     *
     *  @return the number of zeptoseconds 
     */

    @OSID @Override
    public final java.math.BigDecimal getZeptoseconds() {
        return (getSeconds().multiply(new BigDecimal("1e21")));
    }


    /**
     *  Gets the number of yoctoseconds. A yoctosecond is one septillionth of 
     *  a second. This is getting quite small. 
     *
     *  @return the number of yoctoseconds 
     */

    @OSID @Override
    public final java.math.BigDecimal getYoctoseconds() {
        return (getSeconds().multiply(new BigDecimal("1e24")));
    }


    /**
     *  Gets the number of xoxxoseconds. A xoxxosecond is one octillionth of a 
     *  second. We're going with Rudy Rucker here. 
     *
     *  @return the number of xoxxoseconds 
     */

    @OSID @Override
    public final java.math.BigDecimal getXoxxoseconds() {
        return (getSeconds().multiply(new BigDecimal("1e27")));
    }


    /**
     *  Gets the number of weebleseconds. A weeblesecond is one nonillionth of 
     *  a second. 
     *
     *  @return the number of weebleseconds 
     */

    @OSID @Override
    public final java.math.BigDecimal getWeebleseconds() {
        return (getSeconds().multiply(new BigDecimal("1e30")));
    }


    /**
     *  Gets the number of vatoseconds. A vatosecond is one decillionth of a 
     *  second. 
     *
     *  @return the number of vatoseconds 
     */

    @OSID @Override
    public final java.math.BigDecimal getVatoseconds() {
        return (getSeconds().multiply(new BigDecimal("1e33")));
    }


    /**
     *  Gets the number of undaseconds. An undasecond is one unadecillionth of 
     *  a second. 
     *
     *  @return the number of undaseconds 
     */

    @OSID @Override
    public final java.math.BigDecimal getUndaseconds() {
        return (getSeconds().multiply(new BigDecimal("1e36")));
    }


    /**
     *  Gets the number of Planck sseconds. A Planck is 10 
     *  quattuordecillionths of a second. 
     *
     *  @return the number of planck seconds 
     */

    @OSID @Override
    public abstract java.math.BigDecimal getPlanckSeconds();


    /**
     *  Gets the granularity of this duration. The granularity indicates the 
     *  resolution of the clock. More precision than what is specified in this 
     *  method cannot be inferred from the available data. 
     *
     *  @return the time units 
     */

    @OSID @Override
    public final org.osid.calendaring.DateTimeResolution getGranularity() {
        return (this.granularity);
    }


    /**
     *  If the granularity of the time equals <code> getGranularity(), </code> 
     *  then the multiplier is 1. This method may return a different number 
     *  when the granularity differs from one of the defined resolutions. 
     *
     *  @return granularity multiplier 
     */

    @OSID @Override
    public final long getGranularityMultiplier() {
        return (this.multiplier);
    }


    /**
     *  Tests if uncertainty is defined for this time. 
     *
     *  @return <code> true </code> if uncertainty is defined, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public final boolean definesUncertainty() {
        if (this.uncertaintyUnits == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the units of the uncertainty. 
     *
     *  @return units of the uncertainty 
     *  @throws org.osid.IllegalStateException <code> definesUncertainty() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public final org.osid.calendaring.DateTimeResolution getUncertaintyUnits() {
        if (!definesUncertainty()) {
            throw new org.osid.IllegalStateException("no uncertainty");
        }

	return (this.uncertaintyUnits);
    }


    /**
     *  Gets the uncertainty of this time in the negative direction. 
     *
     *  @return the uncertainty under this value 
     *  @throws org.osid.IllegalStateException <code> definesUncertainty() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public final long getUncertaintyMinus() {
        if (!definesUncertainty()) {
            throw new org.osid.IllegalStateException("no uncertainty");
        }

	return (this.uncertaintyMinus);
    }	


    /**
     *  Gets the uncertainty of this time in the positive direction. 
     *
     *  @return the uncertainty over this value 
     *  @throws org.osid.IllegalStateException <code> definesUncertainty() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public final long getUncertaintyPlus() {
        if (!definesUncertainty()) {
            throw new org.osid.IllegalStateException("no uncertainty");
        }

	return (this.uncertaintyPlus);
    }


    /**
     *	Tests if this Duration is greater than the given Duration. A
     *	greater Duration is one whose values minus uncertainty is
     *	greater than another plus its uncertainty and graularity. An
     *	uncertainty of infinity is undefined and returns false.
     *
     *	@param	duration the duration to compare
     *	@return <code>true</code> if this duration is larger,
     *		<code>false</code> otherwise
     *	@throws org.osid.NullArgumentException <code>duration</code>
     *		is <code>null</code>
     *	@throws org.osid.UnsupportedException cannot compare calendar
     */

    @OSIDBinding @Override
    public final boolean isGreater(org.osid.calendaring.Duration duration) {
        compatibilityCheck(this, duration);

        if (reallyUncertain(this) || reallyUncertain(duration)) {
            return (false);
        }
        
        if (duration.getGranularity() == org.osid.calendaring.DateTimeResolution.INFINITY) {
            return (false);
        }

        if (getGranularity() == org.osid.calendaring.DateTimeResolution.INFINITY) {
            return (true);
        }

        if (getLowerBound().getPlanckSeconds().compareTo(duration.getUpperBound().getPlanckSeconds()) > 0) {
            return (true);
        } else {
            return (false);
        }
    }
        

    /**
     *	Tests if this Duration is less than the given Duration. A
     *	lesser Duration is one whose values plus granularity and
     *	uncertainty is less than another minus its d uncertainty. An
     *	uncertainty of infinity is undefined and returns false.
     *
     *	@param	duration the duration to compare
     *	@return <code>true</code> if this duration is less,
     *		<code>false</code> otherwise
     *	@throws org.osid.NullArgumentException <code>duration</code>
     *		is <code>null</code>
     *	@throws org.osid.UnsupportedException cannot compare calendar
     */

    @OSIDBinding @Override
    public final boolean isLess(org.osid.calendaring.Duration duration) {
        compatibilityCheck(this, duration);

        if (reallyUncertain(this) || reallyUncertain(duration)) {
            return (false);
        }

        if (getGranularity() == org.osid.calendaring.DateTimeResolution.INFINITY) {
            return (false);
        }

        if (duration.getGranularity() == org.osid.calendaring.DateTimeResolution.INFINITY) {
            return (true);
        }

        if (getUpperBound().getPlanckSeconds().compareTo(duration.getLowerBound().getPlanckSeconds()) < 0) {
            return (true);
        } else {
            return (false);
        }
    }


    /**
     *  Tests if the range of this Duration, as specified by its
     *  granularity and uncertainty, includes the given Duration
     *  ranged by its granularity and uncertainty.
     *
     *  If either duration has an uncertainty of infinity this method
     *  returns false.
     *
     *  @param  duration the duration to compare
     *  @return <code>true</code> if this duration includes the given
     *          duration, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException <code>duration</code>
     *          is <code>null</code>
     *  @throws org.osid.UnsupportedException cannot compare calendar
     */

    @OSIDBinding @Override
    public final boolean isInclusive(org.osid.calendaring.Duration duration) {
        compatibilityCheck(this, duration);

        if (reallyUncertain(this) || reallyUncertain(duration)) {
            return (false);
        }

        if (duration.getGranularity() == org.osid.calendaring.DateTimeResolution.INFINITY) {
            return (false);
        }

        if (getGranularity() == org.osid.calendaring.DateTimeResolution.INFINITY) {
            return (true);
        }

        if ((getLowerBound().getPlanckSeconds().compareTo(duration.getLowerBound().getPlanckSeconds()) <= 0) &&
            (getUpperBound().getPlanckSeconds().compareTo(duration.getUpperBound().getPlanckSeconds()) >= 0)) {
            return (true);
        } else {
            return (false);
        }
    }


    /**
     *  Tests if the range of this Duration, as specified by its
     *  granularity and uncertainty, does not overlap the given
     *  Duration.
     *
     *  If either duration has an uncertainty of infinity this method
     *  returns false.
     *
     *  @param  duration the duration to compare
     *  @return <code>true</code> if this duration is excluded,
     *          <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException <code>duration</code>
     *          is <code>null</code>
     *  @throws org.osid.UnsupportedException cannot compare calendar
     */

    @OSIDBinding @Override
    public final boolean isExclusive(org.osid.calendaring.Duration duration) {
        compatibilityCheck(this, duration);

        if (reallyUncertain(this) || reallyUncertain(duration)) {
            return (false);
        }

        if (duration.getGranularity() == org.osid.calendaring.DateTimeResolution.INFINITY) {
            return (false);
        }

        if (getGranularity() == org.osid.calendaring.DateTimeResolution.INFINITY) {
            return (false);
        }

        if ((getUpperBound().getPlanckSeconds().compareTo(duration.getLowerBound().getPlanckSeconds()) < 0) ||
            (getLowerBound().getPlanckSeconds().compareTo(duration.getUpperBound().getPlanckSeconds()) > 0)) {
            return (true);
        } else {
            return (false);
        }
    }


    /**
     *  Compares this duration with the specified
     *  <code>Duration</code> to determine the natural order. Returns
     *  a negative integer, zero, or a positive integer as this
     *  duration is less than, equal to, or greater than the specified
     *  Duration.
     *
     *  The natural ordering is determined first by the natural
     *  ordering of the type, then by its values. This method is not
     *  useful for numeric comparisons. The ranges implied by their
     *  granularities and uncertainties may overlap in such a way that
     *  two unequal Durations may be neither less than or greater than
     *  the other.
     *
     *  If <code>compareTo</code> a duration is zero, then
     *  <code>equals()</code> must be <code>true</code> and their hash
     *  codes must also be equal for consistent behavior. For
     *  orderings that may yield inconsistent behavior, an external
     *  <code>Comparator</code> should be used.
     *
     *  @param duration the duration to be compared
     *  @return a negative integer, zero, or a positive integer as
     *          this duration is less than, equal to, or greater than
     *          the specified duration
     *  @throws ClassCastException if the specified duration's type
     *          prevents it from being compared to this duration
     */

    @OSIDBinding @Override
    public final int compareTo(org.osid.calendaring.Duration duration) {
        if (this == duration) {
            return (0);
        }

        int c = getCalendarType().compareTo(duration.getCalendarType());
        if (c != 0) {
            return (c);
        }

        c = getTimeType().compareTo(duration.getTimeType());
        if (c != 0) {
            return (c);
        }
        
        c = getPlanckSeconds().compareTo(duration.getPlanckSeconds());
        if (c != 0) {
            return (c);
        }

        if (getGranularity().ordinal() > duration.getGranularity().ordinal()) {
            return (1);
        }

        if (getGranularity().ordinal() < duration.getGranularity().ordinal()) {
            return (-1);
        }

        if (!definesUncertainty() && duration.definesUncertainty() && 
            (getUncertaintyPlus() > 0) && (getUncertaintyMinus() > 0)) {
            return (-1);
        }

        if (definesUncertainty() && !duration.definesUncertainty() &&
            (duration.getUncertaintyPlus() > 0) && (duration.getUncertaintyMinus() > 0)) {
            return (1);
        }

        c = getUncertaintyUnits().compareTo(duration.getUncertaintyUnits());
        if (c != 0) {
            return (c);
        }

        if (getUncertaintyPlus() > duration.getUncertaintyPlus()) {
            return (1);
        }

        if (getUncertaintyPlus() < duration.getUncertaintyPlus()) {
            return (-1);
        }

        if (getUncertaintyMinus() > duration.getUncertaintyMinus()) {
            return (1);
        }

        if (getUncertaintyMinus() < duration.getUncertaintyMinus()) {
            return (-1);
        }

        return (0);
    }


    /**
     *  Determines if the given <code> Duration </code> is equal to
     *  this one. Two Durations are equal if their Types,
     *  granularities, values, and uncertainties are equal.
     *
     *  @param  obj an object to compare
     *  @return <code> true </code> if the given object is equal to
     *          this <code>Duration</code>, <code> false </code>
     *          otherwise
     */

    @OSIDBinding @Override
    public final boolean equals(Object obj) {
        if (obj == null) {
            return (false);
        }

        if (this == obj) {
            return (true);
        }

        if (!(obj instanceof org.osid.calendaring.Duration)) {
            return (false);
        }

        org.osid.calendaring.Duration duration = (org.osid.calendaring.Duration) obj;
        if (!getCalendarType().equals(duration.getCalendarType())) {
            return (false);
        }

        if (!getTimeType().equals(duration.getTimeType())) {
            return (false);
        }

        if (getGranularity() != duration.getGranularity()) {
            return (false);
        }

        /* if uncertainty is defined, but is zero, it doesn't count */
        if ((definesUncertainty() && ((getUncertaintyPlus() > 0) ||
                                      getUncertaintyMinus() > 0)) != 
            (duration.definesUncertainty() && ((duration.getUncertaintyPlus() > 0) ||
                                               duration.getUncertaintyMinus() > 0))) {
            return (false);
        } else if (definesUncertainty() && duration.definesUncertainty()) {
            if (!getUncertaintyUnits().equals(duration.getUncertaintyUnits())) {
                return (false);
            }

            if (getUncertaintyPlus() != duration.getUncertaintyPlus()) {
                return (false);
            }

            if (getUncertaintyMinus() != duration.getUncertaintyMinus()) {
                return (false);
            }
        }

        if (!getPlanckSeconds().equals(duration.getPlanckSeconds())) {
            return (false);
        }

        if (!getPlanckSeconds().equals(duration.getPlanckSeconds())) {
            return (false);
        }

        return (true);
    }


    /**
     *  Returns a hash code value for this <code>Duration</code> based
     *  on the types, values, granularity, and uncertainties.
     *
     *  @return a hash code value for this object
     */

    @OSIDBinding @Override
    public final int hashCode() {
        if (this.hash == 0) {
            this.hash = calculateHashCode();
        }

        return (this.hash);
    }


    private int calculateHashCode() {
        int hash = getCalendarType().hashCode();
        hash = hash * 31 + getTimeType().hashCode();        

        hash = hash * 31 + getGranularity().hashCode();
        hash = hash * 31 + (int) getGranularityMultiplier();

        if (definesUncertainty()) {
            hash = hash * 31 + getUncertaintyUnits().hashCode();
            hash = hash * 31 + (int) getUncertaintyPlus();
            hash = hash * 31 + (int) getUncertaintyMinus();
        }

        hash = hash * 31 + getLowerBound().getPlanckSeconds().hashCode();
        hash = hash * 31 + getUpperBound().getPlanckSeconds().hashCode();

        return (hash);
    }


    private static void compatibilityCheck(org.osid.calendaring.Duration one,
                                           org.osid.calendaring.Duration two) {

        if (!one.getCalendarType().equals(two.getCalendarType())) {
            throw new org.osid.UnsupportedException (two.getCalendarType() + " not supported");
        }

        if (!one.getTimeType().equals(two.getTimeType())) {
            throw new org.osid.UnsupportedException (two.getTimeType() + " not supported");
        }

        return;
    }


    private static boolean reallyUncertain(org.osid.calendaring.Duration duration) {
        if (duration.definesUncertainty() && 
            (duration.getUncertaintyUnits() == org.osid.calendaring.DateTimeResolution.INFINITY)) {
            return (true);
        } else {
            return (false);
        }
    }
}
