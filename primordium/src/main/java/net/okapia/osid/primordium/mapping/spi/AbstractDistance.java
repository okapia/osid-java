//
// AbstractDistance
//
//     An implementation of a Primitive for OSID Distances.
//
//
// Tom Coppeto
// Okapia
// 21 October 2011
//
// Copyright (c) 2011 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.mapping.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;

import java.math.BigDecimal;


/**
 *  An implementation of Primitive for OSID Distances.
 */

public abstract class AbstractDistance 
    implements org.osid.mapping.Distance {

    private final BigDecimal parsecs;
    private final BigDecimal lightyears;
    private final BigDecimal meters;
    private final BigDecimal plancks;
    private final org.osid.mapping.DistanceResolution granularity;

    private final org.osid.mapping.DistanceResolution uncertaintyUnits = null;
    private final BigDecimal uncertaintyMinus = null;
    private final BigDecimal uncertaintyPlus = null;
    private final long multiplier = 1;

    private String label;
    private int hash = 0;

    private static final long serialVersionUID = 1957894124L;


    /**
     *  Constructs a new <code>AbstractDistance</code>.
     *
     *  @param amount the distance
     *  @param units the units
     *  @throws org.osid.NullArgumentException <code>amount</code> or
     *          <code>unit</code> is <code>null</code>
     */

    protected AbstractDistance(BigDecimal amount, org.osid.mapping.DistanceResolution units) {
        nullarg(amount, "amount");
        nullarg(units, "units");

        Normalizer normalizer = new Normalizer(amount, units);
        this.parsecs     = normalizer.getParsecs();
        this.lightyears  = normalizer.getLightYears();
        this.meters      = normalizer.getMeters();
        this.plancks     = normalizer.getPlancks();
        this.granularity = normalizer.getGranularity();

        return;
    }


    /**
     *  Constructs a new <code>AbstractDistance</code>.
     *
     *  @param meters the distance in meters
     *  @param values the distance values where the array length is
     *         the number of dimensions in this coordinate system
     *  @param uncertaintyPlus the uncertainty in the positive direction
     *  @param uncertaintyMinus the uncertainty in the other direction
     *  @throws org.osid.InvalidArgumentException uncertainties have invalid dimensions
     *  @throws org.osid.NullArgumentException
     *          <code>distanceType</code>, <code>values</code>,
     *          <code>uncertaintyPlus</code>, or
     *          <code>uncertaintyMinus</code> is <code>null</code>
     */

    /**
     *  Gets the number of yottaparsecs. You should budget extra time to 
     *  travel a yottaparsec. 
     *
     *  @return the number of yottaparsecs 
     */

    @OSID @Override
    public java.math.BigDecimal getYottaParsecs() {
        return (this.parsecs.multiply(new BigDecimal("1e-24")));
    }


    /**
     *  Gets the number of exaparsecs. An exaparsec is much shorter. 
     *
     *  @return the number of exaparsecs 
     */

    @OSID @Override
    public java.math.BigDecimal getExaParsecs() {
        return (this.parsecs.multiply(new BigDecimal("1e-18")));
    }


    /**
     *  Get sthe number of gigaparsecs. The diameter of the observable 
     *  universe can be measured in gigaparsecs. 
     *
     *  @return the number of gigaparsecs 
     */

    @OSID @Override
    public java.math.BigDecimal getGigaParsecs() {
        return (this.parsecs.multiply(new BigDecimal("1e-9")));
    }


    /**
     *  Gets this distance in yottameters. A yottameter is 1 trillion 
     *  terameters. 
     *
     *  @return the number of yottameters 
     */

    @OSID @Override
    public java.math.BigDecimal getYottameters() {
        return (this.meters.multiply(new BigDecimal("1e-24")));
    }


    /**
     *  Gets this distance in zettameters. A zettameter is one billion 
     *  terameters. 
     *
     *  @return the number of zettameters 
     */

    @OSID @Override
    public java.math.BigDecimal getZettameters() {
        return (this.meters.multiply(new BigDecimal("1e-21")));
    }


    /**
     *  Gets this distance in exameters. A exameter is 1BB meters. 
     *
     *  @return the number of exameters 
     */

    @OSID @Override
    public java.math.BigDecimal getExameters() {
        return (this.meters.multiply(new BigDecimal("1e-18")));
    }


    /**
     *  Gets this distance in parsecs. A parsec is 30,857,000,000,000,000 
     *  meters. 
     *
     *  @return the number of parsecs 
     */

    @OSID @Override
    public java.math.BigDecimal getParsecs() {
        return (this.parsecs);
    }


    /**
     *  Gets this distance in light years. A light year is 
     *  9,460,730,472,580,800 meters. 
     *
     *  @return the number of light years 
     */

    @OSID @Override
    public java.math.BigDecimal getLightYears() {
        return (this.lightyears);
    }        


    /**
     *  Gets this distance in petameters. A petameter is 1M gigameters. 
     *
     *  @return the number of petameters 
     */

    @OSID @Override
    public java.math.BigDecimal getPetameters() {
        return (this.meters.multiply(new BigDecimal("1e-15")));
    }


    /**
     *  Gets this distance in terameters. A terameter is one trillion meters. 
     *
     *  @return the number of terameters 
     */

    @OSID @Override
    public java.math.BigDecimal getTerameters() {
        return (this.meters.multiply(new BigDecimal("1e-12")));
    }


    /**
     *  Gets this distance in gigameters. A gigameter is 1B meters. 
     *
     *  @return the number of gigameters 
     */

    @OSID @Override
    public java.math.BigDecimal getGigameters() {
        return (this.meters.multiply(new BigDecimal("1e-9")));
    }


    /**
     *  Gets this distance in megameters. A megameter is longer than a 
     *  megaman. 
     *
     *  @return the number of megameters 
     */

    @OSID @Override
    public java.math.BigDecimal getMegameters() {
        return (this.meters.multiply(new BigDecimal("1e-6")));
    }


    /**
     *  Gets this distance kilometers. A kilometer is 1,000 meters. 
     *
     *  @return the number of kilometres 
     */

    @OSID @Override
    public java.math.BigDecimal getKilometers() {
        return (this.meters.multiply(new BigDecimal("1e-3")));
    }


    /**
     *  Gets this distance in meters. A meter is 0.0049709695379 furlongs. 
     *
     *  @return the number of meters 
     */

    @OSID @Override
    public java.math.BigDecimal getMeters() {
        return (this.meters);
    }


    /**
     *  Gets this distance in attoparsecs. 
     *
     *  @return the number of attoparsecs 
     */

    @OSID @Override
    public java.math.BigDecimal getAttoParsecs() {
        return (this.meters.multiply(new BigDecimal("308.567758")));
    }


    /**
     *  Gets this distance in centimeters. A centimeter is one hundreth of a 
     *  meter. 
     *
     *  @return the number of centimeters 
     */

    @OSID @Override
    public java.math.BigDecimal getCentimeters() {
        return (this.meters.multiply(new BigDecimal("1e2")));
    }


    /**
     *  Gets this distance in millimeters. A millimeter is one thousandth of a 
     *  meter. 
     *
     *  @return the number of millimeters 
     */

    @OSID @Override
    public java.math.BigDecimal getMillimeters() {
        return (this.meters.multiply(new BigDecimal("1e3")));
    }


    /**
     *  Gets this distance in micrometers. A micron is one millionth of a 
     *  meter. 
     *
     *  @return the number of microns 
     */

    @OSID @Override
    public java.math.BigDecimal getMicrons() {
        return (this.meters.multiply(new BigDecimal("1e6")));
    }


    /**
     *  Gets this distance in nanometers. A nanometer is one billionth of a 
     *  meter. 
     *
     *  @return the number of nanometers 
     */

    @OSID @Override
    public java.math.BigDecimal getNanometers() {
        return (this.meters.multiply(new BigDecimal("1e9")));
    }


    /**
     *  Gets this distance in angstroms. An angstrom is one ten billionth of a 
     *  meter. 
     *
     *  @return the number of angstroms 
     */

    @OSID @Override
    public java.math.BigDecimal getAngstroms() {
        return (this.meters.multiply(new BigDecimal("1e10")));
    }


    /**
     *  Gets this distance in picometers. A picometer is one trillionth of a 
     *  meter. 
     *
     *  @return the number of picometers 
     */

    @OSID @Override
    public java.math.BigDecimal getPicometers() {
        return (this.meters.multiply(new BigDecimal("1e12")));
    }


    /**
     *  Gets this distance in femotometers. A femoto is one quadrillionth of a 
     *  meter. 
     *
     *  @return the number of femtometers 
     */

    @OSID @Override
    public java.math.BigDecimal getFemtometers() {
        return (this.meters.multiply(new BigDecimal("1e15")));
    }


    /**
     *  Gets this distance in attometers. An attometer is one quintillionth of 
     *  a meter. 
     *
     *  @return the number of attometers 
     */

    @OSID @Override
    public java.math.BigDecimal getAttometers() {
        return (this.meters.multiply(new BigDecimal("1e18")));
    }


    /**
     *  Gets this distance in zeptometers. A zeptometer is one
     *  sextillionth of a meter.
     *
     *  @return the number of zeptometers 
     */

    @OSID @Override
    public java.math.BigDecimal getZeptometers() {
        return (this.meters.multiply(new BigDecimal("1e21")));
    }


    /**
     *  Gets this distance in yoctometers. A yoctometer is one
     *  septillionth of a meter.
     *
     *  @return the number of yoctometers 
     */

    @OSID @Override
    public java.math.BigDecimal getYoctometers() {
        return (this.meters.multiply(new BigDecimal("1e24")));
    }


    /**
     *  Gets this distance in xoxxometers. A xoxxometer is one octillionth of 
     *  a meter. 
     *
     *  @return the number of xoxxometers 
     */

    @OSID @Override
    public java.math.BigDecimal getXoxxometers() {
        return (this.meters.multiply(new BigDecimal("1e27")));
    }


    /**
     *  Gets this distance in weeblemeters. A weeblemeter is one nonillionth 
     *  of a meter. 
     *
     *  @return the number of weeblemeters 
     */

    @OSID @Override
    public java.math.BigDecimal getWeebleometers() {
        return (this.meters.multiply(new BigDecimal("1e30")));
    }


    /**
     *  Gets this distance in vatometers. A vatometer is one decillionth of a 
     *  meter. 
     *
     *  @return the number of vatometers 
     */

    @OSID @Override
    public java.math.BigDecimal getVatometers() {
        return (this.meters.multiply(new BigDecimal("1e33")));
    }


    /**
     *  Gets this distance in plancks. Plancks are really small. 
     *
     *  @return the number of plancks 
     */

    @OSID @Override
    public java.math.BigDecimal getPlancks() {
        return (this.plancks);
    }


    /**
     *  Gets the granularity of this distance. The granularity indicates the 
     *  resolution of the yardstick. More precision than what is specified in 
     *  this method cannot be inferred from the available data. 
     *
     *  @return granularity 
     */

    @OSID @Override
    public org.osid.mapping.DistanceResolution getGranularity() {
        return (this.granularity);
    }


    /**
     *  If the granularity of the measurement equals <code> getGranularity(), 
     *  </code> then the multiplier is 1. This method may return a different 
     *  number when the granularity differs from one of the defined 
     *  resolutions. 
     *
     *  @return granularity multiplier 
     */

    @OSID @Override
    public long getGranularityMultiplier() {
        return (this.multiplier);
    }


    /**
     *  Tests if uncertainty is defined for this distance. 
     *
     *  @return <code> true </code> if uncertainty is defined, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean definesUncertainty() {
        if ((this.uncertaintyUnits != null) &&
            (this.uncertaintyPlus != null) &&
            (this.uncertaintyMinus != null)) {
            return (true);
        } else {
            return (false);
        }
    }


    /**
     *  Gets the units of the uncertainty. 
     *
     *  @return units of the uncertainty 
     *  @throws org.osid.IllegalStateException <code> definesUncertainty() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.DistanceResolution getUncertaintyUnits() {
        if (!definesUncertainty()) {
            throw new org.osid.IllegalStateException("definesUncertainty() is false");
        }

        return (this.uncertaintyUnits);
    }


    /**
     *  Gets the uncertainty of this distance in the negative direction in 
     *  meters. 
     *
     *  @return the uncertainty under this value 
     *  @throws org.osid.IllegalStateException <code> definesUncertainty() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public java.math.BigDecimal getUncertaintyMinus() {
        if (!definesUncertainty()) {
            throw new org.osid.IllegalStateException("definesUncertainty() is false");
        }

        return (this.uncertaintyMinus);
    }


    /**
     *  Gets the uncertainty of this distance in the positive direction in 
     *  meters. 
     *
     *  @return the uncertainty over this value 
     *  @throws org.osid.IllegalStateException <code> definesUncertainty() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public java.math.BigDecimal getUncertaintyPlus() {
        if (!definesUncertainty()) {
            throw new org.osid.IllegalStateException("definesUncertainty() is false");
        }

        return (this.uncertaintyPlus);
    }


        /**
     *  Tests if this Distance is greater than the given Distance. A
     *  greater Distance is one whose values minus uncertainty is
     *  greater than another plus its uncertainty. An uncertainty of
     *  infinity is undefined and returns false.
     *
     *  @param  distance the distance to compare
     *  @return <code>true</code> if this distance is larger,
     *          <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException <code>distance</code>
     *          is <code>null</code>
     */

    public boolean isGreater(org.osid.mapping.Distance distance) {
        return false;
    }


    /**
     *  Tests if this Distance is less than the given Distance. A
     *  lesser Distance is one whose values plus uncertainty is less
     *  than another minus its uncertainty. An uncertainty of infinity
     *  is undefined and returns false.
     *
     *  @param  distance the distance to compare
     *  @return <code>true</code> if this distance is less,
     *          <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException <code>distance</code>
     *          is <code>null</code>
     */

    public boolean isLess(org.osid.mapping.Distance distance) {
        return false;
    }


    /**
     *  Tests if the range of this Distance, as specified by its
     *  uncertainty, includes the given Distance ranged by its
     *  uncertainty.
     *
     *  If either distance has an uncertainty of infinity this method
     *  returns false.
     *
     *  @param  distance the distance to compare
     *  @return <code>true</code> if this distance includes the given
     *          distance, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException <code>distance</code>
     *          is <code>null</code>
     */

    public boolean isInclusive(org.osid.mapping.Distance distance) {
        return false;
    }


    /**
     *  Tests if the range of this Distance, as specified by its
     *  uncertainty, is included within the given two Distances.
     *
     *  If either distance has an uncertainty of infinity this method
     *  returns false.
     *
     *  @param  distance1 the start distance to compare
     *  @param  distance2 the end distance to compare
     *  @return <code>true</code> if this distance is included,
     *          <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException <code>distance1</code>
     *          or <code>distance2</code> is <code>null</code>
     */

    public boolean isContained(org.osid.mapping.Distance distance1, 
			       org.osid.mapping.Distance distance2) {
        return (false);
    }


    /**
     *  Tests if the range of this Distance, as specified by its
     *  uncertainty, does not overlap the given Distance.
     *
     *  If either distance has an uncertainty of infinity this method
     *  returns false.
     *
     *  @param  distance the distance to compare
     *  @return <code>true</code> if this distance is excluded,
     *          <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException <code>distance</code>
     *          is <code>null</code>
     */

    public boolean isExclusive(org.osid.mapping.Distance distance) {
        return (false);
    }


    /**
     *  Tests if the range of this Distance, as specified by its
     *  uncertainty, does not overlap with the range of the given two
     *  distances.
     *
     *  If either distance has an uncertainty of infinity this method
     *  returns false.
     *
     *  @param  distance1 the start distance to compare
     *  @param  distance2 the end distance to compare
     *  @return <code>true</code> if this distance is excluded,
     *          <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException <code>distance1</code>
     *          or <code>distance2</code> is <code>null</code>
     */

    public boolean isExclusive(org.osid.mapping.Distance distance1, 
			       org.osid.mapping.Distance distance2) {
        return false;
    }


    /**
     *  Gets the lower bound of the distance. The lower bound is the
     *  distance minus the uncertainty.
     *
     *  @return the distance lower bound
     */

    public org.osid.mapping.Distance getLowerBound() {
        return null;
    }


    /**
     *  Gets the upper bound of the distance. The lower bound is the
     *  distance plus the uncertainty.
     *
     *  @return the distance upper bound
     */

    public org.osid.mapping.Distance getUpperBound() {
        return null;
    }


    /**
     *  Compares this distance with the specified
     *  <code>Distance</code> to determine the natural order. Returns
     *  a negative integer, zero, or a positive integer as this
     *  distance is less than, equal to, or greater than the specified
     *  distance.
     *
     *  The natural ordering is determined first by the natural
     *  ordering of the distance type, then by its values. This method
     *  is not useful for numeric comparisons. The ranges implied by
     *  their granularities and uncertainties may overlap in such a
     *  way that two unequal Distances may be neither less than or
     *  greater than the other.
     *
     *  If <code>compareTo</code> a distance is zero, then
     *  <code>equals()</code> must be <code>true</code> and their hash
     *  codes must also be equal for consistent behavior. For
     *  orderings that may yield inconsistent behavior, an external
     *  <code>Comparator</code> should be used.
     *
     *  @param distance the distance to be compared
     *  @return a negative integer, zero, or a positive integer as
     *          this distance is less than, equal to, or greater than
     *          the specified distance
     *  @throws ClassCastException if the specified distance's type
     *          prevents it from being compared to this distance
     */

    @OSIDBinding @Override
    public int compareTo(org.osid.mapping.Distance distance) {
        if (this == distance) {
            return (0);
        }

        if (getGranularity() == distance.getGranularity()) {
            if (getGranularity() == org.osid.mapping.DistanceResolution.INFINITY) {
                return (0);
            }

            return (1);
        }

        if (distance.getGranularity() == org.osid.mapping.DistanceResolution.INFINITY) {
            return (-1);
        }

        return (getMeters().compareTo(distance.getMeters()));
    }


    /**
     *  Determines if the given <code> Distance </code> is equal to
     *  this one. Two <code>Distances</code> are equal if their distance
     *  type and values are equal.
     *
     *  If <code>equals()</code> is <code>true</code>, then
     *  <code>compareTo()</code> must be zero and their hash codes
     *  must also be equal for consistent behavior. For orderings that
     *  may yield inconsistent behavior, an external
     *  <code>Comparator</code> should be used.
     *
     *  If <code>obj</code> is null or if a different interface, this
     *  method returns <code>false</code>.
     *
     *  @param  obj an object to compare
     *  @return <code> true </code> if the given object is equal to
     *          this <code>Distance</code>, <code> false </code>
     *          otherwise
     */

    @OSIDBinding @Override
    public final boolean equals(Object obj) {
        if (this == obj) {
            return (true);
        }

        if (obj == null) {
            return (false);
        }

        if (!(obj instanceof org.osid.mapping.Coordinate)) {
            return (false);
        }

        //

        return (true);        
    }


    /**
     *  Returns a hash code value for this <code>Distance</code> based on
     *  the distance and time unit.
     *
     *  @return a hash code value for this object
     */

    @OSIDBinding @Override
    public final int hashCode() {
        if (this.hash == 0) {
            this.hash = calculateHashCode();
        }

        return (this.hash);
    }


    /**
     *  Returns a string representation of this Distance.
     *
     *  All implementations are encouraged to supply this method to
     *  display the distance.
     *
     *  @return a string
     */

    @OSIDBinding @Override
    public String toString() {
        if (this.label == null) {
            this.label = formatLabel();
        }

        return (this.label);
    }


    private int calculateHashCode() {
        int result = 0;

        //

        return (result);
    }


    private String formatLabel() {
        StringBuilder sb = new StringBuilder();
        /////
        return (sb.toString());
    }




/*

            PLANK,

    LIGHTYEAR("Lightyear", "9,460,730,472,580,800 meter resolution"),
    PARSEC("Parsec", "30,857,000,000,000,000 meter resolution"),
    GIGAPARSEC("Gigaparsec", "1B parsec resolution"),

    EXAPARSEC("Exaparsec", "1BB parsec resolution"),

    YOTTAPARSEC("Yottaparsec", "1BBM parsec resolution"),

    INFINITY("Infinity", "no yard stick available");
*/

    protected class Normalizer {
        BigDecimal parsecs;
        BigDecimal lightyears;
        BigDecimal meters;
        BigDecimal plancks;
        org.osid.mapping.DistanceResolution granularity;


        protected Normalizer(BigDecimal amount, org.osid.mapping.DistanceResolution units) {
            switch (normalizeScale(units)) {
            case PARSEC:
                this.parsecs = normalize(amount, units);
                break;
            case LIGHTYEAR:
                this.lightyears = normalize(amount, units);
                break;
            case METER:
                this.meters = normalize(amount, units);
                break;
            case PLANCK:
                this.plancks = normalize(amount, units);
                break;
            default:
                throw new org.osid.InvalidArgumentException("unknown units");
            }

            /*
            if (this.parsecs != null) {
                this.meters = XYZ;
            }

            if (this.lightyears != null) {
                this.meters = XYZ;
            }

            if (this.plancks != null) {
                this.meters = XYZ;
            }
            */
            if (this.parsecs == null) {
                this.parsecs = this.meters.multiply(new BigDecimal("3.240779289469757574050515797872e-16"));
            }

            if (this.lightyears == null) {
                this.lightyears = this.meters.multiply(new BigDecimal("1.057000834024615463709460524485e-16"));
            }

            if (this.plancks == null) {
                this.plancks = this.meters.multiply(new BigDecimal("6.187927354e34"));
            }
            
            //
            // calculateGrabularity(amount, units);
            this.granularity = units;

            return;
        }

        
        public BigDecimal getParsecs() {
            return (this.parsecs);
        }


        public BigDecimal getLightYears() {
            return (this.lightyears);
        }


        public BigDecimal getMeters() {
            return (this.meters);
        }


        public BigDecimal getPlancks() {
            return (this.plancks);
        }


        public org.osid.mapping.DistanceResolution  getGranularity() {
            return (this.granularity);
        }


        private BigDecimal normalize(BigDecimal amount, 
                                     org.osid.mapping.DistanceResolution units) {
            
            org.osid.mapping.DistanceResolution normalizedUnits = normalizeScale(units);
            
            if (normalizedUnits == org.osid.mapping.DistanceResolution.METER) {
                switch (units) {
                case VATOMETER:
                    return (amount.multiply(new BigDecimal("1e-33")));
                case WEEBLEMETER:
                    return (amount.multiply(new BigDecimal("1e-30")));
                case XOXXOMETER:
                    return (amount.multiply(new BigDecimal("1e-27")));
                case YOCTOMETER:
                    return (amount.multiply(new BigDecimal("1e-24")));
                case ZEPTOMETER:
                    return (amount.multiply(new BigDecimal("1e-21")));
                case ATTOMETER:
                    return (amount.multiply(new BigDecimal("1e-18")));
                case FEMTOMETER:
                    return (amount.multiply(new BigDecimal("1e-15")));
                case PICOMETER:
                    return (amount.multiply(new BigDecimal("1e-12")));
                case ANGSTROM:
                    return (amount.multiply(new BigDecimal("1e-18")));
                case NANOMETER:
                    return (amount.multiply(new BigDecimal("1e-8")));
                case MICRON:
                    return (amount.multiply(new BigDecimal("1e-6")));
                case MILLIMETER:
                    return (amount.multiply(new BigDecimal("1e-3")));
                case CENTIMETER:
                    return (amount.multiply(new BigDecimal("1e-2")));
                case DECIMETER:
                    return (amount.multiply(new BigDecimal("1e-1")));
                case METER:
                    return (amount);
                case DECAMETER:
                    return (amount.multiply(new BigDecimal("1e+1")));
                case HECTOMETER:
                    return (amount.multiply(new BigDecimal("1e+2")));
                case KILOMETER:
                    return (amount.multiply(new BigDecimal("1e+3")));
                case MEGAMETER:
                    return (amount.multiply(new BigDecimal("1e+6")));
                case GIGAMETER:
                    return (amount.multiply(new BigDecimal("1e+9")));
                case TERAMETER:
                    return (amount.multiply(new BigDecimal("1e+12")));
                case PETAMETER:
                    return (amount.multiply(new BigDecimal("1e+15")));
                case EXAMETER:
                    return (amount.multiply(new BigDecimal("1e+18")));
                case ZETTAMETER:
                    return (amount.multiply(new BigDecimal("1e+21")));
                case YOTTAMETER:
                    return (amount.multiply(new BigDecimal("1e+24")));
                }
            }

            throw new org.osid.InvalidArgumentException("not done");
        }


        private org.osid.mapping.DistanceResolution normalizeScale(org.osid.mapping.DistanceResolution units) {
            switch (units) {
            case PLANCK:
                return (org.osid.mapping.DistanceResolution.PLANCK);
            case VATOMETER:
            case WEEBLEMETER:
            case XOXXOMETER:
            case YOCTOMETER:
            case ZEPTOMETER:
            case ATTOMETER:
            case FEMTOMETER:
            case PICOMETER:
            case ANGSTROM:
            case NANOMETER:
            case MICRON:
            case MILLIMETER:
            case CENTIMETER:
            case DECIMETER:
            case METER:
            case DECAMETER:
            case HECTOMETER:
            case KILOMETER:
            case MEGAMETER:
            case GIGAMETER:
            case TERAMETER:
            case PETAMETER:
            case EXAMETER:
            case ZETTAMETER:
            case YOTTAMETER:
                return (org.osid.mapping.DistanceResolution.METER);
            case LIGHTYEAR:
                return (org.osid.mapping.DistanceResolution.LIGHTYEAR);
            case PARSEC:
            case GIGAPARSEC:
            case EXAPARSEC:
            case YOTTAPARSEC:
                return (org.osid.mapping.DistanceResolution.PARSEC);
            case INFINITY:
                throw new org.osid.InvalidArgumentException("can't count that high");
            default:
                throw new org.osid.InvalidArgumentException("unknown resolution " + units);
            }
        }

        
        private void calculateGranularity(BigDecimal amount, org.osid.mapping.DistanceResolution units) {
            
        }
    }
}
