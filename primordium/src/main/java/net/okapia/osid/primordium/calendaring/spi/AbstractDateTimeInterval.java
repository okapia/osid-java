//
// AbstractDateTimeInterval.java
//
//     An abstract DateTimeInterval.
//
//
// Tom Coppeto
// Okapia
// 2 May 2012
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.calendaring.spi;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract DateTimeInterval.
 */

public abstract class AbstractDateTimeInterval
    implements org.osid.calendaring.DateTimeInterval {

    private final org.osid.calendaring.DateTime start;
    private final org.osid.calendaring.DateTime end;

    
    /**
     *  Constructs a new <code>AbstractDateTimeInterval</code>.
     *
     *  @param start the starting date
     *  @param end the ending date
     *  @throws org.osid.InvalidArgumentException <code>start</code>
     *          is greater than <code>end</code> or types do not
     *          match
     *  @throws org.osid.NullArgumentException <code>start</code> or
     *          <code>end</code> is <code>null</code>
     */

    protected AbstractDateTimeInterval(org.osid.calendaring.DateTime start,
                                       org.osid.calendaring.DateTime end) {

        nullarg(start, "start date");
        nullarg(end, "end date");

        /* break dependency
          if (start.isGreater(end)) {
            throw new org.osid.InvalidArgumentException("start is greater than end");
        }
        */

        if (!(start.getCalendarType().equals(end.getCalendarType()))) {
            throw new org.osid.InvalidArgumentException("calendar type mismatch");
        }

        if (!(start.getTimeType().equals(end.getTimeType()))) {
            throw new org.osid.InvalidArgumentException("time type mismatch");
        }

        this.start = start;
        this.end   = end;
        
        return;
    }


    /**
     *  Gets the starting time for this interval. 
     *
     *  @return the starting time 
     */

    public final org.osid.calendaring.DateTime getStart() {
        return (this.start);
    }


    /**
     *  Gets the ending time for this interval. The ending time is greater 
     *  than or equal to the starting time. 
     *
     *  @return the ending time 
     */

    public final org.osid.calendaring.DateTime getEnd() {
        return (this.end);
    }
}
