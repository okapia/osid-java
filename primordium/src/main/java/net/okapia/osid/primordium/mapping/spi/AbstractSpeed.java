//
// AbstractSpeed.java
//
//     An implementation of Primitive for OSID Speeds.
//
//
// Tom Coppeto
// Okapia
// 21 October 2011
//
// Copyright (c) 2011 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.mapping.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;

/**
 *  An implementation of Primitive for OSID Speeds.
 */

public abstract class AbstractSpeed 
    implements org.osid.mapping.Speed {

    private final org.osid.mapping.Distance distance;
    private final org.osid.calendaring.DateTimeResolution time;
    private String label;
    private int hash = 0;

    private static final long serialVersionUID = -1276063824L;


    /**
     *  Constructs a new <code>AbstractSpeed</code>.
     *
     *  @param distance the distance traveled
     *  @param time per unit time
     *  @throws org.osid.NullArgumentException <code>distance</code>
     *          or <code>time</code> is <code>null</code>
     */

    protected AbstractSpeed(org.osid.mapping.Distance distance, org.osid.calendaring.DateTimeResolution time) {
        if (distance == null) {
            throw new org.osid.NullArgumentException("distance is null");
        }

        if (time == null) {
            throw new org.osid.NullArgumentException("time is null");
        }

        this.distance = distance;
        this.time = time;

        return;
    }


    /**
     *  Gets the distance. 
     *
     *  @return the distance 
     */

    @OSID @Override
    public final org.osid.mapping.Distance getDistance() {
        return (this.distance);
    }


    /**
     *  Gets the time unit. 
     *
     *  @return the time unit 
     */

    @OSID @Override
    public final org.osid.calendaring.DateTimeResolution getTimeUnit() {
        return (this.time);
    }


    /**
     *  Tests if this Speed is greater than the given Speed. A
     *  greater Speed is one whose values minus uncertainty is
     *  greater than another plus its uncertainty. An uncertainty of
     *  infinity is undefined and returns false.
     *
     *  @param  speed the speed to compare
     *  @return <code>true</code> if this speed is larger,
     *          <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException <code>speed</code>
     *          is <code>null</code>
     *  @throws org.osid.UnsupportedException cannot compare speed
     */

    @OSIDBinding @Override
    public boolean isGreater(org.osid.mapping.Speed speed) {
        return (false);
    }


    /**
     *  Tests if this Speed is less than the given Speed. A
     *  lesser Speed is one whose values plus uncertainty is less
     *  than another minus its uncertainty. An uncertainty of infinity
     *  is undefined and returns false.
     *
     *  @param  speed the speed to compare
     *  @return <code>true</code> if this speed is less,
     *          <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException <code>speed</code>
     *          is <code>null</code>
     *  @throws org.osid.UnsupportedException cannot compare speed
     */

    @OSIDBinding @Override
    public boolean isLess(org.osid.mapping.Speed speed) {
        return (false);
    }


    /**
     *  Tests if the range of this Speed, as specified by its
     *  uncertainty, includes the given Speed ranged by its
     *  uncertainty.
     *
     *  If either speed has an uncertainty of infinity this method
     *  returns false.
     *
     *  @param  speed the speed to compare
     *  @return <code>true</code> if this speed includes the given
     *          speed, <code>false</code> otherwise
     * @throws org.osid.NullArgumentException <code>speed</code>
     *          is <code>null</code>
     */

    @OSIDBinding @Override
    public boolean isInclusive(org.osid.mapping.Speed speed) {
        return (false);
    }


    /**
     *  Tests if the range of this Speed, as specified by its
     *  uncertainty, is included within the given two Speeds.
     *
     *  If either speed has an uncertainty of infinity this method
     *  returns false.
     *
     *  @param  speed1 the start speed to compare
     *  @param  speed2 the end speed to compare
     *  @return <code>true</code> if this speed is included,
     *          <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException <code>speed1</code>
     *          or <code>speed2</code> is <code>null</code>
     */

    @OSIDBinding @Override
    public boolean isContained(org.osid.mapping.Speed speed1,
                               org.osid.mapping.Speed speed2) {
        return (false);
    }


    /**
     *  Tests if the range of this Speed, as specified by its
     *  uncertainty, does not overlap the given Speed.
     *
     *  If either speed has an uncertainty of infinity this method
     *  returns false.
     *
     *  @param  speed the speed to compare
     *  @return <code>true</code> if this speed is excluded,
     *          <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException <code>speed</code>
     *          is <code>null</code>
     */

    @OSIDBinding @Override
    public boolean isExclusive(org.osid.mapping.Speed speed) {
        return (false);
    }


    /**
     *  Tests if the range of this Speed, as specified by its
     *  uncertainty, does not overlap with the range of the given two
     *  speeds.
     *
     *  If either speed has an uncertainty of infinity this method
     *  returns false.
     *
     *  @param  speed1 the start speed to compare
     *  @param  speed2 the end speed to compare
     *  @return <code>true</code> if this speed is excluded,
     *          <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException <code>speed1</code>
     *          or <code>speed2</code> is <code>null</code>
     */

    @OSIDBinding @Override
    public boolean isExclusive(org.osid.mapping.Speed speed1,
                               org.osid.mapping.Speed speed2) {
        return (false);
    }


    /**
     *  Gets the lower bound of the speed. The lower bound is the
     *  speed minus the uncertainty.
     *
     *  @return the speed lower bound
     */

    @OSIDBinding @Override
    public org.osid.mapping.Speed getLowerBound() {
        return (null);
    }


    /**
     *  Gets the upper bound of the speed. The lower bound is the
     *  speed plus the uncertainty.
     *
     *  @return the speed upper bound
     */

    @OSIDBinding @Override
    public org.osid.mapping.Speed getUpperBound() {
        return (null);
    }


    /**
     *  Compares this Speed with the specified Speed to determine the
     *  natural order. Returns a negative integer, zero, or a positive
     *  integer as this speed is less than, equal to, or greater than
     *  the specified speed.
     *
     *  The natural ordering is determined first by the natural
     *  ordering of the speed scheme, then by its components. This
     *  method is not useful for numeric comparisons. The ranges
     *  implied by the granularities and uncertainties of their
     *  Distances and Durations may overlap in such a way that two
     *  unequal Speeds may be neither less than or greater than the
     *  other.
     *
     *  If <code>compareTo</code> a speed is zero, then
     *  <code>equals()</code> must be <code>true</code> and their hash
     *  codes must also be equal for consistent behavior. For
     *  orderings that may yield inconsistent behavior, an external
     *  <code>Comparator</code> should be used.
     *
     *  @param speed the speed to be compared
     *  @return a negative integer, zero, or a positive integer as
     *          this speed is less than, equal to, or greater than
     *          the specified speed
     *  @throws ClassCastException if the specified speed's type
     *          prevents it from being compared to this speed
     */

    @OSIDBinding @Override
    public int compareTo(org.osid.mapping.Speed speed) {
        if (this == speed) {
            return (0);
        }
        
	if (!getDistance().equals(speed.getDistance())) {
            return (getDistance().compareTo(speed.getDistance()));
        }

        return (getTimeUnit().compareTo(speed.getTimeUnit()));
    }


    /**
     *  Determines if the given <code> Speed </code> is equal to
     *  this one. Two Speeds are equal if the scheme and components
     *  are equal.
     *
     *  @param  obj an object to compare
     *  @return <code> true </code> if the given object is equal to
     *          this <code>Speed</code>, <code> false </code>
     *          otherwise
     */

    @Override
    public final boolean equals(Object obj) {
        if (obj == null) {
            return (false);
        }

        if (this == obj) {
            return (true);
        }

        if (!(obj instanceof org.osid.mapping.Speed)) {
            return (false);
        }

        org.osid.mapping.Speed speed = (org.osid.mapping.Speed) obj;
	if (!getTimeUnit().equals(speed.getTimeUnit())) {
            return (false);
        }

	if (!getDistance().equals(speed.getDistance())) {
            return (false);
        }

        return (true);
    }


    /**
     *  Returns a hash code value for this <code>Speed</code> based on
     *  the distance and time unit.
     *
     *  @return a hash code value for this object
     */

    @Override
    public final int hashCode() {
        if (this.hash == 0) {
            this.hash = calculateHashCode();
        }

        return (this.hash);
    }


    /**
     *  Returns a string representation of this Speed.
     *
     *  All implementations are encouraged to supply this method to
     *  display the speed.
     *
     *  @return a string
     */

    @Override
    public String toString() {
        if (this.label == null) {
            this.label = formatLabel();
        }

        return (this.label);
    }


    private int calculateHashCode() {
	int hash = 1;
	hash = hash * 31 + getDistance().hashCode();
	hash = hash * 31 + getTimeUnit().hashCode();

        return (hash);
    }


    private String formatLabel() {
	StringBuilder sb = new StringBuilder();
        sb.append(getDistance().getMeters());
        sb.append("m/");
        sb.append(getLabel(getTimeUnit()));
	return (sb.toString());
    }


    /**
     *  Gets a label for a unit.
     *
     *  @param units the granularity
     *  @return a label
     *  @throws org.osid.NullArgumentException <code>units</code> is
     *          <code>null</code>
     */

    protected String getLabel(org.osid.calendaring.DateTimeResolution units) {
        nullarg(units, "units");

        switch (units) {
        case INFINITY:
            return ("∞");
        case AEON:
            return ("aeon");
        case EPOCH:
            return ("epoch");
        case GLACIAL:
            return ("ice age");
        case MILLENNIA:
            return ("mill");
        case CENTURY:
            return ("cent");
        case DECADE:
            return ("decade");
        case BLUEMOON:
            return ("blue moon");
        case YEAR:
            return ("y");
        case MONTH:
            return ("mon");
        case WEEK:
            return ("wk");
        case DAY:
            return ("day");
        case HOUR:
            return ("h");
        case HALF_HOUR:
        case QUARTER_HOUR:
        case MINUTE:
            return ("m");
        case SECOND:
            return ("s");
        case MILLISECOND:
            return ("ms");
        case MICROSECOND:
            return ("µs");
        case NANOSECOND:
            return ("ns");
        case PICOSECOND:
            return ("ps");
        case FEMTOSECOND:
            return ("fs");
        case ATTOSECOND:
            return ("as");
        case ZEPTOSECOND:
            return ("zs");
        case YOCTOSECOND:
            return ("ys");
        case XOXXOSECOND:
            return ("xs");
        case WEEBLESECOND:
            return ("ws");
        case VATOSECOND:
            return ("vs");
        case UNDASECOND:
            return ("us");
        case PLANCKSECOND:
            return ("planck second");
        default:
            return ("???");
        }
    }
}
