//
// GeometricCoordinateTypes.java
//
//     An enumeration of Geometric Coordinate Types.
//
//
// Tom Coppeto
// Okapia
// 5 April 2012
//
//
// Copyright (c) 2012 Okpaia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.types.coordinate;

import static net.okapia.osid.primordium.locale.text.eng.us.Plain.text;


/**
 *  An enumeration of Geometric Coordinate Types.
 */

public enum GeometricCoordinateTypes
    implements net.okapia.osid.torrefacto.collect.TypeEnum {

    /** Bipolar */
    BIPOLAR ("Bipolar"),

    /** Cartesian */
    CARTESIAN ("Cartesian"),

    /** Conical */
    CONICAL ("Conical"),

    /** Cylindrical */
    CYLINDRICAL ("Cylindrical"),

    /** Ellipsoidal */
    ELLIPSOIDAL ("Ellipsoidal"),

    /** Elliptic Cylindrical */
    ELLIPTIC ("Elliptic", "Elliptic Cylindrical"),

    /** Log Polar */
    LOGPOLAR ("Log Polar"),

    /** Oblate Spheroidal */
    OBLATESPHEROIDAL ("OblateSpheroidal", "Oblate Spheroidal"),

    /** Parabolic Cylindrical */
    PARABOLIC ("Parabolic"),

    /** Pataboloidal */
    PARABOLODIAL ("Paraboloidal"),

    /** Polar */
    POLAR ("Polar"),

    /** Prolate Spheroidal */
    PROLATESPHEROIDAL ("Parabolic", "Prolate Spheroidal"),

    /** Spherical */
    Spherical ("Spherical"),

    /** Toroidal */
    TOROIDAL ("Toroidal");

    private final String identifier;
    private final String name;
    private org.osid.type.Type type;

    
    GeometricCoordinateTypes(String name) {
        this(name, name);
        return;
    }

    
    GeometricCoordinateTypes(String identifier, String name) {
        this.identifier = identifier;
        this.name       = name;
	return;
    }
    

    /**
     *  Gets the Type.
     *
     *  @return the Type
     */

    public org.osid.type.Type getType() {
        if (this.type == null) {
            this.type = new net.okapia.osid.primordium.type.BasicTypeBuilder()
                .authority("okapia.net")
                .namespace("coordinate")
                .identifier(this.identifier)
                .domain(text("Geometric Coordinate Systems"))
                .name(text(this.name + " System"))
                .label(text(this.identifier))
                .description(text("The geometric coordinate type for the " +  this.name + " system."))
                .build();
        }

	return (this.type);
    }
}
