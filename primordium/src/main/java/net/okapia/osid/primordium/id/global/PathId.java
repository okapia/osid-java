//
// PathId.java
//
//     Defines an Id using a file system path.
//
//
// Tom Coppeto
// Okapia
// 20 January 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.id.universal;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>Id</code> using a file system path name. The
 *  namespace is "path" and the authority is ""localhost." 
 */

public final class PathId 
    extends net.okapia.osid.primordium.id.spi.AbstractId
    implements org.osid.id.Id {

    private static final long serialVersionUID = -186444829L;


    /**
     *  Creates a new <code>PathId</code> for localhost from an
     *  absolute path name.
     *
     *  @param path
     *  @throws org.osid.NullArgumentException null argument provided
     */

    public PathId(String path) {        
        super("localhost", "path", path);
        return;
    }


    /**
     *  Returns an <code>Id</code> from an absolute path name. The
     *  path name is converted to its canonical absolute form by
     *  removing redundant components such as . and ..
     *
     *  @param path the absolute path to include as the identifier
     *  @return an <code>Id</code>
     *  @throws org.osid.InvalidArgumentException could not get path
     *  @throws org.osid.NullArgumentException <code>path</code> is
     *          <code>null</code>
     */

    public static org.osid.id.Id valueOf(String path) {
        return (valueOf(new java.io.File(path)));
    }


    /**
     *  Returns an <code>Id</code> from a Java File. The path name is
     *  converted to its canonical absolute form by removing redundant
     *  components such as . and ..
     *
     *  @param file a File
     *  @return an <code>Id</code>
     *  @throws org.osid.InvalidArgumentException could not get path
     *  @throws org.osid.NullArgumentException <code>file</code> is
     *          <code>null</code>
     */

    public static org.osid.id.Id valueOf(java.io.File file) {
        nullarg(file, "file");
        
        try {
            return (new PathId(file.getCanonicalPath()));
        } catch (java.io.IOException ie) {
            throw new org.osid.InvalidArgumentException(ie);
        }
    }

    
    /**
     *  Converts an OSID Id into a Path. 
     *
     *  @return formatted string
     */

    @Override     
    protected String formatLabel() {
	return (getIdentifier());
    }    
}
