//
// GlobalURLId
//
//     Defines an Id object using a URL identifier.
//
//
// Tom Coppeto
// Okapia
// 20 September 2013
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.id.global;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Creates Ids from URLs in a url namespace. This class accepts any
 *  URL of the form:
 *
 *      http://authority/identifier
 *
 *  The Ids produced by this class are immutable and serializable.
 */

public final class GlobalURLId 
    extends net.okapia.osid.primordium.id.spi.AbstractId
    implements org.osid.id.Id {

    private static final long serialVersionUID = -1085798980L;


    /**
     *  Creates a new <code>GlobalURLId</code> using the "url"
     *  namespace.
     *
     *  http://authority/identifier
     *
     *  @param authority the authority
     *  @param identifier
     *  @throws org.osid.NullArgumentException null argument provided
     */

    protected GlobalURLId(String authority, String identifier) {
	super(authority, "url", identifier);
	return;
    }


    /**
     *  Returns an <code>Id</code> from a URL.
     *
     *  @param url the url
     *  @return a <code>URLId</code>
     *  @throws org.osid.InvalidArgumentException url could not be
     *          parsed
     *  @throws org.osid.NullArgumentException <code>url</code> is
     *          <code>null</code>
     */

    public static org.osid.id.Id valueOf(java.net.URL url) {
	nullarg(url, "url");

        if (url.getPort() == 80) {
            return (new GlobalURLId(unescape(url.getHost()), unescape(url.getPath())));
        } else {
            return (new GlobalURLId(unescape(url.getHost()) + ":" + url.getPort(), 
                                    unescape(url.getPath())));
        }
    }


    /**
     *  Returns an <code>Id</code> from its string representation of
     *  the url.
     *
     *  @param url the string to be parsed
     *  @return an <code>Id</code>
     *  @throws org.osid.InvalidArgumentException string could not be
     *          parsed
     *  @throws org.osid.NullArgumentException <code>url</code> is
     *          <code>null</code>
     */

    public static org.osid.id.Id valueOf(String url) {
	nullarg(url, "url");	

	try {
	    return (valueOf(new java.net.URL(url)));
	} catch (java.net.MalformedURLException mue) {
	    throw new org.osid.InvalidArgumentException(url + " not valid", mue);
	}
    }


    /**
     *  Converts an OSID Id into a URL. The resulting format is:
     *  <code>http://authority/namespace/identifier</code>.
     *
     *  @return formatted string
     */

    @Override     
    protected String formatLabel() {
	return ("http://" + escape(getAuthority()) + "/" + escape(getIdentifier()));
    }


    private static final java.util.Collection<Character> ALLOWED_CHARS = java.util.Arrays.asList(
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 
        'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 
        'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 
        '-', '.', '_', '~', '!', '*', '(', ')', '\'', 
        '/', '#', '?', '&', '=', ':');


    private static String escape(String s) {
        StringBuilder sb = new StringBuilder();
        
        for (char c : s.toCharArray()) {
            if (ALLOWED_CHARS.contains(c)) {
                sb.append(c);
            } else {
                sb.append('%');
                sb.append(Integer.toHexString((int) c));
            }
        }
        
        return (sb.toString());
    }


    private static String unescape(String s) {
        StringBuilder sb = new StringBuilder();        
        char c[] = s.toCharArray();

        for (int i = 0; i < c.length; i++) {
            if (c[i] != '%') {
                sb.append(c[i]);
            } else if (c.length < (i + 3)) {
                throw new org.osid.InvalidArgumentException("bad escape sequence");
            } else {                
                ++i;
                try {
                    sb.append((char) Integer.parseInt(new StringBuilder().append(c[i]).append(c[i+1]).toString()));
                } catch (Exception e) {
                    throw new org.osid.InvalidArgumentException("cannot parse hex value");
                }

                ++i;
            }
        }
        
        return (sb.toString());
    }
}
