//
// BytesTerm.java
//
//     Implements a BytesTerm.
//
//
// Tom Coppeto
// Okapia
// 29 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.terms;

import org.osid.binding.java.annotation.OSID;


/**
 *  A  implementation of BytesTerm.
 */

public final class BytesTerm
    extends net.okapia.osid.primordium.terms.spi.AbstractBytesTerm
    implements org.osid.search.terms.BytesTerm {


    /**
     *  Creates a new <code>BytesTerm</code>.
     *
     *  @param b the byte value
     *  @param match <code>true</code> if a positive term,
     *         <code>false</code> for a negative term
     *  @param partial <code>true</code> if a partial match
     *         <code>false</code> for a complete match
     *  @throws org.osid.NullArgumentException <code>b</code> is
     *          <code>null</code>
     */

    public BytesTerm(byte[] b, boolean match, boolean partial) {
	super(b, match, partial);
	return;
    }
}
