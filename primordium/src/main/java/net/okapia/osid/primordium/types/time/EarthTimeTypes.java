//
// EarthTimeTypes.java
//
//     An enumeration of Earth Time Types.
//
//
// Tom Coppeto
// Okapia
// 5 April 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.types.time;

import static net.okapia.osid.primordium.locale.text.eng.us.Plain.text;


/**
 *  An enumeration of Time Types relevant to the planet Earth.
 */

public enum EarthTimeTypes
    implements net.okapia.osid.torrefacto.collect.TypeEnum {

    /** GPS Time */
    GPS ("GPS"),

    /** Metric Time */
    METRIC ("Metric"),

    /** International Atomic Time */
    TAI ("TAI", "International Atomic"),

    /** Universal Time */
    UT0 ("UT0", "Universal (UT0)"),

    /** Universal Time */
    UT1 ("UT1", "Universal (UT1)"),

    /** Universal Time */
    UT1R ("UT1R", "Universal (UT1R)"),

    /** Universal Time */
    UT2 ("UT2", "Universal (UT2)"),

    /** Universal Time */
    UT2R ("UT2R", "Universal (UT2R)"),

    /** Coordinated Universal Time */
    UTC ("UTC", "Coordinated Universal");

    private final String identifier;
    private final String name;
    private org.osid.type.Type type;
    

    EarthTimeTypes(String name) {
        this(name, name);
        return;
    }


    EarthTimeTypes(String identifier, String name) {
        this.identifier = identifier;
        this.name       = name;
	return;
    }
    

    /**
     *  Gets the Type.
     *
     *  @return the Type
     */

    public org.osid.type.Type getType() {
        if (this.type == null) {
            this.type = new net.okapia.osid.primordium.type.BasicTypeBuilder()
                .authority("okapia.net")
                .namespace("time")
                .identifier(identifier)
                .domain(text("Earth Time Systems"))
                .name(text(name + " Time"))
                .label(text(identifier))
                .description(text("The Earth time type for the " +  name + " time."))
                .build();            
        }

	return (this.type);
    }
}
