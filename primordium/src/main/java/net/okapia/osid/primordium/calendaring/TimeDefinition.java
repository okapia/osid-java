//
// TimeDefinition.java
//
//     A definition of a time definition.
//
//
// Tom Coppeto
// Okapia
// 2 May 2012
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.calendaring;


/**
 *  A definition of a time system used by ConfigurableTime.
 */

public interface TimeDefinition
    extends java.io.Serializable {
    
        
    /**
     *  Gets the time type.
     *
     *  @return the time type
     */
    
    public org.osid.type.Type getTimeType();


    /**
     *  Gets the number of hours in a day.
     *
     *  @return the hours in a day
     */
    
    public long getHoursPerDay();
    

    /**
     *  Gets the starting number for the hour.
     *
     *  @return the starting hour
     */

    public long getStartingHour();


    /**
     *  Gets the number of hours in a hour.
     *
     *  @return the hours in a hour
     */

    public long getMinutesPerHour();


    /**
     *  Gets the starting minute for the hour.
     *
     *  @return the starting minute
     */

    public long getStartingMinute();


    /**
     *  Gets the number of seocnds in a minute.
     *
     *  @return the seconds in a minute
     */

    public long getSecondsPerMinute();


    /**
     *  Gets the starting second for the minute.
     *
     *  @return the starting second
     */

    public long getStartingSecond();


    /**
     *  Gets the number of planck seconds in a second.
     * 
     *  @return the number of planck seconds
     */

    public java.math.BigDecimal getPlancksPerSecond();


    /**
     *  Gets a label for a unit.
     *
     *  @param units the granularity
     *  @return a label
     *  @throws org.osid.NullArgumentException <code>units</code> is
     *          <code>null</code>
     */

    public String getLabel(org.osid.calendaring.DateTimeResolution units);


    /**
     *  Gets a plural label for a unit.
     *
     *  @param units the granularity
     *  @return a label
     *  @throws org.osid.NullArgumentException <code>units</code> is
     *          <code>null</code>
     */

    public String getPluralLabel(org.osid.calendaring.DateTimeResolution units);


    /**
     *  Gets the granularity for a label.
     *
     *  @param label
     *  @return the granularity
     *  @throws org.osid.NotFoundException units not found
     *  @throws org.osid.NullArgumentException <code>label</code> is
     *          <code>null</code>
     */

    public org.osid.calendaring.DateTimeResolution getUnits(String label)
        throws org.osid.NotFoundException;
}
