//
// AbstractConfigurableTimeSpecifier.java
//
//     An an OSID Time specifier.
//
//
// Tom Coppeto
// Okapia
// 5 May 2012
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.calendaring.DateTimeResolution;

import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  The AbstractConfigurableTimeSpecifier is used to construct
 *  <code>Times</code> using TimeDefinitions. This class may be
 *  sublassed to perform validations based on the time Type.
 */

public abstract class AbstractConfigurableTimeSpecifier
    extends AbstractTimeSpecifier {
    
    private final net.okapia.osid.primordium.calendaring.TimeDefinition definition;
    
    
    /**
     *  Constructs a new
     *  <code>AbstractConfigurableTimeSpecifier</code>.
     *
     *  @param definition the definition of time system
     *  @throws org.osid.NullArgumentException <code>definition</code>
     *          is <code>null</code>
     */
    
    protected AbstractConfigurableTimeSpecifier(net.okapia.osid.primordium.calendaring.TimeDefinition definition) {        
        super(definition.getTimeType());
        
        nullarg(definition, "time definition");
        this.definition = definition;
        
        return;
    }
    
    
    /**
     *  Gets the Time system definition.
     *
     *  @return the definition
     */
    
    protected net.okapia.osid.primordium.calendaring.TimeDefinition getTimeDefinition() {
        return (this.definition);
    }
    
    
    /**
     *  Sets the hour of the day.
     *
     *  @param hour the hour of the day
     *  @throws org.osid.InvalidArgumentException <code>hour</code> is
     *          out of range
     */
    
    @Override
    public void setHour(long hour) {
        long maxHours = getTimeDefinition().getHoursPerDay() + getTimeDefinition().getStartingHour() - 1;
        
        if ((hour < getTimeDefinition().getStartingHour() || (hour > maxHours))) {
            throw new org.osid.InvalidArgumentException("hour is out of range for " + getTimeDefinition().getTimeType());
        }
        
        super.setHour(hour);
        return;
    }
    
    
    /**
     *  Sets the minute of the hour.
     *
     *  @param minute the minute of the hour
     *  @throws org.osid.InvalidArgumentException <code>minute</code>
     *          is out of range
     */
    
    @Override
    public void setMinute(long minute) {
        long maxMinutes = getTimeDefinition().getMinutesPerHour() + getTimeDefinition().getStartingMinute() - 1;
        
        if ((minute < getTimeDefinition().getStartingMinute() || (minute > maxMinutes))) {
            throw new org.osid.InvalidArgumentException("minute is out of range for " +  getTimeDefinition().getTimeType());
        }
        
        switch (getGranularity()) {
        case HALF_HOUR:
            minute = (getTimeDefinition().getMinutesPerHour() - getTimeDefinition().getStartingMinute()) / 2;
            break;
        case QUARTER_HOUR:
            minute = (getTimeDefinition().getMinutesPerHour() - getTimeDefinition().getStartingMinute()) / 4;
            break;
        }
        
        super.setMinute(minute);
        return;
    }
    
    
    /**
     *  Sets the second of the hour.
     *
     *  @param second the second of the minute
     *  @throws org.osid.InvalidArgumentException <code>minute</code>
     *          is out of range
     */
    
    @Override
    public void setSecond(long second) {
        long maxSeconds = getTimeDefinition().getSecondsPerMinute() + getTimeDefinition().getStartingSecond() - 1;
        
        if ((second < getTimeDefinition().getStartingSecond() || (second > maxSeconds))) {
            throw new org.osid.InvalidArgumentException("second is out of range for " + getTimeDefinition().getTimeType());
        }
        
        super.setSecond(second);
        return;
    }
}
