//
// AbstractDateTimeRangeTerm.java
//
//     Implements a DateTimeRangeTerm.
//
//
// Tom Coppeto
// Okapia
// 29 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.terms.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A basic implementation of DateTimeRangeTerm.
 */

public abstract class AbstractDateTimeRangeTerm
    extends net.okapia.osid.primordium.terms.spi.AbstractQueryTerm
    implements org.osid.search.terms.DateTimeRangeTerm {

    private final org.osid.calendaring.DateTime start;
    private final org.osid.calendaring.DateTime end;


    /**
     *  Creates a new <code>AbstractDateTimeRangeTerm</code>.
     *
     *  @param start the datetime start range
     *  @param end the datetime end range
     *  @param match <code>true</code> if a positive term,
     *         <code>false</code> for a negative term
     *  @throws org.osid.InvalidArgumentException types are not
     *          compatible or <code>start</code> is greater than
     *          <code>end</code>
     *  @throws org.osid.NullArgumentException <code>start</code>
     *          or <code>end</code> is <code>null</code>
     */

    public AbstractDateTimeRangeTerm(org.osid.calendaring.DateTime start,
                                     org.osid.calendaring.DateTime end, boolean match) {
	super(match);

        nullarg(start, "start datetime range");
        nullarg(end, "end datetime range");

        if (!start.getCalendarType().equals(end.getCalendarType()) ||
            !start.getTimeType().equals(end.getTimeType())) {
            throw new org.osid.InvalidArgumentException("datetimes have different calendar or time types");
        }

        if (start.isGreater(end)) {
            throw new org.osid.InvalidArgumentException("start is greater than end");
        }

        this.start = start;
        this.end   = end;

	return;
    }


    /**
     *  Gets the start of the datetime range in the term.
     *
     *  @return the datetime start
     *  @throws org.osid.IllegalStateException
     *          <code>isWildcard()</code> is <code>true</code>
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getDateTimeRangeStart() {
        return (this.start);
    }


    /**
     *  Gets the end of the datetime range in the term.
     *
     *  @return the datetime end
     *  @throws org.osid.IllegalStateException
     *          <code>isWildcard()</code> is <code>true</code>
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getDateTimeRangeEnd() {
        return (this.end);
    }
}
