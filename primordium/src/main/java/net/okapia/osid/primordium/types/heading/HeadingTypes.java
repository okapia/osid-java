//
// HeadingTypes.java
//
//     An enumeration of Heading Types.
//
//
// Tom Coppeto
// Okapia
// 25 April 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.types.heading;

import static net.okapia.osid.primordium.locale.text.eng.us.Plain.text;


/**
 *  An enumeration of Heading Types.
 */

public enum HeadingTypes
    implements net.okapia.osid.torrefacto.collect.TypeEnum {

    /** Degrees, Minutes, Seconds */
    DEGREE ("Degree");

    private final String identifier;
    private final String name;
    private org.osid.type.Type type;


    HeadingTypes(String name) {
        this(name, name);
        return;
    }


    HeadingTypes(String identifier, String name) {
        this.identifier = identifier;
        this.name       = name;
	return;
    }
    

    /**
     *  Gets the Type.
     *
     *  @return the Type
     */

    public org.osid.type.Type getType() {

        if (this.type == null) {
            this.type = new net.okapia.osid.primordium.type.BasicTypeBuilder()
                .authority("okapia.net")
                .namespace("heading")
                .identifier(identifier)
                .domain(text("Headings"))
                .name(text(name + " Heading Type"))
                .label(text(name))
                .description(text("The heading type for the " +  name + " heading."))
                .build();
        }

	return (this.type);
    }
}
