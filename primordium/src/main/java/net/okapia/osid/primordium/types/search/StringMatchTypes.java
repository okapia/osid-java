//
// StrignMatchTypes.java
//
//     An enumeration of string match Types.
//
//
// Tom Coppeto
// Okapia
// 25 February 2013
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.types.search;


/**
 *  An enumeration of string match Types.
 */

public enum StringMatchTypes
    implements net.okapia.osid.torrefacto.collect.TypeEnum {

    EXACT ("Exact"),
    IGNORECASE ("IgnoreCase", "Ignore Case"),
    WORD ("Word"),
    WORDIGNORECASE ("WordIgnoreCase", "Word Ignore Case"),        
    WILDCARD ("Wildcard"),        
    REGEX ("Regex", "Regular Expression"),
    SOUND ("Sound"),
    SOUNDEX ("Soundex"),
    METAPHONE ("Metaphone"),
    DMETAPHONE ("Dmetaphone"),
    LEVENSHTEIN ("Levenshtein");
        
    private final String identifier;
    private final String name;
    private org.osid.type.Type type;

    StringMatchTypes(String identifier) {
        this.identifier = identifier;
        this.name = identifier;
	return;
    }


    StringMatchTypes(String identifier, String name) {
        this.identifier = identifier;
        this.name = name;
	return;
    }
    

    /**
     *  Gets the Type.
     *
     *  @return the Type
     */

    public org.osid.type.Type getType() {
        if (this.type == null) {
            this.type = new net.okapia.osid.primordium.types.text.util.LazyTextTypeBuilder()
                .authority("okapia.net")
                .namespace("stringmatch")
                .identifier(this.identifier)
                .domain("String Match Types")
                .name(this.name + " String Match")
                .label(this.name)
                .description("The string match type for the " +  this.name + ".")
                .build();
        }

	return (this.type);
    }
}
