//
// AbstractIntegerRangeTerm.java
//
//     Implements a IntegerRangeTerm.
//
//
// Tom Coppeto
// Okapia
// 29 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.terms.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.integerrangearg;


/**
 *  A basic implementation of IntegerRangeTerm.
 */

public abstract class AbstractIntegerRangeTerm
    extends AbstractQueryTerm
    implements org.osid.search.terms.IntegerRangeTerm {

    private final long start;
    private final long end;


    /**
     *  Creates a new <code>AbstractIntegerRangeTerm</code>.
     *
     *  @param start the integer start range
     *  @param end the integer end range
     *  @param match <code>true</code> if a positive term,
     *         <code>false</code> for a negative term
     *  @throws org.osid.InvalidArgumentException <code>start</code>
     *          is greater than <code>end</code>
     */

    public AbstractIntegerRangeTerm(long start, long end, boolean match) {
	super(match);

        integerrangearg(start, end, "start integer range", "end integer range");
        this.start = start;
        this.end   = end;

	return;
    }


    /**
     *  Gets the start of the <code> integer </code> range in the term.
     *
     *  @return the integer start
     *  @throws org.osid.IllegalStateException
     *          <code>isWildcard()</code> is <code>true</code>
     */

    @OSID @Override
    public long getIntegerRangeStart() {
        return (this.start);
    }


    /**
     *  Gets the end of the <code> integer </code> range in the term.
     *
     *  @return the integer end
     *  @throws org.osid.IllegalStateException
     *          <code>isWildcard()</code> is <code>true</code>
     */

    @OSID @Override
    public long getIntegerRangeEnd() {
        return (this.end);
    }
}
