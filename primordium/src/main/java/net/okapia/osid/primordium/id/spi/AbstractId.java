//
// AbstractId.java
//
//     Defines a simple Id object implementing the Id interface.
//
//
// Tom Coppeto
// OnTapSolutions
// 20 June 2008
//
//
// Copyright (c) 2008 Massachusetts Institute of Technology. 
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.id.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Id represents an identifier object. Ids are designated by the following 
 *  elements:
 *  
 *  <ul>
 *      <li> <code> identifier: </code> a unique key or guid </li>
 *      <li> <code> namespace: </code> the namespace of the identifier </li>
 *      <li> <code> authority: </code> the issuer of the identifier </li>
 *  </ul>

 *  Two Ids are equal if their namespace, identifier and authority
 *  strings are equal. Only the identifier is
 *  case-sensitive. Persisting an <code> Id </code> means persisting
 *  the above components.
 *
 *  The <code>valueOf()</code> and <code>toString</code> methods
 *  support strings in the form of: namespace:identifier@authority
 *
 *  The Ids produced by this class are immutable and serializable.
 */

public abstract class AbstractId 
    implements org.osid.id.Id {

    private final String authority;
    private final String namespace;
    private final String identifier;
    private String label;
    private int hash = 0;

    private static final long serialVersionUID = -1333920481L;



    /**
     *  Constructs a new <code>AbstractId</code>.
     *
     *  @param authority the authority of this Id
     *  @param namespace the identifier namespace of this Id
     *  @param identifier the identification component of this Id
     *  @throws org.osid.NullArgumentException <code>authority</code>,
     *          <code>namespace</code>, or <code>identifier</code> is
     *          <code>null</code>
     */

    protected AbstractId(String authority, String namespace, String identifier) {
        nullarg(authority, "authority");
        nullarg(namespace, "namespace");
        nullarg(identifier, "identifier");

        this.authority = authority;
        this.namespace = namespace;
        this.identifier = identifier;

        return;
    }


    /**
     *  Gets the authority of this <code>Id</code>. The authority is
     *  a string used to ensure the uniqueness of this <code> Id
     *  </code> when using a non-federated identifier
     *  space. Generally, it is a domain name identifying the party
     *  responsible for this <code> Id</code>. This method is used to
     *  compare one <code> Id </code> to another.
     *
     *  @return the authority of this <code> Id </code>
     */

    @OSID @Override
    public final String getAuthority() {
        return (this.authority);
    }


    /**
     *  Gets the namespace of the identifier. This method is used to compare 
     *  one <code> Id </code> to another. 
     *
     *  @return the authority of this <code> Id </code> 
     */

    @OSID @Override
    public final String getIdentifierNamespace() {
        return (this.namespace);
    }


    /**
     *  Gets the identifier of this <code> Id</code>. This method is used to 
     *  compare one <code> Id </code> to another. 
     *
     *  @return the identifier of this <code> Id </code> 
     */

    @OSID @Override
    public final String getIdentifier() {
        return (this.identifier);
    }


    /**
     *  Determines if the given <code> Id </code> is equal to this one. Two 
     *  Ids are equal if the namespace, authority and identifier components 
     *  are equal. The identifier is case sensitive while the namespace and 
     *  authority strings are not case sensitive. 
     *
     *  @param  obj an object to compare
     *  @return <code> true </code> if the given object is equal to 
     *          this <code>Id</code>, <code> false </code> otherwise 
     */

    @OSIDBinding @Override
    public final boolean equals(Object obj) {
        if (obj == null) {
            return (false);
        }

        if (this == obj) {
            return (true);
        }

        if (!(obj instanceof org.osid.id.Id)) {
            return (false);
        }

        org.osid.id.Id id =  (org.osid.id.Id) obj;
	if (getAuthority().equals(id.getAuthority()) && 
	    getIdentifierNamespace().equals(id.getIdentifierNamespace()) &&
	    getIdentifier().equals(id.getIdentifier())) {
	    return (true);
	} else {
	    return (false);
	}
    }


    /**
     *  Returns a hash code value for this <code>Id</code> based on
     *  the authority, namespace and identifier.
     *
     *  @return a hash code value for this object
     */

    @OSIDBinding @Override
    public final int hashCode() { 
        if (this.hash == 0) {
            this.hash = calculateHashCode();
        }

        return (this.hash);
    }


    /**
     *  Returns a string representation of this Id.
     *
     *  All implementations are encouraged to supply this method to
     *  display a seriaized Id.
     *
     *  @return a string
     */

    @OSIDBinding @Override
    public String toString() {
        if (this.label == null) {
            this.label = formatLabel();
        }

        return (this.label);
    }


    private int calculateHashCode() {
        int hash = 1;
        hash = hash * 31 + getAuthority().hashCode();
        hash = hash * 31 + getIdentifierNamespace().hashCode();
        hash = hash * 31 + getIdentifier().hashCode();

        return (hash);
    }


    /**
     *  Formats the toString label.
     *
     *  @return the formatted Id
     */

    protected abstract String formatLabel();
}

