//
// HeadingRangeTerm.java
//
//     Implements a HeadingRangeTerm.
//
//
// Tom Coppeto
// Okapia
// 29 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.terms;

import org.osid.binding.java.annotation.OSID;


/**
 *  A basic implementation of HeadingRangeTerm.
 */

public final class HeadingRangeTerm
    extends net.okapia.osid.primordium.terms.spi.AbstractHeadingRangeTerm
    implements org.osid.search.terms.HeadingRangeTerm {


    /**
     *  Creates a new <code>HeadingRangeTerm</code>.
     *
     *  @param start the heading start range
     *  @param end the heading end range
     *  @param match <code>true</code> if a positive term,
     *         <code>false</code> for a negative term
     *  @throws org.osid.InvalidArgumentException types are not
     *          compatible
     *  @throws org.osid.NullArgumentException <code>start</code>
     *          or <code>end</code> is <code>null</code>
     */

    public HeadingRangeTerm(org.osid.mapping.Heading start,
                            org.osid.mapping.Heading end, boolean match) {

	super(start, end, match);
	return;
    }
}
