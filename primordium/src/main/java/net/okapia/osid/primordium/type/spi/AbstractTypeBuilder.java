//
// AbstractType.java
//
//     Implements a basic Type.
//
//
// Tom Coppeto
// OnTapSolutions
// 29 April 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.type.spi;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A Type Builder.
 */

public abstract class AbstractTypeBuilder<T extends AbstractTypeBuilder<T>> {
    
    private String authority;
    private String namespace;
    private String identifier;

    private org.osid.locale.DisplayText name;
    private org.osid.locale.DisplayText label;
    private org.osid.locale.DisplayText description;
    private org.osid.locale.DisplayText domain;
    
    
    /**
     *  Constructs a new AbstractTypeBuilder.
     */
    
    protected AbstractTypeBuilder() {
        return;
    }
   
    
    /**
     *  Constructs a new AbstractTypeBuilder from another Type.
     *
     *  @param type a Type to copy
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */
    
    public AbstractTypeBuilder(org.osid.type.Type type) {
        nullarg(type, "type");
        
        this.authority   = type.getAuthority();
        this.namespace   = type.getIdentifierNamespace();
        this.identifier  = type.getIdentifier();
        this.domain      = type.getDomain();
        this.name        = type.getDisplayName();
        this.label       = type.getDisplayLabel();
        this.description = type.getDescription();
        
        return;
    }

        
    /**
     *  Sets the authority for a Type.
     *
     *  @param authority the Type authority
     *  @return the basic type builder
     *  @throws org.osid.NullArgumentException
     *          <code>authority</code> is <code>null</code>
     */
    
    public T authority(String authority) {
        nullarg(authority, "authority");
        this.authority = authority;
        return (self());
    }
    
    
    protected String getAuthority() {
        return (this.authority);
    }
    
    
    /**
     *  Sets the namespace for a Type.
     *
     *  @param namespace the Type namespace
     *  @return the basic type builder
     *  @throws org.osid.NullArgumentException
     *          <code>namespace</code> is <code>null</code>
     */
    
    public T namespace(String namespace) {
        nullarg(namespace, "namespace");
        this.namespace = namespace;
        return (self());
    }
    
    
    protected String getNamespace() {
        return (this.namespace);
    }
    
    
    /**
     *  Sets the identifier for a Type.
     *
     *  @param identifier the Type identifier
     *  @return the basic type builder
     *  @throws org.osid.NullArgumentException
     *          <code>identifier</code> is <code>null</code>
     */
    
    public T identifier(String identifier) {
        nullarg(identifier, "identifier");
        this.identifier = identifier;
        return (self());
    }
    
    
    protected String getIdentifier() {
        return (this.identifier);
    }

        
    /**
     *  Sets the domain for a Type.
     *
     *  @param domain the Type domain
     *  @return the basic type builder
     *  @throws org.osid.NullArgumentException
     *          <code>domain</code> is <code>null</code>
     */
    
    public T domain(org.osid.locale.DisplayText domain) {
        nullarg(domain, "domain");
        this.domain = domain;
        return (self());
    }

        
    protected org.osid.locale.DisplayText getDomain() {
        return (this.domain);
    }


    /**
     *  Sets the display name for a Type.
     *
     *  @param name the Type display name
     *  @return the basic type builder
     *  @throws org.osid.NullArgumentException
     *          <code>name</code> is <code>null</code>
     */
    
    public T name(org.osid.locale.DisplayText name) {
        nullarg(name, "name");
        this.name = name;
        return (self());
    }
    
    
    protected org.osid.locale.DisplayText getDisplayName() {
        return (this.name);
    }
    
    
    /**
     *  Sets the display label for a Type.
     *
     *  @param label the Type display label
     *  @return the basic type builder
     *  @throws org.osid.NullArgumentException
     *          <code>label</code> is <code>null</code>
     */
    
    public T label(org.osid.locale.DisplayText label) {
        nullarg(label, "label");
        this.label = label;
        return (self());
    }
    

    protected org.osid.locale.DisplayText getDisplayLabel() {
        return (this.label);
    }
    

    /**
     *  Sets the display description for a Type.
     *
     *  @param description the Type display description
     *  @return the basic type builder
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */
    
    public T description(org.osid.locale.DisplayText description) {
        nullarg(description, "description");
        this.description = description;
        return (self());
    }
    
    
    protected org.osid.locale.DisplayText getDescription() {
        return (this.description);
    }


    /**
     *  Gets the reference to this instance of the builder.
     *
     *  @return ObjectBuilder
     */

    protected abstract T self();


    /**
     *  Builds the BasicType. The Type authority, namespace, and
     *  identifier must have been set.
     *
     *  @return a new Type
     *  @throws org.osid.IllegalStateException Type cannot be
     *          constructed
     */
    
    public abstract org.osid.type.Type build();    
}
