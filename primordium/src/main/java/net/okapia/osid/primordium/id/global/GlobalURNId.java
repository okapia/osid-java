//
// GlobalURNId
//
//     Defines an Id object using a URN scheme.
//
//
// Tom Coppeto
// OnTapSolutions
// 20 June 2008
//
//
// Copyright (c) 2008,2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.id.global;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Creates Ids fron URNs in the urn namespace. This class accepts any
 *  URL of the form:
 * 
 *      urn:authority:identifier
 */

public final class GlobalURNId 
    extends net.okapia.osid.primordium.id.spi.AbstractId
    implements org.osid.id.Id {

    private static final long serialVersionUID = -1085797058L;


    /**
     *  Creates a new <code>GlobalURNId</code>.
     *
     *  @param authority the authority for this Id
     *  @param identifier the identifier for this Id
     *  @throws org.osid.NullArgumentException <code>authority</code>,
     *          or <code>identifier</code> is <code>null</code>
     */

    protected GlobalURNId(String authority, String identifier) {
	super(authority, "urn", identifier);
	return;
    }


    /**
     *  Returns an <code>Id</code> from its string representation.
     *
     *  @param urn the string to be parsed
     *  @return an <code>Id</code>
     *  @throws org.osid.InvalidArgumentException string could not be
     *          parsed
     *  @throws org.osid.NullArgumentException <code>urn</code> is
     *          <code>null</code>
     */

    public static org.osid.id.Id valueOf(String urn) {        
	nullarg(urn, "urn");

	String identifier = "";
        String nid;

	try {
	    String[] parts = urn.split(":");
	    if ((parts.length < 3) || !parts[0].equals("urn")) {
		throw new org.osid.InvalidArgumentException(urn + " is not a valid URN");
	    }

	    nid = unescape(parts[1]);

	    for (int i = 2; i < parts.length; i++) {
		identifier += unescape(parts[i]);
	    }

	} catch (java.util.regex.PatternSyntaxException pse) {
	    throw new org.osid.InvalidArgumentException("error parsing " + urn, pse);
	}

        return (new GlobalURNId(nid, identifier));
    }


    /**
     *  Converts an OSID Id into a URN. The resulting format is:
     *  <code>urn:osid:authority:namespace:identifier</code>.
     *
     *  @return formatted string
     */

    @Override     
    protected String formatLabel() {
	return ("urn:" + escape(getAuthority()) + ":" + escape(getIdentifier()));
    }


    private static final java.util.Collection<Character> ALLOWED_CHARS = java.util.Arrays.asList(
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 
        'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 
        'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 
        '(', ')', '+', ',', '-', '.', '=', '@', ';', '$', '_', '!', '*', 
        '\'');


    private static String escape(String s) {
        StringBuilder sb = new StringBuilder();
        
        for (char c : s.toCharArray()) {
            if (ALLOWED_CHARS.contains(c)) {
                sb.append(c);
            } else {
                sb.append('%');
                sb.append(Integer.toHexString((int) c));
            }
        }
        
        return (sb.toString());
    }


    private static String unescape(String s) {
        StringBuilder sb = new StringBuilder();        
        char c[] = s.toCharArray();

        for (int i = 0; i < c.length; i++) {
            if (c[i] != '%') {
                sb.append(c[i]);
            } else if (c.length < (i + 3)) {
                throw new org.osid.InvalidArgumentException("bad escape sequence");
            } else {                
                ++i;
                try {
                    sb.append((char) Integer.parseInt(new StringBuilder().append(c[i]).append(c[i+1]).toString()));
                } catch (Exception e) {
                    throw new org.osid.InvalidArgumentException("cannot parse hex value");
                }

                ++i;
            }
        }
        
        return (sb.toString());
    }
}
