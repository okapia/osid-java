//
// URNId
//
//     Defines an Id object using a URN scheme.
//
//
// Tom Coppeto
// OnTapSolutions
// 20 June 2008
//
//
// Copyright (c) 2008,2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.id;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>Id</code> using a URN.  For example:
 *  urn:osid:okapia.net:identifiers:repository:Asset:987.
 */

public final class URNId 
    extends net.okapia.osid.primordium.id.spi.AbstractId
    implements org.osid.id.Id {

    private static final long serialVersionUID = 1053000620L;


    /**
     *  Creates a new <code>URNId</code>.
     *
     *  @param authority the authority for this Id
     *  @param namespace the identifier namespace of this Id
     *  @param identifier the identifier for this Id
     *  @throws org.osid.NullArgumentException <code>authority</code>,
     *          <code>namespace</code>, or <code>identifier</code> is
     *          <code>null</code>
     */

    protected URNId(String authority, String namespace, String identifier) {
	super(authority, namespace, identifier);
	return;
    }


    /**
     *  Creates a new <code>URNId</code> from the authority and
     *  identifier components. This method can be used to wrap an
     *  <code>Id</code> of unknown origin that supports equality and
     *  hash codes based on the unique identifiers.
     *
     *  @param id an Id
     *  @throws org.osid.NullArgumentException null argument provided
     */

    public static org.osid.id.Id valueOf(org.osid.id.Id id) {
	nullarg(id, "id");
	return (new URNId(id.getAuthority(), id.getIdentifierNamespace(), id.getIdentifier()));
    }


    /**
     *  Returns an <code>Id</code> from its string representation of
     *  the form <code>urn:osid:authority:identifiers:namespace:identifier</code>.
     *
     *  @param urn the string to be parsed
     *  @return an <code>Id</code>
     *  @throws org.osid.InvalidArgumentException string could not be
     *          parsed
     *  @throws org.osid.NullArgumentException <code>urn</code> is
     *          <code>null</code>
     */

    public static org.osid.id.Id valueOf(String urn) {        
	nullarg(urn, "urn");

	String authority;
        String namespace;
	String identifier = "";

	try {
	    String[] parts = urn.split(":");
	    if ((parts.length < 6) || !parts[0].equals("urn")) {
		throw new org.osid.InvalidArgumentException(urn + " is not a valid URN");
	    }

	    String nid = parts[1];
	    if (!nid.equals("osid")) {
		throw new org.osid.InvalidArgumentException("nid for urn is not osid namespace");
	    }

	    authority  = unescape(parts[2]);
            
            if (!parts[3].equals("identifiers")) {
                throw new org.osid.InvalidArgumentException("malformed urn");
            }

            namespace  = unescape(parts[4]);

	    for (int i = 5; i < parts.length; i++) {
		if (i > 5) {
		    identifier += "/";
		}

		identifier += unescape(parts[i]);
	    }

	} catch (java.util.regex.PatternSyntaxException pse) {
	    throw new org.osid.InvalidArgumentException("error parsing " + urn, pse);
	}
        
	return (new URNId(authority, namespace, identifier));
    }


    /**
     *  Converts an OSID Id into a URN. The resulting format is:
     *  <code>urn:osid:authority:namespace:identifier</code>.
     *
     *  @return formatted string
     */

    @Override     
    protected String formatLabel() {
	return ("urn:osid:" + escapeNS(getAuthority()) + ":identifiers:" + 
                escapeNS(getIdentifierNamespace()) + ":" + escapeIdent(getIdentifier()));
    }


    private static final java.util.Collection<Character> VALID_CHARS = java.util.Arrays.asList(
                                                                                               '(', ')', '+', ',', 
                                                                                               '-', '.', '=', '@', 
                                                                                               ';', '$', '_', '!', 
                                                                                               '*', '\'');


    private static String escapeIdent(String s) {
	nullarg(s, "string");

        StringBuilder sb = new StringBuilder();
        
        for (char c : s.toCharArray()) {
            if (Character.isLetterOrDigit(c)) {
                sb.append(c);
            } else if (c == '/') {
                sb.append(':');
            } else if (VALID_CHARS.contains(c)) {
                sb.append(c);
            } else {
                sb.append('%');
                sb.append(Integer.toHexString(c));
            }
        }
        
        return (sb.toString());
    }


    private static String escapeNS(String s) {
	nullarg(s, "string");

        StringBuilder sb = new StringBuilder();
        
        for (char c : s.toCharArray()) {
            if (Character.isLetterOrDigit(c)) {
                sb.append(c);
            } else if (VALID_CHARS.contains(c)) {
                sb.append(c);
            } else {
                sb.append('%');
                sb.append(Integer.toHexString(c));
            }
        }
        
        return (sb.toString());
    }

    
    private static String unescape(String s) {
        try {
            return (java.net.URLDecoder.decode(s, "UTF-8"));
        } catch (java.io.UnsupportedEncodingException uee) {
            throw new org.osid.OsidRuntimeException(uee);
        }

        /*
        StringBuilder sb = new StringBuilder();        
        char c[] = s.toCharArray();

        for (int i = 0; i < c.length; i++) {
            if (c[i] != '%') {
                sb.append(c[i]);
            } else if (c.length < (i + 3)) {
                throw new org.osid.InvalidArgumentException("bad escape sequence");
            } else {                
                ++i;
                try {
                    sb.append((char) Integer.parseInt(new StringBuilder().append(c[i]).append(c[i+1]).toString(), 16));
                } catch (Exception e) {
                    throw new org.osid.InvalidArgumentException("cannot parse hex value");
                }

                ++i;
            }
        }
        
        return (sb.toString());
        */

    }
}
