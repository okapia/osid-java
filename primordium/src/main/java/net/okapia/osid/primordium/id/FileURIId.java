//
// FileURIId.java
//
//     Defines an Id object using a file based URI scheme.
//
//
// Tom Coppeto
// Okapia
// 20 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.id;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>Id</code> using a file based URI scheme for local
 *  identifiers.
 */

public final class FileURIId 
    extends net.okapia.osid.primordium.id.spi.AbstractId
    implements org.osid.id.Id {

    private static final long serialVersionUID = -3740799L;


    /**
     *  Creates a new <code>FileURIId</code> for localhost from an
     *  absolute path name.
     *
     *  @param path
     *  @throws org.osid.NullArgumentException null argument provided
     */

    public FileURIId(String path) {
        super("localhost", "file", path);
        return;
    }


    /**
     *  Returns an <code>Id</code> from an absolute path name.
     *
     *  @param path the absolute path to include as the identifier
     *  @return an <code>Id</code> with the string components
     *  @throws org.osid.NullArgumentException <code>path</code> is
     *          <code>null</code>
     */

    public static org.osid.id.Id valueOf(String path) {
        nullarg(path, "path");
        java.nio.file.Path p = java.nio.file.Paths.get(path).toAbsolutePath().normalize();
        return (new FileURIId(p.toString()));
    }


    /**
     *  Returns an <code>Id</code> from a file based URI. 
     *
     *  @param uri the URI
     *  @return an <code>Id</code> with the string components
     *  @throws org.osid.InvalidArgumentException cannot parse file
     *          uri because uri protocol is not <code>file</code> or
     *          host is not <code>localhost</code>
     *  @throws org.osid.NullArgumentException <code>uri</code> is
     *          <code>null</code>
     */
    
    public static org.osid.id.Id valueOf(java.net.URI uri) {
        nullarg(uri, "uri");

        if (!uri.getScheme().equals("file")) {
            throw new org.osid.InvalidArgumentException("cannot parse " + uri);
        }

        if ((uri.getHost() != null) && !uri.getHost().equals("localhost")) {
            throw new org.osid.InvalidArgumentException("cannot parse " + uri);
        }

        return (new FileURIId(uri.getPath()));
    }
    

    /**
     *  Converts an OSID Id into a URI.
     *
     *  @return formatted string
     */

    @Override     
    public String formatLabel() {  
        return ("file:" + getIdentifier());
    }
}
