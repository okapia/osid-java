//
// DateTimeFactory.java
//
//     A factory to assist the abstract DateTime classes.
//
//
// Tom Coppeto
// Okapia
// 2 May 2012
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.calendaring.spi;


/**
 *  A factory to assist the DateTime classes.
 */

public interface DateTimeFactory
    extends java.io.Serializable {


    /**
     *  Gets a <code>CalendarDefinition</code>.
     *
     *  @return the datetime definition
     */
    
    public net.okapia.osid.primordium.calendaring.CalendarDefinition getCalendarDefinition();


    /**
     *  Gets a <code>DateTimeSpecifier</code>.
     */
    
    public AbstractDateTimeSpecifier getDateTimeSpecifier();


    /**
     *  Makes a DateTime from a DateTimeSpecifier.
     *
     *  @param specifier the time specifier
     *  @return a DateTime
     *  @throws org.osid.NullArgumentException <code>specifier</code>
     *          is <code>null</code>
     *  @throws org.osid.UnsupportedException calendarType not supported
     */

    public org.osid.calendaring.DateTime makeDateTime(AbstractDateTimeSpecifier specifier);


    /**
     *  Makes a Time from a TimeSpecifier.
     *
     *  @param specifier the time specifier
     *  @return a Time
     *  @throws org.osid.NullArgumentException <code>specifier</code>
     *          is <code>null</code>
     *  @throws org.osid.UnsupportedException timeType not supported
     */

    public org.osid.calendaring.Time makeTime(AbstractTimeSpecifier specifier);


    /**
     *  Makes a Time from a DateTime.
     *
     *  @param datetime a datetime
     *  @return a DateTime
     *  @throws org.osid.NullArgumentException <code>datetime</code>
     *          is <code>null</code>
     *  @throws org.osid.UnsupportedException timeType not supported
     */

    public org.osid.calendaring.Time makeTime(org.osid.calendaring.DateTime datetime);
}
