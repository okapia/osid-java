//
// NewEarthTime.java
//
//     An implementation of a NewEarth based OSID Time.
//
//
// Tom Coppeto
// Okapia
// 2 May 2012
//
// Copyright (c) 2012 Ingenescus. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.calendaring;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;
import org.osid.calendaring.DateTimeResolution;

import net.okapia.osid.primordium.types.time.EarthTimeTypes;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An implementation of a Time based on NewEarth using a
 *  NewEarthTimeDefinition.
 */

public final class NewEarthTime 
    extends net.okapia.osid.primordium.calendaring.spi.AbstractConfigurableTime
    implements org.osid.calendaring.Time {

    private String label;
    private static final long serialVersionUID = 379681876L;


    /**
     *  Constructs a new <code>NewEarthTime</code> with second
     *  resolution.
     *
     *  @param degree the degree
     *  @param minute the minute
     *  @param second the second
     *  @throws org.osid.InvalidArgumentException <code>hour</code>,
     *          <code>minute</code>, or <code>second</code> is
     *          negative, or <code>hour</code> is greater than 23
     *          <code>minute</code> is greater than 59, or
     *          <code>second</code> is greater than 60
     */

    public NewEarthTime(long degree, long minute, long second) {
        super(new NewEarthTimeFactory(), degree, minute, second);
        
        TimeDefinition td = new NewEarthTimeDefinition();

        if (degree > (td.getHoursPerDay() + td.getStartingHour() - 1)) {
            throw new org.osid.InvalidArgumentException("degree is out of range");
        }

        if (minute > (td.getMinutesPerHour() + td.getStartingMinute() - 1)) {
            throw new org.osid.InvalidArgumentException("minute is out of range");
        }

        if (second > (td.getSecondsPerMinute() + td.getStartingSecond() - 1)) {
            throw new org.osid.InvalidArgumentException("second is out of range");
        }

        if (degree < td.getStartingHour()) {
            throw new org.osid.InvalidArgumentException("degree is out of range");
        }

        if (minute < td.getStartingMinute()) {
            throw new org.osid.InvalidArgumentException("minute is out of range");
        }

        if (second < td.getStartingSecond()) {
            throw new org.osid.InvalidArgumentException("second is out of range");
        }

        return;
    }

    
    /**
     *  Constructs a new <code>NewEarthTime</code>.
     *
     *  @param specifier a time specifier
     *  @throws org.osid.NullArgumentException <code>specifier</code>
     *          is <code>null</code>
     */

    public NewEarthTime(NewEarthTimeSpecifier specifier) {
        super(new NewEarthTimeFactory(), specifier);
    }


    /**
     *  Returns a Time for a time format.
     *
     *  @return a string
     *  @throws org.osid.InvalidArgumentException cannot parse string
     *  @throws org.osid.NullArgumentException <code>s</code> is
     *          <code>null</code>
     */

    public static org.osid.calendaring.Time valueOf(String s) {        
        nullarg(s, "time string");
        return (parseTime(new NewEarthTimeFactory(), s));
    }


    /**
     *  Creates a Time based on a formatted string.
     *
     *  @param factory the time factory
     *  @param str the string to parse
     *  @return a Time
     *  @throws org.osid.InvalidArgumentException cannot parse string
     *  @throws org.osid.NullArgumentException <code>factory</code> or
     *          <code>str</code> is <code>null</code>
     */
    
    @SuppressWarnings("fallthrough")
    protected static org.osid.calendaring.Time parseTime(net.okapia.osid.primordium.calendaring.spi.TimeFactory factory, String str) {
        DateTimeResolution granularity;
        String degStr = null;
        String minStr = null;
        String secStr = null;
        String decStr = null;

        String[] tok = str.split("º ");
        degStr = tok[0];
        if (tok.length > 1) {
            tok = tok[1].split("' ");
            minStr = tok[0];
            if (tok.length > 1) {
                tok = tok[1].split("\" ");
                secStr = tok[0];
            }
        }

        granularity = DateTimeResolution.HOUR;

        if (minStr != null) {
            granularity = DateTimeResolution.MINUTE;
        }

        if (secStr != null) {
            granularity = DateTimeResolution.SECOND;
        }

        String dec = "";
        String[] dtok = secStr.split("\\.");
        if (dtok.length == 2) {
            secStr = tok[0];
            dec    = dtok[1];
        }

        int multiplier = 1;
        org.osid.calendaring.Time decimal = null;
        if (dec.length() > 0) {
            decimal = parseDecimal(factory, dec);
            granularity = decimal.getGranularity();
            multiplier = (int) decimal.getGranularityMultiplier();
        }
                
        net.okapia.osid.primordium.calendaring.spi.AbstractTimeSpecifier spec = factory.getTimeSpecifier();
        spec.setGranularity(granularity, multiplier);

        if (decimal != null) {
            switch (granularity) {
            case PLANCKSECOND:
                spec.setPlanckSeconds((int) decimal.getPlanckSeconds());  
            case UNDASECOND:
                spec.setUndaseconds((int) decimal.getUndaseconds());
            case VATOSECOND:
                spec.setVatoseconds((int) decimal.getVatoseconds());
            case WEEBLESECOND:
                spec.setWeebleseconds((int) decimal.getWeebleseconds());
            case XOXXOSECOND:
                spec.setXoxxoseconds((int) decimal.getXoxxoseconds());
            case YOCTOSECOND:
                spec.setYoctoseconds((int) decimal.getYoctoseconds());
            case ZEPTOSECOND:
                spec.setZeptoseconds((int) decimal.getZeptoseconds());
            case ATTOSECOND:
                spec.setAttoseconds((int) decimal.getAttoseconds());
            case FEMTOSECOND:
                spec.setFemtoseconds((int) decimal.getFemtoseconds());
            case PICOSECOND:
                spec.setPicoseconds((int) decimal.getPicoseconds());
            case NANOSECOND:
                spec.setNanoseconds((int) decimal.getNanoseconds());
            case MICROSECOND:
                spec.setMicroseconds((int) decimal.getMicroseconds());
            case MILLISECOND:
                spec.setMilliseconds((int) decimal.getMilliseconds());
            }
        }

        try {
            switch(granularity) {
            case SECOND:
                spec.setSecond(Long.parseLong(secStr));
            case MINUTE:
                spec.setMinute(Long.parseLong(minStr));
            case HOUR:
                spec.setHour(Long.parseLong(degStr));
            }
        } catch (NumberFormatException nfe) {
            throw new org.osid.InvalidArgumentException(nfe);
        }
        
        return (factory.makeTime(spec));
    }
    
    
    /**
     *  Returns a string representation of this DateTime. 
     *
     *  @return a string
     */

    @OSIDBinding @Override
    public String toString() {        
        if (this.label == null) {
            this.label = formatLabel();
        }

        return (this.label);
    }


    private String formatLabel() {
	if (getGranularity() == DateTimeResolution.INFINITY) {
	    return ("undefined");
	}

	StringBuilder s = new StringBuilder(16);
	s.append(getHour());
        s.append("º");

	if (getGranularity() == DateTimeResolution.HOUR) {
	    return (s.toString());
	}

        s.append(" ");
	s.append(getMinute());
        s.append("'");
	if (getGranularity() == DateTimeResolution.MINUTE) {
	    return (s.toString());
	}

	s.append(" ");
	s.append(getSecond());
        s.append("\"");
	if (getGranularity() == DateTimeResolution.SECOND) {
	    return (s.toString());
	}

	s.append(".");
	s.append(formatDecimal(this));
        s.append("Z");
	return (s.toString());
    }


    /**
     *  The TimeSpecifier is used to construct
     *  <code>Times</code>. This class may be sublassed to perform
     *  validations based on the time Type.
     */

    public static class NewEarthTimeSpecifier
        extends net.okapia.osid.primordium.calendaring.spi.AbstractConfigurableTimeSpecifier {

        
        /**
         *  Constructs a new <code>TimeSpecifier</code>.
         */

        public NewEarthTimeSpecifier() {
            super(new NewEarthTimeDefinition());
            return;
        }


        /**
         *  Constructs a new <code>TimeSpecifier</code> from another Time.
         *
         *  @param time a Time to copy
         *  @throws org.osid.NullArgumentException <code>time</code>
         *          is <code>null</code>
         */
        
        protected NewEarthTimeSpecifier(org.osid.calendaring.Time time) {
            
            super(new NewEarthTimeDefinition());
            setTime(time);
            return;
        }
    }


    protected static class NewEarthTimeFactory
        implements net.okapia.osid.primordium.calendaring.spi.TimeFactory {

        private static final long serialVersionUID = 3796818761L;
        
        
        /**
         *  Gets a <code>TimeDefinition</code>.
         *
         *  @return the time definition
         */
        
        @Override
        public TimeDefinition getTimeDefinition() {
            return (new NewEarthTimeDefinition());
        }


        /**
         *  Gets a <code>NewEarthTimeSpecifier</code>.
         */

        @Override
        public NewEarthTimeSpecifier getTimeSpecifier() {
            return (new NewEarthTimeSpecifier());
        }


        /**
         *  Makes a Time.
         *
         *  @param specifier the time specifier
         *  @return a Time
         *  @throws org.osid.InvalidArgumentException sepcifier is not
         *          for New Earth
         *  @throws org.osid.NullArgumentException <code>specifier</code>
         *          is <code>null</code>
         */

        @Override
        public org.osid.calendaring.Time makeTime(net.okapia.osid.primordium.calendaring.spi.AbstractTimeSpecifier specifier) {
            if (!(specifier instanceof NewEarthTimeSpecifier)) {
                throw new org.osid.InvalidArgumentException("not a New Earth specifier");
            }

            return (new NewEarthTime((NewEarthTimeSpecifier) specifier));
        }
    }
}
