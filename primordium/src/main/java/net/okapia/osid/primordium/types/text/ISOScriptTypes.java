//
// ISOScriptTypes.java
//
//     An enumeration of Display Text Script Types.
//
//
// Tom Coppeto
// Okapia
// 25 April 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.types.text;


/**
 *  An enumeration of DisplayText Script Types from ISO 15924.
 */

public enum ISOScriptTypes
    implements net.okapia.osid.torrefacto.collect.TypeEnum {

        AFAK ("Afak"),
        ARAB ("Arabic"),
        ARMI ("ImperialAramaic", "Imperial Aramaic"),
        ARMN ("Armenian"),
        AVST ("Avestan"),
        BALI ("Balinese"),
        BAMU ("Bamum"),
        BASS ("BassaVah", "Bassa Vah"),
        BATK ("Batak"),
        BENG ("Bengali"),
        BLIS ("Bissymbols"),
        BOPO ("Bopomofo"),
        BRAH ("Brahmi"),
        BUGI ("Buginese"),
        BUHD ("Buhid"),
        CAKM ("Chakma"),
        CANS ("UCAS", "Unified Canadian Aboriginal Syllabics"),
        CARI ("Carian"),
        CHAM ("Cham"),
        CHER ("Cherokee"),
        CIRT ("Cirth"),
        COPT ("Coptic"),
        CPRT ("Cypriot"),
        CYRL ("Cyrillic"),
        DEVA ("Devanagari"),
        DSRT ("Deseret"),
        DUPL ("Duployan"),
        EGYD ("EgyptianDemotic", "Egyptian Demotic"),
        EGYH ("EgyptianHieratic", "Egyptian Hieratic"),
        EGYP ("EgyptianHieroglyphs", "Egyptian Hieroglyphs"),
        ELBA ("Elbasan"),
        ETHI ("Ethopic"),
        GEOK ("Khutsuri"),
        GEOR ("Georgian"),
        GLAG ("Glagolotic"),
        GOTH ("Gothic"),
        GRAN ("Grantha"),
        GREK ("Greek"),
        GUJR ("Gujarati"),
        GURU ("Gurmukhi"),
        HANG ("Hangul"),
        HANI ("Han"),
        HANS ("HanSimple", "Han Simplified"),
        HANT ("HanTraditional", "Han Traditional"),
        HEBR ("Hebrew"),
        HIRA ("Hiragana"),
        HLUW ("AnatolianHieroglyphs", "Anatolian, Luwian, Hittite Hieroglyphs"),
        HMNG ("PahawhHmong", "Pahawh Hmong"),
        HRKT ("HiraganaKatakana", "Japanese Hiragana and Katakana"),
        HUNG ("OldHungarian", "Old Hungarian"),
        INDS ("Indus"),
        ITAL ("OldItalic", "Old Italic"),
        JAVA ("Javanese"),
        JPAN ("Japanese", "Japanese Han, Hiragana, and Katakana"),
        JURC ("Jurchen"),
        KALI ("KayahLi", "Kayah Li"),
        KANA ("Katakana"),
        KHAR ("Kharoshthi"),
        KHMR ("Khmer"),
        KHOJ ("Khojki"),
        KNDA ("Kannada"),
        KORE ("Korean", "Koran Hangul and Han"),
        KPEL ("Kpelle"),
        KTHI ("Kaithi"),
        LANA ("TaiTham", "Tai Tham"),
        LAOO ("Lao"),
        LATF ("LatinFraktur", "Latin Fra"),
        LATG ("LatinGaelic", "Latin Gaelic"),
        LATN ("Latin"),
        LEPC ("Lepcha"),
        LIMB ("Limbu"),
        LINA ("LinearA", "Linear A"),
        LINB ("LinearB", "Linear B"),
        LISU ("Lisu"),
        LOMA ("Loma"),
        LYCI ("Lycian"),
        LYDI ("Lydian"),
        MAND ("Mandaic"),
        MANI ("Manichaean"),
        MAYA ("Mayan", "Mayan Hieroglyphs"),
        MEND ("Mende"),
        MERC ("MeroiticCursive", "Meroitic Cursive"),
        MERO ("MeroiticHieroglyphs", "Meroitic Hieroglyphs"),
        MLYM ("Malayalam"),
        MONG ("Mongolian"),                     
        MOON ("Moon"),
        MROO ("Mro"),
        MTEI ("MeiteiMayek", "Meitei Mayek"),
        MYMR ("Myanmar"),
        NARB ("OldNorthArabian", "Old North Arabian"),
        NBAT ("Nabataean"),
        NKGB ("NakhiGeba", "Nakhi Geba"),
        NKOO ("N’Ko", "N'Ko"),
        NSHU ("Nüshu",  "NüshuO"),
        OGAM ("Ogham"),
        OLCK ("OlChiki", "Ol Chiki"),
        ORKH ("OldTurkic", "Old Turkic"),
        ORYA ("Oriya"),
        OSMA ("Osmanya"),
        PALM ("Palmyrene"),
        PERM ("Old Permic", "Old Permic"),
        PHAG ("Phags-pa"),
        PHLI ("InscriptionalPahlavi", "Inscriptional Pahlavi"),
        PHLP ("Psalter Pahlavi", "Psalter Pahlavi"),
        PHLV ("Book Pahlavi", "Book Pahlavi"),
        PHNX ("Phoenician"),                 
        PLRD ("Miao"),
        PRTI ("InscriptionalParthian", "Inscriptional Parthian"),
        RJNG ("Rejang"),
        RORO ("Rongorongo"),
        RUNR ("Runic"),
        SAMR ("Samaritan"),
        SARA ("Sarati"),
        SARB ("OldSouthArabian", "Old South Arabian"),
        SAUR ("Saurashtra"),
        SGNW ("SignWriting"),
        SHAW ("Shavian"),
        SHRD ("Sharada"),
        SIND ("Khudawadi"),
        SINH ("Sinhala"),
        SORA ("SoraSompeng", "Sora Sompeng"),
        SUND ("Sundanese"),
        SYLO ("SylotiNagri", "Syloti Nagri"),
        SYRC ("Syriac"),
        SYRE ("SyriacEstrangelo", "Syriac Estrangelo"),
        SYRJ ("SyriacWestern", "Syriac Western"),
        SYRN ("SyriacEastern", "Syriac Eastern"),
        TAGB ("Tagbanwa"),
        TAKR ("Takri"),
        TALE ("TaiLe", "Tai Le"),
        TALU ("NewTaiLue", "New Tai Lue"),
        TAML ("Tamil"),
        TANG ("Tangut"),
        TAVT ("TaiViet", "Tai Viet"),
        TELU ("Telugu"),                        
        TENG ("Tengwar"),                     
        TFNG ("Tifinagh"),
        TGLG ("Tagalog"),
        THAA ("Thaana"),
        THAI ("Thai"),
        TIBT ("Tibetan"),
        TIRH ("Tirhuta"),
        UGAR ("Ugaritic"),
        VAII ("Vai"),
        VISP ("VisibleSpeech", "Visible Speech"), 
        WARA ("WarangCiti", "Warang Citi"),
        WOLE ("Woleai"),
        XPEO ("OldPersian", "Old Persian"),                   
        XSUX ("Cuneiform"),
        YIII ("Yi"),
        ZMTH ("Mathematical", "Mathematical Notation"),
        ZSYM ("Symbols"),
        ZYYY ("Undetermined", "Undetermined Script"),
        ZZZZ ("Uncoded", "Uncoded Script");
        
    private final String name;
    private final String label;
    private org.osid.type.Type type;


    ISOScriptTypes(String name) {
        this(name, name);
    }


    ISOScriptTypes(String label, String name) {
        this.label = label;
        this.name  = name;
	return;
    }
    

    /**
     *  Gets the Type.
     *
     *  @return the Type
     */

    public org.osid.type.Type getType() {
        if (this.type == null) {
            this.type = new net.okapia.osid.primordium.types.text.util.LazyTextTypeBuilder()
                .authority("iso.org")
                .namespace("15924")
                .identifier(name())
                .domain("ISO Script Types")
                .name(this.name + " Script")
                .label(this.label)
                .description("The ISO script type for " +  this.name + ".")
                .build();
        }

        return (this.type);
    }
}
