//
// Speed.java
//
//     An implementation of Primitive for OSID Speeds.
//
//
// Tom Coppeto
// Okapia
// 21 October 2011
//
// Copyright (c) 2011 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.mapping;

import org.osid.binding.java.annotation.OSID;


/**
 *  An implementation of Primitive for OSID Speeds.
 */

public final class Speed 
    extends net.okapia.osid.primordium.mapping.spi.AbstractSpeed
    implements org.osid.mapping.Speed {

    private static final long serialVersionUID = 655031028L;


    /**
     *  Constructs a new <code>Speed</code>.
     *
     *  @param distance the distance traveled
     *  @param time per unit time
     *  @throws org.osid.NullArgumentException <code>distance</code>
     *          or <code>time</code> is <code>null</code>
     */

    public Speed(org.osid.mapping.Distance distance, org.osid.calendaring.DateTimeResolution time) {
        super(distance, time);
        return;
    }

    
    /**
     *  Creates a new <code>Speed</code> based on a fixed value of
     *  meters per second.
     *
     *  @param value the value of meters
     *  @throws org.osid.InvalidArgumentException <code>value</code>
     *          is negative
     */

    public static org.osid.mapping.Speed valueOf(long value) {
        return (new Speed(Distance.valueOf(value), org.osid.calendaring.DateTimeResolution.SECOND));
    }


    /**
     *  Creates a new <code>Speed</code> based on a fixed value of
     *  meters per second.
     *
     *  @param value the value of meters
     *  @throws org.osid.InvalidArgumentException <code>value</code>
     *          is negative
     *  @throws org.osid.NullArgumentException <code>value</code>
     *          is <code>null</code>
     */

    public static org.osid.mapping.Speed valueOf(java.math.BigDecimal value) {
        return (new Speed(Distance.valueOf(value), org.osid.calendaring.DateTimeResolution.SECOND));
    }
}
