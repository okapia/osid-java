//
// AbstractDateTimeSpecifier
//
//     An OSID DateTime specifier.
//
//
// Tom Coppeto
// Okapia
// 5 May 2012
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.calendaring.DateTimeResolution;

import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  The AbstractDateTimeSpecifier is used to construct
 *  <code>DateTimes</code>. This class may be sublassed to perform
 *  validations based on the calendar and time Type.
 */

public abstract class AbstractDateTimeSpecifier {
    
    private final org.osid.type.Type calendarType;
    private final AbstractTimeSpecifier timeSpecifier;
    
    private long aeon;
    private long epoch;
    private long millenium;
    private long century;
    private long year;
    private long month = 1;
    private long day = 1;

    private org.osid.calendaring.DateTimeResolution uncertaintyUnits;
    private long uncertaintyPlus;
    private long uncertaintyMinus;
    private boolean uncertaintyDateInclusive = true;
    private boolean uncertaintyTimeInclusive = true;

    private org.osid.calendaring.DateTimeResolution granularity = org.osid.calendaring.DateTimeResolution.INFINITY;
    private long multiplier = 1;


    /**
     *  Constructs a new <code>AbstractDateTimeSpecifier</code>.
     *
     *  @param calendarType the type of calendar
     *  @param timeSpecifier the time specifier
     *  @throws org.osid.NullArgumentException
     *          <code>calendarType</code>, <code>timeSpecifier</code> or
     *          is <code>null</code>
     */
    
    protected AbstractDateTimeSpecifier(org.osid.type.Type calendarType, AbstractTimeSpecifier timeSpecifier) {
        nullarg(calendarType, "calendar type");
        nullarg(timeSpecifier, "time specifier");

        this.calendarType  = calendarType;
        this.timeSpecifier = timeSpecifier;

        return;
    }
    

    /**
     *  Constructs a new <code>AbstractDateTimeSpecifier</code> from
     *  another DateTime.
     *
     *  @param datetime a DateTime to copy
     *  @param timeSpecifier the time specifier
     *  @throws org.osid.InvalidArgumentException incompatible datetimes
     *  @throws org.osid.NullArgumentException <code>datetime</code>
     *          or <code<timeSpecifier</code> is <code>null</code>
     */
    
    protected AbstractDateTimeSpecifier(org.osid.calendaring.DateTime datetime, AbstractTimeSpecifier timeSpecifier) {
        nullarg(datetime, "datetime");
        nullarg(timeSpecifier, "time specifier");

        this.calendarType  = datetime.getCalendarType();
        this.timeSpecifier = timeSpecifier;

        setGranularity(datetime.getGranularity(), datetime.getGranularityMultiplier());

        switch(getGranularity()) {
        case INFINITY:
            break;
        case AEON:
            setAeon(datetime.getAeon());
            break;
        case EPOCH:
            setEpoch(datetime.getEpoch());
            break;
        case GLACIAL:
        case MILLENNIA:
            setMillennium(datetime.getMillennium());
            break;
        case CENTURY:
            setCentury(datetime.getCentury());
            break;
        case DECADE:
        case BLUEMOON:
        case YEAR:
            setYear(datetime.getYear());
            break;
        case MONTH:
            setYear(datetime.getYear());
            setMonth(datetime.getMonth());
            break;
        default:
            setYear(datetime.getYear());
            setMonth(datetime.getMonth());
            setDay(datetime.getDay());
            break;
        }

        return;
    }
    
     
    /**
     *  Gets the calendar type. 
     *
     *  @return the calendar type 
     */

    public org.osid.type.Type getCalendarType() {
        return (this.calendarType);
    }


    /**
     *  Gets the aeon starting from 1. An aeon is 1B years. 
     *
     *  @return the aeon 
     */

    public long getAeon() {
        return (this.aeon);
    }


    /**
     *  Sets the aeon. If the granularity os coarser than this, the
     *  granularity is set to aeons.
     *
     *  @param aeon the aeon
     */
    
    public void setAeon(long aeon) {
        if (getGranularity().ordinal() > org.osid.calendaring.DateTimeResolution.AEON.ordinal()) {
            setGranularity(org.osid.calendaring.DateTimeResolution.AEON);
        }
        
        this.aeon = aeon;
        return;
    }


    /**
     *  Gets the epoch starting from 1. An epoch is 1M years. 
     *
     *  @return the eposh 
     */

    public long getEpoch() {
        return (this.epoch);
    }


    /**
     *  Sets the epoch. If the granularity os coarser than this, the
     *  granularity is set to epochs.
     *
     *  @param epoch the epoch
     */
    
    public void setEpoch(long epoch) {
        if (getGranularity().ordinal() > org.osid.calendaring.DateTimeResolution.EPOCH.ordinal()) {
            setGranularity(org.osid.calendaring.DateTimeResolution.EPOCH);
        }
        
        this.epoch = epoch;
        return;
    }


    /**
     *  Gets the millennium starting from 1. A millenium is 1,000 years. 
     *
     *  @return the millennium 
     */

    public long getMillennium() {
        return (this.millenium);
    }


    /**
     *  Sets the millenium. If the granularity os coarser than this,
     *  the granularity is set to millennia.
     *
     *  @param millenium the millenium
     */
    
    public void setMillennium(long millenium) {
        if (getGranularity().ordinal() > org.osid.calendaring.DateTimeResolution.MILLENNIA.ordinal()) {
            setGranularity(org.osid.calendaring.DateTimeResolution.MILLENNIA);
        }
        
        this.millenium = millenium;
        return;
    }


    /**
     *  Gets the century starting from 1. 
     *
     *  @return the century 
     */

    public long getCentury() {
        return (this.century);
    }


    /**
     *  Sets the century. If the granularity os coarser than this, the
     *  granularity is set to centuries.
     *
     *  @param century the century
     */
    
    public void setCentury(long century) {
        if (getGranularity().ordinal() > org.osid.calendaring.DateTimeResolution.CENTURY.ordinal()) {
            setGranularity(org.osid.calendaring.DateTimeResolution.CENTURY);
        }
        
        this.century = century;
        return;
    }


    /**
     *  Gets the year starting from 1. 
     *
     *  @return the year 
     */

    public long getYear() {
        return (this.year);
    }


    /**
     *  Sets the year. If the granularity os coarser than this, the
     *  granularity is set to years.
     *
     *  @param year the year
     */
    
    public void setYear(long year) {
        if (getGranularity().ordinal() > org.osid.calendaring.DateTimeResolution.YEAR.ordinal()) {
            setGranularity(org.osid.calendaring.DateTimeResolution.YEAR);
        }
        
        this.year = year;
        return;
    }


    /**
     *  Gets the month number starting from 1. 
     *
     *  @return the month 
     */

    public long getMonth() {
        return (this.month);
    }


    /**
     *  Sets the month. If the granularity os coarser than this, the
     *  granularity is set to months.
     *
     *  @param month the month
     *  @throws org.osid.InvalidArgumentException
     *          <code>month</code> is negative
     */
    
    public void setMonth(long month) {
        cardinalarg(month, "month");

        if (month == 0) {
            throw new org.osid.InvalidArgumentException("month starts at 1");
        }

        if (getGranularity().ordinal() > org.osid.calendaring.DateTimeResolution.MONTH.ordinal()) {
            setGranularity(org.osid.calendaring.DateTimeResolution.MONTH);
        }
        
        this.month = month;
        return;
    }


    /**
     *  Gets the day of the month starting from 1. 
     *
     *  @return the day of the month 
     */

    public long getDay() {
        return (this.day);
    }

    
    /**
     *  Sets the day. If the granularity os coarser than this, the
     *  granularity is set to days.
     *
     *  @param day the day
     *  @throws org.osid.InvalidArgumentException
     *          <code>day</code> is negative
     */
    
    public void setDay(long day) {
        cardinalarg(day, "day");

        if (day == 0) {
            throw new org.osid.InvalidArgumentException("day starts at 1");
        }

        if (getGranularity().ordinal() > org.osid.calendaring.DateTimeResolution.DAY.ordinal()) {
            setGranularity(org.osid.calendaring.DateTimeResolution.DAY);
        }
        
        this.day = day;
        return;
    }


    /**
     *  Sets the hour of the day. If the granularity is coarser than
     *  this resolution, the resolution will be set at an hour.
     *
     *  @param hour the hour of the day
     *  @throws org.osid.InvalidArgumentException <code>hour</code> is
     *          negative
     */
    
    public void setHour(long hour) {
        if (getGranularity().ordinal() > org.osid.calendaring.DateTimeResolution.HOUR.ordinal()) {
            setGranularity(org.osid.calendaring.DateTimeResolution.HOUR);
        }

        getTimeSpecifier().setHour(hour);
        return;
    }
    
    
    /**
     *  Sets the minute of the hour. If the granularity is coarser
     *  than this resolution, the resolution will be set at a minute.
     *
     *  @param minute the minute of the hour
     *  @throws org.osid.InvalidArgumentException <code>minute</code>
     *          is negative
     */
    
    public void setMinute(long minute) {
        if (getGranularity().ordinal() > org.osid.calendaring.DateTimeResolution.MINUTE.ordinal()) {
            setGranularity(org.osid.calendaring.DateTimeResolution.MINUTE);
        }

        getTimeSpecifier().setMinute(minute);
        return;
    }

    
    /**
     *  Sets the second of the minute. If the granularity is coarser
     *  than this resolution, the resolution will be set at a second.
     *
     *  @param second the second of the minute
     *  @throws org.osid.InvalidArgumentException <code>minute</code>
     *          is negative
     */
    
    public void setSecond(long second) {
        if (getGranularity().ordinal() > org.osid.calendaring.DateTimeResolution.SECOND.ordinal()) {
            setGranularity(org.osid.calendaring.DateTimeResolution.SECOND);
        }

        getTimeSpecifier().setSecond(second);
        return;
    }
    
    
    /**
     *  Sets the number of milliseconds. If the granularity
     *  is coarser than this resolution, the resolution will be set at
     *  a millisecond.
     *
     *  @param milliseconds 0-999
     *  @throws org.osid.InvalidArgumentException
     *          <code>milliseconds</code> out of range
     */
    
    public void setMilliseconds(int milliseconds) {
        if (getGranularity().ordinal() > org.osid.calendaring.DateTimeResolution.MILLISECOND.ordinal()) {
            setGranularity(org.osid.calendaring.DateTimeResolution.MILLISECOND);
        }

        getTimeSpecifier().setMilliseconds(milliseconds);
        return;
    }

    
    /**
     *  Sets the number of microseconds. If the granularity is coarser
     *  than this resolution, the resolution will be set at a
     *  microsecond.
     *
     *  @param microseconds 0-999
     *  @throws org.osid.InvalidArgumentException
     *          <code>microseconds</code> out of range
     */
    
    public void setMicroseconds(int microseconds) {
        if (getGranularity().ordinal() > org.osid.calendaring.DateTimeResolution.MICROSECOND.ordinal()) {
            setGranularity(org.osid.calendaring.DateTimeResolution.MICROSECOND);
        }

        getTimeSpecifier().setMicroseconds(microseconds);
        return;
    }
    
    
    /**
     *  Sets the number of nanoseconds. If the granularity is coarser
     *  than this resolution, the resolution will be set at a
     *  nanosecond.
     *
     *  @param nanoseconds 0-999
     *  @throws org.osid.InvalidArgumentException
     *          <code>nanoseconds</code> out of range
     */
    
    public void setNanoseconds(int nanoseconds) {
        if (getGranularity().ordinal() > org.osid.calendaring.DateTimeResolution.NANOSECOND.ordinal()) {
            setGranularity(org.osid.calendaring.DateTimeResolution.NANOSECOND);
        }

        getTimeSpecifier().setNanoseconds(nanoseconds);
        return;
    }

    
    /**
     *  Sets the number of picoseconds. If the granularity
     *  is coarser than this resolution, the resolution will be set at
     *  a picosecond.
     *
     *  @param picoseconds 0-999
     *  @throws org.osid.InvalidArgumentException
     *          <code>picoseconds</code> out of range
     */
    
    public void setPicoseconds(int picoseconds) {
        if (getGranularity().ordinal() > org.osid.calendaring.DateTimeResolution.PICOSECOND.ordinal()) {
            setGranularity(org.osid.calendaring.DateTimeResolution.PICOSECOND);
        }

        getTimeSpecifier().setPicoseconds(picoseconds);
        return;
    }
    
    
    /**
     *  Sets the number of femtoseconds.If the granularity is coarser
     *  than this resolution, the resolution will be set at a
     *  femtosecond.
     *
     *  @param femtoseconds 0-999
     *  @throws org.osid.InvalidArgumentException
     *          <code>femtoseconds</code> out of range
     */
    
    public void setFemtoseconds(int femtoseconds) {
        if (getGranularity().ordinal() > org.osid.calendaring.DateTimeResolution.FEMTOSECOND.ordinal()) {
            setGranularity(org.osid.calendaring.DateTimeResolution.FEMTOSECOND);
        }

        getTimeSpecifier().setFemtoseconds(femtoseconds);
        return;
    }

    
    /**
     *  Sets the number of attoseconds. If the granularity is coarser
     *  than this resolution, the resolution will be set at a
     *  attosecond.
     *
     *  @param attoseconds 0-999
     *  @throws org.osid.InvalidArgumentException
     *          <code>attoseconds</code> out of range
     */
    
    public void setAttoseconds(int attoseconds) {
        if (getGranularity().ordinal() > org.osid.calendaring.DateTimeResolution.ATTOSECOND.ordinal()) {
            setGranularity(org.osid.calendaring.DateTimeResolution.ATTOSECOND);
        }

        getTimeSpecifier().setAttoseconds(attoseconds);
        return;
    }
    
    
    /**
     *  Sets the number of zeptoseconds. If the granularity is coarser
     *  than this resolution, the resolution will be set at a
     *  zeptosecond.
     *
     *  @param zeptoseconds 0-999
     *  @throws org.osid.InvalidArgumentException
     *          <code>zeptoseconds</code> out of range
     */
    
    public void setZeptoseconds(int zeptoseconds) {
        if (getGranularity().ordinal() > org.osid.calendaring.DateTimeResolution.ZEPTOSECOND.ordinal()) {
            setGranularity(org.osid.calendaring.DateTimeResolution.ZEPTOSECOND);
        }

        getTimeSpecifier().setZeptoseconds(zeptoseconds);
        return;
    }
    
    
    /**
     *  Sets the number of yoctoseconds. If the granularity is coarser
     *  than this resolution, the resolution will be set at a
     *  yoctosecond.
     *
     *  @param yoctoseconds 0-999
     *  @throws org.osid.InvalidArgumentException
     *          <code>yoctoseconds</code> out of range
     */
    
    public void setYoctoseconds(int yoctoseconds) {
        if (getGranularity().ordinal() > org.osid.calendaring.DateTimeResolution.YOCTOSECOND.ordinal()) {
            setGranularity(org.osid.calendaring.DateTimeResolution.YOCTOSECOND);
        }

        getTimeSpecifier().setYoctoseconds(yoctoseconds);
        return;
    }

    
    /**
     *  Sets the number of xoxxoseconds. If the granularity is coarser
     *  than this resolution, the resolution will be set at a
     *  xoxxosecond.
     *
     *  @param xoxxoseconds 0-999
     *  @throws org.osid.InvalidArgumentException
     *          <code>xoxxoseconds</code> out of range
     */
    
    public void setXoxxoseconds(int xoxxoseconds) {
        if (getGranularity().ordinal() > org.osid.calendaring.DateTimeResolution.XOXXOSECOND.ordinal()) {
            setGranularity(org.osid.calendaring.DateTimeResolution.XOXXOSECOND);
        }

        getTimeSpecifier().setXoxxoseconds(xoxxoseconds);
        return;
    }

    
    /**
     *  Sets the number of weebleseconds. If the granularity is
     *  coarser than this resolution, the resolution will be set at a
     *  weeblesecond.
     *
     *  @param weebleseconds 0-999
     *  @throws org.osid.InvalidArgumentException
     *          <code>weebleseconds</code> out of range
     */
    
    public void setWeebleseconds(int weebleseconds) {
        if (getGranularity().ordinal() > org.osid.calendaring.DateTimeResolution.WEEBLESECOND.ordinal()) {
            setGranularity(org.osid.calendaring.DateTimeResolution.WEEBLESECOND);
        }

        getTimeSpecifier().setWeebleseconds(weebleseconds);
        return;
    }

    
    /**
     *  Sets the number of vatoseconds. If the granularity is coarser
     *  than this resolution, the resolution will be set at a
     *  vatosecond.
     *
     *  @param vatoseconds 0-999
     *  @throws org.osid.InvalidArgumentException
     *          <code>vatoseconds</code> out of range
     */
    
    public void setVatoseconds(int vatoseconds) {
        if (getGranularity().ordinal() > org.osid.calendaring.DateTimeResolution.VATOSECOND.ordinal()) {
            setGranularity(org.osid.calendaring.DateTimeResolution.VATOSECOND);
        }

        getTimeSpecifier().setVatoseconds(vatoseconds);
        return;
    }

    
    /**
     *  Sets the number of undaseconds. If the granularity is coarser
     *  than this resolution, the resolution will be set at a
     *  undasecond.
     *
     *  @param undaseconds 0-999
     *  @throws org.osid.InvalidArgumentException
     *          <code>undaseconds</code> out of range
     */
    
    public void setUndaseconds(int undaseconds) {
        if (getGranularity().ordinal() > org.osid.calendaring.DateTimeResolution.UNDASECOND.ordinal()) {
            setGranularity(org.osid.calendaring.DateTimeResolution.UNDASECOND);
        }

        getTimeSpecifier().setUndaseconds(undaseconds);
        return;
    }
        
    
    /**
     *  Sets the number of planck seconds. If the granularity is
     *  coarser than this resolution, the resolution will be set at a
     *  plancksecond.
     *
     *  @param plancks 0-18550948
     *  @throws org.osid.InvalidArgumentException <code>plancks</code>
     *          out of range
     */
    
    public void setPlanckSeconds(long plancks) {
        if (getGranularity().ordinal() > org.osid.calendaring.DateTimeResolution.PLANCKSECOND.ordinal()) {
            setGranularity(org.osid.calendaring.DateTimeResolution.PLANCKSECOND);
        }

        getTimeSpecifier().setPlanckSeconds(plancks);
        return;
    }


    
    /**
     *  Gets the granularity of this time. The granularity indicates
     *  the resolution of the clock. More precision than what is
     *  specified in this method cannot be inferred from the available
     *  data.
     *
     *  @return granularity 
     */
    
    public org.osid.calendaring.DateTimeResolution getGranularity() {
        return (this.granularity);
    }
    


    /**
     *  Sets the granularity of this time. The granularity indicates
     *  the resolution of the clock. More precision than what is
     *  specified in this method cannot be inferred from the available
     *  data.
     *
     *  @param granularity the resolution
     *  @throws org.osid.NullArgumentException
     *          <code>granularity</code> is <code>null</code>
     */
    
    public void setGranularity(org.osid.calendaring.DateTimeResolution granularity) {
        setGranularity(granularity, 1);
        return;
    }

    
    /**
     *  If the granularity of the time equals <code> getGranularity(),
     *  </code> then the multiplier is 1. This method may return a
     *  different number when the granularity differs from one of the
     *  defined resolutions.
     *
     *  @return granularity multiplier 
     */
    
    public long getGranularityMultiplier() {
        return (this.multiplier);
    }
    

    /**
     *  Sets the granularity of this time. The granularity indicates
     *  the resolution of the clock. More precision than what is
     *  specified in this method cannot be inferred from the available
     *  data.
     *
     *  @param granularity the resolution
     *  @param multiplier the granularity multiplier
     *  @throws org.osid.InvalidArgumentException
     *          <code>multiplier</code> is negative
     *  @throws org.osid.NullArgumentException
     *          <code>granularity</code> is <code>null</code>
     */
    
    public void setGranularity(org.osid.calendaring.DateTimeResolution granularity, long multiplier) {
        nullarg(granularity, "granularity");
        cardinalarg(multiplier, "granularity multiplier");
        
        this.granularity = granularity;
        this.multiplier  = multiplier;
        
        return;
    }


    /**
     *  Gets the units of the uncertainty. 
     *
     *  @return units of the uncertainty 
     */

    public org.osid.calendaring.DateTimeResolution getUncertaintyUnits() {
        return (this.uncertaintyUnits);
    }


    /**
     *  Gets the uncertainty of this time in the negative direction. 
     *
     *  @return the uncertainty under this value 
     */

    public long getUncertaintyMinus() {
        return (this.uncertaintyMinus);
    }


    /**
     *  Gets the uncertainty of this time in the positive direction. 
     *
     *  @return the uncertainty over this value 
     */

    public long getUncertaintyPlus() {
        return (this.uncertaintyPlus);
    }



    /**
     *  Tests if the uncertainty is inclusive of all dates. An
     *  inclusive uncertainty includes the entire range specified by
     *  the uncertainty units e.g. +/- 1 year includes all of the
     *  months and days within that interval. A non-inclusive
     *  uncertainty would mean the year is uncertain but the month and
     *  day is certain.
     *
     *  @return <code> true </code> if the uncertainty includes all dates,
     *          <code> false </code> otherwise
     */

    public boolean isUncertaintyDateInclusive() {
        return (this.uncertaintyDateInclusive);
    }


    /**
     *  Tests if the uncertainty is time inclusive. An inclusive uncertainty
     *  includes the entire range specified by the uncertainty units e.g. +/-
     *  1 year includes all of the seconds within that interval. A
     *  non-inclusive uncertainty would mean the year is uncertain but the
     *  time is certain.
     *
     *  @return <code> true </code> if the uncertainty includes all times,
     *          <code> false </code> otherwise
     */

    public boolean isUncertaintyTimeInclusive() {
        return (this.uncertaintyTimeInclusive);
    }

    
    /**
     *  Sets uncertainty where the positive and negative amounts are
     *  symmetrical.
     *
     *  @param units the uncertainty units
     *  @param amount the uncertainty amount
     *  @throws org.osid.InvalidArgumentException <code>amount</code>
     *          is negative
     *  @throws org.osid.NullArgumentException <code>units</code> is
     *          <code>null</code>
     */

    public void setUncertainty(org.osid.calendaring.DateTimeResolution units, long amount) {
        setUncertainty(units, amount, amount);
        return;
    }


    /**
     *  Sets uncertainty where the positive and negative amounts are
     *  symmetrical.
     *
     *  @param units the uncertainty units
     *  @param amount the uncertainty amount
     *  @param dateInclusive <code>true</code> if the uncertainty
     *         includes all dates in the range, <code>false</code> if
     *         not contiguous
     *  @param timeInclusive <code>true</code> if the uncertainty
     *         includes all times in the range, <code>false</code> if
     *         not contiguous
     *  @throws org.osid.InvalidArgumentException <code>amount</code>
     *          is negative
     *  @throws org.osid.NullArgumentException <code>units</code> is
     *          <code>null</code>
     */

    public void setUncertainty(org.osid.calendaring.DateTimeResolution units, long amount,
                               boolean dateInclusive, boolean timeInclusive) {
        setUncertainty(units, amount, amount, dateInclusive, timeInclusive);
        return;
    }


    /**
     *  Sets uncertainty where the positive and negative amounts are
     *  asymmetrical.
     *
     *  @param units the uncertainty units
     *  @param plus the uncertainty units in the positive direction
     *  @param minus the uncertainty units in the negative direction
     *  @throws org.osid.InvalidArgumentException <code>plus</code> or
     *          <code>minus</code> is negative
     *  @throws org.osid.NullArgumentException <code>units</code> is
     *          <code>null</code>
     */

    public void setUncertainty(org.osid.calendaring.DateTimeResolution units, long plus, long minus) {
        nullarg(units, "uncertainty units");
        cardinalarg(plus, "uncertainty plus");
        cardinalarg(minus, "uncertainty minus");

        this.uncertaintyUnits = units;
        this.uncertaintyPlus = plus;
        this.uncertaintyMinus = minus;

        return;
    }


    /**
     *  Sets uncertainty where the positive and negative amounts are
     *  asymmetrical.
     *
     *  @param units the uncertainty units
     *  @param plus the uncertainty units in the positive direction
     *  @param minus the uncertainty units in the negative direction
     *  @param dateInclusive <code>true</code> if the uncertainty
     *         includes all dates in the range, <code>false</code> if
     *         not contiguous
     *  @param timeInclusive <code>true</code> if the uncertainty
     *         includes all times in the range, <code>false</code> if
     *         not contiguous
     *  @throws org.osid.InvalidArgumentException <code>plus</code> or
     *          <code>minus</code> is negative
     *  @throws org.osid.NullArgumentException <code>units</code> is
     *          <code>null</code>
     */

    public void setUncertainty(org.osid.calendaring.DateTimeResolution units, long plus, long minus,
                               boolean dateInclusive, boolean timeInclusive) {
        nullarg(units, "uncertainty units");
        cardinalarg(plus, "uncertainty plus");
        cardinalarg(minus, "uncertainty minus");

        this.uncertaintyUnits = units;
        this.uncertaintyPlus = plus;
        this.uncertaintyMinus = minus;

        this.uncertaintyDateInclusive = dateInclusive;
        this.uncertaintyTimeInclusive = timeInclusive;

        return;
    }


    /**
     *  Clears the uncertainty.
     */

    public void clearUncertainty() {
        this.uncertaintyUnits = null;
        this.uncertaintyMinus = 0;
        this.uncertaintyPlus = 0;

        return;
    }


    /**
     *  Gets the time component of this specifier.
     *
     *  @return the time component
     */

    public abstract org.osid.calendaring.Time getTime();


    /**
     *  Sets the time components of this specifier.
     *
     *  @param time thetime to copy
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     *  @throws org.osid.UnsupportedException <code>time</code> is
     *          not supported
     */

    @SuppressWarnings("fallthrough")
    public void setTime(org.osid.calendaring.Time time) {
        nullarg(time, "time");
        if (!getTimeSpecifier().getTimeType().equals(time.getTimeType())) {
            throw new org.osid.UnsupportedException(time.getTimeType() + " is not supported");
        }
        

        switch (time.getGranularity()) {
        case PLANCKSECOND:
            setPlanckSeconds(time.getPlanckSeconds());
        case UNDASECOND:
            setUndaseconds((short) time.getUndaseconds());
        case VATOSECOND:
            setVatoseconds((short) time.getVatoseconds());
        case WEEBLESECOND:
            setWeebleseconds((short) time.getWeebleseconds());
        case XOXXOSECOND:
            setXoxxoseconds((short) time.getXoxxoseconds());
        case YOCTOSECOND:
            setYoctoseconds((short) time.getYoctoseconds());
        case ZEPTOSECOND:
            setZeptoseconds((short) time.getZeptoseconds());
        case ATTOSECOND:
            setAttoseconds((short) time.getAttoseconds());
        case FEMTOSECOND:
            setFemtoseconds((short) time.getFemtoseconds());
        case PICOSECOND:
            setPicoseconds((short) time.getPicoseconds());
        case NANOSECOND:
            setNanoseconds((short) time.getNanoseconds());
        case MICROSECOND:
            setMicroseconds((short) time.getMicroseconds());        
        case MILLISECOND:
            setMilliseconds((short) time.getMilliseconds());       
        case SECOND:
            setSecond(time.getSecond());
        case MINUTE:
            setMinute(time.getMinute());
        case HOUR:
            setHour(time.getHour());
        }

        return;
    }


    /**
     *  Gets the time specifier.
     *
     *  @return the time specifier
     */

    protected AbstractTimeSpecifier getTimeSpecifier() {
        return (this.timeSpecifier);
    }
}
