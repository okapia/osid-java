//
// AbstractType.java
//
//     Implements a basic Type.
//
//
// Tom Coppeto
// OnTapSolutions
// 29 June 2008
//
//
// Copyright (c) 2008 Massachusetts Institute of Technology. All Rights 
// Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.type.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.primordium.locale.text.eng.us.Plain.text;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An implementation of Type.
 */

public abstract class AbstractType
    implements org.osid.type.Type {

    private final org.osid.locale.DisplayText name;
    private final org.osid.locale.DisplayText label;
    private final org.osid.locale.DisplayText description;
    private final org.osid.locale.DisplayText domain;

    private final String authority;
    private final String namespace;
    private final String identifier;

    private String str;
    private int hash = 0;

    private static final long serialVersionUID = 1129633471L;


    /**
     *  Constructs a new <code>AbstractType</code>.
     *
     *  @param authority the authority of this Type
     *  @param namespace the identifier namespace of this Type
     *  @param identifier the identification component of this Type
     *  @throws org.osid.NullArgumentException <code>authority</code>,
     *          <code>namespace</code>, or <code>identifier</code> is
     *          <code>null</code>
     */

    protected AbstractType(String authority, String namespace, String identifier) {
        this(authority, namespace, identifier, text(""),
             text(formatDefaultName(authority, namespace, identifier)),
             text(formatDefaultName(authority, namespace, identifier)), 
             text("A Type"));
        
        return;
    }


    /**
     *  Constructs a new <code>AbstractType</code>.
     *
     *  @param authority the authority of this Type
     *  @param namespace the identifier namespace of this Type
     *  @param identifier the identification component of this Type
     *  @param domain the domain label for this Type
     *  @param name the display name for this Type
     *  @param label the display label for this Type
     *  @param description the description of this Type
     *  @throws org.osid.NullArgumentException <code>authority</code>,
     *          <code>namespace</code>, <code>identifier</code>,
     *          <code>domain</code>, <code>name</code>,
     *          <code>label</code>, or <code>description</code> is
     *          <code>null</code>
     */

    protected AbstractType(String authority, String namespace, String identifier, 
                           org.osid.locale.DisplayText domain, org.osid.locale.DisplayText name, 
                           org.osid.locale.DisplayText label, org.osid.locale.DisplayText description) {

        nullarg(authority, "authority");
        nullarg(namespace, "namespace");
        nullarg(identifier, "identifier");
        nullarg(domain, "domain");
        nullarg(name, "name");
        nullarg(label, "label");
        nullarg(description, "description");

        this.authority   = authority;
        this.namespace   = namespace;
        this.identifier  = identifier;        
        this.domain      = domain;
        this.name        = name;
        this.label       = label;
        this.description = description;

        return;
    }


    /**
     *  Gets the full display name of this <code> Type. </code>
     *
     *  @return the display name of this <code> Type </code>
     */

    @OSID @Override
    public final org.osid.locale.DisplayText getDisplayName() {
        return (this.name);
    }


    /**
     *  Gets the shorter display label for this <code> Type </code>.
     *  Where a display name of a <code> Type </code> might be
     *  "Critical Logging Priority Type", the display label could be
     *  "critical."
     *
     *  @return the display label for this <code> Type. </code> The
     *          display name is returned when there is no metadata
     *          availavle for this <code> Type. </code>
     */

    @OSID @Override
    public org.osid.locale.DisplayText getDisplayLabel() {
        return (this.label);
    }


    /**
     *  Gets a description of this <code> Type. </code>
     *
     *  @return the description of this <code> Type. </code> An empty string
     *          is returned when no description is available for this <code>
     *          Type. </code>
     */

    @OSID @Override
    public org.osid.locale.DisplayText getDescription() {
        return (this.description);
    }


    /**
     *  Gets the domain. The domain identifies application space of this Type.
     *  This method is used to compare one <code> Type </code> to another.
     *
     *  @return the domain of this <code> Type </code>
     */

    @OSID @Override
    public org.osid.locale.DisplayText getDomain() {
        return (this.domain);
    }


    /**
     *  Gets the authority of this <code>Type</code>. The authority is
     *  a string used to ensure the uniqueness of this <code> Type
     *  </code> when using a non-federated identifier
     *  space. Generally, it is a domain name identifying the party
     *  responsible for this <code>Type</code>. This method is used to
     *  compare one <code>Type</code> to another.
     *
     *  @return the authority of this <code> Type </code>
     */

    @OSID @Override
    public final String getAuthority() {
        return (this.authority);
    }


    /**
     *  Gets the namespace of the identifier. This method is used to compare 
     *  one <code> Type </code> to another. 
     *
     *  @return the authority of this <code> Type </code> 
     */

    @OSID @Override
    public final String getIdentifierNamespace() {
        return (this.namespace);
    }


    /**
     *  Gets the identifier of this <code>Type</code>. This method is used to 
     *  compare one <code> Type </code> to another. 
     *
     *  @return the identifier of this <code> Id </code> 
     */

    @OSID @Override
    public final String getIdentifier() {
        return (this.identifier);
    }


    /**
     *  Compares this type with the specified <code>Type</code> to
     *  determine the natural order. Returns a negative integer, zero,
     *  or a positive integer as this type is less than, equal to, or
     *  greater than the specified type.
     *
     *  The natural ordering is determined first by the natural
     *  ordering of the authority, namespace, and identifier.
     *
     *  If <code>compareTo</code> an type is zero, then
     *  <code>equals()</code> must be <code>true</code> and their hash
     *  codes must also be equal for consistent behavior. For
     *  orderings that may yield inconsistent behavior, an external
     *  <code>Comparator</code> should be used.
     *
     *  @param type the type to be compared
     *  @return a negative integer, zero, or a positive integer as
     *          this type is less than, equal to, or greater than
     *          the specified type
     *  @throws ClassCastException if the specified type's type
     *          prevents it from being compared to this type
     */

    @OSIDBinding @Override
    public int compareTo(org.osid.type.Type type) {
        if (this == type) {
            return (0);
        }

        int c;
        c = getAuthority().compareTo(type.getAuthority());
        if (c != 0) {
            return (c);
        }

        c = getIdentifierNamespace().compareTo(type.getIdentifierNamespace());
        if (c != 0) {
            return (c);
        }

        return (getIdentifier().compareTo(type.getIdentifier()));
    }


    /**
     *  Determines if the given <code> Type </code> is equal to this one. Two 
     *  Types are equal if the namespace, authority and identifier components 
     *  are equal. The identifier is case sensitive while the namespace and 
     *  authority strings are not case sensitive. 
     *
     *  @param  obj an object to compare
     *  @return <code> true </code> if the given object is equal to 
     *          this <code>Type</code>, <code> false </code> otherwise 
     */

    @OSIDBinding @Override
    public final boolean equals(Object obj) {
        if (obj == null) {
            return (false);
        }

        if (this == obj) {
            return (true);
        }

        if (!(obj instanceof org.osid.type.Type)) {
            return (false);
        }

        org.osid.type.Type type = (org.osid.type.Type) obj;
        if (getAuthority().equals(type.getAuthority()) && 
            getIdentifierNamespace().equals(type.getIdentifierNamespace()) &&
            getIdentifier().equals(type.getIdentifier())) {
            return (true);
        } else {
            return (false);
        }
    }


    /**
     *  Returns a hash code value for this <code>Type</code> based on
     *  the authority, namespace and identifier.
     *
     *  @return a hash code value for this object
     */

    @OSIDBinding @Override
    public final int hashCode() {
        if (this.hash == 0) {
            this.hash = calculateHashCode();
        }

        return (this.hash);
    }


    /**
     *  Returns a string representation of this Type.
     *
     *  All implementations are encouraged to supply this method to
     *  display a serialized Type.
     *
     *  @return a string
     */

    @OSIDBinding @Override
    public String toString() {
        if (this.str == null) {
            this.str = formatLabel();
        }

        return (this.str);
    }


    private int calculateHashCode() {
        int hash = 1;
        hash = hash * 31 + getAuthority().hashCode();
        hash = hash * 31 + getIdentifierNamespace().hashCode();
        hash = hash * 31 + getIdentifier().hashCode();

        return (hash);
    }


    /**
     *  Formats the toString label.
     *
     *  @return the formatted Type
     */
    
    protected abstract String formatLabel();


    /**
     *  Formats a default name if none supplied.
     *
     *  @param authority the type authority
     *  @param namespace the type namespace
     *  @param identifier the type identifier
     */

    public static String formatDefaultName(String authority, String namespace, String identifier) {
        return (namespace + ":" + identifier + "@" + authority);
    }
}
