//
// URNType.java
//
//     Defines a Type object using a URN inet scheme.
//
//
// Tom Coppeto
// Okapia
// 20 September 2013
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.type;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Type</code> using a URN. 
 *
 *  The URN must be under the osid nss. For example:
 *  urn:osid:okapia:types:genera:repository:Asset:JPEG
 *
 *  The serialization and deserialization through toString() and
 *  valueOf() of this Id uses the complete URN format. This Id does
 *  not serialize meta data such as the name, domain or description.
 *
 */

public final class URNType 
    extends net.okapia.osid.primordium.type.spi.AbstractType {

    private static final long serialVersionUID = -1440688556L;


    /**
     *  Creates a new <code>Type</code> without metadata.
     *
     *  @param authority the authority
     *  @param namespace the namespace 
     *  @param identifier the type identifier
     *  @throws org.osid.NullArgumentException <code>null</code> argument
     *          provided
     */

    public URNType(String authority, String namespace, String identifier) {
	super(authority, namespace, identifier);
	return;
    }


    /**
     *  Creates a new <code>URNType</code>.
     *
     *  @param authority the authority of this type identifier
     *  @param namespace the namespace of this type identifier
     *  @param identifier the type identifier
     *  @param domain the domain of this type
     *  @param name the name of this type
     *  @param label the label (short name) for this type
     *  @param description a description of this type
     *  @throws org.osid.NullArgumentException <code>null</code> argument
     *          provided
     */

    public URNType(String authority, String namespace, String identifier, 
                       org.osid.locale.DisplayText domain, org.osid.locale.DisplayText name, 
                       org.osid.locale.DisplayText label, org.osid.locale.DisplayText description) {

	super(authority, namespace, identifier, domain, name, label, description);
	return;
    }


    /*
     *  Parses a URN into its components. The URN is of the form:
     *  <code>urn:osid:<authority>:types:<namespace>:<identifier></code>.
     *
     *  @param urn the urn string to parse
     *  @throws org.osid.InvalidArgumentException if an error is
     *          encountered parsing the URN
     *  @throws org.osid.NullArgumentException <code>urn</code> is
     *          <code>null</code>.
     */

    public static org.osid.type.Type valueOf(String urn) {
	String authority;
        String namespace;
	String identifier = "";

	nullarg(urn, "urn");

	try {
	    String[] parts = urn.split(":");
	    if ((parts.length < 6) || !parts[0].equals("urn")) {
		throw new org.osid.InvalidArgumentException(urn + " is not valid");
	    }

	    String nid = parts[1];
	    if (!nid.equals("osid")) {
		throw new org.osid.InvalidArgumentException(urn + "nid is not osid");
	    }

	    authority = unescape(parts[2]);

	    if (!parts[3].equals("types")) {
		throw new org.osid.InvalidArgumentException(urn + " not a valid type");
	    }

	    namespace = unescape(parts[4]);

	    for (int i = 5; i < parts.length; i++) {
		if (i > 5) {
		    identifier += "/";
		}

		identifier += unescape(parts[i]);
	    }

	} catch (java.util.regex.PatternSyntaxException pse) {
	    throw new org.osid.InvalidArgumentException("error parsing " + urn, pse);
	}

	return (new URNType(authority, namespace, identifier));
    }


    /**
     *  Creates a new <code>URNType</code> from the authority and
     *  identifier components. This method can be used to wrap an
     *  <code>Type</code> of unknown origin that supports equality and
     *  hash codes based on the unique identifiers.
     *
     *  @param type a Type
     *  @throws org.osid.NullArgumentException <code>type</code> is
     *          <code>null</code>
     */

    public static org.osid.type.Type valueOf(org.osid.type.Type type) {
	nullarg(type, "type");

	return (new URNType(type.getAuthority(), type.getIdentifierNamespace(), 
                            type.getIdentifier(), type.getDomain(), type.getDisplayName(), 
                            type.getDisplayLabel(), type.getDescription()));
    }


    /**
     *  Converts an OSID Type into an inet URN. The resulting format
     *  is: <code>urn:inet:authority:identifier</code>.
     *
     *  @return formatted string
     */

    @Override
    protected String formatLabel() {
	return ("urn:osid:" + escapeAuthority(getAuthority()) + ":types:" + 
                escapeAuthority(getIdentifierNamespace()) + ":" + escape(getIdentifier()));
    }


    private static final java.util.Collection<Character> VALID_CHARS = java.util.Arrays.asList(
                                                                                               '(', ')', '+', ',', 
                                                                                               '-', '.', '=', '@', 
                                                                                               ';', '$', '_', '!', 
                                                                                               '*', '\'');


    /**
     *  Escapes the identifier for use in a URN using the rules in RFC
     *  2141. The string should not have any hex codes as they will be
     *  escaped a second time.
     *
     *  Valid characters in a URN include alphanumeric characters plus
     *  "(" | ")" | "+" | "," | "-" | "." | ":" | "=" | "@" | ";" | "$" |
     *  "_" | "!" | "*" | "'"
     *
     *  @param s the string to escape
     *  @return the escaped string
     *  @throws org.osid.NullArgumentException <code>s</code> is
     *          <code>null</code>
     */

    protected static String escape(String s) {
	nullarg(s, "string");

        StringBuilder sb = new StringBuilder(s.length());

        for (char c : s.toCharArray()) {
            if (Character.isLetterOrDigit(c)) {
                sb.append(c);
            } else if (c == '/') {
                sb.append(':');
            } else if (VALID_CHARS.contains(c)) {
                sb.append(c);
            } else {
                sb.append('%');
                sb.append(Integer.toHexString(c));
            }
        }
        
        return (sb.toString());
    }


    /**
     *  Escapes the authority by removing colons.
     *
     *  @param s the string to escape
     *  @return the escaped string
     *  @throws org.osid.NullArgumentException <code>s</code> is
     *          <code>null</code>
     */

    protected String escapeAuthority(String s) {
	nullarg(s, "string");

        StringBuilder sb = new StringBuilder(s.length());

        for (char c : s.toCharArray()) {
            if (Character.isLetterOrDigit(c)) {
                sb.append(c);
            } else if (VALID_CHARS.contains(c)) {
                sb.append(c);
            } else {
                sb.append('%');
                sb.append(Integer.toHexString(c));
            }
        }
        
        return (sb.toString());
    }


    /**
     *  Unescapes hex codes in string.
     *
     *  @param s the string to unescape
     *  @return the unescaped string
     *  @throws org.osid.NullArgumentException <code>s</code> is
     *          <code>null</code>
     */

    protected static String unescape(String s) {
        try {
            return (java.net.URLDecoder.decode(s, "UTF-8"));
        } catch (java.io.UnsupportedEncodingException uee) {
            throw new org.osid.OsidRuntimeException(uee);
        }
    }


    private static boolean isValidChar(char c) {
        for (char valid : VALID_CHARS) {
            if (c == valid) {
                return (true);
            }
        }

        return (false);
    }
}
