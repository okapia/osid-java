//
// AbstractDisplayText.java
//
//     An implementation of an OSID DisplayText.
//
//
// Tom Coppeto
// OnTapSolutions
// 12 May 2012
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.locale.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An implementation of an OSID DisplayText.
 */

public abstract class AbstractDisplayText
    implements org.osid.locale.DisplayText {

    private final org.osid.type.Type languageType;
    private final org.osid.type.Type scriptType;
    private final org.osid.type.Type formatType;
    private final String text;
    private int hash = 0;

    private static final long serialVersionUID = -2008508342L;


    /**
     *  Constructs a new <code>AbstractDisplayText</code>.
     *
     *  @param languageType the language type
     *  @param scriptType the language script type
     *  @param formatType the format type
     *  @param text the text
     *  @throws org.osid.NullArgumentException
     *          <code>languageType</code>, <code>scriptType</code>,
     *          <code>formatType</code>, <code>text</code> is
     *          <code>null</code>
     */

    protected AbstractDisplayText(org.osid.type.Type languageType, org.osid.type.Type scriptType,
                                  org.osid.type.Type formatType, String text) {

        nullarg(languageType, "language type");
        nullarg(scriptType, "script type");
        nullarg(formatType, "format type");

        this.languageType = languageType;
        this.scriptType   = scriptType;
        this.formatType   = formatType;
        this.text         = text;

        return;
    }


    /**
     *  Gets the language type.
     *
     *  @return the language type 
     */

    @OSID @Override
    public final org.osid.type.Type getLanguageType() {
        return (this.languageType);
    }


    /**
     *  Gets the script type.
     *
     *  @return the script type 
     */

    @OSID @Override
    public final org.osid.type.Type getScriptType() {
        return (this.scriptType);
    }


    /**
     *  Gets the format type.
     *
     *  @return the format type 
     */

    @OSID @Override
    public final org.osid.type.Type getFormatType() {
        return (this.formatType);
    }


    /**
     *  Gets the text.
     *
     *  @return the text
     */

    @OSID @Override
    public final String getText() {
        return (this.text);
    }


    /**
     *  Compares this DisplayText with the specified DisplayText for
     *  order. Returns a negative integer, zero, or a positive integer
     *  as this DisplayText is less than, equal to, or greater than the
     *  specified DisplayText.
     *
     *  This implementation provides a natural ordering based on the
     *  types and text. 
     *
     *  @param text the DisplayText to be compared
     *  @return a negative integer, zero, or a positive integer as
     *          this DisplayText is less than, equal to, or greater than
     *          the specified DisplayText
     *  @throws ClassCastException if the specified DisplayText's type
     *          prevents it from being compared to this DisplayText
     */

    @OSIDBinding @Override
    public int compareTo(org.osid.locale.DisplayText text) {
        if (this == text) {
            return (0);
        }

        int c;
        c = getLanguageType().compareTo(text.getLanguageType());
        if (c != 0) {
            return (c);
        }
        
        c = getScriptType().compareTo(text.getScriptType());
        if (c != 0) {
            return (c);
        }

        c = getScriptType().compareTo(text.getFormatType());
        if (c != 0) {
            return (c);
        }

        return (getText().compareTo(text.getText()));
    }


    /**
     *  Determines if the given <code> DisplayText </code> is equal to
     *  this one. 
     *
     *  Two DisplayText objects are equal if their DisplayText Types and
     *  DisplayText amounts, and the precision in the currency amounts
     *  are equal.
     *
     *  @param  obj an object to compare
     *  @return <code> true </code> if the given object is equal to
     *          this <code>DisplayText</code>, <code> false </code>
     *          otherwise
     */

    @OSIDBinding @Override
    public final boolean equals(Object obj) {
        if (obj == null) {
            return (false);
        }

        if (this == obj) {
            return (true);
        }

        if (!(obj instanceof org.osid.locale.DisplayText)) {
            return (false);
        }

        org.osid.locale.DisplayText dt = (org.osid.locale.DisplayText) obj;
        if (!getLanguageType().equals(dt.getLanguageType())) {
            return (false);
        }

        if (!getScriptType().equals(dt.getScriptType())) {
            return (false);
        }

        if (!getFormatType().equals(dt.getFormatType())) {
            return (false);
        }

        if (!getText().equals(dt.getText())) {
            return (false);
        }

        return (true);
    }


    /**
     *  Returns a hash code value for this <code>DisplayText</code>. The
     *  hash code is determined from the currency type and currency
     *  amount.
     *
     *  @return a hash code value for this object
     */

    @OSIDBinding @Override
    public final int hashCode() { 
        if (this.hash == 0) {
            this.hash = calculateHashCode();
        }
        
        return (this.hash);
    }


    /**
     *  Returns a string representation of this DisplayText.
     *
     *  All implementations are encouraged to supply this method to
     *  display the type and value.
     *
     *  @return a string
     */

    @OSIDBinding @Override
    public String toString() {
	return (getText());
    }


    private int calculateHashCode() {
        int hash = getLanguageType().hashCode();
        hash = hash * 31 + getScriptType().hashCode();
        hash = hash * 31 + getFormatType().hashCode();
        hash = hash * 31 + getText().hashCode();

	return (hash);        
    }
}
