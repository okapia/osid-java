//
// AbstractDistance
//
//     An implementation of Primitive for OSID Distance.
//
//
// Tom Coppeto
// OnTapSolutions
// 21 October 2009
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.mapping;

import org.osid.binding.java.annotation.OSID;
import org.osid.mapping.DistanceResolution;


/**
 *  An implementation of dPrimitive for OSID Distance to provide
 *  consistent Java Object behavior across OSID implementations. All
 *  OSID Distance implementations should be a descendent of this
 *  class.
 */

public abstract class AbstractDistance
    implements org.osid.mapping.Distance {

    private final java.math.BigDecimal meters;
    private final java.math.BigDecimal parsecs;
    private final java.math.BigDecimal lightyears;
    private final java.math.BigDecimal plancks;
    
    private java.math.BigDecimal upperBound;
    private java.math.BigDecimal lowerBound;
    
    private boolean hasUncertainty;    
    private String label;
    private int hash = 0;

    private static final java.math.BigDecimal METERS_IN_PLANCK     = new java.math.BigDecimal("1.616199e-35");
    private static final java.math.BigDecimal METERS_IN_LIGHTYEAR  = new java.math.BigDecimal("9.4607304725808e15");
    private static final java.math.BigDecimal METERS_IN_PARSEC     = new java.math.BigDecimal("3.085678e16");
    private static final java.math.BigDecimal LIGHTYEARS_IN_PARSEC = new java.math.BigDecimal("2.71542689");

    
    /**
     *  Tests if this Distance is greater than the given Distance. A
     *  greater distance is one whose values minus the uncertainty is
     *  greater than another plus its uncertainty. An uncertainty of
     *  infinity is undefined and returns false.
     *
     *  @param  distance the distance to compare
     *  @return <code>true</code> if this distance is larger,
     *          <code>false</code> otherwise
     */

    @Override
    public boolean isGreater(org.osid.mapping.Distance distance) {
	if (distance == null) {
	    return (false);
	}

	if (getUncertaintyUnits() == DistanceResolution.INFINITY) {
	    return (false);
	}

	if (distance.getUncertaintyUnits() == DistanceResolution.INFINITY) {
	    return (false);
	}

	if (compare(getLowerBound(), distance.getUpperBound()) > 0) {
	    return (true);
	} else {
	    return (false);
	}	
    }


    /**
     *  Tests if this distance is less than the given distance. A
     *  lesser distance is one whose values plus its uncertainty is
     *  less than another minus its uncertainty. An uncertainty of
     *  infinity is undefined and returns false.
     *
     *  @param  distance the distance to compare
     *  @return <code>true</code> if this distance is less,
     *          <code>false</code> otherwise
     */

    @Override
    public boolean isLess(org.osid.mapping.Distance distance) {
	if (distance == null) {
	    return (false);
	}

	if (getUncertaintyUnits() == DistanceResolution.INFINITY) {
	    return (false);
	}

	if (distance.getUncertaintyUnits() == DistanceResolution.INFINITY) {
	    return (false);
	}

	if (compare(getUpperBound(), distance.getLowerBound()) < 0) {
	    return (true);
	} else {
	    return (false);
	}	
    }


    /**
     *  Tests if the range of this distance, as specified by its
     *  uncertainty, includes the given distance ranged by its
     *  uncertainty.
     *
     *  If either distance has an uncertainty of infinity this method
     *  returns false.
     *
     *  @param  distance the distance to compare
     *  @return <code>true</code> if this distance is included,
     *          <code>false</code> otherwise
     */
    
    @Override
    public boolean isInclusive(org.osid.mapping.Distance distance) {
	return (isContained(distance, distance));
    }
    

    /**
     *  Tests if the range of this distance, as specified by its
     *  uncertainty, is included within the given two distances.
     *
     *  If either distance has an uncertainty of infinity this method
     *  returns false.
     *
     *  @param  distance1 the start distance to compare
     *  @param  distance2 the end distance to compare
     *  @return <code>true</code> if this distance is included,
     *          <code>false</code> otherwise
     */

    @Override
    public boolean isContained(org.osid.mapping.Distance distance1, org.osid.mapping.Distance distance2) {
	if ((distance1 == null) || (distance2 == null))  {
	    return (false);
	}

	if (getUncertaintyUnits() == DistanceResolution.INFINITY) {
	    return (false);
	}

	if (distance1.getUncertaintyUnits() == DistanceResolution.INFINITY) {
	    return (false);
	}

	if (distance2.getUncertaintyUnits() == DistanceResolution.INFINITY) {
	    return (false);
	}

	if (distance1.isGreater(distance2)) {
	    return (isContained(distance1, distance2));
	}

	if ((compare(getLowerBound(), distance1.getLowerBound()) >= 0) &&
	    (compare(getUpperBound(), distance2.getUpperBound()) <= 0)) {
	    return (true);
	} else {
	    return (false);
	}		
    }


    /**
     *  Tests if the range of this distance, as specified by its
     *  uncertainty, does not overlap the given distance.
     *
     *  If either distance has an uncertainty of infinity this method
     *  returns false.
     *
     *  @param  distance the distance to compare
     *  @return <code>true</code> if this distance is excluded,
     *          <code>false</code> otherwise
     */

    @Override
    public boolean isExclusive(org.osid.mapping.Distance distance) {
	if (isLess(distance) || isGreater(distance)) {
	    return (true);
	} else {
	    return (false);
	}
    }


    /**
     *  Tests if the range of this distance, as specified by its
     *  uncertainty, does not overlap with the range of the given two
     *  distances.
     *
     *  If either distance has an uncertainty of infinity this method
     *  returns false.
     *
     *  @param  distance1 the start distance to compare
     *  @param  distance2 the end distance to compare
     *  @return <code>true</code> if this distance is excluded,
     *          <code>false</code> otherwise
     */

    @Override
    public boolean isExclusive(org.osid.mapping.Distance distance1, org.osid.mapping.Distance distance2) {
	if (distance1.isGreater(distance2)) {
	    return (isExclusive(distance1, distance2));
	}

	if (isLess(distance1) || isGreater(distance2)) {
	    return (true);
	} else {
	    return (false);
	}
    }


    /**
     *  Gets the lower bound of the distance. The lower bound is the
     *  distance minus the uncertainty.
     *
     *  @return the distance lower bound
     */

    @Override
    public abstract org.osid.mapping.Distance getLowerBound();


    /**
     *  Gets the upper bound of the distance. The lower bound is the
     *  distance plus the uncertainty.
     *
     *  @return the distance upper bound
     */

    @Override
    public abstract org.osid.mapping.Distance getUpperBound();


    /**
     *  Determines if the given <code> distance </code> is equal to this one.
     *
     *  @param  obj an object to compare
     *  @return <code> true </code> if the given object is equal to
     *          this <code>distance</code>, <code> false </code>
     *          otherwise
     */

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return (false);
        }

        if (this == obj) {
            return (true);
        }

        if (!(obj instanceof org.osid.mapping.Distance)) {
            return (false);
        }

        org.osid.mapping.Distance distance = (org.osid.mapping.Distance) obj;

	if (getGranularity() != distance.getGranularity()) {
	    return (false);
	}

	if (getGranularity() == DistanceResolution.INFINITY) {
	    return (true);
	}

	if (definesUncertainty() != distance.definesUncertainty()) {
	    return (false);
	}

	if (definesUncertainty() && distance.definesUncertainty()) {
	    if (getUncertaintyUnits() != distance.getUncertaintyUnits()) {
		return (false);
	    }
	    
	    if (getUncertaintyMinus() != distance.getUncertaintyMinus()) {
		return (false);
	    }

	    if (getUncertaintyPlus() != distance.getUncertaintyPlus()) {
		return (false);
	    }
	}

        /*
         * this is a silly interface. all the resolutions are absolute.
         */

	if (getMeters().equals(distance.getMeters())) {
	    return (true);
	}

	return (false);
    }


    /**
     *  Compares this Distance with the specified Distance for
     *  ordering.
     *
     *  @param obj the object to be compared
     *  @return a negative integer, zero, or a positive integer as
     *          this object is less than, equal to, or greater than
     *          the specified object
     *  @throws org.osid.UnsupportedException cannot compare distance
     */

    @Override
    public int compareTo(Object obj) {
        if (this == obj) {
            return (0);
        }

        if (!(obj instanceof org.osid.mapping.Distance)) {
            throw new ClassCastException("object not a Distance");
        }

	return (compare(this, (org.osid.mapping.Distance) obj));	
    }


    /**
     *  Compares two normalized distances for their natural order.
     *
     *  @param  distance a <code>distance</code> to compare
     *  @return <code> 1 </code> if this is greater than
     *          <code>distance</code>, -1 if this is less than
     *          <code>distance</code>, 0 if equal
     */

    protected static int compare(org.osid.mapping.Distance distance1, org.osid.mapping.Distance distance2) {
	if (distance1.getGranularity() != distance2.getGranularity()) {
	    return (distance1.getGranularity().compareTo(distance2.getGranularity()));
	}

	return (distance1.getMeters().compareTo(distance2.getMeters()));
    }


    /**
     *  Returns a hash code value for this <code>distance</code> based
     *  on the authority, namespace and identifier.
     *
     *  @return a hash code value for this object
     */

    @Override
    public final int hashCode() { 
        return (getMeters().hashCode());
    }
     

    /**
     *  Returns a string representation of this distance.
     *
     *  @return a string
     */

    @Override
    public String toString() {
	switch (getGranularity()) {
	case INFINITY:
	    return ("undefined");
	case YOTTAPARSEC:
	    return (getYottaParsecs() + "Ypc");
	case EXAPARSEC:
	    return (getExaParsecs() + "Epc");
	case GIGAPARSEC:
	    return (getGigaParsecs() + "Gm");
	case YOTTAMETER:
	    return (getYottameters() + "Ym");
	case ZETTAMETER:
	    return (getZettameters() + "Zm");
	case EXAMETER:
	    return (getExameters() + "Em");
	case PARSEC:
	    return (getParsecs() + "pc");
	case LIGHTYEAR:
	    return (getLightYears() + "ly");
	case PETAMETER:
	    return (getPetameters() + "Pm");
	case TERAMETER:
	    return (getTerameters() + "Tm");
	case GIGAMETER:
	    return (getGigameters() + "Gm");
	case MEGAMETER:
	    return (getMegameters() + "Mm");
	case KILOMETER:
	    return (getKilometers() + "km");
            /*	case DECAMETER:
                return (getMeters()/10 + "dam");*/
	case METER:
	    return (getMeters() + "m");
            /*
	case DECIMETER:
        return (getCentimeters()/10 + "dm");*/
	case CENTIMETER:
	    return (getCentimeters() + "cm");
	case MILLIMETER:
	    return (getMillimeters() + "mm");
	case MICRON:
	    return (getMicrons() + "&#181;");
	case NANOMETER:
	    return (getNanometers() + "nm");
	case ANGSTROM:
	    return (getAngstroms() + "&#8491;");
	case PICOMETER:
	    return (getPicometers() + "pm");
	case FEMTOMETER:
	    return (getFemtometers() + "fm");
	case ATTOMETER:
	    return (getAttometers() + "am");
	case ZEPTOMETER:
	    return (getZeptometers() + "zm");
	case YOCTOMETER:
	    return (getYoctometers() + "ym");
            /*	case XOXXOMETER:
	    return ((getYoctometers() * 1000)  + "xm");
	case WEEBLEMETER:
        return ((getYoctometers() * 100000) + "wm");*/
	case PLANCK:
	    return (getPlancks() + "ℓP");
	default:
	    return (super.toString());
	}
    }


    private String formatDigits(long num, int n) {
	StringBuilder sb = new StringBuilder(n);
	sb.append(num);
	if (sb.length() < n) {
	    for (int i = 0; i < n - sb.length(); i++) {
		sb.insert(0, " ");
	    }
	}
	
	return (sb.toString());
    }


    private void normalizeAmount() {
        org.osid.mapping.DistanceResolution granularity = getGranularity();

        if (granularity == org.osid.mapping.DistanceResolution.INFINITY) {
            normalizeByInfinity();
        } else if (granularity.ordinal() >= org.osid.mapping.DistanceResolution.GIGAPARSEC) {
            normalizeByParsecs();
        } else if (granularity.ordinal() >= org.osid.mapping.DistanceResolution.EXAMETER) {
            normalizeByMeters();
        } else if (granularity.ordinal() == org.osid.mapping.DistanceResolution.PARSEC) {
            normalizeByParsecs();
        } else if (granularity.ordinal() == org.osid.mapping.DistanceResolution.LIGHTYEAR) {
            normalizeByLightYears();
        } else if (granularity.ordinal() >= org.osid.mapping.DistanceResolution.WEEBLEMETER) {
            normalizeByMeters();
        } else if (granularity.ordinal() >= org.osid.mapping.DistanceResolution.PLANCK) {
            normalizeByPlancks();
        }

        throw new org.osid.IllegalStateException("unknown granularity: " + granularity.getDisplayName());
    }


    private void normalizeByInfinity() {        
        this.parsecs   = new java.math.BigDecimal(0);
        this.lighyears = new java.math.BigDecimal(0);
        this.meters    = new java.math.BigDecimal(0);
        this.plancks   = new java.math.BigDecimal(0);

        return;
    }

    
    private void normalizeByParsecs() {
        switch (getGranularity()) {
        case YOTTAPARSEC:
            this.parsecs    = new java.math.BigDecimal(getAmount()).multiply(new java.math.BigDecimal("1e24"));
            break;
        case EXAPARSEC:
            this.parsecs    = new java.math.BigDecimal(getAmount()).multiply(new java.math.BigDecimal("1e18"));
            break;
        case GIGAPARSEC:
            this.parsecs    = new java.math.BigDecimal(getAmount()).multiply(new java.math.BigDecimal("1e9"));
            break;
        case PARSEC:
            this.parsecs    = new java.math.BigDecimal(getAmount());
            break;
        default:
            throw new org.osid.IllegalStateException("unexpected " + getGranularity());
        }

        this.lightyears = this.parsecs.multiply(LIGHTYARS_IN_PARSEC);
        this.meters     = this.parsecs.multiply(METERS_IN_PARSEC);
        this.plancks    = this.meters.divide(METERS_IN_PLANCK, this.precision);

        return;
    }


    private void normalizeByLightYears() {
        switch (getGranularity()) {
        case LIGHTYEAR:
            this.lightyears    = new java.math.BigDecimal(getAmount());
            break;
        default:
            throw new org.osid.IllegalStateException("unexpected " + getGranularity());
        }

        this.parsecs    = this.lightyears.divide(LIGHTYARS_IN_PARSEC, this.precision);
        this.meters     = this.lightyears.multiply(METERS_IN_LIGHYEAR);
        this.plancks    = this.meters.divide(METERS_IN_PLANCK, this.precision);

        return;
    }


    private void normalizeByMeters() {
        switch (getGranularity()) {
        case YOTTAMETER:
            this.meters    = new java.math.BigDecimal(getAmount()).multiply(new java.math.BigDecimal("1e24"));
            break;
        case ZETTAMETER:
            this.meters    = new java.math.BigDecimal(getAmount()).multiply(new java.math.BigDecimal("1e21"));
            break;
        case EXAMETER:
            this.meters    = new java.math.BigDecimal(getAmount()).multiply(new java.math.BigDecimal("1e18"));
            break;
        case PETAMETER:
            this.meters    = new java.math.BigDecimal(getAmount()).multiply(new java.math.BigDecimal("1e15"));
            break;
        case TERAMETER:
            this.meters    = new java.math.BigDecimal(getAmount()).multiply(new java.math.BigDecimal("1e12"));
            break;
        case GIGAMETER:
            this.meters    = new java.math.BigDecimal(getAmount()).multiply(new java.math.BigDecimal("1e9"));
            break;
        case MEGAMETER:
            this.meters    = new java.math.BigDecimal(getAmount()).multiply(new java.math.BigDecimal("1e6"));
            break;
        case KILOMETER:
            this.meters    = new java.math.BigDecimal(getAmount()).multiply(new java.math.BigDecimal("1e3"));
            break;
        case DECAMETER:
            this.meters    = new java.math.BigDecimal(getAmount()).multiply(new java.math.BigDecimal("1e1"));
            break;
        case METER:
            this.meters    = new java.math.BigDecimal(getAmount());
            break;
        case DECIMETER:
            this.meters    = new java.math.BigDecimal(getAmount()).divide(new java.math.BigDecimal("1e1"));
            break;
        case CENTIIMETER:
            this.meters    = new java.math.BigDecimal(getAmount()).divide(new java.math.BigDecimal("1e2"));
            break;
        case MILLIMETER:
            this.meters    = new java.math.BigDecimal(getAmount()).divide(new java.math.BigDecimal("1e3"));
            break;
        case MICRON:
            this.meters    = new java.math.BigDecimal(getAmount()).divide(new java.math.BigDecimal("1e6"));
            break;
        case NANOMETER:
            this.meters    = new java.math.BigDecimal(getAmount()).divide(new java.math.BigDecimal("1e9"));
            break;
        case ANGSTROM:
            this.meters    = new java.math.BigDecimal(getAmount()).divide(new java.math.BigDecimal("1e10"));
            break;
        case PICOMETER:
            this.meters    = new java.math.BigDecimal(getAmount()).divide(new java.math.BigDecimal("1e12"));
            break;
        case FEMTOMETER:
            this.meters    = new java.math.BigDecimal(getAmount()).divide(new java.math.BigDecimal("1e15"));
            break;
        case ATTOMETER:
            this.meters    = new java.math.BigDecimal(getAmount()).divide(new java.math.BigDecimal("1e21"));
            break;
        case YOCTOMETER:
            this.meters    = new java.math.BigDecimal(getAmount()).divide(new java.math.BigDecimal("1e24"));
            break;
        case XOXXOMETER:
            this.meters    = new java.math.BigDecimal(getAmount()).divide(new java.math.BigDecimal("1e27"));
            break;
        case WEEBLEMETER:
            this.meters    = new java.math.BigDecimal(getAmount()).divide(new java.math.BigDecimal("1e30"));
            break;
        default:
            throw new org.osid.IllegalStateException("unexpected " + getGranularity());
        }

        this.parsecs    = this.meters.divide(METERS_IN_PARSEC, this.precision);
        this.lightyears = this.meters.divide(METERS_IN_LIGHYEAR, this.precision);
        this.plancks    = this.meters.divide(METERS_IN_PLANCK, this.precision);

        return;
    }


    private void normalizeByPlancks() {
        switch (getGranularity()) {
        case PLANCK:
            this.plancks    = new java.math.BigDecimal(getAmount());
            break;
        default:
            throw new org.osid.IllegalStateException("unexpected " + getGranularity());
        }

        this.meters     = this.plancks.multiply(METERS_IN_PLANCK);
        this.parsecs    = this.meters.divide(METERS_IN_PARSEC, this.precision);
        this.lightyears = this.meters.divide(METERS_IN_LIGHTYEAR, this.precision);

        return;
    }
}
