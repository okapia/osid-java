//
// ISOAncientLanguageTypes.java
//
//     An enumeration of ISO 639-3 ancient languages.
//
//
// Tom Coppeto
// Okapia
// 25 April 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.types.text;


/**
 *  An enumeration ofISO 639-3 ancient languages.
 */

public enum ISOAncientLanguageTypes
    implements net.okapia.osid.torrefacto.collect.TypeEnum {

    AKK ("Akkadian"),
    ARC ("Official Aramaic (700-300 BCE)"),
    AVE ("Avestan"),
    CHU ("Church Slavi"),
    CMS ("Messapic"),
    ECR ("Eteocretan"),
    ECY ("Eteocypriot"),
    EGY ("Egyptian (Ancient)"),
    ELX ("Elamite"),
    ETT ("Etruscan"),
    GEZ ("Geez"),
    GMY ("Mycenaean Greek"),
    GOT ("Gothic"),
    HIT ("Hittite"),
    HLU ("Hieroglyphic Luwian"),
    HTX ("Middle Hittite"),
    IMS ("Marsian"),
    IMY ("Milyan"),
    INM ("Minaean"),
    KAW ("Kawi"),
    KHO ("Khotanese"),
    LAB ("Linear A"),
    LAT ("Lati"),
    LNG ("Langobardic"),
    NEI ("Neo-Hittite"),
    NRC ("Nori"),
    NRP ("North Picene"),
    NXM ("Numidian"),
    OAR ("Old Aramaic (up to 700 BCE)"),
    OBM ("Moabite"),
    OCH ("Old Chinese"),
    OHT ("Old Hittite"),
    OMN ("Minoan"),
    OOS ("Old Ossetic"),
    OSC ("Osca"),
    OTY ("Old Tamil"),
    PAL ("Pahlavi"),
    PGL ("Primitive Irish"),
    PGN ("Paelignian"),
    PHN ("Phoenician"),
    PLI ("Pali"),
    PLQ ("Palaic"),
    PYX ("Pyu (Myanmar)"),
    SAN ("Sanskrit"),
    SBV ("Sabine"),
    SCX ("Sice"),
    SOG ("Sogdian"),
    SPX ("South Picene"),
    SUX ("Sumerian"),
    SXC ("Sicanian"),
    SXO ("Sorothaptic"),
    TXB ("Tokharian B"),
    TXG ("Tangut"),
    TXH ("Thracian"),
    TXR ("Tartessian"),
    UGA ("Ugaritic"),
    UMC ("Marrucinian"),
    XAE ("Aequian"),
    XAQ ("Aquitanian"),
    XBC ("Bactrian"),
    XCC ("Camunic"),
    XCE ("Celtiberian"),
    XCG ("Cisalpine Gaulish"),
    XCO ("Chorasmian"),
    XCR ("Carian"),
    XDC ("Dacian"),
    XDM ("Edomite"),
    XEB ("Ebla"),
    XEP ("Epi-Olmec"),
    XFA ("Faliscan"),
    XHA ("Harami"),
    XHD ("Hadrami"),
    XHR ("Hernican"),
    XHT ("Hattic"),
    XHU ("Hurrian"),
    XIB ("Iberian"),
    XIL ("Illyrian"),
    XIV ("Indus Valley"),
    XLC ("Lycian"),
    XLD ("Lydian"),
    XLE ("Lemnian"),
    XLG ("Ligurian (Ancient)"),
    XLI ("Liburnian"),
    XLN ("Alanic"),
    XLP ("Lepontic"),
    XLS ("Lusitanian"),
    XLU ("Cuneiform Luwian"),
    XLY ("Elymian"),
    XME ("Median"),
    XMK ("Ancient Macedonian"),
    XMR ("Meroitic"),
    XNA ("Ancient North Arabia"),
    XPG ("Phrygian"),
    XPR ("Parthian"),
    XPU ("Puni"),
    XQT ("Qatabanian"),
    XRR ("Raetic"),
    XSA ("Sabaean"),
    XSC ("Scythian"),
    XSD ("Sidetic"),
    XTG ("Transalpine Gaulish"),
    XTO ("Tokharian A"),
    XTR ("Early Tripur"),
    XUM ("Umbrian"),
    XUR ("Urartian"),
    XVE ("Venetic"),
    XVN ("Vandalic"),
    XVO ("Volscian"),
    XVS ("Vestinian"),
    XZH ("Zhang-Zhung"),
    YMS ("Mysian"),
    ZSK ("Kaskean");

    private final String name;
    private org.osid.type.Type type;

    ISOAncientLanguageTypes(String name) {
        this.name = name;
        return;
    }


    /**
     *  Gets the Type.
     *
     *  @return the Type
     */

    public org.osid.type.Type getType() {
        if (this.type == null) {
            this.type = new net.okapia.osid.primordium.types.text.util.LazyTextTypeBuilder()
                .authority("iso.org")
                .namespace("639-3")
                .identifier(name())
                .domain("Locale Languages")
                .name(name + " Language")
                .label(name)
                .description("The language type for " +  name + ".")
                .build();            
        }

        return (this.type);
    }
}

