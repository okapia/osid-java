//
// AlternateCalendarTypes.java
//
//     An enumeration of Alternate Calendar Types.
//
//
// Tom Coppeto
// Okapia
// 5 April 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.types.calendar;

import static net.okapia.osid.primordium.locale.text.eng.us.Plain.text;


/**
 *  An enumeration of Alternate Calendar Types.
 */

public enum AlternateCalendarTypes
    implements net.okapia.osid.torrefacto.collect.TypeEnum {
 
        /** 360-day Calendar */
        C360 ("360-day"),

        /** Astronomical Calendar */
        ASTRONOMICAL ("Astronomical"),

        /** Colonial Calendar */
        COLONIAL ("Colonial", "Battlestar Galactica"),

        /** Common-Civil-Calendar-and-Time */
        COMMON_CIVIL ("CommonCivil", "Common-Civil-Calendar-and-Time"),

        /** Darian Calendar */
        DARIAN ("Darian"),

        /** Discworld Calendar */
        DISCWORLD ("Discworld"),

        /** Hanke-Henry Permanent Calendar */
        HANKE_HENRY ("Hanke-Henry", "Hanke-Henry Permanent"),

        /** Holocene Calendar */
        HOLOCENE ("Holocene"),

        /** InternationalFixed Calendar */
        INTERNATIONAL_FIXED ("InternationalFixed", "International Fixed"),

        /** MiddleEarth Calendar */
        MIDDLE_EARTH ("MiddleEarth", "Middle-earth"),

        /** Pax Calendar */
        PAX ("Pax"),

        /** Positivist Calendar */
        POSITIVIST ("Positivist"),

        /** Stardate Calendar */
        STARDATE ("Stardate"),

        /** Symmetry454 Calendar */
        SYMMETRY454 ("Symmetry454"),

        /** Tranquility Calendar */
        TRANQUILITY ("Tranquility"),

        /** World Calendar */
        WORLD ("World"),

        /** WorldSeason Calendar */
        WORLD_SEASON ("WorldSeason", "World Season");
        
    private final org.osid.type.Type type;
    

    AlternateCalendarTypes(String name) {
        this(name, name);
        return;
    }


    AlternateCalendarTypes(String identifier, String name) {
	this.type = new net.okapia.osid.primordium.type.BasicTypeBuilder()
            .authority("okapia.net")
            .namespace("calendar")
            .identifier(identifier)
            .domain(text("Alternate Calendar Types"))
            .name(text(name + " Calendar"))
            .label(text(name))
            .description(text("The calendar Type for the " +  name + " calendar."))
            .build();

	return;
    }
    

    /**
     *  Gets the Type.
     *
     *  @return the Type
     */

    public org.osid.type.Type getType() {
	return (this.type);
    }
}
