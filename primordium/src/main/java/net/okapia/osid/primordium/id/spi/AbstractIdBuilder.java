//
// AbstractId.java
//
//     Implements a basic Id.
//
//
// Tom Coppeto
// OnTapSolutions
// 29 April 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.id.spi;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An Id Builder.
 */

public abstract class AbstractIdBuilder<T extends AbstractIdBuilder<T>> {
    
    private String authority;
    private String namespace;
    private String identifier;
    
    
    /**
     *  Constructs a new AbstractIdBuilder.
     */
    
    protected AbstractIdBuilder() {
        return;
    }
   
    
    /**
     *  Constructs a new AbstractIdBuilder from another Id.
     *
     *  @param id an Id to copy
     *  @throws org.osid.NullArgumentException <code>id</code>
     *          is <code>null</code>
     */
    
    public AbstractIdBuilder(org.osid.id.Id id) {
        nullarg(id, "id");
        
        this.authority   = id.getAuthority();
        this.namespace   = id.getIdentifierNamespace();
        this.identifier  = id.getIdentifier();
        
        return;
    }

        
    /**
     *  Sets the authority for an Id.
     *
     *  @param authority the Id authority
     *  @return the basic id builder
     *  @throws org.osid.NullArgumentException
     *          <code>authority</code> is <code>null</code>
     */
    
    public T authority(String authority) {
        nullarg(authority, "authority");
        this.authority = authority;
        return (self());
    }
    
    
    protected String getAuthority() {
        return (this.authority);
    }
    
    
    /**
     *  Sets the namespace for an Id.
     *
     *  @param namespace the Id namespace
     *  @return the basic id builder
     *  @throws org.osid.NullArgumentException
     *          <code>namespace</code> is <code>null</code>
     */
    
    public T namespace(String namespace) {
        nullarg(namespace, "namespace");
        this.namespace = namespace;
        return (self());
    }
    
    
    protected String getNamespace() {
        return (this.namespace);
    }
    
    
    /**
     *  Sets the identifier for an Id.
     *
     *  @param identifier the Id identifier
     *  @return the basic id builder
     *  @throws org.osid.NullArgumentException
     *          <code>identifier</code> is <code>null</code>
     */
    
    public T identifier(String identifier) {
        nullarg(identifier, "identifier");
        this.identifier = identifier;
        return (self());
    }
    
    
    protected String getIdentifier() {
        return (this.identifier);
    }


    /**
     *  Gets the reference to this instance of the builder.
     *
     *  @return ObjectBuilder
     */

    protected abstract T self();


    /**
     *  Builds the BasicId. The Id authority, namespace, and
     *  identifier must have been set.
     *
     *  @return a new Id
     *  @throws org.osid.IllegalStateException Id cannot be
     *          constructed
     */
    
    public abstract org.osid.id.Id build();    
}
