//
// AbstractTime.java
//
//     An implementation of an OSID Time.
//
//
// Tom Coppeto
// Okapia
// 21 October 2009
//
// Copyright (c) 2009,2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;
import org.osid.calendaring.DateTimeResolution;

import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An implementation of OSID Time. This is an abstract shell
 *  implementation that supports the storage of the time elements.
 *
 *  The TimeSpecifier is a built-in utility to aid in the construction
 *  of Times. This inner class may also be subclassed to implement
 *  constraints based on the Time Types.
 */

public abstract class AbstractTime 
    implements org.osid.calendaring.Time {

    private final org.osid.type.Type timeType;
    private final long  hour;
    private final long  minute;
    private final long  second;
    private final short millisecond;
    private final short microsecond;
    private final short nanosecond;
    private final short picosecond;
    private final short femtosecond;
    private final short attosecond;
    private final short zeptosecond;
    private final short yoctosecond;
    private final short xoxxosecond;
    private final short weeblesecond;
    private final short vatosecond;
    private final short undasecond;
    private final long  plancks;

    private final org.osid.calendaring.DateTimeResolution granularity;
    private final long multiplier;

    protected static final int PLANCKS_IN_UNDASECOND = 185_509_483;

    private static final long serialVersionUID = -1807270802L;


    /**
     *  Constructs a new <code>AbstractTime</code> for an undefined
     *  time.
     *
     *  @param timeType the type of Time
     *  @throws org.osid.NullArgumentException <code>factory</code>,
     *          <code>timeType</code> is <code>null</code>
     */

    protected AbstractTime(org.osid.type.Type timeType) {

        nullarg(timeType, "time type");

        this.timeType     = timeType;
        this.granularity  = DateTimeResolution.INFINITY;
        this.multiplier   = 1;

        this.hour         = 0;
        this.minute       = 0;
        this.second       = 0;
        this.millisecond  = 0;
        this.microsecond  = 0;
        this.nanosecond   = 0;
        this.picosecond   = 0;
        this.femtosecond  = 0;
        this.attosecond   = 0;
        this.zeptosecond  = 0;
        this.yoctosecond  = 0;
        this.xoxxosecond  = 0;
        this.weeblesecond = 0;
        this.vatosecond   = 0;
        this.undasecond   = 0;
        this.plancks      = 0;

        return;
    }


    /**
     *  Constructs a new <code>AbstractTime</code> with second
     *  resolution.
     *
     *  @param timeType the type of Time
     *  @param hour the hour
     *  @param minute the minute
     *  @param second the second
     *  @throws org.osid.InvalidArgumentException <code>hour</code>,
     *          <code>minutes</code>, or <code>second</code> is
     *          negative
     *  @throws org.osid.NullArgumentException <code>timeType</code>
     *          is <code>null</code>
     */

    protected AbstractTime(org.osid.type.Type timeType, long hour, long minute, long second) {

        nullarg(timeType, "time type");
        cardinalarg(hour, "hour");
        cardinalarg(minute, "minute");
        cardinalarg(second, "second");

        this.timeType     = timeType;
        this.hour         = hour;
        this.minute       = minute;
        this.second       = second;
        this.granularity  = DateTimeResolution.SECOND;
        this.multiplier   = 1;

        this.millisecond  = 0;
        this.microsecond  = 0;
        this.nanosecond   = 0;
        this.picosecond   = 0;
        this.femtosecond  = 0;
        this.attosecond   = 0;
        this.zeptosecond  = 0;
        this.yoctosecond  = 0;
        this.xoxxosecond  = 0;
        this.weeblesecond = 0;
        this.vatosecond   = 0;
        this.undasecond   = 0;
        this.plancks      = 0;

        return;
    }


    /**
     *  Constructs a new <code>AbstractTime</code>.
     *
     *  @param specifier a time specifier
     *  @throws org.osid.NullArgumentException <code>specifier</code>
     *          is <code>null</code>
     */

    protected AbstractTime(AbstractTimeSpecifier specifier) {

        nullarg(specifier, "time specifier");

        if (specifier.getGranularity().ordinal() > org.osid.calendaring.DateTimeResolution.HOUR.ordinal()) {
            this.granularity = org.osid.calendaring.DateTimeResolution.INFINITY;
        } else {
            this.granularity = specifier.getGranularity();
        }

        this.timeType     = specifier.getTimeType();
        this.multiplier   = specifier.getGranularityMultiplier();

        this.hour         = specifier.getHour();
        this.minute       = specifier.getMinute();
        this.second       = specifier.getSecond();

        this.millisecond  = (short) specifier.getMilliseconds();
        this.microsecond  = (short) specifier.getMicroseconds();
        this.nanosecond   = (short) specifier.getNanoseconds();
        this.picosecond   = (short) specifier.getPicoseconds();
        this.femtosecond  = (short) specifier.getFemtoseconds();
        this.attosecond   = (short) specifier.getAttoseconds();
        this.zeptosecond  = (short) specifier.getZeptoseconds();
        this.yoctosecond  = (short) specifier.getYoctoseconds();
        this.xoxxosecond  = (short) specifier.getXoxxoseconds();
        this.weeblesecond = (short) specifier.getWeebleseconds();
        this.vatosecond   = (short) specifier.getVatoseconds();
        this.undasecond   = (short) specifier.getUndaseconds();
        this.plancks      = specifier.getPlanckSeconds();

        return;
    }


    /**
     *  Gets the time type. 
     *
     *  @return the time type 
     */

    @OSID @Override
    public final org.osid.type.Type getTimeType() {
        return (this.timeType);
    }


    /**
     *  Gets the hour of the day 0-23. 
     *
     *  @return the hour of the day 
     */

    @OSID @Override
    public final long getHour() {
        return (this.hour);
    }


    /**
     *  Gets the minute of the hour 0-59. 
     *
     *  @return the minute of the hour 
     */

    @OSID @Override
    public final long getMinute() {
        return (this.minute);
    }


    /**
     *  Gets the second of the minute 0-59. 
     *
     *  @return the second of the minute 
     */

    @OSID @Override
    public final long getSecond() {
        return (this.second);
    }


    /**
     *  Gets the number of milliseconds in this second 0-999. A millisecond is 
     *  one thousandth of a second. 
     *
     *  @return the milliseconds of the second 
     */

    @OSID @Override
    public final long getMilliseconds() {
        return (this.millisecond);
    }


    /**
     *  Gets the number of microseconds of the second 0-999. A microsecond is 
     *  one millionth of a second. 
     *
     *  @return the micrseconds of the millisecond 
     */

    @OSID @Override
    public final long getMicroseconds() {
        return (this.microsecond);
    }


    /**
     *  Gets the number of nanoseconds of the microsecond 0-999. A nanosecond 
     *  is one billionth of a second. 
     *
     *  @return the nanoseconds of the microsecond 
     */

    @OSID @Override
    public final long getNanoseconds() {
        return (this.nanosecond);
    }


    /**
     *  Gets the number of picoseconds of the nanosecond 0-999. A picosecond 
     *  is one trillionth of a second. 
     *
     *  @return the picoseconds of the nanosecond 
     */

    @OSID @Override
    public final long getPicoseconds() {
        return (this.picosecond);
    }


    /**
     *  Gets the number of femtoseconds of the picosecond 0-999. A femtosecond 
     *  is one quadrillionth of a second. 
     *
     *  @return the femtoseconds of the picosecond 
     */

    @OSID @Override
    public final long getFemtoseconds() {
        return (this.femtosecond);
    }


    /**
     *  Gets the number of attoseconds of the femtoseconds 0-999. An 
     *  attosecond is one quintillionth of a second. 
     *
     *  @return the attoseconds of the femtosecond 
     */

    @OSID @Override
    public final long getAttoseconds() {
        return (this.attosecond);
    }


    /**
     *  Gets the number of zeptoseconds of the attosecond 0-999. A zeptosecond 
     *  is one sextillionth of a second. 
     *
     *  @return the zeptoseconds of the attosecond 
     */

    @OSID @Override
    public final long getZeptoseconds() {
        return (this.zeptosecond);
    }


    /**
     *  Gets the number of yoctoseconds of the picosecond 0-999. A yoctosecond 
     *  is one septillionth of a second. This is getting quite small. 
     *
     *  @return the yoctoseconds of the yoctosecond 
     */

    @OSID @Override
    public final long getYoctoseconds() {
        return (this.yoctosecond);
    }


    /**
     *  Gets the number of xoxxoseconds of the yoctosecond 0-999. A 
     *  xoxxosecond is one octillionth of a second. We're going with Rudy 
     *  Rucker here. 
     *
     *  @return the xoxxoseconds of the yoctosecond 
     */

    @OSID @Override
    public final long getXoxxoseconds() {
        return (this.xoxxosecond);
    }


    /**
     *  Gets the number of weebleseconds of the xoxxosecond 0-999. A 
     *  weeblesecond is one nonillionth of a second. 
     *
     *  @return the weebleseconds of the xoxxoseconds 
     */

    @OSID @Override
    public final long getWeebleseconds() {
        return (this.weeblesecond);
    }


    /**
     *  Gets the number of vatoseconds of the xoxxosecond 0-999. A vatosecond 
     *  is one decillionth of a second. 
     *
     *  @return the vatoseconds of the weeblesecond 
     */

    @OSID @Override
    public final long getVatoseconds() {
        return (this.vatosecond);
    }


    /**
     *  Gets the number of undaseconds of the vatosecond 0-999. An undasecond 
     *  is one unadecillionth of a second. 
     *
     *  @return the undaseconds of the vatosecond 
     */

    @OSID @Override
    public final long getUndaseconds() {
        return (this.undasecond);
    }


    /**
     *  Gets the number of Plancks of the vatoseconds. A Planck is 10 
     *  quattuordecillionths of a second. 
     *
     *  @return the plancks of the undasecond 
     */

    @OSID @Override
    public final long getPlanckSeconds() {
        return (this.plancks);
    }


    /**
     *  Gets the granularity of this time. The granularity indicates
     *  the resolution of the clock. More precision than what is
     *  specified in this method cannot be inferred from the available
     *  data.
     *
     *  @return granularity 
     */

    @OSID @Override
    public final org.osid.calendaring.DateTimeResolution getGranularity() {
        return (this.granularity);
    }


    /**
     *  If the granularity of the time equals <code> getGranularity(),
     *  </code> then the multiplier is 1. This method may return a
     *  different number when the granularity differs from one of the
     *  defined resolutions.
     *
     *  @return granularity multiplier 
     */

    @OSID @Override
    public final long getGranularityMultiplier() {
        return (this.multiplier);
    }
    

    /**
     *  Formats the decimal portion of a Time.
     *
     *  @param time the time
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    protected static String formatDecimal(org.osid.calendaring.Time time) {
        nullarg(time, "time");

	if (time.getGranularity().ordinal() > DateTimeResolution.MILLISECOND.ordinal()) {
	    return ("");
	}

        int digits = getDecimalPlaces(time.getGranularity());
        int gm = Long.toString(time.getGranularityMultiplier()).length() - 1;
        digits = digits - gm;

        StringBuilder s = new StringBuilder();

	s.append(formatSignificantDigits(time.getMilliseconds(), digits > 3 ? 3 : digits));
	if (time.getGranularity() == DateTimeResolution.MILLISECOND) {
	    return (s.toString());
	}
        
        digits -= 3;

	s.append(formatSignificantDigits(time.getMicroseconds(), digits > 3 ? 3 : digits));
	if (time.getGranularity() == DateTimeResolution.MICROSECOND) {
	    return (s.toString());
	}

        digits -= 3;
	s.append(formatSignificantDigits(time.getNanoseconds(), digits > 3 ? 3 : digits));
	if (time.getGranularity() == DateTimeResolution.NANOSECOND) {
	    return (s.toString());
	}

        digits -= 3;
	s.append(formatSignificantDigits(time.getPicoseconds(), digits > 3 ? 3 : digits));
	if (time.getGranularity() == DateTimeResolution.PICOSECOND) {
	    return (s.toString());
	}

        digits -= 3;
	s.append(formatSignificantDigits(time.getFemtoseconds(), digits > 3 ? 3 : digits));
	if (time.getGranularity() == DateTimeResolution.FEMTOSECOND) {
	    return (s.toString());
	}

        digits -= 3;
	s.append(formatSignificantDigits(time.getAttoseconds(), digits > 3 ? 3 : digits));
	if (time.getGranularity() == DateTimeResolution.ATTOSECOND) {
	    return (s.toString());
	}

        digits -= 3;
	s.append(formatSignificantDigits(time.getZeptoseconds(), digits > 3 ? 3 : digits));
	if (time.getGranularity() == DateTimeResolution.ZEPTOSECOND) {
	    return (s.toString());
	}

        digits -= 3;
	s.append(formatSignificantDigits(time.getYoctoseconds(), digits > 3 ? 3 : digits));
	if (time.getGranularity() == DateTimeResolution.YOCTOSECOND) {
	    return (s.toString());
	}

        digits -= 3;
	s.append(formatSignificantDigits(time.getXoxxoseconds(), digits > 3 ? 3 : digits));
	if (time.getGranularity() == DateTimeResolution.XOXXOSECOND) {
	    return (s.toString());
	}

        digits -= 3;
	s.append(formatSignificantDigits(time.getWeebleseconds(), digits > 3 ? 3 : digits));
	if (time.getGranularity() == DateTimeResolution.WEEBLESECOND) {
	    return (s.toString());
	}

        digits -= 3;
	s.append(formatSignificantDigits(time.getVatoseconds(), digits > 3 ? 3 : digits));
	if (time.getGranularity() == DateTimeResolution.VATOSECOND) {
	    return (s.toString());
	}

        digits -= 3;
	s.append(formatSignificantDigits(time.getUndaseconds(), digits > 3 ? 3 : digits));
	if (time.getGranularity() == DateTimeResolution.UNDASECOND) {
	    return (s.toString());
	}

	s.append((int) (time.getPlanckSeconds() / PLANCKS_IN_UNDASECOND));
	return (s.toString());
    }


    /**
     *  Formats significant digits to a number of places.
     *
     *  num the number representing the decimal
     *  n the number of significant digits
     */

    protected static String formatSignificantDigits(long num, int n) {
	StringBuilder sb = new StringBuilder(n);

        int digits = Long.toString(num).length();
        for (int i = digits; i > n; i--) {
            num /= 10;
        }

	sb.append(num);

        int len = sb.length();
	if (len < n) {
	    for (int i = 0; i < n - len; i++) {
		sb.insert(0, "0");
	    }
	}

	return (sb.toString());
    }


    /**
     *  Gets the number of decimal places implied by a granularity.
     *
     *  @param granularity the granularity
     *  @throws org.osid.NullArgumentException
     *          <code>granularity</code> is <code>null</code>
     */

    protected static int getDecimalPlaces(org.osid.calendaring.DateTimeResolution granularity) {
        switch (granularity) {
        case PLANCKSECOND:
            return (44);
        case UNDASECOND:
            return (36);
        case VATOSECOND:
            return (33);
        case WEEBLESECOND:
            return (30);
        case XOXXOSECOND:
            return (27);
        case YOCTOSECOND:
            return (24);
        case ZEPTOSECOND:
            return (21);
        case ATTOSECOND:
            return (18);
        case FEMTOSECOND:
            return (15);
        case PICOSECOND:
            return (12);
        case NANOSECOND:
            return (9);
        case MICROSECOND:
            return (6);
        case MILLISECOND:
            return (3);
        default:
            return (0);
        }
    }
}
