//
// LazyTextTypeBuilder.java
//
//     A builder for a LazyTextType.
//
//
// Tom Coppeto
// OnTapSolutions
// 29 June 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.types.text.util;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A Lazy Text Type Builder.
 */

public final class LazyTextTypeBuilder {
        
    private String authority;
    private String namespace;
    private String identifier;

    private String name;
    private String label;
    private String description;
    private String domain;
    
    
    /**
     *  Constructs a new AbstractTypeBuilder.
     */
    
    public LazyTextTypeBuilder() {
        return;
    }
   
        
    /**
     *  Sets the authority for a Type.
     *
     *  @param authority the Type authority
     *  @return the basic type builder
     *  @throws org.osid.NullArgumentException
     *          <code>authority</code> is <code>null</code>
     */
    
    public LazyTextTypeBuilder authority(String authority) {
        nullarg(authority, "authority");
        this.authority = authority;
        return (self());
    }
    
    
    protected String getAuthority() {
        return (this.authority);
    }
    
    
    /**
     *  Sets the namespace for a Type.
     *
     *  @param namespace the Type namespace
     *  @return the basic type builder
     *  @throws org.osid.NullArgumentException
     *          <code>namespace</code> is <code>null</code>
     */
    
    public LazyTextTypeBuilder namespace(String namespace) {
        nullarg(namespace, "namespace");
        this.namespace = namespace;
        return (self());
    }
    
    
    protected String getNamespace() {
        return (this.namespace);
    }
    
    
    /**
     *  Sets the identifier for a Type.
     *
     *  @param identifier the Type identifier
     *  @return the basic type builder
     *  @throws org.osid.NullArgumentException
     *          <code>identifier</code> is <code>null</code>
     */
    
    public LazyTextTypeBuilder identifier(String identifier) {
        nullarg(identifier, "identifier");
        this.identifier = identifier;
        return (self());
    }
    
    
    protected String getIdentifier() {
        return (this.identifier);
    }

        
    /**
     *  Sets the domain for a Type.
     *
     *  @param domain the Type domain
     *  @return the basic type builder
     *  @throws org.osid.NullArgumentException
     *          <code>domain</code> is <code>null</code>
     */
    
    public LazyTextTypeBuilder domain(String domain) {
        nullarg(domain, "domain");
        this.domain = domain;
        return (self());
    }

        
    protected String getDomain() {
        return (this.domain);
    }


    /**
     *  Sets the display name for a Type.
     *
     *  @param name the Type display name
     *  @return the basic type builder
     *  @throws org.osid.NullArgumentException
     *          <code>name</code> is <code>null</code>
     */
    
    public LazyTextTypeBuilder name(String name) {
        nullarg(name, "name");
        this.name = name;
        return (self());
    }
    
    
    protected String getDisplayName() {
        return (this.name);
    }
    
    
    /**
     *  Sets the display label for a Type.
     *
     *  @param label the Type display label
     *  @return the basic type builder
     *  @throws org.osid.NullArgumentException
     *          <code>label</code> is <code>null</code>
     */
    
    public LazyTextTypeBuilder label(String label) {
        nullarg(label, "label");
        this.label = label;
        return (self());
    }
    

    protected String getDisplayLabel() {
        return (this.label);
    }
    

    /**
     *  Sets the display description for a Type.
     *
     *  @param description the Type display description
     *  @return the basic type builder
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */
    
    public LazyTextTypeBuilder description(String description) {
        nullarg(description, "description");
        this.description = description;
        return (self());
    }
    
    
    protected String getDescription() {
        return (this.description);
    }


    /**
     *  Gets the reference to this instance of the builder.
     *
     *  @return ObjectBuilder
     */

    protected LazyTextTypeBuilder self() {
        return (this);
    }

    
    /**
     *  Builds the LazyTextType. The Type authority, namespace, and
     *  identifier must have been set.
     *
     *  @return a new Type
     *  @throws org.osid.IllegalStateException Type cannot be
     *          constructed
     */

    public org.osid.type.Type build() {
        String authority   = getAuthority();
        String namespace   = getNamespace();
        String identifier  = getIdentifier();

        String domain      = getDomain();
        String name        = getDisplayName();
        String label       = getDisplayLabel();
        String description = getDescription();
        
        if (authority == null) {
            throw new org.osid.IllegalStateException("type authority has not been set");
        }
        
        if (namespace == null) {
            throw new org.osid.IllegalStateException("type namespace has not been set");
        }
        
        if (identifier == null) {
            throw new org.osid.IllegalStateException("type identifier has not been set");
        }
        
        if ((domain == null) && (authority == null) && (namespace == null) &&
            (identifier == null)) {
            
            return new LazyTextType(authority, namespace, identifier);
        }
        
        if (domain == null) {
            domain = "";
        }
        
        if (name == null) {
            if (label != null) {
                name = label;
            } else {
                name = LazyTextType.formatId(authority, namespace, identifier);
                label = name;
            }
        }
        
        if (label == null) {
            label = LazyTextType.formatId(authority, namespace, identifier);
        }
        
        if (description == null) {
            description = "";
        }
        
        return new LazyTextType(authority, namespace, identifier, domain, name, label, description);        
    }
}        
