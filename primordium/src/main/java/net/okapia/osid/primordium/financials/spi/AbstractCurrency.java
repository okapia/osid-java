//
// AbstractCurrency.java
//
//     An implementation of Primitive for an OSID Currency.
//
//
// Tom Coppeto
// OnTapSolutions
// 21 October 2011
//
// Copyright (c) 2011 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.financials.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.primordium.locale.text.eng.us.Plain.text;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An implementation of Primitive for an OSID Currency to provide
 *  consistent Java Object behavior across OSID implementations. All
 *  OSID Currency implementations should be a descendant of this
 *  class.
 */

public abstract class AbstractCurrency
    implements org.osid.financials.Currency {

    private final org.osid.type.Type type;
    private final java.math.BigDecimal amount;
    private String label;
    private int hash = 0;

    private static final long serialVersionUID = 1402389382L;


    /**
     *  Constructs a new <code>AbstractCurrency</code>.
     *
     *  @param currencyType the currency type
     *  @param amount the currency amount
     *  @throws org.osid.NulArgumentException <code>type</code> or
     *          <code>amount</code> is <code>null</code>
     */

    protected AbstractCurrency(org.osid.type.Type currencyType, java.math.BigDecimal amount) {
        nullarg(currencyType, "currency type");
        nullarg(amount, "currency amount");

        this.type = currencyType;
        this.amount = amount;

        return;
    }


    /**
     *  Gets the currency type. 
     *
     *  @return the currency type 
     */

    @OSID @Override
    public final org.osid.type.Type getCurrencyType() {
        return (this.type);
    }


    /**
     *  Gets the total amount. 
     *
     *  @return the amount 
     */

    @OSID @Override
    public final java.math.BigDecimal getAmount() {
        return (this.amount);
    }


    /**
     *	Tests if this Currency is greater than the given Currency.
     *
     *	@param	currency the currency to compare
     *	@return <code>true</code> if this currency is larger,
     *		<code>false</code> otherwise
     *	@throws org.osid.NullArgumentException <code>currency</code>
     *		is <code>null</code>
     *	@throws org.osid.UnsupportedException cannot compare currency types
     */

    @OSIDBinding @Override
    public final boolean isLarger(org.osid.financials.Currency currency) {
        if (compare(this, currency) > 0) {
            return (true);
        } else {
            return (false);
        }
    }


    /**
     *	Tests if this Currency is less than the given Currency. A
     *	lesser Currency is one whose values plus uncertainty is less
     *	than another minus its uncertainty. An uncertainty of infinity
     *	is undefined and returns false.
     *
     *	@param	currency the currency to compare
     *	@return <code>true</code> if this currency is less,
     *		<code>false</code> otherwise
     *	@throws org.osid.NullArgumentException <code>currency</code>
     *		is <code>null</code>
     *	@throws org.osid.UnsupportedException cannot compare currency types
     */

    @OSIDBinding @Override
    public final boolean isSmaller(org.osid.financials.Currency currency) {
        if (compare(this, currency) < 0) {
            return (true);
        } else {
            return (false);
        }
    }


    /**
     *  Compares this currency with the specified Currency for
     *  order. Returns a negative integer, zero, or a positive integer
     *  as this currency is less than, equal to, or greater than the
     *  specified currency.
     *
     *  This implementation provides a natural ordering based on the
     *  currency types and amounts. For more accurrate comparisons,
     *  use <code>isLarger()</code> or <code>isSmaller()</code>
     *  instead.
     *
     *  @param currency the currency to be compared
     *  @return a negative integer, zero, or a positive integer as
     *          this currency is less than, equal to, or greater than
     *          the specified currency
     *  @throws ClassCastException if the specified currency's type
     *          prevents it from being compared to this currency
     */

    @OSIDBinding @Override
    public int compareTo(org.osid.financials.Currency currency) {
        if (this == currency) {
            return (0);
        }

        int c = getCurrencyType().compareTo(currency.getCurrencyType());
        if (c != 0) {
            return (c);
        }

        return (getAmount().compareTo(currency.getAmount()));
    }


    /**
     *  Compares two Currencies for performing value
     *  comparisons. Returns a negative integer, zero, or a positive
     *  integer if currency1 is less than, equal to, or greater than
     *  currency2.
     *
     *  This method is used by other comparison methods and should be
     *  implemented to return accurate comparisons.
     *
     *  @param currency1 the first currency
     *  @param currency2 the second currency
     *  @return a negative integer, zero, or a positive integer as
     *          <code>currency1</code> is less than, equal to, or
     *          greater than <code>currency2</code>
     *  @throws org.osid.NullArgumentException <code>currency1</code>
     *          or <code>currency2</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException cuurency types cannot be
     *          compared
     */

    protected static int compare(org.osid.financials.Currency currency1, 
                                 org.osid.financials.Currency currency2) {

        nullarg(currency1, "currency1");
        nullarg(currency2, "currency2");
        
        if (!currency1.getCurrencyType().equals(currency2.getCurrencyType())) {
            throw new org.osid.UnsupportedException(currency1.getCurrencyType() + 
                                                    " is not comparable to " +
                                                    currency2.getCurrencyType());
        }

        return (currency1.getAmount().compareTo(currency2.getAmount()));
    }


    /**
     *  Determines if the given <code> Currency </code> is equal to
     *  this one. 
     *
     *  Two Currency objects are equal if their Currency Types and
     *  Currency amounts, and the precision in the currency amounts
     *  are equal.
     *
     *  @param  obj an object to compare
     *  @return <code> true </code> if the given object is equal to
     *          this <code>Currency</code>, <code> false </code>
     *          otherwise
     */

    @OSIDBinding @Override
    public final boolean equals(Object obj) {
        if (obj == null) {
            return (false);
        }

        if (this == obj) {
            return (true);
        }

        if (!(obj instanceof org.osid.financials.Currency)) {
            return (false);
        }

        org.osid.financials.Currency currency = (org.osid.financials.Currency) obj;
        if (!getCurrencyType().equals(currency.getCurrencyType())) {
            return (false);
        }

        return (getAmount().equals(currency.getAmount()));
    }


    /**
     *  Returns a hash code value for this <code>Currency</code>. The
     *  hash code is determined from the currency type and currency
     *  amount.
     *
     *  @return a hash code value for this object
     */

    @OSIDBinding @Override
    public final int hashCode() { 
        if (this.hash == 0) {
            this.hash = calculateHashCode();
        }
        
        return (this.hash);
    }


    /**
     *  Returns a string representation of this Currency.
     *
     *  All implementations are encouraged to supply this method to
     *  display the type and value.
     *
     *  @return a string
     */

    @OSIDBinding @Override
    public String toString() {
        if (this.label == null) {
            this.label = formatLabel();
        }

	return (this.label);
    }


    private int calculateHashCode() {
        int hash = getCurrencyType().hashCode();
        hash = hash * 31 + getAmount().hashCode();
	return (hash);        
    }


    private String formatLabel() {
	return (getCurrencyType().getDisplayLabel().getText() + getAmount());
    }
}
