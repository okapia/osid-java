//
// EnglishRegionTypes.java
//
//     An enumeration of English regions.
//
//
// Tom Coppeto
// Okapia
// 25 April 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.types.text.eng;


/**
 *  An enumeration of English regions.
 */

public enum EnglishRegionTypes
    implements net.okapia.osid.torrefacto.collect.TypeEnum {

    AG ("Antigua and Barbuda"),
    AL ("Anguila"),
    AS ("American Samoa"),
    AU ("Australia"),
    BB ("Barbados"),
    BE ("Belgium"),
    BM ("Bermuda"),
    BN ("Brunei"),
    BS ("Bahamas"),
    BW ("Botswana"),
    BZ ("Belize"),
    CA ("Canada"),
    CK ("Cook Islands"),
    CM ("Cameroon"),
    DM ("Dominica"),
    ER ("Erotrea"),
    ET ("Ethiopia"),
    FJ ("Fiji"),
    FK ("Falkland Islands"),
    FM ("Micronesia"),
    GB ("United Kingdom"),
    GD ("Grenada"),
    GH ("Ghana"),
    GI ("Gibraltar"),
    GM ("Gambia"),
    GU ("Guam"),
    GY ("Guyana"),
    HK ("Hong Kong"),
    IE ("Ireland"),
    IN ("India"),
    IO ("British Insian Ocean Territory"),
    JM ("Jamaica"),
    KE ("Kenya"),
    KI ("Kiribati"),
    KN ("Saint Kitts and Nevis"),
    KY ("Cayman Islands"),
    LC ("Saint Lucia"),
    LR ("Liberia"),
    LS ("Lesotho"),
    MS ("Montserrat"),
    MT ("Malta"),
    MU ("Mauritius"),
    MW ("Malawi"),
    NA ("Namibia"),
    NF ("Norfolk Island"),
    NG ("Nigeria"),
    NR ("Nauru"),
    NU ("Niue"),
    NZ ("New Zealand"),
    PG ("Papua New Guinea"),
    PH ("Philippines"),
    PK ("Pakistan"),
    PN ("Pitcairn"),
    PR ("Puerto Rico"),
    PW ("Palau"),
    RW ("Rwanda"),
    SB ("Solomon Islands"),
    SC ("Seychelles"),
    SH ("Saint Helena"),
    SL ("Sierra Leone"),
    SZ ("Swaziland"),
    TC ("Turks and Caicos Islands"),
    TK ("Tokelau"),
    TO ("Tonga"),
    TT ("Trinidad and Tobago"),
    UG ("Uganda"),
    UM ("United States Minor Outlying Islands"),
    US ("United States"),
    VC ("Saint Vincent and The Grenadines"),
    VG ("British Virgin Islands"),
    VI ("U.S. Virgin Islands"),
    WS ("Samoa"),
    ZM ("Zambia"),
    ZW ("Zimbabwe");

    private final String name;
    private org.osid.type.Type type;

    EnglishRegionTypes(String name) {
        this.name = name;
        return;
    }


    /**
     *  Gets the Type.
     *
     *  @return the Type
     */
    public org.osid.type.Type getType() {
        if (this.type == null) {
            this.type = new net.okapia.osid.primordium.types.text.util.LazyTextTypeBuilder()
                .authority("okapia.net")
                .namespace("Language Regions")
                .identifier("ENG/" + name())
                .domain("Locale Languages English")
                .name(this.name + " English")
                .label(this.name)
                .description("The English language type for " +  this.name + ".")
                .build();
        }

        return (this.type);
    }


    private org.osid.type.Type getDefaultType() {
        return (new net.okapia.osid.primordium.type.BasicType("okapia.net", "Language Regions", "ENG/US"));
    }
}


