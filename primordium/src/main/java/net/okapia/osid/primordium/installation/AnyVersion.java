//
// AnyVersion.java
//
//     An implementation of Primitive for OSID Versions.
//
//
// Tom Coppeto
// Okapia
// 21 October 2011
//
// Copyright (c) 2011 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.installation;


/**
 *  An implementation of an OSID Version.
 */

public final class AnyVersion 
    extends net.okapia.osid.primordium.installation.spi.AbstractVersion
    implements org.osid.installation.Version {

    private static final long serialVersionUID = -2074754739L;


    /**
     *  Constructs a new <code>AnyVersion</code>.
     *
     *  @param scheme a versioning schema
     *  @param components the version components
     *  @throws org.osid.NullArgumentException <code>scheme</code> or
     *          <code>components</code> is <code>null</code>
     */

    public AnyVersion(org.osid.type.Type scheme, String[] components) {
        super(scheme, components);
        return;
    }
}
