//
// NewEarthDefinition.java
//
//     A defitnition of a New Earth time definition.
//
//
// Tom Coppeto
// Okapia
// 2 May 2012
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.calendaring;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A definition of a New Earth time system used by
 *  ConfigurableTime. The hours map to degeres in this system.
 */

public final class NewEarthTimeDefinition
    implements TimeDefinition {

    private static final org.osid.type.Type TYPE = net.okapia.osid.primordium.types.time.SuperFunTimeTypes.NET.getType();
    private static final long serialVersionUID = 3796818762L;
    

    /**
     *  Gets the time type.
     *
     *  @return the time type
     */
    
    @Override
    public org.osid.type.Type getTimeType() {
        return (TYPE);
    }


    /**
     *  Gets the number of degrees in a day.
     *
     *  @return the degrees in a day
     */
    
    @Override
    public long getHoursPerDay() {
        return (360);
    }
    

    /**
     *  Gets the starting number for the degree.
     *
     *  @return the starting degree
     */

    @Override
    public long getStartingHour() {
        return (0);
    }


    /**
     *  Gets the number of minutes in a degre.
     *
     *  @return the minutes ina degree
     */

    @Override
    public long getMinutesPerHour() {
        return (60);
    }


    /**
     *  Gets the starting minute for the degree.
     *
     *  @return the starting minute
     */

    @Override
    public long getStartingMinute() {
        return (0);
    }


    /**
     *  Gets the number of seocnds in a minute.
     *
     *  @return the seconds in a minute
     */

    @Override
    public long getSecondsPerMinute() {
        return (60);
    }


    /**
     *  Gets the starting second for the minute.
     *
     *  @return the starting second
     */

    @Override
    public long getStartingSecond() {
        return (0);
    }


    /**
     *  Gets the number of planck seconds in a second.
     * 
     *  @return the number of planck seconds
     */

    @Override
    public java.math.BigDecimal getPlancksPerSecond() {
        return (new java.math.BigDecimal("1.855094832e43"));
    }


    /**
     *  Gets a label for a unit.
     *
     *  @param units the granularity
     *  @return a label
     *  @throws org.osid.NullArgumentException <code>units</code> is
     *          <code>null</code>
     */

    @Override
    public String getLabel(org.osid.calendaring.DateTimeResolution units) {
        nullarg(units, "units");

        switch (units) {
        case HOUR:
            return ("º");
        case HALF_HOUR:
        case QUARTER_HOUR:
        case MINUTE:
            return ("'");
        case SECOND:
            return ("\"");
        default:
            return ("???");
        }
    }


    /**
     *  Gets a plural label for a unit.
     *
     *  @param units the granularity
     *  @return a label
     *  @throws org.osid.NullArgumentException <code>units</code> is
     *          <code>null</code>
     */

    @Override
    public String getPluralLabel(org.osid.calendaring.DateTimeResolution units) {
        return (getLabel(units));
    }


    /**
     *  Gets the granularity for a label.
     *
     *  @param label
     *  @return the granularity
     *  @throws org.osid.NotFoundException units not found
     *  @throws org.osid.NullArgumentException <code>label</code> is
     *          <code>null</code>
     */

    @Override
    public org.osid.calendaring.DateTimeResolution getUnits(String label)
        throws org.osid.NotFoundException {

        switch (label.toLowerCase()) {
        case "º":
        case "d":
        case "deg":
        case "deg.":
        case "degree":
        case "degrees":
            return (org.osid.calendaring.DateTimeResolution.HOUR);
        case "'":
        case "m":
        case "min":
        case "min.":
        case "minuite":
        case "miniutes":
            return (org.osid.calendaring.DateTimeResolution.MINUTE);
        case "s":
        case "sec":
        case "sec.":
        case "second":
        case "seconds":
            return (org.osid.calendaring.DateTimeResolution.SECOND);
        default:
            throw new org.osid.NotFoundException(label + " not found");
        }
    }
}
