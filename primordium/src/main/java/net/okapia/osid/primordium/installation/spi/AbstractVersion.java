//
// AbstractVersion.java
//
//     An implementation of an OSID Version.
//
//
// Tom Coppeto
// Okapia
// 21 October 2011
//
// Copyright (c) 2011 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.installation.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.util.AlphanumComparator;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An implementation of an OSID Version.
 */

public abstract class AbstractVersion 
    implements org.osid.installation.Version {

    private final org.osid.type.Type scheme;
    private final String[] components;
    private final Format format;
    private String label;
    private int hash = 0;

    private static final java.util.Comparator<String> comparator = new AlphanumComparator();
    private static final long serialVersionUID = -1707632845L;


    /**
     *  Constructs a new <code>AbstractVersion</code> using a
     *  Format.DOT output format.
     *
     *  @param scheme a versioning schema
     *  @param components the version components
     *  @throws org.osid.NullArgumentException <code>scheme</code> or
     *          <code>components</code> is <code>null</code>
     */

    protected AbstractVersion(org.osid.type.Type scheme, String[] components) {
        this(scheme, components, Format.DOT);
        return;
    }


    /**
     *  Constructs a new <code>AbstractVersion</code>.
     *
     *  @param scheme a versioning schema
     *  @param components the version components
     *  @param format the output format for this version
     *  @throws org.osid.NullArgumentException <code>scheme</code>,
     *          <code>components</code>, or <code>format</code> is
     *          <code>null</code>
     */

    protected AbstractVersion(org.osid.type.Type scheme, String[] components, Format format) {
        nullarg(scheme, "version scheme");
        nullarg(components, "version components");
        nullarg(format, "version format");

        this.scheme = scheme;
        this.components = components.clone();
        this.format = format;

        return;
    }


    /**
     *  Gets the versioning scheme as a type. 
     *
     *  @return the versioning scheme type 
     */

    @OSID @Override
    public final org.osid.type.Type getScheme() {
        return (this.scheme);
    }


    /**
     *  Gets the components of the version. In a 
     *  major.minor[.maintenance[.build]] scheme, an example is {3, 0, 0}. 
     *
     *  @return the version components 
     */

    @OSID @Override
    public final String[] getComponents() {
        return (this.components.clone());
    }


    /**
     *  Tests if this Version is more recent than the given Version.
     *
     *  @param  version the version to compare
     *  @return <code>true</code> if this version is more recent,
     *          <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException <code>version</code>
     *          is <code>null</code>
     *  @throws org.osid.UnsupportedException version scheme types 
     *          not compatible
     */

    @OSIDBinding @Override
    public boolean isNewer(org.osid.installation.Version version) {
        if (!getScheme().equals(version.getScheme())) {
            throw new org.osid.UnsupportedException(version.getScheme() + " not supported");
        }

        String[] us   = getComponents();
        String[] them = version.getComponents();

        for (int i = 0; i < us.length; i++) {
            if (i == them.length) {
                return (true);
            }

            if (!us[i].equals(them[i])) {
                if (AbstractVersion.comparator.compare(us[i], them[i]) > 0) {
                    return (true);
                } else {
                    return (false);
                }
            }
        }

        if (us.length > them.length) {
            return (true);
        }

        return (false);
    }


    /**
     *  Tests if this Version is older than the given Version.
     *
     *  @param  version the version to compare
     *  @return <code>true</code> if this version is older,
     *          <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException <code>version</code>
     *          is <code>null</code>
     *  @throws org.osid.UnsupportedException version scheme types 
     *          not compatible
     */

    @OSIDBinding @Override
    public boolean isOlder(org.osid.installation.Version version) {
        if (equals(version)) {
            return (false);
        } else {
            return (!isNewer(version));
        }
    }


    /**
     *  Tests if this Version includes the given version. A version
     *  3.2 may include version 3.2.5 but not include version 3.0.
     *
     *  @param  version the version to compare
     *  @return <code>true</code> if this version includes the given
     *          version, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException <code>version</code>
     *          is <code>null</code>
     *  @throws org.osid.UnsupportedException version scheme types 
     *          not compatible
     */

    @OSIDBinding @Override
    public boolean isInclusive(org.osid.installation.Version version) {
        if (!getScheme().equals(version.getScheme())) {
            throw new org.osid.UnsupportedException(version.getScheme() + " not supported");
        }

        String[] us   = getComponents();
        String[] them = version.getComponents();

        for (int i = 0; i < us.length; i++) {
            if (i == them.length) {
                return (false);
            }

            if (!us[i].equals(them[i])) {
                return (false);
            }
        }

        return (true);
    }


    /**
     *  Tests if this Version excludes the given version. Two versions
     *  are exclusive if a.isInclusive(b) and b.isInclusive(c) are
     *  both false.
     *  
     *
     *  @param version the version to compare
     *  @return <code>true</code> if this version excludes the given
     *          version, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException <code>version</code>
     *          is <code>null</code>
     *  @throws org.osid.UnsupportedException version scheme types 
     *          not compatible
     */

    @OSIDBinding @Override
    public boolean isExclusive(org.osid.installation.Version version) {
        return (!isInclusive(version) && !version.isInclusive(this));
    }


    /**
     *  Compares this Version with the specified Version to determine
     *  the natural order. Returns a negative integer, zero, or a
     *  positive integer as this version is less than, equal to, or
     *  greater than the specified version.
     *
     *  The natural ordering is determined first by the natural
     *  ordering of the version scheme, then by its components.
     *
     *  If <code>compareTo</code> a version is zero, then
     *  <code>equals()</code> must be <code>true</code> and their hash
     *  codes must also be equal for consistent behavior. For
     *  orderings that may yield inconsistent behavior, an external
     *  <code>Comparator</code> should be used.
     *
     *  All implementations need to supply this method.
     *
     *  @param version the version to be compared
     *  @return a negative integer, zero, or a positive integer as
     *          this version is less than, equal to, or greater than
     *          the specified version
     *  @throws ClassCastException if the specified version's type
     *          prevents it from being compared to this version
     */

    @OSIDBinding @Override
    public int compareTo(org.osid.installation.Version version) {
        if (this == version) {
            return (0);
        }

        org.osid.type.Type type = version.getScheme();
        if (!getScheme().equals(type)) {
            java.text.Collator collator = java.text.Collator.getInstance();
            String us = getScheme().getAuthority() 
                + getScheme().getIdentifierNamespace() 
                + getScheme().getIdentifier();
            String them = type.getAuthority() + type.getIdentifierNamespace() + type.getIdentifier();
            return (collator.compare(us, them));
        }

        String[] ourComponents = getComponents();
        String[] theirComponents = version.getComponents();

        for (int i = 0; i < ourComponents.length; i++) {
            if (i > (theirComponents.length - 1)) {
                return (1);
            }

            if (!ourComponents[i].equals(theirComponents[i])) {
                return (AbstractVersion.comparator.compare(ourComponents[i], theirComponents[i]));
            }
        }

        return (0);
    }


    /**
     *  Determines if the given <code> Version </code> is equal to
     *  this one. Two Versions are equal if the scheme and components
     *  are equal.
     *
     *  @param  obj an object to compare
     *  @return <code> true </code> if the given object is equal to
     *          this <code>Version</code>, <code> false </code>
     *          otherwise
     */

    @OSIDBinding @Override
    public final boolean equals(Object obj) {
        if (obj == null) {
            return (false);
        }

        if (this == obj) {
            return (true);
        }

        if (!(obj instanceof org.osid.installation.Version)) {
            return (false);
        }

        org.osid.installation.Version version = (org.osid.installation.Version) obj;
	if (!getScheme().equals(version.getScheme())) {
            return (false);
        }

        String[] ourComponents = getComponents();
        String[] theirComponents = version.getComponents();

        if (ourComponents.length != theirComponents.length) {
            return (false);
        }

        for (int i = 0; i < ourComponents.length; i++) {
            if (!ourComponents[i].equals(theirComponents[i])) {
                return (false);
            }
        }

        return (true);
    }


    /**
     *  Returns a hash code value for this <code>Version</code> based on
     *  the authority, namespace and identifier.
     *
     *  @return a hash code value for this object
     */

    @OSIDBinding @Override
    public final int hashCode() {
        if (this.hash == 0) {
            this.hash = calculateHashCode();
        }

        return (this.hash);
    }


    /**
     *  Returns a string representation of this Version.
     *
     *  All implementations are encouraged to supply this method to
     *  display the version.
     *
     *  @return a string
     */

    @OSIDBinding @Override
    public String toString() {
        if (this.label == null) {
            this.label = formatLabel();
        }

        return (this.label);
    }


    /**
     *  Gets the output format for this version.
     *
     *  @return the format
     */

    protected Format getFormat() {
        return (this.format);
    }


    private int calculateHashCode() {
	int hash = 1;
	hash = hash * 31 + getScheme().hashCode();

        String[] components = getComponents();
        for (int i = 0; i < components.length; i++) {
            hash = hash * 31 + components[i].hashCode();
        }

        return (hash);
    }


    private String formatLabel() {
	StringBuilder sb = new StringBuilder();
	
        String[] components = getComponents();
        for (int i = 0; i < components.length; i++) {
            if (i > 0) {
                sb.append(this.format.getDelimiter());
            }

            sb.append(components[i]);
        }

	return (sb.toString());
    }


    /**
     *  An enumeration of output formats for this version.
     */

    public enum Format {
        /** No Notation */
        NONE (""),

        /** Hypehen Notation (1-2-3) */
        DASH ("-"),

        /** Dot Notation (1.2.3) */
        DOT ("."),

        /** Slash Notation (1/2/3) */
        SLASH ("/");

        private final String delimiter;


        Format(String delimiter) {
            this.delimiter = delimiter;
            return;
        }

        
        public String getDelimiter() {
            return (this.delimiter);
        }
    }
}
