//
// CalendarDefinition.java
//
//     A defitnition of a calendar.
//
//
// Tom Coppeto
// Okapia
// 2 May 2012
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.calendaring;


/**
 *  A definition of a calendar used by ConfigurableDateTime.
 */

public interface CalendarDefinition
    extends java.io.Serializable {

    
    /**
     *  Gets the calendar type.
     *
     *  @return the calendar type
     */
    
    public org.osid.type.Type getCalendarType();


    /**
     *  Gets the number of years in a blue moon.
     *
     *  @return the years in a blue moon
     */
    
    public java.math.BigDecimal getYearsPerBluemoon();


    /**
     *  Gets the number of months in a year.
     *
     *  @return the months in a year
     */
    
    public long getMonthsPerYear();    


    /**
     *  Gets average the weeks per year.
     *
     *  @return the weeks in a year
     */
    
    public java.math.BigDecimal getAverageWeeksPerYear();


    /**
     *  Gets average the days per year.
     *
     *  @return the days in a year
     */
    
    public java.math.BigDecimal getAverageDaysPerYear();


    /**
     *  Gets average the days per month.
     *
     *  @return the days in a month
     */
    
    public java.math.BigDecimal getAverageDaysPerMonth();


    /**
     *  Gets average the days per week.
     *
     *  @return the days in a week
     */
    
    public java.math.BigDecimal getAverageDaysPerWeek();


    /**
     *  Gets a label for a unit.
     *
     *  @param units the granularity
     *  @return a label
     */

    public String getLabel(org.osid.calendaring.DateTimeResolution units);


    /**
     *  Gets a plural label for a unit.
     *
     *  @param units the granularity
     *  @return a label
     */

    public String getPluralLabel(org.osid.calendaring.DateTimeResolution units);


    /**
     *  Gets the granularity for a label.
     *
     *  @param label
     *  @return the granularity
     *  @throws org.osid.NotFoundException units not found
     *  @throws org.osid.NullArgumentException <code>label</code> is
     *          <code>null</code>
     */

    public org.osid.calendaring.DateTimeResolution getUnits(String label)
        throws org.osid.NotFoundException;
}
