//
// AnyDisplayText.java
//
//     An implementation of an OSID DisplayText.
//
//
// Tom Coppeto
// OnTapSolutions
// 12 May 2012
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.locale;


/**
 *  An implementation of an OSID DisplayText.
 */

public final class AnyDisplayText
    extends net.okapia.osid.primordium.locale.spi.AbstractDisplayText
    implements org.osid.locale.DisplayText {

    private static final long serialVersionUID = 1021949092L;


    /**
     *  Constructs a new <code>AnyDisplayText</code>.
     *
     *  @param languageType the language type
     *  @param scriptType the language script type
     *  @param formatType the format type
     *  @param text the text
     *  @throws org.osid.NullArgumentException
     *          <code>languageType</code>, <code>scriptType</code>,
     *          <code>formatType</code>, <code>text</code> is
     *          <code>null</code>
     */

    public AnyDisplayText(org.osid.type.Type languageType, org.osid.type.Type scriptType,
                      org.osid.type.Type formatType, String text) {
        
        super(languageType, scriptType, formatType, text);
        return;
    }
}
