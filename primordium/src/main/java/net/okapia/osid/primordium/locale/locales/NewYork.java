//
// NewYork.java
//
//     A central locale.
//
//
// Tom Coppeto
// Okapia
// 12 january 2022
//
// Copyright (c) 2022 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.locale.locales;

import net.okapia.osid.primordium.types.text.FormatTypes;
import net.okapia.osid.primordium.types.text.eng.EnglishRegionTypes;
import net.okapia.osid.primordium.types.text.ISOScriptTypes;

import net.okapia.osid.primordium.types.currency.ISOCurrencyTypes;
import net.okapia.osid.primordium.types.calendar.CalendarTypes;
import net.okapia.osid.primordium.types.locale.UnitSystemTypes;
import net.okapia.osid.primordium.types.time.EarthTimeTypes;

import org.osid.binding.java.annotation.OSID;


/**
 *  A NY locale.
 */

public final class NewYork
    implements org.osid.locale.Locale {


    /**
     *  Gets the language <code> Type. </code> 
     *
     *  @return the language type 
     */

    @OSID @Override
    public org.osid.type.Type getLanguageType() {
        return (EnglishRegionTypes.US.getType());
    }


    /**
     *  Gets the script <code> Type. </code> 
     *
     *  @return the script type 
     */

    @OSID @Override
    public org.osid.type.Type getScriptType() {
        return (EnglishRegionTypes.US.getType());
    }


    /**
     *  Gets the calendar <code> Type. </code> 
     *
     *  @return the calendar type 
     */

    @OSID @Override
    public org.osid.type.Type getCalendarType() {
        return (CalendarTypes.GREGORIAN.getType());
    }


    /**
     *  Gets the time <code> Type. </code> 
     *
     *  @return the time type 
     */

    @OSID @Override
    public org.osid.type.Type getTimeType() {
        return (EarthTimeTypes.UTC.getType());
    }


    /**
     *  Gets the currency <code> Type. </code> 
     *
     *  @return the currency type 
     */

    @OSID @Override
    public org.osid.type.Type getCurrencyType() {
        return (ISOCurrencyTypes.USD.getType());
    }


    /**
     *  Gets the unit system <code> Type. </code> 
     *
     *  @return the unit system type 
     */

    @OSID @Override
    public org.osid.type.Type getUnitSystemType() {
        return (UnitSystemTypes.UCS.getType());
    }
    

    /**
     *  Gets the numeric format <code> Type. </code> 
     *
     *  @return the numeric format type 
     */

    @OSID @Override
    public org.osid.type.Type getNumericFormatType() {
        // not sure what this was for
        return (EnglishRegionTypes.US.getType());
    }


    /**
     *  Gets the calendar format <code> Type. </code> 
     *
     *  @return the calendar format type 
     */

    @OSID @Override
    public org.osid.type.Type getCalendarFormatType() {
        // not sure what this was for
        return (EnglishRegionTypes.US.getType());
    }


    /**
     *  Gets the time format <code> Type. </code> 
     *
     *  @return the time format type 
     */

    @OSID @Override
    public org.osid.type.Type getTimeFormatType() {
        // not sure what this was for
        return (EnglishRegionTypes.US.getType());
    }
    

    /**
     *  Gets the currency format <code> Type. </code> 
     *
     *  @return the currency format type 
     */

    @OSID @Override
    public org.osid.type.Type getCurrencyFormatType() {
        // not sure what this was for - should be part of Currency definition
        return (EnglishRegionTypes.US.getType());
    }


    /**
     *  Gets the coordinate format <code> Type. </code> 
     *
     *  @return the coordinate format type 
     */

    @OSID @Override
    public org.osid.type.Type getCoordinateFormatType() {
        // not sure what this was for
        return (EnglishRegionTypes.US.getType());
    }
}

