//
// GregorianCalendarDefinition.java
//
//     A definition of a datetime.
//
//
// Tom Coppeto
// Okapia
// 2 May 2012
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.calendaring;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A definition of a calendar used by ConfigurableDateTime.
 */

public final class GregorianCalendarDefinition
    implements CalendarDefinition {

    private static final org.osid.type.Type TYPE = net.okapia.osid.primordium.types.calendar.CalendarTypes.GREGORIAN.getType();   
    private static final long serialVersionUID = 11838114702L;
    

    /**
     *  Gets the calendar type.
     *
     *  @return the calendar type
     */
    
    @Override
    public org.osid.type.Type getCalendarType() {
        return (TYPE);
    }


    /**
     *  Gets the number of years in a blue moon.
     *
     *  @return the years in a blue moon.
     */

    @Override
    public java.math.BigDecimal getYearsPerBluemoon() {
        return (new java.math.BigDecimal("2.714285714285714"));
    }


    /**
     *  Gets the number of months in a year.
     *
     *  @return the months in a year.
     */
    
    @Override
    public long getMonthsPerYear() {
        return (12);
    }


    /**
     *  Gets average the weeks per year.
     *
     *  @return the weeks in a year
     */

    @Override
    public java.math.BigDecimal getAverageWeeksPerYear() {
        return (new java.math.BigDecimal("52.1775"));
    }


    /**
     *  Gets average the days per year.
     *
     *  @return the days in a year
     */
    
    @Override
    public java.math.BigDecimal getAverageDaysPerYear() {
        return (new java.math.BigDecimal("365.242"));
    }


    /**
     *  Gets average the days per month.
     *
     *  @return the days in a month
     */
    
    @Override
    public java.math.BigDecimal getAverageDaysPerMonth() {
        return (new java.math.BigDecimal("30.4368"));
    }


    /**
     *  Gets average the days per week.
     *
     *  @return the days in a week
     */
    
    @Override
    public java.math.BigDecimal getAverageDaysPerWeek() {
        return (new java.math.BigDecimal("7"));
    }

    
    /**
     *  Gets a label for a unit.
     *
     *  @param units the granularity
     *  @return a label
     *  @throws org.osid.NullArgumentException <code>units</code> is
     *          <code>null</code>
     */

    @Override
    public String getLabel(org.osid.calendaring.DateTimeResolution units) {
        nullarg(units, "units");

        switch (units) {
        case INFINITY:
            return ("∞");
        case AEON:
            return ("aeon");
        case EPOCH:
            return ("epoch");
        case GLACIAL:
            return ("ice age");
        case MILLENNIA:
            return ("millennium");
        case CENTURY:
            return ("century");
        case DECADE:
            return ("decade");
        case BLUEMOON:
            return ("blue moon");
        case YEAR:
            return ("year");
        case MONTH:
            return ("month");
        case WEEK:
            return ("week");
        case DAY:
            return ("day");
        default:
            return ("???");
        }
    }


    /**
     *  Gets a plural label for a unit.
     *
     *  @param units the granularity
     *  @return a label
     *  @throws org.osid.NullArgumentException <code>units</code> is
     *          <code>null</code>
     */

    @Override
    public String getPluralLabel(org.osid.calendaring.DateTimeResolution units) {
        switch (units) {
        case MILLENNIA:
            return ("millennia");
        case CENTURY:
            return ("centuries");
        }

        return (getLabel(units) + "s");
    }


    /**
     *  Gets the granularity for a label.
     *
     *  @param label
     *  @return the granularity
     *  @throws org.osid.NotFoundException units not found
     *  @throws org.osid.NullArgumentException <code>label</code> is
     *          <code>null</code>
     */

    @Override
    public org.osid.calendaring.DateTimeResolution getUnits(String label)
        throws org.osid.NotFoundException {

        switch (label.toLowerCase()) {
        case "∞":
            return (org.osid.calendaring.DateTimeResolution.INFINITY);
        case "aeon":
        case "aeons":
        case "eon":
        case "eons":
            return (org.osid.calendaring.DateTimeResolution.AEON);
        case "epoch":
        case "epochs":
            return (org.osid.calendaring.DateTimeResolution.EPOCH);
        case "iceage":
        case "iceages":
            return (org.osid.calendaring.DateTimeResolution.GLACIAL);
        case "millennium":
        case "millennia":
        case "mill":
        case "mill.":
            return (org.osid.calendaring.DateTimeResolution.MILLENNIA);
        case "century":
        case "centuries":
        case "cent":
        case "cent.":
            return (org.osid.calendaring.DateTimeResolution.CENTURY);
        case "decade":
        case "decades":
        case "'s":
            return (org.osid.calendaring.DateTimeResolution.DECADE);
        case "bluemoon":
        case "bluemoons":
        case "bm":
            return (org.osid.calendaring.DateTimeResolution.BLUEMOON);
        case "year":
        case "years":
        case "yr":
        case "yr.":
        case "yrs":
        case "yrs.":
            return (org.osid.calendaring.DateTimeResolution.YEAR);
        case "month":
        case "months":
        case "mon":
        case "mon.":
        case "mons":
        case "mons.":
            return (org.osid.calendaring.DateTimeResolution.MONTH);
        case "week":
        case "weeks":
        case "wk":
        case "wk.":
        case "wks":
        case "wks.":
            return (org.osid.calendaring.DateTimeResolution.WEEK);
        case "day":
        case "days":
            return (org.osid.calendaring.DateTimeResolution.DAY);
        default:
            throw new org.osid.NotFoundException(label + " not found");
        }
    }
}
