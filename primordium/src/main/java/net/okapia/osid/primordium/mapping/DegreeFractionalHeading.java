//
// DegreeFractionalHeading.java
//
//     An implementation of Primitive for OSID Headings.
//
//
// Tom Coppeto
// Okapia
// 21 October 2011
//
// Copyright (c) 2011 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.mapping;

import java.math.BigDecimal;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An implementation of Primitive for OSID Headings.
 */

public final class DegreeFractionalHeading 
    extends net.okapia.osid.primordium.mapping.spi.AbstractDegreeFractionalHeading
    implements org.osid.mapping.Heading {

    private static final long serialVersionUID = -181289066L;


    /**
     *  Constructs a new <code>DegreeFractionalHeading</code>.
     *
     *  @param degrees
     *  @throws org.osid.InvalidArgumentException values are out of range
     *  @throws org.osid.NullArgumentException <code>seconds</code>
     *          <code>null</code>
     */

    public DegreeFractionalHeading(BigDecimal degrees) {
        super(degrees);
        return;
    }


    /**
     *  Constructs a new <code>DegreeFractionalHeading</code>.
     *
     *  @param degrees
     *  @param minutes
     *  @param seconds
     *  @throws org.osid.InvalidArgumentException values are out of range
     *  @throws org.osid.NullArgumentException <code>seconds</code>
     *          <code>null</code>
     */

    public DegreeFractionalHeading(int degrees, int minutes, BigDecimal seconds) {
        super(degrees, minutes, seconds);
        return;
    }


    /**
     *  Constructs a new <code>DegreeFractionalHeading</code>.
     *
     *  @param degrees
     *  @param minutes
     *  @param seconds
     *  @param uncertaintyPlus the uncertainty in the positive direction
     *  @param uncertaintyMinus the uncertainty in the other directio
     *  @throws org.osid.InvalidArgumentException values are out of range
     *  @throws org.osid.NullArgumentException <code>seconds</code>,
     *          <code>uncertaintyPlus</code>, or
     *          <code>uncertaintyMinus</code> is <code>null</code>
     */

    public DegreeFractionalHeading(int degrees, int minutes, BigDecimal seconds, 
                                   BigDecimal uncertaintyPlus, BigDecimal uncertaintyMinus) {

        super(degrees, minutes, seconds, uncertaintyPlus, uncertaintyMinus);
        return;
    }


    /**
     *  Creates a degree Heading based on a decimal value in degrees.
     *
     *  @param value the number of degrees in the heading (0 <= value
     *         < 360)
     *  @return a heading
     *  @throws org.osid.InvalidArgumentException <code>value</code> is
     *          out of range
     *  @throws org.osid.NullArgumentException <code>value</code> is
     *          <code>null</code>
     */

    public static org.osid.mapping.Heading valueOf(BigDecimal value) {
        return (new DegreeFractionalHeading(value));
    }


    /**
     *  Creates a degree Heading based on a value in degrees.
     *
     *  @param degrees
     *  @return a heading
     *  @throws org.osid.InvalidArgumentException <code>degrees</code> is
     *          out of range
     */

    public static org.osid.mapping.Heading valueOf(int degrees) {
        return (new DegreeFractionalHeading(degrees, 0, new BigDecimal(0)));
    }


    /**
     *  Creates a degree Heading based on a value in degrees and minutes.
     *
     *  @param degrees
     *  @param minutes
     *  @return a heading
     *  @throws org.osid.InvalidArgumentException <code>degrees</code>
     *          or <code>minutes</code> is out of range
     */

    public static org.osid.mapping.Heading valueOf(int degrees, int minutes) {
        return (new DegreeFractionalHeading(degrees, minutes, new BigDecimal(0)));
    }


    /**
     *  Creates a degree Heading based on a value in degrees,
     *  minutes, and seconds.
     *
     *  @param degrees
     *  @param minutes
     *  @param seconds
     *  @return a heading
     *  @throws org.osid.InvalidArgumentException
     *          <code>degrees</code>, <code>minutes</code>, or
     *          <code>seconds</code> is out of range
     */

    public static org.osid.mapping.Heading valueOf(int degrees, int minutes, int seconds) {
        return (new DegreeFractionalHeading(degrees, minutes, new BigDecimal(seconds)));
    }


    /**
     *  Creates a degree Heading based on a value in degrees,
     *  minutes, and seconds.
     *
     *  @param degrees
     *  @param minutes
     *  @param seconds
     *  @return a heading
     *  @throws org.osid.InvalidArgumentException
     *          <code>degrees</code>, <code>minutes</code>, or
     *          <code>seconds</code> is out of range
     *  @throws org.osid.NullArgumentException <code>seconds</code> is
     *          <code>null</code>
     */

    public static org.osid.mapping.Heading valueOf(int degrees, int minutes, 
                                                   BigDecimal seconds) {
        return (new DegreeFractionalHeading(degrees, minutes, seconds));
    }


    /**
     *  Creates a degree Heading based on a decimal value in degrees.
     *
     *  @value the number of degrees in the heading (0 <= value < 360)
     *  @return a heading
     *  @throws org.osid.InvalidArgumentException <code>value</code> is
     *          out of range
     *  @throws org.osid.NullArgumentException <code>value</code> is
     *          <code>null</code>
     */

    @Override
    protected org.osid.mapping.Heading makeHeading(BigDecimal value) {
        return (valueOf(value));
    }
}
