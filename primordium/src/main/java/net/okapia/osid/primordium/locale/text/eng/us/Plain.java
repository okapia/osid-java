//
// Plain.java
//
//     An implementation of a Plain DisplayText.
//
//
// Tom Coppeto
// OnTapSolutions
// 12 May 2012
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.locale.text.eng.us;

import net.okapia.osid.primordium.types.text.FormatTypes;
import net.okapia.osid.primordium.types.text.eng.EnglishRegionTypes;
import net.okapia.osid.primordium.types.text.ISOScriptTypes;


/**
 *  An implementation of a Plain DisplayText.
 */

public final class Plain
    extends net.okapia.osid.primordium.locale.spi.AbstractDisplayText
    implements org.osid.locale.DisplayText {

    private static final long serialVersionUID = 703509375L;


    /**
     *  Constructs a new <code>Plain</code>.
     *
     *  @param text the text
     *  @throws org.osid.NullArgumentException <code>text</code> is
     *          <code>null</code>
     */

    public Plain(String text) {        
        super(EnglishRegionTypes.US.getType(), ISOScriptTypes.LATN.getType(), 
              FormatTypes.PLAIN.getType(), text);
        return;
    }


    /**
     *  Creates a <code>Plain</code> from a string.
     *
     *  @param s a string
     *  @throws org.osid.NullArgumentException <code>text</code> is
     *          <code>null</code>
     */
    
    public static org.osid.locale.DisplayText valueOf(String s) {
        return (new Plain(s));
    }


    /**
     *  Creates a <code>Plain</code> from a string.
     *
     *  @param s a string
     *  @throws org.osid.NullArgumentException <code>text</code> is
     *          <code>null</code>
     */
    
    public static org.osid.locale.DisplayText text(String s) {
        return (valueOf(s));
    }


    /**
     *  Creates a <code>Plain</code> from a string.
     *
     *  @param s a string
     *  @throws org.osid.NullArgumentException <code>text</code> is
     *          <code>null</code>
     */
    
    public static org.osid.locale.DisplayText engUSText(String s) {
        return (valueOf(s));
    }
}
