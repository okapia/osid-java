//
// AbstractStringTerm.java
//
//     Implements a StringTerm.
//
//
// Tom Coppeto
// Okapia
// 29 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.terms.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A basic implementation of StringTerm.
 */

public abstract class AbstractStringTerm
    extends net.okapia.osid.primordium.terms.spi.AbstractQueryTerm
    implements org.osid.search.terms.StringTerm {

    private final String s;
    private final org.osid.type.Type type;


    /**
     *  Creates a new <code>AbstractStringTerm</code>.
     *
     *  @param s the string
     *  @param stringMatchType a string match type
     *  @param match <code>true</code> if a positive term,
     *         <code>false</code> for a negative term
     *  @throws org.osid.NullArgumentException <code>s</code> or
     *          <code>stringMatchType</code> is <code>null</code>
     */

    public AbstractStringTerm(String s, org.osid.type.Type stringMatchType, boolean match) {
	super(match);

        nullarg(s, "string");
        nullarg(stringMatchType, "string match type");

        this.s = s;
        this.type = stringMatchType;

	return;
    }


    /**
     *  Gets the string in the term.
     *
     *  @return the string
     *  @throws org.osid.IllegalStateException
     *          <code>isWildcard()</code> is <code>true</code>
     */

    @OSID @Override
    public String getString() {
        return (this.s);
    }


    /**
     *  Gets the string match type in the term.
     *
     *  @return the string match type
     *  @throws org.osid.IllegalStateException
     *          <code>isWildcard()</code> is <code>true</code>
     */

    @OSID @Override
    public org.osid.type.Type getStringMatchType() {
        return (this.type);
    }
}
