//
// AbstractTimeSpecifier.java
//
//     An OSID Time specifier.
//
//
// Tom Coppeto
// Okapia
// 5 May 2012
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.calendaring.DateTimeResolution;

import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  The AbstractTimeSpecifier is used to construct
 *  <code>Times</code>. This class may be sublassed to perform
 *  validations based on the time Type.
 */

public abstract class AbstractTimeSpecifier {
    
    private final org.osid.type.Type timeType;
    
    private long hour          = 0;
    private long minute        = 0;
    private long second        = 0;
    private short millisecond  = 0;
    private short microsecond  = 0;
    private short nanosecond   = 0;
    private short picosecond   = 0;
    private short femtosecond  = 0;
    private short attosecond   = 0;
    private short zeptosecond  = 0;
    private short yoctosecond  = 0;
    private short xoxxosecond  = 0;
    private short weeblesecond = 0;
    private short vatosecond   = 0;
    private short undasecond   = 0;
    private long plancks       = 0;
    
    private org.osid.calendaring.DateTimeResolution granularity = org.osid.calendaring.DateTimeResolution.INFINITY;
    private long multiplier = 1;
    

    /**
     *  Constructs a new <code>AbstractTimeSpecifier</code>.
     *
     *  @param timeType the type of time system
     *  @throws org.osid.NullArgumentException <code>timeType</code>
     *          is <code>null</code>
     */
    
    protected AbstractTimeSpecifier(org.osid.type.Type timeType) {        
        nullarg(timeType, "time type");
        this.timeType = timeType;
        
        return;
    }
    
    
    /**
     *  Sets a Time.
     *
     *  @param time a Time to copy
     *  @throws org.osid.InvalidArgumentException <code>time</code>
     *          is not valid
     *  @throws org.osid.NullArgumentException <code>time</code>
     *          <code>null</code>
     */
    
    @SuppressWarnings("fallthrough")
    protected void setTime(org.osid.calendaring.Time time) {
        
        nullarg(time, "time");
        
        if (!time.getTimeType().equals(getTimeType())) {
            throw new org.osid.InvalidArgumentException("incompatible types");
        }

        this.granularity   = time.getGranularity();
        this.multiplier    = time.getGranularityMultiplier();

        switch (time.getGranularity()) {
        case PLANCKSECOND:
            setPlanckSeconds(time.getPlanckSeconds());
        case UNDASECOND:
            setUndaseconds((short) time.getUndaseconds());
        case VATOSECOND:
            setVatoseconds((short) time.getVatoseconds());
        case WEEBLESECOND:
            setWeebleseconds((short) time.getWeebleseconds());
        case XOXXOSECOND:
            setXoxxoseconds((short) time.getXoxxoseconds());
        case YOCTOSECOND:
            setYoctoseconds((short) time.getYoctoseconds());
        case ZEPTOSECOND:
            setZeptoseconds((short) time.getZeptoseconds());
        case ATTOSECOND:
            setAttoseconds((short) time.getAttoseconds());
        case FEMTOSECOND:
            setFemtoseconds((short) time.getFemtoseconds());
        case PICOSECOND:
            setPicoseconds((short) time.getPicoseconds());
        case NANOSECOND:
            setNanoseconds((short) time.getNanoseconds());
        case MICROSECOND:
            setMicroseconds((short) time.getMicroseconds());        
        case MILLISECOND:
            setMilliseconds((short) time.getMilliseconds());       
        case SECOND:
            setSecond(time.getSecond());
        case MINUTE:
            setMinute(time.getMinute());
        case HOUR:
            setHour(time.getHour());
        }
            
        return;
    }
    

    /**
     *  Sets a Time from a  DateTime.
     *
     *  @param datetime a DateTime to copy
     *  @throws org.osid.InvalidArgumentException <code>datetime</code>
     *          is not valid
     *  @throws org.osid.NullArgumentException <code>datetime</code>
     *          <code>null</code>
     */
    
    @SuppressWarnings("fallthrough")
    protected void setDateTime(org.osid.calendaring.DateTime datetime) {
        
        nullarg(datetime, "datetime");
        
        if (!datetime.getTimeType().equals(getTimeType())) {
            throw new org.osid.InvalidArgumentException("incompatible types");
        }

        this.granularity   = datetime.getGranularity();
        this.multiplier    = datetime.getGranularityMultiplier();

        switch (datetime.getGranularity()) {
        case PLANCKSECOND:
            setPlanckSeconds(datetime.getPlanckSeconds());
        case UNDASECOND:
            setUndaseconds((short) datetime.getUndaseconds());
        case VATOSECOND:
            setVatoseconds((short) datetime.getVatoseconds());
        case WEEBLESECOND:
            setWeebleseconds((short) datetime.getWeebleseconds());
        case XOXXOSECOND:
            setXoxxoseconds((short) datetime.getXoxxoseconds());
        case YOCTOSECOND:
            setYoctoseconds((short) datetime.getYoctoseconds());
        case ZEPTOSECOND:
            setZeptoseconds((short) datetime.getZeptoseconds());
        case ATTOSECOND:
            setAttoseconds((short) datetime.getAttoseconds());
        case FEMTOSECOND:
            setFemtoseconds((short) datetime.getFemtoseconds());
        case PICOSECOND:
            setPicoseconds((short) datetime.getPicoseconds());
        case NANOSECOND:
            setNanoseconds((short) datetime.getNanoseconds());
        case MICROSECOND:
            setMicroseconds((short) datetime.getMicroseconds());        
        case MILLISECOND:
            setMilliseconds((short) datetime.getMilliseconds());       
        case SECOND:
            setSecond(datetime.getSecond());
        case MINUTE:
            setMinute(datetime.getMinute());
        case HOUR:
            setHour(datetime.getHour());
        }
            
        return;
    }
    
     
    /**
     *  Gets the time type. 
     *
     *  @return the time type 
     */
    
    public org.osid.type.Type getTimeType() {
        return (this.timeType);
    }
    
    
    /**
     *  Gets the granularity of this time. The granularity indicates
     *  the resolution of the clock. More precision than what is
     *  specified in this method cannot be inferred from the available
     *  data.
     *
     *  @return granularity 
     */
    
    public org.osid.calendaring.DateTimeResolution getGranularity() {
        return (this.granularity);
    }
    


    /**
     *  Sets the granularity of this time. The granularity indicates
     *  the resolution of the clock. More precision than what is
     *  specified in this method cannot be inferred from the available
     *  data.
     *
     *  @param granularity the resolution
     *  @throws org.osid.NullArgumentException
     *          <code>granularity</code> is <code>null</code>
     */
    
    public void setGranularity(org.osid.calendaring.DateTimeResolution granularity) {
        setGranularity(granularity, 1);
        return;
    }

    
    /**
     *  If the granularity of the time equals <code> getGranularity(),
     *  </code> then the multiplier is 1. This method may return a
     *  different number when the granularity differs from one of the
     *  defined resolutions.
     *
     *  @return granularity multiplier 
     */
    
    public long getGranularityMultiplier() {
        return (this.multiplier);
    }
    

    /**
     *  Sets the granularity of this time. The granularity indicates
     *  the resolution of the clock. More precision than what is
     *  specified in this method cannot be inferred from the available
     *  data.
     *
     *  @param granularity the resolution
     *  @param multiplier the granularity multiplier
     *  @throws org.osid.InvalidArgumentException
     *          <code>multiplier</code> is negative
     *  @throws org.osid.NullArgumentException
     *          <code>granularity</code> is <code>null</code>
     */
    
    public void setGranularity(org.osid.calendaring.DateTimeResolution granularity, long multiplier) {
        nullarg(granularity, "granularity");
        cardinalarg(multiplier, "granularity multiplier");
        
        this.granularity = granularity;
        this.multiplier  = multiplier;
        
        return;
    }

    
    /**
     *  Gets the hour of the day.
     *
     *  @return the hour of the day 
     */
    
    protected long getHour() {
        return (this.hour);
    }
    
    
    /**
     *  Sets the hour of the day. If the granularity is coarser than
     *  this resolution, the resolution will be set at an hour.
     *
     *  @param hour the hour of the day
     *  @throws org.osid.InvalidArgumentException <code>hour</code> is
     *          negative
     */
    
    public void setHour(long hour) {
        cardinalarg(hour, "hour");
        this.hour = hour;

        if (getGranularity().ordinal() > org.osid.calendaring.DateTimeResolution.HOUR.ordinal()) {
            setGranularity(org.osid.calendaring.DateTimeResolution.HOUR);
        }

        return;
    }
    
    
    /**
     *  Gets the minute of the hour.
     *
     *  @return the minute of the hour 
     */
    
    protected long getMinute() {
        return (this.minute);
    }
    
    
    /**
     *  Sets the minute of the hour. If the granularity is coarser
     *  than this resolution, the resolution will be set at a minute.
     *
     *  @param minute the minute of the hour
     *  @throws org.osid.InvalidArgumentException <code>minute</code>
     *          is negative
     */
    
    public void setMinute(long minute) {
        cardinalarg(minute, "minute");
        this.minute = minute;

        if (getGranularity().ordinal() > org.osid.calendaring.DateTimeResolution.MINUTE.ordinal()) {
            setGranularity(org.osid.calendaring.DateTimeResolution.MINUTE);
        }

        return;
    }
    
    
    /**
     *  Gets the second of the minute.
     *
     *  @return the second of the minute 
     */
    
    protected long getSecond() {
        return (this.second);
    }
    
    
    /**
     *  Sets the second of the minute. If the granularity is coarser
     *  than this resolution, the resolution will be set at a second.
     *
     *  @param second the second of the minute
     *  @throws org.osid.InvalidArgumentException <code>minute</code>
     *          is negative
     */
    
    public void setSecond(long second) {
        cardinalarg(second, "second");
        this.second = second;

        if (getGranularity().ordinal() > org.osid.calendaring.DateTimeResolution.SECOND.ordinal()) {
            setGranularity(org.osid.calendaring.DateTimeResolution.SECOND);
        }

        return;
    }
    
    
    /**
     *  Gets the number of milliseconds in this second 0-999. A
     *  millisecond is one thousandth of a second.
     *
     *  @return the milliseconds of the second 
     */
    
    protected long getMilliseconds() {
        return (this.millisecond);
    }
    
    
    /**
     *  Sets the number of milliseconds. If the granularity
     *  is coarser than this resolution, the resolution will be set at
     *  a millisecond.
     *
     *  @param milliseconds 0-999
     *  @throws org.osid.InvalidArgumentException
     *          <code>milliseconds</code> out of range
     */
    
    public void setMilliseconds(int milliseconds) {
        if ((milliseconds < 0) || (milliseconds > 999)) {
            throw new org.osid.InvalidArgumentException("milliseconds invalid");
        }
        
        this.millisecond = (short) milliseconds;        

        if (getGranularity().ordinal() > org.osid.calendaring.DateTimeResolution.MILLISECOND.ordinal()) {
            setGranularity(org.osid.calendaring.DateTimeResolution.MILLISECOND);
        }

        return;
    }
    
    
    /**
     *  Gets the number of microseconds of the second 0-999. A
     *  microsecond is one millionth of a second. 
     *
     *  @return the micrseconds of the millisecond 
     */
    
    protected long getMicroseconds() {
        return (this.microsecond);
    }
    
    
    /**
     *  Sets the number of microseconds. If the granularity is coarser
     *  than this resolution, the resolution will be set at a
     *  microsecond.
     *
     *  @param microseconds 0-999
     *  @throws org.osid.InvalidArgumentException
     *          <code>microseconds</code> out of range
     */
    
    public void setMicroseconds(int microseconds) {
        if ((microseconds < 0) || (microseconds > 999)) {
            throw new org.osid.InvalidArgumentException("microseconds invalid");
        }
        
        this.microsecond = (short) microseconds;

        if (getGranularity().ordinal() > org.osid.calendaring.DateTimeResolution.MICROSECOND.ordinal()) {
            setGranularity(org.osid.calendaring.DateTimeResolution.MICROSECOND);
        }

        return;
    }
    
    
    /**
     *  Gets the number of nanoseconds of the microsecond 0-999. A nanosecond 
     *  is one billionth of a second. 
     *
     *  @return the nanoseconds of the microsecond 
     */
    
    protected long getNanoseconds() {
        return (this.nanosecond);
    }
    
    
    /**
     *  Sets the number of nanoseconds. If the granularity is coarser
     *  than this resolution, the resolution will be set at a
     *  nanosecond.
     *
     *  @param nanoseconds 0-999
     *  @throws org.osid.InvalidArgumentException
     *          <code>nanoseconds</code> out of range
     */
    
    public void setNanoseconds(int nanoseconds) {
        if ((nanoseconds < 0) || (nanoseconds > 999)) {
            throw new org.osid.InvalidArgumentException("nanoseconds invalid");
        }
        
        this.nanosecond = (short) nanoseconds;

        if (getGranularity().ordinal() > org.osid.calendaring.DateTimeResolution.NANOSECOND.ordinal()) {
            setGranularity(org.osid.calendaring.DateTimeResolution.NANOSECOND);
        }

        return;
    }
    
    
    /**
     *  Gets the number of picoseconds of the nanosecond 0-999. A picosecond 
     *  is one trillionth of a second. 
     *
     *  @return the picoseconds of the nanosecond 
     */
    
    protected long getPicoseconds() {
        return (this.picosecond);
    }
    
    
    /**
     *  Sets the number of picoseconds. If the granularity
     *  is coarser than this resolution, the resolution will be set at
     *  a picosecond.
     *
     *  @param picoseconds 0-999
     *  @throws org.osid.InvalidArgumentException
     *          <code>picoseconds</code> out of range
     */
    
    public void setPicoseconds(int picoseconds) {
        if ((picoseconds < 0) || (picoseconds > 999)) {
            throw new org.osid.InvalidArgumentException("picoseconds invalid");
        }
        
        this.picosecond = (short) picoseconds;

        if (getGranularity().ordinal() > org.osid.calendaring.DateTimeResolution.PICOSECOND.ordinal()) {
            setGranularity(org.osid.calendaring.DateTimeResolution.PICOSECOND);
        }

        return;
    }
    
    
    /**
     *  Gets the number of femtoseconds of the picosecond 0-999. A femtosecond 
     *  is one quadrillionth of a second. 
     *
     *  @return the femtoseconds of the picosecond 
     */
    
    protected long getFemtoseconds() {
        return (this.femtosecond);
    }
    
    
    /**
     *  Sets the number of femtoseconds.If the granularity is coarser
     *  than this resolution, the resolution will be set at a
     *  femtosecond.
     *
     *  @param femtoseconds 0-999
     *  @throws org.osid.InvalidArgumentException
     *          <code>femtoseconds</code> out of range
     */
    
    public void setFemtoseconds(int femtoseconds) {
        if ((femtoseconds < 0) || (femtoseconds > 999)) {
            throw new org.osid.InvalidArgumentException("femtoseconds invalid");
        }
        
        this.femtosecond = (short) femtoseconds;

        if (getGranularity().ordinal() > org.osid.calendaring.DateTimeResolution.FEMTOSECOND.ordinal()) {
            setGranularity(org.osid.calendaring.DateTimeResolution.FEMTOSECOND);
        }

        return;
    }
    
    
    /**
     *  Gets the number of attoseconds of the femtoseconds 0-999. An 
     *  attosecond is one quintillionth of a second. 
     *
     *  @return the attoseconds of the femtosecond 
     */
    
    protected long getAttoseconds() {
        return (this.attosecond);
    }
    
    
    /**
     *  Sets the number of attoseconds. If the granularity is coarser
     *  than this resolution, the resolution will be set at a
     *  attosecond.
     *
     *  @param attoseconds 0-999
     *  @throws org.osid.InvalidArgumentException
     *          <code>attoseconds</code> out of range
     */
    
    public void setAttoseconds(int attoseconds) {
        if ((attoseconds < 0) || (attoseconds > 999)) {
            throw new org.osid.InvalidArgumentException("attoseconds invalid");
        }
        
        this.attosecond = (short) attoseconds;

        if (getGranularity().ordinal() > org.osid.calendaring.DateTimeResolution.ATTOSECOND.ordinal()) {
            setGranularity(org.osid.calendaring.DateTimeResolution.ATTOSECOND);
        }

        return;
    }
    
    
    /**
     *  Gets the number of zeptoseconds of the attosecond 0-999. A zeptosecond 
     *  is one sextillionth of a second. 
     *
     *  @return the zeptoseconds of the attosecond 
     */
    
    protected long getZeptoseconds() {
        return (this.zeptosecond);
    }
    
    
    /**
     *  Sets the number of zeptoseconds. If the granularity is coarser
     *  than this resolution, the resolution will be set at a
     *  zeptosecond.
     *
     *  @param zeptoseconds 0-999
     *  @throws org.osid.InvalidArgumentException
     *          <code>zeptoseconds</code> out of range
     */
    
    public void setZeptoseconds(int zeptoseconds) {
        if ((zeptoseconds < 0) || (zeptoseconds > 999)) {
            throw new org.osid.InvalidArgumentException("zeptoseconds invalid");
        }
        
        this.zeptosecond = (short) zeptoseconds;

        if (getGranularity().ordinal() > org.osid.calendaring.DateTimeResolution.ZEPTOSECOND.ordinal()) {
            setGranularity(org.osid.calendaring.DateTimeResolution.ZEPTOSECOND);
        }

        return;
    }
    
    
    /**
     *  Gets the number of yoctoseconds of the picosecond 0-999. A yoctosecond 
     *  is one septillionth of a second. This is getting quite small. 
     *
     *  @return the yoctoseconds of the yoctosecond 
     */
    
    protected long getYoctoseconds() {
        return (this.yoctosecond);
    }
    
    
    /**
     *  Sets the number of yoctoseconds. If the granularity is coarser
     *  than this resolution, the resolution will be set at a
     *  yoctosecond.
     *
     *  @param yoctoseconds 0-999
     *  @throws org.osid.InvalidArgumentException
     *          <code>yoctoseconds</code> out of range
     */
    
    public void setYoctoseconds(int yoctoseconds) {
        if ((yoctoseconds < 0) || (yoctoseconds > 999)) {
            throw new org.osid.InvalidArgumentException("yoctoseconds invalid");
        }
        
        this.yoctosecond = (short) yoctoseconds;

        if (getGranularity().ordinal() > org.osid.calendaring.DateTimeResolution.YOCTOSECOND.ordinal()) {
            setGranularity(org.osid.calendaring.DateTimeResolution.YOCTOSECOND);
        }

        return;
    }
    
    
    /**
     *  Gets the number of xoxxoseconds of the yoctosecond 0-999. A 
     *  xoxxosecond is one octillionth of a second. We're going with Rudy 
     *  Rucker here. 
     *
     *  @return the xoxxoseconds of the yoctosecond 
     */
    
    protected long getXoxxoseconds() {
        return (this.xoxxosecond);
    }
    
    
    /**
     *  Sets the number of xoxxoseconds. If the granularity is coarser
     *  than this resolution, the resolution will be set at a
     *  xoxxosecond.
     *
     *  @param xoxxoseconds 0-999
     *  @throws org.osid.InvalidArgumentException
     *          <code>xoxxoseconds</code> out of range
     */
    
    public void setXoxxoseconds(int xoxxoseconds) {
        if ((xoxxoseconds < 0) || (xoxxoseconds > 999)) {
            throw new org.osid.InvalidArgumentException("xoxxoseconds invalid");
        }
        
        this.xoxxosecond = (short) xoxxoseconds;

        if (getGranularity().ordinal() > org.osid.calendaring.DateTimeResolution.XOXXOSECOND.ordinal()) {
            setGranularity(org.osid.calendaring.DateTimeResolution.XOXXOSECOND);
        }

        return;
    }
    
    
    /**
     *  Gets the number of weebleseconds of the xoxxosecond 0-999. A
     *  weeblesecond is one nonillionth of a second.
     *
     *  @return the weebleseconds of the xoxxoseconds 
     */
    
    protected long getWeebleseconds() {
        return (this.weeblesecond);
    }
    
    
    /**
     *  Sets the number of weebleseconds. If the granularity is
     *  coarser than this resolution, the resolution will be set at a
     *  weeblesecond.
     *
     *  @param weebleseconds 0-999
     *  @throws org.osid.InvalidArgumentException
     *          <code>weebleseconds</code> out of range
     */
    
    public void setWeebleseconds(int weebleseconds) {
        if ((weebleseconds < 0) || (weebleseconds > 999)) {
            throw new org.osid.InvalidArgumentException("weebleseconds invalid");
        }
        
        this.weeblesecond = (short) weebleseconds;

        if (getGranularity().ordinal() > org.osid.calendaring.DateTimeResolution.WEEBLESECOND.ordinal()) {
            setGranularity(org.osid.calendaring.DateTimeResolution.WEEBLESECOND);
        }

        return;
    }
    
    
    /**
     *  Gets the number of vatoseconds of the xoxxosecond 0-999. A
     *  vatosecond is one decillionth of a second.
     *
     *  @return the vatoseconds of the weeblesecond 
     */
    
    protected long getVatoseconds() {
        return (this.vatosecond);
    }
    
    
    /**
     *  Sets the number of vatoseconds. If the granularity is coarser
     *  than this resolution, the resolution will be set at a
     *  vatosecond.
     *
     *  @param vatoseconds 0-999
     *  @throws org.osid.InvalidArgumentException
     *          <code>vatoseconds</code> out of range
     */
    
    public void setVatoseconds(int vatoseconds) {
        if ((vatoseconds < 0) || (vatoseconds > 999)) {
            throw new org.osid.InvalidArgumentException("vatoseconds invalid");
        }
        
        this.vatosecond = (short) vatoseconds;

        if (getGranularity().ordinal() > org.osid.calendaring.DateTimeResolution.VATOSECOND.ordinal()) {
            setGranularity(org.osid.calendaring.DateTimeResolution.VATOSECOND);
        }

        return;
    }
    
    
    /**
     *  Gets the number of undaseconds of the vatosecond 0-999. An
     *  undasecond is one unadecillionth of a second.
     *
     *  @return the undaseconds of the vatosecond 
     */
    
    protected long getUndaseconds() {
        return (this.undasecond);
    }
    
    
    /**
     *  Sets the number of undaseconds. If the granularity is coarser
     *  than this resolution, the resolution will be set at a
     *  undasecond.
     *
     *  @param undaseconds 0-999
     *  @throws org.osid.InvalidArgumentException
     *          <code>undaseconds</code> out of range
     */
    
    public void setUndaseconds(int undaseconds) {
        if ((undaseconds < 0) || (undaseconds > 999)) {
            throw new org.osid.InvalidArgumentException("undaseconds invalid");
        }
        
        this.undasecond = (short) undaseconds;

        if (getGranularity().ordinal() > org.osid.calendaring.DateTimeResolution.UNDASECOND.ordinal()) {
            setGranularity(org.osid.calendaring.DateTimeResolution.UNDASECOND);
        }

        return;
    }
    
    
    /**
     *  Gets the number of Plancks of the vatoseconds. A Planck is 10
     *  quattuordecillionths of a second.
     *
     *  @return the plancks of the undasecond 
     */
    
    protected long getPlanckSeconds() {
        return (this.plancks);
    }
    
    
    /**
     *  Sets the number of planck seconds. If the granularity is
     *  coarser than this resolution, the resolution will be set at a
     *  plancksecond.
     *
     *  @param plancks 0-18550948
     *  @throws org.osid.InvalidArgumentException <code>plancks</code>
     *          out of range
     */
    
    public void setPlanckSeconds(long plancks) {
        if ((plancks < 0) || (plancks > AbstractTime.PLANCKS_IN_UNDASECOND)) {
            throw new org.osid.InvalidArgumentException("plancks invalid");
        }
        
        this.plancks = plancks;

        if (getGranularity().ordinal() > org.osid.calendaring.DateTimeResolution.PLANCKSECOND.ordinal()) {
            setGranularity(org.osid.calendaring.DateTimeResolution.PLANCKSECOND);
        }

        return;
    }
}
