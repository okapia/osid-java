//
// GeographicCoordinate.java
//
//     An implementation of a geographic coordinate with latitude and
//     longitude.
//
//
// Tom Coppeto
// Okapia
// 13 May 2012
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.mapping;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.primordium.type.BasicType;
import java.math.BigDecimal;


/**
 *  An implementation of a geographic coordinate based on latitude and
 *  longitude. 
 */

public final class GeographicCoordinate
    extends net.okapia.osid.primordium.mapping.spi.AbstractGeographicCoordinate
    implements org.osid.mapping.Coordinate {

    private static final org.osid.type.Type TYPE = BasicType.valueOf("coordinate:geographic@okapia.net");
    private static final long serialVersionUID = 165876994L;


    /**
     *  Constructs a new <code>GeographicCoordinate</code>.
     *
     *  @param latitude the latitude
     *  @param longitude the longitude
     *  @throws org.osid.InvalidArgumentException
     *          <code>latitude</code> or <code>longitude</code> is out
     *          of range
     */

    public GeographicCoordinate(long latitude, long longitude) {
        this(new BigDecimal(latitude), new BigDecimal(longitude));
        return;
    }


    /**
     *  Constructs a new <code>GeographicCoordinate</code>.
     *
     *  @param latitude the latitude
     *  @param longitude the longitude
     *  @throws org.osid.InvalidArgumentException
     *          <code>latitude</code> or <code>longitude</code> is out
     *          of range
     *  @throws org.osid.NullArgumentException <code>latitude</code>,
     *          or <code>longitude</code> is <code>null</code>
     */

    public GeographicCoordinate(BigDecimal latitude, BigDecimal longitude) {
        super(TYPE, latitude, longitude);
        return;
    }


    /**
     *  Constructs a new <code>GeographicCoordinate</code>.
     *
     *  @param latitude the latitude
     *  @param longitude the longitude
     *  @param latitudeUncertaintyPlus the latitude uncertainty in the
     *         positive direction
     *  @param latitudeUncertaintyMinus the latitude uncertainty in the
     *         other direction
     *  @param longitudeUncertaintyPlus the longitude uncertainty in the
     *         positive direction
     *  @param longitudeUncertaintyMinus the longitude uncertainty in
     *         the other direction
     *  @throws org.osid.InvalidArgumentException
     *          <code>latitude</code> or <code>longitude</code> is out
     *          of range
     *  @throws org.osid.NullArgumentException <code>latitude</code>,
     *          <code>longitude</code,
     *          <code>latitudeUncertaintyPlus</code>,
     *          <code>latitudeUncertaintyMinus</code>,
     *          <code>longitudeUncertaintyPlus</code>, or
     *          <code>longitudeUncertaintyMinus</code>, is
     *          <code>null</code>
     */

    public GeographicCoordinate(BigDecimal latitude, BigDecimal longitude,
                                BigDecimal latitudeUncertaintyPlus, BigDecimal latitudeUncertaintyMinus,
                                BigDecimal longitudeUncertaintyPlus, BigDecimal longitudeUncertaintyMinus) {

        super(TYPE, latitude, longitude, latitudeUncertaintyPlus, latitudeUncertaintyMinus,
              longitudeUncertaintyPlus, longitudeUncertaintyMinus);
        return;
    }


    /**
     *  Makes an unknown Coordinate.
     *
     *  @return a coordinate
     */

    public static org.osid.mapping.Coordinate unknown() {
        return new GeographicCoordinate(new BigDecimal(0), new BigDecimal(0),
                                        new BigDecimal(90), new BigDecimal(90),
                                        new BigDecimal(90), new BigDecimal(90));
    }


    /**
     *  Makes a simple coordinate.
     *
     *  @param latitude
     *  @param longitude
     *  @return a coordinate
     */

    @Override
    protected org.osid.mapping.Coordinate makeCoordinate(BigDecimal latitude, BigDecimal longitude) {
        return (new GeographicCoordinate(latitude, longitude));
    }


    /**
     *  Makes a simple spatial unit.
     *
     *  @param bounds
     *  @return the bounding coordinates
     */

    @Override
    protected org.osid.mapping.SpatialUnit makeSpatialUnit(org.osid.mapping.Coordinate[] bounds) {
        return null;
    }
}
