//
// BasicType.java
//
//     Implements a basic Type.
//
//
// Tom Coppeto
// OnTapSolutions
// 29 June 2008
//
//
// Copyright (c) 2008 Massachusetts Institute of Technology. All Rights 
// Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.type;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A basic implementation of type.
 */

public final class BasicType
    extends net.okapia.osid.primordium.type.spi.AbstractType
    implements org.osid.type.Type {

    private static final long serialVersionUID = -1711019819L;


    /**
     *  Creates a new <code>BasicType</code> without metadata.
     *
     *  @param authority the authority of this type identifier
     *  @param namespace the namespace of this type identifier
     *  @param identifier the type identifier
     *  @throws org.osid.NullArgumentException <code>null</code> argument
     *          provided
     */

    public BasicType(String authority, String namespace, String identifier) {
        super(authority, namespace, identifier);
        return;
    }


    /**
     *  Creates a new <code>BasicType</code>.
     *
     *  @param authority the authority of this type identifier
     *  @param namespace the namespace of this type identifier
     *  @param identifier the type identifier
     *  @param domain the domain of this type
     *  @param name the name of this type
     *  @param label the label (short name) for this type
     *  @param description a description of this type
     *  @throws org.osid.NullArgumentException <code>null</code> argument
     *          provided
     */

    public BasicType(String authority, String namespace, String identifier, 
                     org.osid.locale.DisplayText domain, org.osid.locale.DisplayText name, 
                     org.osid.locale.DisplayText label, org.osid.locale.DisplayText description) {
        
        super(authority, namespace, identifier, domain, name, label, description);
        return;
    }


    /*
     *  Returns a type copied from another type.
     *
     *  @param type a type
     *  @throws org.osid.NullArgumentException <code>type</code> is
     *          <code>null</code>.
     */

    public static org.osid.type.Type valueOf(org.osid.type.Type type) {
        nullarg(type, "type");

        return (new BasicType(type.getAuthority(), type.getIdentifierNamespace(),
                              type.getIdentifier(), type.getDomain(), 
                              type.getDisplayName(), type.getDisplayLabel(), 
                              type.getDescription())); 
    }


    /*
     *  Returns a type from the string components of the format
     *  namespace:identifier@authority.
     *
     *  @param str the string to parse
     *  @throws org.osid.InvalidArgumentException if an error is
     *          encountered parsing the string
     *  @throws org.osid.NullArgumentException <code>str</code> is
     *          <code>null</code>.
     */

    public static org.osid.type.Type valueOf(String str) {
        String authority;
        String namespace;
        String identifier;

        nullarg(str, "str");

        int nindex = str.indexOf(':');
        if (nindex < 0) {
            throw new org.osid.InvalidArgumentException("cannot parse " + str);
        }
        
        namespace = unescape(str.substring(0, nindex));
        
        int aindex = str.lastIndexOf('@');
        if (aindex < 0) {
            throw new org.osid.InvalidArgumentException("cannot parse " + str);
        }
        
        authority = unescape(str.substring(aindex + 1));
        identifier = unescape(str.substring(nindex + 1, aindex));

        return (new BasicType(authority, namespace, identifier));
    }


    @Override
    protected String formatLabel() {
        return (escape(getIdentifierNamespace()) + ":" + escape(getIdentifier()) + "@" + escape(getAuthority()));
    }
   

    private static String escape(String s) {
        return (s.replaceAll("%", "%25").replaceAll(":", "%3a").replaceAll("@", "%40"));
    }


    private static String unescape(String s) {
        return (s.replaceAll("%25", "%").replaceAll("%3a", ":").replaceAll("%40", "@"));
    }        
}
