//
// AbstractVersionTerm.java
//
//     Implements a VersionTerm.
//
//
// Tom Coppeto
// Okapia
// 29 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.terms.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A basic implementation of VersionTerm.
 */

public abstract class AbstractVersionTerm
    extends AbstractQueryTerm
    implements org.osid.search.terms.VersionTerm {

    private final org.osid.installation.Version version;


    /**
     *  Creates a new <code>AbstractVersionTerm</code>.
     *
     *  @param version the version value
     *  @param match <code>true</code> if a positive term,
     *         <code>false</code> for a negative term
     *  @throws org.osid.NullArgumentException <code>version</code>
     *          is <code>null</code>
     */

    public AbstractVersionTerm(org.osid.installation.Version version, boolean match) {
	super(match);

        nullarg(version, "version");
        this.version = version;

	return;
    }


    /**
     *  Gets the version value in the term.
     *
     *  @return the version value
     *  @throws org.osid.IllegalStateException
     *          <code>isWildcard()</code> is <code>true</code>
     */

    @OSID @Override
    public org.osid.installation.Version getVersion() {
        return (this.version);
    }
}
