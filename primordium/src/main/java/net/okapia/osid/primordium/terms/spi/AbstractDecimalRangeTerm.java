//
// AbstractDecimalRangeTerm.java
//
//     Implements a DecimalRangeTerm.
//
//
// Tom Coppeto
// Okapia
// 29 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.terms.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.decimalrangearg;


/**
 *  A basic implementation of DecimalRangeTerm.
 */

public abstract class AbstractDecimalRangeTerm
    extends AbstractQueryTerm
    implements org.osid.search.terms.DecimalRangeTerm {

    private final java.math.BigDecimal start;
    private final java.math.BigDecimal end;


    /**
     *  Creates a new <code>AbstractDecimalRangeTerm</code>.
     *
     *  @param start the decimal start range
     *  @param end the decimal end range
     *  @param match <code>true</code> if a positive term,
     *         <code>false</code> for a negative term
     *  @throws org.osid.InvalidArgumentException <code>start</code> is greater
     *          than <code>end</code>
     *  @throws org.osid.NullArgumentException <code>start</code> or
     *          <code>end</code> is <code>null</code>
     */

    public AbstractDecimalRangeTerm(java.math.BigDecimal start, java.math.BigDecimal end, boolean match) {
	super(match);
        
        decimalrangearg(start, end, "start decimal range", "end decimal range");
        this.start = start;
        this.end   = end;

	return;
    }


    /**
     *  Gets the start of the <code> decimal </code> range in the term.
     *
     *  @return the decimal start
     *  @throws org.osid.IllegalStateException
     *          <code>isWildcard()</code> is <code>true</code>
     */

    @OSID @Override
    public java.math.BigDecimal getDecimalRangeStart() {
        return (this.start);
    }


    /**
     *  Gets the end of the <code> decimal </code> range in the term.
     *
     *  @return the decimal end
     *  @throws org.osid.IllegalStateException
     *          <code>isWildcard()</code> is <code>true</code>
     */

    @OSID @Override
    public java.math.BigDecimal getDecimalRangeEnd() {
        return (this.end);
    }
}
