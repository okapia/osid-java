//
// UTCTimeDefinition.java
//
//     A defitnition of a UTC time definition.
//
//
// Tom Coppeto
// Okapia
// 2 May 2012
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.calendaring;


/**
 *  A definition of a UTC time system used by ConfigurableTime.
 */

public final class UTCTimeDefinition
    extends net.okapia.osid.primordium.calendaring.spi.Abstract246060TimeDefinition
    implements TimeDefinition {

    private static final org.osid.type.Type TYPE = net.okapia.osid.primordium.types.time.EarthTimeTypes.UTC.getType();
    private static final long serialVersionUID = 3176756682L;

    
    /**
     *  Gets the time type.
     *
     *  @return the time type
     */
    
    @Override
    public org.osid.type.Type getTimeType() {
        return (TYPE);
    }
}
