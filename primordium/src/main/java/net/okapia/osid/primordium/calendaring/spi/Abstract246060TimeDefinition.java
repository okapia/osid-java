//
// Abstract246060TimeDefinition.java
//
//     A defitnition of a Abstract246060 time definition.
//
//
// Tom Coppeto
// Okapia
// 2 May 2012
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.primordium.calendaring.spi;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A definition of a Abstract246060 time system used by
 *  ConfigurableTime.
 */

public abstract class Abstract246060TimeDefinition
    implements net.okapia.osid.primordium.calendaring.TimeDefinition {

    private static final long serialVersionUID = -14458119961L;

    
    /**
     *  Gets the number of hours in a day.
     *
     *  @return the hours in a day
     */
    
    @Override
    public long getHoursPerDay() {
        return (24);
    }
    

    /**
     *  Gets the starting number for the hour.
     *
     *  @return the starting hour
     */

    @Override
    public long getStartingHour() {
        return (0);
    }


    /**
     *  Gets the number of minutes in a hour.
     *
     *  @return the minutes in a hour
     */

    @Override
    public long getMinutesPerHour() {
        return (60);
    }


    /**
     *  Gets the starting minute for the hour.
     *
     *  @return the starting minute
     */

    @Override
    public long getStartingMinute() {
        return (0);
    }


    /**
     *  Gets the number of seocnds in a minute.
     *
     *  @return the seconds in a minute
     */

    @Override
    public long getSecondsPerMinute() {
        return (60);
    }


    /**
     *  Gets the starting second for the minute.
     *
     *  @return the starting second
     */

    @Override
    public long getStartingSecond() {
        return (0);
    }


    /**
     *  Gets the number of planck seconds in a second.
     * 
     *  @return the number of planck seconds
     */

    @Override
    public java.math.BigDecimal getPlancksPerSecond() {
        return (new java.math.BigDecimal("1.855094832e43"));
    }


    /**
     *  Gets a label for a unit.
     *
     *  @param units the granularity
     *  @return a label
     *  @throws org.osid.NullArgumentException <code>units</code> is
     *          <code>null</code>
     */

    @Override
    public String getLabel(org.osid.calendaring.DateTimeResolution units) {
        nullarg(units, "units");

        switch (units) {
        case HOUR:
            return ("hour");
        case HALF_HOUR:
        case QUARTER_HOUR:
        case MINUTE:
            return ("minute");
        case SECOND:
            return ("second");
        case MILLISECOND:
            return ("ms");
        case MICROSECOND:
            return ("µs");
        case NANOSECOND:
            return ("ns");
        case PICOSECOND:
            return ("ps");
        case FEMTOSECOND:
            return ("fs");
        case ATTOSECOND:
            return ("as");
        case ZEPTOSECOND:
            return ("zs");
        case YOCTOSECOND:
            return ("ys");
        case XOXXOSECOND:
            return ("xs");
        case WEEBLESECOND:
            return ("ws");
        case VATOSECOND:
            return ("vs");
        case UNDASECOND:
            return ("us");
        case PLANCKSECOND:
            return ("planck second");
        default:
            return ("???");
        }
    }


    /**
     *  Gets a plural label for a unit.
     *
     *  @param units the granularity
     *  @return a label
     *  @throws org.osid.NullArgumentException <code>units</code> is
     *          <code>null</code>
     */

    @Override
    public String getPluralLabel(org.osid.calendaring.DateTimeResolution units) {
        switch (units) {
        case HOUR:
        case HALF_HOUR:
        case QUARTER_HOUR:
        case MINUTE:
        case SECOND:
        case PLANCKSECOND:
            return (getLabel(units) + "s");
        }

        return (getLabel(units));
    }


    /**
     *  Gets the granularity for a label.
     *
     *  @param label
     *  @return the granularity
     *  @throws org.osid.NotFoundException units not found
     *  @throws org.osid.NullArgumentException <code>label</code> is
     *          <code>null</code>
     */

    @Override
    public org.osid.calendaring.DateTimeResolution getUnits(String label)
        throws org.osid.NotFoundException {

        switch (label.toLowerCase()) {
        case "hour":
        case "hours":
        case "hr":
        case "hr.":
        case "hrs":
        case "hrs.":
        case "h":
            return (org.osid.calendaring.DateTimeResolution.HOUR);
        case "minute":
        case "minutes":
        case "min":
        case "min.":
        case "mins":
        case "mins.":
        case "m":
            return (org.osid.calendaring.DateTimeResolution.MINUTE);
        case "second":
        case "seconds":
        case "sec":
        case "sec.":
        case "secs":
        case "secs.":
        case "s":
            return (org.osid.calendaring.DateTimeResolution.SECOND);
        case "millisecond":
        case "milliseconds":
        case "ms":
            return (org.osid.calendaring.DateTimeResolution.MILLISECOND);
        case "microsecond":
        case "microseconds":
        case "micro":
        case "micros":
        case "µs":
            return (org.osid.calendaring.DateTimeResolution.MICROSECOND);
        case "nanosecond":
        case "nanoseconds":
        case "nano":
        case "nanos":
        case "ns":
            return (org.osid.calendaring.DateTimeResolution.NANOSECOND);
        case "picosecond":
        case "picoseconds":
        case "pico":
        case "picos":
        case "ps":
            return (org.osid.calendaring.DateTimeResolution.PICOSECOND);
        case "femtosecond":
        case "femtoseconds":
        case "femto":
        case "femtos":
        case "fs":
            return (org.osid.calendaring.DateTimeResolution.FEMTOSECOND);
        case "attosecond":
        case "attoseconds":
        case "atto":
        case "attos":
        case "as":
            return (org.osid.calendaring.DateTimeResolution.ATTOSECOND);
        case "zeptosecond":
        case "zeptoseconds":
        case "zepto":
        case "zeptos":
        case "zs":
            return (org.osid.calendaring.DateTimeResolution.ZEPTOSECOND);
        case "yoctosecond":
        case "yoctoseconds":
        case "yocto":
        case "yoctos":
        case "ys":
            return (org.osid.calendaring.DateTimeResolution.YOCTOSECOND);
        case "xoxxosecond":
        case "xoxxoseconds":
        case "xoxxo":
        case "xoxxos":
        case "xs":
            return (org.osid.calendaring.DateTimeResolution.XOXXOSECOND);
        case "weeblesecond":
        case "weebleseconds":
        case "weeble":
        case "weebles":
        case "ws":
            return (org.osid.calendaring.DateTimeResolution.WEEBLESECOND);
        case "vatosecond":
        case "vatoseconds":
        case "vato":
        case "vatos":
        case "vs":
            return (org.osid.calendaring.DateTimeResolution.VATOSECOND);
        case "undasecond":
        case "undaseconds":
        case "unda":
        case "undas":
        case "us":
            return (org.osid.calendaring.DateTimeResolution.UNDASECOND);
        case "plancksecond":
        case "planckseconds":
        case "planck second":
        case "planck seconds":
        case "planck sec":
        case "planck secs":
        case "planck sec.":
        case "planck secs.":
        case "planck":
        case "plancks":
            return (org.osid.calendaring.DateTimeResolution.PLANCKSECOND);
        default:
            throw new org.osid.NotFoundException(label + " not found");
        }
    }
}
