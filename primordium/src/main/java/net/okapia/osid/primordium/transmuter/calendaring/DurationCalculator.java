

public class DurationCalculator {


    /**
     *  Calculates the ratio between two date time granularities. This
     *  method returns <code>r1/r2</code>. If <code>r1</code> is
     *  INFINITY then the calculation cannot be performed.
     *
     *  @param r1 a date time resolution
     *  @param r2 a date time resolution
     *  @throws org.osid.InvalidArgumentException <code>r1</code> is INIFNITY
     *  @throws org.osid.NullArgumentException <code>r1</code> or
     *          <code>r2</code> is <code>null</code>
     */

    /*    
    public static java.math.BigDecimal ratio(org.osid.calendaring.DateTimeResolution r1,
                                             org.osid.calendaring.DateTimeResolution r2) {
        
        nullarg(r1, "r1");
        nullarg(r2, "r2");
        
        if (r1 == DateTimeResolution.INFINITY) {
            throw new org.osid.InvalidArgumentException("infinity");
        }

        java.math.BigDecimal ratio = new java.math.BigDecimal(1);
        
        if (r1.equals(r2)) {
            return (ratio);
        }
        }*/
}
    