//
// TestObjects.java
//
//     A unit test for OSID Objects.
//
//
// Tom Coppeto
// Okapia
// 2 December 2012
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.nil.test;

import org.testng.Assert;
import org.testng.annotations.*;

public class TestObjects {


    @Test(dataProvider="objects")
    public void testValidity(Class<?> cl)
        throws Exception {

        java.lang.reflect.Method m = cl.getDeclaredMethod("create");
        Object object = m.invoke(null);
        Assert.assertNotNull(object);
        return;
    }

    
    @DataProvider(name = "objects")
    public Object[][] getObjects()
        throws Exception {
        
        Class[] roots = {
            Class.forName("org.osid.Identifiable", false, getClass().getClassLoader()),
            Class.forName("org.osid.Browsable", false, getClass().getClassLoader()),
            Class.forName("org.osid.OsidCapsule", false, getClass().getClassLoader())
        };

        Class[] exceptions = {
            Class.forName("org.osid.OsidNode", false, getClass().getClassLoader()),
            Class.forName("org.osid.OsidForm", false, getClass().getClassLoader()),
            Class.forName("org.osid.Sourceable", false, getClass().getClassLoader()),
            Class.forName("org.osid.OsidSearchResults", false, getClass().getClassLoader()),
            Class.forName("org.osid.transaction.batch.AliasRequest", false, getClass().getClassLoader())
        };
        

        java.util.Collection<Object> objects = new java.util.ArrayList<>();
        for (Class<?> c : getClasses(roots, exceptions)) {
            if (c.getName().endsWith("Peer") || c.getName().endsWith("IntersectionPath")) {
                continue;
            }

            if (c.getName().endsWith("DirectoryEntry")) {
                continue;
            }

            objects.add(getUnknown(c.getName()));
        }
        
        Object[][] ret = new Object[objects.size()][1];
        int i = 0;
        for (Object o : objects) {
            ret[i][0] = o;
            ++i;
        }

        return (ret);
    }


    public java.util.Collection<Class<?>> getClasses(Class[] roots, Class[] exceptions)
        throws Exception {
        
        java.util.Collection<Class<?>> rootClasses = new java.util.ArrayList<>(roots.length);
        for (Class<?> c : roots) {
            rootClasses.add(c);
        }


        java.util.Collection<Class<?>> exceptClasses = new java.util.ArrayList<>(exceptions.length);
        for (Class<?> c : exceptions) {
            exceptClasses.add(c);
        }

        return (getClasses(rootClasses, exceptClasses));
    }


    public java.util.Collection<Class<?>> getClasses(java.util.Collection<Class<?>> roots,
                                                     java.util.Collection<Class<?>> exceptions)
        throws Exception {

        java.util.Collection<Class<?>> ret = new java.util.HashSet<>();

        for (String name : getClassNamesFromPackage("org.osid")) {
            if (name.split("\\.").length == 3) {
                continue;
            }

            Class<?> c = Class.forName(name, false, getClass().getClassLoader());

            boolean skip = false;
            for (Class<?> exception : exceptions) {
                if (exception.isAssignableFrom(c)) {
                    skip = true;
                    break;
                }
            }

            if (skip) {
                skip = false;
                continue;
            }


            for (Class<?> root : roots) {
                if (root.isAssignableFrom(c)) {
                    ret.add(c);
                    break;
                }
            }
        }

        return (ret);
    }


    public static java.util.Collection<String> getClassNamesFromPackage(String packageName) 
        throws Exception {

        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        java.util.Collection<String> names = new java.util.ArrayList<>();
        java.net.URL packageURL;

        packageName = packageName.replace(".", "/");
        packageURL  = classLoader.getResource(packageName);

        if (packageURL.getProtocol().equals("jar")) {
            String jarFileName = java.net.URLDecoder.decode(packageURL.getFile(), "UTF-8");
            jarFileName = jarFileName.substring(5, jarFileName.indexOf("!"));
            java.util.Enumeration<java.util.jar.JarEntry> jarEntries = new java.util.jar.JarFile(jarFileName).entries();

            while (jarEntries.hasMoreElements()) {
                String entryName = jarEntries.nextElement().getName();
                if (entryName.startsWith(packageName) && entryName.endsWith(".class")) {
                    entryName = entryName.substring(0, entryName.lastIndexOf('.')).replaceAll("/", ".");
                    
                    names.add(entryName);
                }
            }
        } else {
            java.io.File[] files = new java.io.File(packageURL.getFile()).listFiles();

            for (java.io.File actual: files) {
                String entryName = actual.getName();
                entryName = entryName.substring(0, entryName.lastIndexOf('.'));
                names.add(entryName);
            }
        }

        return (names);
    }


    protected Class<?> getUnknown(String name)
        throws Exception {

        String[] tok = name.split("\\.");
        String pkg   = "net.okapia.osid.jamocha.nil";
        for (int i = 2; i < tok.length - 1; i++) {
            pkg = pkg + "." + tok[i];
        }

        String o = tok[tok.length - 1].toLowerCase();
        if (o.equals("package")) {
            o = "pkg";
        }

        pkg += "." + o;
        pkg += ".Unknown";
        pkg += tok[tok.length - 1];

        return (Class.forName(pkg, false, getClass().getClassLoader()));
    }
}