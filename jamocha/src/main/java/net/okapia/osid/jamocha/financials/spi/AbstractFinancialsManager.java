//
// AbstractFinancialsManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractFinancialsManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.financials.FinancialsManager,
               org.osid.financials.FinancialsProxyManager {

    private final Types accountRecordTypes                 = new TypeRefSet();
    private final Types accountSearchRecordTypes           = new TypeRefSet();

    private final Types activityRecordTypes                = new TypeRefSet();
    private final Types activitySearchRecordTypes          = new TypeRefSet();

    private final Types fiscalPeriodRecordTypes            = new TypeRefSet();
    private final Types fiscalPeriodSearchRecordTypes      = new TypeRefSet();

    private final Types summaryRecordTypes                 = new TypeRefSet();
    private final Types businessRecordTypes                = new TypeRefSet();
    private final Types businessSearchRecordTypes          = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractFinancialsManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractFinancialsManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if visible federation is supported. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if looking up accounts is supported. 
     *
     *  @return <code> true </code> if account lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAccountLookup() {
        return (false);
    }


    /**
     *  Tests if querying accounts is supported. 
     *
     *  @return <code> true </code> if account query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAccountQuery() {
        return (false);
    }


    /**
     *  Tests if searching accounts is supported. 
     *
     *  @return <code> true </code> if account search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAccountSearch() {
        return (false);
    }


    /**
     *  Tests if an account <code> </code> administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if account administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAccountAdmin() {
        return (false);
    }


    /**
     *  Tests if an account <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if account notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAccountNotification() {
        return (false);
    }


    /**
     *  Tests if an account hierarchy traversal is supported. 
     *
     *  @return <code> true </code> if an account hierarchy traversal is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAccountHierarchy() {
        return (false);
    }


    /**
     *  Tests if account hierarchy design is supported. 
     *
     *  @return <code> true </code> if an account hierarchy design is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAccountHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests if an account cataloging service is supported. 
     *
     *  @return <code> true </code> if account catalog is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAccountBusiness() {
        return (false);
    }


    /**
     *  Tests if an account cataloging service is supported. A cataloging 
     *  service maps accounts to catalogs. 
     *
     *  @return <code> true </code> if account cataloging is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAccountBusinessAssignment() {
        return (false);
    }


    /**
     *  Tests if an account smart business session is available. 
     *
     *  @return <code> true </code> if an account smart business session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAccountSmartBusiness() {
        return (false);
    }


    /**
     *  Tests if looking up activities is supported. 
     *
     *  @return <code> true </code> if activity lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityLookup() {
        return (false);
    }


    /**
     *  Tests if querying activities is supported. 
     *
     *  @return <code> true </code> if activity query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityQuery() {
        return (false);
    }


    /**
     *  Tests if searching activities is supported. 
     *
     *  @return <code> true </code> if activity search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivitySearch() {
        return (false);
    }


    /**
     *  Tests if activity administrative service is supported. 
     *
     *  @return <code> true </code> if activity administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityAdmin() {
        return (false);
    }


    /**
     *  Tests if an activity <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if activity notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityNotification() {
        return (false);
    }


    /**
     *  Tests if an activity hierarchy traversal is supported. 
     *
     *  @return <code> true </code> if an activity hierarchy traversal is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityHierarchy() {
        return (false);
    }


    /**
     *  Tests if activity hierarchy design is supported. 
     *
     *  @return <code> true </code> if an activity hierarchy design is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests if an activity cataloging service is supported. 
     *
     *  @return <code> true </code> if activity catalog is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityBusiness() {
        return (false);
    }


    /**
     *  Tests if an activity cataloging service is supported. A cataloging 
     *  service maps activities to catalogs. 
     *
     *  @return <code> true </code> if activity cataloging is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityBusinessAssignment() {
        return (false);
    }


    /**
     *  Tests if an activity smart business session is available. 
     *
     *  @return <code> true </code> if an activity smart business session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivitySmartBusiness() {
        return (false);
    }


    /**
     *  Tests if looking up fiscal periods is supported. 
     *
     *  @return <code> true </code> if fiscal period lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFiscalPeriodLookup() {
        return (false);
    }


    /**
     *  Tests if querying fiscal periods is supported. 
     *
     *  @return <code> true </code> if fiscal period query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFiscalPeriodQuery() {
        return (false);
    }


    /**
     *  Tests if searching fiscal periods is supported. 
     *
     *  @return <code> true </code> if fiscal period search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFiscalPeriodSearch() {
        return (false);
    }


    /**
     *  Tests if fiscal period <code> </code> administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if fiscal period administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFiscalPeriodAdmin() {
        return (false);
    }


    /**
     *  Tests if a fiscal period <code> </code> notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if fiscal period notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFiscalPeriodNotification() {
        return (false);
    }


    /**
     *  Tests if a fiscal period cataloging service is supported. 
     *
     *  @return <code> true </code> if fiscal period catalog is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFiscalPeriodBusiness() {
        return (false);
    }


    /**
     *  Tests if a fiscal period cataloging service is supported. A cataloging 
     *  service maps fiscal periods to catalogs. 
     *
     *  @return <code> true </code> if fiscal period cataloging is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFiscalPeriodBusinessAssignment() {
        return (false);
    }


    /**
     *  Tests if a fiscal period smart business session is available. 
     *
     *  @return <code> true </code> if a fiscal period smart business session 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFiscalPeriodSmartBusiness() {
        return (false);
    }


    /**
     *  Tests if looking up businesses is supported. 
     *
     *  @return <code> true </code> if business lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessLookup() {
        return (false);
    }


    /**
     *  Tests if searching businesses is supported. 
     *
     *  @return <code> true </code> if business search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessSearch() {
        return (false);
    }


    /**
     *  Tests if querying businesses is supported. 
     *
     *  @return <code> true </code> if business query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessQuery() {
        return (false);
    }


    /**
     *  Tests if business administrative service is supported. 
     *
     *  @return <code> true </code> if business administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessAdmin() {
        return (false);
    }


    /**
     *  Tests if a business <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if business notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessNotification() {
        return (false);
    }


    /**
     *  Tests for the availability of a business hierarchy traversal service. 
     *
     *  @return <code> true </code> if business hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessHierarchy() {
        return (false);
    }


    /**
     *  Tests for the availability of a business hierarchy design service. 
     *
     *  @return <code> true </code> if business hierarchy design is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests for the availability of a financials batch service. 
     *
     *  @return <code> true </code> if a financials batch service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFinancialsBatch() {
        return (false);
    }


    /**
     *  Tests for the availability of a financials budgeting service. 
     *
     *  @return <code> true </code> if a financials budgeting service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFinancialsBudgeting() {
        return (false);
    }


    /**
     *  Tests for the availability of a financials postng service. 
     *
     *  @return <code> true </code> if a financials posting service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFinancialsPosting() {
        return (false);
    }


    /**
     *  Gets the supported <code> Account </code> record types. 
     *
     *  @return a list containing the supported <code> Account </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAccountRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.accountRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Account </code> record type is supported. 
     *
     *  @param  accountRecordType a <code> Type </code> indicating an <code> 
     *          Account </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> accountRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAccountRecordType(org.osid.type.Type accountRecordType) {
        return (this.accountRecordTypes.contains(accountRecordType));
    }


    /**
     *  Adds support for an account record type.
     *
     *  @param accountRecordType an account record type
     *  @throws org.osid.NullArgumentException
     *  <code>accountRecordType</code> is <code>null</code>
     */

    protected void addAccountRecordType(org.osid.type.Type accountRecordType) {
        this.accountRecordTypes.add(accountRecordType);
        return;
    }


    /**
     *  Removes support for an account record type.
     *
     *  @param accountRecordType an account record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>accountRecordType</code> is <code>null</code>
     */

    protected void removeAccountRecordType(org.osid.type.Type accountRecordType) {
        this.accountRecordTypes.remove(accountRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Account </code> search record types. 
     *
     *  @return a list containing the supported <code> Account </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAccountSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.accountSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Account </code> search record type is 
     *  supported. 
     *
     *  @param  accountSearchRecordType a <code> Type </code> indicating an 
     *          <code> Account </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> accountSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAccountSearchRecordType(org.osid.type.Type accountSearchRecordType) {
        return (this.accountSearchRecordTypes.contains(accountSearchRecordType));
    }


    /**
     *  Adds support for an account search record type.
     *
     *  @param accountSearchRecordType an account search record type
     *  @throws org.osid.NullArgumentException
     *  <code>accountSearchRecordType</code> is <code>null</code>
     */

    protected void addAccountSearchRecordType(org.osid.type.Type accountSearchRecordType) {
        this.accountSearchRecordTypes.add(accountSearchRecordType);
        return;
    }


    /**
     *  Removes support for an account search record type.
     *
     *  @param accountSearchRecordType an account search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>accountSearchRecordType</code> is <code>null</code>
     */

    protected void removeAccountSearchRecordType(org.osid.type.Type accountSearchRecordType) {
        this.accountSearchRecordTypes.remove(accountSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Activity </code> record types. 
     *
     *  @return a list containing the supported <code> Activity </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getActivityRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.activityRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Activity </code> record type is supported. 
     *
     *  @param  activityRecordType a <code> Type </code> indicating an <code> 
     *          Activity </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> activityRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsActivityRecordType(org.osid.type.Type activityRecordType) {
        return (this.activityRecordTypes.contains(activityRecordType));
    }


    /**
     *  Adds support for an activity record type.
     *
     *  @param activityRecordType an activity record type
     *  @throws org.osid.NullArgumentException
     *  <code>activityRecordType</code> is <code>null</code>
     */

    protected void addActivityRecordType(org.osid.type.Type activityRecordType) {
        this.activityRecordTypes.add(activityRecordType);
        return;
    }


    /**
     *  Removes support for an activity record type.
     *
     *  @param activityRecordType an activity record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>activityRecordType</code> is <code>null</code>
     */

    protected void removeActivityRecordType(org.osid.type.Type activityRecordType) {
        this.activityRecordTypes.remove(activityRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Activity </code> search record types. 
     *
     *  @return a list containing the supported <code> Activity </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getActivitySearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.activitySearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Activity </code> search record type is 
     *  supported. 
     *
     *  @param  activitySearchRecordType a <code> Type </code> indicating an 
     *          <code> Activity </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> activitySearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsActivitySearchRecordType(org.osid.type.Type activitySearchRecordType) {
        return (this.activitySearchRecordTypes.contains(activitySearchRecordType));
    }


    /**
     *  Adds support for an activity search record type.
     *
     *  @param activitySearchRecordType an activity search record type
     *  @throws org.osid.NullArgumentException
     *  <code>activitySearchRecordType</code> is <code>null</code>
     */

    protected void addActivitySearchRecordType(org.osid.type.Type activitySearchRecordType) {
        this.activitySearchRecordTypes.add(activitySearchRecordType);
        return;
    }


    /**
     *  Removes support for an activity search record type.
     *
     *  @param activitySearchRecordType an activity search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>activitySearchRecordType</code> is <code>null</code>
     */

    protected void removeActivitySearchRecordType(org.osid.type.Type activitySearchRecordType) {
        this.activitySearchRecordTypes.remove(activitySearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> FiscalPeriod </code> record types. 
     *
     *  @return a list containing the supported <code> FiscalPeriod </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getFiscalPeriodRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.fiscalPeriodRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> FiscalPeriod </code> record type is 
     *  supported. 
     *
     *  @param  fiscalPeriodRecordType a <code> Type </code> indicating an 
     *          <code> FiscalPeriod </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> fiscalPeriodRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsFiscalPeriodRecordType(org.osid.type.Type fiscalPeriodRecordType) {
        return (this.fiscalPeriodRecordTypes.contains(fiscalPeriodRecordType));
    }


    /**
     *  Adds support for a fiscal period record type.
     *
     *  @param fiscalPeriodRecordType a fiscal period record type
     *  @throws org.osid.NullArgumentException
     *  <code>fiscalPeriodRecordType</code> is <code>null</code>
     */

    protected void addFiscalPeriodRecordType(org.osid.type.Type fiscalPeriodRecordType) {
        this.fiscalPeriodRecordTypes.add(fiscalPeriodRecordType);
        return;
    }


    /**
     *  Removes support for a fiscal period record type.
     *
     *  @param fiscalPeriodRecordType a fiscal period record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>fiscalPeriodRecordType</code> is <code>null</code>
     */

    protected void removeFiscalPeriodRecordType(org.osid.type.Type fiscalPeriodRecordType) {
        this.fiscalPeriodRecordTypes.remove(fiscalPeriodRecordType);
        return;
    }


    /**
     *  Gets the supported <code> FiscalPeriod </code> search record types. 
     *
     *  @return a list containing the supported <code> FiscalPeriod </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getFiscalPeriodSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.fiscalPeriodSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> FiscalPeriod </code> search record type is 
     *  supported. 
     *
     *  @param  fiscalPeriodSearchRecordType a <code> Type </code> indicating 
     *          a <code> FiscalPeriod </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          fiscalPeriodSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsFiscalPeriodSearchRecordType(org.osid.type.Type fiscalPeriodSearchRecordType) {
        return (this.fiscalPeriodSearchRecordTypes.contains(fiscalPeriodSearchRecordType));
    }


    /**
     *  Adds support for a fiscal period search record type.
     *
     *  @param fiscalPeriodSearchRecordType a fiscal period search record type
     *  @throws org.osid.NullArgumentException
     *  <code>fiscalPeriodSearchRecordType</code> is <code>null</code>
     */

    protected void addFiscalPeriodSearchRecordType(org.osid.type.Type fiscalPeriodSearchRecordType) {
        this.fiscalPeriodSearchRecordTypes.add(fiscalPeriodSearchRecordType);
        return;
    }


    /**
     *  Removes support for a fiscal period search record type.
     *
     *  @param fiscalPeriodSearchRecordType a fiscal period search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>fiscalPeriodSearchRecordType</code> is <code>null</code>
     */

    protected void removeFiscalPeriodSearchRecordType(org.osid.type.Type fiscalPeriodSearchRecordType) {
        this.fiscalPeriodSearchRecordTypes.remove(fiscalPeriodSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Summary </code> record types. 
     *
     *  @return a list containing the supported <code> Summary </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSummaryRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.summaryRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Summary </code> record type is supported. 
     *
     *  @param  summaryRecordType a <code> Type </code> indicating a <code> 
     *          Summary </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> summaryRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSummaryRecordType(org.osid.type.Type summaryRecordType) {
        return (this.summaryRecordTypes.contains(summaryRecordType));
    }


    /**
     *  Adds support for a summary record type.
     *
     *  @param summaryRecordType a summary record type
     *  @throws org.osid.NullArgumentException
     *  <code>summaryRecordType</code> is <code>null</code>
     */

    protected void addSummaryRecordType(org.osid.type.Type summaryRecordType) {
        this.summaryRecordTypes.add(summaryRecordType);
        return;
    }


    /**
     *  Removes support for a summary record type.
     *
     *  @param summaryRecordType a summary record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>summaryRecordType</code> is <code>null</code>
     */

    protected void removeSummaryRecordType(org.osid.type.Type summaryRecordType) {
        this.summaryRecordTypes.remove(summaryRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Business </code> record types. 
     *
     *  @return a list containing the supported <code> Business </code> types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBusinessRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.businessRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Business </code> record type is supported. 
     *
     *  @param  businessRecordType a <code> Type </code> indicating an <code> 
     *          Business </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> businessRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBusinessRecordType(org.osid.type.Type businessRecordType) {
        return (this.businessRecordTypes.contains(businessRecordType));
    }


    /**
     *  Adds support for a business record type.
     *
     *  @param businessRecordType a business record type
     *  @throws org.osid.NullArgumentException
     *  <code>businessRecordType</code> is <code>null</code>
     */

    protected void addBusinessRecordType(org.osid.type.Type businessRecordType) {
        this.businessRecordTypes.add(businessRecordType);
        return;
    }


    /**
     *  Removes support for a business record type.
     *
     *  @param businessRecordType a business record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>businessRecordType</code> is <code>null</code>
     */

    protected void removeBusinessRecordType(org.osid.type.Type businessRecordType) {
        this.businessRecordTypes.remove(businessRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Business </code> search record types. 
     *
     *  @return a list containing the supported <code> Business </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBusinessSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.businessSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Business </code> search record type is 
     *  supported. 
     *
     *  @param  businessSearchRecordType a <code> Type </code> indicating an 
     *          <code> Business </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> businessSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBusinessSearchRecordType(org.osid.type.Type businessSearchRecordType) {
        return (this.businessSearchRecordTypes.contains(businessSearchRecordType));
    }


    /**
     *  Adds support for a business search record type.
     *
     *  @param businessSearchRecordType a business search record type
     *  @throws org.osid.NullArgumentException
     *  <code>businessSearchRecordType</code> is <code>null</code>
     */

    protected void addBusinessSearchRecordType(org.osid.type.Type businessSearchRecordType) {
        this.businessSearchRecordTypes.add(businessSearchRecordType);
        return;
    }


    /**
     *  Removes support for a business search record type.
     *
     *  @param businessSearchRecordType a business search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>businessSearchRecordType</code> is <code>null</code>
     */

    protected void removeBusinessSearchRecordType(org.osid.type.Type businessSearchRecordType) {
        this.businessSearchRecordTypes.remove(businessSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the reporting 
     *  service. 
     *
     *  @return a <code> ReportingSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsReporting() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ReportingSession getReportingSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getReportingSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the reporting 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> ReportingSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsReporting() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ReportingSession getReportingSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getReportingSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the reporting 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the business 
     *  @return a <code> ReportingSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsReporting() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ReportingSession getReportingSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getReportingSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the reporting 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the business 
     *  @param  proxy proxy 
     *  @return a <code> ReportingSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsReporting() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ReportingSession getReportingSessionForBusiness(org.osid.id.Id businessId, 
                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getReportingSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the account lookup 
     *  service. 
     *
     *  @return an <code> AccountSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAccountLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountLookupSession getAccountLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getAccountLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the account lookup 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> AccountSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAccountLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountLookupSession getAccountLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getAccountLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the account lookup 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the business 
     *  @return an <code> AccountLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAccountLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountLookupSession getAccountLookupSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getAccountLookupSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the account lookup 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the business 
     *  @param  proxy proxy 
     *  @return an <code> AccountLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAccountLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountLookupSession getAccountLookupSessionForBusiness(org.osid.id.Id businessId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getAccountLookupSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the account query 
     *  service. 
     *
     *  @return an <code> AccountQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAccountQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountQuerySession getAccountQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getAccountQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the account query 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> AccountQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAccountQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountQuerySession getAccountQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getAccountQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the account query 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return an <code> AccountQuerySession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAccountQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountQuerySession getAccountQuerySessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getAccountQuerySessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the account query 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return an <code> AccountQuerySession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAccountQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountQuerySession getAccountQuerySessionForBusiness(org.osid.id.Id businessId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getAccountQuerySessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the account search 
     *  service. 
     *
     *  @return an <code> AccountSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAccountSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountSearchSession getAccountSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getAccountSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the account search 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> AccountSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAccountSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountSearchSession getAccountSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getAccountSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the account search 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return an <code> AccountSearchSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAccountSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountSearchSession getAccountSearchSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getAccountSearchSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the account search 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return an <code> AccountSearchSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAccountSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountSearchSession getAccountSearchSessionForBusiness(org.osid.id.Id businessId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getAccountSearchSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the account 
     *  administration service. 
     *
     *  @return an <code> AccountAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAccountAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountAdminSession getAccountAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getAccountAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the account 
     *  administration service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> AccountAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAccountAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountAdminSession getAccountAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getAccountAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the account 
     *  administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return an <code> AccountAdminSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAccountAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountAdminSession getAccountAdminSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getAccountAdminSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the account 
     *  administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return an <code> AccountAdminSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAccountAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountAdminSession getAccountAdminSessionForBusiness(org.osid.id.Id businessId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getAccountAdminSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the account 
     *  notification service. 
     *
     *  @param  accountReceiver the notification callback 
     *  @return an <code> AccountNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> accountReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAccountNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountNotificationSession getAccountNotificationSession(org.osid.financials.AccountReceiver accountReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getAccountNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the account 
     *  notification service. 
     *
     *  @param  accountReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return an <code> AccountNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> accountReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAccountNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountNotificationSession getAccountNotificationSession(org.osid.financials.AccountReceiver accountReceiver, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getAccountNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the account 
     *  notification service for the given business. 
     *
     *  @param  accountReceiver the notification callback 
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return an <code> AccountNotificationSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> accountReceiver </code> 
     *          or <code> businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAccountNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountNotificationSession getAccountNotificationSessionForBusiness(org.osid.financials.AccountReceiver accountReceiver, 
                                                                                                   org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getAccountNotificationSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the account 
     *  notification service for the given business. 
     *
     *  @param  accountReceiver the notification callback 
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return an <code> AccountNotificationSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> accountReceiver, 
     *          businessId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAccountNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountNotificationSession getAccountNotificationSessionForBusiness(org.osid.financials.AccountReceiver accountReceiver, 
                                                                                                   org.osid.id.Id businessId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getAccountNotificationSessionForBusiness not implemented");
    }


    /**
     *  Gets the session traversing account hierarchies. 
     *
     *  @return an <code> AccountHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAccountHierarchy() is false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountHierarchySession getAccountHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getAccountHierarchySession not implemented");
    }


    /**
     *  Gets the session traversing account hierarchies. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AccountHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAccountHierarchy() is false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountHierarchySession getAccountHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getAccountHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the account 
     *  heirarchy traversal service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the business 
     *  @return an <code> AccountHierarchySession </code> 
     *  @throws org.osid.NotFoundException <code> businessId </code> not found 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAccountHierarchy() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountHierarchySession getAccountHierarchySessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getAccountHierarchySessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the account 
     *  heirarchy traversal service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the business 
     *  @param  proxy a proxy 
     *  @return an <code> AccountHierarchySession </code> 
     *  @throws org.osid.NotFoundException <code> businessId </code> not found 
     *  @throws org.osid.NullArgumentException <code> businessId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAccountHierarchy() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountHierarchySession getAccountHierarchySessionForBusiness(org.osid.id.Id businessId, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getAccountHierarchySessionForBusiness not implemented");
    }


    /**
     *  Gets the session designing account hierarchies. 
     *
     *  @return an <code> AccountHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAccountHierarchyDesign() is false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountHierarchyDesignSession getAccountHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getAccountHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the session designing account hierarchies. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AccountHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAccountHierarchyDesign() is false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountHierarchyDesignSession getAccountHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getAccountHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the account 
     *  heirarchy design service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the business 
     *  @return an <code> AccountHierarchyDesignSession </code> 
     *  @throws org.osid.NotFoundException <code> businessId </code> not found 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAccountHierarchyDesign() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountHierarchyDesignSession getAccountHierarchyDesignSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getAccountHierarchyDesignSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the account 
     *  heirarchy design service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the business 
     *  @param  proxy a proxy 
     *  @return an <code> AccountHierarchyDesignSession </code> 
     *  @throws org.osid.NotFoundException <code> businessId </code> not found 
     *  @throws org.osid.NullArgumentException <code> businessId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAccountHierarchyDesign() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountHierarchyDesignSession getAccountHierarchyDesignSessionForBusiness(org.osid.id.Id businessId, 
                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getAccountHierarchyDesignSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup account/catalog 
     *  mappings. 
     *
     *  @return an <code> AccountBusinessSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAccountBusiness() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountBusinessSession getAccountBusinessSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getAccountBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup account/catalog 
     *  mappings. 
     *
     *  @param  proxy proxy 
     *  @return an <code> AccountCatalogSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAccountCatalog() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountBusinessSession getAccountBusinessSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getAccountBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning accounts 
     *  to businesses. 
     *
     *  @return an <code> AccountBusinessAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAccountBusinessAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountBusinessAssignmentSession getAccountBusinessAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getAccountBusinessAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning accounts 
     *  to businesses. 
     *
     *  @param  proxy proxy 
     *  @return an <code> AccountCatalogAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAccountCatalogAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountBusinessAssignmentSession getAccountBusinessAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getAccountBusinessAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the account smart 
     *  business service. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return an <code> AccountSmartBusinessSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAccountSmartBusiness() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountSmartBusinessSession getAccountSmartBusinessSession(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getAccountSmartBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the account smart 
     *  business service. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return an <code> AccountSmartBusinessSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAccountSmartBusiness() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountSmartBusinessSession getAccountSmartBusinessSession(org.osid.id.Id businessId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getAccountSmartBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  lookup service. 
     *
     *  @return an <code> ActivityLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivityLookupSession getActivityLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getActivityLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  lookup service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> ActivityLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivityLookupSession getActivityLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getActivityLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  lookup service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return an <code> ActivityLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivityLookupSession getActivityLookupSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getActivityLookupSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  lookup service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return an <code> ActivityLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivityLookupSession getActivityLookupSessionForBusiness(org.osid.id.Id businessId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getActivityLookupSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity query 
     *  service. 
     *
     *  @return an <code> ActivityQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsActivityQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivityQuerySession getActivityQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getActivityQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity query 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> ActivityQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsActivityQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivityQuerySession getActivityQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getActivityQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity query 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return an <code> ActivityQuerySession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsActivityQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivityQuerySession getActivityQuerySessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getActivityQuerySessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity query 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return an <code> ActivityQuerySession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsActivityQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivityQuerySession getActivityQuerySessionForBusiness(org.osid.id.Id businessId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getActivityQuerySessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  search service. 
     *
     *  @return an <code> ActivitySearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivitySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivitySearchSession getActivitySearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getActivitySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  search service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> ActivitySearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivitySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivitySearchSession getActivitySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getActivitySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  search service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return an <code> ActivitySearchSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivitySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivitySearchSession getActivitySearchSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getActivitySearchSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  search service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return an <code> ActivitySearchSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivitySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivitySearchSession getActivitySearchSessionForBusiness(org.osid.id.Id businessId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getActivitySearchSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  administration service. 
     *
     *  @return an <code> ActivityAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsActivityAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivityAdminSession getActivityAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getActivityAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  administration service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> ActivityAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsActivityAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivityAdminSession getActivityAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getActivityAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return an <code> ActivityAdminSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsActivityAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivityAdminSession getActivityAdminSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getActivityAdminSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return an <code> ActivityAdminSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsActivityAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivityAdminSession getActivityAdminSessionForBusiness(org.osid.id.Id businessId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getActivityAdminSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  notification service. 
     *
     *  @param  activityReceiver the notification callback 
     *  @return an <code> ActivityNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> activityReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivityNotificationSession getActivityNotificationSession(org.osid.financials.ActivityReceiver activityReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getActivityNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  notification service. 
     *
     *  @param  activityReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return an <code> ActivityNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> activityReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivityNotificationSession getActivityNotificationSession(org.osid.financials.ActivityReceiver activityReceiver, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getActivityNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  notification service for the given business. 
     *
     *  @param  activityReceiver the notification callback 
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return an <code> ActivityNotificationSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> activityReceiver </code> 
     *          or <code> businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivityNotificationSession getActivityNotificationSessionForBusiness(org.osid.financials.ActivityReceiver activityReceiver, 
                                                                                                     org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getActivityNotificationSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  notification service for the given business. 
     *
     *  @param  activityReceiver the notification callback 
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return an <code> ActivityNotificationSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> activityReceiver, 
     *          businessId, </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivityNotificationSession getActivityNotificationSessionForBusiness(org.osid.financials.ActivityReceiver activityReceiver, 
                                                                                                     org.osid.id.Id businessId, 
                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getActivityNotificationSessionForBusiness not implemented");
    }


    /**
     *  Gets the session traversing activity hierarchies. 
     *
     *  @return an <code> ActivityHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityHierarchy() is false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivityHierarchySession getActivityHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getActivityHierarchySession not implemented");
    }


    /**
     *  Gets the session traversing activity hierarchies. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ActivityHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityHierarchy() is false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivityHierarchySession getActivityHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getActivityHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  heirarchy traversal service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the business 
     *  @return an <code> ActivityHierarchySession </code> 
     *  @throws org.osid.NotFoundException <code> businessId </code> not found 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityHierarchy() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivityHierarchySession getActivityHierarchySessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getActivityHierarchySessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  heirarchy traversal service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the business 
     *  @param  proxy a proxy 
     *  @return an <code> ActivityHierarchySession </code> 
     *  @throws org.osid.NotFoundException <code> businessId </code> not found 
     *  @throws org.osid.NullArgumentException <code> businessId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityHierarchy() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivityHierarchySession getActivityHierarchySessionForBusiness(org.osid.id.Id businessId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getActivityHierarchySessionForBusiness not implemented");
    }


    /**
     *  Gets the session designing activity hierarchies. 
     *
     *  @return an <code> ActivityHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityHierarchyDesign() is false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivityHierarchyDesignSession getActivityHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getActivityHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the session designing activity hierarchies. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ActivityHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityHierarchyDesign() is false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivityHierarchyDesignSession getActivityHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getActivityHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  heirarchy design service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the business 
     *  @return an <code> ActivityHierarchyDesignSession </code> 
     *  @throws org.osid.NotFoundException <code> businessId </code> not found 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityHierarchyDesign() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivityHierarchyDesignSession getActivityHierarchyDesignSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getActivityHierarchyDesignSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  heirarchy design service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the business 
     *  @param  proxy a proxy 
     *  @return a <code> ActivityHierarchyDesignSession </code> 
     *  @throws org.osid.NotFoundException <code> businessId </code> not found 
     *  @throws org.osid.NullArgumentException <code> businessId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityHierarchyDesign() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivityHierarchyDesignSession getActivityHierarchyDesignSessionForBusiness(org.osid.id.Id businessId, 
                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getActivityHierarchyDesignSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup activity/catalog 
     *  mappings. 
     *
     *  @return an <code> ActivityBusinessSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBusiness() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivityBusinessSession getActivityBusinessSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getActivityBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup activity/catalog 
     *  mappings. 
     *
     *  @param  proxy proxy 
     *  @return an <code> ActivityCatalogSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityCatalog() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivityBusinessSession getActivityBusinessSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getActivityBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  activities to businesses. 
     *
     *  @return an <code> ActivityBusinessAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBusinessAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivityBusinessAssignmentSession getActivityBusinessAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getActivityBusinessAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  activities to businesses. 
     *
     *  @param  proxy proxy 
     *  @return an <code> ActivityCatalogAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityCatalogAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivityBusinessAssignmentSession getActivityBusinessAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getActivityBusinessAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity smart 
     *  business service. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return an <code> ActivitySmartBusinessSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivitySmartBusiness() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivitySmartBusinessSession getActivitySmartBusinessSession(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getActivitySmartBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity smart 
     *  business service. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return an <code> ActivitySmartBusinessSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivitySmartBusiness() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivitySmartBusinessSession getActivitySmartBusinessSession(org.osid.id.Id businessId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getActivitySmartBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the fiscal period 
     *  lookup service. 
     *
     *  @return a <code> FiscalPeriodSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodLookupSession getFiscalPeriodLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getFiscalPeriodLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the fiscal period 
     *  lookup service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> FiscalPeriodSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodLookupSession getFiscalPeriodLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getFiscalPeriodLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the fiscal period 
     *  lookup service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the business 
     *  @return a <code> FiscalPeriodLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodLookupSession getFiscalPeriodLookupSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getFiscalPeriodLookupSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the fiscal period 
     *  lookup service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the business 
     *  @param  proxy proxy 
     *  @return a <code> FiscalPeriodLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodLookupSession getFiscalPeriodLookupSessionForBusiness(org.osid.id.Id businessId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getFiscalPeriodLookupSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the fiscal period 
     *  query service. 
     *
     *  @return a <code> FiscalPeriodQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodQuerySession getFiscalPeriodQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getFiscalPeriodQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the fiscal period 
     *  query service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> FiscalPeriodQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodQuerySession getFiscalPeriodQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getFiscalPeriodQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the fiscal period 
     *  query service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> FiscalPeriodQuerySession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodQuerySession getFiscalPeriodQuerySessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getFiscalPeriodQuerySessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the fiscal period 
     *  query service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> FiscalPeriodQuerySession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodQuerySession getFiscalPeriodQuerySessionForBusiness(org.osid.id.Id businessId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getFiscalPeriodQuerySessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the fiscal period 
     *  search service. 
     *
     *  @return a <code> FiscalPeriodSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodSearchSession getFiscalPeriodSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getFiscalPeriodSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the fiscal period 
     *  search service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> FiscalPeriodSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodSearchSession getFiscalPeriodSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getFiscalPeriodSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the fiscal period 
     *  search service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> FiscalPeriodSearchSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodSearchSession getFiscalPeriodSearchSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getFiscalPeriodSearchSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the fiscal period 
     *  search service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> FiscalPeriodSearchSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodSearchSession getFiscalPeriodSearchSessionForBusiness(org.osid.id.Id businessId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getFiscalPeriodSearchSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the fiscal period 
     *  administration service. 
     *
     *  @return a <code> FiscalPeriodAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodAdminSession getFiscalPeriodAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getFiscalPeriodAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the fiscal period 
     *  administration service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> FiscalPeriodAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodAdminSession getFiscalPeriodAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getFiscalPeriodAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the fiscal period 
     *  administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> FiscalPeriodAdminSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodAdminSession getFiscalPeriodAdminSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getFiscalPeriodAdminSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the fiscal period 
     *  administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> FiscalPeriodAdminSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodAdminSession getFiscalPeriodAdminSessionForBusiness(org.osid.id.Id businessId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getFiscalPeriodAdminSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the fiscal period 
     *  notification service. 
     *
     *  @param  fiscalPeriodReceiver the notification callback 
     *  @return a <code> FiscalPeriodNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> fiscalPeriodReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodNotificationSession getFiscalPeriodNotificationSession(org.osid.financials.FiscalPeriodReceiver fiscalPeriodReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getFiscalPeriodNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the fiscal period 
     *  notification service. 
     *
     *  @param  fiscalPeriodReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return a <code> FiscalPeriodNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> fiscalPeriodReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodNotificationSession getFiscalPeriodNotificationSession(org.osid.financials.FiscalPeriodReceiver fiscalPeriodReceiver, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getFiscalPeriodNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the fiscal period 
     *  notification service for the given business. 
     *
     *  @param  fiscalPeriodReceiver the notification callback 
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> FiscalPeriodNotificationSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> fiscalPeriodReceiver 
     *          </code> or <code> businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodNotificationSession getFiscalPeriodNotificationSessionForBusiness(org.osid.financials.FiscalPeriodReceiver fiscalPeriodReceiver, 
                                                                                                             org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getFiscalPeriodNotificationSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the fiscal period 
     *  notification service for the given business. 
     *
     *  @param  fiscalPeriodReceiver the notification callback 
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> FiscalPeriodNotificationSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> fiscalPeriodReceiver, 
     *          businessId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodNotificationSession getFiscalPeriodNotificationSessionForBusiness(org.osid.financials.FiscalPeriodReceiver fiscalPeriodReceiver, 
                                                                                                             org.osid.id.Id businessId, 
                                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getFiscalPeriodNotificationSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup fiscal period/catalog 
     *  mappings. 
     *
     *  @return a <code> FiscalPeriodBusinessSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodBusiness() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodBusinessSession getFiscalPeriodBusinessSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getFiscalPeriodBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup fiscal period/catalog 
     *  mappings. 
     *
     *  @param  proxy proxy 
     *  @return a <code> FiscalPeriodBusinessSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodBusiness() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodBusinessSession getFiscalPeriodBusinessSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getFiscalPeriodBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning fiscal 
     *  periods to businesses. 
     *
     *  @return a <code> FiscalPeriodBusinessAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodBusinessAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodBusinessAssignmentSession getFiscalPeriodBusinessAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getFiscalPeriodBusinessAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning fiscal 
     *  periods to businesses. 
     *
     *  @param  proxy proxy 
     *  @return a <code> FiscalPeriodBusinessAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodBusinessAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodBusinessAssignmentSession getFiscalPeriodBusinessAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getFiscalPeriodBusinessAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the fiscal period 
     *  smart business service. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> FiscalPeriodSmartBusinessSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodSmartBusiness() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodSmartBusinessSession getFiscalPeriodSmartBusinessSession(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getFiscalPeriodSmartBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the fiscal period 
     *  smart business service. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> FiscalPeriodSmartBusinessSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodSmartBusiness() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodSmartBusinessSession getFiscalPeriodSmartBusinessSession(org.osid.id.Id businessId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getFiscalPeriodSmartBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the business 
     *  lookup service. 
     *
     *  @return a <code> BusinessLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBusinessLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.BusinessLookupSession getBusinessLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getBusinessLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the business 
     *  lookup service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> BusinessLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBusinessLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.BusinessLookupSession getBusinessLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getBusinessLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the business query 
     *  service. 
     *
     *  @return a <code> BusinessQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBusinessQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.BusinessQuerySession getBusinessQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getBusinessQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the business query 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> BusinessQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBusinessQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.BusinessQuerySession getBusinessQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getBusinessQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the business 
     *  search service. 
     *
     *  @return a <code> BusinessSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBusinessSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.BusinessSearchSession getBusinessSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getBusinessSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the business 
     *  search service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> BusinessSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBusinessSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.BusinessSearchSession getBusinessSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getBusinessSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the business 
     *  administrative service. 
     *
     *  @return a <code> BusinessAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBusinessAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.BusinessAdminSession getBusinessAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getBusinessAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the business 
     *  administrative service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> BusinessAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBusinessAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.BusinessAdminSession getBusinessAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getBusinessAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the business 
     *  notification service. 
     *
     *  @param  businessReceiver the notification callback 
     *  @return a <code> BusinessNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> businessReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBusinessNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.BusinessNotificationSession getBusinessNotificationSession(org.osid.financials.BusinessReceiver businessReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getBusinessNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the business 
     *  notification service. 
     *
     *  @param  businessReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return a <code> BusinessNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> businessReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBusinessNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.BusinessNotificationSession getBusinessNotificationSession(org.osid.financials.BusinessReceiver businessReceiver, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getBusinessNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the business 
     *  hierarchy service. 
     *
     *  @return a <code> BusinessHierarchySession </code> for businesses 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBusinessHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.BusinessHierarchySession getBusinessHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getBusinessHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the business 
     *  hierarchy service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> BusinessHierarchySession </code> for businesses 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBusinessHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.BusinessHierarchySession getBusinessHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getBusinessHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the business 
     *  hierarchy design service. 
     *
     *  @return a <code> HierarchyDesignSession </code> for businesses 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBusinessHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.BusinessHierarchyDesignSession getBusinessHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getBusinessHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the business 
     *  hierarchy design service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> HierarchyDesignSession </code> for businesses 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBusinessHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.BusinessHierarchyDesignSession getBusinessHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getBusinessHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> FinancialsBatchManager. </code> 
     *
     *  @return a <code> FinancialsBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFinancialsBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.batch.FinancialsBatchManager getFinancialsBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getFinancialsBatchManager not implemented");
    }


    /**
     *  Gets the <code> FinancialsBatchProxyManager. </code> 
     *
     *  @return a <code> FinancialsBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFinancialsBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.batch.FinancialsBatchProxyManager getFinancialsBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getFinancialsBatchProxyManager not implemented");
    }


    /**
     *  Gets the <code> FinancialsBudgetingManager. </code> 
     *
     *  @return a <code> FinancialsBudgetingManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFinancialsBudgeting() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.FinancialsBudgetingManager getFinancialsBudgetingManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getFinancialsBudgetingManager not implemented");
    }


    /**
     *  Gets the <code> FinancialsBudgetingProxyManager. </code> 
     *
     *  @return a <code> FinancialsBudgetingProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFinancialsBudgeting() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.FinancialsBudgetingProxyManager getFinancialsBudgetingProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getFinancialsBudgetingProxyManager not implemented");
    }


    /**
     *  Gets the <code> FinancialsPostingManager. </code> 
     *
     *  @return a <code> FinancialsPostingManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFinancialsPosting() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.FinancialsPostingManager getFinancialsPostingManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsManager.getFinancialsPostingManager not implemented");
    }


    /**
     *  Gets the <code> FinancialsPostingProxyManager. </code> 
     *
     *  @return a <code> FinancialsPostingProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFinancialsPosting() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.FinancialsPostingProxyManager getFinancialsPostingProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.FinancialsProxyManager.getFinancialsPostingProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.accountRecordTypes.clear();
        this.accountRecordTypes.clear();

        this.accountSearchRecordTypes.clear();
        this.accountSearchRecordTypes.clear();

        this.activityRecordTypes.clear();
        this.activityRecordTypes.clear();

        this.activitySearchRecordTypes.clear();
        this.activitySearchRecordTypes.clear();

        this.fiscalPeriodRecordTypes.clear();
        this.fiscalPeriodRecordTypes.clear();

        this.fiscalPeriodSearchRecordTypes.clear();
        this.fiscalPeriodSearchRecordTypes.clear();

        this.summaryRecordTypes.clear();
        this.summaryRecordTypes.clear();

        this.businessRecordTypes.clear();
        this.businessRecordTypes.clear();

        this.businessSearchRecordTypes.clear();
        this.businessSearchRecordTypes.clear();

        return;
    }
}
