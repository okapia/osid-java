//
// AbstractProcedureQueryInspector.java
//
//     A template for making a ProcedureQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recipe.procedure.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for procedures.
 */

public abstract class AbstractProcedureQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.recipe.ProcedureQueryInspector {

    private final java.util.Collection<org.osid.recipe.records.ProcedureQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the objective <code> Id </code> terms. 
     *
     *  @return the objective <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getLearningObjectiveIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the objective terms. 
     *
     *  @return the objective terms 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQueryInspector[] getLearningObjectiveTerms() {
        return (new org.osid.learning.ObjectiveQueryInspector[0]);
    }


    /**
     *  Gets the cook book <code> Id </code> terms. 
     *
     *  @return the cook book <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCookbookIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the cook book terms. 
     *
     *  @return the cook book terms 
     */

    @OSID @Override
    public org.osid.recipe.CookbookQueryInspector[] getCookbookTerms() {
        return (new org.osid.recipe.CookbookQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given procedure query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a procedure implementing the requested record.
     *
     *  @param procedureRecordType a procedure record type
     *  @return the procedure query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>procedureRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(procedureRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.recipe.records.ProcedureQueryInspectorRecord getProcedureQueryInspectorRecord(org.osid.type.Type procedureRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.recipe.records.ProcedureQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(procedureRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(procedureRecordType + " is not supported");
    }


    /**
     *  Adds a record to this procedure query. 
     *
     *  @param procedureQueryInspectorRecord procedure query inspector
     *         record
     *  @param procedureRecordType procedure record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addProcedureQueryInspectorRecord(org.osid.recipe.records.ProcedureQueryInspectorRecord procedureQueryInspectorRecord, 
                                                   org.osid.type.Type procedureRecordType) {

        addRecordType(procedureRecordType);
        nullarg(procedureRecordType, "procedure record type");
        this.records.add(procedureQueryInspectorRecord);        
        return;
    }
}
