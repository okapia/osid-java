//
// AssessmentMiter.java
//
//     Defines an Assessment miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.assessment.assessment;


/**
 *  Defines an <code>Assessment</code> miter for use with the builders.
 */

public interface AssessmentMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidObjectMiter,
            org.osid.assessment.Assessment {


    /**
     *  Sets the level.
     *
     *  @param level a level
     *  @throws org.osid.NullArgumentException
     *          <code>level</code> is <code>null</code>
     */

    public void setLevel(org.osid.grading.Grade level);


    /**
     *  Sets the rubric.
     *
     *  @param assessment the rubric assessment
     *  @throws org.osid.NullArgumentException
     *          <code>assessment</code> is <code>null</code>
     */

    public void setRubric(org.osid.assessment.Assessment assessment);


    /**
     *  Adds an Assessment record.
     *
     *  @param record an assessment record
     *  @param recordType the type of assessment record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addAssessmentRecord(org.osid.assessment.records.AssessmentRecord record, org.osid.type.Type recordType);
}       


