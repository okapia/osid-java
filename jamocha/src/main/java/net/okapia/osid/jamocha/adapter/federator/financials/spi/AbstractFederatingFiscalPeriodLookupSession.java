//
// AbstractFederatingFiscalPeriodLookupSession.java
//
//     An abstract federating adapter for a FiscalPeriodLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.financials.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  FiscalPeriodLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingFiscalPeriodLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.financials.FiscalPeriodLookupSession>
    implements org.osid.financials.FiscalPeriodLookupSession {

    private boolean parallel = false;
    private org.osid.financials.Business business = new net.okapia.osid.jamocha.nil.financials.business.UnknownBusiness();


    /**
     *  Constructs a new <code>AbstractFederatingFiscalPeriodLookupSession</code>.
     */

    protected AbstractFederatingFiscalPeriodLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.financials.FiscalPeriodLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Business/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Business Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBusinessId() {
        return (this.business.getId());
    }


    /**
     *  Gets the <code>Business</code> associated with this 
     *  session.
     *
     *  @return the <code>Business</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.Business getBusiness()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.business);
    }


    /**
     *  Sets the <code>Business</code>.
     *
     *  @param  business the business for this session
     *  @throws org.osid.NullArgumentException <code>business</code>
     *          is <code>null</code>
     */

    protected void setBusiness(org.osid.financials.Business business) {
        nullarg(business, "business");
        this.business = business;
        return;
    }


    /**
     *  Tests if this user can perform <code>FiscalPeriod</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupFiscalPeriods() {
        for (org.osid.financials.FiscalPeriodLookupSession session : getSessions()) {
            if (session.canLookupFiscalPeriods()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>FiscalPeriod</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeFiscalPeriodView() {
        for (org.osid.financials.FiscalPeriodLookupSession session : getSessions()) {
            session.useComparativeFiscalPeriodView();
        }

        return;
    }


    /**
     *  A complete view of the <code>FiscalPeriod</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryFiscalPeriodView() {
        for (org.osid.financials.FiscalPeriodLookupSession session : getSessions()) {
            session.usePlenaryFiscalPeriodView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include fiscal periods in businesses which are children
     *  of this business in the business hierarchy.
     */

    @OSID @Override
    public void useFederatedBusinessView() {
        for (org.osid.financials.FiscalPeriodLookupSession session : getSessions()) {
            session.useFederatedBusinessView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this business only.
     */

    @OSID @Override
    public void useIsolatedBusinessView() {
        for (org.osid.financials.FiscalPeriodLookupSession session : getSessions()) {
            session.useIsolatedBusinessView();
        }

        return;
    }

     
    /**
     *  Gets the <code>FiscalPeriod</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>FiscalPeriod</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>FiscalPeriod</code> and
     *  retained for compatibility.
     *
     *  @param  fiscalPeriodId <code>Id</code> of the
     *          <code>FiscalPeriod</code>
     *  @return the fiscal period
     *  @throws org.osid.NotFoundException <code>fiscalPeriodId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>fiscalPeriodId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriod getFiscalPeriod(org.osid.id.Id fiscalPeriodId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.financials.FiscalPeriodLookupSession session : getSessions()) {
            try {
                return (session.getFiscalPeriod(fiscalPeriodId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(fiscalPeriodId + " not found");
    }


    /**
     *  Gets a <code>FiscalPeriodList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  fiscalPeriods specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>FiscalPeriods</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  fiscalPeriodIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>FiscalPeriod</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>fiscalPeriodIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodList getFiscalPeriodsByIds(org.osid.id.IdList fiscalPeriodIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.financials.fiscalperiod.MutableFiscalPeriodList ret = new net.okapia.osid.jamocha.financials.fiscalperiod.MutableFiscalPeriodList();

        try (org.osid.id.IdList ids = fiscalPeriodIds) {
            while (ids.hasNext()) {
                ret.addFiscalPeriod(getFiscalPeriod(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>FiscalPeriodList</code> corresponding to the given
     *  fiscal period genus <code>Type</code> which does not include
     *  fiscal periods of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  fiscal periods or an error results. Otherwise, the returned list
     *  may contain only those fiscal periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  fiscalPeriodGenusType a fiscalPeriod genus type 
     *  @return the returned <code>FiscalPeriod</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>fiscalPeriodGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodList getFiscalPeriodsByGenusType(org.osid.type.Type fiscalPeriodGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.financials.fiscalperiod.FederatingFiscalPeriodList ret = getFiscalPeriodList();

        for (org.osid.financials.FiscalPeriodLookupSession session : getSessions()) {
            ret.addFiscalPeriodList(session.getFiscalPeriodsByGenusType(fiscalPeriodGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>FiscalPeriodList</code> corresponding to the given
     *  fiscal period genus <code>Type</code> and include any additional
     *  fiscal periods with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  fiscal periods or an error results. Otherwise, the returned list
     *  may contain only those fiscal periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  fiscalPeriodGenusType a fiscalPeriod genus type 
     *  @return the returned <code>FiscalPeriod</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>fiscalPeriodGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodList getFiscalPeriodsByParentGenusType(org.osid.type.Type fiscalPeriodGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.financials.fiscalperiod.FederatingFiscalPeriodList ret = getFiscalPeriodList();

        for (org.osid.financials.FiscalPeriodLookupSession session : getSessions()) {
            ret.addFiscalPeriodList(session.getFiscalPeriodsByParentGenusType(fiscalPeriodGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>FiscalPeriodList</code> containing the given
     *  fiscal period record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  fiscal periods or an error results. Otherwise, the returned list
     *  may contain only those fiscal periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  fiscalPeriodRecordType a fiscalPeriod record type 
     *  @return the returned <code>FiscalPeriod</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>fiscalPeriodRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodList getFiscalPeriodsByRecordType(org.osid.type.Type fiscalPeriodRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.financials.fiscalperiod.FederatingFiscalPeriodList ret = getFiscalPeriodList();

        for (org.osid.financials.FiscalPeriodLookupSession session : getSessions()) {
            ret.addFiscalPeriodList(session.getFiscalPeriodsByRecordType(fiscalPeriodRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code> FiscalPeriodList </code> containing the given
     *  date. In plenary mode, the returned list contains all known
     *  fiscal periods or an error results. Otherwise, the returned
     *  list may contain only those fiscal periods that are accessible
     *  through this session.
     *
     *  @param  date a date
     *  @return the returned <code> FiscalPeriod </code> list
     *  @throws org.osid.NullArgumentException <code> date </code> is <code>
     *          null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodList getFiscalPeriodsByDate(org.osid.calendaring.DateTime date)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.financials.fiscalperiod.FederatingFiscalPeriodList ret = getFiscalPeriodList();

        for (org.osid.financials.FiscalPeriodLookupSession session : getSessions()) {
            ret.addFiscalPeriodList(session.getFiscalPeriodsByDate(date));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }

        
    /**
     *  Gets all <code>FiscalPeriods</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  fiscal periods or an error results. Otherwise, the returned list
     *  may contain only those fiscal periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>FiscalPeriods</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodList getFiscalPeriods()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.financials.fiscalperiod.FederatingFiscalPeriodList ret = getFiscalPeriodList();

        for (org.osid.financials.FiscalPeriodLookupSession session : getSessions()) {
            ret.addFiscalPeriodList(session.getFiscalPeriods());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.financials.fiscalperiod.FederatingFiscalPeriodList getFiscalPeriodList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.financials.fiscalperiod.ParallelFiscalPeriodList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.financials.fiscalperiod.CompositeFiscalPeriodList());
        }
    }
}
