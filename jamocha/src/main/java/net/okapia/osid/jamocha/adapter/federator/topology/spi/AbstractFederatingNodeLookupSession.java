//
// AbstractFederatingNodeLookupSession.java
//
//     An abstract federating adapter for a NodeLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.topology.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  NodeLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingNodeLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.topology.NodeLookupSession>
    implements org.osid.topology.NodeLookupSession {

    private boolean parallel = false;
    private org.osid.topology.Graph graph = new net.okapia.osid.jamocha.nil.topology.graph.UnknownGraph();


    /**
     *  Constructs a new <code>AbstractFederatingNodeLookupSession</code>.
     */

    protected AbstractFederatingNodeLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.topology.NodeLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Graph/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Graph Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getGraphId() {
        return (this.graph.getId());
    }


    /**
     *  Gets the <code>Graph</code> associated with this 
     *  session.
     *
     *  @return the <code>Graph</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.Graph getGraph()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.graph);
    }


    /**
     *  Sets the <code>Graph</code>.
     *
     *  @param  graph the graph for this session
     *  @throws org.osid.NullArgumentException <code>graph</code>
     *          is <code>null</code>
     */

    protected void setGraph(org.osid.topology.Graph graph) {
        nullarg(graph, "graph");
        this.graph = graph;
        return;
    }


    /**
     *  Tests if this user can perform <code>Node</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupNodes() {
        for (org.osid.topology.NodeLookupSession session : getSessions()) {
            if (session.canLookupNodes()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Node</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeNodeView() {
        for (org.osid.topology.NodeLookupSession session : getSessions()) {
            session.useComparativeNodeView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Node</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryNodeView() {
        for (org.osid.topology.NodeLookupSession session : getSessions()) {
            session.usePlenaryNodeView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include nodes in graphs which are children
     *  of this graph in the graph hierarchy.
     */

    @OSID @Override
    public void useFederatedGraphView() {
        for (org.osid.topology.NodeLookupSession session : getSessions()) {
            session.useFederatedGraphView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this graph only.
     */

    @OSID @Override
    public void useIsolatedGraphView() {
        for (org.osid.topology.NodeLookupSession session : getSessions()) {
            session.useIsolatedGraphView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Node</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Node</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Node</code> and
     *  retained for compatibility.
     *
     *  @param  nodeId <code>Id</code> of the
     *          <code>Node</code>
     *  @return the node
     *  @throws org.osid.NotFoundException <code>nodeId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>nodeId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.Node getNode(org.osid.id.Id nodeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.topology.NodeLookupSession session : getSessions()) {
            try {
                return (session.getNode(nodeId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(nodeId + " not found");
    }


    /**
     *  Gets a <code>NodeList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  nodes specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Nodes</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  nodeIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Node</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>nodeIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.NodeList getNodesByIds(org.osid.id.IdList nodeIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.topology.node.MutableNodeList ret = new net.okapia.osid.jamocha.topology.node.MutableNodeList();

        try (org.osid.id.IdList ids = nodeIds) {
            while (ids.hasNext()) {
                ret.addNode(getNode(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>NodeList</code> corresponding to the given
     *  node genus <code>Type</code> which does not include
     *  nodes of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  nodes or an error results. Otherwise, the returned list
     *  may contain only those nodes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  nodeGenusType a node genus type 
     *  @return the returned <code>Node</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>nodeGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.NodeList getNodesByGenusType(org.osid.type.Type nodeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.topology.node.FederatingNodeList ret = getNodeList();

        for (org.osid.topology.NodeLookupSession session : getSessions()) {
            ret.addNodeList(session.getNodesByGenusType(nodeGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>NodeList</code> corresponding to the given
     *  node genus <code>Type</code> and include any additional
     *  nodes with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  nodes or an error results. Otherwise, the returned list
     *  may contain only those nodes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  nodeGenusType a node genus type 
     *  @return the returned <code>Node</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>nodeGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.NodeList getNodesByParentGenusType(org.osid.type.Type nodeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.topology.node.FederatingNodeList ret = getNodeList();

        for (org.osid.topology.NodeLookupSession session : getSessions()) {
            ret.addNodeList(session.getNodesByParentGenusType(nodeGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>NodeList</code> containing the given
     *  node record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  nodes or an error results. Otherwise, the returned list
     *  may contain only those nodes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  nodeRecordType a node record type 
     *  @return the returned <code>Node</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>nodeRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.NodeList getNodesByRecordType(org.osid.type.Type nodeRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.topology.node.FederatingNodeList ret = getNodeList();

        for (org.osid.topology.NodeLookupSession session : getSessions()) {
            ret.addNodeList(session.getNodesByRecordType(nodeRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Nodes</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  nodes or an error results. Otherwise, the returned list
     *  may contain only those nodes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Nodes</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.NodeList getNodes()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.topology.node.FederatingNodeList ret = getNodeList();

        for (org.osid.topology.NodeLookupSession session : getSessions()) {
            ret.addNodeList(session.getNodes());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.topology.node.FederatingNodeList getNodeList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.topology.node.ParallelNodeList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.topology.node.CompositeNodeList());
        }
    }
}
