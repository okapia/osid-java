//
// AbstractImmutableProvisionable.java
//
//     Wraps a mutable Provisionable to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.provisioning.provisionable.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Provisionable</code> to hide modifiers. This
 *  wrapper provides an immutized Provisionable from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying provisionable whose state changes are visible.
 */

public abstract class AbstractImmutableProvisionable
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRelationship
    implements org.osid.provisioning.Provisionable {

    private final org.osid.provisioning.Provisionable provisionable;


    /**
     *  Constructs a new <code>AbstractImmutableProvisionable</code>.
     *
     *  @param provisionable the provisionable to immutablize
     *  @throws org.osid.NullArgumentException <code>provisionable</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableProvisionable(org.osid.provisioning.Provisionable provisionable) {
        super(provisionable);
        this.provisionable = provisionable;
        return;
    }


    /**
     *  Gets the pool <code> Id. </code> 
     *
     *  @return the pool <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getPoolId() {
        return (this.provisionable.getPoolId());
    }


    /**
     *  Gets the pool. 
     *
     *  @return the pool 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.provisioning.Pool getPool()
        throws org.osid.OperationFailedException {

        return (this.provisionable.getPool());
    }


    /**
     *  Gets the resource <code> Id. </code> 
     *
     *  @return the resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getResourceId() {
        return (this.provisionable.getResourceId());
    }


    /**
     *  Gets the resource. 
     *
     *  @return the resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getResource()
        throws org.osid.OperationFailedException {

        return (this.provisionable.getResource());
    }


    /**
     *  Gets the number of times this provisionable has been provisioned. 
     *
     *  @return the position 
     */

    @OSID @Override
    public long getUse() {
        return (this.provisionable.getUse());
    }


    /**
     *  Gets the provisionable record corresponding to the given <code> 
     *  Provisionable </code> record <code> Type. </code> This method is used 
     *  to retrieve an object implementing the requested record. The <code> 
     *  provisionableRecordType </code> may be the <code> Type </code> 
     *  returned in <code> getRecordTypes() </code> or any of its parents in a 
     *  <code> Type </code> hierarchy where <code> 
     *  hasRecordType(provisionableRecordType) </code> is <code> true </code> 
     *  . 
     *
     *  @param  provisionableRecordType the type of provisionable record to 
     *          retrieve 
     *  @return the provisionable record 
     *  @throws org.osid.NullArgumentException <code> provisionableRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(provisionableRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.records.ProvisionableRecord getProvisionableRecord(org.osid.type.Type provisionableRecordType)
        throws org.osid.OperationFailedException {

        return (this.provisionable.getProvisionableRecord(provisionableRecordType));
    }
}

