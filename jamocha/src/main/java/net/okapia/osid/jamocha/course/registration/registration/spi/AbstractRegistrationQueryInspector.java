//
// AbstractRegistrationQueryInspector.java
//
//     A template for making a RegistrationQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.registration.registration.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for registrations.
 */

public abstract class AbstractRegistrationQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQueryInspector
    implements org.osid.course.registration.RegistrationQueryInspector {

    private final java.util.Collection<org.osid.course.registration.records.RegistrationQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the activity bundle <code> Id </code> query terms. 
     *
     *  @return the activity bundle <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getActivityBundleIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the activity bundle query terms. 
     *
     *  @return the activity bundle query terms 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleQueryInspector[] getActivityBundleTerms() {
        return (new org.osid.course.registration.ActivityBundleQueryInspector[0]);
    }


    /**
     *  Gets the student <code> Id </code> query terms. 
     *
     *  @return the student <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getStudentIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the student query terms. 
     *
     *  @return the student query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getStudentTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the credits query terms. 
     *
     *  @return the credits query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalTerm[] getCreditsTerms() {
        return (new org.osid.search.terms.DecimalTerm[0]);
    }


    /**
     *  Gets the grade system <code> Id </code> query terms. 
     *
     *  @return the grade system <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGradingOptionIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the grade system query terms. 
     *
     *  @return the grade system terms 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQueryInspector[] getGradingOptionTerms() {
        return (new org.osid.grading.GradeSystemQueryInspector[0]);
    }


    /**
     *  Gets the course catalog <code> Id </code> query terms. 
     *
     *  @return the course catalog <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseCatalogIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the course catalog query terms. 
     *
     *  @return the course catalog query terms 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQueryInspector[] getCourseCatalogTerms() {
        return (new org.osid.course.CourseCatalogQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given registration query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a registration implementing the requested record.
     *
     *  @param registrationRecordType a registration record type
     *  @return the registration query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>registrationRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(registrationRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.registration.records.RegistrationQueryInspectorRecord getRegistrationQueryInspectorRecord(org.osid.type.Type registrationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.registration.records.RegistrationQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(registrationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(registrationRecordType + " is not supported");
    }


    /**
     *  Adds a record to this registration query. 
     *
     *  @param registrationQueryInspectorRecord registration query inspector
     *         record
     *  @param registrationRecordType registration record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRegistrationQueryInspectorRecord(org.osid.course.registration.records.RegistrationQueryInspectorRecord registrationQueryInspectorRecord, 
                                                   org.osid.type.Type registrationRecordType) {

        addRecordType(registrationRecordType);
        nullarg(registrationRecordType, "registration record type");
        this.records.add(registrationQueryInspectorRecord);        
        return;
    }
}
