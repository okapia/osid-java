//
// AbstractQueryResourceRelationshipLookupSession.java
//
//    An inline adapter that maps a ResourceRelationshipLookupSession to
//    a ResourceRelationshipQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.resource.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a ResourceRelationshipLookupSession to
 *  a ResourceRelationshipQuerySession.
 */

public abstract class AbstractQueryResourceRelationshipLookupSession
    extends net.okapia.osid.jamocha.resource.spi.AbstractResourceRelationshipLookupSession
    implements org.osid.resource.ResourceRelationshipLookupSession {

    private boolean effectiveonly = false;    
    private final org.osid.resource.ResourceRelationshipQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryResourceRelationshipLookupSession.
     *
     *  @param querySession the underlying resource relationship query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryResourceRelationshipLookupSession(org.osid.resource.ResourceRelationshipQuerySession querySession) {
        nullarg(querySession, "resource relationship query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Bin</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Bin Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBinId() {
        return (this.session.getBinId());
    }


    /**
     *  Gets the <code>Bin</code> associated with this 
     *  session.
     *
     *  @return the <code>Bin</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.Bin getBin()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getBin());
    }


    /**
     *  Tests if this user can perform <code>ResourceRelationship</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupResourceRelationships() {
        return (this.session.canSearchResourceRelationships());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include resource relationships in bins which are children
     *  of this bin in the bin hierarchy.
     */

    @OSID @Override
    public void useFederatedBinView() {
        this.session.useFederatedBinView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this bin only.
     */

    @OSID @Override
    public void useIsolatedBinView() {
        this.session.useIsolatedBinView();
        return;
    }
    

    /**
     *  Only resource relationships whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveResourceRelationshipView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All resource relationships of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveResourceRelationshipView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>ResourceRelationship</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>ResourceRelationship</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>ResourceRelationship</code> and retained for
     *  compatibility.
     *
     *  In effective mode, resource relationships are returned that
     *  are currently effective.  In any effective mode, effective
     *  resource relationships and those currently expired are
     *  returned.
     *
     *  @param  resourceRelationshipId <code>Id</code> of the
     *          <code>ResourceRelationship</code>
     *  @return the resource relationship
     *  @throws org.osid.NotFoundException <code>resourceRelationshipId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>resourceRelationshipId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationship getResourceRelationship(org.osid.id.Id resourceRelationshipId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resource.ResourceRelationshipQuery query = getQuery();
        query.matchId(resourceRelationshipId, true);
        org.osid.resource.ResourceRelationshipList resourceRelationships = this.session.getResourceRelationshipsByQuery(query);
        if (resourceRelationships.hasNext()) {
            return (resourceRelationships.getNextResourceRelationship());
        } 
        
        throw new org.osid.NotFoundException(resourceRelationshipId + " not found");
    }


    /**
     *  Gets a <code>ResourceRelationshipList</code> corresponding to
     *  the given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  resourceRelationships specified in the <code>Id</code> list,
     *  in the order of the list, including duplicates, or an error
     *  results if an <code>Id</code> in the supplied list is not
     *  found or inaccessible. Otherwise, inaccessible
     *  <code>ResourceRelationships</code> may be omitted from the
     *  list and may present the elements in any order including
     *  returning a unique set.
     *
     *  In effective mode, resource relationships are returned that
     *  are currently effective.  In any effective mode, effective
     *  resource relationships and those currently expired are
     *  returned.
     *
     *  @param  resourceRelationshipIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>ResourceRelationship</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>resourceRelationshipIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationshipsByIds(org.osid.id.IdList resourceRelationshipIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resource.ResourceRelationshipQuery query = getQuery();

        try (org.osid.id.IdList ids = resourceRelationshipIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getResourceRelationshipsByQuery(query));
    }


    /**
     *  Gets a <code>ResourceRelationshipList</code> corresponding to
     *  the given resource relationship genus <code>Type</code> which
     *  does not include resource relationships of types derived from
     *  the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known resource
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those resource relationships that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In effective mode, resource relationships are returned that
     *  are currently effective.  In any effective mode, effective
     *  resource relationships and those currently expired are
     *  returned.
     *
     *  @param  resourceRelationshipGenusType a resourceRelationship genus type 
     *  @return the returned <code>ResourceRelationship</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>resourceRelationshipGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationshipsByGenusType(org.osid.type.Type resourceRelationshipGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resource.ResourceRelationshipQuery query = getQuery();
        query.matchGenusType(resourceRelationshipGenusType, true);
        return (this.session.getResourceRelationshipsByQuery(query));
    }


    /**
     *  Gets a <code>ResourceRelationshipList</code> corresponding to
     *  the given resource relationship genus <code>Type</code> and
     *  include any additional resource relationships with genus types
     *  derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known resource
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those resource relationships that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In effective mode, resource relationships are returned that
     *  are currently effective.  In any effective mode, effective
     *  resource relationships and those currently expired are
     *  returned.
     *
     *  @param  resourceRelationshipGenusType a resourceRelationship genus type 
     *  @return the returned <code>ResourceRelationship</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>resourceRelationshipGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationshipsByParentGenusType(org.osid.type.Type resourceRelationshipGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resource.ResourceRelationshipQuery query = getQuery();
        query.matchParentGenusType(resourceRelationshipGenusType, true);
        return (this.session.getResourceRelationshipsByQuery(query));
    }


    /**
     *  Gets a <code>ResourceRelationshipList</code> containing the
     *  given resource relationship record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known resource
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those resource relationships that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In effective mode, resource relationships are returned that
     *  are currently effective.  In any effective mode, effective
     *  resource relationships and those currently expired are
     *  returned.
     *
     *  @param  resourceRelationshipRecordType a resourceRelationship record type 
     *  @return the returned <code>ResourceRelationship</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>resourceRelationshipRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationshipsByRecordType(org.osid.type.Type resourceRelationshipRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resource.ResourceRelationshipQuery query = getQuery();
        query.matchRecordType(resourceRelationshipRecordType, true);
        return (this.session.getResourceRelationshipsByQuery(query));
    }


    /**
     *  Gets a <code>ResourceRelationshipList</code> effective during
     *  the entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known resource
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those resource relationships that are
     *  accessible through this session.
     *  
     *  In effective mode, resource relationships are returned that are currently
     *  effective.  In any effective mode, effective resource relationships and
     *  those currently expired are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>ResourceRelationship</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationshipsOnDate(org.osid.calendaring.DateTime from, 
                                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resource.ResourceRelationshipQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getResourceRelationshipsByQuery(query));
    }
        

    /**
     *  Gets a list of resource relationships corresponding to a
     *  source resource <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known resource
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those resource relationships that are
     *  accessible through this session.
     *
     *  In effective mode, resource relationships are returned that
     *  are currently effective.  In any effective mode, effective
     *  resource relationships and those currently expired are
     *  returned.
     *
     *  @param  sourceRelationshipId the <code>Id</code> of the source resource
     *  @return the returned <code>ResourceRelationshipList</code>
     *  @throws org.osid.NullArgumentException <code>sourceRelationshipId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.resource.ResourceRelationshipList getResourceRelationshipsForSourceResource(org.osid.id.Id sourceRelationshipId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.resource.ResourceRelationshipQuery query = getQuery();
        query.matchSourceResourceId(sourceRelationshipId, true);
        return (this.session.getResourceRelationshipsByQuery(query));
    }


    /**
     *  Gets a list of resource relationships corresponding to a
     *  source resource <code>Id</code> and effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *
     *  In plenary mode, the returned list contains all known resource
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those resource relationships that are
     *  accessible through this session.
     *
     *  In effective mode, resource relationships are returned that
     *  are currently effective.  In any effective mode, effective
     *  resource relationships and those currently expired are
     *  returned.
     *
     *  @param sourceRelationshipId the <code>Id</code> of the source
     *         resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ResourceRelationshipList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>sourceRelationshipId</code>, <code>from</code>
     *          or <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationshipsForSourceResourceOnDate(org.osid.id.Id sourceRelationshipId,
                                                                                                      org.osid.calendaring.DateTime from,
                                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resource.ResourceRelationshipQuery query = getQuery();
        query.matchSourceResourceId(sourceRelationshipId, true);
        query.matchDate(from, to, true);
        return (this.session.getResourceRelationshipsByQuery(query));
    }

    
    /**
     *  Gets the <code> ResourceRelationships </code> of a resource of
     *  relationship genus type that includes any genus type derived
     *  from the given one.
     *  
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those relationships that are accessible
     *  through this session.
     *  
     *  In effective mode, resource relationships are returned that
     *  are currently effective. In any effective mode, effective
     *  resource relationships and those currently expired are
     *  returned.
     *
     *  @param  sourceResourceId <code> Id </code> of a <code> Resource 
     *          </code> 
     *  @param  relationshipGenusType a relationship genus type 
     *  @return the relationships 
     *  @throws org.osid.NullArgumentException <code> sourceResourceId </code> 
     *          or <code> relationshipGenusType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationshipsByGenusTypeForSourceResource(org.osid.id.Id sourceResourceId, 
                                                                                                           org.osid.type.Type relationshipGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resource.ResourceRelationshipQuery query = getQuery();
        query.matchSourceResourceId(sourceResourceId, true);
        query.matchGenusType(relationshipGenusType, true);            
        return (this.session.getResourceRelationshipsByQuery(query));
    }
    

    /**
     *  Gets a list of resource relationships of a given genus type
     *  for a resource and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known resource
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those resource relationships that are
     *  accessible through this session.
     *  
     *  In effective mode, resource relationships are returned that are 
     *  currently effective in addition to being effective during the given 
     *  dates. In any effective mode, effective resource relationships and 
     *  those currently expired are returned. 
     *
     *  @param  sourceResourceId a resource <code> Id </code> 
     *  @param  relationshipGenusType a relationship genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the relationships 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> sourceResourceId, 
     *          relationshipGenusType, from </code> or <code> to </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationshipsByGenusTypeForSourceResourceOnDate(org.osid.id.Id sourceResourceId, 
                                                                                                                 org.osid.type.Type relationshipGenusType, 
                                                                                                                 org.osid.calendaring.DateTime from, 
                                                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resource.ResourceRelationshipQuery query = getQuery();
        query.matchSourceResourceId(sourceResourceId, true);
        query.matchGenusType(relationshipGenusType, true);            
        query.matchDate(from, to, true);
        return (this.session.getResourceRelationshipsByQuery(query));
    }


    /**
     *  Gets a list of resource relationships corresponding to a
     *  destination resource <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known resource
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those resource relationships that are
     *  accessible through this session.
     *
     *  In effective mode, resource relationships are returned that
     *  are currently effective.  In any effective mode, effective
     *  resource relationships and those currently expired are
     *  returned.
     *
     *  @param destinationResourceId the <code>Id</code> of the
     *         destination resource
     *  @return the returned <code>ResourceRelationshipList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>destinationResourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.resource.ResourceRelationshipList getResourceRelationshipsForDestinationResource(org.osid.id.Id destinationResourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.resource.ResourceRelationshipQuery query = getQuery();
        query.matchDestinationResourceId(destinationResourceId, true);
        return (this.session.getResourceRelationshipsByQuery(query));
    }


    /**
     *  Gets a list of resource relationships corresponding to a
     *  destination resource <code>Id</code> and effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *
     *  In plenary mode, the returned list contains all known resource
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those resource relationships that are
     *  accessible through this session.
     *
     *  In effective mode, resource relationships are returned that
     *  are currently effective.  In any effective mode, effective
     *  resource relationships and those currently expired are
     *  returned.
     *
     *  @param destinationResourceId the <code>Id</code> of the
     *         destination resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ResourceRelationshipList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>destinationResourceId</code>, <code>from</code>
     *          or <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationshipsForDestinationResourceOnDate(org.osid.id.Id destinationResourceId,
                                                                                                           org.osid.calendaring.DateTime from,
                                                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resource.ResourceRelationshipQuery query = getQuery();
        query.matchDestinationResourceId(destinationResourceId, true);
        query.matchDate(from, to, true);
        return (this.session.getResourceRelationshipsByQuery(query));
    }


    /**
     *  Gets the <code> ResourceRelationships </code> of a resource of
     *  relationship genus type that includes any genus type derived
     *  from the given one.
     *  
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those relationships that are accessible
     *  through this session.
     *  
     *  In effective mode, resource relationships are returned that
     *  are currently effective. In any effective mode, effective
     *  resource relationships and those currently expired are
     *  returned.
     *
     *  @param destinationResourceId <code> Id </code> of a <code>
     *          Resource </code>
     *  @param  relationshipGenusType a relationship genus type 
     *  @return the relationships 
     *  @throws org.osid.NullArgumentException <code>
     *          destinationResourceId </code> or <code>
     *          relationshipGenusType </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationshipsByGenusTypeForDestinationResource(org.osid.id.Id destinationResourceId, 
                                                                                                                org.osid.type.Type relationshipGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resource.ResourceRelationshipQuery query = getQuery();
        query.matchDestinationResourceId(destinationResourceId, true);
        query.matchGenusType(relationshipGenusType, true);
        return (this.session.getResourceRelationshipsByQuery(query));
    }


    /**
     *  Gets a list of resource relationships of a given genus type
     *  for a resource and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known resource
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those resource relationships that are
     *  accessible through this session.
     *  
     *  In effective mode, resource relationships are returned that
     *  are currently effective in addition to being effective during
     *  the given dates. In any effective mode, effective resource
     *  relationships and those currently expired are returned.
     *
     *  @param  destinationResourceId a resource <code> Id </code> 
     *  @param  relationshipGenusType a relationship genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the relationships 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code>
     *          destinationResourceId, relationshipGenusType, from
     *          </code> or <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationshipsByGenusTypeForDestinationResourceOnDate(org.osid.id.Id destinationResourceId, 
                                                                                                                      org.osid.type.Type relationshipGenusType, 
                                                                                                                      org.osid.calendaring.DateTime from, 
                                                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resource.ResourceRelationshipQuery query = getQuery();
        query.matchDestinationResourceId(destinationResourceId, true);
        query.matchGenusType(relationshipGenusType, true);
        query.matchDate(from, to, true);
        return (this.session.getResourceRelationshipsByQuery(query));
    }


    /**
     *  Gets a list of resource relationships corresponding to source
     *  resource and destination resource <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known resource
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those resource relationships that are
     *  accessible through this session.
     *
     *  In effective mode, resource relationships are returned that
     *  are currently effective.  In any effective mode, effective
     *  resource relationships and those currently expired are
     *  returned.
     *
     *  @param sourceRelationshipId the <code>Id</code> of the source
     *         resource
     *  @param destinationResourceId the <code>Id</code> of the
     *         destination resource
     *  @return the returned <code>ResourceRelationshipList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>sourceRelationshipId</code>,
     *          <code>destinationResourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationshipsForResources(org.osid.id.Id sourceRelationshipId,
                                                                                           org.osid.id.Id destinationResourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resource.ResourceRelationshipQuery query = getQuery();
        query.matchSourceResourceId(sourceRelationshipId, true);
        query.matchDestinationResourceId(destinationResourceId, true);
        return (this.session.getResourceRelationshipsByQuery(query));
    }


    /**
     *  Gets a list of resource relationships corresponding to source
     *  resource and destination resource <code>Ids</code> and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *
     *  In plenary mode, the returned list contains all known resource
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those resource relationships that are
     *  accessible through this session.
     *
     *  In effective mode, resource relationships are returned that
     *  are currently effective.  In any effective mode, effective
     *  resource relationships and those currently expired are
     *  returned.
     *
     *  @param sourceRelationshipId the <code>Id</code> of the source
     *         resource
     *  @param destinationResourceId the <code>Id</code> of the
     *         destination resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ResourceRelationshipList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>sourceRelationshipId</code>,
     *          <code>destinationResourceId</code>, <code>from</code>
     *          or <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationshipsForResourcesOnDate(org.osid.id.Id sourceRelationshipId,
                                                                                                 org.osid.id.Id destinationResourceId,
                                                                                                 org.osid.calendaring.DateTime from,
                                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.resource.ResourceRelationshipQuery query = getQuery();
        query.matchSourceResourceId(sourceRelationshipId, true);
        query.matchDestinationResourceId(destinationResourceId, true);
        query.matchDate(from, to, true);
        return (this.session.getResourceRelationshipsByQuery(query));
    }

    
    /**
     *  Gets the <code> ResourceRelationships </code> given two
     *  resources and a relationship genus type which includes any
     *  genus types derived from the given genus type.
     *  
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those relationships that are accessible
     *  through this session.
     *  
     *  In effective mode, resource relationships are returned that
     *  are currently effective. In any effective mode, effective
     *  resource relationships and those currently expired are
     *  returned.
     *
     *  @param  sourceResourceId <code> Id </code> of a <code> Resource 
     *          </code> 
     *  @param  destinationResourceId <code> Id </code> of another <code> 
     *          Resource </code> 
     *  @param  relationshipGenusType a relationship genus type 
     *  @return the relationships 
     *  @throws org.osid.NullArgumentException <code> sourceResourceId, 
     *          destinationResourceId, </code> or <code> relatonshipGenusType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationshipsByGenusTypeForResources(org.osid.id.Id sourceResourceId, 
                                                                                                      org.osid.id.Id destinationResourceId, 
                                                                                                      org.osid.type.Type relationshipGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resource.ResourceRelationshipQuery query = getQuery();
        query.matchSourceResourceId(sourceResourceId, true);
        query.matchDestinationResourceId(destinationResourceId, true);
        query.matchGenusType(relationshipGenusType, true);
        return (this.session.getResourceRelationshipsByQuery(query));
    }


    /**
     *  Gets a list of resource relationships of a given genus type
     *  for a two peer resources and effective during the entire given
     *  date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known resource
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those resource relationships that are
     *  accessible through this session.
     *  
     *  In effective mode, resource relationships are returned that
     *  are currently effective in addition to being effective during
     *  the given dates. In any effective mode, effective resource
     *  relationships and those currently expired are returned.
     *
     *  @param  sourceResourceId a resource <code> Id </code> 
     *  @param destinationResourceId <code> Id </code> of another
     *          <code> Resource </code>
     *  @param  relationshipGenusType a relationship genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the relationships 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code>
     *          sourceResourceId, destinationResourceId,
     *          relationshipGenusType, from </code> or <code> to
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationshipsByGenusTypeForResourcesOnDate(org.osid.id.Id sourceResourceId, 
                                                                                                            org.osid.id.Id destinationResourceId, 
                                                                                                            org.osid.type.Type relationshipGenusType, 
                                                                                                            org.osid.calendaring.DateTime from, 
                                                                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resource.ResourceRelationshipQuery query = getQuery();
        query.matchSourceResourceId(sourceResourceId, true);
        query.matchDestinationResourceId(destinationResourceId, true);
        query.matchGenusType(relationshipGenusType, true);
        query.matchDate(from, to, true);
        return (this.session.getResourceRelationshipsByQuery(query));
    }


    /**
     *  Gets all <code>ResourceRelationships</code>. 
     *
     *  In plenary mode, the returned list contains all known resource
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those resource relationships that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In effective mode, resource relationships are returned that
     *  are currently effective.  In any effective mode, effective
     *  resource relationships and those currently expired are
     *  returned.
     *
     *  @return a list of <code>ResourceRelationships</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationships()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resource.ResourceRelationshipQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getResourceRelationshipsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.resource.ResourceRelationshipQuery getQuery() {
        org.osid.resource.ResourceRelationshipQuery query = this.session.getResourceRelationshipQuery();
        
        if (isEffectiveOnly()) {
            query.matchEffective(true);
        }

        return (query);
    }
}
