//
// AbstractAssemblyAgentQuery.java
//
//     An AgentQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.authentication.agent.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An AgentQuery that stores terms.
 */

public abstract class AbstractAssemblyAgentQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.authentication.AgentQuery,
               org.osid.authentication.AgentQueryInspector,
               org.osid.authentication.AgentSearchOrder {

    private final java.util.Collection<org.osid.authentication.records.AgentQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.authentication.records.AgentQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.authentication.records.AgentSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyAgentQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyAgentQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the resource <code> Id </code> for this query. 
     *
     *  @param  agencyId a resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agencyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResourceId(org.osid.id.Id agencyId, boolean match) {
        getAssembler().addIdTerm(getResourceIdColumn(), agencyId, match);
        return;
    }


    /**
     *  Clears the resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearResourceIdTerms() {
        getAssembler().clearTerms(getResourceIdColumn());
        return;
    }


    /**
     *  Gets the resource <code> Id </code> terms. 
     *
     *  @return the resource <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResourceIdTerms() {
        return (getAssembler().getIdTerms(getResourceIdColumn()));
    }


    /**
     *  Orders the results by resource. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByResource(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getResourceColumn(), style);
        return;
    }


    /**
     *  Gets the ResourceId column name.
     *
     * @return the column name
     */

    protected String getResourceIdColumn() {
        return ("resource_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsResourceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getResourceQuery() {
        throw new org.osid.UnimplementedException("supportsResourceQuery() is false");
    }


    /**
     *  Matches agents with any resource. 
     *
     *  @param  match <code> true </code> if to match agents with a resource, 
     *          <code> false </code> to match agents with no resource 
     */

    @OSID @Override
    public void matchAnyResource(boolean match) {
        getAssembler().addIdWildcardTerm(getResourceColumn(), match);
        return;
    }


    /**
     *  Clears the resource terms. 
     */

    @OSID @Override
    public void clearResourceTerms() {
        getAssembler().clearTerms(getResourceColumn());
        return;
    }


    /**
     *  Gets the resource terms. 
     *
     *  @return the resource terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getResourceTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a <code> ResourceSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if a resource search order interface is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource search order. 
     *
     *  @return the resource search odrer 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getResourceSearchOrder() {
        throw new org.osid.UnimplementedException("supportsResourceSearchOrder() is false");
    }


    /**
     *  Gets the Resource column name.
     *
     * @return the column name
     */

    protected String getResourceColumn() {
        return ("resource");
    }


    /**
     *  Sets the agency <code> Id </code> for this query. 
     *
     *  @param  agencyId an agency <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> agencyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAgencyId(org.osid.id.Id agencyId, boolean match) {
        getAssembler().addIdTerm(getAgencyIdColumn(), agencyId, match);
        return;
    }


    /**
     *  Clears the agency <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAgencyIdTerms() {
        getAssembler().clearTerms(getAgencyIdColumn());
        return;
    }


    /**
     *  Gets the agency <code> Id </code> terms. 
     *
     *  @return the agency <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAgencyIdTerms() {
        return (getAssembler().getIdTerms(getAgencyIdColumn()));
    }


    /**
     *  Gets the AgencyId column name.
     *
     * @return the column name
     */

    protected String getAgencyIdColumn() {
        return ("agency_id");
    }


    /**
     *  Tests if an <code> AgencyQuery </code> is available. 
     *
     *  @return <code> true </code> if an agency query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgencyQuery() {
        return (false);
    }


    /**
     *  Gets the query for an agency. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the agency query 
     *  @throws org.osid.UnimplementedException <code> supportsAgencyQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgencyQuery getAgencyQuery() {
        throw new org.osid.UnimplementedException("supportsAgencyQuery() is false");
    }


    /**
     *  Clears the agency terms. 
     */

    @OSID @Override
    public void clearAgencyTerms() {
        getAssembler().clearTerms(getAgencyColumn());
        return;
    }


    /**
     *  Gets the agency terms. 
     *
     *  @return the agency terms 
     */

    @OSID @Override
    public org.osid.authentication.AgencyQueryInspector[] getAgencyTerms() {
        return (new org.osid.authentication.AgencyQueryInspector[0]);
    }


    /**
     *  Gets the Agency column name.
     *
     * @return the column name
     */

    protected String getAgencyColumn() {
        return ("agency");
    }


    /**
     *  Tests if this agent supports the given record
     *  <code>Type</code>.
     *
     *  @param  agentRecordType an agent record type 
     *  @return <code>true</code> if the agentRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>agentRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type agentRecordType) {
        for (org.osid.authentication.records.AgentQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(agentRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  agentRecordType the agent record type 
     *  @return the agent query record 
     *  @throws org.osid.NullArgumentException
     *          <code>agentRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(agentRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.authentication.records.AgentQueryRecord getAgentQueryRecord(org.osid.type.Type agentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.authentication.records.AgentQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(agentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(agentRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  agentRecordType the agent record type 
     *  @return the agent query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>agentRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(agentRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.authentication.records.AgentQueryInspectorRecord getAgentQueryInspectorRecord(org.osid.type.Type agentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.authentication.records.AgentQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(agentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(agentRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param agentRecordType the agent record type
     *  @return the agent search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>agentRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(agentRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.authentication.records.AgentSearchOrderRecord getAgentSearchOrderRecord(org.osid.type.Type agentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.authentication.records.AgentSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(agentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(agentRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this agent. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param agentQueryRecord the agent query record
     *  @param agentQueryInspectorRecord the agent query inspector
     *         record
     *  @param agentSearchOrderRecord the agent search order record
     *  @param agentRecordType agent record type
     *  @throws org.osid.NullArgumentException
     *          <code>agentQueryRecord</code>,
     *          <code>agentQueryInspectorRecord</code>,
     *          <code>agentSearchOrderRecord</code> or
     *          <code>agentRecordTypeagent</code> is
     *          <code>null</code>
     */
            
    protected void addAgentRecords(org.osid.authentication.records.AgentQueryRecord agentQueryRecord, 
                                      org.osid.authentication.records.AgentQueryInspectorRecord agentQueryInspectorRecord, 
                                      org.osid.authentication.records.AgentSearchOrderRecord agentSearchOrderRecord, 
                                      org.osid.type.Type agentRecordType) {

        addRecordType(agentRecordType);

        nullarg(agentQueryRecord, "agent query record");
        nullarg(agentQueryInspectorRecord, "agent query inspector record");
        nullarg(agentSearchOrderRecord, "agent search odrer record");

        this.queryRecords.add(agentQueryRecord);
        this.queryInspectorRecords.add(agentQueryInspectorRecord);
        this.searchOrderRecords.add(agentSearchOrderRecord);
        
        return;
    }
}
