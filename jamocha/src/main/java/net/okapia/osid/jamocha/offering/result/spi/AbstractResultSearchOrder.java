//
// AbstractResultSearchOdrer.java
//
//     Defines a ResultSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.result.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code ResultSearchOrder}.
 */

public abstract class AbstractResultSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractTemporalOsidObjectSearchOrder
    implements org.osid.offering.ResultSearchOrder {

    private final java.util.Collection<org.osid.offering.records.ResultSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specifies a preference for ordering the result set by the participant. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByParticipant(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a participant search order is available. 
     *
     *  @return <code> true </code> if a participant search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParticipantSearchOrder() {
        return (false);
    }


    /**
     *  Gets the participant search order. 
     *
     *  @return the participant search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParticipantSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.ParticipantSearchOrder getParticipantSearchOrder() {
        throw new org.osid.UnimplementedException("supportsParticipantSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the commitment. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByGrade(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a grade search order is available. 
     *
     *  @return <code> true </code> if a grade search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeSearchOrder() {
        return (false);
    }


    /**
     *  Gets the grade search order. 
     *
     *  @return the grade search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSearchOrder getGradeSearchOrder() {
        throw new org.osid.UnimplementedException("supportsGradeSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the value. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByValue(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  resultRecordType a result record type 
     *  @return {@code true} if the resultRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code resultRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type resultRecordType) {
        for (org.osid.offering.records.ResultSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(resultRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  resultRecordType the result record type 
     *  @return the result search order record
     *  @throws org.osid.NullArgumentException
     *          {@code resultRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(resultRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.offering.records.ResultSearchOrderRecord getResultSearchOrderRecord(org.osid.type.Type resultRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.offering.records.ResultSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(resultRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(resultRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this result. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param resultRecord the result search odrer record
     *  @param resultRecordType result record type
     *  @throws org.osid.NullArgumentException
     *          {@code resultRecord} or
     *          {@code resultRecordTyperesult} is
     *          {@code null}
     */
            
    protected void addResultRecord(org.osid.offering.records.ResultSearchOrderRecord resultSearchOrderRecord, 
                                     org.osid.type.Type resultRecordType) {

        addRecordType(resultRecordType);
        this.records.add(resultSearchOrderRecord);
        
        return;
    }
}
