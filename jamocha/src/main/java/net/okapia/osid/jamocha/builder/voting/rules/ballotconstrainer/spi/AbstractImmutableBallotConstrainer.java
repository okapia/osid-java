//
// AbstractImmutableBallotConstrainer.java
//
//     Wraps a mutable BallotConstrainer to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.voting.rules.ballotconstrainer.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>BallotConstrainer</code> to hide modifiers. This
 *  wrapper provides an immutized BallotConstrainer from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying ballotConstrainer whose state changes are visible.
 */

public abstract class AbstractImmutableBallotConstrainer
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidConstrainer
    implements org.osid.voting.rules.BallotConstrainer {

    private final org.osid.voting.rules.BallotConstrainer ballotConstrainer;


    /**
     *  Constructs a new <code>AbstractImmutableBallotConstrainer</code>.
     *
     *  @param ballotConstrainer the ballot constrainer to immutablize
     *  @throws org.osid.NullArgumentException <code>ballotConstrainer</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableBallotConstrainer(org.osid.voting.rules.BallotConstrainer ballotConstrainer) {
        super(ballotConstrainer);
        this.ballotConstrainer = ballotConstrainer;
        return;
    }


    /**
     *  Gets the ballot constrainer record corresponding to the given <code> 
     *  BallotConstrainer </code> record <code> Type. </code> This method is 
     *  used to retrieve an object implementing the requested record. The 
     *  <code> ballotConstrainerRecordType </code> may be the <code> Type 
     *  </code> returned in <code> getRecordTypes() </code> or any of its 
     *  parents in a <code> Type </code> hierarchy where <code> 
     *  hasRecordType(ballotConstrainerRecordType) </code> is <code> true 
     *  </code> . 
     *
     *  @param  ballotConstrainerRecordType the type of ballot constrainer 
     *          record to retrieve 
     *  @return the ballot constrainer record 
     *  @throws org.osid.NullArgumentException <code> 
     *          ballotConstrainerRecordType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(ballotConstrainerRecordType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.records.BallotConstrainerRecord getBallotConstrainerRecord(org.osid.type.Type ballotConstrainerRecordType)
        throws org.osid.OperationFailedException {

        return (this.ballotConstrainer.getBallotConstrainerRecord(ballotConstrainerRecordType));
    }
}

