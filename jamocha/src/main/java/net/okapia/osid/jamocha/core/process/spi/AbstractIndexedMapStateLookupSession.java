//
// AbstractIndexedMapStateLookupSession.java
//
//    A simple framework for providing a State lookup service
//    backed by a fixed collection of states with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.process.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a State lookup service backed by a
 *  fixed collection of states. The states are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some states may be compatible
 *  with more types than are indicated through these state
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>States</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapStateLookupSession
    extends AbstractMapStateLookupSession
    implements org.osid.process.StateLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.process.State> statesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.process.State>());
    private final MultiMap<org.osid.type.Type, org.osid.process.State> statesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.process.State>());


    /**
     *  Makes a <code>State</code> available in this session.
     *
     *  @param  state a state
     *  @throws org.osid.NullArgumentException <code>state<code> is
     *          <code>null</code>
     */

    @Override
    protected void putState(org.osid.process.State state) {
        super.putState(state);

        this.statesByGenus.put(state.getGenusType(), state);
        
        try (org.osid.type.TypeList types = state.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.statesByRecord.put(types.getNextType(), state);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a state from this session.
     *
     *  @param stateId the <code>Id</code> of the state
     *  @throws org.osid.NullArgumentException <code>stateId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeState(org.osid.id.Id stateId) {
        org.osid.process.State state;
        try {
            state = getState(stateId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.statesByGenus.remove(state.getGenusType());

        try (org.osid.type.TypeList types = state.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.statesByRecord.remove(types.getNextType(), state);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeState(stateId);
        return;
    }


    /**
     *  Gets a <code>StateList</code> corresponding to the given
     *  state genus <code>Type</code> which does not include
     *  states of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known states or an error results. Otherwise,
     *  the returned list may contain only those states that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  stateGenusType a state genus type 
     *  @return the returned <code>State</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stateGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.process.StateList getStatesByGenusType(org.osid.type.Type stateGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.process.state.ArrayStateList(this.statesByGenus.get(stateGenusType)));
    }


    /**
     *  Gets a <code>StateList</code> containing the given
     *  state record <code>Type</code>. In plenary mode, the
     *  returned list contains all known states or an error
     *  results. Otherwise, the returned list may contain only those
     *  states that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  stateRecordType a state record type 
     *  @return the returned <code>state</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stateRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.process.StateList getStatesByRecordType(org.osid.type.Type stateRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.process.state.ArrayStateList(this.statesByRecord.get(stateRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.statesByGenus.clear();
        this.statesByRecord.clear();

        super.close();

        return;
    }
}
