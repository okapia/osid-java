//
// AbstractGradebookColumnSummaryQuery.java
//
//     A template for making a GradebookColumnSummary Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.gradebookcolumnsummary.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for gradebook column summaries.
 */

public abstract class AbstractGradebookColumnSummaryQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRuleQuery
    implements org.osid.grading.GradebookColumnSummaryQuery {

    private final java.util.Collection<org.osid.grading.records.GradebookColumnSummaryQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the gradebook column <code> Id </code> for this query. 
     *
     *  @param  gradebookColumnId a gradeboo column <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradebookColumnId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchGradebookColumnId(org.osid.id.Id gradebookColumnId, 
                                       boolean match) {
        return;
    }


    /**
     *  Clears the gradebook column <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGradebookColumnIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> GradebookColumnQuery </code> is available for 
     *  querying gradebook column. 
     *
     *  @return <code> true </code> if a gradebook column query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookColumnQuery() {
        return (false);
    }


    /**
     *  Gets the query for a gradebook column. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the gradebook column query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnQuery getGradebookColumnQuery() {
        throw new org.osid.UnimplementedException("supportsGradebookColumnQuery() is false");
    }


    /**
     *  Matches gradebook column derivations with any gradebookc olumn. 
     *
     *  @param  match <code> true </code> to match gradebook column 
     *          derivations with any gradebook column, <code> false </code> to 
     *          match gradebook column derivations with no gradebook columns 
     */

    @OSID @Override
    public void matchAnyGradebookColumn(boolean match) {
        return;
    }


    /**
     *  Clears the source grade system terms. 
     */

    @OSID @Override
    public void clearGradebookColumnTerms() {
        return;
    }


    /**
     *  Matches a mean between the given values inclusive. 
     *
     *  @param  low low end of range 
     *  @param  high high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     */

    @OSID @Override
    public void matchMean(java.math.BigDecimal low, java.math.BigDecimal high, 
                          boolean match) {
        return;
    }


    /**
     *  Clears the mean terms. 
     */

    @OSID @Override
    public void clearMeanTerms() {
        return;
    }


    /**
     *  Matches a mean greater than or equal to the given value. 
     *
     *  @param  value minimum value 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchMinimumMean(java.math.BigDecimal value, boolean match) {
        return;
    }


    /**
     *  Clears the minimum mean terms. 
     */

    @OSID @Override
    public void clearMinimumMeanTerms() {
        return;
    }


    /**
     *  Matches a median between the given values inclusive. 
     *
     *  @param  low low end of range 
     *  @param  high high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     */

    @OSID @Override
    public void matchMedian(java.math.BigDecimal low, 
                            java.math.BigDecimal high, boolean match) {
        return;
    }


    /**
     *  Clears the median terms. 
     */

    @OSID @Override
    public void clearMedianTerms() {
        return;
    }


    /**
     *  Matches a median greater than or equal to the given value. 
     *
     *  @param  value minimum value 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchMinimumMedian(java.math.BigDecimal value, boolean match) {
        return;
    }


    /**
     *  Clears the minimum median terms. 
     */

    @OSID @Override
    public void clearMinimumMedianTerms() {
        return;
    }


    /**
     *  Matches a mode between the given values inclusive. 
     *
     *  @param  low low end of range 
     *  @param  high high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     */

    @OSID @Override
    public void matchMode(java.math.BigDecimal low, java.math.BigDecimal high, 
                          boolean match) {
        return;
    }


    /**
     *  Clears the mode terms. 
     */

    @OSID @Override
    public void clearModeTerms() {
        return;
    }


    /**
     *  Matches a mode greater than or equal to the given value. 
     *
     *  @param  value minimum value 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchMinimumMode(java.math.BigDecimal value, boolean match) {
        return;
    }


    /**
     *  Clears the minimum mode terms. 
     */

    @OSID @Override
    public void clearMinimumModeTerms() {
        return;
    }


    /**
     *  Matches a root mean square between the given values inclusive. 
     *
     *  @param  low low end of range 
     *  @param  high high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     */

    @OSID @Override
    public void matchRMS(java.math.BigDecimal low, java.math.BigDecimal high, 
                         boolean match) {
        return;
    }


    /**
     *  Clears the root mean square terms. 
     */

    @OSID @Override
    public void clearRMSTerms() {
        return;
    }


    /**
     *  Matches a root mean square greater than or equal to the given value. 
     *
     *  @param  value minimum value 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchMinimumRMS(java.math.BigDecimal value, boolean match) {
        return;
    }


    /**
     *  Clears the minimum RMS terms. 
     */

    @OSID @Override
    public void clearMinimumRMSTerms() {
        return;
    }


    /**
     *  Matches a standard deviation mean square between the given values 
     *  inclusive. 
     *
     *  @param  low low end of range 
     *  @param  high high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     */

    @OSID @Override
    public void matchStandardDeviation(java.math.BigDecimal low, 
                                       java.math.BigDecimal high, 
                                       boolean match) {
        return;
    }


    /**
     *  Clears the standard deviation terms. 
     */

    @OSID @Override
    public void clearStandardDeviationTerms() {
        return;
    }


    /**
     *  Matches a standard deviation greater than or equal to the given value. 
     *
     *  @param  value minimum value 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchMinimumStandardDeviation(java.math.BigDecimal value, 
                                              boolean match) {
        return;
    }


    /**
     *  Clears the minimum standard deviation terms. 
     */

    @OSID @Override
    public void clearMinimumStandardDeviationTerms() {
        return;
    }


    /**
     *  Matches a sum mean square between the given values inclusive. 
     *
     *  @param  low low end of range 
     *  @param  high high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     */

    @OSID @Override
    public void matchSum(java.math.BigDecimal low, java.math.BigDecimal high, 
                         boolean match) {
        return;
    }


    /**
     *  Clears the sum terms. 
     */

    @OSID @Override
    public void clearSumTerms() {
        return;
    }


    /**
     *  Matches a sum greater than or equal to the given value. 
     *
     *  @param  value minimum value 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchMinimumSum(java.math.BigDecimal value, boolean match) {
        return;
    }


    /**
     *  Clears the minimum sum terms. 
     */

    @OSID @Override
    public void clearMinimumSumTerms() {
        return;
    }


    /**
     *  Sets the gradebook <code> Id </code> for this query. 
     *
     *  @param  gradebookId a gradebook <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGradebookId(org.osid.id.Id gradebookId, boolean match) {
        return;
    }


    /**
     *  Clears the gradebook <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGradebookIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> GradebookQuery </code> is available . 
     *
     *  @return <code> true </code> if a gradebook query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookQuery() {
        return (false);
    }


    /**
     *  Gets the query for a gradebook. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the gradebook query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookQuery getGradebookQuery() {
        throw new org.osid.UnimplementedException("supportsGradebookQuery() is false");
    }


    /**
     *  Clears the gradebook terms. 
     */

    @OSID @Override
    public void clearGradebookTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given gradebook column summary query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a gradebook column summary implementing the requested record.
     *
     *  @param gradebookColumnSummaryRecordType a gradebook column summary record type
     *  @return the gradebook column summary query record
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumnSummaryRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(gradebookColumnSummaryRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.grading.records.GradebookColumnSummaryQueryRecord getGradebookColumnSummaryQueryRecord(org.osid.type.Type gradebookColumnSummaryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.grading.records.GradebookColumnSummaryQueryRecord record : this.records) {
            if (record.implementsRecordType(gradebookColumnSummaryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(gradebookColumnSummaryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this gradebook column summary query. 
     *
     *  @param gradebookColumnSummaryQueryRecord gradebook column summary query record
     *  @param gradebookColumnSummaryRecordType gradebookColumnSummary record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addGradebookColumnSummaryQueryRecord(org.osid.grading.records.GradebookColumnSummaryQueryRecord gradebookColumnSummaryQueryRecord, 
                                          org.osid.type.Type gradebookColumnSummaryRecordType) {

        addRecordType(gradebookColumnSummaryRecordType);
        nullarg(gradebookColumnSummaryQueryRecord, "gradebook column summary query record");
        this.records.add(gradebookColumnSummaryQueryRecord);        
        return;
    }
}
