//
// AbstractProvisionable.java
//
//     Defines a Provisionable.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.provisionable.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Provisionable</code>.
 */

public abstract class AbstractProvisionable
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.provisioning.Provisionable {

    private org.osid.provisioning.Pool pool;
    private org.osid.resource.Resource resource;
    private long use = 0;

    private final java.util.Collection<org.osid.provisioning.records.ProvisionableRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the pool <code> Id. </code> 
     *
     *  @return the pool <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getPoolId() {
        return (this.pool.getId());
    }


    /**
     *  Gets the pool. 
     *
     *  @return the pool 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.provisioning.Pool getPool()
        throws org.osid.OperationFailedException {

        return (this.pool);
    }


    /**
     *  Sets the pool.
     *
     *  @param pool a pool
     *  @throws org.osid.NullArgumentException <code>pool</code> is
     *          <code>null</code>
     */

    protected void setPool(org.osid.provisioning.Pool pool) {
        nullarg(pool, "pool");
        this.pool = pool;
        return;
    }


    /**
     *  Gets the resource <code> Id. </code> 
     *
     *  @return the resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getResourceId() {
        return (this.resource.getId());
    }


    /**
     *  Gets the resource. 
     *
     *  @return the resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getResource()
        throws org.osid.OperationFailedException {

        return (this.resource);
    }


    /**
     *  Sets the resource.
     *
     *  @param resource a resource
     *  @throws org.osid.NullArgumentException
     *          <code>resource</code> is <code>null</code>
     */

    protected void setResource(org.osid.resource.Resource resource) {
        nullarg(resource, "resource");
        this.resource = resource;
        return;
    }


    /**
     *  Gets the number of times this provisionable has been
     *  provisioned.
     *
     *  @return the position 
     */

    @OSID @Override
    public long getUse() {
        return (this.use);
    }


    /**
     *  Sets the use.
     *
     *  @param use an use
     *  @throws org.osid.InvalidArgumentException <code>use</code> is
     *          negative
     */

    protected void setUse(long use) {
        cardinalarg(use, "use");
        this.use = use;
        return;
    }


    /**
     *  Tests if this provisionable supports the given record
     *  <code>Type</code>.
     *
     *  @param  provisionableRecordType a provisionable record type 
     *  @return <code>true</code> if the provisionableRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>provisionableRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type provisionableRecordType) {
        for (org.osid.provisioning.records.ProvisionableRecord record : this.records) {
            if (record.implementsRecordType(provisionableRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Provisionable</code> record <code>Type</code>.
     *
     *  @param  provisionableRecordType the provisionable record type 
     *  @return the provisionable record 
     *  @throws org.osid.NullArgumentException
     *          <code>provisionableRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(provisionableRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.ProvisionableRecord getProvisionableRecord(org.osid.type.Type provisionableRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.records.ProvisionableRecord record : this.records) {
            if (record.implementsRecordType(provisionableRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(provisionableRecordType + " is not supported");
    }


    /**
     *  Adds a record to this provisionable. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param provisionableRecord the provisionable record
     *  @param provisionableRecordType provisionable record type
     *  @throws org.osid.NullArgumentException
     *          <code>provisionableRecord</code> or
     *          <code>provisionableRecordTypeprovisionable</code> is
     *          <code>null</code>
     */
            
    protected void addProvisionableRecord(org.osid.provisioning.records.ProvisionableRecord provisionableRecord, 
                                          org.osid.type.Type provisionableRecordType) {
        
        nullarg(provisionableRecord, "provisionable record");
        addRecordType(provisionableRecordType);
        this.records.add(provisionableRecord);
        
        return;
    }
}
