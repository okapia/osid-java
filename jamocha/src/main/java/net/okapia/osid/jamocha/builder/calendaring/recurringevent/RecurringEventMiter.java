//
// RecurringEventMiter.java
//
//     Defines a RecurringEvent miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.calendaring.recurringevent;


/**
 *  Defines a <code>RecurringEvent</code> miter for use with the builders.
 */

public interface RecurringEventMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidRuleMiter,
            net.okapia.osid.jamocha.builder.spi.ContainableMiter,
            org.osid.calendaring.RecurringEvent {


    /**
     *  Adds a schedule.
     *
     *  @param schedule a schedule
     *  @throws org.osid.NullArgumentException <code>schedule</code>
     *          is <code>null</code>
     */

    public void addSchedule(org.osid.calendaring.Schedule schedule);


    /**
     *  Sets all the schedules.
     *
     *  @param schedules a collection of schedules
     *  @throws org.osid.NullArgumentException <code>schedules</code>
     *          is <code>null</code>
     */

    public void setSchedules(java.util.Collection<org.osid.calendaring.Schedule> schedules);


    /**
     *  Adds a superseding event.
     *
     *  @param supersedingEvent a superseding event
     *  @throws org.osid.NullArgumentException
     *          <code>supersedingEvent</code> is <code>null</code>
     */

    public void addSupersedingEvent(org.osid.calendaring.SupersedingEvent supersedingEvent);


    /**
     *  Sets all the superseding events.
     *
     *  @param supersedingEvents a collection of superseding events
     *  @throws org.osid.NullArgumentException
     *          <code>supersedingEvents</code> is <code>null</code>
     */

    public void setSupersedingEvents(java.util.Collection<org.osid.calendaring.SupersedingEvent> supersedingEvents);


    /**
     *  Adds a specific meeting time.
     *
     *  @param meetingTime a specific meeting time
     *  @throws org.osid.NullArgumentException
     *          <code>meetingTime</code> is <code>null</code>
     */

    public void addSpecificMeetingTime(org.osid.calendaring.MeetingTime meetingTime);


    /**
     *  Sets all the specific meeting times.
     *
     *  @param meetingTimes a collection of specific meeting times
     *  @throws org.osid.NullArgumentException
     *          <code>meetingTimes</code> is <code>null</code>
     */

    public void setSpecificMeetingTimes(java.util.Collection<org.osid.calendaring.MeetingTime> meetingTimes);


    /**
     *  Adds a event.
     *
     *  @param event a event
     *  @throws org.osid.NullArgumentException <code>event</code> is
     *          <code>null</code>
     */

    public void addEvent(org.osid.calendaring.Event event);


    /**
     *  Sets all the events.
     *
     *  @param events a collection of events
     *  @throws org.osid.NullArgumentException <code>events</code>
     *          is <code>null</code>
     */

    public void setEvents(java.util.Collection<org.osid.calendaring.Event> events);


    /**
     *  Adds a blackout.
     *
     *  @param blackout a blackout
     *  @throws org.osid.NullArgumentException <code>blackout</code>
     *          is <code>null</code>
     */

    public void addBlackout(org.osid.calendaring.DateTimeInterval blackout);


    /**
     *  Sets all the blackouts.
     *
     *  @param blackouts a collection of blackouts
     *  @throws org.osid.NullArgumentException <code>blackouts</code>
     *          is <code>null</code>
     */

    public void setBlackouts(java.util.Collection<org.osid.calendaring.DateTimeInterval> blackouts);


    /**
     *  Adds a sponsor.
     *
     *  @param sponsor a sponsor
     *  @throws org.osid.NullArgumentException <code>sponsor</code> is
     *          <code>null</code>
     */

    public void addSponsor(org.osid.resource.Resource sponsor);


    /**
     *  Sets all the sponsors.
     *
     *  @param sponsors a collection of sponsors
     *  @throws org.osid.NullArgumentException <code>sponsors</code>
     *          is <code>null</code>
     */

    public void setSponsors(java.util.Collection<org.osid.resource.Resource> sponsors);


    /**
     *  Adds a RecurringEvent record.
     *
     *  @param record a recurringEvent record
     *  @param recordType the type of recurringEvent record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addRecurringEventRecord(org.osid.calendaring.records.RecurringEventRecord record, org.osid.type.Type recordType);
}       


