//
// AbstractFederatingResponseLookupSession.java
//
//     An abstract federating adapter for a ResponseLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.inquiry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  ResponseLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingResponseLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.inquiry.ResponseLookupSession>
    implements org.osid.inquiry.ResponseLookupSession {

    private boolean parallel = false;
    private org.osid.inquiry.Inquest inquest = new net.okapia.osid.jamocha.nil.inquiry.inquest.UnknownInquest();


    /**
     *  Constructs a new <code>AbstractFederatingResponseLookupSession</code>.
     */

    protected AbstractFederatingResponseLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.inquiry.ResponseLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Inquest/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Inquest Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getInquestId() {
        return (this.inquest.getId());
    }


    /**
     *  Gets the <code>Inquest</code> associated with this 
     *  session.
     *
     *  @return the <code>Inquest</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.Inquest getInquest()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.inquest);
    }


    /**
     *  Sets the <code>Inquest</code>.
     *
     *  @param  inquest the inquest for this session
     *  @throws org.osid.NullArgumentException <code>inquest</code>
     *          is <code>null</code>
     */

    protected void setInquest(org.osid.inquiry.Inquest inquest) {
        nullarg(inquest, "inquest");
        this.inquest = inquest;
        return;
    }


    /**
     *  Tests if this user can perform <code>Response</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupResponses() {
        for (org.osid.inquiry.ResponseLookupSession session : getSessions()) {
            if (session.canLookupResponses()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Response</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeResponseView() {
        for (org.osid.inquiry.ResponseLookupSession session : getSessions()) {
            session.useComparativeResponseView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Response</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryResponseView() {
        for (org.osid.inquiry.ResponseLookupSession session : getSessions()) {
            session.usePlenaryResponseView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include responses in inquests which are children
     *  of this inquest in the inquest hierarchy.
     */

    @OSID @Override
    public void useFederatedInquestView() {
        for (org.osid.inquiry.ResponseLookupSession session : getSessions()) {
            session.useFederatedInquestView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this inquest only.
     */

    @OSID @Override
    public void useIsolatedInquestView() {
        for (org.osid.inquiry.ResponseLookupSession session : getSessions()) {
            session.useIsolatedInquestView();
        }

        return;
    }


    /**
     *  Only responses whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveResponseView() {
        for (org.osid.inquiry.ResponseLookupSession session : getSessions()) {
            session.useEffectiveResponseView();
        }

        return;
    }


    /**
     *  All responses of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveResponseView() {
        for (org.osid.inquiry.ResponseLookupSession session : getSessions()) {
            session.useAnyEffectiveResponseView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Response</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Response</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Response</code> and
     *  retained for compatibility.
     *
     *  In effective mode, responses are returned that are currently
     *  effective.  In any effective mode, effective responses and
     *  those currently expired are returned.
     *
     *  @param  responseId <code>Id</code> of the
     *          <code>Response</code>
     *  @return the response
     *  @throws org.osid.NotFoundException <code>responseId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>responseId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.Response getResponse(org.osid.id.Id responseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.inquiry.ResponseLookupSession session : getSessions()) {
            try {
                return (session.getResponse(responseId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(responseId + " not found");
    }


    /**
     *  Gets a <code>ResponseList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  responses specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Responses</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, responses are returned that are currently effective.
     *  In any effective mode, effective responses and those currently expired
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getResponses()</code>.
     *
     *  @param  responseIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Response</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>responseIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseList getResponsesByIds(org.osid.id.IdList responseIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.inquiry.response.MutableResponseList ret = new net.okapia.osid.jamocha.inquiry.response.MutableResponseList();

        try (org.osid.id.IdList ids = responseIds) {
            while (ids.hasNext()) {
                ret.addResponse(getResponse(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>ResponseList</code> corresponding to the given
     *  response genus <code>Type</code> which does not include
     *  responses of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  responses or an error results. Otherwise, the returned list
     *  may contain only those responses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, responses are returned that are currently effective.
     *  In any effective mode, effective responses and those currently expired
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getResponses()</code>.
     *
     *  @param  responseGenusType a response genus type 
     *  @return the returned <code>Response</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>responseGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseList getResponsesByGenusType(org.osid.type.Type responseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inquiry.response.FederatingResponseList ret = getResponseList();

        for (org.osid.inquiry.ResponseLookupSession session : getSessions()) {
            ret.addResponseList(session.getResponsesByGenusType(responseGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ResponseList</code> corresponding to the given
     *  response genus <code>Type</code> and include any additional
     *  responses with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  responses or an error results. Otherwise, the returned list
     *  may contain only those responses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, responses are returned that are currently
     *  effective.  In any effective mode, effective responses and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getResponses()</code>.
     *
     *  @param  responseGenusType a response genus type 
     *  @return the returned <code>Response</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>responseGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseList getResponsesByParentGenusType(org.osid.type.Type responseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inquiry.response.FederatingResponseList ret = getResponseList();

        for (org.osid.inquiry.ResponseLookupSession session : getSessions()) {
            ret.addResponseList(session.getResponsesByParentGenusType(responseGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ResponseList</code> containing the given
     *  response record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  responses or an error results. Otherwise, the returned list
     *  may contain only those responses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, responses are returned that are currently
     *  effective.  In any effective mode, effective responses and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getResponses()</code>.
     *
     *  @param  responseRecordType a response record type 
     *  @return the returned <code>Response</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>responseRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseList getResponsesByRecordType(org.osid.type.Type responseRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inquiry.response.FederatingResponseList ret = getResponseList();

        for (org.osid.inquiry.ResponseLookupSession session : getSessions()) {
            ret.addResponseList(session.getResponsesByRecordType(responseRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ResponseList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  responses or an error results. Otherwise, the returned list
     *  may contain only those responses that are accessible
     *  through this session.
     *  
     *  In active mode, responses are returned that are currently
     *  active. In any status mode, active and inactive responses
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Response</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.inquiry.ResponseList getResponsesOnDate(org.osid.calendaring.DateTime from, 
                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inquiry.response.FederatingResponseList ret = getResponseList();

        for (org.osid.inquiry.ResponseLookupSession session : getSessions()) {
            ret.addResponseList(session.getResponsesOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a list of responses corresponding to an inquiry
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  responses or an error results. Otherwise, the returned list
     *  may contain only those responses that are accessible
     *  through this session.
     *
     *  In effective mode, responses are returned that are
     *  currently effective.  In any effective mode, effective
     *  responses and those currently expired are returned.
     *
     *  @param  inquiryId the <code>Id</code> of the inquiry
     *  @return the returned <code>ResponseList</code>
     *  @throws org.osid.NullArgumentException <code>inquiryId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.inquiry.ResponseList getResponsesForInquiry(org.osid.id.Id inquiryId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inquiry.response.FederatingResponseList ret = getResponseList();

        for (org.osid.inquiry.ResponseLookupSession session : getSessions()) {
            ret.addResponseList(session.getResponsesForInquiry(inquiryId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of responses corresponding to an inquiry
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  responses or an error results. Otherwise, the returned list
     *  may contain only those responses that are accessible
     *  through this session.
     *
     *  In effective mode, responses are returned that are
     *  currently effective.  In any effective mode, effective
     *  responses and those currently expired are returned.
     *
     *  @param  inquiryId the <code>Id</code> of the inquiry
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ResponseList</code>
     *  @throws org.osid.NullArgumentException <code>inquiryId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.inquiry.ResponseList getResponsesForInquiryOnDate(org.osid.id.Id inquiryId,
                                                                      org.osid.calendaring.DateTime from,
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inquiry.response.FederatingResponseList ret = getResponseList();

        for (org.osid.inquiry.ResponseLookupSession session : getSessions()) {
            ret.addResponseList(session.getResponsesForInquiryOnDate(inquiryId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of responses corresponding to a responder
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  responses or an error results. Otherwise, the returned list
     *  may contain only those responses that are accessible
     *  through this session.
     *
     *  In effective mode, responses are returned that are
     *  currently effective.  In any effective mode, effective
     *  responses and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the responder
     *  @return the returned <code>ResponseList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.inquiry.ResponseList getResponsesForResponder(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inquiry.response.FederatingResponseList ret = getResponseList();

        for (org.osid.inquiry.ResponseLookupSession session : getSessions()) {
            ret.addResponseList(session.getResponsesForResponder(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of responses corresponding to a responder
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  responses or an error results. Otherwise, the returned list
     *  may contain only those responses that are accessible
     *  through this session.
     *
     *  In effective mode, responses are returned that are
     *  currently effective.  In any effective mode, effective
     *  responses and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the responder
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ResponseList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.inquiry.ResponseList getResponsesForResponderOnDate(org.osid.id.Id resourceId,
                                                                        org.osid.calendaring.DateTime from,
                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inquiry.response.FederatingResponseList ret = getResponseList();

        for (org.osid.inquiry.ResponseLookupSession session : getSessions()) {
            ret.addResponseList(session.getResponsesForResponderOnDate(resourceId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of responses corresponding to inquiry and responder
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  responses or an error results. Otherwise, the returned list
     *  may contain only those responses that are accessible
     *  through this session.
     *
     *  In effective mode, responses are returned that are
     *  currently effective.  In any effective mode, effective
     *  responses and those currently expired are returned.
     *
     *  @param  inquiryId the <code>Id</code> of the inquiry
     *  @param  resourceId the <code>Id</code> of the responder
     *  @return the returned <code>ResponseList</code>
     *  @throws org.osid.NullArgumentException <code>inquiryId</code>,
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.inquiry.ResponseList getResponsesForInquiryAndResponder(org.osid.id.Id inquiryId,
                                                                            org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inquiry.response.FederatingResponseList ret = getResponseList();

        for (org.osid.inquiry.ResponseLookupSession session : getSessions()) {
            ret.addResponseList(session.getResponsesForInquiryAndResponder(inquiryId, resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of responses corresponding to inquiry and responder
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  responses or an error results. Otherwise, the returned list
     *  may contain only those responses that are accessible
     *  through this session.
     *
     *  In effective mode, responses are returned that are
     *  currently effective.  In any effective mode, effective
     *  responses and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the responder
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ResponseList</code>
     *  @throws org.osid.NullArgumentException <code>inquiryId</code>,
     *          <code>resourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.inquiry.ResponseList getResponsesForInquiryAndResponderOnDate(org.osid.id.Id inquiryId,
                                                                                  org.osid.id.Id resourceId,
                                                                                  org.osid.calendaring.DateTime from,
                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inquiry.response.FederatingResponseList ret = getResponseList();

        for (org.osid.inquiry.ResponseLookupSession session : getSessions()) {
            ret.addResponseList(session.getResponsesForInquiryAndResponderOnDate(inquiryId, resourceId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Responses</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  responses or an error results. Otherwise, the returned list
     *  may contain only those responses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, responses are returned that are currently
     *  effective.  In any effective mode, effective responses and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Responses</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseList getResponses()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inquiry.response.FederatingResponseList ret = getResponseList();

        for (org.osid.inquiry.ResponseLookupSession session : getSessions()) {
            ret.addResponseList(session.getResponses());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.inquiry.response.FederatingResponseList getResponseList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.inquiry.response.ParallelResponseList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.inquiry.response.CompositeResponseList());
        }
    }
}
