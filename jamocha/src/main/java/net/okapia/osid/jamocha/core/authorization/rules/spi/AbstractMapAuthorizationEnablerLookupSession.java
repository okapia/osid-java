//
// AbstractMapAuthorizationEnablerLookupSession
//
//    A simple framework for providing an AuthorizationEnabler lookup service
//    backed by a fixed collection of authorization enablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.authorization.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an AuthorizationEnabler lookup service backed by a
 *  fixed collection of authorization enablers. The authorization enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>AuthorizationEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapAuthorizationEnablerLookupSession
    extends net.okapia.osid.jamocha.authorization.rules.spi.AbstractAuthorizationEnablerLookupSession
    implements org.osid.authorization.rules.AuthorizationEnablerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.authorization.rules.AuthorizationEnabler> authorizationEnablers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.authorization.rules.AuthorizationEnabler>());


    /**
     *  Makes an <code>AuthorizationEnabler</code> available in this session.
     *
     *  @param  authorizationEnabler an authorization enabler
     *  @throws org.osid.NullArgumentException <code>authorizationEnabler<code>
     *          is <code>null</code>
     */

    protected void putAuthorizationEnabler(org.osid.authorization.rules.AuthorizationEnabler authorizationEnabler) {
        this.authorizationEnablers.put(authorizationEnabler.getId(), authorizationEnabler);
        return;
    }


    /**
     *  Makes an array of authorization enablers available in this session.
     *
     *  @param  authorizationEnablers an array of authorization enablers
     *  @throws org.osid.NullArgumentException <code>authorizationEnablers<code>
     *          is <code>null</code>
     */

    protected void putAuthorizationEnablers(org.osid.authorization.rules.AuthorizationEnabler[] authorizationEnablers) {
        putAuthorizationEnablers(java.util.Arrays.asList(authorizationEnablers));
        return;
    }


    /**
     *  Makes a collection of authorization enablers available in this session.
     *
     *  @param  authorizationEnablers a collection of authorization enablers
     *  @throws org.osid.NullArgumentException <code>authorizationEnablers<code>
     *          is <code>null</code>
     */

    protected void putAuthorizationEnablers(java.util.Collection<? extends org.osid.authorization.rules.AuthorizationEnabler> authorizationEnablers) {
        for (org.osid.authorization.rules.AuthorizationEnabler authorizationEnabler : authorizationEnablers) {
            this.authorizationEnablers.put(authorizationEnabler.getId(), authorizationEnabler);
        }

        return;
    }


    /**
     *  Removes an AuthorizationEnabler from this session.
     *
     *  @param  authorizationEnablerId the <code>Id</code> of the authorization enabler
     *  @throws org.osid.NullArgumentException <code>authorizationEnablerId<code> is
     *          <code>null</code>
     */

    protected void removeAuthorizationEnabler(org.osid.id.Id authorizationEnablerId) {
        this.authorizationEnablers.remove(authorizationEnablerId);
        return;
    }


    /**
     *  Gets the <code>AuthorizationEnabler</code> specified by its <code>Id</code>.
     *
     *  @param  authorizationEnablerId <code>Id</code> of the <code>AuthorizationEnabler</code>
     *  @return the authorizationEnabler
     *  @throws org.osid.NotFoundException <code>authorizationEnablerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>authorizationEnablerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnabler getAuthorizationEnabler(org.osid.id.Id authorizationEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.authorization.rules.AuthorizationEnabler authorizationEnabler = this.authorizationEnablers.get(authorizationEnablerId);
        if (authorizationEnabler == null) {
            throw new org.osid.NotFoundException("authorizationEnabler not found: " + authorizationEnablerId);
        }

        return (authorizationEnabler);
    }


    /**
     *  Gets all <code>AuthorizationEnablers</code>. In plenary mode, the returned
     *  list contains all known authorizationEnablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  authorizationEnablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>AuthorizationEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnablerList getAuthorizationEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.authorization.rules.authorizationenabler.ArrayAuthorizationEnablerList(this.authorizationEnablers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.authorizationEnablers.clear();
        super.close();
        return;
    }
}
