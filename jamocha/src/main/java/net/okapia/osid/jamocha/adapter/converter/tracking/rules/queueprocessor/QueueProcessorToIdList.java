//
// QueueProcessorToIdList.java
//
//     Implements a QueueProcessor IdList. 
//
//
// Tom Coppeto
// Okapia
// 15 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.converter.tracking.rules.queueprocessor;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  Implements an IdList converting the underlying QueueProcessorList to a
 *  list of Ids.
 */

public final class QueueProcessorToIdList
    extends net.okapia.osid.jamocha.adapter.id.id.spi.AbstractAdapterIdList
    implements org.osid.id.IdList {

    private final org.osid.tracking.rules.QueueProcessorList queueProcessorList;


    /**
     *  Creates a new {@code QueueProcessorToIdList}.
     *
     *  @param queueProcessorList a {@code QueueProcessorList}
     *  @throws org.osid.NullArgumentException {@code queueProcessorList}
     *          is {@code null}
     */

    public QueueProcessorToIdList(org.osid.tracking.rules.QueueProcessorList queueProcessorList) {
        super(queueProcessorList);
        this.queueProcessorList = queueProcessorList;
        return;
    }


    /**
     *  Creates a new {@code QueueProcessorToIdList}.
     *
     *  @param queueProcessors a collection of QueueProcessors
     *  @throws org.osid.NullArgumentException {@code queueProcessors}
     *          is {@code null}
     */

    public QueueProcessorToIdList(java.util.Collection<org.osid.tracking.rules.QueueProcessor> queueProcessors) {
        this(new net.okapia.osid.jamocha.tracking.rules.queueprocessor.ArrayQueueProcessorList(queueProcessors)); 
        return;
    }


    /**
     *  Gets the next {@code Id} in this list. 
     *
     *  @return the next {@code Id} in this list. The {@code
     *          hasNext()} method should be used to test that a next
     *          {@code Id} is available before calling this method.
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.id.Id getNextId()
        throws org.osid.OperationFailedException {

        return (this.queueProcessorList.getNextQueueProcessor().getId());
    }


    /**
     *  Closes this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.queueProcessorList.close();
        return;
    }
}
