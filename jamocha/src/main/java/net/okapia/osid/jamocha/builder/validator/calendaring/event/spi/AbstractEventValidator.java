//
// AbstractEventValidator.java
//
//     Validates an Event.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.calendaring.event.spi;


/**
 *  Validates an Event.
 */

public abstract class AbstractEventValidator
    extends net.okapia.osid.jamocha.builder.validator.spi.AbstractTemporalOsidObjectValidator {


    /**
     *  Constructs a new <code>AbstractEventValidator</code>.
     */

    protected AbstractEventValidator() {
        return;
    }


    /**
     *  Constructs a new <code>AbstractEventValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    protected AbstractEventValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates an Event.
     *
     *  @param event an event to validate
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>event</code>
     *          is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in
     *          assembly
     */
    
    public void validate(org.osid.calendaring.Event event) {
        super.validate(event);

        if (event.isInRecurringSeries() || event.isSupersedingEvent() || event.isOffsetEvent()) {
            if (!event.isImplicit()) {
                throw new org.osid.BadLogicException("isImplicit() is false");
            }
        }

        test(event.getLocationDescription(), "getLocationDescription()");

        testConditionalMethod(event, "getLocationId", event.hasLocation(), "hasLocation()");
        testConditionalMethod(event, "getLocation", event.hasLocation(), "hasLocation()");
        if (event.hasLocation()) {
            testNestedObject(event, "getLocation");
        }

        testConditionalMethod(event, "getSponsorIds", event.hasSponsors(), "hasSponsors()");
        testConditionalMethod(event, "getSponsors", event.hasSponsors(), "hasSponsors()");

        if (event.hasSponsors()) {
            testNestedObjects(event, "getSponsorIds", "getSponsors");
        }

        return;
    }
}
