//
// MutableMapProxyPathLookupSession
//
//    Implements a Path lookup service backed by a collection of
//    paths that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping.path;


/**
 *  Implements a Path lookup service backed by a collection of
 *  paths. The paths are indexed only by <code>Id</code>. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of paths can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyPathLookupSession
    extends net.okapia.osid.jamocha.core.mapping.path.spi.AbstractMapPathLookupSession
    implements org.osid.mapping.path.PathLookupSession {


    /**
     *  Constructs a new
     *  <code>MutableMapProxyPathLookupSession</code> with no
     *  paths.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException <code>proxy</code> is
     *          <code>null</code>
     */

    public MutableMapProxyPathLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new <code>MutableMapProxyPathLookupSession</code> with a
     *  single path.
     *
     *  @param  path a path
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException <code>path</code> or
     *          <code>proxy</code> is <code>null</code>
     */

    public MutableMapProxyPathLookupSession(org.osid.mapping.path.Path path, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putPath(path);
        return;
    }


    /**
     *  Constructs a new
     *  <code>MutableMapProxyPathLookupSession</code> using an
     *  array of paths.
     *
     *  @param  paths an array of paths
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException <code>paths</code> or
     *          <code>proxy</code> is <code>null</code>
     */

    public MutableMapProxyPathLookupSession(org.osid.mapping.path.Path[] paths, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putPaths(paths);
        return;
    }


    /**
     *  Constructs a new <code>MutableMapProxyPathLookupSession</code> using
     *  a collection of paths.
     *
     *  @param  paths a collection of paths
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException <code>paths</code> or
     *          <code>proxy</code> is <code>null</code>
     */

    public MutableMapProxyPathLookupSession(java.util.Collection<? extends org.osid.mapping.path.Path> paths,
                                                org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putPaths(paths);
        return;
    }

    
    /**
     *  Makes a <code>Path</code> available in this session.
     *
     *  @param  path an path
     *  @throws org.osid.NullArgumentException <code>path<code>
     *          is <code>null</code>
     */

    @Override
    public void putPath(org.osid.mapping.path.Path path) {
        super.putPath(path);
        return;
    }


    /**
     *  Makes an array of paths available in this session.
     *
     *  @param  paths an array of paths
     *  @throws org.osid.NullArgumentException <code>paths<code>
     *          is <code>null</code>
     */

    @Override
    public void putPaths(org.osid.mapping.path.Path[] paths) {
        super.putPaths(paths);
        return;
    }


    /**
     *  Makes collection of paths available in this session.
     *
     *  @param  paths
     *  @throws org.osid.NullArgumentException <code>path<code>
     *          is <code>null</code>
     */

    @Override
    public void putPaths(java.util.Collection<? extends org.osid.mapping.path.Path> paths) {
        super.putPaths(paths);
        return;
    }


    /**
     *  Removes a Path from this session.
     *
     *  @param pathId the <code>Id</code> of the path
     *  @throws org.osid.NullArgumentException <code>pathId<code> is
     *          <code>null</code>
     */

    @Override
    public void removePath(org.osid.id.Id pathId) {
        super.removePath(pathId);
        return;
    }    
}
