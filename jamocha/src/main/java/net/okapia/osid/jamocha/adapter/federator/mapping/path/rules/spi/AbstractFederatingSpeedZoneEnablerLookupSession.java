//
// AbstractFederatingSpeedZoneEnablerLookupSession.java
//
//     An abstract federating adapter for a SpeedZoneEnablerLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.mapping.path.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  SpeedZoneEnablerLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingSpeedZoneEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.mapping.path.rules.SpeedZoneEnablerLookupSession>
    implements org.osid.mapping.path.rules.SpeedZoneEnablerLookupSession {

    private boolean parallel = false;
    private org.osid.mapping.Map map = new net.okapia.osid.jamocha.nil.mapping.map.UnknownMap();


    /**
     *  Constructs a new <code>AbstractFederatingSpeedZoneEnablerLookupSession</code>.
     */

    protected AbstractFederatingSpeedZoneEnablerLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.mapping.path.rules.SpeedZoneEnablerLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Map/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Map Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getMapId() {
        return (this.map.getId());
    }


    /**
     *  Gets the <code>Map</code> associated with this 
     *  session.
     *
     *  @return the <code>Map</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.Map getMap()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.map);
    }


    /**
     *  Sets the <code>Map</code>.
     *
     *  @param  map the map for this session
     *  @throws org.osid.NullArgumentException <code>map</code>
     *          is <code>null</code>
     */

    protected void setMap(org.osid.mapping.Map map) {
        nullarg(map, "map");
        this.map = map;
        return;
    }


    /**
     *  Tests if this user can perform <code>SpeedZoneEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupSpeedZoneEnablers() {
        for (org.osid.mapping.path.rules.SpeedZoneEnablerLookupSession session : getSessions()) {
            if (session.canLookupSpeedZoneEnablers()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>SpeedZoneEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeSpeedZoneEnablerView() {
        for (org.osid.mapping.path.rules.SpeedZoneEnablerLookupSession session : getSessions()) {
            session.useComparativeSpeedZoneEnablerView();
        }

        return;
    }


    /**
     *  A complete view of the <code>SpeedZoneEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenarySpeedZoneEnablerView() {
        for (org.osid.mapping.path.rules.SpeedZoneEnablerLookupSession session : getSessions()) {
            session.usePlenarySpeedZoneEnablerView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include speed zone enablers in maps which are children
     *  of this map in the map hierarchy.
     */

    @OSID @Override
    public void useFederatedMapView() {
        for (org.osid.mapping.path.rules.SpeedZoneEnablerLookupSession session : getSessions()) {
            session.useFederatedMapView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this map only.
     */

    @OSID @Override
    public void useIsolatedMapView() {
        for (org.osid.mapping.path.rules.SpeedZoneEnablerLookupSession session : getSessions()) {
            session.useIsolatedMapView();
        }

        return;
    }


    /**
     *  Only active speed zone enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveSpeedZoneEnablerView() {
        for (org.osid.mapping.path.rules.SpeedZoneEnablerLookupSession session : getSessions()) {
            session.useActiveSpeedZoneEnablerView();
        }

        return;
    }


    /**
     *  Active and inactive speed zone enablers are returned by
     *  methods in this session.
     */
    
    @OSID @Override
    public void useAnyStatusSpeedZoneEnablerView() {
        for (org.osid.mapping.path.rules.SpeedZoneEnablerLookupSession session : getSessions()) {
            session.useAnyStatusSpeedZoneEnablerView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>SpeedZoneEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>SpeedZoneEnabler</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>SpeedZoneEnabler</code> and
     *  retained for compatibility.
     *
     *  In active mode, speed zone enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  speed zone enablers are returned.
     *
     *  @param  speedZoneEnablerId <code>Id</code> of the
     *          <code>SpeedZoneEnabler</code>
     *  @return the speed zone enabler
     *  @throws org.osid.NotFoundException <code>speedZoneEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>speedZoneEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnabler getSpeedZoneEnabler(org.osid.id.Id speedZoneEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.mapping.path.rules.SpeedZoneEnablerLookupSession session : getSessions()) {
            try {
                return (session.getSpeedZoneEnabler(speedZoneEnablerId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(speedZoneEnablerId + " not found");
    }


    /**
     *  Gets a <code>SpeedZoneEnablerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  speedZoneEnablers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>SpeedZoneEnablers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, speed zone enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  speed zone enablers are returned.
     *
     *  @param  speedZoneEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>SpeedZoneEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>speedZoneEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerList getSpeedZoneEnablersByIds(org.osid.id.IdList speedZoneEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.mapping.path.rules.speedzoneenabler.MutableSpeedZoneEnablerList ret = new net.okapia.osid.jamocha.mapping.path.rules.speedzoneenabler.MutableSpeedZoneEnablerList();

        try (org.osid.id.IdList ids = speedZoneEnablerIds) {
            while (ids.hasNext()) {
                ret.addSpeedZoneEnabler(getSpeedZoneEnabler(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>SpeedZoneEnablerList</code> corresponding to the given
     *  speed zone enabler genus <code>Type</code> which does not include
     *  speed zone enablers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known speed
     *  zone enablers or an error results. Otherwise, the returned
     *  list may contain only those speed zone enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In active mode, speed zone enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  speed zone enablers are returned.
     *
     *  @param  speedZoneEnablerGenusType a speedZoneEnabler genus type 
     *  @return the returned <code>SpeedZoneEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>speedZoneEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerList getSpeedZoneEnablersByGenusType(org.osid.type.Type speedZoneEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.mapping.path.rules.speedzoneenabler.FederatingSpeedZoneEnablerList ret = getSpeedZoneEnablerList();

        for (org.osid.mapping.path.rules.SpeedZoneEnablerLookupSession session : getSessions()) {
            ret.addSpeedZoneEnablerList(session.getSpeedZoneEnablersByGenusType(speedZoneEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>SpeedZoneEnablerList</code> corresponding to the given
     *  speed zone enabler genus <code>Type</code> and include any additional
     *  speed zone enablers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known speed
     *  zone enablers or an error results. Otherwise, the returned
     *  list may contain only those speed zone enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In active mode, speed zone enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  speed zone enablers are returned.
     *
     *  @param  speedZoneEnablerGenusType a speedZoneEnabler genus type 
     *  @return the returned <code>SpeedZoneEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>speedZoneEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerList getSpeedZoneEnablersByParentGenusType(org.osid.type.Type speedZoneEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.mapping.path.rules.speedzoneenabler.FederatingSpeedZoneEnablerList ret = getSpeedZoneEnablerList();

        for (org.osid.mapping.path.rules.SpeedZoneEnablerLookupSession session : getSessions()) {
            ret.addSpeedZoneEnablerList(session.getSpeedZoneEnablersByParentGenusType(speedZoneEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>SpeedZoneEnablerList</code> containing the given
     *  speed zone enabler record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known speed
     *  zone enablers or an error results. Otherwise, the returned
     *  list may contain only those speed zone enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In active mode, speed zone enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  speed zone enablers are returned.
     *
     *  @param  speedZoneEnablerRecordType a speedZoneEnabler record type 
     *  @return the returned <code>SpeedZoneEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>speedZoneEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerList getSpeedZoneEnablersByRecordType(org.osid.type.Type speedZoneEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.mapping.path.rules.speedzoneenabler.FederatingSpeedZoneEnablerList ret = getSpeedZoneEnablerList();

        for (org.osid.mapping.path.rules.SpeedZoneEnablerLookupSession session : getSessions()) {
            ret.addSpeedZoneEnablerList(session.getSpeedZoneEnablersByRecordType(speedZoneEnablerRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>SpeedZoneEnablerList</code> effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *  
     *  In plenary mode, the returned list contains all known speed
     *  zone enablers or an error results. Otherwise, the returned
     *  list may contain only those speed zone enablers that are
     *  accessible through this session.
     *  
     *  In active mode, speed zone enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  speed zone enablers are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>SpeedZoneEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerList getSpeedZoneEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.mapping.path.rules.speedzoneenabler.FederatingSpeedZoneEnablerList ret = getSpeedZoneEnablerList();

        for (org.osid.mapping.path.rules.SpeedZoneEnablerLookupSession session : getSessions()) {
            ret.addSpeedZoneEnablerList(session.getSpeedZoneEnablersOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a <code>SpeedZoneEnablerList </code> which are effective
     *  for the entire given date range inclusive but not confined to
     *  the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all knownq speed
     *  zone enablers or an error results. Otherwise, the returned
     *  list may contain only those speed zone enablers that are
     *  accessible through this session.
     *
     *  In active mode, speed zone enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  speed zone enablers are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned <code>SpeedZoneEnabler</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>agent</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerList getSpeedZoneEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.mapping.path.rules.speedzoneenabler.FederatingSpeedZoneEnablerList ret = getSpeedZoneEnablerList();

        for (org.osid.mapping.path.rules.SpeedZoneEnablerLookupSession session : getSessions()) {
            ret.addSpeedZoneEnablerList(session.getSpeedZoneEnablersOnDateWithAgent(agentId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>SpeedZoneEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known speed
     *  zone enablers or an error results. Otherwise, the returned
     *  list may contain only those speed zone enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In active mode, speed zone enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  speed zone enablers are returned.
     *
     *  @return a list of <code>SpeedZoneEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerList getSpeedZoneEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.mapping.path.rules.speedzoneenabler.FederatingSpeedZoneEnablerList ret = getSpeedZoneEnablerList();

        for (org.osid.mapping.path.rules.SpeedZoneEnablerLookupSession session : getSessions()) {
            ret.addSpeedZoneEnablerList(session.getSpeedZoneEnablers());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.mapping.path.rules.speedzoneenabler.FederatingSpeedZoneEnablerList getSpeedZoneEnablerList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.mapping.path.rules.speedzoneenabler.ParallelSpeedZoneEnablerList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.mapping.path.rules.speedzoneenabler.CompositeSpeedZoneEnablerList());
        }
    }
}
