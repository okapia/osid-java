//
// CompositeQueueConstrainerEnablerList.java
//
//     Implements a QueueConstrainerEnablerList. This list allows 
//     QueueConstrainerEnablerLists to be added in serial. This is not as 
//     efficient as the ParallelQueueConstrainerEnablerList but 
//     preserves the ordering.
//
//
// Tom Coppeto
// Okapia
// 17 October 2008
//
//
// Copyright (c) 2008 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.provisioning.rules.queueconstrainerenabler;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a <code>QueueConstrainerEnablerList</code>. This list allows
 *  QueueConstrainerEnablerLists to be added a bit at a time and are read off in
 *  serial. <code>noMore</code> must be invoked when all the lists
 *  have been added.
 *
 *  Two error handlers are offered. A hard error throws an exception
 *  for any error received by an underlying list. A soft error ignores
 *  errors in underlying lists and proceeds to the next list.
 */

public class CompositeQueueConstrainerEnablerList
    extends net.okapia.osid.jamocha.provisioning.rules.queueconstrainerenabler.spi.AbstractQueueConstrainerEnablerList
    implements org.osid.provisioning.rules.QueueConstrainerEnablerList,
               net.okapia.osid.jamocha.adapter.federator.provisioning.rules.queueconstrainerenabler.FederatingQueueConstrainerEnablerList {

    private final java.util.List<org.osid.provisioning.rules.QueueConstrainerEnablerList> lists = java.util.Collections.synchronizedList(new java.util.LinkedList<org.osid.provisioning.rules.QueueConstrainerEnablerList>());
    private org.osid.provisioning.rules.QueueConstrainerEnablerList active;

    private boolean soft = false;
    private boolean eol = false;


    /**
     *  Creates a new empty <code>CompositeQueueConstrainerEnablerList</code>. The
     *  underlying errors fail hard.
     */

    public CompositeQueueConstrainerEnablerList() {
        return;
    }


    /**
     *  Creates a new empty <code>CompositeQueueConstrainerEnablerList</code>.
     *
     *  @param soft <code>true</code> to fail soft, <code>false</code>
     *         to fail hard
     */

    public CompositeQueueConstrainerEnablerList(boolean soft) {
        this.soft = soft;
        return;
    }


    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in this 
     *          list, <code> false </code> if the end of the list has been 
     *          reached 
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        primeList();
        if (this.active == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the number of elements available for retrieval. The
     *  number returned by this method may be less than or equal to
     *  the total number of elements in this list. To determine if the
     *  end of the list has been reached, the method <code> hasNext()
     *  </code> should be used. This method conveys what is known
     *  about the number of remaining elements at a point in time and
     *  can be used to determine a minimum size of the remaining
     *  elements, if known. A valid return is zero even if <code>
     *  hasNext() </code> is true.
     *  
     *  This method does not imply asynchronous usage. All OSID
     *  methods may block.
     *
     *  @return the number of elements available for retrieval 
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public long available() {
        primeList();
        if (this.active == null) {
            return (0);
        } else {
            return (this.active.available());
        }
    }


    /**
     *  Skip the specified number of elements in the list. If the number 
     *  skipped is greater than the number of elements in the list, hasNext() 
     *  becomes false and available() returns zero as there are no more 
     *  elements to retrieve. 
     *  
     *  @param  n the number of elements to skip 
     *  @throws org.osid.InvalidArgumentException <code>n</code> is less
     *          than zero
     */

    @OSID @Override
    public void skip(long n) {
        long skipped = 0;

        while (skipped < n) {
            primeList();
            if (this.active == null) {
                return;
            }
            
            long s = this.active.available();
            if ((s + skipped) > n) {
                s = n - skipped;
            }
                        
            this.active.skip(s);
            skipped += s;
        }

        return;
    }


    /**
     *  Gets the next <code> QueueConstrainerEnabler </code> in this list.
     *
     *  @return the next <code> QueueConstrainerEnabler </code> in this list. The
     *          <code> hasNext() </code> method should be used to test
     *          that a next <code> QueueConstrainerEnabler </code> is available
     *          before calling this method.
     *  @throws org.osid.IllegalStateException no more elements
     *          available in this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerEnabler getNextQueueConstrainerEnabler()
        throws org.osid.OperationFailedException {

        while (true) {
            primeList();
            if (this.active != null) {
                try {
                    return (this.active.getNextQueueConstrainerEnabler());
                } catch (org.osid.OperationFailedException ofe) {
                    if (!this.soft) {
                        throw ofe;
                    }
                }
            } else {
                throw new org.osid.OperationFailedException("unable to get a next element");
            }
        }
    }

        
    /**
     *  Gets the next set of <code> QueueConstrainerEnabler </code> elements in this
     *  list. The specified amount must be less than or equal to the
     *  return from <code> available(). </code>
     *
     *  @param n the number of <code> QueueConstrainerEnabler </code> elements
     *          requested which must be less than or equal to <code>
     *          available() </code>
     *  @return an array of <code> QueueConstrainerEnabler </code> elements. <code>
     *          </code> The length of the array is less than or equal
     *          to the number specified.
     *  @throws org.osid.IllegalStateException no more elements
     *          available in this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.InvalidArgumentException <code>n</code> is
     *          less than zero
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerEnabler[] getNextQueueConstrainerEnablers(long n)
        throws org.osid.OperationFailedException {
        
        java.util.List<org.osid.provisioning.rules.QueueConstrainerEnabler> ret = new java.util.ArrayList<>((int) n);
        long count = 0;

        while (count < n) {
            primeList();
            if (this.active == null) {
                break;
            }

            try {
                for (org.osid.provisioning.rules.QueueConstrainerEnabler queueConstrainerEnabler : this.active.getNextQueueConstrainerEnablers(this.active.available())) {
                    ret.add(queueConstrainerEnabler);
                    ++count;
                }
            } catch (org.osid.OperationFailedException ofe) {
                if (!this.soft) {
                    throw ofe;
                }
            }
        }

        return (ret.toArray(new org.osid.provisioning.rules.QueueConstrainerEnabler[ret.size()]));      
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {

        for (org.osid.provisioning.rules.QueueConstrainerEnablerList list : this.lists) {
            try {
                list.close();
            } catch (Exception e) {}
        }

        this.active  = null;
        this.eol     = true;
        this.lists.clear();

        return;
    }

        
    /**
     *  Adds a <code>QueueConstrainerEnablerList</code> to this
     *  <code>QueueConstrainerEnablerList</code>.
     *
     *  @param queueConstrainerEnablerList the <code>QueueConstrainerEnablerList</code> to add
     *  @throws org.osid.IllegalStateException this list already closed
     *  @throws org.osid.NullArgumentException <code>queueConstrainerEnablerList</code>
     *          is <code>null</code>
     */

    @Override
    public void addQueueConstrainerEnablerList(org.osid.provisioning.rules.QueueConstrainerEnablerList queueConstrainerEnablerList) {
        nullarg(queueConstrainerEnablerList, "queueconstrainerenabler list");

        synchronized (this.lists) {
            this.lists.add(queueConstrainerEnablerList);
            this.lists.notifyAll();
        }

        return;
    }


    /**
     *  Adds a collection of <code>QueueConstrainerEnablerLists</code> to this
     *  <code>QueueConstrainerEnablerList</code>.
     *
     *  @param queueConstrainerEnablerLists the <code>QueueConstrainerEnablerList</code> collection
     *         to add
     *  @throws org.osid.IllegalStateException this list already closed
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerEnablerLists</code> is <code>null</code>
     */

    @Override
    public void addQueueConstrainerEnablerLists(java.util.Collection<org.osid.provisioning.rules.QueueConstrainerEnablerList> queueConstrainerEnablerLists) {
        nullarg(queueConstrainerEnablerLists, "queueconstrainerenabler lists");

        for (org.osid.provisioning.rules.QueueConstrainerEnablerList list : queueConstrainerEnablerLists) {
            addQueueConstrainerEnablerList(list);
        }

        return;
    }


    /**
     *  No more. Invoked when no more lists are to be added.
     */

    @Override
    public void noMore() {
        this.eol = true;
        synchronized (this.lists) {
            this.lists.notifyAll();
        }

        return;
    }


    /**
     *  Tests if the list is operational. A list is operational if
     *  <code>close()</code> has not been invoked.
     *
     *  @return <code>true</code> if the list is operational,
     *          <code>false</code> otherwise.
     */

    @Override
    public boolean isOperational() {
        if ((this.lists.size() == 0) && this.eol) {
            return (false);
        }

        return (true);
    }


    /**
     *  Tests if the list is empty.
     *
     *  @return <code>true</code> if the list is empty
     *          retrieved, <code>false</code> otherwise.
     */

    public boolean isEmpty() {
        if (this.lists.size() == 0) {
            return (true);
        }

        return (false);
    }


    private void primeList() {
        while (true) {
            if (this.active == null) {
                if ((this.lists.size() == 0) && (this.eol)) {
                    return;
                }
                
                this.active = this.lists.get(0);
            }
            
            if (this.active.hasNext()) {
                return;
            }
            
            this.lists.remove(0);
            this.active = null;
            
            synchronized (this.lists) {                 
                if (!this.eol && (this.lists.size() == 0)) {
                    try {
                        this.lists.wait();
                    } catch (InterruptedException ie) { }
                }
            }
        }
    }
}
