//
// AbstractGradeSystem.java
//
//     Defines a GradeSystem.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.gradesystem.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>GradeSystem</code>.
 */

public abstract class AbstractGradeSystem
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.grading.GradeSystem {

    private boolean basedOnGrades = false;

    private final java.util.Collection<org.osid.grading.Grade> grades = new java.util.LinkedHashSet<>();
    private java.math.BigDecimal lowestNumericScore;
    private java.math.BigDecimal numericScoreIncrement;
    private java.math.BigDecimal highestNumericScore;

    private final java.util.Collection<org.osid.grading.records.GradeSystemRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Tests if the grading system is based on grades. 
     *
     *  @return true if the grading system is based on grades, <code>
     *          false </code> if the system is a numeric score
     */

    @OSID @Override
    public boolean isBasedOnGrades() {
        return (this.basedOnGrades);
    }


    /**
     *  Gets the grade <code> Ids </code> in this system ranked from highest 
     *  to lowest. 
     *
     *  @return the list of grades <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> isBasedOnGrades() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getGradeIds() {
        if (!isBasedOnGrades()) {
            throw new org.osid.IllegalStateException("isBasedOnGrades() is false");
        }

        try {
            org.osid.grading.GradeList grades = getGrades();
            return (new net.okapia.osid.jamocha.adapter.converter.grading.grade.GradeToIdList(grades));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the grades in this system ranked from highest to lowest. 
     *
     *  @return the list of grades 
     *  @throws org.osid.IllegalStateException <code> isBasedOnGrades() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradeList getGrades()
        throws org.osid.OperationFailedException {

        if (!isBasedOnGrades()) {
            throw new org.osid.IllegalStateException("isBasedOnGrades() is false");
        }

        return (new net.okapia.osid.jamocha.grading.grade.ArrayGradeList(this.grades));
    }


    /**
     *  Adds a grade.
     *
     *  @param grade a grade
     *  @throws org.osid.NullArgumentException
     *          <code>grade</code> is <code>null</code>
     */

    protected void addGrade(org.osid.grading.Grade grade) {
        nullarg(grade, "grade");

        this.grades.add(grade);
        this.basedOnGrades = true;

        return;
    }


    /**
     *  Sets all the grades.
     *
     *  @param grades a collection of grades
     *  @throws org.osid.NullArgumentException
     *          <code>grades</code> is <code>null</code>
     */

    protected void setGrades(java.util.Collection<org.osid.grading.Grade> grades) {
        nullarg(grades, "grades");

        this.grades.clear();
        this.grades.addAll(grades);
        this.basedOnGrades = true;

        return;
    }


    /**
     *  Gets the lowest number in a numeric grading system. 
     *
     *  @return the lowest number 
     *  @throws org.osid.IllegalStateException <code> isBasedOnGrades() 
     *          </code> is <code> true </code> 
     */

    @OSID @Override
    public java.math.BigDecimal getLowestNumericScore() {
        if (isBasedOnGrades()) {
            throw new org.osid.IllegalStateException("isBasedOnGrades() is true");
        }

        return (this.lowestNumericScore);
    }


    /**
     *  Sets the lowest numeric score.
     *
     *  @param score a lowest numeric score
     *  @throws org.osid.NullArgumentException <code>score</code> is
     *          <code>null</code>
     */

    protected void setLowestNumericScore(java.math.BigDecimal score) {
        nullarg(score, "lowest numeric score");

        this.lowestNumericScore = score;
        this.basedOnGrades = false;

        return;
    }


    /**
     *  Gets the incremental step. 
     *
     *  @return the increment 
     *  @throws org.osid.IllegalStateException <code> isBasedOnGrades() 
     *          </code> is <code> true </code> 
     */

    @OSID @Override
    public java.math.BigDecimal getNumericScoreIncrement() {
        if (isBasedOnGrades()) {
            throw new org.osid.IllegalStateException("isBasedOnGrades() is true");
        }

        return (this.numericScoreIncrement);
    }


    /**
     *  Sets the numeric score increment.
     *
     *  @param increment a numeric score increment
     *  @throws org.osid.NullArgumentException <code>increment</code>
     *          is <code>null</code>
     */

    protected void setNumericScoreIncrement(java.math.BigDecimal increment) {
        nullarg(increment, "lowest numeric score increment");

        this.numericScoreIncrement = increment;
        this.basedOnGrades = false;

        return;
    }


    /**
     *  Gets the highest number in a numeric grading system. 
     *
     *  @return the highest number 
     *  @throws org.osid.IllegalStateException <code> isBasedOnGrades() 
     *          </code> is <code> true </code> 
     */

    @OSID @Override
    public java.math.BigDecimal getHighestNumericScore() {
        if (isBasedOnGrades()) {
            throw new org.osid.IllegalStateException("isBasedOnGrades() is true");
        }

        return (this.highestNumericScore);
    }


    /**
     *  Sets the highest numeric score.
     *
     *  @param score a highest numeric score
     *  @throws org.osid.NullArgumentException <code>score</code> is
     *          <code>null</code>
     */

    protected void setHighestNumericScore(java.math.BigDecimal score) {
        nullarg(score, "highest numeric score");

        this.highestNumericScore = score;
        this.basedOnGrades = false;

        return;
    }


    /**
     *  Tests if this gradeSystem supports the given record
     *  <code>Type</code>.
     *
     *  @param  gradeSystemRecordType a grade system record type 
     *  @return <code>true</code> if the gradeSystemRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>gradeSystemRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type gradeSystemRecordType) {
        for (org.osid.grading.records.GradeSystemRecord record : this.records) {
            if (record.implementsRecordType(gradeSystemRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>GradeSystem</code> record <code>Type</code>.
     *
     *  @param  gradeSystemRecordType the grade system record type 
     *  @return the grade system record 
     *  @throws org.osid.NullArgumentException
     *          <code>gradeSystemRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(gradeSystemRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.grading.records.GradeSystemRecord getGradeSystemRecord(org.osid.type.Type gradeSystemRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.grading.records.GradeSystemRecord record : this.records) {
            if (record.implementsRecordType(gradeSystemRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(gradeSystemRecordType + " is not supported");
    }


    /**
     *  Adds a record to this grade system. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param gradeSystemRecord the grade system record
     *  @param gradeSystemRecordType grade system record type
     *  @throws org.osid.NullArgumentException
     *          <code>gradeSystemRecord</code> or
     *          <code>gradeSystemRecordTypegradeSystem</code> is
     *          <code>null</code>
     */
            
    protected void addGradeSystemRecord(org.osid.grading.records.GradeSystemRecord gradeSystemRecord, 
                                        org.osid.type.Type gradeSystemRecordType) {
        
        nullarg(gradeSystemRecord, "grade system record");
        addRecordType(gradeSystemRecordType);
        this.records.add(gradeSystemRecord);
        
        return;
    }
}
