//
// AbstractAssemblyEventQuery.java
//
//     An EventQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.calendaring.event.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An EventQuery that stores terms.
 */

public abstract class AbstractAssemblyEventQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyTemporalOsidObjectQuery
    implements org.osid.calendaring.EventQuery,
               org.osid.calendaring.EventQueryInspector,
               org.osid.calendaring.EventSearchOrder {

    private final java.util.Collection<org.osid.calendaring.records.EventQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.calendaring.records.EventQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.calendaring.records.EventSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();

    private final AssemblyOsidContainableQuery query;


    /** 
     *  Constructs a new <code>AbstractAssemblyEventQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyEventQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        this.query = new AssemblyOsidContainableQuery(assembler);
        return;
    }
    

    /**
     *  Match containables that are sequestered. 
     *
     *  @param  match <code> true </code> to match any sequestered 
     *          containables, <code> false </code> to match non-sequestered 
     *          containables 
     */

    @OSID @Override
    public void matchSequestered(boolean match) {
        this.query.matchSequestered(match);
        return;
    }


    /**
     *  Clears the sequestered query terms. 
     */

    @OSID @Override
    public void clearSequesteredTerms() {
        this.query.clearSequesteredTerms();
        return;
    }

    
    /**
     *  Gets the sequestered query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getSequesteredTerms() {
        return (this.query.getSequesteredTerms());
    }

    
    /**
     *  Specifies a preference for ordering the result set by the sequestered 
     *  flag. 
     *
     *  @param  style the search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySequestered(org.osid.SearchOrderStyle style) {
        this.query.orderBySequestered(style);
        return;
    }


    /**
     *  Gets the column name for the sequestered field.
     *
     *  @return the column name
     */

    protected String getSequesteredColumn() {
        return (this.query.getSequesteredColumn());
    }


    /**
     *  Matches an event that is implicitly generated. 
     *
     *  @param  match <code> true </code> to match events implicitly 
     *          generated, <code> false </code> to match events explicitly 
     *          defined 
     */

    @OSID @Override
    public void matchImplicit(boolean match) {
        getAssembler().addBooleanTerm(getImplicitColumn(), match);
        return;
    }


    /**
     *  Clears the implcit terms. 
     */

    @OSID @Override
    public void clearImplicitTerms() {
        getAssembler().clearTerms(getImplicitColumn());
        return;
    }


    /**
     *  Gets the implicit terms. 
     *
     *  @return the implicit terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getImplicitTerms() {
        return (getAssembler().getBooleanTerms(getImplicitColumn()));
    }


    /**
     *  Gets the Implicit column name.
     *
     * @return the column name
     */

    protected String getImplicitColumn() {
        return ("implicit");
    }


    /**
     *  Matches the event duration between the given range inclusive. 
     *
     *  @param  low low duration range 
     *  @param  high high duration range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     *  @throws org.osid.NullArgumentException <code> high </code> or <code> 
     *          low </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchDuration(org.osid.calendaring.Duration low, 
                              org.osid.calendaring.Duration high, 
                              boolean match) {
        getAssembler().addDurationRangeTerm(getDurationColumn(), low, high, match);
        return;
    }


    /**
     *  Matches an event that has any duration. 
     *
     *  @param  match <code> true </code> to match events with any duration, 
     *          <code> false </code> to match events with no start time 
     */

    @OSID @Override
    public void matchAnyDuration(boolean match) {
        getAssembler().addDurationRangeWildcardTerm(getDurationColumn(), match);
        return;
    }


    /**
     *  Clears the duration terms. 
     */

    @OSID @Override
    public void clearDurationTerms() {
        getAssembler().clearTerms(getDurationColumn());
        return;
    }


    /**
     *  Gets the duration terms. 
     *
     *  @return the duration terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationTerm[] getDurationTerms() {
        return (getAssembler().getDurationTerms(getDurationColumn()));
    }


    /**
     *  Specified a preference for ordering results by the duration. For 
     *  recurring events, the duration is the duration of a single event. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDuration(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDurationColumn(), style);
        return;
    }


    /**
     *  Gets the Duration column name.
     *
     * @return the column name
     */

    protected String getDurationColumn() {
        return ("duration");
    }


    /**
     *  Matches events that related to the recurring event. 
     *
     *  @param  recurringEventId an <code> Id </code> for a recurring event 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> recurringEventId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchRecurringEventId(org.osid.id.Id recurringEventId, 
                                      boolean match) {
        getAssembler().addIdTerm(getRecurringEventIdColumn(), recurringEventId, match);
        return;
    }


    /**
     *  Clears the recurring event terms. 
     */

    @OSID @Override
    public void clearRecurringEventIdTerms() {
        getAssembler().clearTerms(getRecurringEventIdColumn());
        return;
    }


    /**
     *  Gets the recurring event <code> Id </code> terms. 
     *
     *  @return the recurring event <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRecurringEventIdTerms() {
        return (getAssembler().getIdTerms(getRecurringEventIdColumn()));
    }


    /**
     *  Specified a preference for ordering results by the recurring event.. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRecurringEvent(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getRecurringEventColumn(), style);
        return;
    }


    /**
     *  Gets the RecurringEventId column name.
     *
     * @return the column name
     */

    protected String getRecurringEventIdColumn() {
        return ("recurring_event_id");
    }


    /**
     *  Tests if a <code> RecurringEventQuery </code> is available for 
     *  querying recurring events. 
     *
     *  @return <code> true </code> if a recurring event query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecurringEventQuery() {
        return (false);
    }


    /**
     *  Gets the query for a recurring event. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the recurring event query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventQuery getRecurringEventQuery() {
        throw new org.osid.UnimplementedException("supportsRecurringEventQuery() is false");
    }


    /**
     *  Matches an event that is part of any recurring event. 
     *
     *  @param  match <code> true </code> to match events part of any 
     *          recurring event, <code> false </code> to match only standalone 
     *          events 
     */

    @OSID @Override
    public void matchAnyRecurringEvent(boolean match) {
        getAssembler().addIdWildcardTerm(getRecurringEventColumn(), match);
        return;
    }


    /**
     *  Clears the recurring event terms. 
     */

    @OSID @Override
    public void clearRecurringEventTerms() {
        getAssembler().clearTerms(getRecurringEventColumn());
        return;
    }


    /**
     *  Gets the recurring event terms. 
     *
     *  @return the recurring event terms 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventQueryInspector[] getRecurringEventTerms() {
        return (new org.osid.calendaring.RecurringEventQueryInspector[0]);
    }


    /**
     *  Tests if a <code> RecurringEventSearchOrder </code> is available for 
     *  recurring events. 
     *
     *  @return <code> true </code> if a recurring event search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecurringEventSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a recurring event. 
     *
     *  @return the recurring event search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventSearchOrder getRecurringEventSearchOrder() {
        throw new org.osid.UnimplementedException("supportsRecurringEventSearchOrder() is false");
    }


    /**
     *  Gets the RecurringEvent column name.
     *
     * @return the column name
     */

    protected String getRecurringEventColumn() {
        return ("recurring_event");
    }


    /**
     *  Matches events that relate to the superseding event. 
     *
     *  @param  supersedingEventId an <code> Id </code> for a superseding 
     *          event 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchSupersedingEventId(org.osid.id.Id supersedingEventId, 
                                        boolean match) {
        getAssembler().addIdTerm(getSupersedingEventIdColumn(), supersedingEventId, match);
        return;
    }


    /**
     *  Clears the superseding events type terms. 
     */

    @OSID @Override
    public void clearSupersedingEventIdTerms() {
        getAssembler().clearTerms(getSupersedingEventIdColumn());
        return;
    }


    /**
     *  Gets the superseding event <code> Id </code> terms. 
     *
     *  @return the superseding event <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSupersedingEventIdTerms() {
        return (getAssembler().getIdTerms(getSupersedingEventIdColumn()));
    }


    /**
     *  Specified a preference for ordering results by the superseding event.. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySupersedingEvent(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getSupersedingEventColumn(), style);
        return;
    }


    /**
     *  Gets the SupersedingEventId column name.
     *
     * @return the column name
     */

    protected String getSupersedingEventIdColumn() {
        return ("superseding_event_id");
    }


    /**
     *  Tests if a <code> SupersedingEventQuery </code> is available for 
     *  querying offset events. 
     *
     *  @return <code> true </code> if a superseding event query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSupersedingEventQuery() {
        return (false);
    }


    /**
     *  Gets the query for a superseding event. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the superseding event query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventQuery getSupersedingEventQuery() {
        throw new org.osid.UnimplementedException("supportsSupersedingEventQuery() is false");
    }


    /**
     *  Matches any superseding event. 
     *
     *  @param  match <code> true </code> to match any superseding events, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public void matchAnySupersedingEvent(boolean match) {
        getAssembler().addIdWildcardTerm(getSupersedingEventColumn(), match);
        return;
    }


    /**
     *  Clears the superseding event terms. 
     */

    @OSID @Override
    public void clearSupersedingEventTerms() {
        getAssembler().clearTerms(getSupersedingEventColumn());
        return;
    }


    /**
     *  Gets the superseding event terms. 
     *
     *  @return the superseding event terms 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventQueryInspector[] getSupersedingEventTerms() {
        return (new org.osid.calendaring.SupersedingEventQueryInspector[0]);
    }


    /**
     *  Tests if a <code> SupersedingEventSearchOrder </code> is available for 
     *  offset events. 
     *
     *  @return <code> true </code> if a superseding event search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSupersedingEventSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a superseding event. 
     *
     *  @return the superseding event search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventSearchOrder getSupersedingEventSearchOrder() {
        throw new org.osid.UnimplementedException("supportsSupersedingEventSearchOrder() is false");
    }


    /**
     *  Gets the SupersedingEvent column name.
     *
     * @return the column name
     */

    protected String getSupersedingEventColumn() {
        return ("superseding_event");
    }


    /**
     *  Matches events that relates to the offset event. 
     *
     *  @param  offsetEventId an <code> Id </code> for an offset event 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchOffsetEventId(org.osid.id.Id offsetEventId, boolean match) {
        getAssembler().addIdTerm(getOffsetEventIdColumn(), offsetEventId, match);
        return;
    }


    /**
     *  Clears the recurring events type terms. 
     */

    @OSID @Override
    public void clearOffsetEventIdTerms() {
        getAssembler().clearTerms(getOffsetEventIdColumn());
        return;
    }


    /**
     *  Gets the offset event <code> Id </code> terms. 
     *
     *  @return the offset event <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOffsetEventIdTerms() {
        return (getAssembler().getIdTerms(getOffsetEventIdColumn()));
    }


    /**
     *  Specified a preference for ordering results by the offset event.. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByOffsetEvent(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getOffsetEventColumn(), style);
        return;
    }


    /**
     *  Gets the OffsetEventId column name.
     *
     * @return the column name
     */

    protected String getOffsetEventIdColumn() {
        return ("offset_event_id");
    }


    /**
     *  Tests if an <code> OffsetEventQuery </code> is available for querying 
     *  offset events. 
     *
     *  @return <code> true </code> if an offset event query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOffsetEventQuery() {
        return (false);
    }


    /**
     *  Gets the query for an offset event. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the offset event query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventQuery getOffsetEventQuery() {
        throw new org.osid.UnimplementedException("supportsOffsetEventQuery() is false");
    }


    /**
     *  Matches any offset event. 
     *
     *  @param  match <code> true </code> to match any offset events, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public void matchAnyOffsetEvent(boolean match) {
        getAssembler().addIdWildcardTerm(getOffsetEventColumn(), match);
        return;
    }


    /**
     *  Clears the offset event terms. 
     */

    @OSID @Override
    public void clearOffsetEventTerms() {
        getAssembler().clearTerms(getOffsetEventColumn());
        return;
    }


    /**
     *  Gets the offset event terms. 
     *
     *  @return the offset event terms 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventQueryInspector[] getOffsetEventTerms() {
        return (new org.osid.calendaring.OffsetEventQueryInspector[0]);
    }


    /**
     *  Tests if an <code> OffsetEventSearchOrder </code> is available for 
     *  offset events. 
     *
     *  @return <code> true </code> if an offset event search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOffsetEventSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for an offset event. 
     *
     *  @return the offset event search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventSearchOrder getOffsetEventSearchOrder() {
        throw new org.osid.UnimplementedException("supportsOffsetEventSearchOrder() is false");
    }


    /**
     *  Gets the OffsetEvent column name.
     *
     * @return the column name
     */

    protected String getOffsetEventColumn() {
        return ("offset_event");
    }


    /**
     *  Matches the location description string. 
     *
     *  @param  location location string 
     *  @param  stringMatchType string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> location </code> is 
     *          not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> location </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchLocationDescription(String location, 
                                         org.osid.type.Type stringMatchType, 
                                         boolean match) {
        getAssembler().addStringTerm(getLocationDescriptionColumn(), location, stringMatchType, match);
        return;
    }


    /**
     *  Matches an event that has any location description assigned. 
     *
     *  @param  match <code> true </code> to match events with any location 
     *          description, <code> false </code> to match events with no 
     *          location description 
     */

    @OSID @Override
    public void matchAnyLocationDescription(boolean match) {
        getAssembler().addStringWildcardTerm(getLocationDescriptionColumn(), match);
        return;
    }


    /**
     *  Clears the location description terms. 
     */

    @OSID @Override
    public void clearLocationDescriptionTerms() {
        getAssembler().clearTerms(getLocationDescriptionColumn());
        return;
    }


    /**
     *  Gets the location description terms. 
     *
     *  @return the location description terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getLocationDescriptionTerms() {
        return (getAssembler().getStringTerms(getLocationDescriptionColumn()));
    }


    /**
     *  Specified a preference for ordering results by the location 
     *  description. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByLocationDescription(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getLocationDescriptionColumn(), style);
        return;
    }


    /**
     *  Gets the LocationDescription column name.
     *
     * @return the column name
     */

    protected String getLocationDescriptionColumn() {
        return ("location_description");
    }


    /**
     *  Sets the location <code> Id </code> for this query. 
     *
     *  @param  locationId a location <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> locationId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchLocationId(org.osid.id.Id locationId, boolean match) {
        getAssembler().addIdTerm(getLocationIdColumn(), locationId, match);
        return;
    }


    /**
     *  Clears the location <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearLocationIdTerms() {
        getAssembler().clearTerms(getLocationIdColumn());
        return;
    }


    /**
     *  Gets the location <code> Id </code> terms. 
     *
     *  @return the location <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getLocationIdTerms() {
        return (getAssembler().getIdTerms(getLocationIdColumn()));
    }


    /**
     *  Specified a preference for ordering results by the location. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByLocation(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getLocationColumn(), style);
        return;
    }


    /**
     *  Gets the LocationId column name.
     *
     * @return the column name
     */

    protected String getLocationIdColumn() {
        return ("location_id");
    }


    /**
     *  Tests if a <code> LocationQuery </code> is available for querying 
     *  locations. 
     *
     *  @return <code> true </code> if a location query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocationQuery() {
        return (false);
    }


    /**
     *  Gets the query for a location. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the location query 
     *  @throws org.osid.UnimplementedException <code> supportsLocationQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationQuery getLocationQuery() {
        throw new org.osid.UnimplementedException("supportsLocationQuery() is false");
    }


    /**
     *  Matches an event that has any location assigned. 
     *
     *  @param  match <code> true </code> to match events with any location, 
     *          <code> false </code> to match events with no location 
     */

    @OSID @Override
    public void matchAnyLocation(boolean match) {
        getAssembler().addIdWildcardTerm(getLocationColumn(), match);
        return;
    }


    /**
     *  Clears the location terms. 
     */

    @OSID @Override
    public void clearLocationTerms() {
        getAssembler().clearTerms(getLocationColumn());
        return;
    }


    /**
     *  Gets the location terms. 
     *
     *  @return the location terms 
     */

    @OSID @Override
    public org.osid.mapping.LocationQueryInspector[] getLocationTerms() {
        return (new org.osid.mapping.LocationQueryInspector[0]);
    }


    /**
     *  Tests if a <code> LocationSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if a location search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocationSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a location. 
     *
     *  @return the location search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationSearchOrder getLocationSearchOrder() {
        throw new org.osid.UnimplementedException("supportsLocationSearchOrder() is false");
    }


    /**
     *  Gets the Location column name.
     *
     * @return the column name
     */

    protected String getLocationColumn() {
        return ("location");
    }


    /**
     *  Sets the sponsor <code> Id </code> for this query. 
     *
     *  @param  sponsorId a sponsor <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> sponsorId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSponsorId(org.osid.id.Id sponsorId, boolean match) {
        getAssembler().addIdTerm(getSponsorIdColumn(), sponsorId, match);
        return;
    }


    /**
     *  Clears the sponsor <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSponsorIdTerms() {
        getAssembler().clearTerms(getSponsorIdColumn());
        return;
    }


    /**
     *  Gets the sponsor <code> Id </code> terms. 
     *
     *  @return the sponsor <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSponsorIdTerms() {
        return (getAssembler().getIdTerms(getSponsorIdColumn()));
    }


    /**
     *  Gets the SponsorId column name.
     *
     * @return the column name
     */

    protected String getSponsorIdColumn() {
        return ("sponsor_id");
    }


    /**
     *  Tests if a <code> LocationQuery </code> is available for querying 
     *  sponsors. 
     *
     *  @return <code> true </code> if a sponsor query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSponsorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a sponsor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the sponsor query 
     *  @throws org.osid.UnimplementedException <code> supportsSponsorQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getSponsorQuery() {
        throw new org.osid.UnimplementedException("supportsSponsorQuery() is false");
    }


    /**
     *  Clears the sponsor terms. 
     */

    @OSID @Override
    public void clearSponsorTerms() {
        getAssembler().clearTerms(getSponsorColumn());
        return;
    }


    /**
     *  Gets the sponsor terms. 
     *
     *  @return the sponsor terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getSponsorTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the Sponsor column name.
     *
     * @return the column name
     */

    protected String getSponsorColumn() {
        return ("sponsor");
    }


    /**
     *  Matches events whose locations contain the given coordinate. 
     *
     *  @param  coordinate a coordinate 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> coordinate </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCoordinate(org.osid.mapping.Coordinate coordinate, 
                                boolean match) {
        getAssembler().addCoordinateTerm(getCoordinateColumn(), coordinate, match);
        return;
    }


    /**
     *  Clears the cooordinate terms. 
     */

    @OSID @Override
    public void clearCoordinateTerms() {
        getAssembler().clearTerms(getCoordinateColumn());
        return;
    }


    /**
     *  Gets the coordinate terms. 
     *
     *  @return the coordinate terms 
     */

    @OSID @Override
    public org.osid.search.terms.CoordinateTerm[] getCoordinateTerms() {
        return (getAssembler().getCoordinateTerms(getCoordinateColumn()));
    }


    /**
     *  Gets the Coordinate column name.
     *
     * @return the column name
     */

    protected String getCoordinateColumn() {
        return ("coordinate");
    }


    /**
     *  Matches events whose locations fall within the given spatial unit. 
     *
     *  @param  spatialUnit a spatial unit 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> spatialUnit </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSpatialUnit(org.osid.mapping.SpatialUnit spatialUnit, 
                                 boolean match) {
        getAssembler().addSpatialUnitTerm(getSpatialUnitColumn(), spatialUnit, match);
        return;
    }


    /**
     *  Clears the spatial unit terms. 
     */

    @OSID @Override
    public void clearSpatialUnitTerms() {
        getAssembler().clearTerms(getSpatialUnitColumn());
        return;
    }


    /**
     *  Gets the spatial unit terms. 
     *
     *  @return the spatial unit terms 
     */

    @OSID @Override
    public org.osid.search.terms.SpatialUnitTerm[] getSpatialUnitTerms() {
        return (getAssembler().getSpatialUnitTerms(getSpatialUnitColumn()));
    }


    /**
     *  Gets the SpatialUnit column name.
     *
     * @return the column name
     */

    protected String getSpatialUnitColumn() {
        return ("spatial_unit");
    }


    /**
     *  Sets the commitment <code> Id </code> for this query. 
     *
     *  @param  commitmentId a commitment <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> commitmentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCommitmentId(org.osid.id.Id commitmentId, boolean match) {
        getAssembler().addIdTerm(getCommitmentIdColumn(), commitmentId, match);
        return;
    }


    /**
     *  Clears the commitment <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCommitmentIdTerms() {
        getAssembler().clearTerms(getCommitmentIdColumn());
        return;
    }


    /**
     *  Gets the commitment <code> Id </code> terms. 
     *
     *  @return the commitment <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCommitmentIdTerms() {
        return (getAssembler().getIdTerms(getCommitmentIdColumn()));
    }


    /**
     *  Gets the CommitmentId column name.
     *
     * @return the column name
     */

    protected String getCommitmentIdColumn() {
        return ("commitment_id");
    }


    /**
     *  Tests if a <code> CommitmentQuery </code> is available for querying 
     *  recurring event terms. 
     *
     *  @return <code> true </code> if a commitment query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommitmentQuery() {
        return (false);
    }


    /**
     *  Gets the query for a commitment. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the commitment query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentQuery getCommitmentQuery() {
        throw new org.osid.UnimplementedException("supportsCommitmentQuery() is false");
    }


    /**
     *  Matches an event that has any commitment. 
     *
     *  @param  match <code> true </code> to match events with any commitment, 
     *          <code> false </code> to match events with no commitments 
     */

    @OSID @Override
    public void matchAnyCommitment(boolean match) {
        getAssembler().addIdWildcardTerm(getCommitmentColumn(), match);
        return;
    }


    /**
     *  Clears the commitment terms. 
     */

    @OSID @Override
    public void clearCommitmentTerms() {
        getAssembler().clearTerms(getCommitmentColumn());
        return;
    }


    /**
     *  Gets the commitment terms. 
     *
     *  @return the commitment terms 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentQueryInspector[] getCommitmentTerms() {
        return (new org.osid.calendaring.CommitmentQueryInspector[0]);
    }


    /**
     *  Gets the Commitment column name.
     *
     * @return the column name
     */

    protected String getCommitmentColumn() {
        return ("commitment");
    }


    /**
     *  Sets the event <code> Id </code> for this query to match events that 
     *  have the specified event as an ancestor. 
     *
     *  @param  eventId an event <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> eventId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchContainingEventId(org.osid.id.Id eventId, boolean match) {
        getAssembler().addIdTerm(getContainingEventIdColumn(), eventId, match);
        return;
    }


    /**
     *  Clears the containing event <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearContainingEventIdTerms() {
        getAssembler().clearTerms(getContainingEventIdColumn());
        return;
    }


    /**
     *  Gets the containing event <code> Id </code> terms. 
     *
     *  @return the event <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getContainingEventIdTerms() {
        return (getAssembler().getIdTerms(getContainingEventIdColumn()));
    }


    /**
     *  Gets the ContainingEventId column name.
     *
     * @return the column name
     */

    protected String getContainingEventIdColumn() {
        return ("containing_event_id");
    }


    /**
     *  Tests if a containing event query is available. 
     *
     *  @return <code> true </code> if a containing event query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContainingEventQuery() {
        return (false);
    }


    /**
     *  Gets the query for a containing event. 
     *
     *  @return the containing event query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContainingEventQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventQuery getContainingEventQuery() {
        throw new org.osid.UnimplementedException("supportsContainingEventQuery() is false");
    }


    /**
     *  Matches events with any ancestor event. 
     *
     *  @param  match <code> true </code> to match events with any ancestor 
     *          event, <code> false </code> to match events with no ancestor 
     *          events 
     */

    @OSID @Override
    public void matchAnyContainingEvent(boolean match) {
        getAssembler().addIdWildcardTerm(getContainingEventColumn(), match);
        return;
    }


    /**
     *  Clears the containing event terms. 
     */

    @OSID @Override
    public void clearContainingEventTerms() {
        getAssembler().clearTerms(getContainingEventColumn());
        return;
    }


    /**
     *  Gets the containing event terms. 
     *
     *  @return the event terms 
     */

    @OSID @Override
    public org.osid.calendaring.EventQueryInspector[] getContainingEventTerms() {
        return (new org.osid.calendaring.EventQueryInspector[0]);
    }


    /**
     *  Gets the ContainingEvent column name.
     *
     * @return the column name
     */

    protected String getContainingEventColumn() {
        return ("containing_event");
    }


    /**
     *  Sets the calendar <code> Id </code> for this query. 
     *
     *  @param  calendarId a calendar <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCalendarId(org.osid.id.Id calendarId, boolean match) {
        getAssembler().addIdTerm(getCalendarIdColumn(), calendarId, match);
        return;
    }


    /**
     *  Clears the calendar <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCalendarIdTerms() {
        getAssembler().clearTerms(getCalendarIdColumn());
        return;
    }


    /**
     *  Gets the calendar <code> Id </code> terms. 
     *
     *  @return the calendar <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCalendarIdTerms() {
        return (getAssembler().getIdTerms(getCalendarIdColumn()));
    }


    /**
     *  Gets the CalendarId column name.
     *
     * @return the column name
     */

    protected String getCalendarIdColumn() {
        return ("calendar_id");
    }


    /**
     *  Tests if a <code> CalendarQuery </code> is available for querying 
     *  calendars. 
     *
     *  @return <code> true </code> if a calendar query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendarQuery() {
        return (false);
    }


    /**
     *  Gets the query for a calendar. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the calendar query 
     *  @throws org.osid.UnimplementedException <code> supportsCalendarQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQuery getCalendarQuery() {
        throw new org.osid.UnimplementedException("supportsCalendarQuery() is false");
    }


    /**
     *  Clears the calendar terms. 
     */

    @OSID @Override
    public void clearCalendarTerms() {
        getAssembler().clearTerms(getCalendarColumn());
        return;
    }


    /**
     *  Gets the calendar terms. 
     *
     *  @return the calendar terms 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQueryInspector[] getCalendarTerms() {
        return (new org.osid.calendaring.CalendarQueryInspector[0]);
    }


    /**
     *  Gets the Calendar column name.
     *
     * @return the column name
     */

    protected String getCalendarColumn() {
        return ("calendar");
    }


    /**
     *  Tests if this event supports the given record
     *  <code>Type</code>.
     *
     *  @param  eventRecordType an event record type 
     *  @return <code>true</code> if the eventRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>eventRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type eventRecordType) {
        for (org.osid.calendaring.records.EventQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(eventRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  eventRecordType the event record type 
     *  @return the event query record 
     *  @throws org.osid.NullArgumentException
     *          <code>eventRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(eventRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.EventQueryRecord getEventQueryRecord(org.osid.type.Type eventRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.EventQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(eventRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(eventRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  eventRecordType the event record type 
     *  @return the event query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>eventRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(eventRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.EventQueryInspectorRecord getEventQueryInspectorRecord(org.osid.type.Type eventRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.EventQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(eventRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(eventRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param eventRecordType the event record type
     *  @return the event search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>eventRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(eventRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.EventSearchOrderRecord getEventSearchOrderRecord(org.osid.type.Type eventRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.EventSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(eventRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(eventRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this event. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param eventQueryRecord the event query record
     *  @param eventQueryInspectorRecord the event query inspector
     *         record
     *  @param eventSearchOrderRecord the event search order record
     *  @param eventRecordType event record type
     *  @throws org.osid.NullArgumentException
     *          <code>eventQueryRecord</code>,
     *          <code>eventQueryInspectorRecord</code>,
     *          <code>eventSearchOrderRecord</code> or
     *          <code>eventRecordTypeevent</code> is
     *          <code>null</code>
     */
            
    protected void addEventRecords(org.osid.calendaring.records.EventQueryRecord eventQueryRecord, 
                                      org.osid.calendaring.records.EventQueryInspectorRecord eventQueryInspectorRecord, 
                                      org.osid.calendaring.records.EventSearchOrderRecord eventSearchOrderRecord, 
                                      org.osid.type.Type eventRecordType) {

        addRecordType(eventRecordType);

        nullarg(eventQueryRecord, "event query record");
        nullarg(eventQueryInspectorRecord, "event query inspector record");
        nullarg(eventSearchOrderRecord, "event search odrer record");

        this.queryRecords.add(eventQueryRecord);
        this.queryInspectorRecords.add(eventQueryInspectorRecord);
        this.searchOrderRecords.add(eventSearchOrderRecord);
        
        return;
    }


    protected class AssemblyOsidContainableQuery
        extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidContainableQuery
        implements org.osid.OsidContainableQuery,
                   org.osid.OsidContainableQueryInspector,
                   org.osid.OsidContainableSearchOrder {
        
        
        /** 
         *  Constructs a new <code>AssemblyOsidContainableQuery</code>.
         *
         *  @param assembler the query assembler
         *  @throws org.osid.NullArgumentException <code>assembler</code>
         *          is <code>null</code>
         */
        
        protected AssemblyOsidContainableQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
            super(assembler);
            return;
        }


        /**
         *  Gets the column name for the sequestered field.
         *
         *  @return the column name
         */
        
        protected String getSequesteredColumn() {
            return (super.getSequesteredColumn());
        }
    }    
}
