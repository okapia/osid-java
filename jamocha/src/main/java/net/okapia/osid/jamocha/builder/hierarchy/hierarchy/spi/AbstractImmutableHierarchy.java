//
// AbstractImmutableHierarchy.java
//
//     Wraps a mutable Hierarchy to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.hierarchy.hierarchy.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Hierarchy</code> to hide modifiers. This
 *  wrapper provides an immutized Hierarchy from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying hierarchy whose state changes are visible.
 */

public abstract class AbstractImmutableHierarchy
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidCatalog
    implements org.osid.hierarchy.Hierarchy {

    private final org.osid.hierarchy.Hierarchy hierarchy;


    /**
     *  Constructs a new <code>AbstractImmutableHierarchy</code>.
     *
     *  @param hierarchy the hierarchy to immutablize
     *  @throws org.osid.NullArgumentException <code>hierarchy</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableHierarchy(org.osid.hierarchy.Hierarchy hierarchy) {
        super(hierarchy);
        this.hierarchy = hierarchy;
        return;
    }


    /**
     *  Gets the hierarchy record corresponding to the given <code> Hierarchy 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record. The <code> 
     *  hierarchyRecordType </code> may be the <code> Type </code> returned in 
     *  <code> getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(hierarchyRecordType) 
     *  </code> is <code> true </code> . 
     *
     *  @param  hierarchyRecordType the type of the record to retrieve 
     *  @return the hierarchy record 
     *  @throws org.osid.NullArgumentException <code> hierarchyRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(hierarchyrecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.hierarchy.records.HierarchyRecord getHierarchyRecord(org.osid.type.Type hierarchyRecordType)
        throws org.osid.OperationFailedException {

        return (this.hierarchy.getHierarchyRecord(hierarchyRecordType));
    }
}

