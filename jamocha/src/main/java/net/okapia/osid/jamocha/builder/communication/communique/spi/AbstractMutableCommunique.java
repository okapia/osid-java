//
// AbstractMutableCommunique.java
//
//     Defines a mutable Communique.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.communication.communique.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>Communique</code>.
 */

public abstract class AbstractMutableCommunique
    extends net.okapia.osid.jamocha.communication.communique.spi.AbstractCommunique
    implements org.osid.communication.Communique,
               net.okapia.osid.jamocha.builder.communication.communique.CommuniqueMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this communique. 
     *
     *  @param record communique record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addCommuniqueRecord(org.osid.communication.records.CommuniqueRecord record, org.osid.type.Type recordType) {
        super.addCommuniqueRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }

    
    /**
     *  Sets the display name for this communique.
     *
     *  @param displayName the name for this communique
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this communique.
     *
     *  @param description the description of this communique
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException
     *          <code>genusType</code> is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the message.
     *
     *  @param message a message
     *  @throws org.osid.NullArgumentException <code>message</code> is
     *          <code>null</code>
     */

    @Override
    public void setMessage(org.osid.locale.DisplayText message) {
        super.setMessage(message);
        return;
    }


    /**
     *  Sets the level.
     *
     *  @param level a level
     *  @throws org.osid.NullArgumentException <code>level</code> is
     *          <code>null</code>
     */

    @Override
    public void setLevel(org.osid.communication.CommuniqueLevel level) {
        super.setLevel(level);
        return;
    }


    /**
     *  Sets the response required flag.
     *
     *  @param responseRequired {@code true} if a response is
     *         required, {@code false} otherwise
     */

    @Override
    public void setResponseRequired(boolean responseRequired) {
        super.setResponseRequired(responseRequired);
        return;
    }

    
    /**
     *  Adds a response option.
     *
     *  @param option a response option
     *  @throws org.osid.NullArgumentException
     *          <code>option</code> is <code>null</code>
     */

    @Override
    public void addResponseOption(org.osid.communication.ResponseOption option) {
        super.addResponseOption(option);
        return;
    }


    /**
     *  Sets all the response options.
     *
     *  @param options a collection of response options
     *  @throws org.osid.NullArgumentException <code>options</code> is
     *          <code>null</code>
     */

    @Override
    public void setResponseOptions(java.util.Collection<org.osid.communication.ResponseOption> options) {
        super.setResponseOptions(options);
        return;
    }


    /**
     *  Sets the respond via form flag.
     *
     *  @param respondViaForm {@code true} if the response should use
     *         a form, @code false} otherwise
     */

    @Override
    public void setRespondViaForm(boolean respondViaForm) {
        super.setRespondViaForm(respondViaForm);
        return;
    }
}

