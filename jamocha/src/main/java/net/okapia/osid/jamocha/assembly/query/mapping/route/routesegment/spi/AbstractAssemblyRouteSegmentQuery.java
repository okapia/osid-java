//
// AbstractAssemblyRouteSegmentQuery.java
//
//     A RouteSegmentQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.mapping.route.routesegment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A RouteSegmentQuery that stores terms.
 */

public abstract class AbstractAssemblyRouteSegmentQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.mapping.route.RouteSegmentQuery,
               org.osid.mapping.route.RouteSegmentQueryInspector,
               org.osid.mapping.route.RouteSegmentSearchOrder {

    private final java.util.Collection<org.osid.mapping.route.records.RouteSegmentQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.mapping.route.records.RouteSegmentQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.mapping.route.records.RouteSegmentSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyRouteSegmentQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyRouteSegmentQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches the starting instructions for the segment. 
     *
     *  @param  startingInstructions the starting instructions 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.InvalidArgumentException <code> startingInstructions 
     *          </code> is not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> startingInstructions 
     *          </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchStartingInstructions(String startingInstructions, 
                                          org.osid.type.Type stringMatchType, 
                                          boolean match) {
        getAssembler().addStringTerm(getStartingInstructionsColumn(), startingInstructions, stringMatchType, match);
        return;
    }


    /**
     *  Matches segments that have any starting instructions. 
     *
     *  @param  match <code> true </code> to match segments with any starting 
     *          instructions, <code> false </code> to match segments with no 
     *          starting instriuctions 
     */

    @OSID @Override
    public void matchAnyStartingInstructions(boolean match) {
        getAssembler().addStringWildcardTerm(getStartingInstructionsColumn(), match);
        return;
    }


    /**
     *  Clears the starting instruction query terms. 
     */

    @OSID @Override
    public void clearStartingInstructionsTerms() {
        getAssembler().clearTerms(getStartingInstructionsColumn());
        return;
    }


    /**
     *  Gets the starting instruction query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getStartingInstructionsTerms() {
        return (getAssembler().getStringTerms(getStartingInstructionsColumn()));
    }


    /**
     *  Specifies a preference for oredering the results by starting 
     *  instructions. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByStartingInstructions(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getStartingInstructionsColumn(), style);
        return;
    }


    /**
     *  Gets the StartingInstructions column name.
     *
     * @return the column name
     */

    protected String getStartingInstructionsColumn() {
        return ("starting_instructions");
    }


    /**
     *  Matches the ending instructions for the segment. 
     *
     *  @param  endingInstructions the ending instructions 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.InvalidArgumentException <code> endingInstructions 
     *          </code> is not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> endingInstructions 
     *          </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchEndingInstructions(String endingInstructions, 
                                        org.osid.type.Type stringMatchType, 
                                        boolean match) {
        getAssembler().addStringTerm(getEndingInstructionsColumn(), endingInstructions, stringMatchType, match);
        return;
    }


    /**
     *  Matches segments that have any ending instructions. 
     *
     *  @param  match <code> true </code> to match segments with any ending 
     *          instructions, <code> false </code> to match segments with no 
     *          ending instriuctions 
     */

    @OSID @Override
    public void matchAnyEndingInstructions(boolean match) {
        getAssembler().addStringWildcardTerm(getEndingInstructionsColumn(), match);
        return;
    }


    /**
     *  Clears the ending instruction query terms. 
     */

    @OSID @Override
    public void clearEndingInstructionsTerms() {
        getAssembler().clearTerms(getEndingInstructionsColumn());
        return;
    }


    /**
     *  Gets the ending instruction query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getEndingInstructionsTerms() {
        return (getAssembler().getStringTerms(getEndingInstructionsColumn()));
    }


    /**
     *  Specifies a preference for oredering the results by ending 
     *  instructions. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByEndingInstructions(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getEndingInstructionsColumn(), style);
        return;
    }


    /**
     *  Gets the EndingInstructions column name.
     *
     * @return the column name
     */

    protected String getEndingInstructionsColumn() {
        return ("ending_instructions");
    }


    /**
     *  Matches route segments with distances in the specified range. 
     *
     *  @param  from starting range 
     *  @param  to ending range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> to </code> ie less 
     *          than <code> from </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchDistance(org.osid.mapping.Distance from, 
                              org.osid.mapping.Distance to, boolean match) {
        getAssembler().addDistanceRangeTerm(getDistanceColumn(), from, to, match);
        return;
    }


    /**
     *  Clears the distance query terms. 
     */

    @OSID @Override
    public void clearDistanceTerms() {
        getAssembler().clearTerms(getDistanceColumn());
        return;
    }


    /**
     *  Gets the distance query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DistanceRangeTerm[] getDistanceTerms() {
        return (getAssembler().getDistanceRangeTerms(getDistanceColumn()));
    }


    /**
     *  Specifies a preference for oredering the results by distance. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDistance(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDistanceColumn(), style);
        return;
    }


    /**
     *  Gets the Distance column name.
     *
     * @return the column name
     */

    protected String getDistanceColumn() {
        return ("distance");
    }


    /**
     *  Matches route segments with estimated ravel times in the specified 
     *  range. 
     *
     *  @param  from starting range 
     *  @param  to ending range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> to </code> ie less 
     *          than <code> from </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchETA(org.osid.calendaring.Duration from, 
                         org.osid.calendaring.Duration to, boolean match) {
        getAssembler().addDurationRangeTerm(getETAColumn(), from, to, match);
        return;
    }


    /**
     *  Clears the ETA query terms. 
     */

    @OSID @Override
    public void clearETATerms() {
        getAssembler().clearTerms(getETAColumn());
        return;
    }


    /**
     *  Gets the ETA query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getETATerms() {
        return (getAssembler().getDurationRangeTerms(getETAColumn()));
    }


    /**
     *  Specifies a preference for oredering the results by estimated travel 
     *  time. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByETA(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getETAColumn(), style);
        return;
    }


    /**
     *  Gets the ETA column name.
     *
     * @return the column name
     */

    protected String getETAColumn() {
        return ("e_ta");
    }


    /**
     *  Sets the path <code> Id </code> for this query to match route segments 
     *  containing paths. 
     *
     *  @param  pathId the path <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> pathId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchPathId(org.osid.id.Id pathId, boolean match) {
        getAssembler().addIdTerm(getPathIdColumn(), pathId, match);
        return;
    }


    /**
     *  Clears the path <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearPathIdTerms() {
        getAssembler().clearTerms(getPathIdColumn());
        return;
    }


    /**
     *  Gets the path <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPathIdTerms() {
        return (getAssembler().getIdTerms(getPathIdColumn()));
    }


    /**
     *  Specifies a preference for oredering the results by path. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPath(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getPathColumn(), style);
        return;
    }


    /**
     *  Gets the PathId column name.
     *
     * @return the column name
     */

    protected String getPathIdColumn() {
        return ("path_id");
    }


    /**
     *  Tests if a <code> PathQuery </code> is available. 
     *
     *  @return <code> true </code> if a path query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathQuery() {
        return (false);
    }


    /**
     *  Gets the query for a path. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the path query 
     *  @throws org.osid.UnimplementedException <code> supportsPathQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathQuery getPathQuery() {
        throw new org.osid.UnimplementedException("supportsPathQuery() is false");
    }


    /**
     *  Matches route segments that have any path. 
     *
     *  @param  match <code> true </code> to match route segments with any 
     *          path, <code> false </code> to match route segments with no 
     *          path 
     */

    @OSID @Override
    public void matchAnyPath(boolean match) {
        getAssembler().addIdWildcardTerm(getPathColumn(), match);
        return;
    }


    /**
     *  Clears the path query terms. 
     */

    @OSID @Override
    public void clearPathTerms() {
        getAssembler().clearTerms(getPathColumn());
        return;
    }


    /**
     *  Gets the path query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.path.PathQueryInspector[] getPathTerms() {
        return (new org.osid.mapping.path.PathQueryInspector[0]);
    }


    /**
     *  Tests if a search order for the path is available. 
     *
     *  @return <code> true </code> if a path search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathSearchOrder() {
        return (false);
    }


    /**
     *  Gets a path search order. 
     *
     *  @return the path search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPathSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathSearchOrder getPathSearchOrder() {
        throw new org.osid.UnimplementedException("supportsPathSearchOrder() is false");
    }


    /**
     *  Gets the Path column name.
     *
     * @return the column name
     */

    protected String getPathColumn() {
        return ("path");
    }


    /**
     *  Sets the path <code> Id </code> for this query to match route segments 
     *  containing paths. 
     *
     *  @param  pathId the path <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> pathId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchRouteId(org.osid.id.Id pathId, boolean match) {
        getAssembler().addIdTerm(getRouteIdColumn(), pathId, match);
        return;
    }


    /**
     *  Clears the route <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRouteIdTerms() {
        getAssembler().clearTerms(getRouteIdColumn());
        return;
    }


    /**
     *  Gets the route <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRouteIdTerms() {
        return (getAssembler().getIdTerms(getRouteIdColumn()));
    }


    /**
     *  Specifies a preference for oredering the results by path. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRoute(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getRouteColumn(), style);
        return;
    }


    /**
     *  Gets the RouteId column name.
     *
     * @return the column name
     */

    protected String getRouteIdColumn() {
        return ("route_id");
    }


    /**
     *  Tests if a <code> RouteQuery </code> is available. 
     *
     *  @return <code> true </code> if a route query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRouteQuery() {
        return (false);
    }


    /**
     *  Gets the query for a route. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the route query 
     *  @throws org.osid.UnimplementedException <code> supportsRouteQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteQuery getRouteQuery() {
        throw new org.osid.UnimplementedException("supportsRouteQuery() is false");
    }


    /**
     *  Clears the route query terms. 
     */

    @OSID @Override
    public void clearRouteTerms() {
        getAssembler().clearTerms(getRouteColumn());
        return;
    }


    /**
     *  Gets the route query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteQueryInspector[] getRouteTerms() {
        return (new org.osid.mapping.route.RouteQueryInspector[0]);
    }


    /**
     *  Tests if a search order for the route is available. 
     *
     *  @return <code> true </code> if a route search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRouteSearchOrder() {
        return (false);
    }


    /**
     *  Gets a route search order. 
     *
     *  @return the path search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRouteSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteSearchOrder getRouteSearchOrder() {
        throw new org.osid.UnimplementedException("supportsRouteSearchOrder() is false");
    }


    /**
     *  Gets the Route column name.
     *
     * @return the column name
     */

    protected String getRouteColumn() {
        return ("route");
    }


    /**
     *  Tests if this routeSegment supports the given record
     *  <code>Type</code>.
     *
     *  @param  routeSegmentRecordType a route segment record type 
     *  @return <code>true</code> if the routeSegmentRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>routeSegmentRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type routeSegmentRecordType) {
        for (org.osid.mapping.route.records.RouteSegmentQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(routeSegmentRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  routeSegmentRecordType the route segment record type 
     *  @return the route segment query record 
     *  @throws org.osid.NullArgumentException
     *          <code>routeSegmentRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(routeSegmentRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.route.records.RouteSegmentQueryRecord getRouteSegmentQueryRecord(org.osid.type.Type routeSegmentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.route.records.RouteSegmentQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(routeSegmentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(routeSegmentRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  routeSegmentRecordType the route segment record type 
     *  @return the route segment query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>routeSegmentRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(routeSegmentRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.route.records.RouteSegmentQueryInspectorRecord getRouteSegmentQueryInspectorRecord(org.osid.type.Type routeSegmentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.route.records.RouteSegmentQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(routeSegmentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(routeSegmentRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param routeSegmentRecordType the route segment record type
     *  @return the route segment search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>routeSegmentRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(routeSegmentRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.route.records.RouteSegmentSearchOrderRecord getRouteSegmentSearchOrderRecord(org.osid.type.Type routeSegmentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.route.records.RouteSegmentSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(routeSegmentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(routeSegmentRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this route segment. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param routeSegmentQueryRecord the route segment query record
     *  @param routeSegmentQueryInspectorRecord the route segment query inspector
     *         record
     *  @param routeSegmentSearchOrderRecord the route segment search order record
     *  @param routeSegmentRecordType route segment record type
     *  @throws org.osid.NullArgumentException
     *          <code>routeSegmentQueryRecord</code>,
     *          <code>routeSegmentQueryInspectorRecord</code>,
     *          <code>routeSegmentSearchOrderRecord</code> or
     *          <code>routeSegmentRecordTyperouteSegment</code> is
     *          <code>null</code>
     */
            
    protected void addRouteSegmentRecords(org.osid.mapping.route.records.RouteSegmentQueryRecord routeSegmentQueryRecord, 
                                      org.osid.mapping.route.records.RouteSegmentQueryInspectorRecord routeSegmentQueryInspectorRecord, 
                                      org.osid.mapping.route.records.RouteSegmentSearchOrderRecord routeSegmentSearchOrderRecord, 
                                      org.osid.type.Type routeSegmentRecordType) {

        addRecordType(routeSegmentRecordType);

        nullarg(routeSegmentQueryRecord, "route segment query record");
        nullarg(routeSegmentQueryInspectorRecord, "route segment query inspector record");
        nullarg(routeSegmentSearchOrderRecord, "route segment search odrer record");

        this.queryRecords.add(routeSegmentQueryRecord);
        this.queryInspectorRecords.add(routeSegmentQueryInspectorRecord);
        this.searchOrderRecords.add(routeSegmentSearchOrderRecord);
        
        return;
    }
}
