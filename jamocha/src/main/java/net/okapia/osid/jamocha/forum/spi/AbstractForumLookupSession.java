//
// AbstractForumLookupSession.java
//
//    A starter implementation framework for providing a Forum
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.forum.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A starter implementation framework for providing a Forum
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getForums(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractForumLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.forum.ForumLookupSession {

    private boolean pedantic = false;
    private org.osid.forum.Forum forum = new net.okapia.osid.jamocha.nil.forum.forum.UnknownForum();
    

    /**
     *  Tests if this user can perform <code>Forum</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupForums() {
        return (true);
    }


    /**
     *  A complete view of the <code>Forum</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeForumView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Forum</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryForumView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }

     
    /**
     *  Gets the <code>Forum</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Forum</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Forum</code> and
     *  retained for compatibility.
     *
     *  @param  forumId <code>Id</code> of the
     *          <code>Forum</code>
     *  @return the forum
     *  @throws org.osid.NotFoundException <code>forumId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>forumId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.Forum getForum(org.osid.id.Id forumId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.forum.ForumList forums = getForums()) {
            while (forums.hasNext()) {
                org.osid.forum.Forum forum = forums.getNextForum();
                if (forum.getId().equals(forumId)) {
                    return (forum);
                }
            }
        } 

        throw new org.osid.NotFoundException(forumId + " not found");
    }


    /**
     *  Gets a <code>ForumList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  forums specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Forums</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getForums()</code>.
     *
     *  @param  forumIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Forum</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>forumIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.ForumList getForumsByIds(org.osid.id.IdList forumIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.forum.Forum> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = forumIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getForum(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("forum " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.forum.forum.LinkedForumList(ret));
    }


    /**
     *  Gets a <code>ForumList</code> corresponding to the given
     *  forum genus <code>Type</code> which does not include
     *  forums of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  forums or an error results. Otherwise, the returned list
     *  may contain only those forums that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getForums()</code>.
     *
     *  @param  forumGenusType a forum genus type 
     *  @return the returned <code>Forum</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>forumGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.ForumList getForumsByGenusType(org.osid.type.Type forumGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.forum.forum.ForumGenusFilterList(getForums(), forumGenusType));
    }


    /**
     *  Gets a <code>ForumList</code> corresponding to the given
     *  forum genus <code>Type</code> and include any additional
     *  forums with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  forums or an error results. Otherwise, the returned list
     *  may contain only those forums that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getForums()</code>.
     *
     *  @param  forumGenusType a forum genus type 
     *  @return the returned <code>Forum</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>forumGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.ForumList getForumsByParentGenusType(org.osid.type.Type forumGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getForumsByGenusType(forumGenusType));
    }


    /**
     *  Gets a <code>ForumList</code> containing the given
     *  forum record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  forums or an error results. Otherwise, the returned list
     *  may contain only those forums that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getForums()</code>.
     *
     *  @param  forumRecordType a forum record type 
     *  @return the returned <code>Forum</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>forumRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.ForumList getForumsByRecordType(org.osid.type.Type forumRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.forum.forum.ForumRecordFilterList(getForums(), forumRecordType));
    }


    /**
     *  Gets a <code>ForumList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known forums or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  forums that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Forum</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.forum.ForumList getForumsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.forum.forum.ForumProviderFilterList(getForums(), resourceId));
    }


    /**
     *  Gets all <code>Forums</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  forums or an error results. Otherwise, the returned list
     *  may contain only those forums that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Forums</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.forum.ForumList getForums()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the forum list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of forums
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.forum.ForumList filterForumsOnViews(org.osid.forum.ForumList list)
        throws org.osid.OperationFailedException {

        return (list);
    }
}
