//
// InvariantMapRelevancyEnablerLookupSession
//
//    Implements a RelevancyEnabler lookup service backed by a fixed collection of
//    relevancyEnablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.ontology.rules;


/**
 *  Implements a RelevancyEnabler lookup service backed by a fixed
 *  collection of relevancy enablers. The relevancy enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapRelevancyEnablerLookupSession
    extends net.okapia.osid.jamocha.core.ontology.rules.spi.AbstractMapRelevancyEnablerLookupSession
    implements org.osid.ontology.rules.RelevancyEnablerLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapRelevancyEnablerLookupSession</code> with no
     *  relevancy enablers.
     *  
     *  @param ontology the ontology
     *  @throws org.osid.NullArgumnetException {@code ontology} is
     *          {@code null}
     */

    public InvariantMapRelevancyEnablerLookupSession(org.osid.ontology.Ontology ontology) {
        setOntology(ontology);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapRelevancyEnablerLookupSession</code> with a single
     *  relevancy enabler.
     *  
     *  @param ontology the ontology
     *  @param relevancyEnabler a single relevancy enabler
     *  @throws org.osid.NullArgumentException {@code ontology} or
     *          {@code relevancyEnabler} is <code>null</code>
     */

      public InvariantMapRelevancyEnablerLookupSession(org.osid.ontology.Ontology ontology,
                                               org.osid.ontology.rules.RelevancyEnabler relevancyEnabler) {
        this(ontology);
        putRelevancyEnabler(relevancyEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapRelevancyEnablerLookupSession</code> using an array
     *  of relevancy enablers.
     *  
     *  @param ontology the ontology
     *  @param relevancyEnablers an array of relevancy enablers
     *  @throws org.osid.NullArgumentException {@code ontology} or
     *          {@code relevancyEnablers} is <code>null</code>
     */

      public InvariantMapRelevancyEnablerLookupSession(org.osid.ontology.Ontology ontology,
                                               org.osid.ontology.rules.RelevancyEnabler[] relevancyEnablers) {
        this(ontology);
        putRelevancyEnablers(relevancyEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapRelevancyEnablerLookupSession</code> using a
     *  collection of relevancy enablers.
     *
     *  @param ontology the ontology
     *  @param relevancyEnablers a collection of relevancy enablers
     *  @throws org.osid.NullArgumentException {@code ontology} or
     *          {@code relevancyEnablers} is <code>null</code>
     */

      public InvariantMapRelevancyEnablerLookupSession(org.osid.ontology.Ontology ontology,
                                               java.util.Collection<? extends org.osid.ontology.rules.RelevancyEnabler> relevancyEnablers) {
        this(ontology);
        putRelevancyEnablers(relevancyEnablers);
        return;
    }
}
