//
// AbstractRaceProcessorQuery.java
//
//     A template for making a RaceProcessor Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.rules.raceprocessor.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for race processors.
 */

public abstract class AbstractRaceProcessorQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidProcessorQuery
    implements org.osid.voting.rules.RaceProcessorQuery {

    private final java.util.Collection<org.osid.voting.rules.records.RaceProcessorQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches rules with a maximum winners value between the given range 
     *  inclusive. 
     *
     *  @param  low start of range 
     *  @param  high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     */

    @OSID @Override
    public void matchMaximumWinners(long low, long high, boolean match) {
        return;
    }


    /**
     *  Matches rules with any maximum winner value. 
     *
     *  @param  match <code> true </code> to match rules with a maximum winner 
     *          value, <code> false </code> to match rules with no maximum 
     *          winner value 
     */

    @OSID @Override
    public void matchAnyMaximumWinners(boolean match) {
        return;
    }


    /**
     *  Clears the maximum winners terms. 
     */

    @OSID @Override
    public void clearMaximumWinnersTerms() {
        return;
    }


    /**
     *  Matches rules with a minimum percentage to win value between the given 
     *  range inclusive. 
     *
     *  @param  low start of range 
     *  @param  high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     */

    @OSID @Override
    public void matchMinimumPercentageToWin(long low, long high, boolean match) {
        return;
    }


    /**
     *  Matches rules with any minimum percentage to win value. 
     *
     *  @param  match <code> true </code> to match rules with a minimum value, 
     *          <code> false </code> to match rules with no minimum value 
     */

    @OSID @Override
    public void matchAnyMinimumPercentageToWin(boolean match) {
        return;
    }


    /**
     *  Clears the minimum percentage to win terms. 
     */

    @OSID @Override
    public void clearMinimumPercentageToWinTerms() {
        return;
    }


    /**
     *  Matches rules with a minimum votes to win value between the given 
     *  range inclusive. 
     *
     *  @param  low start of range 
     *  @param  high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     */

    @OSID @Override
    public void matchMinimumVotesToWin(long low, long high, boolean match) {
        return;
    }


    /**
     *  Matches rules with any minimum votes to win value. 
     *
     *  @param  match <code> true </code> to match rules with a minimum value, 
     *          <code> false </code> to match rules with no minimum value 
     */

    @OSID @Override
    public void matchAnyMinimumVotesToWin(boolean match) {
        return;
    }


    /**
     *  Clears the minimum votes to win terms. 
     */

    @OSID @Override
    public void clearMinimumVotesToWinTerms() {
        return;
    }


    /**
     *  Matches rules mapped to the race. 
     *
     *  @param  raceId the race <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> raceId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchRuledRaceId(org.osid.id.Id raceId, boolean match) {
        return;
    }


    /**
     *  Clears the race <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledRaceIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> RaceQuery </code> is available. 
     *
     *  @return <code> true </code> if a race query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledRaceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a race. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the race query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledRaceQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.RaceQuery getRuledRaceQuery() {
        throw new org.osid.UnimplementedException("supportsRuledRaceQuery() is false");
    }


    /**
     *  Matches any rule mapped to any race. 
     *
     *  @param  match <code> true </code> for rules mapped to any race, <code> 
     *          false </code> to match rules mapped to no race 
     */

    @OSID @Override
    public void matchAnyRuledRace(boolean match) {
        return;
    }


    /**
     *  Clears the race query terms. 
     */

    @OSID @Override
    public void clearRuledRaceTerms() {
        return;
    }


    /**
     *  Matches rules mapped to the polls. 
     *
     *  @param  pollsId the polls <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPollsId(org.osid.id.Id pollsId, boolean match) {
        return;
    }


    /**
     *  Clears the polls <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearPollsIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> PollsQuery </code> is available. 
     *
     *  @return <code> true </code> if a polls query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPollsQuery() {
        return (false);
    }


    /**
     *  Gets the query for a polls. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the polls query 
     *  @throws org.osid.UnimplementedException <code> supportsPollsQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.PollsQuery getPollsQuery() {
        throw new org.osid.UnimplementedException("supportsPollsQuery() is false");
    }


    /**
     *  Clears the polls query terms. 
     */

    @OSID @Override
    public void clearPollsTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given race processor query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a race processor implementing the requested record.
     *
     *  @param raceProcessorRecordType a race processor record type
     *  @return the race processor query record
     *  @throws org.osid.NullArgumentException
     *          <code>raceProcessorRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(raceProcessorRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.rules.records.RaceProcessorQueryRecord getRaceProcessorQueryRecord(org.osid.type.Type raceProcessorRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.rules.records.RaceProcessorQueryRecord record : this.records) {
            if (record.implementsRecordType(raceProcessorRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(raceProcessorRecordType + " is not supported");
    }


    /**
     *  Adds a record to this race processor query. 
     *
     *  @param raceProcessorQueryRecord race processor query record
     *  @param raceProcessorRecordType raceProcessor record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRaceProcessorQueryRecord(org.osid.voting.rules.records.RaceProcessorQueryRecord raceProcessorQueryRecord, 
                                          org.osid.type.Type raceProcessorRecordType) {

        addRecordType(raceProcessorRecordType);
        nullarg(raceProcessorQueryRecord, "race processor query record");
        this.records.add(raceProcessorQueryRecord);        
        return;
    }
}
