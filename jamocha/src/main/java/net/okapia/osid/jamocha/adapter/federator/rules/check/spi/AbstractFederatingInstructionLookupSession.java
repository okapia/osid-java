//
// AbstractFederatingInstructionLookupSession.java
//
//     An abstract federating adapter for an InstructionLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.rules.check.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for an
 *  InstructionLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingInstructionLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.rules.check.InstructionLookupSession>
    implements org.osid.rules.check.InstructionLookupSession {

    private boolean parallel = false;
    private org.osid.rules.Engine engine = new net.okapia.osid.jamocha.nil.rules.engine.UnknownEngine();


    /**
     *  Constructs a new <code>AbstractFederatingInstructionLookupSession</code>.
     */

    protected AbstractFederatingInstructionLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.rules.check.InstructionLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Engine/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Engine Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getEngineId() {
        return (this.engine.getId());
    }


    /**
     *  Gets the <code>Engine</code> associated with this 
     *  session.
     *
     *  @return the <code>Engine</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.Engine getEngine()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.engine);
    }


    /**
     *  Sets the <code>Engine</code>.
     *
     *  @param  engine the engine for this session
     *  @throws org.osid.NullArgumentException <code>engine</code>
     *          is <code>null</code>
     */

    protected void setEngine(org.osid.rules.Engine engine) {
        nullarg(engine, "engine");
        this.engine = engine;
        return;
    }


    /**
     *  Tests if this user can perform <code>Instruction</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupInstructions() {
        for (org.osid.rules.check.InstructionLookupSession session : getSessions()) {
            if (session.canLookupInstructions()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Instruction</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeInstructionView() {
        for (org.osid.rules.check.InstructionLookupSession session : getSessions()) {
            session.useComparativeInstructionView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Instruction</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryInstructionView() {
        for (org.osid.rules.check.InstructionLookupSession session : getSessions()) {
            session.usePlenaryInstructionView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include instructions in engines which are children
     *  of this engine in the engine hierarchy.
     */

    @OSID @Override
    public void useFederatedEngineView() {
        for (org.osid.rules.check.InstructionLookupSession session : getSessions()) {
            session.useFederatedEngineView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this engine only.
     */

    @OSID @Override
    public void useIsolatedEngineView() {
        for (org.osid.rules.check.InstructionLookupSession session : getSessions()) {
            session.useIsolatedEngineView();
        }

        return;
    }


    /**
     *  Only active instructions are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveInstructionView() {
        for (org.osid.rules.check.InstructionLookupSession session : getSessions()) {
            session.useActiveInstructionView();
        }

        return;
    }


    /**
     *  Active and inactive instructions are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusInstructionView() {
        for (org.osid.rules.check.InstructionLookupSession session : getSessions()) {
            session.useAnyStatusInstructionView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Instruction</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Instruction</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Instruction</code> and
     *  retained for compatibility.
     *
     *  In active mode, instructions are returned that are currently
     *  active. In any status mode, active and inactive instructions
     *  are returned.
     *
     *  @param  instructionId <code>Id</code> of the
     *          <code>Instruction</code>
     *  @return the instruction
     *  @throws org.osid.NotFoundException <code>instructionId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>instructionId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.Instruction getInstruction(org.osid.id.Id instructionId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.rules.check.InstructionLookupSession session : getSessions()) {
            try {
                return (session.getInstruction(instructionId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(instructionId + " not found");
    }


    /**
     *  Gets an <code>InstructionList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  instructions specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>Instructions</code> may be omitted from the list and may
     *  present the elements in any order including returning a unique
     *  set.
     *
     *  In active mode, instructions are returned that are currently
     *  active. In any status mode, active and inactive instructions
     *  are returned.
     *
     *  @param  instructionIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Instruction</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>instructionIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionList getInstructionsByIds(org.osid.id.IdList instructionIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.rules.check.instruction.MutableInstructionList ret = new net.okapia.osid.jamocha.rules.check.instruction.MutableInstructionList();

        try (org.osid.id.IdList ids = instructionIds) {
            while (ids.hasNext()) {
                ret.addInstruction(getInstruction(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets an <code>InstructionList</code> corresponding to the given
     *  instruction genus <code>Type</code> which does not include
     *  instructions of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  instructions or an error results. Otherwise, the returned list
     *  may contain only those instructions that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, instructions are returned that are currently
     *  active. In any status mode, active and inactive instructions
     *  are returned.
     *
     *  @param  instructionGenusType an instruction genus type 
     *  @return the returned <code>Instruction</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>instructionGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionList getInstructionsByGenusType(org.osid.type.Type instructionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.rules.check.instruction.FederatingInstructionList ret = getInstructionList();

        for (org.osid.rules.check.InstructionLookupSession session : getSessions()) {
            ret.addInstructionList(session.getInstructionsByGenusType(instructionGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>InstructionList</code> corresponding to the given
     *  instruction genus <code>Type</code> and include any additional
     *  instructions with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  instructions or an error results. Otherwise, the returned list
     *  may contain only those instructions that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, instructions are returned that are currently
     *  active. In any status mode, active and inactive instructions
     *  are returned.
     *
     *  @param  instructionGenusType an instruction genus type 
     *  @return the returned <code>Instruction</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>instructionGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionList getInstructionsByParentGenusType(org.osid.type.Type instructionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.rules.check.instruction.FederatingInstructionList ret = getInstructionList();

        for (org.osid.rules.check.InstructionLookupSession session : getSessions()) {
            ret.addInstructionList(session.getInstructionsByParentGenusType(instructionGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>InstructionList</code> containing the given
     *  instruction record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  instructions or an error results. Otherwise, the returned list
     *  may contain only those instructions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, instructions are returned that are currently
     *  active. In any status mode, active and inactive instructions
     *  are returned.
     *
     *  @param  instructionRecordType an instruction record type 
     *  @return the returned <code>Instruction</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>instructionRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionList getInstructionsByRecordType(org.osid.type.Type instructionRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.rules.check.instruction.FederatingInstructionList ret = getInstructionList();

        for (org.osid.rules.check.InstructionLookupSession session : getSessions()) {
            ret.addInstructionList(session.getInstructionsByRecordType(instructionRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>InstructionList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  instructions or an error results. Otherwise, the returned list
     *  may contain only those instructions that are accessible
     *  through this session.
     *  
     *  In active mode, instructions are returned that are currently
     *  active. In any status mode, active and inactive instructions
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Instruction</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.rules.check.InstructionList getInstructionsOnDate(org.osid.calendaring.DateTime from, 
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.rules.check.instruction.FederatingInstructionList ret = getInstructionList();

        for (org.osid.rules.check.InstructionLookupSession session : getSessions()) {
            ret.addInstructionList(session.getInstructionsOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a list of instructions corresponding to an agenda
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  instructions or an error results. Otherwise, the returned list
     *  may contain only those instructions that are accessible
     *  through this session.
     *  
     *  In active mode, instructions are returned that are currently
     *  active. In any status mode, active and inactive instructions
     *  are returned.
     *
     *  @param  agendaId the <code>Id</code> of the agenda
     *  @return the returned <code>InstructionList</code>
     *  @throws org.osid.NullArgumentException <code>agendaId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.rules.check.InstructionList getInstructionsForAgenda(org.osid.id.Id agendaId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.rules.check.instruction.FederatingInstructionList ret = getInstructionList();

        for (org.osid.rules.check.InstructionLookupSession session : getSessions()) {
            ret.addInstructionList(session.getInstructionsForAgenda(agendaId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of instructions corresponding to an agenda
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  instructions or an error results. Otherwise, the returned list
     *  may contain only those instructions that are accessible
     *  through this session.
     *  
     *  In active mode, instructions are returned that are currently
     *  active. In any status mode, active and inactive instructions
     *  are returned.
     *
     *  @param  agendaId the <code>Id</code> of the agenda
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>InstructionList</code>
     *  @throws org.osid.NullArgumentException <code>agendaId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.rules.check.InstructionList getInstructionsForAgendaOnDate(org.osid.id.Id agendaId,
                                                                               org.osid.calendaring.DateTime from,
                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.rules.check.instruction.FederatingInstructionList ret = getInstructionList();

        for (org.osid.rules.check.InstructionLookupSession session : getSessions()) {
            ret.addInstructionList(session.getInstructionsForAgendaOnDate(agendaId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of instructions corresponding to a check
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  instructions or an error results. Otherwise, the returned list
     *  may contain only those instructions that are accessible
     *  through this session.
     *  
     *  In active mode, instructions are returned that are currently
     *  active. In any status mode, active and inactive instructions
     *  are returned.
     *
     *  @param  checkId the <code>Id</code> of the check
     *  @return the returned <code>InstructionList</code>
     *  @throws org.osid.NullArgumentException <code>checkId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.rules.check.InstructionList getInstructionsForCheck(org.osid.id.Id checkId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.rules.check.instruction.FederatingInstructionList ret = getInstructionList();

        for (org.osid.rules.check.InstructionLookupSession session : getSessions()) {
            ret.addInstructionList(session.getInstructionsForCheck(checkId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of instructions corresponding to a check
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  instructions or an error results. Otherwise, the returned list
     *  may contain only those instructions that are accessible
     *  through this session.
     *  
     *  In active mode, instructions are returned that are currently
     *  active. In any status mode, active and inactive instructions
     *  are returned.
     *
     *  @param  checkId the <code>Id</code> of the check
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>InstructionList</code>
     *  @throws org.osid.NullArgumentException <code>checkId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.rules.check.InstructionList getInstructionsForCheckOnDate(org.osid.id.Id checkId,
                                                                              org.osid.calendaring.DateTime from,
                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.rules.check.instruction.FederatingInstructionList ret = getInstructionList();

        for (org.osid.rules.check.InstructionLookupSession session : getSessions()) {
            ret.addInstructionList(session.getInstructionsForCheckOnDate(checkId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of instructions corresponding to agenda and check
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  instructions or an error results. Otherwise, the returned list
     *  may contain only those instructions that are accessible
     *  through this session.
     *  
     *  In active mode, instructions are returned that are currently
     *  active. In any status mode, active and inactive instructions
     *  are returned.
     *
     *  @param  agendaId the <code>Id</code> of the agenda
     *  @param  checkId the <code>Id</code> of the check
     *  @return the returned <code>InstructionList</code>
     *  @throws org.osid.NullArgumentException <code>agendaId</code>,
     *          <code>checkId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.rules.check.InstructionList getInstructionsForAgendaAndCheck(org.osid.id.Id agendaId,
                                                                                 org.osid.id.Id checkId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.rules.check.instruction.FederatingInstructionList ret = getInstructionList();

        for (org.osid.rules.check.InstructionLookupSession session : getSessions()) {
            ret.addInstructionList(session.getInstructionsForAgendaAndCheck(agendaId, checkId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of instructions corresponding to agenda and check
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  instructions or an error results. Otherwise, the returned list
     *  may contain only those instructions that are accessible
     *  through this session.
     *  
     *  In active mode, instructions are returned that are currently
     *  active. In any status mode, active and inactive instructions
     *  are returned.
     *
     *  @param  checkId the <code>Id</code> of the check
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>InstructionList</code>
     *  @throws org.osid.NullArgumentException <code>agendaId</code>,
     *          <code>checkId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.rules.check.InstructionList getInstructionsForAgendaAndCheckOnDate(org.osid.id.Id agendaId,
                                                                                       org.osid.id.Id checkId,
                                                                                       org.osid.calendaring.DateTime from,
                                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.rules.check.instruction.FederatingInstructionList ret = getInstructionList();

        for (org.osid.rules.check.InstructionLookupSession session : getSessions()) {
            ret.addInstructionList(session.getInstructionsForAgendaAndCheckOnDate(agendaId, checkId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Instructions</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  instructions or an error results. Otherwise, the returned list
     *  may contain only those instructions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, instructions are returned that are currently
     *  active. In any status mode, active and inactive instructions
     *  are returned.
     *
     *  @return a list of <code>Instructions</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionList getInstructions()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.rules.check.instruction.FederatingInstructionList ret = getInstructionList();

        for (org.osid.rules.check.InstructionLookupSession session : getSessions()) {
            ret.addInstructionList(session.getInstructions());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.rules.check.instruction.FederatingInstructionList getInstructionList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.rules.check.instruction.ParallelInstructionList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.rules.check.instruction.CompositeInstructionList());
        }
    }
}
