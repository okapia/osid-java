//
// MutableIndexedMapPlanLookupSession
//
//    Implements a Plan lookup service backed by a collection of
//    plans indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.plan;


/**
 *  Implements a Plan lookup service backed by a collection of
 *  plans. The plans are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some plans may be compatible
 *  with more types than are indicated through these plan
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of plans can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapPlanLookupSession
    extends net.okapia.osid.jamocha.core.course.plan.spi.AbstractIndexedMapPlanLookupSession
    implements org.osid.course.plan.PlanLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapPlanLookupSession} with no plans.
     *
     *  @param courseCatalog the course catalog
     *  @throws org.osid.NullArgumentException {@code courseCatalog}
     *          is {@code null}
     */

      public MutableIndexedMapPlanLookupSession(org.osid.course.CourseCatalog courseCatalog) {
        setCourseCatalog(courseCatalog);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapPlanLookupSession} with a
     *  single plan.
     *  
     *  @param courseCatalog the course catalog
     *  @param  plan a single plan
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code plan} is {@code null}
     */

    public MutableIndexedMapPlanLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  org.osid.course.plan.Plan plan) {
        this(courseCatalog);
        putPlan(plan);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapPlanLookupSession} using an
     *  array of plans.
     *
     *  @param courseCatalog the course catalog
     *  @param  plans an array of plans
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code plans} is {@code null}
     */

    public MutableIndexedMapPlanLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  org.osid.course.plan.Plan[] plans) {
        this(courseCatalog);
        putPlans(plans);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapPlanLookupSession} using a
     *  collection of plans.
     *
     *  @param courseCatalog the course catalog
     *  @param  plans a collection of plans
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code plans} is {@code null}
     */

    public MutableIndexedMapPlanLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  java.util.Collection<? extends org.osid.course.plan.Plan> plans) {

        this(courseCatalog);
        putPlans(plans);
        return;
    }
    

    /**
     *  Makes a {@code Plan} available in this session.
     *
     *  @param  plan a plan
     *  @throws org.osid.NullArgumentException {@code plan{@code  is
     *          {@code null}
     */

    @Override
    public void putPlan(org.osid.course.plan.Plan plan) {
        super.putPlan(plan);
        return;
    }


    /**
     *  Makes an array of plans available in this session.
     *
     *  @param  plans an array of plans
     *  @throws org.osid.NullArgumentException {@code plans{@code 
     *          is {@code null}
     */

    @Override
    public void putPlans(org.osid.course.plan.Plan[] plans) {
        super.putPlans(plans);
        return;
    }


    /**
     *  Makes collection of plans available in this session.
     *
     *  @param  plans a collection of plans
     *  @throws org.osid.NullArgumentException {@code plan{@code  is
     *          {@code null}
     */

    @Override
    public void putPlans(java.util.Collection<? extends org.osid.course.plan.Plan> plans) {
        super.putPlans(plans);
        return;
    }


    /**
     *  Removes a Plan from this session.
     *
     *  @param planId the {@code Id} of the plan
     *  @throws org.osid.NullArgumentException {@code planId{@code  is
     *          {@code null}
     */

    @Override
    public void removePlan(org.osid.id.Id planId) {
        super.removePlan(planId);
        return;
    }    
}
