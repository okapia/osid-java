//
// AbstractQueryShipmentLookupSession.java
//
//    A ShipmentQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.inventory.shipment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ShipmentQuerySession adapter.
 */

public abstract class AbstractAdapterShipmentQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.inventory.shipment.ShipmentQuerySession {

    private final org.osid.inventory.shipment.ShipmentQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterShipmentQuerySession.
     *
     *  @param session the underlying shipment query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterShipmentQuerySession(org.osid.inventory.shipment.ShipmentQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeWarehouse</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeWarehouse Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getWarehouseId() {
        return (this.session.getWarehouseId());
    }


    /**
     *  Gets the {@codeWarehouse</code> associated with this 
     *  session.
     *
     *  @return the {@codeWarehouse</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.Warehouse getWarehouse()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getWarehouse());
    }


    /**
     *  Tests if this user can perform {@codeShipment</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchShipments() {
        return (this.session.canSearchShipments());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include shipments in warehouses which are children
     *  of this warehouse in the warehouse hierarchy.
     */

    @OSID @Override
    public void useFederatedWarehouseView() {
        this.session.useFederatedWarehouseView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this warehouse only.
     */
    
    @OSID @Override
    public void useIsolatedWarehouseView() {
        this.session.useIsolatedWarehouseView();
        return;
    }
    
      
    /**
     *  Gets a shipment query. The returned query will not have an
     *  extension query.
     *
     *  @return the shipment query 
     */
      
    @OSID @Override
    public org.osid.inventory.shipment.ShipmentQuery getShipmentQuery() {
        return (this.session.getShipmentQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  shipmentQuery the shipment query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code shipmentQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code shipmentQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentList getShipmentsByQuery(org.osid.inventory.shipment.ShipmentQuery shipmentQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getShipmentsByQuery(shipmentQuery));
    }
}
