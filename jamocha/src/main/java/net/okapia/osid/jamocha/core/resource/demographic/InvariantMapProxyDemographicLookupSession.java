//
// InvariantMapProxyDemographicLookupSession
//
//    Implements a Demographic lookup service backed by a fixed
//    collection of demographics. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resource.demographic;


/**
 *  Implements a Demographic lookup service backed by a fixed
 *  collection of demographics. The demographics are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyDemographicLookupSession
    extends net.okapia.osid.jamocha.core.resource.demographic.spi.AbstractMapDemographicLookupSession
    implements org.osid.resource.demographic.DemographicLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyDemographicLookupSession} with no
     *  demographics.
     *
     *  @param bin the bin
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bin} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyDemographicLookupSession(org.osid.resource.Bin bin,
                                                  org.osid.proxy.Proxy proxy) {
        setBin(bin);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyDemographicLookupSession} with a single
     *  demographic.
     *
     *  @param bin the bin
     *  @param demographic a single demographic
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bin},
     *          {@code demographic} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyDemographicLookupSession(org.osid.resource.Bin bin,
                                                  org.osid.resource.demographic.Demographic demographic, org.osid.proxy.Proxy proxy) {

        this(bin, proxy);
        putDemographic(demographic);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyDemographicLookupSession} using
     *  an array of demographics.
     *
     *  @param bin the bin
     *  @param demographics an array of demographics
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bin},
     *          {@code demographics} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyDemographicLookupSession(org.osid.resource.Bin bin,
                                                  org.osid.resource.demographic.Demographic[] demographics, org.osid.proxy.Proxy proxy) {

        this(bin, proxy);
        putDemographics(demographics);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyDemographicLookupSession} using a
     *  collection of demographics.
     *
     *  @param bin the bin
     *  @param demographics a collection of demographics
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bin},
     *          {@code demographics} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyDemographicLookupSession(org.osid.resource.Bin bin,
                                                  java.util.Collection<? extends org.osid.resource.demographic.Demographic> demographics,
                                                  org.osid.proxy.Proxy proxy) {

        this(bin, proxy);
        putDemographics(demographics);
        return;
    }
}
