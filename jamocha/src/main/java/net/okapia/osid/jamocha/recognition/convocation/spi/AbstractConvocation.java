//
// AbstractConvocation.java
//
//     Defines a Convocation.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recognition.convocation.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Convocation</code>.
 */

public abstract class AbstractConvocation
    extends net.okapia.osid.jamocha.spi.AbstractOsidGovernator
    implements org.osid.recognition.Convocation {

    private final java.util.Collection<org.osid.recognition.Award> awards = new java.util.LinkedHashSet<>();
    private org.osid.calendaring.DateTime date;
    private org.osid.calendaring.TimePeriod timePeriod;

    private final java.util.Collection<org.osid.recognition.records.ConvocationRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Ids </code> of the awards. 
     *
     *  @return the award <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getAwardIds() {
        try {
            org.osid.recognition.AwardList awards = getAwards();
            return (new net.okapia.osid.jamocha.adapter.converter.recognition.award.AwardToIdList(awards));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the awards. 
     *
     *  @return the awards 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.recognition.AwardList getAwards()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.recognition.award.ArrayAwardList(this.awards));
    }


    /**
     *  Adds an award.
     *
     *  @param award an award
     *  @throws org.osid.NullArgumentException
     *          <code>award</code> is <code>null</code>
     */

    protected void addAward(org.osid.recognition.Award award) {
        nullarg(award, "award");
        this.awards.add(award);
        return;
    }


    /**
     *  Sets all the awards.
     *
     *  @param awards a collection of awards
     *  @throws org.osid.NullArgumentException
     *          <code>awards</code> is <code>null</code>
     */

    protected void setAwards(java.util.Collection<org.osid.recognition.Award> awards) {
        nullarg(awards, "awards");
        this.awards.clear();
        this.awards.addAll(awards);
        return;
    }


    /**
     *  Tests if the convocation confers awards for a period of time. 
     *
     *  @return <code> true </code> if a time period exists, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasTimePeriod() {
        return (this.timePeriod != null);
    }


    /**
     *  Gets the <code> Id </code> of the time period. 
     *
     *  @return the time period <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasTimePeriod() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getTimePeriodId() {
        if (hasTimePeriod()) {
            throw new org.osid.IllegalStateException("hasTimePeriod() is false");
        }

        return (this.timePeriod.getId());
    }


    /**
     *  Gets the time period. 
     *
     *  @return the time period 
     *  @throws org.osid.IllegalStateException <code> hasTimePeriod() </code> 
     *          is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriod getTimePeriod()
        throws org.osid.OperationFailedException {

        if (hasTimePeriod()) {
            throw new org.osid.IllegalStateException("hasTimePeriod() is false");
        }

        return (this.timePeriod);
    }


    /**
     *  Sets the time period.
     *
     *  @param timePeriod a time period
     *  @throws org.osid.NullArgumentException
     *          <code>timePeriod</code> is <code>null</code>
     */

    protected void setTimePeriod(org.osid.calendaring.TimePeriod timePeriod) {
        nullarg(timePeriod, "time period");
        this.timePeriod = timePeriod;
        return;
    }



    /**
     *  Gets the date the awards in this convocation are to be conferred. 
     *
     *  @return the awards date 
     *  @throws org.osid.IllegalStateException <code> hasTimePeriod() </code> 
     *          is <code>true</code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getDate() {
        return (this.date);
    }


    /**
     *  Sets the date.
     *
     *  @param date a date
     *  @throws org.osid.NullArgumentException
     *          <code>date</code> is <code>null</code>
     */

    protected void setDate(org.osid.calendaring.DateTime date) {
        nullarg(date, "date");
        this.date = date;
        this.timePeriod = null;
        return;
    }


    /**
     *  Tests if this convocation supports the given record
     *  <code>Type</code>.
     *
     *  @param  convocationRecordType a convocation record type 
     *  @return <code>true</code> if the convocationRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>convocationRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type convocationRecordType) {
        for (org.osid.recognition.records.ConvocationRecord record : this.records) {
            if (record.implementsRecordType(convocationRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Convocation</code> record <code>Type</code>.
     *
     *  @param  convocationRecordType the convocation record type 
     *  @return the convocation record 
     *  @throws org.osid.NullArgumentException
     *          <code>convocationRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(convocationRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.recognition.records.ConvocationRecord getConvocationRecord(org.osid.type.Type convocationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.recognition.records.ConvocationRecord record : this.records) {
            if (record.implementsRecordType(convocationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(convocationRecordType + " is not supported");
    }


    /**
     *  Adds a record to this convocation. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param convocationRecord the convocation record
     *  @param convocationRecordType convocation record type
     *  @throws org.osid.NullArgumentException
     *          <code>convocationRecord</code> or
     *          <code>convocationRecordTypeconvocation</code> is
     *          <code>null</code>
     */
            
    protected void addConvocationRecord(org.osid.recognition.records.ConvocationRecord convocationRecord, 
                                        org.osid.type.Type convocationRecordType) {

        nullarg(convocationRecord, "convocation record");
        addRecordType(convocationRecordType);
        this.records.add(convocationRecord);
        
        return;
    }
}
