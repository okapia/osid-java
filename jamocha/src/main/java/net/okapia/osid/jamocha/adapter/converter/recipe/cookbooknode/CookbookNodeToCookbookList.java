//
// CookbookNodeToCookbookList.java
//
//     Implements a CookbookNodeToCookbookList adapter.
//
//
// Tom Coppeto
// Okapia
// 21 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.converter.recipe.cookbooknode;

import org.osid.binding.java.annotation.OSID;


/**
 *  Implements a CookbookList adapter to convert cookbook nodes to
 *  cookbooks.
 */

public final class CookbookNodeToCookbookList
    extends net.okapia.osid.jamocha.adapter.converter.recipe.cookbooknode.spi.AbstractCookbookNodeToCookbookList
    implements org.osid.recipe.CookbookList {


    /**
     *  Constructs a new {@code CookbookNodeToCookbookList}.
     *
     *  @param list the cookbook node list to convert
     *  @throws org.osid.NullArgumentException {@code list} is {@code
     *          null}
     */

    public CookbookNodeToCookbookList(org.osid.recipe.CookbookNodeList list) {
        super(list);
        return;
    }
}
