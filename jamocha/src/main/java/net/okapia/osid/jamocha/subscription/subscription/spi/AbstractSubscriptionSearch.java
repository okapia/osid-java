//
// AbstractSubscriptionSearch.java
//
//     A template for making a Subscription Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.subscription.subscription.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing subscription searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractSubscriptionSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.subscription.SubscriptionSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.subscription.records.SubscriptionSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.subscription.SubscriptionSearchOrder subscriptionSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of subscriptions. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  subscriptionIds list of subscriptions
     *  @throws org.osid.NullArgumentException
     *          <code>subscriptionIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongSubscriptions(org.osid.id.IdList subscriptionIds) {
        while (subscriptionIds.hasNext()) {
            try {
                this.ids.add(subscriptionIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongSubscriptions</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of subscription Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getSubscriptionIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  subscriptionSearchOrder subscription search order 
     *  @throws org.osid.NullArgumentException
     *          <code>subscriptionSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>subscriptionSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderSubscriptionResults(org.osid.subscription.SubscriptionSearchOrder subscriptionSearchOrder) {
	this.subscriptionSearchOrder = subscriptionSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.subscription.SubscriptionSearchOrder getSubscriptionSearchOrder() {
	return (this.subscriptionSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given subscription search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a subscription implementing the requested record.
     *
     *  @param subscriptionSearchRecordType a subscription search record
     *         type
     *  @return the subscription search record
     *  @throws org.osid.NullArgumentException
     *          <code>subscriptionSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(subscriptionSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.subscription.records.SubscriptionSearchRecord getSubscriptionSearchRecord(org.osid.type.Type subscriptionSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.subscription.records.SubscriptionSearchRecord record : this.records) {
            if (record.implementsRecordType(subscriptionSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(subscriptionSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this subscription search. 
     *
     *  @param subscriptionSearchRecord subscription search record
     *  @param subscriptionSearchRecordType subscription search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addSubscriptionSearchRecord(org.osid.subscription.records.SubscriptionSearchRecord subscriptionSearchRecord, 
                                           org.osid.type.Type subscriptionSearchRecordType) {

        addRecordType(subscriptionSearchRecordType);
        this.records.add(subscriptionSearchRecord);        
        return;
    }
}
