//
// AbstractImmutableProgramOffering.java
//
//     Wraps a mutable ProgramOffering to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.program.programoffering.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>ProgramOffering</code> to hide modifiers. This
 *  wrapper provides an immutized ProgramOffering from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying programOffering whose state changes are visible.
 */

public abstract class AbstractImmutableProgramOffering
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRelationship
    implements org.osid.course.program.ProgramOffering {

    private final org.osid.course.program.ProgramOffering programOffering;


    /**
     *  Constructs a new
     *  <code>AbstractImmutableProgramOffering</code>.
     *
     *  @param programOffering the program offering to immutablize
     *  @throws org.osid.NullArgumentException
     *          <code>programOffering</code> is <code>null</code>
     */

    protected AbstractImmutableProgramOffering(org.osid.course.program.ProgramOffering programOffering) {
        super(programOffering);
        this.programOffering = programOffering;
        return;
    }


    /**
     *  Gets the canonical program <code> Id </code> associated with this 
     *  program offering. 
     *
     *  @return the program <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getProgramId() {
        return (this.programOffering.getProgramId());
    }


    /**
     *  Gets the canonical program associated with this program
     *  offering.
     *
     *  @return the program 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.program.Program getProgram()
        throws org.osid.OperationFailedException {

        return (this.programOffering.getProgram());
    }


    /**
     *  Gets the <code> Id </code> of the <code> Term </code> of this 
     *  offering. 
     *
     *  @return the <code> Term </code> <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getTermId() {
        return (this.programOffering.getTermId());
    }


    /**
     *  Gets the <code> Term </code> of this offering. 
     *
     *  @return the term 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.Term getTerm()
        throws org.osid.OperationFailedException {

        return (this.programOffering.getTerm());
    }


    /**
     *  Gets the formal title of this program offering.
     *
     *  @return the program offering title 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getTitle() {
        return (this.programOffering.getTitle());
    }


    /**
     *  Gets the program offering number which is a label generally
     *  used to index the program offering in a catalog.
     *
     *  @return the program offering number 
     */

    @OSID @Override
    public String getNumber() {
        return (this.programOffering.getNumber());
    }


    /**
     *  Tests if this program offering has sponsors. 
     *
     *  @return <code> true </code> if this program offering has
     *          sponsors, <code> false </code> otherwise
     */

    @OSID @Override
    public boolean hasSponsors() {
        return (this.programOffering.hasSponsors());
    }


    /**
     *  Gets the sponsor <code> Ids. </code> 
     *
     *  @return the sponsor <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> hasSponsors() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getSponsorIds() {
        return (this.programOffering.getSponsorIds());
    }


    /**
     *  Gets the sponsors. 
     *
     *  @return the sponsors 
     *  @throws org.osid.IllegalStateException <code> hasSponsors() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getSponsors()
        throws org.osid.OperationFailedException {

        return (this.programOffering.getSponsors());
    }


    /**
     *  Gets the an informational string for the program completion. 
     *
     *  @return the program completion 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getCompletionRequirementsInfo() {
        return (this.programOffering.getCompletionRequirementsInfo());
    }


    /**
     *  Tests if this program has a rule for the program completion. 
     *
     *  @return <code> true </code> if this program has a completion rule, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasCompletionRequirements() {
        return (this.programOffering.hasCompletionRequirements());
    }


    /**
     *  Gets the <code> Requisite </code> <code> Ids </code> for the program 
     *  completion. 
     *
     *  @return the completion requisite <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> 
     *          hasCompletionRequirements() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getCompletionRequirementIds() {
        return (this.programOffering.getCompletionRequirementIds());
    }


    /**
     *  Gets the requisites for the program completion. Each <code> Requisite 
     *  </code> is an <code> AND </code> term and must be true for the 
     *  requirements to be satisifed. 
     *
     *  @return the completion requisites 
     *  @throws org.osid.IllegalStateException <code> 
     *          hasCompletionRequirements() </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteList getCompletionRequirements()
        throws org.osid.OperationFailedException {

        return (this.programOffering.getCompletionRequirements());
    }


    /**
     *  Tests if completion of this program results in credentials awarded. 
     *
     *  @return <code> true </code> if this program earns credentials, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean earnsCredentials() {
        return (this.programOffering.earnsCredentials());
    }


    /**
     *  Gets the awarded credential <code> Ids. </code> 
     *
     *  @return the returned list of credential <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> earnsCredentials() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getCredentialIds() {
        return (this.programOffering.getCredentialIds());
    }


    /**
     *  Gets the awarded credentials. 
     *
     *  @return the returned list of credentials 
     *  @throws org.osid.IllegalStateException <code> earnsCredentials() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.program.CredentialList getCredentials()
        throws org.osid.OperationFailedException {

        return (this.programOffering.getCredentials());
    }


    /**
     *  Tests if this program offering requires advanced registration. 
     *
     *  @return <code> true </code> if this program offering requires
     *          advance registration, <code> false </code> otherwise
     */

    @OSID @Override
    public boolean requiresRegistration() {
        return (this.programOffering.requiresRegistration());
    }


    /**
     *  Gets the minimum number of students this offering can have. 
     *
     *  @return the minimum seats 
     *  @throws org.osid.IllegalStateException <code> requiresRegistration() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public long getMinimumSeats() {
        return (this.programOffering.getMinimumSeats());
    }


    /**
     *  Gets the maximum number of students this offering can have. 
     *
     *  @return the maximum seats 
     *  @throws org.osid.IllegalStateException <code> requiresRegistration() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public long getMaximumSeats() {
        return (this.programOffering.getMaximumSeats());
    }


    /**
     *  Gets an external resource, such as a class web site, associated with 
     *  this offering. 
     *
     *  @return a URL string 
     */

    @OSID @Override
    public String getURL() {
        return (this.programOffering.getURL());
    }


    /**
     *  Gets the program offering record corresponding to the given <code> 
     *  ProgramOffering </code> record <code> Type. </code> This method is 
     *  used to retrieve an object implementing the requested record. The 
     *  <code> programOfferingRecordType </code> may be the <code> Type 
     *  </code> returned in <code> getRecordTypes() </code> or any of its 
     *  parents in a <code> Type </code> hierarchy where <code> 
     *  hasRecordType(programOfferingRecordType) </code> is <code> true 
     *  </code> . 
     *
     *  @param  programOfferingRecordType the type of program offering record 
     *          to retrieve 
     *  @return the program offering record 
     *  @throws org.osid.NullArgumentException <code> 
     *          programOfferingRecordType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(programOfferingRecordType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.program.records.ProgramOfferingRecord getProgramOfferingRecord(org.osid.type.Type programOfferingRecordType)
        throws org.osid.OperationFailedException {

        return (this.programOffering.getProgramOfferingRecord(programOfferingRecordType));
    }
}

