//
// AbstractMapAuctionHouseLookupSession
//
//    A simple framework for providing an AuctionHouse lookup service
//    backed by a fixed collection of auction houses.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.bidding.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an AuctionHouse lookup service backed by a
 *  fixed collection of auction houses. The auction houses are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>AuctionHouses</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapAuctionHouseLookupSession
    extends net.okapia.osid.jamocha.bidding.spi.AbstractAuctionHouseLookupSession
    implements org.osid.bidding.AuctionHouseLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.bidding.AuctionHouse> auctionHouses = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.bidding.AuctionHouse>());


    /**
     *  Makes an <code>AuctionHouse</code> available in this session.
     *
     *  @param  auctionHouse an auction house
     *  @throws org.osid.NullArgumentException <code>auctionHouse<code>
     *          is <code>null</code>
     */

    protected void putAuctionHouse(org.osid.bidding.AuctionHouse auctionHouse) {
        this.auctionHouses.put(auctionHouse.getId(), auctionHouse);
        return;
    }


    /**
     *  Makes an array of auction houses available in this session.
     *
     *  @param  auctionHouses an array of auction houses
     *  @throws org.osid.NullArgumentException <code>auctionHouses<code>
     *          is <code>null</code>
     */

    protected void putAuctionHouses(org.osid.bidding.AuctionHouse[] auctionHouses) {
        putAuctionHouses(java.util.Arrays.asList(auctionHouses));
        return;
    }


    /**
     *  Makes a collection of auction houses available in this session.
     *
     *  @param  auctionHouses a collection of auction houses
     *  @throws org.osid.NullArgumentException <code>auctionHouses<code>
     *          is <code>null</code>
     */

    protected void putAuctionHouses(java.util.Collection<? extends org.osid.bidding.AuctionHouse> auctionHouses) {
        for (org.osid.bidding.AuctionHouse auctionHouse : auctionHouses) {
            this.auctionHouses.put(auctionHouse.getId(), auctionHouse);
        }

        return;
    }


    /**
     *  Removes an AuctionHouse from this session.
     *
     *  @param  auctionHouseId the <code>Id</code> of the auction house
     *  @throws org.osid.NullArgumentException <code>auctionHouseId<code> is
     *          <code>null</code>
     */

    protected void removeAuctionHouse(org.osid.id.Id auctionHouseId) {
        this.auctionHouses.remove(auctionHouseId);
        return;
    }


    /**
     *  Gets the <code>AuctionHouse</code> specified by its <code>Id</code>.
     *
     *  @param  auctionHouseId <code>Id</code> of the <code>AuctionHouse</code>
     *  @return the auctionHouse
     *  @throws org.osid.NotFoundException <code>auctionHouseId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>auctionHouseId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouse getAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.bidding.AuctionHouse auctionHouse = this.auctionHouses.get(auctionHouseId);
        if (auctionHouse == null) {
            throw new org.osid.NotFoundException("auctionHouse not found: " + auctionHouseId);
        }

        return (auctionHouse);
    }


    /**
     *  Gets all <code>AuctionHouses</code>. In plenary mode, the returned
     *  list contains all known auctionHouses or an error
     *  results. Otherwise, the returned list may contain only those
     *  auctionHouses that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>AuctionHouses</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseList getAuctionHouses()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.bidding.auctionhouse.ArrayAuctionHouseList(this.auctionHouses.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.auctionHouses.clear();
        super.close();
        return;
    }
}
