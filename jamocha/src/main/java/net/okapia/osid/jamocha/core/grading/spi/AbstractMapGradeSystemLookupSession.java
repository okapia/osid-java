//
// AbstractMapGradeSystemLookupSession
//
//    A simple framework for providing a GradeSystem lookup service
//    backed by a fixed collection of grade systems.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.grading.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a GradeSystem lookup service backed by a
 *  fixed collection of grade systems. The grade systems are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>GradeSystems</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapGradeSystemLookupSession
    extends net.okapia.osid.jamocha.grading.spi.AbstractGradeSystemLookupSession
    implements org.osid.grading.GradeSystemLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.grading.GradeSystem> gradeSystems = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.grading.GradeSystem>());


    /**
     *  Makes a <code>GradeSystem</code> available in this session.
     *
     *  @param  gradeSystem a grade system
     *  @throws org.osid.NullArgumentException <code>gradeSystem<code>
     *          is <code>null</code>
     */

    protected void putGradeSystem(org.osid.grading.GradeSystem gradeSystem) {
        this.gradeSystems.put(gradeSystem.getId(), gradeSystem);
        return;
    }


    /**
     *  Makes an array of grade systems available in this session.
     *
     *  @param  gradeSystems an array of grade systems
     *  @throws org.osid.NullArgumentException <code>gradeSystems<code>
     *          is <code>null</code>
     */

    protected void putGradeSystems(org.osid.grading.GradeSystem[] gradeSystems) {
        putGradeSystems(java.util.Arrays.asList(gradeSystems));
        return;
    }


    /**
     *  Makes a collection of grade systems available in this session.
     *
     *  @param  gradeSystems a collection of grade systems
     *  @throws org.osid.NullArgumentException <code>gradeSystems<code>
     *          is <code>null</code>
     */

    protected void putGradeSystems(java.util.Collection<? extends org.osid.grading.GradeSystem> gradeSystems) {
        for (org.osid.grading.GradeSystem gradeSystem : gradeSystems) {
            this.gradeSystems.put(gradeSystem.getId(), gradeSystem);
        }

        return;
    }


    /**
     *  Removes a GradeSystem from this session.
     *
     *  @param  gradeSystemId the <code>Id</code> of the grade system
     *  @throws org.osid.NullArgumentException <code>gradeSystemId<code> is
     *          <code>null</code>
     */

    protected void removeGradeSystem(org.osid.id.Id gradeSystemId) {
        this.gradeSystems.remove(gradeSystemId);
        return;
    }


    /**
     *  Gets the <code>GradeSystem</code> specified by its <code>Id</code>.
     *
     *  @param  gradeSystemId <code>Id</code> of the <code>GradeSystem</code>
     *  @return the gradeSystem
     *  @throws org.osid.NotFoundException <code>gradeSystemId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>gradeSystemId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradeSystem getGradeSystem(org.osid.id.Id gradeSystemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.grading.GradeSystem gradeSystem = this.gradeSystems.get(gradeSystemId);
        if (gradeSystem == null) {
            throw new org.osid.NotFoundException("gradeSystem not found: " + gradeSystemId);
        }

        return (gradeSystem);
    }


    /**
     *  Gets all <code>GradeSystems</code>. In plenary mode, the returned
     *  list contains all known gradeSystems or an error
     *  results. Otherwise, the returned list may contain only those
     *  gradeSystems that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>GradeSystems</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemList getGradeSystems()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.grading.gradesystem.ArrayGradeSystemList(this.gradeSystems.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.gradeSystems.clear();
        super.close();
        return;
    }
}
