//
// AbstractQueryHoldLookupSession.java
//
//    A HoldQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.hold.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A HoldQuerySession adapter.
 */

public abstract class AbstractAdapterHoldQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.hold.HoldQuerySession {

    private final org.osid.hold.HoldQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterHoldQuerySession.
     *
     *  @param session the underlying hold query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterHoldQuerySession(org.osid.hold.HoldQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeOubliette</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeOubliette Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getOublietteId() {
        return (this.session.getOublietteId());
    }


    /**
     *  Gets the {@codeOubliette</code> associated with this 
     *  session.
     *
     *  @return the {@codeOubliette</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.Oubliette getOubliette()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getOubliette());
    }


    /**
     *  Tests if this user can perform {@codeHold</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchHolds() {
        return (this.session.canSearchHolds());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include holds in oubliettes which are children
     *  of this oubliette in the oubliette hierarchy.
     */

    @OSID @Override
    public void useFederatedOublietteView() {
        this.session.useFederatedOublietteView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this oubliette only.
     */
    
    @OSID @Override
    public void useIsolatedOublietteView() {
        this.session.useIsolatedOublietteView();
        return;
    }
    
      
    /**
     *  Gets a hold query. The returned query will not have an
     *  extension query.
     *
     *  @return the hold query 
     */
      
    @OSID @Override
    public org.osid.hold.HoldQuery getHoldQuery() {
        return (this.session.getHoldQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  holdQuery the hold query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code holdQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code holdQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.hold.HoldList getHoldsByQuery(org.osid.hold.HoldQuery holdQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getHoldsByQuery(holdQuery));
    }
}
