//
// AbstractQueryMeterLookupSession.java
//
//    An inline adapter that maps a MeterLookupSession to
//    a MeterQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.metering.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a MeterLookupSession to
 *  a MeterQuerySession.
 */

public abstract class AbstractQueryMeterLookupSession
    extends net.okapia.osid.jamocha.metering.spi.AbstractMeterLookupSession
    implements org.osid.metering.MeterLookupSession {

    private final org.osid.metering.MeterQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryMeterLookupSession.
     *
     *  @param querySession the underlying meter query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryMeterLookupSession(org.osid.metering.MeterQuerySession querySession) {
        nullarg(querySession, "meter query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Utility</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Utility Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getUtilityId() {
        return (this.session.getUtilityId());
    }


    /**
     *  Gets the <code>Utility</code> associated with this 
     *  session.
     *
     *  @return the <code>Utility</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.metering.Utility getUtility()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getUtility());
    }


    /**
     *  Tests if this user can perform <code>Meter</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupMeters() {
        return (this.session.canSearchMeters());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include meters in utilities which are children
     *  of this utility in the utility hierarchy.
     */

    @OSID @Override
    public void useFederatedUtilityView() {
        this.session.useFederatedUtilityView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this utility only.
     */

    @OSID @Override
    public void useIsolatedUtilityView() {
        this.session.useIsolatedUtilityView();
        return;
    }
    
     
    /**
     *  Gets the <code>Meter</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Meter</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Meter</code> and
     *  retained for compatibility.
     *
     *  @param  meterId <code>Id</code> of the
     *          <code>Meter</code>
     *  @return the meter
     *  @throws org.osid.NotFoundException <code>meterId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>meterId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.metering.Meter getMeter(org.osid.id.Id meterId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.metering.MeterQuery query = getQuery();
        query.matchId(meterId, true);
        org.osid.metering.MeterList meters = this.session.getMetersByQuery(query);
        if (meters.hasNext()) {
            return (meters.getNextMeter());
        } 
        
        throw new org.osid.NotFoundException(meterId + " not found");
    }


    /**
     *  Gets a <code>MeterList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  meters specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Meters</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  meterIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Meter</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>meterIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.metering.MeterList getMetersByIds(org.osid.id.IdList meterIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.metering.MeterQuery query = getQuery();

        try (org.osid.id.IdList ids = meterIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getMetersByQuery(query));
    }


    /**
     *  Gets a <code>MeterList</code> corresponding to the given
     *  meter genus <code>Type</code> which does not include
     *  meters of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  meters or an error results. Otherwise, the returned list
     *  may contain only those meters that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  meterGenusType a meter genus type 
     *  @return the returned <code>Meter</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>meterGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.metering.MeterList getMetersByGenusType(org.osid.type.Type meterGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.metering.MeterQuery query = getQuery();
        query.matchGenusType(meterGenusType, true);
        return (this.session.getMetersByQuery(query));
    }


    /**
     *  Gets a <code>MeterList</code> corresponding to the given
     *  meter genus <code>Type</code> and include any additional
     *  meters with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  meters or an error results. Otherwise, the returned list
     *  may contain only those meters that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  meterGenusType a meter genus type 
     *  @return the returned <code>Meter</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>meterGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.metering.MeterList getMetersByParentGenusType(org.osid.type.Type meterGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.metering.MeterQuery query = getQuery();
        query.matchParentGenusType(meterGenusType, true);
        return (this.session.getMetersByQuery(query));
    }


    /**
     *  Gets a <code>MeterList</code> containing the given
     *  meter record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  meters or an error results. Otherwise, the returned list
     *  may contain only those meters that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  meterRecordType a meter record type 
     *  @return the returned <code>Meter</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>meterRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.metering.MeterList getMetersByRecordType(org.osid.type.Type meterRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.metering.MeterQuery query = getQuery();
        query.matchRecordType(meterRecordType, true);
        return (this.session.getMetersByQuery(query));
    }

    
    /**
     *  Gets all <code>Meters</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  meters or an error results. Otherwise, the returned list
     *  may contain only those meters that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Meters</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.metering.MeterList getMeters()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.metering.MeterQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getMetersByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.metering.MeterQuery getQuery() {
        org.osid.metering.MeterQuery query = this.session.getMeterQuery();
        return (query);
    }
}
