//
// AbstractNodeCookbookHierarchySession.java
//
//     Defines a Cookbook hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.recipe.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a cookbook hierarchy session for delivering a hierarchy
 *  of cookbooks using the CookbookNode interface.
 */

public abstract class AbstractNodeCookbookHierarchySession
    extends net.okapia.osid.jamocha.recipe.spi.AbstractCookbookHierarchySession
    implements org.osid.recipe.CookbookHierarchySession {

    private java.util.Collection<org.osid.recipe.CookbookNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root cookbook <code> Ids </code> in this hierarchy.
     *
     *  @return the root cookbook <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootCookbookIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.recipe.cookbooknode.CookbookNodeToIdList(this.roots));
    }


    /**
     *  Gets the root cookbooks in the cookbook hierarchy. A node
     *  with no parents is an orphan. While all cookbook <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root cookbooks 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.CookbookList getRootCookbooks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.recipe.cookbooknode.CookbookNodeToCookbookList(new net.okapia.osid.jamocha.recipe.cookbooknode.ArrayCookbookNodeList(this.roots)));
    }


    /**
     *  Adds a root cookbook node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootCookbook(org.osid.recipe.CookbookNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root cookbook nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootCookbooks(java.util.Collection<org.osid.recipe.CookbookNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root cookbook node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootCookbook(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.recipe.CookbookNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Cookbook </code> has any parents. 
     *
     *  @param  cookbookId a cookbook <code> Id </code> 
     *  @return <code> true </code> if the cookbook has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> cookbookId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> cookbookId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentCookbooks(org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getCookbookNode(cookbookId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of a
     *  cookbook.
     *
     *  @param  id an <code> Id </code> 
     *  @param  cookbookId the <code> Id </code> of a cookbook 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> cookbookId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> cookbookId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> cookbookId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfCookbook(org.osid.id.Id id, org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.recipe.CookbookNodeList parents = getCookbookNode(cookbookId).getParentCookbookNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextCookbookNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given cookbook. 
     *
     *  @param  cookbookId a cookbook <code> Id </code> 
     *  @return the parent <code> Ids </code> of the cookbook 
     *  @throws org.osid.NotFoundException <code> cookbookId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> cookbookId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentCookbookIds(org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.recipe.cookbook.CookbookToIdList(getParentCookbooks(cookbookId)));
    }


    /**
     *  Gets the parents of the given cookbook. 
     *
     *  @param  cookbookId the <code> Id </code> to query 
     *  @return the parents of the cookbook 
     *  @throws org.osid.NotFoundException <code> cookbookId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> cookbookId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.CookbookList getParentCookbooks(org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.recipe.cookbooknode.CookbookNodeToCookbookList(getCookbookNode(cookbookId).getParentCookbookNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of a
     *  cookbook.
     *
     *  @param  id an <code> Id </code> 
     *  @param  cookbookId the Id of a cookbook 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> cookbookId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> cookbookId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> cookbookId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfCookbook(org.osid.id.Id id, org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfCookbook(id, cookbookId)) {
            return (true);
        }

        try (org.osid.recipe.CookbookList parents = getParentCookbooks(cookbookId)) {
            while (parents.hasNext()) {
                if (isAncestorOfCookbook(id, parents.getNextCookbook().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if a cookbook has any children. 
     *
     *  @param  cookbookId a cookbook <code> Id </code> 
     *  @return <code> true </code> if the <code> cookbookId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> cookbookId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> cookbookId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildCookbooks(org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getCookbookNode(cookbookId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of a
     *  cookbook.
     *
     *  @param  id an <code> Id </code> 
     *  @param cookbookId the <code> Id </code> of a 
     *         cookbook
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> cookbookId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> cookbookId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> cookbookId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfCookbook(org.osid.id.Id id, org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfCookbook(cookbookId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  cookbook.
     *
     *  @param  cookbookId the <code> Id </code> to query 
     *  @return the children of the cookbook 
     *  @throws org.osid.NotFoundException <code> cookbookId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> cookbookId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildCookbookIds(org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.recipe.cookbook.CookbookToIdList(getChildCookbooks(cookbookId)));
    }


    /**
     *  Gets the children of the given cookbook. 
     *
     *  @param  cookbookId the <code> Id </code> to query 
     *  @return the children of the cookbook 
     *  @throws org.osid.NotFoundException <code> cookbookId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> cookbookId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.CookbookList getChildCookbooks(org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.recipe.cookbooknode.CookbookNodeToCookbookList(getCookbookNode(cookbookId).getChildCookbookNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of a
     *  cookbook.
     *
     *  @param  id an <code> Id </code> 
     *  @param cookbookId the <code> Id </code> of a 
     *         cookbook
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> cookbookId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> cookbookId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> cookbookId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfCookbook(org.osid.id.Id id, org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfCookbook(cookbookId, id)) {
            return (true);
        }

        try (org.osid.recipe.CookbookList children = getChildCookbooks(cookbookId)) {
            while (children.hasNext()) {
                if (isDescendantOfCookbook(id, children.getNextCookbook().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  cookbook.
     *
     *  @param  cookbookId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified cookbook node 
     *  @throws org.osid.NotFoundException <code> cookbookId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> cookbookId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getCookbookNodeIds(org.osid.id.Id cookbookId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.recipe.cookbooknode.CookbookNodeToNode(getCookbookNode(cookbookId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given cookbook.
     *
     *  @param  cookbookId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified cookbook node 
     *  @throws org.osid.NotFoundException <code> cookbookId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> cookbookId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.CookbookNode getCookbookNodes(org.osid.id.Id cookbookId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getCookbookNode(cookbookId));
    }


    /**
     *  Closes this <code>CookbookHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets a cookbook node.
     *
     *  @param cookbookId the id of the cookbook node
     *  @throws org.osid.NotFoundException <code>cookbookId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>cookbookId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.recipe.CookbookNode getCookbookNode(org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(cookbookId, "cookbook Id");
        for (org.osid.recipe.CookbookNode cookbook : this.roots) {
            if (cookbook.getId().equals(cookbookId)) {
                return (cookbook);
            }

            org.osid.recipe.CookbookNode r = findCookbook(cookbook, cookbookId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(cookbookId + " is not found");
    }


    protected org.osid.recipe.CookbookNode findCookbook(org.osid.recipe.CookbookNode node, 
                                                        org.osid.id.Id cookbookId) 
	throws org.osid.OperationFailedException {

        try (org.osid.recipe.CookbookNodeList children = node.getChildCookbookNodes()) {
            while (children.hasNext()) {
                org.osid.recipe.CookbookNode cookbook = children.getNextCookbookNode();
                if (cookbook.getId().equals(cookbookId)) {
                    return (cookbook);
                }
                
                cookbook = findCookbook(cookbook, cookbookId);
                if (cookbook != null) {
                    return (cookbook);
                }
            }
        }

        return (null);
    }
}
