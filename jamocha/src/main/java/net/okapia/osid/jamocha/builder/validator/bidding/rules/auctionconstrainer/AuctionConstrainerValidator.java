//
// AuctionConstrainerValidator.java
//
//     Validates an AuctionConstrainer.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.bidding.rules.auctionconstrainer;


/**
 *  Validates an AuctionConstrainer.
 */

public final class AuctionConstrainerValidator
    extends net.okapia.osid.jamocha.builder.validator.bidding.rules.auctionconstrainer.spi.AbstractAuctionConstrainerValidator {


    /**
     *  Constructs a new <code>AuctionConstrainerValidator</code>.
     */

    public AuctionConstrainerValidator() {
        return;
    }


    /**
     *  Constructs a new <code>AuctionConstrainerValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    public AuctionConstrainerValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates an AuctionConstrainer with a default validation.
     *
     *  @param auctionConstrainer an auction constrainer to validate
     *  @return the auction constrainer
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>auctionConstrainer</code>
     *          is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred
     */

    public static org.osid.bidding.rules.AuctionConstrainer validateAuctionConstrainer(org.osid.bidding.rules.AuctionConstrainer auctionConstrainer) {
        AuctionConstrainerValidator validator = new AuctionConstrainerValidator();
        validator.validate(auctionConstrainer);
        return (auctionConstrainer);
    }


    /**
     *  Validates an AuctionConstrainer for the given validations.
     *
     *  @param validation an EnumSet of validations
     *  @param auctionConstrainer an auction constrainer to validate
     *  @return the auction constrainer
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          or <code>auctionConstrainer</code> is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred
     */

    public static org.osid.bidding.rules.AuctionConstrainer validateAuctionConstrainer(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation,
                                                       org.osid.bidding.rules.AuctionConstrainer auctionConstrainer) {

        AuctionConstrainerValidator validator = new AuctionConstrainerValidator(validation);
        validator.validate(auctionConstrainer);
        return (auctionConstrainer);
    }
}
