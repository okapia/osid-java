//
// AbstractIndexedMapProgramEntryLookupSession.java
//
//    A simple framework for providing a ProgramEntry lookup service
//    backed by a fixed collection of program entries with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.chronicle.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a ProgramEntry lookup service backed by a
 *  fixed collection of program entries. The program entries are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some program entries may be compatible
 *  with more types than are indicated through these program entry
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>ProgramEntries</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapProgramEntryLookupSession
    extends AbstractMapProgramEntryLookupSession
    implements org.osid.course.chronicle.ProgramEntryLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.course.chronicle.ProgramEntry> programEntriesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.course.chronicle.ProgramEntry>());
    private final MultiMap<org.osid.type.Type, org.osid.course.chronicle.ProgramEntry> programEntriesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.course.chronicle.ProgramEntry>());


    /**
     *  Makes a <code>ProgramEntry</code> available in this session.
     *
     *  @param  programEntry a program entry
     *  @throws org.osid.NullArgumentException <code>programEntry<code> is
     *          <code>null</code>
     */

    @Override
    protected void putProgramEntry(org.osid.course.chronicle.ProgramEntry programEntry) {
        super.putProgramEntry(programEntry);

        this.programEntriesByGenus.put(programEntry.getGenusType(), programEntry);
        
        try (org.osid.type.TypeList types = programEntry.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.programEntriesByRecord.put(types.getNextType(), programEntry);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a program entry from this session.
     *
     *  @param programEntryId the <code>Id</code> of the program entry
     *  @throws org.osid.NullArgumentException <code>programEntryId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeProgramEntry(org.osid.id.Id programEntryId) {
        org.osid.course.chronicle.ProgramEntry programEntry;
        try {
            programEntry = getProgramEntry(programEntryId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.programEntriesByGenus.remove(programEntry.getGenusType());

        try (org.osid.type.TypeList types = programEntry.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.programEntriesByRecord.remove(types.getNextType(), programEntry);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeProgramEntry(programEntryId);
        return;
    }


    /**
     *  Gets a <code>ProgramEntryList</code> corresponding to the given
     *  program entry genus <code>Type</code> which does not include
     *  program entries of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known program entries or an error results. Otherwise,
     *  the returned list may contain only those program entries that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  programEntryGenusType a program entry genus type 
     *  @return the returned <code>ProgramEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>programEntryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryList getProgramEntriesByGenusType(org.osid.type.Type programEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.chronicle.programentry.ArrayProgramEntryList(this.programEntriesByGenus.get(programEntryGenusType)));
    }


    /**
     *  Gets a <code>ProgramEntryList</code> containing the given
     *  program entry record <code>Type</code>. In plenary mode, the
     *  returned list contains all known program entries or an error
     *  results. Otherwise, the returned list may contain only those
     *  program entries that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  programEntryRecordType a program entry record type 
     *  @return the returned <code>programEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>programEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryList getProgramEntriesByRecordType(org.osid.type.Type programEntryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.chronicle.programentry.ArrayProgramEntryList(this.programEntriesByRecord.get(programEntryRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.programEntriesByGenus.clear();
        this.programEntriesByRecord.clear();

        super.close();

        return;
    }
}
