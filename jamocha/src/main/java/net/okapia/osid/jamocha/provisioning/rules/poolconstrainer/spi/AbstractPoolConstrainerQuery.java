//
// AbstractPoolConstrainerQuery.java
//
//     A template for making a PoolConstrainer Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.rules.poolconstrainer.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for pool constrainers.
 */

public abstract class AbstractPoolConstrainerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidConstrainerQuery
    implements org.osid.provisioning.rules.PoolConstrainerQuery {

    private final java.util.Collection<org.osid.provisioning.rules.records.PoolConstrainerQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches mapped to a pool. 
     *
     *  @param  distributorId the pool <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledPoolId(org.osid.id.Id distributorId, boolean match) {
        return;
    }


    /**
     *  Clears the pool <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledPoolIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> PoolQuery </code> is available. 
     *
     *  @return <code> true </code> if a pool query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledPoolQuery() {
        return (false);
    }


    /**
     *  Gets the query for a pool. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the pool query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledPoolQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolQuery getRuledPoolQuery() {
        throw new org.osid.UnimplementedException("supportsRuledPoolQuery() is false");
    }


    /**
     *  Matches mapped to any pool. 
     *
     *  @param  match <code> true </code> for mapped to any pool, <code> false 
     *          </code> to match mapped to no pools 
     */

    @OSID @Override
    public void matchAnyRuledPool(boolean match) {
        return;
    }


    /**
     *  Clears the pool query terms. 
     */

    @OSID @Override
    public void clearRuledPoolTerms() {
        return;
    }


    /**
     *  Matches mapped to the distributor. 
     *
     *  @param  distributorId the distributor <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDistributorId(org.osid.id.Id distributorId, boolean match) {
        return;
    }


    /**
     *  Clears the distributor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDistributorIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> DistributorQuery </code> is available. 
     *
     *  @return <code> true </code> if a distributor query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDistributorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a distributor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the distributor query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDistributorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQuery getDistributorQuery() {
        throw new org.osid.UnimplementedException("supportsDistributorQuery() is false");
    }


    /**
     *  Clears the distributor query terms. 
     */

    @OSID @Override
    public void clearDistributorTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given pool constrainer query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a pool constrainer implementing the requested record.
     *
     *  @param poolConstrainerRecordType a pool constrainer record type
     *  @return the pool constrainer query record
     *  @throws org.osid.NullArgumentException
     *          <code>poolConstrainerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(poolConstrainerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.PoolConstrainerQueryRecord getPoolConstrainerQueryRecord(org.osid.type.Type poolConstrainerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.rules.records.PoolConstrainerQueryRecord record : this.records) {
            if (record.implementsRecordType(poolConstrainerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(poolConstrainerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this pool constrainer query. 
     *
     *  @param poolConstrainerQueryRecord pool constrainer query record
     *  @param poolConstrainerRecordType poolConstrainer record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addPoolConstrainerQueryRecord(org.osid.provisioning.rules.records.PoolConstrainerQueryRecord poolConstrainerQueryRecord, 
                                          org.osid.type.Type poolConstrainerRecordType) {

        addRecordType(poolConstrainerRecordType);
        nullarg(poolConstrainerQueryRecord, "pool constrainer query record");
        this.records.add(poolConstrainerQueryRecord);        
        return;
    }
}
