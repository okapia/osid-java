//
// AbstractImmutableComment.java
//
//     Wraps a mutable Comment to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.commenting.comment.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Comment</code> to hide modifiers. This
 *  wrapper provides an immutized Comment from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying comment whose state changes are visible.
 */

public abstract class AbstractImmutableComment
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRelationship
    implements org.osid.commenting.Comment {

    private final org.osid.commenting.Comment comment;


    /**
     *  Constructs a new <code>AbstractImmutableComment</code>.
     *
     *  @param comment the comment to immutablize
     *  @throws org.osid.NullArgumentException <code>comment</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableComment(org.osid.commenting.Comment comment) {
        super(comment);
        this.comment = comment;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the referenced object to which this 
     *  comment pertains. 
     *
     *  @return the reference <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getReferenceId() {
        return (this.comment.getReferenceId());
    }


    /**
     *  Gets the <code> Id </code> of the resource who created this comment. 
     *
     *  @return the <code> Resource </code> <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCommentorId() {
        return (this.comment.getCommentorId());
    }


    /**
     *  Gets the resource who created this comment. 
     *
     *  @return the <code> Resource </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getCommentor()
        throws org.osid.OperationFailedException {

        return (this.comment.getCommentor());
    }


    /**
     *  Gets the <code> Id </code> of the agent who created this comment. 
     *
     *  @return the <code> Agent </code> <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCommentingAgentId() {
        return (this.comment.getCommentingAgentId());
    }


    /**
     *  Gets the agent who created this comment. 
     *
     *  @return the <code> Agent </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getCommentingAgent()
        throws org.osid.OperationFailedException {

        return (this.comment.getCommentingAgent());
    }


    /**
     *  Gets the comment text. 
     *
     *  @return the comment text 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getText() {
        return (this.comment.getText());
    }


    /**
     *  Tests if this comment includes a rating. 
     *
     *  @return <code> true </code> if this comment includes a rating, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasRating() {
        return (this.comment.hasRating());
    }


    /**
     *  Gets the <code> Id </code> of the <code> Grade. </code> 
     *
     *  @return the <code> Agent </code> <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasRating() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getRatingId() {
        return (this.comment.getRatingId());
    }


    /**
     *  Gets the <code> Grade. </code> 
     *
     *  @return the <code> Grade </code> 
     *  @throws org.osid.IllegalStateException <code> hasRating() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.Grade getRating()
        throws org.osid.OperationFailedException {

        return (this.comment.getRating());
    }


    /**
     *  Gets the comment record corresponding to the given <code> Comment 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record. The <code> commentRecordType 
     *  </code> may be the <code> Type </code> returned in <code> 
     *  getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(commentRecordType) 
     *  </code> is <code> true </code> . 
     *
     *  @param  commentRecordType the type of comment record to retrieve 
     *  @return the comment record 
     *  @throws org.osid.NullArgumentException <code> commentRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(commentRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.commenting.records.CommentRecord getCommentRecord(org.osid.type.Type commentRecordType)
        throws org.osid.OperationFailedException {

        return (this.comment.getCommentRecord(commentRecordType));
    }
}

