//
// AbstractOntologyQuery.java
//
//     A template for making an Ontology Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ontology.ontology.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for ontologies.
 */

public abstract class AbstractOntologyQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQuery
    implements org.osid.ontology.OntologyQuery {

    private final java.util.Collection<org.osid.ontology.records.OntologyQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the subject <code> Id </code> for this query. 
     *
     *  @param  subjectId a subject <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> subjectId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSubjectId(org.osid.id.Id subjectId, boolean match) {
        return;
    }


    /**
     *  Clears the subject <code> Id </code> terms for this query. 
     */

    @OSID @Override
    public void clearSubjectIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> SubjectQuery </code> is available. 
     *
     *  @return <code> true </code> if a subject query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubjectQuery() {
        return (false);
    }


    /**
     *  Gets the query for a subject. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the subject query 
     *  @throws org.osid.UnimplementedException <code> supportsSubjectQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectQuery getSubjectQuery() {
        throw new org.osid.UnimplementedException("supportsSubjectQuery() is false");
    }


    /**
     *  Matches ontologies that have any subject. 
     *
     *  @param  match <code> true </code> to match ontologies with any 
     *          subject, <code> false </code> to match ontologies with no 
     *          subject 
     */

    @OSID @Override
    public void matchAnySubject(boolean match) {
        return;
    }


    /**
     *  Clears the subject terms for this query. 
     */

    @OSID @Override
    public void clearSubjectTerms() {
        return;
    }


    /**
     *  Sets the relevancy <code> Id </code> for this query. 
     *
     *  @param  relevancyId a relevancy <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> relevancyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRelevancyId(org.osid.id.Id relevancyId, boolean match) {
        return;
    }


    /**
     *  Clears the relevancy <code> Id </code> terms for this query. 
     */

    @OSID @Override
    public void clearRelevancyIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> RelevancyQuery </code> is available. 
     *
     *  @return <code> true </code> if a relevancy query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelevancyQuery() {
        return (false);
    }


    /**
     *  Gets the query for a relevancy. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the relevancy query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyQuery getRelevancyQuery() {
        throw new org.osid.UnimplementedException("supportsRelevancyQuery() is false");
    }


    /**
     *  Matches ontologies that have any relevancy. 
     *
     *  @param  match <code> true </code> to match ontologies with any 
     *          relevancy, <code> false </code> to match ontologies with no 
     *          relevancy 
     */

    @OSID @Override
    public void matchAnyRelevancy(boolean match) {
        return;
    }


    /**
     *  Clears the relevancy terms for this query. 
     */

    @OSID @Override
    public void clearRelevancyTerms() {
        return;
    }


    /**
     *  Sets the ontology <code> Id </code> for this query to match ontologies 
     *  that have the specified ontology as an ancestor. 
     *
     *  @param  ontologyId an ontology <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorOntologyId(org.osid.id.Id ontologyId, 
                                        boolean match) {
        return;
    }


    /**
     *  Clears the ancestor ontology <code> Id </code> terms for this query. 
     */

    @OSID @Override
    public void clearAncestorOntologyIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> OntologyQuery </code> is available. 
     *
     *  @return <code> true </code> if an ontology query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorOntologyQuery() {
        return (false);
    }


    /**
     *  Gets the query for an ontology. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the ontology query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorOntologyQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ontology.OntologyQuery getAncestorOntologyQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorOntologyQuery() is false");
    }


    /**
     *  Matches ontologies with any ancestor. 
     *
     *  @param  match <code> true </code> to match ontologies with any 
     *          ancestor, <code> false </code> to match root ontologies 
     */

    @OSID @Override
    public void matchAnyAncestorOntology(boolean match) {
        return;
    }


    /**
     *  Clears the ancestor ontology terms for this query. 
     */

    @OSID @Override
    public void clearAncestorOntologyTerms() {
        return;
    }


    /**
     *  Sets the ontology <code> Id </code> for this query to match ontologies 
     *  that have the specified ontology as a descendant. 
     *
     *  @param  ontologyId an ontology <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantOntologyId(org.osid.id.Id ontologyId, 
                                          boolean match) {
        return;
    }


    /**
     *  Clears the descendant ontology <code> Id </code> terms for this query. 
     */

    @OSID @Override
    public void clearDescendantOntologyIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> OntologyQuery </code> is available. 
     *
     *  @return <code> true </code> if an ontology query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantOntologyQuery() {
        return (false);
    }


    /**
     *  Gets the query for an ontology. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the ontology query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantOntologyQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ontology.OntologyQuery getDescendantOntologyQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantOntologyQuery() is false");
    }


    /**
     *  Matches ontologies with any descendant. 
     *
     *  @param  match <code> true </code> to match ontologies with any 
     *          descendant, <code> false </code> to match leaf ontologies 
     */

    @OSID @Override
    public void matchAnyDescendantOntology(boolean match) {
        return;
    }


    /**
     *  Clears the descendant ontology terms for this query. 
     */

    @OSID @Override
    public void clearDescendantOntologyTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given ontology query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an ontology implementing the requested record.
     *
     *  @param ontologyRecordType an ontology record type
     *  @return the ontology query record
     *  @throws org.osid.NullArgumentException
     *          <code>ontologyRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(ontologyRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.ontology.records.OntologyQueryRecord getOntologyQueryRecord(org.osid.type.Type ontologyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ontology.records.OntologyQueryRecord record : this.records) {
            if (record.implementsRecordType(ontologyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(ontologyRecordType + " is not supported");
    }


    /**
     *  Adds a record to this ontology query. 
     *
     *  @param ontologyQueryRecord ontology query record
     *  @param ontologyRecordType ontology record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addOntologyQueryRecord(org.osid.ontology.records.OntologyQueryRecord ontologyQueryRecord, 
                                          org.osid.type.Type ontologyRecordType) {

        addRecordType(ontologyRecordType);
        nullarg(ontologyQueryRecord, "ontology query record");
        this.records.add(ontologyQueryRecord);        
        return;
    }
}
