//
// AbstractRoomConstructionBatchProxyManager.java
//
//     An adapter for a RoomConstructionBatchProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.room.construction.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a RoomConstructionBatchProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterRoomConstructionBatchProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.room.construction.batch.RoomConstructionBatchProxyManager>
    implements org.osid.room.construction.batch.RoomConstructionBatchProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterRoomConstructionBatchProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterRoomConstructionBatchProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterRoomConstructionBatchProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterRoomConstructionBatchProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if bulk administration of renovations is available. 
     *
     *  @return <code> true </code> if a renovation bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRenovationBatchAdmin() {
        return (getAdapteeManager().supportsRenovationBatchAdmin());
    }


    /**
     *  Tests if bulk administration of projects is available. 
     *
     *  @return <code> true </code> if a project bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProjectBatchAdmin() {
        return (getAdapteeManager().supportsProjectBatchAdmin());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk room 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RenovationBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRenovationBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.batch.RenovationBatchAdminSession getRenovationBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRenovationBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  renovation administration service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RenovationBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRenovationBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.batch.RenovationBatchAdminSession getRenovationBatchAdminSessionForCampus(org.osid.id.Id campusId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRenovationBatchAdminSessionForCampus(campusId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk project 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProjectBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProjectBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.batch.ProjectBatchAdminSession getProjectBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProjectBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk project 
     *  administration service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProjectBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProjectBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.batch.ProjectBatchAdminSession getProjectBatchAdminSessionForCampus(org.osid.id.Id campusId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProjectBatchAdminSessionForCampus(campusId, proxy));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
