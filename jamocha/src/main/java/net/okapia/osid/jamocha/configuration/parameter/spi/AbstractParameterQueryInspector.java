//
// AbstractParameterQueryInspector.java
//
//     A template for making a ParameterQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.configuration.parameter.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for parameters.
 */

public abstract class AbstractParameterQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRuleQueryInspector
    implements org.osid.configuration.ParameterQueryInspector {

    private final java.util.Collection<org.osid.configuration.records.ParameterQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the value syntax query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.SyntaxTerm[] getValueSyntaxTerms() {
        return (new org.osid.search.terms.SyntaxTerm[0]);
    }


    /**
     *  Gets the coordinate record type query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.TypeTerm[] getValueCoordinateTypeTerms() {
        return (new org.osid.search.terms.TypeTerm[0]);
    }


    /**
     *  Gets the heading record type query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.TypeTerm[] getValueHeadingTypeTerms() {
        return (new org.osid.search.terms.TypeTerm[0]);
    }


    /**
     *  Gets the object value type query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.TypeTerm[] getValueObjectTypeTerms() {
        return (new org.osid.search.terms.TypeTerm[0]);
    }


    /**
     *  Gets the spatial unit record type query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.TypeTerm[] getValueSpatialUnitRecordTypeTerms() {
        return (new org.osid.search.terms.TypeTerm[0]);
    }


    /**
     *  Gets the version type query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.TypeTerm[] getValueVersionSchemeTerms() {
        return (new org.osid.search.terms.TypeTerm[0]);
    }


    /**
     *  Gets the value query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.configuration.ValueQueryInspector[] getValueTerms() {
        return (new org.osid.configuration.ValueQueryInspector[0]);
    }


    /**
     *  Gets the shuffle query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getValuesShuffledTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the configuration <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getConfigurationIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the configuration query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationQueryInspector[] getConfigurationTerms() {
        return (new org.osid.configuration.ConfigurationQueryInspector[0]);
    }


    /**
     *  Gets the record corresponding to the given parameter query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a parameter implementing the requested record.
     *
     *  @param parameterRecordType a parameter record type
     *  @return the parameter query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>parameterRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(parameterRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.configuration.records.ParameterQueryInspectorRecord getParameterQueryInspectorRecord(org.osid.type.Type parameterRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.configuration.records.ParameterQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(parameterRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(parameterRecordType + " is not supported");
    }


    /**
     *  Adds a record to this parameter query. 
     *
     *  @param parameterQueryInspectorRecord parameter query inspector
     *         record
     *  @param parameterRecordType parameter record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addParameterQueryInspectorRecord(org.osid.configuration.records.ParameterQueryInspectorRecord parameterQueryInspectorRecord, 
                                                   org.osid.type.Type parameterRecordType) {

        addRecordType(parameterRecordType);
        nullarg(parameterRecordType, "parameter record type");
        this.records.add(parameterQueryInspectorRecord);        
        return;
    }
}
