//
// AbstractImmutableInquiry.java
//
//     Wraps a mutable Inquiry to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.inquiry.inquiry.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Inquiry</code> to hide modifiers. This
 *  wrapper provides an immutized Inquiry from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying inquiry whose state changes are visible.
 */

public abstract class AbstractImmutableInquiry
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRule
    implements org.osid.inquiry.Inquiry {

    private final org.osid.inquiry.Inquiry inquiry;


    /**
     *  Constructs a new <code>AbstractImmutableInquiry</code>.
     *
     *  @param inquiry the inquiry to immutablize
     *  @throws org.osid.NullArgumentException <code>inquiry</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableInquiry(org.osid.inquiry.Inquiry inquiry) {
        super(inquiry);
        this.inquiry = inquiry;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the audit. 
     *
     *  @return the audit <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAuditId() {
        return (this.inquiry.getAuditId());
    }


    /**
     *  Gets the audit. 
     *
     *  @return the audit 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.inquiry.Audit getAudit()
        throws org.osid.OperationFailedException {

        return (this.inquiry.getAudit());
    }


    /**
     *  Gets the question. 
     *
     *  @return the question 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getQuestion() {
        return (this.inquiry.getQuestion());
    }


    /**
     *  Tests if a <code> Response </code> is required for this <code> 
     *  Inquiry. </code> 
     *
     *  @return <code> true </code> if a response is required, false if 
     *          optional 
     */

    @OSID @Override
    public boolean isRequired() {
        return (this.inquiry.isRequired());
    }


    /**
     *  Tests if a positive <code> Response </code> is required for this 
     *  <code> Inquiry </code> to pass the <code> Audit. </code> 
     *
     *  @return <code> true </code> if a positive response is required, false 
     *          if optional 
     */

    @OSID @Override
    public boolean isAffirmationRequired() {
        return (this.inquiry.isAffirmationRequired());
    }


    /**
     *  Tests if a single effective <code> Response </code> is required. 
     *
     *  @return <code> true </code> if a single effective response is 
     *          required, false responses should be collected for every 
     *          inquiry 
     */

    @OSID @Override
    public boolean needsOneResponse() {
        return (this.inquiry.needsOneResponse());
    }


    /**
     *  Gets the inquiry record corresponding to the given <code> Inquiry 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record. The <code> inquiryRecordType 
     *  </code> may be the <code> Type </code> returned in <code> 
     *  getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(inquiryRecordType) 
     *  </code> is <code> true </code> . 
     *
     *  @param  inquiryRecordType the type of inquiry record to retrieve 
     *  @return the inquiry record 
     *  @throws org.osid.NullArgumentException <code> inquiryRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(inquiryRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inquiry.records.InquiryRecord getInquiryRecord(org.osid.type.Type inquiryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.inquiry.getInquiryRecord(inquiryRecordType));
    }
}

