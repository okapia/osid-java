//
// AbstractMapIntersectionLookupSession
//
//    A simple framework for providing an Intersection lookup service
//    backed by a fixed collection of intersections.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping.path.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an Intersection lookup service backed by a
 *  fixed collection of intersections. The intersections are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Intersections</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapIntersectionLookupSession
    extends net.okapia.osid.jamocha.mapping.path.spi.AbstractIntersectionLookupSession
    implements org.osid.mapping.path.IntersectionLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.mapping.path.Intersection> intersections = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.mapping.path.Intersection>());


    /**
     *  Makes an <code>Intersection</code> available in this session.
     *
     *  @param  intersection an intersection
     *  @throws org.osid.NullArgumentException <code>intersection<code>
     *          is <code>null</code>
     */

    protected void putIntersection(org.osid.mapping.path.Intersection intersection) {
        this.intersections.put(intersection.getId(), intersection);
        return;
    }


    /**
     *  Makes an array of intersections available in this session.
     *
     *  @param  intersections an array of intersections
     *  @throws org.osid.NullArgumentException <code>intersections<code>
     *          is <code>null</code>
     */

    protected void putIntersections(org.osid.mapping.path.Intersection[] intersections) {
        putIntersections(java.util.Arrays.asList(intersections));
        return;
    }


    /**
     *  Makes a collection of intersections available in this session.
     *
     *  @param  intersections a collection of intersections
     *  @throws org.osid.NullArgumentException <code>intersections<code>
     *          is <code>null</code>
     */

    protected void putIntersections(java.util.Collection<? extends org.osid.mapping.path.Intersection> intersections) {
        for (org.osid.mapping.path.Intersection intersection : intersections) {
            this.intersections.put(intersection.getId(), intersection);
        }

        return;
    }


    /**
     *  Removes an Intersection from this session.
     *
     *  @param  intersectionId the <code>Id</code> of the intersection
     *  @throws org.osid.NullArgumentException <code>intersectionId<code> is
     *          <code>null</code>
     */

    protected void removeIntersection(org.osid.id.Id intersectionId) {
        this.intersections.remove(intersectionId);
        return;
    }


    /**
     *  Gets the <code>Intersection</code> specified by its <code>Id</code>.
     *
     *  @param  intersectionId <code>Id</code> of the <code>Intersection</code>
     *  @return the intersection
     *  @throws org.osid.NotFoundException <code>intersectionId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>intersectionId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.Intersection getIntersection(org.osid.id.Id intersectionId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.mapping.path.Intersection intersection = this.intersections.get(intersectionId);
        if (intersection == null) {
            throw new org.osid.NotFoundException("intersection not found: " + intersectionId);
        }

        return (intersection);
    }


    /**
     *  Gets all <code>Intersections</code>. In plenary mode, the returned
     *  list contains all known intersections or an error
     *  results. Otherwise, the returned list may contain only those
     *  intersections that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Intersections</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionList getIntersections()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.mapping.path.intersection.ArrayIntersectionList(this.intersections.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.intersections.clear();
        super.close();
        return;
    }
}
