//
// MutableMapProxyQueueConstrainerEnablerLookupSession
//
//    Implements a QueueConstrainerEnabler lookup service backed by a collection of
//    queueConstrainerEnablers that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.tracking.rules;


/**
 *  Implements a QueueConstrainerEnabler lookup service backed by a collection of
 *  queueConstrainerEnablers. The queueConstrainerEnablers are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of queue constrainer enablers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyQueueConstrainerEnablerLookupSession
    extends net.okapia.osid.jamocha.core.tracking.rules.spi.AbstractMapQueueConstrainerEnablerLookupSession
    implements org.osid.tracking.rules.QueueConstrainerEnablerLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyQueueConstrainerEnablerLookupSession}
     *  with no queue constrainer enablers.
     *
     *  @param frontOffice the front office
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code frontOffice} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyQueueConstrainerEnablerLookupSession(org.osid.tracking.FrontOffice frontOffice,
                                                  org.osid.proxy.Proxy proxy) {
        setFrontOffice(frontOffice);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyQueueConstrainerEnablerLookupSession} with a
     *  single queue constrainer enabler.
     *
     *  @param frontOffice the front office
     *  @param queueConstrainerEnabler a queue constrainer enabler
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code frontOffice},
     *          {@code queueConstrainerEnabler}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyQueueConstrainerEnablerLookupSession(org.osid.tracking.FrontOffice frontOffice,
                                                org.osid.tracking.rules.QueueConstrainerEnabler queueConstrainerEnabler, org.osid.proxy.Proxy proxy) {
        this(frontOffice, proxy);
        putQueueConstrainerEnabler(queueConstrainerEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyQueueConstrainerEnablerLookupSession} using an
     *  array of queue constrainer enablers.
     *
     *  @param frontOffice the front office
     *  @param queueConstrainerEnablers an array of queue constrainer enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code frontOffice},
     *          {@code queueConstrainerEnablers}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyQueueConstrainerEnablerLookupSession(org.osid.tracking.FrontOffice frontOffice,
                                                org.osid.tracking.rules.QueueConstrainerEnabler[] queueConstrainerEnablers, org.osid.proxy.Proxy proxy) {
        this(frontOffice, proxy);
        putQueueConstrainerEnablers(queueConstrainerEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyQueueConstrainerEnablerLookupSession} using a
     *  collection of queue constrainer enablers.
     *
     *  @param frontOffice the front office
     *  @param queueConstrainerEnablers a collection of queue constrainer enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code frontOffice},
     *          {@code queueConstrainerEnablers}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyQueueConstrainerEnablerLookupSession(org.osid.tracking.FrontOffice frontOffice,
                                                java.util.Collection<? extends org.osid.tracking.rules.QueueConstrainerEnabler> queueConstrainerEnablers,
                                                org.osid.proxy.Proxy proxy) {
   
        this(frontOffice, proxy);
        setSessionProxy(proxy);
        putQueueConstrainerEnablers(queueConstrainerEnablers);
        return;
    }

    
    /**
     *  Makes a {@code QueueConstrainerEnabler} available in this session.
     *
     *  @param queueConstrainerEnabler an queue constrainer enabler
     *  @throws org.osid.NullArgumentException {@code queueConstrainerEnabler{@code 
     *          is {@code null}
     */

    @Override
    public void putQueueConstrainerEnabler(org.osid.tracking.rules.QueueConstrainerEnabler queueConstrainerEnabler) {
        super.putQueueConstrainerEnabler(queueConstrainerEnabler);
        return;
    }


    /**
     *  Makes an array of queueConstrainerEnablers available in this session.
     *
     *  @param queueConstrainerEnablers an array of queue constrainer enablers
     *  @throws org.osid.NullArgumentException {@code queueConstrainerEnablers{@code 
     *          is {@code null}
     */

    @Override
    public void putQueueConstrainerEnablers(org.osid.tracking.rules.QueueConstrainerEnabler[] queueConstrainerEnablers) {
        super.putQueueConstrainerEnablers(queueConstrainerEnablers);
        return;
    }


    /**
     *  Makes collection of queue constrainer enablers available in this session.
     *
     *  @param queueConstrainerEnablers
     *  @throws org.osid.NullArgumentException {@code queueConstrainerEnabler{@code 
     *          is {@code null}
     */

    @Override
    public void putQueueConstrainerEnablers(java.util.Collection<? extends org.osid.tracking.rules.QueueConstrainerEnabler> queueConstrainerEnablers) {
        super.putQueueConstrainerEnablers(queueConstrainerEnablers);
        return;
    }


    /**
     *  Removes a QueueConstrainerEnabler from this session.
     *
     *  @param queueConstrainerEnablerId the {@code Id} of the queue constrainer enabler
     *  @throws org.osid.NullArgumentException {@code queueConstrainerEnablerId{@code  is
     *          {@code null}
     */

    @Override
    public void removeQueueConstrainerEnabler(org.osid.id.Id queueConstrainerEnablerId) {
        super.removeQueueConstrainerEnabler(queueConstrainerEnablerId);
        return;
    }    
}
