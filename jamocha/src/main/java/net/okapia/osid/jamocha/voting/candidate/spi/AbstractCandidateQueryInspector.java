//
// AbstractCandidateQueryInspector.java
//
//     A template for making a CandidateQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.candidate.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for candidates.
 */

public abstract class AbstractCandidateQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQueryInspector
    implements org.osid.voting.CandidateQueryInspector {

    private final java.util.Collection<org.osid.voting.records.CandidateQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the race <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRaceIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the race query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.voting.RaceQueryInspector[] getRaceTerms() {
        return (new org.osid.voting.RaceQueryInspector[0]);
    }


    /**
     *  Gets the resource <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResourceIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the resource query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getResourceTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the vote <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getVoteIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the vote query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.voting.VoteQueryInspector[] getVoteTerms() {
        return (new org.osid.voting.VoteQueryInspector[0]);
    }


    /**
     *  Gets the polls <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPollsIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the polls query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.voting.PollsQueryInspector[] getPollsTerms() {
        return (new org.osid.voting.PollsQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given candidate query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a candidate implementing the requested record.
     *
     *  @param candidateRecordType a candidate record type
     *  @return the candidate query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>candidateRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(candidateRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.records.CandidateQueryInspectorRecord getCandidateQueryInspectorRecord(org.osid.type.Type candidateRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.records.CandidateQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(candidateRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(candidateRecordType + " is not supported");
    }


    /**
     *  Adds a record to this candidate query. 
     *
     *  @param candidateQueryInspectorRecord candidate query inspector
     *         record
     *  @param candidateRecordType candidate record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCandidateQueryInspectorRecord(org.osid.voting.records.CandidateQueryInspectorRecord candidateQueryInspectorRecord, 
                                                   org.osid.type.Type candidateRecordType) {

        addRecordType(candidateRecordType);
        nullarg(candidateRecordType, "candidate record type");
        this.records.add(candidateQueryInspectorRecord);        
        return;
    }
}
