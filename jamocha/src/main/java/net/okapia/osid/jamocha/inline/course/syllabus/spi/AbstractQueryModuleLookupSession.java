//
// AbstractQueryModuleLookupSession.java
//
//    An inline adapter that maps a ModuleLookupSession to
//    a ModuleQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.course.syllabus.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a ModuleLookupSession to
 *  a ModuleQuerySession.
 */

public abstract class AbstractQueryModuleLookupSession
    extends net.okapia.osid.jamocha.course.syllabus.spi.AbstractModuleLookupSession
    implements org.osid.course.syllabus.ModuleLookupSession {

    private boolean activeonly    = false;
    private final org.osid.course.syllabus.ModuleQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryModuleLookupSession.
     *
     *  @param querySession the underlying module query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryModuleLookupSession(org.osid.course.syllabus.ModuleQuerySession querySession) {
        nullarg(querySession, "module query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>CourseCatalog</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>CourseCatalog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.session.getCourseCatalogId());
    }


    /**
     *  Gets the <code>CourseCatalog</code> associated with this 
     *  session.
     *
     *  @return the <code>CourseCatalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getCourseCatalog());
    }


    /**
     *  Tests if this user can perform <code>Module</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupModules() {
        return (this.session.canSearchModules());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include modules in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.session.useFederatedCourseCatalogView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.session.useIsolatedCourseCatalogView();
        return;
    }
    

    /**
     *  Only active modules are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveModuleView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive modules are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusModuleView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>Module</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Module</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Module</code> and
     *  retained for compatibility.
     *
     *  In active mode, modules are returned that are currently
     *  active. In any status mode, active and inactive modules
     *  are returned.
     *
     *  @param  moduleId <code>Id</code> of the
     *          <code>Module</code>
     *  @return the module
     *  @throws org.osid.NotFoundException <code>moduleId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>moduleId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.Module getModule(org.osid.id.Id moduleId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.syllabus.ModuleQuery query = getQuery();
        query.matchId(moduleId, true);
        org.osid.course.syllabus.ModuleList modules = this.session.getModulesByQuery(query);
        if (modules.hasNext()) {
            return (modules.getNextModule());
        } 
        
        throw new org.osid.NotFoundException(moduleId + " not found");
    }


    /**
     *  Gets a <code>ModuleList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  modules specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Modules</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, modules are returned that are currently
     *  active. In any status mode, active and inactive modules
     *  are returned.
     *
     *  @param  moduleIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Module</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>moduleIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleList getModulesByIds(org.osid.id.IdList moduleIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.syllabus.ModuleQuery query = getQuery();

        try (org.osid.id.IdList ids = moduleIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getModulesByQuery(query));
    }


    /**
     *  Gets a <code>ModuleList</code> corresponding to the given
     *  module genus <code>Type</code> which does not include
     *  modules of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  modules or an error results. Otherwise, the returned list
     *  may contain only those modules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, modules are returned that are currently
     *  active. In any status mode, active and inactive modules
     *  are returned.
     *
     *  @param  moduleGenusType a module genus type 
     *  @return the returned <code>Module</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>moduleGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleList getModulesByGenusType(org.osid.type.Type moduleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.syllabus.ModuleQuery query = getQuery();
        query.matchGenusType(moduleGenusType, true);
        return (this.session.getModulesByQuery(query));
    }


    /**
     *  Gets a <code>ModuleList</code> corresponding to the given
     *  module genus <code>Type</code> and include any additional
     *  modules with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  modules or an error results. Otherwise, the returned list
     *  may contain only those modules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, modules are returned that are currently
     *  active. In any status mode, active and inactive modules
     *  are returned.
     *
     *  @param  moduleGenusType a module genus type 
     *  @return the returned <code>Module</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>moduleGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleList getModulesByParentGenusType(org.osid.type.Type moduleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.syllabus.ModuleQuery query = getQuery();
        query.matchParentGenusType(moduleGenusType, true);
        return (this.session.getModulesByQuery(query));
    }


    /**
     *  Gets a <code>ModuleList</code> containing the given
     *  module record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  modules or an error results. Otherwise, the returned list
     *  may contain only those modules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, modules are returned that are currently
     *  active. In any status mode, active and inactive modules
     *  are returned.
     *
     *  @param  moduleRecordType a module record type 
     *  @return the returned <code>Module</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>moduleRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleList getModulesByRecordType(org.osid.type.Type moduleRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.syllabus.ModuleQuery query = getQuery();
        query.matchRecordType(moduleRecordType, true);
        return (this.session.getModulesByQuery(query));
    }


    /**
     *  Gets a <code>ModuleList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known modules or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  modules that are accessible through this session. 
     *
     *  In active mode, modules are returned that are currently
     *  active. In any status mode, active and inactive modules
     *  are returned.
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Module</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleList getModulesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.syllabus.ModuleQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getModulesByQuery(query));        
    }

    
    /**
     *  Gets all <code>Modules</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  modules or an error results. Otherwise, the returned list
     *  may contain only those modules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, modules are returned that are currently
     *  active. In any status mode, active and inactive modules
     *  are returned.
     *
     *  @return a list of <code>Modules</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleList getModules()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.syllabus.ModuleQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getModulesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.course.syllabus.ModuleQuery getQuery() {
        org.osid.course.syllabus.ModuleQuery query = this.session.getModuleQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
