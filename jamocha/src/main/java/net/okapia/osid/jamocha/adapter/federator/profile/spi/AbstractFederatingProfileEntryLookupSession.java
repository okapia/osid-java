//
// AbstractFederatingProfileEntryLookupSession.java
//
//     An abstract federating adapter for a ProfileEntryLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.profile.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  ProfileEntryLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingProfileEntryLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.profile.ProfileEntryLookupSession>
    implements org.osid.profile.ProfileEntryLookupSession {

    private boolean parallel = false;
    private org.osid.profile.Profile profile = new net.okapia.osid.jamocha.nil.profile.profile.UnknownProfile();


    /**
     *  Constructs a new <code>AbstractFederatingProfileEntryLookupSession</code>.
     */

    protected AbstractFederatingProfileEntryLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.profile.ProfileEntryLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Profile/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Profile Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getProfileId() {
        return (this.profile.getId());
    }


    /**
     *  Gets the <code>Profile</code> associated with this 
     *  session.
     *
     *  @return the <code>Profile</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.Profile getProfile()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.profile);
    }


    /**
     *  Sets the <code>Profile</code>.
     *
     *  @param  profile the profile for this session
     *  @throws org.osid.NullArgumentException <code>profile</code>
     *          is <code>null</code>
     */

    protected void setProfile(org.osid.profile.Profile profile) {
        nullarg(profile, "profile");
        this.profile = profile;
        return;
    }


    /**
     *  Tests if this user can perform <code>ProfileEntry</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupProfileEntries() {
        for (org.osid.profile.ProfileEntryLookupSession session : getSessions()) {
            if (session.canLookupProfileEntries()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>ProfileEntry</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeProfileEntryView() {
        for (org.osid.profile.ProfileEntryLookupSession session : getSessions()) {
            session.useComparativeProfileEntryView();
        }

        return;
    }


    /**
     *  A complete view of the <code>ProfileEntry</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryProfileEntryView() {
        for (org.osid.profile.ProfileEntryLookupSession session : getSessions()) {
            session.usePlenaryProfileEntryView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include profile entries in profiles which are children
     *  of this profile in the profile hierarchy.
     */

    @OSID @Override
    public void useFederatedProfileView() {
        for (org.osid.profile.ProfileEntryLookupSession session : getSessions()) {
            session.useFederatedProfileView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this profile only.
     */

    @OSID @Override
    public void useIsolatedProfileView() {
        for (org.osid.profile.ProfileEntryLookupSession session : getSessions()) {
            session.useIsolatedProfileView();
        }

        return;
    }


    /**
     *  Only profile entries whose effective dates are current are returned by
     *  methods in this session.
     */

    @OSID @Override
    public void useEffectiveProfileEntryView() {
        for (org.osid.profile.ProfileEntryLookupSession session : getSessions()) {
            session.useEffectiveProfileEntryView();
        }

        return;
    }


    /**
     *  All profile entries of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveProfileEntryView() {
        for (org.osid.profile.ProfileEntryLookupSession session : getSessions()) {
            session.useAnyEffectiveProfileEntryView();
        }

        return;
    }

    
    /**
     *  Sets the view for methods in this session to implicit profile
     *  entries.  An implicit view will include profile entries
     *  derived from other profile entries. This method is the
     *  opposite of <code> explicitProfileEntryView()</code>.
     */

    @OSID @Override
    public void useImplicitProfileEntryView() {
        for (org.osid.profile.ProfileEntryLookupSession session : getSessions()) {
            session.useImplicitProfileEntryView();
        }

        return;
    }


    /**
     *  Sets the view for methods in this session to explicit profile
     *  entries.  An explicit view includes only those profile entries
     *  that were explicitly defined and not implied. This method is
     *  the opposite of <code> implicitProfileEntryView(). </code>
     */

    @OSID @Override
    public void useExplicitProfileEntryView() {
        for (org.osid.profile.ProfileEntryLookupSession session : getSessions()) {
            session.useExplicitProfileEntryView();
        }

        return;
    }


    /**
     *  Include profile entries of any agent of a resource when
     *  looking up profile entries by resource.
     */

    @OSID @Override
    public void useImplicitResourceView() {
        for (org.osid.profile.ProfileEntryLookupSession session : getSessions()) {
            session.useImplicitResourceView();
        }

        return;
    }


    /**
     *  Only include profile entries explicitly mapped to the given
     *  resource when looking up profile entries by resource.
     */

    @OSID @Override
    public void useExplicitResourceView() {
        for (org.osid.profile.ProfileEntryLookupSession session : getSessions()) {
            session.useExplicitResourceView();
        }

        return;
    }

     
    /**
     *  Gets the <code>ProfileEntry</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>ProfileEntry</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>ProfileEntry</code> and
     *  retained for compatibility.
     *
     *  In effective mode, profile entries are returned that are currently
     *  effective.  In any effective mode, effective profile entries and
     *  those currently expired are returned.
     *
     *  @param  profileEntryId <code>Id</code> of the
     *          <code>ProfileEntry</code>
     *  @return the profile entry
     *  @throws org.osid.NotFoundException <code>profileEntryId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>profileEntryId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntry getProfileEntry(org.osid.id.Id profileEntryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.profile.ProfileEntryLookupSession session : getSessions()) {
            try {
                return (session.getProfileEntry(profileEntryId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(profileEntryId + " not found");
    }


    /**
     *  Gets a <code>ProfileEntryList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  profileEntries specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>ProfileEntries</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, profile entries are returned that are
     *  currently effective.  In any effective mode, effective profile
     *  entries and those currently expired are returned.
     *
     *  @param  profileEntryIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>ProfileEntry</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>profileEntryIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesByIds(org.osid.id.IdList profileEntryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.profile.profileentry.MutableProfileEntryList ret = new net.okapia.osid.jamocha.profile.profileentry.MutableProfileEntryList();

        try (org.osid.id.IdList ids = profileEntryIds) {
            while (ids.hasNext()) {
                ret.addProfileEntry(getProfileEntry(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>ProfileEntryList</code> corresponding to the given
     *  profile entry genus <code>Type</code> which does not include
     *  profile entries of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  profile entries or an error results. Otherwise, the returned list
     *  may contain only those profile entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, profile entries are returned that are
     *  currently effective.  In any effective mode, effective profile
     *  entries and those currently expired are returned.
     *
     *  @param  profileEntryGenusType a profileEntry genus type 
     *  @return the returned <code>ProfileEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>profileEntryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesByGenusType(org.osid.type.Type profileEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.profile.profileentry.FederatingProfileEntryList ret = getProfileEntryList();

        for (org.osid.profile.ProfileEntryLookupSession session : getSessions()) {
            ret.addProfileEntryList(session.getProfileEntriesByGenusType(profileEntryGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ProfileEntryList</code> corresponding to the given
     *  profile entry genus <code>Type</code> and include any additional
     *  profile entries with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  profile entries or an error results. Otherwise, the returned list
     *  may contain only those profile entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, profile entries are returned that are
     *  currently effective.  In any effective mode, effective profile
     *  entries and those currently expired are returned.
     *
     *  @param  profileEntryGenusType a profileEntry genus type 
     *  @return the returned <code>ProfileEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>profileEntryGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesByParentGenusType(org.osid.type.Type profileEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.profile.profileentry.FederatingProfileEntryList ret = getProfileEntryList();

        for (org.osid.profile.ProfileEntryLookupSession session : getSessions()) {
            ret.addProfileEntryList(session.getProfileEntriesByParentGenusType(profileEntryGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ProfileEntryList</code> containing the given
     *  profile entry record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  profile entries or an error results. Otherwise, the returned list
     *  may contain only those profile entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, profile entries are returned that are
     *  currently effective.  In any effective mode, effective profile
     *  entries and those currently expired are returned.
     *
     *  @param  profileEntryRecordType a profileEntry record type 
     *  @return the returned <code>ProfileEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>profileEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesByRecordType(org.osid.type.Type profileEntryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.profile.profileentry.FederatingProfileEntryList ret = getProfileEntryList();

        for (org.osid.profile.ProfileEntryLookupSession session : getSessions()) {
            ret.addProfileEntryList(session.getProfileEntriesByRecordType(profileEntryRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ProfileEntryList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  profile entries or an error results. Otherwise, the returned list
     *  may contain only those profile entries that are accessible
     *  through this session.
     *  
     *  In effective mode, profile entries are returned that are
     *  currently effective.  In any effective mode, effective profile
     *  entries and those currently expired are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>ProfileEntry</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesOnDate(org.osid.calendaring.DateTime from, 
                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.profile.profileentry.FederatingProfileEntryList ret = getProfileEntryList();

        for (org.osid.profile.ProfileEntryLookupSession session : getSessions()) {
            ret.addProfileEntryList(session.getProfileEntriesOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a list of profile entries corresponding to a resource
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  profile entries or an error results. Otherwise, the returned list
     *  may contain only those profile entries that are accessible
     *  through this session.
     *
     *  In effective mode, profile entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  profile entries and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @return the returned <code>ProfileEntryList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.profile.ProfileEntryList getProfileEntriesForResource(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.profile.profileentry.FederatingProfileEntryList ret = getProfileEntryList();

        for (org.osid.profile.ProfileEntryLookupSession session : getSessions()) {
            ret.addProfileEntryList(session.getProfileEntriesForResource(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of profile entries corresponding to a resource
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  profile entries or an error results. Otherwise, the returned list
     *  may contain only those profile entries that are accessible
     *  through this session.
     *
     *  In effective mode, profile entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  profile entries and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ProfileEntryList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesForResourceOnDate(org.osid.id.Id resourceId,
                                                                                org.osid.calendaring.DateTime from,
                                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.profile.profileentry.FederatingProfileEntryList ret = getProfileEntryList();

        for (org.osid.profile.ProfileEntryLookupSession session : getSessions()) {
            ret.addProfileEntryList(session.getProfileEntriesForResourceOnDate(resourceId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of profile entries corresponding to an agent
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known profile
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those profile entries that are accessible through
     *  this session.
     *
     *  In effective mode, profile entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  profile entries and those currently expired are returned.
     *
     *  @param  agentId the <code>Id</code> of the agent
     *  @return the returned <code>ProfileEntryList</code>
     *  @throws org.osid.NullArgumentException <code>agentId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.profile.ProfileEntryList getProfileEntriesForAgent(org.osid.id.Id agentId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.profile.profileentry.FederatingProfileEntryList ret = getProfileEntryList();

        for (org.osid.profile.ProfileEntryLookupSession session : getSessions()) {
            ret.addProfileEntryList(session.getProfileEntriesForAgent(agentId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of profile entries corresponding to an agent
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  profile entries or an error results. Otherwise, the returned list
     *  may contain only those profile entries that are accessible
     *  through this session.
     *
     *  In effective mode, profile entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  profile entries and those currently expired are returned.
     *
     *  @param  agentId the <code>Id</code> of the agent
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ProfileEntryList</code>
     *  @throws org.osid.NullArgumentException <code>agentId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesForAgentOnDate(org.osid.id.Id agentId,
                                                                             org.osid.calendaring.DateTime from,
                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.profile.profileentry.FederatingProfileEntryList ret = getProfileEntryList();

        for (org.osid.profile.ProfileEntryLookupSession session : getSessions()) {
            ret.addProfileEntryList(session.getProfileEntriesForAgentOnDate(agentId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of profile entries corresponding to a profile item
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  profile entries or an error results. Otherwise, the returned list
     *  may contain only those profile entries that are accessible
     *  through this session.
     *
     *  In effective mode, profile entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  profile entries and those currently expired are returned.
     *
     *  @param  profileItemId the <code>Id</code> of the profile item
     *  @return the returned <code>ProfileEntryList</code>
     *  @throws org.osid.NullArgumentException <code>profileItemId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.profile.ProfileEntryList getProfileEntriesForProfileItem(org.osid.id.Id profileItemId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.profile.profileentry.FederatingProfileEntryList ret = getProfileEntryList();

        for (org.osid.profile.ProfileEntryLookupSession session : getSessions()) {
            ret.addProfileEntryList(session.getProfileEntriesForProfileItem(profileItemId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of profile entries corresponding to a profile item
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  profile entries or an error results. Otherwise, the returned list
     *  may contain only those profile entries that are accessible
     *  through this session.
     *
     *  In effective mode, profile entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  profile entries and those currently expired are returned.
     *
     *  @param  profileItemId the <code>Id</code> of the profile item
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ProfileEntryList</code>
     *  @throws org.osid.NullArgumentException <code>profileItemId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesForProfileItemOnDate(org.osid.id.Id profileItemId,
                                                                                   org.osid.calendaring.DateTime from,
                                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.profile.profileentry.FederatingProfileEntryList ret = getProfileEntryList();

        for (org.osid.profile.ProfileEntryLookupSession session : getSessions()) {
            ret.addProfileEntryList(session.getProfileEntriesForProfileItemOnDate(profileItemId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of profile entries corresponding to resource and
     *  profile item <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  profile entries or an error results. Otherwise, the returned list
     *  may contain only those profile entries that are accessible
     *  through this session.
     *
     *  In effective mode, profile entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  profile entries and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  profileItemId the <code>Id</code> of the profile item
     *  @return the returned <code>ProfileEntryList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>profileItemId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesForResourceAndProfileItem(org.osid.id.Id resourceId,
                                                                                        org.osid.id.Id profileItemId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.profile.profileentry.FederatingProfileEntryList ret = getProfileEntryList();

        for (org.osid.profile.ProfileEntryLookupSession session : getSessions()) {
            ret.addProfileEntryList(session.getProfileEntriesForResourceAndProfileItem(resourceId, profileItemId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of profile entries corresponding to resource and profile item
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  profile entries or an error results. Otherwise, the returned list
     *  may contain only those profile entries that are accessible
     *  through this session.
     *
     *  In effective mode, profile entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  profile entries and those currently expired are returned.
     *
     *  @param  profileItemId the <code>Id</code> of the profile item
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ProfileEntryList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>profileItemId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesForResourceAndProfileItemOnDate(org.osid.id.Id resourceId,
                                                                                              org.osid.id.Id profileItemId,
                                                                                              org.osid.calendaring.DateTime from,
                                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.profile.profileentry.FederatingProfileEntryList ret = getProfileEntryList();

        for (org.osid.profile.ProfileEntryLookupSession session : getSessions()) {
            ret.addProfileEntryList(session.getProfileEntriesForResourceAndProfileItemOnDate(resourceId, profileItemId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of profile entries corresponding to agent and
     *  profile item <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  profile entries or an error results. Otherwise, the returned list
     *  may contain only those profile entries that are accessible
     *  through this session.
     *
     *  In effective mode, profile entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  profile entries and those currently expired are returned.
     *
     *  @param  agentId the <code>Id</code> of the agent
     *  @param  profileItemId the <code>Id</code> of the profile item
     *  @return the returned <code>ProfileEntryList</code>
     *  @throws org.osid.NullArgumentException <code>agentId</code>,
     *          <code>profileItemId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesForAgentAndProfileItem(org.osid.id.Id agentId,
                                                                                     org.osid.id.Id profileItemId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.profile.profileentry.FederatingProfileEntryList ret = getProfileEntryList();

        for (org.osid.profile.ProfileEntryLookupSession session : getSessions()) {
            ret.addProfileEntryList(session.getProfileEntriesForAgentAndProfileItem(agentId, profileItemId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of profile entries corresponding to agent and
     *  profile item <code>Ids</code> and effective during the entire
     *  given date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  profile entries or an error results. Otherwise, the returned list
     *  may contain only those profile entries that are accessible
     *  through this session.
     *
     *  In effective mode, profile entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  profile entries and those currently expired are returned.
     *
     *  @param  profileItemId the <code>Id</code> of the profile item
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ProfileEntryList</code>
     *  @throws org.osid.NullArgumentException <code>agentId</code>,
     *          <code>profileItemId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesForAgentAndProfileItemOnDate(org.osid.id.Id agentId,
                                                                                           org.osid.id.Id profileItemId,
                                                                                           org.osid.calendaring.DateTime from,
                                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.profile.profileentry.FederatingProfileEntryList ret = getProfileEntryList();

        for (org.osid.profile.ProfileEntryLookupSession session : getSessions()) {
            ret.addProfileEntryList(session.getProfileEntriesForAgentAndProfileItemOnDate(agentId, profileItemId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>ProfileEntries</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  profile entries or an error results. Otherwise, the returned list
     *  may contain only those profile entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, profile entries are returned that are currently
     *  effective.  In any effective mode, effective profile entries and
     *  those currently expired are returned.
     *
     *  @return a list of <code>ProfileEntries</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.profile.profileentry.FederatingProfileEntryList ret = getProfileEntryList();

        for (org.osid.profile.ProfileEntryLookupSession session : getSessions()) {
            ret.addProfileEntryList(session.getProfileEntries());
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets the explicit <code>ProfileEntry</code> that generated the
     *  given implicit profile entry. If the given
     *  <code>ProfileEntry</code> is explicit, then the same
     *  <code>ProfileEntry</code> is returned.
     *
     *  @param  profileEntryId a profile entry
     *  @return the explicit <code>ProfileEntry</code>
     *  @throws org.osid.NotFoundException <code>profileEntryId</code> is
     *          not found
     *  @throws org.osid.NullArgumentException
     *          <code>profileEntryId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.profile.ProfileEntry getExplicitProfileEntry(org.osid.id.Id profileEntryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.profile.ProfileEntryLookupSession session : getSessions()) {
            try {
                return (session.getExplicitProfileEntry(profileEntryId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(profileEntryId + " not found");
    }


    protected net.okapia.osid.jamocha.adapter.federator.profile.profileentry.FederatingProfileEntryList getProfileEntryList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.profile.profileentry.ParallelProfileEntryList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.profile.profileentry.CompositeProfileEntryList());
        }
    }
}
