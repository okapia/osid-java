//
// AbstractMapResourceLookupSession
//
//    A simple framework for providing a Resource lookup service
//    backed by a fixed collection of resources.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resource.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Resource lookup service backed by a
 *  fixed collection of resources. The resources are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Resources</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapResourceLookupSession
    extends net.okapia.osid.jamocha.resource.spi.AbstractResourceLookupSession
    implements org.osid.resource.ResourceLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.resource.Resource> resources = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.resource.Resource>());


    /**
     *  Makes a <code>Resource</code> available in this session.
     *
     *  @param  resource a resource
     *  @throws org.osid.NullArgumentException <code>resource<code>
     *          is <code>null</code>
     */

    protected void putResource(org.osid.resource.Resource resource) {
        this.resources.put(resource.getId(), resource);
        return;
    }


    /**
     *  Makes an array of resources available in this session.
     *
     *  @param  resources an array of resources
     *  @throws org.osid.NullArgumentException <code>resources<code>
     *          is <code>null</code>
     */

    protected void putResources(org.osid.resource.Resource[] resources) {
        putResources(java.util.Arrays.asList(resources));
        return;
    }


    /**
     *  Makes a collection of resources available in this session.
     *
     *  @param  resources a collection of resources
     *  @throws org.osid.NullArgumentException <code>resources<code>
     *          is <code>null</code>
     */

    protected void putResources(java.util.Collection<? extends org.osid.resource.Resource> resources) {
        for (org.osid.resource.Resource resource : resources) {
            this.resources.put(resource.getId(), resource);
        }

        return;
    }


    /**
     *  Removes a Resource from this session.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @throws org.osid.NullArgumentException <code>resourceId<code> is
     *          <code>null</code>
     */

    protected void removeResource(org.osid.id.Id resourceId) {
        this.resources.remove(resourceId);
        return;
    }


    /**
     *  Gets the <code>Resource</code> specified by its <code>Id</code>.
     *
     *  @param  resourceId <code>Id</code> of the <code>Resource</code>
     *  @return the resource
     *  @throws org.osid.NotFoundException <code>resourceId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.Resource getResource(org.osid.id.Id resourceId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.resource.Resource resource = this.resources.get(resourceId);
        if (resource == null) {
            throw new org.osid.NotFoundException("resource not found: " + resourceId);
        }

        return (resource);
    }


    /**
     *  Gets all <code>Resources</code>. In plenary mode, the returned
     *  list contains all known resources or an error
     *  results. Otherwise, the returned list may contain only those
     *  resources that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Resources</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getResources()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.resource.resource.ArrayResourceList(this.resources.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.resources.clear();
        super.close();
        return;
    }
}
