//
// AbstractValueEnablerLookupSession.java
//
//    A starter implementation framework for providing a ValueEnabler
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.configuration.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a ValueEnabler
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getValueEnablers(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractValueEnablerLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.configuration.rules.ValueEnablerLookupSession {

    private boolean pedantic      = false;
    private boolean activeonly    = false;
    private boolean federated     = false;
    private org.osid.configuration.Configuration configuration = new net.okapia.osid.jamocha.nil.configuration.configuration.UnknownConfiguration();
    

    /**
     *  Gets the <code>Configuration/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Configuration Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getConfigurationId() {
        return (this.configuration.getId());
    }


    /**
     *  Gets the <code>Configuration</code> associated with this 
     *  session.
     *
     *  @return the <code>Configuration</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.Configuration getConfiguration()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.configuration);
    }


    /**
     *  Sets the <code>Configuration</code>.
     *
     *  @param  configuration the configuration for this session
     *  @throws org.osid.NullArgumentException <code>configuration</code>
     *          is <code>null</code>
     */

    protected void setConfiguration(org.osid.configuration.Configuration configuration) {
        nullarg(configuration, "configuration");
        this.configuration = configuration;
        return;
    }


    /**
     *  Tests if this user can perform <code>ValueEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupValueEnablers() {
        return (true);
    }


    /**
     *  A complete view of the <code>ValueEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeValueEnablerView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>ValueEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryValueEnablerView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include value enablers in configurations which are children
     *  of this configuration in the configuration hierarchy.
     */

    @OSID @Override
    public void useFederatedConfigurationView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this configuration only.
     */

    @OSID @Override
    public void useIsolatedConfigurationView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active value enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveValueEnablerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive value enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusValueEnablerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>ValueEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>ValueEnabler</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>ValueEnabler</code> and
     *  retained for compatibility.
     *
     *  In active mode, value enablers are returned that are currently
     *  active. In any status mode, active and inactive value enablers
     *  are returned.
     *
     *  @param  valueEnablerId <code>Id</code> of the
     *          <code>ValueEnabler</code>
     *  @return the value enabler
     *  @throws org.osid.NotFoundException <code>valueEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>valueEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnabler getValueEnabler(org.osid.id.Id valueEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.configuration.rules.ValueEnablerList valueEnablers = getValueEnablers()) {
            while (valueEnablers.hasNext()) {
                org.osid.configuration.rules.ValueEnabler valueEnabler = valueEnablers.getNextValueEnabler();
                if (valueEnabler.getId().equals(valueEnablerId)) {
                    return (valueEnabler);
                }
            }
        } 

        throw new org.osid.NotFoundException(valueEnablerId + " not found");
    }


    /**
     *  Gets a <code>ValueEnablerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  valueEnablers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>ValueEnablers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, value enablers are returned that are currently
     *  active. In any status mode, active and inactive value enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getValueEnablers()</code>.
     *
     *  @param  valueEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>ValueEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
*               found
     *  @throws org.osid.NullArgumentException
     *          <code>valueEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerList getValueEnablersByIds(org.osid.id.IdList valueEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.configuration.rules.ValueEnabler> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = valueEnablerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getValueEnabler(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("value enabler " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.configuration.rules.valueenabler.LinkedValueEnablerList(ret));
    }


    /**
     *  Gets a <code>ValueEnablerList</code> corresponding to the given
     *  value enabler genus <code>Type</code> which does not include
     *  value enablers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  value enablers or an error results. Otherwise, the returned list
     *  may contain only those value enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, value enablers are returned that are currently
     *  active. In any status mode, active and inactive value enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getValueEnablers()</code>.
     *
     *  @param  valueEnablerGenusType a valueEnabler genus type 
     *  @return the returned <code>ValueEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>valueEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerList getValueEnablersByGenusType(org.osid.type.Type valueEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.configuration.rules.valueenabler.ValueEnablerGenusFilterList(getValueEnablers(), valueEnablerGenusType));
    }


    /**
     *  Gets a <code>ValueEnablerList</code> corresponding to the given
     *  value enabler genus <code>Type</code> and include any additional
     *  value enablers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  value enablers or an error results. Otherwise, the returned list
     *  may contain only those value enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, value enablers are returned that are currently
     *  active. In any status mode, active and inactive value enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getValueEnablers()</code>.
     *
     *  @param  valueEnablerGenusType a valueEnabler genus type 
     *  @return the returned <code>ValueEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>valueEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerList getValueEnablersByParentGenusType(org.osid.type.Type valueEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getValueEnablersByGenusType(valueEnablerGenusType));
    }


    /**
     *  Gets a <code>ValueEnablerList</code> containing the given
     *  value enabler record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  value enablers or an error results. Otherwise, the returned list
     *  may contain only those value enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *

     *  In active mode, value enablers are returned that are currently
     *  active. In any status mode, active and inactive value enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getValueEnablers()</code>.
     *
     *  @param  valueEnablerRecordType a valueEnabler record type 
     *  @return the returned <code>ValueEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>valueEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerList getValueEnablersByRecordType(org.osid.type.Type valueEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.configuration.rules.valueenabler.ValueEnablerRecordFilterList(getValueEnablers(), valueEnablerRecordType));
    }


    /**
     *  Gets a <code>ValueEnablerList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  value enablers or an error results. Otherwise, the returned list
     *  may contain only those value enablers that are accessible
     *  through this session.
     *  
     *  In active mode, value enablers are returned that are currently
     *  active in addition to being effective during the given date
     *  range. In any status mode, active and inactive value enablers are
     *  returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>ValueEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerList getValueEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.configuration.rules.valueenabler.TemporalValueEnablerFilterList(getValueEnablers(), from, to));
    }
        

    /**
     *  Gets a <code>ValueEnablerList </code> which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  value enablers or an error results. Otherwise, the returned list
     *  may contain only those value enablers that are accessible
     *  through this session.
     *
     *  In active mode, value enablers are returned that are currently
     *  active in addition to being effective during the date
     *  range. In any status mode, active and inactive value enablers are
     *  returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned <code>ValueEnabler</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>agent</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerList getValueEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (getValueEnablersOnDate(from, to));
    }


    /**
     *  Gets all <code>ValueEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  value enablers or an error results. Otherwise, the returned list
     *  may contain only those value enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, value enablers are returned that are currently
     *  active. In any status mode, active and inactive value enablers
     *  are returned.
     *
     *  @return a list of <code>ValueEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.configuration.rules.ValueEnablerList getValueEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the value enabler list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of value enablers
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.configuration.rules.ValueEnablerList filterValueEnablersOnViews(org.osid.configuration.rules.ValueEnablerList list)
        throws org.osid.OperationFailedException {

        org.osid.configuration.rules.ValueEnablerList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.configuration.rules.valueenabler.ActiveValueEnablerFilterList(ret);
        }

        return (ret);
    }
}
