//
// AbstractNodeQuery.java
//
//     A template for making a Node Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.topology.node.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for nodes.
 */

public abstract class AbstractNodeQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.topology.NodeQuery {

    private final java.util.Collection<org.osid.topology.records.NodeQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the edge <code> Id </code> for this query to match nodes that 
     *  have a related edge. 
     *
     *  @param  edgeId an edge <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> edgeId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchEdgeId(org.osid.id.Id edgeId, boolean match) {
        return;
    }


    /**
     *  Clears the edge <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearEdgeIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> EdgeQuery </code> is available. 
     *
     *  @return <code> true </code> if an edge query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEdgeQuery() {
        return (false);
    }


    /**
     *  Gets the query for an edge. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the edge query 
     *  @throws org.osid.UnimplementedException <code> supportsEdgeQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.EdgeQuery getEdgeQuery() {
        throw new org.osid.UnimplementedException("supportsEdgeQuery() is false");
    }


    /**
     *  Clears the edge terms. 
     */

    @OSID @Override
    public void clearEdgeTerms() {
        return;
    }


    /**
     *  Sets the graph <code> Id </code> for this query. 
     *
     *  @param  graphId the graph <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGraphId(org.osid.id.Id graphId, boolean match) {
        return;
    }


    /**
     *  Clears the graph <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGraphIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> GraphQuery </code> is available. 
     *
     *  @return <code> true </code> if a graph query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGraphQuery() {
        return (false);
    }


    /**
     *  Gets the query for a graph. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the graph query 
     *  @throws org.osid.UnimplementedException <code> supportsGraphQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.GraphQuery getGraphQuery() {
        throw new org.osid.UnimplementedException("supportsGraphQuery() is false");
    }


    /**
     *  Clears the graph terms. 
     */

    @OSID @Override
    public void clearGraphTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given node query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a node implementing the requested record.
     *
     *  @param nodeRecordType a node record type
     *  @return the node query record
     *  @throws org.osid.NullArgumentException
     *          <code>nodeRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(nodeRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.topology.records.NodeQueryRecord getNodeQueryRecord(org.osid.type.Type nodeRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.topology.records.NodeQueryRecord record : this.records) {
            if (record.implementsRecordType(nodeRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(nodeRecordType + " is not supported");
    }


    /**
     *  Adds a record to this node query. 
     *
     *  @param nodeQueryRecord node query record
     *  @param nodeRecordType node record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addNodeQueryRecord(org.osid.topology.records.NodeQueryRecord nodeQueryRecord, 
                                          org.osid.type.Type nodeRecordType) {

        addRecordType(nodeRecordType);
        nullarg(nodeQueryRecord, "node query record");
        this.records.add(nodeQueryRecord);        
        return;
    }
}
