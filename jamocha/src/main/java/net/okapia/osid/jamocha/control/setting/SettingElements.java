//
// SettingElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.setting;

/**
 *  Ids for object elements for use in forms and queries.
 */

public class SettingElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the SettingElement Id.
     *
     *  @return the setting element Id
     */

    public static org.osid.id.Id getSettingEntityId() {
        return (makeEntityId("osid.control.Setting"));
    }


    /**
     *  Gets the ControllerId element Id.
     *
     *  @return the ControllerId element Id
     */

    public static org.osid.id.Id getControllerId() {
        return (makeElementId("osid.control.setting.ControllerId"));
    }


    /**
     *  Gets the Controller element Id.
     *
     *  @return the Controller element Id
     */

    public static org.osid.id.Id getController() {
        return (makeElementId("osid.control.setting.Controller"));
    }


    /**
     *  Gets the VariableAmount element Id.
     *
     *  @return the VariableAmount element Id
     */

    public static org.osid.id.Id getVariableAmount() {
        return (makeElementId("osid.control.setting.VariableAmount"));
    }


    /**
     *  Gets the DiscreetStateId element Id.
     *
     *  @return the DiscreetStateId element Id
     */

    public static org.osid.id.Id getDiscreetStateId() {
        return (makeElementId("osid.control.setting.DiscreetStateId"));
    }


    /**
     *  Gets the DiscreetState element Id.
     *
     *  @return the DiscreetState element Id
     */

    public static org.osid.id.Id getDiscreetState() {
        return (makeElementId("osid.control.setting.DiscreetState"));
    }


    /**
     *  Gets the RampRate element Id.
     *
     *  @return the RampRate element Id
     */

    public static org.osid.id.Id getRampRate() {
        return (makeElementId("osid.control.setting.RampRate"));
    }


    /**
     *  Gets the On element Id.
     *
     *  @return the On element Id
     */

    public static org.osid.id.Id getOn() {
        return (makeQueryElementId("osid.control.setting.On"));
    }


    /**
     *  Gets the Off element Id.
     *
     *  @return the Off element Id
     */

    public static org.osid.id.Id getOff() {
        return (makeQueryElementId("osid.control.setting.Off"));
    }


    /**
     *  Gets the VariablePercentage element Id.
     *
     *  @return the VariablePercentage element Id
     */

    public static org.osid.id.Id getVariablePercentage() {
        return (makeQueryElementId("osid.control.setting.VariablePercentage"));
    }


    /**
     *  Gets the SystemId element Id.
     *
     *  @return the SystemId element Id
     */

    public static org.osid.id.Id getSystemId() {
        return (makeQueryElementId("osid.control.setting.SystemId"));
    }


    /**
     *  Gets the System element Id.
     *
     *  @return the System element Id
     */

    public static org.osid.id.Id getSystem() {
        return (makeQueryElementId("osid.control.setting.System"));
    }


    /**
     *  Gets the VariableRampRate element Id.
     *
     *  @return the VariableRampRate element Id
     */

    public static org.osid.id.Id getVariableRampRate() {
        return (makeSearchOrderElementId("osid.control.setting.VariableRampRate"));
    }
}
