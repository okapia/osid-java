//
// AbstractQueryAuthorizationLookupSession.java
//
//    An inline adapter that maps an AuthorizationLookupSession to
//    an AuthorizationQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.authorization.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps an AuthorizationLookupSession to
 *  an AuthorizationQuerySession.
 */

public abstract class AbstractQueryAuthorizationLookupSession
    extends net.okapia.osid.jamocha.authorization.spi.AbstractAuthorizationLookupSession
    implements org.osid.authorization.AuthorizationLookupSession {

    private boolean effectiveonly = false;
    private final org.osid.authorization.AuthorizationQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryAuthorizationLookupSession.
     *
     *  @param querySession the underlying authorization query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryAuthorizationLookupSession(org.osid.authorization.AuthorizationQuerySession querySession) {
        nullarg(querySession, "authorization query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Vault</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Vault Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getVaultId() {
        return (this.session.getVaultId());
    }


    /**
     *  Gets the <code>Vault</code> associated with this 
     *  session.
     *
     *  @return the <code>Vault</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.Vault getVault()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getVault());
    }


    /**
     *  Tests if this user can perform <code>Authorization</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAuthorizations() {
        return (this.session.canSearchAuthorizations());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include authorizations in vaults which are children
     *  of this vault in the vault hierarchy.
     */

    @OSID @Override
    public void useFederatedVaultView() {
        this.session.useFederatedVaultView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this vault only.
     */

    @OSID @Override
    public void useIsolatedVaultView() {
        this.session.useIsolatedVaultView();
        return;
    }
    

    /**
     *  Only authorizations whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveAuthorizationView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All authorizations of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveAuthorizationView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Authorization</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Authorization</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Authorization</code> and
     *  retained for compatibility.
     *
     *  In effective mode, authorizations are returned that are currently
     *  effective.  In any effective mode, effective authorizations and
     *  those currently expired are returned.
     *
     *  @param  authorizationId <code>Id</code> of the
     *          <code>Authorization</code>
     *  @return the authorization
     *  @throws org.osid.NotFoundException <code>authorizationId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>authorizationId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.Authorization getAuthorization(org.osid.id.Id authorizationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authorization.AuthorizationQuery query = getQuery();
        query.matchId(authorizationId, true);
        org.osid.authorization.AuthorizationList authorizations = this.session.getAuthorizationsByQuery(query);
        if (authorizations.hasNext()) {
            return (authorizations.getNextAuthorization());
        } 
        
        throw new org.osid.NotFoundException(authorizationId + " not found");
    }


    /**
     *  Gets an <code>AuthorizationList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  authorizations specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Authorizations</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, authorizations are returned that are currently effective.
     *  In any effective mode, effective authorizations and those currently expired
     *  are returned.
     *
     *  @param  authorizationIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Authorization</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>authorizationIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsByIds(org.osid.id.IdList authorizationIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authorization.AuthorizationQuery query = getQuery();

        try (org.osid.id.IdList ids = authorizationIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getAuthorizationsByQuery(query));
    }


    /**
     *  Gets an <code>AuthorizationList</code> corresponding to the given
     *  authorization genus <code>Type</code> which does not include
     *  authorizations of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned list
     *  may contain only those authorizations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, authorizations are returned that are currently effective.
     *  In any effective mode, effective authorizations and those currently expired
     *  are returned.
     *
     *  @param  authorizationGenusType an authorization genus type 
     *  @return the returned <code>Authorization</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>authorizationGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsByGenusType(org.osid.type.Type authorizationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authorization.AuthorizationQuery query = getQuery();
        query.matchGenusType(authorizationGenusType, true);
        return (this.session.getAuthorizationsByQuery(query));
    }


    /**
     *  Gets an <code>AuthorizationList</code> corresponding to the given
     *  authorization genus <code>Type</code> and include any additional
     *  authorizations with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned list
     *  may contain only those authorizations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, authorizations are returned that are currently
     *  effective.  In any effective mode, effective authorizations and
     *  those currently expired are returned.
     *
     *  @param  authorizationGenusType an authorization genus type 
     *  @return the returned <code>Authorization</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>authorizationGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsByParentGenusType(org.osid.type.Type authorizationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authorization.AuthorizationQuery query = getQuery();
        query.matchParentGenusType(authorizationGenusType, true);
        return (this.session.getAuthorizationsByQuery(query));
    }


    /**
     *  Gets an <code>AuthorizationList</code> containing the given
     *  authorization record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned list
     *  may contain only those authorizations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, authorizations are returned that are currently
     *  effective.  In any effective mode, effective authorizations and
     *  those currently expired are returned.
     *
     *  @param  authorizationRecordType an authorization record type 
     *  @return the returned <code>Authorization</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>authorizationRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsByRecordType(org.osid.type.Type authorizationRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authorization.AuthorizationQuery query = getQuery();
        query.matchRecordType(authorizationRecordType, true);
        return (this.session.getAuthorizationsByQuery(query));
    }


    /**
     *  Gets an <code>AuthorizationList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned list
     *  may contain only those authorizations that are accessible
     *  through this session.
     *  
     *  In effective mode, authorizations are returned that are currently
     *  effective.  In any effective mode, effective authorizations and
     *  those currently expired are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Authorization</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsOnDate(org.osid.calendaring.DateTime from, 
                                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authorization.AuthorizationQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getAuthorizationsByQuery(query));
    }
        

    /**
     *  Gets a list of authorizations corresponding to a resource
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned list
     *  may contain only those authorizations that are accessible
     *  through this session.
     *
     *  In effective mode, authorizations are returned that are
     *  currently effective.  In any effective mode, effective
     *  authorizations and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @return the returned <code>AuthorizationList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.authorization.AuthorizationList getAuthorizationsForResource(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.authorization.AuthorizationQuery query = getQuery();
        query.matchResourceId(resourceId, true);
        return (this.session.getAuthorizationsByQuery(query));
    }


    /**
     *  Gets a list of authorizations corresponding to a resource
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned list
     *  may contain only those authorizations that are accessible
     *  through this session.
     *
     *  In effective mode, authorizations are returned that are
     *  currently effective.  In any effective mode, effective
     *  authorizations and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>AuthorizationList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsForResourceOnDate(org.osid.id.Id resourceId,
                                                                                       org.osid.calendaring.DateTime from,
                                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authorization.AuthorizationQuery query = getQuery();
        query.matchResourceId(resourceId, true);
        query.matchDate(from, to, true);
        return (this.session.getAuthorizationsByQuery(query));
    }


    /**
     *  Gets a list of authorizations corresponding to an agent
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned list
     *  may contain only those authorizations that are accessible
     *  through this session.
     *
     *  In effective mode, authorizations are returned that are
     *  currently effective.  In any effective mode, effective
     *  authorizations and those currently expired are returned.
     *
     *  @param  agentId the <code>Id</code> of the agent
     *  @return the returned <code>AuthorizationList</code>
     *  @throws org.osid.NullArgumentException <code>agentId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.authorization.AuthorizationList getAuthorizationsForAgent(org.osid.id.Id agentId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.authorization.AuthorizationQuery query = getQuery();
        query.matchAgentId(agentId, true);
        return (this.session.getAuthorizationsByQuery(query));
    }


    /**
     *  Gets a list of authorizations corresponding to an agent
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned list
     *  may contain only those authorizations that are accessible
     *  through this session.
     *
     *  In effective mode, authorizations are returned that are
     *  currently effective.  In any effective mode, effective
     *  authorizations and those currently expired are returned.
     *
     *  @param  agentId the <code>Id</code> of the agent
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>AuthorizationList</code>
     *  @throws org.osid.NullArgumentException <code>agentId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsForAgentOnDate(org.osid.id.Id agentId,
                                                                                       org.osid.calendaring.DateTime from,
                                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authorization.AuthorizationQuery query = getQuery();
        query.matchAgentId(agentId, true);
        query.matchDate(from, to, true);
        return (this.session.getAuthorizationsByQuery(query));
    }


    /**
     *  Gets a list of authorizations corresponding to a function
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned list
     *  may contain only those authorizations that are accessible
     *  through this session.
     *
     *  In effective mode, authorizations are returned that are
     *  currently effective.  In any effective mode, effective
     *  authorizations and those currently expired are returned.
     *
     *  @param  functionId the <code>Id</code> of the function
     *  @return the returned <code>AuthorizationList</code>
     *  @throws org.osid.NullArgumentException <code>functionId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.authorization.AuthorizationList getAuthorizationsForFunction(org.osid.id.Id functionId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.authorization.AuthorizationQuery query = getQuery();
        query.matchFunctionId(functionId, true);
        return (this.session.getAuthorizationsByQuery(query));
    }


    /**
     *  Gets a list of authorizations corresponding to a function
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned list
     *  may contain only those authorizations that are accessible
     *  through this session.
     *
     *  In effective mode, authorizations are returned that are
     *  currently effective.  In any effective mode, effective
     *  authorizations and those currently expired are returned.
     *
     *  @param  functionId the <code>Id</code> of the function
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>AuthorizationList</code>
     *  @throws org.osid.NullArgumentException <code>functionId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsForFunctionOnDate(org.osid.id.Id functionId,
                                                                                       org.osid.calendaring.DateTime from,
                                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authorization.AuthorizationQuery query = getQuery();
        query.matchFunctionId(functionId, true);
        query.matchDate(from, to, true);
        return (this.session.getAuthorizationsByQuery(query));
    }


    /**
     *  Gets a list of authorizations corresponding to resource and function
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned list
     *  may contain only those authorizations that are accessible
     *  through this session.
     *
     *  In effective mode, authorizations are returned that are
     *  currently effective.  In any effective mode, effective
     *  authorizations and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  functionId the <code>Id</code> of the function
     *  @return the returned <code>AuthorizationList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>functionId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */
    
    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsForResourceAndFunction(org.osid.id.Id resourceId,
                                                                                            org.osid.id.Id functionId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.authorization.AuthorizationQuery query = getQuery();
        query.matchResourceId(resourceId, true);
        query.matchFunctionId(functionId, true);
        return (this.session.getAuthorizationsByQuery(query));
    }


    /**
     *  Gets an <code> AuthorizationList </code> effective during the entire 
     *  given date range inclusive but not confined to the date range. 
     *  Authorizations related to the given resource, including those related 
     *  through an <code> Agent, </code> are returned. 
     *  
     *  In plenary mode, the returned list contains all known authorizations 
     *  or an error results. Otherwise, the returned list may contain only 
     *  those authorizations that are accessible through this session. 
     *  
     *  In effective mode, authorizations are returned that are currently 
     *  effective. In any effective mode, active authorizations and those 
     *  currently expired are returned. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  functionId a function <code> Id </code> 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned <code> Authorization </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> resourceId, functionId, 
     *          from </code> or <code> to </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsForResourceAndFunctionOnDate(org.osid.id.Id resourceId, 
                                                                                                  org.osid.id.Id functionId, 
                                                                                                  org.osid.calendaring.DateTime from, 
                                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authorization.AuthorizationQuery query = getQuery();
        query.matchResourceId(resourceId, true);
        query.matchFunctionId(functionId, true);
        query.matchDate(from, to, true);
        return (this.session.getAuthorizationsByQuery(query));
    }


    /**
     *  Gets a list of <code> Authorizations </code> associated with a given 
     *  agent. Authorizations related to the given resource, including those 
     *  related through an <code> Agent, </code> are returned. In plenary 
     *  mode, the returned list contains all known authorizations or an error 
     *  results. Otherwise, the returned list may contain only those 
     *  authorizations that are accessible through this session. 
     *
     *  @param  agentId an agent <code> Id </code> 
     *  @param  functionId a function <code> Id </code> 
     *  @return the returned <code> Authorization list </code> 
     *  @throws org.osid.NullArgumentException <code> agentId </code> or 
     *          <code> functionId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsForAgentAndFunction(org.osid.id.Id agentId, 
                                                                                         org.osid.id.Id functionId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authorization.AuthorizationQuery query = getQuery();
        query.matchAgentId(agentId, true);
        query.matchFunctionId(functionId, true);
        return (this.session.getAuthorizationsByQuery(query));
    }


    /**
     *  Gets an <code> AuthorizationList </code> for the given agent and 
     *  effective during the entire given date range inclusive but not 
     *  confined to the date range. 
     *  
     *  In plenary mode, the returned list contains all known authorizations 
     *  or an error results. Otherwise, the returned list may contain only 
     *  those authorizations that are accessible through this session. 
     *  
     *  In effective mode, authorizations are returned that are currently 
     *  effective. In any effective mode, active authorizations and those 
     *  currently expired are returned. 
     *
     *  @param  agentId an agent <code> Id </code> 
     *  @param  functionId a function <code> Id </code> 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned <code> Authorization </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> agentId, functionId, 
     *          from </code> or <code> to </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsForAgentAndFunctionOnDate(org.osid.id.Id agentId, 
                                                                                               org.osid.id.Id functionId, 
                                                                                               org.osid.calendaring.DateTime from, 
                                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authorization.AuthorizationQuery query = getQuery();
        query.matchAgentId(agentId, true);
        query.matchFunctionId(functionId, true);
        query.matchDate(from, to, true);
        return (this.session.getAuthorizationsByQuery(query));
    }


    /**
     *  Gets a list of <code> Authorizations </code> associated with a given 
     *  qualifier. In plenary mode, the returned list contains all known 
     *  authorizations or an error results. Otherwise, the returned list may 
     *  contain only those authorizations that are accessible through this 
     *  session. 
     *
     *  @param  qualifierId a qualifier <code> Id </code> 
     *  @return the returned <code> Authorization list </code> 
     *  @throws org.osid.NullArgumentException <code> qualifierId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsByQualifier(org.osid.id.Id qualifierId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authorization.AuthorizationQuery query = getQuery();
        query.matchQualifierId(qualifierId, true);
        return (this.session.getAuthorizationsByQuery(query));
    }


    /**
     *  Gets the explicit <code> Authorization </code> that generated the 
     *  given implicit authorization. If the given <code> Authorization 
     *  </code> is explicit, then the same <code> Authorization </code> is 
     *  returned. 
     *
     *  @param  authorizationId an authorization 
     *  @return the explicit <code> Authorization </code> 
     *  @throws org.osid.NotFoundException <code> authorizationId </code> is 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> authorizationId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.authorization.Authorization getExplicitAuthorization(org.osid.id.Id authorizationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authorization.AuthorizationQuery query = getQuery();
        query.matchRelatedAuthorizationId(authorizationId, true);
        query.matchExplicitAuthorizations(true);

        try (org.osid.authorization.AuthorizationList authorizations = this.session.getAuthorizationsByQuery(query)) {
            if (!authorizations.hasNext()) {
                throw new org.osid.NotFoundException("no explicit authorization found for " + authorizationId);
            }
            
            return (authorizations.getNextAuthorization());
        }
    }


    /**
     *  Gets all <code>Authorizations</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned list
     *  may contain only those authorizations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, authorizations are returned that are currently
     *  effective.  In any effective mode, effective authorizations and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Authorizations</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authorization.AuthorizationQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getAuthorizationsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.authorization.AuthorizationQuery getQuery() {
        org.osid.authorization.AuthorizationQuery query = this.session.getAuthorizationQuery();
        
        if (isEffectiveOnly()) {
            query.matchEffective(true);
        }

        return (query);
    }
}
