//
// AbstractAssemblyCredentialEntryQuery.java
//
//     A CredentialEntryQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.course.chronicle.credentialentry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A CredentialEntryQuery that stores terms.
 */

public abstract class AbstractAssemblyCredentialEntryQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRelationshipQuery
    implements org.osid.course.chronicle.CredentialEntryQuery,
               org.osid.course.chronicle.CredentialEntryQueryInspector,
               org.osid.course.chronicle.CredentialEntrySearchOrder {

    private final java.util.Collection<org.osid.course.chronicle.records.CredentialEntryQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.chronicle.records.CredentialEntryQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.chronicle.records.CredentialEntrySearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyCredentialEntryQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyCredentialEntryQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the student <code> Id </code> for this query. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchStudentId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getStudentIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the student <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearStudentIdTerms() {
        getAssembler().clearTerms(getStudentIdColumn());
        return;
    }


    /**
     *  Gets the resource <code> Id </code> query terms. 
     *
     *  @return the resource <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getStudentIdTerms() {
        return (getAssembler().getIdTerms(getStudentIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the resource. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByStudent(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getStudentColumn(), style);
        return;
    }


    /**
     *  Gets the StudentId column name.
     *
     * @return the column name
     */

    protected String getStudentIdColumn() {
        return ("student_id");
    }


    /**
     *  Tests if a <code> StudentQuery </code> is available. 
     *
     *  @return <code> true </code> if a student query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStudentQuery() {
        return (false);
    }


    /**
     *  Gets the query for a student option. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return a student query 
     *  @throws org.osid.UnimplementedException <code> supportsStudentQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getStudentQuery() {
        throw new org.osid.UnimplementedException("supportsStudentQuery() is false");
    }


    /**
     *  Clears the student option terms. 
     */

    @OSID @Override
    public void clearStudentTerms() {
        getAssembler().clearTerms(getStudentColumn());
        return;
    }


    /**
     *  Gets the student query terms. 
     *
     *  @return the resource query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getStudentTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a resource order is available. 
     *
     *  @return <code> true </code> if a resource order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStudentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStudentSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getStudentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsStudentSearchOrder() is false");
    }


    /**
     *  Gets the Student column name.
     *
     * @return the column name
     */

    protected String getStudentColumn() {
        return ("student");
    }


    /**
     *  Sets the credential <code> Id </code> for this query to match entries 
     *  that have an entry for the given course. 
     *
     *  @param  credentialId a credential <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> credentialId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCredentialId(org.osid.id.Id credentialId, boolean match) {
        getAssembler().addIdTerm(getCredentialIdColumn(), credentialId, match);
        return;
    }


    /**
     *  Clears the credential <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCredentialIdTerms() {
        getAssembler().clearTerms(getCredentialIdColumn());
        return;
    }


    /**
     *  Gets the credential <code> Id </code> query terms. 
     *
     *  @return the credential <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCredentialIdTerms() {
        return (getAssembler().getIdTerms(getCredentialIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the credential. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCredential(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCredentialColumn(), style);
        return;
    }


    /**
     *  Gets the CredentialId column name.
     *
     * @return the column name
     */

    protected String getCredentialIdColumn() {
        return ("credential_id");
    }


    /**
     *  Tests if a <code> CredentialQuery </code> is available. 
     *
     *  @return <code> true </code> if a credential query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCredentialQuery() {
        return (false);
    }


    /**
     *  Gets the query for a credential entry. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return a credential query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.CredentialQuery getCredentialQuery() {
        throw new org.osid.UnimplementedException("supportsCredentialQuery() is false");
    }


    /**
     *  Clears the credential terms. 
     */

    @OSID @Override
    public void clearCredentialTerms() {
        getAssembler().clearTerms(getCredentialColumn());
        return;
    }


    /**
     *  Gets the credential query terms. 
     *
     *  @return the credential terms 
     */

    @OSID @Override
    public org.osid.course.program.CredentialQueryInspector[] getCredentialTerms() {
        return (new org.osid.course.program.CredentialQueryInspector[0]);
    }


    /**
     *  Tests if a credential order is available. 
     *
     *  @return <code> true </code> if a credential order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCredentialSearchOrder() {
        return (false);
    }


    /**
     *  Gets the credential order. 
     *
     *  @return the credential search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.program.CredentialSearchOrder getCredentialSearchOrder() {
        throw new org.osid.UnimplementedException("supportsCredentialSearchOrder() is false");
    }


    /**
     *  Gets the Credential column name.
     *
     * @return the column name
     */

    protected String getCredentialColumn() {
        return ("credential");
    }


    /**
     *  Matches award dates between the given dates inclusive. 
     *
     *  @param  from starting date 
     *  @param  to ending date 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchDateAwarded(org.osid.calendaring.DateTime from, 
                                 org.osid.calendaring.DateTime to, 
                                 boolean match) {
        getAssembler().addDateTimeRangeTerm(getDateAwardedColumn(), from, to, match);
        return;
    }


    /**
     *  Matches entries that have any award date. 
     *
     *  @param  match <code> true </code> to match entries with any award 
     *          date, <code> false </code> to match entries with no award date 
     */

    @OSID @Override
    public void matchAnyDateAwarded(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getDateAwardedColumn(), match);
        return;
    }


    /**
     *  Clears the award date terms. 
     */

    @OSID @Override
    public void clearDateAwardedTerms() {
        getAssembler().clearTerms(getDateAwardedColumn());
        return;
    }


    /**
     *  Gets the award date query terms. 
     *
     *  @return the date terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getDateAwardedTerms() {
        return (getAssembler().getDateTimeRangeTerms(getDateAwardedColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the award date. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDateAwarded(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDateAwardedColumn(), style);
        return;
    }


    /**
     *  Gets the DateAwarded column name.
     *
     * @return the column name
     */

    protected String getDateAwardedColumn() {
        return ("date_awarded");
    }


    /**
     *  Sets the program <code> Id </code> for this query. 
     *
     *  @param  programId a program <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> programId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchProgramId(org.osid.id.Id programId, boolean match) {
        getAssembler().addIdTerm(getProgramIdColumn(), programId, match);
        return;
    }


    /**
     *  Clears the program <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearProgramIdTerms() {
        getAssembler().clearTerms(getProgramIdColumn());
        return;
    }


    /**
     *  Gets the program <code> Id </code> query terms. 
     *
     *  @return the program <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProgramIdTerms() {
        return (getAssembler().getIdTerms(getProgramIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the program. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByProgram(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getProgramColumn(), style);
        return;
    }


    /**
     *  Gets the ProgramId column name.
     *
     * @return the column name
     */

    protected String getProgramIdColumn() {
        return ("program_id");
    }


    /**
     *  Tests if a <code> ProgramQuery </code> is available. 
     *
     *  @return <code> true </code> if a program query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramQuery() {
        return (false);
    }


    /**
     *  Gets the query for a program. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return a program query 
     *  @throws org.osid.UnimplementedException <code> supportsProgramQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramQuery getProgramQuery() {
        throw new org.osid.UnimplementedException("supportsProgramQuery() is false");
    }


    /**
     *  Matches entries that have any program. 
     *
     *  @param  match <code> true </code> to match entries with any program 
     *          <code> false </code> to match entries with no program 
     */

    @OSID @Override
    public void matchAnyProgram(boolean match) {
        getAssembler().addIdWildcardTerm(getProgramColumn(), match);
        return;
    }


    /**
     *  Clears the program terms. 
     */

    @OSID @Override
    public void clearProgramTerms() {
        getAssembler().clearTerms(getProgramColumn());
        return;
    }


    /**
     *  Gets the program query terms. 
     *
     *  @return the program terms 
     */

    @OSID @Override
    public org.osid.course.program.ProgramQueryInspector[] getProgramTerms() {
        return (new org.osid.course.program.ProgramQueryInspector[0]);
    }


    /**
     *  Tests if a program order is available. 
     *
     *  @return <code> true </code> if a program order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramSearchOrder() {
        return (false);
    }


    /**
     *  Gets the program order. 
     *
     *  @return the program search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramSearchOrder getProgramSearchOrder() {
        throw new org.osid.UnimplementedException("supportsProgramSearchOrder() is false");
    }


    /**
     *  Gets the Program column name.
     *
     * @return the column name
     */

    protected String getProgramColumn() {
        return ("program");
    }


    /**
     *  Sets the course catalog <code> Id </code> for this query to match 
     *  entries assigned to course catalogs. 
     *
     *  @param  courseCatalogId the course catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseCatalogId(org.osid.id.Id courseCatalogId, 
                                     boolean match) {
        getAssembler().addIdTerm(getCourseCatalogIdColumn(), courseCatalogId, match);
        return;
    }


    /**
     *  Clears the course catalog <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseCatalogIdTerms() {
        getAssembler().clearTerms(getCourseCatalogIdColumn());
        return;
    }


    /**
     *  Gets the course catalog <code> Id </code> query terms. 
     *
     *  @return the course catalog <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseCatalogIdTerms() {
        return (getAssembler().getIdTerms(getCourseCatalogIdColumn()));
    }


    /**
     *  Gets the CourseCatalogId column name.
     *
     * @return the column name
     */

    protected String getCourseCatalogIdColumn() {
        return ("course_catalog_id");
    }


    /**
     *  Tests if a <code> CourseCatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a course catalog query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course catalog. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the course catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQuery getCourseCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsCourseCatalogQuery() is false");
    }


    /**
     *  Clears the course catalog terms. 
     */

    @OSID @Override
    public void clearCourseCatalogTerms() {
        getAssembler().clearTerms(getCourseCatalogColumn());
        return;
    }


    /**
     *  Gets the course catalog query terms. 
     *
     *  @return the course catalog query terms 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQueryInspector[] getCourseCatalogTerms() {
        return (new org.osid.course.CourseCatalogQueryInspector[0]);
    }


    /**
     *  Gets the CourseCatalog column name.
     *
     * @return the column name
     */

    protected String getCourseCatalogColumn() {
        return ("course_catalog");
    }


    /**
     *  Tests if this credentialEntry supports the given record
     *  <code>Type</code>.
     *
     *  @param  credentialEntryRecordType a credential entry record type 
     *  @return <code>true</code> if the credentialEntryRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>credentialEntryRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type credentialEntryRecordType) {
        for (org.osid.course.chronicle.records.CredentialEntryQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(credentialEntryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  credentialEntryRecordType the credential entry record type 
     *  @return the credential entry query record 
     *  @throws org.osid.NullArgumentException
     *          <code>credentialEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(credentialEntryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.chronicle.records.CredentialEntryQueryRecord getCredentialEntryQueryRecord(org.osid.type.Type credentialEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.chronicle.records.CredentialEntryQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(credentialEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(credentialEntryRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  credentialEntryRecordType the credential entry record type 
     *  @return the credential entry query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>credentialEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(credentialEntryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.chronicle.records.CredentialEntryQueryInspectorRecord getCredentialEntryQueryInspectorRecord(org.osid.type.Type credentialEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.chronicle.records.CredentialEntryQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(credentialEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(credentialEntryRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param credentialEntryRecordType the credential entry record type
     *  @return the credential entry search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>credentialEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(credentialEntryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.chronicle.records.CredentialEntrySearchOrderRecord getCredentialEntrySearchOrderRecord(org.osid.type.Type credentialEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.chronicle.records.CredentialEntrySearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(credentialEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(credentialEntryRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this credential entry. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param credentialEntryQueryRecord the credential entry query record
     *  @param credentialEntryQueryInspectorRecord the credential entry query inspector
     *         record
     *  @param credentialEntrySearchOrderRecord the credential entry search order record
     *  @param credentialEntryRecordType credential entry record type
     *  @throws org.osid.NullArgumentException
     *          <code>credentialEntryQueryRecord</code>,
     *          <code>credentialEntryQueryInspectorRecord</code>,
     *          <code>credentialEntrySearchOrderRecord</code> or
     *          <code>credentialEntryRecordTypecredentialEntry</code> is
     *          <code>null</code>
     */
            
    protected void addCredentialEntryRecords(org.osid.course.chronicle.records.CredentialEntryQueryRecord credentialEntryQueryRecord, 
                                      org.osid.course.chronicle.records.CredentialEntryQueryInspectorRecord credentialEntryQueryInspectorRecord, 
                                      org.osid.course.chronicle.records.CredentialEntrySearchOrderRecord credentialEntrySearchOrderRecord, 
                                      org.osid.type.Type credentialEntryRecordType) {

        addRecordType(credentialEntryRecordType);

        nullarg(credentialEntryQueryRecord, "credential entry query record");
        nullarg(credentialEntryQueryInspectorRecord, "credential entry query inspector record");
        nullarg(credentialEntrySearchOrderRecord, "credential entry search odrer record");

        this.queryRecords.add(credentialEntryQueryRecord);
        this.queryInspectorRecords.add(credentialEntryQueryInspectorRecord);
        this.searchOrderRecords.add(credentialEntrySearchOrderRecord);
        
        return;
    }
}
