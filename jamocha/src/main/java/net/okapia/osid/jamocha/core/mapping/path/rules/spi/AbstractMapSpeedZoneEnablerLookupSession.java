//
// AbstractMapSpeedZoneEnablerLookupSession
//
//    A simple framework for providing a SpeedZoneEnabler lookup service
//    backed by a fixed collection of speed zone enablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping.path.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a SpeedZoneEnabler lookup service backed by a
 *  fixed collection of speed zone enablers. The speed zone enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>SpeedZoneEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapSpeedZoneEnablerLookupSession
    extends net.okapia.osid.jamocha.mapping.path.rules.spi.AbstractSpeedZoneEnablerLookupSession
    implements org.osid.mapping.path.rules.SpeedZoneEnablerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.mapping.path.rules.SpeedZoneEnabler> speedZoneEnablers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.mapping.path.rules.SpeedZoneEnabler>());


    /**
     *  Makes a <code>SpeedZoneEnabler</code> available in this session.
     *
     *  @param  speedZoneEnabler a speed zone enabler
     *  @throws org.osid.NullArgumentException <code>speedZoneEnabler<code>
     *          is <code>null</code>
     */

    protected void putSpeedZoneEnabler(org.osid.mapping.path.rules.SpeedZoneEnabler speedZoneEnabler) {
        this.speedZoneEnablers.put(speedZoneEnabler.getId(), speedZoneEnabler);
        return;
    }


    /**
     *  Makes an array of speed zone enablers available in this session.
     *
     *  @param  speedZoneEnablers an array of speed zone enablers
     *  @throws org.osid.NullArgumentException <code>speedZoneEnablers<code>
     *          is <code>null</code>
     */

    protected void putSpeedZoneEnablers(org.osid.mapping.path.rules.SpeedZoneEnabler[] speedZoneEnablers) {
        putSpeedZoneEnablers(java.util.Arrays.asList(speedZoneEnablers));
        return;
    }


    /**
     *  Makes a collection of speed zone enablers available in this session.
     *
     *  @param  speedZoneEnablers a collection of speed zone enablers
     *  @throws org.osid.NullArgumentException <code>speedZoneEnablers<code>
     *          is <code>null</code>
     */

    protected void putSpeedZoneEnablers(java.util.Collection<? extends org.osid.mapping.path.rules.SpeedZoneEnabler> speedZoneEnablers) {
        for (org.osid.mapping.path.rules.SpeedZoneEnabler speedZoneEnabler : speedZoneEnablers) {
            this.speedZoneEnablers.put(speedZoneEnabler.getId(), speedZoneEnabler);
        }

        return;
    }


    /**
     *  Removes a SpeedZoneEnabler from this session.
     *
     *  @param  speedZoneEnablerId the <code>Id</code> of the speed zone enabler
     *  @throws org.osid.NullArgumentException <code>speedZoneEnablerId<code> is
     *          <code>null</code>
     */

    protected void removeSpeedZoneEnabler(org.osid.id.Id speedZoneEnablerId) {
        this.speedZoneEnablers.remove(speedZoneEnablerId);
        return;
    }


    /**
     *  Gets the <code>SpeedZoneEnabler</code> specified by its <code>Id</code>.
     *
     *  @param  speedZoneEnablerId <code>Id</code> of the <code>SpeedZoneEnabler</code>
     *  @return the speedZoneEnabler
     *  @throws org.osid.NotFoundException <code>speedZoneEnablerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>speedZoneEnablerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnabler getSpeedZoneEnabler(org.osid.id.Id speedZoneEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.mapping.path.rules.SpeedZoneEnabler speedZoneEnabler = this.speedZoneEnablers.get(speedZoneEnablerId);
        if (speedZoneEnabler == null) {
            throw new org.osid.NotFoundException("speedZoneEnabler not found: " + speedZoneEnablerId);
        }

        return (speedZoneEnabler);
    }


    /**
     *  Gets all <code>SpeedZoneEnablers</code>. In plenary mode, the returned
     *  list contains all known speedZoneEnablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  speedZoneEnablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>SpeedZoneEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerList getSpeedZoneEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.mapping.path.rules.speedzoneenabler.ArraySpeedZoneEnablerList(this.speedZoneEnablers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.speedZoneEnablers.clear();
        super.close();
        return;
    }
}
