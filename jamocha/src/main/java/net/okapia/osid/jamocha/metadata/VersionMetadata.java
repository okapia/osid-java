//
// VersionMetadata.java
//
//     Defines a version Metadata.
//
//
// Tom Coppeto
// Okapia
// 11 January 2023
//
//
// Copyright (c) 2023 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.metadata;


/**
 *  Defines a version Metadata.
 */

public final class VersionMetadata
    extends net.okapia.osid.jamocha.metadata.spi.AbstractVersionMetadata
    implements org.osid.Metadata {


    /**
     *  Constructs a new single unlinked {@code VersionMetadata}.
     *
     *  @param elementId the Id of the element
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    public VersionMetadata(org.osid.id.Id elementId) {
        super(elementId);
        return;
    }


    /**
     *  Constructs a new unlinked {@code VersionMetadata}.
     *
     *  @param elementId the Id of the element
     *  @param isArray {@code true} if the element is an array another
     *         element, {@code false} if a single element
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    public VersionMetadata(org.osid.id.Id elementId, boolean isArray) {
        super(elementId, isArray, false);
        return;
    }


    /**
     *  Constructs a new {@code VersionMetadata}.
     *
     *  @param elementId the Id of the element
     *  @param isArray {@code true} if the element is an array another
     *         element, {@code false} if a single element
     *  @param isLinked {@code true} if the element is linked to
     *         another element, {@code false} otherwise
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    public VersionMetadata(org.osid.id.Id elementId, boolean isArray, boolean isLinked) {
        super(elementId, isArray, isLinked);
        return;
    }


    /**
     *  Sets the element label.
     *
     *  @param label the new element label
     *  @throws org.osid.NullArgumentException {@code label} is {@code
     *          null}
     */

    public void setLabel(org.osid.locale.DisplayText label) {
        super.setLabel(label);
        return;
    }


    /**
     *  Sets the instructions.
     *
     *  @param instructions the new instructions
     *  @throws org.osid.NullArgumentException {@code instructions}
     *          is {@code null}
     */

    public void setInstructions(org.osid.locale.DisplayText instructions) {
        super.setInstructions(instructions);
        return;
    }

    
    /**
     *  Sets the required flag.
     *
     *  @param required {@code true} if required, {@code false} if
     *         optional
     */

    public void setRequired(boolean required) {
        super.setRequired(required);
        return;
    }

    
    /**
     *  Sets the has value flag.
     *
     *  @param exists {@code true} if has existing value, {@code
     *         false} if no value exists
     */

    public void setValueExists(boolean exists) {
        super.setValueExists(exists);
        return;
    }


    /**
     *  Sets the read only flag.
     *
     *  @param readonly {@code true} if read only, {@code false} if
     *         can be updated
     */

    public void setReadOnly(boolean readonly) {
        super.setReadOnly(readonly);
        return;
    }


    /**
     *  Sets the units.
     *
     *	@param units the new units
     *  @throws org.osid.NullArgumentException {@code units}
     *          is {@code null}
     */

    public void setUnits(org.osid.locale.DisplayText units) {
        super.setUnits(units);
        return;
    }

    
    /**
     *  Add support for a version type.
     *
     *  @param versionType the type of version
     *  @throws org.osid.NullArgumentException {@code versionType} is
     *          {@code null}
     */

    public void addVersionType(org.osid.type.Type versionType) {
        super.addVersionType(versionType);
        return;
    }


    /**
     *  Sets the min and max versions.
     *
     *  @param min the minimum value
     *  @param max the maximum value
     *  @throws org.osid.InvalidArgumentException {@code min} is
     *          newer than {@code max}
     *  @throws org.osid.NullArgumentException {@code min} or
     *          {@code max} is {@code null}
     */

    public void setVersionRange(org.osid.installation.Version min, org.osid.installation.Version max) {
        super.setVersionRange(min, max);
        return;
    }

    
    /**
     *  Sets the version set.
     *
     *  @param values a collection of accepted version values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    public void setVersionSet(java.util.Collection<org.osid.installation.Version> values) {
        super.setVersionSet(values);
        return;
    }


    /**
     *  Adds a collection of values to the version set.
     *
     *  @param values a collection of accepted version values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    public void addToVersionSet(java.util.Collection<org.osid.installation.Version> values) {
        super.addToVersionSet(values);
        return;
    }


    /**
     *  Adds a value to the version set.
     *
     *  @param value a version value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void addToVersionSet(org.osid.installation.Version value) {
        super.addToVersionSet(value);
        return;
    }


    /**
     *  Removes a value from the version set.
     *
     *  @param value a version value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void removeFromVersionSet(org.osid.installation.Version value) {
        super.removeFromVersionSet(value);
        return;
    }


    /**
     *  Clears the version set.
     */

    public void clearVersionSet() {
        super.clearVersionSet();
        return;
    }


    /**
     *  Sets the default version set.
     *
     *  @param values a collection of default version values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    public void setDefaultVersionValues(java.util.Collection<org.osid.installation.Version> values) {
        super.setDefaultVersionValues(values);
        return;
    }


    /**
     *  Adds a collection of default version values.
     *
     *  @param values a collection of default version values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    public void addDefaultVersionValues(java.util.Collection<org.osid.installation.Version> values) {
        super.addDefaultVersionValues(values);
        return;
    }


    /**
     *  Adds a default version value.
     *
     *  @param value a version value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void addDefaultVersionValue(org.osid.installation.Version value) {
        super.addDefaultVersionValue(value);
        return;
    }


    /**
     *  Removes a default version value.
     *
     *  @param value a version value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void removeDefaultVersionValue(org.osid.installation.Version value) {
        super.removeDefaultVersionValue(value);
        return;
    }


    /**
     *  Clears the default version values.
     */

    public void clearDefaultVersionValues() {
        super.clearDefaultVersionValues();
        return;
    }


    /**
     *  Sets the existing version set.
     *
     *  @param values a collection of existing version values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    public void setExistingVersionValues(java.util.Collection<org.osid.installation.Version> values) {
        super.setExistingVersionValues(values);
        return;
    }


    /**
     *  Adds a collection of existing version values.
     *
     *  @param values a collection of existing version values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    public void addExistingVersionValues(java.util.Collection<org.osid.installation.Version> values) {
        super.addExistingVersionValues(values);
        return;
    }


    /**
     *  Adds a existing version value.
     *
     *  @param value a version value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void addExistingVersionValue(org.osid.installation.Version value) {
        super.addExistingVersionValue(value);
        return;
    }


    /**
     *  Removes a existing version value.
     *
     *  @param value a version value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void removeExistingVersionValue(org.osid.installation.Version value) {
        super.removeExistingVersionValue(value);
        return;
    }


    /**
     *  Clears the existing version values.
     */

    public void clearExistingVersionValues() {
        super.clearExistingVersionValues();
        return;
    }    
}
