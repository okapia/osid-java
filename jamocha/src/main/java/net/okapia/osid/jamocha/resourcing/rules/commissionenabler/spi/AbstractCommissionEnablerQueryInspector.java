//
// AbstractCommissionEnablerQueryInspector.java
//
//     A template for making a CommissionEnablerQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.rules.commissionenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for commission enablers.
 */

public abstract class AbstractCommissionEnablerQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQueryInspector
    implements org.osid.resourcing.rules.CommissionEnablerQueryInspector {

    private final java.util.Collection<org.osid.resourcing.rules.records.CommissionEnablerQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the commission <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledCommissionIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the commission query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionQueryInspector[] getRuledCommissionTerms() {
        return (new org.osid.resourcing.CommissionQueryInspector[0]);
    }


    /**
     *  Gets the foundry <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getFoundryIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the foundry query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryQueryInspector[] getFoundryTerms() {
        return (new org.osid.resourcing.FoundryQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given commission enabler query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a commission enabler implementing the requested record.
     *
     *  @param commissionEnablerRecordType a commission enabler record type
     *  @return the commission enabler query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>commissionEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(commissionEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.rules.records.CommissionEnablerQueryInspectorRecord getCommissionEnablerQueryInspectorRecord(org.osid.type.Type commissionEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.rules.records.CommissionEnablerQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(commissionEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(commissionEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this commission enabler query. 
     *
     *  @param commissionEnablerQueryInspectorRecord commission enabler query inspector
     *         record
     *  @param commissionEnablerRecordType commissionEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCommissionEnablerQueryInspectorRecord(org.osid.resourcing.rules.records.CommissionEnablerQueryInspectorRecord commissionEnablerQueryInspectorRecord, 
                                                   org.osid.type.Type commissionEnablerRecordType) {

        addRecordType(commissionEnablerRecordType);
        nullarg(commissionEnablerRecordType, "commission enabler record type");
        this.records.add(commissionEnablerQueryInspectorRecord);        
        return;
    }
}
