//
// AbstractDirectionSearch.java
//
//     A template for making a Direction Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recipe.direction.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing direction searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractDirectionSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.recipe.DirectionSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.recipe.records.DirectionSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.recipe.DirectionSearchOrder directionSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of directions. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  directionIds list of directions
     *  @throws org.osid.NullArgumentException
     *          <code>directionIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongDirections(org.osid.id.IdList directionIds) {
        while (directionIds.hasNext()) {
            try {
                this.ids.add(directionIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongDirections</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of direction Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getDirectionIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  directionSearchOrder direction search order 
     *  @throws org.osid.NullArgumentException
     *          <code>directionSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>directionSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderDirectionResults(org.osid.recipe.DirectionSearchOrder directionSearchOrder) {
	this.directionSearchOrder = directionSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.recipe.DirectionSearchOrder getDirectionSearchOrder() {
	return (this.directionSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given direction search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a direction implementing the requested record.
     *
     *  @param directionSearchRecordType a direction search record
     *         type
     *  @return the direction search record
     *  @throws org.osid.NullArgumentException
     *          <code>directionSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(directionSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.recipe.records.DirectionSearchRecord getDirectionSearchRecord(org.osid.type.Type directionSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.recipe.records.DirectionSearchRecord record : this.records) {
            if (record.implementsRecordType(directionSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(directionSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this direction search. 
     *
     *  @param directionSearchRecord direction search record
     *  @param directionSearchRecordType direction search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addDirectionSearchRecord(org.osid.recipe.records.DirectionSearchRecord directionSearchRecord, 
                                           org.osid.type.Type directionSearchRecordType) {

        addRecordType(directionSearchRecordType);
        this.records.add(directionSearchRecord);        
        return;
    }
}
