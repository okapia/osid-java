//
// AbstractQuerySubscriptionLookupSession.java
//
//    An inline adapter that maps a SubscriptionLookupSession to
//    a SubscriptionQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.subscription.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a SubscriptionLookupSession to
 *  a SubscriptionQuerySession.
 */

public abstract class AbstractQuerySubscriptionLookupSession
    extends net.okapia.osid.jamocha.subscription.spi.AbstractSubscriptionLookupSession
    implements org.osid.subscription.SubscriptionLookupSession {

    private boolean effectiveonly = false;
    private final org.osid.subscription.SubscriptionQuerySession session;
    

    /**
     *  Constructs a new AbstractQuerySubscriptionLookupSession.
     *
     *  @param querySession the underlying subscription query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQuerySubscriptionLookupSession(org.osid.subscription.SubscriptionQuerySession querySession) {
        nullarg(querySession, "subscription query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Publisher</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Publisher Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getPublisherId() {
        return (this.session.getPublisherId());
    }


    /**
     *  Gets the <code>Publisher</code> associated with this 
     *  session.
     *
     *  @return the <code>Publisher</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.Publisher getPublisher()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getPublisher());
    }


    /**
     *  Tests if this user can perform <code>Subscription</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupSubscriptions() {
        return (this.session.canSearchSubscriptions());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include subscriptions in publishers which are children
     *  of this publisher in the publisher hierarchy.
     */

    @OSID @Override
    public void useFederatedPublisherView() {
        this.session.useFederatedPublisherView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this publisher only.
     */

    @OSID @Override
    public void useIsolatedPublisherView() {
        this.session.useIsolatedPublisherView();
        return;
    }
    

    /**
     *  Only subscriptions whose effective dates are current are
     *  returned by methods in this session.
     */

    public void useEffectiveSubscriptionView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All subscriptions of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveSubscriptionView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Subscription</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Subscription</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Subscription</code> and
     *  retained for compatibility.
     *
     *  In effective mode, subscriptions are returned that are currently
     *  effective.  In any effective mode, effective subscriptions and
     *  those currently expired are returned.
     *
     *  @param  subscriptionId <code>Id</code> of the
     *          <code>Subscription</code>
     *  @return the subscription
     *  @throws org.osid.NotFoundException <code>subscriptionId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>subscriptionId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.Subscription getSubscription(org.osid.id.Id subscriptionId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.subscription.SubscriptionQuery query = getQuery();
        query.matchId(subscriptionId, true);
        org.osid.subscription.SubscriptionList subscriptions = this.session.getSubscriptionsByQuery(query);
        if (subscriptions.hasNext()) {
            return (subscriptions.getNextSubscription());
        } 
        
        throw new org.osid.NotFoundException(subscriptionId + " not found");
    }


    /**
     *  Gets a <code>SubscriptionList</code> corresponding to the
     *  given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  subscriptions specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>Subscriptions</code> may be omitted from the list and
     *  may present the elements in any order including returning a
     *  unique set.
     *
     *  In effective mode, subscriptions are returned that are
     *  currently effective.  In any effective mode, effective
     *  subscriptions and those currently expired are returned.
     *
     *  @param  subscriptionIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Subscription</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>subscriptionIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptionsByIds(org.osid.id.IdList subscriptionIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.subscription.SubscriptionQuery query = getQuery();

        try (org.osid.id.IdList ids = subscriptionIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getSubscriptionsByQuery(query));
    }


    /**
     *  Gets a <code>SubscriptionList</code> corresponding to the
     *  given subscription genus <code>Type</code> which does not
     *  include subscriptions of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned
     *  list may contain only those subscriptions that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, subscriptions are returned that are
     *  currently effective.  In any effective mode, effective
     *  subscriptions and those currently expired are returned.
     *
     *  @param  subscriptionGenusType a subscription genus type 
     *  @return the returned <code>Subscription</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>subscriptionGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptionsByGenusType(org.osid.type.Type subscriptionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.subscription.SubscriptionQuery query = getQuery();
        query.matchGenusType(subscriptionGenusType, true);
        return (this.session.getSubscriptionsByQuery(query));
    }


    /**
     *  Gets a <code>SubscriptionList</code> corresponding to the
     *  given subscription genus <code>Type</code> and include any
     *  additional subscriptions with genus types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned
     *  list may contain only those subscriptions that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, subscriptions are returned that are
     *  currently effective.  In any effective mode, effective
     *  subscriptions and those currently expired are returned.
     *
     *  @param  subscriptionGenusType a subscription genus type 
     *  @return the returned <code>Subscription</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>subscriptionGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptionsByParentGenusType(org.osid.type.Type subscriptionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.subscription.SubscriptionQuery query = getQuery();
        query.matchParentGenusType(subscriptionGenusType, true);
        return (this.session.getSubscriptionsByQuery(query));
    }


    /**
     *  Gets a <code>SubscriptionList</code> containing the given
     *  subscription record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned
     *  list may contain only those subscriptions that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, subscriptions are returned that are
     *  currently effective.  In any effective mode, effective
     *  subscriptions and those currently expired are returned.
     *
     *  @param  subscriptionRecordType a subscription record type 
     *  @return the returned <code>Subscription</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>subscriptionRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptionsByRecordType(org.osid.type.Type subscriptionRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.subscription.SubscriptionQuery query = getQuery();
        query.matchRecordType(subscriptionRecordType, true);
        return (this.session.getSubscriptionsByQuery(query));
    }


    /**
     *  Gets a <code>SubscriptionList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned
     *  list may contain only those subscriptions that are accessible
     *  through this session.
     *  
     *  In effective mode, subscriptions are returned that are
     *  currently effective.  In any effective mode, effective
     *  subscriptions and those currently expired are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Subscription</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptionsOnDate(org.osid.calendaring.DateTime from, 
                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.subscription.SubscriptionQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getSubscriptionsByQuery(query));
    }
        

    /**
     *  Gets a <code> SubscriptionList </code> of a subscription genus
     *  type and effective during the entire given date range
     *  inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned
     *  list may contain only those subscriptions that are accessible
     *  through this session.
     *  
     *  In effective mode, subscriptions are returned that are
     *  currently effective. In any effective mode, effective
     *  subscriptions and those currently expired are returned.
     *
     *  @param  subscriptionGenusType a subscription genus type 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned <code> Subscriptions </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> subscriptionGenusType, 
     *          from, </code> or <code> to </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptionsByGenusTypeOnDate(org.osid.type.Type subscriptionGenusType, 
                                                                                    org.osid.calendaring.DateTime from, 
                                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.subscription.SubscriptionQuery query = getQuery();
        query.matchGenusType(subscriptionGenusType, true);
        query.matchDate(from, to, true);
        return (this.session.getSubscriptionsByQuery(query));
    }


    /**
     *  Gets a list of subscriptions corresponding to a subscriber
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned
     *  list may contain only those subscriptions that are accessible
     *  through this session.
     *
     *  In effective mode, subscriptions are returned that are
     *  currently effective.  In any effective mode, effective
     *  subscriptions and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the subscriber
     *  @return the returned <code>SubscriptionList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.subscription.SubscriptionList getSubscriptionsForSubscriber(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.subscription.SubscriptionQuery query = getQuery();
        query.matchSubscriberId(resourceId, true);
        return (this.session.getSubscriptionsByQuery(query));
    }


    /**
     *  Gets a list of subscriptions corresponding to a subscriber
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned list
     *  may contain only those subscriptions that are accessible
     *  through this session.
     *
     *  In effective mode, subscriptions are returned that are
     *  currently effective.  In any effective mode, effective
     *  subscriptions and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the subscriber
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>SubscriptionList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptionsForSubscriberOnDate(org.osid.id.Id resourceId,
                                                                                      org.osid.calendaring.DateTime from,
                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.subscription.SubscriptionQuery query = getQuery();
        query.matchSubscriberId(resourceId, true);
        query.matchDate(from, to, true);
        return (this.session.getSubscriptionsByQuery(query));
    }

    
    /**
     *  Gets a list of subscriptions of the given genus type
     *  corresponding to a subscriber <code> Id. </code>
     *  
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned
     *  list may contain only those entries that are accessible
     *  through this session.
     *  
     *  In effective mode, subscriptions are returned that are
     *  currently effective. In any effective mode, effective
     *  subscriptions and those currently expired are returned.
     *
     *  @param  resourceId the <code> Id </code> of the resource 
     *  @param  subscriptionGenusType a subscription genus type 
     *  @return the returned <code> SubscriptionList </code> 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> or 
     *          <code> subscriptionGenusType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptionsByGenusTypeForSubscriber(org.osid.id.Id resourceId, 
                                                                                           org.osid.type.Type subscriptionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.subscription.SubscriptionQuery query = getQuery();
        query.matchSubscriberId(resourceId, true);
        query.matchGenusType(subscriptionGenusType, true);
        return (this.session.getSubscriptionsByQuery(query));
    }


    /**
     *  Gets a list of all subscriptions of the given genus type
     *  corresponding to a subscriber <code> Id </code> and effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned
     *  list may contain only those entries that are accessible
     *  through this session.
     *  
     *  In effective mode, subscriptions are returned that are
     *  currently effective. In any effective mode, effective
     *  subscriptions and those currently expired are returned.
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  subscriptionGenusType a subscription genus type 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned <code> SubscriptionList </code> 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less 
     *          than <code> from </code> 
     *  @throws org.osid.NullArgumentException <code> resourceId, 
     *          subscriptionGenusType, from </code> or <code> to </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptionsByGenusTypeForSubscriberOnDate(org.osid.id.Id resourceId, 
                                                                                                 org.osid.type.Type subscriptionGenusType, 
                                                                                                 org.osid.calendaring.DateTime from, 
                                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.subscription.SubscriptionQuery query = getQuery();
        query.matchSubscriberId(resourceId, true);
        query.matchGenusType(subscriptionGenusType, true);
        query.matchDate(from, to, true);
        return (this.session.getSubscriptionsByQuery(query));
    }


    /**
     *  Gets a list of subscriptions corresponding to a dispatch
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned
     *  list may contain only those subscriptions that are accessible
     *  through this session.
     *
     *  In effective mode, subscriptions are returned that are
     *  currently effective.  In any effective mode, effective
     *  subscriptions and those currently expired are returned.
     *
     *  @param  dispatchId the <code>Id</code> of the dispatch
     *  @return the returned <code>SubscriptionList</code>
     *  @throws org.osid.NullArgumentException <code>dispatchId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.subscription.SubscriptionList getSubscriptionsForDispatch(org.osid.id.Id dispatchId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.subscription.SubscriptionQuery query = getQuery();
        query.matchDispatchId(dispatchId, true);
        return (this.session.getSubscriptionsByQuery(query));
    }


    /**
     *  Gets a list of subscriptions corresponding to a dispatch
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned list
     *  may contain only those subscriptions that are accessible
     *  through this session.
     *
     *  In effective mode, subscriptions are returned that are
     *  currently effective.  In any effective mode, effective
     *  subscriptions and those currently expired are returned.
     *
     *  @param  dispatchId the <code>Id</code> of the dispatch
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>SubscriptionList</code>
     *  @throws org.osid.NullArgumentException <code>dispatchId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptionsForDispatchOnDate(org.osid.id.Id dispatchId,
                                                                                    org.osid.calendaring.DateTime from,
                                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.subscription.SubscriptionQuery query = getQuery();
        query.matchDispatchId(dispatchId, true);
        query.matchDate(from, to, true);
        return (this.session.getSubscriptionsByQuery(query));
    }


    /**
     *  Gets a list of all subscriptions of the given genus type
     *  corresponding to a dispatch <code> Id. </code>
     *  
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned
     *  list may contain only those entries that are accessible
     *  through this session.
     *  
     *  In effective mode, subscriptions are returned that are
     *  currently effective. In any effective mode, effective
     *  subscriptions and those currently expired are returned.
     *
     *  @param  dispatchId the <code> Id </code> of the dispatch 
     *  @param  subscriptionGenusType a subscription genus type 
     *  @return the returned <code> SubscriptionList </code> 
     *  @throws org.osid.NullArgumentException <code> dispatchId </code> or 
     *          <code> subscriptionGenusType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptionsByGenusTypeForDispatch(org.osid.id.Id dispatchId, 
                                                                                         org.osid.type.Type subscriptionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.subscription.SubscriptionQuery query = getQuery();
        query.matchDispatchId(dispatchId, true);
        query.matchGenusType(subscriptionGenusType, true);
        return (this.session.getSubscriptionsByQuery(query));
    }


    /**
     *  Gets a list of all subscriptions of the given genus type
     *  corresponding to a dispatch <code> Id </code> and effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned
     *  list may contain only those entries that are accessible
     *  through this session.
     *  
     *  In effective mode, subscriptions are returned that are
     *  currently effective. In any effective mode, effective
     *  subscriptions and those currently expired are returned.
     *
     *  @param  dispatchId a dispatch <code> Id </code> 
     *  @param  subscriptionGenusType a subscription genus type 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned <code> SubscriptionList </code> 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is
     *          less than <code> from </code>
     *  @throws org.osid.NullArgumentException <code> dispatchId,
     *          subscriptionGenusType, from </code> or <code> to
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    public org.osid.subscription.SubscriptionList getSubscriptionsByGenusTypeForDispatchOnDate(org.osid.id.Id dispatchId, 
                                                                                               org.osid.type.Type subscriptionGenusType, 
                                                                                               org.osid.calendaring.DateTime from, 
                                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.subscription.SubscriptionQuery query = getQuery();
        query.matchDispatchId(dispatchId, true);
        query.matchGenusType(subscriptionGenusType, true);
        query.matchDate(from, to, true);
        return (this.session.getSubscriptionsByQuery(query));
    }


    /**
     *  Gets a list of subscriptions corresponding to subscriber and
     *  dispatch <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned
     *  list may contain only those subscriptions that are accessible
     *  through this session.
     *
     *  In effective mode, subscriptions are returned that are
     *  currently effective.  In any effective mode, effective
     *  subscriptions and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the subscriber
     *  @param  dispatchId the <code>Id</code> of the dispatch
     *  @return the returned <code>SubscriptionList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>dispatchId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptionsForSubscriberAndDispatch(org.osid.id.Id resourceId,
                                                                                           org.osid.id.Id dispatchId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.subscription.SubscriptionQuery query = getQuery();
        query.matchSubscriberId(resourceId, true);
        query.matchDispatchId(dispatchId, true);
        return (this.session.getSubscriptionsByQuery(query));
    }


    /**
     *  Gets a list of subscriptions corresponding to subscriber and
     *  dispatch <code>Ids</code> and effective during the entire
     *  given date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned
     *  list may contain only those subscriptions that are accessible
     *  through this session.
     *
     *  In effective mode, subscriptions are returned that are
     *  currently effective.  In any effective mode, effective
     *  subscriptions and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the subscriber
     *  @param  dispatchId the <code>Id</code> of the dispatch
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>SubscriptionList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>dispatchId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptionsForSubscriberAndDispatchOnDate(org.osid.id.Id resourceId,
                                                                                                 org.osid.id.Id dispatchId,
                                                                                                 org.osid.calendaring.DateTime from,
                                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.subscription.SubscriptionQuery query = getQuery();
        query.matchSubscriberId(resourceId, true);
        query.matchDispatchId(dispatchId, true);
        query.matchDate(from, to, true);
        return (this.session.getSubscriptionsByQuery(query));
    }


    /**
     *  Gets a list of all subscriptions of the given genus type
     *  corresponding to a susbcriber and dispatch <code> Id. </code>
     *  
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned
     *  list may contain only those entries that are accessible
     *  through this session.
     *  
     *  In effective mode, subscriptions are returned that are
     *  currently effective. In any effective mode, effective
     *  subscriptions and those currently expired are returned.
     *
     *  @param  resourceId the <code> Id </code> of the resource 
     *  @param  dispatchId the <code> Id </code> of the dispatch 
     *  @param  subscriptionGenusType a subscription genus type 
     *  @return the returned <code> SubscriptionList </code> 
     *  @throws org.osid.NullArgumentException <code> resourceId,
     *          dispatchId </code> or <code> subscriptionGenusType
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptionsByGenusTypeForSubscriberAndDispatch(org.osid.id.Id resourceId, 
                                                                                                      org.osid.id.Id dispatchId, 
                                                                                                      org.osid.type.Type subscriptionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.subscription.SubscriptionQuery query = getQuery();
        query.matchSubscriberId(resourceId, true);
        query.matchDispatchId(dispatchId, true);
        query.matchGenusType(subscriptionGenusType, true);
        return (this.session.getSubscriptionsByQuery(query));
    }


    /**
     *  Gets a list of all subscriptions of the given genus type
     *  corresponding to a subscriber and dispatch <code> Id </code>
     *  and effective during the entire given date range inclusive but
     *  not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned
     *  list may contain only those entries that are accessible
     *  through this session.
     *  
     *  In effective mode, subscriptions are returned that are
     *  currently effective. In any effective mode, effective
     *  subscriptions and those currently expired are returned.
     *
     *  @param  resourceId the <code> Id </code> of the resource 
     *  @param  dispatchId a dispatch <code> Id </code> 
     *  @param  subscriptionGenusType a subscription genus type 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned <code> SubscriptionList </code> 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is
     *          less than <code> from </code>
     *  @throws org.osid.NullArgumentException <code> resourceId,
     *          dispatch, subscriptionGenusType, from </code> or
     *          <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptionsByGenusTypeForSubscriberAndDispatchOnDate(org.osid.id.Id resourceId, 
                                                                                                            org.osid.id.Id dispatchId, 
                                                                                                            org.osid.type.Type subscriptionGenusType, 
                                                                                                            org.osid.calendaring.DateTime from, 
                                                                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.subscription.SubscriptionQuery query = getQuery();
        query.matchSubscriberId(resourceId, true);
        query.matchDispatchId(dispatchId, true);
        query.matchGenusType(subscriptionGenusType, true);
        query.matchDate(from, to, true);
        return (this.session.getSubscriptionsByQuery(query));
    }


    /**
     *  Gets all <code>Subscriptions</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned list
     *  may contain only those subscriptions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, subscriptions are returned that are currently
     *  effective.  In any effective mode, effective subscriptions and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Subscriptions</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptions()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.subscription.SubscriptionQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getSubscriptionsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.subscription.SubscriptionQuery getQuery() {
        org.osid.subscription.SubscriptionQuery query = this.session.getSubscriptionQuery();
        
        if (isEffectiveOnly()) {
            query.matchEffective(true);
        }

        return (query);
    }
}
