//
// AbstractImmutableOrganization.java
//
//     Wraps a mutable Organization to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.personnel.organization.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Organization</code> to hide modifiers. This
 *  wrapper provides an immutized Organization from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying organization whose state changes are visible.
 */

public abstract class AbstractImmutableOrganization
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableTemporalOsidObject
    implements org.osid.personnel.Organization {

    private final org.osid.personnel.Organization organization;


    /**
     *  Constructs a new <code>AbstractImmutableOrganization</code>.
     *
     *  @param organization the organization to immutablize
     *  @throws org.osid.NullArgumentException <code>organization</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableOrganization(org.osid.personnel.Organization organization) {
        super(organization);
        this.organization = organization;
        return;
    }


    /**
     *  Gets the display label or code for this organization. 
     *
     *  @return the display label 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getDisplayLabel() {
        return (this.organization.getDisplayLabel());
    }


    /**
     *  Gets the record corresponding to the given <code> Organization </code> 
     *  record <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record. The <code> organizationRecordType 
     *  </code> may be the <code> Type </code> returned in <code> 
     *  getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(organizationRecordType) 
     *  </code> is <code> true </code> . 
     *
     *  @param  organizationRecordType the type of organization record to 
     *          retrieve 
     *  @return the organization record 
     *  @throws org.osid.NullArgumentException <code> organizationRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(organizationRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.personnel.records.OrganizationRecord getOrganizationRecord(org.osid.type.Type organizationRecordType)
        throws org.osid.OperationFailedException {

        return (this.organization.getOrganizationRecord(organizationRecordType));
    }
}

