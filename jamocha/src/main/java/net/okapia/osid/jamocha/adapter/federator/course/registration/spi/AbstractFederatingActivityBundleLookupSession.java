//
// AbstractFederatingActivityBundleLookupSession.java
//
//     An abstract federating adapter for an ActivityBundleLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.course.registration.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for an
 *  ActivityBundleLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingActivityBundleLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.course.registration.ActivityBundleLookupSession>
    implements org.osid.course.registration.ActivityBundleLookupSession {

    private boolean parallel = false;
    private org.osid.course.CourseCatalog courseCatalog = new net.okapia.osid.jamocha.nil.course.coursecatalog.UnknownCourseCatalog();


    /**
     *  Constructs a new <code>AbstractFederatingActivityBundleLookupSession</code>.
     */

    protected AbstractFederatingActivityBundleLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.course.registration.ActivityBundleLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>CourseCatalog/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>CourseCatalog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.courseCatalog.getId());
    }


    /**
     *  Gets the <code>CourseCatalog</code> associated with this 
     *  session.
     *
     *  @return the <code>CourseCatalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.courseCatalog);
    }


    /**
     *  Sets the <code>CourseCatalog</code>.
     *
     *  @param  courseCatalog the course catalog for this session
     *  @throws org.osid.NullArgumentException <code>courseCatalog</code>
     *          is <code>null</code>
     */

    protected void setCourseCatalog(org.osid.course.CourseCatalog courseCatalog) {
        nullarg(courseCatalog, "course catalog");
        this.courseCatalog = courseCatalog;
        return;
    }


    /**
     *  Tests if this user can perform <code>ActivityBundle</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupActivityBundles() {
        for (org.osid.course.registration.ActivityBundleLookupSession session : getSessions()) {
            if (session.canLookupActivityBundles()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>ActivityBundle</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeActivityBundleView() {
        for (org.osid.course.registration.ActivityBundleLookupSession session : getSessions()) {
            session.useComparativeActivityBundleView();
        }

        return;
    }


    /**
     *  A complete view of the <code>ActivityBundle</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryActivityBundleView() {
        for (org.osid.course.registration.ActivityBundleLookupSession session : getSessions()) {
            session.usePlenaryActivityBundleView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include activity bundles in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        for (org.osid.course.registration.ActivityBundleLookupSession session : getSessions()) {
            session.useFederatedCourseCatalogView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        for (org.osid.course.registration.ActivityBundleLookupSession session : getSessions()) {
            session.useIsolatedCourseCatalogView();
        }

        return;
    }

     
    /**
     *  Gets the <code>ActivityBundle</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>ActivityBundle</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>ActivityBundle</code> and
     *  retained for compatibility.
     *
     *  @param  activityBundleId <code>Id</code> of the
     *          <code>ActivityBundle</code>
     *  @return the activity bundle
     *  @throws org.osid.NotFoundException <code>activityBundleId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>activityBundleId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundle getActivityBundle(org.osid.id.Id activityBundleId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.course.registration.ActivityBundleLookupSession session : getSessions()) {
            try {
                return (session.getActivityBundle(activityBundleId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(activityBundleId + " not found");
    }


    /**
     *  Gets an <code>ActivityBundleList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  activityBundles specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>ActivityBundles</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  activityBundleIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>ActivityBundle</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>activityBundleIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleList getActivityBundlesByIds(org.osid.id.IdList activityBundleIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.course.registration.activitybundle.MutableActivityBundleList ret = new net.okapia.osid.jamocha.course.registration.activitybundle.MutableActivityBundleList();

        try (org.osid.id.IdList ids = activityBundleIds) {
            while (ids.hasNext()) {
                ret.addActivityBundle(getActivityBundle(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets an <code>ActivityBundleList</code> corresponding to the given
     *  activity bundle genus <code>Type</code> which does not include
     *  activity bundles of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  activity bundles or an error results. Otherwise, the returned list
     *  may contain only those activity bundles that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  activityBundleGenusType an activityBundle genus type 
     *  @return the returned <code>ActivityBundle</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>activityBundleGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleList getActivityBundlesByGenusType(org.osid.type.Type activityBundleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.registration.activitybundle.FederatingActivityBundleList ret = getActivityBundleList();

        for (org.osid.course.registration.ActivityBundleLookupSession session : getSessions()) {
            ret.addActivityBundleList(session.getActivityBundlesByGenusType(activityBundleGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>ActivityBundleList</code> corresponding to the given
     *  activity bundle genus <code>Type</code> and include any additional
     *  activity bundles with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  activity bundles or an error results. Otherwise, the returned list
     *  may contain only those activity bundles that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  activityBundleGenusType an activityBundle genus type 
     *  @return the returned <code>ActivityBundle</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>activityBundleGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleList getActivityBundlesByParentGenusType(org.osid.type.Type activityBundleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.registration.activitybundle.FederatingActivityBundleList ret = getActivityBundleList();

        for (org.osid.course.registration.ActivityBundleLookupSession session : getSessions()) {
            ret.addActivityBundleList(session.getActivityBundlesByParentGenusType(activityBundleGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>ActivityBundleList</code> containing the given
     *  activity bundle record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  activity bundles or an error results. Otherwise, the returned list
     *  may contain only those activity bundles that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  activityBundleRecordType an activityBundle record type 
     *  @return the returned <code>ActivityBundle</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>activityBundleRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleList getActivityBundlesByRecordType(org.osid.type.Type activityBundleRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.registration.activitybundle.FederatingActivityBundleList ret = getActivityBundleList();

        for (org.osid.course.registration.ActivityBundleLookupSession session : getSessions()) {
            ret.addActivityBundleList(session.getActivityBundlesByRecordType(activityBundleRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code> ActivityBundleList </code> for a given course
     *  offering <code> . </code> In plenary mode, the returned list
     *  contains all known courses or an error results. Otherwise, the
     *  returned list may contain only those activity bundles that are
     *  accessible through this session.
     *
     *  @param  courseOfferingId a course offering <code> Id </code>
     *  @return the returned <code> ActivityBundle </code> list
     *  @throws org.osid.NullArgumentException <code> courseOfferingId </code>
     *          is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleList getActivityBundlesForCourseOffering(org.osid.id.Id courseOfferingId)
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.registration.activitybundle.FederatingActivityBundleList ret = getActivityBundleList();

        for (org.osid.course.registration.ActivityBundleLookupSession session : getSessions()) {
            ret.addActivityBundleList(session.getActivityBundlesForCourseOffering(courseOfferingId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code> ActivityBundleList </code> for a given
     *  activity. In plenary mode, the returned list contains all
     *  known courses or an error results. Otherwise, the returned
     *  list may contain only those activity bundles that are
     *  accessible through this session.
     *
     *  @param  activityId an activity <code> Id </code>
     *  @return the returned <code> ActivityBundle </code> list
     *  @throws org.osid.NullArgumentException <code> activityId </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleList getActivityBundlesByActivity(org.osid.id.Id activityId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.registration.activitybundle.FederatingActivityBundleList ret = getActivityBundleList();

        for (org.osid.course.registration.ActivityBundleLookupSession session : getSessions()) {
            ret.addActivityBundleList(session.getActivityBundlesByActivity(activityId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>ActivityBundles</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  activity bundles or an error results. Otherwise, the returned list
     *  may contain only those activity bundles that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>ActivityBundles</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleList getActivityBundles()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.registration.activitybundle.FederatingActivityBundleList ret = getActivityBundleList();

        for (org.osid.course.registration.ActivityBundleLookupSession session : getSessions()) {
            ret.addActivityBundleList(session.getActivityBundles());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.course.registration.activitybundle.FederatingActivityBundleList getActivityBundleList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.course.registration.activitybundle.ParallelActivityBundleList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.course.registration.activitybundle.CompositeActivityBundleList());
        }
    }
}
