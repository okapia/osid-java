//
// GraphElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.topology.graph.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class GraphElements
    extends net.okapia.osid.jamocha.spi.OsidCatalogElements {


    /**
     *  Gets the GraphElement Id.
     *
     *  @return the graph element Id
     */

    public static org.osid.id.Id getGraphEntityId() {
        return (makeEntityId("osid.topology.Graph"));
    }


    /**
     *  Gets the NodeId element Id.
     *
     *  @return the NodeId element Id
     */

    public static org.osid.id.Id getNodeId() {
        return (makeQueryElementId("osid.topology.graph.NodeId"));
    }


    /**
     *  Gets the Node element Id.
     *
     *  @return the Node element Id
     */

    public static org.osid.id.Id getNode() {
        return (makeQueryElementId("osid.topology.graph.Node"));
    }


    /**
     *  Gets the EdgeId element Id.
     *
     *  @return the EdgeId element Id
     */

    public static org.osid.id.Id getEdgeId() {
        return (makeQueryElementId("osid.topology.graph.EdgeId"));
    }


    /**
     *  Gets the Edge element Id.
     *
     *  @return the Edge element Id
     */

    public static org.osid.id.Id getEdge() {
        return (makeQueryElementId("osid.topology.graph.Edge"));
    }


    /**
     *  Gets the AncestorGraphId element Id.
     *
     *  @return the AncestorGraphId element Id
     */

    public static org.osid.id.Id getAncestorGraphId() {
        return (makeQueryElementId("osid.topology.graph.AncestorGraphId"));
    }


    /**
     *  Gets the AncestorGraph element Id.
     *
     *  @return the AncestorGraph element Id
     */

    public static org.osid.id.Id getAncestorGraph() {
        return (makeQueryElementId("osid.topology.graph.AncestorGraph"));
    }


    /**
     *  Gets the DescendantGraphId element Id.
     *
     *  @return the DescendantGraphId element Id
     */

    public static org.osid.id.Id getDescendantGraphId() {
        return (makeQueryElementId("osid.topology.graph.DescendantGraphId"));
    }


    /**
     *  Gets the DescendantGraph element Id.
     *
     *  @return the DescendantGraph element Id
     */

    public static org.osid.id.Id getDescendantGraph() {
        return (makeQueryElementId("osid.topology.graph.DescendantGraph"));
    }
}
