//
// AbstractIndexedMapChecklistLookupSession.java
//
//    A simple framework for providing a Checklist lookup service
//    backed by a fixed collection of checklists with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.checklist.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Checklist lookup service backed by a
 *  fixed collection of checklists. The checklists are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some checklists may be compatible
 *  with more types than are indicated through these checklist
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Checklists</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapChecklistLookupSession
    extends AbstractMapChecklistLookupSession
    implements org.osid.checklist.ChecklistLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.checklist.Checklist> checklistsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.checklist.Checklist>());
    private final MultiMap<org.osid.type.Type, org.osid.checklist.Checklist> checklistsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.checklist.Checklist>());


    /**
     *  Makes a <code>Checklist</code> available in this session.
     *
     *  @param  checklist a checklist
     *  @throws org.osid.NullArgumentException <code>checklist<code> is
     *          <code>null</code>
     */

    @Override
    protected void putChecklist(org.osid.checklist.Checklist checklist) {
        super.putChecklist(checklist);

        this.checklistsByGenus.put(checklist.getGenusType(), checklist);
        
        try (org.osid.type.TypeList types = checklist.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.checklistsByRecord.put(types.getNextType(), checklist);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a checklist from this session.
     *
     *  @param checklistId the <code>Id</code> of the checklist
     *  @throws org.osid.NullArgumentException <code>checklistId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeChecklist(org.osid.id.Id checklistId) {
        org.osid.checklist.Checklist checklist;
        try {
            checklist = getChecklist(checklistId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.checklistsByGenus.remove(checklist.getGenusType());

        try (org.osid.type.TypeList types = checklist.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.checklistsByRecord.remove(types.getNextType(), checklist);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeChecklist(checklistId);
        return;
    }


    /**
     *  Gets a <code>ChecklistList</code> corresponding to the given
     *  checklist genus <code>Type</code> which does not include
     *  checklists of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known checklists or an error results. Otherwise,
     *  the returned list may contain only those checklists that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  checklistGenusType a checklist genus type 
     *  @return the returned <code>Checklist</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>checklistGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistList getChecklistsByGenusType(org.osid.type.Type checklistGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.checklist.checklist.ArrayChecklistList(this.checklistsByGenus.get(checklistGenusType)));
    }


    /**
     *  Gets a <code>ChecklistList</code> containing the given
     *  checklist record <code>Type</code>. In plenary mode, the
     *  returned list contains all known checklists or an error
     *  results. Otherwise, the returned list may contain only those
     *  checklists that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  checklistRecordType a checklist record type 
     *  @return the returned <code>checklist</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>checklistRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistList getChecklistsByRecordType(org.osid.type.Type checklistRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.checklist.checklist.ArrayChecklistList(this.checklistsByRecord.get(checklistRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.checklistsByGenus.clear();
        this.checklistsByRecord.clear();

        super.close();

        return;
    }
}
