//
// AbstractDispatchSearch.java
//
//     A template for making a Dispatch Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.subscription.dispatch.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing dispatch searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractDispatchSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.subscription.DispatchSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.subscription.records.DispatchSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.subscription.DispatchSearchOrder dispatchSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of dispatches. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  dispatchIds list of dispatches
     *  @throws org.osid.NullArgumentException
     *          <code>dispatchIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongDispatches(org.osid.id.IdList dispatchIds) {
        while (dispatchIds.hasNext()) {
            try {
                this.ids.add(dispatchIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongDispatches</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of dispatch Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getDispatchIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  dispatchSearchOrder dispatch search order 
     *  @throws org.osid.NullArgumentException
     *          <code>dispatchSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>dispatchSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderDispatchResults(org.osid.subscription.DispatchSearchOrder dispatchSearchOrder) {
	this.dispatchSearchOrder = dispatchSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.subscription.DispatchSearchOrder getDispatchSearchOrder() {
	return (this.dispatchSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given dispatch search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a dispatch implementing the requested record.
     *
     *  @param dispatchSearchRecordType a dispatch search record
     *         type
     *  @return the dispatch search record
     *  @throws org.osid.NullArgumentException
     *          <code>dispatchSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(dispatchSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.subscription.records.DispatchSearchRecord getDispatchSearchRecord(org.osid.type.Type dispatchSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.subscription.records.DispatchSearchRecord record : this.records) {
            if (record.implementsRecordType(dispatchSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(dispatchSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this dispatch search. 
     *
     *  @param dispatchSearchRecord dispatch search record
     *  @param dispatchSearchRecordType dispatch search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addDispatchSearchRecord(org.osid.subscription.records.DispatchSearchRecord dispatchSearchRecord, 
                                           org.osid.type.Type dispatchSearchRecordType) {

        addRecordType(dispatchSearchRecordType);
        this.records.add(dispatchSearchRecord);        
        return;
    }
}
