//
// ProvisionReturnElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.provisionreturn.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class ProvisionReturnElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the ProvisionReturnElement Id.
     *
     *  @return the provision return element Id
     */

    public static org.osid.id.Id getProvisionReturnEntityId() {
        return (makeEntityId("osid.provisioning.ProvisionReturn"));
    }


    /**
     *  Gets the ReturnDate element Id.
     *
     *  @return the ReturnDate element Id
     */

    public static org.osid.id.Id getReturnDate() {
        return (makeElementId("osid.provisioning.provisionreturn.ReturnDate"));
    }


    /**
     *  Gets the ReturnerId element Id.
     *
     *  @return the ReturnerId element Id
     */

    public static org.osid.id.Id getReturnerId() {
        return (makeElementId("osid.provisioning.provisionreturn.ReturnerId"));
    }


    /**
     *  Gets the Returner element Id.
     *
     *  @return the Returner element Id
     */

    public static org.osid.id.Id getReturner() {
        return (makeElementId("osid.provisioning.provisionreturn.Returner"));
    }


    /**
     *  Gets the ReturningAgentId element Id.
     *
     *  @return the ReturningAgentId element Id
     */

    public static org.osid.id.Id getReturningAgentId() {
        return (makeElementId("osid.provisioning.provisionreturn.ReturningAgentId"));
    }


    /**
     *  Gets the ReturningAgent element Id.
     *
     *  @return the ReturningAgent element Id
     */

    public static org.osid.id.Id getReturningAgent() {
        return (makeElementId("osid.provisioning.provisionreturn.ReturningAgent"));
    }
}
