//
// InvariantIndexedMapProxyActionEnablerLookupSession
//
//    Implements an ActionEnabler lookup service backed by a fixed
//    collection of actionEnablers indexed by their types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.control.rules;


/**
 *  Implements a ActionEnabler lookup service backed by a fixed
 *  collection of actionEnablers. The actionEnablers are indexed by
 *  {@code Id}, genus and record types.
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some actionEnablers may be compatible
 *  with more types than are indicated through these actionEnabler
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 */

public final class InvariantIndexedMapProxyActionEnablerLookupSession
    extends net.okapia.osid.jamocha.core.control.rules.spi.AbstractIndexedMapActionEnablerLookupSession
    implements org.osid.control.rules.ActionEnablerLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapProxyActionEnablerLookupSession}
     *  using an array of action enablers.
     *
     *  @param system the system
     *  @param actionEnablers an array of action enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code system},
     *          {@code actionEnablers} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapProxyActionEnablerLookupSession(org.osid.control.System system,
                                                         org.osid.control.rules.ActionEnabler[] actionEnablers, 
                                                         org.osid.proxy.Proxy proxy) {

        setSystem(system);
        setSessionProxy(proxy);
        putActionEnablers(actionEnablers);

        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapProxyActionEnablerLookupSession}
     *  using a collection of action enablers.
     *
     *  @param system the system
     *  @param actionEnablers a collection of action enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code system},
     *          {@code actionEnablers} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapProxyActionEnablerLookupSession(org.osid.control.System system,
                                                         java.util.Collection<? extends org.osid.control.rules.ActionEnabler> actionEnablers,
                                                         org.osid.proxy.Proxy proxy) {

        setSystem(system);
        setSessionProxy(proxy);
        putActionEnablers(actionEnablers);

        return;
    }
}
