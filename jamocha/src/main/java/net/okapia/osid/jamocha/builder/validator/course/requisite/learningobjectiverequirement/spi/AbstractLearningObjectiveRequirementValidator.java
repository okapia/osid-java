//
// AbstractLearningObjectiveRequirementValidator.java
//
//     Validates a LearningObjectiveRequirement.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.course.requisite.learningobjectiverequirement.spi;


/**
 *  Validates a LearningObjectiveRequirement.
 */

public abstract class AbstractLearningObjectiveRequirementValidator
    extends net.okapia.osid.jamocha.builder.validator.spi.AbstractOsidRuleValidator {


    /**
     *  Constructs a new <code>AbstractLearningObjectiveRequirementValidator</code>.
     */

    protected AbstractLearningObjectiveRequirementValidator() {
        return;
    }


    /**
     *  Constructs a new <code>AbstractLearningObjectiveRequirementValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    protected AbstractLearningObjectiveRequirementValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates a LearningObjectiveRequirement.
     *
     *  @param learningObjectiveRequirement a learning objective requirement to validate
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>learningObjectiveRequirement</code>
     *          is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in
     *          assembly
     */
    
    public void validate(org.osid.course.requisite.LearningObjectiveRequirement learningObjectiveRequirement) {
        super.validate(learningObjectiveRequirement);

        test(learningObjectiveRequirement.getAltRequisites(), "getAltRequisites()");
        testNestedObject(learningObjectiveRequirement, "getLearningObjective");
        
        testConditionalMethod(learningObjectiveRequirement, "getMinimumProficiencyId", learningObjectiveRequirement.hasMinimumProficiency(), "hasMinimumProficiency()");
        testConditionalMethod(learningObjectiveRequirement, "getMinimumProficiency", learningObjectiveRequirement.hasMinimumProficiency(), "hasMinimumProficiency()");
        if (learningObjectiveRequirement.hasMinimumProficiency()) {
            testNestedObject(learningObjectiveRequirement, "getMinimumProficiency");
        }

        return;
    }
}
