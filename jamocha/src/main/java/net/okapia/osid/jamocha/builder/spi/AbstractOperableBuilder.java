//
// AbstractOperableBuilder.java
//
//     Defines a builder for an Operable.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.spi;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines the Operable builder.
 */

public abstract class AbstractOperableBuilder<T extends AbstractOperableBuilder<? extends T>>
    extends AbstractBuilder<T> {

    private final OperableMiter operable;


    /**
     *  Creates a new <code>AbstractOperableBuilder</code>.
     *
     *  @param operable an operable miter interface
     *  @throws org.osid.NullArgumentException <code>operable</code> is
     *          <code>null</code>
     */

    protected AbstractOperableBuilder(OperableMiter operable) {
        nullarg(operable, "operable");
        this.operable = operable;
        return;
    }


    /**
     *  Enables this object. Enabling an operable overrides any
     *  enabling rule that may exist. 
     */
    
    public T enabled() {
        this.operable.setEnabled(true);
        return (self());
    }


    /**
     *  Disables this object. Disabling an operable overrides any
     *  enabling rule that may exist.
     */
    
    public T disabled() {
        this.operable.setDisabled(true);
        return (self());
    }


    /**
     *  Sets the operational flag.
     *
     *  @param operational <code>true</code>if operational,
     *         <code>false</code> if not operational
     */
    
    public T operational(boolean operational) {
        this.operable.setOperational(operational);
        return (self());
    }
}
