//
// ItemElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.billing.item.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class ItemElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the ItemElement Id.
     *
     *  @return the item element Id
     */

    public static org.osid.id.Id getItemEntityId() {
        return (makeEntityId("osid.billing.Item"));
    }


    /**
     *  Gets the CategoryId element Id.
     *
     *  @return the CategoryId element Id
     */

    public static org.osid.id.Id getCategoryId() {
        return (makeElementId("osid.billing.item.CategoryId"));
    }


    /**
     *  Gets the Category element Id.
     *
     *  @return the Category element Id
     */

    public static org.osid.id.Id getCategory() {
        return (makeElementId("osid.billing.item.Category"));
    }


    /**
     *  Gets the AccountId element Id.
     *
     *  @return the AccountId element Id
     */

    public static org.osid.id.Id getAccountId() {
        return (makeElementId("osid.billing.item.AccountId"));
    }


    /**
     *  Gets the Account element Id.
     *
     *  @return the Account element Id
     */

    public static org.osid.id.Id getAccount() {
        return (makeElementId("osid.billing.item.Account"));
    }


    /**
     *  Gets the ProductId element Id.
     *
     *  @return the ProductId element Id
     */

    public static org.osid.id.Id getProductId() {
        return (makeElementId("osid.billing.item.ProductId"));
    }


    /**
     *  Gets the Product element Id.
     *
     *  @return the Product element Id
     */

    public static org.osid.id.Id getProduct() {
        return (makeElementId("osid.billing.item.Product"));
    }


    /**
     *  Gets the Amount element Id.
     *
     *  @return the Amount element Id
     */

    public static org.osid.id.Id getAmount() {
        return (makeElementId("osid.billing.item.Amount"));
    }


    /**
     *  Gets the RecurringInterval element Id.
     *
     *  @return the RecurringInterval element Id
     */

    public static org.osid.id.Id getRecurringInterval() {
        return (makeElementId("osid.billing.item.RecurringInterval"));
    }


    /**
     *  Gets the Debit element Id.
     *
     *  @return the Debit element Id
     */

    public static org.osid.id.Id getDebit() {
        return (makeElementId("osid.billing.item.Debit"));
    }


    /**
     *  Gets the EntryId element Id.
     *
     *  @return the EntryId element Id
     */

    public static org.osid.id.Id getEntryId() {
        return (makeQueryElementId("osid.billing.item.EntryId"));
    }


    /**
     *  Gets the Entry element Id.
     *
     *  @return the Entry element Id
     */

    public static org.osid.id.Id getEntry() {
        return (makeQueryElementId("osid.billing.item.Entry"));
    }


    /**
     *  Gets the BusinessId element Id.
     *
     *  @return the BusinessId element Id
     */

    public static org.osid.id.Id getBusinessId() {
        return (makeQueryElementId("osid.billing.item.BusinessId"));
    }


    /**
     *  Gets the Business element Id.
     *
     *  @return the Business element Id
     */

    public static org.osid.id.Id getBusiness() {
        return (makeQueryElementId("osid.billing.item.Business"));
    }
}
