//
// AbstractActivityUnitQuery.java
//
//     A template for making an ActivityUnit Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.activityunit.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for activity units.
 */

public abstract class AbstractActivityUnitQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOperableOsidObjectQuery
    implements org.osid.course.ActivityUnitQuery {

    private final java.util.Collection<org.osid.course.records.ActivityUnitQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the course <code> Id </code> for this query to match activity 
     *  units that have a related course. 
     *
     *  @param  courseId a course <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCourseId(org.osid.id.Id courseId, boolean match) {
        return;
    }


    /**
     *  Clears the course <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CourseQuery </code> is available. 
     *
     *  @return <code> true </code> if a course query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the course query 
     *  @throws org.osid.UnimplementedException <code> supportsCourseQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseQuery getCourseQuery() {
        throw new org.osid.UnimplementedException("supportsCourseQuery() is false");
    }


    /**
     *  Clears the course terms. 
     */

    @OSID @Override
    public void clearCourseTerms() {
        return;
    }


    /**
     *  Matches activity units with a total effort between the given durations 
     *  inclusive. 
     *
     *  @param  min low range 
     *  @param  max high range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> max </code> is less 
     *          than <code> min </code> 
     *  @throws org.osid.NullArgumentException <code> min </code> or <code> 
     *          max </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchTotalTargetEffort(org.osid.calendaring.Duration min, 
                                       org.osid.calendaring.Duration max, 
                                       boolean match) {
        return;
    }


    /**
     *  Matches an activity unit that has any total effort assigned. 
     *
     *  @param  match <code> true </code> to match activity units with any 
     *          total effort, <code> false </code> to match activity units 
     *          with no total effort 
     */

    @OSID @Override
    public void matchAnyTotalTargetEffort(boolean match) {
        return;
    }


    /**
     *  Clears the total effort terms. 
     */

    @OSID @Override
    public void clearTotalTargetEffortTerms() {
        return;
    }


    /**
     *  Matches activity units that are contact activities. 
     *
     *  @param  match <code> true </code> to match activity units that have 
     *          contact, <code> false </code> to match activity units with no 
     *          contact 
     */

    @OSID @Override
    public void matchContact(boolean match) {
        return;
    }


    /**
     *  Clears the contact terms. 
     */

    @OSID @Override
    public void clearContactTerms() {
        return;
    }


    /**
     *  Matches activity units with a total contact time between the given 
     *  durations inclusive. 
     *
     *  @param  min low range 
     *  @param  max high range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> max </code> is less 
     *          than <code> min </code> 
     *  @throws org.osid.NullArgumentException <code> min </code> or <code> 
     *          max </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchTotalTargetContactTime(org.osid.calendaring.Duration min, 
                                            org.osid.calendaring.Duration max, 
                                            boolean match) {
        return;
    }


    /**
     *  Matches an activity unit that has any total contact assigned. 
     *
     *  @param  match <code> true </code> to match activity units with any 
     *          total contatc, <code> false </code> to match activity units 
     *          with no total contact 
     */

    @OSID @Override
    public void matchAnyTotalTargetContactTime(boolean match) {
        return;
    }


    /**
     *  Clears the total contact terms. 
     */

    @OSID @Override
    public void clearTotalTargetContactTimeTerms() {
        return;
    }


    /**
     *  Matches activity units with a individual effort between the given 
     *  durations inclusive. 
     *
     *  @param  min low range 
     *  @param  max high range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> max </code> is less 
     *          than <code> min </code> 
     *  @throws org.osid.NullArgumentException <code> min </code> or <code> 
     *          max </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchTotalTargetIndividualEffort(org.osid.calendaring.Duration min, 
                                                 org.osid.calendaring.Duration max, 
                                                 boolean match) {
        return;
    }


    /**
     *  Matches an activity unit that has any individual effort assigned. 
     *
     *  @param  match <code> true </code> to match activity units with any 
     *          individual effort, <code> false </code> to match activity 
     *          units with no individual effort 
     */

    @OSID @Override
    public void matchAnyTotalTargetIndividualEffort(boolean match) {
        return;
    }


    /**
     *  Clears the individual effort terms. 
     */

    @OSID @Override
    public void clearTotalTargetIndividualEffortTerms() {
        return;
    }


    /**
     *  Matches activity units that recur weekly. 
     *
     *  @param  match <code> true </code> to match activity units that recur 
     *          weekly, <code> false </code> to match activity units with no 
     *          weekly recurrance 
     */

    @OSID @Override
    public void matchRecurringWeekly(boolean match) {
        return;
    }


    /**
     *  Clears the recurring weekly terms. 
     */

    @OSID @Override
    public void clearRecurringWeeklyTerms() {
        return;
    }


    /**
     *  Matches activity units with a weekly effort between the given 
     *  durations inclusive. 
     *
     *  @param  min low range 
     *  @param  max high range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> max </code> is less 
     *          than <code> min </code> 
     *  @throws org.osid.NullArgumentException <code> min </code> or <code> 
     *          max </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchWeeklyEffort(org.osid.calendaring.Duration min, 
                                  org.osid.calendaring.Duration max, 
                                  boolean match) {
        return;
    }


    /**
     *  Matches an activity unit that has any weekly effort assigned. 
     *
     *  @param  match <code> true </code> to match activity units with any 
     *          weekly effort, <code> false </code> to match activity units 
     *          with no weekly effort 
     */

    @OSID @Override
    public void matchAnyWeeklyEffort(boolean match) {
        return;
    }


    /**
     *  Clears the weekly effort terms. 
     */

    @OSID @Override
    public void clearWeeklyEffortTerms() {
        return;
    }


    /**
     *  Matches activity units with a weekly contact time between the given 
     *  durations inclusive. 
     *
     *  @param  min low range 
     *  @param  max high range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> max </code> is less 
     *          than <code> min </code> 
     *  @throws org.osid.NullArgumentException <code> min </code> or <code> 
     *          max </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchWeeklyContactTime(org.osid.calendaring.Duration min, 
                                       org.osid.calendaring.Duration max, 
                                       boolean match) {
        return;
    }


    /**
     *  Matches an activity unit that has any weekly contact time assigned. 
     *
     *  @param  match <code> true </code> to match activity units with any 
     *          weekly contact time, <code> false </code> to match activity 
     *          units with no weekly contact time 
     */

    @OSID @Override
    public void matchAnyWeeklyContactTime(boolean match) {
        return;
    }


    /**
     *  Clears the weekly contact time terms. 
     */

    @OSID @Override
    public void clearWeeklyContactTimeTerms() {
        return;
    }


    /**
     *  Matches activity units with a weekly individual effort between the 
     *  given durations inclusive. 
     *
     *  @param  min low range 
     *  @param  max high range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> max </code> is less 
     *          than <code> min </code> 
     *  @throws org.osid.NullArgumentException <code> min </code> or <code> 
     *          max </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchWeeklyIndividualEffort(org.osid.calendaring.Duration min, 
                                            org.osid.calendaring.Duration max, 
                                            boolean match) {
        return;
    }


    /**
     *  Matches an activity unit that has any weekly individual effort 
     *  assigned. 
     *
     *  @param  match <code> true </code> to match activity units with any 
     *          weekly individual effort, <code> false </code> to match 
     *          activity units with no weekly individual effort 
     */

    @OSID @Override
    public void matchAnyWeeklyIndividualEffort(boolean match) {
        return;
    }


    /**
     *  Clears the weekly individual effort terms. 
     */

    @OSID @Override
    public void clearWeeklyIndividualEffortTerms() {
        return;
    }


    /**
     *  Sets the objective <code> Id </code> for this query. 
     *
     *  @param  objectiveId an objective <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> objectiveId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchLearningObjectiveId(org.osid.id.Id objectiveId, 
                                         boolean match) {
        return;
    }


    /**
     *  Clears the objective <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearLearningObjectiveIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ObjectiveQuery </code> is available. 
     *
     *  @return <code> true </code> if an objective query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLearningObjectiveQuery() {
        return (false);
    }


    /**
     *  Gets the query for a learning objective. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return an objective query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLearningObjectiveQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQuery getLearningObjectiveQuery() {
        throw new org.osid.UnimplementedException("supportsLearningObjectiveQuery() is false");
    }


    /**
     *  Matches activity units that have any learning objective. 
     *
     *  @param  match <code> true </code> to match activity units with any 
     *          learning objective, <code> false </code> to match activity 
     *          units with no learning objectives 
     */

    @OSID @Override
    public void matchAnyLearningObjective(boolean match) {
        return;
    }


    /**
     *  Clears the learning objective terms. 
     */

    @OSID @Override
    public void clearLearningObjectiveTerms() {
        return;
    }


    /**
     *  Sets the activity <code> Id </code> for this query to match activity 
     *  units that have a related activity. 
     *
     *  @param  activityId an activity <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> activityId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchActivityId(org.osid.id.Id activityId, boolean match) {
        return;
    }


    /**
     *  Clears the activity <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearActivityIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> ActivityQuery </code> is available. 
     *
     *  @return <code> true </code> if an activity query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityQuery() {
        return (false);
    }


    /**
     *  Gets the query for an activity. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the activity query 
     *  @throws org.osid.UnimplementedException <code> supportsActivityQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityQuery getActivityQuery() {
        throw new org.osid.UnimplementedException("supportsActivityQuery() is false");
    }


    /**
     *  Matches activity units that have any activity. 
     *
     *  @param  match <code> true </code> to match activity units with any 
     *          activity. <code> false </code> to match activity units with no 
     *          activity 
     */

    @OSID @Override
    public void matchAnyActivity(boolean match) {
        return;
    }


    /**
     *  Clears the activity terms. 
     */

    @OSID @Override
    public void clearActivityTerms() {
        return;
    }


    /**
     *  Sets the course catalog <code> Id </code> for this query to match 
     *  courses assigned to course catalogs. 
     *
     *  @param  courseCatalogId the course catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseCatalogId(org.osid.id.Id courseCatalogId, 
                                     boolean match) {
        return;
    }


    /**
     *  Clears the course catalog <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseCatalogIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CourseCatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a course catalog query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course catalog. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the course catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQuery getCourseCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsCourseCatalogQuery() is false");
    }


    /**
     *  Clears the course catalog terms. 
     */

    @OSID @Override
    public void clearCourseCatalogTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given activity unit query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an activity unit implementing the requested record.
     *
     *  @param activityUnitRecordType an activity unit record type
     *  @return the activity unit query record
     *  @throws org.osid.NullArgumentException
     *          <code>activityUnitRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(activityUnitRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.records.ActivityUnitQueryRecord getActivityUnitQueryRecord(org.osid.type.Type activityUnitRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.records.ActivityUnitQueryRecord record : this.records) {
            if (record.implementsRecordType(activityUnitRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(activityUnitRecordType + " is not supported");
    }


    /**
     *  Adds a record to this activity unit query. 
     *
     *  @param activityUnitQueryRecord activity unit query record
     *  @param activityUnitRecordType activityUnit record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addActivityUnitQueryRecord(org.osid.course.records.ActivityUnitQueryRecord activityUnitQueryRecord, 
                                          org.osid.type.Type activityUnitRecordType) {

        addRecordType(activityUnitRecordType);
        nullarg(activityUnitQueryRecord, "activity unit query record");
        this.records.add(activityUnitQueryRecord);        
        return;
    }
}
