//
// AbstractMapRelevancyLookupSession
//
//    A simple framework for providing a Relevancy lookup service
//    backed by a fixed collection of relevancies.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.ontology.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Relevancy lookup service backed by a
 *  fixed collection of relevancies. The relevancies are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Relevancies</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapRelevancyLookupSession
    extends net.okapia.osid.jamocha.ontology.spi.AbstractRelevancyLookupSession
    implements org.osid.ontology.RelevancyLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.ontology.Relevancy> relevancies = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.ontology.Relevancy>());


    /**
     *  Makes a <code>Relevancy</code> available in this session.
     *
     *  @param  relevancy a relevancy
     *  @throws org.osid.NullArgumentException <code>relevancy<code>
     *          is <code>null</code>
     */

    protected void putRelevancy(org.osid.ontology.Relevancy relevancy) {
        this.relevancies.put(relevancy.getId(), relevancy);
        return;
    }


    /**
     *  Makes an array of relevancies available in this session.
     *
     *  @param  relevancies an array of relevancies
     *  @throws org.osid.NullArgumentException <code>relevancies<code>
     *          is <code>null</code>
     */

    protected void putRelevancies(org.osid.ontology.Relevancy[] relevancies) {
        putRelevancies(java.util.Arrays.asList(relevancies));
        return;
    }


    /**
     *  Makes a collection of relevancies available in this session.
     *
     *  @param  relevancies a collection of relevancies
     *  @throws org.osid.NullArgumentException <code>relevancies<code>
     *          is <code>null</code>
     */

    protected void putRelevancies(java.util.Collection<? extends org.osid.ontology.Relevancy> relevancies) {
        for (org.osid.ontology.Relevancy relevancy : relevancies) {
            this.relevancies.put(relevancy.getId(), relevancy);
        }

        return;
    }


    /**
     *  Removes a Relevancy from this session.
     *
     *  @param  relevancyId the <code>Id</code> of the relevancy
     *  @throws org.osid.NullArgumentException <code>relevancyId<code> is
     *          <code>null</code>
     */

    protected void removeRelevancy(org.osid.id.Id relevancyId) {
        this.relevancies.remove(relevancyId);
        return;
    }


    /**
     *  Gets the <code>Relevancy</code> specified by its <code>Id</code>.
     *
     *  @param  relevancyId <code>Id</code> of the <code>Relevancy</code>
     *  @return the relevancy
     *  @throws org.osid.NotFoundException <code>relevancyId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>relevancyId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.Relevancy getRelevancy(org.osid.id.Id relevancyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.ontology.Relevancy relevancy = this.relevancies.get(relevancyId);
        if (relevancy == null) {
            throw new org.osid.NotFoundException("relevancy not found: " + relevancyId);
        }

        return (relevancy);
    }


    /**
     *  Gets all <code>Relevancies</code>. In plenary mode, the returned
     *  list contains all known relevancies or an error
     *  results. Otherwise, the returned list may contain only those
     *  relevancies that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Relevancies</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevancies()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.ontology.relevancy.ArrayRelevancyList(this.relevancies.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.relevancies.clear();
        super.close();
        return;
    }
}
