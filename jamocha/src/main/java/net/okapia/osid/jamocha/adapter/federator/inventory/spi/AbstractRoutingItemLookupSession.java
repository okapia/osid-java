//
// AbstractRoutingItemLookupSession
//
//     A routing federating adapter for an ItemLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.inventory.spi;

import org.osid.binding.java.annotation.OSID;

import net.okapia.osid.torrefacto.collect.IdHashMap;
import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A federating adapter for an ItemLookupSession. Sessions are
 *  added to this session through <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 *
 *  The routing for Ids and Types are manually set by subclasses of
 *  this session that add child sessions for federation.
 *
 *  Routing for Ids is based on the existince of an Id embedded in the
 *  authority of an item Id. The embedded Id is specified when
 *  adding child sessions. Optionally, a list of types for genus and
 *  record may also be mapped to a child session. A service Id for
 *  routing must be unique across all federated sessions while a Type
 *  may map to more than one session.
 *
 *  Lookup methods consult the mapping tables for Ids and Types, and
 *  will fallback to searching all sessions if the item is not
 *  found and fallback is true. In the case of record and genus Type
 *  lookups, a fallback will search all child sessions regardless if
 *  any are found.
 */

public abstract class AbstractRoutingItemLookupSession
    extends AbstractFederatingItemLookupSession
    implements org.osid.inventory.ItemLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.inventory.ItemLookupSession> sessionsById = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.inventory.ItemLookupSession>());
    private final MultiMap<org.osid.type.Type, org.osid.inventory.ItemLookupSession> sessionsByType = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.inventory.ItemLookupSession>());
    private final boolean idFallback;
    private final boolean typeFallback;

    
    /**
     *  Constructs a new
     *  <code>AbstractRoutingItemLookupSession</code> with
     *  fallbacks for both Types and Ids.
     */

    protected AbstractRoutingItemLookupSession() {
        this.idFallback   = true;
        this.typeFallback = true;
        selectAll();
        return;
    }


    /**
     *  Constructs a new
     *  <code>AbstractRoutingItemLookupSession</code>.
     *
     *  @param idFallback <code>true</code> to fall back to searching
     *         all sessions if an Id is not in routing table
     *  @param typeFallback <code>true</code> to fall back to
     *         searching all sessions if a Type is not in routing
     *         table
     */

    protected AbstractRoutingItemLookupSession(boolean idFallback, boolean typeFallback) {
        this.idFallback   = idFallback;
        this.typeFallback = typeFallback;
        selectAll();
        return;
    }


    /**
     *  Maps a session to a service Id. Use <code>addSession()</code>
     *  to add a session to this federation.
     *
     *  @param session a session to map
     *  @param serviceId 
     *  @throws org.osid.NullArgumentException <code>session</code> or
     *          <code>serviceId</code> is <code>null</code>
     */

    protected void mapSession(org.osid.inventory.ItemLookupSession session, org.osid.id.Id serviceId) {
        nullarg(serviceId, "service Id");
        this.sessionsById.put(serviceId, session);
        return;
    }


    /**
     *  Maps a session to a record or genus Type. Use
     *  <code>addSession()</code> to add a session to this federation.
     *
     *  @param session a session to add
     *  @param type
     *  @throws org.osid.NullArgumentException <code>session</code> or
     *          <code>type</code> is <code>null</code>
     */

    protected void mapSession(org.osid.inventory.ItemLookupSession session, org.osid.type.Type type) {
        nullarg(type, "type");
        this.sessionsByType.put(type, session);
        return;
    }


    /**
     *  Unmaps a session from a serviceId.
     *
     *  @param serviceId 
     *  @throws org.osid.NullArgumentException 
     *          <code>serviceId</code> is <code>null</code>
     */

    protected void unmapSession(org.osid.id.Id serviceId) {
        nullarg(serviceId, "service Id");
        this.sessionsById.remove(serviceId);
        return;
    }


    /**
     *  Unmaps a session from both the Id and Type indices.
     *
     *  @param session
     *  @throws org.osid.NullArgumentException 
     *          <code>session</code> is <code>null</code>
     */

    protected void unmapSession(org.osid.inventory.ItemLookupSession session) {
        nullarg(session, "session");

        for (org.osid.id.Id id : this.sessionsById.keySet()) {
            if (session.equals(this.sessionsById.get(id))) {
                this.sessionsById.remove(id);
            }
        }

        this.sessionsByType.removeValue(session);

        return;
    }


    /**
     *  Removes a session from this federation.
     *
     *  @param session
     *  @throws org.osid.NullArgumentException 
     *          <code>session</code> is <code>null</code>
     */

    protected void removeSession(org.osid.inventory.ItemLookupSession session) {
        unmapSession(session);
        super.removeSession(session);
        return;
    }


    /**
     *  Gets the <code>Item</code> specified by its <code>Id</code>. 
     *
     *  @param  itemId <code>Id</code> of the
     *          <code>Item</code>
     *  @return the item
     *  @throws org.osid.NotFoundException <code>itemId</code> not 
     *          found in any session
     *  @throws org.osid.NullArgumentException <code>itemId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.Item getItem(org.osid.id.Id itemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.inventory.ItemLookupSession session = this.sessionsById.get(itemId.getAuthority());
        if (session != null) {
            return (session.getItem(itemId));
        }
        
        if (this.idFallback) {
            return (super.getItem(itemId));
        }

        throw new org.osid.NotFoundException(itemId + " not found");
    }


    /**
     *  Gets a <code>ItemList</code> corresponding to the given
     *  item genus <code>Type</code> which does not include
     *  items of types derived from the specified
     *  <code>Type</code>.
     *  
     *
     *  @param  itemGenusType an item genus type 
     *  @return the returned <code>Item</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>itemGenusType</code> is
     *          <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.ItemList getItemsByGenusType(org.osid.type.Type itemGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inventory.item.FederatingItemList ret = getItemList();
        java.util.Collection<org.osid.inventory.ItemLookupSession> sessions = this.sessionsByType.get(itemGenusType);

        if (sessions.size() == 0) {
            if (this.typeFallback) {
                return (super.getItemsByGenusType(itemGenusType));
            } else {
                return (new net.okapia.osid.jamocha.nil.inventory.item.EmptyItemList());
            }
        }


        for (org.osid.inventory.ItemLookupSession session : sessions) {
            ret.addItemList(session.getItemsByGenusType(itemGenusType));
        }
        
        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ItemList</code> corresponding to the given
     *  item genus <code>Type</code> and include any additional
     *  items with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  @param  itemGenusType an item genus type 
     *  @return the returned <code>Item</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>itemGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.ItemList getItemsByParentGenusType(org.osid.type.Type itemGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inventory.item.FederatingItemList ret = getItemList();
        java.util.Collection<org.osid.inventory.ItemLookupSession> sessions = this.sessionsByType.get(itemGenusType);

        if (sessions.size() == 0) {
            if (this.typeFallback) {
                return (super.getItemsByParentGenusType(itemGenusType));
            } else {
                return (new net.okapia.osid.jamocha.nil.inventory.item.EmptyItemList());
            }
        }


        for (org.osid.inventory.ItemLookupSession session : sessions) {
            ret.addItemList(session.getItemsByParentGenusType(itemGenusType));
        }
        
        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ItemList</code> containing the given
     *  item record <code>Type</code>.
     *
     *  @param  itemRecordType an item record type 
     *  @return the returned <code>Item</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>itemRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.ItemList getItemsByRecordType(org.osid.type.Type itemRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inventory.item.FederatingItemList ret = getItemList();
        java.util.Collection<org.osid.inventory.ItemLookupSession> sessions = this.sessionsByType.get(itemRecordType);

        if (sessions.size() == 0) {
            if (this.typeFallback) {
                return (super.getItemsByRecordType(itemRecordType));
            } else {
                return (new net.okapia.osid.jamocha.nil.inventory.item.EmptyItemList());
            }
        }

        for (org.osid.inventory.ItemLookupSession session : sessions) {
            ret.addItemList(session.getItemsByRecordType(itemRecordType));
        }
        
        ret.noMore();
        return (ret);
    }
}
