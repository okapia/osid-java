//
// AbstractMapBankLookupSession
//
//    A simple framework for providing a Bank lookup service
//    backed by a fixed collection of banks.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.assessment.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Bank lookup service backed by a
 *  fixed collection of banks. The banks are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Banks</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapBankLookupSession
    extends net.okapia.osid.jamocha.assessment.spi.AbstractBankLookupSession
    implements org.osid.assessment.BankLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.assessment.Bank> banks = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.assessment.Bank>());


    /**
     *  Makes a <code>Bank</code> available in this session.
     *
     *  @param  bank a bank
     *  @throws org.osid.NullArgumentException <code>bank<code>
     *          is <code>null</code>
     */

    protected void putBank(org.osid.assessment.Bank bank) {
        this.banks.put(bank.getId(), bank);
        return;
    }


    /**
     *  Makes an array of banks available in this session.
     *
     *  @param  banks an array of banks
     *  @throws org.osid.NullArgumentException <code>banks<code>
     *          is <code>null</code>
     */

    protected void putBanks(org.osid.assessment.Bank[] banks) {
        putBanks(java.util.Arrays.asList(banks));
        return;
    }


    /**
     *  Makes a collection of banks available in this session.
     *
     *  @param  banks a collection of banks
     *  @throws org.osid.NullArgumentException <code>banks<code>
     *          is <code>null</code>
     */

    protected void putBanks(java.util.Collection<? extends org.osid.assessment.Bank> banks) {
        for (org.osid.assessment.Bank bank : banks) {
            this.banks.put(bank.getId(), bank);
        }

        return;
    }


    /**
     *  Removes a Bank from this session.
     *
     *  @param  bankId the <code>Id</code> of the bank
     *  @throws org.osid.NullArgumentException <code>bankId<code> is
     *          <code>null</code>
     */

    protected void removeBank(org.osid.id.Id bankId) {
        this.banks.remove(bankId);
        return;
    }


    /**
     *  Gets the <code>Bank</code> specified by its <code>Id</code>.
     *
     *  @param  bankId <code>Id</code> of the <code>Bank</code>
     *  @return the bank
     *  @throws org.osid.NotFoundException <code>bankId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>bankId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.Bank getBank(org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.assessment.Bank bank = this.banks.get(bankId);
        if (bank == null) {
            throw new org.osid.NotFoundException("bank not found: " + bankId);
        }

        return (bank);
    }


    /**
     *  Gets all <code>Banks</code>. In plenary mode, the returned
     *  list contains all known banks or an error
     *  results. Otherwise, the returned list may contain only those
     *  banks that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Banks</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.BankList getBanks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.assessment.bank.ArrayBankList(this.banks.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.banks.clear();
        super.close();
        return;
    }
}
