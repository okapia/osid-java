//
// AbstractParticipantQueryInspector.java
//
//     A template for making a ParticipantQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.participant.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for participants.
 */

public abstract class AbstractParticipantQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQueryInspector
    implements org.osid.offering.ParticipantQueryInspector {

    private final java.util.Collection<org.osid.offering.records.ParticipantQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the offering <code> Id </code> query terms. 
     *
     *  @return the offering <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOfferingIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the offering query terms. 
     *
     *  @return the offering terms 
     */

    @OSID @Override
    public org.osid.offering.OfferingQueryInspector[] getOfferingTerms() {
        return (new org.osid.offering.OfferingQueryInspector[0]);
    }


    /**
     *  Gets the resource <code> Id </code> query terms. 
     *
     *  @return the resource <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResourceIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the resource query terms. 
     *
     *  @return the resource terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getResourceTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the time period <code> Id </code> query terms. 
     *
     *  @return the time period <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getTimePeriodIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the time period query terms. 
     *
     *  @return the time period query terms 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodQueryInspector[] getTimePeriodTerms() {
        return (new org.osid.calendaring.TimePeriodQueryInspector[0]);
    }


    /**
     *  Gets the grade system <code> Id </code> query terms. 
     *
     *  @return the grade system <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResultOptionIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the grade system query terms. 
     *
     *  @return the grade system terms 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQueryInspector[] getResultOptionTerms() {
        return (new org.osid.grading.GradeSystemQueryInspector[0]);
    }


    /**
     *  Gets the catalogue <code> Id </code> query terms. 
     *
     *  @return the catalogue <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCatalogueIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the catalogue query terms. 
     *
     *  @return the catalogue terms 
     */

    @OSID @Override
    public org.osid.offering.CatalogueQueryInspector[] getCatalogueTerms() {
        return (new org.osid.offering.CatalogueQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given participant query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a participant implementing the requested record.
     *
     *  @param participantRecordType a participant record type
     *  @return the participant query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>participantRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(participantRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.records.ParticipantQueryInspectorRecord getParticipantQueryInspectorRecord(org.osid.type.Type participantRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.offering.records.ParticipantQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(participantRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(participantRecordType + " is not supported");
    }


    /**
     *  Adds a record to this participant query. 
     *
     *  @param participantQueryInspectorRecord participant query inspector
     *         record
     *  @param participantRecordType participant record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addParticipantQueryInspectorRecord(org.osid.offering.records.ParticipantQueryInspectorRecord participantQueryInspectorRecord, 
                                                   org.osid.type.Type participantRecordType) {

        addRecordType(participantRecordType);
        nullarg(participantRecordType, "participant record type");
        this.records.add(participantQueryInspectorRecord);        
        return;
    }
}
