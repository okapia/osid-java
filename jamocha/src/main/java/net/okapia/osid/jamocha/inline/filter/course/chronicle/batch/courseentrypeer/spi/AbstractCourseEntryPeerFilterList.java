//
// AbstractCourseEntryPeerList
//
//     Implements a filter for a CourseEntryPeerList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.course.chronicle.batch.courseentrypeer.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filter for a CourseEntryPeerList. Subclasses call this
 *  constructor and implement the <code>pass()</code> method. This
 *  filter is synchronous but can be wrapped in a BufferedCourseEntryPeerList
 *  to improve performance.
 */

public abstract class AbstractCourseEntryPeerFilterList
    extends net.okapia.osid.jamocha.course.chronicle.batch.courseentrypeer.spi.AbstractCourseEntryPeerList
    implements org.osid.course.chronicle.batch.CourseEntryPeerList,
               net.okapia.osid.jamocha.inline.filter.course.chronicle.batch.courseentrypeer.CourseEntryPeerFilter {

    private org.osid.course.chronicle.batch.CourseEntryPeer courseEntryPeer;
    private final org.osid.course.chronicle.batch.CourseEntryPeerList list;
    private org.osid.OsidException error;


    /**
     *  Creates a new <code>AbstractCourseEntryPeerFilterList</code>.
     *
     *  @param courseEntryPeerList a <code>CourseEntryPeerList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>courseEntryPeerList</code> is <code>null</code>
     */

    protected AbstractCourseEntryPeerFilterList(org.osid.course.chronicle.batch.CourseEntryPeerList courseEntryPeerList) {
        nullarg(courseEntryPeerList, "course entry peer list");
        this.list = courseEntryPeerList;
        return;
    }    

        
    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in
     *          this list, <code> false </code> if the end of the list
     *          has been reached
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        if (hasError()) {
            return (true);
        }

        prime();

        if (this.courseEntryPeer == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the next <code> CourseEntryPeer </code> in this list. 
     *
     *  @return the next <code> CourseEntryPeer </code> in this list. The <code> 
     *          hasNext() </code> method should be used to test that a next 
     *          <code> CourseEntryPeer </code> is available before calling this method. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.chronicle.batch.CourseEntryPeer getNextCourseEntryPeer()
        throws org.osid.OperationFailedException {

        if (hasError()) {
            throw new org.osid.OperationFailedException(this.error);
        }

        if (hasNext()) {
            org.osid.course.chronicle.batch.CourseEntryPeer courseEntryPeer = this.courseEntryPeer;
            this.courseEntryPeer = null;
            return (courseEntryPeer);
        } else {
            throw new org.osid.IllegalStateException("no more elements available in course entry peer list");
        }
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.courseEntryPeer = null;
        this.list.close();
        return;
    }

    
    /**
     *  Filters CourseEntryPeers.
     *
     *  @param courseEntryPeer the course entry peer to filter
     *  @return <code>true</code> if the course entry peer passes the filter,
     *          <code>false</code> if the course entry peer should be filtered
     */

    public abstract boolean pass(org.osid.course.chronicle.batch.CourseEntryPeer courseEntryPeer);


    protected void prime() {
        if (this.courseEntryPeer != null) {
            return;
        }

        org.osid.course.chronicle.batch.CourseEntryPeer courseEntryPeer = null;

        while (this.list.hasNext()) {
            try {
                courseEntryPeer = this.list.getNextCourseEntryPeer();
            } catch (org.osid.OsidException oe) {
                error(oe);
                return;
            }

            if (pass(courseEntryPeer)) {
                this.courseEntryPeer = courseEntryPeer;
                return;
            }
        }

        return;
    }


    /**
     *  Set an error to be thrown on the next retrieval.
     *
     *  @param error
     *  @throws org.osid.IllegalStateException this list already closed
     */

    protected void error(org.osid.OsidException error) {
        this.error = error;
        return;
    }


    /**
     *  Tests if an error exists.
     *
     *  @return <code>true</code> if an error has occurred,
     *          <code>false</code> otherwise
     */

    protected boolean hasError() {
        if (this.error == null) {
            return (false);
        } else {
            return (true);
        }
    }
}
