//
// InvariantMapProxyCatalogueLookupSession
//
//    Implements a Catalogue lookup service backed by a fixed
//    collection of catalogues. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.offering;


/**
 *  Implements a Catalogue lookup service backed by a fixed
 *  collection of catalogues. The catalogues are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyCatalogueLookupSession
    extends net.okapia.osid.jamocha.core.offering.spi.AbstractMapCatalogueLookupSession
    implements org.osid.offering.CatalogueLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyCatalogueLookupSession} with no
     *  catalogues.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public InvariantMapProxyCatalogueLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyCatalogueLookupSession} with a
     *  single catalogue.
     *
     *  @param catalogue a single catalogue
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code catalogue} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyCatalogueLookupSession(org.osid.offering.Catalogue catalogue, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putCatalogue(catalogue);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyCatalogueLookupSession} using
     *  an array of catalogues.
     *
     *  @param catalogues an array of catalogues
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code catalogues} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyCatalogueLookupSession(org.osid.offering.Catalogue[] catalogues, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putCatalogues(catalogues);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyCatalogueLookupSession} using a
     *  collection of catalogues.
     *
     *  @param catalogues a collection of catalogues
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code catalogues} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyCatalogueLookupSession(java.util.Collection<? extends org.osid.offering.Catalogue> catalogues,
                                                  org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putCatalogues(catalogues);
        return;
    }
}
