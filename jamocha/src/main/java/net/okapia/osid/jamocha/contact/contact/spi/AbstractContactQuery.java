//
// AbstractContactQuery.java
//
//     A template for making a Contact Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.contact.contact.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for contacts.
 */

public abstract class AbstractContactQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQuery
    implements org.osid.contact.ContactQuery {

    private final java.util.Collection<org.osid.contact.records.ContactQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets a reference <code> Id. </code> 
     *
     *  @param  referenceId a reference <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> referenceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchReferenceId(org.osid.id.Id referenceId, boolean match) {
        return;
    }


    /**
     *  Clears the reference <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearReferenceIdTerms() {
        return;
    }


    /**
     *  Sets a resource <code> Id. </code> 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAddresseeId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAddresseeIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAddresseeQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAddresseeQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getAddresseeQuery() {
        throw new org.osid.UnimplementedException("supportsAddresseeQuery() is false");
    }


    /**
     *  Clears the resource terms. 
     */

    @OSID @Override
    public void clearAddresseeTerms() {
        return;
    }


    /**
     *  Sets an address <code> Id. </code> 
     *
     *  @param  addressId an address <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> addressId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAddressId(org.osid.id.Id addressId, boolean match) {
        return;
    }


    /**
     *  Clears the address <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAddressIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AddressQuery </code> is available. 
     *
     *  @return <code> true </code> if an address query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAddressQuery() {
        return (false);
    }


    /**
     *  Gets the query for an address query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the address query 
     *  @throws org.osid.UnimplementedException <code> supportsAddressQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.AddressQuery getAddressQuery() {
        throw new org.osid.UnimplementedException("supportsAddressQuery() is false");
    }


    /**
     *  Clears the address terms. 
     */

    @OSID @Override
    public void clearAddressTerms() {
        return;
    }


    /**
     *  Sets the address <code> Id </code> for this query to match contacts 
     *  assigned to address books. 
     *
     *  @param  addressBookId an address book <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> addressBookId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAddressBookId(org.osid.id.Id addressBookId, boolean match) {
        return;
    }


    /**
     *  Clears the address book <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAddressBookIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AddressBookQuery </code> is available. 
     *
     *  @return <code> true </code> if an address book query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAddressBookQuery() {
        return (false);
    }


    /**
     *  Gets the query for an address book query. Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the address book query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAddressBookQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.AddressBookQuery getAddressBookQuery() {
        throw new org.osid.UnimplementedException("supportsAddressBookQuery() is false");
    }


    /**
     *  Clears the address book terms. 
     */

    @OSID @Override
    public void clearAddressBookTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given contact query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a contact implementing the requested record.
     *
     *  @param contactRecordType a contact record type
     *  @return the contact query record
     *  @throws org.osid.NullArgumentException
     *          <code>contactRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(contactRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.contact.records.ContactQueryRecord getContactQueryRecord(org.osid.type.Type contactRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.contact.records.ContactQueryRecord record : this.records) {
            if (record.implementsRecordType(contactRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(contactRecordType + " is not supported");
    }


    /**
     *  Adds a record to this contact query. 
     *
     *  @param contactQueryRecord contact query record
     *  @param contactRecordType contact record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addContactQueryRecord(org.osid.contact.records.ContactQueryRecord contactQueryRecord, 
                                          org.osid.type.Type contactRecordType) {

        addRecordType(contactRecordType);
        nullarg(contactQueryRecord, "contact query record");
        this.records.add(contactQueryRecord);        
        return;
    }
}
