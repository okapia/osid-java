//
// AbstractAdapterCompetencyLookupSession.java
//
//    A Competency lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.resourcing.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Competency lookup session adapter.
 */

public abstract class AbstractAdapterCompetencyLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.resourcing.CompetencyLookupSession {

    private final org.osid.resourcing.CompetencyLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterCompetencyLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterCompetencyLookupSession(org.osid.resourcing.CompetencyLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Foundry/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Foundry Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFoundryId() {
        return (this.session.getFoundryId());
    }


    /**
     *  Gets the {@code Foundry} associated with this session.
     *
     *  @return the {@code Foundry} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Foundry getFoundry()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getFoundry());
    }


    /**
     *  Tests if this user can perform {@code Competency} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupCompetencies() {
        return (this.session.canLookupCompetencies());
    }


    /**
     *  A complete view of the {@code Competency} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCompetencyView() {
        this.session.useComparativeCompetencyView();
        return;
    }


    /**
     *  A complete view of the {@code Competency} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCompetencyView() {
        this.session.usePlenaryCompetencyView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include competencies in foundries which are children
     *  of this foundry in the foundry hierarchy.
     */

    @OSID @Override
    public void useFederatedFoundryView() {
        this.session.useFederatedFoundryView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this foundry only.
     */

    @OSID @Override
    public void useIsolatedFoundryView() {
        this.session.useIsolatedFoundryView();
        return;
    }
    
     
    /**
     *  Gets the {@code Competency} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Competency} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Competency} and
     *  retained for compatibility.
     *
     *  @param competencyId {@code Id} of the {@code Competency}
     *  @return the competency
     *  @throws org.osid.NotFoundException {@code competencyId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code competencyId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Competency getCompetency(org.osid.id.Id competencyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCompetency(competencyId));
    }


    /**
     *  Gets a {@code CompetencyList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  competencies specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Competencies} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  competencyIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Competency} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code competencyIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyList getCompetenciesByIds(org.osid.id.IdList competencyIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCompetenciesByIds(competencyIds));
    }


    /**
     *  Gets a {@code CompetencyList} corresponding to the given
     *  competency genus {@code Type} which does not include
     *  competencies of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  competencies or an error results. Otherwise, the returned list
     *  may contain only those competencies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  competencyGenusType a competency genus type 
     *  @return the returned {@code Competency} list
     *  @throws org.osid.NullArgumentException
     *          {@code competencyGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyList getCompetenciesByGenusType(org.osid.type.Type competencyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCompetenciesByGenusType(competencyGenusType));
    }


    /**
     *  Gets a {@code CompetencyList} corresponding to the given
     *  competency genus {@code Type} and include any additional
     *  competencies with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  competencies or an error results. Otherwise, the returned list
     *  may contain only those competencies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  competencyGenusType a competency genus type 
     *  @return the returned {@code Competency} list
     *  @throws org.osid.NullArgumentException
     *          {@code competencyGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyList getCompetenciesByParentGenusType(org.osid.type.Type competencyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCompetenciesByParentGenusType(competencyGenusType));
    }


    /**
     *  Gets a {@code CompetencyList} containing the given
     *  competency record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  competencies or an error results. Otherwise, the returned list
     *  may contain only those competencies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  competencyRecordType a competency record type 
     *  @return the returned {@code Competency} list
     *  @throws org.osid.NullArgumentException
     *          {@code competencyRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyList getCompetenciesByRecordType(org.osid.type.Type competencyRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCompetenciesByRecordType(competencyRecordType));
    }


    /**
     *  Gets all {@code Competencies}. 
     *
     *  In plenary mode, the returned list contains all known
     *  competencies or an error results. Otherwise, the returned list
     *  may contain only those competencies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Competencies} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyList getCompetencies()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCompetencies());
    }
}
