//
// AbstractCatalogingManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.cataloging.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractCatalogingManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.cataloging.CatalogingManager,
               org.osid.cataloging.CatalogingProxyManager {

    private final Types catalogRecordTypes                 = new TypeRefSet();
    private final Types catalogSearchRecordTypes           = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractCatalogingManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractCatalogingManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests for the availability of a cataloging service retrieving <code> 
     *  Id </code> to <code> Catalog </code> mappings. 
     *
     *  @return <code> true </code> if cataloging is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalog() {
        return (false);
    }


    /**
     *  Tests for the availability of a cataloging service for mapping <code> 
     *  Ids </code> to <code> Catalogs. </code> 
     *
     *  @return <code> true </code> if catalog assignment is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogAssignment() {
        return (false);
    }


    /**
     *  Tests for the availability of a cataloging notification service for 
     *  mapping <code> Ids </code> to <code> Catalogs. </code> 
     *
     *  @return <code> true </code> if catalog entry notification is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogEntryNotification() {
        return (false);
    }


    /**
     *  Tests for the availability of a catalog lookup service. 
     *
     *  @return <code> true </code> if catalog lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogLookup() {
        return (false);
    }


    /**
     *  Tests for the availability of a catalog query service that defines 
     *  more comprehensive queries. 
     *
     *  @return <code> true </code> if catalog query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogQuery() {
        return (false);
    }


    /**
     *  Tests for the availability of a catalog search service that defines 
     *  more comprehensive queries. 
     *
     *  @return <code> true </code> if catalog search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogSearch() {
        return (false);
    }


    /**
     *  Tests for the availability of a catalog administration service for the 
     *  addition and deletion of catalogs. 
     *
     *  @return <code> true </code> if catalog administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogAdmin() {
        return (false);
    }


    /**
     *  Tests for the availability of a catalog notification service. 
     *
     *  @return <code> true </code> if catalog notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogNotification() {
        return (false);
    }


    /**
     *  Tests for the availability of a catalog hierarchy traversal service. 
     *
     *  @return <code> true </code> if catalog hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogHierarchy() {
        return (false);
    }


    /**
     *  Tests for the availability of a catalog hierarchy design service. 
     *
     *  @return <code> true </code> if catalog hierarchy design is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests if the cataloging rules sub services is supported. 
     *
     *  @return <code> true </code> if cataloging rules is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogingRules() {
        return (false);
    }


    /**
     *  Gets the supported <code> Catalog </code> record types. 
     *
     *  @return a list containing the supported <code> Catalog </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCatalogRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.catalogRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Catalog </code> record type is supported. 
     *
     *  @param  catalogRecordType a <code> Type </code> indicating a <code> 
     *          Catalog </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> catalogRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCatalogRecordType(org.osid.type.Type catalogRecordType) {
        return (this.catalogRecordTypes.contains(catalogRecordType));
    }


    /**
     *  Adds support for a catalog record type.
     *
     *  @param catalogRecordType a catalog record type
     *  @throws org.osid.NullArgumentException
     *  <code>catalogRecordType</code> is <code>null</code>
     */

    protected void addCatalogRecordType(org.osid.type.Type catalogRecordType) {
        this.catalogRecordTypes.add(catalogRecordType);
        return;
    }


    /**
     *  Removes support for a catalog record type.
     *
     *  @param catalogRecordType a catalog record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>catalogRecordType</code> is <code>null</code>
     */

    protected void removeCatalogRecordType(org.osid.type.Type catalogRecordType) {
        this.catalogRecordTypes.remove(catalogRecordType);
        return;
    }


    /**
     *  Gets the supported catalog search reciord types. 
     *
     *  @return a list containing the supported search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCatalogSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.catalogSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given catalog search record type is supported. 
     *
     *  @param  catalogSearchRecordType a <code> Type </code> indicating a 
     *          catalog search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> catalogSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCatalogSearchRecordType(org.osid.type.Type catalogSearchRecordType) {
        return (this.catalogSearchRecordTypes.contains(catalogSearchRecordType));
    }


    /**
     *  Adds support for a catalog search record type.
     *
     *  @param catalogSearchRecordType a catalog search record type
     *  @throws org.osid.NullArgumentException
     *  <code>catalogSearchRecordType</code> is <code>null</code>
     */

    protected void addCatalogSearchRecordType(org.osid.type.Type catalogSearchRecordType) {
        this.catalogSearchRecordTypes.add(catalogSearchRecordType);
        return;
    }


    /**
     *  Removes support for a catalog search record type.
     *
     *  @param catalogSearchRecordType a catalog search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>catalogSearchRecordType</code> is <code>null</code>
     */

    protected void removeCatalogSearchRecordType(org.osid.type.Type catalogSearchRecordType) {
        this.catalogSearchRecordTypes.remove(catalogSearchRecordType);
        return;
    }


    /**
     *  Gets the cataloging session for retrieving mappings to catalogs. 
     *
     *  @return a <code> CatalogSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCatalog() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogSession getCatalogSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.CatalogingManager.getCatalogSession not implemented");
    }


    /**
     *  Gets the catalog session for retrieving <code> Id </code> to <code> 
     *  Catalog </code> mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CatalogSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCatalog() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogSession getCatalogSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.CatalogingProxyManager.getCatalogSession not implemented");
    }


    /**
     *  Gets the cataloging session for adding and removing mappings to 
     *  catalogs. 
     *
     *  @return a <code> CatalogAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogAssignmentSession getCatalogAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.CatalogingManager.getCatalogAssignmentSession not implemented");
    }


    /**
     *  Gets the catalog session for mapping <code> Ids </code> to <code> 
     *  Catalogs. </code> 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CatalogAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogAssignmentSession getCatalogAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.CatalogingProxyManager.getCatalogAssignmentSession not implemented");
    }


    /**
     *  Gets the notification session for subscribing to changes to catalogs. 
     *
     *  @param  catalogEntryReceiver the notification callback 
     *  @return a <code> CatalogEntryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> catalogEntryReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEntryNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogEntryNotificationSession getCatalogEntryNotificationSession(org.osid.cataloging.CatalogEntryReceiver catalogEntryReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.CatalogingManager.getCatalogEntryNotificationSession not implemented");
    }


    /**
     *  Gets the catalog session for mapping <code> Ids </code> to <code> 
     *  Catalogs. </code> 
     *
     *  @param  catalogEntryReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> CatalogEntryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> catalogEntryReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEntrytNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogEntryNotificationSession getCatalogEntryNotificationSession(org.osid.cataloging.CatalogEntryReceiver catalogEntryReceiver, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.CatalogingProxyManager.getCatalogEntryNotificationSession not implemented");
    }


    /**
     *  Gets the notification session for subscribing to changes to catalogs 
     *  for the given catalog. 
     *
     *  @param  catalogEntryReceiver the notification callback 
     *  @param  catalogId the <code> Id </code> of the <code> Catalog </code> 
     *  @return a <code> CatalogEntryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> catalogEntryReceiver 
     *          </code> or <code> catalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEntryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogEntryNotificationSession getCatalogEntryNotificationSessionForCatalog(org.osid.cataloging.CatalogEntryReceiver catalogEntryReceiver, 
                                                                                                            org.osid.id.Id catalogId)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.CatalogingManager.getCatalogEntryNotificationSessionForCatalog not implemented");
    }
        

    /**
     *  Gets the notification session for subscribing to changes to catalogs 
     *  for the given catalog. 
     *
     *  @param  catalogEntryReceiver the notification callback 
     *  @param  catalogId the <code> Id </code> of the <code> Catalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CatalogEntryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> catalogEntryReceiver, 
     *          catalogId, </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEntryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogEntryNotificationSession getCatalogEntryNotificationSessionForCatalog(org.osid.cataloging.CatalogEntryReceiver catalogEntryReceiver, 
                                                                                                            org.osid.id.Id catalogId, 
                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {
        
        throw new org.osid.UnimplementedException("org.osid.cataloging.CatalogingProxyManager.getCatalogEntryNotificationSessionForCatalog not implemented");
    }

    
    /**
     *  Gets the catalog lookup session. 
     *
     *  @return a <code> CatalogLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCatalogLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogLookupSession getCatalogLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.CatalogingManager.getCatalogLookupSession not implemented");
    }


    /**
     *  Gets the catalog lookup session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CatalogLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCatalogLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogLookupSession getCatalogLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.CatalogingProxyManager.getCatalogLookupSession not implemented");
    }


    /**
     *  Gets the catalog query session. 
     *
     *  @return a <code> CatalogQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCatalogQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogQuerySession getCatalogQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.CatalogingManager.getCatalogQuerySession not implemented");
    }


    /**
     *  Gets the catalog query session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CatalogQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCatalogQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogQuerySession getCatalogQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.CatalogingProxyManager.getCatalogQuerySession not implemented");
    }


    /**
     *  Gets the catalog search session. 
     *
     *  @return a <code> CatalogSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCatalogSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogSearchSession getCatalogSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.CatalogingManager.getCatalogSearchSession not implemented");
    }


    /**
     *  Gets the catalog search session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CatalogSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCatalogSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogSearchSession getCatalogSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.CatalogingProxyManager.getCatalogSearchSession not implemented");
    }


    /**
     *  Gets the catalog administrative session for creating, updating and 
     *  deleting catalogs. 
     *
     *  @return a <code> CatalogAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCatalogAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogAdminSession getCatalogAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.CatalogingManager.getCatalogAdminSession not implemented");
    }


    /**
     *  Gets the catalog administrative session for creating, updating and 
     *  deleting catalogs. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CatalogAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCatalogAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogAdminSession getCatalogAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.CatalogingProxyManager.getCatalogAdminSession not implemented");
    }


    /**
     *  Gets the notification session for subscribing to changes to catalogs. 
     *
     *  @param  catalogReceiver the notification callback 
     *  @return a <code> CatalogNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> catalogReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogNotificationSession getCatalogNotificationSession(org.osid.cataloging.CatalogReceiver catalogReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.CatalogingManager.getCatalogNotificationSession not implemented");
    }


    /**
     *  Gets the notification session for subscribing to changes to catalogs. 
     *
     *  @param  catalogReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> CatalogNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> catalogReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogNotificationSession getCatalogNotificationSession(org.osid.cataloging.CatalogReceiver catalogReceiver, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.CatalogingProxyManager.getCatalogNotificationSession not implemented");
    }


    /**
     *  Gets the catalog hierarchy traversal session. 
     *
     *  @return a <code> CatalogHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogHierarchySession getCatalogHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.CatalogingManager.getCatalogHierarchySession not implemented");
    }


    /**
     *  Gets the catalog hierarchy traversal session. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CatalogHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogHierarchySession getCatalogHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.CatalogingProxyManager.getCatalogHierarchySession not implemented");
    }


    /**
     *  Gets the catalog hierarchy design session. 
     *
     *  @return a <code> CatalogHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogHierarchyDesignSession getCatalogHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.CatalogingManager.getCatalogHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the catalog hierarchy design session. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CatalogHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogHierarchyDesignSession getCatalogHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.CatalogingProxyManager.getCatalogHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the cataloging rules manager. 
     *
     *  @return a <code> CatalogingRulesManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogingRules() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogingRulesManager getCatalogingRulesManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.CatalogingManager.getCatalogingRulesManager not implemented");
    }


    /**
     *  Gets the cataloging rules proxy manager. 
     *
     *  @return a <code> CatalogingRulesManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogingRules() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogingRulesProxyManager getCatalogingRulesProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.CatalogingProxyManager.getCatalogingRulesProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.catalogRecordTypes.clear();
        this.catalogRecordTypes.clear();

        this.catalogSearchRecordTypes.clear();
        this.catalogSearchRecordTypes.clear();

        return;
    }
}
