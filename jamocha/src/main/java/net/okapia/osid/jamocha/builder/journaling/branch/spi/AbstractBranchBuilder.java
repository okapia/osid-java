//
// AbstractBranch.java
//
//     Defines a Branch builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.journaling.branch.spi;


/**
 *  Defines a <code>Branch</code> builder.
 */

public abstract class AbstractBranchBuilder<T extends AbstractBranchBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOperableOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.journaling.branch.BranchMiter branch;


    /**
     *  Constructs a new <code>AbstractBranchBuilder</code>.
     *
     *  @param branch the branch to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractBranchBuilder(net.okapia.osid.jamocha.builder.journaling.branch.BranchMiter branch) {
        super(branch);
        this.branch = branch;
        return;
    }


    /**
     *  Builds the branch.
     *
     *  @return the new branch
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.journaling.Branch build() {
        (new net.okapia.osid.jamocha.builder.validator.journaling.branch.BranchValidator(getValidations())).validate(this.branch);
        return (new net.okapia.osid.jamocha.builder.journaling.branch.ImmutableBranch(this.branch));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the branch miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.journaling.branch.BranchMiter getMiter() {
        return (this.branch);
    }


    /**
     *  Sets the origin journal entry.
     *
     *  @param entry an origin journal entry
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>entry</code> is
     *          <code>null</code>
     */

    public T originJournalEntry(org.osid.journaling.JournalEntry entry) {
        getMiter().setOriginJournalEntry(entry);
        return (self());
    }


    /**
     *  Sets the latest journal entry.
     *
     *  @param entry a latest journal entry
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>entry</code> is
     *          <code>null</code>
     */

    public T latestJournalEntry(org.osid.journaling.JournalEntry entry) {
        getMiter().setLatestJournalEntry(entry);
        return (self());
    }


    /**
     *  Adds a Branch record.
     *
     *  @param record a branch record
     *  @param recordType the type of branch record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.journaling.records.BranchRecord record, org.osid.type.Type recordType) {
        getMiter().addBranchRecord(record, recordType);
        return (self());
    }
}       


