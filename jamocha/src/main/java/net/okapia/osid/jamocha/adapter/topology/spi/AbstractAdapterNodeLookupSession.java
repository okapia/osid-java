//
// AbstractAdapterNodeLookupSession.java
//
//    A Node lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.topology.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Node lookup session adapter.
 */

public abstract class AbstractAdapterNodeLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.topology.NodeLookupSession {

    private final org.osid.topology.NodeLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterNodeLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterNodeLookupSession(org.osid.topology.NodeLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Graph/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Graph Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getGraphId() {
        return (this.session.getGraphId());
    }


    /**
     *  Gets the {@code Graph} associated with this session.
     *
     *  @return the {@code Graph} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.Graph getGraph()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getGraph());
    }


    /**
     *  Tests if this user can perform {@code Node} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupNodes() {
        return (this.session.canLookupNodes());
    }


    /**
     *  A complete view of the {@code Node} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeNodeView() {
        this.session.useComparativeNodeView();
        return;
    }


    /**
     *  A complete view of the {@code Node} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryNodeView() {
        this.session.usePlenaryNodeView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include nodes in graphs which are children
     *  of this graph in the graph hierarchy.
     */

    @OSID @Override
    public void useFederatedGraphView() {
        this.session.useFederatedGraphView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this graph only.
     */

    @OSID @Override
    public void useIsolatedGraphView() {
        this.session.useIsolatedGraphView();
        return;
    }
    
     
    /**
     *  Gets the {@code Node} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Node} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Node} and
     *  retained for compatibility.
     *
     *  @param nodeId {@code Id} of the {@code Node}
     *  @return the node
     *  @throws org.osid.NotFoundException {@code nodeId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code nodeId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.Node getNode(org.osid.id.Id nodeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getNode(nodeId));
    }


    /**
     *  Gets a {@code NodeList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  nodes specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Nodes} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  nodeIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Node} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code nodeIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.NodeList getNodesByIds(org.osid.id.IdList nodeIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getNodesByIds(nodeIds));
    }


    /**
     *  Gets a {@code NodeList} corresponding to the given
     *  node genus {@code Type} which does not include
     *  nodes of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  nodes or an error results. Otherwise, the returned list
     *  may contain only those nodes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  nodeGenusType a node genus type 
     *  @return the returned {@code Node} list
     *  @throws org.osid.NullArgumentException
     *          {@code nodeGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.NodeList getNodesByGenusType(org.osid.type.Type nodeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getNodesByGenusType(nodeGenusType));
    }


    /**
     *  Gets a {@code NodeList} corresponding to the given
     *  node genus {@code Type} and include any additional
     *  nodes with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  nodes or an error results. Otherwise, the returned list
     *  may contain only those nodes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  nodeGenusType a node genus type 
     *  @return the returned {@code Node} list
     *  @throws org.osid.NullArgumentException
     *          {@code nodeGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.NodeList getNodesByParentGenusType(org.osid.type.Type nodeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getNodesByParentGenusType(nodeGenusType));
    }


    /**
     *  Gets a {@code NodeList} containing the given
     *  node record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  nodes or an error results. Otherwise, the returned list
     *  may contain only those nodes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  nodeRecordType a node record type 
     *  @return the returned {@code Node} list
     *  @throws org.osid.NullArgumentException
     *          {@code nodeRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.NodeList getNodesByRecordType(org.osid.type.Type nodeRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getNodesByRecordType(nodeRecordType));
    }


    /**
     *  Gets all {@code Nodes}. 
     *
     *  In plenary mode, the returned list contains all known
     *  nodes or an error results. Otherwise, the returned list
     *  may contain only those nodes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Nodes} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.NodeList getNodes()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getNodes());
    }
}
