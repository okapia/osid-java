//
// FiscalPeriodElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.fiscalperiod.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class FiscalPeriodElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the FiscalPeriodElement Id.
     *
     *  @return the fiscal period element Id
     */

    public static org.osid.id.Id getFiscalPeriodEntityId() {
        return (makeEntityId("osid.financials.FiscalPeriod"));
    }


    /**
     *  Gets the DisplayLabel element Id.
     *
     *  @return the DisplayLabel element Id
     */

    public static org.osid.id.Id getDisplayLabel() {
        return (makeElementId("osid.financials.fiscalperiod.DisplayLabel"));
    }


    /**
     *  Gets the FiscalYear element Id.
     *
     *  @return the FiscalYear element Id
     */

    public static org.osid.id.Id getFiscalYear() {
        return (makeElementId("osid.financials.fiscalperiod.FiscalYear"));
    }


    /**
     *  Gets the StartDate element Id.
     *
     *  @return the StartDate element Id
     */

    public static org.osid.id.Id getStartDate() {
        return (makeElementId("osid.financials.fiscalperiod.StartDate"));
    }


    /**
     *  Gets the EndDate element Id.
     *
     *  @return the EndDate element Id
     */

    public static org.osid.id.Id getEndDate() {
        return (makeElementId("osid.financials.fiscalperiod.EndDate"));
    }


    /**
     *  Gets the BudgetDeadline element Id.
     *
     *  @return the BudgetDeadline element Id
     */

    public static org.osid.id.Id getBudgetDeadline() {
        return (makeElementId("osid.financials.fiscalperiod.BudgetDeadline"));
    }


    /**
     *  Gets the PostingDeadline element Id.
     *
     *  @return the PostingDeadline element Id
     */

    public static org.osid.id.Id getPostingDeadline() {
        return (makeElementId("osid.financials.fiscalperiod.PostingDeadline"));
    }


    /**
     *  Gets the Closing element Id.
     *
     *  @return the Closing element Id
     */

    public static org.osid.id.Id getClosing() {
        return (makeElementId("osid.financials.fiscalperiod.Closing"));
    }


    /**
     *  Gets the Duration element Id.
     *
     *  @return the Duration element Id
     */

    public static org.osid.id.Id getDuration() {
        return (makeQueryElementId("osid.financials.fiscalperiod.Duration"));
    }


    /**
     *  Gets the BusinessId element Id.
     *
     *  @return the BusinessId element Id
     */

    public static org.osid.id.Id getBusinessId() {
        return (makeQueryElementId("osid.financials.fiscalperiod.BusinessId"));
    }


    /**
     *  Gets the Business element Id.
     *
     *  @return the Business element Id
     */

    public static org.osid.id.Id getBusiness() {
        return (makeQueryElementId("osid.financials.fiscalperiod.Business"));
    }
}
