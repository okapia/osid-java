//
// AbstractRoute.java
//
//     Defines a Route.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.route.route.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Route</code>.
 */

public abstract class AbstractRoute
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.mapping.route.Route {

    private org.osid.mapping.Location startingLocation;
    private org.osid.mapping.Location endingLocation;
    private org.osid.mapping.Distance distance;
    private org.osid.calendaring.Duration eta;
    private final java.util.Collection<org.osid.mapping.route.RouteSegment> segments = new java.util.LinkedHashSet<>();

    private final java.util.Collection<org.osid.mapping.route.records.RouteRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the starting location of this route. 
     *
     *  @return the starting location <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getStartingLocationId() {
        return (this.startingLocation.getId());
    }


    /**
     *  Gets the starting location of this route. 
     *
     *  @return the starting location 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.mapping.Location getStartingLocation()
        throws org.osid.OperationFailedException {

        return (this.startingLocation);
    }


    /**
     *  Sets the starting location.
     *
     *  @param location a starting location
     *  @throws org.osid.NullArgumentException
     *          <code>location</code> is <code>null</code>
     */

    protected void setStartingLocation(org.osid.mapping.Location location) {
        nullarg(location, "starting location");
        this.startingLocation = location;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the ending location of this route. 
     *
     *  @return the ending location <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getEndingLocationId() {
        return (this.endingLocation.getId());
    }


    /**
     *  Gets the ending location of this route. 
     *
     *  @return the ending location 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.mapping.Location getEndingLocation()
        throws org.osid.OperationFailedException {

        return (this.endingLocation);
    }


    /**
     *  Sets the ending location.
     *
     *  @param location an ending location
     *  @throws org.osid.NullArgumentException
     *          <code>location</code> is <code>null</code>
     */

    protected void setEndingLocation(org.osid.mapping.Location location) {
        nullarg(location, "ending location");
        this.endingLocation = location;
        return;
    }


    /**
     *  Gets the total distance of this route. 
     *
     *  @return the total route distance 
     */

    @OSID @Override
    public org.osid.mapping.Distance getDistance() {
        return (this.distance);
    }


    /**
     *  Sets the distance.
     *
     *  @param distance a distance
     *  @throws org.osid.NullArgumentException
     *          <code>distance</code> is <code>null</code>
     */

    protected void setDistance(org.osid.mapping.Distance distance) {
        nullarg(distance, "distance");
        this.distance = distance;
        return;
    }


    /**
     *  Gets the estimated travel time across the entire route. 
     *
     *  @return the estimated travel time 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getETA() {
        return (this.eta);
    }


    /**
     *  Sets the ETA.
     *
     *  @param eta the estimated time of arrival
     *  @throws org.osid.NullArgumentException <code>eta</code> is
     *          <code>null</code>
     */

    protected void setETA(org.osid.calendaring.Duration eta) {
        nullarg(eta, "estimated time of arrival");
        this.eta = eta;;
        return;
    }


    /**
     *  Gets the route segment <code> Ids </code> of this route. 
     *
     *  @return the segment <code> Ids </code> of this route 
     */

    @OSID @Override
    public org.osid.id.IdList getSegmentIds() {
        try {
            org.osid.mapping.route.RouteSegmentList segments = getSegments();
            return (new net.okapia.osid.jamocha.adapter.converter.mapping.route.routesegment.RouteSegmentToIdList(segments));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the segments of this route. 
     *
     *  @return the segments of this route 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteSegmentList getSegments()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.mapping.route.routesegment.ArrayRouteSegmentList(this.segments));
    }


    /**
     *  Adds a segment.
     *
     *  @param segment a segment
     *  @throws org.osid.NullArgumentException
     *          <code>segment</code> is <code>null</code>
     */

    protected void addSegment(org.osid.mapping.route.RouteSegment segment) {
        nullarg(segment, "segment");
        this.segments.add(segment);
        return;
    }


    /**
     *  Sets all the segments.
     *
     *  @param segments a collection of segments
     *  @throws org.osid.NullArgumentException
     *          <code>segments</code> is <code>null</code>
     */

    protected void setSegments(java.util.Collection<org.osid.mapping.route.RouteSegment> segments) {
        nullarg(segments, "segments");
        this.segments.clear();
        this.segments.addAll(segments);
        return;
    }


    /**
     *  Tests if this route supports the given record
     *  <code>Type</code>.
     *
     *  @param  routeRecordType a route record type 
     *  @return <code>true</code> if the routeRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>routeRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type routeRecordType) {
        for (org.osid.mapping.route.records.RouteRecord record : this.records) {
            if (record.implementsRecordType(routeRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given <code>Route</code>
     *  record <code>Type</code>.
     *
     *  @param  routeRecordType the route record type 
     *  @return the route record 
     *  @throws org.osid.NullArgumentException
     *          <code>routeRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(routeRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.route.records.RouteRecord getRouteRecord(org.osid.type.Type routeRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.route.records.RouteRecord record : this.records) {
            if (record.implementsRecordType(routeRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(routeRecordType + " is not supported");
    }


    /**
     *  Adds a record to this route. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param routeRecord the route record
     *  @param routeRecordType route record type
     *  @throws org.osid.NullArgumentException
     *          <code>routeRecord</code> or
     *          <code>routeRecordTyperoute</code> is
     *          <code>null</code>
     */
            
    protected void addRouteRecord(org.osid.mapping.route.records.RouteRecord routeRecord, 
                                  org.osid.type.Type routeRecordType) {

        nullarg(routeRecord, "route record");
        addRecordType(routeRecordType);
        this.records.add(routeRecord);
        
        return;
    }
}
