//
// MutableBudgetEntryList.java
//
//     Implements a BudgetEntryList. This list allows BudgetEntries to be
//     added after this list has been created.
//
//
// Tom Coppeto
// OnTapSolutions
// 29 June 2008
//
//
// Copyright (c) 2008, 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.budgeting.budgetentry;


/**
 *  <p>Implements a BudgetEntryList. This list allows BudgetEntries to be
 *  added after this budgetEntry has been created. One this list has been
 *  returned to the consumer, all subsequent additions occur in a
 *  separate processing thread.  The creator of this budgetEntry must
 *  invoke <code>eol()</code> when there are no more budgetEntries to be
 *  added.</p>
 *
 *  <p> If the consumer of the <code>BudgetEntryList</code> interface
 *  reaches the end of the internal buffer before <code>eol()</code>,
 *  then methods will block until more budgetEntries are added or
 *  <code>eol()</code> is invoked.</p>
 *
 *  <p><code>available()</code> never blocks but may return
 *  <code>0</code> if waiting for more budgetEntries to be added.</p>
 */

public final class MutableBudgetEntryList
    extends net.okapia.osid.jamocha.financials.budgeting.budgetentry.spi.AbstractMutableBudgetEntryList
    implements org.osid.financials.budgeting.BudgetEntryList {


    /**
     *  Creates a new empty <code>MutableBudgetEntryList</code>.
     */

    public MutableBudgetEntryList() {
        super();
    }


    /**
     *  Creates a new <code>MutableBudgetEntryList</code>.
     *
     *  @param budgetEntry a <code>BudgetEntry</code>
     *  @throws org.osid.NullArgumentException <code>budgetEntry</code>
     *          is <code>null</code>
     */

    public MutableBudgetEntryList(org.osid.financials.budgeting.BudgetEntry budgetEntry) {
        super(budgetEntry);
        return;
    }


    /**
     *  Creates a new <code>MutableBudgetEntryList</code>.
     *
     *  @param array an array of budgetentries
     *  @throws org.osid.NullArgumentException <code>array</code>
     *          is <code>null</code>
     */

    public MutableBudgetEntryList(org.osid.financials.budgeting.BudgetEntry[] array) {
        super(array);
        return;
    }


    /**
     *  Creates a new <code>MutableBudgetEntryList</code>.
     *
     *  @param collection a java.util.Collection of budgetentries
     *  @throws org.osid.NullArgumentException <code>collection</code>
     *          is <code>null</code>
     */

    public MutableBudgetEntryList(java.util.Collection<org.osid.financials.budgeting.BudgetEntry> collection) {
        super(collection);
        return;
    }
}
