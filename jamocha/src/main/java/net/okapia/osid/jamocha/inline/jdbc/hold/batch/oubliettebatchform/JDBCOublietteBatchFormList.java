//
// JDBCOublietteBatchFormList
//
//     Implements an OublietteBatchFormList. This list creates oubliettebatchforms from
//     the return of a JDBC query.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.jdbc.hold.batch.oubliettebatchform;

import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  <p>Implements an OublietteBatchFormList. This list creates oubliettebatchforms from the
 *  return of a JDBC query. The query is processed in a separate
 *  thread.</p>
 *
 *  <p><code>available()</code> never blocks but may return
 *  <code>0</code> if waiting for more oubliettebatchforms to be added.</p>
 */

public final class JDBCOublietteBatchFormList
    extends net.okapia.osid.jamocha.hold.batch.oubliettebatchform.spi.AbstractMutableOublietteBatchFormList
    implements org.osid.hold.batch.OublietteBatchFormList,
               Runnable {

    private boolean running = false;
    private OublietteBatchFormFetcher fetcher;


    /**
     *  Creates a new <code>JDBCOublietteBatchFormList</code>.
     *
     *  @param query an SQL query
     *  @param connection a JDBC connection
     *  @param generator an oublietteBatchForm to parse a result row and
     *         generate an OublietteBatchForm
     *  @param closeWhenDone <code>true</code> if the connection
     *         should be closed following the database transaction,
     *         <code>false</code> to leave it open
     *  @param bufferSize limits the number of oubliettebatchforms in this list
     *         buffer such that when the number of oubliettebatchforms exceeds
     *         <code>bufferSize</code>, reading from the JDBC result
     *         set will stop until oubliettebatchforms are retrieved. The value
     *         of this may depend on the amount of memory an OublietteBatchForm
     *         consumes.
     *  @throws org.osid.InvalidArgumentException
     *          <code>bufferSize</code> not greater than zero
     *  @throws org.osid.NullArgumentException <code>oublietteBatchForm</code>
     *          is <code>null</code>
     */

    public JDBCOublietteBatchFormList(String query, java.sql.Connection connection, JDBCOublietteBatchFormGenerator generator,
                        boolean closeWhenDone, int bufferSize)
        throws org.osid.OperationFailedException {

        nullarg(query, "query");
        nullarg(connection, "connection");
        nullarg(generator, "generator");

        if (bufferSize <= 0) {
            throw new org.osid.InvalidArgumentException("buffer size too small");
        }

        this.fetcher = new OublietteBatchFormFetcher(query, connection, generator, closeWhenDone, bufferSize);
        return;
    }


    /**
     *  Creates a new <code>JDBCOublietteBatchFormList</code> and runs it upon
     *  instantiation.
     *
     *  @param query an SQL query
     *  @param connection a JDBC connection
     *  @param generator an OublietteBatchForm to parse a result row and
     *         generate an OublietteBatchForm
     *  @param closeWhenDone <code>true</code> if the connection
     *         should be closed following the database transaction,
     *         <code>false</code> to leave it open
     *  @param bufferSize limits the number of oubliettebatchforms in this list
     *         buffer such that when the number of oubliettebatchforms exceeds
     *         <code>bufferSize</code>, reading from the JDBC result
     *         set will stop until oubliettebatchforms are retrieved. The value
     *         of this may depend on the amount of memory an OublietteBatchForm
     *         consumes.
     *  @param run <code>true</code> to start the fetching thread
     *         immediately
     *  @throws org.osid.InvalidArgumentException
     *          <code>bufferSize</code> not greater than zero
     *  @throws org.osid.NullArgumentException <code>oublietteBatchForm</code>
     *          is <code>null</code>
     */

    public JDBCOublietteBatchFormList(String query, java.sql.Connection connection, JDBCOublietteBatchFormGenerator generator,
                        boolean closeWhenDone, int bufferSize, boolean run)
        throws org.osid.OperationFailedException {

        this(query, connection, generator, closeWhenDone, bufferSize);
        if (run) {
            run();
        }
        return;
    }


    /**
     *  Starts the JDBC process.
     *
     *  @throws org.osid.IllegalStateException already started
     */

    public void run() {
        if (this.running || hasError()) {
            throw new org.osid.IllegalStateException("already started");
        }

        this.fetcher.start();
        return;
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already
     *          closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.running = false;
        return;
    }


    /**
     *  Tests if the list thread is running to populate elements from
     *  the underlying list.
     *
     *  @return <code>true</code> if the list is running,
     *          <code>false</code> otherwise.
     */

    public boolean isRunning() {
        return (this.running);
    }


    class OublietteBatchFormFetcher
        extends Thread {
        
        private String query;
        private java.sql.Connection connection;
        private JDBCOublietteBatchFormGenerator generator;
        private boolean closeWhenDone;
        private int bufferSize;

        
        OublietteBatchFormFetcher(String query, java.sql.Connection connection, JDBCOublietteBatchFormGenerator generator,
                        boolean closeWhenDone, int bufferSize) {

            this.query         = query;
            this.connection    = connection;
            this.generator     = generator;
            this.closeWhenDone = closeWhenDone;
            this.bufferSize    = bufferSize;

            return;
        }
            
        
        public void run() {
            java.sql.Statement statement = null;
            java.sql.ResultSet resultset = null;
            JDBCOublietteBatchFormList.this.running = true;

            try {
                statement = this.connection.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY,
                                                            java.sql.ResultSet.CONCUR_READ_ONLY);
                resultset = statement.executeQuery(this.query);
                long length = 0;

                while (resultset.next() && JDBCOublietteBatchFormList.this.running && !JDBCOublietteBatchFormList.this.hasError()) {
                    JDBCOublietteBatchFormList.this.addOublietteBatchForm(generator.makeOublietteBatchForm(resultset));
                    synchronized (JDBCOublietteBatchFormList.this) {
                        JDBCOublietteBatchFormList.this.notifyAll();
                    }

                    if (++length > this.bufferSize) {
                        length = JDBCOublietteBatchFormList.this.available();
                        if (length > this.bufferSize) {
                            synchronized (JDBCOublietteBatchFormList.this) {
                                try {
                                    JDBCOublietteBatchFormList.this.wait();
                                } catch (InterruptedException ie) {}
                            }
                        }
                    } 
                }
            } catch (Exception e) {
                JDBCOublietteBatchFormList.this.error(new org.osid.OperationFailedException("cannot generate oubliettebatchform", e));
                return;
            } finally {
                try {
                    if (this.closeWhenDone) {
                        this.connection.close();
                    }
                 
                    JDBCOublietteBatchFormList.this.running = false;

                    if (statement != null) {
                        statement.close();
                    }

                    if (resultset != null) {
                        resultset.close();
                    }
                } catch (Exception e) {}
            }

            JDBCOublietteBatchFormList.this.eol();
            return;
        }
    }
}
