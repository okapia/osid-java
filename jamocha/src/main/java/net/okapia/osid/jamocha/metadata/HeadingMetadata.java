//
// HeadingMetadata.java
//
//     Defines a heading Metadata.
//
//
// Tom Coppeto
// Okapia
// 11 January 2023
//
//
// Copyright (c) 2023 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.metadata;


/**
 *  Defines a heading Metadata.
 */

public final class HeadingMetadata
    extends net.okapia.osid.jamocha.metadata.spi.AbstractHeadingMetadata
    implements org.osid.Metadata {


    /**
     *  Constructs a new single unlinked {@code HeadingMetadata}.
     *
     *  @param elementId the Id of the element
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    public HeadingMetadata(org.osid.id.Id elementId) {
        super(elementId);
        return;
    }


    /**
     *  Constructs a new unlinked {@code HeadingMetadata}.
     *
     *  @param elementId the Id of the element
     *  @param isArray {@code true} if the element is an array another
     *         element, {@code false} if a single element
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    public HeadingMetadata(org.osid.id.Id elementId, boolean isArray) {
        super(elementId, isArray, false);
        return;
    }


    /**
     *  Constructs a new {@code HeadingMetadata}.
     *
     *  @param elementId the Id of the element
     *  @param isArray {@code true} if the element is an array another
     *         element, {@code false} if a single element
     *  @param isLinked {@code true} if the element is linked to
     *         another element, {@code false} otherwise
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    public HeadingMetadata(org.osid.id.Id elementId, boolean isArray, boolean isLinked) {
        super(elementId, isArray, isLinked);
        return;
    }

    
    /**
     *  Add support for a heading type.
     *
     *  @param headingType the type of heading
     *  @param dimensions the number of dimensions for the type
     *  @throws org.osid.InvalidArgumentException {@code dimensions}
     *          is negative
     *  @throws org.osid.NullArgumentException {@code headingType} is
     *          {@code null}
     */

    public void addHeadingType(org.osid.type.Type headingType, long dimensions) {
        super.addHeadingType(headingType, dimensions);
        return;
    }


    /**
     *  Sets the heading range.
     *
     *  @param min the minimum value
     *  @param max the maximum value
     *  @throws org.osid.InvalidArgumentException {@code min} is
     *          greater than {@code max} or, {@code min} or {@code
     *          max} is negative
     *  @throws org.osid.UnsupportedException {@code} types not supported
     */

    public void setHeadingRange(org.osid.mapping.Heading min, org.osid.mapping.Heading max) {
        super.setHeadingRange(min, max);
        return;
    }

    
    /**
     *  Sets the heading set.
     *
     *  @param values a collection of accepted heading values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    public void setHeadingSet(java.util.Collection<org.osid.mapping.Heading> values) {
        super.setHeadingSet(values);
        return;
    }


    /**
     *  Adds a collection of values to the heading set.
     *
     *  @param values a collection of accepted heading values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    public void addToHeadingSet(java.util.Collection<org.osid.mapping.Heading> values) {
        super.addToHeadingSet(values);
        return;
    }


    /**
     *  Adds a value to the heading set.
     *
     *  @param value a heading value
     *  @throws org.osid.InvalidArgumentException value is negative
     */

    public void addToHeadingSet(org.osid.mapping.Heading value) {
        super.addToHeadingSet(value);
        return;
    }


    /**
     *  Removes a value from the heading set.
     *
     *  @param value a heading value
     *  @throws org.osid.InvalidArgumentException value is negative
     */

    public void removeFromHeadingSet(org.osid.mapping.Heading value) {
        super.removeFromHeadingSet(value);
        return;
    }


    /**
     *  Clears the heading set.
     */

    public void clearHeadingSet() {
        super.clearHeadingSet();
        return;
    }


    /**
     *  Sets the default heading set.
     *
     *  @param values a collection of default heading values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    public void setDefaultHeadingValues(java.util.Collection<org.osid.mapping.Heading> values) {
        super.setDefaultHeadingValues(values);
        return;
    }


    /**
     *  Adds a collection of default heading values.
     *
     *  @param values a collection of default heading values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    public void addDefaultHeadingValues(java.util.Collection<org.osid.mapping.Heading> values) {
        super.addDefaultHeadingValues(values);
        return;
    }


    /**
     *  Adds a default heading value.
     *
     *  @param value a heading value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void addDefaultHeadingValue(org.osid.mapping.Heading value) {
        super.addDefaultHeadingValue(value);
        return;
    }


    /**
     *  Removes a default heading value.
     *
     *  @param value a heading value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void removeDefaultHeadingValue(org.osid.mapping.Heading value) {
        super.removeDefaultHeadingValue(value);
        return;
    }


    /**
     *  Clears the default heading values.
     */

    public void clearDefaultHeadingValues() {
        super.clearDefaultHeadingValues();
        return;
    }


    /**
     *  Sets the existing heading set.
     *
     *  @param values a collection of existing heading values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    public void setExistingHeadingValues(java.util.Collection<org.osid.mapping.Heading> values) {
        super.setExistingHeadingValues(values);
        return;
    }


    /**
     *  Adds a collection of existing heading values.
     *
     *  @param values a collection of existing heading values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    public void addExistingHeadingValues(java.util.Collection<org.osid.mapping.Heading> values) {
        super.addExistingHeadingValues(values);
        return;
    }


    /**
     *  Adds a existing heading value.
     *
     *  @param value a heading value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void addExistingHeadingValue(org.osid.mapping.Heading value) {
        super.addExistingHeadingValue(value);
        return;
    }


    /**
     *  Removes a existing heading value.
     *
     *  @param value a heading value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void removeExistingHeadingValue(org.osid.mapping.Heading value) {
        super.removeExistingHeadingValue(value);
        return;
    }


    /**
     *  Clears the existing heading values.
     */

    public void clearExistingHeadingValues() {
        super.clearExistingHeadingValues();
        return;
    }    
}
