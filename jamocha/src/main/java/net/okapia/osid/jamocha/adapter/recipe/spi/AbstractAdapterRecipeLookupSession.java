//
// AbstractAdapterRecipeLookupSession.java
//
//    A Recipe lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.recipe.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Recipe lookup session adapter.
 */

public abstract class AbstractAdapterRecipeLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.recipe.RecipeLookupSession {

    private final org.osid.recipe.RecipeLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterRecipeLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterRecipeLookupSession(org.osid.recipe.RecipeLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Cookbook/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Cookbook Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCookbookId() {
        return (this.session.getCookbookId());
    }


    /**
     *  Gets the {@code Cookbook} associated with this session.
     *
     *  @return the {@code Cookbook} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.Cookbook getCookbook()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCookbook());
    }


    /**
     *  Tests if this user can perform {@code Recipe} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupRecipes() {
        return (this.session.canLookupRecipes());
    }


    /**
     *  A complete view of the {@code Recipe} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeRecipeView() {
        this.session.useComparativeRecipeView();
        return;
    }


    /**
     *  A complete view of the {@code Recipe} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryRecipeView() {
        this.session.usePlenaryRecipeView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include recipes in cookbooks which are children
     *  of this cookbook in the cookbook hierarchy.
     */

    @OSID @Override
    public void useFederatedCookbookView() {
        this.session.useFederatedCookbookView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this cookbook only.
     */

    @OSID @Override
    public void useIsolatedCookbookView() {
        this.session.useIsolatedCookbookView();
        return;
    }
    
     
    /**
     *  Gets the {@code Recipe} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Recipe} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Recipe} and
     *  retained for compatibility.
     *
     *  @param recipeId {@code Id} of the {@code Recipe}
     *  @return the recipe
     *  @throws org.osid.NotFoundException {@code recipeId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code recipeId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.Recipe getRecipe(org.osid.id.Id recipeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRecipe(recipeId));
    }


    /**
     *  Gets a {@code RecipeList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  recipes specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Recipes} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  recipeIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Recipe} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code recipeIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.RecipeList getRecipesByIds(org.osid.id.IdList recipeIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRecipesByIds(recipeIds));
    }


    /**
     *  Gets a {@code RecipeList} corresponding to the given
     *  recipe genus {@code Type} which does not include
     *  recipes of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  recipes or an error results. Otherwise, the returned list
     *  may contain only those recipes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  recipeGenusType a recipe genus type 
     *  @return the returned {@code Recipe} list
     *  @throws org.osid.NullArgumentException
     *          {@code recipeGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.RecipeList getRecipesByGenusType(org.osid.type.Type recipeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRecipesByGenusType(recipeGenusType));
    }


    /**
     *  Gets a {@code RecipeList} corresponding to the given
     *  recipe genus {@code Type} and include any additional
     *  recipes with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  recipes or an error results. Otherwise, the returned list
     *  may contain only those recipes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  recipeGenusType a recipe genus type 
     *  @return the returned {@code Recipe} list
     *  @throws org.osid.NullArgumentException
     *          {@code recipeGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.RecipeList getRecipesByParentGenusType(org.osid.type.Type recipeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRecipesByParentGenusType(recipeGenusType));
    }


    /**
     *  Gets a {@code RecipeList} containing the given
     *  recipe record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  recipes or an error results. Otherwise, the returned list
     *  may contain only those recipes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  recipeRecordType a recipe record type 
     *  @return the returned {@code Recipe} list
     *  @throws org.osid.NullArgumentException
     *          {@code recipeRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.RecipeList getRecipesByRecordType(org.osid.type.Type recipeRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRecipesByRecordType(recipeRecordType));
    }


    /**
     *  Gets a {@code RecipeList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  recipes or an error results. Otherwise, the returned list
     *  may contain only those recipes that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Recipe} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.recipe.RecipeList getRecipesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRecipesByProvider(resourceId));
    }


    /**
     *  Gets all {@code Recipes}. 
     *
     *  In plenary mode, the returned list contains all known
     *  recipes or an error results. Otherwise, the returned list
     *  may contain only those recipes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Recipes} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.RecipeList getRecipes()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRecipes());
    }
}
