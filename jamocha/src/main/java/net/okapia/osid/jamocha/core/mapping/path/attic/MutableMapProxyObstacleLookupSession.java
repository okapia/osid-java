//
// MutableMapProxyObstacleLookupSession
//
//    Implements an Obstacle lookup service backed by a collection of
//    obstacles that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping.path;


/**
 *  Implements an Obstacle lookup service backed by a collection of
 *  obstacles. The obstacles are indexed only by <code>Id</code>. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of obstacles can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyObstacleLookupSession
    extends net.okapia.osid.jamocha.core.mapping.path.spi.AbstractMapObstacleLookupSession
    implements org.osid.mapping.path.ObstacleLookupSession {


    /**
     *  Constructs a new
     *  <code>MutableMapProxyObstacleLookupSession</code> with no
     *  obstacles.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException <code>proxy</code> is
     *          <code>null</code>
     */

    public MutableMapProxyObstacleLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new <code>MutableMapProxyObstacleLookupSession</code> with a
     *  single obstacle.
     *
     *  @param  obstacle an obstacle
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException <code>obstacle</code> or
     *          <code>proxy</code> is <code>null</code>
     */

    public MutableMapProxyObstacleLookupSession(org.osid.mapping.path.Obstacle obstacle, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putObstacle(obstacle);
        return;
    }


    /**
     *  Constructs a new
     *  <code>MutableMapProxyObstacleLookupSession</code> using an
     *  array of obstacles.
     *
     *  @param  obstacles an array of obstacles
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException <code>obstacles</code> or
     *          <code>proxy</code> is <code>null</code>
     */

    public MutableMapProxyObstacleLookupSession(org.osid.mapping.path.Obstacle[] obstacles, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putObstacles(obstacles);
        return;
    }


    /**
     *  Constructs a new <code>MutableMapProxyObstacleLookupSession</code> using
     *  a collection of obstacles.
     *
     *  @param  obstacles a collection of obstacles
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException <code>obstacles</code> or
     *          <code>proxy</code> is <code>null</code>
     */

    public MutableMapProxyObstacleLookupSession(java.util.Collection<? extends org.osid.mapping.path.Obstacle> obstacles,
                                                org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putObstacles(obstacles);
        return;
    }

    
    /**
     *  Makes a <code>Obstacle</code> available in this session.
     *
     *  @param  obstacle an obstacle
     *  @throws org.osid.NullArgumentException <code>obstacle<code>
     *          is <code>null</code>
     */

    @Override
    public void putObstacle(org.osid.mapping.path.Obstacle obstacle) {
        super.putObstacle(obstacle);
        return;
    }


    /**
     *  Makes an array of obstacles available in this session.
     *
     *  @param  obstacles an array of obstacles
     *  @throws org.osid.NullArgumentException <code>obstacles<code>
     *          is <code>null</code>
     */

    @Override
    public void putObstacles(org.osid.mapping.path.Obstacle[] obstacles) {
        super.putObstacles(obstacles);
        return;
    }


    /**
     *  Makes collection of obstacles available in this session.
     *
     *  @param  obstacles
     *  @throws org.osid.NullArgumentException <code>obstacle<code>
     *          is <code>null</code>
     */

    @Override
    public void putObstacles(java.util.Collection<? extends org.osid.mapping.path.Obstacle> obstacles) {
        super.putObstacles(obstacles);
        return;
    }


    /**
     *  Removes a Obstacle from this session.
     *
     *  @param obstacleId the <code>Id</code> of the obstacle
     *  @throws org.osid.NullArgumentException <code>obstacleId<code> is
     *          <code>null</code>
     */

    @Override
    public void removeObstacle(org.osid.id.Id obstacleId) {
        super.removeObstacle(obstacleId);
        return;
    }    
}
