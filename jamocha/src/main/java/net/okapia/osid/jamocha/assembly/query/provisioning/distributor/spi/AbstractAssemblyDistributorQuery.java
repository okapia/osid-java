//
// AbstractAssemblyDistributorQuery.java
//
//     A DistributorQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.provisioning.distributor.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A DistributorQuery that stores terms.
 */

public abstract class AbstractAssemblyDistributorQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCatalogQuery
    implements org.osid.provisioning.DistributorQuery,
               org.osid.provisioning.DistributorQueryInspector,
               org.osid.provisioning.DistributorSearchOrder {

    private final java.util.Collection<org.osid.provisioning.records.DistributorQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.provisioning.records.DistributorQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.provisioning.records.DistributorSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyDistributorQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyDistributorQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the broker <code> Id </code> for this query. 
     *
     *  @param  brokerId the broker <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> brokerId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBrokerId(org.osid.id.Id brokerId, boolean match) {
        getAssembler().addIdTerm(getBrokerIdColumn(), brokerId, match);
        return;
    }


    /**
     *  Clears the broker <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearBrokerIdTerms() {
        getAssembler().clearTerms(getBrokerIdColumn());
        return;
    }


    /**
     *  Gets the broker <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBrokerIdTerms() {
        return (getAssembler().getIdTerms(getBrokerIdColumn()));
    }


    /**
     *  Gets the BrokerId column name.
     *
     * @return the column name
     */

    protected String getBrokerIdColumn() {
        return ("broker_id");
    }


    /**
     *  Tests if an <code> BrokerQuery </code> is available. 
     *
     *  @return <code> true </code> if a broker query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a broker. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the broker query 
     *  @throws org.osid.UnimplementedException <code> supportsBrokerQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerQuery getBrokerQuery() {
        throw new org.osid.UnimplementedException("supportsBrokerQuery() is false");
    }


    /**
     *  Matches distributors with any broker. 
     *
     *  @param  match <code> true </code> to match distributors with any 
     *          broker, <code> false </code> to match distributors with no 
     *          broker 
     */

    @OSID @Override
    public void matchAnyBroker(boolean match) {
        getAssembler().addIdWildcardTerm(getBrokerColumn(), match);
        return;
    }


    /**
     *  Clears the broker query terms. 
     */

    @OSID @Override
    public void clearBrokerTerms() {
        getAssembler().clearTerms(getBrokerColumn());
        return;
    }


    /**
     *  Gets the broker query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerQueryInspector[] getBrokerTerms() {
        return (new org.osid.provisioning.BrokerQueryInspector[0]);
    }


    /**
     *  Gets the Broker column name.
     *
     * @return the column name
     */

    protected String getBrokerColumn() {
        return ("broker");
    }


    /**
     *  Sets the queue <code> Id </code> for this query. 
     *
     *  @param  queueId the queue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> queueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchQueueId(org.osid.id.Id queueId, boolean match) {
        getAssembler().addIdTerm(getQueueIdColumn(), queueId, match);
        return;
    }


    /**
     *  Clears the queue <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearQueueIdTerms() {
        getAssembler().clearTerms(getQueueIdColumn());
        return;
    }


    /**
     *  Gets the queue <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getQueueIdTerms() {
        return (getAssembler().getIdTerms(getQueueIdColumn()));
    }


    /**
     *  Gets the QueueId column name.
     *
     * @return the column name
     */

    protected String getQueueIdColumn() {
        return ("queue_id");
    }


    /**
     *  Tests if a <code> QueueQuery </code> is available. 
     *
     *  @return <code> true </code> if a queue query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a queue. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the queue query 
     *  @throws org.osid.UnimplementedException <code> supportsQueueQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.QueueQuery getQueueQuery() {
        throw new org.osid.UnimplementedException("supportsQueueQuery() is false");
    }


    /**
     *  Matches distributors with any queue. 
     *
     *  @param  match <code> true </code> to match distributors with any 
     *          queue, <code> false </code> to match distributors with no 
     *          queue 
     */

    @OSID @Override
    public void matchAnyQueue(boolean match) {
        getAssembler().addIdWildcardTerm(getQueueColumn(), match);
        return;
    }


    /**
     *  Clears the queue query terms. 
     */

    @OSID @Override
    public void clearQueueTerms() {
        getAssembler().clearTerms(getQueueColumn());
        return;
    }


    /**
     *  Gets the queue query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.QueueQueryInspector[] getQueueTerms() {
        return (new org.osid.provisioning.QueueQueryInspector[0]);
    }


    /**
     *  Gets the Queue column name.
     *
     * @return the column name
     */

    protected String getQueueColumn() {
        return ("queue");
    }


    /**
     *  Sets the pool <code> Id </code> for this query. 
     *
     *  @param  poolId the pool <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> poolId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchPoolId(org.osid.id.Id poolId, boolean match) {
        getAssembler().addIdTerm(getPoolIdColumn(), poolId, match);
        return;
    }


    /**
     *  Clears the pool <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearPoolIdTerms() {
        getAssembler().clearTerms(getPoolIdColumn());
        return;
    }


    /**
     *  Gets the pool <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPoolIdTerms() {
        return (getAssembler().getIdTerms(getPoolIdColumn()));
    }


    /**
     *  Gets the PoolId column name.
     *
     * @return the column name
     */

    protected String getPoolIdColumn() {
        return ("pool_id");
    }


    /**
     *  Tests if a <code> PoolQuery </code> is available. 
     *
     *  @return <code> true </code> if a pool query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolQuery() {
        return (false);
    }


    /**
     *  Gets the query for a pool. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the pool query 
     *  @throws org.osid.UnimplementedException <code> supportsPoolQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolQuery getPoolQuery() {
        throw new org.osid.UnimplementedException("supportsPoolQuery() is false");
    }


    /**
     *  Matches brokers with any pool. 
     *
     *  @param  match <code> true </code> to match distributors with any pool, 
     *          <code> false </code> to match distributors with no pool 
     */

    @OSID @Override
    public void matchAnyPool(boolean match) {
        getAssembler().addIdWildcardTerm(getPoolColumn(), match);
        return;
    }


    /**
     *  Clears the pool query terms. 
     */

    @OSID @Override
    public void clearPoolTerms() {
        getAssembler().clearTerms(getPoolColumn());
        return;
    }


    /**
     *  Gets the pool query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.PoolQueryInspector[] getPoolTerms() {
        return (new org.osid.provisioning.PoolQueryInspector[0]);
    }


    /**
     *  Gets the Pool column name.
     *
     * @return the column name
     */

    protected String getPoolColumn() {
        return ("pool");
    }


    /**
     *  Sets the provision <code> Id </code> for this query to match 
     *  distributors that have a related provisionable. 
     *
     *  @param  provisionId a provision <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> provisionId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchProvisionId(org.osid.id.Id provisionId, boolean match) {
        getAssembler().addIdTerm(getProvisionIdColumn(), provisionId, match);
        return;
    }


    /**
     *  Clears the provision <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearProvisionIdTerms() {
        getAssembler().clearTerms(getProvisionIdColumn());
        return;
    }


    /**
     *  Gets the provision <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProvisionIdTerms() {
        return (getAssembler().getIdTerms(getProvisionIdColumn()));
    }


    /**
     *  Gets the ProvisionId column name.
     *
     * @return the column name
     */

    protected String getProvisionIdColumn() {
        return ("provision_id");
    }


    /**
     *  Tests if a <code> ProvisionQuery </code> is available. 
     *
     *  @return <code> true </code> if a provision query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a provision. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the provision query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionQuery getProvisionQuery() {
        throw new org.osid.UnimplementedException("supportsProvisionQuery() is false");
    }


    /**
     *  Matches distributors that have any provision. 
     *
     *  @param  match <code> true </code> to match distributors with any 
     *          provision, <code> false </code> to match distributors with no 
     *          provision 
     */

    @OSID @Override
    public void matchAnyProvision(boolean match) {
        getAssembler().addIdWildcardTerm(getProvisionColumn(), match);
        return;
    }


    /**
     *  Clears the provision query terms. 
     */

    @OSID @Override
    public void clearProvisionTerms() {
        getAssembler().clearTerms(getProvisionColumn());
        return;
    }


    /**
     *  Gets the provision query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionQueryInspector[] getProvisionTerms() {
        return (new org.osid.provisioning.ProvisionQueryInspector[0]);
    }


    /**
     *  Gets the Provision column name.
     *
     * @return the column name
     */

    protected String getProvisionColumn() {
        return ("provision");
    }


    /**
     *  Sets the distributor <code> Id </code> for this query to match 
     *  distributors that have the specified distributor as an ancestor. 
     *
     *  @param  distributorId a distributor <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorDistributorId(org.osid.id.Id distributorId, 
                                           boolean match) {
        getAssembler().addIdTerm(getAncestorDistributorIdColumn(), distributorId, match);
        return;
    }


    /**
     *  Clears the ancestor distributor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorDistributorIdTerms() {
        getAssembler().clearTerms(getAncestorDistributorIdColumn());
        return;
    }


    /**
     *  Gets the ancestor distributor <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorDistributorIdTerms() {
        return (getAssembler().getIdTerms(getAncestorDistributorIdColumn()));
    }


    /**
     *  Gets the AncestorDistributorId column name.
     *
     * @return the column name
     */

    protected String getAncestorDistributorIdColumn() {
        return ("ancestor_distributor_id");
    }


    /**
     *  Tests if a <code> DistributorQuery </code> is available. 
     *
     *  @return <code> true </code> if a distributor query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorDistributorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a distributor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the distributor query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorDistributorQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQuery getAncestorDistributorQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorDistributorQuery() is false");
    }


    /**
     *  Matches distributors with any ancestor. 
     *
     *  @param  match <code> true </code> to match distributors with any 
     *          ancestor, <code> false </code> to match root distributors 
     */

    @OSID @Override
    public void matchAnyAncestorDistributor(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorDistributorColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor distributor query terms. 
     */

    @OSID @Override
    public void clearAncestorDistributorTerms() {
        getAssembler().clearTerms(getAncestorDistributorColumn());
        return;
    }


    /**
     *  Gets the ancestor distributor query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQueryInspector[] getAncestorDistributorTerms() {
        return (new org.osid.provisioning.DistributorQueryInspector[0]);
    }


    /**
     *  Gets the AncestorDistributor column name.
     *
     * @return the column name
     */

    protected String getAncestorDistributorColumn() {
        return ("ancestor_distributor");
    }


    /**
     *  Sets the distributor <code> Id </code> for this query to match 
     *  distributors that have the specified distributor as a descendant. 
     *
     *  @param  distributorId a distributor <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantDistributorId(org.osid.id.Id distributorId, 
                                             boolean match) {
        getAssembler().addIdTerm(getDescendantDistributorIdColumn(), distributorId, match);
        return;
    }


    /**
     *  Clears the descendant distributor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantDistributorIdTerms() {
        getAssembler().clearTerms(getDescendantDistributorIdColumn());
        return;
    }


    /**
     *  Gets the descendant distributor <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantDistributorIdTerms() {
        return (getAssembler().getIdTerms(getDescendantDistributorIdColumn()));
    }


    /**
     *  Gets the DescendantDistributorId column name.
     *
     * @return the column name
     */

    protected String getDescendantDistributorIdColumn() {
        return ("descendant_distributor_id");
    }


    /**
     *  Tests if a <code> DistributorQuery </code> is available. 
     *
     *  @return <code> true </code> if a distributor query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantDistributorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a distributor/ Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the distributor query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantDistributorQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQuery getDescendantDistributorQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantDistributorQuery() is false");
    }


    /**
     *  Matches distributors with any descendant. 
     *
     *  @param  match <code> true </code> to match distributors with any 
     *          descendant, <code> false </code> to match leaf distributors 
     */

    @OSID @Override
    public void matchAnyDescendantDistributor(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantDistributorColumn(), match);
        return;
    }


    /**
     *  Clears the descendant distributor query terms. 
     */

    @OSID @Override
    public void clearDescendantDistributorTerms() {
        getAssembler().clearTerms(getDescendantDistributorColumn());
        return;
    }


    /**
     *  Gets the descendant distributor query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQueryInspector[] getDescendantDistributorTerms() {
        return (new org.osid.provisioning.DistributorQueryInspector[0]);
    }


    /**
     *  Gets the DescendantDistributor column name.
     *
     * @return the column name
     */

    protected String getDescendantDistributorColumn() {
        return ("descendant_distributor");
    }


    /**
     *  Tests if this distributor supports the given record
     *  <code>Type</code>.
     *
     *  @param  distributorRecordType a distributor record type 
     *  @return <code>true</code> if the distributorRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>distributorRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type distributorRecordType) {
        for (org.osid.provisioning.records.DistributorQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(distributorRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  distributorRecordType the distributor record type 
     *  @return the distributor query record 
     *  @throws org.osid.NullArgumentException
     *          <code>distributorRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(distributorRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.DistributorQueryRecord getDistributorQueryRecord(org.osid.type.Type distributorRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.records.DistributorQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(distributorRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(distributorRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  distributorRecordType the distributor record type 
     *  @return the distributor query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>distributorRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(distributorRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.DistributorQueryInspectorRecord getDistributorQueryInspectorRecord(org.osid.type.Type distributorRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.records.DistributorQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(distributorRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(distributorRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param distributorRecordType the distributor record type
     *  @return the distributor search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>distributorRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(distributorRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.DistributorSearchOrderRecord getDistributorSearchOrderRecord(org.osid.type.Type distributorRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.records.DistributorSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(distributorRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(distributorRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this distributor. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param distributorQueryRecord the distributor query record
     *  @param distributorQueryInspectorRecord the distributor query inspector
     *         record
     *  @param distributorSearchOrderRecord the distributor search order record
     *  @param distributorRecordType distributor record type
     *  @throws org.osid.NullArgumentException
     *          <code>distributorQueryRecord</code>,
     *          <code>distributorQueryInspectorRecord</code>,
     *          <code>distributorSearchOrderRecord</code> or
     *          <code>distributorRecordTypedistributor</code> is
     *          <code>null</code>
     */
            
    protected void addDistributorRecords(org.osid.provisioning.records.DistributorQueryRecord distributorQueryRecord, 
                                      org.osid.provisioning.records.DistributorQueryInspectorRecord distributorQueryInspectorRecord, 
                                      org.osid.provisioning.records.DistributorSearchOrderRecord distributorSearchOrderRecord, 
                                      org.osid.type.Type distributorRecordType) {

        addRecordType(distributorRecordType);

        nullarg(distributorQueryRecord, "distributor query record");
        nullarg(distributorQueryInspectorRecord, "distributor query inspector record");
        nullarg(distributorSearchOrderRecord, "distributor search odrer record");

        this.queryRecords.add(distributorQueryRecord);
        this.queryInspectorRecords.add(distributorQueryInspectorRecord);
        this.searchOrderRecords.add(distributorSearchOrderRecord);
        
        return;
    }
}
