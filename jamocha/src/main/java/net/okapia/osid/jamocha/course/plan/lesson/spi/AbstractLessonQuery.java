//
// AbstractLessonQuery.java
//
//     A template for making a Lesson Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.plan.lesson.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for lessons.
 */

public abstract class AbstractLessonQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQuery
    implements org.osid.course.plan.LessonQuery {

    private final java.util.Collection<org.osid.course.plan.records.LessonQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the plan <code> Id </code> for this query. 
     *
     *  @param  planId a plan <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> planId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchPlanId(org.osid.id.Id planId, boolean match) {
        return;
    }


    /**
     *  Clears the plan <code> Id </code> tems. 
     */

    @OSID @Override
    public void clearPlanIdTerms() {
        return;
    }


    /**
     *  Tests if a plan query is available. 
     *
     *  @return <code> true </code> if a plan query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPlanQuery() {
        return (false);
    }


    /**
     *  Gets the query for a plan. 
     *
     *  @return the plan query 
     *  @throws org.osid.UnimplementedException <code> supportsPlanQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.PlanQuery getPlanQuery() {
        throw new org.osid.UnimplementedException("supportsPlanQuery() is false");
    }


    /**
     *  Clears the plan terms. 
     */

    @OSID @Override
    public void clearPlanTerms() {
        return;
    }


    /**
     *  Sets the docet <code> Id </code> for this query. 
     *
     *  @param  docetId a docet <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> docetId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDocetId(org.osid.id.Id docetId, boolean match) {
        return;
    }


    /**
     *  Clears the docet <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDocetIdTerms() {
        return;
    }


    /**
     *  Tests if a docet query is available. 
     *
     *  @return <code> true </code> if a docet query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDocetQuery() {
        return (false);
    }


    /**
     *  Gets the query for a docet. 
     *
     *  @return the docet query 
     *  @throws org.osid.UnimplementedException <code> supportsDocetQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetQuery getDocetQuery() {
        throw new org.osid.UnimplementedException("supportsDocetQuery() is false");
    }


    /**
     *  Matches a lesson that has any docet. 
     *
     *  @param  match <code> true </code> to match lessons with any docet, 
     *          <code> false </code> to match lessons with no docet 
     */

    @OSID @Override
    public void matchAnyDocet(boolean match) {
        return;
    }


    /**
     *  Clears the docet terms. 
     */

    @OSID @Override
    public void clearDocetTerms() {
        return;
    }


    /**
     *  Sets the activity <code> Id </code> for this query. 
     *
     *  @param  activitytId an activity <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> activityId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchActivityId(org.osid.id.Id activitytId, boolean match) {
        return;
    }


    /**
     *  Clears the activity <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearActivityIdTerms() {
        return;
    }


    /**
     *  Tests if an activity query is available. 
     *
     *  @return <code> true </code> if an activity query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityQuery() {
        return (false);
    }


    /**
     *  Gets the query for an activity. 
     *
     *  @return the activity query 
     *  @throws org.osid.UnimplementedException <code> supportsActivityQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityQuery getActivityQuery() {
        throw new org.osid.UnimplementedException("supportsActivityQuery() is false");
    }


    /**
     *  Matches a lesson that has any activity. 
     *
     *  @param  match <code> true </code> to match lessons with any activity, 
     *          <code> false </code> to match lessons with no activities 
     */

    @OSID @Override
    public void matchAnyActivity(boolean match) {
        return;
    }


    /**
     *  Clears the activity terms. 
     */

    @OSID @Override
    public void clearActivityTerms() {
        return;
    }


    /**
     *  Matches a planned start time within the given duration range 
     *  inclusive. 
     *
     *  @param  from a starting date 
     *  @param  to a ending date 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchPlannedStartTime(org.osid.calendaring.Duration from, 
                                      org.osid.calendaring.Duration to, 
                                      boolean match) {
        return;
    }


    /**
     *  Matches a lesson that has any planned start time. 
     *
     *  @param  match <code> true </code> to match lessons with any planned 
     *          start time, <code> false </code> to match lessons with no 
     *          planned start time 
     */

    @OSID @Override
    public void matchAnyPlannedStartTime(boolean match) {
        return;
    }


    /**
     *  Clears the planned start time terms. 
     */

    @OSID @Override
    public void clearPlannedStartTimeTerms() {
        return;
    }


    /**
     *  Matches an actual start time within the given date range inclusive. 
     *
     *  @param  from a starting date 
     *  @param  to a ending date 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchActualStartTime(org.osid.calendaring.Duration from, 
                                     org.osid.calendaring.Duration to, 
                                     boolean match) {
        return;
    }


    /**
     *  Matches a lesson that has any actual start time. 
     *
     *  @param  match <code> true </code> to match lessons with any actual 
     *          start time, <code> false </code> to match lessons with no 
     *          actual start time 
     */

    @OSID @Override
    public void matchAnyActualStartTime(boolean match) {
        return;
    }


    /**
     *  Clears the actual start time terms. 
     */

    @OSID @Override
    public void clearActualStartTimeTerms() {
        return;
    }


    /**
     *  Sets the starting activity <code> Id </code> for this query. 
     *
     *  @param  activitytId an activity <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> activityId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchActualStartingActivityId(org.osid.id.Id activitytId, 
                                              boolean match) {
        return;
    }


    /**
     *  Clears the starting activity <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearActualStartingActivityIdTerms() {
        return;
    }


    /**
     *  Tests if a starting activity query is available. 
     *
     *  @return <code> true </code> if a starting activity query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActualStartingActivityQuery() {
        return (false);
    }


    /**
     *  Gets the query for a starting activity. 
     *
     *  @return the activity query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActualStartingActivityQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityQuery getActualStartingActivityQuery() {
        throw new org.osid.UnimplementedException("supportsActualStartingActivityQuery() is false");
    }


    /**
     *  Matches a lesson that has any starting activity. 
     *
     *  @param  match <code> true </code> to match lessons with any starting 
     *          activity, <code> false </code> to match lessons with no 
     *          starting activity 
     */

    @OSID @Override
    public void matchAnyActualStartingActivity(boolean match) {
        return;
    }


    /**
     *  Clears the starting activity terms. 
     */

    @OSID @Override
    public void clearActualStartingActivityTerms() {
        return;
    }


    /**
     *  Matches a lesson that has been completed. 
     *
     *  @param  match <code> true </code> to match lessons that have been 
     *          completed, <code> false </code> to match lessons that have not 
     *          been completed 
     */

    @OSID @Override
    public void matchComplete(boolean match) {
        return;
    }


    /**
     *  Clears the complete terms. 
     */

    @OSID @Override
    public void clearCompleteTerms() {
        return;
    }


    /**
     *  Matches a lesson that has been skipped. 
     *
     *  @param  match <code> true </code> to match lessons that have been 
     *          skipped, <code> false </code> to match lessons that have not 
     *          been skipped 
     */

    @OSID @Override
    public void matchSkipped(boolean match) {
        return;
    }


    /**
     *  Clears the skipped terms. 
     */

    @OSID @Override
    public void clearSkippedTerms() {
        return;
    }


    /**
     *  Matches an actual ending time within the given date range inclusive. 
     *
     *  @param  from a starting date 
     *  @param  to a ending date 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchActualEndTime(org.osid.calendaring.Duration from, 
                                   org.osid.calendaring.Duration to, 
                                   boolean match) {
        return;
    }


    /**
     *  Matches a lesson that has any actual end time. 
     *
     *  @param  match <code> true </code> to match lessons with any actual end 
     *          time, <code> false </code> to match lessons with no actual end 
     *          time 
     */

    @OSID @Override
    public void matchAnyActualEndTime(boolean match) {
        return;
    }


    /**
     *  Clears the actual end time terms. 
     */

    @OSID @Override
    public void clearActualEndTimeTerms() {
        return;
    }


    /**
     *  Sets the ending activity <code> Id </code> for this query. 
     *
     *  @param  activitytId an activity <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> activityId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchActualEndingActivityId(org.osid.id.Id activitytId, 
                                            boolean match) {
        return;
    }


    /**
     *  Clears the ending activity <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearActualEndingActivityIdTerms() {
        return;
    }


    /**
     *  Tests if an ending activity query is available. 
     *
     *  @return <code> true </code> if an ending activity query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActualEndingActivityQuery() {
        return (false);
    }


    /**
     *  Gets the query for an ending activity. 
     *
     *  @return the activity query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEndingActivityQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityQuery getActualEndingActivityQuery() {
        throw new org.osid.UnimplementedException("supportsActualEndingActivityQuery() is false");
    }


    /**
     *  Matches a lesson that has any ending activity. 
     *
     *  @param  match <code> true </code> to match lessons with any ending 
     *          activity, <code> false </code> to match lessons with no ending 
     *          activity 
     */

    @OSID @Override
    public void matchAnyActualEndingActivity(boolean match) {
        return;
    }


    /**
     *  Clears the ending activity terms. 
     */

    @OSID @Override
    public void clearActualEndingActivityTerms() {
        return;
    }


    /**
     *  Matches an actual time spent within the given duration range 
     *  inclusive. 
     *
     *  @param  from a starting date 
     *  @param  to a ending date 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchActualTimeSpent(org.osid.calendaring.Duration from, 
                                     org.osid.calendaring.Duration to, 
                                     boolean match) {
        return;
    }


    /**
     *  Matches a lesson that has any actual time spent. 
     *
     *  @param  match <code> true </code> to match lessons with any actual 
     *          time spent, <code> false </code> to match lessons with no time 
     *          spent 
     */

    @OSID @Override
    public void matchAnyActualTimeSpent(boolean match) {
        return;
    }


    /**
     *  Clears the actual time spent terms. 
     */

    @OSID @Override
    public void clearActualTimeSpentTerms() {
        return;
    }


    /**
     *  Sets the lesson <code> Id </code> for this query to match syllabi 
     *  assigned to course catalogs. 
     *
     *  @param  courseCatalogId a course catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseCatalogId(org.osid.id.Id courseCatalogId, 
                                     boolean match) {
        return;
    }


    /**
     *  Clears the course catalog <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseCatalogIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> CourseCatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a course catalog query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course catalog query. Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the course catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQuery getCourseCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsCourseCatalogQuery() is false");
    }


    /**
     *  Clears the course catalog terms. 
     */

    @OSID @Override
    public void clearCourseCatalogTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given lesson query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a lesson implementing the requested record.
     *
     *  @param lessonRecordType a lesson record type
     *  @return the lesson query record
     *  @throws org.osid.NullArgumentException
     *          <code>lessonRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(lessonRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.plan.records.LessonQueryRecord getLessonQueryRecord(org.osid.type.Type lessonRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.plan.records.LessonQueryRecord record : this.records) {
            if (record.implementsRecordType(lessonRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(lessonRecordType + " is not supported");
    }


    /**
     *  Adds a record to this lesson query. 
     *
     *  @param lessonQueryRecord lesson query record
     *  @param lessonRecordType lesson record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addLessonQueryRecord(org.osid.course.plan.records.LessonQueryRecord lessonQueryRecord, 
                                          org.osid.type.Type lessonRecordType) {

        addRecordType(lessonRecordType);
        nullarg(lessonQueryRecord, "lesson query record");
        this.records.add(lessonQueryRecord);        
        return;
    }
}
