//
// AbstractIndexedMapEnrollmentLookupSession.java
//
//    A simple framework for providing an Enrollment lookup service
//    backed by a fixed collection of enrollments with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.program.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an Enrollment lookup service backed by a
 *  fixed collection of enrollments. The enrollments are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some enrollments may be compatible
 *  with more types than are indicated through these enrollment
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Enrollments</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapEnrollmentLookupSession
    extends AbstractMapEnrollmentLookupSession
    implements org.osid.course.program.EnrollmentLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.course.program.Enrollment> enrollmentsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.course.program.Enrollment>());
    private final MultiMap<org.osid.type.Type, org.osid.course.program.Enrollment> enrollmentsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.course.program.Enrollment>());


    /**
     *  Makes an <code>Enrollment</code> available in this session.
     *
     *  @param  enrollment an enrollment
     *  @throws org.osid.NullArgumentException <code>enrollment<code> is
     *          <code>null</code>
     */

    @Override
    protected void putEnrollment(org.osid.course.program.Enrollment enrollment) {
        super.putEnrollment(enrollment);

        this.enrollmentsByGenus.put(enrollment.getGenusType(), enrollment);
        
        try (org.osid.type.TypeList types = enrollment.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.enrollmentsByRecord.put(types.getNextType(), enrollment);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an enrollment from this session.
     *
     *  @param enrollmentId the <code>Id</code> of the enrollment
     *  @throws org.osid.NullArgumentException <code>enrollmentId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeEnrollment(org.osid.id.Id enrollmentId) {
        org.osid.course.program.Enrollment enrollment;
        try {
            enrollment = getEnrollment(enrollmentId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.enrollmentsByGenus.remove(enrollment.getGenusType());

        try (org.osid.type.TypeList types = enrollment.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.enrollmentsByRecord.remove(types.getNextType(), enrollment);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeEnrollment(enrollmentId);
        return;
    }


    /**
     *  Gets an <code>EnrollmentList</code> corresponding to the given
     *  enrollment genus <code>Type</code> which does not include
     *  enrollments of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known enrollments or an error results. Otherwise,
     *  the returned list may contain only those enrollments that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  enrollmentGenusType an enrollment genus type 
     *  @return the returned <code>Enrollment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>enrollmentGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollmentsByGenusType(org.osid.type.Type enrollmentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.program.enrollment.ArrayEnrollmentList(this.enrollmentsByGenus.get(enrollmentGenusType)));
    }


    /**
     *  Gets an <code>EnrollmentList</code> containing the given
     *  enrollment record <code>Type</code>. In plenary mode, the
     *  returned list contains all known enrollments or an error
     *  results. Otherwise, the returned list may contain only those
     *  enrollments that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  enrollmentRecordType an enrollment record type 
     *  @return the returned <code>enrollment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>enrollmentRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollmentsByRecordType(org.osid.type.Type enrollmentRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.program.enrollment.ArrayEnrollmentList(this.enrollmentsByRecord.get(enrollmentRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.enrollmentsByGenus.clear();
        this.enrollmentsByRecord.clear();

        super.close();

        return;
    }
}
