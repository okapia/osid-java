//
// AbstractAdapterFoundryLookupSession.java
//
//    A Foundry lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.resourcing.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Foundry lookup session adapter.
 */

public abstract class AbstractAdapterFoundryLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.resourcing.FoundryLookupSession {

    private final org.osid.resourcing.FoundryLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterFoundryLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterFoundryLookupSession(org.osid.resourcing.FoundryLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@code Foundry} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupFoundries() {
        return (this.session.canLookupFoundries());
    }


    /**
     *  A complete view of the {@code Foundry} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeFoundryView() {
        this.session.useComparativeFoundryView();
        return;
    }


    /**
     *  A complete view of the {@code Foundry} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryFoundryView() {
        this.session.usePlenaryFoundryView();
        return;
    }

     
    /**
     *  Gets the {@code Foundry} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Foundry} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Foundry} and
     *  retained for compatibility.
     *
     *  @param foundryId {@code Id} of the {@code Foundry}
     *  @return the foundry
     *  @throws org.osid.NotFoundException {@code foundryId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code foundryId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Foundry getFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getFoundry(foundryId));
    }


    /**
     *  Gets a {@code FoundryList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  foundries specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Foundries} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  foundryIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Foundry} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code foundryIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryList getFoundriesByIds(org.osid.id.IdList foundryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getFoundriesByIds(foundryIds));
    }


    /**
     *  Gets a {@code FoundryList} corresponding to the given
     *  foundry genus {@code Type} which does not include
     *  foundries of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  foundries or an error results. Otherwise, the returned list
     *  may contain only those foundries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  foundryGenusType a foundry genus type 
     *  @return the returned {@code Foundry} list
     *  @throws org.osid.NullArgumentException
     *          {@code foundryGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryList getFoundriesByGenusType(org.osid.type.Type foundryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getFoundriesByGenusType(foundryGenusType));
    }


    /**
     *  Gets a {@code FoundryList} corresponding to the given
     *  foundry genus {@code Type} and include any additional
     *  foundries with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  foundries or an error results. Otherwise, the returned list
     *  may contain only those foundries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  foundryGenusType a foundry genus type 
     *  @return the returned {@code Foundry} list
     *  @throws org.osid.NullArgumentException
     *          {@code foundryGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryList getFoundriesByParentGenusType(org.osid.type.Type foundryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getFoundriesByParentGenusType(foundryGenusType));
    }


    /**
     *  Gets a {@code FoundryList} containing the given
     *  foundry record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  foundries or an error results. Otherwise, the returned list
     *  may contain only those foundries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  foundryRecordType a foundry record type 
     *  @return the returned {@code Foundry} list
     *  @throws org.osid.NullArgumentException
     *          {@code foundryRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryList getFoundriesByRecordType(org.osid.type.Type foundryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getFoundriesByRecordType(foundryRecordType));
    }


    /**
     *  Gets a {@code FoundryList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  foundries or an error results. Otherwise, the returned list
     *  may contain only those foundries that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Foundry} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryList getFoundriesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getFoundriesByProvider(resourceId));
    }


    /**
     *  Gets all {@code Foundries}. 
     *
     *  In plenary mode, the returned list contains all known
     *  foundries or an error results. Otherwise, the returned list
     *  may contain only those foundries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Foundries} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryList getFoundries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getFoundries());
    }
}
