//
// AbstractAdapterPackageLookupSession.java
//
//    A Package lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.installation.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Package lookup session adapter.
 */

public abstract class AbstractAdapterPackageLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.installation.PackageLookupSession {

    private final org.osid.installation.PackageLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterPackageLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterPackageLookupSession(org.osid.installation.PackageLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Depot/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Depot Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDepotId() {
        return (this.session.getDepotId());
    }


    /**
     *  Gets the {@code Depot} associated with this session.
     *
     *  @return the {@code Depot} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.Depot getDepot()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getDepot());
    }


    /**
     *  Tests if this user can perform {@code Package} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupPackages() {
        return (this.session.canLookupPackages());
    }


    /**
     *  A complete view of the {@code Package} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativePackageView() {
        this.session.useComparativePackageView();
        return;
    }


    /**
     *  A complete view of the {@code Package} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryPackageView() {
        this.session.usePlenaryPackageView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include packages in depots which are children
     *  of this depot in the depot hierarchy.
     */

    @OSID @Override
    public void useFederatedDepotView() {
        this.session.useFederatedDepotView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this depot only.
     */

    @OSID @Override
    public void useIsolatedDepotView() {
        this.session.useIsolatedDepotView();
        return;
    }


    /**
     *  The returns from the lookup methods may omit multiple versions
     *  of the same installation.
     */

    @OSID @Override
    public void useNormalizedVersionView() {
        this.session.useNormalizedVersionView();
        return;
    }


    /**
     *  All versions of the same installation are returned. 
     */

    @OSID @Override
    public void useDenormalizedVersionView() {
        this.session.useDenormalizedVersionView();
        return;
    }

     
    /**
     *  Gets the {@code Package} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Package} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Package} and
     *  retained for compatibility.
     *
     *  @param pkgId {@code Id} of the {@code Package}
     *  @return the package
     *  @throws org.osid.NotFoundException {@code pkgId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code pkgId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.Package getPackage(org.osid.id.Id pkgId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPackage(pkgId));
    }


    /**
     *  Gets a {@code PackageList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  packages specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Packages} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  pkgIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Package} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code pkgIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.PackageList getPackagesByIds(org.osid.id.IdList pkgIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPackagesByIds(pkgIds));
    }


    /**
     *  Gets a {@code PackageList} corresponding to the given
     *  package genus {@code Type} which does not include
     *  packages of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  packages or an error results. Otherwise, the returned list
     *  may contain only those packages that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  pkgGenusType a pkg genus type 
     *  @return the returned {@code Package} list
     *  @throws org.osid.NullArgumentException
     *          {@code pkgGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.PackageList getPackagesByGenusType(org.osid.type.Type pkgGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPackagesByGenusType(pkgGenusType));
    }


    /**
     *  Gets a {@code PackageList} corresponding to the given
     *  package genus {@code Type} and include any additional
     *  packages with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  packages or an error results. Otherwise, the returned list
     *  may contain only those packages that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  pkgGenusType a pkg genus type 
     *  @return the returned {@code Package} list
     *  @throws org.osid.NullArgumentException
     *          {@code pkgGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.PackageList getPackagesByParentGenusType(org.osid.type.Type pkgGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPackagesByParentGenusType(pkgGenusType));
    }


    /**
     *  Gets a {@code PackageList} containing the given
     *  package record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  packages or an error results. Otherwise, the returned list
     *  may contain only those packages that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  pkgRecordType a pkg record type 
     *  @return the returned {@code Package} list
     *  @throws org.osid.NullArgumentException
     *          {@code pkgRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.PackageList getPackagesByRecordType(org.osid.type.Type pkgRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPackagesByRecordType(pkgRecordType));
    }


    /**
     *  Gets a {@code PackageList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  packages or an error results. Otherwise, the returned list
     *  may contain only those packages that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Package} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.installation.PackageList getPackagesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPackagesByProvider(resourceId));
    }


    /**
     *  Gets a list of packages depending on the given package. 
     *
     *  @param  packageId an {@code Id} of a {@code Package} 
     *  @return list of package dependents 
     *  @throws org.osid.NotFoundException {@code packageId} is not
     *          found
     *  @throws org.osid.NullArgumentException {@code packageId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @compliance mandatory This method must be implemented. 
     */

    @OSID @Override
    public org.osid.installation.PackageList getDependentPackages(org.osid.id.Id packageId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDependentPackages(packageId));
    }


    /**
     *  Gets a list of packages in the specified package version
     *  chain.
     *
     *  @param  packageId an {@code Id} of a {@code Package} 
     *  @return list of dependencies 
     *  @throws org.osid.NotFoundException {@code packageId} is not
     *          found
     *  @throws org.osid.NullArgumentException {@code packageId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.installation.PackageList getPackageVersions(org.osid.id.Id packageId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPackageVersions(packageId));
    }

    
    /**
     *  Gets all {@code Packages}. 
     *
     *  In plenary mode, the returned list contains all known
     *  packages or an error results. Otherwise, the returned list
     *  may contain only those packages that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Packages} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.PackageList getPackages()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPackages());
    }
}
