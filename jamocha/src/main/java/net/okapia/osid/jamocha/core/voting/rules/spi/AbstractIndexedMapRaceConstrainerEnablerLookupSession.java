//
// AbstractIndexedMapRaceConstrainerEnablerLookupSession.java
//
//    A simple framework for providing a RaceConstrainerEnabler lookup service
//    backed by a fixed collection of race constrainer enablers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.voting.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a RaceConstrainerEnabler lookup service backed by a
 *  fixed collection of race constrainer enablers. The race constrainer enablers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some race constrainer enablers may be compatible
 *  with more types than are indicated through these race constrainer enabler
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>RaceConstrainerEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapRaceConstrainerEnablerLookupSession
    extends AbstractMapRaceConstrainerEnablerLookupSession
    implements org.osid.voting.rules.RaceConstrainerEnablerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.voting.rules.RaceConstrainerEnabler> raceConstrainerEnablersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.voting.rules.RaceConstrainerEnabler>());
    private final MultiMap<org.osid.type.Type, org.osid.voting.rules.RaceConstrainerEnabler> raceConstrainerEnablersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.voting.rules.RaceConstrainerEnabler>());


    /**
     *  Makes a <code>RaceConstrainerEnabler</code> available in this session.
     *
     *  @param  raceConstrainerEnabler a race constrainer enabler
     *  @throws org.osid.NullArgumentException <code>raceConstrainerEnabler<code> is
     *          <code>null</code>
     */

    @Override
    protected void putRaceConstrainerEnabler(org.osid.voting.rules.RaceConstrainerEnabler raceConstrainerEnabler) {
        super.putRaceConstrainerEnabler(raceConstrainerEnabler);

        this.raceConstrainerEnablersByGenus.put(raceConstrainerEnabler.getGenusType(), raceConstrainerEnabler);
        
        try (org.osid.type.TypeList types = raceConstrainerEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.raceConstrainerEnablersByRecord.put(types.getNextType(), raceConstrainerEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a race constrainer enabler from this session.
     *
     *  @param raceConstrainerEnablerId the <code>Id</code> of the race constrainer enabler
     *  @throws org.osid.NullArgumentException <code>raceConstrainerEnablerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeRaceConstrainerEnabler(org.osid.id.Id raceConstrainerEnablerId) {
        org.osid.voting.rules.RaceConstrainerEnabler raceConstrainerEnabler;
        try {
            raceConstrainerEnabler = getRaceConstrainerEnabler(raceConstrainerEnablerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.raceConstrainerEnablersByGenus.remove(raceConstrainerEnabler.getGenusType());

        try (org.osid.type.TypeList types = raceConstrainerEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.raceConstrainerEnablersByRecord.remove(types.getNextType(), raceConstrainerEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeRaceConstrainerEnabler(raceConstrainerEnablerId);
        return;
    }


    /**
     *  Gets a <code>RaceConstrainerEnablerList</code> corresponding to the given
     *  race constrainer enabler genus <code>Type</code> which does not include
     *  race constrainer enablers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known race constrainer enablers or an error results. Otherwise,
     *  the returned list may contain only those race constrainer enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  raceConstrainerEnablerGenusType a race constrainer enabler genus type 
     *  @return the returned <code>RaceConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>raceConstrainerEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerList getRaceConstrainerEnablersByGenusType(org.osid.type.Type raceConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.voting.rules.raceconstrainerenabler.ArrayRaceConstrainerEnablerList(this.raceConstrainerEnablersByGenus.get(raceConstrainerEnablerGenusType)));
    }


    /**
     *  Gets a <code>RaceConstrainerEnablerList</code> containing the given
     *  race constrainer enabler record <code>Type</code>. In plenary mode, the
     *  returned list contains all known race constrainer enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  race constrainer enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  raceConstrainerEnablerRecordType a race constrainer enabler record type 
     *  @return the returned <code>raceConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>raceConstrainerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerList getRaceConstrainerEnablersByRecordType(org.osid.type.Type raceConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.voting.rules.raceconstrainerenabler.ArrayRaceConstrainerEnablerList(this.raceConstrainerEnablersByRecord.get(raceConstrainerEnablerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.raceConstrainerEnablersByGenus.clear();
        this.raceConstrainerEnablersByRecord.clear();

        super.close();

        return;
    }
}
