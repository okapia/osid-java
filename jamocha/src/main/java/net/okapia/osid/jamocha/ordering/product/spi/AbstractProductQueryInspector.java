//
// AbstractProductQueryInspector.java
//
//     A template for making a ProductQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ordering.product.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for products.
 */

public abstract class AbstractProductQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.ordering.ProductQueryInspector {

    private final java.util.Collection<org.osid.ordering.records.ProductQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the code terms. 
     *
     *  @return the code terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getCodeTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the price schedule <code> Id </code> terms. 
     *
     *  @return the price schedule <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPriceScheduleIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the price schedule terms. 
     *
     *  @return the price schedule terms 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleQueryInspector[] getPriceScheduleTerms() {
        return (new org.osid.ordering.PriceScheduleQueryInspector[0]);
    }


    /**
     *  Gets the availability terms. 
     *
     *  @return the availability terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getAvailabilityTerms() {
        return (new org.osid.search.terms.CardinalRangeTerm[0]);
    }


    /**
     *  Gets the item <code> Id </code> terms. 
     *
     *  @return the item <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getItemIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the item terms. 
     *
     *  @return the item terms 
     */

    @OSID @Override
    public org.osid.ordering.ItemQueryInspector[] getItemTerms() {
        return (new org.osid.ordering.ItemQueryInspector[0]);
    }


    /**
     *  Gets the store <code> Id </code> terms. 
     *
     *  @return the store <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getStoreIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the store terms. 
     *
     *  @return the store terms 
     */

    @OSID @Override
    public org.osid.ordering.StoreQueryInspector[] getStoreTerms() {
        return (new org.osid.ordering.StoreQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given product query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a product implementing the requested record.
     *
     *  @param productRecordType a product record type
     *  @return the product query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>productRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(productRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.ordering.records.ProductQueryInspectorRecord getProductQueryInspectorRecord(org.osid.type.Type productRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ordering.records.ProductQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(productRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(productRecordType + " is not supported");
    }


    /**
     *  Adds a record to this product query. 
     *
     *  @param productQueryInspectorRecord product query inspector
     *         record
     *  @param productRecordType product record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addProductQueryInspectorRecord(org.osid.ordering.records.ProductQueryInspectorRecord productQueryInspectorRecord, 
                                                   org.osid.type.Type productRecordType) {

        addRecordType(productRecordType);
        nullarg(productRecordType, "product record type");
        this.records.add(productQueryInspectorRecord);        
        return;
    }
}
