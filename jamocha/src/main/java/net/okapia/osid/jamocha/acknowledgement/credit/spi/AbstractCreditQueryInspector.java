//
// AbstractCreditQueryInspector.java
//
//     A template for making a CreditQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.acknowledgement.credit.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for credits.
 */

public abstract class AbstractCreditQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQueryInspector
    implements org.osid.acknowledgement.CreditQueryInspector {

    private final java.util.Collection<org.osid.acknowledgement.records.CreditQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the reference <code> Id </code> query terms. 
     *
     *  @return the reference <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getReferenceIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the resource <code> Id </code> query terms. 
     *
     *  @return the resource <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResourceIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the resource query terms. 
     *
     *  @return the resource terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getResourceTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the billing <code> Id </code> query terms. 
     *
     *  @return the billing <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBillingIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the billing query terms. 
     *
     *  @return the billing terms 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingQueryInspector[] getBillingTerms() {
        return (new org.osid.acknowledgement.BillingQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given credit query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a credit implementing the requested record.
     *
     *  @param creditRecordType a credit record type
     *  @return the credit query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>creditRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(creditRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.acknowledgement.records.CreditQueryInspectorRecord getCreditQueryInspectorRecord(org.osid.type.Type creditRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.acknowledgement.records.CreditQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(creditRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(creditRecordType + " is not supported");
    }


    /**
     *  Adds a record to this credit query. 
     *
     *  @param creditQueryInspectorRecord credit query inspector
     *         record
     *  @param creditRecordType credit record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCreditQueryInspectorRecord(org.osid.acknowledgement.records.CreditQueryInspectorRecord creditQueryInspectorRecord, 
                                                   org.osid.type.Type creditRecordType) {

        addRecordType(creditRecordType);
        nullarg(creditRecordType, "credit record type");
        this.records.add(creditQueryInspectorRecord);        
        return;
    }
}
