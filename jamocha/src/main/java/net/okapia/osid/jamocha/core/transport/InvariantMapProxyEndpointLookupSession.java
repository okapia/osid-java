//
// InvariantMapProxyEndpointLookupSession
//
//    Implements an Endpoint lookup service backed by a fixed
//    collection of endpoints. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.transport;


/**
 *  Implements an Endpoint lookup service backed by a fixed
 *  collection of endpoints. The endpoints are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyEndpointLookupSession
    extends net.okapia.osid.jamocha.core.transport.spi.AbstractMapEndpointLookupSession
    implements org.osid.transport.EndpointLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyEndpointLookupSession} with no
     *  endpoints.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public InvariantMapProxyEndpointLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyEndpointLookupSession} with a
     *  single endpoint.
     *
     *  @param endpoint an single endpoint
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code endpoint} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyEndpointLookupSession(org.osid.transport.Endpoint endpoint, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putEndpoint(endpoint);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyEndpointLookupSession} using
     *  an array of endpoints.
     *
     *  @param endpoints an array of endpoints
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code endpoints} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyEndpointLookupSession(org.osid.transport.Endpoint[] endpoints, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putEndpoints(endpoints);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyEndpointLookupSession} using a
     *  collection of endpoints.
     *
     *  @param endpoints a collection of endpoints
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code endpoints} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyEndpointLookupSession(java.util.Collection<? extends org.osid.transport.Endpoint> endpoints,
                                                  org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putEndpoints(endpoints);
        return;
    }
}
