//
// AbstractGradingCalculationManager.java
//
//     An adapter for a GradingCalculationManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.grading.calculation.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a GradingCalculationManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterGradingCalculationManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.grading.calculation.GradingCalculationManager>
    implements org.osid.grading.calculation.GradingCalculationManager {


    /**
     *  Constructs a new {@code AbstractAdapterGradingCalculationManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterGradingCalculationManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterGradingCalculationManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterGradingCalculationManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if a gradebook column calculation lookup service is supported. 
     *
     *  @return <code> true </code> if gradebook column calculation lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookColumnCalculationLookup() {
        return (getAdapteeManager().supportsGradebookColumnCalculationLookup());
    }


    /**
     *  Tests if a gradebook column calculation administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if gradebook column calculation admin is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookColumnCalculationAdmin() {
        return (getAdapteeManager().supportsGradebookColumnCalculationAdmin());
    }


    /**
     *  Gets the supported <code> GradebookColumnCalculation </code> record 
     *  types. 
     *
     *  @return a list containing the supported <code> 
     *          GradebookColumnCalculation </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getGradebookColumnCalculationRecordTypes() {
        return (getAdapteeManager().getGradebookColumnCalculationRecordTypes());
    }


    /**
     *  Tests if the given <code> GradebookColumnCalculation </code> record 
     *  type is supported. 
     *
     *  @param  gradebookColumnCalculationRecordType a <code> Type </code> 
     *          indicating a <code> GradebookColumnCalculation </code> type 
     *  @return <code> true </code> if the given gradebook column calculation 
     *          record <code> Type </code> is supported, <code> false </code> 
     *          otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          gradebookColumnCalculationRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsGradebookColumnCalculationRecordType(org.osid.type.Type gradebookColumnCalculationRecordType) {
        return (getAdapteeManager().supportsGradebookColumnCalculationRecordType(gradebookColumnCalculationRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the gradebook 
     *  column calculation lookup service. 
     *
     *  @return a <code> GradebookColumnCalculationLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.calculation.GradebookColumnCalculationLookupSession getGradebookColumnCalculationLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getGradebookColumnCalculationLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the gradebook 
     *  column calculation lookup service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @return <code> a GradebookColumnCalculationLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnCalculationLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.calculation.GradebookColumnCalculationLookupSession getGradebookColumnCalculationLookupSessionForGradebook(org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getGradebookColumnCalculationLookupSessionForGradebook(gradebookId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the gradebook 
     *  column calculation administrative service. 
     *
     *  @return a <code> GradebookColumnCalculationAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.calculation.GradebookColumnCalculationAdminSession getGradebookColumnCalculationAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getGradebookColumnCalculationAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the gradebook 
     *  column calculation administrative service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @return <code> a GradebookColumnCalculationAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnCalculationAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.calculation.GradebookColumnCalculationAdminSession getGradebookColumnCalculationAdminSessionForGradebook(org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getGradebookColumnCalculationAdminSessionForGradebook(gradebookId));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
