//
// AbstractIndexedMapSiteLookupSession.java
//
//    A simple framework for providing a Site lookup service
//    backed by a fixed collection of sites with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.installation.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Site lookup service backed by a
 *  fixed collection of sites. 
 */

public abstract class AbstractIndexedMapSiteLookupSession
    extends AbstractMapSiteLookupSession
    implements org.osid.installation.SiteLookupSession {

    /**
     *  Makes a <code>Site</code> available in this session.
     *
     *  @param  site a site
     *  @throws org.osid.NullArgumentException <code>site<code> is
     *          <code>null</code>
     */

    @Override
    protected void putSite(org.osid.installation.Site site) {
        super.putSite(site);
        return;
    }


    /**
     *  Removes a site from this session.
     *
     *  @param siteId the <code>Id</code> of the site
     *  @throws org.osid.NullArgumentException <code>siteId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeSite(org.osid.id.Id siteId) {
        super.removeSite(siteId);
        return;
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        return;
    }
}
