//
// AbstractSpeedZoneEnablerQueryInspector.java
//
//     A template for making a SpeedZoneEnablerQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.path.rules.speedzoneenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for speed zone enablers.
 */

public abstract class AbstractSpeedZoneEnablerQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQueryInspector
    implements org.osid.mapping.path.rules.SpeedZoneEnablerQueryInspector {

    private final java.util.Collection<org.osid.mapping.path.rules.records.SpeedZoneEnablerQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the speed zone <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledSpeedZoneIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the speed zone query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneQueryInspector[] getRuledSpeedZoneTerms() {
        return (new org.osid.mapping.path.SpeedZoneQueryInspector[0]);
    }


    /**
     *  Gets the map <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getMapIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the map query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.MapQueryInspector[] getMapTerms() {
        return (new org.osid.mapping.MapQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given speed zone enabler query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a speed zone enabler implementing the requested record.
     *
     *  @param speedZoneEnablerRecordType a speed zone enabler record type
     *  @return the speed zone enabler query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>speedZoneEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(speedZoneEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.path.rules.records.SpeedZoneEnablerQueryInspectorRecord getSpeedZoneEnablerQueryInspectorRecord(org.osid.type.Type speedZoneEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.path.rules.records.SpeedZoneEnablerQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(speedZoneEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(speedZoneEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this speed zone enabler query. 
     *
     *  @param speedZoneEnablerQueryInspectorRecord speed zone enabler query inspector
     *         record
     *  @param speedZoneEnablerRecordType speedZoneEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addSpeedZoneEnablerQueryInspectorRecord(org.osid.mapping.path.rules.records.SpeedZoneEnablerQueryInspectorRecord speedZoneEnablerQueryInspectorRecord, 
                                                   org.osid.type.Type speedZoneEnablerRecordType) {

        addRecordType(speedZoneEnablerRecordType);
        nullarg(speedZoneEnablerRecordType, "speed zone enabler record type");
        this.records.add(speedZoneEnablerQueryInspectorRecord);        
        return;
    }
}
