//
// AbstractMapAntimatroidLookupSession
//
//    A simple framework for providing an Antimatroid lookup service
//    backed by a fixed collection of antimatroids.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.sequencing.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an Antimatroid lookup service backed by a
 *  fixed collection of antimatroids. The antimatroids are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Antimatroids</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapAntimatroidLookupSession
    extends net.okapia.osid.jamocha.sequencing.spi.AbstractAntimatroidLookupSession
    implements org.osid.sequencing.AntimatroidLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.sequencing.Antimatroid> antimatroids = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.sequencing.Antimatroid>());


    /**
     *  Makes an <code>Antimatroid</code> available in this session.
     *
     *  @param  antimatroid an antimatroid
     *  @throws org.osid.NullArgumentException <code>antimatroid<code>
     *          is <code>null</code>
     */

    protected void putAntimatroid(org.osid.sequencing.Antimatroid antimatroid) {
        this.antimatroids.put(antimatroid.getId(), antimatroid);
        return;
    }


    /**
     *  Makes an array of antimatroids available in this session.
     *
     *  @param  antimatroids an array of antimatroids
     *  @throws org.osid.NullArgumentException <code>antimatroids<code>
     *          is <code>null</code>
     */

    protected void putAntimatroids(org.osid.sequencing.Antimatroid[] antimatroids) {
        putAntimatroids(java.util.Arrays.asList(antimatroids));
        return;
    }


    /**
     *  Makes a collection of antimatroids available in this session.
     *
     *  @param  antimatroids a collection of antimatroids
     *  @throws org.osid.NullArgumentException <code>antimatroids<code>
     *          is <code>null</code>
     */

    protected void putAntimatroids(java.util.Collection<? extends org.osid.sequencing.Antimatroid> antimatroids) {
        for (org.osid.sequencing.Antimatroid antimatroid : antimatroids) {
            this.antimatroids.put(antimatroid.getId(), antimatroid);
        }

        return;
    }


    /**
     *  Removes an Antimatroid from this session.
     *
     *  @param  antimatroidId the <code>Id</code> of the antimatroid
     *  @throws org.osid.NullArgumentException <code>antimatroidId<code> is
     *          <code>null</code>
     */

    protected void removeAntimatroid(org.osid.id.Id antimatroidId) {
        this.antimatroids.remove(antimatroidId);
        return;
    }


    /**
     *  Gets the <code>Antimatroid</code> specified by its <code>Id</code>.
     *
     *  @param  antimatroidId <code>Id</code> of the <code>Antimatroid</code>
     *  @return the antimatroid
     *  @throws org.osid.NotFoundException <code>antimatroidId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>antimatroidId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.sequencing.Antimatroid getAntimatroid(org.osid.id.Id antimatroidId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.sequencing.Antimatroid antimatroid = this.antimatroids.get(antimatroidId);
        if (antimatroid == null) {
            throw new org.osid.NotFoundException("antimatroid not found: " + antimatroidId);
        }

        return (antimatroid);
    }


    /**
     *  Gets all <code>Antimatroids</code>. In plenary mode, the returned
     *  list contains all known antimatroids or an error
     *  results. Otherwise, the returned list may contain only those
     *  antimatroids that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Antimatroids</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.sequencing.AntimatroidList getAntimatroids()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.sequencing.antimatroid.ArrayAntimatroidList(this.antimatroids.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.antimatroids.clear();
        super.close();
        return;
    }
}
