//
// OffsetEventEnablerElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.rules.offseteventenabler.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class OffsetEventEnablerElements
    extends net.okapia.osid.jamocha.spi.OsidEnablerElements {


    /**
     *  Gets the OffsetEventEnablerElement Id.
     *
     *  @return the offset event enabler element Id
     */

    public static org.osid.id.Id getOffsetEventEnablerEntityId() {
        return (makeEntityId("osid.calendaring.rules.OffsetEventEnabler"));
    }


    /**
     *  Gets the RuledOffsetEventId element Id.
     *
     *  @return the RuledOffsetEventId element Id
     */

    public static org.osid.id.Id getRuledOffsetEventId() {
        return (makeQueryElementId("osid.calendaring.rules.offseteventenabler.RuledOffsetEventId"));
    }


    /**
     *  Gets the RuledOffsetEvent element Id.
     *
     *  @return the RuledOffsetEvent element Id
     */

    public static org.osid.id.Id getRuledOffsetEvent() {
        return (makeQueryElementId("osid.calendaring.rules.offseteventenabler.RuledOffsetEvent"));
    }


    /**
     *  Gets the CalendarId element Id.
     *
     *  @return the CalendarId element Id
     */

    public static org.osid.id.Id getCalendarId() {
        return (makeQueryElementId("osid.calendaring.rules.offseteventenabler.CalendarId"));
    }


    /**
     *  Gets the Calendar element Id.
     *
     *  @return the Calendar element Id
     */

    public static org.osid.id.Id getCalendar() {
        return (makeQueryElementId("osid.calendaring.rules.offseteventenabler.Calendar"));
    }
}
