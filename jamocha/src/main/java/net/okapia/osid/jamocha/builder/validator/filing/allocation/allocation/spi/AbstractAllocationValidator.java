//
// AbstractAllocationValidator.java
//
//     Validates an Allocation.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.filing.allocation.allocation.spi;


/**
 *  Validates an Allocation.
 */

public abstract class AbstractAllocationValidator
    extends net.okapia.osid.jamocha.builder.validator.spi.AbstractExtensibleValidator {


    /**
     *  Constructs a new <code>AbstractAllocationValidator</code>.
     */

    protected AbstractAllocationValidator() {
        return;
    }


    /**
     *  Constructs a new <code>AbstractAllocationValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    protected AbstractAllocationValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates an Allocation.
     *
     *  @param allocation an allocation to validate
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>allocation</code>
     *          is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in
     *          assembly
     */
    
    public void validate(org.osid.filing.allocation.Allocation allocation) {
        super.validate(allocation);

        test(allocation.getDirectoryPath(), "getDirectoryPath()");
        test(allocation.getDirectory(), "getDirectory()");

        if (!allocation.getDirectoryPath().equals(allocation.getDirectory().getPath())) {
            throw new org.osid.BadLogicException(allocation.getDirectoryPath() + 
                                                 " does not match " + allocation.getDirectory().getPath());
        }

        testConditionalMethod(allocation, "getAgentId", allocation.isAssignedToUser(), "isAssignedToUser()");
        testConditionalMethod(allocation, "getAgent", allocation.isAssignedToUser(), "isAssignedToUser()");
        if (allocation.isAssignedToUser()) {
            testNestedObject(allocation, "getAgent");
        }

        testCardinal(allocation.getTotalSpace(), "getTotalSpace()");
        testCardinal(allocation.getUsedSpace(), "getUsedSpace()");
        testCardinal(allocation.getAvailableSpace(), "getAvailableSpace()");
        testCardinal(allocation.getTotalFiles(), "getTotalFiles()");
        testCardinal(allocation.getUsedFiles(), "getUsedFiles()");
        testCardinal(allocation.getAvailableFiles(), "getAvailableFiles()");

        return;
    }
}
