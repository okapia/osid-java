//
// MutableMapProxyTrustLookupSession
//
//    Implements a Trust lookup service backed by a collection of
//    trusts that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.authentication.process;


/**
 *  Implements a Trust lookup service backed by a collection of
 *  trusts. The trusts are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of trusts can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyTrustLookupSession
    extends net.okapia.osid.jamocha.core.authentication.process.spi.AbstractMapTrustLookupSession
    implements org.osid.authentication.process.TrustLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyTrustLookupSession}
     *  with no trusts.
     *
     *  @param agency the agency
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code agency} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyTrustLookupSession(org.osid.authentication.Agency agency,
                                                  org.osid.proxy.Proxy proxy) {
        setAgency(agency);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyTrustLookupSession} with a
     *  single trust.
     *
     *  @param agency the agency
     *  @param trust a trust
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code agency},
     *          {@code trust}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyTrustLookupSession(org.osid.authentication.Agency agency,
                                                org.osid.authentication.process.Trust trust, org.osid.proxy.Proxy proxy) {
        this(agency, proxy);
        putTrust(trust);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyTrustLookupSession} using an
     *  array of trusts.
     *
     *  @param agency the agency
     *  @param trusts an array of trusts
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code agency},
     *          {@code trusts}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyTrustLookupSession(org.osid.authentication.Agency agency,
                                                org.osid.authentication.process.Trust[] trusts, org.osid.proxy.Proxy proxy) {
        this(agency, proxy);
        putTrusts(trusts);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyTrustLookupSession} using a
     *  collection of trusts.
     *
     *  @param agency the agency
     *  @param trusts a collection of trusts
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code agency},
     *          {@code trusts}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyTrustLookupSession(org.osid.authentication.Agency agency,
                                                java.util.Collection<? extends org.osid.authentication.process.Trust> trusts,
                                                org.osid.proxy.Proxy proxy) {
   
        this(agency, proxy);
        setSessionProxy(proxy);
        putTrusts(trusts);
        return;
    }

    
    /**
     *  Makes a {@code Trust} available in this session.
     *
     *  @param trust an trust
     *  @throws org.osid.NullArgumentException {@code trust{@code 
     *          is {@code null}
     */

    @Override
    public void putTrust(org.osid.authentication.process.Trust trust) {
        super.putTrust(trust);
        return;
    }


    /**
     *  Makes an array of trusts available in this session.
     *
     *  @param trusts an array of trusts
     *  @throws org.osid.NullArgumentException {@code trusts{@code 
     *          is {@code null}
     */

    @Override
    public void putTrusts(org.osid.authentication.process.Trust[] trusts) {
        super.putTrusts(trusts);
        return;
    }


    /**
     *  Makes collection of trusts available in this session.
     *
     *  @param trusts
     *  @throws org.osid.NullArgumentException {@code trust{@code 
     *          is {@code null}
     */

    @Override
    public void putTrusts(java.util.Collection<? extends org.osid.authentication.process.Trust> trusts) {
        super.putTrusts(trusts);
        return;
    }


    /**
     *  Removes a Trust from this session.
     *
     *  @param trustId the {@code Id} of the trust
     *  @throws org.osid.NullArgumentException {@code trustId{@code  is
     *          {@code null}
     */

    @Override
    public void removeTrust(org.osid.id.Id trustId) {
        super.removeTrust(trustId);
        return;
    }    
}
