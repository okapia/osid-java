//
// AbstractMapTodoProducerLookupSession
//
//    A simple framework for providing a TodoProducer lookup service
//    backed by a fixed collection of todo producers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.checklist.mason.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a TodoProducer lookup service backed by a
 *  fixed collection of todo producers. The todo producers are indexed
 *  only by <code>Id</code>. This class can be used for small
 *  collections or subclassed to provide additional indices for faster
 *  lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>TodoProducers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapTodoProducerLookupSession
    extends net.okapia.osid.jamocha.checklist.mason.spi.AbstractTodoProducerLookupSession
    implements org.osid.checklist.mason.TodoProducerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.checklist.mason.TodoProducer> todoProducers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.checklist.mason.TodoProducer>());


    /**
     *  Makes a <code>TodoProducer</code> available in this session.
     *
     *  @param  todoProducer a todo producer
     *  @throws org.osid.NullArgumentException <code>todoProducer<code>
     *          is <code>null</code>
     */

    protected void putTodoProducer(org.osid.checklist.mason.TodoProducer todoProducer) {
        this.todoProducers.put(todoProducer.getId(), todoProducer);
        return;
    }


    /**
     *  Makes an array of todo producers available in this session.
     *
     *  @param  todoProducers an array of todo producers
     *  @throws org.osid.NullArgumentException <code>todoProducers<code>
     *          is <code>null</code>
     */

    protected void putTodoProducers(org.osid.checklist.mason.TodoProducer[] todoProducers) {
        putTodoProducers(java.util.Arrays.asList(todoProducers));
        return;
    }


    /**
     *  Makes a collection of todo producers available in this session.
     *
     *  @param  todoProducers a collection of todo producers
     *  @throws org.osid.NullArgumentException <code>todoProducers<code>
     *          is <code>null</code>
     */

    protected void putTodoProducers(java.util.Collection<? extends org.osid.checklist.mason.TodoProducer> todoProducers) {
        for (org.osid.checklist.mason.TodoProducer todoProducer : todoProducers) {
            this.todoProducers.put(todoProducer.getId(), todoProducer);
        }

        return;
    }


    /**
     *  Removes a TodoProducer from this session.
     *
     *  @param  todoProducerId the <code>Id</code> of the todo producer
     *  @throws org.osid.NullArgumentException <code>todoProducerId<code> is
     *          <code>null</code>
     */

    protected void removeTodoProducer(org.osid.id.Id todoProducerId) {
        this.todoProducers.remove(todoProducerId);
        return;
    }


    /**
     *  Gets the <code>TodoProducer</code> specified by its <code>Id</code>.
     *
     *  @param  todoProducerId <code>Id</code> of the <code>TodoProducer</code>
     *  @return the todoProducer
     *  @throws org.osid.NotFoundException <code>todoProducerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>todoProducerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducer getTodoProducer(org.osid.id.Id todoProducerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.checklist.mason.TodoProducer todoProducer = this.todoProducers.get(todoProducerId);
        if (todoProducer == null) {
            throw new org.osid.NotFoundException("todoProducer not found: " + todoProducerId);
        }

        return (todoProducer);
    }


    /**
     *  Gets all <code>TodoProducers</code>. In plenary mode, the returned
     *  list contains all known todoProducers or an error
     *  results. Otherwise, the returned list may contain only those
     *  todoProducers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>TodoProducers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerList getTodoProducers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.checklist.mason.todoproducer.ArrayTodoProducerList(this.todoProducers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.todoProducers.clear();
        super.close();
        return;
    }
}
