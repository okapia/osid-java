//
// AbstractAdapterAddressLookupSession.java
//
//    An Address lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.contact.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An Address lookup session adapter.
 */

public abstract class AbstractAdapterAddressLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.contact.AddressLookupSession {

    private final org.osid.contact.AddressLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterAddressLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterAddressLookupSession(org.osid.contact.AddressLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code AddressBook/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code AddressBook Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAddressBookId() {
        return (this.session.getAddressBookId());
    }


    /**
     *  Gets the {@code AddressBook} associated with this session.
     *
     *  @return the {@code AddressBook} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressBook getAddressBook()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getAddressBook());
    }


    /**
     *  Tests if this user can perform {@code Address} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupAddresses() {
        return (this.session.canLookupAddresses());
    }


    /**
     *  A complete view of the {@code Address} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAddressView() {
        this.session.useComparativeAddressView();
        return;
    }


    /**
     *  A complete view of the {@code Address} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAddressView() {
        this.session.usePlenaryAddressView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include addresses in address books which are children
     *  of this address book in the address book hierarchy.
     */

    @OSID @Override
    public void useFederatedAddressBookView() {
        this.session.useFederatedAddressBookView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this address book only.
     */

    @OSID @Override
    public void useIsolatedAddressBookView() {
        this.session.useIsolatedAddressBookView();
        return;
    }
    
     
    /**
     *  Gets the {@code Address} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Address} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Address} and
     *  retained for compatibility.
     *
     *  @param addressId {@code Id} of the {@code Address}
     *  @return the address
     *  @throws org.osid.NotFoundException {@code addressId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code addressId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.Address getAddress(org.osid.id.Id addressId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAddress(addressId));
    }


    /**
     *  Gets an {@code AddressList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  addresses specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Addresses} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  addressIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Address} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code addressIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressList getAddressesByIds(org.osid.id.IdList addressIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAddressesByIds(addressIds));
    }


    /**
     *  Gets an {@code AddressList} corresponding to the given
     *  address genus {@code Type} which does not include
     *  addresses of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  addresses or an error results. Otherwise, the returned list
     *  may contain only those addresses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  addressGenusType an address genus type 
     *  @return the returned {@code Address} list
     *  @throws org.osid.NullArgumentException
     *          {@code addressGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressList getAddressesByGenusType(org.osid.type.Type addressGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAddressesByGenusType(addressGenusType));
    }


    /**
     *  Gets an {@code AddressList} corresponding to the given
     *  address genus {@code Type} and include any additional
     *  addresses with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  addresses or an error results. Otherwise, the returned list
     *  may contain only those addresses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  addressGenusType an address genus type 
     *  @return the returned {@code Address} list
     *  @throws org.osid.NullArgumentException
     *          {@code addressGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressList getAddressesByParentGenusType(org.osid.type.Type addressGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAddressesByParentGenusType(addressGenusType));
    }


    /**
     *  Gets an {@code AddressList} containing the given
     *  address record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  addresses or an error results. Otherwise, the returned list
     *  may contain only those addresses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  addressRecordType an address record type 
     *  @return the returned {@code Address} list
     *  @throws org.osid.NullArgumentException
     *          {@code addressRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressList getAddressesByRecordType(org.osid.type.Type addressRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAddressesByRecordType(addressRecordType));
    }


    /**
     *  Gets an {@code AddressList} for the given resource.  In
     *  plenary mode, the returned list contains all known addresses
     *  or an error results. Otherwise, the returned list may contain
     *  only those addresses that are accessible through this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Address} list 
     *  @throws org.osid.NullArgumentException {@code resourceId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.contact.AddressList getAddressesByResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.getAddressesByResource(resourceId));
    }


    /**
     *  Gets all {@code Addresses}. 
     *
     *  In plenary mode, the returned list contains all known
     *  addresses or an error results. Otherwise, the returned list
     *  may contain only those addresses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Addresses} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressList getAddresses()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAddresses());
    }
}
