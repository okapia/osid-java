//
// AbstractAssemblyEntryQuery.java
//
//     An EntryQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.blogging.entry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An EntryQuery that stores terms.
 */

public abstract class AbstractAssemblyEntryQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblySourceableOsidObjectQuery
    implements org.osid.blogging.EntryQuery,
               org.osid.blogging.EntryQueryInspector,
               org.osid.blogging.EntrySearchOrder {

    private final java.util.Collection<org.osid.blogging.records.EntryQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.blogging.records.EntryQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.blogging.records.EntrySearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyEntryQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyEntryQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches entries whose sent time is between the supplied range 
     *  inclusive. 
     *
     *  @param  startTime start time 
     *  @param  endTime end time 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> startTime </code> is 
     *          greater than <code> endTime </code> 
     *  @throws org.osid.NullArgumentException <code> startTime </code> or 
     *          <code> endTime </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchTimestamp(org.osid.calendaring.DateTime startTime, 
                               org.osid.calendaring.DateTime endTime, 
                               boolean match) {
        getAssembler().addDateTimeRangeTerm(getTimestampColumn(), startTime, endTime, match);
        return;
    }


    /**
     *  Clears the timestamp terms. 
     */

    @OSID @Override
    public void clearTimestampTerms() {
        getAssembler().clearTerms(getTimestampColumn());
        return;
    }


    /**
     *  Gets the timestamp terms. 
     *
     *  @return the timestamp terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeTerm[] getTimestampTerms() {
        return (getAssembler().getDateTimeTerms(getTimestampColumn()));
    }


    /**
     *  Specifies a preference for ordering entries by time. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTimestamp(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getTimestampColumn(), style);
        return;
    }


    /**
     *  Gets the Timestamp column name.
     *
     * @return the column name
     */

    protected String getTimestampColumn() {
        return ("timestamp");
    }


    /**
     *  Matches the poster of the entry. 
     *
     *  @param  resourceId <code> Id </code> of a <code> Resource </code> to 
     *          match 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPosterId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getPosterIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the poster <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearPosterIdTerms() {
        getAssembler().clearTerms(getPosterIdColumn());
        return;
    }


    /**
     *  Gets the poster <code> Id </code> terms. 
     *
     *  @return the resource <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPosterIdTerms() {
        return (getAssembler().getIdTerms(getPosterIdColumn()));
    }


    /**
     *  Specifies a preference for ordering entries by the poster. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPoster(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getPosterColumn(), style);
        return;
    }


    /**
     *  Gets the PosterId column name.
     *
     * @return the column name
     */

    protected String getPosterIdColumn() {
        return ("poster_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available for querying 
     *  posters. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPosterQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsPosterQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getPosterQuery() {
        throw new org.osid.UnimplementedException("supportsPosterQuery() is false");
    }


    /**
     *  Clears the poster terms. 
     */

    @OSID @Override
    public void clearPosterTerms() {
        getAssembler().clearTerms(getPosterColumn());
        return;
    }


    /**
     *  Gets the poster terms. 
     *
     *  @return the resource query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getPosterTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a resource order interface is available. 
     *
     *  @return <code> true </code> if a resource order interface is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPosterSearchOrder() {
        return (false);
    }


    /**
     *  Gets the poster resource search order interface. 
     *
     *  @return the resource search order interface 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getPosterSearchOrder() {
        throw new org.osid.UnimplementedException("supportsPosterSearchOrder() is false");
    }


    /**
     *  Gets the Poster column name.
     *
     * @return the column name
     */

    protected String getPosterColumn() {
        return ("poster");
    }


    /**
     *  Matches the posting agent of the entry. 
     *
     *  @param  agentId <code> Id </code> of an <code> Agent </code> to match 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPostingAgentId(org.osid.id.Id agentId, boolean match) {
        getAssembler().addIdTerm(getPostingAgentIdColumn(), agentId, match);
        return;
    }


    /**
     *  Clears the posting agent <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearPostingAgentIdTerms() {
        getAssembler().clearTerms(getPostingAgentIdColumn());
        return;
    }


    /**
     *  Gets the posting agent <code> Id </code> terms. 
     *
     *  @return the agent <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPostingAgentIdTerms() {
        return (getAssembler().getIdTerms(getPostingAgentIdColumn()));
    }


    /**
     *  Specifies a preference for ordering entries by the posting agent. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPostingAgent(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getPostingAgentColumn(), style);
        return;
    }


    /**
     *  Gets the PostingAgentId column name.
     *
     * @return the column name
     */

    protected String getPostingAgentIdColumn() {
        return ("posting_agent_id");
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available for querying 
     *  senders. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostingAgentQuery() {
        return (false);
    }


    /**
     *  Gets the query for an agent. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostingAgentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getPostingAgentQuery() {
        throw new org.osid.UnimplementedException("supportsPostingAgentQuery() is false");
    }


    /**
     *  Clears the posting agent terms. 
     */

    @OSID @Override
    public void clearPostingAgentTerms() {
        getAssembler().clearTerms(getPostingAgentColumn());
        return;
    }


    /**
     *  Gets the posting agent terms. 
     *
     *  @return the agent query terms 
     */

    @OSID @Override
    public org.osid.authentication.AgentQueryInspector[] getPostingAgentTerms() {
        return (new org.osid.authentication.AgentQueryInspector[0]);
    }


    /**
     *  Tests if an agent order interface is available. 
     *
     *  @return <code> true </code> if an agent order interface is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostingAgentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the posting agent order interface. 
     *
     *  @return the agent search order interface 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostingAgentSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getPostingAgentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsPostingAgentSearchOrder() is false");
    }


    /**
     *  Gets the PostingAgent column name.
     *
     * @return the column name
     */

    protected String getPostingAgentColumn() {
        return ("posting_agent");
    }


    /**
     *  Adds a subject line to match. Multiple subject line matches can be 
     *  added to perform a boolean <code> OR </code> among them. 
     *
     *  @param  subject subject line to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> subject is </code> 
     *          not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> subject </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchSubjectLine(String subject, 
                                 org.osid.type.Type stringMatchType, 
                                 boolean match) {
        getAssembler().addStringTerm(getSubjectLineColumn(), subject, stringMatchType, match);
        return;
    }


    /**
     *  Matches entries with any subject line. 
     *
     *  @param  match <code> true </code> to match entries with any subject 
     *          line, <code> false </code> to match entries with no subject 
     *          line 
     */

    @OSID @Override
    public void matchAnySubjectLine(boolean match) {
        getAssembler().addStringWildcardTerm(getSubjectLineColumn(), match);
        return;
    }


    /**
     *  Clears the subject line terms. 
     */

    @OSID @Override
    public void clearSubjectLineTerms() {
        getAssembler().clearTerms(getSubjectLineColumn());
        return;
    }


    /**
     *  Gets the subject line terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getSubjectLineTerms() {
        return (getAssembler().getStringTerms(getSubjectLineColumn()));
    }


    /**
     *  Specifies a preference for ordering entries by subject line. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySubjectLine(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getSubjectLineColumn(), style);
        return;
    }


    /**
     *  Gets the SubjectLine column name.
     *
     * @return the column name
     */

    protected String getSubjectLineColumn() {
        return ("subject_line");
    }


    /**
     *  Adds a summary to match. Multiple summaries matches can be added to 
     *  perform a boolean <code> OR </code> among them. 
     *
     *  @param  summary summary to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> summary is </code> 
     *          not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> summary </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchSummary(String summary, 
                             org.osid.type.Type stringMatchType, boolean match) {
        getAssembler().addStringTerm(getSummaryColumn(), summary, stringMatchType, match);
        return;
    }


    /**
     *  Matches entries with any summary. 
     *
     *  @param  match <code> true </code> to match entries with any summary, 
     *          <code> false </code> to match entries with no summary 
     */

    @OSID @Override
    public void matchAnySummary(boolean match) {
        getAssembler().addStringWildcardTerm(getSummaryColumn(), match);
        return;
    }


    /**
     *  Clears the summary terms. 
     */

    @OSID @Override
    public void clearSummaryTerms() {
        getAssembler().clearTerms(getSummaryColumn());
        return;
    }


    /**
     *  Gets the summary terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getSummaryTerms() {
        return (getAssembler().getStringTerms(getSummaryColumn()));
    }


    /**
     *  Specifies a preference for ordering entries by summary. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySummary(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getSummaryColumn(), style);
        return;
    }


    /**
     *  Gets the Summary column name.
     *
     * @return the column name
     */

    protected String getSummaryColumn() {
        return ("summary");
    }


    /**
     *  Adds text to match. Multiple text matches can be added to perform a 
     *  boolean <code> OR </code> among them. 
     *
     *  @param  text text to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> text is </code> not 
     *          of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> text </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchText(String text, org.osid.type.Type stringMatchType, 
                          boolean match) {
        getAssembler().addStringTerm(getTextColumn(), text, stringMatchType, match);
        return;
    }


    /**
     *  Matches entries with any text. 
     *
     *  @param  match <code> true </code> to match entries with any text, 
     *          <code> false </code> to match entries with no text 
     */

    @OSID @Override
    public void matchAnyText(boolean match) {
        getAssembler().addStringWildcardTerm(getTextColumn(), match);
        return;
    }


    /**
     *  Clears the text terms. 
     */

    @OSID @Override
    public void clearTextTerms() {
        getAssembler().clearTerms(getTextColumn());
        return;
    }


    /**
     *  Gets the text terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getTextTerms() {
        return (getAssembler().getStringTerms(getTextColumn()));
    }


    /**
     *  Specifies a preference for ordering entries by text. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByText(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getTextColumn(), style);
        return;
    }


    /**
     *  Gets the Text column name.
     *
     * @return the column name
     */

    protected String getTextColumn() {
        return ("text");
    }


    /**
     *  Sets the blog <code> Id </code> for this query. 
     *
     *  @param  blogId the blog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> blogId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchBlogId(org.osid.id.Id blogId, boolean match) {
        getAssembler().addIdTerm(getBlogIdColumn(), blogId, match);
        return;
    }


    /**
     *  Clears the blog <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBlogIdTerms() {
        getAssembler().clearTerms(getBlogIdColumn());
        return;
    }


    /**
     *  Gets the blog <code> Id </code> terms. 
     *
     *  @return the blog <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBlogIdTerms() {
        return (getAssembler().getIdTerms(getBlogIdColumn()));
    }


    /**
     *  Gets the BlogId column name.
     *
     * @return the column name
     */

    protected String getBlogIdColumn() {
        return ("blog_id");
    }


    /**
     *  Tests if a <code> BlogQuery </code> is available. 
     *
     *  @return <code> true </code> if a blog query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBlogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a blog. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the blog query 
     *  @throws org.osid.UnimplementedException <code> supportsBlogQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.BlogQuery getBlogQuery() {
        throw new org.osid.UnimplementedException("supportsBlogQuery() is false");
    }


    /**
     *  Clears the blog terms. 
     */

    @OSID @Override
    public void clearBlogTerms() {
        getAssembler().clearTerms(getBlogColumn());
        return;
    }


    /**
     *  Gets the blog terms. 
     *
     *  @return the blog terms 
     */

    @OSID @Override
    public org.osid.blogging.BlogQueryInspector[] getBlogTerms() {
        return (new org.osid.blogging.BlogQueryInspector[0]);
    }


    /**
     *  Gets the Blog column name.
     *
     * @return the column name
     */

    protected String getBlogColumn() {
        return ("blog");
    }


    /**
     *  Tests if this entry supports the given record
     *  <code>Type</code>.
     *
     *  @param  entryRecordType an entry record type 
     *  @return <code>true</code> if the entryRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>entryRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type entryRecordType) {
        for (org.osid.blogging.records.EntryQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(entryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  entryRecordType the entry record type 
     *  @return the entry query record 
     *  @throws org.osid.NullArgumentException
     *          <code>entryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(entryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.blogging.records.EntryQueryRecord getEntryQueryRecord(org.osid.type.Type entryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.blogging.records.EntryQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(entryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(entryRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  entryRecordType the entry record type 
     *  @return the entry query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>entryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(entryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.blogging.records.EntryQueryInspectorRecord getEntryQueryInspectorRecord(org.osid.type.Type entryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.blogging.records.EntryQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(entryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(entryRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param entryRecordType the entry record type
     *  @return the entry search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>entryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(entryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.blogging.records.EntrySearchOrderRecord getEntrySearchOrderRecord(org.osid.type.Type entryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.blogging.records.EntrySearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(entryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(entryRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this entry. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param entryQueryRecord the entry query record
     *  @param entryQueryInspectorRecord the entry query inspector
     *         record
     *  @param entrySearchOrderRecord the entry search order record
     *  @param entryRecordType entry record type
     *  @throws org.osid.NullArgumentException
     *          <code>entryQueryRecord</code>,
     *          <code>entryQueryInspectorRecord</code>,
     *          <code>entrySearchOrderRecord</code> or
     *          <code>entryRecordTypeentry</code> is
     *          <code>null</code>
     */
            
    protected void addEntryRecords(org.osid.blogging.records.EntryQueryRecord entryQueryRecord, 
                                      org.osid.blogging.records.EntryQueryInspectorRecord entryQueryInspectorRecord, 
                                      org.osid.blogging.records.EntrySearchOrderRecord entrySearchOrderRecord, 
                                      org.osid.type.Type entryRecordType) {

        addRecordType(entryRecordType);

        nullarg(entryQueryRecord, "entry query record");
        nullarg(entryQueryInspectorRecord, "entry query inspector record");
        nullarg(entrySearchOrderRecord, "entry search odrer record");

        this.queryRecords.add(entryQueryRecord);
        this.queryInspectorRecords.add(entryQueryInspectorRecord);
        this.searchOrderRecords.add(entrySearchOrderRecord);
        
        return;
    }
}
