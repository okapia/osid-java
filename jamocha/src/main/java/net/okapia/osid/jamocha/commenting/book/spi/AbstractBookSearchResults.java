//
// AbstractBookSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.commenting.book.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractBookSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.commenting.BookSearchResults {

    private org.osid.commenting.BookList books;
    private final org.osid.commenting.BookQueryInspector inspector;
    private final java.util.Collection<org.osid.commenting.records.BookSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractBookSearchResults.
     *
     *  @param books the result set
     *  @param bookQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>books</code>
     *          or <code>bookQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractBookSearchResults(org.osid.commenting.BookList books,
                                            org.osid.commenting.BookQueryInspector bookQueryInspector) {
        nullarg(books, "books");
        nullarg(bookQueryInspector, "book query inspectpr");

        this.books = books;
        this.inspector = bookQueryInspector;

        return;
    }


    /**
     *  Gets the book list resulting from a search.
     *
     *  @return a book list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.commenting.BookList getBooks() {
        if (this.books == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.commenting.BookList books = this.books;
        this.books = null;
	return (books);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.commenting.BookQueryInspector getBookQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  book search record <code> Type. </code> This method must
     *  be used to retrieve a book implementing the requested
     *  record.
     *
     *  @param bookSearchRecordType a book search 
     *         record type 
     *  @return the book search
     *  @throws org.osid.NullArgumentException
     *          <code>bookSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(bookSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.commenting.records.BookSearchResultsRecord getBookSearchResultsRecord(org.osid.type.Type bookSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.commenting.records.BookSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(bookSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(bookSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record book search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addBookRecord(org.osid.commenting.records.BookSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "book record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
