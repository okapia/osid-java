//
// AssessmentSectionFilterList.java
//
//     Implements a filtering AssessmentSectionList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.assessment.assessmentsection;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filtering AssessmentSectionList.
 */

public final class AssessmentSectionFilterList
    extends net.okapia.osid.jamocha.inline.filter.assessment.assessmentsection.spi.AbstractAssessmentSectionFilterList
    implements org.osid.assessment.AssessmentSectionList,
               AssessmentSectionFilter {

    private final AssessmentSectionFilter filter;


    /**
     *  Creates a new <code>AssessmentSectionFilterList</code>.
     *
     *  @param filter an inline query filter
     *  @param list an <code>AssessmentSectionList</code>
     *  @throws org.osid.NullArgumentException <code>filter</code> or
     *          <code>list</code> is <code>null</code>
     */

    public AssessmentSectionFilterList(AssessmentSectionFilter filter, org.osid.assessment.AssessmentSectionList list) {
        super(list);

        nullarg(filter, "inline query filter");
        this.filter = filter;

        return;
    }    

    
    /**
     *  Filters AssessmentSections.
     *
     *  @param assessmentSection the assessment section to filter
     *  @return <code>true</code> if the assessment section passes the filter,
     *          <code>false</code> if the assessment section should be filtered
     */

    @Override
    public boolean pass(org.osid.assessment.AssessmentSection assessmentSection) {
        return (this.filter.pass(assessmentSection));
    }
}
