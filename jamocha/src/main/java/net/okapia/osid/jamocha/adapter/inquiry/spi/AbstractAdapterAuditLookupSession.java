//
// AbstractAdapterAuditLookupSession.java
//
//    An Audit lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.inquiry.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An Audit lookup session adapter.
 */

public abstract class AbstractAdapterAuditLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.inquiry.AuditLookupSession {

    private final org.osid.inquiry.AuditLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterAuditLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterAuditLookupSession(org.osid.inquiry.AuditLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Inquest/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Inquest Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getInquestId() {
        return (this.session.getInquestId());
    }


    /**
     *  Gets the {@code Inquest} associated with this session.
     *
     *  @return the {@code Inquest} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.Inquest getInquest()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getInquest());
    }


    /**
     *  Tests if this user can perform {@code Audit} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupAudits() {
        return (this.session.canLookupAudits());
    }


    /**
     *  A complete view of the {@code Audit} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAuditView() {
        this.session.useComparativeAuditView();
        return;
    }


    /**
     *  A complete view of the {@code Audit} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAuditView() {
        this.session.usePlenaryAuditView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include audits in inquests which are children
     *  of this inquest in the inquest hierarchy.
     */

    @OSID @Override
    public void useFederatedInquestView() {
        this.session.useFederatedInquestView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this inquest only.
     */

    @OSID @Override
    public void useIsolatedInquestView() {
        this.session.useIsolatedInquestView();
        return;
    }
    

    /**
     *  Only active audits are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveAuditView() {
        this.session.useActiveAuditView();
        return;
    }


    /**
     *  Active and inactive audits are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusAuditView() {
        this.session.useAnyStatusAuditView();
        return;
    }
    
     
    /**
     *  Gets the {@code Audit} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Audit} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Audit} and
     *  retained for compatibility.
     *
     *  In active mode, audits are returned that are currently
     *  active. In any status mode, active and inactive audits
     *  are returned.
     *
     *  @param auditId {@code Id} of the {@code Audit}
     *  @return the audit
     *  @throws org.osid.NotFoundException {@code auditId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code auditId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.Audit getAudit(org.osid.id.Id auditId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAudit(auditId));
    }


    /**
     *  Gets an {@code AuditList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  audits specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Audits} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, audits are returned that are currently
     *  active. In any status mode, active and inactive audits
     *  are returned.
     *
     *  @param  auditIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Audit} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code auditIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.AuditList getAuditsByIds(org.osid.id.IdList auditIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuditsByIds(auditIds));
    }


    /**
     *  Gets an {@code AuditList} corresponding to the given
     *  audit genus {@code Type} which does not include
     *  audits of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  audits or an error results. Otherwise, the returned list
     *  may contain only those audits that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, audits are returned that are currently
     *  active. In any status mode, active and inactive audits
     *  are returned.
     *
     *  @param  auditGenusType an audit genus type 
     *  @return the returned {@code Audit} list
     *  @throws org.osid.NullArgumentException
     *          {@code auditGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.AuditList getAuditsByGenusType(org.osid.type.Type auditGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuditsByGenusType(auditGenusType));
    }


    /**
     *  Gets an {@code AuditList} corresponding to the given
     *  audit genus {@code Type} and include any additional
     *  audits with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  audits or an error results. Otherwise, the returned list
     *  may contain only those audits that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, audits are returned that are currently
     *  active. In any status mode, active and inactive audits
     *  are returned.
     *
     *  @param  auditGenusType an audit genus type 
     *  @return the returned {@code Audit} list
     *  @throws org.osid.NullArgumentException
     *          {@code auditGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.AuditList getAuditsByParentGenusType(org.osid.type.Type auditGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuditsByParentGenusType(auditGenusType));
    }


    /**
     *  Gets an {@code AuditList} containing the given
     *  audit record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  audits or an error results. Otherwise, the returned list
     *  may contain only those audits that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, audits are returned that are currently
     *  active. In any status mode, active and inactive audits
     *  are returned.
     *
     *  @param  auditRecordType an audit record type 
     *  @return the returned {@code Audit} list
     *  @throws org.osid.NullArgumentException
     *          {@code auditRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.AuditList getAuditsByRecordType(org.osid.type.Type auditRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuditsByRecordType(auditRecordType));
    }


    /**
     *  Gets an {@code AuditList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  audits or an error results. Otherwise, the returned list
     *  may contain only those audits that are accessible through
     *  this session.
     *
     *  In active mode, audits are returned that are currently
     *  active. In any status mode, active and inactive audits
     *  are returned.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Audit} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inquiry.AuditList getAuditsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuditsByProvider(resourceId));
    }


    /**
     *  Gets all {@code Audits}. 
     *
     *  In plenary mode, the returned list contains all known
     *  audits or an error results. Otherwise, the returned list
     *  may contain only those audits that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, audits are returned that are currently
     *  active. In any status mode, active and inactive audits
     *  are returned.
     *
     *  @return a list of {@code Audits} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.AuditList getAudits()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAudits());
    }
}
