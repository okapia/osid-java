//
// AbstractAuditEnablerSearch.java
//
//     A template for making an AuditEnabler Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inquiry.rules.auditenabler.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing audit enabler searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractAuditEnablerSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.inquiry.rules.AuditEnablerSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.inquiry.rules.records.AuditEnablerSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.inquiry.rules.AuditEnablerSearchOrder auditEnablerSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of audit enablers. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  auditEnablerIds list of audit enablers
     *  @throws org.osid.NullArgumentException
     *          <code>auditEnablerIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongAuditEnablers(org.osid.id.IdList auditEnablerIds) {
        while (auditEnablerIds.hasNext()) {
            try {
                this.ids.add(auditEnablerIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongAuditEnablers</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of audit enabler Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getAuditEnablerIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  auditEnablerSearchOrder audit enabler search order 
     *  @throws org.osid.NullArgumentException
     *          <code>auditEnablerSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>auditEnablerSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderAuditEnablerResults(org.osid.inquiry.rules.AuditEnablerSearchOrder auditEnablerSearchOrder) {
	this.auditEnablerSearchOrder = auditEnablerSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.inquiry.rules.AuditEnablerSearchOrder getAuditEnablerSearchOrder() {
	return (this.auditEnablerSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given audit enabler search
     *  record <code> Type. </code> This method must be used to
     *  retrieve an audit enabler implementing the requested record.
     *
     *  @param auditEnablerSearchRecordType an audit enabler search record
     *         type
     *  @return the audit enabler search record
     *  @throws org.osid.NullArgumentException
     *          <code>auditEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(auditEnablerSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.inquiry.rules.records.AuditEnablerSearchRecord getAuditEnablerSearchRecord(org.osid.type.Type auditEnablerSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.inquiry.rules.records.AuditEnablerSearchRecord record : this.records) {
            if (record.implementsRecordType(auditEnablerSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(auditEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this audit enabler search. 
     *
     *  @param auditEnablerSearchRecord audit enabler search record
     *  @param auditEnablerSearchRecordType auditEnabler search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAuditEnablerSearchRecord(org.osid.inquiry.rules.records.AuditEnablerSearchRecord auditEnablerSearchRecord, 
                                           org.osid.type.Type auditEnablerSearchRecordType) {

        addRecordType(auditEnablerSearchRecordType);
        this.records.add(auditEnablerSearchRecord);        
        return;
    }
}
