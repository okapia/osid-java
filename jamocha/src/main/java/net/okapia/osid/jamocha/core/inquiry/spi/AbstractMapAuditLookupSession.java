//
// AbstractMapAuditLookupSession
//
//    A simple framework for providing an Audit lookup service
//    backed by a fixed collection of audits.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.inquiry.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an Audit lookup service backed by a
 *  fixed collection of audits. The audits are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Audits</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapAuditLookupSession
    extends net.okapia.osid.jamocha.inquiry.spi.AbstractAuditLookupSession
    implements org.osid.inquiry.AuditLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.inquiry.Audit> audits = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.inquiry.Audit>());


    /**
     *  Makes an <code>Audit</code> available in this session.
     *
     *  @param  audit an audit
     *  @throws org.osid.NullArgumentException <code>audit<code>
     *          is <code>null</code>
     */

    protected void putAudit(org.osid.inquiry.Audit audit) {
        this.audits.put(audit.getId(), audit);
        return;
    }


    /**
     *  Makes an array of audits available in this session.
     *
     *  @param  audits an array of audits
     *  @throws org.osid.NullArgumentException <code>audits<code>
     *          is <code>null</code>
     */

    protected void putAudits(org.osid.inquiry.Audit[] audits) {
        putAudits(java.util.Arrays.asList(audits));
        return;
    }


    /**
     *  Makes a collection of audits available in this session.
     *
     *  @param  audits a collection of audits
     *  @throws org.osid.NullArgumentException <code>audits<code>
     *          is <code>null</code>
     */

    protected void putAudits(java.util.Collection<? extends org.osid.inquiry.Audit> audits) {
        for (org.osid.inquiry.Audit audit : audits) {
            this.audits.put(audit.getId(), audit);
        }

        return;
    }


    /**
     *  Removes an Audit from this session.
     *
     *  @param  auditId the <code>Id</code> of the audit
     *  @throws org.osid.NullArgumentException <code>auditId<code> is
     *          <code>null</code>
     */

    protected void removeAudit(org.osid.id.Id auditId) {
        this.audits.remove(auditId);
        return;
    }


    /**
     *  Gets the <code>Audit</code> specified by its <code>Id</code>.
     *
     *  @param  auditId <code>Id</code> of the <code>Audit</code>
     *  @return the audit
     *  @throws org.osid.NotFoundException <code>auditId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>auditId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.Audit getAudit(org.osid.id.Id auditId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.inquiry.Audit audit = this.audits.get(auditId);
        if (audit == null) {
            throw new org.osid.NotFoundException("audit not found: " + auditId);
        }

        return (audit);
    }


    /**
     *  Gets all <code>Audits</code>. In plenary mode, the returned
     *  list contains all known audits or an error
     *  results. Otherwise, the returned list may contain only those
     *  audits that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Audits</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.AuditList getAudits()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inquiry.audit.ArrayAuditList(this.audits.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.audits.clear();
        super.close();
        return;
    }
}
