//
// AbstractValueEnablerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.configuration.rules.valueenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractValueEnablerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.configuration.rules.ValueEnablerSearchResults {

    private org.osid.configuration.rules.ValueEnablerList valueEnablers;
    private final org.osid.configuration.rules.ValueEnablerQueryInspector inspector;
    private final java.util.Collection<org.osid.configuration.rules.records.ValueEnablerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractValueEnablerSearchResults.
     *
     *  @param valueEnablers the result set
     *  @param valueEnablerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>valueEnablers</code>
     *          or <code>valueEnablerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractValueEnablerSearchResults(org.osid.configuration.rules.ValueEnablerList valueEnablers,
                                            org.osid.configuration.rules.ValueEnablerQueryInspector valueEnablerQueryInspector) {
        nullarg(valueEnablers, "value enablers");
        nullarg(valueEnablerQueryInspector, "value enabler query inspectpr");

        this.valueEnablers = valueEnablers;
        this.inspector = valueEnablerQueryInspector;

        return;
    }


    /**
     *  Gets the value enabler list resulting from a search.
     *
     *  @return a value enabler list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerList getValueEnablers() {
        if (this.valueEnablers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.configuration.rules.ValueEnablerList valueEnablers = this.valueEnablers;
        this.valueEnablers = null;
	return (valueEnablers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.configuration.rules.ValueEnablerQueryInspector getValueEnablerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  value enabler search record <code> Type. </code> This method must
     *  be used to retrieve a valueEnabler implementing the requested
     *  record.
     *
     *  @param valueEnablerSearchRecordType a valueEnabler search 
     *         record type 
     *  @return the value enabler search
     *  @throws org.osid.NullArgumentException
     *          <code>valueEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(valueEnablerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.configuration.rules.records.ValueEnablerSearchResultsRecord getValueEnablerSearchResultsRecord(org.osid.type.Type valueEnablerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.configuration.rules.records.ValueEnablerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(valueEnablerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(valueEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record value enabler search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addValueEnablerRecord(org.osid.configuration.rules.records.ValueEnablerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "value enabler record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
