//
// AbstractBusinessBatchFormList
//
//     Implements a filter for a BusinessBatchFormList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.billing.batch.businessbatchform.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filter for a BusinessBatchFormList. Subclasses call this
 *  constructor and implement the <code>pass()</code> method. This
 *  filter is synchronous but can be wrapped in a BufferedBusinessBatchFormList
 *  to improve performance.
 */

public abstract class AbstractBusinessBatchFormFilterList
    extends net.okapia.osid.jamocha.billing.batch.businessbatchform.spi.AbstractBusinessBatchFormList
    implements org.osid.billing.batch.BusinessBatchFormList,
               net.okapia.osid.jamocha.inline.filter.billing.batch.businessbatchform.BusinessBatchFormFilter {

    private org.osid.billing.batch.BusinessBatchForm businessBatchForm;
    private final org.osid.billing.batch.BusinessBatchFormList list;
    private org.osid.OsidException error;


    /**
     *  Creates a new <code>AbstractBusinessBatchFormFilterList</code>.
     *
     *  @param businessBatchFormList a <code>BusinessBatchFormList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>businessBatchFormList</code> is <code>null</code>
     */

    protected AbstractBusinessBatchFormFilterList(org.osid.billing.batch.BusinessBatchFormList businessBatchFormList) {
        nullarg(businessBatchFormList, "business batch form list");
        this.list = businessBatchFormList;
        return;
    }    

        
    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in
     *          this list, <code> false </code> if the end of the list
     *          has been reached
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        if (hasError()) {
            return (true);
        }

        prime();

        if (this.businessBatchForm == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the next <code> BusinessBatchForm </code> in this list. 
     *
     *  @return the next <code> BusinessBatchForm </code> in this list. The <code> 
     *          hasNext() </code> method should be used to test that a next 
     *          <code> BusinessBatchForm </code> is available before calling this method. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.billing.batch.BusinessBatchForm getNextBusinessBatchForm()
        throws org.osid.OperationFailedException {

        if (hasError()) {
            throw new org.osid.OperationFailedException(this.error);
        }

        if (hasNext()) {
            org.osid.billing.batch.BusinessBatchForm businessBatchForm = this.businessBatchForm;
            this.businessBatchForm = null;
            return (businessBatchForm);
        } else {
            throw new org.osid.IllegalStateException("no more elements available in business batch form list");
        }
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.businessBatchForm = null;
        this.list.close();
        return;
    }

    
    /**
     *  Filters BusinessBatchForms.
     *
     *  @param businessBatchForm the business batch form to filter
     *  @return <code>true</code> if the business batch form passes the filter,
     *          <code>false</code> if the business batch form should be filtered
     */

    public abstract boolean pass(org.osid.billing.batch.BusinessBatchForm businessBatchForm);


    protected void prime() {
        if (this.businessBatchForm != null) {
            return;
        }

        org.osid.billing.batch.BusinessBatchForm businessBatchForm = null;

        while (this.list.hasNext()) {
            try {
                businessBatchForm = this.list.getNextBusinessBatchForm();
            } catch (org.osid.OsidException oe) {
                error(oe);
                return;
            }

            if (pass(businessBatchForm)) {
                this.businessBatchForm = businessBatchForm;
                return;
            }
        }

        return;
    }


    /**
     *  Set an error to be thrown on the next retrieval.
     *
     *  @param error
     *  @throws org.osid.IllegalStateException this list already closed
     */

    protected void error(org.osid.OsidException error) {
        this.error = error;
        return;
    }


    /**
     *  Tests if an error exists.
     *
     *  @return <code>true</code> if an error has occurred,
     *          <code>false</code> otherwise
     */

    protected boolean hasError() {
        if (this.error == null) {
            return (false);
        } else {
            return (true);
        }
    }
}
