//
// AbstractMapAwardLookupSession
//
//    A simple framework for providing an Award lookup service
//    backed by a fixed collection of awards.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.recognition.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an Award lookup service backed by a
 *  fixed collection of awards. The awards are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Awards</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapAwardLookupSession
    extends net.okapia.osid.jamocha.recognition.spi.AbstractAwardLookupSession
    implements org.osid.recognition.AwardLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.recognition.Award> awards = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.recognition.Award>());


    /**
     *  Makes an <code>Award</code> available in this session.
     *
     *  @param  award an award
     *  @throws org.osid.NullArgumentException <code>award<code>
     *          is <code>null</code>
     */

    protected void putAward(org.osid.recognition.Award award) {
        this.awards.put(award.getId(), award);
        return;
    }


    /**
     *  Makes an array of awards available in this session.
     *
     *  @param  awards an array of awards
     *  @throws org.osid.NullArgumentException <code>awards<code>
     *          is <code>null</code>
     */

    protected void putAwards(org.osid.recognition.Award[] awards) {
        putAwards(java.util.Arrays.asList(awards));
        return;
    }


    /**
     *  Makes a collection of awards available in this session.
     *
     *  @param  awards a collection of awards
     *  @throws org.osid.NullArgumentException <code>awards<code>
     *          is <code>null</code>
     */

    protected void putAwards(java.util.Collection<? extends org.osid.recognition.Award> awards) {
        for (org.osid.recognition.Award award : awards) {
            this.awards.put(award.getId(), award);
        }

        return;
    }


    /**
     *  Removes an Award from this session.
     *
     *  @param  awardId the <code>Id</code> of the award
     *  @throws org.osid.NullArgumentException <code>awardId<code> is
     *          <code>null</code>
     */

    protected void removeAward(org.osid.id.Id awardId) {
        this.awards.remove(awardId);
        return;
    }


    /**
     *  Gets the <code>Award</code> specified by its <code>Id</code>.
     *
     *  @param  awardId <code>Id</code> of the <code>Award</code>
     *  @return the award
     *  @throws org.osid.NotFoundException <code>awardId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>awardId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.Award getAward(org.osid.id.Id awardId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.recognition.Award award = this.awards.get(awardId);
        if (award == null) {
            throw new org.osid.NotFoundException("award not found: " + awardId);
        }

        return (award);
    }


    /**
     *  Gets all <code>Awards</code>. In plenary mode, the returned
     *  list contains all known awards or an error
     *  results. Otherwise, the returned list may contain only those
     *  awards that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Awards</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.AwardList getAwards()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.recognition.award.ArrayAwardList(this.awards.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.awards.clear();
        super.close();
        return;
    }
}
