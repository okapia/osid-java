//
// AbstractSiteQueryInspector.java
//
//     A template for making a SiteQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.installation.site.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for sites.
 */

public abstract class AbstractSiteQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.installation.SiteQueryInspector {

    private final java.util.Collection<org.osid.installation.records.SiteQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the installation <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getInstallationIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the installation query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.installation.InstallationQueryInspector[] getInstallationTerms() {
        return (new org.osid.installation.InstallationQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given site query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a site implementing the requested record.
     *
     *  @param siteRecordType a site record type
     *  @return the site query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>siteRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(siteRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.installation.records.SiteQueryInspectorRecord getSiteQueryInspectorRecord(org.osid.type.Type siteRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.installation.records.SiteQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(siteRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(siteRecordType + " is not supported");
    }


    /**
     *  Adds a record to this site query. 
     *
     *  @param siteQueryInspectorRecord site query inspector
     *         record
     *  @param siteRecordType site record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addSiteQueryInspectorRecord(org.osid.installation.records.SiteQueryInspectorRecord siteQueryInspectorRecord, 
                                                   org.osid.type.Type siteRecordType) {

        addRecordType(siteRecordType);
        nullarg(siteRecordType, "site record type");
        this.records.add(siteQueryInspectorRecord);        
        return;
    }
}
