//
// MutableMapProxyRaceConstrainerEnablerLookupSession
//
//    Implements a RaceConstrainerEnabler lookup service backed by a collection of
//    raceConstrainerEnablers that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.voting.rules;


/**
 *  Implements a RaceConstrainerEnabler lookup service backed by a collection of
 *  raceConstrainerEnablers. The raceConstrainerEnablers are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of race constrainer enablers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyRaceConstrainerEnablerLookupSession
    extends net.okapia.osid.jamocha.core.voting.rules.spi.AbstractMapRaceConstrainerEnablerLookupSession
    implements org.osid.voting.rules.RaceConstrainerEnablerLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyRaceConstrainerEnablerLookupSession}
     *  with no race constrainer enablers.
     *
     *  @param polls the polls
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code polls} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyRaceConstrainerEnablerLookupSession(org.osid.voting.Polls polls,
                                                  org.osid.proxy.Proxy proxy) {
        setPolls(polls);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyRaceConstrainerEnablerLookupSession} with a
     *  single race constrainer enabler.
     *
     *  @param polls the polls
     *  @param raceConstrainerEnabler a race constrainer enabler
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code polls},
     *          {@code raceConstrainerEnabler}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyRaceConstrainerEnablerLookupSession(org.osid.voting.Polls polls,
                                                org.osid.voting.rules.RaceConstrainerEnabler raceConstrainerEnabler, org.osid.proxy.Proxy proxy) {
        this(polls, proxy);
        putRaceConstrainerEnabler(raceConstrainerEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyRaceConstrainerEnablerLookupSession} using an
     *  array of race constrainer enablers.
     *
     *  @param polls the polls
     *  @param raceConstrainerEnablers an array of race constrainer enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code polls},
     *          {@code raceConstrainerEnablers}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyRaceConstrainerEnablerLookupSession(org.osid.voting.Polls polls,
                                                org.osid.voting.rules.RaceConstrainerEnabler[] raceConstrainerEnablers, org.osid.proxy.Proxy proxy) {
        this(polls, proxy);
        putRaceConstrainerEnablers(raceConstrainerEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyRaceConstrainerEnablerLookupSession} using a
     *  collection of race constrainer enablers.
     *
     *  @param polls the polls
     *  @param raceConstrainerEnablers a collection of race constrainer enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code polls},
     *          {@code raceConstrainerEnablers}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyRaceConstrainerEnablerLookupSession(org.osid.voting.Polls polls,
                                                java.util.Collection<? extends org.osid.voting.rules.RaceConstrainerEnabler> raceConstrainerEnablers,
                                                org.osid.proxy.Proxy proxy) {
   
        this(polls, proxy);
        setSessionProxy(proxy);
        putRaceConstrainerEnablers(raceConstrainerEnablers);
        return;
    }

    
    /**
     *  Makes a {@code RaceConstrainerEnabler} available in this session.
     *
     *  @param raceConstrainerEnabler an race constrainer enabler
     *  @throws org.osid.NullArgumentException {@code raceConstrainerEnabler{@code 
     *          is {@code null}
     */

    @Override
    public void putRaceConstrainerEnabler(org.osid.voting.rules.RaceConstrainerEnabler raceConstrainerEnabler) {
        super.putRaceConstrainerEnabler(raceConstrainerEnabler);
        return;
    }


    /**
     *  Makes an array of raceConstrainerEnablers available in this session.
     *
     *  @param raceConstrainerEnablers an array of race constrainer enablers
     *  @throws org.osid.NullArgumentException {@code raceConstrainerEnablers{@code 
     *          is {@code null}
     */

    @Override
    public void putRaceConstrainerEnablers(org.osid.voting.rules.RaceConstrainerEnabler[] raceConstrainerEnablers) {
        super.putRaceConstrainerEnablers(raceConstrainerEnablers);
        return;
    }


    /**
     *  Makes collection of race constrainer enablers available in this session.
     *
     *  @param raceConstrainerEnablers
     *  @throws org.osid.NullArgumentException {@code raceConstrainerEnabler{@code 
     *          is {@code null}
     */

    @Override
    public void putRaceConstrainerEnablers(java.util.Collection<? extends org.osid.voting.rules.RaceConstrainerEnabler> raceConstrainerEnablers) {
        super.putRaceConstrainerEnablers(raceConstrainerEnablers);
        return;
    }


    /**
     *  Removes a RaceConstrainerEnabler from this session.
     *
     *  @param raceConstrainerEnablerId the {@code Id} of the race constrainer enabler
     *  @throws org.osid.NullArgumentException {@code raceConstrainerEnablerId{@code  is
     *          {@code null}
     */

    @Override
    public void removeRaceConstrainerEnabler(org.osid.id.Id raceConstrainerEnablerId) {
        super.removeRaceConstrainerEnabler(raceConstrainerEnablerId);
        return;
    }    
}
