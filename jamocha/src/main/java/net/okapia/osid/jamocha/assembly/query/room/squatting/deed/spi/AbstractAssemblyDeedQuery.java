//
// AbstractAssemblyDeedQuery.java
//
//     A DeedQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.room.squatting.deed.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A DeedQuery that stores terms.
 */

public abstract class AbstractAssemblyDeedQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRelationshipQuery
    implements org.osid.room.squatting.DeedQuery,
               org.osid.room.squatting.DeedQueryInspector,
               org.osid.room.squatting.DeedSearchOrder {

    private final java.util.Collection<org.osid.room.squatting.records.DeedQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.room.squatting.records.DeedQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.room.squatting.records.DeedSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyDeedQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyDeedQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets a building <code> Id. </code> 
     *
     *  @param  buildingId a building <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> buildingId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBuildingId(org.osid.id.Id buildingId, boolean match) {
        getAssembler().addIdTerm(getBuildingIdColumn(), buildingId, match);
        return;
    }


    /**
     *  Clears the building <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBuildingIdTerms() {
        getAssembler().clearTerms(getBuildingIdColumn());
        return;
    }


    /**
     *  Gets the building <code> Id </code> terms. 
     *
     *  @return the building <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBuildingIdTerms() {
        return (getAssembler().getIdTerms(getBuildingIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the building. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByBuilding(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getBuildingColumn(), style);
        return;
    }


    /**
     *  Gets the BuildingId column name.
     *
     * @return the column name
     */

    protected String getBuildingIdColumn() {
        return ("building_id");
    }


    /**
     *  Tests if a <code> BuildingQuery </code> is available. 
     *
     *  @return <code> true </code> if a building query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBuildingQuery() {
        return (false);
    }


    /**
     *  Gets the query for a building query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the building query 
     *  @throws org.osid.UnimplementedException <code> supportsBuildingQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingQuery getBuildingQuery() {
        throw new org.osid.UnimplementedException("supportsBuildingQuery() is false");
    }


    /**
     *  Clears the building terms. 
     */

    @OSID @Override
    public void clearBuildingTerms() {
        getAssembler().clearTerms(getBuildingColumn());
        return;
    }


    /**
     *  Gets the building terms. 
     *
     *  @return the building terms 
     */

    @OSID @Override
    public org.osid.room.BuildingQueryInspector[] getBuildingTerms() {
        return (new org.osid.room.BuildingQueryInspector[0]);
    }


    /**
     *  Tests if a building search order is available. 
     *
     *  @return <code> true </code> if a building search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBuildingSearchOrder() {
        return (false);
    }


    /**
     *  Gets the building search order. 
     *
     *  @return the building search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBuildingSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingSearchOrder getBuildingSearchOrder() {
        throw new org.osid.UnimplementedException("supportsBuildingSearchOrder() is false");
    }


    /**
     *  Gets the Building column name.
     *
     * @return the column name
     */

    protected String getBuildingColumn() {
        return ("building");
    }


    /**
     *  Sets a resource <code> Id. </code> 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchOwnerId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getOwnerIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the owner resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearOwnerIdTerms() {
        getAssembler().clearTerms(getOwnerIdColumn());
        return;
    }


    /**
     *  Gets the owner resource <code> Id </code> terms. 
     *
     *  @return the owner <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOwnerIdTerms() {
        return (getAssembler().getIdTerms(getOwnerIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the owner. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByOwner(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getOwnerColumn(), style);
        return;
    }


    /**
     *  Gets the OwnerId column name.
     *
     * @return the column name
     */

    protected String getOwnerIdColumn() {
        return ("owner_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOwnerQuery() {
        return (false);
    }


    /**
     *  Gets the query for an owner resource query. Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsOwnerQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getOwnerQuery() {
        throw new org.osid.UnimplementedException("supportsOwnerQuery() is false");
    }


    /**
     *  Clears the owner terms. 
     */

    @OSID @Override
    public void clearOwnerTerms() {
        getAssembler().clearTerms(getOwnerColumn());
        return;
    }


    /**
     *  Gets the owner resource terms. 
     *
     *  @return the resource terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getOwnerTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if an owner resource search order is available. 
     *
     *  @return <code> true </code> if an owner search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOwnerSearchOrder() {
        return (false);
    }


    /**
     *  Gets the owner resource search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOwnerSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getOwnerSearchOrder() {
        throw new org.osid.UnimplementedException("supportsOwnerSearchOrder() is false");
    }


    /**
     *  Gets the Owner column name.
     *
     * @return the column name
     */

    protected String getOwnerColumn() {
        return ("owner");
    }


    /**
     *  Sets the deed <code> Id </code> for this query to match rooms assigned 
     *  to campuses. 
     *
     *  @param  campusId a campus <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCampusId(org.osid.id.Id campusId, boolean match) {
        getAssembler().addIdTerm(getCampusIdColumn(), campusId, match);
        return;
    }


    /**
     *  Clears the campus <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCampusIdTerms() {
        getAssembler().clearTerms(getCampusIdColumn());
        return;
    }


    /**
     *  Gets the campus <code> Id </code> terms. 
     *
     *  @return the campus <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCampusIdTerms() {
        return (getAssembler().getIdTerms(getCampusIdColumn()));
    }


    /**
     *  Gets the CampusId column name.
     *
     * @return the column name
     */

    protected String getCampusIdColumn() {
        return ("campus_id");
    }


    /**
     *  Tests if a <code> CampusQuery </code> is available. 
     *
     *  @return <code> true </code> if a campus query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCampusQuery() {
        return (false);
    }


    /**
     *  Gets the query for a campus query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the campus query 
     *  @throws org.osid.UnimplementedException <code> supportsCampusQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.CampusQuery getCampusQuery() {
        throw new org.osid.UnimplementedException("supportsCampusQuery() is false");
    }


    /**
     *  Clears the campus terms. 
     */

    @OSID @Override
    public void clearCampusTerms() {
        getAssembler().clearTerms(getCampusColumn());
        return;
    }


    /**
     *  Gets the campus terms. 
     *
     *  @return the campus terms 
     */

    @OSID @Override
    public org.osid.room.CampusQueryInspector[] getCampusTerms() {
        return (new org.osid.room.CampusQueryInspector[0]);
    }


    /**
     *  Gets the Campus column name.
     *
     * @return the column name
     */

    protected String getCampusColumn() {
        return ("campus");
    }


    /**
     *  Tests if this deed supports the given record
     *  <code>Type</code>.
     *
     *  @param  deedRecordType a deed record type 
     *  @return <code>true</code> if the deedRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>deedRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type deedRecordType) {
        for (org.osid.room.squatting.records.DeedQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(deedRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  deedRecordType the deed record type 
     *  @return the deed query record 
     *  @throws org.osid.NullArgumentException
     *          <code>deedRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(deedRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.room.squatting.records.DeedQueryRecord getDeedQueryRecord(org.osid.type.Type deedRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.room.squatting.records.DeedQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(deedRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(deedRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  deedRecordType the deed record type 
     *  @return the deed query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>deedRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(deedRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.room.squatting.records.DeedQueryInspectorRecord getDeedQueryInspectorRecord(org.osid.type.Type deedRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.room.squatting.records.DeedQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(deedRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(deedRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param deedRecordType the deed record type
     *  @return the deed search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>deedRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(deedRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.room.squatting.records.DeedSearchOrderRecord getDeedSearchOrderRecord(org.osid.type.Type deedRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.room.squatting.records.DeedSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(deedRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(deedRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this deed. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param deedQueryRecord the deed query record
     *  @param deedQueryInspectorRecord the deed query inspector
     *         record
     *  @param deedSearchOrderRecord the deed search order record
     *  @param deedRecordType deed record type
     *  @throws org.osid.NullArgumentException
     *          <code>deedQueryRecord</code>,
     *          <code>deedQueryInspectorRecord</code>,
     *          <code>deedSearchOrderRecord</code> or
     *          <code>deedRecordTypedeed</code> is
     *          <code>null</code>
     */
            
    protected void addDeedRecords(org.osid.room.squatting.records.DeedQueryRecord deedQueryRecord, 
                                      org.osid.room.squatting.records.DeedQueryInspectorRecord deedQueryInspectorRecord, 
                                      org.osid.room.squatting.records.DeedSearchOrderRecord deedSearchOrderRecord, 
                                      org.osid.type.Type deedRecordType) {

        addRecordType(deedRecordType);

        nullarg(deedQueryRecord, "deed query record");
        nullarg(deedQueryInspectorRecord, "deed query inspector record");
        nullarg(deedSearchOrderRecord, "deed search odrer record");

        this.queryRecords.add(deedQueryRecord);
        this.queryInspectorRecords.add(deedQueryInspectorRecord);
        this.searchOrderRecords.add(deedSearchOrderRecord);
        
        return;
    }
}
