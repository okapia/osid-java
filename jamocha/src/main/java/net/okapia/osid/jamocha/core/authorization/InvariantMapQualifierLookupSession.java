//
// InvariantMapQualifierLookupSession
//
//    Implements a Qualifier lookup service backed by a fixed collection of
//    qualifiers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.authorization;


/**
 *  Implements a Qualifier lookup service backed by a fixed
 *  collection of qualifiers. The qualifiers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapQualifierLookupSession
    extends net.okapia.osid.jamocha.core.authorization.spi.AbstractMapQualifierLookupSession
    implements org.osid.authorization.QualifierLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapQualifierLookupSession</code> with no
     *  qualifiers.
     *  
     *  @param vault the vault
     *  @throws org.osid.NullArgumnetException {@code vault} is
     *          {@code null}
     */

    public InvariantMapQualifierLookupSession(org.osid.authorization.Vault vault) {
        setVault(vault);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapQualifierLookupSession</code> with a single
     *  qualifier.
     *  
     *  @param vault the vault
     *  @param qualifier a single qualifier
     *  @throws org.osid.NullArgumentException {@code vault} or
     *          {@code qualifier} is <code>null</code>
     */

      public InvariantMapQualifierLookupSession(org.osid.authorization.Vault vault,
                                               org.osid.authorization.Qualifier qualifier) {
        this(vault);
        putQualifier(qualifier);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapQualifierLookupSession</code> using an array
     *  of qualifiers.
     *  
     *  @param vault the vault
     *  @param qualifiers an array of qualifiers
     *  @throws org.osid.NullArgumentException {@code vault} or
     *          {@code qualifiers} is <code>null</code>
     */

      public InvariantMapQualifierLookupSession(org.osid.authorization.Vault vault,
                                               org.osid.authorization.Qualifier[] qualifiers) {
        this(vault);
        putQualifiers(qualifiers);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapQualifierLookupSession</code> using a
     *  collection of qualifiers.
     *
     *  @param vault the vault
     *  @param qualifiers a collection of qualifiers
     *  @throws org.osid.NullArgumentException {@code vault} or
     *          {@code qualifiers} is <code>null</code>
     */

      public InvariantMapQualifierLookupSession(org.osid.authorization.Vault vault,
                                               java.util.Collection<? extends org.osid.authorization.Qualifier> qualifiers) {
        this(vault);
        putQualifiers(qualifiers);
        return;
    }
}
