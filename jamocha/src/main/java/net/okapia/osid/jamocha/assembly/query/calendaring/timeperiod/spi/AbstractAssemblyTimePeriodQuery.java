//
// AbstractAssemblyTimePeriodQuery.java
//
//     A TimePeriodQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.calendaring.timeperiod.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A TimePeriodQuery that stores terms.
 */

public abstract class AbstractAssemblyTimePeriodQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.calendaring.TimePeriodQuery,
               org.osid.calendaring.TimePeriodQueryInspector,
               org.osid.calendaring.TimePeriodSearchOrder {

    private final java.util.Collection<org.osid.calendaring.records.TimePeriodQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.calendaring.records.TimePeriodQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.calendaring.records.TimePeriodSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyTimePeriodQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyTimePeriodQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches the time period start time between the given range inclusive. 
     *
     *  @param  low low time range 
     *  @param  high high time range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     *  @throws org.osid.NullArgumentException <code> high </code> or <code> 
     *          low </code> is <code> zero </code> 
     */

    @OSID @Override
    public void matchStart(org.osid.calendaring.DateTime low, 
                           org.osid.calendaring.DateTime high, boolean match) {
        getAssembler().addDateTimeRangeTerm(getStartColumn(), low, high, match);
        return;
    }


    /**
     *  Matches a time period that has any start time assigned. 
     *
     *  @param  match <code> true </code> to match time periods with any start 
     *          time, <code> false </code> to match time periods with no start 
     *          time 
     */

    @OSID @Override
    public void matchAnyStart(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getStartColumn(), match);
        return;
    }


    /**
     *  Clears the time period start terms. 
     */

    @OSID @Override
    public void clearStartTerms() {
        getAssembler().clearTerms(getStartColumn());
        return;
    }


    /**
     *  Gets the start terms. 
     *
     *  @return the start terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeTerm[] getStartTerms() {
        return (getAssembler().getDateTimeTerms(getStartColumn()));
    }


    /**
     *  Specified a preference for ordering results by the start time. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByStart(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getStartColumn(), style);
        return;
    }


    /**
     *  Gets the Start column name.
     *
     * @return the column name
     */

    protected String getStartColumn() {
        return ("start");
    }


    /**
     *  Matches the time period end time between the given range inclusive. 
     *
     *  @param  low low time range 
     *  @param  high high time range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     *  @throws org.osid.NullArgumentException <code> high </code> or <code> 
     *          low </code> is <code> zero </code> 
     */

    @OSID @Override
    public void matchEnd(org.osid.calendaring.DateTime low, 
                         org.osid.calendaring.DateTime high, boolean match) {
        getAssembler().addDateTimeRangeTerm(getEndColumn(), low, high, match);
        return;
    }


    /**
     *  Matches a time period that has any end time assigned. 
     *
     *  @param  match <code> true </code> to match time periods with any end 
     *          time, <code> false </code> to match time periods with no end 
     *          time 
     */

    @OSID @Override
    public void matchAnyEnd(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getEndColumn(), match);
        return;
    }


    /**
     *  Clears the time period end terms. 
     */

    @OSID @Override
    public void clearEndTerms() {
        getAssembler().clearTerms(getEndColumn());
        return;
    }


    /**
     *  Gets the end terms. 
     *
     *  @return the end terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeTerm[] getEndTerms() {
        return (getAssembler().getDateTimeTerms(getEndColumn()));
    }


    /**
     *  Specified a preference for ordering results by the end time. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByEnd(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getEndColumn(), style);
        return;
    }


    /**
     *  Gets the End column name.
     *
     * @return the column name
     */

    protected String getEndColumn() {
        return ("end");
    }


    /**
     *  Matches time periods that include the given time. 
     *
     *  @param  time date 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchTime(org.osid.calendaring.DateTime time, boolean match) {
        getAssembler().addDateTimeTerm(getTimeColumn(), time, match);
        return;
    }


    /**
     *  Matches a time period that has any time assigned. 
     *
     *  @param  match <code> true </code> to match time periods with any time, 
     *          <code> false </code> to match time periods with no time 
     */

    @OSID @Override
    public void matchAnyTime(boolean match) {
        getAssembler().addDateTimeWildcardTerm(getTimeColumn(), match);
        return;
    }


    /**
     *  Clears the time terms. 
     */

    @OSID @Override
    public void clearTimeTerms() {
        getAssembler().clearTerms(getTimeColumn());
        return;
    }


    /**
     *  Gets the time terms. 
     *
     *  @return the time terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeTerm[] getTimeTerms() {
        return (getAssembler().getDateTimeTerms(getTimeColumn()));
    }


    /**
     *  Gets the Time column name.
     *
     * @return the column name
     */

    protected String getTimeColumn() {
        return ("time");
    }


    /**
     *  Matches time periods with start and end times between the given range 
     *  inclusive. 
     *
     *  @param  start start date 
     *  @param  end end date 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> end </code> is less 
     *          than <code> start </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> zero </code> 
     */

    @OSID @Override
    public void matchTimeInclusive(org.osid.calendaring.DateTime start, 
                                   org.osid.calendaring.DateTime end, 
                                   boolean match) {
        getAssembler().addDateTimeRangeTerm(getTimeInclusiveColumn(), start, end, match);
        return;
    }


    /**
     *  Clears the time inclusive terms. 
     */

    @OSID @Override
    public void clearTimeInclusiveTerms() {
        getAssembler().clearTerms(getTimeInclusiveColumn());
        return;
    }


    /**
     *  Gets the inclusive time terms. 
     *
     *  @return the time range terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getTimeInclusiveTerms() {
        return (getAssembler().getDateTimeRangeTerms(getTimeInclusiveColumn()));
    }


    /**
     *  Gets the TimeInclusive column name.
     *
     * @return the column name
     */

    protected String getTimeInclusiveColumn() {
        return ("time_inclusive");
    }


    /**
     *  Matches the time period duration between the given range inclusive. 
     *
     *  @param  low low duration range 
     *  @param  high high duration range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     *  @throws org.osid.NullArgumentException <code> high </code> or <code> 
     *          low </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchDuration(org.osid.calendaring.Duration low, 
                              org.osid.calendaring.Duration high, 
                              boolean match) {
        getAssembler().addDurationRangeTerm(getDurationColumn(), low, high, match);
        return;
    }


    /**
     *  Clears the duration terms. 
     */

    @OSID @Override
    public void clearDurationTerms() {
        getAssembler().clearTerms(getDurationColumn());
        return;
    }


    /**
     *  Gets the duration terms. 
     *
     *  @return the duration terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationTerm[] getDurationTerms() {
        return (getAssembler().getDurationTerms(getDurationColumn()));
    }


    /**
     *  Specified a preference for ordering results by the time period 
     *  duration. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDuration(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDurationColumn(), style);
        return;
    }


    /**
     *  Gets the Duration column name.
     *
     * @return the column name
     */

    protected String getDurationColumn() {
        return ("duration");
    }


    /**
     *  Sets the event <code> Id </code> for this query to match exceptions. 
     *
     *  @param  eventId an exception event <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> eventId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchExceptionId(org.osid.id.Id eventId, boolean match) {
        getAssembler().addIdTerm(getExceptionIdColumn(), eventId, match);
        return;
    }


    /**
     *  Clears the exception event <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearExceptionIdTerms() {
        getAssembler().clearTerms(getExceptionIdColumn());
        return;
    }


    /**
     *  Gets the exception event <code> Id </code> terms. 
     *
     *  @return the exception event <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getExceptionIdTerms() {
        return (getAssembler().getIdTerms(getExceptionIdColumn()));
    }


    /**
     *  Gets the ExceptionId column name.
     *
     * @return the column name
     */

    protected String getExceptionIdColumn() {
        return ("exception_id");
    }


    /**
     *  Tests if an <code> EventQuery </code> is available for querying 
     *  exception events. 
     *
     *  @return <code> true </code> if a exception query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsExceptionQuery() {
        return (false);
    }


    /**
     *  Gets the query for an exception event. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the event query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsExceptionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventQuery getExceptionQuery() {
        throw new org.osid.UnimplementedException("supportsExceptionQuery() is false");
    }


    /**
     *  Matches a time period that has any exception event assigned. 
     *
     *  @param  match <code> true </code> to match time periods with any 
     *          exception, <code> false </code> to match time periods with no 
     *          exception 
     */

    @OSID @Override
    public void matchAnyException(boolean match) {
        getAssembler().addIdWildcardTerm(getExceptionColumn(), match);
        return;
    }


    /**
     *  Clears the exception event terms. 
     */

    @OSID @Override
    public void clearExceptionTerms() {
        getAssembler().clearTerms(getExceptionColumn());
        return;
    }


    /**
     *  Gets the exception event terms. 
     *
     *  @return the exception event terms 
     */

    @OSID @Override
    public org.osid.calendaring.EventQueryInspector[] getExceptionTerms() {
        return (new org.osid.calendaring.EventQueryInspector[0]);
    }


    /**
     *  Gets the Exception column name.
     *
     * @return the column name
     */

    protected String getExceptionColumn() {
        return ("exception");
    }


    /**
     *  Sets the event <code> Id </code> for this query. 
     *
     *  @param  eventId an event or recurring event <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> eventId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchEventId(org.osid.id.Id eventId, boolean match) {
        getAssembler().addIdTerm(getEventIdColumn(), eventId, match);
        return;
    }


    /**
     *  Clears the event <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearEventIdTerms() {
        getAssembler().clearTerms(getEventIdColumn());
        return;
    }


    /**
     *  Gets the event <code> Id </code> terms. 
     *
     *  @return the event <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEventIdTerms() {
        return (getAssembler().getIdTerms(getEventIdColumn()));
    }


    /**
     *  Gets the EventId column name.
     *
     * @return the column name
     */

    protected String getEventIdColumn() {
        return ("event_id");
    }


    /**
     *  Tests if an <code> EventQuery </code> is available for querying 
     *  events. 
     *
     *  @return <code> true </code> if an event query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEventQuery() {
        return (false);
    }


    /**
     *  Gets the query for an event or recurring event. Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the event query 
     *  @throws org.osid.UnimplementedException <code> supportsEventQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventQuery getEventQuery() {
        throw new org.osid.UnimplementedException("supportsEventQuery() is false");
    }


    /**
     *  Matches a time period that has any event assigned. 
     *
     *  @param  match <code> true </code> to match time periods with any 
     *          event, <code> false </code> to match time periods with no 
     *          events 
     */

    @OSID @Override
    public void matchAnyEvent(boolean match) {
        getAssembler().addIdWildcardTerm(getEventColumn(), match);
        return;
    }


    /**
     *  Clears the event terms. 
     */

    @OSID @Override
    public void clearEventTerms() {
        getAssembler().clearTerms(getEventColumn());
        return;
    }


    /**
     *  Gets the event terms. 
     *
     *  @return the event terms 
     */

    @OSID @Override
    public org.osid.calendaring.EventQueryInspector[] getEventTerms() {
        return (new org.osid.calendaring.EventQueryInspector[0]);
    }


    /**
     *  Gets the Event column name.
     *
     * @return the column name
     */

    protected String getEventColumn() {
        return ("event");
    }


    /**
     *  Sets the calendar <code> Id </code> for this query. 
     *
     *  @param  calendarId a calendar <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCalendarId(org.osid.id.Id calendarId, boolean match) {
        getAssembler().addIdTerm(getCalendarIdColumn(), calendarId, match);
        return;
    }


    /**
     *  Clears the calendar <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCalendarIdTerms() {
        getAssembler().clearTerms(getCalendarIdColumn());
        return;
    }


    /**
     *  Gets the calendar <code> Id </code> terms. 
     *
     *  @return the calendar <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCalendarIdTerms() {
        return (getAssembler().getIdTerms(getCalendarIdColumn()));
    }


    /**
     *  Gets the CalendarId column name.
     *
     * @return the column name
     */

    protected String getCalendarIdColumn() {
        return ("calendar_id");
    }


    /**
     *  Tests if a <code> CalendarQuery </code> is available for querying 
     *  resources. 
     *
     *  @return <code> true </code> if a calendar query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendarQuery() {
        return (false);
    }


    /**
     *  Gets the query for a calendar. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the calendar query 
     *  @throws org.osid.UnimplementedException <code> supportsCalendarQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQuery getCalendarQuery() {
        throw new org.osid.UnimplementedException("supportsCalendarQuery() is false");
    }


    /**
     *  Clears the calendar terms. 
     */

    @OSID @Override
    public void clearCalendarTerms() {
        getAssembler().clearTerms(getCalendarColumn());
        return;
    }


    /**
     *  Gets the calendar terms. 
     *
     *  @return the calendar terms 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQueryInspector[] getCalendarTerms() {
        return (new org.osid.calendaring.CalendarQueryInspector[0]);
    }


    /**
     *  Gets the Calendar column name.
     *
     * @return the column name
     */

    protected String getCalendarColumn() {
        return ("calendar");
    }


    /**
     *  Tests if this timePeriod supports the given record
     *  <code>Type</code>.
     *
     *  @param  timePeriodRecordType a time period record type 
     *  @return <code>true</code> if the timePeriodRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>timePeriodRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type timePeriodRecordType) {
        for (org.osid.calendaring.records.TimePeriodQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(timePeriodRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  timePeriodRecordType the time period record type 
     *  @return the time period query record 
     *  @throws org.osid.NullArgumentException
     *          <code>timePeriodRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(timePeriodRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.TimePeriodQueryRecord getTimePeriodQueryRecord(org.osid.type.Type timePeriodRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.TimePeriodQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(timePeriodRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(timePeriodRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  timePeriodRecordType the time period record type 
     *  @return the time period query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>timePeriodRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(timePeriodRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.TimePeriodQueryInspectorRecord getTimePeriodQueryInspectorRecord(org.osid.type.Type timePeriodRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.TimePeriodQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(timePeriodRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(timePeriodRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param timePeriodRecordType the time period record type
     *  @return the time period search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>timePeriodRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(timePeriodRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.TimePeriodSearchOrderRecord getTimePeriodSearchOrderRecord(org.osid.type.Type timePeriodRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.TimePeriodSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(timePeriodRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(timePeriodRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this time period. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param timePeriodQueryRecord the time period query record
     *  @param timePeriodQueryInspectorRecord the time period query inspector
     *         record
     *  @param timePeriodSearchOrderRecord the time period search order record
     *  @param timePeriodRecordType time period record type
     *  @throws org.osid.NullArgumentException
     *          <code>timePeriodQueryRecord</code>,
     *          <code>timePeriodQueryInspectorRecord</code>,
     *          <code>timePeriodSearchOrderRecord</code> or
     *          <code>timePeriodRecordTypetimePeriod</code> is
     *          <code>null</code>
     */
            
    protected void addTimePeriodRecords(org.osid.calendaring.records.TimePeriodQueryRecord timePeriodQueryRecord, 
                                      org.osid.calendaring.records.TimePeriodQueryInspectorRecord timePeriodQueryInspectorRecord, 
                                      org.osid.calendaring.records.TimePeriodSearchOrderRecord timePeriodSearchOrderRecord, 
                                      org.osid.type.Type timePeriodRecordType) {

        addRecordType(timePeriodRecordType);

        nullarg(timePeriodQueryRecord, "time period query record");
        nullarg(timePeriodQueryInspectorRecord, "time period query inspector record");
        nullarg(timePeriodSearchOrderRecord, "time period search odrer record");

        this.queryRecords.add(timePeriodQueryRecord);
        this.queryInspectorRecords.add(timePeriodQueryInspectorRecord);
        this.searchOrderRecords.add(timePeriodSearchOrderRecord);
        
        return;
    }
}
