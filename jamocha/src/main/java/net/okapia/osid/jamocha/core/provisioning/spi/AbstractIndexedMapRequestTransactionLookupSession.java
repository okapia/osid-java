//
// AbstractIndexedMapRequestTransactionLookupSession.java
//
//    A simple framework for providing a RequestTransaction lookup service
//    backed by a fixed collection of request transactions with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a RequestTransaction lookup service backed by a
 *  fixed collection of request transactions. The request transactions are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some request transactions may be compatible
 *  with more types than are indicated through these request transaction
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>RequestTransactions</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapRequestTransactionLookupSession
    extends AbstractMapRequestTransactionLookupSession
    implements org.osid.provisioning.RequestTransactionLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.provisioning.RequestTransaction> requestTransactionsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.provisioning.RequestTransaction>());
    private final MultiMap<org.osid.type.Type, org.osid.provisioning.RequestTransaction> requestTransactionsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.provisioning.RequestTransaction>());


    /**
     *  Makes a <code>RequestTransaction</code> available in this session.
     *
     *  @param  requestTransaction a request transaction
     *  @throws org.osid.NullArgumentException <code>requestTransaction<code> is
     *          <code>null</code>
     */

    @Override
    protected void putRequestTransaction(org.osid.provisioning.RequestTransaction requestTransaction) {
        super.putRequestTransaction(requestTransaction);

        this.requestTransactionsByGenus.put(requestTransaction.getGenusType(), requestTransaction);
        
        try (org.osid.type.TypeList types = requestTransaction.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.requestTransactionsByRecord.put(types.getNextType(), requestTransaction);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a request transaction from this session.
     *
     *  @param requestTransactionId the <code>Id</code> of the request transaction
     *  @throws org.osid.NullArgumentException <code>requestTransactionId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeRequestTransaction(org.osid.id.Id requestTransactionId) {
        org.osid.provisioning.RequestTransaction requestTransaction;
        try {
            requestTransaction = getRequestTransaction(requestTransactionId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.requestTransactionsByGenus.remove(requestTransaction.getGenusType());

        try (org.osid.type.TypeList types = requestTransaction.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.requestTransactionsByRecord.remove(types.getNextType(), requestTransaction);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeRequestTransaction(requestTransactionId);
        return;
    }


    /**
     *  Gets a <code>RequestTransactionList</code> corresponding to the given
     *  request transaction genus <code>Type</code> which does not include
     *  request transactions of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known request transactions or an error results. Otherwise,
     *  the returned list may contain only those request transactions that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  requestTransactionGenusType a request transaction genus type 
     *  @return the returned <code>RequestTransaction</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>requestTransactionGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransactionList getRequestTransactionsByGenusType(org.osid.type.Type requestTransactionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.requesttransaction.ArrayRequestTransactionList(this.requestTransactionsByGenus.get(requestTransactionGenusType)));
    }


    /**
     *  Gets a <code>RequestTransactionList</code> containing the given
     *  request transaction record <code>Type</code>. In plenary mode, the
     *  returned list contains all known request transactions or an error
     *  results. Otherwise, the returned list may contain only those
     *  request transactions that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  requestTransactionRecordType a request transaction record type 
     *  @return the returned <code>requestTransaction</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>requestTransactionRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransactionList getRequestTransactionsByRecordType(org.osid.type.Type requestTransactionRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.requesttransaction.ArrayRequestTransactionList(this.requestTransactionsByRecord.get(requestTransactionRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.requestTransactionsByGenus.clear();
        this.requestTransactionsByRecord.clear();

        super.close();

        return;
    }
}
