//
// AbstractMapOrganizationLookupSession
//
//    A simple framework for providing an Organization lookup service
//    backed by a fixed collection of organizations.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.personnel.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an Organization lookup service backed by a
 *  fixed collection of organizations. The organizations are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Organizations</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapOrganizationLookupSession
    extends net.okapia.osid.jamocha.personnel.spi.AbstractOrganizationLookupSession
    implements org.osid.personnel.OrganizationLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.personnel.Organization> organizations = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.personnel.Organization>());


    /**
     *  Makes an <code>Organization</code> available in this session.
     *
     *  @param  organization an organization
     *  @throws org.osid.NullArgumentException <code>organization<code>
     *          is <code>null</code>
     */

    protected void putOrganization(org.osid.personnel.Organization organization) {
        this.organizations.put(organization.getId(), organization);
        return;
    }


    /**
     *  Makes an array of organizations available in this session.
     *
     *  @param  organizations an array of organizations
     *  @throws org.osid.NullArgumentException <code>organizations<code>
     *          is <code>null</code>
     */

    protected void putOrganizations(org.osid.personnel.Organization[] organizations) {
        putOrganizations(java.util.Arrays.asList(organizations));
        return;
    }


    /**
     *  Makes a collection of organizations available in this session.
     *
     *  @param  organizations a collection of organizations
     *  @throws org.osid.NullArgumentException <code>organizations<code>
     *          is <code>null</code>
     */

    protected void putOrganizations(java.util.Collection<? extends org.osid.personnel.Organization> organizations) {
        for (org.osid.personnel.Organization organization : organizations) {
            this.organizations.put(organization.getId(), organization);
        }

        return;
    }


    /**
     *  Removes an Organization from this session.
     *
     *  @param  organizationId the <code>Id</code> of the organization
     *  @throws org.osid.NullArgumentException <code>organizationId<code> is
     *          <code>null</code>
     */

    protected void removeOrganization(org.osid.id.Id organizationId) {
        this.organizations.remove(organizationId);
        return;
    }


    /**
     *  Gets the <code>Organization</code> specified by its <code>Id</code>.
     *
     *  @param  organizationId <code>Id</code> of the <code>Organization</code>
     *  @return the organization
     *  @throws org.osid.NotFoundException <code>organizationId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>organizationId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.Organization getOrganization(org.osid.id.Id organizationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.personnel.Organization organization = this.organizations.get(organizationId);
        if (organization == null) {
            throw new org.osid.NotFoundException("organization not found: " + organizationId);
        }

        return (organization);
    }


    /**
     *  Gets all <code>Organizations</code>. In plenary mode, the returned
     *  list contains all known organizations or an error
     *  results. Otherwise, the returned list may contain only those
     *  organizations that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Organizations</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationList getOrganizations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.personnel.organization.ArrayOrganizationList(this.organizations.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.organizations.clear();
        super.close();
        return;
    }
}
