//
// InvariantIndexedMapNodeLookupSession
//
//    Implements a Node lookup service backed by a fixed
//    collection of nodes indexed by their types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.topology;


/**
 *  Implements a Node lookup service backed by a fixed
 *  collection of nodes. The nodes are indexed by
 *  {@code Id}, genus and record types.
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some nodes may be compatible
 *  with more types than are indicated through these node
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 */

public final class InvariantIndexedMapNodeLookupSession
    extends net.okapia.osid.jamocha.core.topology.spi.AbstractIndexedMapNodeLookupSession
    implements org.osid.topology.NodeLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapNodeLookupSession} using an
     *  array of nodes.
     *
     *  @param graph the graph
     *  @param nodes an array of nodes
     *  @throws org.osid.NullArgumentException {@code graph},
     *          {@code nodes} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapNodeLookupSession(org.osid.topology.Graph graph,
                                                    org.osid.topology.Node[] nodes) {

        setGraph(graph);
        putNodes(nodes);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapNodeLookupSession} using a
     *  collection of nodes.
     *
     *  @param graph the graph
     *  @param nodes a collection of nodes
     *  @throws org.osid.NullArgumentException {@code graph},
     *          {@code nodes} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapNodeLookupSession(org.osid.topology.Graph graph,
                                                    java.util.Collection<? extends org.osid.topology.Node> nodes) {

        setGraph(graph);
        putNodes(nodes);
        return;
    }
}
