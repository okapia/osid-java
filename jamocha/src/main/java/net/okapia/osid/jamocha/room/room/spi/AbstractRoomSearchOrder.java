//
// AbstractRoomSearchOdrer.java
//
//     Defines a RoomSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.room.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code RoomSearchOrder}.
 */

public abstract class AbstractRoomSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractTemporalOsidObjectSearchOrder
    implements org.osid.room.RoomSearchOrder {

    private final java.util.Collection<org.osid.room.records.RoomSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specifies a preference for ordering the result set by the building. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByBuilding(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a building search order is available. 
     *
     *  @return <code> true </code> if a building search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBuildingSearchOrder() {
        return (false);
    }


    /**
     *  Gets the building search order. 
     *
     *  @return the building search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBuildingSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingSearchOrder getBuildingSearchOrder() {
        throw new org.osid.UnimplementedException("supportsBuildingSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the floor. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByFloor(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a floor order is available. 
     *
     *  @return <code> true </code> if a floor search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFloorSearchOrder() {
        return (false);
    }


    /**
     *  Gets the floor search order. 
     *
     *  @return the floor search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFloorSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.FloorSearchOrder getFloorSearchOrder() {
        throw new org.osid.UnimplementedException("supportsFloorSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the enclosing 
     *  room. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByEnclosingRoom(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a room search order is available. 
     *
     *  @return <code> true </code> if a room order is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEnclosingRoomSearchOrder() {
        return (false);
    }


    /**
     *  Gets the room search order. 
     *
     *  @return the enclosing room search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEnclosingRoomSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.room.RoomSearchOrder getEnclosingRoomSearchOrder() {
        throw new org.osid.UnimplementedException("supportsEnclosingRoomSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the room number. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRoomNumber(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the room code. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCode(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the room area. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByArea(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the room 
     *  occupancy limit. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByOccupancyLimit(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  roomRecordType a room record type 
     *  @return {@code true} if the roomRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code roomRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type roomRecordType) {
        for (org.osid.room.records.RoomSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(roomRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  roomRecordType the room record type 
     *  @return the room search order record
     *  @throws org.osid.NullArgumentException
     *          {@code roomRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(roomRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.room.records.RoomSearchOrderRecord getRoomSearchOrderRecord(org.osid.type.Type roomRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.room.records.RoomSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(roomRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(roomRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this room. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param roomRecord the room search odrer record
     *  @param roomRecordType room record type
     *  @throws org.osid.NullArgumentException
     *          {@code roomRecord} or
     *          {@code roomRecordTyperoom} is
     *          {@code null}
     */
            
    protected void addRoomRecord(org.osid.room.records.RoomSearchOrderRecord roomSearchOrderRecord, 
                                     org.osid.type.Type roomRecordType) {

        addRecordType(roomRecordType);
        this.records.add(roomSearchOrderRecord);
        
        return;
    }
}
