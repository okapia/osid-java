//
// AbstractAdapterValueEnablerLookupSession.java
//
//    A ValueEnabler lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.configuration.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A ValueEnabler lookup session adapter.
 */

public abstract class AbstractAdapterValueEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.configuration.rules.ValueEnablerLookupSession {

    private final org.osid.configuration.rules.ValueEnablerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterValueEnablerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterValueEnablerLookupSession(org.osid.configuration.rules.ValueEnablerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Configuration/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Configuration Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getConfigurationId() {
        return (this.session.getConfigurationId());
    }


    /**
     *  Gets the {@code Configuration} associated with this session.
     *
     *  @return the {@code Configuration} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.Configuration getConfiguration()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getConfiguration());
    }


    /**
     *  Tests if this user can perform {@code ValueEnabler} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupValueEnablers() {
        return (this.session.canLookupValueEnablers());
    }


    /**
     *  A complete view of the {@code ValueEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeValueEnablerView() {
        this.session.useComparativeValueEnablerView();
        return;
    }


    /**
     *  A complete view of the {@code ValueEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryValueEnablerView() {
        this.session.usePlenaryValueEnablerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include value enablers in configurations which are children
     *  of this configuration in the configuration hierarchy.
     */

    @OSID @Override
    public void useFederatedConfigurationView() {
        this.session.useFederatedConfigurationView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this configuration only.
     */

    @OSID @Override
    public void useIsolatedConfigurationView() {
        this.session.useIsolatedConfigurationView();
        return;
    }
    

    /**
     *  Only active value enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveValueEnablerView() {
        this.session.useActiveValueEnablerView();
        return;
    }


    /**
     *  Active and inactive value enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusValueEnablerView() {
        this.session.useAnyStatusValueEnablerView();
        return;
    }
    
     
    /**
     *  Gets the {@code ValueEnabler} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code ValueEnabler} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code ValueEnabler} and
     *  retained for compatibility.
     *
     *  In active mode, value enablers are returned that are currently
     *  active. In any status mode, active and inactive value enablers
     *  are returned.
     *
     *  @param valueEnablerId {@code Id} of the {@code ValueEnabler}
     *  @return the value enabler
     *  @throws org.osid.NotFoundException {@code valueEnablerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code valueEnablerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnabler getValueEnabler(org.osid.id.Id valueEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getValueEnabler(valueEnablerId));
    }


    /**
     *  Gets a {@code ValueEnablerList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  valueEnablers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code ValueEnablers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, value enablers are returned that are currently
     *  active. In any status mode, active and inactive value enablers
     *  are returned.
     *
     *  @param  valueEnablerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code ValueEnabler} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code valueEnablerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerList getValueEnablersByIds(org.osid.id.IdList valueEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getValueEnablersByIds(valueEnablerIds));
    }


    /**
     *  Gets a {@code ValueEnablerList} corresponding to the given
     *  value enabler genus {@code Type} which does not include
     *  value enablers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  value enablers or an error results. Otherwise, the returned list
     *  may contain only those value enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, value enablers are returned that are currently
     *  active. In any status mode, active and inactive value enablers
     *  are returned.
     *
     *  @param  valueEnablerGenusType a valueEnabler genus type 
     *  @return the returned {@code ValueEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code valueEnablerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerList getValueEnablersByGenusType(org.osid.type.Type valueEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getValueEnablersByGenusType(valueEnablerGenusType));
    }


    /**
     *  Gets a {@code ValueEnablerList} corresponding to the given
     *  value enabler genus {@code Type} and include any additional
     *  value enablers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  value enablers or an error results. Otherwise, the returned list
     *  may contain only those value enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, value enablers are returned that are currently
     *  active. In any status mode, active and inactive value enablers
     *  are returned.
     *
     *  @param  valueEnablerGenusType a valueEnabler genus type 
     *  @return the returned {@code ValueEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code valueEnablerGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerList getValueEnablersByParentGenusType(org.osid.type.Type valueEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getValueEnablersByParentGenusType(valueEnablerGenusType));
    }


    /**
     *  Gets a {@code ValueEnablerList} containing the given
     *  value enabler record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  value enablers or an error results. Otherwise, the returned list
     *  may contain only those value enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, value enablers are returned that are currently
     *  active. In any status mode, active and inactive value enablers
     *  are returned.
     *
     *  @param  valueEnablerRecordType a valueEnabler record type 
     *  @return the returned {@code ValueEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code valueEnablerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerList getValueEnablersByRecordType(org.osid.type.Type valueEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getValueEnablersByRecordType(valueEnablerRecordType));
    }


    /**
     *  Gets a {@code ValueEnablerList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  value enablers or an error results. Otherwise, the returned list
     *  may contain only those value enablers that are accessible
     *  through this session.
     *  
     *  In active mode, value enablers are returned that are currently
     *  active. In any status mode, active and inactive value enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code ValueEnabler} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerList getValueEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getValueEnablersOnDate(from, to));
    }
        

    /**
     *  Gets a {@code ValueEnablerList } which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  value enablers or an error results. Otherwise, the returned list
     *  may contain only those value enablers that are accessible
     *  through this session.
     *
     *  In active mode, value enablers are returned that are currently
     *  active. In any status mode, active and inactive value enablers
     *  are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned {@code ValueEnabler} list
     *  @throws org.osid.InvalidArgumentException {@code from} is
     *          greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code agent},
     *          {@code from}, or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerList getValueEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (this.session.getValueEnablersOnDateWithAgent(agentId, from, to));
    }


    /**
     *  Gets all {@code ValueEnablers}. 
     *
     *  In plenary mode, the returned list contains all known
     *  value enablers or an error results. Otherwise, the returned list
     *  may contain only those value enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, value enablers are returned that are currently
     *  active. In any status mode, active and inactive value enablers
     *  are returned.
     *
     *  @return a list of {@code ValueEnablers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerList getValueEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getValueEnablers());
    }
}
