//
// AbstractAdapterActivityLookupSession.java
//
//    An Activity lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.course.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An Activity lookup session adapter.
 */

public abstract class AbstractAdapterActivityLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.course.ActivityLookupSession {

    private final org.osid.course.ActivityLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterActivityLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterActivityLookupSession(org.osid.course.ActivityLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code CourseCatalog/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code CourseCatalog Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.session.getCourseCatalogId());
    }


    /**
     *  Gets the {@code CourseCatalog} associated with this session.
     *
     *  @return the {@code CourseCatalog} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCourseCatalog());
    }


    /**
     *  Tests if this user can perform {@code Activity} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupActivities() {
        return (this.session.canLookupActivities());
    }


    /**
     *  A complete view of the {@code Activity} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeActivityView() {
        this.session.useComparativeActivityView();
        return;
    }


    /**
     *  A complete view of the {@code Activity} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryActivityView() {
        this.session.usePlenaryActivityView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include activities in course catalogs which are
     *  children of this course catalog in the course catalog
     *  hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.session.useFederatedCourseCatalogView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.session.useIsolatedCourseCatalogView();
        return;
    }
    

    /**
     *  Only activities whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveActivityView() {
        this.session.useEffectiveActivityView();
        return;
    }
    

    /**
     *  All activities of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveActivityView() {
        this.session.useAnyEffectiveActivityView();
        return;
    }


    /**
     *  A normalized view uses a single <code> Activity </code> to
     *  represent a set of recurring activities.
     */

    @OSID @Override
    public void useNormalizedActivityView() {
        this.useNormalizedActivityView();
        return;
    }


    /**
     *  A denormalized view expands recurring activities into a series
     *  of activities.
     */

    @OSID @Override
    public void useDenormalizedActivityView() {
        this.useDenormalizedActivityView();
        return;
    }

     
    /**
     *  Gets the {@code Activity} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a {@code
     *  NOT_FOUND} results. Otherwise, the returned {@code Activity}
     *  may have a different {@code Id} than requested, such as the
     *  case where a duplicate {@code Id} was assigned to a {@code
     *  Activity} and retained for compatibility.
     *
     *  In effective mode, activities are returned that are currently
     *  effective.  In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  @param activityId {@code Id} of the {@code Activity}
     *  @return the activity
     *  @throws org.osid.NotFoundException {@code activityId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code activityId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.Activity getActivity(org.osid.id.Id activityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActivity(activityId));
    }


    /**
     *  Gets an {@code ActivityList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  activities specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Activities} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, activities are returned that are currently
     *  effective.  In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  @param  activityIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Activity} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code activityIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.ActivityList getActivitiesByIds(org.osid.id.IdList activityIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActivitiesByIds(activityIds));
    }


    /**
     *  Gets an {@code ActivityList} corresponding to the given
     *  activity genus {@code Type} which does not include
     *  activities of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, activities are returned that are currently
     *  effective.  In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  @param  activityGenusType an activity genus type 
     *  @return the returned {@code Activity} list
     *  @throws org.osid.NullArgumentException
     *          {@code activityGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.ActivityList getActivitiesByGenusType(org.osid.type.Type activityGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActivitiesByGenusType(activityGenusType));
    }


    /**
     *  Gets an {@code ActivityList} corresponding to the given
     *  activity genus {@code Type} and include any additional
     *  activities with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, activities are returned that are currently
     *  effective.  In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  @param  activityGenusType an activity genus type 
     *  @return the returned {@code Activity} list
     *  @throws org.osid.NullArgumentException
     *          {@code activityGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.ActivityList getActivitiesByParentGenusType(org.osid.type.Type activityGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActivitiesByParentGenusType(activityGenusType));
    }


    /**
     *  Gets an {@code ActivityList} containing the given
     *  activity record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, activities are returned that are currently
     *  effective.  In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  @param  activityRecordType an activity record type 
     *  @return the returned {@code Activity} list
     *  @throws org.osid.NullArgumentException
     *          {@code activityRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.ActivityList getActivitiesByRecordType(org.osid.type.Type activityRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActivitiesByRecordType(activityRecordType));
    }


    /**
     *  Gets an {@code ActivityList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible
     *  through this session.
     *  
     *  In active mode, activities are returned that are currently
     *  active. In any status mode, active and inactive activities
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Activity} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.ActivityList getActivitiesOnDate(org.osid.calendaring.DateTime from, 
                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActivitiesOnDate(from, to));
    }
        

    /**
     *  Gets a list of activities corresponding to a activity unit
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session.
     *
     *  In effective mode, activities are returned that are
     *  currently effective.  In any effective mode, effective
     *  activities and those currently expired are returned.
     *
     *  @param  activityUnitId the {@code Id} of the activity unit
     *  @return the returned {@code ActivityList}
     *  @throws org.osid.NullArgumentException {@code activityUnitId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.ActivityList getActivitiesForActivityUnit(org.osid.id.Id activityUnitId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActivitiesForActivityUnit(activityUnitId));
    }


    /**
     *  Gets a list of activities corresponding to a activity unit
     *  {@code Id} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible
     *  through this session.
     *
     *  In effective mode, activities are returned that are
     *  currently effective.  In any effective mode, effective
     *  activities and those currently expired are returned.
     *
     *  @param  activityUnitId the {@code Id} of the activity unit
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code ActivityList}
     *  @throws org.osid.NullArgumentException {@code activityUnitId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.ActivityList getActivitiesForActivityUnitOnDate(org.osid.id.Id activityUnitId,
                                                                           org.osid.calendaring.DateTime from,
                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActivitiesForActivityUnitOnDate(activityUnitId, from, to));
    }


    /**
     *  Gets a list of activities corresponding to a term
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible
     *  through this session.
     *
     *  In effective mode, activities are returned that are
     *  currently effective.  In any effective mode, effective
     *  activities and those currently expired are returned.
     *
     *  @param  termId the {@code Id} of the term
     *  @return the returned {@code ActivityList}
     *  @throws org.osid.NullArgumentException {@code termId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.ActivityList getActivitiesForTerm(org.osid.id.Id termId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActivitiesForTerm(termId));
    }


    /**
     *  Gets a list of activities corresponding to a term
     *  {@code Id} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible
     *  through this session.
     *
     *  In effective mode, activities are returned that are
     *  currently effective.  In any effective mode, effective
     *  activities and those currently expired are returned.
     *
     *  @param  termId the {@code Id} of the term
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code ActivityList}
     *  @throws org.osid.NullArgumentException {@code termId}, {@code
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.ActivityList getActivitiesForTermOnDate(org.osid.id.Id termId,
                                                                      org.osid.calendaring.DateTime from,
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActivitiesForTermOnDate(termId, from, to));
    }


    /**
     *  Gets a list of activities corresponding to activity unit and term
     *  {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible
     *  through this session.
     *
     *  In effective mode, activities are returned that are
     *  currently effective.  In any effective mode, effective
     *  activities and those currently expired are returned.
     *
     *  @param  activityUnitId the {@code Id} of the activity unit
     *  @param  termId the {@code Id} of the term
     *  @return the returned {@code ActivityList}
     *  @throws org.osid.NullArgumentException {@code activityUnitId},
     *          {@code termId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.ActivityList getActivitiesForActivityUnitAndTerm(org.osid.id.Id activityUnitId,
                                                                            org.osid.id.Id termId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActivitiesForActivityUnitAndTerm(activityUnitId, termId));
    }


    /**
     *  Gets a list of activities corresponding to activity unit and
     *  term {@code Ids} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible
     *  through this session.
     *
     *  In effective mode, activities are returned that are currently
     *  effective. In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  @param  termId the {@code Id} of the term
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code ActivityList}
     *  @throws org.osid.NullArgumentException {@code activityUnitId},
     *          {@code termId}, {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.ActivityList getActivitiesForActivityUnitAndTermOnDate(org.osid.id.Id activityUnitId,
                                                                                  org.osid.id.Id termId,
                                                                                  org.osid.calendaring.DateTime from,
                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActivitiesForActivityUnitAndTermOnDate(activityUnitId, termId, from, to));
    }


    /**
     *  Gets all {@code Activities} associated with a given {@code 
     *  CourseOffering.} 
     *  
     *  In plenary mode, the returned list contains all known activities or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  activities that are accessible through this session. 
     *  
     *  In effective mode, activities are returned where the current date 
     *  falls within the effective dates inclusive. In any effective mode, 
     *  effective and expired activities are returned. 
     *
     *  @param  courseOfferingId a course {@code Id} 
     *  @return the returned {@code Activity} list 
     *  @throws org.osid.NullArgumentException {@code courseOfferingId} 
     *          is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.ActivityList getActivitiesForCourseOffering(org.osid.id.Id courseOfferingId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {


        return (this.session.getActivitiesForCourseOffering(courseOfferingId));
    }


    /**
     *  Gets all {@code Activities}. 
     *
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, activities are returned that are currently
     *  effective.  In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  @return a list of {@code Activities} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.ActivityList getActivities()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActivities());
    }
}
