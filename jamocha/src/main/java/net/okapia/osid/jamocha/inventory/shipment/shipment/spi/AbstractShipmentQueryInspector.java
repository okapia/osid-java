//
// AbstractShipmentQueryInspector.java
//
//     A template for making a ShipmentQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inventory.shipment.shipment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for shipments.
 */

public abstract class AbstractShipmentQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.inventory.shipment.ShipmentQueryInspector {

    private final java.util.Collection<org.osid.inventory.shipment.records.ShipmentQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the resource <code> Id </code> query terms. 
     *
     *  @return the resource <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSourceIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the resource query terms. 
     *
     *  @return the resource query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getSourceTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the order <code> Id </code> query terms. 
     *
     *  @return the order <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOrderIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the order query terms. 
     *
     *  @return the order query terms 
     */

    @OSID @Override
    public org.osid.ordering.OrderQueryInspector[] getOrderTerms() {
        return (new org.osid.ordering.OrderQueryInspector[0]);
    }


    /**
     *  Gets the date terms. 
     *
     *  @return the date terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getDateTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the entry <code> Id </code> query terms. 
     *
     *  @return the entry <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEntryIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the entry query terms. 
     *
     *  @return the entry query terms 
     */

    @OSID @Override
    public org.osid.inventory.shipment.EntryQueryInspector[] getEntryTerms() {
        return (new org.osid.inventory.shipment.EntryQueryInspector[0]);
    }


    /**
     *  Gets the warehouse <code> Id </code> query terms. 
     *
     *  @return the warehouse <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getWarehouseIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the warehouse query terms. 
     *
     *  @return the warehouse query terms 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseQueryInspector[] getWarehouseTerms() {
        return (new org.osid.inventory.WarehouseQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given shipment query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a shipment implementing the requested record.
     *
     *  @param shipmentRecordType a shipment record type
     *  @return the shipment query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>shipmentRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(shipmentRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.inventory.shipment.records.ShipmentQueryInspectorRecord getShipmentQueryInspectorRecord(org.osid.type.Type shipmentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inventory.shipment.records.ShipmentQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(shipmentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(shipmentRecordType + " is not supported");
    }


    /**
     *  Adds a record to this shipment query. 
     *
     *  @param shipmentQueryInspectorRecord shipment query inspector
     *         record
     *  @param shipmentRecordType shipment record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addShipmentQueryInspectorRecord(org.osid.inventory.shipment.records.ShipmentQueryInspectorRecord shipmentQueryInspectorRecord, 
                                                   org.osid.type.Type shipmentRecordType) {

        addRecordType(shipmentRecordType);
        nullarg(shipmentRecordType, "shipment record type");
        this.records.add(shipmentQueryInspectorRecord);        
        return;
    }
}
