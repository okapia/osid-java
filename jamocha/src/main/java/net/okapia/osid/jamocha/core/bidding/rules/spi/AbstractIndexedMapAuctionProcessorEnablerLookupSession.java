//
// AbstractIndexedMapAuctionProcessorEnablerLookupSession.java
//
//    A simple framework for providing an AuctionProcessorEnabler lookup service
//    backed by a fixed collection of auction processor enablers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.bidding.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an AuctionProcessorEnabler lookup service backed by a
 *  fixed collection of auction processor enablers. The auction processor enablers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some auction processor enablers may be compatible
 *  with more types than are indicated through these auction processor enabler
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>AuctionProcessorEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapAuctionProcessorEnablerLookupSession
    extends AbstractMapAuctionProcessorEnablerLookupSession
    implements org.osid.bidding.rules.AuctionProcessorEnablerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.bidding.rules.AuctionProcessorEnabler> auctionProcessorEnablersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.bidding.rules.AuctionProcessorEnabler>());
    private final MultiMap<org.osid.type.Type, org.osid.bidding.rules.AuctionProcessorEnabler> auctionProcessorEnablersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.bidding.rules.AuctionProcessorEnabler>());


    /**
     *  Makes an <code>AuctionProcessorEnabler</code> available in this session.
     *
     *  @param  auctionProcessorEnabler an auction processor enabler
     *  @throws org.osid.NullArgumentException <code>auctionProcessorEnabler<code> is
     *          <code>null</code>
     */

    @Override
    protected void putAuctionProcessorEnabler(org.osid.bidding.rules.AuctionProcessorEnabler auctionProcessorEnabler) {
        super.putAuctionProcessorEnabler(auctionProcessorEnabler);

        this.auctionProcessorEnablersByGenus.put(auctionProcessorEnabler.getGenusType(), auctionProcessorEnabler);
        
        try (org.osid.type.TypeList types = auctionProcessorEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.auctionProcessorEnablersByRecord.put(types.getNextType(), auctionProcessorEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an auction processor enabler from this session.
     *
     *  @param auctionProcessorEnablerId the <code>Id</code> of the auction processor enabler
     *  @throws org.osid.NullArgumentException <code>auctionProcessorEnablerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeAuctionProcessorEnabler(org.osid.id.Id auctionProcessorEnablerId) {
        org.osid.bidding.rules.AuctionProcessorEnabler auctionProcessorEnabler;
        try {
            auctionProcessorEnabler = getAuctionProcessorEnabler(auctionProcessorEnablerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.auctionProcessorEnablersByGenus.remove(auctionProcessorEnabler.getGenusType());

        try (org.osid.type.TypeList types = auctionProcessorEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.auctionProcessorEnablersByRecord.remove(types.getNextType(), auctionProcessorEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeAuctionProcessorEnabler(auctionProcessorEnablerId);
        return;
    }


    /**
     *  Gets an <code>AuctionProcessorEnablerList</code> corresponding to the given
     *  auction processor enabler genus <code>Type</code> which does not include
     *  auction processor enablers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known auction processor enablers or an error results. Otherwise,
     *  the returned list may contain only those auction processor enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  auctionProcessorEnablerGenusType an auction processor enabler genus type 
     *  @return the returned <code>AuctionProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionProcessorEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerList getAuctionProcessorEnablersByGenusType(org.osid.type.Type auctionProcessorEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.bidding.rules.auctionprocessorenabler.ArrayAuctionProcessorEnablerList(this.auctionProcessorEnablersByGenus.get(auctionProcessorEnablerGenusType)));
    }


    /**
     *  Gets an <code>AuctionProcessorEnablerList</code> containing the given
     *  auction processor enabler record <code>Type</code>. In plenary mode, the
     *  returned list contains all known auction processor enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  auction processor enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  auctionProcessorEnablerRecordType an auction processor enabler record type 
     *  @return the returned <code>auctionProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionProcessorEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerList getAuctionProcessorEnablersByRecordType(org.osid.type.Type auctionProcessorEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.bidding.rules.auctionprocessorenabler.ArrayAuctionProcessorEnablerList(this.auctionProcessorEnablersByRecord.get(auctionProcessorEnablerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.auctionProcessorEnablersByGenus.clear();
        this.auctionProcessorEnablersByRecord.clear();

        super.close();

        return;
    }
}
