//
// AbstractAcknowledgementManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.acknowledgement.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractAcknowledgementManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.acknowledgement.AcknowledgementManager,
               org.osid.acknowledgement.AcknowledgementProxyManager {

    private final Types creditRecordTypes                  = new TypeRefSet();
    private final Types creditSearchRecordTypes            = new TypeRefSet();

    private final Types billingRecordTypes                 = new TypeRefSet();
    private final Types billingSearchRecordTypes           = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractAcknowledgementManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractAcknowledgementManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any billing federation is exposed. Federation is exposed when 
     *  a specific billing may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  billings appears as a single billing. 
     *
     *  @return <code> true </code> if visible federation is supproted, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests for the availability of a credit lookup service. 
     *
     *  @return <code> true </code> if credit lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCreditLookup() {
        return (false);
    }


    /**
     *  Tests for the availability of a credit query service. 
     *
     *  @return <code> true </code> if credit query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCreditQuery() {
        return (false);
    }


    /**
     *  Tests if searching for credits is available. 
     *
     *  @return <code> true </code> if credit search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCreditSearch() {
        return (false);
    }


    /**
     *  Tests if managing for credits is available. 
     *
     *  @return <code> true </code> if a credit adminstrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCreditAdmin() {
        return (false);
    }


    /**
     *  Tests if credit notification is available. 
     *
     *  @return <code> true </code> if credit notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCreditNotification() {
        return (false);
    }


    /**
     *  Tests if a credit to billing lookup session is available. 
     *
     *  @return <code> true </code> if credit billing lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCreditBilling() {
        return (false);
    }


    /**
     *  Tests if a credit to billing assignment session is available. 
     *
     *  @return <code> true </code> if credit billing assignment is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCreditBillingAssignment() {
        return (false);
    }


    /**
     *  Tests if a credit smart billing session is available. 
     *
     *  @return <code> true </code> if credit smart billing is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCreditSmartBilling() {
        return (false);
    }


    /**
     *  Tests for the availability of an billing lookup service. 
     *
     *  @return <code> true </code> if billing lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBillingLookup() {
        return (false);
    }


    /**
     *  Tests if querying billings is available. 
     *
     *  @return <code> true </code> if billing query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBillingQuery() {
        return (false);
    }


    /**
     *  Tests if searching for billings is available. 
     *
     *  @return <code> true </code> if billing search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBillingSearch() {
        return (false);
    }


    /**
     *  Tests for the availability of a billing administrative service for 
     *  creating and deleting billings. 
     *
     *  @return <code> true </code> if billing administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBillingAdmin() {
        return (false);
    }


    /**
     *  Tests for the availability of a billing notification service. 
     *
     *  @return <code> true </code> if billing notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBillingNotification() {
        return (false);
    }


    /**
     *  Tests for the availability of a billing hierarchy traversal service. 
     *
     *  @return <code> true </code> if billing hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBillingHierarchy() {
        return (false);
    }


    /**
     *  Tests for the availability of a billing hierarchy design service. 
     *
     *  @return <code> true </code> if billing hierarchy design is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBillingHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests for the availability of an acknowledgement batch service. 
     *
     *  @return <code> true </code> if an acknowledgement batch service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAcknowledgementBatch() {
        return (false);
    }


    /**
     *  Gets the supported <code> Credit </code> record types. 
     *
     *  @return a list containing the supported credit record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCreditRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.creditRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Credit </code> record type is supported. 
     *
     *  @param  creditRecordType a <code> Type </code> indicating a <code> 
     *          Credit </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> creditRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCreditRecordType(org.osid.type.Type creditRecordType) {
        return (this.creditRecordTypes.contains(creditRecordType));
    }


    /**
     *  Adds support for a credit record type.
     *
     *  @param creditRecordType a credit record type
     *  @throws org.osid.NullArgumentException
     *  <code>creditRecordType</code> is <code>null</code>
     */

    protected void addCreditRecordType(org.osid.type.Type creditRecordType) {
        this.creditRecordTypes.add(creditRecordType);
        return;
    }


    /**
     *  Removes support for a credit record type.
     *
     *  @param creditRecordType a credit record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>creditRecordType</code> is <code>null</code>
     */

    protected void removeCreditRecordType(org.osid.type.Type creditRecordType) {
        this.creditRecordTypes.remove(creditRecordType);
        return;
    }


    /**
     *  Gets the supported credit search record types. 
     *
     *  @return a list containing the supported credit search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCreditSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.creditSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given credit search record type is supported. 
     *
     *  @param  creditSearchRecordType a <code> Type </code> indicating a 
     *          credit record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> creditSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCreditSearchRecordType(org.osid.type.Type creditSearchRecordType) {
        return (this.creditSearchRecordTypes.contains(creditSearchRecordType));
    }


    /**
     *  Adds support for a credit search record type.
     *
     *  @param creditSearchRecordType a credit search record type
     *  @throws org.osid.NullArgumentException
     *  <code>creditSearchRecordType</code> is <code>null</code>
     */

    protected void addCreditSearchRecordType(org.osid.type.Type creditSearchRecordType) {
        this.creditSearchRecordTypes.add(creditSearchRecordType);
        return;
    }


    /**
     *  Removes support for a credit search record type.
     *
     *  @param creditSearchRecordType a credit search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>creditSearchRecordType</code> is <code>null</code>
     */

    protected void removeCreditSearchRecordType(org.osid.type.Type creditSearchRecordType) {
        this.creditSearchRecordTypes.remove(creditSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Billing </code> record types. 
     *
     *  @return a list containing the supported billing record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBillingRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.billingRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Billing </code> record type is supported. 
     *
     *  @param  billingRecordType a <code> Type </code> indicating a <code> 
     *          Billing </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> billingRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBillingRecordType(org.osid.type.Type billingRecordType) {
        return (this.billingRecordTypes.contains(billingRecordType));
    }


    /**
     *  Adds support for a billing record type.
     *
     *  @param billingRecordType a billing record type
     *  @throws org.osid.NullArgumentException
     *  <code>billingRecordType</code> is <code>null</code>
     */

    protected void addBillingRecordType(org.osid.type.Type billingRecordType) {
        this.billingRecordTypes.add(billingRecordType);
        return;
    }


    /**
     *  Removes support for a billing record type.
     *
     *  @param billingRecordType a billing record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>billingRecordType</code> is <code>null</code>
     */

    protected void removeBillingRecordType(org.osid.type.Type billingRecordType) {
        this.billingRecordTypes.remove(billingRecordType);
        return;
    }


    /**
     *  Gets the supported billing search record types. 
     *
     *  @return a list containing the supported billing search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBillingSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.billingSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given billing search record type is supported. 
     *
     *  @param  billingSearchRecordType a <code> Type </code> indicating a 
     *          billing record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> billingSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBillingSearchRecordType(org.osid.type.Type billingSearchRecordType) {
        return (this.billingSearchRecordTypes.contains(billingSearchRecordType));
    }


    /**
     *  Adds support for a billing search record type.
     *
     *  @param billingSearchRecordType a billing search record type
     *  @throws org.osid.NullArgumentException
     *  <code>billingSearchRecordType</code> is <code>null</code>
     */

    protected void addBillingSearchRecordType(org.osid.type.Type billingSearchRecordType) {
        this.billingSearchRecordTypes.add(billingSearchRecordType);
        return;
    }


    /**
     *  Removes support for a billing search record type.
     *
     *  @param billingSearchRecordType a billing search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>billingSearchRecordType</code> is <code>null</code>
     */

    protected void removeBillingSearchRecordType(org.osid.type.Type billingSearchRecordType) {
        this.billingSearchRecordTypes.remove(billingSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credit lookup 
     *  service. 
     *
     *  @return a <code> CreditLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCreditLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditLookupSession getCreditLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.acknowledgement.AcknowledgementManager.getCreditLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credit lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CreditLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCreditLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditLookupSession getCreditLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.acknowledgement.AcknowledgementProxyManager.getCreditLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credit lookup 
     *  service for the given billing. 
     *
     *  @param  billingId the <code> Id </code> of the <code> Billing </code> 
     *  @return a <code> CreditLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Billing </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> billingId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCreditLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditLookupSession getCreditLookupSessionForBilling(org.osid.id.Id billingId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.acknowledgement.AcknowledgementManager.getCreditLookupSessionForBilling not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credit lookup 
     *  service for the given billing. 
     *
     *  @param  billingId the <code> Id </code> of the <code> Billing </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CreditLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Billing </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> billingId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCreditLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditLookupSession getCreditLookupSessionForBilling(org.osid.id.Id billingId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.acknowledgement.AcknowledgementProxyManager.getCreditLookupSessionForBilling not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credit query 
     *  service. 
     *
     *  @return a <code> CreditQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCreditQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditQuerySession getCreditQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.acknowledgement.AcknowledgementManager.getCreditQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credit query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CreditQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCreditQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditQuerySession getCreditQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.acknowledgement.AcknowledgementProxyManager.getCreditQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credit query 
     *  service for the given billing. 
     *
     *  @param  billingId the <code> Id </code> of the <code> Billing </code> 
     *  @return a <code> CreditQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Billing </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> billingId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCreditQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditQuerySession getCreditQuerySessionForBilling(org.osid.id.Id billingId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.acknowledgement.AcknowledgementManager.getCreditQuerySessionForBilling not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credit query 
     *  service for the given billing. 
     *
     *  @param  billingId the <code> Id </code> of the <code> Billing </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CreditQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Billing </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> billingId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCreditQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditQuerySession getCreditQuerySessionForBilling(org.osid.id.Id billingId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.acknowledgement.AcknowledgementProxyManager.getCreditQuerySessionForBilling not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credit search 
     *  service. 
     *
     *  @return a <code> CreditSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCreditSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditSearchSession getCreditSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.acknowledgement.AcknowledgementManager.getCreditSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credit search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CreditSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCreditSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditSearchSession getCreditSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.acknowledgement.AcknowledgementProxyManager.getCreditSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credit search 
     *  service for the given billing. 
     *
     *  @param  billingId the <code> Id </code> of the <code> Billing </code> 
     *  @return a <code> CreditSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Billing </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> billingId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCreditSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditSearchSession getCreditSearchSessionForBilling(org.osid.id.Id billingId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.acknowledgement.AcknowledgementManager.getCreditSearchSessionForBilling not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credit search 
     *  service for the given billing. 
     *
     *  @param  billingId the <code> Id </code> of the <code> Billing </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CreditSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Billing </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> billingId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCreditSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditSearchSession getCreditSearchSessionForBilling(org.osid.id.Id billingId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.acknowledgement.AcknowledgementProxyManager.getCreditSearchSessionForBilling not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credit 
     *  administration service. 
     *
     *  @return a <code> CreditAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCreditAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditAdminSession getCreditAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.acknowledgement.AcknowledgementManager.getCreditAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credit 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CreditAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCreditAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditAdminSession getCreditAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.acknowledgement.AcknowledgementProxyManager.getCreditAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credit 
     *  administration service for the given billing. 
     *
     *  @param  billingId the <code> Id </code> of the <code> Billing </code> 
     *  @return a <code> CreditAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Billing </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> billingId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCreditAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditAdminSession getCreditAdminSessionForBilling(org.osid.id.Id billingId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.acknowledgement.AcknowledgementManager.getCreditAdminSessionForBilling not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credit 
     *  administration service for the given billing. 
     *
     *  @param  billingId the <code> Id </code> of the <code> Billing </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CreditAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Billing </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> billingId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCreditAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditAdminSession getCreditAdminSessionForBilling(org.osid.id.Id billingId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.acknowledgement.AcknowledgementProxyManager.getCreditAdminSessionForBilling not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credit 
     *  notification service. 
     *
     *  @param  creditReceiver the receiver 
     *  @return a <code> CreditNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> creditReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCreditNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditNotificationSession getCreditNotificationSession(org.osid.acknowledgement.CreditReceiver creditReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.acknowledgement.AcknowledgementManager.getCreditNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credit 
     *  notification service. 
     *
     *  @param  creditReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> CreditNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> creditReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCreditNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditNotificationSession getCreditNotificationSession(org.osid.acknowledgement.CreditReceiver creditReceiver, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.acknowledgement.AcknowledgementProxyManager.getCreditNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credit 
     *  notification service for the given billing. 
     *
     *  @param  creditReceiver the receiver 
     *  @param  billingId the <code> Id </code> of the <code> Billing </code> 
     *  @return a <code> CreditNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Billing </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> creditReceiver </code> 
     *          or <code> billingId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCreditNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditNotificationSession getCreditNotificationSessionForBilling(org.osid.acknowledgement.CreditReceiver creditReceiver, 
                                                                                                     org.osid.id.Id billingId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.acknowledgement.AcknowledgementManager.getCreditNotificationSessionForBilling not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credit 
     *  notification service for the given billing. 
     *
     *  @param  creditReceiver the receiver 
     *  @param  billingId the <code> Id </code> of the <code> Billing </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CreditNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Billing </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> creditReceiver, 
     *          billingId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCreditNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditNotificationSession getCreditNotificationSessionForBilling(org.osid.acknowledgement.CreditReceiver creditReceiver, 
                                                                                                     org.osid.id.Id billingId, 
                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.acknowledgement.AcknowledgementProxyManager.getCreditNotificationSessionForBilling not implemented");
    }


    /**
     *  Gets the session for retrieving credit to billing mappings. 
     *
     *  @return a <code> CreditBillingSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCreditBilling() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditBillingSession getCreditBillingSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.acknowledgement.AcknowledgementManager.getCreditBillingSession not implemented");
    }


    /**
     *  Gets the session for retrieving credit to billing mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CreditBillingSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCreditBilling() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditBillingSession getCreditBillingSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.acknowledgement.AcknowledgementProxyManager.getCreditBillingSession not implemented");
    }


    /**
     *  Gets the session for assigning credit to billing mappings. 
     *
     *  @return a <code> CreditBillingAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCreditBillingAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditBillingAssignmentSession getCreditBillingAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.acknowledgement.AcknowledgementManager.getCreditBillingAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning credit to billing mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CreditBillingAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCreditBillingAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditBillingAssignmentSession getCreditBillingAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.acknowledgement.AcknowledgementProxyManager.getCreditBillingAssignmentSession not implemented");
    }


    /**
     *  Gets the session associated with the credit smart billing for the 
     *  given billing. 
     *
     *  @param  billingId the <code> Id </code> of the billing 
     *  @return a <code> CreditSmartBillingSession </code> 
     *  @throws org.osid.NotFoundException <code> billingId </code> not found 
     *  @throws org.osid.NullArgumentException <code> billingId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCreditSmartBilling() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditSmartBillingSession getCreditSmartBillingSession(org.osid.id.Id billingId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.acknowledgement.AcknowledgementManager.getCreditSmartBillingSession not implemented");
    }


    /**
     *  Gets the session for managing dynamic credit billings for the given 
     *  billing. 
     *
     *  @param  billingId the <code> Id </code> of a billing 
     *  @param  proxy a proxy 
     *  @return a <code> CreditSmartBillingSession </code> 
     *  @throws org.osid.NotFoundException <code> billingId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> billingId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCreditSmartBilling() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditSmartBillingSession getCreditSmartBillingSession(org.osid.id.Id billingId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.acknowledgement.AcknowledgementProxyManager.getCreditSmartBillingSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the billing lookup 
     *  service. 
     *
     *  @return a <code> BillingLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBillingLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingLookupSession getBillingLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.acknowledgement.AcknowledgementManager.getBillingLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the billing lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BillingLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBillingLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingLookupSession getBillingLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.acknowledgement.AcknowledgementProxyManager.getBillingLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the billing query 
     *  service. 
     *
     *  @return a <code> BillingQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBillingQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingQuerySession getBillingQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.acknowledgement.AcknowledgementManager.getBillingQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the billing query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BillingQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBillingQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingQuerySession getBillingQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.acknowledgement.AcknowledgementProxyManager.getBillingQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the billing search 
     *  service. 
     *
     *  @return a <code> BillingSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBillingSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingSearchSession getBillingSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.acknowledgement.AcknowledgementManager.getBillingSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the billing search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BillingSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBillingSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingSearchSession getBillingSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.acknowledgement.AcknowledgementProxyManager.getBillingSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the billing 
     *  administrative service. 
     *
     *  @return a <code> BillingAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBillingAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingAdminSession getBillingAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.acknowledgement.AcknowledgementManager.getBillingAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the billing 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BillingAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBillingAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingAdminSession getBillingAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.acknowledgement.AcknowledgementProxyManager.getBillingAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the billing 
     *  notification service. 
     *
     *  @param  billingReceiver the receiver 
     *  @return a <code> BillingNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> billingReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBillingNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingNotificationSession getBillingNotificationSession(org.osid.acknowledgement.BillingReceiver billingReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.acknowledgement.AcknowledgementManager.getBillingNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the billing 
     *  notification service. 
     *
     *  @param  billingReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> BillingNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> billingReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBillingNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingNotificationSession getBillingNotificationSession(org.osid.acknowledgement.BillingReceiver billingReceiver, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.acknowledgement.AcknowledgementProxyManager.getBillingNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the billing 
     *  hierarchy service. 
     *
     *  @return a <code> BillingHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBillingHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingHierarchySession getBillingHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.acknowledgement.AcknowledgementManager.getBillingHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the billing 
     *  hierarchy service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BillingHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBillingHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingHierarchySession getBillingHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.acknowledgement.AcknowledgementProxyManager.getBillingHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the billing 
     *  hierarchy design service. 
     *
     *  @return a <code> BillingHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBillingHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingHierarchyDesignSession getBillingHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.acknowledgement.AcknowledgementManager.getBillingHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the billing 
     *  hierarchy design service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BillingHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBillingHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingHierarchyDesignSession getBillingHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.acknowledgement.AcknowledgementProxyManager.getBillingHierarchyDesignSession not implemented");
    }


    /**
     *  Gets an <code> AcknowledgementBatchManager. </code> 
     *
     *  @return an <code> AcknowledgementBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAcknowledgementBach() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.batch.AcknowledgementBatchManager getAcknowledgementBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.acknowledgement.AcknowledgementManager.getAcknowledgementBatchManager not implemented");
    }


    /**
     *  Gets an <code> AcknowledgementBatchProxyManager. </code> 
     *
     *  @return an <code> AcknowledgementBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAcknowledgementBach() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.batch.AcknowledgementBatchProxyManager getAcknowledgementBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.acknowledgement.AcknowledgementProxyManager.getAcknowledgementBatchProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.creditRecordTypes.clear();
        this.creditRecordTypes.clear();

        this.creditSearchRecordTypes.clear();
        this.creditSearchRecordTypes.clear();

        this.billingRecordTypes.clear();
        this.billingRecordTypes.clear();

        this.billingSearchRecordTypes.clear();
        this.billingSearchRecordTypes.clear();

        return;
    }
}
