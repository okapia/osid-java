//
// AbstractMapCommitmentEnablerLookupSession
//
//    A simple framework for providing a CommitmentEnabler lookup service
//    backed by a fixed collection of commitment enablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a CommitmentEnabler lookup service backed by a
 *  fixed collection of commitment enablers. The commitment enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>CommitmentEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapCommitmentEnablerLookupSession
    extends net.okapia.osid.jamocha.calendaring.rules.spi.AbstractCommitmentEnablerLookupSession
    implements org.osid.calendaring.rules.CommitmentEnablerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.calendaring.rules.CommitmentEnabler> commitmentEnablers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.calendaring.rules.CommitmentEnabler>());


    /**
     *  Makes a <code>CommitmentEnabler</code> available in this session.
     *
     *  @param  commitmentEnabler a commitment enabler
     *  @throws org.osid.NullArgumentException <code>commitmentEnabler<code>
     *          is <code>null</code>
     */

    protected void putCommitmentEnabler(org.osid.calendaring.rules.CommitmentEnabler commitmentEnabler) {
        this.commitmentEnablers.put(commitmentEnabler.getId(), commitmentEnabler);
        return;
    }


    /**
     *  Makes an array of commitment enablers available in this session.
     *
     *  @param  commitmentEnablers an array of commitment enablers
     *  @throws org.osid.NullArgumentException <code>commitmentEnablers<code>
     *          is <code>null</code>
     */

    protected void putCommitmentEnablers(org.osid.calendaring.rules.CommitmentEnabler[] commitmentEnablers) {
        putCommitmentEnablers(java.util.Arrays.asList(commitmentEnablers));
        return;
    }


    /**
     *  Makes a collection of commitment enablers available in this session.
     *
     *  @param  commitmentEnablers a collection of commitment enablers
     *  @throws org.osid.NullArgumentException <code>commitmentEnablers<code>
     *          is <code>null</code>
     */

    protected void putCommitmentEnablers(java.util.Collection<? extends org.osid.calendaring.rules.CommitmentEnabler> commitmentEnablers) {
        for (org.osid.calendaring.rules.CommitmentEnabler commitmentEnabler : commitmentEnablers) {
            this.commitmentEnablers.put(commitmentEnabler.getId(), commitmentEnabler);
        }

        return;
    }


    /**
     *  Removes a CommitmentEnabler from this session.
     *
     *  @param  commitmentEnablerId the <code>Id</code> of the commitment enabler
     *  @throws org.osid.NullArgumentException <code>commitmentEnablerId<code> is
     *          <code>null</code>
     */

    protected void removeCommitmentEnabler(org.osid.id.Id commitmentEnablerId) {
        this.commitmentEnablers.remove(commitmentEnablerId);
        return;
    }


    /**
     *  Gets the <code>CommitmentEnabler</code> specified by its <code>Id</code>.
     *
     *  @param  commitmentEnablerId <code>Id</code> of the <code>CommitmentEnabler</code>
     *  @return the commitmentEnabler
     *  @throws org.osid.NotFoundException <code>commitmentEnablerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>commitmentEnablerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnabler getCommitmentEnabler(org.osid.id.Id commitmentEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.calendaring.rules.CommitmentEnabler commitmentEnabler = this.commitmentEnablers.get(commitmentEnablerId);
        if (commitmentEnabler == null) {
            throw new org.osid.NotFoundException("commitmentEnabler not found: " + commitmentEnablerId);
        }

        return (commitmentEnabler);
    }


    /**
     *  Gets all <code>CommitmentEnablers</code>. In plenary mode, the returned
     *  list contains all known commitmentEnablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  commitmentEnablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>CommitmentEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerList getCommitmentEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.calendaring.rules.commitmentenabler.ArrayCommitmentEnablerList(this.commitmentEnablers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.commitmentEnablers.clear();
        super.close();
        return;
    }
}
