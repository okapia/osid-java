//
// AbstractFinancialsManager.java
//
//     An adapter for a FinancialsManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.financials.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a FinancialsManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterFinancialsManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.financials.FinancialsManager>
    implements org.osid.financials.FinancialsManager {


    /**
     *  Constructs a new {@code AbstractAdapterFinancialsManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterFinancialsManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterFinancialsManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterFinancialsManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if visible federation is supported. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if looking up accounts is supported. 
     *
     *  @return <code> true </code> if account lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAccountLookup() {
        return (getAdapteeManager().supportsAccountLookup());
    }


    /**
     *  Tests if querying accounts is supported. 
     *
     *  @return <code> true </code> if account query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAccountQuery() {
        return (getAdapteeManager().supportsAccountQuery());
    }


    /**
     *  Tests if searching accounts is supported. 
     *
     *  @return <code> true </code> if account search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAccountSearch() {
        return (getAdapteeManager().supportsAccountSearch());
    }


    /**
     *  Tests if an account <code> </code> administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if account administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAccountAdmin() {
        return (getAdapteeManager().supportsAccountAdmin());
    }


    /**
     *  Tests if an account <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if account notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAccountNotification() {
        return (getAdapteeManager().supportsAccountNotification());
    }


    /**
     *  Tests if an account hierarchy traversal is supported. 
     *
     *  @return <code> true </code> if an account hierarchy traversal is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAccountHierarchy() {
        return (getAdapteeManager().supportsAccountHierarchy());
    }


    /**
     *  Tests if account hierarchy design is supported. 
     *
     *  @return <code> true </code> if an account hierarchy design is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAccountHierarchyDesign() {
        return (getAdapteeManager().supportsAccountHierarchyDesign());
    }


    /**
     *  Tests if an account cataloging service is supported. 
     *
     *  @return <code> true </code> if account catalog is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAccountBusiness() {
        return (getAdapteeManager().supportsAccountBusiness());
    }


    /**
     *  Tests if an account cataloging service is supported. A cataloging 
     *  service maps accounts to catalogs. 
     *
     *  @return <code> true </code> if account cataloging is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAccountBusinessAssignment() {
        return (getAdapteeManager().supportsAccountBusinessAssignment());
    }


    /**
     *  Tests if an account smart business session is available. 
     *
     *  @return <code> true </code> if an account smart business session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAccountSmartBusiness() {
        return (getAdapteeManager().supportsAccountSmartBusiness());
    }


    /**
     *  Tests if looking up activities is supported. 
     *
     *  @return <code> true </code> if activity lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityLookup() {
        return (getAdapteeManager().supportsActivityLookup());
    }


    /**
     *  Tests if querying activities is supported. 
     *
     *  @return <code> true </code> if activity query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityQuery() {
        return (getAdapteeManager().supportsActivityQuery());
    }


    /**
     *  Tests if searching activities is supported. 
     *
     *  @return <code> true </code> if activity search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivitySearch() {
        return (getAdapteeManager().supportsActivitySearch());
    }


    /**
     *  Tests if activity administrative service is supported. 
     *
     *  @return <code> true </code> if activity administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityAdmin() {
        return (getAdapteeManager().supportsActivityAdmin());
    }


    /**
     *  Tests if an activity <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if activity notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityNotification() {
        return (getAdapteeManager().supportsActivityNotification());
    }


    /**
     *  Tests if an activity hierarchy traversal is supported. 
     *
     *  @return <code> true </code> if an activity hierarchy traversal is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityHierarchy() {
        return (getAdapteeManager().supportsActivityHierarchy());
    }


    /**
     *  Tests if activity hierarchy design is supported. 
     *
     *  @return <code> true </code> if an activity hierarchy design is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityHierarchyDesign() {
        return (getAdapteeManager().supportsActivityHierarchyDesign());
    }


    /**
     *  Tests if an activity cataloging service is supported. 
     *
     *  @return <code> true </code> if activity catalog is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityBusiness() {
        return (getAdapteeManager().supportsActivityBusiness());
    }


    /**
     *  Tests if an activity cataloging service is supported. A cataloging 
     *  service maps activities to catalogs. 
     *
     *  @return <code> true </code> if activity cataloging is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityBusinessAssignment() {
        return (getAdapteeManager().supportsActivityBusinessAssignment());
    }


    /**
     *  Tests if an activity smart business session is available. 
     *
     *  @return <code> true </code> if an activity smart business session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivitySmartBusiness() {
        return (getAdapteeManager().supportsActivitySmartBusiness());
    }


    /**
     *  Tests if looking up fiscal periods is supported. 
     *
     *  @return <code> true </code> if fiscal period lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFiscalPeriodLookup() {
        return (getAdapteeManager().supportsFiscalPeriodLookup());
    }


    /**
     *  Tests if querying fiscal periods is supported. 
     *
     *  @return <code> true </code> if fiscal period query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFiscalPeriodQuery() {
        return (getAdapteeManager().supportsFiscalPeriodQuery());
    }


    /**
     *  Tests if searching fiscal periods is supported. 
     *
     *  @return <code> true </code> if fiscal period search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFiscalPeriodSearch() {
        return (getAdapteeManager().supportsFiscalPeriodSearch());
    }


    /**
     *  Tests if fiscal period <code> </code> administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if fiscal period administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFiscalPeriodAdmin() {
        return (getAdapteeManager().supportsFiscalPeriodAdmin());
    }


    /**
     *  Tests if a fiscal period <code> </code> notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if fiscal period notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFiscalPeriodNotification() {
        return (getAdapteeManager().supportsFiscalPeriodNotification());
    }


    /**
     *  Tests if a fiscal period cataloging service is supported. 
     *
     *  @return <code> true </code> if fiscal period catalog is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFiscalPeriodBusiness() {
        return (getAdapteeManager().supportsFiscalPeriodBusiness());
    }


    /**
     *  Tests if a fiscal period cataloging service is supported. A cataloging 
     *  service maps fiscal periods to catalogs. 
     *
     *  @return <code> true </code> if fiscal period cataloging is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFiscalPeriodBusinessAssignment() {
        return (getAdapteeManager().supportsFiscalPeriodBusinessAssignment());
    }


    /**
     *  Tests if a fiscal period smart business session is available. 
     *
     *  @return <code> true </code> if a fiscal period smart business session 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFiscalPeriodSmartBusiness() {
        return (getAdapteeManager().supportsFiscalPeriodSmartBusiness());
    }


    /**
     *  Tests if looking up businesses is supported. 
     *
     *  @return <code> true </code> if business lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessLookup() {
        return (getAdapteeManager().supportsBusinessLookup());
    }


    /**
     *  Tests if searching businesses is supported. 
     *
     *  @return <code> true </code> if business search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessSearch() {
        return (getAdapteeManager().supportsBusinessSearch());
    }


    /**
     *  Tests if querying businesses is supported. 
     *
     *  @return <code> true </code> if business query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessQuery() {
        return (getAdapteeManager().supportsBusinessQuery());
    }


    /**
     *  Tests if business administrative service is supported. 
     *
     *  @return <code> true </code> if business administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessAdmin() {
        return (getAdapteeManager().supportsBusinessAdmin());
    }


    /**
     *  Tests if a business <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if business notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessNotification() {
        return (getAdapteeManager().supportsBusinessNotification());
    }


    /**
     *  Tests for the availability of a business hierarchy traversal service. 
     *
     *  @return <code> true </code> if business hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessHierarchy() {
        return (getAdapteeManager().supportsBusinessHierarchy());
    }


    /**
     *  Tests for the availability of a business hierarchy design service. 
     *
     *  @return <code> true </code> if business hierarchy design is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessHierarchyDesign() {
        return (getAdapteeManager().supportsBusinessHierarchyDesign());
    }


    /**
     *  Tests for the availability of a financials batch service. 
     *
     *  @return <code> true </code> if a financials batch service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFinancialsBatch() {
        return (getAdapteeManager().supportsFinancialsBatch());
    }


    /**
     *  Tests for the availability of a financials budgeting service. 
     *
     *  @return <code> true </code> if a financials budgeting service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFinancialsBudgeting() {
        return (getAdapteeManager().supportsFinancialsBudgeting());
    }


    /**
     *  Tests for the availability of a financials postng service. 
     *
     *  @return <code> true </code> if a financials posting service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFinancialsPosting() {
        return (getAdapteeManager().supportsFinancialsPosting());
    }


    /**
     *  Gets the supported <code> Account </code> record types. 
     *
     *  @return a list containing the supported <code> Account </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAccountRecordTypes() {
        return (getAdapteeManager().getAccountRecordTypes());
    }


    /**
     *  Tests if the given <code> Account </code> record type is supported. 
     *
     *  @param  accountRecordType a <code> Type </code> indicating an <code> 
     *          Account </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> accountRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAccountRecordType(org.osid.type.Type accountRecordType) {
        return (getAdapteeManager().supportsAccountRecordType(accountRecordType));
    }


    /**
     *  Gets the supported <code> Account </code> search record types. 
     *
     *  @return a list containing the supported <code> Account </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAccountSearchRecordTypes() {
        return (getAdapteeManager().getAccountSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Account </code> search record type is 
     *  supported. 
     *
     *  @param  accountSearchRecordType a <code> Type </code> indicating an 
     *          <code> Account </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> accountSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAccountSearchRecordType(org.osid.type.Type accountSearchRecordType) {
        return (getAdapteeManager().supportsAccountSearchRecordType(accountSearchRecordType));
    }


    /**
     *  Gets the supported <code> Activity </code> record types. 
     *
     *  @return a list containing the supported <code> Activity </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getActivityRecordTypes() {
        return (getAdapteeManager().getActivityRecordTypes());
    }


    /**
     *  Tests if the given <code> Activity </code> record type is supported. 
     *
     *  @param  activityRecordType a <code> Type </code> indicating an <code> 
     *          Activity </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> activityRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsActivityRecordType(org.osid.type.Type activityRecordType) {
        return (getAdapteeManager().supportsActivityRecordType(activityRecordType));
    }


    /**
     *  Gets the supported <code> Activity </code> search record types. 
     *
     *  @return a list containing the supported <code> Activity </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getActivitySearchRecordTypes() {
        return (getAdapteeManager().getActivitySearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Activity </code> search record type is 
     *  supported. 
     *
     *  @param  activitySearchRecordType a <code> Type </code> indicating an 
     *          <code> Activity </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> activitySearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsActivitySearchRecordType(org.osid.type.Type activitySearchRecordType) {
        return (getAdapteeManager().supportsActivitySearchRecordType(activitySearchRecordType));
    }


    /**
     *  Gets the supported <code> FiscalPeriod </code> record types. 
     *
     *  @return a list containing the supported <code> FiscalPeriod </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getFiscalPeriodRecordTypes() {
        return (getAdapteeManager().getFiscalPeriodRecordTypes());
    }


    /**
     *  Tests if the given <code> FiscalPeriod </code> record type is 
     *  supported. 
     *
     *  @param  fiscalPeriodRecordType a <code> Type </code> indicating an 
     *          <code> FiscalPeriod </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> fiscalPeriodRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsFiscalPeriodRecordType(org.osid.type.Type fiscalPeriodRecordType) {
        return (getAdapteeManager().supportsFiscalPeriodRecordType(fiscalPeriodRecordType));
    }


    /**
     *  Gets the supported <code> FiscalPeriod </code> search record types. 
     *
     *  @return a list containing the supported <code> FiscalPeriod </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getFiscalPeriodSearchRecordTypes() {
        return (getAdapteeManager().getFiscalPeriodSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> FiscalPeriod </code> search record type is 
     *  supported. 
     *
     *  @param  fiscalPeriodSearchRecordType a <code> Type </code> indicating 
     *          a <code> FiscalPeriod </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          fiscalPeriodSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsFiscalPeriodSearchRecordType(org.osid.type.Type fiscalPeriodSearchRecordType) {
        return (getAdapteeManager().supportsFiscalPeriodSearchRecordType(fiscalPeriodSearchRecordType));
    }


    /**
     *  Gets the supported <code> Summary </code> record types. 
     *
     *  @return a list containing the supported <code> Summary </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSummaryRecordTypes() {
        return (getAdapteeManager().getSummaryRecordTypes());
    }


    /**
     *  Tests if the given <code> Summary </code> record type is supported. 
     *
     *  @param  summaryRecordType a <code> Type </code> indicating a <code> 
     *          Summary </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> summaryRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSummaryRecordType(org.osid.type.Type summaryRecordType) {
        return (getAdapteeManager().supportsSummaryRecordType(summaryRecordType));
    }


    /**
     *  Gets the supported <code> Business </code> record types. 
     *
     *  @return a list containing the supported <code> Business </code> types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBusinessRecordTypes() {
        return (getAdapteeManager().getBusinessRecordTypes());
    }


    /**
     *  Tests if the given <code> Business </code> record type is supported. 
     *
     *  @param  businessRecordType a <code> Type </code> indicating an <code> 
     *          Business </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> businessRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBusinessRecordType(org.osid.type.Type businessRecordType) {
        return (getAdapteeManager().supportsBusinessRecordType(businessRecordType));
    }


    /**
     *  Gets the supported <code> Business </code> search record types. 
     *
     *  @return a list containing the supported <code> Business </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBusinessSearchRecordTypes() {
        return (getAdapteeManager().getBusinessSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Business </code> search record type is 
     *  supported. 
     *
     *  @param  businessSearchRecordType a <code> Type </code> indicating an 
     *          <code> Business </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> businessSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBusinessSearchRecordType(org.osid.type.Type businessSearchRecordType) {
        return (getAdapteeManager().supportsBusinessSearchRecordType(businessSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the reporting 
     *  service. 
     *
     *  @return a <code> ReportingSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsReporting() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ReportingSession getReportingSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getReportingSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the reporting 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the business 
     *  @return a <code> ReportingSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsReporting() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ReportingSession getReportingSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getReportingSessionForBusiness(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the account lookup 
     *  service. 
     *
     *  @return an <code> AccountSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAccountLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountLookupSession getAccountLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAccountLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the account lookup 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the business 
     *  @return an <code> AccountLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAccountLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountLookupSession getAccountLookupSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAccountLookupSessionForBusiness(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the account query 
     *  service. 
     *
     *  @return an <code> AccountQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAccountQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountQuerySession getAccountQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAccountQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the account query 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return an <code> AccountQuerySession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAccountQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountQuerySession getAccountQuerySessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAccountQuerySessionForBusiness(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the account search 
     *  service. 
     *
     *  @return an <code> AccountSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAccountSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountSearchSession getAccountSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAccountSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the account search 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return an <code> AccountSearchSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAccountSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountSearchSession getAccountSearchSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAccountSearchSessionForBusiness(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the account 
     *  administration service. 
     *
     *  @return an <code> AccountAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAccountAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountAdminSession getAccountAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAccountAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the account 
     *  administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return an <code> AccountAdminSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAccountAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountAdminSession getAccountAdminSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAccountAdminSessionForBusiness(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the account 
     *  notification service. 
     *
     *  @param  accountReceiver the notification callback 
     *  @return an <code> AccountNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> accountReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAccountNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountNotificationSession getAccountNotificationSession(org.osid.financials.AccountReceiver accountReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAccountNotificationSession(accountReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the account 
     *  notification service for the given business. 
     *
     *  @param  accountReceiver the notification callback 
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return an <code> AccountNotificationSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> accountReceiver </code> 
     *          or <code> businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAccountNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountNotificationSession getAccountNotificationSessionForBusiness(org.osid.financials.AccountReceiver accountReceiver, 
                                                                                                   org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAccountNotificationSessionForBusiness(accountReceiver, businessId));
    }


    /**
     *  Gets the session traversing account hierarchies. 
     *
     *  @return an <code> AccountHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAccountHierarchy() is false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountHierarchySession getAccountHierarchySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAccountHierarchySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the account 
     *  heirarchy traversal service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the business 
     *  @return an <code> AccountHierarchySession </code> 
     *  @throws org.osid.NotFoundException <code> businessId </code> not found 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAccountHierarchy() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountHierarchySession getAccountHierarchySessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAccountHierarchySessionForBusiness(businessId));
    }


    /**
     *  Gets the session designing account hierarchies. 
     *
     *  @return an <code> AccountHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAccountHierarchyDesign() is false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountHierarchyDesignSession getAccountHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAccountHierarchyDesignSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the account 
     *  heirarchy design service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the business 
     *  @return an <code> AccountHierarchyDesignSession </code> 
     *  @throws org.osid.NotFoundException <code> businessId </code> not found 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAccountHierarchyDesign() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountHierarchyDesignSession getAccountHierarchyDesignSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAccountHierarchyDesignSessionForBusiness(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup account/catalog 
     *  mappings. 
     *
     *  @return an <code> AccountBusinessSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAccountBusiness() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountBusinessSession getAccountBusinessSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAccountBusinessSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning accounts 
     *  to businesses. 
     *
     *  @return an <code> AccountBusinessAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAccountBusinessAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountBusinessAssignmentSession getAccountBusinessAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAccountBusinessAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the account smart 
     *  business service. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return an <code> AccountSmartBusinessSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAccountSmartBusiness() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountSmartBusinessSession getAccountSmartBusinessSession(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAccountSmartBusinessSession(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  lookup service. 
     *
     *  @return an <code> ActivityLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivityLookupSession getActivityLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  lookup service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return an <code> ActivityLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivityLookupSession getActivityLookupSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityLookupSessionForBusiness(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity query 
     *  service. 
     *
     *  @return an <code> ActivityQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsActivityQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivityQuerySession getActivityQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity query 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return an <code> ActivityQuerySession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsActivityQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivityQuerySession getActivityQuerySessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityQuerySessionForBusiness(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  search service. 
     *
     *  @return an <code> ActivitySearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivitySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivitySearchSession getActivitySearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActivitySearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  search service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return an <code> ActivitySearchSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivitySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivitySearchSession getActivitySearchSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActivitySearchSessionForBusiness(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  administration service. 
     *
     *  @return an <code> ActivityAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsActivityAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivityAdminSession getActivityAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return an <code> ActivityAdminSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsActivityAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivityAdminSession getActivityAdminSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityAdminSessionForBusiness(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  notification service. 
     *
     *  @param  activityReceiver the notification callback 
     *  @return an <code> ActivityNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> activityReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivityNotificationSession getActivityNotificationSession(org.osid.financials.ActivityReceiver activityReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityNotificationSession(activityReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  notification service for the given business. 
     *
     *  @param  activityReceiver the notification callback 
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return an <code> ActivityNotificationSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> activityReceiver </code> 
     *          or <code> businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivityNotificationSession getActivityNotificationSessionForBusiness(org.osid.financials.ActivityReceiver activityReceiver, 
                                                                                                     org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityNotificationSessionForBusiness(activityReceiver, businessId));
    }


    /**
     *  Gets the session traversing activity hierarchies. 
     *
     *  @return an <code> ActivityHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityHierarchy() is false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivityHierarchySession getActivityHierarchySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityHierarchySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  heirarchy traversal service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the business 
     *  @return an <code> ActivityHierarchySession </code> 
     *  @throws org.osid.NotFoundException <code> businessId </code> not found 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityHierarchy() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivityHierarchySession getActivityHierarchySessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityHierarchySessionForBusiness(businessId));
    }


    /**
     *  Gets the session designing activity hierarchies. 
     *
     *  @return an <code> ActivityHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityHierarchyDesign() is false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivityHierarchyDesignSession getActivityHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityHierarchyDesignSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  heirarchy design service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the business 
     *  @return an <code> ActivityHierarchyDesignSession </code> 
     *  @throws org.osid.NotFoundException <code> businessId </code> not found 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityHierarchyDesign() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivityHierarchyDesignSession getActivityHierarchyDesignSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityHierarchyDesignSessionForBusiness(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup activity/catalog 
     *  mappings. 
     *
     *  @return an <code> ActivityBusinessSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBusiness() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivityBusinessSession getActivityBusinessSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityBusinessSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  activities to businesses. 
     *
     *  @return an <code> ActivityBusinessAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBusinessAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivityBusinessAssignmentSession getActivityBusinessAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityBusinessAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity smart 
     *  business service. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return an <code> ActivitySmartBusinessSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivitySmartBusiness() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivitySmartBusinessSession getActivitySmartBusinessSession(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActivitySmartBusinessSession(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the fiscal period 
     *  lookup service. 
     *
     *  @return a <code> FiscalPeriodSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodLookupSession getFiscalPeriodLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFiscalPeriodLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the fiscal period 
     *  lookup service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the business 
     *  @return a <code> FiscalPeriodLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodLookupSession getFiscalPeriodLookupSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getFiscalPeriodLookupSessionForBusiness(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the fiscal period 
     *  query service. 
     *
     *  @return a <code> FiscalPeriodQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodQuerySession getFiscalPeriodQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFiscalPeriodQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the fiscal period 
     *  query service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> FiscalPeriodQuerySession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodQuerySession getFiscalPeriodQuerySessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getFiscalPeriodQuerySessionForBusiness(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the fiscal period 
     *  search service. 
     *
     *  @return a <code> FiscalPeriodSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodSearchSession getFiscalPeriodSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFiscalPeriodSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the fiscal period 
     *  search service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> FiscalPeriodSearchSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodSearchSession getFiscalPeriodSearchSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getFiscalPeriodSearchSessionForBusiness(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the fiscal period 
     *  administration service. 
     *
     *  @return a <code> FiscalPeriodAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodAdminSession getFiscalPeriodAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFiscalPeriodAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the fiscal period 
     *  administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> FiscalPeriodAdminSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodAdminSession getFiscalPeriodAdminSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getFiscalPeriodAdminSessionForBusiness(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the fiscal period 
     *  notification service. 
     *
     *  @param  fiscalPeriodReceiver the notification callback 
     *  @return a <code> FiscalPeriodNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> fiscalPeriodReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodNotificationSession getFiscalPeriodNotificationSession(org.osid.financials.FiscalPeriodReceiver fiscalPeriodReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFiscalPeriodNotificationSession(fiscalPeriodReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the fiscal period 
     *  notification service for the given business. 
     *
     *  @param  fiscalPeriodReceiver the notification callback 
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> FiscalPeriodNotificationSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> fiscalPeriodReceiver 
     *          </code> or <code> businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodNotificationSession getFiscalPeriodNotificationSessionForBusiness(org.osid.financials.FiscalPeriodReceiver fiscalPeriodReceiver, 
                                                                                                             org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getFiscalPeriodNotificationSessionForBusiness(fiscalPeriodReceiver, businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup fiscal period/catalog 
     *  mappings. 
     *
     *  @return a <code> FiscalPeriodBusinessSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodBusiness() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodBusinessSession getFiscalPeriodBusinessSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFiscalPeriodBusinessSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning fiscal 
     *  periods to businesses. 
     *
     *  @return a <code> FiscalPeriodBusinessAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodBusinessAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodBusinessAssignmentSession getFiscalPeriodBusinessAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFiscalPeriodBusinessAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the fiscal period 
     *  smart business service. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> FiscalPeriodSmartBusinessSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodSmartBusiness() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodSmartBusinessSession getFiscalPeriodSmartBusinessSession(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getFiscalPeriodSmartBusinessSession(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the business 
     *  lookup service. 
     *
     *  @return a <code> BusinessLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBusinessLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.BusinessLookupSession getBusinessLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBusinessLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the business query 
     *  service. 
     *
     *  @return a <code> BusinessQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBusinessQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.BusinessQuerySession getBusinessQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBusinessQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the business 
     *  search service. 
     *
     *  @return a <code> BusinessSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBusinessSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.BusinessSearchSession getBusinessSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBusinessSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the business 
     *  administrative service. 
     *
     *  @return a <code> BusinessAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBusinessAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.BusinessAdminSession getBusinessAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBusinessAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the business 
     *  notification service. 
     *
     *  @param  businessReceiver the notification callback 
     *  @return a <code> BusinessNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> businessReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBusinessNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.BusinessNotificationSession getBusinessNotificationSession(org.osid.financials.BusinessReceiver businessReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBusinessNotificationSession(businessReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the business 
     *  hierarchy service. 
     *
     *  @return a <code> BusinessHierarchySession </code> for businesses 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBusinessHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.BusinessHierarchySession getBusinessHierarchySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBusinessHierarchySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the business 
     *  hierarchy design service. 
     *
     *  @return a <code> HierarchyDesignSession </code> for businesses 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBusinessHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.BusinessHierarchyDesignSession getBusinessHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBusinessHierarchyDesignSession());
    }


    /**
     *  Gets the <code> FinancialsBatchManager. </code> 
     *
     *  @return a <code> FinancialsBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFinancialsBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.batch.FinancialsBatchManager getFinancialsBatchManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFinancialsBatchManager());
    }


    /**
     *  Gets the <code> FinancialsBudgetingManager. </code> 
     *
     *  @return a <code> FinancialsBudgetingManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFinancialsBudgeting() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.FinancialsBudgetingManager getFinancialsBudgetingManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFinancialsBudgetingManager());
    }


    /**
     *  Gets the <code> FinancialsPostingManager. </code> 
     *
     *  @return a <code> FinancialsPostingManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFinancialsPosting() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.FinancialsPostingManager getFinancialsPostingManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFinancialsPostingManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
