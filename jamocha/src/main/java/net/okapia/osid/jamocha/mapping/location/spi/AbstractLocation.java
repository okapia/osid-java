//
// AbstractLocation.java
//
//     Defines a Location.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.location.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Location</code>.
 */

public abstract class AbstractLocation
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.mapping.Location {

    private org.osid.mapping.SpatialUnit spatialUnit;

    private final java.util.Collection<org.osid.mapping.records.LocationRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Tests if a spatial unit is available for this location. 
     *
     *  @return <code> true </code> if a spatial unit is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasSpatialUnit() {
        return (this.spatialUnit != null);
    }


    /**
     *  Gets the spatial unit corresponding to this location. 
     *
     *  @return the spatial unit for this location 
     *  @throws org.osid.IllegalStateException <code> hasSpatialUnit() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.SpatialUnit getSpatialUnit() {
        if (!hasSpatialUnit()) {
            throw new org.osid.IllegalStateException("hasSpatialUnit() is false");
        }

        return (this.spatialUnit);
    }


    /**
     *  Sets the spatial unit.
     *
     *  @param spatialUnit a spatial unit
     *  @throws org.osid.NullArgumentException
     *          <code>spatialUnit</code> is <code>null</code>
     */

    protected void setSpatialUnit(org.osid.mapping.SpatialUnit spatialUnit) {
        nullarg(spatialUnit, "spatial unit");
        this.spatialUnit = spatialUnit;
        return;
    }


    /**
     *  Tests if this location supports the given record
     *  <code>Type</code>.
     *
     *  @param  locationRecordType a location record type 
     *  @return <code>true</code> if the locationRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>locationRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type locationRecordType) {
        for (org.osid.mapping.records.LocationRecord record : this.records) {
            if (record.implementsRecordType(locationRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Location</code> record <code>Type</code>.
     *
     *  @param  locationRecordType the location record type 
     *  @return the location record 
     *  @throws org.osid.NullArgumentException
     *          <code>locationRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(locationRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.records.LocationRecord getLocationRecord(org.osid.type.Type locationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.records.LocationRecord record : this.records) {
            if (record.implementsRecordType(locationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(locationRecordType + " is not supported");
    }


    /**
     *  Adds a record to this location. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param locationRecord the location record
     *  @param locationRecordType location record type
     *  @throws org.osid.NullArgumentException
     *          <code>locationRecord</code> or
     *          <code>locationRecordTypelocation</code> is
     *          <code>null</code>
     */
            
    protected void addLocationRecord(org.osid.mapping.records.LocationRecord locationRecord, 
                                     org.osid.type.Type locationRecordType) {

        nullarg(locationRecord, "location record");
        addRecordType(locationRecordType);
        this.records.add(locationRecord);
        
        return;
    }
}
