//
// AgendaElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.rules.check.agenda.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class AgendaElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the AgendaElement Id.
     *
     *  @return the agenda element Id
     */

    public static org.osid.id.Id getAgendaEntityId() {
        return (makeEntityId("osid.rules.check.Agenda"));
    }


    /**
     *  Gets the InstructionId element Id.
     *
     *  @return the InstructionId element Id
     */

    public static org.osid.id.Id getInstructionId() {
        return (makeQueryElementId("osid.rules.check.agenda.InstructionId"));
    }


    /**
     *  Gets the Instruction element Id.
     *
     *  @return the Instruction element Id
     */

    public static org.osid.id.Id getInstruction() {
        return (makeQueryElementId("osid.rules.check.agenda.Instruction"));
    }


    /**
     *  Gets the EngineId element Id.
     *
     *  @return the EngineId element Id
     */

    public static org.osid.id.Id getEngineId() {
        return (makeQueryElementId("osid.rules.check.agenda.EngineId"));
    }


    /**
     *  Gets the Engine element Id.
     *
     *  @return the Engine element Id
     */

    public static org.osid.id.Id getEngine() {
        return (makeQueryElementId("osid.rules.check.agenda.Engine"));
    }
}
