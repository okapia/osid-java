//
// AbstractIndexedMapAvailabilityLookupSession.java
//
//    A simple framework for providing an Availability lookup service
//    backed by a fixed collection of availabilities with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an Availability lookup service backed by a
 *  fixed collection of availabilities. The availabilities are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some availabilities may be compatible
 *  with more types than are indicated through these availability
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Availabilities</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapAvailabilityLookupSession
    extends AbstractMapAvailabilityLookupSession
    implements org.osid.resourcing.AvailabilityLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.resourcing.Availability> availabilitiesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.resourcing.Availability>());
    private final MultiMap<org.osid.type.Type, org.osid.resourcing.Availability> availabilitiesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.resourcing.Availability>());


    /**
     *  Makes an <code>Availability</code> available in this session.
     *
     *  @param  availability an availability
     *  @throws org.osid.NullArgumentException <code>availability<code> is
     *          <code>null</code>
     */

    @Override
    protected void putAvailability(org.osid.resourcing.Availability availability) {
        super.putAvailability(availability);

        this.availabilitiesByGenus.put(availability.getGenusType(), availability);
        
        try (org.osid.type.TypeList types = availability.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.availabilitiesByRecord.put(types.getNextType(), availability);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an availability from this session.
     *
     *  @param availabilityId the <code>Id</code> of the availability
     *  @throws org.osid.NullArgumentException <code>availabilityId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeAvailability(org.osid.id.Id availabilityId) {
        org.osid.resourcing.Availability availability;
        try {
            availability = getAvailability(availabilityId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.availabilitiesByGenus.remove(availability.getGenusType());

        try (org.osid.type.TypeList types = availability.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.availabilitiesByRecord.remove(types.getNextType(), availability);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeAvailability(availabilityId);
        return;
    }


    /**
     *  Gets an <code>AvailabilityList</code> corresponding to the given
     *  availability genus <code>Type</code> which does not include
     *  availabilities of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known availabilities or an error results. Otherwise,
     *  the returned list may contain only those availabilities that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  availabilityGenusType an availability genus type 
     *  @return the returned <code>Availability</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>availabilityGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityList getAvailabilitiesByGenusType(org.osid.type.Type availabilityGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.resourcing.availability.ArrayAvailabilityList(this.availabilitiesByGenus.get(availabilityGenusType)));
    }


    /**
     *  Gets an <code>AvailabilityList</code> containing the given
     *  availability record <code>Type</code>. In plenary mode, the
     *  returned list contains all known availabilities or an error
     *  results. Otherwise, the returned list may contain only those
     *  availabilities that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  availabilityRecordType an availability record type 
     *  @return the returned <code>availability</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>availabilityRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityList getAvailabilitiesByRecordType(org.osid.type.Type availabilityRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.resourcing.availability.ArrayAvailabilityList(this.availabilitiesByRecord.get(availabilityRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.availabilitiesByGenus.clear();
        this.availabilitiesByRecord.clear();

        super.close();

        return;
    }
}
