//
// AbstractNodePollsHierarchySession.java
//
//     Defines a Polls hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.voting.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a polls hierarchy session for delivering a hierarchy
 *  of polls using the PollsNode interface.
 */

public abstract class AbstractNodePollsHierarchySession
    extends net.okapia.osid.jamocha.voting.spi.AbstractPollsHierarchySession
    implements org.osid.voting.PollsHierarchySession {

    private java.util.Collection<org.osid.voting.PollsNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root polls <code> Ids </code> in this hierarchy.
     *
     *  @return the root polls <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootPollsIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.voting.pollsnode.PollsNodeToIdList(this.roots));
    }


    /**
     *  Gets the root polls in the polls hierarchy. A node
     *  with no parents is an orphan. While all polls <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root polls 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.PollsList getRootPolls()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.voting.pollsnode.PollsNodeToPollsList(new net.okapia.osid.jamocha.voting.pollsnode.ArrayPollsNodeList(this.roots)));
    }


    /**
     *  Adds a root polls node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootPolls(org.osid.voting.PollsNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root polls nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootPolls(java.util.Collection<org.osid.voting.PollsNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root polls node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootPolls(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.voting.PollsNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Polls </code> has any parents. 
     *
     *  @param  pollsId a polls <code> Id </code> 
     *  @return <code> true </code> if the polls has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> pollsId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> pollsId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getPollsNode(pollsId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of a
     *  polls.
     *
     *  @param  id an <code> Id </code> 
     *  @param  pollsId the <code> Id </code> of a polls 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> pollsId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> pollsId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> pollsId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfPolls(org.osid.id.Id id, org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.voting.PollsNodeList parents = getPollsNode(pollsId).getParentPollsNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextPollsNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given polls. 
     *
     *  @param  pollsId a polls <code> Id </code> 
     *  @return the parent <code> Ids </code> of the polls 
     *  @throws org.osid.NotFoundException <code> pollsId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> pollsId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentPollsIds(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.voting.polls.PollsToIdList(getParentPolls(pollsId)));
    }


    /**
     *  Gets the parents of the given polls. 
     *
     *  @param  pollsId the <code> Id </code> to query 
     *  @return the parents of the polls 
     *  @throws org.osid.NotFoundException <code> pollsId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> pollsId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.PollsList getParentPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.voting.pollsnode.PollsNodeToPollsList(getPollsNode(pollsId).getParentPollsNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of a
     *  polls.
     *
     *  @param  id an <code> Id </code> 
     *  @param  pollsId the Id of a polls 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> pollsId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> pollsId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> pollsId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfPolls(org.osid.id.Id id, org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfPolls(id, pollsId)) {
            return (true);
        }

        try (org.osid.voting.PollsList parents = getParentPolls(pollsId)) {
            while (parents.hasNext()) {
                if (isAncestorOfPolls(id, parents.getNextPolls().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if a polls has any children. 
     *
     *  @param  pollsId a polls <code> Id </code> 
     *  @return <code> true </code> if the <code> pollsId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> pollsId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> pollsId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getPollsNode(pollsId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of a
     *  polls.
     *
     *  @param  id an <code> Id </code> 
     *  @param pollsId the <code> Id </code> of a 
     *         polls
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> pollsId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> pollsId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> pollsId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfPolls(org.osid.id.Id id, org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfPolls(pollsId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  polls.
     *
     *  @param  pollsId the <code> Id </code> to query 
     *  @return the children of the polls 
     *  @throws org.osid.NotFoundException <code> pollsId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> pollsId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildPollsIds(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.voting.polls.PollsToIdList(getChildPolls(pollsId)));
    }


    /**
     *  Gets the children of the given polls. 
     *
     *  @param  pollsId the <code> Id </code> to query 
     *  @return the children of the polls 
     *  @throws org.osid.NotFoundException <code> pollsId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> pollsId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.PollsList getChildPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.voting.pollsnode.PollsNodeToPollsList(getPollsNode(pollsId).getChildPollsNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of a
     *  polls.
     *
     *  @param  id an <code> Id </code> 
     *  @param pollsId the <code> Id </code> of a 
     *         polls
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> pollsId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> pollsId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> pollsId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfPolls(org.osid.id.Id id, org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfPolls(pollsId, id)) {
            return (true);
        }

        try (org.osid.voting.PollsList children = getChildPolls(pollsId)) {
            while (children.hasNext()) {
                if (isDescendantOfPolls(id, children.getNextPolls().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  polls.
     *
     *  @param  pollsId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified polls node 
     *  @throws org.osid.NotFoundException <code> pollsId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> pollsId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getPollsNodeIds(org.osid.id.Id pollsId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.voting.pollsnode.PollsNodeToNode(getPollsNode(pollsId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given polls.
     *
     *  @param  pollsId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified polls node 
     *  @throws org.osid.NotFoundException <code> pollsId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> pollsId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.PollsNode getPollsNodes(org.osid.id.Id pollsId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getPollsNode(pollsId));
    }


    /**
     *  Closes this <code>PollsHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets a polls node.
     *
     *  @param pollsId the id of the polls node
     *  @throws org.osid.NotFoundException <code>pollsId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>pollsId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.voting.PollsNode getPollsNode(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(pollsId, "polls Id");
        for (org.osid.voting.PollsNode polls : this.roots) {
            if (polls.getId().equals(pollsId)) {
                return (polls);
            }

            org.osid.voting.PollsNode r = findPolls(polls, pollsId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(pollsId + " is not found");
    }


    protected org.osid.voting.PollsNode findPolls(org.osid.voting.PollsNode node, 
                                                  org.osid.id.Id pollsId) 
        throws org.osid.OperationFailedException {

        try (org.osid.voting.PollsNodeList children = node.getChildPollsNodes()) {
            while (children.hasNext()) {
                org.osid.voting.PollsNode polls = children.getNextPollsNode();
                if (polls.getId().equals(pollsId)) {
                    return (polls);
                }
                
                polls = findPolls(polls, pollsId);
                if (polls != null) {
                    return (polls);
                }
            }
        }

        return (null);
    }
}
