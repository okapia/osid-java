//
// AbstractImmutableRecurringEvent.java
//
//     Wraps a mutable RecurringEvent to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.calendaring.recurringevent.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>RecurringEvent</code> to hide modifiers. This
 *  wrapper provides an immutized RecurringEvent from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying recurringEvent whose state changes are visible.
 */

public abstract class AbstractImmutableRecurringEvent
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRule
    implements org.osid.calendaring.RecurringEvent {

    private final org.osid.calendaring.RecurringEvent recurringEvent;


    /**
     *  Constructs a new <code>AbstractImmutableRecurringEvent</code>.
     *
     *  @param recurringEvent the recurring event to immutablize
     *  @throws org.osid.NullArgumentException <code>recurringEvent</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableRecurringEvent(org.osid.calendaring.RecurringEvent recurringEvent) {
        super(recurringEvent);
        this.recurringEvent = recurringEvent;
        return;
    }


    /**
     *  Tests if this <code> Containable </code> is sequestered in
     *  that it should not appear outside of its aggregated
     *  event.
     *
     *  @return <code> true </code> if this containable is
     *          sequestered, <code> false </code> if this containable
     *          may appear outside its aggregate
     */

    @OSID @Override
    public boolean isSequestered() {
        return (this.recurringEvent.isSequestered());
    }


    /**
     *  Gets the <code> Schedule Id </code> of this composite event. 
     *
     *  @return the recurring schedule <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getScheduleIds() {
        return (this.recurringEvent.getScheduleIds());
    }


    /**
     *  Gets the recurring schedule in this composite event. 
     *
     *  @return the recurring schedules 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleList getSchedules()
        throws org.osid.OperationFailedException {

        return (this.recurringEvent.getSchedules());
    }


    /**
     *  Gets the superseding event <code> Ids. </code> A superseding event is 
     *  one that replaces another in a series. 
     *
     *  @return list of superseding event <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getSupersedingEventIds() {
        return (this.recurringEvent.getSupersedingEventIds());
    }


    /**
     *  Gets the superseding events. A superseding event is one that replaces 
     *  another in a series. 
     *
     *  @return list of superseding events 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventList getSupersedingEvents()
        throws org.osid.OperationFailedException {

        return (this.recurringEvent.getSupersedingEvents());
    }


    /**
     *  Gets specific dates for this event outside of the schedules. Specific 
     *  dates are excluded from blackouts. 
     *
     *  @return speciifc list of dates 
     */

    @OSID @Override
    public org.osid.calendaring.MeetingTimeList getSpecificMeetingTimes() {
        return (this.recurringEvent.getSpecificMeetingTimes());
    }


    /**
     *  Gets the composed event <code> Ids. </code>
     *
     *  @return list of event <code> Ids </code>
     */

    @OSID @Override
    public org.osid.id.IdList getEventIds() {
        return (this.recurringEvent.getEventIds());
    }
    

    /**
     *  Gets the composed events.
     *
     *  @return list of events
     *  @throws org.osid.OperationFailedException unable to complete request
     */

    @OSID @Override
    public org.osid.calendaring.EventList getEvents()
        throws org.osid.OperationFailedException {

        return (this.recurringEvent.getEvents());
    }


    /**
     *  Gets the blackout dates for this recurring event. Recurring events 
     *  overlapping with the time intervals do not appear in their series. 
     *
     *  @return recurring event exceptions 
     */

    @OSID @Override
    public org.osid.calendaring.DateTimeIntervalList getBlackouts() {
        return (this.recurringEvent.getBlackouts());
    }


    /**
     *  Tests if a sponsor is associated with the overall recurring event. 
     *
     *  @return <code> true </code> if there is an associated sponsor. <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasSponsors() {
        return (this.recurringEvent.hasSponsors());
    }


    /**
     *  Gets the <code> Id </code> of the recurring event sponsors. 
     *
     *  @return the sponsor <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> hasSponsors() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getSponsorIds() {
        return (this.recurringEvent.getSponsorIds());
    }


    /**
     *  Gets the <code> Sponsors. </code> 
     *
     *  @return the sponsors 
     *  @throws org.osid.IllegalStateException <code> hasSponsors() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getSponsors()
        throws org.osid.OperationFailedException {

        return (this.recurringEvent.getSponsors());
    }


    /**
     *  Gets the recurring event record corresponding to the given <code> 
     *  RecurringEvent </code> record <code> Type. </code> This method is used 
     *  to retrieve an object implementing the requested record. The <code> 
     *  recurringEventRecordType </code> may be the <code> Type </code> 
     *  returned in <code> getRecordTypes() </code> or any of its parents in a 
     *  <code> Type </code> hierarchy where <code> 
     *  hasRecordType(recurringEventRecordType) </code> is <code> true </code> 
     *  . 
     *
     *  @param  recurringEventRecordType the type of the record to retrieve 
     *  @return the recurring event record 
     *  @throws org.osid.NullArgumentException <code> recurringEventRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(recurringEventRecordType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.records.RecurringEventRecord getRecurringEventRecord(org.osid.type.Type recurringEventRecordType)
        throws org.osid.OperationFailedException {

        return (this.recurringEvent.getRecurringEventRecord(recurringEventRecordType));
    }
}

