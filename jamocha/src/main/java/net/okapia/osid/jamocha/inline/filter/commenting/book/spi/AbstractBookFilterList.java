//
// AbstractBookList
//
//     Implements a filter for a BookList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.commenting.book.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filter for a BookList. Subclasses call this
 *  constructor and implement the <code>pass()</code> method. This
 *  filter is synchronous but can be wrapped in a BufferedBookList
 *  to improve performance.
 */

public abstract class AbstractBookFilterList
    extends net.okapia.osid.jamocha.commenting.book.spi.AbstractBookList
    implements org.osid.commenting.BookList,
               net.okapia.osid.jamocha.inline.filter.commenting.book.BookFilter {

    private org.osid.commenting.Book book;
    private final org.osid.commenting.BookList list;
    private org.osid.OsidException error;


    /**
     *  Creates a new <code>AbstractBookFilterList</code>.
     *
     *  @param bookList a <code>BookList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>bookList</code> is <code>null</code>
     */

    protected AbstractBookFilterList(org.osid.commenting.BookList bookList) {
        nullarg(bookList, "book list");
        this.list = bookList;
        return;
    }    

        
    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in
     *          this list, <code> false </code> if the end of the list
     *          has been reached
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        if (hasError()) {
            return (true);
        }

        prime();

        if (this.book == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the next <code> Book </code> in this list. 
     *
     *  @return the next <code> Book </code> in this list. The <code> 
     *          hasNext() </code> method should be used to test that a next 
     *          <code> Book </code> is available before calling this method. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.commenting.Book getNextBook()
        throws org.osid.OperationFailedException {

        if (hasError()) {
            throw new org.osid.OperationFailedException(this.error);
        }

        if (hasNext()) {
            org.osid.commenting.Book book = this.book;
            this.book = null;
            return (book);
        } else {
            throw new org.osid.IllegalStateException("no more elements available in book list");
        }
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.book = null;
        this.list.close();
        return;
    }

    
    /**
     *  Filters Books.
     *
     *  @param book the book to filter
     *  @return <code>true</code> if the book passes the filter,
     *          <code>false</code> if the book should be filtered
     */

    public abstract boolean pass(org.osid.commenting.Book book);


    protected void prime() {
        if (this.book != null) {
            return;
        }

        org.osid.commenting.Book book = null;

        while (this.list.hasNext()) {
            try {
                book = this.list.getNextBook();
            } catch (org.osid.OsidException oe) {
                error(oe);
                return;
            }

            if (pass(book)) {
                this.book = book;
                return;
            }
        }

        return;
    }


    /**
     *  Set an error to be thrown on the next retrieval.
     *
     *  @param error
     *  @throws org.osid.IllegalStateException this list already closed
     */

    protected void error(org.osid.OsidException error) {
        this.error = error;
        return;
    }


    /**
     *  Tests if an error exists.
     *
     *  @return <code>true</code> if an error has occurred,
     *          <code>false</code> otherwise
     */

    protected boolean hasError() {
        if (this.error == null) {
            return (false);
        } else {
            return (true);
        }
    }
}
