//
// SingleAuthorizationEnablerList.java
//
//     Implements an AuthorizationEnablerList based on a single AuthorizationEnabler.
//
//
// Tom Coppeto
// OnTapSolutions
// 29 June 2008
//
//
// Copyright (c) 2008, 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authorization.rules.authorizationenabler;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements an ObjectList based on a single AuthorizationEnabler.
 */

public final class SingleAuthorizationEnablerList
    extends net.okapia.osid.jamocha.authorization.rules.authorizationenabler.spi.AbstractAuthorizationEnablerList
    implements org.osid.authorization.rules.AuthorizationEnablerList {

    private final org.osid.authorization.rules.AuthorizationEnabler authorizationEnabler;
    private boolean read = false;


    /**
     *  Creates a new <code>SingleAuthorizationEnablerList</code>.
     *
     *  @param authorizationEnabler an authorizationenabler
     *  @throws org.osid.NullArgumentException <code>authorizationEnabler</code>
     *          is <code>null</code>
     */

    public SingleAuthorizationEnablerList(org.osid.authorization.rules.AuthorizationEnabler authorizationEnabler) {
        nullarg(authorizationEnabler, "authorizationenabler");
        this.authorizationEnabler = authorizationEnabler;
        return;
    }


    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code>true</code> if more elements are available in
     *          this list, <code>false</code> if the end of the list
     *          has been reached
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        if (this.read) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the next <code>AuthorizationEnabler</code> in this list. 
     *
     *  @return the next <code>AuthorizationEnabler</code> in this list. The
     *          <code> hasNext() </code> method should be used to test
     *          that a next <code>AuthorizationEnabler</code> is available before
     *          calling this method.
     *  @throws org.osid.IllegalStateException no more elements
     *          available in this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnabler getNextAuthorizationEnabler()
        throws org.osid.OperationFailedException {

        if (hasNext()) {
            this.read = true;
            return (this.authorizationEnabler);
        } else {
            throw new org.osid.IllegalStateException("no more elements available in authorizationenabler list");
        }
    }
}
