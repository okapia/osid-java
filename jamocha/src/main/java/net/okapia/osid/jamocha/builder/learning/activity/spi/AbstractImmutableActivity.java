//
// AbstractImmutableActivity.java
//
//     Wraps a mutable Activity to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.learning.activity.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Activity</code> to hide modifiers. This
 *  wrapper provides an immutized Activity from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying activity whose state changes are visible.
 */

public abstract class AbstractImmutableActivity
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.learning.Activity {

    private final org.osid.learning.Activity activity;


    /**
     *  Constructs a new <code>AbstractImmutableActivity</code>.
     *
     *  @param activity the activity to immutablize
     *  @throws org.osid.NullArgumentException <code>activity</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableActivity(org.osid.learning.Activity activity) {
        super(activity);
        this.activity = activity;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the related objective. 
     *
     *  @return the objective <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getObjectiveId() {
        return (this.activity.getObjectiveId());
    }


    /**
     *  Gets the related objective. 
     *
     *  @return the related objective 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.learning.Objective getObjective()
        throws org.osid.OperationFailedException {

        return (this.activity.getObjective());
    }


    /**
     *  Tests if this is an asset based activity. 
     *
     *  @return <code> true </code> if this activity is based on assets, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean isAssetBasedActivity() {
        return (this.activity.isAssetBasedActivity());
    }


    /**
     *  Gets the <code> Ids </code> of any assets associated with this 
     *  activity. 
     *
     *  @return list of asset <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> isAssetBasedActivity() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getAssetIds() {
        return (this.activity.getAssetIds());
    }


    /**
     *  Gets any assets associated with this activity. 
     *
     *  @return list of assets 
     *  @throws org.osid.IllegalStateException <code> isAssetBasedActivity() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.repository.AssetList getAssets()
        throws org.osid.OperationFailedException {

        return (this.activity.getAssets());
    }


    /**
     *  Tests if this is a course based activity. 
     *
     *  @return <code> true </code> if this activity is based on courses, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean isCourseBasedActivity() {
        return (this.activity.isCourseBasedActivity());
    }


    /**
     *  Gets the <code> Ids </code> of any courses associated with this 
     *  activity. 
     *
     *  @return list of course <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> isCourseBasedActivity() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getCourseIds() {
        return (this.activity.getCourseIds());
    }


    /**
     *  Gets any courses associated with this activity. 
     *
     *  @return list of courses 
     *  @throws org.osid.IllegalStateException <code> isCourseBasedActivity() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.CourseList getCourses()
        throws org.osid.OperationFailedException {

        return (this.activity.getCourses());
    }


    /**
     *  Tests if this is an assessment based activity. These assessments are 
     *  for learning the objective and not for assessing prodiciency in the 
     *  objective. 
     *
     *  @return <code> true </code> if this activity is based on assessments, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean isAssessmentBasedActivity() {
        return (this.activity.isAssessmentBasedActivity());
    }


    /**
     *  Gets the <code> Ids </code> of any assessments associated with this 
     *  activity. 
     *
     *  @return list of assessment <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> 
     *          isAssessmentBasedActivity() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getAssessmentIds() {
        return (this.activity.getAssessmentIds());
    }


    /**
     *  Gets any assessments associated with this activity. 
     *
     *  @return list of assessments 
     *  @throws org.osid.IllegalStateException <code> 
     *          isAssessmentBasedActivity() </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentList getAssessments()
        throws org.osid.OperationFailedException {

        return (this.activity.getAssessments());
    }


    /**
     *  Gets the activity record corresponding to the given <code> Activity 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record. The <code> 
     *  activityRecordType </code> may be the <code> Type </code> returned in 
     *  <code> getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(activityRecordType) 
     *  </code> is <code> true </code> . 
     *
     *  @param  activityRecordType the type of the record to retrieve 
     *  @return the activity record 
     *  @throws org.osid.NullArgumentException <code> activityRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(activityRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.records.ActivityRecord getActivityRecord(org.osid.type.Type activityRecordType)
        throws org.osid.OperationFailedException {

        return (this.activity.getActivityRecord(activityRecordType));
    }
}

