//
// InvariantMapFileLookupSession
//
//    Implements a File lookup service backed by a fixed collection of
//    files.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.filing;


/**
 *  Implements a File lookup service backed by a fixed
 *  collection of files. The files are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapFileLookupSession
    extends net.okapia.osid.jamocha.core.filing.spi.AbstractMapFileLookupSession
    implements org.osid.filing.FileLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapFileLookupSession</code> with no
     *  files.
     *  
     *  @param directory the directory
     *  @throws org.osid.NullArgumnetException {@code directory} is
     *          {@code null}
     */

    public InvariantMapFileLookupSession(org.osid.filing.Directory directory) {
        setDirectory(directory);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapFileLookupSession</code> with a single
     *  file.
     *  
     *  @param directory the directory
     *  @param file a single file
     *  @throws org.osid.NullArgumentException {@code directory} or
     *          {@code file} is <code>null</code>
     */

      public InvariantMapFileLookupSession(org.osid.filing.Directory directory,
                                               org.osid.filing.File file) {
        this(directory);
        putFile(file);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapFileLookupSession</code> using an array
     *  of files.
     *  
     *  @param directory the directory
     *  @param files an array of files
     *  @throws org.osid.NullArgumentException {@code directory} or
     *          {@code files} is <code>null</code>
     */

      public InvariantMapFileLookupSession(org.osid.filing.Directory directory,
                                               org.osid.filing.File[] files) {
        this(directory);
        putFiles(files);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapFileLookupSession</code> using a
     *  collection of files.
     *
     *  @param directory the directory
     *  @param files a collection of files
     *  @throws org.osid.NullArgumentException {@code directory} or
     *          {@code files} is <code>null</code>
     */

      public InvariantMapFileLookupSession(org.osid.filing.Directory directory,
                                               java.util.Collection<? extends org.osid.filing.File> files) {
        this(directory);
        putFiles(files);
        return;
    }
}
