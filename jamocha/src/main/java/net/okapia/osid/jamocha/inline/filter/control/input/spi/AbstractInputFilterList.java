//
// AbstractInputList
//
//     Implements a filter for an InputList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.control.input.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filter for an InputList. Subclasses call this
 *  constructor and implement the <code>pass()</code> method. This
 *  filter is synchronous but can be wrapped in a BufferedInputList
 *  to improve performance.
 */

public abstract class AbstractInputFilterList
    extends net.okapia.osid.jamocha.control.input.spi.AbstractInputList
    implements org.osid.control.InputList,
               net.okapia.osid.jamocha.inline.filter.control.input.InputFilter {

    private org.osid.control.Input input;
    private final org.osid.control.InputList list;
    private org.osid.OsidException error;


    /**
     *  Creates a new <code>AbstractInputFilterList</code>.
     *
     *  @param inputList an <code>InputList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>inputList</code> is <code>null</code>
     */

    protected AbstractInputFilterList(org.osid.control.InputList inputList) {
        nullarg(inputList, "input list");
        this.list = inputList;
        return;
    }    

        
    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in
     *          this list, <code> false </code> if the end of the list
     *          has been reached
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        if (hasError()) {
            return (true);
        }

        prime();

        if (this.input == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the next <code> Input </code> in this list. 
     *
     *  @return the next <code> Input </code> in this list. The <code> 
     *          hasNext() </code> method should be used to test that a next 
     *          <code> Input </code> is available before calling this method. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.control.Input getNextInput()
        throws org.osid.OperationFailedException {

        if (hasError()) {
            throw new org.osid.OperationFailedException(this.error);
        }

        if (hasNext()) {
            org.osid.control.Input input = this.input;
            this.input = null;
            return (input);
        } else {
            throw new org.osid.IllegalStateException("no more elements available in input list");
        }
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.input = null;
        this.list.close();
        return;
    }

    
    /**
     *  Filters Inputs.
     *
     *  @param input the input to filter
     *  @return <code>true</code> if the input passes the filter,
     *          <code>false</code> if the input should be filtered
     */

    public abstract boolean pass(org.osid.control.Input input);


    protected void prime() {
        if (this.input != null) {
            return;
        }

        org.osid.control.Input input = null;

        while (this.list.hasNext()) {
            try {
                input = this.list.getNextInput();
            } catch (org.osid.OsidException oe) {
                error(oe);
                return;
            }

            if (pass(input)) {
                this.input = input;
                return;
            }
        }

        return;
    }


    /**
     *  Set an error to be thrown on the next retrieval.
     *
     *  @param error
     *  @throws org.osid.IllegalStateException this list already closed
     */

    protected void error(org.osid.OsidException error) {
        this.error = error;
        return;
    }


    /**
     *  Tests if an error exists.
     *
     *  @return <code>true</code> if an error has occurred,
     *          <code>false</code> otherwise
     */

    protected boolean hasError() {
        if (this.error == null) {
            return (false);
        } else {
            return (true);
        }
    }
}
