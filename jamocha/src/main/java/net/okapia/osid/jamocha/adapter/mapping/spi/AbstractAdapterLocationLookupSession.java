//
// AbstractAdapterLocationLookupSession.java
//
//    A Location lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.mapping.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Location lookup session adapter.
 */

public abstract class AbstractAdapterLocationLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.mapping.LocationLookupSession {

    private final org.osid.mapping.LocationLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterLocationLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterLocationLookupSession(org.osid.mapping.LocationLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Map/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Map Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getMapId() {
        return (this.session.getMapId());
    }


    /**
     *  Gets the {@code Map} associated with this session.
     *
     *  @return the {@code Map} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.Map getMap()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getMap());
    }


    /**
     *  Tests if this user can perform {@code Location} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupLocations() {
        return (this.session.canLookupLocations());
    }


    /**
     *  A complete view of the {@code Location} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeLocationView() {
        this.session.useComparativeLocationView();
        return;
    }


    /**
     *  A complete view of the {@code Location} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryLocationView() {
        this.session.usePlenaryLocationView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include locations in maps which are children
     *  of this map in the map hierarchy.
     */

    @OSID @Override
    public void useFederatedMapView() {
        this.session.useFederatedMapView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this map only.
     */

    @OSID @Override
    public void useIsolatedMapView() {
        this.session.useIsolatedMapView();
        return;
    }
    
     
    /**
     *  Gets the {@code Location} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Location} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Location} and
     *  retained for compatibility.
     *
     *  @param locationId {@code Id} of the {@code Location}
     *  @return the location
     *  @throws org.osid.NotFoundException {@code locationId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code locationId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.Location getLocation(org.osid.id.Id locationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLocation(locationId));
    }


    /**
     *  Gets a {@code LocationList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  locations specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Locations} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  locationIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Location} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code locationIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.LocationList getLocationsByIds(org.osid.id.IdList locationIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLocationsByIds(locationIds));
    }


    /**
     *  Gets a {@code LocationList} corresponding to the given
     *  location genus {@code Type} which does not include
     *  locations of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  locations or an error results. Otherwise, the returned list
     *  may contain only those locations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  locationGenusType a location genus type 
     *  @return the returned {@code Location} list
     *  @throws org.osid.NullArgumentException
     *          {@code locationGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.LocationList getLocationsByGenusType(org.osid.type.Type locationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLocationsByGenusType(locationGenusType));
    }


    /**
     *  Gets a {@code LocationList} corresponding to the given
     *  location genus {@code Type} and include any additional
     *  locations with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  locations or an error results. Otherwise, the returned list
     *  may contain only those locations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  locationGenusType a location genus type 
     *  @return the returned {@code Location} list
     *  @throws org.osid.NullArgumentException
     *          {@code locationGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.LocationList getLocationsByParentGenusType(org.osid.type.Type locationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLocationsByParentGenusType(locationGenusType));
    }


    /**
     *  Gets a {@code LocationList} containing the given
     *  location record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  locations or an error results. Otherwise, the returned list
     *  may contain only those locations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  locationRecordType a location record type 
     *  @return the returned {@code Location} list
     *  @throws org.osid.NullArgumentException
     *          {@code locationRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.LocationList getLocationsByRecordType(org.osid.type.Type locationRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLocationsByRecordType(locationRecordType));
    }


    /**
     *  Gets all {@code Locations}. 
     *
     *  In plenary mode, the returned list contains all known
     *  locations or an error results. Otherwise, the returned list
     *  may contain only those locations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Locations} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.LocationList getLocations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLocations());
    }
}
