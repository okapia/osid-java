//
// AbstractCandidateQuery.java
//
//     A template for making a Candidate Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.candidate.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for candidates.
 */

public abstract class AbstractCandidateQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQuery
    implements org.osid.voting.CandidateQuery {

    private final java.util.Collection<org.osid.voting.records.CandidateQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the race <code> Id </code> for this query. 
     *
     *  @param  raceId a race <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> raceId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchRaceId(org.osid.id.Id raceId, boolean match) {
        return;
    }


    /**
     *  Clears the race <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRaceIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> RaceQuery </code> is available for querying 
     *  candidates. 
     *
     *  @return <code> true </code> if a race query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a race. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the race query 
     *  @throws org.osid.UnimplementedException <code> supportsRaceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.RaceQuery getRaceQuery() {
        throw new org.osid.UnimplementedException("supportsRaceQuery() is false");
    }


    /**
     *  Clears the race terms. 
     */

    @OSID @Override
    public void clearRaceTerms() {
        return;
    }


    /**
     *  Sets the resource <code> Id </code> for this query. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResourceId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearResourceIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available for querying 
     *  candidates. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsResourceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getResourceQuery() {
        throw new org.osid.UnimplementedException("supportsResourceQuery() is false");
    }


    /**
     *  Clears the resource terms. 
     */

    @OSID @Override
    public void clearResourceTerms() {
        return;
    }


    /**
     *  Sets the vote <code> Id </code> for this query. 
     *
     *  @param  voteId the vote <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> voteId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchVoteId(org.osid.id.Id voteId, boolean match) {
        return;
    }


    /**
     *  Clears the vote <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearVoteIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> VoteQuery </code> is available. 
     *
     *  @return <code> true </code> if a vote query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVoteQuery() {
        return (false);
    }


    /**
     *  Gets the query for a vote. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the vote query 
     *  @throws org.osid.UnimplementedException <code> supportsVoteQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VoteQuery getVoteQuery() {
        throw new org.osid.UnimplementedException("supportsVoteQuery() is false");
    }


    /**
     *  Matches candidates with any vote. 
     *
     *  @param  match <code> true </code> to match any vote, <code> false 
     *          </code> to match candidates with no votes 
     */

    @OSID @Override
    public void matchAnyVote(boolean match) {
        return;
    }


    /**
     *  Clears the vote terms. 
     */

    @OSID @Override
    public void clearVoteTerms() {
        return;
    }


    /**
     *  Sets the polls <code> Id </code> for this query. 
     *
     *  @param  pollsId the polls <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPollsId(org.osid.id.Id pollsId, boolean match) {
        return;
    }


    /**
     *  Clears the polls <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearPollsIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> PollsQuery </code> is available. 
     *
     *  @return <code> true </code> if a polls query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPollsQuery() {
        return (false);
    }


    /**
     *  Gets the query for a polls. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the polls query 
     *  @throws org.osid.UnimplementedException <code> supportsPollsQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.PollsQuery getPollsQuery() {
        throw new org.osid.UnimplementedException("supportsPollsQuery() is false");
    }


    /**
     *  Clears the polls terms. 
     */

    @OSID @Override
    public void clearPollsTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given candidate query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a candidate implementing the requested record.
     *
     *  @param candidateRecordType a candidate record type
     *  @return the candidate query record
     *  @throws org.osid.NullArgumentException
     *          <code>candidateRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(candidateRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.records.CandidateQueryRecord getCandidateQueryRecord(org.osid.type.Type candidateRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.records.CandidateQueryRecord record : this.records) {
            if (record.implementsRecordType(candidateRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(candidateRecordType + " is not supported");
    }


    /**
     *  Adds a record to this candidate query. 
     *
     *  @param candidateQueryRecord candidate query record
     *  @param candidateRecordType candidate record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCandidateQueryRecord(org.osid.voting.records.CandidateQueryRecord candidateQueryRecord, 
                                          org.osid.type.Type candidateRecordType) {

        addRecordType(candidateRecordType);
        nullarg(candidateQueryRecord, "candidate query record");
        this.records.add(candidateQueryRecord);        
        return;
    }
}
