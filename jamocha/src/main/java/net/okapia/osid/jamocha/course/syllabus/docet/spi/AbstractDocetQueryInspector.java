//
// AbstractDocetQueryInspector.java
//
//     A template for making a DocetQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.syllabus.docet.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for docets.
 */

public abstract class AbstractDocetQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQueryInspector
    implements org.osid.course.syllabus.DocetQueryInspector {

    private final java.util.Collection<org.osid.course.syllabus.records.DocetQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the module <code> Id </code> terms. 
     *
     *  @return the module <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getModuleIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the module terms. 
     *
     *  @return the module terms 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleQueryInspector[] getModuleTerms() {
        return (new org.osid.course.syllabus.ModuleQueryInspector[0]);
    }


    /**
     *  Gets the activity unit <code> Id </code> terms. 
     *
     *  @return the activity unit <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getActivityUnitIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the activity unit terms. 
     *
     *  @return the activity unit terms 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitQueryInspector[] getActivityUnitTerms() {
        return (new org.osid.course.ActivityUnitQueryInspector[0]);
    }


    /**
     *  Gets the objective <code> Id </code> query terms. 
     *
     *  @return the objective <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getLearningObjectiveIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the objective query terms. 
     *
     *  @return the objective terms 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQueryInspector[] getLearningObjectiveTerms() {
        return (new org.osid.learning.ObjectiveQueryInspector[0]);
    }


    /**
     *  Gets the in class query terms. 
     *
     *  @return the in class query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getInClassTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the duration query terms. 
     *
     *  @return the duration query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getDurationTerms() {
        return (new org.osid.search.terms.DurationRangeTerm[0]);
    }


    /**
     *  Gets the asset <code> Id </code> query terms. 
     *
     *  @return the asset <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAssetIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the asset query terms. 
     *
     *  @return the asset terms 
     */

    @OSID @Override
    public org.osid.repository.AssetQueryInspector[] getAssetTerms() {
        return (new org.osid.repository.AssetQueryInspector[0]);
    }


    /**
     *  Gets the asset <code> Id </code> query terms. 
     *
     *  @return the asset <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAssessmentIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the asset query terms. 
     *
     *  @return the asset terms 
     */

    @OSID @Override
    public org.osid.repository.AssetQueryInspector[] getAssessmentTerms() {
        return (new org.osid.repository.AssetQueryInspector[0]);
    }


    /**
     *  Gets the course catalog <code> Id </code> terms. 
     *
     *  @return the course catalog <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseCatalogIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the course catalog terms. 
     *
     *  @return the course catalog terms 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQueryInspector[] getCourseCatalogTerms() {
        return (new org.osid.course.CourseCatalogQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given docet query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a docet implementing the requested record.
     *
     *  @param docetRecordType a docet record type
     *  @return the docet query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>docetRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(docetRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.syllabus.records.DocetQueryInspectorRecord getDocetQueryInspectorRecord(org.osid.type.Type docetRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.syllabus.records.DocetQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(docetRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(docetRecordType + " is not supported");
    }


    /**
     *  Adds a record to this docet query. 
     *
     *  @param docetQueryInspectorRecord docet query inspector
     *         record
     *  @param docetRecordType docet record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addDocetQueryInspectorRecord(org.osid.course.syllabus.records.DocetQueryInspectorRecord docetQueryInspectorRecord, 
                                                   org.osid.type.Type docetRecordType) {

        addRecordType(docetRecordType);
        nullarg(docetRecordType, "docet record type");
        this.records.add(docetQueryInspectorRecord);        
        return;
    }
}
