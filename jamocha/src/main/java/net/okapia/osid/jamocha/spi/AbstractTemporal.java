//
// AbstractTemporal.java
//
//     Defines a Temporal object.
//
//
// Tom Coppeto
// Okapia
// 22 January 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a temporal object.  The default implementation calculates
 *  the effectiveness based on the current time using the
 *  <code>getCurrentTime()</code> method. This method can be
 *  overridden to put the object in another time context.
 *
 *  Unless <code>isEffective()</code> is overridden, the start, end,
 *  and current dateTimes must be of the same calendar and time type.
 */

public abstract class AbstractTemporal
    implements org.osid.Temporal {

    private org.osid.calendaring.DateTime start;
    private org.osid.calendaring.DateTime end;
    

    /**
     *  Tests if the current date is within the start end end dates
     *  inclusive.
     *
     *  @return <code> true </code> if this is effective, <code> false
     *          </code> otherwise
     */

    @OSID @Override
    public boolean isEffective() {
        org.osid.calendaring.DateTime current = getCurrentTime();

        if (current.isLess(this.start) || current.isGreater(this.end)) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the current time. This method defaults to Gregorian/UTC
     *  but should be overridden to support different types.
     *
     *  @return the current time
     */

    protected org.osid.calendaring.DateTime getCurrentTime() {
        return (net.okapia.osid.primordium.calendaring.GregorianUTCDateTime.now());
    }


    /**
     *  Gets the start date. 
     *
     *  @return the start date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getStartDate() {
        return (this.start);
    }


    /**
     *  Sets the start date.
     *
     *  @param date the start date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    protected void setStartDate(org.osid.calendaring.DateTime date) {
        nullarg(date, "date");
        this.start = date;
        return;
    }


    /**
     *  Gets the end date. 
     *
     *  @return the end date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getEndDate() {
        return (this.end);
    }


    /**
     *  Sets the end date.
     *
     *  @param date the end date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    protected void setEndDate(org.osid.calendaring.DateTime date) {
        nullarg(date, "date");
        this.end = date;
        return;
    }
}

