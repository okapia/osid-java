//
// AbstractCandidate.java
//
//     Defines a Candidate builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.voting.candidate.spi;


/**
 *  Defines a <code>Candidate</code> builder.
 */

public abstract class AbstractCandidateBuilder<T extends AbstractCandidateBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRelationshipBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.voting.candidate.CandidateMiter candidate;


    /**
     *  Constructs a new <code>AbstractCandidateBuilder</code>.
     *
     *  @param candidate the candidate to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractCandidateBuilder(net.okapia.osid.jamocha.builder.voting.candidate.CandidateMiter candidate) {
        super(candidate);
        this.candidate = candidate;
        return;
    }


    /**
     *  Builds the candidate.
     *
     *  @return the new candidate
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.voting.Candidate build() {
        (new net.okapia.osid.jamocha.builder.validator.voting.candidate.CandidateValidator(getValidations())).validate(this.candidate);
        return (new net.okapia.osid.jamocha.builder.voting.candidate.ImmutableCandidate(this.candidate));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the candidate miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.voting.candidate.CandidateMiter getMiter() {
        return (this.candidate);
    }


    /**
     *  Sets the race.
     *
     *  @param race a race
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>race</code> is <code>null</code>
     */

    public T race(org.osid.voting.Race race) {
        getMiter().setRace(race);
        return (self());
    }


    /**
     *  Sets the resource.
     *
     *  @param resource a resource
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>resource</code> is <code>null</code>
     */

    public T resource(org.osid.resource.Resource resource) {
        getMiter().setResource(resource);
        return (self());
    }


    /**
     *  Adds a Candidate record.
     *
     *  @param record a candidate record
     *  @param recordType the type of candidate record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.voting.records.CandidateRecord record, org.osid.type.Type recordType) {
        getMiter().addCandidateRecord(record, recordType);
        return (self());
    }
}       


