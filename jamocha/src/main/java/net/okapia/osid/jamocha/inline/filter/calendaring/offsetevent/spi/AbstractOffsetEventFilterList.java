//
// AbstractOffsetEventList
//
//     Implements a filter for an OffsetEventList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.calendaring.offsetevent.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filter for an OffsetEventList. Subclasses call this
 *  constructor and implement the <code>pass()</code> method. This
 *  filter is synchronous but can be wrapped in a BufferedOffsetEventList
 *  to improve performance.
 */

public abstract class AbstractOffsetEventFilterList
    extends net.okapia.osid.jamocha.calendaring.offsetevent.spi.AbstractOffsetEventList
    implements org.osid.calendaring.OffsetEventList,
               net.okapia.osid.jamocha.inline.filter.calendaring.offsetevent.OffsetEventFilter {

    private org.osid.calendaring.OffsetEvent offsetEvent;
    private final org.osid.calendaring.OffsetEventList list;
    private org.osid.OsidException error;


    /**
     *  Creates a new <code>AbstractOffsetEventFilterList</code>.
     *
     *  @param offsetEventList an <code>OffsetEventList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>offsetEventList</code> is <code>null</code>
     */

    protected AbstractOffsetEventFilterList(org.osid.calendaring.OffsetEventList offsetEventList) {
        nullarg(offsetEventList, "offset event list");
        this.list = offsetEventList;
        return;
    }    

        
    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in
     *          this list, <code> false </code> if the end of the list
     *          has been reached
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        if (hasError()) {
            return (true);
        }

        prime();

        if (this.offsetEvent == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the next <code> OffsetEvent </code> in this list. 
     *
     *  @return the next <code> OffsetEvent </code> in this list. The <code> 
     *          hasNext() </code> method should be used to test that a next 
     *          <code> OffsetEvent </code> is available before calling this method. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEvent getNextOffsetEvent()
        throws org.osid.OperationFailedException {

        if (hasError()) {
            throw new org.osid.OperationFailedException(this.error);
        }

        if (hasNext()) {
            org.osid.calendaring.OffsetEvent offsetEvent = this.offsetEvent;
            this.offsetEvent = null;
            return (offsetEvent);
        } else {
            throw new org.osid.IllegalStateException("no more elements available in offset event list");
        }
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.offsetEvent = null;
        this.list.close();
        return;
    }

    
    /**
     *  Filters OffsetEvents.
     *
     *  @param offsetEvent the offset event to filter
     *  @return <code>true</code> if the offset event passes the filter,
     *          <code>false</code> if the offset event should be filtered
     */

    public abstract boolean pass(org.osid.calendaring.OffsetEvent offsetEvent);


    protected void prime() {
        if (this.offsetEvent != null) {
            return;
        }

        org.osid.calendaring.OffsetEvent offsetEvent = null;

        while (this.list.hasNext()) {
            try {
                offsetEvent = this.list.getNextOffsetEvent();
            } catch (org.osid.OsidException oe) {
                error(oe);
                return;
            }

            if (pass(offsetEvent)) {
                this.offsetEvent = offsetEvent;
                return;
            }
        }

        return;
    }


    /**
     *  Set an error to be thrown on the next retrieval.
     *
     *  @param error
     *  @throws org.osid.IllegalStateException this list already closed
     */

    protected void error(org.osid.OsidException error) {
        this.error = error;
        return;
    }


    /**
     *  Tests if an error exists.
     *
     *  @return <code>true</code> if an error has occurred,
     *          <code>false</code> otherwise
     */

    protected boolean hasError() {
        if (this.error == null) {
            return (false);
        } else {
            return (true);
        }
    }
}
