//
// AbstractFederatingTrustLookupSession.java
//
//     An abstract federating adapter for a TrustLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.authentication.process.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  TrustLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingTrustLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.authentication.process.TrustLookupSession>
    implements org.osid.authentication.process.TrustLookupSession {

    private boolean parallel = false;
    private org.osid.authentication.Agency agency = new net.okapia.osid.jamocha.nil.authentication.agency.UnknownAgency();


    /**
     *  Constructs a new <code>AbstractFederatingTrustLookupSession</code>.
     */

    protected AbstractFederatingTrustLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.authentication.process.TrustLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Agency/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Agency Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAgencyId() {
        return (this.agency.getId());
    }


    /**
     *  Gets the <code>Agency</code> associated with this 
     *  session.
     *
     *  @return the <code>Agency</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.Agency getAgency()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.agency);
    }


    /**
     *  Sets the <code>Agency</code>.
     *
     *  @param  agency the agency for this session
     *  @throws org.osid.NullArgumentException <code>agency</code>
     *          is <code>null</code>
     */

    protected void setAgency(org.osid.authentication.Agency agency) {
        nullarg(agency, "agency");
        this.agency = agency;
        return;
    }


    /**
     *  Tests if this user can perform <code>Trust</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupTrusts() {
        for (org.osid.authentication.process.TrustLookupSession session : getSessions()) {
            if (session.canLookupTrusts()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Trust</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeTrustView() {
        for (org.osid.authentication.process.TrustLookupSession session : getSessions()) {
            session.useComparativeTrustView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Trust</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryTrustView() {
        for (org.osid.authentication.process.TrustLookupSession session : getSessions()) {
            session.usePlenaryTrustView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include trusts in agencies which are children
     *  of this agency in the agency hierarchy.
     */

    @OSID @Override
    public void useFederatedAgencyView() {
        for (org.osid.authentication.process.TrustLookupSession session : getSessions()) {
            session.useFederatedAgencyView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this agency only.
     */

    @OSID @Override
    public void useIsolatedAgencyView() {
        for (org.osid.authentication.process.TrustLookupSession session : getSessions()) {
            session.useIsolatedAgencyView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Trust</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Trust</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Trust</code> and
     *  retained for compatibility.
     *
     *  @param  trustId <code>Id</code> of the
     *          <code>Trust</code>
     *  @return the trust
     *  @throws org.osid.NotFoundException <code>trustId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>trustId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.process.Trust getTrust(org.osid.id.Id trustId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.authentication.process.TrustLookupSession session : getSessions()) {
            try {
                return (session.getTrust(trustId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(trustId + " not found");
    }


    /**
     *  Gets a <code>TrustList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  trusts specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Trusts</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  trustIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Trust</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>trustIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.process.TrustList getTrustsByIds(org.osid.id.IdList trustIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.authentication.process.trust.MutableTrustList ret = new net.okapia.osid.jamocha.authentication.process.trust.MutableTrustList();

        try (org.osid.id.IdList ids = trustIds) {
            while (ids.hasNext()) {
                ret.addTrust(getTrust(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>TrustList</code> corresponding to the given
     *  trust genus <code>Type</code> which does not include
     *  trusts of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  trusts or an error results. Otherwise, the returned list
     *  may contain only those trusts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  trustGenusType a trust genus type 
     *  @return the returned <code>Trust</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>trustGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.process.TrustList getTrustsByGenusType(org.osid.type.Type trustGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.authentication.process.trust.FederatingTrustList ret = getTrustList();

        for (org.osid.authentication.process.TrustLookupSession session : getSessions()) {
            ret.addTrustList(session.getTrustsByGenusType(trustGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>TrustList</code> corresponding to the given
     *  trust genus <code>Type</code> and include any additional
     *  trusts with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  trusts or an error results. Otherwise, the returned list
     *  may contain only those trusts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  trustGenusType a trust genus type 
     *  @return the returned <code>Trust</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>trustGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.process.TrustList getTrustsByParentGenusType(org.osid.type.Type trustGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.authentication.process.trust.FederatingTrustList ret = getTrustList();

        for (org.osid.authentication.process.TrustLookupSession session : getSessions()) {
            ret.addTrustList(session.getTrustsByParentGenusType(trustGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>TrustList</code> containing the given
     *  trust record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  trusts or an error results. Otherwise, the returned list
     *  may contain only those trusts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  trustRecordType a trust record type 
     *  @return the returned <code>Trust</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>trustRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.process.TrustList getTrustsByRecordType(org.osid.type.Type trustRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.authentication.process.trust.FederatingTrustList ret = getTrustList();

        for (org.osid.authentication.process.TrustLookupSession session : getSessions()) {
            ret.addTrustList(session.getTrustsByRecordType(trustRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>TrustList</code> in the same circle, or the same
     *  level of confidence, as the given trust. In plenary mode, the
     *  returned list contains all known trusts or an error
     *  results. Otherwise, the returned list may contain only those
     *  trusts that are accessible through this session.
     *
     *  This method returns an empty list.
     *
     *  @param  trustId a trust <code>Id</code>
     *  @return the returned <code>Trust</code> list
     *  @throws org.osid.NotFoundException <code>trustId</code> is not found
     *  @throws org.osid.NullArgumentException <code>trustId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.authentication.process.TrustList getCircleOfTrust(org.osid.id.Id trustId)
        throws org.osid.NotFoundException,
        org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.authentication.process.trust.FederatingTrustList ret = getTrustList();

        for (org.osid.authentication.process.TrustLookupSession session : getSessions()) {
            ret.addTrustList(session.getCircleOfTrust(trustId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Trusts</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  trusts or an error results. Otherwise, the returned list
     *  may contain only those trusts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Trusts</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.process.TrustList getTrusts()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.authentication.process.trust.FederatingTrustList ret = getTrustList();

        for (org.osid.authentication.process.TrustLookupSession session : getSessions()) {
            ret.addTrustList(session.getTrusts());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.authentication.process.trust.FederatingTrustList getTrustList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.authentication.process.trust.ParallelTrustList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.authentication.process.trust.CompositeTrustList());
        }
    }
}
