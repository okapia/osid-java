//
// AbstractActivityRegistrationSearch.java
//
//     A template for making an ActivityRegistration Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.registration.activityregistration.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing activity registration searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractActivityRegistrationSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.course.registration.ActivityRegistrationSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.course.registration.records.ActivityRegistrationSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.course.registration.ActivityRegistrationSearchOrder activityRegistrationSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of activity registrations. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  activityRegistrationIds list of activity registrations
     *  @throws org.osid.NullArgumentException
     *          <code>activityRegistrationIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongActivityRegistrations(org.osid.id.IdList activityRegistrationIds) {
        while (activityRegistrationIds.hasNext()) {
            try {
                this.ids.add(activityRegistrationIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongActivityRegistrations</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of activity registration Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getActivityRegistrationIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  activityRegistrationSearchOrder activity registration search order 
     *  @throws org.osid.NullArgumentException
     *          <code>activityRegistrationSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>activityRegistrationSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderActivityRegistrationResults(org.osid.course.registration.ActivityRegistrationSearchOrder activityRegistrationSearchOrder) {
	this.activityRegistrationSearchOrder = activityRegistrationSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.course.registration.ActivityRegistrationSearchOrder getActivityRegistrationSearchOrder() {
	return (this.activityRegistrationSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given activity registration search
     *  record <code> Type. </code> This method must be used to
     *  retrieve an activity registration implementing the requested record.
     *
     *  @param activityRegistrationSearchRecordType an activity registration search record
     *         type
     *  @return the activity registration search record
     *  @throws org.osid.NullArgumentException
     *          <code>activityRegistrationSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(activityRegistrationSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.registration.records.ActivityRegistrationSearchRecord getActivityRegistrationSearchRecord(org.osid.type.Type activityRegistrationSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.course.registration.records.ActivityRegistrationSearchRecord record : this.records) {
            if (record.implementsRecordType(activityRegistrationSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(activityRegistrationSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this activity registration search. 
     *
     *  @param activityRegistrationSearchRecord activity registration search record
     *  @param activityRegistrationSearchRecordType activityRegistration search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addActivityRegistrationSearchRecord(org.osid.course.registration.records.ActivityRegistrationSearchRecord activityRegistrationSearchRecord, 
                                           org.osid.type.Type activityRegistrationSearchRecordType) {

        addRecordType(activityRegistrationSearchRecordType);
        this.records.add(activityRegistrationSearchRecord);        
        return;
    }
}
