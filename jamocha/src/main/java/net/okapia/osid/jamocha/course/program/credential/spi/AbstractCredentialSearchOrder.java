//
// AbstractCredentialSearchOdrer.java
//
//     Defines a CredentialSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.program.credential.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code CredentialSearchOrder}.
 */

public abstract class AbstractCredentialSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectSearchOrder
    implements org.osid.course.program.CredentialSearchOrder {

    private final java.util.Collection<org.osid.course.program.records.CredentialSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specifies a preference for ordering the result set by lifetime. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByLifetime(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  credentialRecordType a credential record type 
     *  @return {@code true} if the credentialRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code credentialRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type credentialRecordType) {
        for (org.osid.course.program.records.CredentialSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(credentialRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  credentialRecordType the credential record type 
     *  @return the credential search order record
     *  @throws org.osid.NullArgumentException
     *          {@code credentialRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(credentialRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.course.program.records.CredentialSearchOrderRecord getCredentialSearchOrderRecord(org.osid.type.Type credentialRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.program.records.CredentialSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(credentialRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(credentialRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this credential. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param credentialRecord the credential search odrer record
     *  @param credentialRecordType credential record type
     *  @throws org.osid.NullArgumentException
     *          {@code credentialRecord} or
     *          {@code credentialRecordTypecredential} is
     *          {@code null}
     */
            
    protected void addCredentialRecord(org.osid.course.program.records.CredentialSearchOrderRecord credentialSearchOrderRecord, 
                                     org.osid.type.Type credentialRecordType) {

        addRecordType(credentialRecordType);
        this.records.add(credentialSearchOrderRecord);
        
        return;
    }
}
