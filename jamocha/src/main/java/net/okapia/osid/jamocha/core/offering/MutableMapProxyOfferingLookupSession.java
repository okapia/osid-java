//
// MutableMapProxyOfferingLookupSession
//
//    Implements an Offering lookup service backed by a collection of
//    offerings that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.offering;


/**
 *  Implements an Offering lookup service backed by a collection of
 *  offerings. The offerings are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of offerings can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyOfferingLookupSession
    extends net.okapia.osid.jamocha.core.offering.spi.AbstractMapOfferingLookupSession
    implements org.osid.offering.OfferingLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyOfferingLookupSession}
     *  with no offerings.
     *
     *  @param catalogue the catalogue
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code catalogue} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyOfferingLookupSession(org.osid.offering.Catalogue catalogue,
                                                  org.osid.proxy.Proxy proxy) {
        setCatalogue(catalogue);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyOfferingLookupSession} with a
     *  single offering.
     *
     *  @param catalogue the catalogue
     *  @param offering an offering
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code catalogue},
     *          {@code offering}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyOfferingLookupSession(org.osid.offering.Catalogue catalogue,
                                                org.osid.offering.Offering offering, org.osid.proxy.Proxy proxy) {
        this(catalogue, proxy);
        putOffering(offering);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyOfferingLookupSession} using an
     *  array of offerings.
     *
     *  @param catalogue the catalogue
     *  @param offerings an array of offerings
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code catalogue},
     *          {@code offerings}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyOfferingLookupSession(org.osid.offering.Catalogue catalogue,
                                                org.osid.offering.Offering[] offerings, org.osid.proxy.Proxy proxy) {
        this(catalogue, proxy);
        putOfferings(offerings);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyOfferingLookupSession} using a
     *  collection of offerings.
     *
     *  @param catalogue the catalogue
     *  @param offerings a collection of offerings
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code catalogue},
     *          {@code offerings}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyOfferingLookupSession(org.osid.offering.Catalogue catalogue,
                                                java.util.Collection<? extends org.osid.offering.Offering> offerings,
                                                org.osid.proxy.Proxy proxy) {
   
        this(catalogue, proxy);
        setSessionProxy(proxy);
        putOfferings(offerings);
        return;
    }

    
    /**
     *  Makes a {@code Offering} available in this session.
     *
     *  @param offering an offering
     *  @throws org.osid.NullArgumentException {@code offering{@code 
     *          is {@code null}
     */

    @Override
    public void putOffering(org.osid.offering.Offering offering) {
        super.putOffering(offering);
        return;
    }


    /**
     *  Makes an array of offerings available in this session.
     *
     *  @param offerings an array of offerings
     *  @throws org.osid.NullArgumentException {@code offerings{@code 
     *          is {@code null}
     */

    @Override
    public void putOfferings(org.osid.offering.Offering[] offerings) {
        super.putOfferings(offerings);
        return;
    }


    /**
     *  Makes collection of offerings available in this session.
     *
     *  @param offerings
     *  @throws org.osid.NullArgumentException {@code offering{@code 
     *          is {@code null}
     */

    @Override
    public void putOfferings(java.util.Collection<? extends org.osid.offering.Offering> offerings) {
        super.putOfferings(offerings);
        return;
    }


    /**
     *  Removes a Offering from this session.
     *
     *  @param offeringId the {@code Id} of the offering
     *  @throws org.osid.NullArgumentException {@code offeringId{@code  is
     *          {@code null}
     */

    @Override
    public void removeOffering(org.osid.id.Id offeringId) {
        super.removeOffering(offeringId);
        return;
    }    
}
