//
// ActiveAssessmentPartFilterList.java
//
//     Implements a filter for active operables.
//
//
// Tom Coppeto
// Okapia
// 17 December 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.assessment.authoring.assessmentpart;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filter for active operables.
 */

public final class ActiveAssessmentPartFilterList
    extends net.okapia.osid.jamocha.inline.filter.assessment.authoring.assessmentpart.spi.AbstractAssessmentPartFilterList
    implements org.osid.assessment.authoring.AssessmentPartList,
               AssessmentPartFilter {


    /**
     *  Creates a new <code>ActiveAssessmentPartFilterList</code>.
     *
     *  @param list an <code>AssessmentPartList</code>
     *  @throws org.osid.NullArgumentException <code>list</code> is
     *          <code>null</code>
     */

    public ActiveAssessmentPartFilterList(org.osid.assessment.authoring.AssessmentPartList list) {
        super(list);
        return;
    }    

    
    /**
     *  Filters AssessmentParts.
     *
     *  @param assessmentPart the assessment part to filter
     *  @return <code>true</code> if the assessment part passes the filter,
     *          <code>false</code> if the assessment part should be filtered
     */

    @Override
    public boolean pass(org.osid.assessment.authoring.AssessmentPart assessmentPart) {
        return (assessmentPart.isActive());
    }
}
