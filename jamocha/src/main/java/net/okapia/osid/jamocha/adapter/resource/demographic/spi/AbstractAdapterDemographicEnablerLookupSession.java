//
// AbstractAdapterDemographicEnablerLookupSession.java
//
//    A DemographicEnabler lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.resource.demographic.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A DemographicEnabler lookup session adapter.
 */

public abstract class AbstractAdapterDemographicEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.resource.demographic.DemographicEnablerLookupSession {

    private final org.osid.resource.demographic.DemographicEnablerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterDemographicEnablerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterDemographicEnablerLookupSession(org.osid.resource.demographic.DemographicEnablerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Bin/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Bin Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBinId() {
        return (this.session.getBinId());
    }


    /**
     *  Gets the {@code Bin} associated with this session.
     *
     *  @return the {@code Bin} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.Bin getBin()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getBin());
    }


    /**
     *  Tests if this user can perform {@code DemographicEnabler} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupDemographicEnablers() {
        return (this.session.canLookupDemographicEnablers());
    }


    /**
     *  A complete view of the {@code DemographicEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeDemographicEnablerView() {
        this.session.useComparativeDemographicEnablerView();
        return;
    }


    /**
     *  A complete view of the {@code DemographicEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryDemographicEnablerView() {
        this.session.usePlenaryDemographicEnablerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include demographic enablers in bins which are children
     *  of this bin in the bin hierarchy.
     */

    @OSID @Override
    public void useFederatedBinView() {
        this.session.useFederatedBinView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this bin only.
     */

    @OSID @Override
    public void useIsolatedBinView() {
        this.session.useIsolatedBinView();
        return;
    }
    

    /**
     *  Only active demographic enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveDemographicEnablerView() {
        this.session.useActiveDemographicEnablerView();
        return;
    }


    /**
     *  Active and inactive demographic enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusDemographicEnablerView() {
        this.session.useAnyStatusDemographicEnablerView();
        return;
    }
    
     
    /**
     *  Gets the {@code DemographicEnabler} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code DemographicEnabler} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code DemographicEnabler} and
     *  retained for compatibility.
     *
     *  In active mode, demographic enablers are returned that are currently
     *  active. In any status mode, active and inactive demographic enablers
     *  are returned.
     *
     *  @param demographicEnablerId {@code Id} of the {@code DemographicEnabler}
     *  @return the demographic enabler
     *  @throws org.osid.NotFoundException {@code demographicEnablerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code demographicEnablerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnabler getDemographicEnabler(org.osid.id.Id demographicEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDemographicEnabler(demographicEnablerId));
    }


    /**
     *  Gets a {@code DemographicEnablerList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  demographicEnablers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code DemographicEnablers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, demographic enablers are returned that are currently
     *  active. In any status mode, active and inactive demographic enablers
     *  are returned.
     *
     *  @param  demographicEnablerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code DemographicEnabler} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code demographicEnablerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerList getDemographicEnablersByIds(org.osid.id.IdList demographicEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDemographicEnablersByIds(demographicEnablerIds));
    }


    /**
     *  Gets a {@code DemographicEnablerList} corresponding to the given
     *  demographic enabler genus {@code Type} which does not include
     *  demographic enablers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  demographic enablers or an error results. Otherwise, the returned list
     *  may contain only those demographic enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, demographic enablers are returned that are currently
     *  active. In any status mode, active and inactive demographic enablers
     *  are returned.
     *
     *  @param  demographicEnablerGenusType a demographicEnabler genus type 
     *  @return the returned {@code DemographicEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code demographicEnablerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerList getDemographicEnablersByGenusType(org.osid.type.Type demographicEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDemographicEnablersByGenusType(demographicEnablerGenusType));
    }


    /**
     *  Gets a {@code DemographicEnablerList} corresponding to the given
     *  demographic enabler genus {@code Type} and include any additional
     *  demographic enablers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  demographic enablers or an error results. Otherwise, the returned list
     *  may contain only those demographic enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, demographic enablers are returned that are currently
     *  active. In any status mode, active and inactive demographic enablers
     *  are returned.
     *
     *  @param  demographicEnablerGenusType a demographicEnabler genus type 
     *  @return the returned {@code DemographicEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code demographicEnablerGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerList getDemographicEnablersByParentGenusType(org.osid.type.Type demographicEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDemographicEnablersByParentGenusType(demographicEnablerGenusType));
    }


    /**
     *  Gets a {@code DemographicEnablerList} containing the given
     *  demographic enabler record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  demographic enablers or an error results. Otherwise, the returned list
     *  may contain only those demographic enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, demographic enablers are returned that are currently
     *  active. In any status mode, active and inactive demographic enablers
     *  are returned.
     *
     *  @param  demographicEnablerRecordType a demographicEnabler record type 
     *  @return the returned {@code DemographicEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code demographicEnablerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerList getDemographicEnablersByRecordType(org.osid.type.Type demographicEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDemographicEnablersByRecordType(demographicEnablerRecordType));
    }


    /**
     *  Gets a {@code DemographicEnablerList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  demographic enablers or an error results. Otherwise, the returned list
     *  may contain only those demographic enablers that are accessible
     *  through this session.
     *  
     *  In active mode, demographic enablers are returned that are currently
     *  active. In any status mode, active and inactive demographic enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code DemographicEnabler} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerList getDemographicEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDemographicEnablersOnDate(from, to));
    }
        

    /**
     *  Gets a {@code DemographicEnablerList } which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  demographic enablers or an error results. Otherwise, the returned list
     *  may contain only those demographic enablers that are accessible
     *  through this session.
     *
     *  In active mode, demographic enablers are returned that are currently
     *  active. In any status mode, active and inactive demographic enablers
     *  are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned {@code DemographicEnabler} list
     *  @throws org.osid.InvalidArgumentException {@code from} is
     *          greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code agent},
     *          {@code from}, or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerList getDemographicEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (this.session.getDemographicEnablersOnDateWithAgent(agentId, from, to));
    }


    /**
     *  Gets all {@code DemographicEnablers}. 
     *
     *  In plenary mode, the returned list contains all known
     *  demographic enablers or an error results. Otherwise, the returned list
     *  may contain only those demographic enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, demographic enablers are returned that are currently
     *  active. In any status mode, active and inactive demographic enablers
     *  are returned.
     *
     *  @return a list of {@code DemographicEnablers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerList getDemographicEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDemographicEnablers());
    }
}
