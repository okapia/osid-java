//
// AbstractCookbookSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recipe.cookbook.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractCookbookSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.recipe.CookbookSearchResults {

    private org.osid.recipe.CookbookList cookbooks;
    private final org.osid.recipe.CookbookQueryInspector inspector;
    private final java.util.Collection<org.osid.recipe.records.CookbookSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractCookbookSearchResults.
     *
     *  @param cookbooks the result set
     *  @param cookbookQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>cookbooks</code>
     *          or <code>cookbookQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractCookbookSearchResults(org.osid.recipe.CookbookList cookbooks,
                                            org.osid.recipe.CookbookQueryInspector cookbookQueryInspector) {
        nullarg(cookbooks, "cookbooks");
        nullarg(cookbookQueryInspector, "cookbook query inspectpr");

        this.cookbooks = cookbooks;
        this.inspector = cookbookQueryInspector;

        return;
    }


    /**
     *  Gets the cookbook list resulting from a search.
     *
     *  @return a cookbook list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.recipe.CookbookList getCookbooks() {
        if (this.cookbooks == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.recipe.CookbookList cookbooks = this.cookbooks;
        this.cookbooks = null;
	return (cookbooks);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.recipe.CookbookQueryInspector getCookbookQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  cookbook search record <code> Type. </code> This method must
     *  be used to retrieve a cookbook implementing the requested
     *  record.
     *
     *  @param cookbookSearchRecordType a cookbook search 
     *         record type 
     *  @return the cookbook search
     *  @throws org.osid.NullArgumentException
     *          <code>cookbookSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(cookbookSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.recipe.records.CookbookSearchResultsRecord getCookbookSearchResultsRecord(org.osid.type.Type cookbookSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.recipe.records.CookbookSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(cookbookSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(cookbookSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record cookbook search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addCookbookRecord(org.osid.recipe.records.CookbookSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "cookbook record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
