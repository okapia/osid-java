//
// AbstractAuthorizationNotificationSession.java
//
//     A template for making AuthorizationNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authorization.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code Authorization} objects. This session is
 *  intended for consumers needing to synchronize their state with
 *  this service without the use of polling. Notifications are
 *  cancelled when this session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code Authorization} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for authorization entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractAuthorizationNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.authorization.AuthorizationNotificationSession {

    private boolean federated = false;
    private boolean implicit  = false;
    private org.osid.authorization.Vault vault = new net.okapia.osid.jamocha.nil.authorization.vault.UnknownVault();


    /**
     *  Gets the {@code Vault/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Vault Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getVaultId() {
        return (this.vault.getId());
    }

    
    /**
     *  Gets the {@code Vault} associated with this 
     *  session.
     *
     *  @return the {@code Vault} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.Vault getVault()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.vault);
    }


    /**
     *  Sets the {@code Vault}.
     *
     *  @param  vault the vault for this session
     *  @throws org.osid.NullArgumentException {@code vault}
     *          is {@code null}
     */

    protected void setVault(org.osid.authorization.Vault vault) {
        nullarg(vault, "vault");
        this.vault = vault;
        return;
    }


    /**
     *  Tests if this user can register for {@code Authorization}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForAuthorizationNotifications() {
        return (true);
    }


    /**
     *  Reliable notifications are desired. In reliable mode,
     *  notifications are to be acknowledged using <code>
     *  acknowledgeAuthorizationNotification() </code>.
     */

    @OSID @Override
    public void reliableAuthorizationNotifications() {
        return;
    }


    /**
     *  Unreliable notifications are desired. In unreliable mode,
     *  notifications do not need to be acknowledged.
     */

    @OSID @Override
    public void unreliableAuthorizationNotifications() {
        return;
    }


    /**
     *  Acknowledge an authorization notification.
     *
     *  @param  notificationId the <code> Id </code> of the notification
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void acknowledgeAuthorizationNotification(org.osid.id.Id notificationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include notifications for authorizations in vaults
     *  which are children of this vault in the vault hierarchy.
     */

    @OSID @Override
    public void useFederatedVaultView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts notifications to this vault only.
     */

    @OSID @Override
    public void useIsolatedVaultView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return {@codetrue</code> if federated view,
     *          {@codefalse</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Sets the view for methods in this session to implicit
     *  authorizations.  An implicit view will include authorizations
     *  derived from other authorizations as a result of the {@code
     *  Qualifier,} {@code Function} or {@code Resource}
     *  hierarchies. This method is the opposite of {@code
     *  explicitAuthorizationView()}.
     */

    @OSID @Override
    public void useImplicitAuthorizationView() {
        this.implicit = false;
        return;
    }


    /**
     *  Sets the view for methods in this session to explicit
     *  authorizations.  An explicit view includes only those
     *  authorizations that were explicitly defined and not
     *  implied. This method is the opposite of {@code
     *  implicitAuthorizationView()}.
     */

    public void useExplicitAuthorizationView() {
        this.implicit = true;
        return;
    }


    /**
     *  Tests if an implicit or explicit view is set.
     *
     *  @return <code>true</code> if explicit and implicit
     *          authorizations should be returned</code>,
     *          <code>false</code> if only explicit authorizations
     */

    protected boolean isImplicit() {
        return (this.implicit);
    }


    /**
     *  Register for notifications of new authorizations. {@code
     *  AuthorizationReceiver.newAuthorization()} is invoked when an
     *  new {@code Authorization} is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewAuthorizations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new authorizations for the given
     *  resource {@code Id}. {@code
     *  AuthorizationReceiver.newAuthorization()} is invoked when a
     *  new {@code Authorization} is created.
     *
     *  @param  resourceId the {@code Id} of the resource to monitor
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForNewAuthorizationsForResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /** 
     *  Register for notifications of new authorizations for the given
     *  function {@code Id}. {@code
     *  AuthorizationReceiver.newAuthorization()} is invoked when a
     *  new {@code Authorization} is created.
     *
     *  @param  functionId the {@code Id} of the function to monitor
     *  @throws org.osid.NullArgumentException {@code functionId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForNewAuthorizationsForFunction(org.osid.id.Id functionId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of updated authorizations. {@code
     *  AuthorizationReceiver.changedAuthorization()} is invoked when
     *  an authorization is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedAuthorizations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of updated authorizations for the
     *  given resource {@code Id}. {@code
     *  AuthorizationReceiver.changedAuthorization()} is invoked when
     *  a {@code Authorization} in this vault is changed.
     *
     *  @param  resourceId the {@code Id} of the resource to monitor
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForChangedAuthorizationsForResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of updated authorizations for the
     *  given function {@code Id}. {@code
     *  AuthorizationReceiver.changedAuthorization()} is invoked when
     *  a {@code Authorization} in this vault is changed.
     *
     *  @param  functionId the {@code Id} of the function to monitor
     *  @throws org.osid.NullArgumentException {@code functionId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForChangedAuthorizationsForFunction(org.osid.id.Id functionId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated
     *  authorization. {@code AuthorizationReceiver.changedAuthorization()} is
     *  invoked when the specified authorization is changed.
     *
     *  @param authorizationId the {@code Id} of the {@code Authorization} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code authorizationId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedAuthorization(org.osid.id.Id authorizationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted authorizations. {@code
     *  AuthorizationReceiver.deletedAuthorization()} is invoked when
     *  an authorization is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedAuthorizations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of deleted authorizations for the
     *  given resource {@code Id}. {@code
     *  AuthorizationReceiver.deletedAuthorization()} is invoked when
     *  a {@code Authorization} is deleted or removed from this vault.
     *
     *  @param  resourceId the {@code Id} of the resource to monitor
     *  @throws org.osid.NullArgumentException {@code resourceId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */
      
    @OSID @Override
    public void registerForDeletedAuthorizationsForResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of deleted authorizations for the
     *  given function {@code Id}. {@code
     *  AuthorizationReceiver.deletedAuthorization()} is invoked when
     *  a {@code Authorization} is deleted or removed from this vault.
     *
     *  @param  functionId the {@code Id} of the function to monitor
     *  @throws org.osid.NullArgumentException {@code functionId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForDeletedAuthorizationsForFunction(org.osid.id.Id functionId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of a deleted authorization. {@code
     *  AuthorizationReceiver.deletedAuthorization()} is invoked when
     *  the specified authorization is deleted.
     *
     *  @param authorizationId the {@code Id} of the
     *          {@code Authorization} to monitor
     *  @throws org.osid.NullArgumentException {@code authorizationId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedAuthorization(org.osid.id.Id authorizationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
