//
// MutableIndexedMapRelationshipEnablerLookupSession
//
//    Implements a RelationshipEnabler lookup service backed by a collection of
//    relationshipEnablers indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.relationship.rules;


/**
 *  Implements a RelationshipEnabler lookup service backed by a collection of
 *  relationship enablers. The relationship enablers are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some relationship enablers may be compatible
 *  with more types than are indicated through these relationship enabler
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of relationship enablers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapRelationshipEnablerLookupSession
    extends net.okapia.osid.jamocha.core.relationship.rules.spi.AbstractIndexedMapRelationshipEnablerLookupSession
    implements org.osid.relationship.rules.RelationshipEnablerLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapRelationshipEnablerLookupSession} with no relationship enablers.
     *
     *  @param family the family
     *  @throws org.osid.NullArgumentException {@code family}
     *          is {@code null}
     */

      public MutableIndexedMapRelationshipEnablerLookupSession(org.osid.relationship.Family family) {
        setFamily(family);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapRelationshipEnablerLookupSession} with a
     *  single relationship enabler.
     *  
     *  @param family the family
     *  @param  relationshipEnabler a single relationshipEnabler
     *  @throws org.osid.NullArgumentException {@code family} or
     *          {@code relationshipEnabler} is {@code null}
     */

    public MutableIndexedMapRelationshipEnablerLookupSession(org.osid.relationship.Family family,
                                                  org.osid.relationship.rules.RelationshipEnabler relationshipEnabler) {
        this(family);
        putRelationshipEnabler(relationshipEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapRelationshipEnablerLookupSession} using an
     *  array of relationship enablers.
     *
     *  @param family the family
     *  @param  relationshipEnablers an array of relationship enablers
     *  @throws org.osid.NullArgumentException {@code family} or
     *          {@code relationshipEnablers} is {@code null}
     */

    public MutableIndexedMapRelationshipEnablerLookupSession(org.osid.relationship.Family family,
                                                  org.osid.relationship.rules.RelationshipEnabler[] relationshipEnablers) {
        this(family);
        putRelationshipEnablers(relationshipEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapRelationshipEnablerLookupSession} using a
     *  collection of relationship enablers.
     *
     *  @param family the family
     *  @param  relationshipEnablers a collection of relationship enablers
     *  @throws org.osid.NullArgumentException {@code family} or
     *          {@code relationshipEnablers} is {@code null}
     */

    public MutableIndexedMapRelationshipEnablerLookupSession(org.osid.relationship.Family family,
                                                  java.util.Collection<? extends org.osid.relationship.rules.RelationshipEnabler> relationshipEnablers) {

        this(family);
        putRelationshipEnablers(relationshipEnablers);
        return;
    }
    

    /**
     *  Makes a {@code RelationshipEnabler} available in this session.
     *
     *  @param  relationshipEnabler a relationship enabler
     *  @throws org.osid.NullArgumentException {@code relationshipEnabler{@code  is
     *          {@code null}
     */

    @Override
    public void putRelationshipEnabler(org.osid.relationship.rules.RelationshipEnabler relationshipEnabler) {
        super.putRelationshipEnabler(relationshipEnabler);
        return;
    }


    /**
     *  Makes an array of relationship enablers available in this session.
     *
     *  @param  relationshipEnablers an array of relationship enablers
     *  @throws org.osid.NullArgumentException {@code relationshipEnablers{@code 
     *          is {@code null}
     */

    @Override
    public void putRelationshipEnablers(org.osid.relationship.rules.RelationshipEnabler[] relationshipEnablers) {
        super.putRelationshipEnablers(relationshipEnablers);
        return;
    }


    /**
     *  Makes collection of relationship enablers available in this session.
     *
     *  @param  relationshipEnablers a collection of relationship enablers
     *  @throws org.osid.NullArgumentException {@code relationshipEnabler{@code  is
     *          {@code null}
     */

    @Override
    public void putRelationshipEnablers(java.util.Collection<? extends org.osid.relationship.rules.RelationshipEnabler> relationshipEnablers) {
        super.putRelationshipEnablers(relationshipEnablers);
        return;
    }


    /**
     *  Removes a RelationshipEnabler from this session.
     *
     *  @param relationshipEnablerId the {@code Id} of the relationship enabler
     *  @throws org.osid.NullArgumentException {@code relationshipEnablerId{@code  is
     *          {@code null}
     */

    @Override
    public void removeRelationshipEnabler(org.osid.id.Id relationshipEnablerId) {
        super.removeRelationshipEnabler(relationshipEnablerId);
        return;
    }    
}
