//
// FinancialsBudgetingBatchManager
//
//     Defines a null FinancialsBudgetingBatchManager.
//
//
// Tom Coppeto
// Okapia
// 8 October 2008
//
//
// Copyright (c) 2008, 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.nil.financials.budgeting.batch;


/**
 *  Defines a do-nothing null <code>FinancialsBudgetingBatchManager</code>.
 */

public final class FinancialsBudgetingBatchManager 
    extends net.okapia.osid.jamocha.financials.budgeting.batch.spi.AbstractFinancialsBudgetingBatchManager
    implements org.osid.financials.budgeting.batch.FinancialsBudgetingBatchManager,
               org.osid.financials.budgeting.batch.FinancialsBudgetingBatchProxyManager {

    private static final String ID           = "urn:osid:okapia.net:identifiers:providers:financials:budgeting:batch:nil";
    private static final String DISPLAY_NAME = "Null Financials Budgeting Batch Service";
    private static final String DESCRIPTION  = "This Financials Budgeting Batch service does absolutely nothing.";
    private static final String VERSION      = "1.0.0";
    private static final String RELEASE_DATE = "1919-1-15 12:40Z";


    /**
     *  Constructs a new <code>FinancialsBudgetingBatchManager</code>.
     */

    public FinancialsBudgetingBatchManager() {
        super(new FinancialsBudgetingBatchProvider());
        return;
    }

    
    static class FinancialsBudgetingBatchProvider     
        extends net.okapia.osid.provider.spi.AbstractServiceProvider
        implements net.okapia.osid.provider.ServiceProvider {

        FinancialsBudgetingBatchProvider() {
            setServiceId(ID);
            setServiceName(net.okapia.osid.primordium.locale.text.eng.us.Plain.valueOf(DISPLAY_NAME));
            setServiceDescription(net.okapia.osid.primordium.locale.text.eng.us.Plain.valueOf(DESCRIPTION));
            setImplementationVersion(VERSION);
            setReleaseDate(RELEASE_DATE);
            setProvider(net.okapia.osid.provider.Providers.OKAPIA.getProvider());

            return;
        }
    }        
}

