//
// BlockMiter.java
//
//     Defines a Block miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.hold.block;


/**
 *  Defines a <code>Block</code> miter for use with the builders.
 */

public interface BlockMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidObjectMiter,
            org.osid.hold.Block {


    /**
     *  Adds an issue.
     *
     *  @param issue an issue
     *  @throws org.osid.NullArgumentException <code>issue</code> is
     *          <code>null</code>
     */

    public void addIssue(org.osid.hold.Issue issue);


    /**
     *  Sets all the issues.
     *
     *  @param issues a collection of issues
     *  @throws org.osid.NullArgumentException <code>issues</code> is
     *          <code>null</code>
     */

    public void setIssues(java.util.Collection<org.osid.hold.Issue> issues);


    /**
     *  Adds a Block record.
     *
     *  @param record a block record
     *  @param recordType the type of block record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addBlockRecord(org.osid.hold.records.BlockRecord record, org.osid.type.Type recordType);
}       


