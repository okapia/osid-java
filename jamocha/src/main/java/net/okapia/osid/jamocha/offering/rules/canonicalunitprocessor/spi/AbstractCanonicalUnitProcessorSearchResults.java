//
// AbstractCanonicalUnitProcessorSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.rules.canonicalunitprocessor.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractCanonicalUnitProcessorSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.offering.rules.CanonicalUnitProcessorSearchResults {

    private org.osid.offering.rules.CanonicalUnitProcessorList canonicalUnitProcessors;
    private final org.osid.offering.rules.CanonicalUnitProcessorQueryInspector inspector;
    private final java.util.Collection<org.osid.offering.rules.records.CanonicalUnitProcessorSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractCanonicalUnitProcessorSearchResults.
     *
     *  @param canonicalUnitProcessors the result set
     *  @param canonicalUnitProcessorQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>canonicalUnitProcessors</code>
     *          or <code>canonicalUnitProcessorQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractCanonicalUnitProcessorSearchResults(org.osid.offering.rules.CanonicalUnitProcessorList canonicalUnitProcessors,
                                            org.osid.offering.rules.CanonicalUnitProcessorQueryInspector canonicalUnitProcessorQueryInspector) {
        nullarg(canonicalUnitProcessors, "canonical unit processors");
        nullarg(canonicalUnitProcessorQueryInspector, "canonical unit processor query inspectpr");

        this.canonicalUnitProcessors = canonicalUnitProcessors;
        this.inspector = canonicalUnitProcessorQueryInspector;

        return;
    }


    /**
     *  Gets the canonical unit processor list resulting from a search.
     *
     *  @return a canonical unit processor list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorList getCanonicalUnitProcessors() {
        if (this.canonicalUnitProcessors == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.offering.rules.CanonicalUnitProcessorList canonicalUnitProcessors = this.canonicalUnitProcessors;
        this.canonicalUnitProcessors = null;
	return (canonicalUnitProcessors);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.offering.rules.CanonicalUnitProcessorQueryInspector getCanonicalUnitProcessorQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  canonical unit processor search record <code> Type. </code> This method must
     *  be used to retrieve a canonicalUnitProcessor implementing the requested
     *  record.
     *
     *  @param canonicalUnitProcessorSearchRecordType a canonicalUnitProcessor search 
     *         record type 
     *  @return the canonical unit processor search
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitProcessorSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(canonicalUnitProcessorSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.rules.records.CanonicalUnitProcessorSearchResultsRecord getCanonicalUnitProcessorSearchResultsRecord(org.osid.type.Type canonicalUnitProcessorSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.offering.rules.records.CanonicalUnitProcessorSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(canonicalUnitProcessorSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(canonicalUnitProcessorSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record canonical unit processor search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addCanonicalUnitProcessorRecord(org.osid.offering.rules.records.CanonicalUnitProcessorSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "canonical unit processor record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
