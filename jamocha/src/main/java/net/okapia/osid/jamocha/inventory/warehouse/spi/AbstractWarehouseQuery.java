//
// AbstractWarehouseQuery.java
//
//     A template for making a Warehouse Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inventory.warehouse.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for warehouses.
 */

public abstract class AbstractWarehouseQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQuery
    implements org.osid.inventory.WarehouseQuery {

    private final java.util.Collection<org.osid.inventory.records.WarehouseQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the item <code> Id </code> for this query. 
     *
     *  @param  itemId an item <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> itemId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchItemId(org.osid.id.Id itemId, boolean match) {
        return;
    }


    /**
     *  Clears the item <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearItemIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> ItemQuery </code> is available. 
     *
     *  @return <code> true </code> if an item query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemQuery() {
        return (false);
    }


    /**
     *  Gets the query for an item. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the item query 
     *  @throws org.osid.UnimplementedException <code> supportsItemQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ItemQuery getItemQuery() {
        throw new org.osid.UnimplementedException("supportsItemQuery() is false");
    }


    /**
     *  Matches warehouses that have any item. 
     *
     *  @param  match <code> true </code> to match warehouses with any item, 
     *          <code> false </code> to match warehouses with no items 
     */

    @OSID @Override
    public void matchAnyItem(boolean match) {
        return;
    }


    /**
     *  Clears the item query terms. 
     */

    @OSID @Override
    public void clearItemTerms() {
        return;
    }


    /**
     *  Sets the catalog <code> Id </code> for this query. 
     *
     *  @param  stockId a stock <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> stockId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchStockId(org.osid.id.Id stockId, boolean match) {
        return;
    }


    /**
     *  Clears the stock <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearStockIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> StockQuery </code> is available. 
     *
     *  @return <code> true </code> if a stock query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStockQuery() {
        return (false);
    }


    /**
     *  Gets the query for a stock. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the stock query 
     *  @throws org.osid.UnimplementedException <code> supportsStockQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockQuery getStockQuery() {
        throw new org.osid.UnimplementedException("supportsStockQuery() is false");
    }


    /**
     *  Matches warehouses that have any stock. 
     *
     *  @param  match <code> true </code> to match courses with any stock, 
     *          <code> false </code> to match courses with no stock 
     */

    @OSID @Override
    public void matchAnyStock(boolean match) {
        return;
    }


    /**
     *  Clears the stock query terms. 
     */

    @OSID @Override
    public void clearStockTerms() {
        return;
    }


    /**
     *  Sets the model <code> Id </code> for this query. 
     *
     *  @param  modelId a model <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> modelId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchModelId(org.osid.id.Id modelId, boolean match) {
        return;
    }


    /**
     *  Clears the model <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearModelIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ModelQuery </code> is available. 
     *
     *  @return <code> true </code> if a model query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModelQuery() {
        return (false);
    }


    /**
     *  Gets the query for an inventory. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the model query 
     *  @throws org.osid.UnimplementedException <code> supportsModelQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ModelQuery getModelQuery() {
        throw new org.osid.UnimplementedException("supportsModelQuery() is false");
    }


    /**
     *  Matches any related model. 
     *
     *  @param  match <code> true </code> to match warehouses with any model, 
     *          <code> false </code> to match warehouses with no models 
     */

    @OSID @Override
    public void matchAnyModel(boolean match) {
        return;
    }


    /**
     *  Clears the model terms. 
     */

    @OSID @Override
    public void clearModelTerms() {
        return;
    }


    /**
     *  Sets the inventory <code> Id </code> for this query. 
     *
     *  @param  itemId an inventory <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> inventoryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchInventoryId(org.osid.id.Id itemId, boolean match) {
        return;
    }


    /**
     *  Clears the inventory <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearInventoryIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> InventoryQuery </code> is available. 
     *
     *  @return <code> true </code> if an inventory query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInventoryQuery() {
        return (false);
    }


    /**
     *  Gets the query for an inventory. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the inventory query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventoryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.InventoryQuery getInventoryQuery() {
        throw new org.osid.UnimplementedException("supportsInventoryQuery() is false");
    }


    /**
     *  Matches warehouses that have any inventory. 
     *
     *  @param  match <code> true </code> to match warehouses with any 
     *          inventory, <code> false </code> to match warehouses with no 
     *          inventories 
     */

    @OSID @Override
    public void matchAnyInventory(boolean match) {
        return;
    }


    /**
     *  Clears the inventory query terms. 
     */

    @OSID @Override
    public void clearInventoryTerms() {
        return;
    }


    /**
     *  Sets the warehouse <code> Id </code> for this query to match 
     *  warehouses that have the specified warehouse as an ancestor. 
     *
     *  @param  warehouseId a warehouse <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorWarehouseId(org.osid.id.Id warehouseId, 
                                         boolean match) {
        return;
    }


    /**
     *  Clears the ancestor warehouse <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorWarehouseIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> WarehouseQuery </code> is available. 
     *
     *  @return <code> true </code> if a warehouse query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorWarehouseQuery() {
        return (false);
    }


    /**
     *  Gets the query for a warehouse. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the warehouse query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorWarehouseQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseQuery getAncestorWarehouseQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorWarehouseQuery() is false");
    }


    /**
     *  Matches warehouses with any warehouse ancestor. 
     *
     *  @param  match <code> true </code> to match warehouses with any 
     *          ancestor, <code> false </code> to match root warehouses 
     */

    @OSID @Override
    public void matchAnyAncestorWarehouse(boolean match) {
        return;
    }


    /**
     *  Clears the ancestor warehouse query terms. 
     */

    @OSID @Override
    public void clearAncestorWarehouseTerms() {
        return;
    }


    /**
     *  Sets the warehouse <code> Id </code> for this query to match 
     *  warehouses that have the specified warehouse as an descendant. 
     *
     *  @param  warehouseId a warehouse <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantWarehouseId(org.osid.id.Id warehouseId, 
                                           boolean match) {
        return;
    }


    /**
     *  Clears the descendant warehouse <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantWarehouseIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> WarehouseQuery </code> is available. 
     *
     *  @return <code> true </code> if a warehouse query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantWarehouseQuery() {
        return (false);
    }


    /**
     *  Gets the query for a warehouse. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the warehouse query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantWarehouseQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseQuery getDescendantWarehouseQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantWarehouseQuery() is false");
    }


    /**
     *  Matches warehouses with any descendant warehouse. 
     *
     *  @param  match <code> true </code> to match warehouses with any 
     *          descendant, <code> false </code> to match leaf warehouses 
     */

    @OSID @Override
    public void matchAnyDescendantWarehouse(boolean match) {
        return;
    }


    /**
     *  Clears the descendant warehouse query terms. 
     */

    @OSID @Override
    public void clearDescendantWarehouseTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given warehouse query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a warehouse implementing the requested record.
     *
     *  @param warehouseRecordType a warehouse record type
     *  @return the warehouse query record
     *  @throws org.osid.NullArgumentException
     *          <code>warehouseRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(warehouseRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.inventory.records.WarehouseQueryRecord getWarehouseQueryRecord(org.osid.type.Type warehouseRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inventory.records.WarehouseQueryRecord record : this.records) {
            if (record.implementsRecordType(warehouseRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(warehouseRecordType + " is not supported");
    }


    /**
     *  Adds a record to this warehouse query. 
     *
     *  @param warehouseQueryRecord warehouse query record
     *  @param warehouseRecordType warehouse record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addWarehouseQueryRecord(org.osid.inventory.records.WarehouseQueryRecord warehouseQueryRecord, 
                                          org.osid.type.Type warehouseRecordType) {

        addRecordType(warehouseRecordType);
        nullarg(warehouseQueryRecord, "warehouse query record");
        this.records.add(warehouseQueryRecord);        
        return;
    }
}
