//
// AbstractResultLookupSession.java
//
//    A starter implementation framework for providing a Result
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Result
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getResults(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractResultLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.offering.ResultLookupSession {

    private boolean pedantic      = false;
    private boolean effectiveonly = false;
    private boolean federated     = false;
    private org.osid.offering.Catalogue catalogue = new net.okapia.osid.jamocha.nil.offering.catalogue.UnknownCatalogue();
    

    /**
     *  Gets the <code>Catalogue/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Catalogue Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCatalogueId() {
        return (this.catalogue.getId());
    }


    /**
     *  Gets the <code>Catalogue</code> associated with this 
     *  session.
     *
     *  @return the <code>Catalogue</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.Catalogue getCatalogue()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.catalogue);
    }


    /**
     *  Sets the <code>Catalogue</code>.
     *
     *  @param  catalogue the catalogue for this session
     *  @throws org.osid.NullArgumentException <code>catalogue</code>
     *          is <code>null</code>
     */

    protected void setCatalogue(org.osid.offering.Catalogue catalogue) {
        nullarg(catalogue, "catalogue");
        this.catalogue = catalogue;
        return;
    }


    /**
     *  Tests if this user can perform <code>Result</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupResults() {
        return (true);
    }


    /**
     *  A complete view of the <code>Result</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeResultView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Result</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryResultView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include results in catalogues which are children
     *  of this catalogue in the catalogue hierarchy.
     */

    @OSID @Override
    public void useFederatedCatalogueView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this catalogue only.
     */

    @OSID @Override
    public void useIsolatedCatalogueView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only results whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveResultView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All results of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveResultView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Result</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Result</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Result</code> and
     *  retained for compatibility.
     *
     *  In effective mode, results are returned that are currently
     *  effective.  In any effective mode, effective results and
     *  those currently expired are returned.
     *
     *  @param  resultId <code>Id</code> of the
     *          <code>Result</code>
     *  @return the result
     *  @throws org.osid.NotFoundException <code>resultId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>resultId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.Result getResult(org.osid.id.Id resultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.offering.ResultList results = getResults()) {
            while (results.hasNext()) {
                org.osid.offering.Result result = results.getNextResult();
                if (result.getId().equals(resultId)) {
                    return (result);
                }
            }
        } 

        throw new org.osid.NotFoundException(resultId + " not found");
    }


    /**
     *  Gets a <code>ResultList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  results specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Results</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, results are returned that are currently
     *  effective.  In any effective mode, effective results and those
     *  currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getResults()</code>.
     *
     *  @param  resultIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Result</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
*               found
     *  @throws org.osid.NullArgumentException
     *          <code>resultIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.ResultList getResultsByIds(org.osid.id.IdList resultIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.offering.Result> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = resultIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getResult(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("result " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.offering.result.LinkedResultList(ret));
    }


    /**
     *  Gets a <code>ResultList</code> corresponding to the given
     *  result genus <code>Type</code> which does not include
     *  results of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  results or an error results. Otherwise, the returned list
     *  may contain only those results that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, results are returned that are currently
     *  effective.  In any effective mode, effective results and those
     *  currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getResults()</code>.
     *
     *  @param  resultGenusType a result genus type 
     *  @return the returned <code>Result</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>resultGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.ResultList getResultsByGenusType(org.osid.type.Type resultGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.offering.result.ResultGenusFilterList(getResults(), resultGenusType));
    }


    /**
     *  Gets a <code>ResultList</code> corresponding to the given
     *  result genus <code>Type</code> and include any additional
     *  results with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  results or an error results. Otherwise, the returned list
     *  may contain only those results that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, results are returned that are currently
     *  effective.  In any effective mode, effective results and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getResults()</code>.
     *
     *  @param  resultGenusType a result genus type 
     *  @return the returned <code>Result</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>resultGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.ResultList getResultsByParentGenusType(org.osid.type.Type resultGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getResultsByGenusType(resultGenusType));
    }


    /**
     *  Gets a <code>ResultList</code> containing the given
     *  result record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  results or an error results. Otherwise, the returned list
     *  may contain only those results that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, results are returned that are currently
     *  effective.  In any effective mode, effective results and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getResults()</code>.
     *
     *  @param  resultRecordType a result record type 
     *  @return the returned <code>Result</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>resultRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.ResultList getResultsByRecordType(org.osid.type.Type resultRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.offering.result.ResultRecordFilterList(getResults(), resultRecordType));
    }


    /**
     *  Gets a <code>ResultList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  results or an error results. Otherwise, the returned list
     *  may contain only those results that are accessible
     *  through this session.
     *  
     *  In active mode, results are returned that are currently
     *  active. In any status mode, active and inactive results
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Result</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.offering.ResultList getResultsOnDate(org.osid.calendaring.DateTime from, 
                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.offering.result.TemporalResultFilterList(getResults(), from, to));
    }


    /**
     *  Gets a <code>ResultList</code> by genus type and effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  results or an error results. Otherwise, the returned list
     *  may contain only those results that are accessible
     *  through this session.
     *  
     *  In active mode, results are returned that are currently
     *  active. In any status mode, active and inactive results
     *  are returned.
     *
     *  @param resultGenusType a result genus type
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Result</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException
     *          <code>resultGenusType</code>, <code>from</code>, or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.offering.ResultList getResultsByGenusTypeOnDate(org.osid.type.Type resultGenusType,
                                                                    org.osid.calendaring.DateTime from, 
                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.offering.result.TemporalResultFilterList(getResultsByGenusType(resultGenusType), from, to));
    }
        

    /**
     *  Gets an <code> ResultList </code> for the given participant
     *  <code>Id</code>.
     *  
     *  In plenary mode, the returned list contains all of the results
     *  corresponding to the given participant, including duplicates,
     *  or an error results if an result is inaccessible. Otherwise,
     *  inaccessible <code> Results </code> may be omitted from the
     *  list.
     *  
     *  In effective mode, results are returned that are currently
     *  effective.  In any effective mode, effective results and those
     *  currently expired are returned.
     *
     *  @param participantId a participant <code> Id </code>
     *  @return the returned <code> ResultList </code> 
     *  @throws org.osid.NullArgumentException <code> participantId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ResultList getResultsForParticipant(org.osid.id.Id participantId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.offering.result.ResultFilterList(new ParticipantFilter(participantId), getResults()));
    }        


    /**
     *  Gets an <code> ResultList </code> for the given participant
     *  <code>Id</code>.
     *  
     *  In plenary mode, the returned list contains all of the results
     *  corresponding to the given participant, including duplicates,
     *  or an error results if an result is inaccessible. Otherwise,
     *  inaccessible <code> Results </code> may be omitted from the
     *  list.
     *  
     *  In effective mode, results are returned that are currently
     *  effective.  In any effective mode, effective results and those
     *  currently expired are returned.
     *
     *  @param  participantId a participant <code> Id </code> 
     *  @param  resultGenusType a results genus type 
     *  @return the returned <code> ResultList </code> 
     *  @throws org.osid.NullArgumentException <code> participantId </code> or 
     *          <code> resultGenusType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ResultList getResultsByGenusTypeForParticipant(org.osid.id.Id participantId, 
                                                                            org.osid.type.Type resultGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.offering.result.ResultFilterList(new ParticipantFilter(participantId), getResultsByGenusType(resultGenusType)));
    }


    /**
     *  Gets a list of results for a participant and effective during
     *  the entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known results
     *  or an error results. Otherwise, the returned list may contain
     *  only those results that are accessible through this session.
     *  
     *  In effective mode, results are returned that are currently
     *  effective.  In any effective mode, effective results and those
     *  currently expired are returned.
     *
     *  @param  participantId a participant <code> Id </code> 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code> ResultList </code> 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> participantId,
     *          from, </code> or <code> to </code> is <code> null
     *          </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ResultList getResultsForParticipantOnDate(org.osid.id.Id participantId, 
                                                                       org.osid.calendaring.DateTime from, 
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.offering.result.TemporalResultFilterList(getResultsForParticipant(participantId), from, to));
    }


    /**
     *  Gets a list of results for a participant and effective during
     *  the entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known results
     *  or an error results. Otherwise, the returned list may contain
     *  only those results that are accessible through this session.
     *  
     *  In effective mode, results are returned that are currently
     *  effective.  In any effective mode, effective results and those
     *  currently expired are returned.
     *
     *  @param  participantId a participant <code> Id </code> 
     *  @param  resultGenusType a results genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code> ResultList </code> 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> participantId,
     *          resultGenusType, from, </code> or <code> to </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ResultList getResultsByGenusTypeForParticipantOnDate(org.osid.id.Id participantId, 
                                                                                  org.osid.type.Type resultGenusType, 
                                                                                  org.osid.calendaring.DateTime from, 
                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.offering.result.TemporalResultFilterList(getResultsByGenusTypeForParticipant(participantId, resultGenusType), from, to));
    }


    /**
     *  Gets all <code>Results</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  results or an error results. Otherwise, the returned list
     *  may contain only those results that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, results are returned that are currently
     *  effective.  In any effective mode, effective results and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Results</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.offering.ResultList getResults()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the result list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of results
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.offering.ResultList filterResultsOnViews(org.osid.offering.ResultList list)
        throws org.osid.OperationFailedException {

        org.osid.offering.ResultList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.offering.result.EffectiveResultFilterList(ret);
        }

        return (ret);
    }


    public static class ParticipantFilter
        implements net.okapia.osid.jamocha.inline.filter.offering.result.ResultFilter {
         
        private final org.osid.id.Id participantId;
         
         
        /**
         *  Constructs a new <code>ParticipantFilter</code>.
         *
         *  @param participantId the canonical unit to filter
         *  @throws org.osid.NullArgumentException
         *          <code>participantId</code> is <code>null</code>
         */
        
        public ParticipantFilter(org.osid.id.Id participantId) {
            nullarg(participantId, "canonical unit Id");
            this.participantId = participantId;
            return;
        }

         
        /**
         *  Used by the ResultFilterList to filter the 
         *  offering list based on canonical unit.
         *
         *  @param offering the offering
         *  @return <code>true</code> to pass the offering,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.offering.Result offering) {
            return (offering.getParticipantId().equals(this.participantId));
        }
    }
}
