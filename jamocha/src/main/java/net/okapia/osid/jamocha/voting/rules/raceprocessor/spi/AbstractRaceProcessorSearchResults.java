//
// AbstractRaceProcessorSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.rules.raceprocessor.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractRaceProcessorSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.voting.rules.RaceProcessorSearchResults {

    private org.osid.voting.rules.RaceProcessorList raceProcessors;
    private final org.osid.voting.rules.RaceProcessorQueryInspector inspector;
    private final java.util.Collection<org.osid.voting.rules.records.RaceProcessorSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractRaceProcessorSearchResults.
     *
     *  @param raceProcessors the result set
     *  @param raceProcessorQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>raceProcessors</code>
     *          or <code>raceProcessorQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractRaceProcessorSearchResults(org.osid.voting.rules.RaceProcessorList raceProcessors,
                                            org.osid.voting.rules.RaceProcessorQueryInspector raceProcessorQueryInspector) {
        nullarg(raceProcessors, "race processors");
        nullarg(raceProcessorQueryInspector, "race processor query inspectpr");

        this.raceProcessors = raceProcessors;
        this.inspector = raceProcessorQueryInspector;

        return;
    }


    /**
     *  Gets the race processor list resulting from a search.
     *
     *  @return a race processor list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorList getRaceProcessors() {
        if (this.raceProcessors == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.voting.rules.RaceProcessorList raceProcessors = this.raceProcessors;
        this.raceProcessors = null;
	return (raceProcessors);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.voting.rules.RaceProcessorQueryInspector getRaceProcessorQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  race processor search record <code> Type. </code> This method must
     *  be used to retrieve a raceProcessor implementing the requested
     *  record.
     *
     *  @param raceProcessorSearchRecordType a raceProcessor search 
     *         record type 
     *  @return the race processor search
     *  @throws org.osid.NullArgumentException
     *          <code>raceProcessorSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(raceProcessorSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.rules.records.RaceProcessorSearchResultsRecord getRaceProcessorSearchResultsRecord(org.osid.type.Type raceProcessorSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.voting.rules.records.RaceProcessorSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(raceProcessorSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(raceProcessorSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record race processor search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addRaceProcessorRecord(org.osid.voting.rules.records.RaceProcessorSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "race processor record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
