//
// AbstractQueryStoreLookupSession.java
//
//    An inline adapter that maps a StoreLookupSession to
//    a StoreQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.ordering.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a StoreLookupSession to
 *  a StoreQuerySession.
 */

public abstract class AbstractQueryStoreLookupSession
    extends net.okapia.osid.jamocha.ordering.spi.AbstractStoreLookupSession
    implements org.osid.ordering.StoreLookupSession {

    private final org.osid.ordering.StoreQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryStoreLookupSession.
     *
     *  @param querySession the underlying store query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryStoreLookupSession(org.osid.ordering.StoreQuerySession querySession) {
        nullarg(querySession, "store query session");
        this.session = querySession;
        return;
    }



    /**
     *  Tests if this user can perform <code>Store</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupStores() {
        return (this.session.canSearchStores());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }

     
    /**
     *  Gets the <code>Store</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Store</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Store</code> and
     *  retained for compatibility.
     *
     *  @param  storeId <code>Id</code> of the
     *          <code>Store</code>
     *  @return the store
     *  @throws org.osid.NotFoundException <code>storeId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>storeId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.Store getStore(org.osid.id.Id storeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ordering.StoreQuery query = getQuery();
        query.matchId(storeId, true);
        org.osid.ordering.StoreList stores = this.session.getStoresByQuery(query);
        if (stores.hasNext()) {
            return (stores.getNextStore());
        } 
        
        throw new org.osid.NotFoundException(storeId + " not found");
    }


    /**
     *  Gets a <code>StoreList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  stores specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Stores</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  storeIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Store</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>storeIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.StoreList getStoresByIds(org.osid.id.IdList storeIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ordering.StoreQuery query = getQuery();

        try (org.osid.id.IdList ids = storeIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getStoresByQuery(query));
    }


    /**
     *  Gets a <code>StoreList</code> corresponding to the given
     *  store genus <code>Type</code> which does not include
     *  stores of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  stores or an error results. Otherwise, the returned list
     *  may contain only those stores that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  storeGenusType a store genus type 
     *  @return the returned <code>Store</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>storeGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.StoreList getStoresByGenusType(org.osid.type.Type storeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ordering.StoreQuery query = getQuery();
        query.matchGenusType(storeGenusType, true);
        return (this.session.getStoresByQuery(query));
    }


    /**
     *  Gets a <code>StoreList</code> corresponding to the given
     *  store genus <code>Type</code> and include any additional
     *  stores with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  stores or an error results. Otherwise, the returned list
     *  may contain only those stores that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  storeGenusType a store genus type 
     *  @return the returned <code>Store</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>storeGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.StoreList getStoresByParentGenusType(org.osid.type.Type storeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ordering.StoreQuery query = getQuery();
        query.matchParentGenusType(storeGenusType, true);
        return (this.session.getStoresByQuery(query));
    }


    /**
     *  Gets a <code>StoreList</code> containing the given
     *  store record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  stores or an error results. Otherwise, the returned list
     *  may contain only those stores that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  storeRecordType a store record type 
     *  @return the returned <code>Store</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>storeRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.StoreList getStoresByRecordType(org.osid.type.Type storeRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ordering.StoreQuery query = getQuery();
        query.matchRecordType(storeRecordType, true);
        return (this.session.getStoresByQuery(query));
    }


    /**
     *  Gets a <code>StoreList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known stores or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  stores that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Store</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ordering.StoreList getStoresByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ordering.StoreQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getStoresByQuery(query));        
    }

    
    /**
     *  Gets all <code>Stores</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  stores or an error results. Otherwise, the returned list
     *  may contain only those stores that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Stores</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.StoreList getStores()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ordering.StoreQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getStoresByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.ordering.StoreQuery getQuery() {
        org.osid.ordering.StoreQuery query = this.session.getStoreQuery();
        return (query);
    }
}
