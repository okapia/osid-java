//
// AbstractMapChecklistLookupSession
//
//    A simple framework for providing a Checklist lookup service
//    backed by a fixed collection of checklists.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.checklist.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Checklist lookup service backed by a
 *  fixed collection of checklists. The checklists are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Checklists</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapChecklistLookupSession
    extends net.okapia.osid.jamocha.checklist.spi.AbstractChecklistLookupSession
    implements org.osid.checklist.ChecklistLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.checklist.Checklist> checklists = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.checklist.Checklist>());


    /**
     *  Makes a <code>Checklist</code> available in this session.
     *
     *  @param  checklist a checklist
     *  @throws org.osid.NullArgumentException <code>checklist<code>
     *          is <code>null</code>
     */

    protected void putChecklist(org.osid.checklist.Checklist checklist) {
        this.checklists.put(checklist.getId(), checklist);
        return;
    }


    /**
     *  Makes an array of checklists available in this session.
     *
     *  @param  checklists an array of checklists
     *  @throws org.osid.NullArgumentException <code>checklists<code>
     *          is <code>null</code>
     */

    protected void putChecklists(org.osid.checklist.Checklist[] checklists) {
        putChecklists(java.util.Arrays.asList(checklists));
        return;
    }


    /**
     *  Makes a collection of checklists available in this session.
     *
     *  @param  checklists a collection of checklists
     *  @throws org.osid.NullArgumentException <code>checklists<code>
     *          is <code>null</code>
     */

    protected void putChecklists(java.util.Collection<? extends org.osid.checklist.Checklist> checklists) {
        for (org.osid.checklist.Checklist checklist : checklists) {
            this.checklists.put(checklist.getId(), checklist);
        }

        return;
    }


    /**
     *  Removes a Checklist from this session.
     *
     *  @param  checklistId the <code>Id</code> of the checklist
     *  @throws org.osid.NullArgumentException <code>checklistId<code> is
     *          <code>null</code>
     */

    protected void removeChecklist(org.osid.id.Id checklistId) {
        this.checklists.remove(checklistId);
        return;
    }


    /**
     *  Gets the <code>Checklist</code> specified by its <code>Id</code>.
     *
     *  @param  checklistId <code>Id</code> of the <code>Checklist</code>
     *  @return the checklist
     *  @throws org.osid.NotFoundException <code>checklistId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>checklistId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.Checklist getChecklist(org.osid.id.Id checklistId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.checklist.Checklist checklist = this.checklists.get(checklistId);
        if (checklist == null) {
            throw new org.osid.NotFoundException("checklist not found: " + checklistId);
        }

        return (checklist);
    }


    /**
     *  Gets all <code>Checklists</code>. In plenary mode, the returned
     *  list contains all known checklists or an error
     *  results. Otherwise, the returned list may contain only those
     *  checklists that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Checklists</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistList getChecklists()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.checklist.checklist.ArrayChecklistList(this.checklists.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.checklists.clear();
        super.close();
        return;
    }
}
