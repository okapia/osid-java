//
// AbstractQueryEngineLookupSession.java
//
//    An inline adapter that maps an EngineLookupSession to
//    an EngineQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.search.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps an EngineLookupSession to
 *  an EngineQuerySession.
 */

public abstract class AbstractQueryEngineLookupSession
    extends net.okapia.osid.jamocha.search.spi.AbstractEngineLookupSession
    implements org.osid.search.EngineLookupSession {

    private final org.osid.search.EngineQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryEngineLookupSession.
     *
     *  @param querySession the underlying engine query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryEngineLookupSession(org.osid.search.EngineQuerySession querySession) {
        nullarg(querySession, "engine query session");
        this.session = querySession;
        return;
    }



    /**
     *  Tests if this user can perform <code>Engine</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupEngines() {
        return (this.session.canSearchEngines());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }

     
    /**
     *  Gets the <code>Engine</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Engine</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Engine</code> and
     *  retained for compatibility.
     *
     *  @param  engineId <code>Id</code> of the
     *          <code>Engine</code>
     *  @return the engine
     *  @throws org.osid.NotFoundException <code>engineId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>engineId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.search.Engine getEngine(org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.search.EngineQuery query = getQuery();
        query.matchId(engineId, true);
        org.osid.search.EngineList engines = this.session.getEnginesByQuery(query);
        if (engines.hasNext()) {
            return (engines.getNextEngine());
        } 
        
        throw new org.osid.NotFoundException(engineId + " not found");
    }


    /**
     *  Gets an <code>EngineList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  engines specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Engines</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  engineIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Engine</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>engineIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.search.EngineList getEnginesByIds(org.osid.id.IdList engineIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.search.EngineQuery query = getQuery();

        try (org.osid.id.IdList ids = engineIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getEnginesByQuery(query));
    }


    /**
     *  Gets an <code>EngineList</code> corresponding to the given
     *  engine genus <code>Type</code> which does not include
     *  engines of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  engines or an error results. Otherwise, the returned list
     *  may contain only those engines that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  engineGenusType an engine genus type 
     *  @return the returned <code>Engine</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>engineGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.search.EngineList getEnginesByGenusType(org.osid.type.Type engineGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.search.EngineQuery query = getQuery();
        query.matchGenusType(engineGenusType, true);
        return (this.session.getEnginesByQuery(query));
    }


    /**
     *  Gets an <code>EngineList</code> corresponding to the given
     *  engine genus <code>Type</code> and include any additional
     *  engines with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  engines or an error results. Otherwise, the returned list
     *  may contain only those engines that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  engineGenusType an engine genus type 
     *  @return the returned <code>Engine</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>engineGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.search.EngineList getEnginesByParentGenusType(org.osid.type.Type engineGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.search.EngineQuery query = getQuery();
        query.matchParentGenusType(engineGenusType, true);
        return (this.session.getEnginesByQuery(query));
    }


    /**
     *  Gets an <code>EngineList</code> containing the given
     *  engine record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  engines or an error results. Otherwise, the returned list
     *  may contain only those engines that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  engineRecordType an engine record type 
     *  @return the returned <code>Engine</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>engineRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.search.EngineList getEnginesByRecordType(org.osid.type.Type engineRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.search.EngineQuery query = getQuery();
        query.matchRecordType(engineRecordType, true);
        return (this.session.getEnginesByQuery(query));
    }


    /**
     *  Gets an <code>EngineList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known engines or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  engines that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Engine</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.search.EngineList getEnginesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.search.EngineQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getEnginesByQuery(query));        
    }

    
    /**
     *  Gets all <code>Engines</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  engines or an error results. Otherwise, the returned list
     *  may contain only those engines that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Engines</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.search.EngineList getEngines()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.search.EngineQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getEnginesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.search.EngineQuery getQuery() {
        org.osid.search.EngineQuery query = this.session.getEngineQuery();
        return (query);
    }
}
