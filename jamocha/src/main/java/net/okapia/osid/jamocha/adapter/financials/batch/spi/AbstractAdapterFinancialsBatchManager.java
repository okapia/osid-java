//
// AbstractFinancialsBatchManager.java
//
//     An adapter for a FinancialsBatchManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.financials.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a FinancialsBatchManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterFinancialsBatchManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.financials.batch.FinancialsBatchManager>
    implements org.osid.financials.batch.FinancialsBatchManager {


    /**
     *  Constructs a new {@code AbstractAdapterFinancialsBatchManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterFinancialsBatchManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterFinancialsBatchManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterFinancialsBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if bulk administration of accounts is available. 
     *
     *  @return <code> true </code> if an account bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAccountBatchAdmin() {
        return (getAdapteeManager().supportsAccountBatchAdmin());
    }


    /**
     *  Tests if bulk administration of activities is available. 
     *
     *  @return <code> true </code> if an activity bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityBatchAdmin() {
        return (getAdapteeManager().supportsActivityBatchAdmin());
    }


    /**
     *  Tests if bulk administration of fiscal periods is available. 
     *
     *  @return <code> true </code> if a fiscal period bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFiscalPeriodBatchAdmin() {
        return (getAdapteeManager().supportsFiscalPeriodBatchAdmin());
    }


    /**
     *  Tests if bulk administration of businesses is available. 
     *
     *  @return <code> true </code> if a business bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessBatchAdmin() {
        return (getAdapteeManager().supportsBusinessBatchAdmin());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk account 
     *  administration service. 
     *
     *  @return an <code> AccountBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAccountBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.batch.AccountBatchAdminSession getAccountBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAccountBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk account 
     *  administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return an <code> AccountBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAccountBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.batch.AccountBatchAdminSession getAccountBatchAdminSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAccountBatchAdminSessionForBusiness(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk activity 
     *  administration service. 
     *
     *  @return an <code> ActivityBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.batch.ActivityBatchAdminSession getActivityBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk activity 
     *  administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return an <code> ActivityBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.batch.ActivityBatchAdminSession getActivityBatchAdminSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityBatchAdminSessionForBusiness(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk fiscal 
     *  period administration service. 
     *
     *  @return a <code> FiscalPeriodBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.batch.FiscalPeriodBatchAdminSession getFiscalPeriodBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFiscalPeriodBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk fiscal 
     *  period administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> FiscalPeriodBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.batch.FiscalPeriodBatchAdminSession getFiscalPeriodBatchAdminSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getFiscalPeriodBatchAdminSessionForBusiness(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk business 
     *  administration service. 
     *
     *  @return a <code> BusinessBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBusinessBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.batch.BusinessBatchAdminSession getBusinessBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBusinessBatchAdminSession());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
