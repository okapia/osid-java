//
// AbstractActivityRegistrationQuery.java
//
//     A template for making an ActivityRegistration Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.registration.activityregistration.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for activity registrations.
 */

public abstract class AbstractActivityRegistrationQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQuery
    implements org.osid.course.registration.ActivityRegistrationQuery {

    private final java.util.Collection<org.osid.course.registration.records.ActivityRegistrationQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the registration <code> Id </code> for this query. 
     *
     *  @param  registrationId a registration <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> registrationId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchRegistrationId(org.osid.id.Id registrationId, 
                                    boolean match) {
        return;
    }


    /**
     *  Clears the registration <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRegistrationIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> RegistrationQuery </code> is available. 
     *
     *  @return <code> true </code> if a registration query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRegistrationQuery() {
        return (false);
    }


    /**
     *  Gets the query for a registration. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the registration query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRegistrationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationQuery getRegistrationQuery() {
        throw new org.osid.UnimplementedException("supportsRegistrationQuery() is false");
    }


    /**
     *  Clears the registration terms. 
     */

    @OSID @Override
    public void clearRegistrationTerms() {
        return;
    }


    /**
     *  Sets the activity <code> Id </code> for this query. 
     *
     *  @param  activityId an activity <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> activityId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchActivityId(org.osid.id.Id activityId, boolean match) {
        return;
    }


    /**
     *  Clears the activity <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearActivityIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> ActivityQuery </code> is available. 
     *
     *  @return <code> true </code> if an activity query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityQuery() {
        return (false);
    }


    /**
     *  Gets the query for an activity. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the activity query 
     *  @throws org.osid.UnimplementedException <code> supportsActivityQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityQuery getActivityQuery() {
        throw new org.osid.UnimplementedException("supportsActivityQuery() is false");
    }


    /**
     *  Clears the activity terms. 
     */

    @OSID @Override
    public void clearActivityTerms() {
        return;
    }


    /**
     *  Sets the student resource <code> Id </code> for this query. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchStudentId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the student resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearStudentIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStudentQuery() {
        return (false);
    }


    /**
     *  Gets the query for a student. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsStudentQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getStudentQuery() {
        throw new org.osid.UnimplementedException("supportsStudentQuery() is false");
    }


    /**
     *  Clears the student resource terms. 
     */

    @OSID @Override
    public void clearStudentTerms() {
        return;
    }


    /**
     *  Sets the course catalog <code> Id </code> for this query. 
     *
     *  @param  courseCatalogId the course catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseCatalogId(org.osid.id.Id courseCatalogId, 
                                     boolean match) {
        return;
    }


    /**
     *  Clears the course catalog <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseCatalogIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CourseCatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a course catalog query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course catalog. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the course catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQuery getCourseCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsCourseCatalogQuery() is false");
    }


    /**
     *  Clears the course catalog terms. 
     */

    @OSID @Override
    public void clearCourseCatalogTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given activity registration query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an activity registration implementing the requested record.
     *
     *  @param activityRegistrationRecordType an activity registration record type
     *  @return the activity registration query record
     *  @throws org.osid.NullArgumentException
     *          <code>activityRegistrationRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(activityRegistrationRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.registration.records.ActivityRegistrationQueryRecord getActivityRegistrationQueryRecord(org.osid.type.Type activityRegistrationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.registration.records.ActivityRegistrationQueryRecord record : this.records) {
            if (record.implementsRecordType(activityRegistrationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(activityRegistrationRecordType + " is not supported");
    }


    /**
     *  Adds a record to this activity registration query. 
     *
     *  @param activityRegistrationQueryRecord activity registration query record
     *  @param activityRegistrationRecordType activityRegistration record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addActivityRegistrationQueryRecord(org.osid.course.registration.records.ActivityRegistrationQueryRecord activityRegistrationQueryRecord, 
                                          org.osid.type.Type activityRegistrationRecordType) {

        addRecordType(activityRegistrationRecordType);
        nullarg(activityRegistrationQueryRecord, "activity registration query record");
        this.records.add(activityRegistrationQueryRecord);        
        return;
    }
}
