//
// AbstractModuleQueryInspector.java
//
//     A template for making a ModuleQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.syllabus.module.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for modules.
 */

public abstract class AbstractModuleQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidGovernatorQueryInspector
    implements org.osid.course.syllabus.ModuleQueryInspector {

    private final java.util.Collection<org.osid.course.syllabus.records.ModuleQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the syllabus <code> Id </code> terms. 
     *
     *  @return the syllabus <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSyllabusIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the syllabus terms. 
     *
     *  @return the syllabus terms 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusQueryInspector[] getSyllabusTerms() {
        return (new org.osid.course.syllabus.SyllabusQueryInspector[0]);
    }


    /**
     *  Gets the docet <code> Id </code> terms. 
     *
     *  @return the docet <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDocetIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the docet terms. 
     *
     *  @return the docet terms 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetQueryInspector[] getDocetTerms() {
        return (new org.osid.course.syllabus.DocetQueryInspector[0]);
    }


    /**
     *  Gets the course catalog <code> Id </code> terms. 
     *
     *  @return the course catalog <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseCatalogIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the course catalog terms. 
     *
     *  @return the course catalog terms 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQueryInspector[] getCourseCatalogTerms() {
        return (new org.osid.course.CourseCatalogQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given module query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a module implementing the requested record.
     *
     *  @param moduleRecordType a module record type
     *  @return the module query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>moduleRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(moduleRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.syllabus.records.ModuleQueryInspectorRecord getModuleQueryInspectorRecord(org.osid.type.Type moduleRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.syllabus.records.ModuleQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(moduleRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(moduleRecordType + " is not supported");
    }


    /**
     *  Adds a record to this module query. 
     *
     *  @param moduleQueryInspectorRecord module query inspector
     *         record
     *  @param moduleRecordType module record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addModuleQueryInspectorRecord(org.osid.course.syllabus.records.ModuleQueryInspectorRecord moduleQueryInspectorRecord, 
                                                   org.osid.type.Type moduleRecordType) {

        addRecordType(moduleRecordType);
        nullarg(moduleRecordType, "module record type");
        this.records.add(moduleQueryInspectorRecord);        
        return;
    }
}
