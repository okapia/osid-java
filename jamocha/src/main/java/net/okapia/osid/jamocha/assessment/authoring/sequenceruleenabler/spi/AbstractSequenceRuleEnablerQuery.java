//
// AbstractSequenceRuleEnablerQuery.java
//
//     A template for making a SequenceRuleEnabler Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.authoring.sequenceruleenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for sequence rule enablers.
 */

public abstract class AbstractSequenceRuleEnablerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQuery
    implements org.osid.assessment.authoring.SequenceRuleEnablerQuery {

    private final java.util.Collection<org.osid.assessment.authoring.records.SequenceRuleEnablerQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches enablers mapped to the sequence rule. 
     *
     *  @param  sequenceRuleId the sequence rule <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> sequenceRuleId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledSequenceRuleId(org.osid.id.Id sequenceRuleId, 
                                         boolean match) {
        return;
    }


    /**
     *  Clears the sequence rule <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledSequenceRuleIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> SequenceRuleQuery </code> is available. 
     *
     *  @return <code> true </code> if a sequence rule query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledSequenceRuleQuery() {
        return (false);
    }


    /**
     *  Gets the query for a sequence rule. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the sequence rule query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledSequenceRuleQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleQuery getRuledSequenceRuleQuery() {
        throw new org.osid.UnimplementedException("supportsRuledSequenceRuleQuery() is false");
    }


    /**
     *  Matches enablers mapped to any sequence rule. 
     *
     *  @param  match <code> true </code> for enablers mapped to any sequence 
     *          rule, <code> false </code> to match enablers mapped to no 
     *          sequence rules 
     */

    @OSID @Override
    public void matchAnyRuledSequenceRule(boolean match) {
        return;
    }


    /**
     *  Clears the sequence rule query terms. 
     */

    @OSID @Override
    public void clearRuledSequenceRuleTerms() {
        return;
    }


    /**
     *  Matches enablers mapped to the bank. 
     *
     *  @param  bankId the bank <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> bankId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchBankId(org.osid.id.Id bankId, boolean match) {
        return;
    }


    /**
     *  Clears the bank <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearBankIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> BankQuery </code> is available. 
     *
     *  @return <code> true </code> if a bank query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBankQuery() {
        return (false);
    }


    /**
     *  Gets the query for a bank. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the bank query 
     *  @throws org.osid.UnimplementedException <code> supportsBankQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.BankQuery getBankQuery() {
        throw new org.osid.UnimplementedException("supportsBankQuery() is false");
    }


    /**
     *  Clears the bank query terms. 
     */

    @OSID @Override
    public void clearBankTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given sequence rule enabler query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a sequence rule enabler implementing the requested record.
     *
     *  @param sequenceRuleEnablerRecordType a sequence rule enabler record type
     *  @return the sequence rule enabler query record
     *  @throws org.osid.NullArgumentException
     *          <code>sequenceRuleEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(sequenceRuleEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.authoring.records.SequenceRuleEnablerQueryRecord getSequenceRuleEnablerQueryRecord(org.osid.type.Type sequenceRuleEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.authoring.records.SequenceRuleEnablerQueryRecord record : this.records) {
            if (record.implementsRecordType(sequenceRuleEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(sequenceRuleEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this sequence rule enabler query. 
     *
     *  @param sequenceRuleEnablerQueryRecord sequence rule enabler query record
     *  @param sequenceRuleEnablerRecordType sequenceRuleEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addSequenceRuleEnablerQueryRecord(org.osid.assessment.authoring.records.SequenceRuleEnablerQueryRecord sequenceRuleEnablerQueryRecord, 
                                          org.osid.type.Type sequenceRuleEnablerRecordType) {

        addRecordType(sequenceRuleEnablerRecordType);
        nullarg(sequenceRuleEnablerQueryRecord, "sequence rule enabler query record");
        this.records.add(sequenceRuleEnablerQueryRecord);        
        return;
    }
}
