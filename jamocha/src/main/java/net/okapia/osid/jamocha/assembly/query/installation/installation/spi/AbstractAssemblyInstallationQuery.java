//
// AbstractAssemblyInstallationQuery.java
//
//     An InstallationQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.installation.installation.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An InstallationQuery that stores terms.
 */

public abstract class AbstractAssemblyInstallationQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.installation.InstallationQuery,
               org.osid.installation.InstallationQueryInspector,
               org.osid.installation.InstallationSearchOrder {

    private final java.util.Collection<org.osid.installation.records.InstallationQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.installation.records.InstallationQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.installation.records.InstallationSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyInstallationQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyInstallationQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the site <code> Id </code> for this query. 
     *
     *  @param  siteId a site <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> siteId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchSiteId(org.osid.id.Id siteId, boolean match) {
        getAssembler().addIdTerm(getSiteIdColumn(), siteId, match);
        return;
    }


    /**
     *  Clears the site <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearSiteIdTerms() {
        getAssembler().clearTerms(getSiteIdColumn());
        return;
    }


    /**
     *  Gets the site <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSiteIdTerms() {
        return (getAssembler().getIdTerms(getSiteIdColumn()));
    }


    /**
     *  Specified a preference for ordering results by the site. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySite(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getSiteColumn(), style);
        return;
    }


    /**
     *  Gets the SiteId column name.
     *
     * @return the column name
     */

    protected String getSiteIdColumn() {
        return ("site_id");
    }


    /**
     *  Tests if a <code> SiteQuery </code> is available for querying sites. 
     *
     *  @return <code> true </code> if a site query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSiteQuery() {
        return (false);
    }


    /**
     *  Gets the query for a site. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the site query 
     *  @throws org.osid.UnimplementedException <code> supportsSiteQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.SiteQuery getSiteQuery() {
        throw new org.osid.UnimplementedException("supportsSiteQuery() is false");
    }


    /**
     *  Clears the site query terms. 
     */

    @OSID @Override
    public void clearSiteTerms() {
        getAssembler().clearTerms(getSiteColumn());
        return;
    }


    /**
     *  Gets the site query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.installation.SiteQueryInspector[] getSiteTerms() {
        return (new org.osid.installation.SiteQueryInspector[0]);
    }


    /**
     *  Tests if a <code> SiteSearchOrder </code> is available for sites. 
     *
     *  @return <code> true </code> if a site search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSiteSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a site. 
     *
     *  @return the site search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSiteSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.SiteSearchOrder getSiteSearchOrder() {
        throw new org.osid.UnimplementedException("supportsSiteSearchOrder() is false");
    }


    /**
     *  Gets the Site column name.
     *
     * @return the column name
     */

    protected String getSiteColumn() {
        return ("site");
    }


    /**
     *  Sets the package <code> Id </code> for this query. 
     *
     *  @param  packageId a package <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> packageId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPackageId(org.osid.id.Id packageId, boolean match) {
        getAssembler().addIdTerm(getPackageIdColumn(), packageId, match);
        return;
    }


    /**
     *  Clears the package <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearPackageIdTerms() {
        getAssembler().clearTerms(getPackageIdColumn());
        return;
    }


    /**
     *  Gets the package <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPackageIdTerms() {
        return (getAssembler().getIdTerms(getPackageIdColumn()));
    }


    /**
     *  Specified a preference for ordering results by the package. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPackage(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getPackageColumn(), style);
        return;
    }


    /**
     *  Gets the PackageId column name.
     *
     * @return the column name
     */

    protected String getPackageIdColumn() {
        return ("package_id");
    }


    /**
     *  Tests if a <code> PackageQuery </code> is available for querying 
     *  agents. 
     *
     *  @return <code> true </code> if a package query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPackageQuery() {
        return (false);
    }


    /**
     *  Gets the query for a package. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the package query 
     *  @throws org.osid.UnimplementedException <code> supportsPackageQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.PackageQuery getPackageQuery() {
        throw new org.osid.UnimplementedException("supportsPackageQuery() is false");
    }


    /**
     *  Clears the package query terms. 
     */

    @OSID @Override
    public void clearPackageTerms() {
        getAssembler().clearTerms(getPackageColumn());
        return;
    }


    /**
     *  Gets the package query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.installation.PackageQueryInspector[] getPackageTerms() {
        return (new org.osid.installation.PackageQueryInspector[0]);
    }


    /**
     *  Tests if a <code> PackageSearchOrder </code> is available for 
     *  packages. 
     *
     *  @return <code> true </code> if a package search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPackageSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a package. 
     *
     *  @return the package search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPackageSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.PackageSearchOrder getPackageSearchOrder() {
        throw new org.osid.UnimplementedException("supportsPackageSearchOrder() is false");
    }


    /**
     *  Gets the Package column name.
     *
     * @return the column name
     */

    protected String getPackageColumn() {
        return ("package");
    }


    /**
     *  Matches the install date between the given times inclusive. 
     *
     *  @param  from starting range 
     *  @param  to ending range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is <code> 
     *          less than from </code> 
     */

    @OSID @Override
    public void matchInstallDate(org.osid.calendaring.DateTime from, 
                                 org.osid.calendaring.DateTime to, 
                                 boolean match) {
        getAssembler().addDateTimeRangeTerm(getInstallDateColumn(), from, to, match);
        return;
    }


    /**
     *  Clears the install date query terms. 
     */

    @OSID @Override
    public void clearInstallDateTerms() {
        getAssembler().clearTerms(getInstallDateColumn());
        return;
    }


    /**
     *  Gets the install date query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getInstallDateTerms() {
        return (getAssembler().getDateTimeRangeTerms(getInstallDateColumn()));
    }


    /**
     *  Specified a preference for ordering results by the install date. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByInstallDate(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getInstallDateColumn(), style);
        return;
    }


    /**
     *  Gets the InstallDate column name.
     *
     * @return the column name
     */

    protected String getInstallDateColumn() {
        return ("install_date");
    }


    /**
     *  Sets the agent <code> Id </code> for this query. 
     *
     *  @param  agentId an agent <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAgentId(org.osid.id.Id agentId, boolean match) {
        getAssembler().addIdTerm(getAgentIdColumn(), agentId, match);
        return;
    }


    /**
     *  Clears the agent <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAgentIdTerms() {
        getAssembler().clearTerms(getAgentIdColumn());
        return;
    }


    /**
     *  Gets the agent <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAgentIdTerms() {
        return (getAssembler().getIdTerms(getAgentIdColumn()));
    }


    /**
     *  Specified a preference for ordering results by the agent. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAgent(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getAgentColumn(), style);
        return;
    }


    /**
     *  Gets the AgentId column name.
     *
     * @return the column name
     */

    protected String getAgentIdColumn() {
        return ("agent_id");
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available for querying 
     *  agents. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgentQuery() {
        return (false);
    }


    /**
     *  Gets the query for an agent. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> supportsAgentQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getAgentQuery() {
        throw new org.osid.UnimplementedException("supportsAgentQuery() is false");
    }


    /**
     *  Clears the agent query terms. 
     */

    @OSID @Override
    public void clearAgentTerms() {
        getAssembler().clearTerms(getAgentColumn());
        return;
    }


    /**
     *  Gets the agent query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authentication.AgentQueryInspector[] getAgentTerms() {
        return (new org.osid.authentication.AgentQueryInspector[0]);
    }


    /**
     *  Tests if an <code> AgenteSearchOrder </code> is available for agents. 
     *
     *  @return <code> true </code> if an agent search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for an agent. 
     *
     *  @return the agent search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAgentSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getAgentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsAgentSearchOrder() is false");
    }


    /**
     *  Gets the Agent column name.
     *
     * @return the column name
     */

    protected String getAgentColumn() {
        return ("agent");
    }


    /**
     *  Matches the last checked date between the given times inclusive. 
     *
     *  @param  from starting range 
     *  @param  to ending range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is <code> 
     *          less than from </code> 
     */

    @OSID @Override
    public void matchLastCheckDate(org.osid.calendaring.DateTime from, 
                                   org.osid.calendaring.DateTime to, 
                                   boolean match) {
        getAssembler().addDateTimeRangeTerm(getLastCheckDateColumn(), from, to, match);
        return;
    }


    /**
     *  Clears the last check date query terms. 
     */

    @OSID @Override
    public void clearLastCheckDateTerms() {
        getAssembler().clearTerms(getLastCheckDateColumn());
        return;
    }


    /**
     *  Gets the check date query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getLastCheckDateTerms() {
        return (getAssembler().getDateTimeRangeTerms(getLastCheckDateColumn()));
    }


    /**
     *  Specified a preference for ordering results by the last checked date. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByLastCheckDate(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getLastCheckDateColumn(), style);
        return;
    }


    /**
     *  Gets the LastCheckDate column name.
     *
     * @return the column name
     */

    protected String getLastCheckDateColumn() {
        return ("last_check_date");
    }


    /**
     *  Tests if this installation supports the given record
     *  <code>Type</code>.
     *
     *  @param  installationRecordType an installation record type 
     *  @return <code>true</code> if the installationRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>installationRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type installationRecordType) {
        for (org.osid.installation.records.InstallationQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(installationRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  installationRecordType the installation record type 
     *  @return the installation query record 
     *  @throws org.osid.NullArgumentException
     *          <code>installationRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(installationRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.installation.records.InstallationQueryRecord getInstallationQueryRecord(org.osid.type.Type installationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.installation.records.InstallationQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(installationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(installationRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  installationRecordType the installation record type 
     *  @return the installation query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>installationRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(installationRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.installation.records.InstallationQueryInspectorRecord getInstallationQueryInspectorRecord(org.osid.type.Type installationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.installation.records.InstallationQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(installationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(installationRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param installationRecordType the installation record type
     *  @return the installation search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>installationRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(installationRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.installation.records.InstallationSearchOrderRecord getInstallationSearchOrderRecord(org.osid.type.Type installationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.installation.records.InstallationSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(installationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(installationRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this installation. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param installationQueryRecord the installation query record
     *  @param installationQueryInspectorRecord the installation query inspector
     *         record
     *  @param installationSearchOrderRecord the installation search order record
     *  @param installationRecordType installation record type
     *  @throws org.osid.NullArgumentException
     *          <code>installationQueryRecord</code>,
     *          <code>installationQueryInspectorRecord</code>,
     *          <code>installationSearchOrderRecord</code> or
     *          <code>installationRecordTypeinstallation</code> is
     *          <code>null</code>
     */
            
    protected void addInstallationRecords(org.osid.installation.records.InstallationQueryRecord installationQueryRecord, 
                                      org.osid.installation.records.InstallationQueryInspectorRecord installationQueryInspectorRecord, 
                                      org.osid.installation.records.InstallationSearchOrderRecord installationSearchOrderRecord, 
                                      org.osid.type.Type installationRecordType) {

        addRecordType(installationRecordType);

        nullarg(installationQueryRecord, "installation query record");
        nullarg(installationQueryInspectorRecord, "installation query inspector record");
        nullarg(installationSearchOrderRecord, "installation search odrer record");

        this.queryRecords.add(installationQueryRecord);
        this.queryInspectorRecords.add(installationQueryInspectorRecord);
        this.searchOrderRecords.add(installationSearchOrderRecord);
        
        return;
    }
}
