//
// AbstractImmutableOsidRelationship
//
//     Defines an immutable wrapper for an OsidRelationship.
//
//
// Tom Coppeto
// Okapia
// 8 december 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines an immutable wrapper for an OsidRelationship.
 */

public abstract class AbstractImmutableOsidRelationship
    extends AbstractImmutableTemporalOsidObject
    implements org.osid.OsidRelationship {

    private final org.osid.OsidRelationship relationship;


    /**
     *  Constructs a new <code>AbstractImmutableOsidRelationship</code>.
     *
     *  @param relationship
     *  @throws org.osid.NullArgumentException <code>relationship</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableOsidRelationship(org.osid.OsidRelationship relationship) {
        super(relationship);
        this.relationship = relationship;
        return;
    }


    /**
     *  Tests if a reason this relationship came to an end is known. 
     *
     *  @return <code> true </code> if an end reason is available, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.IllegalStateException <code>isEffective</code> is
     *          <code>true</code>
     */

    @OSID @Override
    public boolean hasEndReason() {
        return (this.relationship.hasEndReason());
    }


    /**
     *  Gets a state <code> Id </code> indicating why this relationship has 
     *  ended. 
     *
     *  @return a state <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasEndReason() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getEndReasonId() {
        return (this.relationship.getEndReasonId());
    }


    /**
     *  Gets a state indicating why this relationship has ended. 
     *
     *  @return a state 
     *  @throws org.osid.IllegalStateException <code> hasEndReason() </code> 
     *          is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.process.State getEndReason()
        throws org.osid.OperationFailedException {

        return (this.relationship.getEndReason());
    }
}
