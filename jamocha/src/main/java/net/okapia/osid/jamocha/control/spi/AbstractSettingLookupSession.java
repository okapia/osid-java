//
// AbstractSettingLookupSession.java
//
//    A starter implementation framework for providing a Setting
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Setting
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getSettings(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractSettingLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.control.SettingLookupSession {

    private boolean pedantic      = false;
    private boolean federated     = false;
    private org.osid.control.System system = new net.okapia.osid.jamocha.nil.control.system.UnknownSystem();
    

    /**
     *  Gets the <code>System/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>System Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getSystemId() {
        return (this.system.getId());
    }


    /**
     *  Gets the <code>System</code> associated with this 
     *  session.
     *
     *  @return the <code>System</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.System getSystem()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.system);
    }


    /**
     *  Sets the <code>System</code>.
     *
     *  @param  system the system for this session
     *  @throws org.osid.NullArgumentException <code>system</code>
     *          is <code>null</code>
     */

    protected void setSystem(org.osid.control.System system) {
        nullarg(system, "system");
        this.system = system;
        return;
    }


    /**
     *  Tests if this user can perform <code>Setting</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupSettings() {
        return (true);
    }


    /**
     *  A complete view of the <code>Setting</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeSettingView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Setting</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenarySettingView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include settings in systems which are children
     *  of this system in the system hierarchy.
     */

    @OSID @Override
    public void useFederatedSystemView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this system only.
     */

    @OSID @Override
    public void useIsolatedSystemView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>Setting</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Setting</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Setting</code> and
     *  retained for compatibility.
     *
     *  @param  settingId <code>Id</code> of the
     *          <code>Setting</code>
     *  @return the setting
     *  @throws org.osid.NotFoundException <code>settingId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>settingId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.Setting getSetting(org.osid.id.Id settingId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.control.SettingList settings = getSettings()) {
            while (settings.hasNext()) {
                org.osid.control.Setting setting = settings.getNextSetting();
                if (setting.getId().equals(settingId)) {
                    return (setting);
                }
            }
        } 

        throw new org.osid.NotFoundException(settingId + " not found");
    }


    /**
     *  Gets a <code>SettingList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  settings specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Settings</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getSettings()</code>.
     *
     *  @param  settingIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Setting</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>settingIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.SettingList getSettingsByIds(org.osid.id.IdList settingIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.control.Setting> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = settingIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getSetting(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("setting " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.control.setting.LinkedSettingList(ret));
    }


    /**
     *  Gets a <code>SettingList</code> corresponding to the given
     *  setting genus <code>Type</code> which does not include
     *  settings of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  settings or an error results. Otherwise, the returned list
     *  may contain only those settings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getSettings()</code>.
     *
     *  @param  settingGenusType a setting genus type 
     *  @return the returned <code>Setting</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>settingGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.SettingList getSettingsByGenusType(org.osid.type.Type settingGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.control.setting.SettingGenusFilterList(getSettings(), settingGenusType));
    }


    /**
     *  Gets a <code>SettingList</code> corresponding to the given
     *  setting genus <code>Type</code> and include any additional
     *  settings with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  settings or an error results. Otherwise, the returned list
     *  may contain only those settings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getSettings()</code>.
     *
     *  @param  settingGenusType a setting genus type 
     *  @return the returned <code>Setting</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>settingGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.SettingList getSettingsByParentGenusType(org.osid.type.Type settingGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getSettingsByGenusType(settingGenusType));
    }


    /**
     *  Gets a <code>SettingList</code> containing the given
     *  setting record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  settings or an error results. Otherwise, the returned list
     *  may contain only those settings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getSettings()</code>.
     *
     *  @param  settingRecordType a setting record type 
     *  @return the returned <code>Setting</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>settingRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.SettingList getSettingsByRecordType(org.osid.type.Type settingRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.control.setting.SettingRecordFilterList(getSettings(), settingRecordType));
    }


    /**
     *  Gets a list of settings for a controller. <code> </code>
     *
     *  In plenary mode, the returned list contains all known settings
     *  or an error results. Otherwise, the returned list may contain
     *  only those settings that are accessible through this session.
     *
     *  @param  controllerId a controller <code> Id </code>
     *  @return the returned <code> Setting </code> list
     *  @throws org.osid.NullArgumentException <code> controllerId </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.control.SettingList getSettingsForController(org.osid.id.Id controllerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.control.setting.SettingFilterList(new ControllerFilter(controllerId), getSettings()));
    }


    /**
     *  Gets all <code>Settings</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  settings or an error results. Otherwise, the returned list
     *  may contain only those settings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Settings</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.control.SettingList getSettings()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the setting list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of settings
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.control.SettingList filterSettingsOnViews(org.osid.control.SettingList list)
        throws org.osid.OperationFailedException {

        return (list);
    }


    public static class ControllerFilter
        implements net.okapia.osid.jamocha.inline.filter.control.setting.SettingFilter {
         
        private final org.osid.id.Id controllerId;
         
         
        /**
         *  Constructs a new <code>ControllerFilter</code>.
         *
         *  @param controllerId the controller to filter
         *  @throws org.osid.NullArgumentException
         *          <code>controllerId</code> is <code>null</code>
         */
        
        public ControllerFilter(org.osid.id.Id controllerId) {
            nullarg(controllerId, "controller Id");
            this.controllerId = controllerId;
            return;
        }

         
        /**
         *  Used by the SettingFilterList to filter the setting list
         *  based on controller.
         *
         *  @param setting the setting
         *  @return <code>true</code> to pass the setting,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.control.Setting setting) {
            return (setting.getControllerId().equals(this.controllerId));
        }
    }
}
