//
// AbstractBrokerConstrainerEnablerNotificationSession.java
//
//     A template for making BrokerConstrainerEnablerNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code BrokerConstrainerEnabler} objects. This
 *  session is intended for consumers needing to synchronize their
 *  state with this service without the use of polling. Notifications
 *  are cancelled when this session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code BrokerConstrainerEnabler} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for broker constrainer enabler entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractBrokerConstrainerEnablerNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.provisioning.rules.BrokerConstrainerEnablerNotificationSession {

    private boolean federated = false;
    private org.osid.provisioning.Distributor distributor = new net.okapia.osid.jamocha.nil.provisioning.distributor.UnknownDistributor();


    /**
     *  Gets the {@code Distributor} {@code Id} associated with
     *  this session.
     *
     *  @return the {@code Distributor Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getDistributorId() {
        return (this.distributor.getId());
    }

    
    /**
     *  Gets the {@code Distributor} associated with this session.
     *
     *  @return the {@code Distributor} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Distributor getDistributor()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.distributor);
    }


    /**
     *  Sets the {@code Distributor}.
     *
     *  @param distributor the distributor for this session
     *  @throws org.osid.NullArgumentException {@code distributor}
     *          is {@code null}
     */

    protected void setDistributor(org.osid.provisioning.Distributor distributor) {
        nullarg(distributor, "distributor");
        this.distributor = distributor;
        return;
    }


    /**
     *  Tests if this user can register for {@code
     *  BrokerConstrainerEnabler} notifications.  A return of true
     *  does not guarantee successful authorization. A return of false
     *  indicates that it is known all methods in this session will
     *  result in a {@code PERMISSION_DENIED}. This is intended as a
     *  hint to an application that may opt not to offer notification
     *  operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForBrokerConstrainerEnablerNotifications() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include broker constrainer enablers in distributors
     *  which are children of this distributor in the distributor
     *  hierarchy.
     */

    @OSID @Override
    public void useFederatedDistributorView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this distributor only.
     */

    @OSID @Override
    public void useIsolatedDistributorView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications of new broker constrainer
     *  enablers. {@code
     *  BrokerConstrainerEnablerReceiver.newBrokerConstrainerEnabler()}
     *  is invoked when a new {@code BrokerConstrainerEnabler} is
     *  created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewBrokerConstrainerEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of updated broker constrainer
     *  enablers. {@code
     *  BrokerConstrainerEnablerReceiver.changedBrokerConstrainerEnabler()}
     *  is invoked when a broker constrainer enabler is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedBrokerConstrainerEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated broker constrainer
     *  enabler. {@code
     *  BrokerConstrainerEnablerReceiver.changedBrokerConstrainerEnabler()}
     *  is invoked when the specified broker constrainer enabler is
     *  changed.
     *
     *  @param brokerConstrainerEnablerId the {@code Id} of the {@code BrokerConstrainerEnabler} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code brokerConstrainerEnablerId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedBrokerConstrainerEnabler(org.osid.id.Id brokerConstrainerEnablerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted broker constrainer
     *  enablers. {@code
     *  BrokerConstrainerEnablerReceiver.deletedBrokerConstrainerEnabler()}
     *  is invoked when a broker constrainer enabler is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedBrokerConstrainerEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of a deleted broker constrainer
     *  enabler. {@code
     *  BrokerConstrainerEnablerReceiver.deletedBrokerConstrainerEnabler()}
     *  is invoked when the specified broker constrainer enabler is
     *  deleted.
     *
     *  @param brokerConstrainerEnablerId the {@code Id} of the
     *          {@code BrokerConstrainerEnabler} to monitor
     *  @throws org.osid.NullArgumentException {@code brokerConstrainerEnablerId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedBrokerConstrainerEnabler(org.osid.id.Id brokerConstrainerEnablerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
