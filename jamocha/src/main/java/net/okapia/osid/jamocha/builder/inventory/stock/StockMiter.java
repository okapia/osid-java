//
// StockMiter.java
//
//     Defines a Stock miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.inventory.stock;


/**
 *  Defines a <code>Stock</code> miter for use with the builders.
 */

public interface StockMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidObjectMiter,
            org.osid.inventory.Stock {


    /**
     *  Sets the SKU.
     *
     *  @param sku a stock keeping unit
     *  @throws org.osid.NullArgumentException <code>sku</code> is
     *          <code>null</code>
     */

    public void setSKU(String sku);


    /**
     *  Adds a model.
     *
     *  @param model a model
     *  @throws org.osid.NullArgumentException <code>model</code> is
     *          <code>null</code>
     */

    public void addModel(org.osid.inventory.Model model);


    /**
     *  Sets all the models.
     *
     *  @param models a collection of models
     *  @throws org.osid.NullArgumentException <code>models</code> is
     *          <code>null</code>
     */

    public void setModels(java.util.Collection<org.osid.inventory.Model> models);


    /**
     *  Sets the location description.
     *
     *  @param description a location description
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    public void setLocationDescription(org.osid.locale.DisplayText description);


    /**
     *  Adds a location.
     *
     *  @param location a location
     *  @throws org.osid.NullArgumentException <code>location</code>
     *          is <code>null</code>
     */

    public void addLocation(org.osid.mapping.Location location);


    /**
     *  Sets all the locations.
     *
     *  @param locations a collection of locations
     *  @throws org.osid.NullArgumentException <code>locations</code>
     *          is <code>null</code>
     */

    public void setLocations(java.util.Collection<org.osid.mapping.Location> locations);


    /**
     *  Adds a Stock record.
     *
     *  @param record a stock record
     *  @param recordType the type of stock record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addStockRecord(org.osid.inventory.records.StockRecord record, org.osid.type.Type recordType);
}       


