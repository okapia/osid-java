//
// ActivityMiter.java
//
//     Defines an Activity miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.learning.activity;


/**
 *  Defines an <code>Activity</code> miter for use with the builders.
 */

public interface ActivityMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidObjectMiter,
            org.osid.learning.Activity {


    /**
     *  Sets the objective.
     *
     *  @param objective an objective
     *  @throws org.osid.NullArgumentException <code>objective</code>
     *          is <code>null</code>
     */

    public void setObjective(org.osid.learning.Objective objective);


    /**
     *  Adds an asset.
     *
     *  @param asset an asset
     *  @throws org.osid.NullArgumentException <code>asset</code> is
     *          <code>null</code>
     */

    public void addAsset(org.osid.repository.Asset asset);


    /**
     *  Sets all the assets.
     *
     *  @param assets a collection of assets
     *  @throws org.osid.NullArgumentException <code>assets</code> is
     *          <code>null</code>
     */

    public void setAssets(java.util.Collection<org.osid.repository.Asset> assets);


    /**
     *  Adds a course.
     *
     *  @param course a course
     *  @throws org.osid.NullArgumentException <code>course</code> is
     *          <code>null</code>
     */

    public void addCourse(org.osid.course.Course course);


    /**
     *  Sets all the courses.
     *
     *  @param courses a collection of courses
     *  @throws org.osid.NullArgumentException <code>courses</code> is
     *          <code>null</code>
     */

    public void setCourses(java.util.Collection<org.osid.course.Course> courses);


    /**
     *  Adds an assessment.
     *
     *  @param assessment an assessment
     *  @throws org.osid.NullArgumentException <code>assessment</code>
     *          is <code>null</code>
     */

    public void addAssessment(org.osid.assessment.Assessment assessment);


    /**
     *  Sets all the assessments.
     *
     *  @param assessments a collection of assessments
     *  @throws org.osid.NullArgumentException
     *          <code>assessments</code> is <code>null</code>
     */

    public void setAssessments(java.util.Collection<org.osid.assessment.Assessment> assessments);


    /**
     *  Adds an Activity record.
     *
     *  @param record an activity record
     *  @param recordType the type of activity record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addActivityRecord(org.osid.learning.records.ActivityRecord record, org.osid.type.Type recordType);
}       


