//
// AbstractBlogNodeList.java
//
//     Implements an AbstractBlogNodeList adapter.
//
//
// Tom Coppeto
// Okapia
// 21 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.blogging.blognode.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An AbstractBlogNodeList adapter template.
 */

public abstract class AbstractAdapterBlogNodeList
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidList
    implements org.osid.blogging.BlogNodeList {


    /**
     *  Constructs a new {@code AbstractAdapterBlogNodeList}.
     *
     *  @param list
     *  @throws org.osid.NullArgumentException {@code list} is 
     *          {@code null}
     */

    protected AbstractAdapterBlogNodeList(org.osid.OsidList list) {
        super(list);
        return;
    }


    /**
     *  Gets the next {@code BlogNode} in this list. 
     *
     *  @return the next {@code BlogNode} in this list. The {@code
     *          hasNext()} method should be used to test that a next
     *          {@code BlogNode} is available before calling this
     *          method.
     *  @throws org.osid.IllegalStateException no more elements
     *          available in this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public abstract org.osid.blogging.BlogNode getNextBlogNode()
        throws org.osid.OperationFailedException;


    /**
     *  Gets the next set of {@code BlogNode} elements in this list
     *  which must be less than or equal to the return from {@code
     *  available()}.
     *
     *  @param n the number of {@code BlogNode} elements requested
     *          which must be less than or equal to {@code
     *          available()}
     *  @return an array of {@code BlogNode} elements. The
     *          length of the array is less than or equal to the
     *          number specified.
     *  @throws org.osid.IllegalStateException no more elements
     *          available in this list or this list has been closed
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.InvalidArgumentException cardinal value is negative
     */

    @OSID @Override
    public org.osid.blogging.BlogNode[] getNextBlogNodes(long n)
        throws org.osid.OperationFailedException {
        
        if (n > available()) {
            throw new org.osid.IllegalStateException("insufficient elements available");
        }

        org.osid.blogging.BlogNode[] ret = new org.osid.blogging.BlogNode[(int) n];

        for (int i = 0; i < n; i++) {
            ret[i] = getNextBlogNode();
        }

        return (ret);
    }
}
