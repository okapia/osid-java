//
// AbstractMutableTrigger.java
//
//     Defines a mutable Trigger.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.control.trigger.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>Trigger</code>.
 */

public abstract class AbstractMutableTrigger
    extends net.okapia.osid.jamocha.control.trigger.spi.AbstractTrigger
    implements org.osid.control.Trigger,
               net.okapia.osid.jamocha.builder.control.trigger.TriggerMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this trigger. 
     *
     *  @param record trigger record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addTriggerRecord(org.osid.control.records.TriggerRecord record, org.osid.type.Type recordType) {
        super.addTriggerRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Enables this trigger. Enabling an operable overrides any
     *  enabling rule that may exist.
     *  
     *  @param enabled <code>true</code> if enabled, <code>false<code>
     *         if disabled
     */
    
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);

        if (isDisabled()) {
            setDisabled(false);
        }

        return;
    }


    /**
     *  Disables this trigger. Disabling an operable overrides any
     *  enabling rule that may exist.
     *
     *  @param disabled <code>true</code> if disabled, <code>false<code>
     *         if enabled
     */
    
    public void setDisabled(boolean disabled) {
        super.setDisabled(disabled);

        if (isEnabled()) {
            setEnabled(false);
        }

        return;
    }


    /**
     *  Sets the operational flag.
     *
     *  @param operational <code>true</code>if operational,
     *         <code>false</code> if not operational
     */
    
    public void setOperational(boolean operational) {
        super.setOperational(operational);
        return;
    }


    /**
     *  Sets the display name for this trigger.
     *
     *  @param displayName the name for this trigger
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this trigger.
     *
     *  @param description the description of this trigger
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException
     *          <code>genusType</code> is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the rule.
     *
     *  @param rule the rule
     *  @throws org.osid.NullArgumentException
     *          <code>rule</code> is <code>null</code>
     */
    
    public void setRule(org.osid.rules.Rule rule) {
        super.setRule(rule);
        return;
    }


    /**
     *  Sets the controller.
     *
     *  @param controller a controller
     *  @throws org.osid.NullArgumentException <code>controller</code>
     *          is <code>null</code>
     */

    @Override
    public void setController(org.osid.control.Controller controller) {
        super.setController(controller);
        return;
    }


    /**
     *  Listen to on events.
     *
     *  @param listen {@code true} to listen to on events, {@code
     *         false} otherwise
     */

    @Override
    public void setTurnedOn(boolean listen) {
        super.setTurnedOn(listen);
        return;
    }


    /**
     *  Listen to turned off events.
     *
     *  @param listen {@code true} to listen to off events, {@code
     *         false} otherwise
     */
    
    @Override
    public void setTurnedOff(boolean listen) {
        super.setTurnedOff(listen);
        return;
    }

    
    /**
     *  Sets the changed variable amount.
     *
     *  @param listen {@code true} to listen to changed amount events,
     *         {@code false} otherwise
     */

    @Override
    public void setChangedVariableAmount(boolean listen) {
        super.setChangedVariableAmount(listen);
        return;
    }


    /**
     *  Listen to changes above the given threshold.
     *
     *  @param max a max threshold
     *  @throws org.osid.NullArgumentException <code>max</code> is
     *          <code>null</code>
     */

    @Override
    public void setChangedExceedsVariableAmount(java.math.BigDecimal max) {
        super.setChangedExceedsVariableAmount(max);
        return;
    }


    /**
     *  Listen to changes below the given threshold.
     *
     *  @param min a min threshold
     *  @throws org.osid.NullArgumentException <code>min</code> is
     *          <code>null</code>
     */

    @Override
    public void setChangedDeceedsVariableAmount(java.math.BigDecimal min) {
        super.setChangedDeceedsVariableAmount(min);
        return;
    }


    /**
     *  Listen for state changes.
     *
     *  @param listen {@code true} to listen to state change events,
     *         {@code false} otherwise
     */

    @Override
    public void setChangedDiscreetState(boolean listen) {
        super.setChangedDiscreetState(listen);
        return;
    }

    
    /**
     *  Sets the discreet state.
     *
     *  @param state a discreet state
     *  @throws org.osid.NullArgumentException <code>state</code> is
     *          <code>null</code>
     */

    @Override
    public void setDiscreetState(org.osid.process.State state) {
        super.setDiscreetState(state);
        return;
    }


    /**
     *  Adds an action group.
     *
     *  @param actionGroup an action group
     *  @throws org.osid.NullArgumentException
     *          <code>actionGroup</code> is <code>null</code>
     */

    @Override
    public void addActionGroup(org.osid.control.ActionGroup actionGroup) {
        super.addActionGroup(actionGroup);
        return;
    }


    /**
     *  Sets all the action groups.
     *
     *  @param actionGroups a collection of action groups
     *  @throws org.osid.NullArgumentException
     *          <code>actionGroups</code> is <code>null</code>
     */

    @Override
    public void setActionGroups(java.util.Collection<org.osid.control.ActionGroup> actionGroups) {
        super.setActionGroups(actionGroups);
        return;
    }

    
    /**
     *  Adds a scene.
     *
     *  @param scene a Scene
     *  @throws org.osid.NullArgumentException <code>scene</code> is
     *          <code>null</code>
     */

    @Override
    public void addScene(org.osid.control.Scene scene) {
        super.addScene(scene);
        return;
    }


    /**
     *  Sets all the scenes.
     *
     *  @param scenes a collection of scenes
     *  @throws org.osid.NullArgumentException <code>scenes</code> is
     *          <code>null</code>
     */

    @Override
    public void setScenes(java.util.Collection<org.osid.control.Scene> scenes) {
        super.setScenes(scenes);
        return;
    }
    

    /**
     *  Adds a setting.
     *
     *  @param setting a setting
     *  @throws org.osid.NullArgumentException
     *          <code>setting</code> is <code>null</code>
     */

    @Override
    public void addSetting(org.osid.control.Setting setting) {
        super.addSetting(setting);
        return;
    }


    /**
     *  Sets all the settings.
     *
     *  @param settings a collection of settings
     *  @throws org.osid.NullArgumentException <code>settings</code> is
     *          <code>null</code>
     */

    @Override
    public void setSettings(java.util.Collection<org.osid.control.Setting> settings) {
        super.setSettings(settings);
        return;
    }
}

