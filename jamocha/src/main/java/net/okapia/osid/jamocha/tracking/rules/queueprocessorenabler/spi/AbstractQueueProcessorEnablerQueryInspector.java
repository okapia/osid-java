//
// AbstractQueueProcessorEnablerQueryInspector.java
//
//     A template for making a QueueProcessorEnablerQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.tracking.rules.queueprocessorenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for queue processor enablers.
 */

public abstract class AbstractQueueProcessorEnablerQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQueryInspector
    implements org.osid.tracking.rules.QueueProcessorEnablerQueryInspector {

    private final java.util.Collection<org.osid.tracking.rules.records.QueueProcessorEnablerQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the queue processor <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledQueueProcessorIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the queue processor query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorQueryInspector[] getRuledQueueProcessorTerms() {
        return (new org.osid.tracking.rules.QueueProcessorQueryInspector[0]);
    }


    /**
     *  Gets the front office <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getFrontOfficeIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the front office query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeQueryInspector[] getFrontOfficeTerms() {
        return (new org.osid.tracking.FrontOfficeQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given queue processor enabler query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a queue processor enabler implementing the requested record.
     *
     *  @param queueProcessorEnablerRecordType a queue processor enabler record type
     *  @return the queue processor enabler query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>queueProcessorEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(queueProcessorEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.tracking.rules.records.QueueProcessorEnablerQueryInspectorRecord getQueueProcessorEnablerQueryInspectorRecord(org.osid.type.Type queueProcessorEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.tracking.rules.records.QueueProcessorEnablerQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(queueProcessorEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(queueProcessorEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this queue processor enabler query. 
     *
     *  @param queueProcessorEnablerQueryInspectorRecord queue processor enabler query inspector
     *         record
     *  @param queueProcessorEnablerRecordType queueProcessorEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addQueueProcessorEnablerQueryInspectorRecord(org.osid.tracking.rules.records.QueueProcessorEnablerQueryInspectorRecord queueProcessorEnablerQueryInspectorRecord, 
                                                   org.osid.type.Type queueProcessorEnablerRecordType) {

        addRecordType(queueProcessorEnablerRecordType);
        nullarg(queueProcessorEnablerRecordType, "queue processor enabler record type");
        this.records.add(queueProcessorEnablerQueryInspectorRecord);        
        return;
    }
}
