//
// AbstractWorkLookupSession.java
//
//    A starter implementation framework for providing a Work
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Work
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getWorks(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractWorkLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.resourcing.WorkLookupSession {

    private boolean pedantic  = false;
    private boolean federated = false;
    private org.osid.resourcing.Foundry foundry = new net.okapia.osid.jamocha.nil.resourcing.foundry.UnknownFoundry();
    

    /**
     *  Gets the <code>Foundry/code> <code>Id</code> associated with
     *  this session.
     *
     *  @return the <code>Foundry Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFoundryId() {
        return (this.foundry.getId());
    }


    /**
     *  Gets the <code>Foundry</code> associated with this session.
     *
     *  @return the <code>Foundry</code> associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Foundry getFoundry()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.foundry);
    }


    /**
     *  Sets the <code>Foundry</code>.
     *
     *  @param  foundry the foundry for this session
     *  @throws org.osid.NullArgumentException <code>foundry</code>
     *          is <code>null</code>
     */

    protected void setFoundry(org.osid.resourcing.Foundry foundry) {
        nullarg(foundry, "foundry");
        this.foundry = foundry;
        return;
    }


    /**
     *  Tests if this user can perform <code>Work</code> lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupWorks() {
        return (true);
    }


    /**
     *  A complete view of the <code>Work</code> returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeWorkView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Work</code> returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryWorkView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include works in foundries which are children of
     *  this foundry in the foundry hierarchy.
     */

    @OSID @Override
    public void useFederatedFoundryView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this foundry only.
     */

    @OSID @Override
    public void useIsolatedFoundryView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>Work</code> specified by its <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Work</code> may have a different <code>Id</code> than
     *  requested, such as the case where a duplicate <code>Id</code>
     *  was assigned to a <code>Work</code> and retained for
     *  compatibility.
     *
     *  @param  workId <code>Id</code> of the
     *          <code>Work</code>
     *  @return the work
     *  @throws org.osid.NotFoundException <code>workId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>workId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Work getWork(org.osid.id.Id workId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.resourcing.WorkList works = getWorks()) {
            while (works.hasNext()) {
                org.osid.resourcing.Work work = works.getNextWork();
                if (work.getId().equals(workId)) {
                    return (work);
                }
            }
        } 

        throw new org.osid.NotFoundException(workId + " not found");
    }


    /**
     *  Gets a <code>WorkList</code> corresponding to the given
     *  <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the works
     *  specified in the <code>Id</code> list, in the order of the
     *  list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Works</code> may
     *  be omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getWorks()</code>.
     *
     *  @param  workIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Work</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>workIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.WorkList getWorksByIds(org.osid.id.IdList workIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.resourcing.Work> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = workIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getWork(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("work " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.resourcing.work.LinkedWorkList(ret));
    }


    /**
     *  Gets a <code>WorkList</code> corresponding to the given work
     *  genus <code>Type</code> which does not include works of types
     *  derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known works or
     *  an error results. Otherwise, the returned list may contain
     *  only those works that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getWorks()</code>.
     *
     *  @param  workGenusType a work genus type 
     *  @return the returned <code>Work</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>workGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.WorkList getWorksByGenusType(org.osid.type.Type workGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resourcing.work.WorkGenusFilterList(getWorks(), workGenusType));
    }


    /**
     *  Gets a <code>WorkList</code> corresponding to the given work
     *  genus <code>Type</code> and include any additional works with
     *  genus types derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known works or
     *  an error results. Otherwise, the returned list may contain
     *  only those works that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getWorks()</code>.
     *
     *  @param  workGenusType a work genus type 
     *  @return the returned <code>Work</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>workGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.WorkList getWorksByParentGenusType(org.osid.type.Type workGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getWorksByGenusType(workGenusType));
    }


    /**
     *  Gets a <code>WorkList</code> containing the given work record
     *  <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known works or
     *  an error results. Otherwise, the returned list may contain
     *  only those works that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getWorks()</code>.
     *
     *  @param  workRecordType a work record type 
     *  @return the returned <code>Work</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>workRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.WorkList getWorksByRecordType(org.osid.type.Type workRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resourcing.work.WorkRecordFilterList(getWorks(), workRecordType));
    }


    /**
     *  Gets a <code>WorkList</code> corresponding to the given job.
     *  In plenary mode, the returned list contains all known works or
     *  an error results. Otherwise, the returned list may contain
     *  only those works that are accessible through this session.
     *
     *  @param  jobId a job <code>Id</code> 
     *  @return the returned <code>Work</code> list 
     *  @throws org.osid.NullArgumentException <code>jobId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resourcing.WorkList getWorksForJob(org.osid.id.Id jobId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resourcing.work.WorkFilterList(new JobFilter(jobId), getWorks()));
    }


    /**
     *  Gets a <code>WorkList</code> of uncommitted works
     *  corresponding to the given job. In plenary mode, the returned
     *  list contains all known works or an error results. Otherwise,
     *  the returned list may contain only those works that are
     *  accessible through this session.
     *
     *  @param  jobId a job <code>Id</code> 
     *  @return the returned <code>Work</code> list 
     *  @throws org.osid.NullArgumentException <code>jobId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resourcing.WorkList getUncommittedWorksForJob(org.osid.id.Id jobId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resourcing.work.WorkFilterList(new JobFilter(jobId), getUncommittedWorks()));
    }


    /**
     *  Gets a <code>WorkList</code> of incomplete works
     *  corresponding to the given job. In plenary mode, the returned
     *  list contains all known works or an error results. Otherwise,
     *  the returned list may contain only those works that are
     *  accessible through this session.
     *
     *  @param  jobId a job <code>Id</code> 
     *  @return the returned <code>Work</code> list 
     *  @throws org.osid.NullArgumentException <code>jobId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resourcing.WorkList getIncompleteWorksForJob(org.osid.id.Id jobId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resourcing.work.WorkFilterList(new IncompleteFilter(), getWorksForJob(jobId)));
    }


    /**
     *  Gets a <code>WorkList</code> of all works created between the
     *  given date range inclusive. In plenary mode, the returned list
     *  contains all known works or an error results. Otherwise, the
     *  returned list may contain only those works that are accessible
     *  through this session.
     *
     *  @param  from start range 
     *  @param  to end range 
     *  @return the returned <code>Work</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resourcing.WorkList getWorksByDate(org.osid.calendaring.DateTime from, 
                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resourcing.work.WorkFilterList(new DateFilter(from, to), getWorks()));
    }


    /**
     *  Gets a <code>WorkList</code> of incomplete works created
     *  between the given date range inclusive. In plenary mode, the
     *  returned list contains all known works or an error
     *  results. Otherwise, the returned list may contain only those
     *  works that are accessible through this session.
     *
     *  @param  from start range 
     *  @param  to end range 
     *  @return the returned <code>Work</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is 
     *          greater than <code>to</code> 
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resourcing.WorkList getIncompleteWorksByDate(org.osid.calendaring.DateTime from, 
                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resourcing.work.WorkFilterList(new DateFilter(from, to), getIncompleteWorks()));
    }


    /**
     *  Gets a <code>WorkList</code> of uncommitted works created
     *  between the given date range inclusive. In plenary mode, the
     *  returned list contains all known works or an error
     *  results. Otherwise, the returned list may contain only those
     *  works that are accessible through this session.
     *
     *  @param  from start range 
     *  @param  to end range 
     *  @return the returned <code>Work</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is 
     *          greater than <code>to</code> 
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resourcing.WorkList getUncommittedWorksByDate(org.osid.calendaring.DateTime from, 
                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resourcing.work.WorkFilterList(new DateFilter(from, to), getUncommittedWorks()));
    }


    /**
     *  Gets a <code>WorkList</code> of all works of corresponding to
     *  the given job and created between the given date range
     *  inclusive. In plenary mode, the returned list contains all
     *  known works or an error results. Otherwise, the returned list
     *  may contain only those works that are accessible through this
     *  session.
     *
     *  @param jobId a job <code>Id</code>
     *  @param  from start range 
     *  @param  to end range 
     *  @return the returned <code>Work</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is 
     *          greater than <code>to</code> 
     *  @throws org.osid.NullArgumentException <code>jobId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resourcing.WorkList getWorksByDateForJob(org.osid.id.Id jobId, 
                                                             org.osid.calendaring.DateTime from, 
                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resourcing.work.WorkFilterList(new DateFilter(from, to), getWorksForJob(jobId)));
    }

    
    /**
     *  Gets a <code>WorkList</code> of incomplete works
     *  corresponding to the given job and created between the given
     *  date range inclusive. In plenary mode, the returned list
     *  contains all known works or an error results. Otherwise, the
     *  returned list may contain only those works that are accessible
     *  through this session.
     *
     *  @param jobId a job <code>Id</code>
     *  @param  from start range 
     *  @param  to end range 
     *  @return the returned <code>Work</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is 
     *          greater than <code>to</code> 
     *  @throws org.osid.NullArgumentException <code>jobId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resourcing.WorkList getIncompleteWorksByDateForJob(org.osid.id.Id jobId, 
                                                                       org.osid.calendaring.DateTime from, 
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resourcing.work.WorkFilterList(new IncompleteFilter(), getWorksForJob(jobId)));
    }


    /**
     *  Gets all incomplete <code>Works</code>. In plenary mode, the
     *  returned list contains all known works or an error
     *  results. Otherwise, the returned list may contain only those
     *  works that are accessible through this session.
     *
     *  @return a list of <code>Works</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resourcing.WorkList getIncompleteWorks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resourcing.work.WorkFilterList(new IncompleteFilter(), getWorks()));
    }


    /**
     *  Gets all <code>Works</code> that have no commitments. In
     *  plenary mode, the returned list contains all known works or an
     *  error results.  Otherwise, the returned list may contain only
     *  those works that are accessible through this session.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public abstract org.osid.resourcing.WorkList getUncommittedWorks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Gets all <code>Works</code>.
     *
     *  In plenary mode, the returned list contains all known works or
     *  an error results. Otherwise, the returned list may contain
     *  only those works that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  @return a list of <code>Works</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.resourcing.WorkList getWorks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the work list for active and effective views. Should
     *  be called by <code>getObjects()</code> if no filtering is
     *  already performed.
     *
     *  @param list the list of works
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.resourcing.WorkList filterWorksOnViews(org.osid.resourcing.WorkList list)
        throws org.osid.OperationFailedException {

        return (list);
    }


    public static class JobFilter
        implements net.okapia.osid.jamocha.inline.filter.resourcing.work.WorkFilter {
         
        private final org.osid.id.Id jobId;
         
         
        /**
         *  Constructs a new <code>JobFilter</code>.
         *
         *  @param jobId the job to filter
         *  @throws org.osid.NullArgumentException
         *          <code>jobId</code> is <code>null</code>
         */
        
        public JobFilter(org.osid.id.Id jobId) {
            nullarg(jobId, "job Id");
            this.jobId = jobId;
            return;
        }

         
        /**
         *  Used by the WorkFilterList to filter the 
         *  work list based on job.
         *
         *  @param work the work
         *  @return <code>true</code> to pass the work,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.resourcing.Work work) {
            return (work.getJobId().equals(this.jobId));
        }
    }


    public static class DateFilter
        implements net.okapia.osid.jamocha.inline.filter.resourcing.work.WorkFilter {
         
        private final org.osid.calendaring.DateTime from;
        private final org.osid.calendaring.DateTime to;
         
         
        /**
         *  Constructs a new <code>DateFilter</code>.
         *
         *  @param from start date
         *  @param to end date
         *  @throws org.osid.NullArgumentException <code>from</code>
         *          or <code>to</code> is <code>null</code>
         */
        
        public DateFilter(org.osid.calendaring.DateTime from, org.osid.calendaring.DateTime to) {
            nullarg(from, "start date");
            nullarg(to, "end date");

            this.from = from;
            this.to = to;

            return;
        }

         
        /**
         *  Used by the WorkFilterList to filter the 
         *  work list based on date.
         *
         *  @param work the work
         *  @return <code>true</code> to pass the work,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.resourcing.Work work) {
            if (work.getCreatedDate().isLess(this.from)) {
                return (false);
            }

            if (work.getCreatedDate().isGreater(this.to)) {
                return (false);
            }

            return (true);
        }
    }


    public static class IncompleteFilter
        implements net.okapia.osid.jamocha.inline.filter.resourcing.work.WorkFilter {
         

        /**
         *  Used by the WorkFilterList to filter the 
         *  work list based on incomplete.
         *
         *  @param work the work
         *  @return <code>true</code> to pass the work,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.resourcing.Work work) {
            return (!work.isComplete());
        }
    }
}
