//
// AbstractRelevancyEnabler.java
//
//     Defines a RelevancyEnabler.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ontology.rules.relevancyenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>RelevancyEnabler</code>.
 */

public abstract class AbstractRelevancyEnabler
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnabler
    implements org.osid.ontology.rules.RelevancyEnabler {

    private final java.util.Collection<org.osid.ontology.rules.records.RelevancyEnablerRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Tests if this relevancyEnabler supports the given record
     *  <code>Type</code>.
     *
     *  @param  relevancyEnablerRecordType a relevancy enabler record type 
     *  @return <code>true</code> if the relevancyEnablerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>relevancyEnablerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type relevancyEnablerRecordType) {
        for (org.osid.ontology.rules.records.RelevancyEnablerRecord record : this.records) {
            if (record.implementsRecordType(relevancyEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>RelevancyEnabler</code> record <code>Type</code>.
     *
     *  @param  relevancyEnablerRecordType the relevancy enabler record type 
     *  @return the relevancy enabler record 
     *  @throws org.osid.NullArgumentException
     *          <code>relevancyEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(relevancyEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.ontology.rules.records.RelevancyEnablerRecord getRelevancyEnablerRecord(org.osid.type.Type relevancyEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ontology.rules.records.RelevancyEnablerRecord record : this.records) {
            if (record.implementsRecordType(relevancyEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(relevancyEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this relevancy enabler. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param relevancyEnablerRecord the relevancy enabler record
     *  @param relevancyEnablerRecordType relevancy enabler record type
     *  @throws org.osid.NullArgumentException
     *          <code>relevancyEnablerRecord</code> or
     *          <code>relevancyEnablerRecordTyperelevancyEnabler</code> is
     *          <code>null</code>
     */
            
    protected void addRelevancyEnablerRecord(org.osid.ontology.rules.records.RelevancyEnablerRecord relevancyEnablerRecord, 
                                             org.osid.type.Type relevancyEnablerRecordType) {

        nullarg(relevancyEnablerRecord, "relevancy enabler record");
        addRecordType(relevancyEnablerRecordType);
        this.records.add(relevancyEnablerRecord);
        
        return;
    }
}
