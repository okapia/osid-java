//
// ProcedureElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recipe.procedure.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class ProcedureElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the ProcedureElement Id.
     *
     *  @return the procedure element Id
     */

    public static org.osid.id.Id getProcedureEntityId() {
        return (makeEntityId("osid.recipe.Procedure"));
    }


    /**
     *  Gets the LearningObjectiveIds element Id.
     *
     *  @return the LearningObjectiveIds element Id
     */

    public static org.osid.id.Id getLearningObjectiveIds() {
        return (makeElementId("osid.recipe.procedure.LearningObjectiveIds"));
    }


    /**
     *  Gets the LearningObjectives element Id.
     *
     *  @return the LearningObjectives element Id
     */

    public static org.osid.id.Id getLearningObjectives() {
        return (makeElementId("osid.recipe.procedure.LearningObjectives"));
    }


    /**
     *  Gets the CookbookId element Id.
     *
     *  @return the CookbookId element Id
     */

    public static org.osid.id.Id getCookbookId() {
        return (makeQueryElementId("osid.recipe.procedure.CookbookId"));
    }


    /**
     *  Gets the Cookbook element Id.
     *
     *  @return the Cookbook element Id
     */

    public static org.osid.id.Id getCookbook() {
        return (makeQueryElementId("osid.recipe.procedure.Cookbook"));
    }
}
