//
// AbstractBallotConstrainerQueryInspector.java
//
//     A template for making a BallotConstrainerQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.rules.ballotconstrainer.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for ballot constrainers.
 */

public abstract class AbstractBallotConstrainerQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidConstrainerQueryInspector
    implements org.osid.voting.rules.BallotConstrainerQueryInspector {

    private final java.util.Collection<org.osid.voting.rules.records.BallotConstrainerQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the ballot <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledBallotIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ballot query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.voting.BallotQueryInspector[] getRuledBallotTerms() {
        return (new org.osid.voting.BallotQueryInspector[0]);
    }


    /**
     *  Gets the polls <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPollsIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the polls query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.voting.PollsQueryInspector[] getPollsTerms() {
        return (new org.osid.voting.PollsQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given ballot constrainer query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a ballot constrainer implementing the requested record.
     *
     *  @param ballotConstrainerRecordType a ballot constrainer record type
     *  @return the ballot constrainer query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>ballotConstrainerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(ballotConstrainerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.rules.records.BallotConstrainerQueryInspectorRecord getBallotConstrainerQueryInspectorRecord(org.osid.type.Type ballotConstrainerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.rules.records.BallotConstrainerQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(ballotConstrainerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(ballotConstrainerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this ballot constrainer query. 
     *
     *  @param ballotConstrainerQueryInspectorRecord ballot constrainer query inspector
     *         record
     *  @param ballotConstrainerRecordType ballotConstrainer record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addBallotConstrainerQueryInspectorRecord(org.osid.voting.rules.records.BallotConstrainerQueryInspectorRecord ballotConstrainerQueryInspectorRecord, 
                                                   org.osid.type.Type ballotConstrainerRecordType) {

        addRecordType(ballotConstrainerRecordType);
        nullarg(ballotConstrainerRecordType, "ballot constrainer record type");
        this.records.add(ballotConstrainerQueryInspectorRecord);        
        return;
    }
}
