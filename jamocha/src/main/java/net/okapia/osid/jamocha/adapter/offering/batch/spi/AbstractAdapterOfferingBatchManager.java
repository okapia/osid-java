//
// AbstractOfferingBatchManager.java
//
//     An adapter for a OfferingBatchManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.offering.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a OfferingBatchManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterOfferingBatchManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.offering.batch.OfferingBatchManager>
    implements org.osid.offering.batch.OfferingBatchManager {


    /**
     *  Constructs a new {@code AbstractAdapterOfferingBatchManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterOfferingBatchManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterOfferingBatchManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterOfferingBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if bulk administration of canonical units is available. 
     *
     *  @return <code> true </code> if a canonical unit bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitBatchAdmin() {
        return (getAdapteeManager().supportsCanonicalUnitBatchAdmin());
    }


    /**
     *  Tests if bulk administration of offerings is available. 
     *
     *  @return <code> true </code> if an offering bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingBatchAdmin() {
        return (getAdapteeManager().supportsOfferingBatchAdmin());
    }


    /**
     *  Tests if bulk administration of participants is available. 
     *
     *  @return <code> true </code> if a participant bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParticipantBatchAdmin() {
        return (getAdapteeManager().supportsParticipantBatchAdmin());
    }


    /**
     *  Tests if bulk administration of results is available. 
     *
     *  @return <code> true </code> if a result bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResultBatchAdmin() {
        return (getAdapteeManager().supportsResultBatchAdmin());
    }


    /**
     *  Tests if bulk administration of catalogues is available. 
     *
     *  @return <code> true </code> if a catalogue bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogueBatchAdmin() {
        return (getAdapteeManager().supportsCatalogueBatchAdmin());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk canonical 
     *  unit administration service. 
     *
     *  @return a <code> CanonicalUnitBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.batch.CanonicalUnitBatchAdminSession getCanonicalUnitBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk canonical 
     *  unit administration service for the given catalogue 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return a <code> CanonicalUnitBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.batch.CanonicalUnitBatchAdminSession getCanonicalUnitBatchAdminSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitBatchAdminSessionForCatalogue(catalogueId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk offering 
     *  administration service. 
     *
     *  @return an <code> OfferingBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.batch.OfferingBatchAdminSession getOfferingBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk offering 
     *  administration service for the given catalogue 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return an <code> OfferingBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.batch.OfferingBatchAdminSession getOfferingBatchAdminSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingBatchAdminSessionForCatalogue(catalogueId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  participant administration service. 
     *
     *  @return a <code> ParticipantBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParticipantBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.batch.ParticipantBatchAdminSession getParticipantBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getParticipantBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  participant administration service for the given catalogue 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return a <code> ParticipantBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParticipantBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.batch.ParticipantBatchAdminSession getParticipantBatchAdminSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getParticipantBatchAdminSessionForCatalogue(catalogueId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk result 
     *  administration service. 
     *
     *  @return a <code> ResultBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResultBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.batch.ResultBatchAdminSession getResultBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResultBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk result 
     *  administration service for the given catalogue 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return a <code> ResultBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResultBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.batch.ResultBatchAdminSession getResultBatchAdminSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getResultBatchAdminSessionForCatalogue(catalogueId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk catalogue 
     *  administration service. 
     *
     *  @return a <code> CatalogueBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogueBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.batch.CatalogueBatchAdminSession getCatalogueBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCatalogueBatchAdminSession());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
