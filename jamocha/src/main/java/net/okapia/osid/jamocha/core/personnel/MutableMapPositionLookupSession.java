//
// MutableMapPositionLookupSession
//
//    Implements a Position lookup service backed by a collection of
//    positions that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.personnel;


/**
 *  Implements a Position lookup service backed by a collection of
 *  positions. The positions are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of positions can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapPositionLookupSession
    extends net.okapia.osid.jamocha.core.personnel.spi.AbstractMapPositionLookupSession
    implements org.osid.personnel.PositionLookupSession {


    /**
     *  Constructs a new {@code MutableMapPositionLookupSession}
     *  with no positions.
     *
     *  @param realm the realm
     *  @throws org.osid.NullArgumentException {@code realm} is
     *          {@code null}
     */

      public MutableMapPositionLookupSession(org.osid.personnel.Realm realm) {
        setRealm(realm);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapPositionLookupSession} with a
     *  single position.
     *
     *  @param realm the realm  
     *  @param position a position
     *  @throws org.osid.NullArgumentException {@code realm} or
     *          {@code position} is {@code null}
     */

    public MutableMapPositionLookupSession(org.osid.personnel.Realm realm,
                                           org.osid.personnel.Position position) {
        this(realm);
        putPosition(position);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapPositionLookupSession}
     *  using an array of positions.
     *
     *  @param realm the realm
     *  @param positions an array of positions
     *  @throws org.osid.NullArgumentException {@code realm} or
     *          {@code positions} is {@code null}
     */

    public MutableMapPositionLookupSession(org.osid.personnel.Realm realm,
                                           org.osid.personnel.Position[] positions) {
        this(realm);
        putPositions(positions);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapPositionLookupSession}
     *  using a collection of positions.
     *
     *  @param realm the realm
     *  @param positions a collection of positions
     *  @throws org.osid.NullArgumentException {@code realm} or
     *          {@code positions} is {@code null}
     */

    public MutableMapPositionLookupSession(org.osid.personnel.Realm realm,
                                           java.util.Collection<? extends org.osid.personnel.Position> positions) {

        this(realm);
        putPositions(positions);
        return;
    }

    
    /**
     *  Makes a {@code Position} available in this session.
     *
     *  @param position a position
     *  @throws org.osid.NullArgumentException {@code position{@code  is
     *          {@code null}
     */

    @Override
    public void putPosition(org.osid.personnel.Position position) {
        super.putPosition(position);
        return;
    }


    /**
     *  Makes an array of positions available in this session.
     *
     *  @param positions an array of positions
     *  @throws org.osid.NullArgumentException {@code positions{@code 
     *          is {@code null}
     */

    @Override
    public void putPositions(org.osid.personnel.Position[] positions) {
        super.putPositions(positions);
        return;
    }


    /**
     *  Makes collection of positions available in this session.
     *
     *  @param positions a collection of positions
     *  @throws org.osid.NullArgumentException {@code positions{@code  is
     *          {@code null}
     */

    @Override
    public void putPositions(java.util.Collection<? extends org.osid.personnel.Position> positions) {
        super.putPositions(positions);
        return;
    }


    /**
     *  Removes a Position from this session.
     *
     *  @param positionId the {@code Id} of the position
     *  @throws org.osid.NullArgumentException {@code positionId{@code 
     *          is {@code null}
     */

    @Override
    public void removePosition(org.osid.id.Id positionId) {
        super.removePosition(positionId);
        return;
    }    
}
