//
// AbstractAssemblyPeriodQuery.java
//
//     A PeriodQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.billing.period.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A PeriodQuery that stores terms.
 */

public abstract class AbstractAssemblyPeriodQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.billing.PeriodQuery,
               org.osid.billing.PeriodQueryInspector,
               org.osid.billing.PeriodSearchOrder {

    private final java.util.Collection<org.osid.billing.records.PeriodQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.billing.records.PeriodQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.billing.records.PeriodSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyPeriodQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyPeriodQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Adds a display label for this query. 
     *
     *  @param  label label string to match 
     *  @param  stringMatchType the label match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> label </code> not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> label </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchDisplayLabel(String label, 
                                  org.osid.type.Type stringMatchType, 
                                  boolean match) {
        getAssembler().addStringTerm(getDisplayLabelColumn(), label, stringMatchType, match);
        return;
    }


    /**
     *  Matches a display label that has any value. 
     *
     *  @param  match <code> true </code> to match customers with any label, 
     *          <code> false </code> to match assets with no title 
     */

    @OSID @Override
    public void matchAnyDisplayLabel(boolean match) {
        getAssembler().addStringWildcardTerm(getDisplayLabelColumn(), match);
        return;
    }


    /**
     *  Clears the display label query terms. 
     */

    @OSID @Override
    public void clearDisplayLabelTerms() {
        getAssembler().clearTerms(getDisplayLabelColumn());
        return;
    }


    /**
     *  Gets the display label query terms. 
     *
     *  @return the display labelquery terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getDisplayLabelTerms() {
        return (getAssembler().getStringTerms(getDisplayLabelColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by customer title. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDisplayLabel(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDisplayLabelColumn(), style);
        return;
    }


    /**
     *  Gets the DisplayLabel column name.
     *
     * @return the column name
     */

    protected String getDisplayLabelColumn() {
        return ("display_label");
    }


    /**
     *  Matches the open date between the given range inclusive. 
     *
     *  @param  low low date range 
     *  @param  high high date range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     */

    @OSID @Override
    public void matchOpenDate(org.osid.calendaring.DateTime low, 
                              org.osid.calendaring.DateTime high, 
                              boolean match) {
        getAssembler().addDateTimeRangeTerm(getOpenDateColumn(), low, high, match);
        return;
    }


    /**
     *  Matches a period that has any open date assigned. 
     *
     *  @param  match <code> true </code> to match periods with any open date, 
     *          <code> false </code> to match events with no open date 
     */

    @OSID @Override
    public void matchAnyOpenDate(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getOpenDateColumn(), match);
        return;
    }


    /**
     *  Clears the open date query terms. 
     */

    @OSID @Override
    public void clearOpenDateTerms() {
        getAssembler().clearTerms(getOpenDateColumn());
        return;
    }


    /**
     *  Gets the open date query terms. 
     *
     *  @return the open date query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getOpenDateTerms() {
        return (getAssembler().getDateTimeRangeTerms(getOpenDateColumn()));
    }


    /**
     *  Specified a preference for ordering results by the open date. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByOpenDate(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getOpenDateColumn(), style);
        return;
    }


    /**
     *  Gets the OpenDate column name.
     *
     * @return the column name
     */

    protected String getOpenDateColumn() {
        return ("open_date");
    }


    /**
     *  Matches the close date between the given range inclusive. 
     *
     *  @param  low low date range 
     *  @param  high high date range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     */

    @OSID @Override
    public void matchCloseDate(org.osid.calendaring.DateTime low, 
                               org.osid.calendaring.DateTime high, 
                               boolean match) {
        getAssembler().addDateTimeRangeTerm(getCloseDateColumn(), low, high, match);
        return;
    }


    /**
     *  Matches a period that has any close date assigned. 
     *
     *  @param  match <code> true </code> to match periods with any close 
     *          date, <code> false </code> to match events with no close date 
     */

    @OSID @Override
    public void matchAnyCloseDate(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getCloseDateColumn(), match);
        return;
    }


    /**
     *  Clears the close date query terms. 
     */

    @OSID @Override
    public void clearCloseDateTerms() {
        getAssembler().clearTerms(getCloseDateColumn());
        return;
    }


    /**
     *  Gets the close date query terms. 
     *
     *  @return the close date query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getCloseDateTerms() {
        return (getAssembler().getDateTimeRangeTerms(getCloseDateColumn()));
    }


    /**
     *  Specified a preference for ordering results by close date. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCloseDate(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCloseDateColumn(), style);
        return;
    }


    /**
     *  Gets the CloseDate column name.
     *
     * @return the column name
     */

    protected String getCloseDateColumn() {
        return ("close_date");
    }


    /**
     *  Matches the billing date between the given range inclusive. 
     *
     *  @param  low low date range 
     *  @param  high high date range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     */

    @OSID @Override
    public void matchBillingDate(org.osid.calendaring.DateTime low, 
                                 org.osid.calendaring.DateTime high, 
                                 boolean match) {
        getAssembler().addDateTimeRangeTerm(getBillingDateColumn(), low, high, match);
        return;
    }


    /**
     *  Matches a period that has any billing date assigned. 
     *
     *  @param  match <code> true </code> to match periods with any billing 
     *          date, <code> false </code> to match events with no billing 
     *          date 
     */

    @OSID @Override
    public void matchAnyBillingDate(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getBillingDateColumn(), match);
        return;
    }


    /**
     *  Clears the billing date query terms. 
     */

    @OSID @Override
    public void clearBillingDateTerms() {
        getAssembler().clearTerms(getBillingDateColumn());
        return;
    }


    /**
     *  Gets the billing date query terms. 
     *
     *  @return the billing date query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getBillingDateTerms() {
        return (getAssembler().getDateTimeRangeTerms(getBillingDateColumn()));
    }


    /**
     *  Specified a preference for ordering results by billing date. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByBillingDate(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getBillingDateColumn(), style);
        return;
    }


    /**
     *  Gets the BillingDate column name.
     *
     * @return the column name
     */

    protected String getBillingDateColumn() {
        return ("billing_date");
    }


    /**
     *  Matches the due date between the given range inclusive. 
     *
     *  @param  low low date range 
     *  @param  high high date range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     */

    @OSID @Override
    public void matchDueDate(org.osid.calendaring.DateTime low, 
                             org.osid.calendaring.DateTime high, boolean match) {
        getAssembler().addDateTimeRangeTerm(getDueDateColumn(), low, high, match);
        return;
    }


    /**
     *  Matches a period that has any due date assigned. 
     *
     *  @param  match <code> true </code> to match periods with any due date, 
     *          <code> false </code> to match events with no due date 
     */

    @OSID @Override
    public void matchAnyDueDate(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getDueDateColumn(), match);
        return;
    }


    /**
     *  Clears the due date query terms. 
     */

    @OSID @Override
    public void clearDueDateTerms() {
        getAssembler().clearTerms(getDueDateColumn());
        return;
    }


    /**
     *  Gets the dur date query terms. 
     *
     *  @return the dur date query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getDueDateTerms() {
        return (getAssembler().getDateTimeRangeTerms(getDueDateColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the due date. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDueDate(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDueDateColumn(), style);
        return;
    }


    /**
     *  Gets the DueDate column name.
     *
     * @return the column name
     */

    protected String getDueDateColumn() {
        return ("due_date");
    }


    /**
     *  Sets the business <code> Id </code> for this query to match periods 
     *  assigned to businesses. 
     *
     *  @param  businessId the business <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBusinessId(org.osid.id.Id businessId, boolean match) {
        getAssembler().addIdTerm(getBusinessIdColumn(), businessId, match);
        return;
    }


    /**
     *  Clears the business <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearBusinessIdTerms() {
        getAssembler().clearTerms(getBusinessIdColumn());
        return;
    }


    /**
     *  Gets the business <code> Id </code> query terms. 
     *
     *  @return the business <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBusinessIdTerms() {
        return (getAssembler().getIdTerms(getBusinessIdColumn()));
    }


    /**
     *  Gets the BusinessId column name.
     *
     * @return the column name
     */

    protected String getBusinessIdColumn() {
        return ("business_id");
    }


    /**
     *  Tests if a <code> BusinessQuery </code> is available. 
     *
     *  @return <code> true </code> if a business query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessQuery() {
        return (false);
    }


    /**
     *  Gets the query for a business. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the business query 
     *  @throws org.osid.UnimplementedException <code> supportsBusinessQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.BusinessQuery getBusinessQuery() {
        throw new org.osid.UnimplementedException("supportsBusinessQuery() is false");
    }


    /**
     *  Clears the business query terms. 
     */

    @OSID @Override
    public void clearBusinessTerms() {
        getAssembler().clearTerms(getBusinessColumn());
        return;
    }


    /**
     *  Gets the business query terms. 
     *
     *  @return the business query terms 
     */

    @OSID @Override
    public org.osid.billing.BusinessQueryInspector[] getBusinessTerms() {
        return (new org.osid.billing.BusinessQueryInspector[0]);
    }


    /**
     *  Gets the Business column name.
     *
     * @return the column name
     */

    protected String getBusinessColumn() {
        return ("business");
    }


    /**
     *  Tests if this period supports the given record
     *  <code>Type</code>.
     *
     *  @param  periodRecordType a period record type 
     *  @return <code>true</code> if the periodRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>periodRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type periodRecordType) {
        for (org.osid.billing.records.PeriodQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(periodRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  periodRecordType the period record type 
     *  @return the period query record 
     *  @throws org.osid.NullArgumentException
     *          <code>periodRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(periodRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.records.PeriodQueryRecord getPeriodQueryRecord(org.osid.type.Type periodRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.billing.records.PeriodQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(periodRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(periodRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  periodRecordType the period record type 
     *  @return the period query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>periodRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(periodRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.records.PeriodQueryInspectorRecord getPeriodQueryInspectorRecord(org.osid.type.Type periodRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.billing.records.PeriodQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(periodRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(periodRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param periodRecordType the period record type
     *  @return the period search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>periodRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(periodRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.records.PeriodSearchOrderRecord getPeriodSearchOrderRecord(org.osid.type.Type periodRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.billing.records.PeriodSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(periodRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(periodRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this period. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param periodQueryRecord the period query record
     *  @param periodQueryInspectorRecord the period query inspector
     *         record
     *  @param periodSearchOrderRecord the period search order record
     *  @param periodRecordType period record type
     *  @throws org.osid.NullArgumentException
     *          <code>periodQueryRecord</code>,
     *          <code>periodQueryInspectorRecord</code>,
     *          <code>periodSearchOrderRecord</code> or
     *          <code>periodRecordTypeperiod</code> is
     *          <code>null</code>
     */
            
    protected void addPeriodRecords(org.osid.billing.records.PeriodQueryRecord periodQueryRecord, 
                                      org.osid.billing.records.PeriodQueryInspectorRecord periodQueryInspectorRecord, 
                                      org.osid.billing.records.PeriodSearchOrderRecord periodSearchOrderRecord, 
                                      org.osid.type.Type periodRecordType) {

        addRecordType(periodRecordType);

        nullarg(periodQueryRecord, "period query record");
        nullarg(periodQueryInspectorRecord, "period query inspector record");
        nullarg(periodSearchOrderRecord, "period search odrer record");

        this.queryRecords.add(periodQueryRecord);
        this.queryInspectorRecords.add(periodQueryInspectorRecord);
        this.searchOrderRecords.add(periodSearchOrderRecord);
        
        return;
    }
}
