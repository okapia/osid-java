//
// InvariantMapUtilityLookupSession
//
//    Implements an Utility lookup service backed by a fixed collection of
//    utilities.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.metering;


/**
 *  Implements an Utility lookup service backed by a fixed
 *  collection of utilities. The utilities are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapUtilityLookupSession
    extends net.okapia.osid.jamocha.core.metering.spi.AbstractMapUtilityLookupSession
    implements org.osid.metering.UtilityLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapUtilityLookupSession</code> with no
     *  utilities.
     */

    public InvariantMapUtilityLookupSession() {
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapUtilityLookupSession</code> with a single
     *  utility.
     *  
     *  @throws org.osid.NullArgumentException {@code utility}
     *          is <code>null</code>
     */

    public InvariantMapUtilityLookupSession(org.osid.metering.Utility utility) {
        putUtility(utility);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapUtilityLookupSession</code> using an array
     *  of utilities.
     *  
     *  @throws org.osid.NullArgumentException {@code utilities}
     *          is <code>null</code>
     */

    public InvariantMapUtilityLookupSession(org.osid.metering.Utility[] utilities) {
        putUtilities(utilities);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapUtilityLookupSession</code> using a
     *  collection of utilities.
     *
     *  @throws org.osid.NullArgumentException {@code utilities}
     *          is <code>null</code>
     */

    public InvariantMapUtilityLookupSession(java.util.Collection<? extends org.osid.metering.Utility> utilities) {
        putUtilities(utilities);
        return;
    }
}
