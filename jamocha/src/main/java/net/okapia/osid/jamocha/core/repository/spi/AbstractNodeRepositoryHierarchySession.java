//
// AbstractNodeRepositoryHierarchySession.java
//
//     Defines a Repository hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.repository.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a repository hierarchy session for delivering a hierarchy
 *  of repositories using the RepositoryNode interface.
 */

public abstract class AbstractNodeRepositoryHierarchySession
    extends net.okapia.osid.jamocha.repository.spi.AbstractRepositoryHierarchySession
    implements org.osid.repository.RepositoryHierarchySession {

    private java.util.Collection<org.osid.repository.RepositoryNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root repository <code> Ids </code> in this hierarchy.
     *
     *  @return the root repository <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootRepositoryIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.repository.repositorynode.RepositoryNodeToIdList(this.roots));
    }


    /**
     *  Gets the root repositories in the repository hierarchy. A node
     *  with no parents is an orphan. While all repository <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root repositories 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.RepositoryList getRootRepositories()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.repository.repositorynode.RepositoryNodeToRepositoryList(new net.okapia.osid.jamocha.repository.repositorynode.ArrayRepositoryNodeList(this.roots)));
    }


    /**
     *  Adds a root repository node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootRepository(org.osid.repository.RepositoryNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root repository nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootRepositories(java.util.Collection<org.osid.repository.RepositoryNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root repository node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootRepository(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.repository.RepositoryNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Repository </code> has any parents. 
     *
     *  @param  repositoryId a repository <code> Id </code> 
     *  @return <code> true </code> if the repository has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> repositoryId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> repositoryId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentRepositories(org.osid.id.Id repositoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getRepositoryNode(repositoryId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of a
     *  repository.
     *
     *  @param  id an <code> Id </code> 
     *  @param  repositoryId the <code> Id </code> of a repository 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> repositoryId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> repositoryId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> repositoryId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfRepository(org.osid.id.Id id, org.osid.id.Id repositoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.repository.RepositoryNodeList parents = getRepositoryNode(repositoryId).getParentRepositoryNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextRepositoryNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given repository. 
     *
     *  @param  repositoryId a repository <code> Id </code> 
     *  @return the parent <code> Ids </code> of the repository 
     *  @throws org.osid.NotFoundException <code> repositoryId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> repositoryId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentRepositoryIds(org.osid.id.Id repositoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.repository.repository.RepositoryToIdList(getParentRepositories(repositoryId)));
    }


    /**
     *  Gets the parents of the given repository. 
     *
     *  @param  repositoryId the <code> Id </code> to query 
     *  @return the parents of the repository 
     *  @throws org.osid.NotFoundException <code> repositoryId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> repositoryId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.RepositoryList getParentRepositories(org.osid.id.Id repositoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.repository.repositorynode.RepositoryNodeToRepositoryList(getRepositoryNode(repositoryId).getParentRepositoryNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of a
     *  repository.
     *
     *  @param  id an <code> Id </code> 
     *  @param  repositoryId the Id of a repository 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> repositoryId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> repositoryId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> repositoryId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfRepository(org.osid.id.Id id, org.osid.id.Id repositoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfRepository(id, repositoryId)) {
            return (true);
        }

        try (org.osid.repository.RepositoryList parents = getParentRepositories(repositoryId)) {
            while (parents.hasNext()) {
                if (isAncestorOfRepository(id, parents.getNextRepository().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if a repository has any children. 
     *
     *  @param  repositoryId a repository <code> Id </code> 
     *  @return <code> true </code> if the <code> repositoryId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> repositoryId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> repositoryId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildRepositories(org.osid.id.Id repositoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getRepositoryNode(repositoryId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of a
     *  repository.
     *
     *  @param  id an <code> Id </code> 
     *  @param repositoryId the <code> Id </code> of a 
     *         repository
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> repositoryId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> repositoryId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> repositoryId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfRepository(org.osid.id.Id id, org.osid.id.Id repositoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfRepository(repositoryId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  repository.
     *
     *  @param  repositoryId the <code> Id </code> to query 
     *  @return the children of the repository 
     *  @throws org.osid.NotFoundException <code> repositoryId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> repositoryId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildRepositoryIds(org.osid.id.Id repositoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.repository.repository.RepositoryToIdList(getChildRepositories(repositoryId)));
    }


    /**
     *  Gets the children of the given repository. 
     *
     *  @param  repositoryId the <code> Id </code> to query 
     *  @return the children of the repository 
     *  @throws org.osid.NotFoundException <code> repositoryId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> repositoryId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.RepositoryList getChildRepositories(org.osid.id.Id repositoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.repository.repositorynode.RepositoryNodeToRepositoryList(getRepositoryNode(repositoryId).getChildRepositoryNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of a
     *  repository.
     *
     *  @param  id an <code> Id </code> 
     *  @param repositoryId the <code> Id </code> of a 
     *         repository
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> repositoryId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> repositoryId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> repositoryId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfRepository(org.osid.id.Id id, org.osid.id.Id repositoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfRepository(repositoryId, id)) {
            return (true);
        }

        try (org.osid.repository.RepositoryList children = getChildRepositories(repositoryId)) {
            while (children.hasNext()) {
                if (isDescendantOfRepository(id, children.getNextRepository().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  repository.
     *
     *  @param  repositoryId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified repository node 
     *  @throws org.osid.NotFoundException <code> repositoryId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> repositoryId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getRepositoryNodeIds(org.osid.id.Id repositoryId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.repository.repositorynode.RepositoryNodeToNode(getRepositoryNode(repositoryId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given repository.
     *
     *  @param  repositoryId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified repository node 
     *  @throws org.osid.NotFoundException <code> repositoryId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> repositoryId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.RepositoryNode getRepositoryNodes(org.osid.id.Id repositoryId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getRepositoryNode(repositoryId));
    }


    /**
     *  Closes this <code>RepositoryHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets a repository node.
     *
     *  @param repositoryId the id of the repository node
     *  @throws org.osid.NotFoundException <code>repositoryId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>repositoryId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.repository.RepositoryNode getRepositoryNode(org.osid.id.Id repositoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(repositoryId, "repository Id");
        for (org.osid.repository.RepositoryNode repository : this.roots) {
            if (repository.getId().equals(repositoryId)) {
                return (repository);
            }

            org.osid.repository.RepositoryNode r = findRepository(repository, repositoryId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(repositoryId + " is not found");
    }


    protected org.osid.repository.RepositoryNode findRepository(org.osid.repository.RepositoryNode node, 
                                                                org.osid.id.Id repositoryId) 
	throws org.osid.OperationFailedException {

        try (org.osid.repository.RepositoryNodeList children = node.getChildRepositoryNodes()) {
            while (children.hasNext()) {
                org.osid.repository.RepositoryNode repository = children.getNextRepositoryNode();
                if (repository.getId().equals(repositoryId)) {
                    return (repository);
                }
                
                repository = findRepository(repository, repositoryId);
                if (repository != null) {
                    return (repository);
                }
            }
        }

        return (null);
    }
}
