//
// AbstractPackage.java
//
//     Defines a Package.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.installation.pkg.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Package</code>.
 */

public abstract class AbstractPackage
    extends net.okapia.osid.jamocha.spi.AbstractSourceableOsidObject
    implements org.osid.installation.Package {

    private final java.util.Collection<org.osid.installation.Package> dependencies = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.installation.InstallationContent> contents = new java.util.LinkedHashSet<>();

    private org.osid.installation.Version version;
    private org.osid.locale.DisplayText copyright;
    private org.osid.resource.Resource creator;
    private org.osid.calendaring.DateTime releaseDate;
    private String url;

    boolean requestsLicenseAcknowledgement = false;

    private final java.util.Collection<org.osid.installation.records.PackageRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the version of this package. 
     *
     *  @return the package version 
     */

    @OSID @Override
    public org.osid.installation.Version getVersion() {
        return (this.version);
    }


    /**
     *  Sets the version.
     *
     *  @param version a version
     *  @throws org.osid.NullArgumentException
     *          <code>version</code> is <code>null</code>
     */

    protected void setVersion(org.osid.installation.Version version) {
        nullarg(version, "version");
        this.version = version;
        return;
    }


    /**
     *  Gets the copyright of this package. 
     *
     *  @return the copyright 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getCopyright() {
        return (this.copyright);
    }


    /**
     *  Sets the copyright.
     *
     *  @param copyright a copyright
     *  @throws org.osid.NullArgumentException <code>copyright</code>
     *          is <code>null</code>
     */

    protected void setCopyright(org.osid.locale.DisplayText copyright) {
        nullarg(copyright, "copyright");
        this.copyright = copyright;
        return;
    }


    /**
     *  Tests if the provider requests acknowledgement of the license. 
     *
     *  @return <code> true </code> if the consumer should acknowledge the 
     *          terms in the license, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean requestsLicenseAcknowledgement() {
        return (this.requestsLicenseAcknowledgement);
    }


    /**
     *  Sets the requests license acknowledgement flag.
     *
     *  @param ack a requests license acknowledgement
     */

    protected void setRequestsLicenseAcknowledgement(boolean ack) {
        this.requestsLicenseAcknowledgement = ack;
        return;
    }


    /**
     *  Gets the creator or author of this package. 
     *
     *  @return the creator <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCreatorId() {
        return (this.creator.getId());
    }


    /**
     *  Gets the creator <code> Id </code> of this package. 
     *
     *  @return the creator 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getCreator()
        throws org.osid.OperationFailedException {

        return (this.creator);
    }


    /**
     *  Sets the creator.
     *
     *  @param creator a creator
     *  @throws org.osid.NullArgumentException
     *          <code>creator</code> is <code>null</code>
     */

    protected void setCreator(org.osid.resource.Resource creator) {
        nullarg(creator, "creator");
        this.creator = creator;
        return;
    }


    /**
     *  Gets the release date of this package. 
     *
     *  @return the timestamp of this package 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getReleaseDate() {
        return (this.releaseDate);
    }


    /**
     *  Sets the release date.
     *
     *  @param date a release date
     *  @throws org.osid.NullArgumentException
     *          <code>date</code> is <code>null</code>
     */

    protected void setReleaseDate(org.osid.calendaring.DateTime date) {
        nullarg(date, "release date");
        this.releaseDate = date;
        return;
    }


    /**
     *  Gets the package <code> Ids </code> on which this package
     *  directly depends.
     *
     *  @return the package dependency <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getDependencyIds() {
        try {
            org.osid.installation.PackageList dependencies = getDependencies();
            return (new net.okapia.osid.jamocha.adapter.converter.installation.pkg.PackageToIdList(dependencies));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }        
    }


    /**
     *  Gets the packages on which this package directly depends.
     *
     *  @return the package dependencies 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.installation.PackageList getDependencies()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.installation.pkg.ArrayPackageList(this.dependencies));
    }


    /**
     *  Adds a dependency.
     *
     *  @param pkg a dependency
     *  @throws org.osid.NullArgumentException <code>pkg</code> is
     *          <code>null</code>
     */

    protected void addDependency(org.osid.installation.Package pkg) {
        nullarg(pkg, "package dependency");
        this.dependencies.add(pkg);
        return;
    }


    /**
     *  Sets all the package dependencies.
     *
     *  @param packages a collection of packages
     *  @throws org.osid.NullArgumentException <code>packages</code>
     *          is <code>null</code>
     */

    protected void setDependencies(java.util.Collection<org.osid.installation.Package> packages) {
        nullarg(packages, "package dependencies");

        this.dependencies.clear();
        this.dependencies.addAll(packages);

        return;
    }

    
    /**
     *  Gets a url for this package. The url may point to an external project 
     *  or product site. If no url is available an empty string is returned. 
     *
     *  @return the url for this package 
     */

    @OSID @Override
    public String getURL() {
        return (this.url);
    }


    /**
     *  Sets the url.
     *
     *  @param url a url
     *  @throws org.osid.NullArgumentException <code>url</code> is
     *          <code>null</code>
     */

    protected void setURL(String url) {
        nullarg(url, "url");
        this.url = url;
        return;
    }


    /**
     *  Gets the installation content <code> Ids. </code> 
     *
     *  @return the installation content <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.id.IdList getInstallationContentIds()
        throws org.osid.OperationFailedException {

        try {
            org.osid.installation.InstallationContentList contents = getInstallationContents();
            return (new net.okapia.osid.jamocha.adapter.converter.installation.installationcontent.InstallationContentToIdList(contents));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }        
    }


    /**
     *  Gets the installation contents. 
     *
     *  @return the installation contents 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.installation.InstallationContentList getInstallationContents()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.installation.installationcontent.ArrayInstallationContentList(this.contents));
    }


    /**
     *  Adds an installation content.
     *
     *  @param content an installation content
     *  @throws org.osid.NullArgumentException <code>content</code> is
     *          <code>null</code>
     */

    protected void addInstallationContent(org.osid.installation.InstallationContent content) {
        nullarg(content, "installation content");
        this.contents.add(content);
        return;
    }


    /**
     *  Sets all the installation contents.
     *
     *  @param contents a collection of contents
     *  @throws org.osid.NullArgumentException <code>contents</code>
     *          is <code>null</code>
     */

    protected void setInstallationContents(java.util.Collection<org.osid.installation.InstallationContent> contents) {
        nullarg(contents, "installation contents");

        this.contents.clear();
        this.contents.addAll(contents);

        return;
    }

    
    /**
     *  Tests if this pkg supports the given record
     *  <code>Type</code>.
     *
     *  @param  pkgRecordType a package record type 
     *  @return <code>true</code> if the pkgRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>pkgRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type pkgRecordType) {
        for (org.osid.installation.records.PackageRecord record : this.records) {
            if (record.implementsRecordType(pkgRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>package</code> record <code>Type</code>.
     *
     *  @param  pkgRecordType the package record type 
     *  @return the package record 
     *  @throws org.osid.NullArgumentException
     *          <code>pkgRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(pkgRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.installation.records.PackageRecord getPackageRecord(org.osid.type.Type pkgRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.installation.records.PackageRecord record : this.records) {
            if (record.implementsRecordType(pkgRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(pkgRecordType + " is not supported");
    }


    /**
     *  Adds a record to this package. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param pkgRecord the package record
     *  @param pkgRecordType package record type
     *  @throws org.osid.NullArgumentException
     *          <code>pkgRecord</code> or
     *          <code>pkgRecordTypepkg</code> is
     *          <code>null</code>
     */
            
    protected void addPackageRecord(org.osid.installation.records.PackageRecord pkgRecord, 
                                     org.osid.type.Type pkgRecordType) {

        nullarg(pkgRecord, "package record");
        addRecordType(pkgRecordType);
        this.records.add(pkgRecord);
        
        return;
    }
}
