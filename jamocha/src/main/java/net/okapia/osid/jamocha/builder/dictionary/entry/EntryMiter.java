//
// EntryMiter.java
//
//     Defines an Entry miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.dictionary.entry;


/**
 *  Defines an <code>Entry</code> miter for use with the builders.
 */

public interface EntryMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidObjectMiter,
            org.osid.dictionary.Entry {


    /**
     *  Sets the key type.
     *
     *  @param keyType a key type
     *  @throws org.osid.NullArgumentException <code>keyType</code> is
     *          <code>null</code>
     */

    public void setKeyType(org.osid.type.Type keyType);


    /**
     *  Sets the key.
     *
     *  @param key a key
     *  @throws org.osid.NullArgumentException <code>key</code> is
     *          <code>null</code>
     */

    public void setKey(java.lang.Object key);


    /**
     *  Sets the value type.
     *
     *  @param valueType a value type
     *  @throws org.osid.NullArgumentException <code>valueType</code>
     *          is <code>null</code>
     */

    public void setValueType(org.osid.type.Type valueType);


    /**
     *  Sets the value.
     *
     *  @param value a value
     *  @throws org.osid.NullArgumentException <code>value</code> is
     *          <code>null</code>
     */

    public void setValue(java.lang.Object value);


    /**
     *  Adds an Entry record.
     *
     *  @param record an entry record
     *  @param recordType the type of entry record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addEntryRecord(org.osid.dictionary.records.EntryRecord record, org.osid.type.Type recordType);
}       


