//
// AbstractAdapterOsidGovernator.java
//
//     Defines an OsidGovernator wrapper.
//
//
//
// Tom Coppeto
// Okapia
// 20 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a generic <code>OsidGovernator</code> wrapper. 
 */

public abstract class AbstractAdapterOsidGovernator
    extends AbstractAdapterSourceableOsidObject
    implements org.osid.OsidGovernator {

    private final org.osid.OsidGovernator governator;


    /**
     *  Constructs a new <code>AbstractAdapterOsidGovernator</code>.
     *
     *  @param governator
     *  @throws org.osid.NullArgumentException
     *          <code>governator</code> is <code>null</code>
     */

    protected AbstractAdapterOsidGovernator(org.osid.OsidGovernator governator) {
        super(governator);
        this.governator = governator;
        return;
    }


    /**
     *  Tests if this governator is active. <code> isActive() </code>
     *  is <code> true </code> if <code> isEnabled() </code> and
     *  <code> isOperational() </code> are <code> true </code> and
     *  <code> isDisabled() </code> is <code> false. </code>
     *
     *  @return <code> true </code> if this governator is active,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean isActive() {
        return (this.governator.isActive());
    }


    /**
     *  Tests if this governator is administravely
     *  enabled. Administratively enabling overrides any enabling rule
     *  which may exist. If this method returns <code> true </code>
     *  then <code> isDisabled() </code> must return <code>
     *  false. </code>
     *
     *  @return <code> true </code> if this governator is enabled,
     *          <code> false </code> is the active status is
     *          determined by other rules
     */

    @OSID @Override
    public boolean isEnabled() {
        return (this.governator.isEnabled());
    }


    /**
     *  Tests if this governator is administravely
     *  disabled. Administratively disabling overrides any disabling
     *  rule which may exist. If this method returns <code> true
     *  </code> then <code> isEnabled() </code> must return <code>
     *  false. </code>
     *
     *  @return <code> true </code> if this governator is disabled,
     *          <code> false </code> is the active status is
     *          determined by other rules
     */

    @OSID @Override
    public boolean isDisabled() {
        return (this.governator.isDisabled());
    }


    /**
     *  Tests if this governator is operational in that all rules
     *  pertaining to this operation except for an administrative
     *  disable are <code> true.  </code>
     *
     *  @return <code> true </code> if this governator is operational,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean isOperational() {
        return (this.governator.isOperational());
    }
}
