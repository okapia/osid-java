//
// AbstractDemographic.java
//
//     Defines a Demographic.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resource.demographic.demographic.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Demographic</code>.
 */

public abstract class AbstractDemographic
    extends net.okapia.osid.jamocha.spi.AbstractOsidRule
    implements org.osid.resource.demographic.Demographic {

    private final java.util.Collection<org.osid.resource.demographic.Demographic> includedDemographics = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.resource.demographic.Demographic> includedIntersectingDemographics = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.resource.demographic.Demographic> includedExclusiveDemographics = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.resource.demographic.Demographic> excludedDemographics = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.resource.Resource> includedResources = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.resource.Resource> excludedResources = new java.util.LinkedHashSet<>();

    private final java.util.Collection<org.osid.resource.demographic.records.DemographicRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets a list of <code> Demographic </code> <code> Ids </code> whose 
     *  members are added to this demographic. The union of these demographics 
     *  is added to this demographic. 
     *
     *  @return the demographic <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getIncludedDemographicIds() {
        try {
            org.osid.resource.demographic.DemographicList includedDemographics = getIncludedDemographics();
            return (new net.okapia.osid.jamocha.adapter.converter.resource.demographic.demographic.DemographicToIdList(includedDemographics));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets a list of <code> Demographics </code> whose members are added to 
     *  this demographic. The union of these demographics is added to this 
     *  demographic. 
     *
     *  @return the demographics 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicList getIncludedDemographics()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.resource.demographic.demographic.ArrayDemographicList(this.includedDemographics));
    }


    /**
     *  Adds an included demographic.
     *
     *  @param demographic an included demographic
     *  @throws org.osid.NullArgumentException
     *          <code>demographic</code> is <code>null</code>
     */

    protected void addIncludedDemographic(org.osid.resource.demographic.Demographic demographic) {
        nullarg(demographic, "demographic");
        this.includedDemographics.add(demographic);
        return;
    }


    /**
     *  Sets all the included demographics.
     *
     *  @param demographics a collection of included demographics
     *  @throws org.osid.NullArgumentException
     *          <code>demographics</code> is <code>null</code>
     */

    protected void setIncludedDemographics(java.util.Collection<org.osid.resource.demographic.Demographic> demographics) {
        nullarg(demographics, "demographics");
        this.includedDemographics.clear();
        this.includedDemographics.addAll(demographics);
        return;
    }


    /**
     *  Gets a list of <code> Demographic </code> <code> Ids </code>
     *  whose intersection is added to this demographic. The
     *  intersection of these demographics is added to this
     *  demographic.
     *
     *  @return the intersecting demographic <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getIncludedIntersectingDemographicIds() {
        try {
            org.osid.resource.demographic.DemographicList includedIntersectingDemographics = getIncludedIntersectingDemographics();
            return (new net.okapia.osid.jamocha.adapter.converter.resource.demographic.demographic.DemographicToIdList(includedIntersectingDemographics));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets a list of <code> Demographics </code> whose intersection is added 
     *  to this demographic. The intersection of these demographics is added 
     *  to this demographic. 
     *
     *  @return the intersecting demographics 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicList getIncludedIntersectingDemographics()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.resource.demographic.demographic.ArrayDemographicList(this.includedIntersectingDemographics));
    }


    /**
     *  Adds an included intersecting demographic.
     *
     *  @param includedIntersectingDemographic an included intersecting demographic
     *  @throws org.osid.NullArgumentException
     *          <code>includedIntersectingDemographic</code> is <code>null</code>
     */

    protected void addIncludedIntersectingDemographic(org.osid.resource.demographic.Demographic includedIntersectingDemographic) {
        this.includedIntersectingDemographics.add(includedIntersectingDemographic);
        return;
    }


    /**
     *  Sets all the included intersecting demographics.
     *
     *  @param includedIntersectingDemographics a collection of included intersecting demographics
     *  @throws org.osid.NullArgumentException
     *          <code>includedIntersectingDemographics</code> is <code>null</code>
     */

    protected void setIncludedIntersectingDemographics(java.util.Collection<org.osid.resource.demographic.Demographic> includedIntersectingDemographics) {
        this.includedIntersectingDemographics.clear();
        this.includedIntersectingDemographics.addAll(includedIntersectingDemographics);
        return;
    }


    /**
     *  Gets a list of <code> Demographic </code> <code> Ids </code> whose 
     *  non-members are added to this demographic. 
     *
     *  @return the exclusive demographic <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getIncludedExclusiveDemographicIds() {
        try {
            org.osid.resource.demographic.DemographicList includedExclusiveDemographics = getIncludedExclusiveDemographics();
            return (new net.okapia.osid.jamocha.adapter.converter.resource.demographic.demographic.DemographicToIdList(includedExclusiveDemographics));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets a list of <code> Demographics </code> whose non-members are added 
     *  to this demographic. 
     *
     *  @return the exclusive demographics 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicList getIncludedExclusiveDemographics()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.resource.demographic.demographic.ArrayDemographicList(this.includedExclusiveDemographics));
    }


    /**
     *  Adds an included exclusive demographic.
     *
     *  @param demographic an included exclusive demographic
     *  @throws org.osid.NullArgumentException
     *          <code>demographic</code> is <code>null</code>
     */

    protected void addIncludedExclusiveDemographic(org.osid.resource.demographic.Demographic demographic) {
        nullarg(demographic, "demographic");
        this.includedExclusiveDemographics.add(demographic);
        return;
    }


    /**
     *  Sets all the included exclusive demographics.
     *
     *  @param demographics a collection of included exclusive demographics
     *  @throws org.osid.NullArgumentException
     *          <code>demographics</code> is <code>null</code>
     */

    protected void setIncludedExclusiveDemographics(java.util.Collection<org.osid.resource.demographic.Demographic> demographics) {
        nullarg(demographics, "demographics");
        this.includedExclusiveDemographics.clear();
        this.includedExclusiveDemographics.addAll(demographics);
        return;
    }


    /**
     *  Gets a list of <code> Demographic </code> <code> Ids </code> whose 
     *  members are subtracted from in this demographic. 
     *
     *  @return the excluded demographic <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getExcludedDemographicIds() {
        try {
            org.osid.resource.demographic.DemographicList excludedDemographics = getExcludedDemographics();
            return (new net.okapia.osid.jamocha.adapter.converter.resource.demographic.demographic.DemographicToIdList(excludedDemographics));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets a list of <code> Demographics </code> whose members subtracted 
     *  from this demographic. 
     *
     *  @return the excluded demographics 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicList getExcludedDemographics()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.resource.demographic.demographic.ArrayDemographicList(this.excludedDemographics));
    }


    /**
     *  Adds an excluded demographic.
     *
     *  @param demographic an excluded demographic
     *  @throws org.osid.NullArgumentException
     *          <code>demographic</code> is <code>null</code>
     */

    protected void addExcludedDemographic(org.osid.resource.demographic.Demographic demographic) {
        nullarg(demographic, "demographic");
        this.excludedDemographics.add(demographic);
        return;
    }


    /**
     *  Sets all the excluded demographics.
     *
     *  @param demographics a collection of excluded demographics
     *  @throws org.osid.NullArgumentException
     *          <code>demographics</code> is <code>null</code>
     */

    protected void setExcludedDemographics(java.util.Collection<org.osid.resource.demographic.Demographic> demographics) {
        nullarg(demographics, "demographics");
        this.excludedDemographics.clear();
        this.excludedDemographics.addAll(demographics);
        return;
    }


    /**
     *  Gets a list of individual <code> Resource </code> <code> Ids
     *  </code> to be added to this demographic.
     *
     *  @return the included resource <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getIncludedResourceIds() {
        try {
            org.osid.resource.ResourceList includedResources = getIncludedResources();
            return (new net.okapia.osid.jamocha.adapter.converter.resource.resource.ResourceToIdList(includedResources));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets a list of individual <code> Resources </code> to be added to this 
     *  demographic. 
     *
     *  @return the included resources 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getIncludedResources()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.resource.resource.ArrayResourceList(this.includedResources));
    }


    /**
     *  Adds an included resource.
     *
     *  @param resource an included resource
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    protected void addIncludedResource(org.osid.resource.Resource resource) {
        nullarg(resource, "resource");
        this.includedResources.add(resource);
        return;
    }


    /**
     *  Sets all the included resources.
     *
     *  @param resources a collection of included resources
     *  @throws org.osid.NullArgumentException
     *          <code>resources</code> is <code>null</code>
     */

    protected void setIncludedResources(java.util.Collection<org.osid.resource.Resource> resources) {
        nullarg(resources, "resources");
        this.includedResources.clear();
        this.includedResources.addAll(resources);
        return;
    }


    /**
     *  Gets a list of <code> Resource </code> <code> Ids </code> to be 
     *  subtracted from this demographic. 
     *
     *  @return the excluded resource <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getExcludedResourceIds() {
        try {
            org.osid.resource.ResourceList excludedResources = getExcludedResources();
            return (new net.okapia.osid.jamocha.adapter.converter.resource.resource.ResourceToIdList(excludedResources));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets a list of <code> Resources </code> to be subtracted from this 
     *  demographic. 
     *
     *  @return the exempted resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getExcludedResources()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.resource.resource.ArrayResourceList(this.excludedResources));
    }


    /**
     *  Adds an excluded resource.
     *
     *  @param resource an excluded resource
     *  @throws org.osid.NullArgumentException
     *          <code>resource</code> is <code>null</code>
     */

    protected void addExcludedResource(org.osid.resource.Resource resource) {
        nullarg(resource, "resource");
        this.excludedResources.add(resource);
        return;
    }


    /**
     *  Sets all the excluded resources.
     *
     *  @param resources a collection of excluded resources
     *  @throws org.osid.NullArgumentException <code>resources</code>
     *          is <code>null</code>
     */

    protected void setExcludedResources(java.util.Collection<org.osid.resource.Resource> resources) {
        nullarg(resources, "resources");
        this.excludedResources.clear();
        this.excludedResources.addAll(resources);
        return;
    }


    /**
     *  Tests if this demographic supports the given record
     *  <code>Type</code>.
     *
     *  @param  demographicRecordType a demographic record type 
     *  @return <code>true</code> if the demographicRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>demographicRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type demographicRecordType) {
        for (org.osid.resource.demographic.records.DemographicRecord record : this.records) {
            if (record.implementsRecordType(demographicRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Demographic</code> record <code>Type</code>.
     *
     *  @param  demographicRecordType the demographic record type 
     *  @return the demographic record 
     *  @throws org.osid.NullArgumentException
     *          <code>demographicRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(demographicRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resource.demographic.records.DemographicRecord getDemographicRecord(org.osid.type.Type demographicRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resource.demographic.records.DemographicRecord record : this.records) {
            if (record.implementsRecordType(demographicRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(demographicRecordType + " is not supported");
    }


    /**
     *  Adds a record to this demographic. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param demographicRecord the demographic record
     *  @param demographicRecordType demographic record type
     *  @throws org.osid.NullArgumentException
     *          <code>demographicRecord</code> or
     *          <code>demographicRecordTypedemographic</code> is
     *          <code>null</code>
     */
            
    protected void addDemographicRecord(org.osid.resource.demographic.records.DemographicRecord demographicRecord, 
                                        org.osid.type.Type demographicRecordType) {
        
        nullarg(demographicRecord, "demographic record");
        addRecordType(demographicRecordType);
        this.records.add(demographicRecord);
        
        return;
    }
}
