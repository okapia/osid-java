//
// AbstractMapOntologyLookupSession
//
//    A simple framework for providing an Ontology lookup service
//    backed by a fixed collection of ontologies.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.ontology.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an Ontology lookup service backed by a
 *  fixed collection of ontologies. The ontologies are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Ontologies</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapOntologyLookupSession
    extends net.okapia.osid.jamocha.ontology.spi.AbstractOntologyLookupSession
    implements org.osid.ontology.OntologyLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.ontology.Ontology> ontologies = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.ontology.Ontology>());


    /**
     *  Makes an <code>Ontology</code> available in this session.
     *
     *  @param  ontology an ontology
     *  @throws org.osid.NullArgumentException <code>ontology<code>
     *          is <code>null</code>
     */

    protected void putOntology(org.osid.ontology.Ontology ontology) {
        this.ontologies.put(ontology.getId(), ontology);
        return;
    }


    /**
     *  Makes an array of ontologies available in this session.
     *
     *  @param  ontologies an array of ontologies
     *  @throws org.osid.NullArgumentException <code>ontologies<code>
     *          is <code>null</code>
     */

    protected void putOntologies(org.osid.ontology.Ontology[] ontologies) {
        putOntologies(java.util.Arrays.asList(ontologies));
        return;
    }


    /**
     *  Makes a collection of ontologies available in this session.
     *
     *  @param  ontologies a collection of ontologies
     *  @throws org.osid.NullArgumentException <code>ontologies<code>
     *          is <code>null</code>
     */

    protected void putOntologies(java.util.Collection<? extends org.osid.ontology.Ontology> ontologies) {
        for (org.osid.ontology.Ontology ontology : ontologies) {
            this.ontologies.put(ontology.getId(), ontology);
        }

        return;
    }


    /**
     *  Removes an Ontology from this session.
     *
     *  @param  ontologyId the <code>Id</code> of the ontology
     *  @throws org.osid.NullArgumentException <code>ontologyId<code> is
     *          <code>null</code>
     */

    protected void removeOntology(org.osid.id.Id ontologyId) {
        this.ontologies.remove(ontologyId);
        return;
    }


    /**
     *  Gets the <code>Ontology</code> specified by its <code>Id</code>.
     *
     *  @param  ontologyId <code>Id</code> of the <code>Ontology</code>
     *  @return the ontology
     *  @throws org.osid.NotFoundException <code>ontologyId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>ontologyId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.Ontology getOntology(org.osid.id.Id ontologyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.ontology.Ontology ontology = this.ontologies.get(ontologyId);
        if (ontology == null) {
            throw new org.osid.NotFoundException("ontology not found: " + ontologyId);
        }

        return (ontology);
    }


    /**
     *  Gets all <code>Ontologies</code>. In plenary mode, the returned
     *  list contains all known ontologies or an error
     *  results. Otherwise, the returned list may contain only those
     *  ontologies that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Ontologies</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.OntologyList getOntologies()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.ontology.ontology.ArrayOntologyList(this.ontologies.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.ontologies.clear();
        super.close();
        return;
    }
}
