//
// AbstractFederatingCommitmentLookupSession.java
//
//     An abstract federating adapter for a CommitmentLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  CommitmentLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingCommitmentLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.calendaring.CommitmentLookupSession>
    implements org.osid.calendaring.CommitmentLookupSession {

    private boolean parallel = false;
    private org.osid.calendaring.Calendar calendar = new net.okapia.osid.jamocha.nil.calendaring.calendar.UnknownCalendar();


    /**
     *  Constructs a new <code>AbstractFederatingCommitmentLookupSession</code>.
     */

    protected AbstractFederatingCommitmentLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.calendaring.CommitmentLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Calendar/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Calendar Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCalendarId() {
        return (this.calendar.getId());
    }


    /**
     *  Gets the <code>Calendar</code> associated with this 
     *  session.
     *
     *  @return the <code>Calendar</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.calendar);
    }


    /**
     *  Sets the <code>Calendar</code>.
     *
     *  @param  calendar the calendar for this session
     *  @throws org.osid.NullArgumentException <code>calendar</code>
     *          is <code>null</code>
     */

    protected void setCalendar(org.osid.calendaring.Calendar calendar) {
        nullarg(calendar, "calendar");
        this.calendar = calendar;
        return;
    }


    /**
     *  Tests if this user can perform <code>Commitment</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCommitments() {
        for (org.osid.calendaring.CommitmentLookupSession session : getSessions()) {
            if (session.canLookupCommitments()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Commitment</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCommitmentView() {
        for (org.osid.calendaring.CommitmentLookupSession session : getSessions()) {
            session.useComparativeCommitmentView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Commitment</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCommitmentView() {
        for (org.osid.calendaring.CommitmentLookupSession session : getSessions()) {
            session.usePlenaryCommitmentView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include commitments in calendars which are children
     *  of this calendar in the calendar hierarchy.
     */

    @OSID @Override
    public void useFederatedCalendarView() {
        for (org.osid.calendaring.CommitmentLookupSession session : getSessions()) {
            session.useFederatedCalendarView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this calendar only.
     */

    @OSID @Override
    public void useIsolatedCalendarView() {
        for (org.osid.calendaring.CommitmentLookupSession session : getSessions()) {
            session.useIsolatedCalendarView();
        }

        return;
    }


    /**
     *  Only commitments whose effective dates are current are returned by
     *  methods in this session.
     */

    @OSID @Override
    public void useEffectiveCommitmentView() {
        for (org.osid.calendaring.CommitmentLookupSession session : getSessions()) {
            session.useEffectiveCommitmentView();
        }

        return;
    }


    /**
     *  All commitments of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveCommitmentView() {
        for (org.osid.calendaring.CommitmentLookupSession session : getSessions()) {
            session.useAnyEffectiveCommitmentView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Commitment</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Commitment</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Commitment</code> and
     *  retained for compatibility.
     *
     *  In effective mode, commitments are returned that are currently
     *  effective.  In any effective mode, effective commitments and
     *  those currently expired are returned.
     *
     *  @param  commitmentId <code>Id</code> of the
     *          <code>Commitment</code>
     *  @return the commitment
     *  @throws org.osid.NotFoundException <code>commitmentId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>commitmentId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Commitment getCommitment(org.osid.id.Id commitmentId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.calendaring.CommitmentLookupSession session : getSessions()) {
            try {
                return (session.getCommitment(commitmentId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(commitmentId + " not found");
    }


    /**
     *  Gets a <code>CommitmentList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  commitments specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Commitments</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, commitments are returned that are currently effective.
     *  In any effective mode, effective commitments and those currently expired
     *  are returned.
     *
     *  @param  commitmentIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Commitment</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>commitmentIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitmentsByIds(org.osid.id.IdList commitmentIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.calendaring.commitment.MutableCommitmentList ret = new net.okapia.osid.jamocha.calendaring.commitment.MutableCommitmentList();

        try (org.osid.id.IdList ids = commitmentIds) {
            while (ids.hasNext()) {
                ret.addCommitment(getCommitment(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>CommitmentList</code> corresponding to the given
     *  commitment genus <code>Type</code> which does not include
     *  commitments of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  commitments or an error results. Otherwise, the returned list
     *  may contain only those commitments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, commitments are returned that are currently effective.
     *  In any effective mode, effective commitments and those currently expired
     *  are returned.
     *
     *  @param  commitmentGenusType a commitment genus type 
     *  @return the returned <code>Commitment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>commitmentGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitmentsByGenusType(org.osid.type.Type commitmentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.commitment.FederatingCommitmentList ret = getCommitmentList();

        for (org.osid.calendaring.CommitmentLookupSession session : getSessions()) {
            ret.addCommitmentList(session.getCommitmentsByGenusType(commitmentGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CommitmentList</code> corresponding to the given
     *  commitment genus <code>Type</code> and include any additional
     *  commitments with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  commitments or an error results. Otherwise, the returned list
     *  may contain only those commitments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, commitments are returned that are currently
     *  effective.  In any effective mode, effective commitments and
     *  those currently expired are returned.
     *
     *  @param  commitmentGenusType a commitment genus type 
     *  @return the returned <code>Commitment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>commitmentGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitmentsByParentGenusType(org.osid.type.Type commitmentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.commitment.FederatingCommitmentList ret = getCommitmentList();

        for (org.osid.calendaring.CommitmentLookupSession session : getSessions()) {
            ret.addCommitmentList(session.getCommitmentsByParentGenusType(commitmentGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CommitmentList</code> containing the given
     *  commitment record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  commitments or an error results. Otherwise, the returned list
     *  may contain only those commitments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, commitments are returned that are currently
     *  effective.  In any effective mode, effective commitments and
     *  those currently expired are returned.
     *
     *  @param  commitmentRecordType a commitment record type 
     *  @return the returned <code>Commitment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>commitmentRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitmentsByRecordType(org.osid.type.Type commitmentRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.commitment.FederatingCommitmentList ret = getCommitmentList();

        for (org.osid.calendaring.CommitmentLookupSession session : getSessions()) {
            ret.addCommitmentList(session.getCommitmentsByRecordType(commitmentRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CommitmentList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  commitments or an error results. Otherwise, the returned list
     *  may contain only those commitments that are accessible
     *  through this session.
     *  
     *  In active mode, commitments are returned that are currently
     *  active. In any status mode, active and inactive commitments
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Commitment</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitmentsOnDate(org.osid.calendaring.DateTime from, 
                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.commitment.FederatingCommitmentList ret = getCommitmentList();

        for (org.osid.calendaring.CommitmentLookupSession session : getSessions()) {
            ret.addCommitmentList(session.getCommitmentsOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a <code> CommitmentList </code> effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *
     *  In plenary mode, the returned list contains all known
     *  commitments or an error results. Otherwise, the returned list
     *  may contain only those commitments that are accessible through
     *  this session.
     *
     *  In effective mode, commitments are returned that are currently
     *  effective. In any effective mode, effective commitments and those
     *  currently expired are returned.
     *
     *  @param  commitmentGenusType a commitment genus type
     *  @param  from starting date
     *  @param  to ending date
     *  @return list of commitments
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException
     *          <code>commitmentGenusType</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitmentsByGenusTypeOnDate(org.osid.type.Type commitmentGenusType,
                                                                               org.osid.calendaring.DateTime from,
                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.commitment.FederatingCommitmentList ret = getCommitmentList();

        for (org.osid.calendaring.CommitmentLookupSession session : getSessions()) {
            ret.addCommitmentList(session.getCommitmentsByGenusTypeOnDate(commitmentGenusType, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of commitments corresponding to a event
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  commitments or an error results. Otherwise, the returned list
     *  may contain only those commitments that are accessible
     *  through this session.
     *
     *  In effective mode, commitments are returned that are
     *  currently effective.  In any effective mode, effective
     *  commitments and those currently expired are returned.
     *
     *  @param  eventId the <code>Id</code> of the event
     *  @return the returned <code>CommitmentList</code>
     *  @throws org.osid.NullArgumentException <code>eventId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitmentsForEvent(org.osid.id.Id eventId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

         net.okapia.osid.jamocha.adapter.federator.calendaring.commitment.FederatingCommitmentList ret = getCommitmentList();
         
         for (org.osid.calendaring.CommitmentLookupSession session : getSessions()) {
             ret.addCommitmentList(session.getCommitmentsForEvent(eventId));
         }
         
         ret.noMore();
         return (ret);
     }


     /**
      *  Gets the commitments for the given event and commitment genus
      *  type including any genus types derived from the given genus
      *  type.
      *
      *  If the event is a recurring event, the commitments are
      *  returned for the recurring event only. In plenary mode, the
      *  returned list contains all of the commitments mapped to the
      *  event <code> Id </code> or an error results if an Id in the
      *  supplied list is not found or inaccessible. Otherwise,
      *  inaccessible commitments may be ommitted.
      *
      *  In effective mode, commitments are returned that are currently
      *  effective. In any effective mode, effective commitments and
      *  those currently expired are returned.
      *
      *  @param  eventId <code> Id </code> of the <code> Event </code>
      *  @param  commitmentGenusType commitment genus type
      *  @return list of commitments
      *  @throws org.osid.NullArgumentException <code> eventId </code> or
      *          <code> commitmentGenusType </code> is <code> null </code>
      *  @throws org.osid.OperationFailedException unable to complete request
      *  @throws org.osid.PermissionDeniedException authorization failure
      */

    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitmentsByGenusTypeForEvent(org.osid.id.Id eventId,
                                                                                  org.osid.type.Type commitmentGenusType)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.commitment.FederatingCommitmentList ret = getCommitmentList();
        
        for (org.osid.calendaring.CommitmentLookupSession session : getSessions()) {
            ret.addCommitmentList(session.getCommitmentsByGenusTypeForEvent(eventId, commitmentGenusType));
        }
        
        ret.noMore();
        return (ret);
    }
    
    
    /**
     *  Gets a list of commitments corresponding to a event
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  commitments or an error results. Otherwise, the returned list
     *  may contain only those commitments that are accessible
     *  through this session.
     *
     *  In effective mode, commitments are returned that are
     *  currently effective.  In any effective mode, effective
     *  commitments and those currently expired are returned.
     *
     *  @param  eventId the <code>Id</code> of the event
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>CommitmentList</code>
     *  @throws org.osid.NullArgumentException <code>eventId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */
    
    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitmentsForEventOnDate(org.osid.id.Id eventId,
                                                                            org.osid.calendaring.DateTime from,
                                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.commitment.FederatingCommitmentList ret = getCommitmentList();
        
        for (org.osid.calendaring.CommitmentLookupSession session : getSessions()) {
            ret.addCommitmentList(session.getCommitmentsForEventOnDate(eventId, from, to));
        }
        
        ret.noMore();
        return (ret);
    }
    

    /**
     *  Gets a <code> CommitmentList </code> for the given event and
     *  commitment genus type effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  commitments or an error results. Otherwise, the returned list
     *  may contain only those commitments that are accessible through
     *  this session.
     *
     *  In effective mode, commitments are returned that are currently
     *  effective. In any effective mode, effective commitments and
     *  those currently expired are returned.
     *
     *  @param  eventId <code> Id </code> of the <code> Event </code>
     *  @param  commitmentGenusType commitment genus type
     *  @param  from starting date
     *  @param  to ending date
     *  @return list of commitments
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> eventId,
     *          commitmentGenusType, from </code> or <code> to </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitmentsByGenusTypeForEventOnDate(org.osid.id.Id eventId,
                                                                                       org.osid.type.Type commitmentGenusType,
                                                                                       org.osid.calendaring.DateTime from,
                                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.commitment.FederatingCommitmentList ret = getCommitmentList();
        
        for (org.osid.calendaring.CommitmentLookupSession session : getSessions()) {
            ret.addCommitmentList(session.getCommitmentsByGenusTypeForEventOnDate(eventId, commitmentGenusType, from, to));
        }
        
        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of commitments corresponding to a resource
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  commitments or an error results. Otherwise, the returned list
     *  may contain only those commitments that are accessible
     *  through this session.
     *
     *  In effective mode, commitments are returned that are
     *  currently effective.  In any effective mode, effective
     *  commitments and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @return the returned <code>CommitmentList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */
    
    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitmentsForResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.commitment.FederatingCommitmentList ret = getCommitmentList();
        
        for (org.osid.calendaring.CommitmentLookupSession session : getSessions()) {
            ret.addCommitmentList(session.getCommitmentsForResource(resourceId));
        }
        
        ret.noMore();
        return (ret);
    }
    
    
    /**
     *  Gets a list of commitments corresponding to a resource
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  commitments or an error results. Otherwise, the returned list
     *  may contain only those commitments that are accessible
     *  through this session.
     *
     *  In effective mode, commitments are returned that are
     *  currently effective.  In any effective mode, effective
     *  commitments and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>CommitmentList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */
    
    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitmentsForResourceOnDate(org.osid.id.Id resourceId,
                                                                               org.osid.calendaring.DateTime from,
                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        net.okapia.osid.jamocha.adapter.federator.calendaring.commitment.FederatingCommitmentList ret = getCommitmentList();
        
        for (org.osid.calendaring.CommitmentLookupSession session : getSessions()) {
            ret.addCommitmentList(session.getCommitmentsForResourceOnDate(resourceId, from, to));
        }
        
        ret.noMore();
        return (ret);
    }
    

    /**
     *  Gets the commitments for the given resource.
     *
     *  In plenary mode, the returned list contains all of the
     *  commitments mapped to the resource <code> Id </code> or an
     *  error results if an Id in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code> Commitments
     *  </code> may be omitted from the list and may present the
     *  elements in any order including returning a unique set.
     *
     *  In effective mode, commitments are returned that are currently
     *  effective. In any effective mode, effective commitments and
     *  those currently expired are returned.
     *
     *  @param  resourceId <code> Id </code> of a <code> Resource </code>
     *  @param  commitmentGenusType commitment genus type
     *  @return list of commitments
     *  @throws org.osid.NullArgumentException <code> resourceId </code> or
     *  <code> commitmentGenusType </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitmentsByGenusTypeForResource(org.osid.id.Id resourceId,
                                                                                    org.osid.type.Type commitmentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.commitment.FederatingCommitmentList ret = getCommitmentList();
        
        for (org.osid.calendaring.CommitmentLookupSession session : getSessions()) {
            ret.addCommitmentList(session.getCommitmentsByGenusTypeForResource(resourceId, commitmentGenusType));
        }
        
        ret.noMore();
        return (ret);
    }

    
    /**
     *  Gets a <code> CommitmentList </code> for the given resource
     *  and effective during the entire given date range inclusive but
     *  not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  commitments or an error results. Otherwise, the returned list
     *  may contain only those commitments that are accessible through
     *  this session.
     *
     *  In effective mode, commitments are returned that are currently
     *  effective. In any effective mode, effective commitments and
     *  those currently expired are returned.
     *
     *  @param  resourceId <code> Id </code> of the <code> Event </code>
     *  @param  commitmentGenusType commitment genus type
     *  @param  from starting date
     *  @param  to ending date
     *  @return list of commitments
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> resourceId,
     *          commitmentGenusType, from </code> or <code> to </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitmentsByGenusTypeForResourceOnDate(org.osid.id.Id resourceId,
                                                                                          org.osid.type.Type commitmentGenusType,
                                                                                          org.osid.calendaring.DateTime from,
                                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.commitment.FederatingCommitmentList ret = getCommitmentList();
        
        for (org.osid.calendaring.CommitmentLookupSession session : getSessions()) {
            ret.addCommitmentList(session.getCommitmentsByGenusTypeForResourceOnDate(resourceId, commitmentGenusType, from, to));
        }
        
        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of commitments corresponding to event and resource
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  commitments or an error results. Otherwise, the returned list
     *  may contain only those commitments that are accessible
     *  through this session.
     *
     *  In effective mode, commitments are returned that are
     *  currently effective.  In any effective mode, effective
     *  commitments and those currently expired are returned.
     *
     *  @param  eventId the <code>Id</code> of the event
     *  @param  resourceId the <code>Id</code> of the resource
     *  @return the returned <code>CommitmentList</code>
     *  @throws org.osid.NullArgumentException <code>eventId</code>,
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */
    
    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitmentsForEventAndResource(org.osid.id.Id eventId,
                                                                                 org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.commitment.FederatingCommitmentList ret = getCommitmentList();
        
        for (org.osid.calendaring.CommitmentLookupSession session : getSessions()) {
            ret.addCommitmentList(session.getCommitmentsForEventAndResource(eventId, resourceId));
        }
        
        ret.noMore();
        return (ret);
    }
    
    
    /**
     *  Gets a list of commitments corresponding to event and resource
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  commitments or an error results. Otherwise, the returned list
     *  may contain only those commitments that are accessible
     *  through this session.
     *
     *  In effective mode, commitments are returned that are
     *  currently effective.  In any effective mode, effective
     *  commitments and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>CommitmentList</code>
     *  @throws org.osid.NullArgumentException <code>eventId</code>,
     *          <code>resourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */
    
    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitmentsForEventAndResourceOnDate(org.osid.id.Id eventId,
                                                                                       org.osid.id.Id resourceId,
                                                                                       org.osid.calendaring.DateTime from,
                                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.commitment.FederatingCommitmentList ret = getCommitmentList();
        
        for (org.osid.calendaring.CommitmentLookupSession session : getSessions()) {
            ret.addCommitmentList(session.getCommitmentsForEventAndResourceOnDate(eventId, resourceId, from, to));
        }
        
        ret.noMore();
        return (ret);
    }
    

    /**
     *  Gets the commitmentsof the given genus type for the given
     *  event and resource. If the event is a recurring event, the
     *  commitments are returned for the recurring event only.
     *
     *  In plenary mode, the returned list contains all of the
     *  commitments mapped to the event <code> Id </code> or an error
     *  results if an Id in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code> Commitments
     *  </code> may be omitted from the list and may present the
     *  elements in any order including returning a unique set.
     *
     *  In effective mode, commitments are returned that are currently
     *  effective. In any effective mode, effective commitments and
     *  those currently expired are returned.
     *
     *  @param  eventId <code> Id </code> of the <code> Event </code>
     *  @param  resourceId <code> Id </code> of a <code> Resource </code>
     *  @param  commitmentGenusType commitment genus type
     *  @return list of commitments
     *  @throws org.osid.NullArgumentException <code> eventId, resourceId
     *          </code> or <code> commitmentGenusType </code> is <code> null
     *  </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitmentsByGenusTypeForEventAndResource(org.osid.id.Id eventId,
                                                                                            org.osid.id.Id resourceId,
                                                                                            org.osid.type.Type commitmentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.commitment.FederatingCommitmentList ret = getCommitmentList();
        
        for (org.osid.calendaring.CommitmentLookupSession session : getSessions()) {
            ret.addCommitmentList(session.getCommitmentsByGenusTypeForEventAndResource(eventId, resourceId, commitmentGenusType));
        }
        
        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code> CommitmentList </code> of the given genus type
     *  for the given event and resource and effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *
     *  In plenary mode, the returned list contains all known
     *  commitments or an error results. Otherwise, the returned list
     *  may contain only those commitments that are accessible through
     *  this session.
     *
     *  In effective mode, commitments are returned that are currently
     *  effective. In any effective mode, effective commitments and
     *  those currently expired are returned.
     *
     *  @param  eventId <code> Id </code> of the <code> Event </code>
     *  @param  resourceId <code> Id </code> of a <code> Resource </code>
     *  @param  commitmentGenusType commitment genus type
     *  @param  from starting date
     *  @param  to ending date
     *  @return list of commitments
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> eventId, resourceId,
     *          commitmentGenusType, from </code> or <code> to </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitmentsByGenusTypeForEventAndResourceOnDate(org.osid.id.Id eventId,
                                                                                                  org.osid.id.Id resourceId,
                                                                                                  org.osid.type.Type commitmentGenusType,
                                                                                                  org.osid.calendaring.DateTime from,
                                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.commitment.FederatingCommitmentList ret = getCommitmentList();
        
        for (org.osid.calendaring.CommitmentLookupSession session : getSessions()) {
            ret.addCommitmentList(session.getCommitmentsByGenusTypeForEventAndResourceOnDate(eventId, resourceId, commitmentGenusType, from, to));
        }
        
        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Commitments</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  commitments or an error results. Otherwise, the returned list
     *  may contain only those commitments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, commitments are returned that are currently
     *  effective.  In any effective mode, effective commitments and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Commitments</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */
    
    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitments()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.commitment.FederatingCommitmentList ret = getCommitmentList();
        
        for (org.osid.calendaring.CommitmentLookupSession session : getSessions()) {
            ret.addCommitmentList(session.getCommitments());
        }
        
        ret.noMore();
        return (ret);
    }
    
    
    protected net.okapia.osid.jamocha.adapter.federator.calendaring.commitment.FederatingCommitmentList getCommitmentList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.calendaring.commitment.ParallelCommitmentList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.calendaring.commitment.CompositeCommitmentList());
        }
    }
}
