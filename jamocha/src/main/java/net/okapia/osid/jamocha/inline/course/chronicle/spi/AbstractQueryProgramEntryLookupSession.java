//
// AbstractQueryProgramEntryLookupSession.java
//
//    An inline adapter that maps a ProgramEntryLookupSession to
//    a ProgramEntryQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.course.chronicle.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a ProgramEntryLookupSession to
 *  a ProgramEntryQuerySession.
 */

public abstract class AbstractQueryProgramEntryLookupSession
    extends net.okapia.osid.jamocha.course.chronicle.spi.AbstractProgramEntryLookupSession
    implements org.osid.course.chronicle.ProgramEntryLookupSession {

    private boolean effectiveonly = false;
    private final org.osid.course.chronicle.ProgramEntryQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryProgramEntryLookupSession.
     *
     *  @param querySession the underlying program entry query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryProgramEntryLookupSession(org.osid.course.chronicle.ProgramEntryQuerySession querySession) {
        nullarg(querySession, "program entry query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>CourseCatalog</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>CourseCatalog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.session.getCourseCatalogId());
    }


    /**
     *  Gets the <code>CourseCatalog</code> associated with this 
     *  session.
     *
     *  @return the <code>CourseCatalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getCourseCatalog());
    }


    /**
     *  Tests if this user can perform <code>ProgramEntry</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupProgramEntries() {
        return (this.session.canSearchProgramEntries());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include program entries in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.session.useFederatedCourseCatalogView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.session.useIsolatedCourseCatalogView();
        return;
    }
    

    /**
     *  Only program entries whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveProgramEntryView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All program entries of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveProgramEntryView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>ProgramEntry</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>ProgramEntry</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>ProgramEntry</code> and
     *  retained for compatibility.
     *
     *  In effective mode, program entries are returned that are currently
     *  effective.  In any effective mode, effective program entries and
     *  those currently expired are returned.
     *
     *  @param  programEntryId <code>Id</code> of the
     *          <code>ProgramEntry</code>
     *  @return the program entry
     *  @throws org.osid.NotFoundException <code>programEntryId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>programEntryId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntry getProgramEntry(org.osid.id.Id programEntryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.chronicle.ProgramEntryQuery query = getQuery();
        query.matchId(programEntryId, true);
        org.osid.course.chronicle.ProgramEntryList programEntries = this.session.getProgramEntriesByQuery(query);
        if (programEntries.hasNext()) {
            return (programEntries.getNextProgramEntry());
        } 
        
        throw new org.osid.NotFoundException(programEntryId + " not found");
    }


    /**
     *  Gets a <code>ProgramEntryList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  programEntries specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>ProgramEntries</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, program entries are returned that are currently effective.
     *  In any effective mode, effective program entries and those currently expired
     *  are returned.
     *
     *  @param  programEntryIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>ProgramEntry</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>programEntryIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryList getProgramEntriesByIds(org.osid.id.IdList programEntryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.chronicle.ProgramEntryQuery query = getQuery();

        try (org.osid.id.IdList ids = programEntryIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getProgramEntriesByQuery(query));
    }


    /**
     *  Gets a <code>ProgramEntryList</code> corresponding to the given
     *  program entry genus <code>Type</code> which does not include
     *  program entries of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  program entries or an error results. Otherwise, the returned list
     *  may contain only those program entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, program entries are returned that are currently effective.
     *  In any effective mode, effective program entries and those currently expired
     *  are returned.
     *
     *  @param  programEntryGenusType a programEntry genus type 
     *  @return the returned <code>ProgramEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>programEntryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryList getProgramEntriesByGenusType(org.osid.type.Type programEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.chronicle.ProgramEntryQuery query = getQuery();
        query.matchGenusType(programEntryGenusType, true);
        return (this.session.getProgramEntriesByQuery(query));
    }


    /**
     *  Gets a <code>ProgramEntryList</code> corresponding to the given
     *  program entry genus <code>Type</code> and include any additional
     *  program entries with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  program entries or an error results. Otherwise, the returned list
     *  may contain only those program entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, program entries are returned that are currently
     *  effective.  In any effective mode, effective program entries and
     *  those currently expired are returned.
     *
     *  @param  programEntryGenusType a programEntry genus type 
     *  @return the returned <code>ProgramEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>programEntryGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryList getProgramEntriesByParentGenusType(org.osid.type.Type programEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.chronicle.ProgramEntryQuery query = getQuery();
        query.matchParentGenusType(programEntryGenusType, true);
        return (this.session.getProgramEntriesByQuery(query));
    }


    /**
     *  Gets a <code>ProgramEntryList</code> containing the given
     *  program entry record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  program entries or an error results. Otherwise, the returned list
     *  may contain only those program entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, program entries are returned that are currently
     *  effective.  In any effective mode, effective program entries and
     *  those currently expired are returned.
     *
     *  @param  programEntryRecordType a programEntry record type 
     *  @return the returned <code>ProgramEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>programEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryList getProgramEntriesByRecordType(org.osid.type.Type programEntryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.chronicle.ProgramEntryQuery query = getQuery();
        query.matchRecordType(programEntryRecordType, true);
        return (this.session.getProgramEntriesByQuery(query));
    }


    /**
     *  Gets a <code>ProgramEntryList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  program entries or an error results. Otherwise, the returned list
     *  may contain only those program entries that are accessible
     *  through this session.
     *  
     *  In effective mode, program entries are returned that are currently
     *  effective.  In any effective mode, effective program entries and
     *  those currently expired are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>ProgramEntry</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryList getProgramEntriesOnDate(org.osid.calendaring.DateTime from, 
                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.chronicle.ProgramEntryQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getProgramEntriesByQuery(query));
    }
        

    /**
     *  Gets a list of program entries corresponding to a student
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  program entries or an error results. Otherwise, the returned list
     *  may contain only those program entries that are accessible
     *  through this session.
     *
     *  In effective mode, program entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  program entries and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the student
     *  @return the returned <code>ProgramEntryList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.course.chronicle.ProgramEntryList getProgramEntriesForStudent(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.course.chronicle.ProgramEntryQuery query = getQuery();
        query.matchStudentId(resourceId, true);
        return (this.session.getProgramEntriesByQuery(query));
    }


    /**
     *  Gets a list of program entries corresponding to a student
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  program entries or an error results. Otherwise, the returned list
     *  may contain only those program entries that are accessible
     *  through this session.
     *
     *  In effective mode, program entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  program entries and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the student
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ProgramEntryList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryList getProgramEntriesForStudentOnDate(org.osid.id.Id resourceId,
                                                                                        org.osid.calendaring.DateTime from,
                                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.chronicle.ProgramEntryQuery query = getQuery();
        query.matchStudentId(resourceId, true);
        query.matchDate(from, to, true);
        return (this.session.getProgramEntriesByQuery(query));
    }


    /**
     *  Gets a list of program entries corresponding to a program
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  program entries or an error results. Otherwise, the returned list
     *  may contain only those program entries that are accessible
     *  through this session.
     *
     *  In effective mode, program entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  program entries and those currently expired are returned.
     *
     *  @param  programId the <code>Id</code> of the program
     *  @return the returned <code>ProgramEntryList</code>
     *  @throws org.osid.NullArgumentException <code>programId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.course.chronicle.ProgramEntryList getProgramEntriesForProgram(org.osid.id.Id programId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.course.chronicle.ProgramEntryQuery query = getQuery();
        query.matchProgramId(programId, true);
        return (this.session.getProgramEntriesByQuery(query));
    }


    /**
     *  Gets a list of program entries corresponding to a program
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  program entries or an error results. Otherwise, the returned list
     *  may contain only those program entries that are accessible
     *  through this session.
     *
     *  In effective mode, program entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  program entries and those currently expired are returned.
     *
     *  @param  programId the <code>Id</code> of the program
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ProgramEntryList</code>
     *  @throws org.osid.NullArgumentException <code>programId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryList getProgramEntriesForProgramOnDate(org.osid.id.Id programId,
                                                                                        org.osid.calendaring.DateTime from,
                                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.chronicle.ProgramEntryQuery query = getQuery();
        query.matchProgramId(programId, true);
        query.matchDate(from, to, true);
        return (this.session.getProgramEntriesByQuery(query));
    }


    /**
     *  Gets a list of program entries corresponding to student and program
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  program entries or an error results. Otherwise, the returned list
     *  may contain only those program entries that are accessible
     *  through this session.
     *
     *  In effective mode, program entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  program entries and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the student
     *  @param  programId the <code>Id</code> of the program
     *  @return the returned <code>ProgramEntryList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>programId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryList getProgramEntriesForStudentAndProgram(org.osid.id.Id resourceId,
                                                                                            org.osid.id.Id programId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.course.chronicle.ProgramEntryQuery query = getQuery();
        query.matchStudentId(resourceId, true);
        query.matchProgramId(programId, true);
        return (this.session.getProgramEntriesByQuery(query));
    }


    /**
     *  Gets a list of program entries corresponding to student and program
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  program entries or an error results. Otherwise, the returned list
     *  may contain only those program entries that are accessible
     *  through this session.
     *
     *  In effective mode, program entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  program entries and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the student
     *  @param  programId the <code>Id</code> of the program
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ProgramEntryList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>programId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryList getProgramEntriesForStudentAndProgramOnDate(org.osid.id.Id resourceId,
                                                                                                  org.osid.id.Id programId,
                                                                                                  org.osid.calendaring.DateTime from,
                                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.course.chronicle.ProgramEntryQuery query = getQuery();
        query.matchStudentId(resourceId, true);
        query.matchProgramId(programId, true);
        query.matchDate(from, to, true);
        return (this.session.getProgramEntriesByQuery(query));
    }

    
    /**
     *  Gets all <code>ProgramEntries</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  program entries or an error results. Otherwise, the returned list
     *  may contain only those program entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, program entries are returned that are currently
     *  effective.  In any effective mode, effective program entries and
     *  those currently expired are returned.
     *
     *  @return a list of <code>ProgramEntries</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryList getProgramEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.chronicle.ProgramEntryQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getProgramEntriesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.course.chronicle.ProgramEntryQuery getQuery() {
        org.osid.course.chronicle.ProgramEntryQuery query = this.session.getProgramEntryQuery();
        
        if (isEffectiveOnly()) {
            query.matchEffective(true);
        }

        return (query);
    }
}
