//
// AbstractBrokerProcessorQuery.java
//
//     A template for making a BrokerProcessor Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.rules.brokerprocessor.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for broker processors.
 */

public abstract class AbstractBrokerProcessorQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidProcessorQuery
    implements org.osid.provisioning.rules.BrokerProcessorQuery {

    private final java.util.Collection<org.osid.provisioning.rules.records.BrokerProcessorQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches brokers that issue leases. 
     *
     *  @param  match <code> true </code> to match brokers that issue leases, 
     *          <code> false </code> to match brokers that issue permanent 
     *          provisions 
     */

    @OSID @Override
    public void matchLeasing(boolean match) {
        return;
    }


    /**
     *  Clears the leasing query terms. 
     */

    @OSID @Override
    public void clearLeasingTerms() {
        return;
    }


    /**
     *  Matches brokers that issue fixed duration leases between the given 
     *  durations inclusive. 
     *
     *  @param  from starting duration range 
     *  @param  to ending duration range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchFixedLeaseDuration(org.osid.calendaring.Duration from, 
                                        org.osid.calendaring.Duration to, 
                                        boolean match) {
        return;
    }


    /**
     *  Matches brokers with any fixed lease duration. 
     *
     *  @param  match <code> true </code> to match brokers with any fixed 
     *          lease duration, returns, <code> false </code> otherwise 
     */

    @OSID @Override
    public void matchAnyFixedLeaseDuration(boolean match) {
        return;
    }


    /**
     *  Clears the fixed lease duration query terms. 
     */

    @OSID @Override
    public void clearFixedLeaseDurationTerms() {
        return;
    }


    /**
     *  Matches brokers that require provisions to be returned. 
     *
     *  @param  match <code> true </code> to match brokers that require 
     *          provision returns,, <code> false </code> otherwise 
     */

    @OSID @Override
    public void matchMustReturnProvisions(boolean match) {
        return;
    }


    /**
     *  Clears the must return provisions query terms. 
     */

    @OSID @Override
    public void clearMustReturnProvisionsTerms() {
        return;
    }


    /**
     *  Matches brokers that allow provision exchange. 
     *
     *  @param  match <code> true </code> to match brokers that permit 
     *          provision exchange, <code> false </code> otherwise 
     */

    @OSID @Override
    public void matchAllowsProvisionExchange(boolean match) {
        return;
    }


    /**
     *  Clears the allows provision exchange query terms. 
     */

    @OSID @Override
    public void clearAllowsProvisionExchangeTerms() {
        return;
    }


    /**
     *  Matches brokers that allow comound requests. 
     *
     *  @param  match <code> true </code> to match brokers that permit 
     *          compound requests, <code> false </code> otherwise 
     */

    @OSID @Override
    public void matchAllowsCompoundRequests(boolean match) {
        return;
    }


    /**
     *  Clears the allows compound requests query terms. 
     */

    @OSID @Override
    public void clearAllowsCompoundRequestsTerms() {
        return;
    }


    /**
     *  Matches mapped to the broker. 
     *
     *  @param  distributorId the broker <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledBrokerId(org.osid.id.Id distributorId, boolean match) {
        return;
    }


    /**
     *  Clears the broker <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledBrokerIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BrokerQuery </code> is available. 
     *
     *  @return <code> true </code> if a broker query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledBrokerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a broker. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the broker query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledBrokerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerQuery getRuledBrokerQuery() {
        throw new org.osid.UnimplementedException("supportsRuledBrokerQuery() is false");
    }


    /**
     *  Matches mapped to any broker. 
     *
     *  @param  match <code> true </code> for mapped to any broker, <code> 
     *          false </code> to match mapped to no broker 
     */

    @OSID @Override
    public void matchAnyRuledBroker(boolean match) {
        return;
    }


    /**
     *  Clears the broker query terms. 
     */

    @OSID @Override
    public void clearRuledBrokerTerms() {
        return;
    }


    /**
     *  Matches mapped to the distributor. 
     *
     *  @param  distributorId the distributor <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDistributorId(org.osid.id.Id distributorId, boolean match) {
        return;
    }


    /**
     *  Clears the distributor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDistributorIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> DistributorQuery </code> is available. 
     *
     *  @return <code> true </code> if a distributor query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDistributorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a distributor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the distributor query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDistributorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQuery getDistributorQuery() {
        throw new org.osid.UnimplementedException("supportsDistributorQuery() is false");
    }


    /**
     *  Clears the distributor query terms. 
     */

    @OSID @Override
    public void clearDistributorTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given broker processor query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a broker processor implementing the requested record.
     *
     *  @param brokerProcessorRecordType a broker processor record type
     *  @return the broker processor query record
     *  @throws org.osid.NullArgumentException
     *          <code>brokerProcessorRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(brokerProcessorRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.BrokerProcessorQueryRecord getBrokerProcessorQueryRecord(org.osid.type.Type brokerProcessorRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.rules.records.BrokerProcessorQueryRecord record : this.records) {
            if (record.implementsRecordType(brokerProcessorRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(brokerProcessorRecordType + " is not supported");
    }


    /**
     *  Adds a record to this broker processor query. 
     *
     *  @param brokerProcessorQueryRecord broker processor query record
     *  @param brokerProcessorRecordType brokerProcessor record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addBrokerProcessorQueryRecord(org.osid.provisioning.rules.records.BrokerProcessorQueryRecord brokerProcessorQueryRecord, 
                                          org.osid.type.Type brokerProcessorRecordType) {

        addRecordType(brokerProcessorRecordType);
        nullarg(brokerProcessorQueryRecord, "broker processor query record");
        this.records.add(brokerProcessorQueryRecord);        
        return;
    }
}
