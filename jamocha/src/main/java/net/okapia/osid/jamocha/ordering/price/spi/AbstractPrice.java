//
// AbstractPrice.java
//
//     Defines a Price.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ordering.price.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Price</code>.
 */

public abstract class AbstractPrice
    extends net.okapia.osid.jamocha.spi.AbstractOsidRule
    implements org.osid.ordering.Price {

    private org.osid.ordering.PriceSchedule priceSchedule;
    private long minimumQuantity = -1;
    private long maximumQuantity = -1;
    private org.osid.resource.Resource demographic;
    private org.osid.financials.Currency amount;
    private org.osid.calendaring.Duration recurringInterval;

    private final java.util.Collection<org.osid.ordering.records.PriceRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the price schedule <code> Id </code> to which this price
     *  belongs.
     *
     *  @return the price schedule <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getPriceScheduleId() {
        return (this.priceSchedule.getId());
    }


    /**
     *  Gets the price schedule to which this price belongs. 
     *
     *  @return the price schedule 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.ordering.PriceSchedule getPriceSchedule()
        throws org.osid.OperationFailedException {

        return (this.priceSchedule);
    }


    /**
     *  Sets the price schedule.
     *
     *  @param schedule a price schedule
     *  @throws org.osid.NullArgumentException <code>schedulex</code>
     *          is <code>null</code>
     */

    protected void setPriceSchedule(org.osid.ordering.PriceSchedule schedule) {
        nullarg(schedule, "price schedule");
        this.priceSchedule = schedule;
        return;
    }


    /**
     *  Tests if this price is restricted by quantity. 
     *
     *  @return <code> true </code> if this price has a quantity range, <code> 
     *          false </code> if this price is the same for any quantity 
     */

    @OSID @Override
    public boolean hasQuantityRange() {
        return ((this.minimumQuantity >= 0) && (this.maximumQuantity >= 0));
    }


    /**
     *  Gets the minium quantity for this price. 
     *
     *  @return the quantity 
     *  @throws org.osid.IllegalStateException <code> hasQuantityRange() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public long getMinimumQuantity() {
        if (!hasQuantityRange()) {
            throw new org.osid.IllegalStateException("hasQuantityRange() is false");
        }

        return (this.minimumQuantity);
    }


    /**
     *  Sets the minimum quantity.
     *
     *  @param quantity the minimum quantity
     *  @throws org.osid.InvalidArgumentException
     *          <code>quantity</code> is negative
     */

    protected void setMinimumQuantity(long quantity) {
        cardinalarg(quantity, "minimum quantity");
        this.minimumQuantity = quantity;
        return;
    }


    /**
     *  Gets the maximum quantity for this price. 
     *
     *  @return the quantity 
     *  @throws org.osid.IllegalStateException <code>
     *          hasQuantityRange() </code> is <code> false </code>
     */

    @OSID @Override
    public long getMaximumQuantity() {
        if (!hasQuantityRange()) {
            throw new org.osid.IllegalStateException("hasQuantityRange() is false");
        }

        return (this.maximumQuantity);
    }


    /**
     *  Sets the maximum quantity.
     *
     *  @param quantity the maximum quantity
     *  @throws org.osid.InvalidArgumentException
     *          <code>quantity</code> is negative
     */

    protected void setMaximumQuantity(long quantity) {
        cardinalarg(quantity, "maximum quantity");
        this.maximumQuantity = quantity;
        return;
    }


    /**
     *  Tests if this price is restricted by demographic. 
     *
     *  @return <code> true </code> if this price has a demographic, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasDemographic() {
        return (this.demographic != null);
    }


    /**
     *  Gets the resource <code> Id </code> representing the
     *  demographic.
     *
     *  @return the resource <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasDemographic()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.id.Id getDemographicId() {
        if (!hasDemographic()) {
            throw new org.osid.IllegalStateException("hasDemographic() is null");
        }

        return (this.demographic.getId());
    }


    /**
     *  Gets the resource representing the demographic. 
     *
     *  @return the resource 
     *  @throws org.osid.IllegalStateException <code> hasDemographic() </code> 
     *          is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getDemographic()
        throws org.osid.OperationFailedException {

        if (!hasDemographic()) {
            throw new org.osid.IllegalStateException("hasDemographic() is null");
        }

        return (this.demographic);
    }


    /**
     *  Sets the demographic.
     *
     *  @param demographic a demographic
     *  @throws org.osid.NullArgumentException
     *          <code>demographic</code> is <code>null</code>
     */

    protected void setDemographic(org.osid.resource.Resource demographic) {
        nullarg(demographic, "demographic");
        this.demographic = demographic;
        return;
    }


    /**
     *  Gets the amount. 
     *
     *  @return the amount 
     */

    @OSID @Override
    public org.osid.financials.Currency getAmount() {
        return (this.amount);
    }


    /**
     *  Sets the amount.
     *
     *  @param amount an amount
     *  @throws org.osid.NullArgumentException
     *          <code>amount</code> is <code>null</code>
     */

    protected void setAmount(org.osid.financials.Currency amount) {
        nullarg(amount, "amount");
        this.amount = amount;
        return;
    }


    /**
     *  Tests if this price has a recurring charge. 
     *
     *  @return <code> true </code> if this price is recurring, <code> false 
     *          </code> if one-time 
     */

    @OSID @Override
    public boolean isRecurring() {
        return (this.recurringInterval != null);
    }


    /**
     *  Gets the recurring interval. 
     *
     *  @return the interval 
     *  @throws org.osid.IllegalStateException <code> isRecurring() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getRecurringInterval() {
        if (!isRecurring()) {
            throw new org.osid.IllegalStateException("isRecurring() is false");
        }

        return (this.recurringInterval);
    }


    /**
     *  Sets the recurring interval.
     *
     *  @param interval a recurring interval
     *  @throws org.osid.NullArgumentException <code>interval</code>
     *          is <code>null</code>
     */

    protected void setRecurringInterval(org.osid.calendaring.Duration interval) {
        nullarg(interval, "recurring interval");
        this.recurringInterval = interval;
        return;
    }


    /**
     *  Tests if this price supports the given record
     *  <code>Type</code>.
     *
     *  @param  priceRecordType a price record type 
     *  @return <code>true</code> if the priceRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>priceRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type priceRecordType) {
        for (org.osid.ordering.records.PriceRecord record : this.records) {
            if (record.implementsRecordType(priceRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given <code>Price</code>
     *  record <code>Type</code>.
     *
     *  @param  priceRecordType the price record type 
     *  @return the price record 
     *  @throws org.osid.NullArgumentException
     *          <code>priceRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(priceRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.ordering.records.PriceRecord getPriceRecord(org.osid.type.Type priceRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ordering.records.PriceRecord record : this.records) {
            if (record.implementsRecordType(priceRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(priceRecordType + " is not supported");
    }


    /**
     *  Adds a record to this price. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param priceRecord the price record
     *  @param priceRecordType price record type
     *  @throws org.osid.NullArgumentException
     *          <code>priceRecord</code> or
     *          <code>priceRecordTypeprice</code> is
     *          <code>null</code>
     */
            
    protected void addPriceRecord(org.osid.ordering.records.PriceRecord priceRecord, 
                                  org.osid.type.Type priceRecordType) {
        
        nullarg(priceRecord, "price record");
        addRecordType(priceRecordType);
        this.records.add(priceRecord);
        
        return;
    }
}
