//
// AbstractActionValidator.java
//
//     Validates an Action.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.control.action.spi;


/**
 *  Validates an Action.
 */

public abstract class AbstractActionValidator
    extends net.okapia.osid.jamocha.builder.validator.spi.AbstractOsidRuleValidator {


    /**
     *  Constructs a new <code>AbstractActionValidator</code>.
     */

    protected AbstractActionValidator() {
        return;
    }


    /**
     *  Constructs a new <code>AbstractActionValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    protected AbstractActionValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates an Action.
     *
     *  @param action an action to validate
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>action</code>
     *          is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in
     *          assembly
     */
    
    public void validate(org.osid.control.Action action) {
        super.validate(action);

        testNestedObject(action, "getActionGroup");
        test(action.getDelay(), "getDelay()");

        if (action.executesActionGroup() &&
            (action.executesScene() || action.executesSetting() ||
             action.executesMatchingSetting())) {
            throw new org.osid.BadLogicException("executesActionGroup() is true");
        }

        if (action.executesScene() &&
            (action.executesActionGroup() || action.executesSetting() ||
             action.executesMatchingSetting())) {
            throw new org.osid.BadLogicException("executesSetting() is true");
        }

        if (action.executesSetting() &&
            (action.executesActionGroup() || action.executesScene() ||
             action.executesMatchingSetting())) {
            throw new org.osid.BadLogicException("executesSetting() is true");
        }

        if (action.executesMatchingSetting() &&
            (action.executesActionGroup() || action.executesScene() ||
             action.executesSetting())) {
            throw new org.osid.BadLogicException("executesMatchingSetting() is true");
        }

        testConditionalMethod(action, "getNextActionGroupId", action.executesActionGroup(), "executesActionGroup()");
        testConditionalMethod(action, "getNextActionGroup", action.executesActionGroup(), "executesActionGroup()");
        if (action.executesActionGroup()) {
            testNestedObject(action, "getNextActionGroup");
        }

        testConditionalMethod(action, "getSceneId", action.executesScene(), "executesScene()");
        testConditionalMethod(action, "getScene", action.executesScene(), "executesScene()");
        if (action.executesScene()) {
            testNestedObject(action, "getScene");
        }

        testConditionalMethod(action, "getSettingId", action.executesSetting(), "executesSetting()");
        testConditionalMethod(action, "getSetting", action.executesSetting(), "executesSetting()");
        if (action.executesSetting()) {
            testNestedObject(action, "getSetting");
        }

        testConditionalMethod(action, "getMatchingControllerId", action.executesMatchingSetting(), "executesMatchingSetting()");
        testConditionalMethod(action, "getMatchingController", action.executesMatchingSetting(), "executesMatchingSetting()");
        if (action.executesMatchingSetting()) {
            testNestedObject(action, "getMatchingController");
        }

        test(action.getMatchingAmountFactor(), "getMatchingAmountFactor()");
        test(action.getMatchingRateFactor(), "getMatchingRateFactor()");

        return;
    }
}
