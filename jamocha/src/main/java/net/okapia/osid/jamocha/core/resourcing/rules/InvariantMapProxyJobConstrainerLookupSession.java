//
// InvariantMapProxyJobConstrainerLookupSession
//
//    Implements a JobConstrainer lookup service backed by a fixed
//    collection of jobConstrainers. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing.rules;


/**
 *  Implements a JobConstrainer lookup service backed by a fixed
 *  collection of job constrainers. The job constrainers are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyJobConstrainerLookupSession
    extends net.okapia.osid.jamocha.core.resourcing.rules.spi.AbstractMapJobConstrainerLookupSession
    implements org.osid.resourcing.rules.JobConstrainerLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyJobConstrainerLookupSession} with no
     *  job constrainers.
     *
     *  @param foundry the foundry
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyJobConstrainerLookupSession(org.osid.resourcing.Foundry foundry,
                                                  org.osid.proxy.Proxy proxy) {
        setFoundry(foundry);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyJobConstrainerLookupSession} with a single
     *  job constrainer.
     *
     *  @param foundry the foundry
     *  @param jobConstrainer a single job constrainer
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code foundry},
     *          {@code jobConstrainer} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyJobConstrainerLookupSession(org.osid.resourcing.Foundry foundry,
                                                  org.osid.resourcing.rules.JobConstrainer jobConstrainer, org.osid.proxy.Proxy proxy) {

        this(foundry, proxy);
        putJobConstrainer(jobConstrainer);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyJobConstrainerLookupSession} using
     *  an array of job constrainers.
     *
     *  @param foundry the foundry
     *  @param jobConstrainers an array of job constrainers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code foundry},
     *          {@code jobConstrainers} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyJobConstrainerLookupSession(org.osid.resourcing.Foundry foundry,
                                                  org.osid.resourcing.rules.JobConstrainer[] jobConstrainers, org.osid.proxy.Proxy proxy) {

        this(foundry, proxy);
        putJobConstrainers(jobConstrainers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyJobConstrainerLookupSession} using a
     *  collection of job constrainers.
     *
     *  @param foundry the foundry
     *  @param jobConstrainers a collection of job constrainers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code foundry},
     *          {@code jobConstrainers} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyJobConstrainerLookupSession(org.osid.resourcing.Foundry foundry,
                                                  java.util.Collection<? extends org.osid.resourcing.rules.JobConstrainer> jobConstrainers,
                                                  org.osid.proxy.Proxy proxy) {

        this(foundry, proxy);
        putJobConstrainers(jobConstrainers);
        return;
    }
}
