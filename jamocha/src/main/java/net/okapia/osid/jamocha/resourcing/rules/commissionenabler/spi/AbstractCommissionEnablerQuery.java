//
// AbstractCommissionEnablerQuery.java
//
//     A template for making a CommissionEnabler Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.rules.commissionenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for commission enablers.
 */

public abstract class AbstractCommissionEnablerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQuery
    implements org.osid.resourcing.rules.CommissionEnablerQuery {

    private final java.util.Collection<org.osid.resourcing.rules.records.CommissionEnablerQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches mapped to a commission. 
     *
     *  @param  foundryId the commission <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledCommissionId(org.osid.id.Id foundryId, boolean match) {
        return;
    }


    /**
     *  Clears the commission <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledCommissionIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> CommissionQuery </code> is available. 
     *
     *  @return <code> true </code> if a commission query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledCommissionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a commission. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the commission query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledCommissionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionQuery getRuledCommissionQuery() {
        throw new org.osid.UnimplementedException("supportsRuledCommissionQuery() is false");
    }


    /**
     *  Matches mapped to any commission. 
     *
     *  @param  match <code> true </code> for mapped to any commission, <code> 
     *          false </code> to match mapped to no commissions 
     */

    @OSID @Override
    public void matchAnyRuledCommission(boolean match) {
        return;
    }


    /**
     *  Clears the commission query terms. 
     */

    @OSID @Override
    public void clearRuledCommissionTerms() {
        return;
    }


    /**
     *  Matches mapped to the foundry. 
     *
     *  @param  foundryId the foundry <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchFoundryId(org.osid.id.Id foundryId, boolean match) {
        return;
    }


    /**
     *  Clears the foundry <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearFoundryIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> FoundryQuery </code> is available. 
     *
     *  @return <code> true </code> if a foundry query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFoundryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a foundry. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the foundry query 
     *  @throws org.osid.UnimplementedException <code> supportsFoundryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryQuery getFoundryQuery() {
        throw new org.osid.UnimplementedException("supportsFoundryQuery() is false");
    }


    /**
     *  Clears the foundry query terms. 
     */

    @OSID @Override
    public void clearFoundryTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given commission enabler query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a commission enabler implementing the requested record.
     *
     *  @param commissionEnablerRecordType a commission enabler record type
     *  @return the commission enabler query record
     *  @throws org.osid.NullArgumentException
     *          <code>commissionEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(commissionEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.rules.records.CommissionEnablerQueryRecord getCommissionEnablerQueryRecord(org.osid.type.Type commissionEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.rules.records.CommissionEnablerQueryRecord record : this.records) {
            if (record.implementsRecordType(commissionEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(commissionEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this commission enabler query. 
     *
     *  @param commissionEnablerQueryRecord commission enabler query record
     *  @param commissionEnablerRecordType commissionEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCommissionEnablerQueryRecord(org.osid.resourcing.rules.records.CommissionEnablerQueryRecord commissionEnablerQueryRecord, 
                                          org.osid.type.Type commissionEnablerRecordType) {

        addRecordType(commissionEnablerRecordType);
        nullarg(commissionEnablerQueryRecord, "commission enabler query record");
        this.records.add(commissionEnablerQueryRecord);        
        return;
    }
}
