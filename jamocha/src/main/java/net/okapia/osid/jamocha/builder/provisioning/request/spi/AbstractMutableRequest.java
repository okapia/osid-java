//
// AbstractMutableRequest.java
//
//     Defines a mutable Request.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.provisioning.request.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>Request</code>.
 */

public abstract class AbstractMutableRequest
    extends net.okapia.osid.jamocha.provisioning.request.spi.AbstractRequest
    implements org.osid.provisioning.Request,
               net.okapia.osid.jamocha.builder.provisioning.request.RequestMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this request. 
     *
     *  @param record request record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addRequestRecord(org.osid.provisioning.records.RequestRecord record, org.osid.type.Type recordType) {
        super.addRequestRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Sets the start date for when this request is effective.
     *
     *  @param date the start date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setStartDate(org.osid.calendaring.DateTime date) {
        super.setStartDate(date);
        return;
    }


    /**
     *  Sets the end date for when this request ceases to be
     *  effective.
     *
     *  @param date the end date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setEndDate(org.osid.calendaring.DateTime date) {
        super.setEndDate(date);
        return;
    }


    /**
     *  Sets the display name for this request.
     *
     *  @param displayName the name for this request
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this request.
     *
     *  @param description the description of this request
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException
     *          <code>genusType</code> is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the end state for the expiration of this request
     *  relationship.
     *
     *  @param state the end state
     *  @throws org.osid.NullArgumentException <code>state</code> is
     *          <code>null</code>
     */

    @Override
    public void setEndReason(org.osid.process.State state) {
        super.setEndReason(state);
        return;
    }


    /**
     *  Sets the request transaction.
     *
     *  @param requestTransaction a request transaction
     *  @throws org.osid.NullArgumentException
     *          <code>requestTransaction</code> is <code>null</code>
     */

    @Override
    public void setRequestTransaction(org.osid.provisioning.RequestTransaction requestTransaction) {
        super.setRequestTransaction(requestTransaction);
        return;
    }


    /**
     *  Sets the queue.
     *
     *  @param queue a queue
     *  @throws org.osid.NullArgumentException <code>queue</code> is
     *          <code>null</code>
     */

    @Override
    public void setQueue(org.osid.provisioning.Queue queue) {
        super.setQueue(queue);
        return;
    }


    /**
     *  Sets the request date.
     *
     *  @param date a request date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    @Override
    public void setRequestDate(org.osid.calendaring.DateTime date) {
        super.setRequestDate(date);
        return;
    }


    /**
     *  Sets the requester.
     *
     *  @param requester a requester
     *  @throws org.osid.NullArgumentException <code>requester</code>
     *          is <code>null</code>
     */

    @Override
    public void setRequester(org.osid.resource.Resource requester) {
        super.setRequester(requester);
        return;
    }


    /**
     *  Sets the requesting agent.
     *
     *  @param agent a requesting agent
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    @Override
    public void setRequestingAgent(org.osid.authentication.Agent agent) {
        super.setRequestingAgent(agent);
        return;
    }


    /**
     *  Sets the pool.
     *
     *  @param pool a pool
     *  @throws org.osid.NullArgumentException <code>pool</code> is
     *          <code>null</code>
     */

    @Override
    public void setPool(org.osid.provisioning.Pool pool) {
        super.setPool(pool);
        return;
    }


    /**
     *  Adds a requested provisionable.
     *
     *  @param provisionable a requested provisionable
     *  @throws org.osid.NullArgumentException
     *          <code>provisionable</code> is <code>null</code>
     */

    @Override
    public void addRequestedProvisionable(org.osid.provisioning.Provisionable provisionable) {
        super.addRequestedProvisionable(provisionable);
        return;
    }


    /**
     *  Sets all the requested provisionables.
     *
     *  @param provisionables a collection of requested provisionables
     *  @throws org.osid.NullArgumentException
     *          <code>provisionables</code> is <code>null</code>
     */

    @Override
    public void setRequestedProvisionables(java.util.Collection<org.osid.provisioning.Provisionable> provisionables) {
        super.setRequestedProvisionables(provisionables);
        return;
    }


    /**
     *  Sets the exchange provision.
     *
     *  @param provision an exchange provision
     *  @throws org.osid.NullArgumentException
     *          <code>provision</code> is <code>null</code>
     */

    @Override
    public void setExchangeProvision(org.osid.provisioning.Provision provision) {
        super.setExchangeProvision(provision);
        return;
    }


    /**
     *  Sets the origin provision.
     *
     *  @param provision an origin provision
     *  @throws org.osid.NullArgumentException <code>provision</code>
     *          is <code>null</code>
     */

    @Override
    public void setOriginProvision(org.osid.provisioning.Provision provision) {
        super.setOriginProvision(provision);
        return;
    }


    /**
     *  Sets the position.
     *
     *  @param position a position
     *  @throws org.osid.InvalidArgumentException
     *          <code>position</code> is negative
     */

    @Override
    public void setPosition(long position) {
        super.setPosition(position);
        return;
    }


    /**
     *  Sets the ewa.
     *
     *  @param ewa the estimated waiting time
     *  @throws org.osid.NullArgumentException <code>ewa</code> is
     *          negative
     */

    public void setEWA(org.osid.calendaring.Duration ewa) {
        super.setEWA(ewa);
        return;
    }
}

