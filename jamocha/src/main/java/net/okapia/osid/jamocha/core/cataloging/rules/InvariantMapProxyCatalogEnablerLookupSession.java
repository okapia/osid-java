//
// InvariantMapProxyCatalogEnablerLookupSession
//
//    Implements a CatalogEnabler lookup service backed by a fixed
//    collection of catalogEnablers. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.cataloging.rules;


/**
 *  Implements a CatalogEnabler lookup service backed by a fixed
 *  collection of catalog enablers. The catalog enablers are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyCatalogEnablerLookupSession
    extends net.okapia.osid.jamocha.core.cataloging.rules.spi.AbstractMapCatalogEnablerLookupSession
    implements org.osid.cataloging.rules.CatalogEnablerLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyCatalogEnablerLookupSession} with no
     *  catalog enablers.
     *
     *  @param catalog the catalog
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code catalog} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyCatalogEnablerLookupSession(org.osid.cataloging.Catalog catalog,
                                                  org.osid.proxy.Proxy proxy) {
        setCatalog(catalog);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyCatalogEnablerLookupSession} with a single
     *  catalog enabler.
     *
     *  @param catalog the catalog
     *  @param catalogEnabler a single catalog enabler
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code catalog},
     *          {@code catalogEnabler} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyCatalogEnablerLookupSession(org.osid.cataloging.Catalog catalog,
                                                  org.osid.cataloging.rules.CatalogEnabler catalogEnabler, org.osid.proxy.Proxy proxy) {

        this(catalog, proxy);
        putCatalogEnabler(catalogEnabler);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyCatalogEnablerLookupSession} using
     *  an array of catalog enablers.
     *
     *  @param catalog the catalog
     *  @param catalogEnablers an array of catalog enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code catalog},
     *          {@code catalogEnablers} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyCatalogEnablerLookupSession(org.osid.cataloging.Catalog catalog,
                                                  org.osid.cataloging.rules.CatalogEnabler[] catalogEnablers, org.osid.proxy.Proxy proxy) {

        this(catalog, proxy);
        putCatalogEnablers(catalogEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyCatalogEnablerLookupSession} using a
     *  collection of catalog enablers.
     *
     *  @param catalog the catalog
     *  @param catalogEnablers a collection of catalog enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code catalog},
     *          {@code catalogEnablers} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyCatalogEnablerLookupSession(org.osid.cataloging.Catalog catalog,
                                                  java.util.Collection<? extends org.osid.cataloging.rules.CatalogEnabler> catalogEnablers,
                                                  org.osid.proxy.Proxy proxy) {

        this(catalog, proxy);
        putCatalogEnablers(catalogEnablers);
        return;
    }
}
