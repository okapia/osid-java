//
// AbstractFederatingMeterLookupSession.java
//
//     An abstract federating adapter for a MeterLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.metering.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  MeterLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingMeterLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.metering.MeterLookupSession>
    implements org.osid.metering.MeterLookupSession {

    private boolean parallel = false;
    private org.osid.metering.Utility utility = new net.okapia.osid.jamocha.nil.metering.utility.UnknownUtility();


    /**
     *  Constructs a new <code>AbstractFederatingMeterLookupSession</code>.
     */

    protected AbstractFederatingMeterLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.metering.MeterLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Utility/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Utility Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getUtilityId() {
        return (this.utility.getId());
    }


    /**
     *  Gets the <code>Utility</code> associated with this 
     *  session.
     *
     *  @return the <code>Utility</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.metering.Utility getUtility()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.utility);
    }


    /**
     *  Sets the <code>Utility</code>.
     *
     *  @param  utility the utility for this session
     *  @throws org.osid.NullArgumentException <code>utility</code>
     *          is <code>null</code>
     */

    protected void setUtility(org.osid.metering.Utility utility) {
        nullarg(utility, "utility");
        this.utility = utility;
        return;
    }


    /**
     *  Tests if this user can perform <code>Meter</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupMeters() {
        for (org.osid.metering.MeterLookupSession session : getSessions()) {
            if (session.canLookupMeters()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Meter</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeMeterView() {
        for (org.osid.metering.MeterLookupSession session : getSessions()) {
            session.useComparativeMeterView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Meter</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryMeterView() {
        for (org.osid.metering.MeterLookupSession session : getSessions()) {
            session.usePlenaryMeterView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include meters in utilities which are children
     *  of this utility in the utility hierarchy.
     */

    @OSID @Override
    public void useFederatedUtilityView() {
        for (org.osid.metering.MeterLookupSession session : getSessions()) {
            session.useFederatedUtilityView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this utility only.
     */

    @OSID @Override
    public void useIsolatedUtilityView() {
        for (org.osid.metering.MeterLookupSession session : getSessions()) {
            session.useIsolatedUtilityView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Meter</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Meter</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Meter</code> and
     *  retained for compatibility.
     *
     *  @param  meterId <code>Id</code> of the
     *          <code>Meter</code>
     *  @return the meter
     *  @throws org.osid.NotFoundException <code>meterId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>meterId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.metering.Meter getMeter(org.osid.id.Id meterId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.metering.MeterLookupSession session : getSessions()) {
            try {
                return (session.getMeter(meterId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(meterId + " not found");
    }


    /**
     *  Gets a <code>MeterList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  meters specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Meters</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  meterIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Meter</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>meterIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.metering.MeterList getMetersByIds(org.osid.id.IdList meterIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.metering.meter.MutableMeterList ret = new net.okapia.osid.jamocha.metering.meter.MutableMeterList();

        try (org.osid.id.IdList ids = meterIds) {
            while (ids.hasNext()) {
                ret.addMeter(getMeter(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>MeterList</code> corresponding to the given
     *  meter genus <code>Type</code> which does not include
     *  meters of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  meters or an error results. Otherwise, the returned list
     *  may contain only those meters that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  meterGenusType a meter genus type 
     *  @return the returned <code>Meter</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>meterGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.metering.MeterList getMetersByGenusType(org.osid.type.Type meterGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.metering.meter.FederatingMeterList ret = getMeterList();

        for (org.osid.metering.MeterLookupSession session : getSessions()) {
            ret.addMeterList(session.getMetersByGenusType(meterGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>MeterList</code> corresponding to the given
     *  meter genus <code>Type</code> and include any additional
     *  meters with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  meters or an error results. Otherwise, the returned list
     *  may contain only those meters that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  meterGenusType a meter genus type 
     *  @return the returned <code>Meter</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>meterGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.metering.MeterList getMetersByParentGenusType(org.osid.type.Type meterGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.metering.meter.FederatingMeterList ret = getMeterList();

        for (org.osid.metering.MeterLookupSession session : getSessions()) {
            ret.addMeterList(session.getMetersByParentGenusType(meterGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>MeterList</code> containing the given
     *  meter record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  meters or an error results. Otherwise, the returned list
     *  may contain only those meters that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  meterRecordType a meter record type 
     *  @return the returned <code>Meter</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>meterRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.metering.MeterList getMetersByRecordType(org.osid.type.Type meterRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.metering.meter.FederatingMeterList ret = getMeterList();

        for (org.osid.metering.MeterLookupSession session : getSessions()) {
            ret.addMeterList(session.getMetersByRecordType(meterRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Meters</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  meters or an error results. Otherwise, the returned list
     *  may contain only those meters that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Meters</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.metering.MeterList getMeters()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.metering.meter.FederatingMeterList ret = getMeterList();

        for (org.osid.metering.MeterLookupSession session : getSessions()) {
            ret.addMeterList(session.getMeters());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.metering.meter.FederatingMeterList getMeterList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.metering.meter.ParallelMeterList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.metering.meter.CompositeMeterList());
        }
    }
}
