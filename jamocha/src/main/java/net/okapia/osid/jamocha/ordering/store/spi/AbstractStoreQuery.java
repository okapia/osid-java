//
// AbstractStoreQuery.java
//
//     A template for making a Store Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ordering.store.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for stores.
 */

public abstract class AbstractStoreQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQuery
    implements org.osid.ordering.StoreQuery {

    private final java.util.Collection<org.osid.ordering.records.StoreQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the order <code> Id </code> for this query to match orders 
     *  assigned to stores. 
     *
     *  @param  orderId a order <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> orderId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchOrderId(org.osid.id.Id orderId, boolean match) {
        return;
    }


    /**
     *  Clears the order <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearOrderIdTerms() {
        return;
    }


    /**
     *  Tests if a order query is available. 
     *
     *  @return <code> true </code> if a order query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrderQuery() {
        return (false);
    }


    /**
     *  Gets the query for an order. 
     *
     *  @return the order query 
     *  @throws org.osid.UnimplementedException <code> supportsOrderQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.OrderQuery getOrderQuery() {
        throw new org.osid.UnimplementedException("supportsOrderQuery() is false");
    }


    /**
     *  Matches stores with any order. 
     *
     *  @param  match <code> true </code> to match stores with any order, 
     *          <code> false </code> to match stores with no orders 
     */

    @OSID @Override
    public void matchAnyOrder(boolean match) {
        return;
    }


    /**
     *  Clears the order terms. 
     */

    @OSID @Override
    public void clearOrderTerms() {
        return;
    }


    /**
     *  Sets the product <code> Id </code> for this query. 
     *
     *  @param  productId a product <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> productId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchProductId(org.osid.id.Id productId, boolean match) {
        return;
    }


    /**
     *  Clears the product <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearProductIdTerms() {
        return;
    }


    /**
     *  Tests if a product query is available. 
     *
     *  @return <code> true </code> if a product query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProductQuery() {
        return (false);
    }


    /**
     *  Gets the query for a product. 
     *
     *  @return the product query 
     *  @throws org.osid.UnimplementedException <code> supportsProductQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ProductQuery getProductQuery() {
        throw new org.osid.UnimplementedException("supportsProductQuery() is false");
    }


    /**
     *  Matches stores with any product. 
     *
     *  @param  match <code> true </code> to match stores with any product, 
     *          <code> false </code> to match stores with no products 
     */

    @OSID @Override
    public void matchAnyProduct(boolean match) {
        return;
    }


    /**
     *  Clears the product terms. 
     */

    @OSID @Override
    public void clearProductTerms() {
        return;
    }


    /**
     *  Sets the price schedule <code> Id </code> for this query. 
     *
     *  @param  priceScheduleId a price schedule <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> priceScheduleId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchPriceScheduleId(org.osid.id.Id priceScheduleId, 
                                     boolean match) {
        return;
    }


    /**
     *  Clears the price schedule <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearPriceScheduleIdTerms() {
        return;
    }


    /**
     *  Tests if a price schedule query is available. 
     *
     *  @return <code> true </code> if a price schedule query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPriceScheduleQuery() {
        return (false);
    }


    /**
     *  Gets the query for a price schedule. 
     *
     *  @return the price schedule query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceScheduleQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleQuery getPriceScheduleQuery() {
        throw new org.osid.UnimplementedException("supportsPriceScheduleQuery() is false");
    }


    /**
     *  Matches products with any price schedule. 
     *
     *  @param  match <code> true </code> to match products with any price 
     *          schedule, <code> false </code> to match products with no price 
     *          schedule 
     */

    @OSID @Override
    public void matchAnyPriceSchedule(boolean match) {
        return;
    }


    /**
     *  Clears the price schedule terms. 
     */

    @OSID @Override
    public void clearPriceScheduleTerms() {
        return;
    }


    /**
     *  Sets the store <code> Id </code> for this query to match stores that 
     *  have the specified store as an ancestor. 
     *
     *  @param  storeId a store <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> storeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorStoreId(org.osid.id.Id storeId, boolean match) {
        return;
    }


    /**
     *  Clears the ancestor store <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorStoreIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> StoreQuery </code> is available. 
     *
     *  @return <code> true </code> if a store query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorStoreQuery() {
        return (false);
    }


    /**
     *  Gets the query for a store. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the store query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorStoreQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.StoreQuery getAncestorStoreQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorStoreQuery() is false");
    }


    /**
     *  Matches stores with any ancestor. 
     *
     *  @param  match <code> true </code> to match stores with any ancestor, 
     *          <code> false </code> to match root stores 
     */

    @OSID @Override
    public void matchAnyAncestorStore(boolean match) {
        return;
    }


    /**
     *  Clears the ancestor store terms. 
     */

    @OSID @Override
    public void clearAncestorStoreTerms() {
        return;
    }


    /**
     *  Sets the store <code> Id </code> for this query to match stores that 
     *  have the specified store as a descendant. 
     *
     *  @param  storeId a store <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> storeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantStoreId(org.osid.id.Id storeId, boolean match) {
        return;
    }


    /**
     *  Clears the descendant store <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantStoreIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> StoreQuery </code> is available. 
     *
     *  @return <code> true </code> if a store query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantStoreQuery() {
        return (false);
    }


    /**
     *  Gets the query for a store. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the store query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantStoreQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.StoreQuery getDescendantStoreQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantStoreQuery() is false");
    }


    /**
     *  Matches stores with any descendant. 
     *
     *  @param  match <code> true </code> to match stores with any descendant, 
     *          <code> false </code> to match leaf stores 
     */

    @OSID @Override
    public void matchAnyDescendantStore(boolean match) {
        return;
    }


    /**
     *  Clears the descendant store terms. 
     */

    @OSID @Override
    public void clearDescendantStoreTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given store query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a store implementing the requested record.
     *
     *  @param storeRecordType a store record type
     *  @return the store query record
     *  @throws org.osid.NullArgumentException
     *          <code>storeRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(storeRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.ordering.records.StoreQueryRecord getStoreQueryRecord(org.osid.type.Type storeRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ordering.records.StoreQueryRecord record : this.records) {
            if (record.implementsRecordType(storeRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(storeRecordType + " is not supported");
    }


    /**
     *  Adds a record to this store query. 
     *
     *  @param storeQueryRecord store query record
     *  @param storeRecordType store record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addStoreQueryRecord(org.osid.ordering.records.StoreQueryRecord storeQueryRecord, 
                                          org.osid.type.Type storeRecordType) {

        addRecordType(storeRecordType);
        nullarg(storeQueryRecord, "store query record");
        this.records.add(storeQueryRecord);        
        return;
    }
}
