//
// AbstractBuildingValidator.java
//
//     Validates a Building.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.room.building.spi;


/**
 *  Validates a Building.
 */

public abstract class AbstractBuildingValidator
    extends net.okapia.osid.jamocha.builder.validator.spi.AbstractTemporalOsidObjectValidator {


    /**
     *  Constructs a new <code>AbstractBuildingValidator</code>.
     */

    protected AbstractBuildingValidator() {
        return;
    }


    /**
     *  Constructs a new <code>AbstractBuildingValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    protected AbstractBuildingValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates a Building.
     *
     *  @param building a building to validate
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>building</code>
     *          is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in
     *          assembly
     */
    
    public void validate(org.osid.room.Building building) {
        super.validate(building);

        testNestedObject(building, "getAddress");
        test(building.getOfficialName(), "getOfficialName()");
        test(building.getNumber(), "getNumber()");

        testConditionalMethod(building, "getEnclosingBuildingId", building.isSubdivision(), "isSibdivision()");
        testConditionalMethod(building, "getEnclosingBuilding", building.isSubdivision(), "isSibdivision()");
        if (building.isSubdivision()) {
            testNestedObject(building, "getEnclosingBuilding");
        }

        testNestedObjects(building, "getSubdivisionIds", "getSubdivisions");
        testConditionalMethod(building, "getGrossArea", building.hasArea(), "hasArea()");
        if (building.hasArea()) {
            testCardinal(building.getGrossArea(), "gross area");
        }

        return;
    }
}
