//
// AbstractNodeCourseCatalogHierarchySession.java
//
//     Defines a CourseCatalog hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a course catalog hierarchy session for delivering a hierarchy
 *  of course catalogs using the CourseCatalogNode interface.
 */

public abstract class AbstractNodeCourseCatalogHierarchySession
    extends net.okapia.osid.jamocha.course.spi.AbstractCourseCatalogHierarchySession
    implements org.osid.course.CourseCatalogHierarchySession {

    private java.util.Collection<org.osid.course.CourseCatalogNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root course catalog <code> Ids </code> in this hierarchy.
     *
     *  @return the root course catalog <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootCourseCatalogIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.course.coursecatalognode.CourseCatalogNodeToIdList(this.roots));
    }


    /**
     *  Gets the root course catalogs in the course catalog hierarchy. A node
     *  with no parents is an orphan. While all course catalog <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root course catalogs 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogList getRootCourseCatalogs()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.course.coursecatalognode.CourseCatalogNodeToCourseCatalogList(new net.okapia.osid.jamocha.course.coursecatalognode.ArrayCourseCatalogNodeList(this.roots)));
    }


    /**
     *  Adds a root course catalog node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootCourseCatalog(org.osid.course.CourseCatalogNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root course catalog nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootCourseCatalogs(java.util.Collection<org.osid.course.CourseCatalogNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root course catalog node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootCourseCatalog(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.course.CourseCatalogNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> CourseCatalog </code> has any parents. 
     *
     *  @param  courseCatalogId a course catalog <code> Id </code> 
     *  @return <code> true </code> if the course catalog has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> courseCatalogId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> courseCatalogId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentCourseCatalogs(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getCourseCatalogNode(courseCatalogId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of a
     *  course catalog.
     *
     *  @param  id an <code> Id </code> 
     *  @param  courseCatalogId the <code> Id </code> of a course catalog 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> courseCatalogId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> courseCatalogId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> courseCatalogId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfCourseCatalog(org.osid.id.Id id, org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.course.CourseCatalogNodeList parents = getCourseCatalogNode(courseCatalogId).getParentCourseCatalogNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextCourseCatalogNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given course catalog. 
     *
     *  @param  courseCatalogId a course catalog <code> Id </code> 
     *  @return the parent <code> Ids </code> of the course catalog 
     *  @throws org.osid.NotFoundException <code> courseCatalogId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> courseCatalogId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentCourseCatalogIds(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.course.coursecatalog.CourseCatalogToIdList(getParentCourseCatalogs(courseCatalogId)));
    }


    /**
     *  Gets the parents of the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> to query 
     *  @return the parents of the course catalog 
     *  @throws org.osid.NotFoundException <code> courseCatalogId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> courseCatalogId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogList getParentCourseCatalogs(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.course.coursecatalognode.CourseCatalogNodeToCourseCatalogList(getCourseCatalogNode(courseCatalogId).getParentCourseCatalogNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of a
     *  course catalog.
     *
     *  @param  id an <code> Id </code> 
     *  @param  courseCatalogId the Id of a course catalog 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> courseCatalogId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> courseCatalogId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> courseCatalogId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfCourseCatalog(org.osid.id.Id id, org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfCourseCatalog(id, courseCatalogId)) {
            return (true);
        }

        try (org.osid.course.CourseCatalogList parents = getParentCourseCatalogs(courseCatalogId)) {
            while (parents.hasNext()) {
                if (isAncestorOfCourseCatalog(id, parents.getNextCourseCatalog().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if a course catalog has any children. 
     *
     *  @param  courseCatalogId a course catalog <code> Id </code> 
     *  @return <code> true </code> if the <code> courseCatalogId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> courseCatalogId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> courseCatalogId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildCourseCatalogs(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getCourseCatalogNode(courseCatalogId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of a
     *  course catalog.
     *
     *  @param  id an <code> Id </code> 
     *  @param courseCatalogId the <code> Id </code> of a 
     *         course catalog
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> courseCatalogId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> courseCatalogId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> courseCatalogId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfCourseCatalog(org.osid.id.Id id, org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfCourseCatalog(courseCatalogId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  course catalog.
     *
     *  @param  courseCatalogId the <code> Id </code> to query 
     *  @return the children of the course catalog 
     *  @throws org.osid.NotFoundException <code> courseCatalogId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> courseCatalogId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildCourseCatalogIds(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.course.coursecatalog.CourseCatalogToIdList(getChildCourseCatalogs(courseCatalogId)));
    }


    /**
     *  Gets the children of the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> to query 
     *  @return the children of the course catalog 
     *  @throws org.osid.NotFoundException <code> courseCatalogId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> courseCatalogId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogList getChildCourseCatalogs(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.course.coursecatalognode.CourseCatalogNodeToCourseCatalogList(getCourseCatalogNode(courseCatalogId).getChildCourseCatalogNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of a
     *  course catalog.
     *
     *  @param  id an <code> Id </code> 
     *  @param courseCatalogId the <code> Id </code> of a 
     *         course catalog
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> courseCatalogId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> courseCatalogId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> courseCatalogId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfCourseCatalog(org.osid.id.Id id, org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfCourseCatalog(courseCatalogId, id)) {
            return (true);
        }

        try (org.osid.course.CourseCatalogList children = getChildCourseCatalogs(courseCatalogId)) {
            while (children.hasNext()) {
                if (isDescendantOfCourseCatalog(id, children.getNextCourseCatalog().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  course catalog.
     *
     *  @param  courseCatalogId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified course catalog node 
     *  @throws org.osid.NotFoundException <code> courseCatalogId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> courseCatalogId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getCourseCatalogNodeIds(org.osid.id.Id courseCatalogId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.course.coursecatalognode.CourseCatalogNodeToNode(getCourseCatalogNode(courseCatalogId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given course catalog.
     *
     *  @param  courseCatalogId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified course catalog node 
     *  @throws org.osid.NotFoundException <code> courseCatalogId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> courseCatalogId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogNode getCourseCatalogNodes(org.osid.id.Id courseCatalogId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getCourseCatalogNode(courseCatalogId));
    }


    /**
     *  Closes this <code>CourseCatalogHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets a course catalog node.
     *
     *  @param courseCatalogId the id of the course catalog node
     *  @throws org.osid.NotFoundException <code>courseCatalogId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>courseCatalogId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.course.CourseCatalogNode getCourseCatalogNode(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(courseCatalogId, "course catalog Id");
        for (org.osid.course.CourseCatalogNode courseCatalog : this.roots) {
            if (courseCatalog.getId().equals(courseCatalogId)) {
                return (courseCatalog);
            }

            org.osid.course.CourseCatalogNode r = findCourseCatalog(courseCatalog, courseCatalogId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(courseCatalogId + " is not found");
    }


    protected org.osid.course.CourseCatalogNode findCourseCatalog(org.osid.course.CourseCatalogNode node, 
                                                            org.osid.id.Id courseCatalogId)
	throws org.osid.OperationFailedException { 

        try (org.osid.course.CourseCatalogNodeList children = node.getChildCourseCatalogNodes()) {
            while (children.hasNext()) {
                org.osid.course.CourseCatalogNode courseCatalog = children.getNextCourseCatalogNode();
                if (courseCatalog.getId().equals(courseCatalogId)) {
                    return (courseCatalog);
                }
                
                courseCatalog = findCourseCatalog(courseCatalog, courseCatalogId);
                if (courseCatalog != null) {
                    return (courseCatalog);
                }
            }
        }

        return (null);
    }
}
