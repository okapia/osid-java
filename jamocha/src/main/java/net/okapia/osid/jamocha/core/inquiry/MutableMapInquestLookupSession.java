//
// MutableMapInquestLookupSession
//
//    Implements an Inquest lookup service backed by a collection of
//    inquests that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.inquiry;


/**
 *  Implements an Inquest lookup service backed by a collection of
 *  inquests. The inquests are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of inquests can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapInquestLookupSession
    extends net.okapia.osid.jamocha.core.inquiry.spi.AbstractMapInquestLookupSession
    implements org.osid.inquiry.InquestLookupSession {


    /**
     *  Constructs a new {@code MutableMapInquestLookupSession}
     *  with no inquests.
     */

    public MutableMapInquestLookupSession() {
        return;
    }


    /**
     *  Constructs a new {@code MutableMapInquestLookupSession} with a
     *  single inquest.
     *  
     *  @param inquest an inquest
     *  @throws org.osid.NullArgumentException {@code inquest}
     *          is {@code null}
     */

    public MutableMapInquestLookupSession(org.osid.inquiry.Inquest inquest) {
        putInquest(inquest);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapInquestLookupSession}
     *  using an array of inquests.
     *
     *  @param inquests an array of inquests
     *  @throws org.osid.NullArgumentException {@code inquests}
     *          is {@code null}
     */

    public MutableMapInquestLookupSession(org.osid.inquiry.Inquest[] inquests) {
        putInquests(inquests);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapInquestLookupSession}
     *  using a collection of inquests.
     *
     *  @param inquests a collection of inquests
     *  @throws org.osid.NullArgumentException {@code inquests}
     *          is {@code null}
     */

    public MutableMapInquestLookupSession(java.util.Collection<? extends org.osid.inquiry.Inquest> inquests) {
        putInquests(inquests);
        return;
    }

    
    /**
     *  Makes an {@code Inquest} available in this session.
     *
     *  @param inquest an inquest
     *  @throws org.osid.NullArgumentException {@code inquest{@code  is
     *          {@code null}
     */

    @Override
    public void putInquest(org.osid.inquiry.Inquest inquest) {
        super.putInquest(inquest);
        return;
    }


    /**
     *  Makes an array of inquests available in this session.
     *
     *  @param inquests an array of inquests
     *  @throws org.osid.NullArgumentException {@code inquests{@code 
     *          is {@code null}
     */

    @Override
    public void putInquests(org.osid.inquiry.Inquest[] inquests) {
        super.putInquests(inquests);
        return;
    }


    /**
     *  Makes collection of inquests available in this session.
     *
     *  @param inquests a collection of inquests
     *  @throws org.osid.NullArgumentException {@code inquests{@code  is
     *          {@code null}
     */

    @Override
    public void putInquests(java.util.Collection<? extends org.osid.inquiry.Inquest> inquests) {
        super.putInquests(inquests);
        return;
    }


    /**
     *  Removes an Inquest from this session.
     *
     *  @param inquestId the {@code Id} of the inquest
     *  @throws org.osid.NullArgumentException {@code inquestId{@code 
     *          is {@code null}
     */

    @Override
    public void removeInquest(org.osid.id.Id inquestId) {
        super.removeInquest(inquestId);
        return;
    }    
}
