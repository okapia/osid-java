//
// InlineDemographicEnablerNotifier.java
//
//     A callback interface for performing demographic enabler
//     notifications.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.resource.demographic.spi;


/**
 *  A callback interface for performing demographic enabler
 *  notifications.
 */

public interface InlineDemographicEnablerNotifier {



    /**
     *  Notifies the creation of a new demographic enabler.
     *
     *  @param demographicEnablerId the {@code Id} of the new 
     *         demographic enabler
     *  @throws org.osid.NullArgumentException {@code [objectId]}
     *          is {@code null}
     */

    public void newDemographicEnabler(org.osid.id.Id demographicEnablerId);


    /**
     *  Notifies the change of an updated demographic enabler.
     *
     *  @param demographicEnablerId the {@code Id} of the changed
     *         demographic enabler
     *  @throws org.osid.NullArgumentException {@code [objectId]}
     *          is {@code null}
     */
      
    public void changedDemographicEnabler(org.osid.id.Id demographicEnablerId);


    /**
     *  Notifies the deletion of an demographic enabler.
     *
     *  @param demographicEnablerId the {@code Id} of the deleted
     *         demographic enabler
     *  @throws org.osid.NullArgumentException {@code [objectId]} is
     *          {@code null}
     */

    public void deletedDemographicEnabler(org.osid.id.Id demographicEnablerId);

}
