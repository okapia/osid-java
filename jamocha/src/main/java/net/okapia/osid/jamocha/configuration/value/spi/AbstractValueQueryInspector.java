//
// AbstractValueQueryInspector.java
//
//     A template for making a ValueQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.configuration.value.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for values.
 */

public abstract class AbstractValueQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOperableOsidObjectQueryInspector
    implements org.osid.configuration.ValueQueryInspector {

    private final java.util.Collection<org.osid.configuration.records.ValueQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the priority query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getPriorityTerms() {
        return (new org.osid.search.terms.CardinalRangeTerm[0]);
    }


    /**
     *  Gets the boolean value query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getBooleanValueTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the bytes value query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BytesTerm[] getBytesValueTerms() {
        return (new org.osid.search.terms.BytesTerm[0]);
    }


    /**
     *  Gets the cardinal value query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getCardinalValueTerms() {
        return (new org.osid.search.terms.CardinalRangeTerm[0]);
    }


    /**
     *  Gets the coordinate value query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CoordinateTerm[] getCoordinateValueTerms() {
        return (new org.osid.search.terms.CoordinateTerm[0]);
    }


    /**
     *  Gets the currency value query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyTerm[] getCurrencyValueTerms() {
        return (new org.osid.search.terms.CurrencyTerm[0]);
    }


    /**
     *  Gets the date time value query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getDateTimeValueTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the decimal value query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getDecimalValueTerms() {
        return (new org.osid.search.terms.DecimalRangeTerm[0]);
    }


    /**
     *  Gets the distance value query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DistanceTerm[] getDistanceValueTerms() {
        return (new org.osid.search.terms.DistanceTerm[0]);
    }


    /**
     *  Gets the duration value query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationTerm[] getDurationValueTerms() {
        return (new org.osid.search.terms.DurationTerm[0]);
    }


    /**
     *  Gets the heading value query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.HeadingTerm[] getHeadingValueTerms() {
        return (new org.osid.search.terms.HeadingTerm[0]);
    }


    /**
     *  Gets the Id value query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getIdValueTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the integer value query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IntegerRangeTerm[] getIntegerValueTerms() {
        return (new org.osid.search.terms.IntegerRangeTerm[0]);
    }


    /**
     *  Gets the spatial unit value query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.SpatialUnitTerm[] getSpatialUnitValueTerms() {
        return (new org.osid.search.terms.SpatialUnitTerm[0]);
    }


    /**
     *  Gets the speed value query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.SpeedTerm[] getSpeedValueTerms() {
        return (new org.osid.search.terms.SpeedTerm[0]);
    }


    /**
     *  Gets the string value query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getStringValueTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the time value query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.TimeTerm[] getTimeValueTerms() {
        return (new org.osid.search.terms.TimeTerm[0]);
    }


    /**
     *  Gets the type value query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.TypeTerm[] getTypeValueTerms() {
        return (new org.osid.search.terms.TypeTerm[0]);
    }


    /**
     *  Gets the version value query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.VersionRangeTerm[] getVersionValueTerms() {
        return (new org.osid.search.terms.VersionRangeTerm[0]);
    }


    /**
     *  Gets the object value type query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.TypeTerm[] getObjectValueTypeTerms() {
        return (new org.osid.search.terms.TypeTerm[0]);
    }


    /**
     *  Gets the object value query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.ObjectTerm[] getObjectValueTerms() {
        return (new org.osid.search.terms.ObjectTerm[0]);
    }


    /**
     *  Gets the parameter <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getParameterIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the parameter query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.configuration.ParameterQueryInspector[] getParameterTerms() {
        return (new org.osid.configuration.ParameterQueryInspector[0]);
    }


    /**
     *  Gets the configuration <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getConfigurationIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the configuration query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationQueryInspector[] getConfigurationTerms() {
        return (new org.osid.configuration.ConfigurationQueryInspector[0]);
    }


    /**
     *  Gets the record corresponding to the given value query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a value implementing the requested record.
     *
     *  @param valueRecordType a value record type
     *  @return the value query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>valueRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(valueRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.configuration.records.ValueQueryInspectorRecord getValueQueryInspectorRecord(org.osid.type.Type valueRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.configuration.records.ValueQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(valueRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(valueRecordType + " is not supported");
    }


    /**
     *  Adds a record to this value query. 
     *
     *  @param valueQueryInspectorRecord value query inspector
     *         record
     *  @param valueRecordType value record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addValueQueryInspectorRecord(org.osid.configuration.records.ValueQueryInspectorRecord valueQueryInspectorRecord, 
                                                org.osid.type.Type valueRecordType) {

        addRecordType(valueRecordType);
        nullarg(valueRecordType, "value record type");
        this.records.add(valueQueryInspectorRecord);        
        return;
    }
}
