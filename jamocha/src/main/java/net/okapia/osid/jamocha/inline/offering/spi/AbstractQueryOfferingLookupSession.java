//
// AbstractQueryOfferingLookupSession.java
//
//    An inline adapter that maps an OfferingLookupSession to
//    an OfferingQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.offering.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps an OfferingLookupSession to
 *  an OfferingQuerySession.
 */

public abstract class AbstractQueryOfferingLookupSession
    extends net.okapia.osid.jamocha.offering.spi.AbstractOfferingLookupSession
    implements org.osid.offering.OfferingLookupSession {

    private boolean effectiveonly = false;
    private final org.osid.offering.OfferingQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryOfferingLookupSession.
     *
     *  @param querySession the underlying offering query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryOfferingLookupSession(org.osid.offering.OfferingQuerySession querySession) {
        nullarg(querySession, "offering query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Catalogue</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Catalogue Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCatalogueId() {
        return (this.session.getCatalogueId());
    }


    /**
     *  Gets the <code>Catalogue</code> associated with this 
     *  session.
     *
     *  @return the <code>Catalogue</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.Catalogue getCatalogue()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getCatalogue());
    }


    /**
     *  Tests if this user can perform <code>Offering</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupOfferings() {
        return (this.session.canSearchOfferings());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include offerings in catalogues which are children
     *  of this catalogue in the catalogue hierarchy.
     */

    @OSID @Override
    public void useFederatedCatalogueView() {
        this.session.useFederatedCatalogueView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this catalogue only.
     */

    @OSID @Override
    public void useIsolatedCatalogueView() {
        this.session.useIsolatedCatalogueView();
        return;
    }
    

    /**
     *  Only offerings whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveOfferingView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All offerings of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveOfferingView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Offering</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Offering</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Offering</code> and
     *  retained for compatibility.
     *
     *  In effective mode, offerings are returned that are currently
     *  effective.  In any effective mode, effective offerings and
     *  those currently expired are returned.
     *
     *  @param  offeringId <code>Id</code> of the
     *          <code>Offering</code>
     *  @return the offering
     *  @throws org.osid.NotFoundException <code>offeringId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>offeringId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.Offering getOffering(org.osid.id.Id offeringId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.OfferingQuery query = getQuery();
        query.matchId(offeringId, true);
        org.osid.offering.OfferingList offerings = this.session.getOfferingsByQuery(query);
        if (offerings.hasNext()) {
            return (offerings.getNextOffering());
        } 
        
        throw new org.osid.NotFoundException(offeringId + " not found");
    }


    /**
     *  Gets an <code>OfferingList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  offerings specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Offerings</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, offerings are returned that are currently effective.
     *  In any effective mode, effective offerings and those currently expired
     *  are returned.
     *
     *  @param  offeringIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Offering</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>offeringIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.OfferingList getOfferingsByIds(org.osid.id.IdList offeringIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.OfferingQuery query = getQuery();

        try (org.osid.id.IdList ids = offeringIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getOfferingsByQuery(query));
    }


    /**
     *  Gets an <code>OfferingList</code> corresponding to the given
     *  offering genus <code>Type</code> which does not include
     *  offerings of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those offerings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, offerings are returned that are currently effective.
     *  In any effective mode, effective offerings and those currently expired
     *  are returned.
     *
     *  @param  offeringGenusType an offering genus type 
     *  @return the returned <code>Offering</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>offeringGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.OfferingList getOfferingsByGenusType(org.osid.type.Type offeringGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.OfferingQuery query = getQuery();
        query.matchGenusType(offeringGenusType, true);
        return (this.session.getOfferingsByQuery(query));
    }


    /**
     *  Gets an <code>OfferingList</code> corresponding to the given
     *  offering genus <code>Type</code> and include any additional
     *  offerings with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those offerings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, offerings are returned that are currently
     *  effective.  In any effective mode, effective offerings and
     *  those currently expired are returned.
     *
     *  @param  offeringGenusType an offering genus type 
     *  @return the returned <code>Offering</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>offeringGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.OfferingList getOfferingsByParentGenusType(org.osid.type.Type offeringGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.OfferingQuery query = getQuery();
        query.matchParentGenusType(offeringGenusType, true);
        return (this.session.getOfferingsByQuery(query));
    }


    /**
     *  Gets an <code>OfferingList</code> containing the given
     *  offering record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those offerings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, offerings are returned that are currently
     *  effective.  In any effective mode, effective offerings and
     *  those currently expired are returned.
     *
     *  @param  offeringRecordType an offering record type 
     *  @return the returned <code>Offering</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>offeringRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.OfferingList getOfferingsByRecordType(org.osid.type.Type offeringRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.OfferingQuery query = getQuery();
        query.matchRecordType(offeringRecordType, true);
        return (this.session.getOfferingsByQuery(query));
    }


    /**
     *  Gets an <code>OfferingList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those offerings that are accessible
     *  through this session.
     *  
     *  In effective mode, offerings are returned that are currently
     *  effective.  In any effective mode, effective offerings and
     *  those currently expired are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Offering</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.offering.OfferingList getOfferingsOnDate(org.osid.calendaring.DateTime from, 
                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.OfferingQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getOfferingsByQuery(query));
    }
        

    /**
     *  Gets a list of offerings corresponding to a canonical unit
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those offerings that are accessible
     *  through this session.
     *
     *  In effective mode, offerings are returned that are
     *  currently effective.  In any effective mode, effective
     *  offerings and those currently expired are returned.
     *
     *  @param  canonicalUnitId the <code>Id</code> of the canonical unit
     *  @return the returned <code>OfferingList</code>
     *  @throws org.osid.NullArgumentException <code>canonicalUnitId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.offering.OfferingList getOfferingsForCanonicalUnit(org.osid.id.Id canonicalUnitId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.offering.OfferingQuery query = getQuery();
        query.matchCanonicalUnitId(canonicalUnitId, true);
        return (this.session.getOfferingsByQuery(query));
    }


    /**
     *  Gets a list of offerings corresponding to a canonical unit
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those offerings that are accessible
     *  through this session.
     *
     *  In effective mode, offerings are returned that are
     *  currently effective.  In any effective mode, effective
     *  offerings and those currently expired are returned.
     *
     *  @param  canonicalUnitId the <code>Id</code> of the canonical unit
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>OfferingList</code>
     *  @throws org.osid.NullArgumentException <code>canonicalUnitId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.offering.OfferingList getOfferingsForCanonicalUnitOnDate(org.osid.id.Id canonicalUnitId,
                                                                             org.osid.calendaring.DateTime from,
                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.OfferingQuery query = getQuery();
        query.matchCanonicalUnitId(canonicalUnitId, true);
        query.matchDate(from, to, true);
        return (this.session.getOfferingsByQuery(query));
    }


    /**
     *  Gets a list of offerings corresponding to a time period
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those offerings that are accessible
     *  through this session.
     *
     *  In effective mode, offerings are returned that are
     *  currently effective.  In any effective mode, effective
     *  offerings and those currently expired are returned.
     *
     *  @param  timePeriodId the <code>Id</code> of the time period
     *  @return the returned <code>OfferingList</code>
     *  @throws org.osid.NullArgumentException <code>timePeriodId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.offering.OfferingList getOfferingsForTimePeriod(org.osid.id.Id timePeriodId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.offering.OfferingQuery query = getQuery();
        query.matchTimePeriodId(timePeriodId, true);
        return (this.session.getOfferingsByQuery(query));
    }


    /**
     *  Gets a list of offerings corresponding to a time period
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those offerings that are accessible
     *  through this session.
     *
     *  In effective mode, offerings are returned that are
     *  currently effective.  In any effective mode, effective
     *  offerings and those currently expired are returned.
     *
     *  @param  timePeriodId the <code>Id</code> of the time period
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>OfferingList</code>
     *  @throws org.osid.NullArgumentException <code>timePeriodId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.offering.OfferingList getOfferingsForTimePeriodOnDate(org.osid.id.Id timePeriodId,
                                                                          org.osid.calendaring.DateTime from,
                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.OfferingQuery query = getQuery();
        query.matchTimePeriodId(timePeriodId, true);
        query.matchDate(from, to, true);
        return (this.session.getOfferingsByQuery(query));
    }


    /**
     *  Gets a list of offerings corresponding to canonical unit and
     *  time period <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those offerings that are accessible
     *  through this session.
     *
     *  In effective mode, offerings are returned that are
     *  currently effective.  In any effective mode, effective
     *  offerings and those currently expired are returned.
     *
     *  @param  canonicalUnitId the <code>Id</code> of the canonical unit
     *  @param  timePeriodId the <code>Id</code> of the time period
     *  @return the returned <code>OfferingList</code>
     *  @throws org.osid.NullArgumentException <code>canonicalUnitId</code>,
     *          <code>timePeriodId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.offering.OfferingList getOfferingsForCanonicalUnitAndTimePeriod(org.osid.id.Id canonicalUnitId,
                                                                                    org.osid.id.Id timePeriodId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.offering.OfferingQuery query = getQuery();
        query.matchCanonicalUnitId(canonicalUnitId, true);
        query.matchTimePeriodId(timePeriodId, true);
        return (this.session.getOfferingsByQuery(query));
    }


    /**
     *  Gets a list of offerings corresponding to canonical unit and
     *  time period <code>Ids</code> and effective during the entire
     *  given date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those offerings that are accessible
     *  through this session.
     *
     *  In effective mode, offerings are returned that are currently
     *  effective.  In any effective mode, effective offerings and
     *  those currently expired are returned.
     *
     *  @param  canonicalUnitId the <code>Id</code> of the canonical unit
     *  @param  timePeriodId the <code>Id</code> of the time period
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>OfferingList</code>
     *  @throws org.osid.NullArgumentException <code>canonicalUnitId</code>,
     *          <code>timePeriodId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.offering.OfferingList getOfferingsForCanonicalUnitAndTimePeriodOnDate(org.osid.id.Id canonicalUnitId,
                                                                                          org.osid.id.Id timePeriodId,
                                                                                          org.osid.calendaring.DateTime from,
                                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.offering.OfferingQuery query = getQuery();
        query.matchCanonicalUnitId(canonicalUnitId, true);
        query.matchTimePeriodId(timePeriodId, true);
        query.matchDate(from, to, true);
        return (this.session.getOfferingsByQuery(query));
    }

    
    /**
     *  Gets all <code>Offerings</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those offerings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, offerings are returned that are currently
     *  effective.  In any effective mode, effective offerings and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Offerings</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.OfferingList getOfferings()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {


        org.osid.offering.OfferingQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getOfferingsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.offering.OfferingQuery getQuery() {
        org.osid.offering.OfferingQuery query = this.session.getOfferingQuery();
        
        if (isEffectiveOnly()) {
            query.matchEffective(true);
        }

        return (query);
    }
}
