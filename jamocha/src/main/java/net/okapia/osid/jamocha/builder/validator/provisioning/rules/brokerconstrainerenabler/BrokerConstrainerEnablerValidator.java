//
// BrokerConstrainerEnablerValidator.java
//
//     Validates a BrokerConstrainerEnabler.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.provisioning.rules.brokerconstrainerenabler;


/**
 *  Validates a BrokerConstrainerEnabler.
 */

public final class BrokerConstrainerEnablerValidator
    extends net.okapia.osid.jamocha.builder.validator.provisioning.rules.brokerconstrainerenabler.spi.AbstractBrokerConstrainerEnablerValidator {


    /**
     *  Constructs a new <code>BrokerConstrainerEnablerValidator</code>.
     */

    public BrokerConstrainerEnablerValidator() {
        return;
    }


    /**
     *  Constructs a new <code>BrokerConstrainerEnablerValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    public BrokerConstrainerEnablerValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates a BrokerConstrainerEnabler with a default validation.
     *
     *  @param brokerConstrainerEnabler a broker constrainer enabler to validate
     *  @return the broker constrainer enabler
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>brokerConstrainerEnabler</code>
     *          is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred
     */

    public static org.osid.provisioning.rules.BrokerConstrainerEnabler validateBrokerConstrainerEnabler(org.osid.provisioning.rules.BrokerConstrainerEnabler brokerConstrainerEnabler) {
        BrokerConstrainerEnablerValidator validator = new BrokerConstrainerEnablerValidator();
        validator.validate(brokerConstrainerEnabler);
        return (brokerConstrainerEnabler);
    }


    /**
     *  Validates a BrokerConstrainerEnabler for the given validations.
     *
     *  @param validation an EnumSet of validations
     *  @param brokerConstrainerEnabler a broker constrainer enabler to validate
     *  @return the broker constrainer enabler
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          or <code>brokerConstrainerEnabler</code> is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred
     */

    public static org.osid.provisioning.rules.BrokerConstrainerEnabler validateBrokerConstrainerEnabler(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation,
                                                       org.osid.provisioning.rules.BrokerConstrainerEnabler brokerConstrainerEnabler) {

        BrokerConstrainerEnablerValidator validator = new BrokerConstrainerEnablerValidator(validation);
        validator.validate(brokerConstrainerEnabler);
        return (brokerConstrainerEnabler);
    }
}
