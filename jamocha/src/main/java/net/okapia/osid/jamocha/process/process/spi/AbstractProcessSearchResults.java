//
// AbstractProcessSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.process.process.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractProcessSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.process.ProcessSearchResults {

    private org.osid.process.ProcessList processes;
    private final org.osid.process.ProcessQueryInspector inspector;
    private final java.util.Collection<org.osid.process.records.ProcessSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractProcessSearchResults.
     *
     *  @param processes the result set
     *  @param processQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>processes</code>
     *          or <code>processQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractProcessSearchResults(org.osid.process.ProcessList processes,
                                            org.osid.process.ProcessQueryInspector processQueryInspector) {
        nullarg(processes, "processes");
        nullarg(processQueryInspector, "process query inspectpr");

        this.processes = processes;
        this.inspector = processQueryInspector;

        return;
    }


    /**
     *  Gets the process list resulting from a search.
     *
     *  @return a process list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.process.ProcessList getProcesses() {
        if (this.processes == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.process.ProcessList processes = this.processes;
        this.processes = null;
	return (processes);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.process.ProcessQueryInspector getProcessQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  process search record <code> Type. </code> This method must
     *  be used to retrieve a process implementing the requested
     *  record.
     *
     *  @param processSearchRecordType a process search 
     *         record type 
     *  @return the process search
     *  @throws org.osid.NullArgumentException
     *          <code>processSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(processSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.process.records.ProcessSearchResultsRecord getProcessSearchResultsRecord(org.osid.type.Type processSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.process.records.ProcessSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(processSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(processSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record process search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addProcessRecord(org.osid.process.records.ProcessSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "process record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
