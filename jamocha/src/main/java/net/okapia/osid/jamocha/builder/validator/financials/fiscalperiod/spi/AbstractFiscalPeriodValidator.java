//
// AbstractFiscalPeriodValidator.java
//
//     Validates a FiscalPeriod.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.financials.fiscalperiod.spi;


/**
 *  Validates a FiscalPeriod.
 */

public abstract class AbstractFiscalPeriodValidator
    extends net.okapia.osid.jamocha.builder.validator.spi.AbstractOsidObjectValidator {


    /**
     *  Constructs a new <code>AbstractFiscalPeriodValidator</code>.
     */

    protected AbstractFiscalPeriodValidator() {
        return;
    }


    /**
     *  Constructs a new <code>AbstractFiscalPeriodValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    protected AbstractFiscalPeriodValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates a FiscalPeriod.
     *
     *  @param fiscalPeriod a fiscal period to validate
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>fiscalPeriod</code>
     *          is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in
     *          assembly
     */
    
    public void validate(org.osid.financials.FiscalPeriod fiscalPeriod) {
        super.validate(fiscalPeriod);

        test(fiscalPeriod.getDisplayLabel(), "getDisplayLabel()");
        test(fiscalPeriod.getFiscalYear(), "getFiscalYear()");
        test(fiscalPeriod.getStartDate(), "getStartDate()");
        test(fiscalPeriod.getEndDate(), "getEndDate()");

        try {
            if (!fiscalPeriod.hasMilestones() && fiscalPeriod.requiresBudgetSubmission()) {
                throw new org.osid.BadLogicException("hasMilestones() is false");
            }
        } catch (org.osid.IllegalStateException ise) {}

        try {
            if (!fiscalPeriod.hasMilestones() && fiscalPeriod.hasPostingDeadline()) {
                throw new org.osid.BadLogicException("hasMilestones() is false");
            }
        } catch (org.osid.IllegalStateException ise) {}

        try {
            if (!fiscalPeriod.hasMilestones() && fiscalPeriod.hasClosing()) {
                throw new org.osid.BadLogicException("hasMilestones() is false");
            }
        } catch (org.osid.IllegalStateException ise) {}        

        if (fiscalPeriod.hasMilestones()) {
            testConditionalMethod(fiscalPeriod, "getBudgetDeadline", fiscalPeriod.requiresBudgetSubmission(), "requiresBudgetSubmission()");
            testConditionalMethod(fiscalPeriod, "getPostingDeadline", fiscalPeriod.hasPostingDeadline(), "hasPostingDeadline()");
            testConditionalMethod(fiscalPeriod, "getClosing", fiscalPeriod.hasClosing(), "hasClosing()");
        }

        return;
    }
}
