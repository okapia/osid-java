//
// AbstractAssemblyActionGroupQuery.java
//
//     An ActionGroupQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.control.actiongroup.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An ActionGroupQuery that stores terms.
 */

public abstract class AbstractAssemblyActionGroupQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.control.ActionGroupQuery,
               org.osid.control.ActionGroupQueryInspector,
               org.osid.control.ActionGroupSearchOrder {

    private final java.util.Collection<org.osid.control.records.ActionGroupQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.control.records.ActionGroupQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.control.records.ActionGroupSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyActionGroupQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyActionGroupQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the action <code> Id </code> for this query. 
     *
     *  @param  actionId the action <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> actionId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchActionId(org.osid.id.Id actionId, boolean match) {
        getAssembler().addIdTerm(getActionIdColumn(), actionId, match);
        return;
    }


    /**
     *  Clears the action <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearActionIdTerms() {
        getAssembler().clearTerms(getActionIdColumn());
        return;
    }


    /**
     *  Gets the action <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getActionIdTerms() {
        return (getAssembler().getIdTerms(getActionIdColumn()));
    }


    /**
     *  Gets the ActionId column name.
     *
     * @return the column name
     */

    protected String getActionIdColumn() {
        return ("action_id");
    }


    /**
     *  Tests if an <code> ActionQuery </code> is available. 
     *
     *  @return <code> true </code> if an action query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActionQuery() {
        return (false);
    }


    /**
     *  Gets the query for an action. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the action query 
     *  @throws org.osid.UnimplementedException <code> supportsActionQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ActionQuery getActionQuery() {
        throw new org.osid.UnimplementedException("supportsActionQuery() is false");
    }


    /**
     *  Matches action groups with any action. 
     *
     *  @param  match <code> true </code> to match action groups with any 
     *          action, <code> false </code> to match action groups with no 
     *          actions 
     */

    @OSID @Override
    public void matchAnyAction(boolean match) {
        getAssembler().addIdWildcardTerm(getActionColumn(), match);
        return;
    }


    /**
     *  Clears the action query terms. 
     */

    @OSID @Override
    public void clearActionTerms() {
        getAssembler().clearTerms(getActionColumn());
        return;
    }


    /**
     *  Gets the action query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.ActionQueryInspector[] getActionTerms() {
        return (new org.osid.control.ActionQueryInspector[0]);
    }


    /**
     *  Gets the Action column name.
     *
     * @return the column name
     */

    protected String getActionColumn() {
        return ("action");
    }


    /**
     *  Sets the action group <code> Id </code> for this query to match 
     *  controllers assigned to action groups. 
     *
     *  @param  actionGroupId the action group <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> actionGroupId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSystemId(org.osid.id.Id actionGroupId, boolean match) {
        getAssembler().addIdTerm(getSystemIdColumn(), actionGroupId, match);
        return;
    }


    /**
     *  Clears the system <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearSystemIdTerms() {
        getAssembler().clearTerms(getSystemIdColumn());
        return;
    }


    /**
     *  Gets the system <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSystemIdTerms() {
        return (getAssembler().getIdTerms(getSystemIdColumn()));
    }


    /**
     *  Gets the SystemId column name.
     *
     * @return the column name
     */

    protected String getSystemIdColumn() {
        return ("system_id");
    }


    /**
     *  Tests if a <code> SystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a system query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSystemQuery() {
        return (false);
    }


    /**
     *  Gets the query for a system. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the system query 
     *  @throws org.osid.UnimplementedException <code> supportsSystemQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SystemQuery getSystemQuery() {
        throw new org.osid.UnimplementedException("supportsSystemQuery() is false");
    }


    /**
     *  Clears the system query terms. 
     */

    @OSID @Override
    public void clearSystemTerms() {
        getAssembler().clearTerms(getSystemColumn());
        return;
    }


    /**
     *  Gets the system query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.SystemQueryInspector[] getSystemTerms() {
        return (new org.osid.control.SystemQueryInspector[0]);
    }


    /**
     *  Gets the System column name.
     *
     * @return the column name
     */

    protected String getSystemColumn() {
        return ("system");
    }


    /**
     *  Tests if this actionGroup supports the given record
     *  <code>Type</code>.
     *
     *  @param  actionGroupRecordType an action group record type 
     *  @return <code>true</code> if the actionGroupRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>actionGroupRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type actionGroupRecordType) {
        for (org.osid.control.records.ActionGroupQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(actionGroupRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  actionGroupRecordType the action group record type 
     *  @return the action group query record 
     *  @throws org.osid.NullArgumentException
     *          <code>actionGroupRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(actionGroupRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.ActionGroupQueryRecord getActionGroupQueryRecord(org.osid.type.Type actionGroupRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.records.ActionGroupQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(actionGroupRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(actionGroupRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  actionGroupRecordType the action group record type 
     *  @return the action group query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>actionGroupRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(actionGroupRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.ActionGroupQueryInspectorRecord getActionGroupQueryInspectorRecord(org.osid.type.Type actionGroupRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.records.ActionGroupQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(actionGroupRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(actionGroupRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param actionGroupRecordType the action group record type
     *  @return the action group search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>actionGroupRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(actionGroupRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.ActionGroupSearchOrderRecord getActionGroupSearchOrderRecord(org.osid.type.Type actionGroupRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.records.ActionGroupSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(actionGroupRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(actionGroupRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this action group. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param actionGroupQueryRecord the action group query record
     *  @param actionGroupQueryInspectorRecord the action group query inspector
     *         record
     *  @param actionGroupSearchOrderRecord the action group search order record
     *  @param actionGroupRecordType action group record type
     *  @throws org.osid.NullArgumentException
     *          <code>actionGroupQueryRecord</code>,
     *          <code>actionGroupQueryInspectorRecord</code>,
     *          <code>actionGroupSearchOrderRecord</code> or
     *          <code>actionGroupRecordTypeactionGroup</code> is
     *          <code>null</code>
     */
            
    protected void addActionGroupRecords(org.osid.control.records.ActionGroupQueryRecord actionGroupQueryRecord, 
                                      org.osid.control.records.ActionGroupQueryInspectorRecord actionGroupQueryInspectorRecord, 
                                      org.osid.control.records.ActionGroupSearchOrderRecord actionGroupSearchOrderRecord, 
                                      org.osid.type.Type actionGroupRecordType) {

        addRecordType(actionGroupRecordType);

        nullarg(actionGroupQueryRecord, "action group query record");
        nullarg(actionGroupQueryInspectorRecord, "action group query inspector record");
        nullarg(actionGroupSearchOrderRecord, "action group search odrer record");

        this.queryRecords.add(actionGroupQueryRecord);
        this.queryInspectorRecords.add(actionGroupQueryInspectorRecord);
        this.searchOrderRecords.add(actionGroupSearchOrderRecord);
        
        return;
    }
}
