//
// AbstractIndexedMapAgencyLookupSession.java
//
//    A simple framework for providing an Agency lookup service
//    backed by a fixed collection of agencies with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.authentication.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an Agency lookup service backed by a
 *  fixed collection of agencies. The agencies are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some agencies may be compatible
 *  with more types than are indicated through these agency
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Agencies</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapAgencyLookupSession
    extends AbstractMapAgencyLookupSession
    implements org.osid.authentication.AgencyLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.authentication.Agency> agenciesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.authentication.Agency>());
    private final MultiMap<org.osid.type.Type, org.osid.authentication.Agency> agenciesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.authentication.Agency>());


    /**
     *  Makes an <code>Agency</code> available in this session.
     *
     *  @param  agency an agency
     *  @throws org.osid.NullArgumentException <code>agency<code> is
     *          <code>null</code>
     */

    @Override
    protected void putAgency(org.osid.authentication.Agency agency) {
        super.putAgency(agency);

        this.agenciesByGenus.put(agency.getGenusType(), agency);
        
        try (org.osid.type.TypeList types = agency.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.agenciesByRecord.put(types.getNextType(), agency);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an agency from this session.
     *
     *  @param agencyId the <code>Id</code> of the agency
     *  @throws org.osid.NullArgumentException <code>agencyId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeAgency(org.osid.id.Id agencyId) {
        org.osid.authentication.Agency agency;
        try {
            agency = getAgency(agencyId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.agenciesByGenus.remove(agency.getGenusType());

        try (org.osid.type.TypeList types = agency.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.agenciesByRecord.remove(types.getNextType(), agency);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeAgency(agencyId);
        return;
    }


    /**
     *  Gets an <code>AgencyList</code> corresponding to the given
     *  agency genus <code>Type</code> which does not include
     *  agencies of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known agencies or an error results. Otherwise,
     *  the returned list may contain only those agencies that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  agencyGenusType an agency genus type 
     *  @return the returned <code>Agency</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>agencyGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.AgencyList getAgenciesByGenusType(org.osid.type.Type agencyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.authentication.agency.ArrayAgencyList(this.agenciesByGenus.get(agencyGenusType)));
    }


    /**
     *  Gets an <code>AgencyList</code> containing the given
     *  agency record <code>Type</code>. In plenary mode, the
     *  returned list contains all known agencies or an error
     *  results. Otherwise, the returned list may contain only those
     *  agencies that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  agencyRecordType an agency record type 
     *  @return the returned <code>agency</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>agencyRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.AgencyList getAgenciesByRecordType(org.osid.type.Type agencyRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.authentication.agency.ArrayAgencyList(this.agenciesByRecord.get(agencyRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.agenciesByGenus.clear();
        this.agenciesByRecord.clear();

        super.close();

        return;
    }
}
