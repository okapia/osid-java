//
// AbstractCommissionEnablerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.rules.commissionenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractCommissionEnablerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.resourcing.rules.CommissionEnablerSearchResults {

    private org.osid.resourcing.rules.CommissionEnablerList commissionEnablers;
    private final org.osid.resourcing.rules.CommissionEnablerQueryInspector inspector;
    private final java.util.Collection<org.osid.resourcing.rules.records.CommissionEnablerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractCommissionEnablerSearchResults.
     *
     *  @param commissionEnablers the result set
     *  @param commissionEnablerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>commissionEnablers</code>
     *          or <code>commissionEnablerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractCommissionEnablerSearchResults(org.osid.resourcing.rules.CommissionEnablerList commissionEnablers,
                                            org.osid.resourcing.rules.CommissionEnablerQueryInspector commissionEnablerQueryInspector) {
        nullarg(commissionEnablers, "commission enablers");
        nullarg(commissionEnablerQueryInspector, "commission enabler query inspectpr");

        this.commissionEnablers = commissionEnablers;
        this.inspector = commissionEnablerQueryInspector;

        return;
    }


    /**
     *  Gets the commission enabler list resulting from a search.
     *
     *  @return a commission enabler list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerList getCommissionEnablers() {
        if (this.commissionEnablers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.resourcing.rules.CommissionEnablerList commissionEnablers = this.commissionEnablers;
        this.commissionEnablers = null;
	return (commissionEnablers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.resourcing.rules.CommissionEnablerQueryInspector getCommissionEnablerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  commission enabler search record <code> Type. </code> This method must
     *  be used to retrieve a commissionEnabler implementing the requested
     *  record.
     *
     *  @param commissionEnablerSearchRecordType a commissionEnabler search 
     *         record type 
     *  @return the commission enabler search
     *  @throws org.osid.NullArgumentException
     *          <code>commissionEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(commissionEnablerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.rules.records.CommissionEnablerSearchResultsRecord getCommissionEnablerSearchResultsRecord(org.osid.type.Type commissionEnablerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.resourcing.rules.records.CommissionEnablerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(commissionEnablerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(commissionEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record commission enabler search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addCommissionEnablerRecord(org.osid.resourcing.rules.records.CommissionEnablerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "commission enabler record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
