//
// AbstractHierarchySearch.java
//
//     A template for making a Hierarchy Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.hierarchy.hierarchy.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing hierarchy searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractHierarchySearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.hierarchy.HierarchySearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.hierarchy.records.HierarchySearchRecord> records = new java.util.ArrayList<>();
    private org.osid.hierarchy.HierarchySearchOrder hierarchySearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of hierarchies. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  hierarchyIds list of hierarchies
     *  @throws org.osid.NullArgumentException
     *          <code>hierarchyIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongHierarchies(org.osid.id.IdList hierarchyIds) {
        while (hierarchyIds.hasNext()) {
            try {
                this.ids.add(hierarchyIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongHierarchies</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of hierarchy Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getHierarchyIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  hierarchySearchOrder hierarchy search order 
     *  @throws org.osid.NullArgumentException
     *          <code>hierarchySearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hierarchySearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderHierarchyResults(org.osid.hierarchy.HierarchySearchOrder hierarchySearchOrder) {
	this.hierarchySearchOrder = hierarchySearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.hierarchy.HierarchySearchOrder getHierarchySearchOrder() {
	return (this.hierarchySearchOrder);
    }


    /**
     *  Gets the record corresponding to the given hierarchy search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a hierarchy implementing the requested record.
     *
     *  @param hierarchySearchRecordType a hierarchy search record
     *         type
     *  @return the hierarchy search record
     *  @throws org.osid.NullArgumentException
     *          <code>hierarchySearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(hierarchySearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.hierarchy.records.HierarchySearchRecord getHierarchySearchRecord(org.osid.type.Type hierarchySearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.hierarchy.records.HierarchySearchRecord record : this.records) {
            if (record.implementsRecordType(hierarchySearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(hierarchySearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this hierarchy search. 
     *
     *  @param hierarchySearchRecord hierarchy search record
     *  @param hierarchySearchRecordType hierarchy search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addHierarchySearchRecord(org.osid.hierarchy.records.HierarchySearchRecord hierarchySearchRecord, 
                                           org.osid.type.Type hierarchySearchRecordType) {

        addRecordType(hierarchySearchRecordType);
        this.records.add(hierarchySearchRecord);        
        return;
    }
}
