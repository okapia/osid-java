//
// AbstractMapTermLookupSession
//
//    A simple framework for providing a Term lookup service
//    backed by a fixed collection of terms.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Term lookup service backed by a
 *  fixed collection of terms. The terms are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Terms</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapTermLookupSession
    extends net.okapia.osid.jamocha.course.spi.AbstractTermLookupSession
    implements org.osid.course.TermLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.course.Term> terms = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.course.Term>());


    /**
     *  Makes a <code>Term</code> available in this session.
     *
     *  @param  term a term
     *  @throws org.osid.NullArgumentException <code>term<code>
     *          is <code>null</code>
     */

    protected void putTerm(org.osid.course.Term term) {
        this.terms.put(term.getId(), term);
        return;
    }


    /**
     *  Makes an array of terms available in this session.
     *
     *  @param  terms an array of terms
     *  @throws org.osid.NullArgumentException <code>terms<code>
     *          is <code>null</code>
     */

    protected void putTerms(org.osid.course.Term[] terms) {
        putTerms(java.util.Arrays.asList(terms));
        return;
    }


    /**
     *  Makes a collection of terms available in this session.
     *
     *  @param  terms a collection of terms
     *  @throws org.osid.NullArgumentException <code>terms<code>
     *          is <code>null</code>
     */

    protected void putTerms(java.util.Collection<? extends org.osid.course.Term> terms) {
        for (org.osid.course.Term term : terms) {
            this.terms.put(term.getId(), term);
        }

        return;
    }


    /**
     *  Removes a Term from this session.
     *
     *  @param  termId the <code>Id</code> of the term
     *  @throws org.osid.NullArgumentException <code>termId<code> is
     *          <code>null</code>
     */

    protected void removeTerm(org.osid.id.Id termId) {
        this.terms.remove(termId);
        return;
    }


    /**
     *  Gets the <code>Term</code> specified by its <code>Id</code>.
     *
     *  @param  termId <code>Id</code> of the <code>Term</code>
     *  @return the term
     *  @throws org.osid.NotFoundException <code>termId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>termId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.Term getTerm(org.osid.id.Id termId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.course.Term term = this.terms.get(termId);
        if (term == null) {
            throw new org.osid.NotFoundException("term not found: " + termId);
        }

        return (term);
    }


    /**
     *  Gets all <code>Terms</code>. In plenary mode, the returned
     *  list contains all known terms or an error
     *  results. Otherwise, the returned list may contain only those
     *  terms that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Terms</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.TermList getTerms()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.term.ArrayTermList(this.terms.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.terms.clear();
        super.close();
        return;
    }
}
