//
// AbstractDictionaryProxyManager.java
//
//     An adapter for a DictionaryProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.dictionary.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a DictionaryProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterDictionaryProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.dictionary.DictionaryProxyManager>
    implements org.osid.dictionary.DictionaryProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterDictionaryProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterDictionaryProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterDictionaryProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterDictionaryProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any dictionary federation is exposed. Federation is exposed 
     *  when a specific dictionary may be identified, selected and used to 
     *  create a lookup or admin session. Federation is not exposed when a set 
     *  of dictionaries appears as a single dictionary. 
     *
     *  @return <code> true </code> if federation is visible <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if retrieving dictionary entries are supported. 
     *
     *  @return <code> true </code> if entry retrieval is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryRetrieval() {
        return (getAdapteeManager().supportsEntryRetrieval());
    }


    /**
     *  Tests if looking up dictionary entries are supported. 
     *
     *  @return <code> true </code> if entry lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryLookup() {
        return (getAdapteeManager().supportsEntryLookup());
    }


    /**
     *  Tests if querying dictionary entries are supported. 
     *
     *  @return <code> true </code> if entry query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryQuery() {
        return (getAdapteeManager().supportsEntryQuery());
    }


    /**
     *  Tests if searching dictionary entries are supported. 
     *
     *  @return <code> true </code> if entry search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntrySearch() {
        return (getAdapteeManager().supportsEntrySearch());
    }


    /**
     *  Tests if a dictionary entry administrative service is supported. 
     *
     *  @return <code> true </code> if dictionary entry administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryAdmin() {
        return (getAdapteeManager().supportsEntryAdmin());
    }


    /**
     *  Tests if a dictionary entry notification service is supported. 
     *
     *  @return <code> true </code> if dictionary entry notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryNotification() {
        return (getAdapteeManager().supportsEntryNotification());
    }


    /**
     *  Tests if retrieving mappings of entry and dictionarys is supported. 
     *
     *  @return <code> true </code> if entry dictionary mapping retrieval is 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryDictionary() {
        return (getAdapteeManager().supportsEntryDictionary());
    }


    /**
     *  Tests if managing mappings of entry and dictionarys is supported. 
     *
     *  @return <code> true </code> if entry dictionary assignment is 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryDictionaryAssignment() {
        return (getAdapteeManager().supportsEntryDictionaryAssignment());
    }


    /**
     *  Tests if entry smart dictionarys are available. 
     *
     *  @return <code> true </code> if entry smart dictionarys are supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntrySmartDictionary() {
        return (getAdapteeManager().supportsEntrySmartDictionary());
    }


    /**
     *  Tests if a dictionary lookup service is supported. 
     *
     *  @return <code> true </code> if dictionary lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDictionaryLookup() {
        return (getAdapteeManager().supportsDictionaryLookup());
    }


    /**
     *  Tests if a dictionary query service is supported. 
     *
     *  @return <code> true </code> if dictionary query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDictionaryQuery() {
        return (getAdapteeManager().supportsDictionaryQuery());
    }


    /**
     *  Tests if a dictionary search service is supported. 
     *
     *  @return <code> true </code> if dictionary search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDictionarySearch() {
        return (getAdapteeManager().supportsDictionarySearch());
    }


    /**
     *  Tests if a dictionary administrative service is supported. 
     *
     *  @return <code> true </code> if dictionary administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDictionaryAdmin() {
        return (getAdapteeManager().supportsDictionaryAdmin());
    }


    /**
     *  Tests if a dictionary notification service is supported. 
     *
     *  @return <code> true </code> if dictionary notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDictionaryNotification() {
        return (getAdapteeManager().supportsDictionaryNotification());
    }


    /**
     *  Tests if a dictionary hierarchy traversal service is supported. 
     *
     *  @return <code> true </code> if dictionary hierarchy traversal is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDictionaryHierachyTraversal() {
        return (getAdapteeManager().supportsDictionaryHierachyTraversal());
    }


    /**
     *  Tests if a dictionary hierarchy design service is supported. 
     *
     *  @return <code> true </code> if dictionary hierarchy design is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDictionaryHierachyDesign() {
        return (getAdapteeManager().supportsDictionaryHierachyDesign());
    }


    /**
     *  Tests if a dictionary batch service is supported. 
     *
     *  @return <code> true </code> if dictionary batch service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDictionaryBatch() {
        return (getAdapteeManager().supportsDictionaryBatch());
    }


    /**
     *  Gets the supported <code> Entry </code> key types. 
     *
     *  @return a list containing the supported <code> Entry </code> key types 
     */

    @OSID @Override
    public org.osid.type.TypeList getEntryKeyTypes() {
        return (getAdapteeManager().getEntryKeyTypes());
    }


    /**
     *  Tests if the given <code> Entry </code> key type is supported. 
     *
     *  @param  entryKeyType a <code> Type </code> indicating an <code> Entry 
     *          </code> key type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> entryKeyType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean supportsEntryKeyType(org.osid.type.Type entryKeyType) {
        return (getAdapteeManager().supportsEntryKeyType(entryKeyType));
    }


    /**
     *  Gets the supported <code> Entry </code> value types. 
     *
     *  @return a list containing the supported <code> Entry </code> value 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getEntryValueTypes() {
        return (getAdapteeManager().getEntryValueTypes());
    }


    /**
     *  Tests if the given <code> Entry </code> value type is supported. 
     *
     *  @param  entryValueType a <code> Type </code> indicating an <code> 
     *          Entry </code> value type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> entryRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsEntryValueType(org.osid.type.Type entryValueType) {
        return (getAdapteeManager().supportsEntryValueType(entryValueType));
    }


    /**
     *  Gets the list of value types supported for the given key type. 
     *
     *  @param  entryKeyType a <code> Type </code> indicating an <code> Entry 
     *          </code> key type 
     *  @return a list of value types 
     *  @throws org.osid.NullArgumentException <code> entryKeyType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public org.osid.type.TypeList getEntryValueTypesForKeyType(org.osid.type.Type entryKeyType) {
        return (getAdapteeManager().getEntryValueTypesForKeyType(entryKeyType));
    }


    /**
     *  Tests if the given <code> Entry </code> key and value types are 
     *  supported. 
     *
     *  @param  entryKeyType a <code> Type </code> indicating an <code> Entry 
     *          </code> key type 
     *  @param  entryValueType a <code> Type </code> indicating an <code> 
     *          Entry </code> value type 
     *  @return <code> true </code> if the given Types are supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> entryKeyType </code> or 
     *          <code> entryValueType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsEntryTypes(org.osid.type.Type entryKeyType, 
                                      org.osid.type.Type entryValueType) {
        return (getAdapteeManager().supportsEntryTypes(entryKeyType, entryValueType));
    }


    /**
     *  Gets the supported <code> Entry </code> record types. 
     *
     *  @return a list containing the supported <code> Entry </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getEntryRecordTypes() {
        return (getAdapteeManager().getEntryRecordTypes());
    }


    /**
     *  Tests if the given <code> Entry </code> record type is supported. 
     *
     *  @param  entryRecordType a <code> Type </code> indicating an <code> 
     *          Entry </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> entryRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsEntryRecordType(org.osid.type.Type entryRecordType) {
        return (getAdapteeManager().supportsEntryRecordType(entryRecordType));
    }


    /**
     *  Gets the supported <code> Entry </code> search record types. 
     *
     *  @return a list containing the supported <code> Entry </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getEntrySearchRecordTypes() {
        return (getAdapteeManager().getEntrySearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Entry </code> search record type is 
     *  supported. 
     *
     *  @param  entrySearchRecordType a <code> Type </code> indicating an 
     *          <code> Entry </code> search record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> entrySearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsEntrySearchRecordType(org.osid.type.Type entrySearchRecordType) {
        return (getAdapteeManager().supportsEntrySearchRecordType(entrySearchRecordType));
    }


    /**
     *  Gets the supported <code> Dictionary </code> record types. 
     *
     *  @return a list containing the supported <code> Dictionary </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDictionaryRecordTypes() {
        return (getAdapteeManager().getDictionaryRecordTypes());
    }


    /**
     *  Tests if the given <code> Dictionary </code> record type is supported. 
     *
     *  @param  dictionaryRecordType a <code> Type </code> indicating a <code> 
     *          Dictionary </code> record type 
     *  @return <code> true </code> if the given record Type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> dictionaryRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsDictionaryRecordType(org.osid.type.Type dictionaryRecordType) {
        return (getAdapteeManager().supportsDictionaryRecordType(dictionaryRecordType));
    }


    /**
     *  Gets the supported <code> Dictionary </code> search record types. 
     *
     *  @return a list containing the supported <code> Dictionary </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDictionarySearchRecordTypes() {
        return (getAdapteeManager().getDictionarySearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Dictionary </code> search record type is 
     *  supported. 
     *
     *  @param  dictionarySearchRecordType a <code> Type </code> indicating a 
     *          <code> Dictionary </code> search record type 
     *  @return <code> true </code> if the given record <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          dictionarySearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsDictionarySearchRecordType(org.osid.type.Type dictionarySearchRecordType) {
        return (getAdapteeManager().supportsDictionarySearchRecordType(dictionarySearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> used to retrieve dictionary 
     *  entries. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EntryRetrievalSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntryRetrieval() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.EntryRetrievalSession getEntryRetrievalSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEntryRetrievalSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> used to retrieve dictionary 
     *  entries for the specified <code> Dictionary </code> . 
     *
     *  @param  dictionaryId the <code> Id </code> of the <code> Dictionary 
     *          </code> to use 
     *  @param  proxy a proxy 
     *  @return an <code> EntryRetrievalSession </code> 
     *  @throws org.osid.NotFoundException no <code> Dictionary </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> dictionaryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntryRetrieval() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.EntryRetrievalSession getEntryRetrievalSessionForDictionary(org.osid.id.Id dictionaryId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEntryRetrievalSessionForDictionary(dictionaryId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> used to lookup dictionary entries. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EntryLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.EntryLookupSession getEntryLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEntryLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> used to lookup dictionary entries 
     *  for the specified <code> Dictionary </code> . 
     *
     *  @param  dictionaryId the <code> Id </code> of the <code> Dictionary 
     *          </code> to use 
     *  @param  proxy a proxy 
     *  @return an <code> EntryLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Dictionary </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> dictionaryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.EntryLookupSession getEntryLookupSessionForDictionary(org.osid.id.Id dictionaryId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEntryLookupSessionForDictionary(dictionaryId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> used to query dictionary entries. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EntryQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.EntryQuerySession getEntryQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEntryQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> used to query dictionary entries 
     *  for the specified <code> Dictionary </code> . 
     *
     *  @param  dictionaryId the <code> Id </code> of the <code> Dictionary 
     *          </code> to use 
     *  @param  proxy a proxy 
     *  @return an <code> EntryQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Dictionary </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> dictionaryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.EntryQuerySession getEntryQuerySessionForDictionary(org.osid.id.Id dictionaryId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEntryQuerySessionForDictionary(dictionaryId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> used to search among dictionary 
     *  entries. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EntrySearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntrySearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.EntrySearchSession getEntrySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEntrySearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> used to search among dictionary 
     *  entries for the specified <code> Dictionary </code> . 
     *
     *  @param  dictionaryId the <code> Id </code> of the <code> Dictionary 
     *          </code> to use 
     *  @param  proxy a proxy 
     *  @return an <code> EntrySearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Dictionary </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> dictionaryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntrySearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.EntrySearchSession getEntrySearchSessionForDictionary(org.osid.id.Id dictionaryId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEntrySearchSessionForDictionary(dictionaryId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> used to administer dictionary 
     *  entries. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EntryAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.EntryAdminSession getEntryAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEntryAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> used to administer dictionary 
     *  entries for the specified <code> Dictionary </code> . 
     *
     *  @param  dictionaryId the <code> Id </code> of the <code> Dictionary 
     *          </code> to use 
     *  @param  proxy a proxy 
     *  @return an <code> EntryAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Dictionary </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> dictionaryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryAdmin() 
     *          </code> os <code> supportsVisibleFederration() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.EntryAdminSession getEntryAdminSessionForDictionary(org.osid.id.Id dictionaryId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEntryAdminSessionForDictionary(dictionaryId, proxy));
    }


    /**
     *  Gets an <code> EntryNotificationSession </code> which is responsible 
     *  for subscribing to entry changes within a default <code> Dictionary. 
     *  </code> 
     *
     *  @param  entryReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> EntryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> entryReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntryNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.EntryNotificationSession getEntryNotificationSession(org.osid.dictionary.EntryReceiver entryReceiver, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEntryNotificationSession(entryReceiver, proxy));
    }


    /**
     *  Gets an <code> EntryNotificationSession </code> which is responsible 
     *  for subscribing to entry changes for a specified <code> Dictionary. 
     *  </code> 
     *
     *  @param  entryReceiver the notification callback 
     *  @param  dictionaryId the <code> Id </code> of the <code> Dictionary 
     *          </code> to use 
     *  @param  proxy a proxy 
     *  @return an <code> EntryNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Dictionary </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> entryReceiver, 
     *          dictionaryId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.EntryNotificationSession getEntryNotificationSessionForDictionary(org.osid.dictionary.EntryReceiver entryReceiver, 
                                                                                                 org.osid.id.Id dictionaryId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEntryNotificationSessionForDictionary(entryReceiver, dictionaryId, proxy));
    }


    /**
     *  Gets the session for retrieving entry to dictionary mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> EntryDictionarySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntryDictionary() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.EntryDictionarySession getEntryDictionarySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEntryDictionarySession(proxy));
    }


    /**
     *  Gets the session for assigning entry to dictionary mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> EntryDictionaryAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntryDictionaryAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.dictionary.EntryDictionaryAssignmentSession getEntryDictionaryAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEntryDictionaryAssignmentSession(proxy));
    }


    /**
     *  Gets the session for managing dynamic entry dictionarys. 
     *
     *  @param  dictionaryId the <code> Id </code> of the dictionary 
     *  @param  proxy a proxy 
     *  @return a <code> EntrySmartDictionarySession </code> 
     *  @throws org.osid.NotFoundException <code> dictionaryId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> dictionaryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntrySmartDictionary() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.EntrySmartDictionarySession getEntrySmartDictionarySession(org.osid.id.Id dictionaryId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEntrySmartDictionarySession(dictionaryId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> used to lookup dictionaries. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DictionaryLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDictionaryLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryLookupSession getDictionaryLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDictionaryLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> used to query dictionaries. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DictionaryQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDictionaryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryQuerySession getDictionaryQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDictionaryQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> used to search for dictionaries. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DictionarySearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDictionarySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.DictionarySearchSession getDictionarySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDictionarySearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> used to administer dictionaries. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DictionaryAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDictionaryAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryAdminSession getDictionaryAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDictionaryAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> used to subscribe to notifications 
     *  of new, updated or deleted dictionaries dictionaries. 
     *
     *  @param  dictionaryReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> DictionaryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> dictionaryReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDictionaryNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryNotificationSession getDictionaryNotificationSession(org.osid.dictionary.DictionaryReceiver dictionaryReceiver, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDictionaryNotificationSession(dictionaryReceiver, proxy));
    }


    /**
     *  Gets the hierarchy traversing the <code> Dictionary </code> hierarchy. 
     *  The parent includes all dictionary elements of its children. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DictionaryHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDictionaryHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryHierarchySession getDictionaryHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDictionaryHierarchySession(proxy));
    }


    /**
     *  Gets the hierarchy managing the <code> Dictionary </code> hierarchy. 
     *  The parent includes all dictionary elements of its children. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DictionaryHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDictionaryHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryHierarchyDesignSession getDictionaryHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDictionaryHierarchyDesignSession(proxy));
    }


    /**
     *  Gets a <code> DictionaryBatchProxyManager. </code> 
     *
     *  @return a <code> DictionaryBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDictionaryBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.batch.DictionaryBatchProxyManager getDictionaryBatchProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDictionaryBatchProxyManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
