//
// AbstractObjectiveBankQueryInspector.java
//
//     A template for making an ObjectiveBankQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.learning.objectivebank.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for objective banks.
 */

public abstract class AbstractObjectiveBankQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQueryInspector
    implements org.osid.learning.ObjectiveBankQueryInspector {

    private final java.util.Collection<org.osid.learning.records.ObjectiveBankQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the objective <code> Id </code> query terms. 
     *
     *  @return the objective <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getObjectiveIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the objective query terms. 
     *
     *  @return the objective terms 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQueryInspector[] getObjectiveTerms() {
        return (new org.osid.learning.ObjectiveQueryInspector[0]);
    }


    /**
     *  Gets the activity <code> Id </code> query terms. 
     *
     *  @return the activity <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getActivityIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the activity query terms. 
     *
     *  @return the activity terms 
     */

    @OSID @Override
    public org.osid.learning.ActivityQueryInspector[] getActivityTerms() {
        return (new org.osid.learning.ActivityQueryInspector[0]);
    }


    /**
     *  Gets the ancestor objective bank <code> Id </code> query terms. 
     *
     *  @return the ancestor objective bank <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorObjectiveBankIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor objective bank query terms. 
     *
     *  @return the ancestor objective bank terms 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankQueryInspector[] getAncestorObjectiveBankTerms() {
        return (new org.osid.learning.ObjectiveBankQueryInspector[0]);
    }


    /**
     *  Gets the descendant objective bank <code> Id </code> query terms. 
     *
     *  @return the descendant objective bank <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantObjectiveBankIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant objective bank query terms. 
     *
     *  @return the descendant objective bank terms 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankQueryInspector[] getDescendantObjectiveBankTerms() {
        return (new org.osid.learning.ObjectiveBankQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given objective bank query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an objective bank implementing the requested record.
     *
     *  @param objectiveBankRecordType an objective bank record type
     *  @return the objective bank query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveBankRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(objectiveBankRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.learning.records.ObjectiveBankQueryInspectorRecord getObjectiveBankQueryInspectorRecord(org.osid.type.Type objectiveBankRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.learning.records.ObjectiveBankQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(objectiveBankRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(objectiveBankRecordType + " is not supported");
    }


    /**
     *  Adds a record to this objective bank query. 
     *
     *  @param objectiveBankQueryInspectorRecord objective bank query inspector
     *         record
     *  @param objectiveBankRecordType objectiveBank record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addObjectiveBankQueryInspectorRecord(org.osid.learning.records.ObjectiveBankQueryInspectorRecord objectiveBankQueryInspectorRecord, 
                                                   org.osid.type.Type objectiveBankRecordType) {

        addRecordType(objectiveBankRecordType);
        nullarg(objectiveBankRecordType, "objective bank record type");
        this.records.add(objectiveBankQueryInspectorRecord);        
        return;
    }
}
