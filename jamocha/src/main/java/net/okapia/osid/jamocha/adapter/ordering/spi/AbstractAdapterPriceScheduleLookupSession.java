//
// AbstractAdapterPriceScheduleLookupSession.java
//
//    A PriceSchedule lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.ordering.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A PriceSchedule lookup session adapter.
 */

public abstract class AbstractAdapterPriceScheduleLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.ordering.PriceScheduleLookupSession {

    private final org.osid.ordering.PriceScheduleLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterPriceScheduleLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterPriceScheduleLookupSession(org.osid.ordering.PriceScheduleLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Store/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Store Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getStoreId() {
        return (this.session.getStoreId());
    }


    /**
     *  Gets the {@code Store} associated with this session.
     *
     *  @return the {@code Store} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.Store getStore()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getStore());
    }


    /**
     *  Tests if this user can perform {@code PriceSchedule} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupPriceSchedules() {
        return (this.session.canLookupPriceSchedules());
    }


    /**
     *  A complete view of the {@code PriceSchedule} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativePriceScheduleView() {
        this.session.useComparativePriceScheduleView();
        return;
    }


    /**
     *  A complete view of the {@code PriceSchedule} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryPriceScheduleView() {
        this.session.usePlenaryPriceScheduleView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include price schedules in stores which are children
     *  of this store in the store hierarchy.
     */

    @OSID @Override
    public void useFederatedStoreView() {
        this.session.useFederatedStoreView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this store only.
     */

    @OSID @Override
    public void useIsolatedStoreView() {
        this.session.useIsolatedStoreView();
        return;
    }
    
     
    /**
     *  Gets the {@code PriceSchedule} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code PriceSchedule} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code PriceSchedule} and
     *  retained for compatibility.
     *
     *  @param priceScheduleId {@code Id} of the {@code PriceSchedule}
     *  @return the price schedule
     *  @throws org.osid.NotFoundException {@code priceScheduleId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code priceScheduleId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.PriceSchedule getPriceSchedule(org.osid.id.Id priceScheduleId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPriceSchedule(priceScheduleId));
    }


    /**
     *  Gets the {@code PriceSchedule} by a {@code Price} {@code Id}.
     *
     *  @param  priceId {@code Id} of a {@code Price} 
     *  @return the price schedule 
     *  @throws org.osid.NotFoundException {@code priceId} not found 
     *  @throws org.osid.NullArgumentException {@code priceId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ordering.PriceSchedule getPriceScheduleByPrice(org.osid.id.Id priceId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPriceScheduleByPrice(priceId));
    }


    /**
     *  Gets a {@code PriceScheduleList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  priceSchedules specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code PriceSchedules} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  priceScheduleIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code PriceSchedule} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code priceScheduleIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleList getPriceSchedulesByIds(org.osid.id.IdList priceScheduleIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPriceSchedulesByIds(priceScheduleIds));
    }


    /**
     *  Gets a {@code PriceScheduleList} corresponding to the given
     *  price schedule genus {@code Type} which does not include
     *  price schedules of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  price schedules or an error results. Otherwise, the returned list
     *  may contain only those price schedules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  priceScheduleGenusType a priceSchedule genus type 
     *  @return the returned {@code PriceSchedule} list
     *  @throws org.osid.NullArgumentException
     *          {@code priceScheduleGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleList getPriceSchedulesByGenusType(org.osid.type.Type priceScheduleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPriceSchedulesByGenusType(priceScheduleGenusType));
    }


    /**
     *  Gets a {@code PriceScheduleList} corresponding to the given
     *  price schedule genus {@code Type} and include any additional
     *  price schedules with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  price schedules or an error results. Otherwise, the returned list
     *  may contain only those price schedules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  priceScheduleGenusType a priceSchedule genus type 
     *  @return the returned {@code PriceSchedule} list
     *  @throws org.osid.NullArgumentException
     *          {@code priceScheduleGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleList getPriceSchedulesByParentGenusType(org.osid.type.Type priceScheduleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPriceSchedulesByParentGenusType(priceScheduleGenusType));
    }


    /**
     *  Gets a {@code PriceScheduleList} containing the given
     *  price schedule record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  price schedules or an error results. Otherwise, the returned list
     *  may contain only those price schedules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  priceScheduleRecordType a priceSchedule record type 
     *  @return the returned {@code PriceSchedule} list
     *  @throws org.osid.NullArgumentException
     *          {@code priceScheduleRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleList getPriceSchedulesByRecordType(org.osid.type.Type priceScheduleRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPriceSchedulesByRecordType(priceScheduleRecordType));
    }


    /**
     *  Gets all {@code PriceSchedules}. 
     *
     *  In plenary mode, the returned list contains all known
     *  price schedules or an error results. Otherwise, the returned list
     *  may contain only those price schedules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code PriceSchedules} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleList getPriceSchedules()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPriceSchedules());
    }
}
