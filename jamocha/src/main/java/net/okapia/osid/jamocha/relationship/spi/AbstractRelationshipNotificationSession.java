//
// AbstractRelationshipNotificationSession.java
//
//     A template for making RelationshipNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.relationship.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code Relationship} objects. This session is
 *  intended for consumers needing to synchronize their state with
 *  this service without the use of polling. Notifications are
 *  cancelled when this session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code Relationship} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for relationship entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractRelationshipNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.relationship.RelationshipNotificationSession {

    private boolean federated = false;
    private org.osid.relationship.Family family = new net.okapia.osid.jamocha.nil.relationship.family.UnknownFamily();


    /**
     *  Gets the {@code Family/code> {@code Id} associated with this
     *  session.
     *
     *  @return the {@code Family Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getFamilyId() {
        return (this.family.getId());
    }

    
    /**
     *  Gets the {@code Family} associated with this session.
     *
     *  @return the {@code Family} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.Family getFamily()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.family);
    }


    /**
     *  Sets the {@code Family}.
     *
     *  @param family the family for this session
     *  @throws org.osid.NullArgumentException {@code family}
     *          is {@code null}
     */

    protected void setFamily(org.osid.relationship.Family family) {
        nullarg(family, "family");
        this.family = family;
        return;
    }


    /**
     *  Tests if this user can register for {@code Relationship}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForRelationshipNotifications() {
        return (true);
    }


    /**
     *  Reliable notifications are desired. In reliable mode, notifications 
     *  are to be acknowledged using <code> acknowledgeRelationshipNotification() 
     *  </code>. 
     */

    @OSID @Override
    public void reliableRelationshipNotifications() {
        return;
    }


    /**
     *  Unreliable notifications are desired. In unreliable mode, 
     *  notifications do not need to be acknowledged. 
     */

    @OSID @Override
    public void unreliableRelationshipNotifications() {
        return;
    }


    /**
     *  Acknowledge a relationship notification. 
     *
     *  @param  notificationId the <code> Id </code> of the notification 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void acknowledgeRelationshipNotification(org.osid.id.Id notificationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include relationships in families which are children
     *  of this family in the family hierarchy.
     */

    @OSID @Override
    public void useFederatedFamilyView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this family only.
     */

    @OSID @Override
    public void useIsolatedFamilyView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return {@codetrue</code> if federated view,
     *          {@codefalse</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications of new relationships. {@code
     *  RelationshipReceiver.newRelationship()} is invoked when a new
     *  {@code Relationship} is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewRelationships()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new relationships. {@code
     *  RelationshipReceiver.newRelationship()} is invoked when a new
     *  {@code Relationship} appears for the given peer.
     *
     *  @param  relationshipGenusType the genus type of the {@code 
     *          Relationship} to monitor 
     *  @throws org.osid.NullArgumentException {@code
     *          relationshipGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForNewRelationshipsByGenusType(org.osid.type.Type relationshipGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new relationships for the given
     *  source {@code Id}. {@code
     *  RelationshipReceiver.newRelationship()} is invoked when a new
     *  {@code Relationship} is created.
     *
     *  @param sourceId the {@code Id} of the source to monitor
     *  @throws org.osid.NullArgumentException {@code sourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForNewRelationshipsForSource(org.osid.id.Id sourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /** 
     *  Register for notifications of new relationships for the given
     *  destination {@code Id}. {@code
     *  RelationshipReceiver.newRelationship()} is invoked when a new
     *  {@code Relationship} is created.
     *
     *  @param destinationId the {@code Id} of the destination to
     *         monitor
     *  @throws org.osid.NullArgumentException {@code destinationId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForNewRelationshipsForDestination(org.osid.id.Id destinationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of updated relationships. {@code
     *  RelationshipReceiver.changedRelationship()} is invoked when a
     *  relationship is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedRelationships()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of updated relationships. {@code
     *  RelationshipReceiver.changedRelationship()} is invoked when a
     *  {@code Relationship} if changed for the given peer.
     *
     *  @param  relationshipGenusType the genus type of the {@code 
     *          Relationship} to monitor 
     *  @throws org.osid.NullArgumentException {@code
     *         relationshipGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForChangedRelationshipsByGenusType(org.osid.type.Type relationshipGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of updated relationships for the
     *  given source {@code Id}. {@code
     *  RelationshipReceiver.changedRelationship()} is invoked when a
     *  {@code Relationship} in this family is changed.
     *
     *  @param  sourceId the {@code Id} of the source to monitor
     *  @throws org.osid.NullArgumentException {@code sourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForChangedRelationshipsForSource(org.osid.id.Id sourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of updated relationships for the
     *  given destination {@code Id}. {@code
     *  RelationshipReceiver.changedRelationship()} is invoked when a
     *  {@code Relationship} in this family is changed.
     *
     *  @param destinationId the {@code Id} of the destination to
     *         monitor
     *  @throws org.osid.NullArgumentException {@code destinationId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForChangedRelationshipsForDestination(org.osid.id.Id destinationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated relationship. {@code
     *  RelationshipReceiver.changedRelationship()} is invoked when
     *  the specified relationship is changed.
     *
     *  @param relationshipId the {@code Id} of the {@code
     *         Relationship} to monitor
     *  @throws org.osid.NullArgumentException {@code relationshipId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedRelationship(org.osid.id.Id relationshipId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted relationships. {@code
     *  RelationshipReceiver.deletedRelationship()} is invoked when a
     *  relationship is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedRelationships()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of deleted relationships. {@code
     *  RelationshipReceiver.deletedRelationship()} is invoked when a
     *  {@code Relationship} if removed for the given peer.
     *
     *  @param  relationshipGenusType the genus type of the {@code 
     *          Relationship} to monitor 
     *  @throws org.osid.NullArgumentException {@code
     *         relationshipGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForDeletedRelationshipsByGenusType(org.osid.type.Type relationshipGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of deleted relationships for the
     *  given source {@code Id}. {@code
     *  RelationshipReceiver.deletedRelationship()} is invoked when a
     *  {@code Relationship} is deleted or removed from this family.
     *
     *  @param  sourceId the {@code Id} of the source to monitor
     *  @throws org.osid.NullArgumentException {@code sourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */
      
    @OSID @Override
    public void registerForDeletedRelationshipsForSource(org.osid.id.Id sourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of deleted relationships for the
     *  given destination {@code Id}. {@code
     *  RelationshipReceiver.deletedRelationship()} is invoked when a
     *  {@code Relationship} is deleted or removed from this family.
     *
     *  @param destinationId the {@code Id} of the destination to
     *         monitor
     *  @throws org.osid.NullArgumentException {@code destinationId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForDeletedRelationshipsForDestination(org.osid.id.Id destinationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of a deleted relationship. {@code
     *  RelationshipReceiver.deletedRelationship()} is invoked when
     *  the specified relationship is deleted.
     *
     *  @param relationshipId the {@code Id} of the {@code
     *         Relationship} to monitor
     *  @throws org.osid.NullArgumentException {@code relationshipId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedRelationship(org.osid.id.Id relationshipId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
