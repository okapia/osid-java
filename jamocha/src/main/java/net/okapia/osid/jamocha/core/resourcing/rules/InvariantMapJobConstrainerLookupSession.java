//
// InvariantMapJobConstrainerLookupSession
//
//    Implements a JobConstrainer lookup service backed by a fixed collection of
//    jobConstrainers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing.rules;


/**
 *  Implements a JobConstrainer lookup service backed by a fixed
 *  collection of job constrainers. The job constrainers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapJobConstrainerLookupSession
    extends net.okapia.osid.jamocha.core.resourcing.rules.spi.AbstractMapJobConstrainerLookupSession
    implements org.osid.resourcing.rules.JobConstrainerLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapJobConstrainerLookupSession</code> with no
     *  job constrainers.
     *  
     *  @param foundry the foundry
     *  @throws org.osid.NullArgumnetException {@code foundry} is
     *          {@code null}
     */

    public InvariantMapJobConstrainerLookupSession(org.osid.resourcing.Foundry foundry) {
        setFoundry(foundry);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapJobConstrainerLookupSession</code> with a single
     *  job constrainer.
     *  
     *  @param foundry the foundry
     *  @param jobConstrainer a single job constrainer
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code jobConstrainer} is <code>null</code>
     */

      public InvariantMapJobConstrainerLookupSession(org.osid.resourcing.Foundry foundry,
                                               org.osid.resourcing.rules.JobConstrainer jobConstrainer) {
        this(foundry);
        putJobConstrainer(jobConstrainer);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapJobConstrainerLookupSession</code> using an array
     *  of job constrainers.
     *  
     *  @param foundry the foundry
     *  @param jobConstrainers an array of job constrainers
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code jobConstrainers} is <code>null</code>
     */

      public InvariantMapJobConstrainerLookupSession(org.osid.resourcing.Foundry foundry,
                                               org.osid.resourcing.rules.JobConstrainer[] jobConstrainers) {
        this(foundry);
        putJobConstrainers(jobConstrainers);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapJobConstrainerLookupSession</code> using a
     *  collection of job constrainers.
     *
     *  @param foundry the foundry
     *  @param jobConstrainers a collection of job constrainers
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code jobConstrainers} is <code>null</code>
     */

      public InvariantMapJobConstrainerLookupSession(org.osid.resourcing.Foundry foundry,
                                               java.util.Collection<? extends org.osid.resourcing.rules.JobConstrainer> jobConstrainers) {
        this(foundry);
        putJobConstrainers(jobConstrainers);
        return;
    }
}
