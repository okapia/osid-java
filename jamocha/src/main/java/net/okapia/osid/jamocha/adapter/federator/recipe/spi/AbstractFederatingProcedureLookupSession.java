//
// AbstractFederatingProcedureLookupSession.java
//
//     An abstract federating adapter for a ProcedureLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.recipe.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  ProcedureLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingProcedureLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.recipe.ProcedureLookupSession>
    implements org.osid.recipe.ProcedureLookupSession {

    private boolean parallel = false;
    private org.osid.recipe.Cookbook cookbook = new net.okapia.osid.jamocha.nil.recipe.cookbook.UnknownCookbook();


    /**
     *  Constructs a new <code>AbstractFederatingProcedureLookupSession</code>.
     */

    protected AbstractFederatingProcedureLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.recipe.ProcedureLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Cookbook/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Cookbook Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCookbookId() {
        return (this.cookbook.getId());
    }


    /**
     *  Gets the <code>Cookbook</code> associated with this 
     *  session.
     *
     *  @return the <code>Cookbook</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.Cookbook getCookbook()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.cookbook);
    }


    /**
     *  Sets the <code>Cookbook</code>.
     *
     *  @param  cookbook the cookbook for this session
     *  @throws org.osid.NullArgumentException <code>cookbook</code>
     *          is <code>null</code>
     */

    protected void setCookbook(org.osid.recipe.Cookbook cookbook) {
        nullarg(cookbook, "cookbook");
        this.cookbook = cookbook;
        return;
    }


    /**
     *  Tests if this user can perform <code>Procedure</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupProcedures() {
        for (org.osid.recipe.ProcedureLookupSession session : getSessions()) {
            if (session.canLookupProcedures()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Procedure</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeProcedureView() {
        for (org.osid.recipe.ProcedureLookupSession session : getSessions()) {
            session.useComparativeProcedureView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Procedure</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryProcedureView() {
        for (org.osid.recipe.ProcedureLookupSession session : getSessions()) {
            session.usePlenaryProcedureView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include procedures in cookbooks which are children
     *  of this cookbook in the cookbook hierarchy.
     */

    @OSID @Override
    public void useFederatedCookbookView() {
        for (org.osid.recipe.ProcedureLookupSession session : getSessions()) {
            session.useFederatedCookbookView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this cookbook only.
     */

    @OSID @Override
    public void useIsolatedCookbookView() {
        for (org.osid.recipe.ProcedureLookupSession session : getSessions()) {
            session.useIsolatedCookbookView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Procedure</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Procedure</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Procedure</code> and
     *  retained for compatibility.
     *
     *  @param  procedureId <code>Id</code> of the
     *          <code>Procedure</code>
     *  @return the procedure
     *  @throws org.osid.NotFoundException <code>procedureId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>procedureId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.Procedure getProcedure(org.osid.id.Id procedureId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.recipe.ProcedureLookupSession session : getSessions()) {
            try {
                return (session.getProcedure(procedureId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(procedureId + " not found");
    }


    /**
     *  Gets a <code>ProcedureList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  procedures specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Procedures</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  procedureIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Procedure</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>procedureIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureList getProceduresByIds(org.osid.id.IdList procedureIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.recipe.procedure.MutableProcedureList ret = new net.okapia.osid.jamocha.recipe.procedure.MutableProcedureList();

        try (org.osid.id.IdList ids = procedureIds) {
            while (ids.hasNext()) {
                ret.addProcedure(getProcedure(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>ProcedureList</code> corresponding to the given
     *  procedure genus <code>Type</code> which does not include
     *  procedures of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  procedures or an error results. Otherwise, the returned list
     *  may contain only those procedures that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  procedureGenusType a procedure genus type 
     *  @return the returned <code>Procedure</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>procedureGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureList getProceduresByGenusType(org.osid.type.Type procedureGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.recipe.procedure.FederatingProcedureList ret = getProcedureList();

        for (org.osid.recipe.ProcedureLookupSession session : getSessions()) {
            ret.addProcedureList(session.getProceduresByGenusType(procedureGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ProcedureList</code> corresponding to the given
     *  procedure genus <code>Type</code> and include any additional
     *  procedures with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  procedures or an error results. Otherwise, the returned list
     *  may contain only those procedures that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  procedureGenusType a procedure genus type 
     *  @return the returned <code>Procedure</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>procedureGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureList getProceduresByParentGenusType(org.osid.type.Type procedureGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.recipe.procedure.FederatingProcedureList ret = getProcedureList();

        for (org.osid.recipe.ProcedureLookupSession session : getSessions()) {
            ret.addProcedureList(session.getProceduresByParentGenusType(procedureGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ProcedureList</code> containing the given
     *  procedure record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  procedures or an error results. Otherwise, the returned list
     *  may contain only those procedures that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  procedureRecordType a procedure record type 
     *  @return the returned <code>Procedure</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>procedureRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureList getProceduresByRecordType(org.osid.type.Type procedureRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.recipe.procedure.FederatingProcedureList ret = getProcedureList();

        for (org.osid.recipe.ProcedureLookupSession session : getSessions()) {
            ret.addProcedureList(session.getProceduresByRecordType(procedureRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Procedures</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  procedures or an error results. Otherwise, the returned list
     *  may contain only those procedures that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Procedures</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureList getProcedures()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.recipe.procedure.FederatingProcedureList ret = getProcedureList();

        for (org.osid.recipe.ProcedureLookupSession session : getSessions()) {
            ret.addProcedureList(session.getProcedures());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.recipe.procedure.FederatingProcedureList getProcedureList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.recipe.procedure.ParallelProcedureList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.recipe.procedure.CompositeProcedureList());
        }
    }
}
