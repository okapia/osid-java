//
// AbstractRepositoryQuery.java
//
//     A template for making a Repository Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.repository.repository.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for repositories.
 */

public abstract class AbstractRepositoryQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQuery
    implements org.osid.repository.RepositoryQuery {

    private final java.util.Collection<org.osid.repository.records.RepositoryQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the asset <code> Id </code> for this query. 
     *
     *  @param  assetId an asset <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assetId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAssetId(org.osid.id.Id assetId, boolean match) {
        return;
    }


    /**
     *  Clears the asset <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAssetIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AssetQuery </code> is available. 
     *
     *  @return <code> true </code> if an asset query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssetQuery() {
        return (false);
    }


    /**
     *  Gets the query for an asset. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the asset query 
     *  @throws org.osid.UnimplementedException <code> supportsAssetQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetQuery getAssetQuery() {
        throw new org.osid.UnimplementedException("supportsAssetQuery() is false");
    }


    /**
     *  Matches repositories that has any asset mapping. 
     *
     *  @param  match <code> true </code> to match repositories with any 
     *          asset, <code> false </code> to match repositories with no 
     *          asset 
     */

    @OSID @Override
    public void matchAnyAsset(boolean match) {
        return;
    }


    /**
     *  Clears the asset terms. 
     */

    @OSID @Override
    public void clearAssetTerms() {
        return;
    }


    /**
     *  Sets the composition <code> Id </code> for this query. 
     *
     *  @param  compositionId a composition <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> compositionId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCompositionId(org.osid.id.Id compositionId, boolean match) {
        return;
    }


    /**
     *  Clears the composition <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCompositionIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CompositionQuery </code> is available. 
     *
     *  @return <code> true </code> if a composition query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompositionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a composition. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the composition query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.CompositionQuery getCompositionQuery() {
        throw new org.osid.UnimplementedException("supportsCompositionQuery() is false");
    }


    /**
     *  Matches repositories that has any composition mapping. 
     *
     *  @param  match <code> true </code> to match repositories with any 
     *          composition, <code> false </code> to match repositories with 
     *          no composition 
     */

    @OSID @Override
    public void matchAnyComposition(boolean match) {
        return;
    }


    /**
     *  Clears the composition terms. 
     */

    @OSID @Override
    public void clearCompositionTerms() {
        return;
    }


    /**
     *  Sets the repository <code> Id </code> for this query to match 
     *  repositories that have the specified repository as an ancestor. 
     *
     *  @param  repositoryId a repository <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorRepositoryId(org.osid.id.Id repositoryId, 
                                          boolean match) {
        return;
    }


    /**
     *  Clears the ancestor repository <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorRepositoryIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> RepositoryQuery </code> is available. 
     *
     *  @return <code> true </code> if a repository query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorRepositoryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a repository. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the repository query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorRepositoryQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.RepositoryQuery getAncestorRepositoryQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorRepositoryQuery() is false");
    }


    /**
     *  Matches repositories with any ancestor. 
     *
     *  @param  match <code> true </code> to match repositories with any 
     *          ancestor, <code> false </code> to match root repositories 
     */

    @OSID @Override
    public void matchAnyAncestorRepository(boolean match) {
        return;
    }


    /**
     *  Clears the ancestor repository terms. 
     */

    @OSID @Override
    public void clearAncestorRepositoryTerms() {
        return;
    }


    /**
     *  Sets the repository <code> Id </code> for this query to match 
     *  repositories that have the specified repository as a descendant. 
     *
     *  @param  repositoryId a repository <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantRepositoryId(org.osid.id.Id repositoryId, 
                                            boolean match) {
        return;
    }


    /**
     *  Clears the descendant repository <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantRepositoryIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> RepositoryQuery </code> is available. 
     *
     *  @return <code> true </code> if a repository query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantRepositoryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a repository. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the repository query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantRepositoryQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.RepositoryQuery getDescendantRepositoryQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantRepositoryQuery() is false");
    }


    /**
     *  Matches repositories with any descendant. 
     *
     *  @param  match <code> true </code> to match repositories with any 
     *          descendant, <code> false </code> to match leaf repositories 
     */

    @OSID @Override
    public void matchAnyDescendantRepository(boolean match) {
        return;
    }


    /**
     *  Clears the descendant repository terms. 
     */

    @OSID @Override
    public void clearDescendantRepositoryTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given repository query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a repository implementing the requested record.
     *
     *  @param repositoryRecordType a repository record type
     *  @return the repository query record
     *  @throws org.osid.NullArgumentException
     *          <code>repositoryRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(repositoryRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.repository.records.RepositoryQueryRecord getRepositoryQueryRecord(org.osid.type.Type repositoryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.repository.records.RepositoryQueryRecord record : this.records) {
            if (record.implementsRecordType(repositoryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(repositoryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this repository query. 
     *
     *  @param repositoryQueryRecord repository query record
     *  @param repositoryRecordType repository record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRepositoryQueryRecord(org.osid.repository.records.RepositoryQueryRecord repositoryQueryRecord, 
                                          org.osid.type.Type repositoryRecordType) {

        addRecordType(repositoryRecordType);
        nullarg(repositoryQueryRecord, "repository query record");
        this.records.add(repositoryQueryRecord);        
        return;
    }
}
