//
// InvariantMapProxyForumLookupSession
//
//    Implements a Forum lookup service backed by a fixed
//    collection of forums. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.forum;


/**
 *  Implements a Forum lookup service backed by a fixed
 *  collection of forums. The forums are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyForumLookupSession
    extends net.okapia.osid.jamocha.core.forum.spi.AbstractMapForumLookupSession
    implements org.osid.forum.ForumLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyForumLookupSession} with no
     *  forums.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public InvariantMapProxyForumLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyForumLookupSession} with a
     *  single forum.
     *
     *  @param forum a single forum
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code forum} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyForumLookupSession(org.osid.forum.Forum forum, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putForum(forum);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyForumLookupSession} using
     *  an array of forums.
     *
     *  @param forums an array of forums
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code forums} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyForumLookupSession(org.osid.forum.Forum[] forums, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putForums(forums);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyForumLookupSession} using a
     *  collection of forums.
     *
     *  @param forums a collection of forums
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code forums} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyForumLookupSession(java.util.Collection<? extends org.osid.forum.Forum> forums,
                                                  org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putForums(forums);
        return;
    }
}
