//
// AbstractFloorQuery.java
//
//     A template for making a Floor Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.floor.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for floors.
 */

public abstract class AbstractFloorQuery    
    extends net.okapia.osid.jamocha.spi.AbstractTemporalOsidObjectQuery
    implements org.osid.room.FloorQuery {

    private final java.util.Collection<org.osid.room.records.FloorQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the building <code> Id </code> for this query to match floors 
     *  assigned to buildings. 
     *
     *  @param  buildingId a building <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> buildingId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBuildingId(org.osid.id.Id buildingId, boolean match) {
        return;
    }


    /**
     *  Clears the building <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBuildingIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BuildingQuery </code> is available. 
     *
     *  @return <code> true </code> if a building query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBuildingQuery() {
        return (false);
    }


    /**
     *  Gets the query for a building query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the building query 
     *  @throws org.osid.UnimplementedException <code> supportsBuildingQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingQuery getBuildingQuery() {
        throw new org.osid.UnimplementedException("supportsBuildingQuery() is false");
    }


    /**
     *  Clears the building terms. 
     */

    @OSID @Override
    public void clearBuildingTerms() {
        return;
    }


    /**
     *  Sets a floor number. 
     *
     *  @param  number a number 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> number </code> is not 
     *          of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> number </code> is <code> 
     *          null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchNumber(String number, org.osid.type.Type stringMatchType, 
                            boolean match) {
        return;
    }


    /**
     *  Matches any floor number. 
     *
     *  @param  match <code> true </code> to match floors with any number, 
     *          <code> false </code> to match floors with no number 
     */

    @OSID @Override
    public void matchAnyNumber(boolean match) {
        return;
    }


    /**
     *  Clears the number terms. 
     */

    @OSID @Override
    public void clearNumberTerms() {
        return;
    }


    /**
     *  Matches an area within the given range inclusive. 
     *
     *  @param  low start of range 
     *  @param  high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     */

    @OSID @Override
    public void matchGrossArea(java.math.BigDecimal low, 
                               java.math.BigDecimal high, boolean match) {
        return;
    }


    /**
     *  Matches any area. 
     *
     *  @param  match <code> true </code> to match floors with any area, 
     *          <code> false </code> to match floors with no area assigned 
     */

    @OSID @Override
    public void matchAnyGrossArea(boolean match) {
        return;
    }


    /**
     *  Clears the area terms. 
     */

    @OSID @Override
    public void clearGrossAreaTerms() {
        return;
    }


    /**
     *  Sets the room <code> Id </code> for this query to match rooms assigned 
     *  to floors. 
     *
     *  @param  roomId a room <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> roomId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchRoomId(org.osid.id.Id roomId, boolean match) {
        return;
    }


    /**
     *  Clears the room <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRoomIdTerms() {
        return;
    }


    /**
     *  Tests if a room query is available. 
     *
     *  @return <code> true </code> if a room query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRoomQuery() {
        return (false);
    }


    /**
     *  Gets the query for a floor. 
     *
     *  @return the room query 
     *  @throws org.osid.UnimplementedException <code> supportsRoomQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomQuery getRoomQuery() {
        throw new org.osid.UnimplementedException("supportsRoomQuery() is false");
    }


    /**
     *  Matches floors. with any room. 
     *
     *  @param  match <code> true </code> to match floors with any room, 
     *          <code> false </code> to match floors with no rooms 
     */

    @OSID @Override
    public void matchAnyRoom(boolean match) {
        return;
    }


    /**
     *  Clears the room terms. 
     */

    @OSID @Override
    public void clearRoomTerms() {
        return;
    }


    /**
     *  Sets the floor <code> Id </code> for this query to match rooms 
     *  assigned to campuses. 
     *
     *  @param  campusId a campus <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCampusId(org.osid.id.Id campusId, boolean match) {
        return;
    }


    /**
     *  Clears the campus <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCampusIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CampusQuery </code> is available. 
     *
     *  @return <code> true </code> if a campus query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCampusQuery() {
        return (false);
    }


    /**
     *  Gets the query for a campus query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the campus query 
     *  @throws org.osid.UnimplementedException <code> supportsCampusQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.CampusQuery getCampusQuery() {
        throw new org.osid.UnimplementedException("supportsCampusQuery() is false");
    }


    /**
     *  Clears the campus terms. 
     */

    @OSID @Override
    public void clearCampusTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given floor query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a floor implementing the requested record.
     *
     *  @param floorRecordType a floor record type
     *  @return the floor query record
     *  @throws org.osid.NullArgumentException
     *          <code>floorRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(floorRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.room.records.FloorQueryRecord getFloorQueryRecord(org.osid.type.Type floorRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.room.records.FloorQueryRecord record : this.records) {
            if (record.implementsRecordType(floorRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(floorRecordType + " is not supported");
    }


    /**
     *  Adds a record to this floor query. 
     *
     *  @param floorQueryRecord floor query record
     *  @param floorRecordType floor record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addFloorQueryRecord(org.osid.room.records.FloorQueryRecord floorQueryRecord, 
                                          org.osid.type.Type floorRecordType) {

        addRecordType(floorRecordType);
        nullarg(floorQueryRecord, "floor query record");
        this.records.add(floorQueryRecord);        
        return;
    }
}
