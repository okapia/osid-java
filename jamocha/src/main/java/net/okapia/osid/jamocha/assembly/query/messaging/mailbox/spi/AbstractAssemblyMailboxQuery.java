//
// AbstractAssemblyMailboxQuery.java
//
//     A MailboxQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.messaging.mailbox.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A MailboxQuery that stores terms.
 */

public abstract class AbstractAssemblyMailboxQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCatalogQuery
    implements org.osid.messaging.MailboxQuery,
               org.osid.messaging.MailboxQueryInspector,
               org.osid.messaging.MailboxSearchOrder {

    private final java.util.Collection<org.osid.messaging.records.MailboxQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.messaging.records.MailboxQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.messaging.records.MailboxSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyMailboxQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyMailboxQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the message <code> Id </code> for this query. 
     *
     *  @param  messageId a message <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> messageId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchMessageId(org.osid.id.Id messageId, boolean match) {
        getAssembler().addIdTerm(getMessageIdColumn(), messageId, match);
        return;
    }


    /**
     *  Clears the message <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearMessageIdTerms() {
        getAssembler().clearTerms(getMessageIdColumn());
        return;
    }


    /**
     *  Gets the message <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getMessageIdTerms() {
        return (getAssembler().getIdTerms(getMessageIdColumn()));
    }


    /**
     *  Gets the MessageId column name.
     *
     * @return the column name
     */

    protected String getMessageIdColumn() {
        return ("message_id");
    }


    /**
     *  Tests if a <code> MessageQuery </code> is available. 
     *
     *  @return <code> true </code> if a message query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMessageQuery() {
        return (false);
    }


    /**
     *  Gets the query for a message. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the message query 
     *  @throws org.osid.UnimplementedException <code> supportsMessageQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageQuery getMessageQuery() {
        throw new org.osid.UnimplementedException("supportsMessageQuery() is false");
    }


    /**
     *  Matches mailboxes with any messages. 
     *
     *  @param  match <code> true </code> to match mailboxes with any 
     *          messages, <code> false </code> to match mailboxes with no 
     *          messages 
     */

    @OSID @Override
    public void matchAnyMessage(boolean match) {
        getAssembler().addIdWildcardTerm(getMessageColumn(), match);
        return;
    }


    /**
     *  Clears the message terms. 
     */

    @OSID @Override
    public void clearMessageTerms() {
        getAssembler().clearTerms(getMessageColumn());
        return;
    }


    /**
     *  Gets the message query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.messaging.MessageQueryInspector[] getMessageTerms() {
        return (new org.osid.messaging.MessageQueryInspector[0]);
    }


    /**
     *  Gets the Message column name.
     *
     * @return the column name
     */

    protected String getMessageColumn() {
        return ("message");
    }


    /**
     *  Sets the mailbox <code> Id </code> for this query to match mailboxes 
     *  that have the specified mailbox as an ancestor. 
     *
     *  @param  mailboxId a mailbox <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> mailboxId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorMailboxId(org.osid.id.Id mailboxId, boolean match) {
        getAssembler().addIdTerm(getAncestorMailboxIdColumn(), mailboxId, match);
        return;
    }


    /**
     *  Clears the ancestor mailbox <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorMailboxIdTerms() {
        getAssembler().clearTerms(getAncestorMailboxIdColumn());
        return;
    }


    /**
     *  Gets the ancestor mailbox <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorMailboxIdTerms() {
        return (getAssembler().getIdTerms(getAncestorMailboxIdColumn()));
    }


    /**
     *  Gets the AncestorMailboxId column name.
     *
     * @return the column name
     */

    protected String getAncestorMailboxIdColumn() {
        return ("ancestor_mailbox_id");
    }


    /**
     *  Tests if a <code> MailboxQuery </code> is available. 
     *
     *  @return <code> true </code> if a mailbox query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorMailboxQuery() {
        return (false);
    }


    /**
     *  Gets the query for a mailbox. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the mailbox query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorMailboxQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MailboxQuery getAncestorMailboxQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorMailboxQuery() is false");
    }


    /**
     *  Matches mailboxes with any ancestor. 
     *
     *  @param  match <code> true </code> to match mailboxes with any 
     *          ancestor, <code> false </code> to match root mailboxes 
     */

    @OSID @Override
    public void matchAnyAncestorMailbox(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorMailboxColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor mailbox terms. 
     */

    @OSID @Override
    public void clearAncestorMailboxTerms() {
        getAssembler().clearTerms(getAncestorMailboxColumn());
        return;
    }


    /**
     *  Gets the ancestor mailbox query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.messaging.MailboxQueryInspector[] getAncestorMailboxTerms() {
        return (new org.osid.messaging.MailboxQueryInspector[0]);
    }


    /**
     *  Gets the AncestorMailbox column name.
     *
     * @return the column name
     */

    protected String getAncestorMailboxColumn() {
        return ("ancestor_mailbox");
    }


    /**
     *  Sets the mailbox <code> Id </code> for this query to match mailboxes 
     *  that have the specified mailbox as a descendant. 
     *
     *  @param  mailboxId a mailbox <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> mailboxId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantMailboxId(org.osid.id.Id mailboxId, 
                                         boolean match) {
        getAssembler().addIdTerm(getDescendantMailboxIdColumn(), mailboxId, match);
        return;
    }


    /**
     *  Clears the descendant mailbox <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantMailboxIdTerms() {
        getAssembler().clearTerms(getDescendantMailboxIdColumn());
        return;
    }


    /**
     *  Gets the descendant mailbox <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantMailboxIdTerms() {
        return (getAssembler().getIdTerms(getDescendantMailboxIdColumn()));
    }


    /**
     *  Gets the DescendantMailboxId column name.
     *
     * @return the column name
     */

    protected String getDescendantMailboxIdColumn() {
        return ("descendant_mailbox_id");
    }


    /**
     *  Tests if a <code> MailboxQuery </code> is available. 
     *
     *  @return <code> true </code> if a mailbox query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantMailboxQuery() {
        return (false);
    }


    /**
     *  Gets the query for a mailbox. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the mailbox query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantMailboxQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.messaging.MailboxQuery getDescendantMailboxQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantMailboxQuery() is false");
    }


    /**
     *  Matches mailboxes with any descendant. 
     *
     *  @param  match <code> true </code> to match mailboxes with any 
     *          descendant, <code> false </code> to match leaf mailboxes 
     */

    @OSID @Override
    public void matchAnyDescendantMailbox(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantMailboxColumn(), match);
        return;
    }


    /**
     *  Clears the descendant mailbox terms. 
     */

    @OSID @Override
    public void clearDescendantMailboxTerms() {
        getAssembler().clearTerms(getDescendantMailboxColumn());
        return;
    }


    /**
     *  Gets the descendant mailbox query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.messaging.MailboxQueryInspector[] getDescendantMailboxTerms() {
        return (new org.osid.messaging.MailboxQueryInspector[0]);
    }


    /**
     *  Gets the DescendantMailbox column name.
     *
     * @return the column name
     */

    protected String getDescendantMailboxColumn() {
        return ("descendant_mailbox");
    }


    /**
     *  Tests if this mailbox supports the given record
     *  <code>Type</code>.
     *
     *  @param  mailboxRecordType a mailbox record type 
     *  @return <code>true</code> if the mailboxRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>mailboxRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type mailboxRecordType) {
        for (org.osid.messaging.records.MailboxQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(mailboxRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  mailboxRecordType the mailbox record type 
     *  @return the mailbox query record 
     *  @throws org.osid.NullArgumentException
     *          <code>mailboxRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(mailboxRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.messaging.records.MailboxQueryRecord getMailboxQueryRecord(org.osid.type.Type mailboxRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.messaging.records.MailboxQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(mailboxRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(mailboxRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  mailboxRecordType the mailbox record type 
     *  @return the mailbox query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>mailboxRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(mailboxRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.messaging.records.MailboxQueryInspectorRecord getMailboxQueryInspectorRecord(org.osid.type.Type mailboxRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.messaging.records.MailboxQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(mailboxRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(mailboxRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param mailboxRecordType the mailbox record type
     *  @return the mailbox search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>mailboxRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(mailboxRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.messaging.records.MailboxSearchOrderRecord getMailboxSearchOrderRecord(org.osid.type.Type mailboxRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.messaging.records.MailboxSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(mailboxRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(mailboxRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this mailbox. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param mailboxQueryRecord the mailbox query record
     *  @param mailboxQueryInspectorRecord the mailbox query inspector
     *         record
     *  @param mailboxSearchOrderRecord the mailbox search order record
     *  @param mailboxRecordType mailbox record type
     *  @throws org.osid.NullArgumentException
     *          <code>mailboxQueryRecord</code>,
     *          <code>mailboxQueryInspectorRecord</code>,
     *          <code>mailboxSearchOrderRecord</code> or
     *          <code>mailboxRecordTypemailbox</code> is
     *          <code>null</code>
     */
            
    protected void addMailboxRecords(org.osid.messaging.records.MailboxQueryRecord mailboxQueryRecord, 
                                      org.osid.messaging.records.MailboxQueryInspectorRecord mailboxQueryInspectorRecord, 
                                      org.osid.messaging.records.MailboxSearchOrderRecord mailboxSearchOrderRecord, 
                                      org.osid.type.Type mailboxRecordType) {

        addRecordType(mailboxRecordType);

        nullarg(mailboxQueryRecord, "mailbox query record");
        nullarg(mailboxQueryInspectorRecord, "mailbox query inspector record");
        nullarg(mailboxSearchOrderRecord, "mailbox search odrer record");

        this.queryRecords.add(mailboxQueryRecord);
        this.queryInspectorRecords.add(mailboxQueryInspectorRecord);
        this.searchOrderRecords.add(mailboxSearchOrderRecord);
        
        return;
    }
}
