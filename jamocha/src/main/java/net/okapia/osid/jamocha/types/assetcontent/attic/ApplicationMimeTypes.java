application/activemessage
    ATOM ("application/atom+xml", "Atom Syndication Format", "atom"),
    application/atomcat+xml				atomcat
application/atomsvc+xml				atomsvc
application/calendar+xml
application/cellml+xml
application/cpl+xml
application/csta+xml
application/cstadata+xml
application/dialog-info+xml
application/docbook+xml				dbk
application/dssc+xml				xdssc
application/ecmascript				ecma
application/emma+xml				emma
application/epp+xml
application/epub+zip				epub
application/font-tdpfr				pfr
application/framework-attributes+xml
application/gml+xml				gml
application/gpx+xml				gpx
application/gxf					gxf
application/held+xml
application/inkml+xml				ink inkml
application/isup
application/java-archive			jar
application/java-vm				class
application/javascript				js
application/json				json
application/jsonml+json				jsonml
application/kpml-request+xml
application/kpml-response+xml
application/lost+xml				lostxml
application/mac-binhex40			hqx
application/mac-compactpro			cpt
application/macwriteii
application/mads+xml				mads
application/marc				mrc
application/marcxml+xml				mrcx
application/mathematica				ma nb mb
application/mathml-content+xml
application/mathml-presentation+xml
application/mathml+xml				mathml
application/mbox				mbox
application/metalink+xml			metalink
application/metalink4+xml			meta4
application/mets+xml				mets
application/mikey
application/mods+xml				mods
application/mp21				m21 mp21
application/mp4					mp4s
application/mpeg4-generic
application/mpeg4-iod
application/mpeg4-iod-xmt
application/msc-ivr+xml
application/msc-mixer+xml
application/msword				doc dot
application/mxf					mxf
application/nasdata
application/octet-stream	bin dms lrf mar so dist distz pkg bpk dump elc deploy
application/oda					oda
application/oebps-package+xml			opf
application/ogg					ogx
application/omdoc+xml				omdoc
application/onenote				onetoc onetoc2 onetmp onepkg
application/oxps				oxps
application/pdf					pdf
application/pgp-encrypted			pgp
application/pgp-keys
application/pgp-signature			asc sig
application/pidf+xml
application/pidf-diff+xml
application/pkcs10				p10
application/pkcs7-mime				p7m p7c
application/pkcs7-signature			p7s
application/pkcs8				p8
application/pkix-attr-cert			ac
application/pkix-cert				cer
application/pkix-crl				crl
application/pkix-pkipath			pkipath
application/pkixcmp				pki
application/pls+xml				pls
application/poc-settings+xml
application/postscript				ai eps ps
application/prs.alvestrand.titrax-sheet
application/prs.cww				cww
application/prs.nprend
application/prs.plucker
application/prs.rdf-xml-crypt
application/prs.xsf+xml
application/pskc+xml				pskcxml
application/qsig
application/rdf+xml				rdf
application/reginfo+xml				rif
application/relax-ng-compact-syntax		rnc
application/resource-lists+xml			rl
application/resource-lists-diff+xml		rld
application/riscos
application/rlmi+xml
application/rls-services+xml			rs
application/rpki-ghostbusters			gbr
application/rpki-manifest			mft
application/rpki-roa				roa
application/rpki-updown
application/rsd+xml				rsd
application/rss+xml				rss
application/rtf					rtf
application/rtx
application/samlassertion+xml
application/samlmetadata+xml
application/sbml+xml				sbml
application/sdp					sdp
application/sgml
application/sgml-open-catalog
application/shf+xml				shf
application/sieve
application/simple-filter+xml
application/simple-message-summary
application/simplesymbolcontainer
application/slate
application/smil
application/smil+xml				smi smil
application/sparql-query			rq
application/sparql-results+xml			srx
application/spirits-event+xml
application/srgs				gram
application/srgs+xml				grxml
application/sru+xml				sru
application/ssdl+xml				ssdl
application/ssml+xml				ssml
application/tei+xml				tei teicorpus
application/thraud+xml				tfi
application/tve-trigger
application/vcard+xml
application/vemmi
application/vividence.scriptfile
application/vnd.geocube+xml
application/vnd.geogebra.file			ggb
application/vnd.geogebra.tool			ggt
application/vnd.geometry-explorer		gex gre
application/vnd.dvb.service			svc
application/vnd.dynageo				geo
application/vnd.ecowin.chart			mag
application/vnd.enliven				nml
application/vnd.epson.esf			esf
application/vnd.epson.msf			msf
application/vnd.epson.quickanime		qam
application/vnd.epson.salt			slt
application/vnd.epson.ssf			ssf
application/vnd.eszigno3+xml			es3 et3
application/vnd.ezpix-album			ez2
application/vnd.ezpix-package			ez3
application/vnd.fdf				fdf
application/vnd.fdsn.mseed			mseed
application/vnd.fdsn.seed			seed dataless
application/vnd.flographit			gph
application/vnd.fluxtime.clip			ftc
application/vnd.framemaker			fm frame maker book
application/vnd.frogans.fnc			fnc
application/vnd.frogans.ltf			ltf
application/vnd.fsc.weblaunch			fsc
application/vnd.fujitsu.oasys			oas
application/vnd.fujitsu.oasys2			oa2
application/vnd.fujitsu.oasys3			oa3
application/vnd.fujitsu.oasysgp			fg5
application/vnd.fujitsu.oasysprs		bh2
application/vnd.fujixerox.ddd			ddd
application/vnd.fujixerox.docuworks		xdw
application/vnd.fujixerox.docuworks.binder	xbd
application/vnd.fuzzysheet			fzs
application/vnd.genomatix.tuxedo		txd
application/vnd.geonext				gxt
application/vnd.geoplan				g2w
application/vnd.geospace			g3w
application/vnd.gmx				gmx
application/vnd.google-earth.kml+xml		kml
application/vnd.google-earth.kmz		kmz
application/vnd.grafeq				gqf gqs
application/vnd.gridmp
application/vnd.hal+json
application/vnd.hal+xml				hal
application/vnd.handheld-entertainment+xml	zmm
application/vnd.hbci				hbci
application/vnd.hhe.lesson-player		les
application/vnd.hp-hpgl				hpgl
application/vnd.hp-hpid				hpid
application/vnd.hp-hps				hps
application/vnd.hp-jlyt				jlt
application/vnd.hp-pcl				pcl
application/vnd.hp-pclxl			pclxl
application/vnd.hydrostatix.sof-data		sfd-hdstx
application/vnd.hzn-3d-crossword
application/vnd.ibm.afplinedata
application/vnd.ibm.electronic-media
application/vnd.ibm.minipay			mpy
application/vnd.ibm.modcap			afp listafp list3820
application/vnd.ibm.rights-management		irm
application/vnd.ibm.secure-container		sc
application/vnd.iccprofile			icc icm
application/vnd.igloader			igl
application/vnd.immervision-ivp			ivp
application/vnd.immervision-ivu			ivu
application/vnd.insors.igm			igm
application/vnd.intercon.formnet		xpw xpx
application/vnd.intergeo			i2g
application/vnd.intu.qbo			qbo
application/vnd.intu.qfx			qfx
application/vnd.iptc.g2.conceptitem+xml
application/vnd.iptc.g2.knowledgeitem+xml
application/vnd.iptc.g2.newsitem+xml
application/vnd.iptc.g2.newsmessage+xml
application/vnd.iptc.g2.packageitem+xml
application/vnd.iptc.g2.planningitem+xml
application/vnd.ipunplugged.rcprofile		rcprofile
application/vnd.irepository.package+xml		irp
application/vnd.is-xpr				xpr
application/vnd.isac.fcs			fcs
application/vnd.jam				jam
application/vnd.jcp.javame.midlet-rms		rms
application/vnd.jisp				jisp
application/vnd.joost.joda-archive		joda
application/vnd.kahootz				ktz ktr
application/vnd.kde.karbon			karbon
application/vnd.kde.kchart			chrt
application/vnd.kde.kformula			kfo
application/vnd.kde.kivio			flw
application/vnd.kde.kontour			kon
application/vnd.kde.kpresenter			kpr kpt
application/vnd.kde.kspread			ksp
application/vnd.kde.kword			kwd kwt
application/vnd.kenameaapp			htke
application/vnd.kidspiration			kia
application/vnd.kinar				kne knp
application/vnd.koan				skp skd skt skm
application/vnd.kodak-descriptor		sse
application/vnd.las.las+xml			lasxml
application/vnd.liberty-request+xml
application/vnd.llamagraphics.life-balance.desktop	lbd
application/vnd.llamagraphics.life-balance.exchange+xml	lbe
application/vnd.lotus-1-2-3			123
application/vnd.lotus-approach			apr
application/vnd.lotus-freelance			pre
application/vnd.lotus-notes			nsf
application/vnd.lotus-organizer			org
application/vnd.lotus-screencam			scm
application/vnd.lotus-wordpro			lwp
application/vnd.macports.portpkg		portpkg
application/vnd.mcd				mcd
application/vnd.medcalcdata			mc1
application/vnd.mediastation.cdkey		cdkey
application/vnd.meridian-slingshot
application/vnd.mfer				mwf
application/vnd.mfmp				mfm
application/vnd.micrografx.flo			flo
application/vnd.micrografx.igx			igx
application/vnd.mif				mif
application/vnd.mobius.daf			daf
application/vnd.mobius.dis			dis
application/vnd.mobius.mbk			mbk
application/vnd.mobius.mqy			mqy
application/vnd.mobius.msl			msl
application/vnd.mobius.plc			plc
application/vnd.mobius.txf			txf
application/vnd.mophun.application		mpn
application/vnd.mophun.certificate		mpc
application/vnd.mozilla.xul+xml			xul
application/vnd.ms-artgalry			cil
application/vnd.ms-cab-compressed		cab
application/vnd.ms-excel			xls xlm xla xlc xlt xlw
application/vnd.ms-excel.addin.macroenabled.12		xlam
application/vnd.ms-excel.sheet.binary.macroenabled.12	xlsb
application/vnd.ms-excel.sheet.macroenabled.12		xlsm
application/vnd.ms-excel.template.macroenabled.12	xltm
application/vnd.ms-fontobject			eot
application/vnd.ms-htmlhelp			chm
application/vnd.ms-ims				ims
application/vnd.ms-lrm				lrm
application/vnd.ms-officetheme			thmx
application/vnd.ms-opentype
application/vnd.ms-pki.seccat			cat
application/vnd.ms-pki.stl			stl
application/vnd.ms-playready.initiator+xml
application/vnd.ms-powerpoint			ppt pps pot
application/vnd.ms-powerpoint.addin.macroenabled.12		ppam
application/vnd.ms-powerpoint.presentation.macroenabled.12	pptm
application/vnd.ms-powerpoint.slide.macroenabled.12		sldm
application/vnd.ms-powerpoint.slideshow.macroenabled.12		ppsm
application/vnd.ms-powerpoint.template.macroenabled.12		potm
application/vnd.ms-printing.printticket+xml
application/vnd.ms-project			mpp mpt
application/vnd.ms-tnef
application/vnd.ms-word.document.macroenabled.12	docm
application/vnd.ms-word.template.macroenabled.12	dotm
application/vnd.ms-works			wps wks wcm wdb
application/vnd.ms-wpl				wpl
application/vnd.ms-xpsdocument			xps
application/vnd.mseq				mseq
application/vnd.musician			mus
application/vnd.muvee.style			msty
application/vnd.mynfc				taglet
application/vnd.neurolanguage.nlu		nlu
application/vnd.nitf				ntf nitf
application/vnd.noblenet-directory		nnd
application/vnd.noblenet-sealer			nns
application/vnd.noblenet-web			nnw
application/vnd.nokia.n-gage.data		ngdat
application/vnd.nokia.n-gage.symbian.install	n-gage
application/vnd.nokia.radio-preset		rpst
application/vnd.nokia.radio-presets		rpss
application/vnd.novadigm.edm			edm
application/vnd.novadigm.edx			edx
application/vnd.novadigm.ext			ext
application/vnd.oasis.opendocument.chart		odc
application/vnd.oasis.opendocument.chart-template	otc
application/vnd.oasis.opendocument.database		odb
application/vnd.oasis.opendocument.formula		odf
application/vnd.oasis.opendocument.formula-template	odft
application/vnd.oasis.opendocument.graphics		odg
application/vnd.oasis.opendocument.graphics-template	otg
application/vnd.oasis.opendocument.image		odi
application/vnd.oasis.opendocument.image-template	oti
application/vnd.oasis.opendocument.presentation		odp
application/vnd.oasis.opendocument.presentation-template	otp
application/vnd.oasis.opendocument.spreadsheet		ods
application/vnd.oasis.opendocument.spreadsheet-template	ots
application/vnd.oasis.opendocument.text			odt
application/vnd.oasis.opendocument.text-master		odm
application/vnd.oasis.opendocument.text-template	ott
application/vnd.oasis.opendocument.text-web		oth
application/vnd.olpc-sugar			xo
application/vnd.oma.dd2+xml			dd2
application/vnd.openofficeorg.extension		oxt
application/vnd.openxmlformats-officedocument.custom-properties+xml
application/vnd.openxmlformats-officedocument.customxmlproperties+xml
application/vnd.openxmlformats-officedocument.drawing+xml
application/vnd.openxmlformats-officedocument.drawingml.chart+xml
application/vnd.openxmlformats-officedocument.drawingml.chartshapes+xml
application/vnd.openxmlformats-officedocument.drawingml.diagramcolors+xml
application/vnd.openxmlformats-officedocument.drawingml.diagramdata+xml
application/vnd.openxmlformats-officedocument.drawingml.diagramlayout+xml
application/vnd.openxmlformats-officedocument.drawingml.diagramstyle+xml
application/vnd.openxmlformats-officedocument.extended-properties+xml
application/vnd.openxmlformats-officedocument.presentationml.commentauthors+xml
application/vnd.openxmlformats-officedocument.presentationml.comments+xml
application/vnd.openxmlformats-officedocument.presentationml.handoutmaster+xml
application/vnd.openxmlformats-officedocument.presentationml.notesmaster+xml
application/vnd.openxmlformats-officedocument.presentationml.notesslide+xml
application/vnd.openxmlformats-officedocument.presentationml.presentation	pptx
application/vnd.openxmlformats-officedocument.presentationml.presentation.main+xml
application/vnd.openxmlformats-officedocument.presentationml.presprops+xml
application/vnd.openxmlformats-officedocument.presentationml.slide	sldx
application/vnd.openxmlformats-officedocument.presentationml.slide+xml
application/vnd.openxmlformats-officedocument.presentationml.slidelayout+xml
application/vnd.openxmlformats-officedocument.presentationml.slidemaster+xml
application/vnd.openxmlformats-officedocument.presentationml.slideshow	ppsx
application/vnd.openxmlformats-officedocument.presentationml.slideshow.main+xml
application/vnd.openxmlformats-officedocument.presentationml.slideupdateinfo+xml
application/vnd.openxmlformats-officedocument.presentationml.tablestyles+xml
application/vnd.openxmlformats-officedocument.presentationml.tags+xml
application/vnd.openxmlformats-officedocument.presentationml.template	potx
application/vnd.openxmlformats-officedocument.presentationml.template.main+xml
application/vnd.openxmlformats-officedocument.presentationml.viewprops+xml
application/vnd.openxmlformats-officedocument.spreadsheetml.calcchain+xml
application/vnd.openxmlformats-officedocument.spreadsheetml.chartsheet+xml
application/vnd.openxmlformats-officedocument.spreadsheetml.comments+xml
application/vnd.openxmlformats-officedocument.spreadsheetml.connections+xml
application/vnd.openxmlformats-officedocument.spreadsheetml.dialogsheet+xml
application/vnd.openxmlformats-officedocument.spreadsheetml.externallink+xml
application/vnd.openxmlformats-officedocument.spreadsheetml.pivotcachedefinition+xml
application/vnd.openxmlformats-officedocument.spreadsheetml.pivotcacherecords+xml
application/vnd.openxmlformats-officedocument.spreadsheetml.pivottable+xml
application/vnd.openxmlformats-officedocument.spreadsheetml.querytable+xml
application/vnd.openxmlformats-officedocument.spreadsheetml.revisionheaders+xml
application/vnd.openxmlformats-officedocument.spreadsheetml.revisionlog+xml
application/vnd.openxmlformats-officedocument.spreadsheetml.sharedstrings+xml
application/vnd.openxmlformats-officedocument.spreadsheetml.sheet	xlsx
application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml
application/vnd.openxmlformats-officedocument.spreadsheetml.sheetmetadata+xml
application/vnd.openxmlformats-officedocument.spreadsheetml.styles+xml
application/vnd.openxmlformats-officedocument.spreadsheetml.table+xml
application/vnd.openxmlformats-officedocument.spreadsheetml.tablesinglecells+xml
application/vnd.openxmlformats-officedocument.spreadsheetml.template	xltx
application/vnd.openxmlformats-officedocument.spreadsheetml.template.main+xml
application/vnd.openxmlformats-officedocument.spreadsheetml.usernames+xml
application/vnd.openxmlformats-officedocument.spreadsheetml.volatiledependencies+xml
application/vnd.openxmlformats-officedocument.spreadsheetml.worksheet+xml
application/vnd.openxmlformats-officedocument.theme+xml
application/vnd.openxmlformats-officedocument.themeoverride+xml
application/vnd.openxmlformats-officedocument.vmldrawing
application/vnd.openxmlformats-officedocument.wordprocessingml.comments+xml
application/vnd.openxmlformats-officedocument.wordprocessingml.document	docx
application/vnd.openxmlformats-officedocument.wordprocessingml.document.glossary+xml
application/vnd.openxmlformats-officedocument.wordprocessingml.document.main+xml
application/vnd.openxmlformats-officedocument.wordprocessingml.endnotes+xml
application/vnd.openxmlformats-officedocument.wordprocessingml.fonttable+xml
application/vnd.openxmlformats-officedocument.wordprocessingml.footer+xml
application/vnd.openxmlformats-officedocument.wordprocessingml.footnotes+xml
application/vnd.openxmlformats-officedocument.wordprocessingml.numbering+xml
application/vnd.openxmlformats-officedocument.wordprocessingml.settings+xml
application/vnd.openxmlformats-officedocument.wordprocessingml.styles+xml
application/vnd.openxmlformats-officedocument.wordprocessingml.template	dotx
application/vnd.openxmlformats-officedocument.wordprocessingml.template.main+xml
application/vnd.openxmlformats-officedocument.wordprocessingml.websettings+xml
application/vnd.openxmlformats-package.core-properties+xml
application/vnd.openxmlformats-package.digital-signature-xmlsignature+xml
application/vnd.openxmlformats-package.relationships+xml
application/vnd.osgeo.mapguide.package		mgp
application/vnd.osgi.dp				dp
application/vnd.osgi.subsystem			esa
application/vnd.palm				pdb pqa oprc
application/vnd.pawaafile			paw
application/vnd.pg.format			str
application/vnd.pg.osasli			ei6
application/vnd.picsel				efif
application/vnd.pmi.widget			wg
application/vnd.pocketlearn			plf
application/vnd.previewsystems.box		box
application/vnd.proteus.magazine		mgz
application/vnd.publishare-delta-tree		qps
application/vnd.pvi.ptid1			ptid
application/vnd.quark.quarkxpress		qxd qxt qwd qwt qxl qxb
application/vnd.realvnc.bed			bed
application/vnd.recordare.musicxml		mxl
application/vnd.recordare.musicxml+xml		musicxml
application/vnd.renlearn.rlprint
application/vnd.rig.cryptonote			cryptonote
application/vnd.rim.cod				cod
application/vnd.rn-realmedia			rm
application/vnd.rn-realmedia-vbr		rmvb
application/vnd.route66.link66+xml		link66
application/vnd.sailingtracker.track		st
application/vnd.seemail				see
application/vnd.sema				sema
application/vnd.semd				semd
application/vnd.semf				semf
application/vnd.shana.informed.formdata		ifm
application/vnd.shana.informed.formtemplate	itp
application/vnd.shana.informed.interchange	iif
application/vnd.shana.informed.package		ipk
application/vnd.simtech-mindmapper		twd twds
application/vnd.smaf				mmf
application/vnd.smart.notebook
application/vnd.smart.teacher			teacher
application/vnd.software602.filler.form+xml
application/vnd.software602.filler.form-xml-zip
application/vnd.solent.sdkm+xml			sdkm sdkd
application/vnd.spotfire.dxp			dxp
application/vnd.spotfire.sfs			sfs
application/vnd.sss-cod
application/vnd.sss-dtf
application/vnd.sss-ntf
application/vnd.stardivision.calc		sdc
application/vnd.stardivision.draw		sda
application/vnd.stardivision.impress		sdd
application/vnd.stardivision.math		smf
application/vnd.stardivision.writer		sdw vor
application/vnd.stardivision.writer-global	sgl
application/vnd.stepmania.package		smzip
application/vnd.stepmania.stepchart		sm
application/vnd.street-stream
application/vnd.sun.xml.calc			sxc
application/vnd.sun.xml.calc.template		stc
application/vnd.sun.xml.draw			sxd
application/vnd.sun.xml.draw.template		std
application/vnd.sun.xml.impress			sxi
application/vnd.sun.xml.impress.template	sti
application/vnd.sun.xml.math			sxm
application/vnd.sun.xml.writer			sxw
application/vnd.sun.xml.writer.global		sxg
application/vnd.sun.xml.writer.template		stw
application/vnd.sun.wadl+xml
application/vnd.sus-calendar			sus susp
application/vnd.svd				svd
application/vnd.swiftview-ics
application/vnd.symbian.install			sis sisx
application/vnd.syncml+xml			xsm
application/vnd.syncml.dm+wbxml			bdm
application/vnd.syncml.dm+xml			xdm
application/vnd.syncml.dm.notification
application/vnd.syncml.ds.notification
application/vnd.tao.intent-module-archive	tao
application/vnd.tcpdump.pcap			pcap cap dmp
application/vnd.tmobile-livetv			tmo
application/vnd.trid.tpt			tpt
application/vnd.triscape.mxs			mxs
application/vnd.trueapp				tra
application/vnd.truedoc
application/vnd.ubisoft.webplayer
application/vnd.ufdl				ufd ufdl
application/vnd.uiq.theme			utz
application/vnd.umajin				umj
application/vnd.unity				unityweb
application/vnd.uoml+xml			uoml
application/vnd.uplanet.alert
application/vnd.uplanet.alert-wbxml
application/vnd.uplanet.bearer-choice
application/vnd.uplanet.bearer-choice-wbxml
application/vnd.uplanet.cacheop
application/vnd.uplanet.cacheop-wbxml
application/vnd.uplanet.channel
application/vnd.uplanet.channel-wbxml
application/vnd.uplanet.list
application/vnd.uplanet.list-wbxml
application/vnd.uplanet.listcmd
application/vnd.uplanet.listcmd-wbxml
application/vnd.uplanet.signal
application/vnd.vcx				vcx
application/vnd.vd-study
application/vnd.vectorworks
application/vnd.verimatrix.vcas
application/vnd.vidsoft.vidconference
application/vnd.visio				vsd vst vss vsw
application/vnd.visionary			vis
application/vnd.vividence.scriptfile
application/vnd.vsf				vsf
application/vnd.wap.sic
application/vnd.wap.slc
application/vnd.wap.wbxml			wbxml
application/vnd.wap.wmlc			wmlc
application/vnd.wap.wmlscriptc			wmlsc
application/vnd.webturbo			wtb
application/vnd.wfa.wsc
application/vnd.wmc
application/vnd.wmf.bootstrap
application/vnd.wolfram.mathematica
application/vnd.wolfram.mathematica.package
application/vnd.wolfram.player			nbp
application/vnd.wordperfect			wpd
application/vnd.wqd				wqd
application/vnd.wrq-hp3000-labelled
application/vnd.wt.stf				stf
application/vnd.wv.csp+wbxml
application/vnd.wv.csp+xml
application/vnd.wv.ssp+xml
application/vnd.xara				xar
application/vnd.xfdl				xfdl
application/vnd.xfdl.webform
application/vnd.xmi+xml
application/vnd.xmpie.cpkg
application/vnd.xmpie.dpkg
application/vnd.xmpie.plan
application/vnd.xmpie.ppkg
application/vnd.xmpie.xlim
application/vnd.yamaha.hv-dic			hvd
application/vnd.yamaha.hv-script		hvs
application/vnd.yamaha.hv-voice			hvp
application/vnd.yamaha.openscoreformat			osf
application/vnd.yamaha.openscoreformat.osfpvg+xml	osfpvg
application/vnd.yamaha.remote-setup
application/vnd.yamaha.smaf-audio		saf
application/vnd.yamaha.smaf-phrase		spf
application/vnd.yamaha.through-ngn
application/vnd.yamaha.tunnel-udpencap
application/vnd.yellowriver-custom-menu		cmp
application/vnd.zul				zir zirz
application/vnd.zzazz.deck+xml			zaz
application/voicexml+xml			vxml
application/vq-rtcpxr
application/watcherinfo+xml
application/whoispp-query
application/whoispp-response
application/widget				wgt
application/winhlp				hlp
application/wita
application/wordperfect5.1
application/wsdl+xml				wsdl
application/wspolicy+xml			wspolicy
application/x-7z-compressed			7z
application/x-abiword				abw
application/x-ace-compressed			ace
application/x-amf
application/x-apple-diskimage			dmg
application/x-authorware-bin			aab x32 u32 vox
application/x-authorware-map			aam
application/x-authorware-seg			aas
application/x-bcpio				bcpio
application/x-bittorrent			torrent
application/x-blorb				blb blorb
application/x-bzip				bz
application/x-bzip2				bz2 boz
application/x-cbr				cbr cba cbt cbz cb7
application/x-cdlink				vcd
application/x-cfs-compressed			cfs
application/x-chat				chat
application/x-chess-pgn				pgn
application/x-conference			nsc
application/x-compress
application/x-cpio				cpio
application/x-csh				csh
application/x-debian-package			deb udeb
application/x-dgc-compressed			dgc
application/x-director			dir dcr dxr cst cct cxt w3d fgd swa
application/x-doom				wad
application/x-dtbncx+xml			ncx
application/x-dtbook+xml			dtb
application/x-dtbresource+xml			res
application/x-dvi				dvi
application/x-envoy				evy
application/x-eva				eva
application/x-font-bdf				bdf
application/x-font-dos
application/x-font-framemaker
application/x-font-ghostscript			gsf
application/x-font-libgrx
application/x-font-linux-psf			psf
application/x-font-otf				otf
application/x-font-pcf				pcf
application/x-font-snf				snf
application/x-font-speedo
application/x-font-sunos-news
application/x-font-ttf				ttf ttc
application/x-font-type1			pfa pfb pfm afm
application/font-woff				woff
application/x-font-vfont
application/x-freearc				arc
application/x-futuresplash			spl
application/x-gca-compressed			gca
application/x-glulx				ulx
application/x-gnumeric				gnumeric
application/x-gramps-xml			gramps
application/x-gtar				gtar
application/x-gzip
application/x-hdf				hdf
application/x-install-instructions		install
application/x-iso9660-image			iso
application/x-java-jnlp-file			jnlp
application/x-latex				latex
application/x-lzh-compressed			lzh lha
application/x-mie				mie
application/x-mobipocket-ebook			prc mobi
application/x-ms-application			application
application/x-ms-shortcut			lnk
application/x-ms-wmd				wmd
application/x-ms-wmz				wmz
application/x-ms-xbap				xbap
application/x-msaccess				mdb
application/x-msbinder				obd
application/x-mscardfile			crd
application/x-msclip				clp
application/x-msdownload			exe dll com bat msi
application/x-msmediaview			mvb m13 m14
application/x-msmetafile			wmf wmz emf emz
application/x-msmoney				mny
application/x-mspublisher			pub
application/x-msschedule			scd
application/x-msterminal			trm
application/x-mswrite				wri
application/x-netcdf				nc cdf
application/x-nzb				nzb
application/x-pkcs12				p12 pfx
application/x-pkcs7-certificates		p7b spc
application/x-pkcs7-certreqresp			p7r
application/x-rar-compressed			rar
application/x-research-info-systems		ris
application/x-sh				sh
application/x-shar				shar
application/x-shockwave-flash			swf
application/x-silverlight-app			xap
application/x-sql				sql
application/x-stuffit				sit
application/x-stuffitx				sitx
application/x-subrip				srt
application/x-sv4cpio				sv4cpio
application/x-sv4crc				sv4crc
application/x-t3vm-image			t3
application/x-tads				gam
application/x-tar				tar
application/x-tcl				tcl
application/x-tex				tex
application/x-tex-tfm				tfm
application/x-texinfo				texinfo texi
application/x-tgif				obj
application/x-ustar				ustar
application/x-wais-source			src
application/x-x509-ca-cert			der crt
application/x-xfig				fig
application/x-xliff+xml				xlf
application/x-xpinstall				xpi
application/x-xz				xz
application/x-zmachine				z1 z2 z3 z4 z5 z6 z7 z8
application/x400-bp
application/xaml+xml				xaml
application/xcap-att+xml
application/xcap-caps+xml
application/xcap-diff+xml			xdf
application/xcap-el+xml
application/xcap-error+xml
application/xcap-ns+xml
application/xcon-conference-info-diff+xml
application/xcon-conference-info+xml
application/xenc+xml				xenc
application/xhtml+xml				xhtml xht
application/xhtml-voice+xml
application/xml					xml xsl
application/xml-dtd				dtd
application/xml-external-parsed-entity
application/xmpp+xml
application/xop+xml				xop
application/xproc+xml				xpl
application/xslt+xml				xslt
application/xspf+xml				xspf
application/xv+xml				mxml xhvml xvml xvm
application/yang				yang
application/yin+xml				yin
    ZIP ("application/zip", "Zip", "zip");
