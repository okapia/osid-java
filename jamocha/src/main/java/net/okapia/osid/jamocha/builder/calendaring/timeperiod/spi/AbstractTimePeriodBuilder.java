//
// AbstractTimePeriod.java
//
//     Defines a TimePeriod builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.calendaring.timeperiod.spi;


/**
 *  Defines a <code>TimePeriod</code> builder.
 */

public abstract class AbstractTimePeriodBuilder<T extends AbstractTimePeriodBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.calendaring.timeperiod.TimePeriodMiter timePeriod;


    /**
     *  Constructs a new <code>AbstractTimePeriodBuilder</code>.
     *
     *  @param timePeriod the time period to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractTimePeriodBuilder(net.okapia.osid.jamocha.builder.calendaring.timeperiod.TimePeriodMiter timePeriod) {
        super(timePeriod);
        this.timePeriod = timePeriod;
        return;
    }


    /**
     *  Builds the time period.
     *
     *  @return the new time period
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.calendaring.TimePeriod build() {
        (new net.okapia.osid.jamocha.builder.validator.calendaring.timeperiod.TimePeriodValidator(getValidations())).validate(this.timePeriod);
        return (new net.okapia.osid.jamocha.builder.calendaring.timeperiod.ImmutableTimePeriod(this.timePeriod));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the time period miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.calendaring.timeperiod.TimePeriodMiter getMiter() {
        return (this.timePeriod);
    }


    /**
     *  Sets the start.
     *
     *  @param start a start
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>start</code> is
     *          <code>null</code>
     */

    public T start(org.osid.calendaring.DateTime start) {
        getMiter().setStart(start);
        return (self());
    }


    /**
     *  Sets the end.
     *
     *  @param end an end
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>end</code> is
     *          <code>null</code>
     */

    public T end(org.osid.calendaring.DateTime end) {
        getMiter().setEnd(end);
        return (self());
    }


    /**
     *  Adds an exception.
     *
     *  @param exception an exception
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>exception</code> is <code>null</code>
     */

    public T exception(org.osid.calendaring.Event exception) {
        getMiter().addException(exception);
        return (self());
    }


    /**
     *  Sets all the exceptions.
     *
     *  @param exceptions a collection of exceptions
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>exceptions</code> is <code>null</code>
     */

    public T exceptions(java.util.Collection<org.osid.calendaring.Event> exceptions) {
        getMiter().setExceptions(exceptions);
        return (self());
    }


    /**
     *  Adds a TimePeriod record.
     *
     *  @param record a time period record
     *  @param recordType the type of time period record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.calendaring.records.TimePeriodRecord record, org.osid.type.Type recordType) {
        getMiter().addTimePeriodRecord(record, recordType);
        return (self());
    }
}       


