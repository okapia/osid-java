//
// AbstractAdapterOsidRule.java
//
//     Defines an OsidRule wrapper.
//
//
//
// Tom Coppeto
// Okapia
// 20 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a generic <code>OsidRule</code> wrapper.
 */

public abstract class AbstractAdapterOsidRule
    extends AbstractAdapterOperableOsidObject
    implements org.osid.OsidRule {

    private final org.osid.OsidRule rule;


    /**
     *  Constructs a new <code>AbstractAdapterOsidRule</code>.
     *
     *  @param rule the underlying rule
     *  @throws org.osid.NullArgumentException
     *          <code>rule</code> is <code>null</code>
     */

    protected AbstractAdapterOsidRule(org.osid.OsidRule rule) {
        super(rule);
        this.rule = rule;
        return;
    }


    /**
     *  Tests if an explicit rule is available.
     *
     *  @return <code> true </code> if an explicit rule is available,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean hasRule() {
        return (this.rule.hasRule());
    }


    /**
     *  Gets the rule <code> Id </code>.
     *
     *  @return the rule <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasRule() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getRuleId() {
        return (this.rule.getRuleId());
    }


    /**
     *  Gets the rule.
     *
     *  @return the rule 
     *  @throws org.osid.IllegalStateException <code> hasRule() </code> 
     *          is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.rules.Rule getRule()
        throws org.osid.OperationFailedException {

        return (this.rule.getRule());
    }
}
