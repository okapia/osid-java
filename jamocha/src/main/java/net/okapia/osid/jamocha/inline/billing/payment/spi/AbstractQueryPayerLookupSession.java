//
// AbstractQueryPayerLookupSession.java
//
//    An inline adapter that maps a PayerLookupSession to
//    a PayerQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.billing.payment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a PayerLookupSession to
 *  a PayerQuerySession.
 */

public abstract class AbstractQueryPayerLookupSession
    extends net.okapia.osid.jamocha.billing.payment.spi.AbstractPayerLookupSession
    implements org.osid.billing.payment.PayerLookupSession {

    private boolean effectiveonly = false;
    private final org.osid.billing.payment.PayerQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryPayerLookupSession.
     *
     *  @param querySession the underlying payer query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryPayerLookupSession(org.osid.billing.payment.PayerQuerySession querySession) {
        nullarg(querySession, "payer query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Business</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Business Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBusinessId() {
        return (this.session.getBusinessId());
    }


    /**
     *  Gets the <code>Business</code> associated with this 
     *  session.
     *
     *  @return the <code>Business</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.Business getBusiness()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getBusiness());
    }


    /**
     *  Tests if this user can perform <code>Payer</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupPayers() {
        return (this.session.canSearchPayers());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include payers in businesses which are children
     *  of this business in the business hierarchy.
     */

    @OSID @Override
    public void useFederatedBusinessView() {
        this.session.useFederatedBusinessView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this business only.
     */

    @OSID @Override
    public void useIsolatedBusinessView() {
        this.session.useIsolatedBusinessView();
        return;
    }
    

    /**
     *  Only payers whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectivePayerView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All payers of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectivePayerView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Payer</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Payer</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Payer</code> and
     *  retained for compatibility.
     *
     *  In effective mode, payers are returned that are currently
     *  effective.  In any effective mode, effective payers and
     *  those currently expired are returned.
     *
     *  @param  payerId <code>Id</code> of the
     *          <code>Payer</code>
     *  @return the payer
     *  @throws org.osid.NotFoundException <code>payerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>payerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.payment.Payer getPayer(org.osid.id.Id payerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.payment.PayerQuery query = getQuery();
        query.matchId(payerId, true);
        org.osid.billing.payment.PayerList payers = this.session.getPayersByQuery(query);
        if (payers.hasNext()) {
            return (payers.getNextPayer());
        } 
        
        throw new org.osid.NotFoundException(payerId + " not found");
    }


    /**
     *  Gets a <code>PayerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  payers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Payers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, payers are returned that are currently effective.
     *  In any effective mode, effective payers and those currently expired
     *  are returned.
     *
     *  @param  payerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Payer</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>payerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerList getPayersByIds(org.osid.id.IdList payerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.payment.PayerQuery query = getQuery();

        try (org.osid.id.IdList ids = payerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getPayersByQuery(query));
    }


    /**
     *  Gets a <code>PayerList</code> corresponding to the given
     *  payer genus <code>Type</code> which does not include
     *  payers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  payers or an error results. Otherwise, the returned list
     *  may contain only those payers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, payers are returned that are currently effective.
     *  In any effective mode, effective payers and those currently expired
     *  are returned.
     *
     *  @param  payerGenusType a payer genus type 
     *  @return the returned <code>Payer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>payerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerList getPayersByGenusType(org.osid.type.Type payerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.payment.PayerQuery query = getQuery();
        query.matchGenusType(payerGenusType, true);
        return (this.session.getPayersByQuery(query));
    }


    /**
     *  Gets a <code>PayerList</code> corresponding to the given
     *  payer genus <code>Type</code> and include any additional
     *  payers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  payers or an error results. Otherwise, the returned list
     *  may contain only those payers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, payers are returned that are currently
     *  effective.  In any effective mode, effective payers and
     *  those currently expired are returned.
     *
     *  @param  payerGenusType a payer genus type 
     *  @return the returned <code>Payer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>payerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerList getPayersByParentGenusType(org.osid.type.Type payerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.payment.PayerQuery query = getQuery();
        query.matchParentGenusType(payerGenusType, true);
        return (this.session.getPayersByQuery(query));
    }


    /**
     *  Gets a <code>PayerList</code> containing the given
     *  payer record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  payers or an error results. Otherwise, the returned list
     *  may contain only those payers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, payers are returned that are currently
     *  effective.  In any effective mode, effective payers and
     *  those currently expired are returned.
     *
     *  @param  payerRecordType a payer record type 
     *  @return the returned <code>Payer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>payerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerList getPayersByRecordType(org.osid.type.Type payerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.payment.PayerQuery query = getQuery();
        query.matchRecordType(payerRecordType, true);
        return (this.session.getPayersByQuery(query));
    }


    /**
     *  Gets a <code>PayerList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  payers or an error results. Otherwise, the returned list
     *  may contain only those payers that are accessible
     *  through this session.
     *  
     *  In effective mode, payers are returned that are currently
     *  effective.  In any effective mode, effective payers and
     *  those currently expired are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Payer</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.billing.payment.PayerList getPayersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.payment.PayerQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getPayersByQuery(query));
    }
        

    /**
     *  Gets a <code> PayerList </code> related to the given resource.
     *  
     *  In plenary mode, the returned list contains all known payers
     *  or an error results. Otherwise, the returned list may contain
     *  only those payers that are accessible through this session.
     *  
     *  In effective mode, payers are returned that are currently
     *  effective.  In any effective mode, effective payers and those
     *  currently expired are returned.
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @return the returned <code> PayerList </code> list 
     *  @throws org.osid.NullArgumentException <code> resourceId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerList getPayersByResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.payment.PayerQuery query = getQuery();
        query.matchResourceId(resourceId, true);
        return (this.session.getPayersByQuery(query));        
    }


    /**
     *  Gets a <code> PayerList </code> of the given resource and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known payers
     *  or an error results. Otherwise, the returned list may contain
     *  only those payers that are accessible through this session.
     *  
     *  In effective mode, payers are returned that are currently
     *  effective in addition to being effective during the given date
     *  range. In any effective mode, effective payers and those
     *  currently expired are returned.
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned <code> PayerList </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> resourceId, from, 
     *          </code> or <code> to </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerList getPayersByResourceOnDate(org.osid.id.Id resourceId, 
                                                                        org.osid.calendaring.DateTime from, 
                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.payment.PayerQuery query = getQuery();
        query.matchResourceId(resourceId, true);
        query.matchDate(from, to, true);
        return (this.session.getPayersByQuery(query));        
    }


    /**
     *  Gets a <code> PayerList </code> related to the given payer
     *  customer.
     *  
     *  In plenary mode, the returned list contains all known payers
     *  or an error results. Otherwise, the returned list may contain
     *  only those payers that are accessible through this session.
     *  
     *  In effective mode, payers are returned that are currently
     *  effective.  In any effective mode, effective payers and those
     *  currently expired are returned.
     *
     *  @param  customerId a customer <code> Id </code> 
     *  @return the returned <code> PayerList </code> list 
     *  @throws org.osid.NullArgumentException <code> customerId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerList getPayersByCustomer(org.osid.id.Id customerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.payment.PayerQuery query = getQuery();
        query.matchCustomerId(customerId, true);
        return (this.session.getPayersByQuery(query));        
    }


    /**
     *  Gets a <code> PayerList </code> of the given customer and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known payers
     *  or an error results. Otherwise, the returned list may contain
     *  only those payers that are accessible through this session.
     *  
     *  In effective mode, payers are returned that are currently
     *  effective in addition to being effective during the given date
     *  range. In any effective mode, effective payers and those
     *  currently expired are returned.
     *
     *  @param  customerId a customer <code> Id </code> 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned <code> PayerList </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> customerId, from, 
     *          </code> or <code> to </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerList getPayersByCustomerOnDate(org.osid.id.Id customerId, 
                                                                        org.osid.calendaring.DateTime from, 
                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.payment.PayerQuery query = getQuery();
        query.matchCustomerId(customerId, true);
        query.matchDate(from, to, true);
        return (this.session.getPayersByQuery(query));
    }


    /**
     *  Gets all <code>Payers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  payers or an error results. Otherwise, the returned list
     *  may contain only those payers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, payers are returned that are currently
     *  effective.  In any effective mode, effective payers and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Payers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerList getPayers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.payment.PayerQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getPayersByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.billing.payment.PayerQuery getQuery() {
        org.osid.billing.payment.PayerQuery query = this.session.getPayerQuery();
        
        if (isEffectiveOnly()) {
            query.matchEffective(true);
        }

        return (query);
    }
}
