//
// AbstractAdapterInventoryLookupSession.java
//
//    An Inventory lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.inventory.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An Inventory lookup session adapter.
 */

public abstract class AbstractAdapterInventoryLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.inventory.InventoryLookupSession {

    private final org.osid.inventory.InventoryLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterInventoryLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterInventoryLookupSession(org.osid.inventory.InventoryLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Warehouse/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Warehouse Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getWarehouseId() {
        return (this.session.getWarehouseId());
    }


    /**
     *  Gets the {@code Warehouse} associated with this session.
     *
     *  @return the {@code Warehouse} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.Warehouse getWarehouse()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getWarehouse());
    }


    /**
     *  Tests if this user can perform {@code Inventory} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupInventories() {
        return (this.session.canLookupInventories());
    }


    /**
     *  A complete view of the {@code Inventory} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeInventoryView() {
        this.session.useComparativeInventoryView();
        return;
    }


    /**
     *  A complete view of the {@code Inventory} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryInventoryView() {
        this.session.usePlenaryInventoryView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include inventories in warehouses which are children
     *  of this warehouse in the warehouse hierarchy.
     */

    @OSID @Override
    public void useFederatedWarehouseView() {
        this.session.useFederatedWarehouseView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this warehouse only.
     */

    @OSID @Override
    public void useIsolatedWarehouseView() {
        this.session.useIsolatedWarehouseView();
        return;
    }
    
     
    /**
     *  Gets the {@code Inventory} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Inventory} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Inventory} and
     *  retained for compatibility.
     *
     *  @param inventoryId {@code Id} of the {@code Inventory}
     *  @return the inventory
     *  @throws org.osid.NotFoundException {@code inventoryId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code inventoryId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.Inventory getInventory(org.osid.id.Id inventoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getInventory(inventoryId));
    }


    /**
     *  Gets an {@code InventoryList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  inventories specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Inventories} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  inventoryIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Inventory} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code inventoryIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.InventoryList getInventoriesByIds(org.osid.id.IdList inventoryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getInventoriesByIds(inventoryIds));
    }


    /**
     *  Gets an {@code InventoryList} corresponding to the given
     *  inventory genus {@code Type} which does not include
     *  inventories of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  inventories or an error results. Otherwise, the returned list
     *  may contain only those inventories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  inventoryGenusType an inventory genus type 
     *  @return the returned {@code Inventory} list
     *  @throws org.osid.NullArgumentException
     *          {@code inventoryGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.InventoryList getInventoriesByGenusType(org.osid.type.Type inventoryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getInventoriesByGenusType(inventoryGenusType));
    }


    /**
     *  Gets an {@code InventoryList} corresponding to the given
     *  inventory genus {@code Type} and include any additional
     *  inventories with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  inventories or an error results. Otherwise, the returned list
     *  may contain only those inventories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  inventoryGenusType an inventory genus type 
     *  @return the returned {@code Inventory} list
     *  @throws org.osid.NullArgumentException
     *          {@code inventoryGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.InventoryList getInventoriesByParentGenusType(org.osid.type.Type inventoryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getInventoriesByParentGenusType(inventoryGenusType));
    }


    /**
     *  Gets an {@code InventoryList} containing the given
     *  inventory record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  inventories or an error results. Otherwise, the returned list
     *  may contain only those inventories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  inventoryRecordType an inventory record type 
     *  @return the returned {@code Inventory} list
     *  @throws org.osid.NullArgumentException
     *          {@code inventoryRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.InventoryList getInventoriesByRecordType(org.osid.type.Type inventoryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getInventoriesByRecordType(inventoryRecordType));
    }


    /**
     *  Gets the most recent {@code Inventories.} In plenary mode, the
     *  returned list contains all known inventories or an error
     *  results.  Otherwise, the returned list may contain only those
     *  inventories that are accessible through this session.
     *
     *  @return the returned {@code Inventory} list 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inventory.InventoryList getRecentInventories()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRecentInventories());
    }


    /**
     *  Gets {@code Inventories} between the given date range
     *  inclusive. In plenary mode, the returned list contains all
     *  known inventories or an error results. Otherwise, the returned
     *  list may contain only those inventories that are accessible
     *  through this session.
     *
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned {@code Inventory} list 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code from} or {@code 
     *          to} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inventory.InventoryList getInventoriesByDate(org.osid.calendaring.DateTime from, 
                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getInventoriesByDate(from, to));
    }


    /**
     *  Gets all {@code Inventories} for the given {@code Stock.  } In
     *  plenary mode, the returned list contains all known inventories
     *  or an error results. Otherwise, the returned list may contain
     *  only those inventories that are accessible through this
     *  session.
     *
     *  @param  stockId a stock {@code Id} 
     *  @return the returned {@code Inventory} list 
     *  @throws org.osid.NullArgumentException {@code stockId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inventory.InventoryList getInventoriesForStock(org.osid.id.Id stockId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getInventoriesForStock(stockId));
    }


    /**
     *  Gets the most recent {@code Inventories} for with the given
     *  {@code Stock.} In plenary mode, the returned list contains all
     *  known inventories or an error results. Otherwise, the returned
     *  list may contain only those inventories that are accessible
     *  through this session.
     *
     *  @param  stockId an inventory {@code Id} 
     *  @return the returned {@code Inventory} list 
     *  @throws org.osid.NullArgumentException {@code stockId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inventory.InventoryList getRecentInventoriesForStock(org.osid.id.Id stockId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRecentInventoriesForStock(stockId));
    }


    /**
     *  Gets {@code Inventories} between the given date range
     *  inclusive for the given stock. In plenary mode, the returned
     *  list contains all known inventories or an error
     *  results. Otherwise, the returned list may contain only those
     *  inventories that are accessible through this session.
     *
     *  @param  stockId an inventory {@code Id} 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned {@code Inventory} list 
     *  @throws org.osid.InvalidArgumentException {@code from} is
     *          greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code stockid, from}
     *          or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
    
    @OSID @Override
    public org.osid.inventory.InventoryList getInventoriesByDateForStock(org.osid.id.Id stockId, 
                                                                         org.osid.calendaring.DateTime from, 
                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getInventoriesByDateForStock(stockId, from, to));
    }


    /**
     *  Gets all {@code Inventories}. 
     *
     *  In plenary mode, the returned list contains all known
     *  inventories or an error results. Otherwise, the returned list
     *  may contain only those inventories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Inventories} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.InventoryList getInventories()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getInventories());
    }
}
