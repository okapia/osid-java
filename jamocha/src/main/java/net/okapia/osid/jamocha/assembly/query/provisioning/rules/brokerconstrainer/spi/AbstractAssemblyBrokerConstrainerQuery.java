//
// AbstractAssemblyBrokerConstrainerQuery.java
//
//     A BrokerConstrainerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.provisioning.rules.brokerconstrainer.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A BrokerConstrainerQuery that stores terms.
 */

public abstract class AbstractAssemblyBrokerConstrainerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidConstrainerQuery
    implements org.osid.provisioning.rules.BrokerConstrainerQuery,
               org.osid.provisioning.rules.BrokerConstrainerQueryInspector,
               org.osid.provisioning.rules.BrokerConstrainerSearchOrder {

    private final java.util.Collection<org.osid.provisioning.rules.records.BrokerConstrainerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.provisioning.rules.records.BrokerConstrainerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.provisioning.rules.records.BrokerConstrainerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyBrokerConstrainerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyBrokerConstrainerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches mapped to a broker. 
     *
     *  @param  distributorId the broker <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledBrokerId(org.osid.id.Id distributorId, boolean match) {
        getAssembler().addIdTerm(getRuledBrokerIdColumn(), distributorId, match);
        return;
    }


    /**
     *  Clears the broker <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledBrokerIdTerms() {
        getAssembler().clearTerms(getRuledBrokerIdColumn());
        return;
    }


    /**
     *  Gets the broker <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledBrokerIdTerms() {
        return (getAssembler().getIdTerms(getRuledBrokerIdColumn()));
    }


    /**
     *  Gets the RuledBrokerId column name.
     *
     * @return the column name
     */

    protected String getRuledBrokerIdColumn() {
        return ("ruled_broker_id");
    }


    /**
     *  Tests if a <code> BrokerQuery </code> is available. 
     *
     *  @return <code> true </code> if a broker query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledBrokerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a broker. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the broker query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledBrokerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerQuery getRuledBrokerQuery() {
        throw new org.osid.UnimplementedException("supportsRuledBrokerQuery() is false");
    }


    /**
     *  Matches mapped to any broker. 
     *
     *  @param  match <code> true </code> for mapped to any broker, <code> 
     *          false </code> to match mapped to no brokers 
     */

    @OSID @Override
    public void matchAnyRuledBroker(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledBrokerColumn(), match);
        return;
    }


    /**
     *  Clears the broker query terms. 
     */

    @OSID @Override
    public void clearRuledBrokerTerms() {
        getAssembler().clearTerms(getRuledBrokerColumn());
        return;
    }


    /**
     *  Gets the broker query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerQueryInspector[] getRuledBrokerTerms() {
        return (new org.osid.provisioning.BrokerQueryInspector[0]);
    }


    /**
     *  Gets the RuledBroker column name.
     *
     * @return the column name
     */

    protected String getRuledBrokerColumn() {
        return ("ruled_broker");
    }


    /**
     *  Matches mapped to the distributor. 
     *
     *  @param  distributorId the distributor <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDistributorId(org.osid.id.Id distributorId, boolean match) {
        getAssembler().addIdTerm(getDistributorIdColumn(), distributorId, match);
        return;
    }


    /**
     *  Clears the distributor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDistributorIdTerms() {
        getAssembler().clearTerms(getDistributorIdColumn());
        return;
    }


    /**
     *  Gets the distributor <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDistributorIdTerms() {
        return (getAssembler().getIdTerms(getDistributorIdColumn()));
    }


    /**
     *  Gets the DistributorId column name.
     *
     * @return the column name
     */

    protected String getDistributorIdColumn() {
        return ("distributor_id");
    }


    /**
     *  Tests if a <code> DistributorQuery </code> is available. 
     *
     *  @return <code> true </code> if a distributor query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDistributorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a distributor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the distributor query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDistributorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQuery getDistributorQuery() {
        throw new org.osid.UnimplementedException("supportsDistributorQuery() is false");
    }


    /**
     *  Clears the distributor query terms. 
     */

    @OSID @Override
    public void clearDistributorTerms() {
        getAssembler().clearTerms(getDistributorColumn());
        return;
    }


    /**
     *  Gets the distributor query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQueryInspector[] getDistributorTerms() {
        return (new org.osid.provisioning.DistributorQueryInspector[0]);
    }


    /**
     *  Gets the Distributor column name.
     *
     * @return the column name
     */

    protected String getDistributorColumn() {
        return ("distributor");
    }


    /**
     *  Tests if this brokerConstrainer supports the given record
     *  <code>Type</code>.
     *
     *  @param  brokerConstrainerRecordType a broker constrainer record type 
     *  @return <code>true</code> if the brokerConstrainerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>brokerConstrainerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type brokerConstrainerRecordType) {
        for (org.osid.provisioning.rules.records.BrokerConstrainerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(brokerConstrainerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  brokerConstrainerRecordType the broker constrainer record type 
     *  @return the broker constrainer query record 
     *  @throws org.osid.NullArgumentException
     *          <code>brokerConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(brokerConstrainerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.BrokerConstrainerQueryRecord getBrokerConstrainerQueryRecord(org.osid.type.Type brokerConstrainerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.rules.records.BrokerConstrainerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(brokerConstrainerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(brokerConstrainerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  brokerConstrainerRecordType the broker constrainer record type 
     *  @return the broker constrainer query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>brokerConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(brokerConstrainerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.BrokerConstrainerQueryInspectorRecord getBrokerConstrainerQueryInspectorRecord(org.osid.type.Type brokerConstrainerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.rules.records.BrokerConstrainerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(brokerConstrainerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(brokerConstrainerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param brokerConstrainerRecordType the broker constrainer record type
     *  @return the broker constrainer search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>brokerConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(brokerConstrainerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.BrokerConstrainerSearchOrderRecord getBrokerConstrainerSearchOrderRecord(org.osid.type.Type brokerConstrainerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.rules.records.BrokerConstrainerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(brokerConstrainerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(brokerConstrainerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this broker constrainer. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param brokerConstrainerQueryRecord the broker constrainer query record
     *  @param brokerConstrainerQueryInspectorRecord the broker constrainer query inspector
     *         record
     *  @param brokerConstrainerSearchOrderRecord the broker constrainer search order record
     *  @param brokerConstrainerRecordType broker constrainer record type
     *  @throws org.osid.NullArgumentException
     *          <code>brokerConstrainerQueryRecord</code>,
     *          <code>brokerConstrainerQueryInspectorRecord</code>,
     *          <code>brokerConstrainerSearchOrderRecord</code> or
     *          <code>brokerConstrainerRecordTypebrokerConstrainer</code> is
     *          <code>null</code>
     */
            
    protected void addBrokerConstrainerRecords(org.osid.provisioning.rules.records.BrokerConstrainerQueryRecord brokerConstrainerQueryRecord, 
                                      org.osid.provisioning.rules.records.BrokerConstrainerQueryInspectorRecord brokerConstrainerQueryInspectorRecord, 
                                      org.osid.provisioning.rules.records.BrokerConstrainerSearchOrderRecord brokerConstrainerSearchOrderRecord, 
                                      org.osid.type.Type brokerConstrainerRecordType) {

        addRecordType(brokerConstrainerRecordType);

        nullarg(brokerConstrainerQueryRecord, "broker constrainer query record");
        nullarg(brokerConstrainerQueryInspectorRecord, "broker constrainer query inspector record");
        nullarg(brokerConstrainerSearchOrderRecord, "broker constrainer search odrer record");

        this.queryRecords.add(brokerConstrainerQueryRecord);
        this.queryInspectorRecords.add(brokerConstrainerQueryInspectorRecord);
        this.searchOrderRecords.add(brokerConstrainerSearchOrderRecord);
        
        return;
    }
}
