//
// AbstractResourceSearch.java
//
//     A template for making a Resource Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resource.resource.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing resource searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractResourceSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.resource.ResourceSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.resource.records.ResourceSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.resource.ResourceSearchOrder resourceSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of resources. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  resourceIds list of resources
     *  @throws org.osid.NullArgumentException
     *          <code>resourceIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongResources(org.osid.id.IdList resourceIds) {
        while (resourceIds.hasNext()) {
            try {
                this.ids.add(resourceIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongResources</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of resource Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getResourceIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  resourceSearchOrder resource search order 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>resourceSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderResourceResults(org.osid.resource.ResourceSearchOrder resourceSearchOrder) {
	this.resourceSearchOrder = resourceSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.resource.ResourceSearchOrder getResourceSearchOrder() {
	return (this.resourceSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given resource search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a resource implementing the requested record.
     *
     *  @param resourceSearchRecordType a resource search record
     *         type
     *  @return the resource search record
     *  @throws org.osid.NullArgumentException
     *          <code>resourceSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(resourceSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.resource.records.ResourceSearchRecord getResourceSearchRecord(org.osid.type.Type resourceSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.resource.records.ResourceSearchRecord record : this.records) {
            if (record.implementsRecordType(resourceSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(resourceSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this resource search. 
     *
     *  @param resourceSearchRecord resource search record
     *  @param resourceSearchRecordType resource search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addResourceSearchRecord(org.osid.resource.records.ResourceSearchRecord resourceSearchRecord, 
                                           org.osid.type.Type resourceSearchRecordType) {

        addRecordType(resourceSearchRecordType);
        this.records.add(resourceSearchRecord);        
        return;
    }
}
