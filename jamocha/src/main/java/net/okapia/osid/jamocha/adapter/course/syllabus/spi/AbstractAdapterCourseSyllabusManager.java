//
// AbstractCourseSyllabusManager.java
//
//     An adapter for a CourseSyllabusManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.course.syllabus.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a CourseSyllabusManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterCourseSyllabusManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.course.syllabus.CourseSyllabusManager>
    implements org.osid.course.syllabus.CourseSyllabusManager {


    /**
     *  Constructs a new {@code AbstractAdapterCourseSyllabusManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterCourseSyllabusManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterCourseSyllabusManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterCourseSyllabusManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any docet federation is exposed. Federation is exposed when a 
     *  specific docet may be identified, selected and used to create a lookup 
     *  or admin session. Federation is not exposed when a set of 
     *  docetsappears as a single docet. 
     *
     *  @return <code> true </code> if visible federation is supproted, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests for the availability of a syllabus lookup service. 
     *
     *  @return <code> true </code> if syllabus lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSyllabusLookup() {
        return (getAdapteeManager().supportsSyllabusLookup());
    }


    /**
     *  Tests if querying syllabi is available. 
     *
     *  @return <code> true </code> if syllabus query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSyllabusQuery() {
        return (getAdapteeManager().supportsSyllabusQuery());
    }


    /**
     *  Tests if searching for syllabi is available. 
     *
     *  @return <code> true </code> if syllabus search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSyllabusSearch() {
        return (getAdapteeManager().supportsSyllabusSearch());
    }


    /**
     *  Tests if searching for syllabi is available. 
     *
     *  @return <code> true </code> if syllabus search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSyllabusAdmin() {
        return (getAdapteeManager().supportsSyllabusAdmin());
    }


    /**
     *  Tests if syllabus notification is available. 
     *
     *  @return <code> true </code> if syllabus notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSyllabusNotification() {
        return (getAdapteeManager().supportsSyllabusNotification());
    }


    /**
     *  Tests if a syllabus to course catalog lookup session is available. 
     *
     *  @return <code> true </code> if syllabus course catalog lookup session 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSyllabusCourseCatalog() {
        return (getAdapteeManager().supportsSyllabusCourseCatalog());
    }


    /**
     *  Tests if a syllabus to course catalog assignment session is available. 
     *
     *  @return <code> true </code> if syllabus course catalog assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSyllabusCourseCatalogAssignment() {
        return (getAdapteeManager().supportsSyllabusCourseCatalogAssignment());
    }


    /**
     *  Tests if a syllabus smart course catalog session is available. 
     *
     *  @return <code> true </code> if syllabus smart course catalog is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSyllabusSmartCourseCatalog() {
        return (getAdapteeManager().supportsSyllabusSmartCourseCatalog());
    }


    /**
     *  Tests for the availability of a module service for getting available 
     *  modules for a resource. 
     *
     *  @return <code> true </code> if module is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModule() {
        return (getAdapteeManager().supportsModule());
    }


    /**
     *  Tests for the availability of a module lookup service. 
     *
     *  @return <code> true </code> if module lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModuleLookup() {
        return (getAdapteeManager().supportsModuleLookup());
    }


    /**
     *  Tests if querying modules is available. 
     *
     *  @return <code> true </code> if module query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModuleQuery() {
        return (getAdapteeManager().supportsModuleQuery());
    }


    /**
     *  Tests if searching for modules is available. 
     *
     *  @return <code> true </code> if module search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModuleSearch() {
        return (getAdapteeManager().supportsModuleSearch());
    }


    /**
     *  Tests if searching for modules is available. 
     *
     *  @return <code> true </code> if module search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModuleAdmin() {
        return (getAdapteeManager().supportsModuleAdmin());
    }


    /**
     *  Tests if module notification is available. 
     *
     *  @return <code> true </code> if module notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModuleNotification() {
        return (getAdapteeManager().supportsModuleNotification());
    }


    /**
     *  Tests if a module to course catalog lookup session is available. 
     *
     *  @return <code> true </code> if module course catalog lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModuleCourseCatalog() {
        return (getAdapteeManager().supportsModuleCourseCatalog());
    }


    /**
     *  Tests if a module to course catalog assignment session is available. 
     *
     *  @return <code> true </code> if module course catalog assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModuleCourseCatalogAssignment() {
        return (getAdapteeManager().supportsModuleCourseCatalogAssignment());
    }


    /**
     *  Tests if a module smart course catalog session is available. 
     *
     *  @return <code> true </code> if module smart course catalog is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModuleSmartCourseCatalog() {
        return (getAdapteeManager().supportsModuleSmartCourseCatalog());
    }


    /**
     *  Tests for the availability of a docet lookup service. 
     *
     *  @return <code> true </code> if docet lookup is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDocetLookup() {
        return (getAdapteeManager().supportsDocetLookup());
    }


    /**
     *  Tests if querying docetsis available. 
     *
     *  @return <code> true </code> if docet query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDocetQuery() {
        return (getAdapteeManager().supportsDocetQuery());
    }


    /**
     *  Tests if searching for docets is available. 
     *
     *  @return <code> true </code> if docet search is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDocetSearch() {
        return (getAdapteeManager().supportsDocetSearch());
    }


    /**
     *  Tests for the availability of a docet administrative service for 
     *  creating and deleting docets. 
     *
     *  @return <code> true </code> if docet administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDocetAdmin() {
        return (getAdapteeManager().supportsDocetAdmin());
    }


    /**
     *  Tests for the availability of a docet notification service. 
     *
     *  @return <code> true </code> if docet notification is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDocetNotification() {
        return (getAdapteeManager().supportsDocetNotification());
    }


    /**
     *  Tests if a docet to course catalog lookup session is available. 
     *
     *  @return <code> true </code> if docet course catalog lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDocetCourseCatalog() {
        return (getAdapteeManager().supportsDocetCourseCatalog());
    }


    /**
     *  Tests if a docet to course catalog assignment session is available. 
     *
     *  @return <code> true </code> if docet course catalog assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDocetCourseCatalogAssignment() {
        return (getAdapteeManager().supportsDocetCourseCatalogAssignment());
    }


    /**
     *  Tests if a docet smart course catalog session is available. 
     *
     *  @return <code> true </code> if docet smart course catalog is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDocetSmartCourseCatalog() {
        return (getAdapteeManager().supportsDocetSmartCourseCatalog());
    }


    /**
     *  Tests if a course syllabus batch service is available. 
     *
     *  @return <code> true </code> if a course syllabus batch service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseSyllabusBatch() {
        return (getAdapteeManager().supportsCourseSyllabusBatch());
    }


    /**
     *  Gets the supported <code> Syllabus </code> record types. 
     *
     *  @return a list containing the supported syllabus record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSyllabusRecordTypes() {
        return (getAdapteeManager().getSyllabusRecordTypes());
    }


    /**
     *  Tests if the given <code> Syllabus </code> record type is supported. 
     *
     *  @param  syllabusRecordType a <code> Type </code> indicating a <code> 
     *          Syllabus </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> syllabusRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSyllabusRecordType(org.osid.type.Type syllabusRecordType) {
        return (getAdapteeManager().supportsSyllabusRecordType(syllabusRecordType));
    }


    /**
     *  Gets the supported syllabus search record types. 
     *
     *  @return a list containing the supported syllabus search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSyllabusSearchRecordTypes() {
        return (getAdapteeManager().getSyllabusSearchRecordTypes());
    }


    /**
     *  Tests if the given syllabus search record type is supported. 
     *
     *  @param  syllabusSearchRecordType a <code> Type </code> indicating a 
     *          syllabus record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> syllabusSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSyllabusSearchRecordType(org.osid.type.Type syllabusSearchRecordType) {
        return (getAdapteeManager().supportsSyllabusSearchRecordType(syllabusSearchRecordType));
    }


    /**
     *  Gets the supported <code> Module </code> record types. 
     *
     *  @return a list containing the supported module record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getModuleRecordTypes() {
        return (getAdapteeManager().getModuleRecordTypes());
    }


    /**
     *  Tests if the given <code> Module </code> record type is supported. 
     *
     *  @param  moduleRecordType a <code> Type </code> indicating a <code> 
     *          Module </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> moduleRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsModuleRecordType(org.osid.type.Type moduleRecordType) {
        return (getAdapteeManager().supportsModuleRecordType(moduleRecordType));
    }


    /**
     *  Gets the supported module search record types. 
     *
     *  @return a list containing the supported module search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getModuleSearchRecordTypes() {
        return (getAdapteeManager().getModuleSearchRecordTypes());
    }


    /**
     *  Tests if the given module search record type is supported. 
     *
     *  @param  moduleSearchRecordType a <code> Type </code> indicating a 
     *          module record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> moduleSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsModuleSearchRecordType(org.osid.type.Type moduleSearchRecordType) {
        return (getAdapteeManager().supportsModuleSearchRecordType(moduleSearchRecordType));
    }


    /**
     *  Gets the supported <code> Docet </code> record types. 
     *
     *  @return a list containing the supported docet record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDocetRecordTypes() {
        return (getAdapteeManager().getDocetRecordTypes());
    }


    /**
     *  Tests if the given <code> Docet </code> record type is supported. 
     *
     *  @param  docetRecordType a <code> Type </code> indicating a <code> 
     *          Docet </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> docetRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsDocetRecordType(org.osid.type.Type docetRecordType) {
        return (getAdapteeManager().supportsDocetRecordType(docetRecordType));
    }


    /**
     *  Gets the supported docet search record types. 
     *
     *  @return a list containing the supported docet search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDocetSearchRecordTypes() {
        return (getAdapteeManager().getDocetSearchRecordTypes());
    }


    /**
     *  Tests if the given docet search record type is supported. 
     *
     *  @param  docetSearchRecordType a <code> Type </code> indicating a docet 
     *          record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> docetSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsDocetSearchRecordType(org.osid.type.Type docetSearchRecordType) {
        return (getAdapteeManager().supportsDocetSearchRecordType(docetSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the syllabus 
     *  lookup service. 
     *
     *  @return a <code> SyllabusLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSyllabusLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusLookupSession getSyllabusLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSyllabusLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the syllabus 
     *  lookup service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> SyllabusLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSyllabusLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusLookupSession getSyllabusLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSyllabusLookupSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the syllabus query 
     *  service. 
     *
     *  @return a <code> SyllabusQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSyllabusQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusQuerySession getSyllabusQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSyllabusQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the syllabus query 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> SyllabusQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSyllabusQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusQuerySession getSyllabusQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSyllabusQuerySessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the syllabus 
     *  search service. 
     *
     *  @return a <code> SyllabusSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSyllabusSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusSearchSession getSyllabusSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSyllabusSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the syllabus 
     *  search service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> SyllabusSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSyllabusSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusSearchSession getSyllabusSearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSyllabusSearchSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the syllabus 
     *  administration service. 
     *
     *  @return a <code> SyllabusAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSyllabusAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusAdminSession getSyllabusAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSyllabusAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the syllabus 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> SyllabusAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSyllabusAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusAdminSession getSyllabusAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSyllabusAdminSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the syllabus 
     *  notification service. 
     *
     *  @param  syllabusReceiver the receiver 
     *  @return a <code> SyllabusNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> syllabusReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSyllabusNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusNotificationSession getSyllabusNotificationSession(org.osid.course.syllabus.SyllabusReceiver syllabusReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSyllabusNotificationSession(syllabusReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the syllabus 
     *  notification service for the given course catalog. 
     *
     *  @param  syllabusReceiver the receiver 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> SyllabusNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> syllabusReceiver </code> 
     *          or <code> courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSyllabusNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusNotificationSession getSyllabusNotificationSessionForCourseCatalog(org.osid.course.syllabus.SyllabusReceiver syllabusReceiver, 
                                                                                                               org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSyllabusNotificationSessionForCourseCatalog(syllabusReceiver, courseCatalogId));
    }


    /**
     *  Gets the session for retrieving syllabus to course catalog mappings. 
     *
     *  @return a <code> SyllabusCourseCatalogSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSyllabusCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusCourseCatalogSession getSyllabusCourseCatalogSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSyllabusCourseCatalogSession());
    }


    /**
     *  Gets the session for assigning syllabus to course catalog mappings. 
     *
     *  @return a <code> SyllabusCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSyllabusCourseCatalogAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusCourseCatalogAssignmentSession getSyllabusCourseCatalogAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSyllabusCourseCatalogAssignmentSession());
    }


    /**
     *  Gets the session associated with the syllabus smart course catalog for 
     *  the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @return a <code> SyllabusSmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException <code> courseCatalogId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSyllabusSmartCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusSmartCourseCatalogSession getSyllabusSmartCourseCatalogSession(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSyllabusSmartCourseCatalogSession(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the module lookup 
     *  service. 
     *
     *  @return a <code> ModuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsModuleLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleLookupSession getModuleLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getModuleLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the module lookup 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> ModuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsModuleLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleLookupSession getModuleLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getModuleLookupSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the module query 
     *  service. 
     *
     *  @return a <code> ModuleQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsModuleQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleQuerySession getModuleQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getModuleQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the module query 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> ModuleQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsModuleQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleQuerySession getModuleQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getModuleQuerySessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the module search 
     *  service. 
     *
     *  @return a <code> ModuleSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsModuleSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleSearchSession getModuleSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getModuleSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the module search 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> ModuleSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsModuleSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleSearchSession getModuleSearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getModuleSearchSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the module 
     *  administration service. 
     *
     *  @return a <code> ModuleAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsModuleAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleAdminSession getModuleAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getModuleAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the module 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> ModuleAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsModuleAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleAdminSession getModuleAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getModuleAdminSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the module 
     *  notification service. 
     *
     *  @param  moduleReceiver the receiver 
     *  @return a <code> ModuleNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> moduleReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsModuleNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleNotificationSession getModuleNotificationSession(org.osid.course.syllabus.ModuleReceiver moduleReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getModuleNotificationSession(moduleReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the module 
     *  notification service for the given course catalog. 
     *
     *  @param  moduleReceiver the receiver 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> ModuleNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> moduleReceiver </code> 
     *          or <code> courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsModuleNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleNotificationSession getModuleNotificationSessionForCourseCatalog(org.osid.course.syllabus.ModuleReceiver moduleReceiver, 
                                                                                                           org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getModuleNotificationSessionForCourseCatalog(moduleReceiver, courseCatalogId));
    }


    /**
     *  Gets the session for retrieving module to course catalog mappings. 
     *
     *  @return a <code> ModuleCourseCatalogSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsModuleCourseCatalog() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleCourseCatalogSession getModuleCourseCatalogSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getModuleCourseCatalogSession());
    }


    /**
     *  Gets the session for assigning module to course catalog mappings. 
     *
     *  @return a <code> ModuleCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsModuleCourseCatalogAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleCourseCatalogAssignmentSession getModuleCourseCatalogAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getModuleCourseCatalogAssignmentSession());
    }


    /**
     *  Gets the session associated with the module smart course catalog for 
     *  the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @return a <code> ModuleSmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException <code> courseCatalogId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsModuleSmartCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleSmartCourseCatalogSession getModuleSmartCourseCatalogSession(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getModuleSmartCourseCatalogSession(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the docet lookup 
     *  service. 
     *
     *  @return a <code> DocetLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDocetLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetLookupSession getDocetLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDocetLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the docet lookup 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> Docet 
     *          </code> 
     *  @return a <code> DocetLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDocetLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetLookupSession getDocetLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDocetLookupSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the docet query 
     *  service. 
     *
     *  @return a <code> DocetQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDocetQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetQuerySession getDocetQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDocetQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the docet query 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> Docet 
     *          </code> 
     *  @return a <code> DocetQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDocetQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetQuerySession getDocetQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDocetQuerySessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the docet search 
     *  service. 
     *
     *  @return a <code> DocetSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDocetSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetSearchSession getDocetSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDocetSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the docet search 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> Docet 
     *          </code> 
     *  @return a <code> DocetSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDocetSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetSearchSession getDocetSearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDocetSearchSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the docet 
     *  administrative service. 
     *
     *  @return a <code> DocetAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDocetAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetAdminSession getDocetAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDocetAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the docet 
     *  administrative service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> Docet 
     *          </code> 
     *  @return a <code> DocetAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDocetAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetAdminSession getDocetAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDocetAdminSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the docet 
     *  notification service. 
     *
     *  @param  docetReceiver the receiver 
     *  @return a <code> DocetNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> docetReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDocetNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetNotificationSession getDocetNotificationSession(org.osid.course.syllabus.DocetReceiver docetReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDocetNotificationSession(docetReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the docet 
     *  notification service for the given course catalog. 
     *
     *  @param  docetReceiver the receiver 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> DocetNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> docetReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDocetNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetNotificationSession getDocetNotificationSessionForCourseCatalog(org.osid.course.syllabus.DocetReceiver docetReceiver, 
                                                                                                         org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDocetNotificationSessionForCourseCatalog(docetReceiver, courseCatalogId));
    }


    /**
     *  Gets the session for retrieving docet to course catalog mappings. 
     *
     *  @return a <code> DocetCourseCatalogSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDocetCourseCatalog() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetCourseCatalogSession getDocetCourseCatalogSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDocetCourseCatalogSession());
    }


    /**
     *  Gets the session for assigning docet to course catalog mappings. 
     *
     *  @return a <code> DocetCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDocetCourseCatalogAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetCourseCatalogAssignmentSession getDocetCourseCatalogAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDocetCourseCatalogAssignmentSession());
    }


    /**
     *  Gets the session associated with the docet smart course catalog for 
     *  the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @return a <code> DocetSmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException <code> courseCatalogId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDocetSmartCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetSmartCourseCatalogSession getDocetSmartCourseCatalogSession(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDocetSmartCourseCatalogSession(courseCatalogId));
    }


    /**
     *  Gets a <code> CourseSyllabusBatchManager. </code> 
     *
     *  @return a <code> CourseSyllabusBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseSyllabusBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.batch.CourseSyllabusBatchManager getCourseSyllabusBatchManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseSyllabusBatchManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
