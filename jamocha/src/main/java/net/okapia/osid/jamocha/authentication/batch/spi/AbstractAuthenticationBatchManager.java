//
// AbstractAuthenticationBatchManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authentication.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractAuthenticationBatchManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.authentication.batch.AuthenticationBatchManager,
               org.osid.authentication.batch.AuthenticationBatchProxyManager {


    /**
     *  Constructs a new
     *  <code>AbstractAuthenticationBatchManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractAuthenticationBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if bulk administration of agents is available. 
     *
     *  @return <code> true </code> if an agent bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgentBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of agencies is available. 
     *
     *  @return <code> true </code> if an agency bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgencyBatchAdmin() {
        return (false);
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk agent 
     *  administration service. 
     *
     *  @return an <code> AgentBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAgentBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.batch.AgentBatchAdminSession getAgentBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.batch.AuthenticationBatchManager.getAgentBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk agent 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> AgentBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAgentBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.batch.AgentBatchAdminSession getAgentBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.batch.AuthenticationBatchProxyManager.getAgentBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk agent 
     *  administration service for the given agency. 
     *
     *  @param  agencyId the <code> Id </code> of the <code> Agency </code> 
     *  @return an <code> AgentBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Agency </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> agencyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAgentBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.batch.AgentBatchAdminSession getAgentBatchAdminSessionForAgency(org.osid.id.Id agencyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.batch.AuthenticationBatchManager.getAgentBatchAdminSessionForAgency not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk agent 
     *  administration service for the given agency. 
     *
     *  @param  agencyId the <code> Id </code> of the <code> Agency </code> 
     *  @param  proxy a proxy 
     *  @return a <code> AgentBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Agency </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> agencyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAgentBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.batch.AgentBatchAdminSession getAgentBatchAdminSessionForAgency(org.osid.id.Id agencyId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.batch.AuthenticationBatchProxyManager.getAgentBatchAdminSessionForAgency not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk agency 
     *  administration service. 
     *
     *  @return an <code> AgencyBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAgencyBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.batch.AgencyBatchAdminSession getAgencyBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.batch.AuthenticationBatchManager.getAgencyBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk agency 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> AgencyBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAgencyBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.batch.AgencyBatchAdminSession getAgencyBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.batch.AuthenticationBatchProxyManager.getAgencyBatchAdminSession not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        return;
    }
}
