//
// AbstractAuctionProcessorQuery.java
//
//     A template for making an AuctionProcessor Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.bidding.rules.auctionprocessor.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for auction processors.
 */

public abstract class AbstractAuctionProcessorQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidProcessorQuery
    implements org.osid.bidding.rules.AuctionProcessorQuery {

    private final java.util.Collection<org.osid.bidding.rules.records.AuctionProcessorQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches mapped to the auction. 
     *
     *  @param  auctionId the auction <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> auctionId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledAuctionId(org.osid.id.Id auctionId, boolean match) {
        return;
    }


    /**
     *  Clears the auction <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledAuctionIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AuctionQuery </code> is available. 
     *
     *  @return <code> true </code> if an auction query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledAuctionQuery() {
        return (false);
    }


    /**
     *  Gets the query for an auction. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the auction query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledAuctionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionQuery getRuledAuctionQuery() {
        throw new org.osid.UnimplementedException("supportsRuledAuctionQuery() is false");
    }


    /**
     *  Matches mapped to any auction. 
     *
     *  @param  match <code> true </code> for mapped to any auction, <code> 
     *          false </code> to match mapped to no auction 
     */

    @OSID @Override
    public void matchAnyRuledAuction(boolean match) {
        return;
    }


    /**
     *  Clears the auction query terms. 
     */

    @OSID @Override
    public void clearRuledAuctionTerms() {
        return;
    }


    /**
     *  Matches mapped to the auction house. 
     *
     *  @param  auctionHouseId the auction house <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchAuctionHouseId(org.osid.id.Id auctionHouseId, 
                                    boolean match) {
        return;
    }


    /**
     *  Clears the auction house <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAuctionHouseIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> AuctionHouseQuery </code> is available. 
     *
     *  @return <code> true </code> if a auction house query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionHouseQuery() {
        return (false);
    }


    /**
     *  Gets the query for a auction house. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the auction house query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionHouseQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseQuery getAuctionHouseQuery() {
        throw new org.osid.UnimplementedException("supportsAuctionHouseQuery() is false");
    }


    /**
     *  Clears the auction house query terms. 
     */

    @OSID @Override
    public void clearAuctionHouseTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given auction processor query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an auction processor implementing the requested record.
     *
     *  @param auctionProcessorRecordType an auction processor record type
     *  @return the auction processor query record
     *  @throws org.osid.NullArgumentException
     *          <code>auctionProcessorRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(auctionProcessorRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.bidding.rules.records.AuctionProcessorQueryRecord getAuctionProcessorQueryRecord(org.osid.type.Type auctionProcessorRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.bidding.rules.records.AuctionProcessorQueryRecord record : this.records) {
            if (record.implementsRecordType(auctionProcessorRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(auctionProcessorRecordType + " is not supported");
    }


    /**
     *  Adds a record to this auction processor query. 
     *
     *  @param auctionProcessorQueryRecord auction processor query record
     *  @param auctionProcessorRecordType auctionProcessor record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAuctionProcessorQueryRecord(org.osid.bidding.rules.records.AuctionProcessorQueryRecord auctionProcessorQueryRecord, 
                                          org.osid.type.Type auctionProcessorRecordType) {

        addRecordType(auctionProcessorRecordType);
        nullarg(auctionProcessorQueryRecord, "auction processor query record");
        this.records.add(auctionProcessorQueryRecord);        
        return;
    }
}
