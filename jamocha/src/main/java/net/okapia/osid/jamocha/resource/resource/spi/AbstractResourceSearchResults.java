//
// AbstractResourceSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resource.resource.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractResourceSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.resource.ResourceSearchResults {

    private org.osid.resource.ResourceList resources;
    private final org.osid.resource.ResourceQueryInspector inspector;
    private final java.util.Collection<org.osid.resource.records.ResourceSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractResourceSearchResults.
     *
     *  @param resources the result set
     *  @param resourceQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>resources</code>
     *          or <code>resourceQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractResourceSearchResults(org.osid.resource.ResourceList resources,
                                            org.osid.resource.ResourceQueryInspector resourceQueryInspector) {
        nullarg(resources, "resources");
        nullarg(resourceQueryInspector, "resource query inspectpr");

        this.resources = resources;
        this.inspector = resourceQueryInspector;

        return;
    }


    /**
     *  Gets the resource list resulting from a search.
     *
     *  @return a resource list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.resource.ResourceList getResources() {
        if (this.resources == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.resource.ResourceList resources = this.resources;
        this.resources = null;
	return (resources);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.resource.ResourceQueryInspector getResourceQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  resource search record <code> Type. </code> This method must
     *  be used to retrieve a resource implementing the requested
     *  record.
     *
     *  @param resourceSearchRecordType a resource search 
     *         record type 
     *  @return the resource search
     *  @throws org.osid.NullArgumentException
     *          <code>resourceSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(resourceSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resource.records.ResourceSearchResultsRecord getResourceSearchResultsRecord(org.osid.type.Type resourceSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.resource.records.ResourceSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(resourceSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(resourceSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record resource search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addResourceRecord(org.osid.resource.records.ResourceSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "resource record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
