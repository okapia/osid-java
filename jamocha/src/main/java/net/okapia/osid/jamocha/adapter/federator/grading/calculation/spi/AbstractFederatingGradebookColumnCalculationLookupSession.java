//
// AbstractFederatingGradebookColumnCalculationLookupSession.java
//
//     An abstract federating adapter for a GradebookColumnCalculationLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.grading.calculation.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  GradebookColumnCalculationLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingGradebookColumnCalculationLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.grading.calculation.GradebookColumnCalculationLookupSession>
    implements org.osid.grading.calculation.GradebookColumnCalculationLookupSession {

    private boolean parallel = false;
    private org.osid.grading.Gradebook gradebook = new net.okapia.osid.jamocha.nil.grading.gradebook.UnknownGradebook();


    /**
     *  Constructs a new <code>AbstractFederatingGradebookColumnCalculationLookupSession</code>.
     */

    protected AbstractFederatingGradebookColumnCalculationLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.grading.calculation.GradebookColumnCalculationLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Gradebook/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Gradebook Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getGradebookId() {
        return (this.gradebook.getId());
    }


    /**
     *  Gets the <code>Gradebook</code> associated with this 
     *  session.
     *
     *  @return the <code>Gradebook</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.Gradebook getGradebook()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.gradebook);
    }


    /**
     *  Sets the <code>Gradebook</code>.
     *
     *  @param  gradebook the gradebook for this session
     *  @throws org.osid.NullArgumentException <code>gradebook</code>
     *          is <code>null</code>
     */

    protected void setGradebook(org.osid.grading.Gradebook gradebook) {
        nullarg(gradebook, "gradebook");
        this.gradebook = gradebook;
        return;
    }


    /**
     *  Tests if this user can perform <code>GradebookColumnCalculation</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupGradebookColumnCalculations() {
        for (org.osid.grading.calculation.GradebookColumnCalculationLookupSession session : getSessions()) {
            if (session.canLookupGradebookColumnCalculations()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>GradebookColumnCalculation</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeGradebookColumnCalculationView() {
        for (org.osid.grading.calculation.GradebookColumnCalculationLookupSession session : getSessions()) {
            session.useComparativeGradebookColumnCalculationView();
        }

        return;
    }


    /**
     *  A complete view of the <code>GradebookColumnCalculation</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryGradebookColumnCalculationView() {
        for (org.osid.grading.calculation.GradebookColumnCalculationLookupSession session : getSessions()) {
            session.usePlenaryGradebookColumnCalculationView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include gradebook column calculations in gradebooks which are children
     *  of this gradebook in the gradebook hierarchy.
     */

    @OSID @Override
    public void useFederatedGradebookView() {
        for (org.osid.grading.calculation.GradebookColumnCalculationLookupSession session : getSessions()) {
            session.useFederatedGradebookView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this gradebook only.
     */

    @OSID @Override
    public void useIsolatedGradebookView() {
        for (org.osid.grading.calculation.GradebookColumnCalculationLookupSession session : getSessions()) {
            session.useIsolatedGradebookView();
        }

        return;
    }


    /**
     *  Only active gradebook column calculations are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveGradebookColumnCalculationView() {
        for (org.osid.grading.calculation.GradebookColumnCalculationLookupSession session : getSessions()) {
            session.useActiveGradebookColumnCalculationView();
        }

        return;
    }


    /**
     *  Active and inactive gradebook column calculations are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusGradebookColumnCalculationView() {
        for (org.osid.grading.calculation.GradebookColumnCalculationLookupSession session : getSessions()) {
            session.useAnyStatusGradebookColumnCalculationView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>GradebookColumnCalculation</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>GradebookColumnCalculation</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>GradebookColumnCalculation</code> and
     *  retained for compatibility.
     *
     *  In active mode, gradebook column calculations are returned that are currently
     *  active. In any status mode, active and inactive gradebook column calculations
     *  are returned.
     *
     *  @param  gradebookColumnCalculationId <code>Id</code> of the
     *          <code>GradebookColumnCalculation</code>
     *  @return the gradebook column calculation
     *  @throws org.osid.NotFoundException <code>gradebookColumnCalculationId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>gradebookColumnCalculationId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.calculation.GradebookColumnCalculation getGradebookColumnCalculation(org.osid.id.Id gradebookColumnCalculationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.grading.calculation.GradebookColumnCalculationLookupSession session : getSessions()) {
            try {
                return (session.getGradebookColumnCalculation(gradebookColumnCalculationId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(gradebookColumnCalculationId + " not found");
    }


    /**
     *  Gets a <code>GradebookColumnCalculationList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  gradebookColumnCalculations specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>GradebookColumnCalculations</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, gradebook column calculations are returned that are currently
     *  active. In any status mode, active and inactive gradebook column calculations
     *  are returned.
     *
     *  @param  gradebookColumnCalculationIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>GradebookColumnCalculation</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumnCalculationIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.calculation.GradebookColumnCalculationList getGradebookColumnCalculationsByIds(org.osid.id.IdList gradebookColumnCalculationIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.grading.calculation.gradebookcolumncalculation.MutableGradebookColumnCalculationList ret = new net.okapia.osid.jamocha.grading.calculation.gradebookcolumncalculation.MutableGradebookColumnCalculationList();

        try (org.osid.id.IdList ids = gradebookColumnCalculationIds) {
            while (ids.hasNext()) {
                ret.addGradebookColumnCalculation(getGradebookColumnCalculation(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>GradebookColumnCalculationList</code> corresponding to the given
     *  gradebook column calculation genus <code>Type</code> which does not include
     *  gradebook column calculations of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  gradebook column calculations or an error results. Otherwise, the returned list
     *  may contain only those gradebook column calculations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, gradebook column calculations are returned that are currently
     *  active. In any status mode, active and inactive gradebook column calculations
     *  are returned.
     *
     *  @param  gradebookColumnCalculationGenusType a gradebookColumnCalculation genus type 
     *  @return the returned <code>GradebookColumnCalculation</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumnCalculationGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.calculation.GradebookColumnCalculationList getGradebookColumnCalculationsByGenusType(org.osid.type.Type gradebookColumnCalculationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.grading.calculation.gradebookcolumncalculation.FederatingGradebookColumnCalculationList ret = getGradebookColumnCalculationList();

        for (org.osid.grading.calculation.GradebookColumnCalculationLookupSession session : getSessions()) {
            ret.addGradebookColumnCalculationList(session.getGradebookColumnCalculationsByGenusType(gradebookColumnCalculationGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>GradebookColumnCalculationList</code> corresponding to the given
     *  gradebook column calculation genus <code>Type</code> and include any additional
     *  gradebook column calculations with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  gradebook column calculations or an error results. Otherwise, the returned list
     *  may contain only those gradebook column calculations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, gradebook column calculations are returned that are currently
     *  active. In any status mode, active and inactive gradebook column calculations
     *  are returned.
     *
     *  @param  gradebookColumnCalculationGenusType a gradebookColumnCalculation genus type 
     *  @return the returned <code>GradebookColumnCalculation</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumnCalculationGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.calculation.GradebookColumnCalculationList getGradebookColumnCalculationsByParentGenusType(org.osid.type.Type gradebookColumnCalculationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.grading.calculation.gradebookcolumncalculation.FederatingGradebookColumnCalculationList ret = getGradebookColumnCalculationList();

        for (org.osid.grading.calculation.GradebookColumnCalculationLookupSession session : getSessions()) {
            ret.addGradebookColumnCalculationList(session.getGradebookColumnCalculationsByParentGenusType(gradebookColumnCalculationGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>GradebookColumnCalculationList</code> containing the given
     *  gradebook column calculation record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  gradebook column calculations or an error results. Otherwise, the returned list
     *  may contain only those gradebook column calculations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, gradebook column calculations are returned that are currently
     *  active. In any status mode, active and inactive gradebook column calculations
     *  are returned.
     *
     *  @param  gradebookColumnCalculationRecordType a gradebookColumnCalculation record type 
     *  @return the returned <code>GradebookColumnCalculation</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumnCalculationRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.calculation.GradebookColumnCalculationList getGradebookColumnCalculationsByRecordType(org.osid.type.Type gradebookColumnCalculationRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.grading.calculation.gradebookcolumncalculation.FederatingGradebookColumnCalculationList ret = getGradebookColumnCalculationList();

        for (org.osid.grading.calculation.GradebookColumnCalculationLookupSession session : getSessions()) {
            ret.addGradebookColumnCalculationList(session.getGradebookColumnCalculationsByRecordType(gradebookColumnCalculationRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets the <code>GradebookColumnCalculation</code> mapped to a
     *  <code>GradebookColumn</code> to which this calculation is
     *  applied. In plenary mode, the exact <code>Id</code> is found
     *  or a <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>GradebookColumnCalculation</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>GradebookColumnCalculation</code> and retained for
     *  compatibility.
     *
     *  @param gradebookColumnId <code>Id</code> of a
     *         <code>GradebookColumn</code>
     *  @return the gradebook column calculation
     *  @throws org.osid.NotFoundException <code>gradebookColumnId</code>
     *          not found
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumnId</code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.grading.calculation.GradebookColumnCalculation getGradebookColumnCalculationForGradebookColumn(org.osid.id.Id gradebookColumnId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.grading.calculation.GradebookColumnCalculationLookupSession session : getSessions()) {
            try {
                return (session.getGradebookColumnCalculationForGradebookColumn(gradebookColumnId));
            } catch (org.osid.NotFoundException nfe) {}
        }

        throw new org.osid.NotFoundException("no calculation for " + gradebookColumnId);
    }


    /**
     *  Gets a <code>GradebookColumnCalculationList</code>
     *  corresponding to the given gradebook column <code>Ids</code>
     *  to which this calculation is applied. In plenary mode, the
     *  returned list contains all of the gradebook column
     *  calculations specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if a <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible gradeboook column
     *  calculations may be omitted from the list.
     *
     *  @param gradebookColumnIds a list of gradebook column
     *         <code>Ids</code>
     *  @return the returned <code>GradebookColumnCalculation</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not found
     *  @throws org.osid.NullArgumentException
     *          <code>gradeBookColumnIds</code> <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.grading.calculation.GradebookColumnCalculationList getGradebookColumnCalculationsForGradebookColumns(org.osid.id.IdList gradebookColumnIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.grading.calculation.gradebookcolumncalculation.FederatingGradebookColumnCalculationList ret = getGradebookColumnCalculationList();

        for (org.osid.grading.calculation.GradebookColumnCalculationLookupSession session : getSessions()) {
            ret.addGradebookColumnCalculationList(session.getGradebookColumnCalculationsForGradebookColumns(gradebookColumnIds));
        }

        ret.noMore();
        return (ret);
    }
        
        
    /**
     *  Gets all <code>GradebookColumnCalculations</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  gradebook column calculations or an error results. Otherwise, the returned list
     *  may contain only those gradebook column calculations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, gradebook column calculations are returned that are currently
     *  active. In any status mode, active and inactive gradebook column calculations
     *  are returned.
     *
     *  @return a list of <code>GradebookColumnCalculations</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.calculation.GradebookColumnCalculationList getGradebookColumnCalculations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.grading.calculation.gradebookcolumncalculation.FederatingGradebookColumnCalculationList ret = getGradebookColumnCalculationList();

        for (org.osid.grading.calculation.GradebookColumnCalculationLookupSession session : getSessions()) {
            ret.addGradebookColumnCalculationList(session.getGradebookColumnCalculations());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.grading.calculation.gradebookcolumncalculation.FederatingGradebookColumnCalculationList getGradebookColumnCalculationList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.grading.calculation.gradebookcolumncalculation.ParallelGradebookColumnCalculationList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.grading.calculation.gradebookcolumncalculation.CompositeGradebookColumnCalculationList());
        }
    }
}
