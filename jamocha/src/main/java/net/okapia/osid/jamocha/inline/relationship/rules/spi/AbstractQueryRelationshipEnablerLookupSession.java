//
// AbstractQueryRelationshipEnablerLookupSession.java
//
//    An inline adapter that maps a RelationshipEnablerLookupSession to
//    a RelationshipEnablerQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.relationship.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a RelationshipEnablerLookupSession to
 *  a RelationshipEnablerQuerySession.
 */

public abstract class AbstractQueryRelationshipEnablerLookupSession
    extends net.okapia.osid.jamocha.relationship.rules.spi.AbstractRelationshipEnablerLookupSession
    implements org.osid.relationship.rules.RelationshipEnablerLookupSession {

    private boolean activeonly    = false;
    private boolean effectiveonly = false;
    private final org.osid.relationship.rules.RelationshipEnablerQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryRelationshipEnablerLookupSession.
     *
     *  @param querySession the underlying relationship enabler query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryRelationshipEnablerLookupSession(org.osid.relationship.rules.RelationshipEnablerQuerySession querySession) {
        nullarg(querySession, "relationship enabler query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Family</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Family Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFamilyId() {
        return (this.session.getFamilyId());
    }


    /**
     *  Gets the <code>Family</code> associated with this 
     *  session.
     *
     *  @return the <code>Family</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.Family getFamily()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getFamily());
    }


    /**
     *  Tests if this user can perform <code>RelationshipEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupRelationshipEnablers() {
        return (this.session.canSearchRelationshipEnablers());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include relationship enablers in families which are children
     *  of this family in the family hierarchy.
     */

    @OSID @Override
    public void useFederatedFamilyView() {
        this.session.useFederatedFamilyView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this family only.
     */

    @OSID @Override
    public void useIsolatedFamilyView() {
        this.session.useIsolatedFamilyView();
        return;
    }
    

    /**
     *  Only active relationship enablers are returned by methods in
     *  this session.
     */
     
    @OSID @Override
    public void useActiveRelationshipEnablerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive relationship enablers are returned by
     *  methods in this session.
     */
    
    @OSID @Override
    public void useAnyStatusRelationshipEnablerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>RelationshipEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>RelationshipEnabler</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>RelationshipEnabler</code> and retained for
     *  compatibility.
     *
     *  In active mode, relationship enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  relationship enablers are returned.
     *
     *  @param  relationshipEnablerId <code>Id</code> of the
     *          <code>RelationshipEnabler</code>
     *  @return the relationship enabler
     *  @throws org.osid.NotFoundException <code>relationshipEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>relationshipEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnabler getRelationshipEnabler(org.osid.id.Id relationshipEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.relationship.rules.RelationshipEnablerQuery query = getQuery();
        query.matchId(relationshipEnablerId, true);
        org.osid.relationship.rules.RelationshipEnablerList relationshipEnablers = this.session.getRelationshipEnablersByQuery(query);
        if (relationshipEnablers.hasNext()) {
            return (relationshipEnablers.getNextRelationshipEnabler());
        } 
        
        throw new org.osid.NotFoundException(relationshipEnablerId + " not found");
    }


    /**
     *  Gets a <code>RelationshipEnablerList</code> corresponding to
     *  the given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  relationshipEnablers specified in the <code>Id</code> list, in
     *  the order of the list, including duplicates, or an error
     *  results if an <code>Id</code> in the supplied list is not
     *  found or inaccessible. Otherwise, inaccessible
     *  <code>RelationshipEnablers</code> may be omitted from the list
     *  and may present the elements in any order including returning
     *  a unique set.
     *
     *  In active mode, relationship enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  relationship enablers are returned.
     *
     *  @param  relationshipEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>RelationshipEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>relationshipEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerList getRelationshipEnablersByIds(org.osid.id.IdList relationshipEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.relationship.rules.RelationshipEnablerQuery query = getQuery();

        try (org.osid.id.IdList ids = relationshipEnablerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getRelationshipEnablersByQuery(query));
    }


    /**
     *  Gets a <code>RelationshipEnablerList</code> corresponding to
     *  the given relationship enabler genus <code>Type</code> which
     *  does not include relationship enablers of types derived from
     *  the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  relationship enablers or an error results. Otherwise, the
     *  returned list may contain only those relationship enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, relationship enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  relationship enablers are returned.
     *
     *  @param  relationshipEnablerGenusType a relationshipEnabler genus type 
     *  @return the returned <code>RelationshipEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>relationshipEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerList getRelationshipEnablersByGenusType(org.osid.type.Type relationshipEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.relationship.rules.RelationshipEnablerQuery query = getQuery();
        query.matchGenusType(relationshipEnablerGenusType, true);
        return (this.session.getRelationshipEnablersByQuery(query));
    }


    /**
     *  Gets a <code>RelationshipEnablerList</code> corresponding to
     *  the given relationship enabler genus <code>Type</code> and
     *  include any additional relationship enablers with genus types
     *  derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  relationship enablers or an error results. Otherwise, the
     *  returned list may contain only those relationship enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, relationship enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  relationship enablers are returned.
     *
     *  @param  relationshipEnablerGenusType a relationshipEnabler genus type 
     *  @return the returned <code>RelationshipEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>relationshipEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerList getRelationshipEnablersByParentGenusType(org.osid.type.Type relationshipEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.relationship.rules.RelationshipEnablerQuery query = getQuery();
        query.matchParentGenusType(relationshipEnablerGenusType, true);
        return (this.session.getRelationshipEnablersByQuery(query));
    }


    /**
     *  Gets a <code>RelationshipEnablerList</code> containing the
     *  given relationship enabler record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known
     *  relationship enablers or an error results. Otherwise, the
     *  returned list may contain only those relationship enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, relationship enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  relationship enablers are returned.
     *
     *  @param  relationshipEnablerRecordType a relationshipEnabler record type 
     *  @return the returned <code>RelationshipEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>relationshipEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerList getRelationshipEnablersByRecordType(org.osid.type.Type relationshipEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.relationship.rules.RelationshipEnablerQuery query = getQuery();
        query.matchRecordType(relationshipEnablerRecordType, true);
        return (this.session.getRelationshipEnablersByQuery(query));
    }


    /**
     *  Gets a <code>RelationshipEnablerList</code> effective during
     *  the entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  relationship enablers or an error results. Otherwise, the
     *  returned list may contain only those relationship enablers
     *  that are accessible through this session.
     *  
     *  In active mode, relationship enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  relationship enablers are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>RelationshipEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerList getRelationshipEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.relationship.rules.RelationshipEnablerQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getRelationshipEnablersByQuery(query));
    }
        
    
    /**
     *  Gets all <code>RelationshipEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  relationship enablers or an error results. Otherwise, the
     *  returned list may contain only those relationship enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, relationship enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  relationship enablers are returned.
     *
     *  @return a list of <code>RelationshipEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerList getRelationshipEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.relationship.rules.RelationshipEnablerQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getRelationshipEnablersByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.relationship.rules.RelationshipEnablerQuery getQuery() {
        org.osid.relationship.rules.RelationshipEnablerQuery query = this.session.getRelationshipEnablerQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
