//
// MutableIndexedMapProxyRecipeLookupSession
//
//    Implements a Recipe lookup service backed by a collection of
//    recipes indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.recipe;


/**
 *  Implements a Recipe lookup service backed by a collection of
 *  recipes. The recipes are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some recipes may be compatible
 *  with more types than are indicated through these recipe
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of recipes can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyRecipeLookupSession
    extends net.okapia.osid.jamocha.core.recipe.spi.AbstractIndexedMapRecipeLookupSession
    implements org.osid.recipe.RecipeLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyRecipeLookupSession} with
     *  no recipe.
     *
     *  @param cookbook the cookbook
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code cookbook} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyRecipeLookupSession(org.osid.recipe.Cookbook cookbook,
                                                       org.osid.proxy.Proxy proxy) {
        setCookbook(cookbook);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyRecipeLookupSession} with
     *  a single recipe.
     *
     *  @param cookbook the cookbook
     *  @param  recipe an recipe
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code cookbook},
     *          {@code recipe}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyRecipeLookupSession(org.osid.recipe.Cookbook cookbook,
                                                       org.osid.recipe.Recipe recipe, org.osid.proxy.Proxy proxy) {

        this(cookbook, proxy);
        putRecipe(recipe);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyRecipeLookupSession} using
     *  an array of recipes.
     *
     *  @param cookbook the cookbook
     *  @param  recipes an array of recipes
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code cookbook},
     *          {@code recipes}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyRecipeLookupSession(org.osid.recipe.Cookbook cookbook,
                                                       org.osid.recipe.Recipe[] recipes, org.osid.proxy.Proxy proxy) {

        this(cookbook, proxy);
        putRecipes(recipes);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyRecipeLookupSession} using
     *  a collection of recipes.
     *
     *  @param cookbook the cookbook
     *  @param  recipes a collection of recipes
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code cookbook},
     *          {@code recipes}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyRecipeLookupSession(org.osid.recipe.Cookbook cookbook,
                                                       java.util.Collection<? extends org.osid.recipe.Recipe> recipes,
                                                       org.osid.proxy.Proxy proxy) {
        this(cookbook, proxy);
        putRecipes(recipes);
        return;
    }

    
    /**
     *  Makes a {@code Recipe} available in this session.
     *
     *  @param  recipe a recipe
     *  @throws org.osid.NullArgumentException {@code recipe{@code 
     *          is {@code null}
     */

    @Override
    public void putRecipe(org.osid.recipe.Recipe recipe) {
        super.putRecipe(recipe);
        return;
    }


    /**
     *  Makes an array of recipes available in this session.
     *
     *  @param  recipes an array of recipes
     *  @throws org.osid.NullArgumentException {@code recipes{@code 
     *          is {@code null}
     */

    @Override
    public void putRecipes(org.osid.recipe.Recipe[] recipes) {
        super.putRecipes(recipes);
        return;
    }


    /**
     *  Makes collection of recipes available in this session.
     *
     *  @param  recipes a collection of recipes
     *  @throws org.osid.NullArgumentException {@code recipe{@code 
     *          is {@code null}
     */

    @Override
    public void putRecipes(java.util.Collection<? extends org.osid.recipe.Recipe> recipes) {
        super.putRecipes(recipes);
        return;
    }


    /**
     *  Removes a Recipe from this session.
     *
     *  @param recipeId the {@code Id} of the recipe
     *  @throws org.osid.NullArgumentException {@code recipeId{@code  is
     *          {@code null}
     */

    @Override
    public void removeRecipe(org.osid.id.Id recipeId) {
        super.removeRecipe(recipeId);
        return;
    }    
}
