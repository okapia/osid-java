//
// AbstractFederatingEntryLookupSession.java
//
//     An abstract federating adapter for an EntryLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.blogging.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for an
 *  EntryLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingEntryLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.blogging.EntryLookupSession>
    implements org.osid.blogging.EntryLookupSession {

    private boolean parallel = false;
    private org.osid.blogging.Blog blog = new net.okapia.osid.jamocha.nil.blogging.blog.UnknownBlog();


    /**
     *  Constructs a new <code>AbstractFederatingEntryLookupSession</code>.
     */

    protected AbstractFederatingEntryLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.blogging.EntryLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Blog/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Blog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBlogId() {
        return (this.blog.getId());
    }


    /**
     *  Gets the <code>Blog</code> associated with this 
     *  session.
     *
     *  @return the <code>Blog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.blogging.Blog getBlog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.blog);
    }


    /**
     *  Sets the <code>Blog</code>.
     *
     *  @param  blog the blog for this session
     *  @throws org.osid.NullArgumentException <code>blog</code>
     *          is <code>null</code>
     */

    protected void setBlog(org.osid.blogging.Blog blog) {
        nullarg(blog, "blog");
        this.blog = blog;
        return;
    }


    /**
     *  Tests if this user can perform <code>Entry</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupEntries() {
        for (org.osid.blogging.EntryLookupSession session : getSessions()) {
            if (session.canLookupEntries()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Entry</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeEntryView() {
        for (org.osid.blogging.EntryLookupSession session : getSessions()) {
            session.useComparativeEntryView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Entry</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryEntryView() {
        for (org.osid.blogging.EntryLookupSession session : getSessions()) {
            session.usePlenaryEntryView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include entries in blogs which are children
     *  of this blog in the blog hierarchy.
     */

    @OSID @Override
    public void useFederatedBlogView() {
        for (org.osid.blogging.EntryLookupSession session : getSessions()) {
            session.useFederatedBlogView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this blog only.
     */

    @OSID @Override
    public void useIsolatedBlogView() {
        for (org.osid.blogging.EntryLookupSession session : getSessions()) {
            session.useIsolatedBlogView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Entry</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Entry</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Entry</code> and
     *  retained for compatibility.
     *
     *  @param  entryId <code>Id</code> of the
     *          <code>Entry</code>
     *  @return the entry
     *  @throws org.osid.NotFoundException <code>entryId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>entryId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.blogging.Entry getEntry(org.osid.id.Id entryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.blogging.EntryLookupSession session : getSessions()) {
            try {
                return (session.getEntry(entryId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(entryId + " not found");
    }


    /**
     *  Gets an <code>EntryList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  entries specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Entries</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  entryIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Entry</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>entryIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.blogging.EntryList getEntriesByIds(org.osid.id.IdList entryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.blogging.entry.MutableEntryList ret = new net.okapia.osid.jamocha.blogging.entry.MutableEntryList();

        try (org.osid.id.IdList ids = entryIds) {
            while (ids.hasNext()) {
                ret.addEntry(getEntry(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets an <code>EntryList</code> corresponding to the given
     *  entry genus <code>Type</code> which does not include
     *  entries of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  entries or an error results. Otherwise, the returned list
     *  may contain only those entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  entryGenusType an entry genus type 
     *  @return the returned <code>Entry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>entryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.blogging.EntryList getEntriesByGenusType(org.osid.type.Type entryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.blogging.entry.FederatingEntryList ret = getEntryList();

        for (org.osid.blogging.EntryLookupSession session : getSessions()) {
            ret.addEntryList(session.getEntriesByGenusType(entryGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>EntryList</code> corresponding to the given
     *  entry genus <code>Type</code> and include any additional
     *  entries with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  entries or an error results. Otherwise, the returned list
     *  may contain only those entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  entryGenusType an entry genus type 
     *  @return the returned <code>Entry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>entryGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.blogging.EntryList getEntriesByParentGenusType(org.osid.type.Type entryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.blogging.entry.FederatingEntryList ret = getEntryList();

        for (org.osid.blogging.EntryLookupSession session : getSessions()) {
            ret.addEntryList(session.getEntriesByParentGenusType(entryGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>EntryList</code> containing the given
     *  entry record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  entries or an error results. Otherwise, the returned list
     *  may contain only those entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  entryRecordType an entry record type 
     *  @return the returned <code>Entry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>entryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.blogging.EntryList getEntriesByRecordType(org.osid.type.Type entryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.blogging.entry.FederatingEntryList ret = getEntryList();

        for (org.osid.blogging.EntryLookupSession session : getSessions()) {
            ret.addEntryList(session.getEntriesByRecordType(entryRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>EntryList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known entries or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  entries that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Entry</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.blogging.EntryList getEntriesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        net.okapia.osid.jamocha.adapter.federator.blogging.entry.FederatingEntryList ret = getEntryList();

        for (org.osid.blogging.EntryLookupSession session : getSessions()) {
            ret.addEntryList(session.getEntriesByProvider(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code> EntryList </code> posted within the specified
     *  range inclusive. In plenary mode, the returned list contains
     *  all known entries or an error results. Otherwise, the returned
     *  list may contain only those entries that are accessible
     *  through this session.
     *
     *  @param  from starting date
     *  @param  to ending date
     *  @return the returned <code> Entry </code> list
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less
     *          than <code> from </code>
     *  @throws org.osid.NullArgumentException <code> from </code> or <code>
     *          to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.blogging.EntryList getEntriesByDate(org.osid.calendaring.DateTime from,
                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.blogging.entry.FederatingEntryList ret = getEntryList();

        for (org.osid.blogging.EntryLookupSession session : getSessions()) {
            ret.addEntryList(session.getEntriesByDate(from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code> EntryList </code> for the given poster resource
     *  <code> Id. </code> In plenary mode, the returned list contains
     *  all known entries or an error results. Otherwise, the returned
     *  list may contain only those entries that are accessible
     *  through this session.
     *
     *  @param  resourceId a poster <code> Id </code>
     *  @return the returned <code> Entry </code> list
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.blogging.EntryList getEntriesForPoster(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.blogging.entry.FederatingEntryList ret = getEntryList();

        for (org.osid.blogging.EntryLookupSession session : getSessions()) {
            ret.addEntryList(session.getEntriesForPoster(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code> EntryList </code> for a poster resource <code> Id
     *  </code> posted within the specified range inclusive. In plenary mode,
     *  the returned list contains all known entries or an error results.
     *  Otherwise, the returned list may contain only those entries that are
     *  accessible through this session.
     *
     *  @param  resourceId a poster <code> Id </code>
     *  @param  from starting date
     *  @param  to ending date
     *  @return the returned <code> Entry </code> list
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less
     *          than <code> from </code>
     *  @throws org.osid.NullArgumentException <code> resourceId, from </code>
     *          or <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.blogging.EntryList getEntriesByDateForPoster(org.osid.id.Id resourceId,
                                                                 org.osid.calendaring.DateTime from,
                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.blogging.entry.FederatingEntryList ret = getEntryList();

        for (org.osid.blogging.EntryLookupSession session : getSessions()) {
            ret.addEntryList(session.getEntriesByDateForPoster(resourceId, from, to));
        }

        ret.noMore();
        return (ret);
    }
    
    
    /**
     *  Gets all <code>Entries</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  entries or an error results. Otherwise, the returned list
     *  may contain only those entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Entries</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.blogging.EntryList getEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.blogging.entry.FederatingEntryList ret = getEntryList();

        for (org.osid.blogging.EntryLookupSession session : getSessions()) {
            ret.addEntryList(session.getEntries());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.blogging.entry.FederatingEntryList getEntryList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.blogging.entry.ParallelEntryList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.blogging.entry.CompositeEntryList());
        }
    }
}
