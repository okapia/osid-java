//
// AbstractEngineQueryInspector.java
//
//     A template for making an EngineQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.search.engine.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for engines.
 */

public abstract class AbstractEngineQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQueryInspector
    implements org.osid.search.EngineQueryInspector {

    private final java.util.Collection<org.osid.search.records.EngineQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the ancestor engine <code> Id </code> query terms. 
     *
     *  @return the ancestor engine <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorEngineIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor engine query terms. 
     *
     *  @return the ancestor engine terms 
     */

    @OSID @Override
    public org.osid.search.EngineQueryInspector[] getAncestorEngineTerms() {
        return (new org.osid.search.EngineQueryInspector[0]);
    }


    /**
     *  Gets the descendant engine <code> Id </code> query terms. 
     *
     *  @return the descendant engine <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantEngineIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant engine query terms. 
     *
     *  @return the descendant engine terms 
     */

    @OSID @Override
    public org.osid.search.EngineQueryInspector[] getDescendantEngineTerms() {
        return (new org.osid.search.EngineQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given engine query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an engine implementing the requested record.
     *
     *  @param engineRecordType an engine record type
     *  @return the engine query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>engineRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(engineRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.search.records.EngineQueryInspectorRecord getEngineQueryInspectorRecord(org.osid.type.Type engineRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.search.records.EngineQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(engineRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(engineRecordType + " is not supported");
    }


    /**
     *  Adds a record to this engine query. 
     *
     *  @param engineQueryInspectorRecord engine query inspector
     *         record
     *  @param engineRecordType engine record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addEngineQueryInspectorRecord(org.osid.search.records.EngineQueryInspectorRecord engineQueryInspectorRecord, 
                                                   org.osid.type.Type engineRecordType) {

        addRecordType(engineRecordType);
        nullarg(engineRecordType, "engine record type");
        this.records.add(engineQueryInspectorRecord);        
        return;
    }
}
