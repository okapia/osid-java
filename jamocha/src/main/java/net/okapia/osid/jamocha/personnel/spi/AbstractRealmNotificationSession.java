//
// AbstractRealmNotificationSession.java
//
//     A template for making RealmNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.personnel.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code Realm} objects. This session is intended
 *  for consumers needing to synchronize their state with this service
 *  without the use of polling. Notifications are cancelled when this
 *  session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code Realm} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for realm entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractRealmNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.personnel.RealmNotificationSession {


    /**
     *  Tests if this user can register for {@code Realm}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForRealmNotifications() {
        return (true);
    }


    /**
     *  Register for notifications of new realms. {@code
     *  RealmReceiver.newRealm()} is invoked when a new {@code Realm}
     *  is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewRealms()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of an updated hierarchy structure
     *  that introduces a new ancestor of the specified realm. {@code
     *  RealmReceiver.newAncestorRealm()} is invoked when the
     *  specified realm node gets a new ancestor.
     *
     *  @param realmId the {@code Id} of the
     *         {@code Realm} node to monitor
     *  @throws org.osid.NullArgumentException {@code realmId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewRealmAncestors(org.osid.id.Id realmId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of an updated hierarchy structure
     *  that introduces a new descendant of the specified
     *  realm. {@code RealmReceiver.newDescendantRealm()} is invoked
     *  when the specified realm node gets a new descendant.
     *
     *  @param realmId the {@code Id} of the
     *         {@code Realm} node to monitor
     *  @throws org.osid.NullArgumentException {@code realmId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewRealmDescendants(org.osid.id.Id realmId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of updated realms. {@code
     *  RealmReceiver.changedRealm()} is invoked when a realm is
     *  changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedRealms()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated realm. {@code
     *  RealmReceiver.changedRealm()} is invoked when the specified
     *  realm is changed.
     *
     *  @param realmId the {@code Id} of the {@code Realm} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code realmId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedRealm(org.osid.id.Id realmId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted realms. {@code
     *  RealmReceiver.deletedRealm()} is invoked when a realm is
     *  deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedRealms()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of a deleted realm. {@code
     *  RealmReceiver.deletedRealm()} is invoked when the specified
     *  realm is deleted.
     *
     *  @param realmId the {@code Id} of the
     *          {@code Realm} to monitor
     *  @throws org.osid.NullArgumentException {@code realmId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedRealm(org.osid.id.Id realmId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated hierarchy structure
     *  that removes an ancestor of the specified realm. {@code
     *  RealmReceiver.deletedAncestor()} is invoked when the specified
     *  realm node loses an ancestor.
     *
     *  @param realmId the {@code Id} of the
     *         {@code Realm} node to monitor
     *  @throws org.osid.NullArgumentException {@code realmId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedRealmAncestors(org.osid.id.Id realmId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated hierarchy structure
     *  that removes a descendant of the specified realm. {@code
     *  RealmReceiver.deletedDescendant()} is invoked when the
     *  specified realm node loses a descendant.
     *
     *  @param realmId the {@code Id} of the
     *          {@code Realm} node to monitor
     *  @throws org.osid.NullArgumentException {@code realmId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedRealmDescendants(org.osid.id.Id realmId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
