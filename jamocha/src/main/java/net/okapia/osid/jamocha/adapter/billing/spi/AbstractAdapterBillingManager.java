//
// AbstractBillingManager.java
//
//     An adapter for a BillingManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.billing.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a BillingManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterBillingManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.billing.BillingManager>
    implements org.osid.billing.BillingManager {


    /**
     *  Constructs a new {@code AbstractAdapterBillingManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterBillingManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterBillingManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterBillingManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any business federation is exposed. Federation is exposed 
     *  when a specific business may be identified, selected and used to 
     *  create a lookup or admin session. Federation is not exposed when a set 
     *  of catalogs appears as a single catalog. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if looking up customers is supported. 
     *
     *  @return <code> true </code> if customer lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCustomerLookup() {
        return (getAdapteeManager().supportsCustomerLookup());
    }


    /**
     *  Tests if querying customers is supported. 
     *
     *  @return <code> true </code> if customer query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCustomerQuery() {
        return (getAdapteeManager().supportsCustomerQuery());
    }


    /**
     *  Tests if searching customers is supported. 
     *
     *  @return <code> true </code> if customer search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCustomerSearch() {
        return (getAdapteeManager().supportsCustomerSearch());
    }


    /**
     *  Tests if customer <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if customer administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCustomerAdmin() {
        return (getAdapteeManager().supportsCustomerAdmin());
    }


    /**
     *  Tests if a customer <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if customer notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCustomerNotification() {
        return (getAdapteeManager().supportsCustomerNotification());
    }


    /**
     *  Tests if a businessing service is supported. 
     *
     *  @return <code> true </code> if businessing is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCustomerBusiness() {
        return (getAdapteeManager().supportsCustomerBusiness());
    }


    /**
     *  Tests if a businessing service is supported. A businessing service 
     *  maps customers to catalogs. 
     *
     *  @return <code> true </code> if businessing is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCustomerBusinessAssignment() {
        return (getAdapteeManager().supportsCustomerBusinessAssignment());
    }


    /**
     *  Tests if a customer smart business session is available. 
     *
     *  @return <code> true </code> if a customer smart business session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCustomerSmartBusiness() {
        return (getAdapteeManager().supportsCustomerSmartBusiness());
    }


    /**
     *  Tests if looking up items is supported. 
     *
     *  @return <code> true </code> if item lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemLookup() {
        return (getAdapteeManager().supportsItemLookup());
    }


    /**
     *  Tests if querying items is supported. 
     *
     *  @return <code> true </code> if item query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemQuery() {
        return (getAdapteeManager().supportsItemQuery());
    }


    /**
     *  Tests if searching items is supported. 
     *
     *  @return <code> true </code> if item search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemSearch() {
        return (getAdapteeManager().supportsItemSearch());
    }


    /**
     *  Tests if an item <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if item administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemAdmin() {
        return (getAdapteeManager().supportsItemAdmin());
    }


    /**
     *  Tests if an item <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if item notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemNotification() {
        return (getAdapteeManager().supportsItemNotification());
    }


    /**
     *  Tests if an item cataloging service is supported. 
     *
     *  @return <code> true </code> if item catalog is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemBusiness() {
        return (getAdapteeManager().supportsItemBusiness());
    }


    /**
     *  Tests if an item cataloging service is supported. A cataloging service 
     *  maps items to catalogs. 
     *
     *  @return <code> true </code> if item cataloging is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemBusinessAssignment() {
        return (getAdapteeManager().supportsItemBusinessAssignment());
    }


    /**
     *  Tests if an item smart business session is available. 
     *
     *  @return <code> true </code> if an item smart business session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemSmartBusiness() {
        return (getAdapteeManager().supportsItemSmartBusiness());
    }


    /**
     *  Tests if looking up categories is supported. 
     *
     *  @return <code> true </code> if category lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCategoryLookup() {
        return (getAdapteeManager().supportsCategoryLookup());
    }


    /**
     *  Tests if querying categories is supported. 
     *
     *  @return <code> true </code> if category query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCategoryQuery() {
        return (getAdapteeManager().supportsCategoryQuery());
    }


    /**
     *  Tests if searching categories is supported. 
     *
     *  @return <code> true </code> if category search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCategorySearch() {
        return (getAdapteeManager().supportsCategorySearch());
    }


    /**
     *  Tests if category <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if category administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCategoryAdmin() {
        return (getAdapteeManager().supportsCategoryAdmin());
    }


    /**
     *  Tests if a category <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if category notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCategoryNotification() {
        return (getAdapteeManager().supportsCategoryNotification());
    }


    /**
     *  Tests if a category cataloging service is supported. 
     *
     *  @return <code> true </code> if category catalog is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCategoryBusiness() {
        return (getAdapteeManager().supportsCategoryBusiness());
    }


    /**
     *  Tests if a category cataloging service is supported. A cataloging 
     *  service maps categories to catalogs. 
     *
     *  @return <code> true </code> if category cataloging is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCategoryBusinessAssignment() {
        return (getAdapteeManager().supportsCategoryBusinessAssignment());
    }


    /**
     *  Tests if a category smart business session is available. 
     *
     *  @return <code> true </code> if a category smart business session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCategorySmartBusiness() {
        return (getAdapteeManager().supportsCategorySmartBusiness());
    }


    /**
     *  Tests if looking up entries is supported. 
     *
     *  @return <code> true </code> if entry lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryLookup() {
        return (getAdapteeManager().supportsEntryLookup());
    }


    /**
     *  Tests if querying entries is supported. 
     *
     *  @return <code> true </code> if entry query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryQuery() {
        return (getAdapteeManager().supportsEntryQuery());
    }


    /**
     *  Tests if searching entries is supported. 
     *
     *  @return <code> true </code> if entry search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntrySearch() {
        return (getAdapteeManager().supportsEntrySearch());
    }


    /**
     *  Tests if entry administrative service is supported. 
     *
     *  @return <code> true </code> if entry administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryAdmin() {
        return (getAdapteeManager().supportsEntryAdmin());
    }


    /**
     *  Tests if an entry <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if entry notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryNotification() {
        return (getAdapteeManager().supportsEntryNotification());
    }


    /**
     *  Tests if an entry cataloging service is supported. 
     *
     *  @return <code> true </code> if entry catalog is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryBusiness() {
        return (getAdapteeManager().supportsEntryBusiness());
    }


    /**
     *  Tests if an entry cataloging service is supported. A cataloging 
     *  service maps entries to catalogs. 
     *
     *  @return <code> true </code> if entry cataloging is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryBusinessAssignment() {
        return (getAdapteeManager().supportsEntryBusinessAssignment());
    }


    /**
     *  Tests if an entry smart business session is available. 
     *
     *  @return <code> true </code> if an entry smart business session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntrySmartBusiness() {
        return (getAdapteeManager().supportsEntrySmartBusiness());
    }


    /**
     *  Tests if looking up periods is supported. 
     *
     *  @return <code> true </code> if period lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPeriodLookup() {
        return (getAdapteeManager().supportsPeriodLookup());
    }


    /**
     *  Tests if querying periods is supported. 
     *
     *  @return <code> true </code> if period query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPeriodQuery() {
        return (getAdapteeManager().supportsPeriodQuery());
    }


    /**
     *  Tests if searching periods is supported. 
     *
     *  @return <code> true </code> if period search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPeriodSearch() {
        return (getAdapteeManager().supportsPeriodSearch());
    }


    /**
     *  Tests if period <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if period administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPeriodAdmin() {
        return (getAdapteeManager().supportsPeriodAdmin());
    }


    /**
     *  Tests if a period <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if period notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPeriodNotification() {
        return (getAdapteeManager().supportsPeriodNotification());
    }


    /**
     *  Tests if a period cataloging service is supported. 
     *
     *  @return <code> true </code> if period catalog is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPeriodBusiness() {
        return (getAdapteeManager().supportsPeriodBusiness());
    }


    /**
     *  Tests if a period cataloging service is supported. A cataloging 
     *  service maps periods to catalogs. 
     *
     *  @return <code> true </code> if period cataloging is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPeriodBusinessAssignment() {
        return (getAdapteeManager().supportsPeriodBusinessAssignment());
    }


    /**
     *  Tests if a period smart business session is available. 
     *
     *  @return <code> true </code> if a period smart business session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPeriodSmartBusiness() {
        return (getAdapteeManager().supportsPeriodSmartBusiness());
    }


    /**
     *  Tests if looking up businesses is supported. 
     *
     *  @return <code> true </code> if business lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessLookup() {
        return (getAdapteeManager().supportsBusinessLookup());
    }


    /**
     *  Tests if searching businesses is supported. 
     *
     *  @return <code> true </code> if business search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessSearch() {
        return (getAdapteeManager().supportsBusinessSearch());
    }


    /**
     *  Tests if querying businesses is supported. 
     *
     *  @return <code> true </code> if business query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessQuery() {
        return (getAdapteeManager().supportsBusinessQuery());
    }


    /**
     *  Tests if business administrative service is supported. 
     *
     *  @return <code> true </code> if business administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessAdmin() {
        return (getAdapteeManager().supportsBusinessAdmin());
    }


    /**
     *  Tests if a business <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if business notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessNotification() {
        return (getAdapteeManager().supportsBusinessNotification());
    }


    /**
     *  Tests for the availability of a business hierarchy traversal service. 
     *
     *  @return <code> true </code> if business hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessHierarchy() {
        return (getAdapteeManager().supportsBusinessHierarchy());
    }


    /**
     *  Tests for the availability of a business hierarchy design service. 
     *
     *  @return <code> true </code> if business hierarchy design is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessHierarchyDesign() {
        return (getAdapteeManager().supportsBusinessHierarchyDesign());
    }


    /**
     *  Tests for the availability of a billing batch service. 
     *
     *  @return <code> true </code> if a billing batch service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBillingBatch() {
        return (getAdapteeManager().supportsBillingBatch());
    }


    /**
     *  Tests for the availability of a billing payment service. 
     *
     *  @return <code> true </code> if a billing payment service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBillingPayment() {
        return (getAdapteeManager().supportsBillingPayment());
    }


    /**
     *  Gets the supported <code> Customer </code> record types. 
     *
     *  @return a list containing the supported <code> Customer </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCustomerRecordTypes() {
        return (getAdapteeManager().getCustomerRecordTypes());
    }


    /**
     *  Tests if the given <code> Customer </code> record type is supported. 
     *
     *  @param  customerRecordType a <code> Type </code> indicating a <code> 
     *          Customer </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> customerRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCustomerRecordType(org.osid.type.Type customerRecordType) {
        return (getAdapteeManager().supportsCustomerRecordType(customerRecordType));
    }


    /**
     *  Gets the supported <code> Customer </code> search record types. 
     *
     *  @return a list containing the supported <code> Customer </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCustomerSearchRecordTypes() {
        return (getAdapteeManager().getCustomerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Customer </code> search record type is 
     *  supported. 
     *
     *  @param  customerSearchRecordType a <code> Type </code> indicating a 
     *          <code> Customer </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> customerSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCustomerSearchRecordType(org.osid.type.Type customerSearchRecordType) {
        return (getAdapteeManager().supportsCustomerSearchRecordType(customerSearchRecordType));
    }


    /**
     *  Gets the supported <code> Item </code> record types. 
     *
     *  @return a list containing the supported <code> Item </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getItemRecordTypes() {
        return (getAdapteeManager().getItemRecordTypes());
    }


    /**
     *  Tests if the given <code> Item </code> record type is supported. 
     *
     *  @param  itemRecordType a <code> Type </code> indicating an <code> Item 
     *          </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> itemRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsItemRecordType(org.osid.type.Type itemRecordType) {
        return (getAdapteeManager().supportsItemRecordType(itemRecordType));
    }


    /**
     *  Gets the supported <code> Item </code> search record types. 
     *
     *  @return a list containing the supported <code> Item </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getItemSearchRecordTypes() {
        return (getAdapteeManager().getItemSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Item </code> search record type is 
     *  supported. 
     *
     *  @param  itemSearchRecordType a <code> Type </code> indicating an 
     *          <code> Item </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> itemSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsItemSearchRecordType(org.osid.type.Type itemSearchRecordType) {
        return (getAdapteeManager().supportsItemSearchRecordType(itemSearchRecordType));
    }


    /**
     *  Gets the supported <code> Category </code> record types. 
     *
     *  @return a list containing the supported <code> Category </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCategoryRecordTypes() {
        return (getAdapteeManager().getCategoryRecordTypes());
    }


    /**
     *  Tests if the given <code> Category </code> record type is supported. 
     *
     *  @param  categoryRecordType a <code> Type </code> indicating an <code> 
     *          Category </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> categoryRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCategoryRecordType(org.osid.type.Type categoryRecordType) {
        return (getAdapteeManager().supportsCategoryRecordType(categoryRecordType));
    }


    /**
     *  Gets the supported <code> Category </code> search record types. 
     *
     *  @return a list containing the supported <code> Category </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCategorySearchRecordTypes() {
        return (getAdapteeManager().getCategorySearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Category </code> search record type is 
     *  supported. 
     *
     *  @param  categorySearchRecordType a <code> Type </code> indicating an 
     *          <code> Category </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> categorySearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCategorySearchRecordType(org.osid.type.Type categorySearchRecordType) {
        return (getAdapteeManager().supportsCategorySearchRecordType(categorySearchRecordType));
    }


    /**
     *  Gets the supported <code> Entry </code> record types. 
     *
     *  @return a list containing the supported <code> Entry </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getEntryRecordTypes() {
        return (getAdapteeManager().getEntryRecordTypes());
    }


    /**
     *  Tests if the given <code> Entry </code> record type is supported. 
     *
     *  @param  entryRecordType a <code> Type </code> indicating an <code> 
     *          Entry </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> entryRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsEntryRecordType(org.osid.type.Type entryRecordType) {
        return (getAdapteeManager().supportsEntryRecordType(entryRecordType));
    }


    /**
     *  Gets the supported <code> Entry </code> search record types. 
     *
     *  @return a list containing the supported <code> Entry </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getEntrySearchRecordTypes() {
        return (getAdapteeManager().getEntrySearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Entry </code> search record type is 
     *  supported. 
     *
     *  @param  entrySearchRecordType a <code> Type </code> indicating an 
     *          <code> Entry </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> entrySearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsEntrySearchRecordType(org.osid.type.Type entrySearchRecordType) {
        return (getAdapteeManager().supportsEntrySearchRecordType(entrySearchRecordType));
    }


    /**
     *  Gets the supported <code> Period </code> record types. 
     *
     *  @return a list containing the supported <code> Period </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPeriodRecordTypes() {
        return (getAdapteeManager().getPeriodRecordTypes());
    }


    /**
     *  Tests if the given <code> Period </code> record type is supported. 
     *
     *  @param  periodRecordType a <code> Type </code> indicating a <code> 
     *          Period </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> periodRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPeriodRecordType(org.osid.type.Type periodRecordType) {
        return (getAdapteeManager().supportsPeriodRecordType(periodRecordType));
    }


    /**
     *  Gets the supported <code> Period </code> search record types. 
     *
     *  @return a list containing the supported <code> Period </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPeriodSearchRecordTypes() {
        return (getAdapteeManager().getPeriodSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Period </code> search record type is 
     *  supported. 
     *
     *  @param  periodSearchRecordType a <code> Type </code> indicating a 
     *          <code> Period </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> periodSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPeriodSearchRecordType(org.osid.type.Type periodSearchRecordType) {
        return (getAdapteeManager().supportsPeriodSearchRecordType(periodSearchRecordType));
    }


    /**
     *  Gets the supported <code> Business </code> record types. 
     *
     *  @return a list containing the supported <code> Business </code> types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBusinessRecordTypes() {
        return (getAdapteeManager().getBusinessRecordTypes());
    }


    /**
     *  Tests if the given <code> Business </code> record type is supported. 
     *
     *  @param  businessRecordType a <code> Type </code> indicating an <code> 
     *          Business </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> businessRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBusinessRecordType(org.osid.type.Type businessRecordType) {
        return (getAdapteeManager().supportsBusinessRecordType(businessRecordType));
    }


    /**
     *  Gets the supported <code> Business </code> search record types. 
     *
     *  @return a list containing the supported <code> Business </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBusinessSearchRecordTypes() {
        return (getAdapteeManager().getBusinessSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Business </code> search record type is 
     *  supported. 
     *
     *  @param  businessSearchRecordType a <code> Type </code> indicating an 
     *          <code> Business </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> businessSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBusinessSearchRecordType(org.osid.type.Type businessSearchRecordType) {
        return (getAdapteeManager().supportsBusinessSearchRecordType(businessSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the customer 
     *  lookup service. 
     *
     *  @return a <code> CustomerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCustomerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerLookupSession getCustomerLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCustomerLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the customer 
     *  lookup service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the business 
     *  @return a <code> CustomerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCustomerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerLookupSession getCustomerLookupSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCustomerLookupSessionForBusiness(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the customer query 
     *  service. 
     *
     *  @return a <code> CustomerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCustomerQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerQuerySession getCustomerQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCustomerQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the customer query 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> CustomerQuerySession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCustomerQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerQuerySession getCustomerQuerySessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCustomerQuerySessionForBusiness(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the customer 
     *  search service. 
     *
     *  @return a <code> CustomerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCustomerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerSearchSession getCustomerSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCustomerSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the customer 
     *  search service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> CustomerSearchSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCustomerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerSearchSession getCustomerSearchSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCustomerSearchSessionForBusiness(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the customer 
     *  administration service. 
     *
     *  @return a <code> CustomerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCustomerAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerAdminSession getCustomerAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCustomerAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the customer 
     *  administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> CustomerAdminSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCustomerAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerAdminSession getCustomerAdminSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCustomerAdminSessionForBusiness(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the customer 
     *  notification service. 
     *
     *  @param  customerReceiver the notification callback 
     *  @return a <code> CustomerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> customerReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCustomerNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerNotificationSession getCustomerNotificationSession(org.osid.billing.CustomerReceiver customerReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCustomerNotificationSession(customerReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the customer 
     *  notification service for the given business. 
     *
     *  @param  customerReceiver the notification callback 
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> CustomerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> customerReceiver </code> 
     *          or <code> businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCustomerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerNotificationSession getCustomerNotificationSessionForBusiness(org.osid.billing.CustomerReceiver customerReceiver, 
                                                                                                  org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCustomerNotificationSessionForBusiness(customerReceiver, businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup customer/catalog 
     *  mappings. 
     *
     *  @return a <code> CustomerCustomerBusinessSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCustomerBusiness() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerBusinessSession getCustomerBusinessSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCustomerBusinessSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  customers to businesses. 
     *
     *  @return a <code> CustomerBusinessAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCustomerBusinessAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerBusinessAssignmentSession getCustomerBusinessAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCustomerBusinessAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the customer smart 
     *  business service. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> CustomerSmartBusinessSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCustomerSmartBusiness() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerSmartBusinessSession getCustomerSmartBusinessSession(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCustomerSmartBusinessSession(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item lookup 
     *  service. 
     *
     *  @return an <code> ItemSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.ItemLookupSession getItemLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getItemLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item lookup 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the business 
     *  @return an <code> ItemLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.ItemLookupSession getItemLookupSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getItemLookupSessionForBusiness(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item query 
     *  service. 
     *
     *  @return an <code> ItemQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerQuerySession getItemQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getItemQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item query 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return an <code> ItemQuerySession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerQuerySession getItemQuerySessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getItemQuerySessionForBusiness(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item search 
     *  service. 
     *
     *  @return an <code> ItemSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.ItemSearchSession getItemSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getItemSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item search 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return an <code> ItemSearchSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.ItemSearchSession getItemSearchSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getItemSearchSessionForBusiness(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item 
     *  administration service. 
     *
     *  @return an <code> ItemAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.ItemAdminSession getItemAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getItemAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item 
     *  administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return an <code> ItemAdminSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.ItemAdminSession getItemAdminSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getItemAdminSessionForBusiness(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item 
     *  notification service. 
     *
     *  @param  itemReceiver the notification callback 
     *  @return an <code> ItemNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> itemReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsItemNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.ItemNotificationSession getItemNotificationSession(org.osid.billing.ItemReceiver itemReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getItemNotificationSession(itemReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item 
     *  notification service for the given business. 
     *
     *  @param  itemReceiver the notification callback 
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return an <code> ItemNotificationSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> itemReceiver </code> or 
     *          <code> businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsItemNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.ItemNotificationSession getItemNotificationSessionForBusiness(org.osid.billing.ItemReceiver itemReceiver, 
                                                                                          org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getItemNotificationSessionForBusiness(itemReceiver, businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup item/catalog mappings. 
     *
     *  @return an <code> ItemBusinessSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemBusiness() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.ItemBusinessSession getItemBusinessSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getItemBusinessSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning items to 
     *  businesses. 
     *
     *  @return an <code> ItemBusinessAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsItemBusinessAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.billing.ItemBusinessAssignmentSession getItemBusinessAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getItemBusinessAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item smart 
     *  business service. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return an <code> ItemSmartBusinessSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsItemSmartBusiness() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.ItemSmartBusinessSession getItemSmartBusinessSession(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getItemSmartBusinessSession(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the category 
     *  lookup service. 
     *
     *  @return a <code> CategorySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCategoryLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CategoryLookupSession getCategoryLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCategoryLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the category 
     *  lookup service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the business 
     *  @return a <code> CategoryLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCategoryLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CategoryLookupSession getCategoryLookupSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCategoryLookupSessionForBusiness(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the category query 
     *  service. 
     *
     *  @return a <code> CategoryQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCategoryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerQuerySession getCategoryQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCategoryQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the category query 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> CategoryQuerySession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCategoryQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerQuerySession getCategoryQuerySessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCategoryQuerySessionForBusiness(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the category 
     *  search service. 
     *
     *  @return a <code> CategorySearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCategorySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CategorySearchSession getCategorySearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCategorySearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the category 
     *  search service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> CategorySearchSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCategorySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CategorySearchSession getCategorySearchSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCategorySearchSessionForBusiness(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the category 
     *  administration service. 
     *
     *  @return a <code> CategoryAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCategoryAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CategoryAdminSession getCategoryAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCategoryAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the category 
     *  administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> CategoryAdminSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCategoryAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CategoryAdminSession getCategoryAdminSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCategoryAdminSessionForBusiness(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the category 
     *  notification service. 
     *
     *  @param  categoryReceiver the notification callback 
     *  @return a <code> CategoryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> categoryReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCategoryNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CategoryNotificationSession getCategoryNotificationSession(org.osid.billing.CategoryReceiver categoryReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCategoryNotificationSession(categoryReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the category 
     *  notification service for the given business. 
     *
     *  @param  categoryReceiver the notification callback 
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> CategoryNotificationSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> categoryReceiver </code> 
     *          or <code> businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCategoryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CategoryNotificationSession getCategoryNotificationSessionForBusiness(org.osid.billing.CategoryReceiver categoryReceiver, 
                                                                                                  org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCategoryNotificationSessionForBusiness(categoryReceiver, businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup category/catalog 
     *  mappings. 
     *
     *  @return a <code> CategoryBusinessSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCategoryBusiness() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CategoryBusinessSession getCategoryBusinessSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCategoryBusinessSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  categories to businesses. 
     *
     *  @return a <code> CategoryBusinessAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCategoryBusinessAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.billing.CategoryBusinessAssignmentSession getCategoryBusinessAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCategoryBusinessAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the category smart 
     *  business service. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> CategorySmartBusinessSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCategorySmartBusiness() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CategorySmartBusinessSession getCategorySmartBusinessSession(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCategorySmartBusinessSession(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the entry lookup 
     *  service. 
     *
     *  @return an <code> EntryLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.EntryLookupSession getEntryLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEntryLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the entry lookup 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return an <code> EntryLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.EntryLookupSession getEntryLookupSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEntryLookupSessionForBusiness(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the entry query 
     *  service. 
     *
     *  @return an <code> EntryQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.EntryQuerySession getEntryQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEntryQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the entry query 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return an <code> EntryQuerySession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.EntryQuerySession getEntryQuerySessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEntryQuerySessionForBusiness(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the entry search 
     *  service. 
     *
     *  @return an <code> EntrySearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntrySearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.EntrySearchSession getEntrySearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEntrySearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the entry search 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return an <code> EntrySearchSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntrySearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.EntrySearchSession getEntrySearchSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEntrySearchSessionForBusiness(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the entry 
     *  administration service. 
     *
     *  @return an <code> EntryAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.EntryAdminSession getEntryAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEntryAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the entry 
     *  administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return an <code> EntryAdminSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.EntryAdminSession getEntryAdminSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEntryAdminSessionForBusiness(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the entry 
     *  notification service. 
     *
     *  @param  entryReceiver the notification callback 
     *  @return an <code> EntryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> entryReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntryNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.EntryNotificationSession getEntryNotificationSession(org.osid.billing.EntryReceiver entryReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEntryNotificationSession(entryReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the entry 
     *  notification service for the given business. 
     *
     *  @param  entryReceiver the notification callback 
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return an <code> EntryNotificationSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> entryReceiver </code> or 
     *          <code> businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.EntryNotificationSession getEntryNotificationSessionForBusiness(org.osid.billing.EntryReceiver entryReceiver, 
                                                                                            org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEntryNotificationSessionForBusiness(entryReceiver, businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup entry/catalog mappings. 
     *
     *  @return an <code> EntryBusinessSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryBusiness() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.EntryBusinessSession getEntryBusinessSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEntryBusinessSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning entries 
     *  to businesses. 
     *
     *  @return an <code> EntryBusinessAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntryBusinessAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.billing.EntryBusinessAssignmentSession getEntryBusinessAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEntryBusinessAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the entry smart 
     *  business service. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return an <code> EntrySmartBusinessSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntrySmartBusiness() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.EntrySmartBusinessSession getEntrySmartBusinessSession(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEntrySmartBusinessSession(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the period lookup 
     *  service. 
     *
     *  @return a <code> PeriodLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPeriodLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.PeriodLookupSession getPeriodLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPeriodLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the period lookup 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> PeriodLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPeriodLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.PeriodLookupSession getPeriodLookupSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPeriodLookupSessionForBusiness(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the period query 
     *  service. 
     *
     *  @return a <code> PeriodQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPeriodQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.PeriodQuerySession getPeriodQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPeriodQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the period query 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> PeriodQuerySession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPeriodQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.PeriodQuerySession getPeriodQuerySessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPeriodQuerySessionForBusiness(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the period search 
     *  service. 
     *
     *  @return a <code> PeriodSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPeriodSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.PeriodSearchSession getPeriodSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPeriodSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the period search 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> PeriodSearchSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPeriodSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.PeriodSearchSession getPeriodSearchSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPeriodSearchSessionForBusiness(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the period 
     *  administration service. 
     *
     *  @return a <code> PeriodAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPeriodAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.PeriodAdminSession getPeriodAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPeriodAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the period 
     *  administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> PeriodAdminSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPeriodAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.PeriodAdminSession getPeriodAdminSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPeriodAdminSessionForBusiness(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the period 
     *  notification service. 
     *
     *  @param  periodReceiver the notification callback 
     *  @return a <code> PeriodNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> periodReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPeriodNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.PeriodNotificationSession getPeriodNotificationSession(org.osid.billing.PeriodReceiver periodReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPeriodNotificationSession(periodReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the period 
     *  notification service for the given business. 
     *
     *  @param  periodReceiver the notification callback 
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> PeriodNotificationSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> periodReceiver </code> 
     *          or <code> businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPeriodNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.PeriodNotificationSession getPeriodNotificationSessionForBusiness(org.osid.billing.PeriodReceiver periodReceiver, 
                                                                                              org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPeriodNotificationSessionForBusiness(periodReceiver, businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup period/catalog mappings. 
     *
     *  @return a <code> PeriodBusinessSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPeriodBusiness() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.PeriodBusinessSession getPeriodBusinessSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPeriodBusinessSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning periods 
     *  to businesses. 
     *
     *  @return a <code> PeriodBusinessAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPeriodBusinessAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.billing.PeriodBusinessAssignmentSession getPeriodBusinessAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPeriodBusinessAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the period smart 
     *  business service. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> PeriodSmartBusinessSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPeriodSmartBusiness() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.PeriodSmartBusinessSession getPeriodSmartBusinessSession(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPeriodSmartBusinessSession(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the business 
     *  lookup service. 
     *
     *  @return a <code> BusinessLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBusinessLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.BusinessLookupSession getBusinessLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBusinessLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the business query 
     *  service. 
     *
     *  @return a <code> BusinessQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBusinessQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.BusinessQuerySession getBusinessQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBusinessQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the business 
     *  search service. 
     *
     *  @return a <code> BusinessSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBusinessSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.BusinessSearchSession getBusinessSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBusinessSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the business 
     *  administrative service. 
     *
     *  @return a <code> BusinessAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBusinessAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.BusinessAdminSession getBusinessAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBusinessAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the business 
     *  notification service. 
     *
     *  @param  businessReceiver the notification callback 
     *  @return a <code> BusinessNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> businessReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBusinessNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.BusinessNotificationSession getBusinessNotificationSession(org.osid.billing.BusinessReceiver businessReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBusinessNotificationSession(businessReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the business 
     *  hierarchy service. 
     *
     *  @return a <code> BusinessHierarchySession </code> for businesses 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBusinessHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.BusinessHierarchySession getBusinessHierarchySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBusinessHierarchySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the business 
     *  hierarchy design service. 
     *
     *  @return a <code> HierarchyDesignSession </code> for businesses 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBusinessHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.billing.BusinessHierarchyDesignSession getBusinessHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBusinessHierarchyDesignSession());
    }


    /**
     *  Gets a <code> BillingBatchManager. </code> 
     *
     *  @return a <code> BillingBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBillingBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.batch.BillingBatchManager getBillingBatchManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBillingBatchManager());
    }


    /**
     *  Gets a <code> BillingPaymentManager. </code> 
     *
     *  @return a <code> BillingPaymentManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBillingPayment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.BillingPaymentManager getBillingPaymentManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBillingPaymentManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
