//
// AbstractSyllabus.java
//
//     Defines a Syllabus.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.syllabus.syllabus.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Syllabus</code>.
 */

public abstract class AbstractSyllabus
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.course.syllabus.Syllabus {

    private org.osid.course.Course course;
    private final java.util.Collection<org.osid.course.syllabus.records.SyllabusRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the course. 
     *
     *  @return the course <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCourseId() {
        return (this.course.getId());
    }


    /**
     *  Gets the course. 
     *
     *  @return the course 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.Course getCourse()
        throws org.osid.OperationFailedException {

        return (this.course);
    }


    /**
     *  Sets the course.
     *
     *  @param course a course
     *  @throws org.osid.NullArgumentException
     *          <code>course</code> is <code>null</code>
     */

    protected void setCourse(org.osid.course.Course course) {
        nullarg(course, "course");
        this.course = course;
        return;
    }


    /**
     *  Tests if this syllabus supports the given record
     *  <code>Type</code>.
     *
     *  @param  syllabusRecordType a syllabus record type 
     *  @return <code>true</code> if the syllabusRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>syllabusRecordType</code> is <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type syllabusRecordType) {
        for (org.osid.course.syllabus.records.SyllabusRecord record : this.records) {
            if (record.implementsRecordType(syllabusRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given <code>Course</code>
     *  record <code>Type</code>.
     *
     *  @param  syllabusRecordType the syllabus record type 
     *  @return the syllabus record 
     *  @throws org.osid.NullArgumentException
     *          <code>syllabusRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(syllabusRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.syllabus.records.SyllabusRecord getSyllabusRecord(org.osid.type.Type syllabusRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.syllabus.records.SyllabusRecord record : this.records) {
            if (record.implementsRecordType(syllabusRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(syllabusRecordType + " is not supported");
    }


    /**
     *  Adds a record to this syllabus. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param syllabusRecord the syllabus record
     *  @param syllabusRecordType syllabus record type
     *  @throws org.osid.NullArgumentException
     *          <code>syllabusRecord</code> or
     *          <code>syllabusRecordTypesyllabus</code> is
     *          <code>null</code>
     */
            
    protected void addSyllabusRecord(org.osid.course.syllabus.records.SyllabusRecord syllabusRecord, 
                                     org.osid.type.Type syllabusRecordType) {
        
        nullarg(syllabusRecord, "syllabus record");
        addRecordType(syllabusRecordType);
        this.records.add(syllabusRecord);
        
        return;
    }
}
