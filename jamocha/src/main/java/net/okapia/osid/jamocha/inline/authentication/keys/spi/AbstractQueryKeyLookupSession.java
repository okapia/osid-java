//
// AbstractQueryKeyLookupSession.java
//
//    An inline adapter that maps a KeyLookupSession to
//    a KeyQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.authentication.keys.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a KeyLookupSession to
 *  a KeyQuerySession.
 */

public abstract class AbstractQueryKeyLookupSession
    extends net.okapia.osid.jamocha.authentication.keys.spi.AbstractKeyLookupSession
    implements org.osid.authentication.keys.KeyLookupSession {

    private final org.osid.authentication.keys.KeyQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryKeyLookupSession.
     *
     *  @param querySession the underlying key query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryKeyLookupSession(org.osid.authentication.keys.KeyQuerySession querySession) {
        nullarg(querySession, "key query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Agency</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Agency Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAgencyId() {
        return (this.session.getAgencyId());
    }


    /**
     *  Gets the <code>Agency</code> associated with this 
     *  session.
     *
     *  @return the <code>Agency</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.Agency getAgency()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getAgency());
    }


    /**
     *  Tests if this user can perform <code>Key</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupKeys() {
        return (this.session.canSearchKeys());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include keys in agencies which are children
     *  of this agency in the agency hierarchy.
     */

    @OSID @Override
    public void useFederatedAgencyView() {
        this.session.useFederatedAgencyView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this agency only.
     */

    @OSID @Override
    public void useIsolatedAgencyView() {
        this.session.useIsolatedAgencyView();
        return;
    }
    
     
    /**
     *  Gets the <code>Key</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Key</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Key</code> and
     *  retained for compatibility.
     *
     *  @param  keyId <code>Id</code> of the
     *          <code>Key</code>
     *  @return the key
     *  @throws org.osid.NotFoundException <code>keyId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>keyId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.keys.Key getKey(org.osid.id.Id keyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authentication.keys.KeyQuery query = getQuery();
        query.matchId(keyId, true);
        org.osid.authentication.keys.KeyList keys = this.session.getKeysByQuery(query);
        if (keys.hasNext()) {
            return (keys.getNextKey());
        } 
        
        throw new org.osid.NotFoundException(keyId + " not found");
    }


    /**
     *  Gets a <code>KeyList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  keys specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Keys</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  keyIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Key</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>keyIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeyList getKeysByIds(org.osid.id.IdList keyIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authentication.keys.KeyQuery query = getQuery();

        try (org.osid.id.IdList ids = keyIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getKeysByQuery(query));
    }


    /**
     *  Gets a <code>KeyList</code> corresponding to the given
     *  key genus <code>Type</code> which does not include
     *  keys of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  keys or an error results. Otherwise, the returned list
     *  may contain only those keys that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  keyGenusType a key genus type 
     *  @return the returned <code>Key</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>keyGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeyList getKeysByGenusType(org.osid.type.Type keyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authentication.keys.KeyQuery query = getQuery();
        query.matchGenusType(keyGenusType, true);
        return (this.session.getKeysByQuery(query));
    }


    /**
     *  Gets a <code>KeyList</code> corresponding to the given
     *  key genus <code>Type</code> and include any additional
     *  keys with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  keys or an error results. Otherwise, the returned list
     *  may contain only those keys that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  keyGenusType a key genus type 
     *  @return the returned <code>Key</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>keyGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeyList getKeysByParentGenusType(org.osid.type.Type keyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authentication.keys.KeyQuery query = getQuery();
        query.matchParentGenusType(keyGenusType, true);
        return (this.session.getKeysByQuery(query));
    }


    /**
     *  Gets a <code>KeyList</code> containing the given
     *  key record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  keys or an error results. Otherwise, the returned list
     *  may contain only those keys that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  keyRecordType a key record type 
     *  @return the returned <code>Key</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>keyRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeyList getKeysByRecordType(org.osid.type.Type keyRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authentication.keys.KeyQuery query = getQuery();
        query.matchRecordType(keyRecordType, true);
        return (this.session.getKeysByQuery(query));
    }

    
    /**
     *  Gets the agent key. 
     *
     *  @param  agentId the <code> Id </code> of the <code> Agent </code> 
     *  @return the key 
     *  @throws org.osid.NotFoundException <code> agentId </code> is not found 
     *          or no key exists 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.authentication.keys.Key getKeyForAgent(org.osid.id.Id agentId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authentication.keys.KeyQuery query = getQuery();
        query.matchAgentId(agentId, true);
        try (org.osid.authentication.keys.KeyList keys = this.session.getKeysByQuery(query)) {
            if (keys.hasNext()) {
                return (keys.getNextKey());
            }
        }

        throw new org.osid.NotFoundException("key for " + agentId + " not found");
    }


    /**
     *  Gets all <code>Keys</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  keys or an error results. Otherwise, the returned list
     *  may contain only those keys that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Keys</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeyList getKeys()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authentication.keys.KeyQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getKeysByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.authentication.keys.KeyQuery getQuery() {
        org.osid.authentication.keys.KeyQuery query = this.session.getKeyQuery();
        return (query);
    }
}
