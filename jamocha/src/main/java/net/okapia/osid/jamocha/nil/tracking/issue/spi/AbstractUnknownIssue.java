//
// AbstractUnknownIssue.java
//
//     Defines an unknown Issue.
//
//
// Tom Coppeto
// Okapia
// 8 December 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.nil.tracking.issue.spi;


/**
 *  Defines an unknown <code>Issue</code>.
 */

public abstract class AbstractUnknownIssue
    extends net.okapia.osid.jamocha.tracking.issue.spi.AbstractIssue
    implements org.osid.tracking.Issue {

    protected static final String OBJECT = "osid.tracking.Issue";


    /**
     *  Constructs a new <code>AbstractUnknownIssue</code>.
     */

    public AbstractUnknownIssue() {
        setId(net.okapia.osid.jamocha.nil.privateutil.UnknownId.valueOf(OBJECT));
        setDisplayName(net.okapia.osid.jamocha.nil.privateutil.DisplayName.valueOf(OBJECT));
        setDescription(net.okapia.osid.jamocha.nil.privateutil.Description.valueOf(OBJECT));

        setStartDate(net.okapia.osid.primordium.calendaring.GregorianUTCDateTime.unknown());
        setEndDate(net.okapia.osid.primordium.calendaring.GregorianUTCDateTime.unknown());

        setQueue(new net.okapia.osid.jamocha.nil.tracking.queue.UnknownQueue());
        setCustomer(new net.okapia.osid.jamocha.nil.resource.resource.UnknownResource());
        setTopic(new net.okapia.osid.jamocha.nil.ontology.subject.UnknownSubject());
        
        setPriorityType(new net.okapia.osid.primordium.type.BasicType("okapia.net", "issue priority", "unknown"));
        
        setCreator(new net.okapia.osid.jamocha.nil.resource.resource.UnknownResource());
        setCreatingAgent(new net.okapia.osid.jamocha.nil.authentication.agent.UnknownAgent());
        setCreatedDate(net.okapia.osid.primordium.calendaring.GregorianUTCDateTime.unknown());

        return;
    }


    /**
     *  Constructs a new <code>AbstractUnknownIssue</code> with all
     *  the optional methods enabled.
     *
     *  @param optional <code>true</code> to enable the optional
     *         methods
     */

    public AbstractUnknownIssue(boolean optional) {
        this();

        addDuplicateIssue(new net.okapia.osid.jamocha.nil.tracking.issue.UnknownIssue());
        setBranchedIssue(new net.okapia.osid.jamocha.nil.tracking.issue.UnknownIssue());
        setRootIssue(new net.okapia.osid.jamocha.nil.tracking.issue.UnknownIssue());

        setReopener(new net.okapia.osid.jamocha.nil.resource.resource.UnknownResource());
        setReopeningAgent(new net.okapia.osid.jamocha.nil.authentication.agent.UnknownAgent());
        setReopenedDate(net.okapia.osid.primordium.calendaring.GregorianUTCDateTime.unknown());

        setDueDate(net.okapia.osid.primordium.calendaring.GregorianUTCDateTime.unknown());
        addBlocker(new net.okapia.osid.jamocha.nil.tracking.issue.UnknownIssue());

        setResolver(new net.okapia.osid.jamocha.nil.resource.resource.UnknownResource());
        setResolvingAgent(new net.okapia.osid.jamocha.nil.authentication.agent.UnknownAgent());
        setResolvedDate(net.okapia.osid.primordium.calendaring.GregorianUTCDateTime.unknown());
        setResolutionType(new net.okapia.osid.primordium.type.BasicType("okapia.net", "issue resolution", "unknown"));

        setCloser(new net.okapia.osid.jamocha.nil.resource.resource.UnknownResource());
        setClosingAgent(new net.okapia.osid.jamocha.nil.authentication.agent.UnknownAgent());
        setClosedDate(net.okapia.osid.primordium.calendaring.GregorianUTCDateTime.unknown());

        setAssignedResource(new net.okapia.osid.jamocha.nil.resource.resource.UnknownResource());

        return;
    }
}
