//
// AbstractUtilityQuery.java
//
//     A template for making an Utility Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.metering.utility.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for utilities.
 */

public abstract class AbstractUtilityQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQuery
    implements org.osid.metering.UtilityQuery {

    private final java.util.Collection<org.osid.metering.records.UtilityQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets a meter <code> Id. </code> 
     *
     *  @param  meterId a meter <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> meterId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchMeterId(org.osid.id.Id meterId, boolean match) {
        return;
    }


    /**
     *  Clears the meter <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearMeterIdTerms() {
        return;
    }


    /**
     *  Tests if a meter query is available. 
     *
     *  @return <code> true </code> if a meter query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMeterQuery() {
        return (false);
    }


    /**
     *  Gets the query for a meter. 
     *
     *  @return the meter query 
     *  @throws org.osid.UnimplementedException <code> supportsMeterQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterQuery getMeterQuery() {
        throw new org.osid.UnimplementedException("supportsMeterQuery() is false");
    }


    /**
     *  Matches utilities with any meter. 
     *
     *  @param  match <code> true </code> to match utilities with any meter, 
     *          <code> false </code> to match utilities with no meters 
     */

    @OSID @Override
    public void matchAnyMeter(boolean match) {
        return;
    }


    /**
     *  Clears the meter query terms. 
     */

    @OSID @Override
    public void clearMeterTerms() {
        return;
    }


    /**
     *  Sets the utility <code> Id </code> for this query to match utilities 
     *  that have the specified utility as an ancestor. 
     *
     *  @param  utilityId a utility <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> utilityId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorUtilityId(org.osid.id.Id utilityId, boolean match) {
        return;
    }


    /**
     *  Clears the ancestor utility <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorUtilityIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> UtilityQuery </code> is available. 
     *
     *  @return <code> true </code> if a utility query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorUtilityQuery() {
        return (false);
    }


    /**
     *  Gets the query for a utility. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the utility query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorUtilityQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.UtilityQuery getAncestorUtilityQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorUtilityQuery() is false");
    }


    /**
     *  Clears the ancestor utility query terms. 
     */

    @OSID @Override
    public void clearAncestorUtilityTerms() {
        return;
    }


    /**
     *  Matches utilities with any ancestor. 
     *
     *  @param  match <code> true </code> to match utilities with any 
     *          ancestor, <code> false </code> to match root utilities 
     */

    @OSID @Override
    public void matchAnyAncestorUtility(boolean match) {
        return;
    }


    /**
     *  Sets the utility <code> Id </code> for this query to match utilities 
     *  that have the specified utility as a descendant. 
     *
     *  @param  utilityId a utility <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> utilityId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantUtilityId(org.osid.id.Id utilityId, 
                                         boolean match) {
        return;
    }


    /**
     *  Clears the descendant utility <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantUtilityIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> UtilityQuery </code> is available. 
     *
     *  @return <code> true </code> if a utility query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantUtilityQuery() {
        return (false);
    }


    /**
     *  Gets the query for a utility. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the utility query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantUtilityQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.metering.UtilityQuery getDescendantUtilityQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantUtilityQuery() is false");
    }


    /**
     *  Matches utilities with any descendant. 
     *
     *  @param  match <code> true </code> to match utilities with any 
     *          descendant, <code> false </code> to match leaf utilities 
     */

    @OSID @Override
    public void matchAnyDescendantUtility(boolean match) {
        return;
    }


    /**
     *  Clears the descendant utility query terms. 
     */

    @OSID @Override
    public void clearDescendantUtilityTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given utility query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an utility implementing the requested record.
     *
     *  @param utilityRecordType an utility record type
     *  @return the utility query record
     *  @throws org.osid.NullArgumentException
     *          <code>utilityRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(utilityRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.metering.records.UtilityQueryRecord getUtilityQueryRecord(org.osid.type.Type utilityRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.metering.records.UtilityQueryRecord record : this.records) {
            if (record.implementsRecordType(utilityRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(utilityRecordType + " is not supported");
    }


    /**
     *  Adds a record to this utility query. 
     *
     *  @param utilityQueryRecord utility query record
     *  @param utilityRecordType utility record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addUtilityQueryRecord(org.osid.metering.records.UtilityQueryRecord utilityQueryRecord, 
                                          org.osid.type.Type utilityRecordType) {

        addRecordType(utilityRecordType);
        nullarg(utilityQueryRecord, "utility query record");
        this.records.add(utilityQueryRecord);        
        return;
    }
}
