//
// AbstractQueryStepLookupSession.java
//
//    A StepQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.workflow.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A StepQuerySession adapter.
 */

public abstract class AbstractAdapterStepQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.workflow.StepQuerySession {

    private final org.osid.workflow.StepQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterStepQuerySession.
     *
     *  @param session the underlying step query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterStepQuerySession(org.osid.workflow.StepQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeOffice</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeOffice Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getOfficeId() {
        return (this.session.getOfficeId());
    }


    /**
     *  Gets the {@codeOffice</code> associated with this 
     *  session.
     *
     *  @return the {@codeOffice</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.Office getOffice()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getOffice());
    }


    /**
     *  Tests if this user can perform {@codeStep</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchSteps() {
        return (this.session.canSearchSteps());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include steps in offices which are children
     *  of this office in the office hierarchy.
     */

    @OSID @Override
    public void useFederatedOfficeView() {
        this.session.useFederatedOfficeView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this office only.
     */
    
    @OSID @Override
    public void useIsolatedOfficeView() {
        this.session.useIsolatedOfficeView();
        return;
    }
    
      
    /**
     *  Gets a step query. The returned query will not have an
     *  extension query.
     *
     *  @return the step query 
     */
      
    @OSID @Override
    public org.osid.workflow.StepQuery getStepQuery() {
        return (this.session.getStepQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  stepQuery the step query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code stepQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code stepQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.workflow.StepList getStepsByQuery(org.osid.workflow.StepQuery stepQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getStepsByQuery(stepQuery));
    }
}
