//
// AbstractFederatingMessageLookupSession.java
//
//     An abstract federating adapter for a MessageLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.messaging.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  MessageLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingMessageLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.messaging.MessageLookupSession>
    implements org.osid.messaging.MessageLookupSession {

    private boolean parallel = false;
    private org.osid.messaging.Mailbox mailbox = new net.okapia.osid.jamocha.nil.messaging.mailbox.UnknownMailbox();


    /**
     *  Constructs a new <code>AbstractFederatingMessageLookupSession</code>.
     */

    protected AbstractFederatingMessageLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.messaging.MessageLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Mailbox/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Mailbox Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getMailboxId() {
        return (this.mailbox.getId());
    }


    /**
     *  Gets the <code>Mailbox</code> associated with this 
     *  session.
     *
     *  @return the <code>Mailbox</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.Mailbox getMailbox()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.mailbox);
    }


    /**
     *  Sets the <code>Mailbox</code>.
     *
     *  @param  mailbox the mailbox for this session
     *  @throws org.osid.NullArgumentException <code>mailbox</code>
     *          is <code>null</code>
     */

    protected void setMailbox(org.osid.messaging.Mailbox mailbox) {
        nullarg(mailbox, "mailbox");
        this.mailbox = mailbox;
        return;
    }


    /**
     *  Tests if this user can perform <code>Message</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupMessages() {
        for (org.osid.messaging.MessageLookupSession session : getSessions()) {
            if (session.canLookupMessages()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Message</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeMessageView() {
        for (org.osid.messaging.MessageLookupSession session : getSessions()) {
            session.useComparativeMessageView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Message</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryMessageView() {
        for (org.osid.messaging.MessageLookupSession session : getSessions()) {
            session.usePlenaryMessageView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include messages in mailboxes which are children
     *  of this mailbox in the mailbox hierarchy.
     */

    @OSID @Override
    public void useFederatedMailboxView() {
        for (org.osid.messaging.MessageLookupSession session : getSessions()) {
            session.useFederatedMailboxView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this mailbox only.
     */

    @OSID @Override
    public void useIsolatedMailboxView() {
        for (org.osid.messaging.MessageLookupSession session : getSessions()) {
            session.useIsolatedMailboxView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Message</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Message</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Message</code> and
     *  retained for compatibility.
     *
     *  @param  messageId <code>Id</code> of the
     *          <code>Message</code>
     *  @return the message
     *  @throws org.osid.NotFoundException <code>messageId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>messageId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.Message getMessage(org.osid.id.Id messageId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.messaging.MessageLookupSession session : getSessions()) {
            try {
                return (session.getMessage(messageId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(messageId + " not found");
    }


    /**
     *  Gets a <code>MessageList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  messages specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Messages</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  messageIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Message</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>messageIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesByIds(org.osid.id.IdList messageIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.messaging.message.MutableMessageList ret = new net.okapia.osid.jamocha.messaging.message.MutableMessageList();

        try (org.osid.id.IdList ids = messageIds) {
            while (ids.hasNext()) {
                ret.addMessage(getMessage(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>MessageList</code> corresponding to the given
     *  message genus <code>Type</code> which does not include
     *  messages of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  messages or an error results. Otherwise, the returned list
     *  may contain only those messages that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  messageGenusType a message genus type 
     *  @return the returned <code>Message</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>messageGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesByGenusType(org.osid.type.Type messageGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.messaging.message.FederatingMessageList ret = getMessageList();

        for (org.osid.messaging.MessageLookupSession session : getSessions()) {
            ret.addMessageList(session.getMessagesByGenusType(messageGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>MessageList</code> corresponding to the given
     *  message genus <code>Type</code> and include any additional
     *  messages with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  messages or an error results. Otherwise, the returned list
     *  may contain only those messages that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  messageGenusType a message genus type 
     *  @return the returned <code>Message</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>messageGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesByParentGenusType(org.osid.type.Type messageGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.messaging.message.FederatingMessageList ret = getMessageList();

        for (org.osid.messaging.MessageLookupSession session : getSessions()) {
            ret.addMessageList(session.getMessagesByParentGenusType(messageGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>MessageList</code> containing the given
     *  message record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  messages or an error results. Otherwise, the returned list
     *  may contain only those messages that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  messageRecordType a message record type 
     *  @return the returned <code>Message</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>messageRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesByRecordType(org.osid.type.Type messageRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.messaging.message.FederatingMessageList ret = getMessageList();

        for (org.osid.messaging.MessageLookupSession session : getSessions()) {
            ret.addMessageList(session.getMessagesByRecordType(messageRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>MessageList</code> sent within the specified
     *  range inclusive. In plenary mode, the returned list contains
     *  all known messages or an error results. Otherwise, the
     *  returned list may contain only those messages that are
     *  accessible through this session.
     *
     *  @param  from starting date
     *  @param  to ending date
     *  @return the returned <code>Message</code> list
     *  @throws org.osid.InvalidArgumentException <code>to</code> is
     *          less than <code>from</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesBySentTime(org.osid.calendaring.DateTime from,
                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.messaging.message.FederatingMessageList ret = getMessageList();

        for (org.osid.messaging.MessageLookupSession session : getSessions()) {
            ret.addMessageList(session.getMessagesBySentTime(from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>MessageList</code> of the given genus type and
     *  sent within the specified range inclusive. In plenary mode,
     *  the returned list contains all known messages or an error
     *  results. Otherwise, the returned list may contain only those
     *  messages that are accessible through this session.
     *
     *  @param  messageGenusType a message genus type
     *  @param  from starting date
     *  @param  to ending date
     *  @return the returned <code>Message</code> list
     *  @throws org.osid.InvalidArgumentException <code>to</code> is
     *          less than <code>from</code>
     *  @throws org.osid.NullArgumentException
     *          <code>messageGenusType</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesBySentTimeAndGenusType(org.osid.type.Type messageGenusType,
                                                                            org.osid.calendaring.DateTime from,
                                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.messaging.message.FederatingMessageList ret = getMessageList();

        for (org.osid.messaging.MessageLookupSession session : getSessions()) {
            ret.addMessageList(session.getMessagesBySentTimeAndGenusType(messageGenusType, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>MessageList</code> sent by the specified
     *  sender. In plenary mode, the returned list contains all known
     *  messages or an error results. Otherwise, the returned list may
     *  contain only those messages that are accessible through this
     *  session.
     *
     *  @param  resourceId a resource Id
     *  @return the returned <code>Message</code> list
     *  @throws org.osid.NullArgumentException <code>resourceId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesFromSender(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.messaging.message.FederatingMessageList ret = getMessageList();

        for (org.osid.messaging.MessageLookupSession session : getSessions()) {
            ret.addMessageList(session.getMessagesFromSender(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>MessageList</code> of the given genus type and
     *  sender.  In plenary mode, the returned list contains all known
     *  messages or an error results. Otherwise, the returned list may
     *  contain only those messages that are accessible through this
     *  session.
     *
     *  @param  resourceId a resource Id
     *  @param  messageGenusType a message genus type
     *  @return the returned <code>Message</code> list
     *  @throws org.osid.NullArgumentException <code>resourceId</code>
     *          or <code>messageGenusType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesByGenusTypeFromSender(org.osid.id.Id resourceId,
                                                                           org.osid.type.Type messageGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.messaging.message.FederatingMessageList ret = getMessageList();

        for (org.osid.messaging.MessageLookupSession session : getSessions()) {
            ret.addMessageList(session.getMessagesByGenusTypeFromSender(resourceId, messageGenusType));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>MessageList</code> sent by the specified sender
     *  and sent time. In plenary mode, the returned list contains all
     *  known messages or an error results. Otherwise, the returned
     *  list may contain only those messages that are accessible
     *  through this session
     *
     *  @param  resourceId a resource Id
     *  @param  from starting date
     *  @param  to ending date
     *  @return the returned <code>Message</code> list
     *  @throws org.osid.InvalidArgumentException <code>to</code> is
     *          less than <code>from</code>
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesBySentTimeFromSender(org.osid.id.Id resourceId,
                                                                          org.osid.calendaring.DateTime from,
                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.messaging.message.FederatingMessageList ret = getMessageList();

        for (org.osid.messaging.MessageLookupSession session : getSessions()) {
            ret.addMessageList(session.getMessagesBySentTimeFromSender(resourceId, from, to));
        }

        ret.noMore();
        return (ret);
    }

    
    /**
     *  Gets a <code>MessageList</code> of the given genus type and
     *  sent within the specified range inclusive. In plenary mode,
     *  the returned list contains all known messages or an error
     *  results. Otherwise, the returned list may contain only those
     *  messages that are accessible through this session.
     *
     *  @param  resourceId a resource Id
     *  @param  messageGenusType a message genus type
     *  @param  from starting date
     *  @param  to ending date
     *  @return the returned <code>Message</code> list
     *  @throws org.osid.InvalidArgumentException <code>to</code> is
     *          less than <code>from</code>
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code>,
     *          <code>messageGenusType</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesBySentTimeAndGenusTypeFromSender(org.osid.id.Id resourceId,
                                                                                      org.osid.type.Type messageGenusType,
                                                                                      org.osid.calendaring.DateTime from,
                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.messaging.message.FederatingMessageList ret = getMessageList();

        for (org.osid.messaging.MessageLookupSession session : getSessions()) {
            ret.addMessageList(session.getMessagesBySentTimeAndGenusTypeFromSender(resourceId, messageGenusType, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>MessageList</code> received by the specified
     *  recipient.  In plenary mode, the returned list contains all
     *  known messages or an error results. Otherwise, the returned
     *  list may contain only those messages that are accessible
     *  through this session.
     *
     *  @param  resourceId a resource Id
     *  @return the returned <code>Message</code> list
     *  @throws org.osid.NullArgumentException <code>resourceId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesForRecipient(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.messaging.message.FederatingMessageList ret = getMessageList();

        for (org.osid.messaging.MessageLookupSession session : getSessions()) {
            ret.addMessageList(session.getMessagesForRecipient(resourceId));
        }

        ret.noMore();
        return (ret);
    }

 
    /**
     *  Gets a <code>MessageList</code> of the given genus type and
     *  recipient.  In plenary mode, the returned list contains all known
     *  messages or an error results. Otherwise, the returned list may
     *  contain only those messages that are accessible through this
     *  session.
     *
     *  @param  resourceId a resource Id
     *  @param  messageGenusType a message genus type
     *  @return the returned <code>Message</code> list
     *  @throws org.osid.NullArgumentException <code>resourceId</code>
     *          or <code>messageGenusType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesByGenusTypeForRecipient(org.osid.id.Id resourceId,
                                                                             org.osid.type.Type messageGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.messaging.message.FederatingMessageList ret = getMessageList();

        for (org.osid.messaging.MessageLookupSession session : getSessions()) {
            ret.addMessageList(session.getMessagesByGenusTypeForRecipient(resourceId, messageGenusType));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>MessageList</code> recipient by the specified
     *  recipient and received time. In plenary mode, the returned
     *  list contains all known messages or an error
     *  results. Otherwise, the returned list may contain only those
     *  messages that are accessible through this session
     *
     *  @param  resourceId a resource Id
     *  @param  from starting date
     *  @param  to ending date
     *  @return the returned <code>Message</code> list
     *  @throws org.osid.InvalidArgumentException <code>to</code> is
     *          less than <code>from</code>
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesByReceivedTimeForRecipient(org.osid.id.Id resourceId,
                                                                                org.osid.calendaring.DateTime from,
                                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.messaging.message.FederatingMessageList ret = getMessageList();

        for (org.osid.messaging.MessageLookupSession session : getSessions()) {
            ret.addMessageList(session.getMessagesByReceivedTimeForRecipient(resourceId, from, to));
        }

        ret.noMore();
        return (ret);
    }

    
    /**
     *  Gets a <code>MessageList</code> of the given genus type,
     *  recipient, and received within the specified range
     *  inclusive. In plenary mode, the returned list contains all
     *  known messages or an error results. Otherwise, the returned
     *  list may contain only those messages that are accessible
     *  through this session.
     *
     *  @param  resourceId a resource Id
     *  @param  messageGenusType a message genus type
     *  @param  from starting date
     *  @param  to ending date
     *  @return the returned <code>Message</code> list
     *  @throws org.osid.InvalidArgumentException <code>to</code> is
     *          less than <code>from</code>
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code>,
     *          <code>messageGenusType</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesByReceivedTimeAndGenusTypeForRecipient(org.osid.id.Id resourceId,
                                                                                            org.osid.type.Type messageGenusType,
                                                                                            org.osid.calendaring.DateTime from,
                                                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.messaging.message.FederatingMessageList ret = getMessageList();

        for (org.osid.messaging.MessageLookupSession session : getSessions()) {
            ret.addMessageList(session.getMessagesByReceivedTimeAndGenusTypeForRecipient(resourceId, messageGenusType, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>MessageList</code> sent by the specified sender
     *  and received by the specified recipient. In plenary mode, the
     *  returned list contains all known messages or an error
     *  results. Otherwise, the returned list may contain only those
     *  messages that are accessible through this session.
     *
     *  @param  senderResourceId a resource Id
     *  @param  recipientResourceId a resource Id
     *  @return the returned <code>Message</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>senderResourceId</code> or
     *          <code>recipientResourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesFromSenderForRecipient(org.osid.id.Id senderResourceId,
                                                                            org.osid.id.Id recipientResourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.messaging.message.FederatingMessageList ret = getMessageList();

        for (org.osid.messaging.MessageLookupSession session : getSessions()) {
            ret.addMessageList(session.getMessagesFromSenderForRecipient(senderResourceId, recipientResourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>MessageList </code>of the given genus type sent
     *  by the specified sender and received by the specified
     *  recipient. In plenary mode, the returned list contains all
     *  known messages or an error results. Otherwise, the returned
     *  list may contain only those messages that are accessible
     *  through this session.
     *
     *  @param  senderResourceId a resource Id
     *  @param  recipientResourceId a resource Id
     *  @param  messageGenusType a message genus type
     *  @return the returned <code>Message </code>list
     *  @throws org.osid.NullArgumentException <code>senderResourceId,
     *          recipientResourceId </code>or <code>messageGenusType
     *          </code> is <code>null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesByGenusTypeFromSenderForRecipient(org.osid.id.Id senderResourceId,
                                                                                       org.osid.id.Id recipientResourceId,
                                                                                       org.osid.type.Type messageGenusType)
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.messaging.message.FederatingMessageList ret = getMessageList();

        for (org.osid.messaging.MessageLookupSession session : getSessions()) {
            ret.addMessageList(session.getMessagesByGenusTypeFromSenderForRecipient(senderResourceId, recipientResourceId, messageGenusType));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>MessageList</code>by the specified sender,
     *  recipient, and received time. In plenary mode, the returned
     *  list contains all known messages or an error
     *  results. Otherwise, the returned list may contain only those
     *  messages that are accessible through this session.
     *
     *  @param  senderResourceId a resource Id
     *  @param  recipientResourceId a resource Id
     *  @param  from starting date
     *  @param  to ending date
     *  @return the returned <code>Message</code>list
     *  @throws org.osid.InvalidArgumentException <code>to</code>is
     *          less than <code>from</code>
     *  @throws org.osid.NullArgumentException <code>senderResourceId,
     *          recipientResourceId, from</code>or <code>to</code>is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesByReceivedTimeFromSenderForRecipient(org.osid.id.Id senderResourceId,
                                                                                          org.osid.id.Id recipientResourceId,
                                                                                          org.osid.calendaring.DateTime from,
                                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
         
        net.okapia.osid.jamocha.adapter.federator.messaging.message.FederatingMessageList ret = getMessageList();

        for (org.osid.messaging.MessageLookupSession session : getSessions()) {
            ret.addMessageList(session.getMessagesByReceivedTimeFromSenderForRecipient(senderResourceId, recipientResourceId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>MessageList</code>of the given genus type and
     *  received by the specified resource and received time. In
     *  plenary mode, the returned list contains all known messages or
     *  an error results.  Otherwise, the returned list may contain
     *  only those messages that are accessible through this session.
     *
     *  @param  senderResourceId a resource Id
     *  @param  recipientResourceId a resource Id
     *  @param  messageGenusType a message genus type
     *  @param  from starting date
     *  @param  to ending date
     *  @return the returned <code>Message</code>list
     *  @throws org.osid.InvalidArgumentException <code>to</code>is
     *          less than <code>from</code>
     *  @throws org.osid.NullArgumentException <code>senderResourceId,
     *          recipientResourceId, messageGenusType, from</code>or
     *          <code> to</code>is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesByReceivedTimeAndGenusTypeFromSenderForRecipient(org.osid.id.Id senderResourceId,
                                                                                                      org.osid.id.Id recipientResourceId,
                                                                                                      org.osid.type.Type messageGenusType,
                                                                                                      org.osid.calendaring.DateTime from,
                                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.messaging.message.FederatingMessageList ret = getMessageList();

        for (org.osid.messaging.MessageLookupSession session : getSessions()) {
            ret.addMessageList(session.getMessagesByReceivedTimeAndGenusTypeFromSenderForRecipient(senderResourceId, recipientResourceId, messageGenusType, from, to));
        }

        ret.noMore();
        return (ret);
    }

    
    /**
     *  Gets all <code>Messages</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  messages or an error results. Otherwise, the returned list
     *  may contain only those messages that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Messages</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessages()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.messaging.message.FederatingMessageList ret = getMessageList();

        for (org.osid.messaging.MessageLookupSession session : getSessions()) {
            ret.addMessageList(session.getMessages());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.messaging.message.FederatingMessageList getMessageList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.messaging.message.ParallelMessageList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.messaging.message.CompositeMessageList());
        }
    }
}
