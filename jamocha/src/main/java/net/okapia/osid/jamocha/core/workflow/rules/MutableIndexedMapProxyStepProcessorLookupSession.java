//
// MutableIndexedMapProxyStepProcessorLookupSession
//
//    Implements a StepProcessor lookup service backed by a collection of
//    stepProcessors indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.workflow.rules;


/**
 *  Implements a StepProcessor lookup service backed by a collection of
 *  stepProcessors. The step processors are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some stepProcessors may be compatible
 *  with more types than are indicated through these stepProcessor
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of step processors can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyStepProcessorLookupSession
    extends net.okapia.osid.jamocha.core.workflow.rules.spi.AbstractIndexedMapStepProcessorLookupSession
    implements org.osid.workflow.rules.StepProcessorLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyStepProcessorLookupSession} with
     *  no step processor.
     *
     *  @param office the office
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code office} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyStepProcessorLookupSession(org.osid.workflow.Office office,
                                                       org.osid.proxy.Proxy proxy) {
        setOffice(office);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyStepProcessorLookupSession} with
     *  a single step processor.
     *
     *  @param office the office
     *  @param  stepProcessor an step processor
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code office},
     *          {@code stepProcessor}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyStepProcessorLookupSession(org.osid.workflow.Office office,
                                                       org.osid.workflow.rules.StepProcessor stepProcessor, org.osid.proxy.Proxy proxy) {

        this(office, proxy);
        putStepProcessor(stepProcessor);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyStepProcessorLookupSession} using
     *  an array of step processors.
     *
     *  @param office the office
     *  @param  stepProcessors an array of step processors
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code office},
     *          {@code stepProcessors}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyStepProcessorLookupSession(org.osid.workflow.Office office,
                                                       org.osid.workflow.rules.StepProcessor[] stepProcessors, org.osid.proxy.Proxy proxy) {

        this(office, proxy);
        putStepProcessors(stepProcessors);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyStepProcessorLookupSession} using
     *  a collection of step processors.
     *
     *  @param office the office
     *  @param  stepProcessors a collection of step processors
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code office},
     *          {@code stepProcessors}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyStepProcessorLookupSession(org.osid.workflow.Office office,
                                                       java.util.Collection<? extends org.osid.workflow.rules.StepProcessor> stepProcessors,
                                                       org.osid.proxy.Proxy proxy) {
        this(office, proxy);
        putStepProcessors(stepProcessors);
        return;
    }

    
    /**
     *  Makes a {@code StepProcessor} available in this session.
     *
     *  @param  stepProcessor a step processor
     *  @throws org.osid.NullArgumentException {@code stepProcessor{@code 
     *          is {@code null}
     */

    @Override
    public void putStepProcessor(org.osid.workflow.rules.StepProcessor stepProcessor) {
        super.putStepProcessor(stepProcessor);
        return;
    }


    /**
     *  Makes an array of step processors available in this session.
     *
     *  @param  stepProcessors an array of step processors
     *  @throws org.osid.NullArgumentException {@code stepProcessors{@code 
     *          is {@code null}
     */

    @Override
    public void putStepProcessors(org.osid.workflow.rules.StepProcessor[] stepProcessors) {
        super.putStepProcessors(stepProcessors);
        return;
    }


    /**
     *  Makes collection of step processors available in this session.
     *
     *  @param  stepProcessors a collection of step processors
     *  @throws org.osid.NullArgumentException {@code stepProcessor{@code 
     *          is {@code null}
     */

    @Override
    public void putStepProcessors(java.util.Collection<? extends org.osid.workflow.rules.StepProcessor> stepProcessors) {
        super.putStepProcessors(stepProcessors);
        return;
    }


    /**
     *  Removes a StepProcessor from this session.
     *
     *  @param stepProcessorId the {@code Id} of the step processor
     *  @throws org.osid.NullArgumentException {@code stepProcessorId{@code  is
     *          {@code null}
     */

    @Override
    public void removeStepProcessor(org.osid.id.Id stepProcessorId) {
        super.removeStepProcessor(stepProcessorId);
        return;
    }    
}
