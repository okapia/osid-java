//
// AbstractIndexedMapDirectoryLookupSession.java
//
//    A simple framework for providing a Directory lookup service
//    backed by a fixed collection of directories with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.filing.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Directory lookup service backed by a
 *  fixed collection of directories. The directories are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some directories may be compatible
 *  with more types than are indicated through these directory
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Directories</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapDirectoryLookupSession
    extends AbstractMapDirectoryLookupSession
    implements org.osid.filing.DirectoryLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.filing.Directory> directoriesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.filing.Directory>());
    private final MultiMap<org.osid.type.Type, org.osid.filing.Directory> directoriesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.filing.Directory>());


    /**
     *  Makes a <code>Directory</code> available in this session.
     *
     *  @param  directory a directory
     *  @throws org.osid.NullArgumentException <code>directory<code> is
     *          <code>null</code>
     */

    @Override
    protected void putDirectory(org.osid.filing.Directory directory) {
        super.putDirectory(directory);

        this.directoriesByGenus.put(directory.getGenusType(), directory);
        
        try (org.osid.type.TypeList types = directory.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.directoriesByRecord.put(types.getNextType(), directory);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }

    
    /**
     *  Makes an array of directories available in this session.
     *
     *  @param  directories an array of directories
     *  @throws org.osid.NullArgumentException <code>directories<code>
     *          is <code>null</code>
     */

    @Override
    protected void putDirectories(org.osid.filing.Directory[] directories) {
        for (org.osid.filing.Directory directory : directories) {
            putDirectory(directory);
        }

        return;
    }


    /**
     *  Makes a collection of directories available in this session.
     *
     *  @param  directories a collection of directories
     *  @throws org.osid.NullArgumentException <code>directories<code>
     *          is <code>null</code>
     */

    @Override
    protected void putDirectories(java.util.Collection<? extends org.osid.filing.Directory> directories) {
        for (org.osid.filing.Directory directory : directories) {
            putDirectory(directory);
        }

        return;
    }


    /**
     *  Removes a directory from this session.
     *
     *  @param directoryId the <code>Id</code> of the directory
     *  @throws org.osid.NullArgumentException <code>directoryId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeDirectory(org.osid.id.Id directoryId) {
        org.osid.filing.Directory directory;
        try {
            directory = getDirectory(directoryId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.directoriesByGenus.remove(directory.getGenusType());

        try (org.osid.type.TypeList types = directory.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.directoriesByRecord.remove(types.getNextType(), directory);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeDirectory(directoryId);
        return;
    }


    /**
     *  Gets a <code>DirectoryList</code> corresponding to the given
     *  directory genus <code>Type</code> which does not include
     *  directories of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known directories or an error results. Otherwise,
     *  the returned list may contain only those directories that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  directoryGenusType a directory genus type 
     *  @return the returned <code>Directory</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>directoryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.filing.DirectoryList getDirectoriesByGenusType(org.osid.type.Type directoryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.filing.directory.ArrayDirectoryList(this.directoriesByGenus.get(directoryGenusType)));
    }


    /**
     *  Gets a <code>DirectoryList</code> containing the given
     *  directory record <code>Type</code>. In plenary mode, the
     *  returned list contains all known directories or an error
     *  results. Otherwise, the returned list may contain only those
     *  directories that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  directoryRecordType a directory record type 
     *  @return the returned <code>directory</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>directoryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.filing.DirectoryList getDirectoriesByRecordType(org.osid.type.Type directoryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.filing.directory.ArrayDirectoryList(this.directoriesByRecord.get(directoryRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.directoriesByGenus.clear();
        this.directoriesByRecord.clear();

        super.close();

        return;
    }
}
