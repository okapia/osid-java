//
// ScheduleMiter.java
//
//     Defines a Schedule miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.calendaring.schedule;


/**
 *  Defines a <code>Schedule</code> miter for use with the builders.
 */

public interface ScheduleMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidObjectMiter,
            org.osid.calendaring.Schedule {


    /**
     *  Sets the schedule slot.
     *
     *  @param scheduleSlot a schedule slot
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleSlot</code> is <code>null</code>
     */

    public void setScheduleSlot(org.osid.calendaring.ScheduleSlot scheduleSlot);


    /**
     *  Sets the time period.
     *
     *  @param timePeriod a time period
     *  @throws org.osid.NullArgumentException <code>timePeriod</code>
     *          is <code>null</code>
     */

    public void setTimePeriod(org.osid.calendaring.TimePeriod timePeriod);


    /**
     *  Sets the schedule start.
     *
     *  @param date a schedule start
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public void setScheduleStart(org.osid.calendaring.DateTime date);


    /**
     *  Sets the schedule end.
     *
     *  @param date a schedule end
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public void setScheduleEnd(org.osid.calendaring.DateTime date);


    /**
     *  Sets the limit.
     *
     *  @param limit a limit
     *  @throws org.osid.InvalidArgumentException <code>limit</code>
     *          is negative
     */

    public void setLimit(long limit);


    /**
     *  Sets the total duration.
     *
     *  @param duration the total duration
     *  @throws org.osid.NullArgumentException <code>duration</code>
     *          is <code>null</code>
     */

    public void setTotalDuration(org.osid.calendaring.Duration duration);


    /**
     *  Sets the location description.
     *
     *  @param description a location description
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    public void setLocationDescription(org.osid.locale.DisplayText description);


    /**
     *  Sets the location.
     *
     *  @param location a location
     *  @throws org.osid.NullArgumentException <code>location</code>
     *          is <code>null</code>
     */

    public void setLocation(org.osid.mapping.Location location);


    /**
     *  Adds a Schedule record.
     *
     *  @param record a schedule record
     *  @param recordType the type of schedule record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addScheduleRecord(org.osid.calendaring.records.ScheduleRecord record, org.osid.type.Type recordType);
}       


