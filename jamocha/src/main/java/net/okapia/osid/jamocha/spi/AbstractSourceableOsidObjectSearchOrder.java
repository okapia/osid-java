//
// AbstractSourceableOsidObjectSearchOrder.java
//
//     Defines a simple OSID search order to draw from.
//
//
// Tom Coppeto
// Okapia
// 14 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a simple OsidSearchOrder to extend. This class
 *  does nothing.
 */

public abstract class AbstractSourceableOsidObjectSearchOrder
    extends AbstractOsidObjectSearchOrder
    implements org.osid.OsidObjectSearchOrder,
               org.osid.OsidSourceableSearchOrder {
    
    private final OsidSourceableSearchOrder order = new OsidSourceableSearchOrder();


    /**
     *  Specifies a preference for ordering the results by
     *  provider. The element of the provider to order is not
     *  specified but may be managed through the provider ordering
     *  interface.
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException {@code style} is {@code
     *          null}
     */

    @OSID @Override
    public void orderByProvider(org.osid.SearchOrderStyle style) {
        this.order.orderByProvider(style);
        return;
    }


    /**
     *  Tests if a {@code ProviderSearchOrder} interface is
     *  available.
     *
     *  @return {@code true} if a provider search order interface is 
     *          available, {@code false} otherwise 
     */

    @OSID @Override
    public boolean supportsProviderSearchOrder() {
        return (this.order.supportsProviderSearchOrder());
    }


    /**
     *  Gets the search order interface for a provider. 
     *
     *  @return the provider search order interface 
     *  @throws org.osid.UnimplementedException {@code
     *          supportsProviderSearchOrder()} is {@code false}
     *          supportsProviderSearchOrder()} is {@code true}.
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getProviderSearchOrder() {
        return (this.order.getProviderSearchOrder());
    }


    protected class OsidSourceableSearchOrder
        extends AbstractOsidSourceableSearchOrder
        implements org.osid.OsidSourceableSearchOrder {
    }
}
