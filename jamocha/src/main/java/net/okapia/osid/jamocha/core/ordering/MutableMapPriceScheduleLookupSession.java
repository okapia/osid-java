//
// MutableMapPriceScheduleLookupSession
//
//    Implements a PriceSchedule lookup service backed by a collection of
//    priceSchedules that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.ordering;


/**
 *  Implements a PriceSchedule lookup service backed by a collection of
 *  price schedules. The price schedules are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of price schedules can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapPriceScheduleLookupSession
    extends net.okapia.osid.jamocha.core.ordering.spi.AbstractMapPriceScheduleLookupSession
    implements org.osid.ordering.PriceScheduleLookupSession {


    /**
     *  Constructs a new {@code MutableMapPriceScheduleLookupSession}
     *  with no price schedules.
     *
     *  @param store the store
     *  @throws org.osid.NullArgumentException {@code store} is
     *          {@code null}
     */

      public MutableMapPriceScheduleLookupSession(org.osid.ordering.Store store) {
        setStore(store);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapPriceScheduleLookupSession} with a
     *  single priceSchedule.
     *
     *  @param store the store  
     *  @param priceSchedule a price schedule
     *  @throws org.osid.NullArgumentException {@code store} or
     *          {@code priceSchedule} is {@code null}
     */

    public MutableMapPriceScheduleLookupSession(org.osid.ordering.Store store,
                                           org.osid.ordering.PriceSchedule priceSchedule) {
        this(store);
        putPriceSchedule(priceSchedule);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapPriceScheduleLookupSession}
     *  using an array of price schedules.
     *
     *  @param store the store
     *  @param priceSchedules an array of price schedules
     *  @throws org.osid.NullArgumentException {@code store} or
     *          {@code priceSchedules} is {@code null}
     */

    public MutableMapPriceScheduleLookupSession(org.osid.ordering.Store store,
                                           org.osid.ordering.PriceSchedule[] priceSchedules) {
        this(store);
        putPriceSchedules(priceSchedules);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapPriceScheduleLookupSession}
     *  using a collection of price schedules.
     *
     *  @param store the store
     *  @param priceSchedules a collection of price schedules
     *  @throws org.osid.NullArgumentException {@code store} or
     *          {@code priceSchedules} is {@code null}
     */

    public MutableMapPriceScheduleLookupSession(org.osid.ordering.Store store,
                                           java.util.Collection<? extends org.osid.ordering.PriceSchedule> priceSchedules) {

        this(store);
        putPriceSchedules(priceSchedules);
        return;
    }

    
    /**
     *  Makes a {@code PriceSchedule} available in this session.
     *
     *  @param priceSchedule a price schedule
     *  @throws org.osid.NullArgumentException {@code priceSchedule{@code  is
     *          {@code null}
     */

    @Override
    public void putPriceSchedule(org.osid.ordering.PriceSchedule priceSchedule) {
        super.putPriceSchedule(priceSchedule);
        return;
    }


    /**
     *  Makes an array of price schedules available in this session.
     *
     *  @param priceSchedules an array of price schedules
     *  @throws org.osid.NullArgumentException {@code priceSchedules{@code 
     *          is {@code null}
     */

    @Override
    public void putPriceSchedules(org.osid.ordering.PriceSchedule[] priceSchedules) {
        super.putPriceSchedules(priceSchedules);
        return;
    }


    /**
     *  Makes collection of price schedules available in this session.
     *
     *  @param priceSchedules a collection of price schedules
     *  @throws org.osid.NullArgumentException {@code priceSchedules{@code  is
     *          {@code null}
     */

    @Override
    public void putPriceSchedules(java.util.Collection<? extends org.osid.ordering.PriceSchedule> priceSchedules) {
        super.putPriceSchedules(priceSchedules);
        return;
    }


    /**
     *  Removes a PriceSchedule from this session.
     *
     *  @param priceScheduleId the {@code Id} of the price schedule
     *  @throws org.osid.NullArgumentException {@code priceScheduleId{@code 
     *          is {@code null}
     */

    @Override
    public void removePriceSchedule(org.osid.id.Id priceScheduleId) {
        super.removePriceSchedule(priceScheduleId);
        return;
    }    
}
