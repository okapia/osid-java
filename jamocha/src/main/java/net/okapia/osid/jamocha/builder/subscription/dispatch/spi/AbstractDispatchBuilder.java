//
// AbstractDispatch.java
//
//     Defines a Dispatch builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.subscription.dispatch.spi;


/**
 *  Defines a <code>Dispatch</code> builder.
 */

public abstract class AbstractDispatchBuilder<T extends AbstractDispatchBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidGovernatorBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.subscription.dispatch.DispatchMiter dispatch;


    /**
     *  Constructs a new <code>AbstractDispatchBuilder</code>.
     *
     *  @param dispatch the dispatch to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractDispatchBuilder(net.okapia.osid.jamocha.builder.subscription.dispatch.DispatchMiter dispatch) {
        super(dispatch);
        this.dispatch = dispatch;
        return;
    }


    /**
     *  Builds the dispatch.
     *
     *  @return the new dispatch
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.subscription.Dispatch build() {
        (new net.okapia.osid.jamocha.builder.validator.subscription.dispatch.DispatchValidator(getValidations())).validate(this.dispatch);
        return (new net.okapia.osid.jamocha.builder.subscription.dispatch.ImmutableDispatch(this.dispatch));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the dispatch miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.subscription.dispatch.DispatchMiter getMiter() {
        return (this.dispatch);
    }


    /**
     *  Adds an address genus type.
     *
     *  @param addressGenusType an address genus type
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>addressGenusType</code> is <code>null</code>
     */

    public T addressGenusType(org.osid.type.Type addressGenusType) {
        getMiter().addAddressGenusType(addressGenusType);
        return (self());
    }


    /**
     *  Sets all the address genus types.
     *
     *  @param addressGenusTypes a collection of address genus types
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>addressGenusTypes</code> is <code>null</code>
     */

    public T addressGenusTypes(java.util.Collection<org.osid.type.Type> addressGenusTypes) {
        getMiter().setAddressGenusTypes(addressGenusTypes);
        return (self());
    }


    /**
     *  Adds a Dispatch record.
     *
     *  @param record a dispatch record
     *  @param recordType the type of dispatch record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.subscription.records.DispatchRecord record, org.osid.type.Type recordType) {
        getMiter().addDispatchRecord(record, recordType);
        return (self());
    }
}       


