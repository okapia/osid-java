//
// AbstractAssemblyUtilityQuery.java
//
//     An UtilityQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.metering.utility.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An UtilityQuery that stores terms.
 */

public abstract class AbstractAssemblyUtilityQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCatalogQuery
    implements org.osid.metering.UtilityQuery,
               org.osid.metering.UtilityQueryInspector,
               org.osid.metering.UtilitySearchOrder {

    private final java.util.Collection<org.osid.metering.records.UtilityQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.metering.records.UtilityQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.metering.records.UtilitySearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyUtilityQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyUtilityQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets a meter <code> Id. </code> 
     *
     *  @param  meterId a meter <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> meterId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchMeterId(org.osid.id.Id meterId, boolean match) {
        getAssembler().addIdTerm(getMeterIdColumn(), meterId, match);
        return;
    }


    /**
     *  Clears the meter <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearMeterIdTerms() {
        getAssembler().clearTerms(getMeterIdColumn());
        return;
    }


    /**
     *  Gets the meter <code> Id </code> terms. 
     *
     *  @return the meter <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getMeterIdTerms() {
        return (getAssembler().getIdTerms(getMeterIdColumn()));
    }


    /**
     *  Gets the MeterId column name.
     *
     * @return the column name
     */

    protected String getMeterIdColumn() {
        return ("meter_id");
    }


    /**
     *  Tests if a meter query is available. 
     *
     *  @return <code> true </code> if a meter query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMeterQuery() {
        return (false);
    }


    /**
     *  Gets the query for a meter. 
     *
     *  @return the meter query 
     *  @throws org.osid.UnimplementedException <code> supportsMeterQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterQuery getMeterQuery() {
        throw new org.osid.UnimplementedException("supportsMeterQuery() is false");
    }


    /**
     *  Matches utilities with any meter. 
     *
     *  @param  match <code> true </code> to match utilities with any meter, 
     *          <code> false </code> to match utilities with no meters 
     */

    @OSID @Override
    public void matchAnyMeter(boolean match) {
        getAssembler().addIdWildcardTerm(getMeterColumn(), match);
        return;
    }


    /**
     *  Clears the meter query terms. 
     */

    @OSID @Override
    public void clearMeterTerms() {
        getAssembler().clearTerms(getMeterColumn());
        return;
    }


    /**
     *  Gets the meter terms. 
     *
     *  @return the meter terms 
     */

    @OSID @Override
    public org.osid.metering.MeterQueryInspector[] getMeterTerms() {
        return (new org.osid.metering.MeterQueryInspector[0]);
    }


    /**
     *  Gets the Meter column name.
     *
     * @return the column name
     */

    protected String getMeterColumn() {
        return ("meter");
    }


    /**
     *  Sets the utility <code> Id </code> for this query to match utilities 
     *  that have the specified utility as an ancestor. 
     *
     *  @param  utilityId a utility <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> utilityId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorUtilityId(org.osid.id.Id utilityId, boolean match) {
        getAssembler().addIdTerm(getAncestorUtilityIdColumn(), utilityId, match);
        return;
    }


    /**
     *  Clears the ancestor utility <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorUtilityIdTerms() {
        getAssembler().clearTerms(getAncestorUtilityIdColumn());
        return;
    }


    /**
     *  Gets the ancestor utility <code> Id </code> terms. 
     *
     *  @return the utility <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorUtilityIdTerms() {
        return (getAssembler().getIdTerms(getAncestorUtilityIdColumn()));
    }


    /**
     *  Gets the AncestorUtilityId column name.
     *
     * @return the column name
     */

    protected String getAncestorUtilityIdColumn() {
        return ("ancestor_utility_id");
    }


    /**
     *  Tests if a <code> UtilityQuery </code> is available. 
     *
     *  @return <code> true </code> if a utility query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorUtilityQuery() {
        return (false);
    }


    /**
     *  Gets the query for a utility. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the utility query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorUtilityQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.UtilityQuery getAncestorUtilityQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorUtilityQuery() is false");
    }


    /**
     *  Matches utilities with any ancestor. 
     *
     *  @param  match <code> true </code> to match utilities with any 
     *          ancestor, <code> false </code> to match root utilities 
     */

    @OSID @Override
    public void matchAnyAncestorUtility(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorUtilityColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor utility query terms. 
     */

    @OSID @Override
    public void clearAncestorUtilityTerms() {
        getAssembler().clearTerms(getAncestorUtilityColumn());
        return;
    }


    /**
     *  Gets the ancestor utility terms. 
     *
     *  @return the utility terms 
     */

    @OSID @Override
    public org.osid.metering.UtilityQueryInspector[] getAncestorUtilityTerms() {
        return (new org.osid.metering.UtilityQueryInspector[0]);
    }


    /**
     *  Gets the AncestorUtility column name.
     *
     * @return the column name
     */

    protected String getAncestorUtilityColumn() {
        return ("ancestor_utility");
    }


    /**
     *  Sets the utility <code> Id </code> for this query to match utilities 
     *  that have the specified utility as a descendant. 
     *
     *  @param  utilityId a utility <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> utilityId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantUtilityId(org.osid.id.Id utilityId, 
                                         boolean match) {
        getAssembler().addIdTerm(getDescendantUtilityIdColumn(), utilityId, match);
        return;
    }


    /**
     *  Clears the descendant utility <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantUtilityIdTerms() {
        getAssembler().clearTerms(getDescendantUtilityIdColumn());
        return;
    }


    /**
     *  Gets the descendant utility <code> Id </code> terms. 
     *
     *  @return the utility <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantUtilityIdTerms() {
        return (getAssembler().getIdTerms(getDescendantUtilityIdColumn()));
    }


    /**
     *  Gets the DescendantUtilityId column name.
     *
     * @return the column name
     */

    protected String getDescendantUtilityIdColumn() {
        return ("descendant_utility_id");
    }


    /**
     *  Tests if a <code> UtilityQuery </code> is available. 
     *
     *  @return <code> true </code> if a utility query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantUtilityQuery() {
        return (false);
    }


    /**
     *  Gets the query for a utility. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the utility query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantUtilityQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.metering.UtilityQuery getDescendantUtilityQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantUtilityQuery() is false");
    }


    /**
     *  Matches utilities with any descendant. 
     *
     *  @param  match <code> true </code> to match utilities with any 
     *          descendant, <code> false </code> to match leaf utilities 
     */

    @OSID @Override
    public void matchAnyDescendantUtility(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantUtilityColumn(), match);
        return;
    }


    /**
     *  Clears the descendant utility query terms. 
     */

    @OSID @Override
    public void clearDescendantUtilityTerms() {
        getAssembler().clearTerms(getDescendantUtilityColumn());
        return;
    }


    /**
     *  Gets the descendant utility terms. 
     *
     *  @return the utility terms 
     */

    @OSID @Override
    public org.osid.metering.UtilityQueryInspector[] getDescendantUtilityTerms() {
        return (new org.osid.metering.UtilityQueryInspector[0]);
    }


    /**
     *  Gets the DescendantUtility column name.
     *
     * @return the column name
     */

    protected String getDescendantUtilityColumn() {
        return ("descendant_utility");
    }


    /**
     *  Tests if this utility supports the given record
     *  <code>Type</code>.
     *
     *  @param  utilityRecordType an utility record type 
     *  @return <code>true</code> if the utilityRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>utilityRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type utilityRecordType) {
        for (org.osid.metering.records.UtilityQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(utilityRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  utilityRecordType the utility record type 
     *  @return the utility query record 
     *  @throws org.osid.NullArgumentException
     *          <code>utilityRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(utilityRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.metering.records.UtilityQueryRecord getUtilityQueryRecord(org.osid.type.Type utilityRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.metering.records.UtilityQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(utilityRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(utilityRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  utilityRecordType the utility record type 
     *  @return the utility query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>utilityRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(utilityRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.metering.records.UtilityQueryInspectorRecord getUtilityQueryInspectorRecord(org.osid.type.Type utilityRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.metering.records.UtilityQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(utilityRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(utilityRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param utilityRecordType the utility record type
     *  @return the utility search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>utilityRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(utilityRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.metering.records.UtilitySearchOrderRecord getUtilitySearchOrderRecord(org.osid.type.Type utilityRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.metering.records.UtilitySearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(utilityRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(utilityRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this utility. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param utilityQueryRecord the utility query record
     *  @param utilityQueryInspectorRecord the utility query inspector
     *         record
     *  @param utilitySearchOrderRecord the utility search order record
     *  @param utilityRecordType utility record type
     *  @throws org.osid.NullArgumentException
     *          <code>utilityQueryRecord</code>,
     *          <code>utilityQueryInspectorRecord</code>,
     *          <code>utilitySearchOrderRecord</code> or
     *          <code>utilityRecordTypeutility</code> is
     *          <code>null</code>
     */
            
    protected void addUtilityRecords(org.osid.metering.records.UtilityQueryRecord utilityQueryRecord, 
                                      org.osid.metering.records.UtilityQueryInspectorRecord utilityQueryInspectorRecord, 
                                      org.osid.metering.records.UtilitySearchOrderRecord utilitySearchOrderRecord, 
                                      org.osid.type.Type utilityRecordType) {

        addRecordType(utilityRecordType);

        nullarg(utilityQueryRecord, "utility query record");
        nullarg(utilityQueryInspectorRecord, "utility query inspector record");
        nullarg(utilitySearchOrderRecord, "utility search odrer record");

        this.queryRecords.add(utilityQueryRecord);
        this.queryInspectorRecords.add(utilityQueryInspectorRecord);
        this.searchOrderRecords.add(utilitySearchOrderRecord);
        
        return;
    }
}
