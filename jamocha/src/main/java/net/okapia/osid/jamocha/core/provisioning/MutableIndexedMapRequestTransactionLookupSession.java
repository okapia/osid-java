//
// MutableIndexedMapRequestTransactionLookupSession
//
//    Implements a RequestTransaction lookup service backed by a collection of
//    requestTransactions indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning;


/**
 *  Implements a RequestTransaction lookup service backed by a collection of
 *  request transactions. The request transactions are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some request transactions may be compatible
 *  with more types than are indicated through these request transaction
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of request transactions can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapRequestTransactionLookupSession
    extends net.okapia.osid.jamocha.core.provisioning.spi.AbstractIndexedMapRequestTransactionLookupSession
    implements org.osid.provisioning.RequestTransactionLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapRequestTransactionLookupSession} with no request transactions.
     *
     *  @param distributor the distributor
     *  @throws org.osid.NullArgumentException {@code distributor}
     *          is {@code null}
     */

      public MutableIndexedMapRequestTransactionLookupSession(org.osid.provisioning.Distributor distributor) {
        setDistributor(distributor);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapRequestTransactionLookupSession} with a
     *  single request transaction.
     *  
     *  @param distributor the distributor
     *  @param  requestTransaction a single requestTransaction
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code requestTransaction} is {@code null}
     */

    public MutableIndexedMapRequestTransactionLookupSession(org.osid.provisioning.Distributor distributor,
                                                  org.osid.provisioning.RequestTransaction requestTransaction) {
        this(distributor);
        putRequestTransaction(requestTransaction);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapRequestTransactionLookupSession} using an
     *  array of request transactions.
     *
     *  @param distributor the distributor
     *  @param  requestTransactions an array of request transactions
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code requestTransactions} is {@code null}
     */

    public MutableIndexedMapRequestTransactionLookupSession(org.osid.provisioning.Distributor distributor,
                                                  org.osid.provisioning.RequestTransaction[] requestTransactions) {
        this(distributor);
        putRequestTransactions(requestTransactions);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapRequestTransactionLookupSession} using a
     *  collection of request transactions.
     *
     *  @param distributor the distributor
     *  @param  requestTransactions a collection of request transactions
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code requestTransactions} is {@code null}
     */

    public MutableIndexedMapRequestTransactionLookupSession(org.osid.provisioning.Distributor distributor,
                                                  java.util.Collection<? extends org.osid.provisioning.RequestTransaction> requestTransactions) {

        this(distributor);
        putRequestTransactions(requestTransactions);
        return;
    }
    

    /**
     *  Makes a {@code RequestTransaction} available in this session.
     *
     *  @param  requestTransaction a request transaction
     *  @throws org.osid.NullArgumentException {@code requestTransaction{@code  is
     *          {@code null}
     */

    @Override
    public void putRequestTransaction(org.osid.provisioning.RequestTransaction requestTransaction) {
        super.putRequestTransaction(requestTransaction);
        return;
    }


    /**
     *  Makes an array of request transactions available in this session.
     *
     *  @param  requestTransactions an array of request transactions
     *  @throws org.osid.NullArgumentException {@code requestTransactions{@code 
     *          is {@code null}
     */

    @Override
    public void putRequestTransactions(org.osid.provisioning.RequestTransaction[] requestTransactions) {
        super.putRequestTransactions(requestTransactions);
        return;
    }


    /**
     *  Makes collection of request transactions available in this session.
     *
     *  @param  requestTransactions a collection of request transactions
     *  @throws org.osid.NullArgumentException {@code requestTransaction{@code  is
     *          {@code null}
     */

    @Override
    public void putRequestTransactions(java.util.Collection<? extends org.osid.provisioning.RequestTransaction> requestTransactions) {
        super.putRequestTransactions(requestTransactions);
        return;
    }


    /**
     *  Removes a RequestTransaction from this session.
     *
     *  @param requestTransactionId the {@code Id} of the request transaction
     *  @throws org.osid.NullArgumentException {@code requestTransactionId{@code  is
     *          {@code null}
     */

    @Override
    public void removeRequestTransaction(org.osid.id.Id requestTransactionId) {
        super.removeRequestTransaction(requestTransactionId);
        return;
    }    
}
