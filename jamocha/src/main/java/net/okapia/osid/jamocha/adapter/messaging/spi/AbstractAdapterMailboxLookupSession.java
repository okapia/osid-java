//
// AbstractAdapterMailboxLookupSession.java
//
//    A Mailbox lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.messaging.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Mailbox lookup session adapter.
 */

public abstract class AbstractAdapterMailboxLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.messaging.MailboxLookupSession {

    private final org.osid.messaging.MailboxLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterMailboxLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterMailboxLookupSession(org.osid.messaging.MailboxLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@code Mailbox} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupMailboxes() {
        return (this.session.canLookupMailboxes());
    }


    /**
     *  A complete view of the {@code Mailbox} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeMailboxView() {
        this.session.useComparativeMailboxView();
        return;
    }


    /**
     *  A complete view of the {@code Mailbox} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryMailboxView() {
        this.session.usePlenaryMailboxView();
        return;
    }

     
    /**
     *  Gets the {@code Mailbox} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Mailbox} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Mailbox} and
     *  retained for compatibility.
     *
     *  @param mailboxId {@code Id} of the {@code Mailbox}
     *  @return the mailbox
     *  @throws org.osid.NotFoundException {@code mailboxId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code mailboxId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.Mailbox getMailbox(org.osid.id.Id mailboxId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getMailbox(mailboxId));
    }


    /**
     *  Gets a {@code MailboxList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  mailboxes specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Mailboxes} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  mailboxIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Mailbox} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code mailboxIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.MailboxList getMailboxesByIds(org.osid.id.IdList mailboxIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getMailboxesByIds(mailboxIds));
    }


    /**
     *  Gets a {@code MailboxList} corresponding to the given
     *  mailbox genus {@code Type} which does not include
     *  mailboxes of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  mailboxes or an error results. Otherwise, the returned list
     *  may contain only those mailboxes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  mailboxGenusType a mailbox genus type 
     *  @return the returned {@code Mailbox} list
     *  @throws org.osid.NullArgumentException
     *          {@code mailboxGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.MailboxList getMailboxesByGenusType(org.osid.type.Type mailboxGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getMailboxesByGenusType(mailboxGenusType));
    }


    /**
     *  Gets a {@code MailboxList} corresponding to the given
     *  mailbox genus {@code Type} and include any additional
     *  mailboxes with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  mailboxes or an error results. Otherwise, the returned list
     *  may contain only those mailboxes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  mailboxGenusType a mailbox genus type 
     *  @return the returned {@code Mailbox} list
     *  @throws org.osid.NullArgumentException
     *          {@code mailboxGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.MailboxList getMailboxesByParentGenusType(org.osid.type.Type mailboxGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getMailboxesByParentGenusType(mailboxGenusType));
    }


    /**
     *  Gets a {@code MailboxList} containing the given
     *  mailbox record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  mailboxes or an error results. Otherwise, the returned list
     *  may contain only those mailboxes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  mailboxRecordType a mailbox record type 
     *  @return the returned {@code Mailbox} list
     *  @throws org.osid.NullArgumentException
     *          {@code mailboxRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.MailboxList getMailboxesByRecordType(org.osid.type.Type mailboxRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getMailboxesByRecordType(mailboxRecordType));
    }


    /**
     *  Gets a {@code MailboxList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  mailboxes or an error results. Otherwise, the returned list
     *  may contain only those mailboxes that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Mailbox} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.messaging.MailboxList getMailboxesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getMailboxesByProvider(resourceId));
    }


    /**
     *  Gets all {@code Mailboxes}. 
     *
     *  In plenary mode, the returned list contains all known
     *  mailboxes or an error results. Otherwise, the returned list
     *  may contain only those mailboxes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Mailboxes} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.MailboxList getMailboxes()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getMailboxes());
    }
}
