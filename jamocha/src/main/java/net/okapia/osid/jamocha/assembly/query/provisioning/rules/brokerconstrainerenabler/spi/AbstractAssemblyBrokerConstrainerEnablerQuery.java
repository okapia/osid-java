//
// AbstractAssemblyBrokerConstrainerEnablerQuery.java
//
//     A BrokerConstrainerEnablerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.provisioning.rules.brokerconstrainerenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A BrokerConstrainerEnablerQuery that stores terms.
 */

public abstract class AbstractAssemblyBrokerConstrainerEnablerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidEnablerQuery
    implements org.osid.provisioning.rules.BrokerConstrainerEnablerQuery,
               org.osid.provisioning.rules.BrokerConstrainerEnablerQueryInspector,
               org.osid.provisioning.rules.BrokerConstrainerEnablerSearchOrder {

    private final java.util.Collection<org.osid.provisioning.rules.records.BrokerConstrainerEnablerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.provisioning.rules.records.BrokerConstrainerEnablerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.provisioning.rules.records.BrokerConstrainerEnablerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyBrokerConstrainerEnablerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyBrokerConstrainerEnablerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches enablers mapped to the broker constrainer. 
     *
     *  @param  brokerConstrainerId the broker constrainer <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> brokerConstrainerId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledBrokerConstrainerId(org.osid.id.Id brokerConstrainerId, 
                                              boolean match) {
        getAssembler().addIdTerm(getRuledBrokerConstrainerIdColumn(), brokerConstrainerId, match);
        return;
    }


    /**
     *  Clears the broker constrainer <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledBrokerConstrainerIdTerms() {
        getAssembler().clearTerms(getRuledBrokerConstrainerIdColumn());
        return;
    }


    /**
     *  Gets the broker constrainer <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledBrokerConstrainerIdTerms() {
        return (getAssembler().getIdTerms(getRuledBrokerConstrainerIdColumn()));
    }


    /**
     *  Gets the RuledBrokerConstrainerId column name.
     *
     * @return the column name
     */

    protected String getRuledBrokerConstrainerIdColumn() {
        return ("ruled_broker_constrainer_id");
    }


    /**
     *  Tests if a <code> BrokerConstrainerQuery </code> is available. 
     *
     *  @return <code> true </code> if a broker constrainer query is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledBrokerConstrainerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a broker constrainer. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the broker constrainer query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledBrokerConstrainerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerQuery getRuledBrokerConstrainerQuery() {
        throw new org.osid.UnimplementedException("supportsRuledBrokerConstrainerQuery() is false");
    }


    /**
     *  Matches enablers mapped to any broker constrainer. 
     *
     *  @param  match <code> true </code> for enablers mapped to any broker 
     *          constrainer, <code> false </code> to match enablers mapped to 
     *          no broker constrainers 
     */

    @OSID @Override
    public void matchAnyRuledBrokerConstrainer(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledBrokerConstrainerColumn(), match);
        return;
    }


    /**
     *  Clears the broker constrainer query terms. 
     */

    @OSID @Override
    public void clearRuledBrokerConstrainerTerms() {
        getAssembler().clearTerms(getRuledBrokerConstrainerColumn());
        return;
    }


    /**
     *  Gets the broker constrainer query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerQueryInspector[] getRuledBrokerConstrainerTerms() {
        return (new org.osid.provisioning.rules.BrokerConstrainerQueryInspector[0]);
    }


    /**
     *  Gets the RuledBrokerConstrainer column name.
     *
     * @return the column name
     */

    protected String getRuledBrokerConstrainerColumn() {
        return ("ruled_broker_constrainer");
    }


    /**
     *  Matches enablers mapped to the distributor. 
     *
     *  @param  distributorId the distributor <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDistributorId(org.osid.id.Id distributorId, boolean match) {
        getAssembler().addIdTerm(getDistributorIdColumn(), distributorId, match);
        return;
    }


    /**
     *  Clears the distributor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDistributorIdTerms() {
        getAssembler().clearTerms(getDistributorIdColumn());
        return;
    }


    /**
     *  Gets the distributor <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDistributorIdTerms() {
        return (getAssembler().getIdTerms(getDistributorIdColumn()));
    }


    /**
     *  Gets the DistributorId column name.
     *
     * @return the column name
     */

    protected String getDistributorIdColumn() {
        return ("distributor_id");
    }


    /**
     *  Tests if a <code> DistributorQuery </code> is available. 
     *
     *  @return <code> true </code> if a distributor query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDistributorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a distributor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the distributor query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDistributorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQuery getDistributorQuery() {
        throw new org.osid.UnimplementedException("supportsDistributorQuery() is false");
    }


    /**
     *  Clears the distributor query terms. 
     */

    @OSID @Override
    public void clearDistributorTerms() {
        getAssembler().clearTerms(getDistributorColumn());
        return;
    }


    /**
     *  Gets the distributor query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQueryInspector[] getDistributorTerms() {
        return (new org.osid.provisioning.DistributorQueryInspector[0]);
    }


    /**
     *  Gets the Distributor column name.
     *
     * @return the column name
     */

    protected String getDistributorColumn() {
        return ("distributor");
    }


    /**
     *  Tests if this brokerConstrainerEnabler supports the given record
     *  <code>Type</code>.
     *
     *  @param  brokerConstrainerEnablerRecordType a broker constrainer enabler record type 
     *  @return <code>true</code> if the brokerConstrainerEnablerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>brokerConstrainerEnablerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type brokerConstrainerEnablerRecordType) {
        for (org.osid.provisioning.rules.records.BrokerConstrainerEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(brokerConstrainerEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  brokerConstrainerEnablerRecordType the broker constrainer enabler record type 
     *  @return the broker constrainer enabler query record 
     *  @throws org.osid.NullArgumentException
     *          <code>brokerConstrainerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(brokerConstrainerEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.BrokerConstrainerEnablerQueryRecord getBrokerConstrainerEnablerQueryRecord(org.osid.type.Type brokerConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.rules.records.BrokerConstrainerEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(brokerConstrainerEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(brokerConstrainerEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  brokerConstrainerEnablerRecordType the broker constrainer enabler record type 
     *  @return the broker constrainer enabler query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>brokerConstrainerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(brokerConstrainerEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.BrokerConstrainerEnablerQueryInspectorRecord getBrokerConstrainerEnablerQueryInspectorRecord(org.osid.type.Type brokerConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.rules.records.BrokerConstrainerEnablerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(brokerConstrainerEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(brokerConstrainerEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param brokerConstrainerEnablerRecordType the broker constrainer enabler record type
     *  @return the broker constrainer enabler search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>brokerConstrainerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(brokerConstrainerEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.BrokerConstrainerEnablerSearchOrderRecord getBrokerConstrainerEnablerSearchOrderRecord(org.osid.type.Type brokerConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.rules.records.BrokerConstrainerEnablerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(brokerConstrainerEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(brokerConstrainerEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this broker constrainer enabler. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param brokerConstrainerEnablerQueryRecord the broker constrainer enabler query record
     *  @param brokerConstrainerEnablerQueryInspectorRecord the broker constrainer enabler query inspector
     *         record
     *  @param brokerConstrainerEnablerSearchOrderRecord the broker constrainer enabler search order record
     *  @param brokerConstrainerEnablerRecordType broker constrainer enabler record type
     *  @throws org.osid.NullArgumentException
     *          <code>brokerConstrainerEnablerQueryRecord</code>,
     *          <code>brokerConstrainerEnablerQueryInspectorRecord</code>,
     *          <code>brokerConstrainerEnablerSearchOrderRecord</code> or
     *          <code>brokerConstrainerEnablerRecordTypebrokerConstrainerEnabler</code> is
     *          <code>null</code>
     */
            
    protected void addBrokerConstrainerEnablerRecords(org.osid.provisioning.rules.records.BrokerConstrainerEnablerQueryRecord brokerConstrainerEnablerQueryRecord, 
                                      org.osid.provisioning.rules.records.BrokerConstrainerEnablerQueryInspectorRecord brokerConstrainerEnablerQueryInspectorRecord, 
                                      org.osid.provisioning.rules.records.BrokerConstrainerEnablerSearchOrderRecord brokerConstrainerEnablerSearchOrderRecord, 
                                      org.osid.type.Type brokerConstrainerEnablerRecordType) {

        addRecordType(brokerConstrainerEnablerRecordType);

        nullarg(brokerConstrainerEnablerQueryRecord, "broker constrainer enabler query record");
        nullarg(brokerConstrainerEnablerQueryInspectorRecord, "broker constrainer enabler query inspector record");
        nullarg(brokerConstrainerEnablerSearchOrderRecord, "broker constrainer enabler search odrer record");

        this.queryRecords.add(brokerConstrainerEnablerQueryRecord);
        this.queryInspectorRecords.add(brokerConstrainerEnablerQueryInspectorRecord);
        this.searchOrderRecords.add(brokerConstrainerEnablerSearchOrderRecord);
        
        return;
    }
}
