//
// AbstractAntimatroid.java
//
//     Defines an Antimatroid.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.sequencing.antimatroid.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>Antimatroid</code>.
 */

public abstract class AbstractAntimatroid
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalog
    implements org.osid.sequencing.Antimatroid {

    private final java.util.Collection<org.osid.sequencing.records.AntimatroidRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Tests if this antimatroid supports the given record
     *  <code>Type</code>.
     *
     *  @param  antimatroidRecordType an antimatroid record type 
     *  @return <code>true</code> if the antimatroidRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>antimatroidRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type antimatroidRecordType) {
        for (org.osid.sequencing.records.AntimatroidRecord record : this.records) {
            if (record.implementsRecordType(antimatroidRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  antimatroidRecordType the antimatroid record type 
     *  @return the antimatroid record 
     *  @throws org.osid.NullArgumentException
     *          <code>antimatroidRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(antimatroidRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.sequencing.records.AntimatroidRecord getAntimatroidRecord(org.osid.type.Type antimatroidRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.sequencing.records.AntimatroidRecord record : this.records) {
            if (record.implementsRecordType(antimatroidRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(antimatroidRecordType + " is not supported");
    }


    /**
     *  Adds a record to this antimatroid. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param antimatroidRecord the antimatroid record
     *  @param antimatroidRecordType antimatroid record type
     *  @throws org.osid.NullArgumentException
     *          <code>antimatroidRecord</code> or
     *          <code>antimatroidRecordTypeantimatroid</code> is
     *          <code>null</code>
     */
            
    protected void addAntimatroidRecord(org.osid.sequencing.records.AntimatroidRecord antimatroidRecord, 
                                     org.osid.type.Type antimatroidRecordType) {

        addRecordType(antimatroidRecordType);
        this.records.add(antimatroidRecord);
        
        return;
    }
}
