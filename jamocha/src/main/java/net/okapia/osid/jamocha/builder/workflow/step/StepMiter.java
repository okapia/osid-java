//
// StepMiter.java
//
//     Defines a Step miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.workflow.step;


/**
 *  Defines a <code>Step</code> miter for use with the builders.
 */

public interface StepMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidGovernatorMiter,
            org.osid.workflow.Step {


    /**
     *  Sets the process.
     *
     *  @param process a process
     *  @throws org.osid.NullArgumentException
     *          <code>process</code> is <code>null</code>
     */

    public void setProcess(org.osid.workflow.Process process);


    /**
     *  Adds a resource.
     *
     *  @param resource a resource
     *  @throws org.osid.NullArgumentException
     *          <code>resource</code> is <code>null</code>
     */

    public void addResource(org.osid.resource.Resource resource);


    /**
     *  Sets all the resources.
     *
     *  @param resources a collection of resources
     *  @throws org.osid.NullArgumentException
     *          <code>resources</code> is <code>null</code>
     */

    public void setResources(java.util.Collection<org.osid.resource.Resource> resources);


    /**
     *  Adds an input state.
     *
     *  @param state an input state
     *  @throws org.osid.NullArgumentException <code>state</code> is
     *          <code>null</code>
     */

    public void addInputState(org.osid.process.State state);


    /**
     *  Sets all the input states.
     *
     *  @param states a collection of input states
     *  @throws org.osid.NullArgumentException <code>states</code> is
     *          <code>null</code>
     */

    public void setInputStates(java.util.Collection<org.osid.process.State> states);


    /**
     *  Sets the next state.
     *
     *  @param state a next state
     *  @throws org.osid.NullArgumentException <code>state</code> is
     *          <code>null</code>
     */

    public void setNextState(org.osid.process.State state);


    /**
     *  Adds a Step record.
     *
     *  @param record a step record
     *  @param recordType the type of step record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addStepRecord(org.osid.workflow.records.StepRecord record, org.osid.type.Type recordType);
}       


