//
// AbstractGradeSystemQueryInspector.java
//
//     A template for making a GradeSystemQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.gradesystem.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for grade systems.
 */

public abstract class AbstractGradeSystemQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.grading.GradeSystemQueryInspector {

    private final java.util.Collection<org.osid.grading.records.GradeSystemQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the grade-based systems terms. 
     *
     *  @return the grade-based systems terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getBasedOnGradesTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the grade <code> Id </code> terms. 
     *
     *  @return the grade <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGradeIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the grade terms. 
     *
     *  @return the grade terms 
     */

    @OSID @Override
    public org.osid.grading.GradeQueryInspector[] getGradeTerms() {
        return (new org.osid.grading.GradeQueryInspector[0]);
    }


    /**
     *  Gets the lowest numeric score terms. 
     *
     *  @return the lowest numeric score terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getLowestNumericScoreTerms() {
        return (new org.osid.search.terms.DecimalRangeTerm[0]);
    }


    /**
     *  Gets the numeric score increment terms. 
     *
     *  @return the numeric score increment terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getNumericScoreIncrementTerms() {
        return (new org.osid.search.terms.DecimalRangeTerm[0]);
    }


    /**
     *  Gets the highest numeric score terms. 
     *
     *  @return the highest numeric score terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getHighestNumericScoreTerms() {
        return (new org.osid.search.terms.DecimalRangeTerm[0]);
    }


    /**
     *  Gets the gradebook column <code> Id </code> terms. 
     *
     *  @return the gradebook column <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGradebookColumnIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the gradebook column terms. 
     *
     *  @return the gradebook column terms 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnQueryInspector[] getGradebookColumnTerms() {
        return (new org.osid.grading.GradebookColumnQueryInspector[0]);
    }


    /**
     *  Gets the gradebook <code> Id </code> terms. 
     *
     *  @return the gradebook <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGradebookIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the gradebook terms. 
     *
     *  @return the gradebook terms 
     */

    @OSID @Override
    public org.osid.grading.GradebookQueryInspector[] getGradebookTerms() {
        return (new org.osid.grading.GradebookQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given grade system query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a grade system implementing the requested record.
     *
     *  @param gradeSystemRecordType a grade system record type
     *  @return the grade system query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>gradeSystemRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(gradeSystemRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.grading.records.GradeSystemQueryInspectorRecord getGradeSystemQueryInspectorRecord(org.osid.type.Type gradeSystemRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.grading.records.GradeSystemQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(gradeSystemRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(gradeSystemRecordType + " is not supported");
    }


    /**
     *  Adds a record to this grade system query. 
     *
     *  @param gradeSystemQueryInspectorRecord grade system query inspector
     *         record
     *  @param gradeSystemRecordType gradeSystem record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addGradeSystemQueryInspectorRecord(org.osid.grading.records.GradeSystemQueryInspectorRecord gradeSystemQueryInspectorRecord, 
                                                   org.osid.type.Type gradeSystemRecordType) {

        addRecordType(gradeSystemRecordType);
        nullarg(gradeSystemRecordType, "grade system record type");
        this.records.add(gradeSystemQueryInspectorRecord);        
        return;
    }
}
