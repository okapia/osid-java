//
// AbstractCredentialEntry.java
//
//     Defines a CredentialEntry.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.chronicle.credentialentry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>CredentialEntry</code>.
 */

public abstract class AbstractCredentialEntry
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.course.chronicle.CredentialEntry {

    private org.osid.resource.Resource student;
    private org.osid.course.program.Credential credential;
    private org.osid.calendaring.DateTime dateAwarded;
    private org.osid.course.program.Program program;

    private final java.util.Collection<org.osid.course.chronicle.records.CredentialEntryRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the <code> Student. </code> 
     *
     *  @return the student <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getStudentId() {
        return (this.student.getId());
    }


    /**
     *  Gets the <code> Student. </code> 
     *
     *  @return the student 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getStudent()
        throws org.osid.OperationFailedException {

        return (this.student);
    }


    /**
     *  Sets the student.
     *
     *  @param student a student
     *  @throws org.osid.NullArgumentException <code>student</code> is
     *          <code>null</code>
     */

    protected void setStudent(org.osid.resource.Resource student) {
        nullarg(student, "student");
        this.student = student;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the <code> Credential. </code> 
     *
     *  @return the credential <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCredentialId() {
        return (this.credential.getId());
    }


    /**
     *  Gets the <code> Credential. </code> 
     *
     *  @return the credential 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.program.Credential getCredential()
        throws org.osid.OperationFailedException {

        return (this.credential);
    }


    /**
     *  Sets the credential.
     *
     *  @param credential a credential
     *  @throws org.osid.NullArgumentException <code>credential</code>
     *          is <code>null</code>
     */

    protected void setCredential(org.osid.course.program.Credential credential) {
        nullarg(credential, "credential");
        this.credential = credential;
        return;
    }


    /**
     *  Gets the award date. 
     *
     *  @return the date awarded 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getDateAwarded() {
        return (this.dateAwarded);
    }


    /**
     *  Sets the date awarded.
     *
     *  @param date a date awarded
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    protected void setDateAwarded(org.osid.calendaring.DateTime date) {
        nullarg(date, "date awarded");
        this.dateAwarded = date;
        return;
    }


    /**
     *  Tests if a program is associated with this credential. 
     *
     *  @return <code> true </code> if a program is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasProgram() {
        return (this.program != null);
    }


    /**
     *  Gets the <code> Id </code> of the <code> Program. </code> 
     *
     *  @return the program <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasProgram() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getProgramId() {
        if (!hasProgram()) {
            throw new org.osid.IllegalStateException("hasProgram() is false");
        }

        return (this.program.getId());
    }


    /**
     *  Gets the <code> Program. </code> 
     *
     *  @return the program 
     *  @throws org.osid.IllegalStateException <code> hasProgram() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.program.Program getProgram()
        throws org.osid.OperationFailedException {

        if (!hasProgram()) {
            throw new org.osid.IllegalStateException("hasProgram() is false");
        }

        return (this.program);
    }


    /**
     *  Sets the program.
     *
     *  @param program a program
     *  @throws org.osid.NullArgumentException <code>program</code> is
     *          <code>null</code>
     */

    protected void setProgram(org.osid.course.program.Program program) {
        nullarg(program, "program");
        this.program = program;
        return;
    }

    /**
     *  Tests if this credentialEntry supports the given record
     *  <code>Type</code>.
     *
     *  @param  credentialEntryRecordType a credential entry record type 
     *  @return <code>true</code> if the credentialEntryRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>credentialEntryRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type credentialEntryRecordType) {
        for (org.osid.course.chronicle.records.CredentialEntryRecord record : this.records) {
            if (record.implementsRecordType(credentialEntryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>CredentialEntry</code> record <code>Type</code>.
     *
     *  @param  credentialEntryRecordType the credential entry record type 
     *  @return the credential entry record 
     *  @throws org.osid.NullArgumentException
     *          <code>credentialEntryRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(credentialEntryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.chronicle.records.CredentialEntryRecord getCredentialEntryRecord(org.osid.type.Type credentialEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.chronicle.records.CredentialEntryRecord record : this.records) {
            if (record.implementsRecordType(credentialEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(credentialEntryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this credential entry. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param credentialEntryRecord the credential entry record
     *  @param credentialEntryRecordType credential entry record type
     *  @throws org.osid.NullArgumentException
     *          <code>credentialEntryRecord</code> or
     *          <code>credentialEntryRecordTypecredentialEntry</code> is
     *          <code>null</code>
     */
            
    protected void addCredentialEntryRecord(org.osid.course.chronicle.records.CredentialEntryRecord credentialEntryRecord, 
                                            org.osid.type.Type credentialEntryRecordType) {

        nullarg(credentialEntryRecord, "credential entry record");
        addRecordType(credentialEntryRecordType);
        this.records.add(credentialEntryRecord);
        
        return;
    }
}
