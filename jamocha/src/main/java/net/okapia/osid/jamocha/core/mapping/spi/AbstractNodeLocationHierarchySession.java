//
// AbstractNodeLocationHierarchySession.java
//
//     Defines a Location hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a location hierarchy session for delivering a hierarchy
 *  of locations using the LocationNode interface.
 */

public abstract class AbstractNodeLocationHierarchySession
    extends net.okapia.osid.jamocha.mapping.spi.AbstractLocationHierarchySession
    implements org.osid.mapping.LocationHierarchySession {

    private java.util.Collection<org.osid.mapping.LocationNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root location <code> Ids </code> in this hierarchy.
     *
     *  @return the root location <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootLocationIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.mapping.locationnode.LocationNodeToIdList(this.roots));
    }


    /**
     *  Gets the root locations in the location hierarchy. A node
     *  with no parents is an orphan. While all location <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root locations 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.LocationList getRootLocations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.mapping.locationnode.LocationNodeToLocationList(new net.okapia.osid.jamocha.mapping.locationnode.ArrayLocationNodeList(this.roots)));
    }


    /**
     *  Adds a root location node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootLocation(org.osid.mapping.LocationNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root location nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootLocations(java.util.Collection<org.osid.mapping.LocationNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root location node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootLocation(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.mapping.LocationNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Location </code> has any parents. 
     *
     *  @param  locationId a location <code> Id </code> 
     *  @return <code> true </code> if the location has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> locationId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> locationId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentLocations(org.osid.id.Id locationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getLocationNode(locationId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of a
     *  location.
     *
     *  @param  id an <code> Id </code> 
     *  @param  locationId the <code> Id </code> of a location 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> locationId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> locationId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> locationId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfLocation(org.osid.id.Id id, org.osid.id.Id locationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.mapping.LocationNodeList parents = getLocationNode(locationId).getParentLocationNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextLocationNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given location. 
     *
     *  @param  locationId a location <code> Id </code> 
     *  @return the parent <code> Ids </code> of the location 
     *  @throws org.osid.NotFoundException <code> locationId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> locationId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentLocationIds(org.osid.id.Id locationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.mapping.location.LocationToIdList(getParentLocations(locationId)));
    }


    /**
     *  Gets the parents of the given location. 
     *
     *  @param  locationId the <code> Id </code> to query 
     *  @return the parents of the location 
     *  @throws org.osid.NotFoundException <code> locationId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> locationId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.LocationList getParentLocations(org.osid.id.Id locationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.mapping.locationnode.LocationNodeToLocationList(getLocationNode(locationId).getParentLocationNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of a
     *  location.
     *
     *  @param  id an <code> Id </code> 
     *  @param  locationId the Id of a location 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> locationId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> locationId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> locationId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfLocation(org.osid.id.Id id, org.osid.id.Id locationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfLocation(id, locationId)) {
            return (true);
        }

        try (org.osid.mapping.LocationList parents = getParentLocations(locationId)) {
            while (parents.hasNext()) {
                if (isAncestorOfLocation(id, parents.getNextLocation().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if a location has any children. 
     *
     *  @param  locationId a location <code> Id </code> 
     *  @return <code> true </code> if the <code> locationId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> locationId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> locationId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildLocations(org.osid.id.Id locationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getLocationNode(locationId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of a
     *  location.
     *
     *  @param  id an <code> Id </code> 
     *  @param locationId the <code> Id </code> of a 
     *         location
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> locationId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> locationId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> locationId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfLocation(org.osid.id.Id id, org.osid.id.Id locationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfLocation(locationId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  location.
     *
     *  @param  locationId the <code> Id </code> to query 
     *  @return the children of the location 
     *  @throws org.osid.NotFoundException <code> locationId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> locationId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildLocationIds(org.osid.id.Id locationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.mapping.location.LocationToIdList(getChildLocations(locationId)));
    }


    /**
     *  Gets the children of the given location. 
     *
     *  @param  locationId the <code> Id </code> to query 
     *  @return the children of the location 
     *  @throws org.osid.NotFoundException <code> locationId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> locationId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.LocationList getChildLocations(org.osid.id.Id locationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.mapping.locationnode.LocationNodeToLocationList(getLocationNode(locationId).getChildLocationNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of a
     *  location.
     *
     *  @param  id an <code> Id </code> 
     *  @param locationId the <code> Id </code> of a 
     *         location
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> locationId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> locationId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> locationId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfLocation(org.osid.id.Id id, org.osid.id.Id locationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfLocation(locationId, id)) {
            return (true);
        }

        try (org.osid.mapping.LocationList children = getChildLocations(locationId)) {
            while (children.hasNext()) {
                if (isDescendantOfLocation(id, children.getNextLocation().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  location.
     *
     *  @param  locationId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified location node 
     *  @throws org.osid.NotFoundException <code> locationId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> locationId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getLocationNodeIds(org.osid.id.Id locationId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.mapping.locationnode.LocationNodeToNode(getLocationNode(locationId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given location.
     *
     *  @param  locationId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified location node 
     *  @throws org.osid.NotFoundException <code> locationId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> locationId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.LocationNode getLocationNodes(org.osid.id.Id locationId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getLocationNode(locationId));
    }


    /**
     *  Closes this <code>LocationHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets a location node.
     *
     *  @param locationId the id of the location node
     *  @throws org.osid.NotFoundException <code>locationId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>locationId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.mapping.LocationNode getLocationNode(org.osid.id.Id locationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(locationId, "location Id");
        for (org.osid.mapping.LocationNode location : this.roots) {
            if (location.getId().equals(locationId)) {
                return (location);
            }

            org.osid.mapping.LocationNode r = findLocation(location, locationId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(locationId + " is not found");
    }


    protected org.osid.mapping.LocationNode findLocation(org.osid.mapping.LocationNode node, 
                                                         org.osid.id.Id locationId) 
	throws org.osid.OperationFailedException {

        try (org.osid.mapping.LocationNodeList children = node.getChildLocationNodes()) {
            while (children.hasNext()) {
                org.osid.mapping.LocationNode location = children.getNextLocationNode();
                if (location.getId().equals(locationId)) {
                    return (location);
                }
                
                location = findLocation(location, locationId);
                if (location != null) {
                    return (location);
                }
            }
        }

        return (null);
    }
}
