//
// AbstractGradebookColumnSummarySearchOdrer.java
//
//     Defines a GradebookColumnSummarySearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.gradebookcolumnsummary.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code GradebookColumnSummarySearchOrder}.
 */

public abstract class AbstractGradebookColumnSummarySearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidRuleSearchOrder
    implements org.osid.grading.GradebookColumnSummarySearchOrder {

    private final java.util.Collection<org.osid.grading.records.GradebookColumnSummarySearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specified a preference for ordering results by the mean. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByMean(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specified a preference for ordering results by the median. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByMedian(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specified a preference for ordering results by the mode. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByMode(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specified a preference for ordering results by the root mean square. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRMS(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specified a preference for ordering results by the standard deviation. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByStandardDeviation(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specified a preference for ordering results by the sum. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySum(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  gradebookColumnSummaryRecordType a gradebook column summary record type 
     *  @return {@code true} if the gradebookColumnSummaryRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code gradebookColumnSummaryRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type gradebookColumnSummaryRecordType) {
        for (org.osid.grading.records.GradebookColumnSummarySearchOrderRecord record : this.records) {
            if (record.implementsRecordType(gradebookColumnSummaryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  gradebookColumnSummaryRecordType the gradebook column summary record type 
     *  @return the gradebook column summary search order record
     *  @throws org.osid.NullArgumentException
     *          {@code gradebookColumnSummaryRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(gradebookColumnSummaryRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.grading.records.GradebookColumnSummarySearchOrderRecord getGradebookColumnSummarySearchOrderRecord(org.osid.type.Type gradebookColumnSummaryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.grading.records.GradebookColumnSummarySearchOrderRecord record : this.records) {
            if (record.implementsRecordType(gradebookColumnSummaryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(gradebookColumnSummaryRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this gradebook column summary. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param gradebookColumnSummaryRecord the gradebook column summary search odrer record
     *  @param gradebookColumnSummaryRecordType gradebook column summary record type
     *  @throws org.osid.NullArgumentException
     *          {@code gradebookColumnSummaryRecord} or
     *          {@code gradebookColumnSummaryRecordTypegradebookColumnSummary} is
     *          {@code null}
     */
            
    protected void addGradebookColumnSummaryRecord(org.osid.grading.records.GradebookColumnSummarySearchOrderRecord gradebookColumnSummarySearchOrderRecord, 
                                     org.osid.type.Type gradebookColumnSummaryRecordType) {

        addRecordType(gradebookColumnSummaryRecordType);
        this.records.add(gradebookColumnSummarySearchOrderRecord);
        
        return;
    }
}
