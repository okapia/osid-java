//
// InvariantMapPaymentLookupSession
//
//    Implements a Payment lookup service backed by a fixed collection of
//    payments.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.billing.payment;


/**
 *  Implements a Payment lookup service backed by a fixed
 *  collection of payments. The payments are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapPaymentLookupSession
    extends net.okapia.osid.jamocha.core.billing.payment.spi.AbstractMapPaymentLookupSession
    implements org.osid.billing.payment.PaymentLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapPaymentLookupSession</code> with no
     *  payments.
     *  
     *  @param business the business
     *  @throws org.osid.NullArgumnetException {@code business} is
     *          {@code null}
     */

    public InvariantMapPaymentLookupSession(org.osid.billing.Business business) {
        setBusiness(business);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapPaymentLookupSession</code> with a single
     *  payment.
     *  
     *  @param business the business
     *  @param payment a single payment
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code payment} is <code>null</code>
     */

      public InvariantMapPaymentLookupSession(org.osid.billing.Business business,
                                               org.osid.billing.payment.Payment payment) {
        this(business);
        putPayment(payment);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapPaymentLookupSession</code> using an array
     *  of payments.
     *  
     *  @param business the business
     *  @param payments an array of payments
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code payments} is <code>null</code>
     */

      public InvariantMapPaymentLookupSession(org.osid.billing.Business business,
                                               org.osid.billing.payment.Payment[] payments) {
        this(business);
        putPayments(payments);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapPaymentLookupSession</code> using a
     *  collection of payments.
     *
     *  @param business the business
     *  @param payments a collection of payments
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code payments} is <code>null</code>
     */

      public InvariantMapPaymentLookupSession(org.osid.billing.Business business,
                                               java.util.Collection<? extends org.osid.billing.payment.Payment> payments) {
        this(business);
        putPayments(payments);
        return;
    }
}
