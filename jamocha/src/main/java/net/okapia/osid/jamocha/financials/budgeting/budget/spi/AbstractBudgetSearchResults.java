//
// AbstractBudgetSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.budgeting.budget.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractBudgetSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.financials.budgeting.BudgetSearchResults {

    private org.osid.financials.budgeting.BudgetList budgets;
    private final org.osid.financials.budgeting.BudgetQueryInspector inspector;
    private final java.util.Collection<org.osid.financials.budgeting.records.BudgetSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractBudgetSearchResults.
     *
     *  @param budgets the result set
     *  @param budgetQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>budgets</code>
     *          or <code>budgetQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractBudgetSearchResults(org.osid.financials.budgeting.BudgetList budgets,
                                            org.osid.financials.budgeting.BudgetQueryInspector budgetQueryInspector) {
        nullarg(budgets, "budgets");
        nullarg(budgetQueryInspector, "budget query inspectpr");

        this.budgets = budgets;
        this.inspector = budgetQueryInspector;

        return;
    }


    /**
     *  Gets the budget list resulting from a search.
     *
     *  @return a budget list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetList getBudgets() {
        if (this.budgets == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.financials.budgeting.BudgetList budgets = this.budgets;
        this.budgets = null;
	return (budgets);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.financials.budgeting.BudgetQueryInspector getBudgetQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  budget search record <code> Type. </code> This method must
     *  be used to retrieve a budget implementing the requested
     *  record.
     *
     *  @param budgetSearchRecordType a budget search 
     *         record type 
     *  @return the budget search
     *  @throws org.osid.NullArgumentException
     *          <code>budgetSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(budgetSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.budgeting.records.BudgetSearchResultsRecord getBudgetSearchResultsRecord(org.osid.type.Type budgetSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.financials.budgeting.records.BudgetSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(budgetSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(budgetSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record budget search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addBudgetRecord(org.osid.financials.budgeting.records.BudgetSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "budget record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
