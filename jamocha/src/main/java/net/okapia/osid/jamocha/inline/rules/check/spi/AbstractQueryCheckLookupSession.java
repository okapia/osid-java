//
// AbstractQueryCheckLookupSession.java
//
//    An inline adapter that maps a CheckLookupSession to
//    a CheckQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.rules.check.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a CheckLookupSession to
 *  a CheckQuerySession.
 */

public abstract class AbstractQueryCheckLookupSession
    extends net.okapia.osid.jamocha.rules.check.spi.AbstractCheckLookupSession
    implements org.osid.rules.check.CheckLookupSession {

    private boolean activeonly    = false;
    private final org.osid.rules.check.CheckQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryCheckLookupSession.
     *
     *  @param querySession the underlying check query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryCheckLookupSession(org.osid.rules.check.CheckQuerySession querySession) {
        nullarg(querySession, "check query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Engine</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Engine Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getEngineId() {
        return (this.session.getEngineId());
    }


    /**
     *  Gets the <code>Engine</code> associated with this 
     *  session.
     *
     *  @return the <code>Engine</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.Engine getEngine()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getEngine());
    }


    /**
     *  Tests if this user can perform <code>Check</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupChecks() {
        return (this.session.canSearchChecks());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include checks in engines which are children
     *  of this engine in the engine hierarchy.
     */

    @OSID @Override
    public void useFederatedEngineView() {
        this.session.useFederatedEngineView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this engine only.
     */

    @OSID @Override
    public void useIsolatedEngineView() {
        this.session.useIsolatedEngineView();
        return;
    }
    

    /**
     *  Only active checks are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveCheckView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive checks are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusCheckView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>Check</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Check</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Check</code> and
     *  retained for compatibility.
     *
     *  In active mode, checks are returned that are currently
     *  active. In any status mode, active and inactive checks
     *  are returned.
     *
     *  @param  checkId <code>Id</code> of the
     *          <code>Check</code>
     *  @return the check
     *  @throws org.osid.NotFoundException <code>checkId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>checkId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.Check getCheck(org.osid.id.Id checkId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.rules.check.CheckQuery query = getQuery();
        query.matchId(checkId, true);
        org.osid.rules.check.CheckList checks = this.session.getChecksByQuery(query);
        if (checks.hasNext()) {
            return (checks.getNextCheck());
        } 
        
        throw new org.osid.NotFoundException(checkId + " not found");
    }


    /**
     *  Gets a <code>CheckList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  checks specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Checks</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, checks are returned that are currently
     *  active. In any status mode, active and inactive checks
     *  are returned.
     *
     *  @param  checkIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Check</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>checkIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.CheckList getChecksByIds(org.osid.id.IdList checkIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.rules.check.CheckQuery query = getQuery();

        try (org.osid.id.IdList ids = checkIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getChecksByQuery(query));
    }


    /**
     *  Gets a <code>CheckList</code> corresponding to the given
     *  check genus <code>Type</code> which does not include
     *  checks of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  checks or an error results. Otherwise, the returned list
     *  may contain only those checks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, checks are returned that are currently
     *  active. In any status mode, active and inactive checks
     *  are returned.
     *
     *  @param  checkGenusType a check genus type 
     *  @return the returned <code>Check</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>checkGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.CheckList getChecksByGenusType(org.osid.type.Type checkGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.rules.check.CheckQuery query = getQuery();
        query.matchGenusType(checkGenusType, true);
        return (this.session.getChecksByQuery(query));
    }


    /**
     *  Gets a <code>CheckList</code> corresponding to the given
     *  check genus <code>Type</code> and include any additional
     *  checks with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  checks or an error results. Otherwise, the returned list
     *  may contain only those checks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, checks are returned that are currently
     *  active. In any status mode, active and inactive checks
     *  are returned.
     *
     *  @param  checkGenusType a check genus type 
     *  @return the returned <code>Check</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>checkGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.CheckList getChecksByParentGenusType(org.osid.type.Type checkGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.rules.check.CheckQuery query = getQuery();
        query.matchParentGenusType(checkGenusType, true);
        return (this.session.getChecksByQuery(query));
    }


    /**
     *  Gets a <code>CheckList</code> containing the given
     *  check record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  checks or an error results. Otherwise, the returned list
     *  may contain only those checks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, checks are returned that are currently
     *  active. In any status mode, active and inactive checks
     *  are returned.
     *
     *  @param  checkRecordType a check record type 
     *  @return the returned <code>Check</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>checkRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.CheckList getChecksByRecordType(org.osid.type.Type checkRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.rules.check.CheckQuery query = getQuery();
        query.matchRecordType(checkRecordType, true);
        return (this.session.getChecksByQuery(query));
    }

    
    /**
     *  Gets all <code>Checks</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  checks or an error results. Otherwise, the returned list
     *  may contain only those checks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, checks are returned that are currently
     *  active. In any status mode, active and inactive checks
     *  are returned.
     *
     *  @return a list of <code>Checks</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.CheckList getChecks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.rules.check.CheckQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getChecksByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.rules.check.CheckQuery getQuery() {
        org.osid.rules.check.CheckQuery query = this.session.getCheckQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
