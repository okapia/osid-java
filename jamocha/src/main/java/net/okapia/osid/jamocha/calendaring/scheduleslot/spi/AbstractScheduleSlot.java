//
// AbstractScheduleSlot.java
//
//     Defines a ScheduleSlot.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.scheduleslot.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>ScheduleSlot</code>.
 */

public abstract class AbstractScheduleSlot
    extends net.okapia.osid.jamocha.spi.AbstractContainableOsidObject
    implements org.osid.calendaring.ScheduleSlot {

    private final java.util.Collection<org.osid.calendaring.ScheduleSlot> scheduleSlots = new java.util.LinkedHashSet<>();
    private java.util.Collection<Long> weekdays = new java.util.HashSet<>();
    private long weeklyInterval;
    private long weekOfMonth;
    private org.osid.calendaring.Time weekdayTime;
    private org.osid.calendaring.Duration fixedInterval;
    private org.osid.calendaring.Duration duration;

    private boolean hasWeekOfMonthInterval = false;

    private final java.util.Collection<org.osid.calendaring.records.ScheduleSlotRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Ids </code> of the schedule slots included inside this 
     *  one. 
     *
     *  @return the schedules slot <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getScheduleSlotIds() {
        try {
            org.osid.calendaring.ScheduleSlotList scheduleSlots = getScheduleSlots();
            return (new net.okapia.osid.jamocha.adapter.converter.calendaring.scheduleslot.ScheduleSlotToIdList(scheduleSlots));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the schedule slots included inside this one. 
     *
     *  @return the schedule slots 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotList getScheduleSlots()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.calendaring.scheduleslot.ArrayScheduleSlotList(this.scheduleSlots));
    }


    /**
     *  Adds a schedule slot.
     *
     *  @param slot a schedule slot
     *  @throws org.osid.NullArgumentException
     *          <code>slot</code> is <code>null</code>
     */

    protected void addScheduleSlot(org.osid.calendaring.ScheduleSlot slot) {
        nullarg(slot, "schedule slot");
        this.scheduleSlots.add(slot);
        return;
    }


    /**
     *  Sets all the schedule slots.
     *
     *  @param slots a collection of schedule slots
     *  @throws org.osid.NullArgumentException
     *          <code>slots</code> is <code>null</code>
     */

    protected void setScheduleSlots(java.util.Collection<org.osid.calendaring.ScheduleSlot> slots) {
        nullarg(slots, "schedule slots");

        this.scheduleSlots.clear();
        this.scheduleSlots.addAll(slots);

        return;
    }


    /**
     *  Tests if this schedule has a weekly interval. If <code> true,
     *  </code> <code> hasFixedInterval() </code> must be <code>
     *  false. </code>
     *
     *  @return <code> true </code> if there is a weekly interval,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean hasWeeklyInterval() {
        return (!hasFixedInterval() && (this.weekdays.size() > 0));
    }


    /**
     *  Gets the weekdays of the schedule. On a Gregorian calendar,
     *  Sunday is zero.
     *
     *  @return the weekdays 
     *  @throws org.osid.IllegalStateException <code>
     *          hasWeeklyInterval() </code> is <code> false </code>
     */

    @OSID @Override
    public long[] getWeekdays() {
        if (!hasWeeklyInterval()) {
            throw new org.osid.IllegalStateException("hasWeeklyInterval() is false");
        }

        long[] ret = new long[this.weekdays.size()];
        int i = 0;
        for (Long l : this.weekdays) {
            ret[i++] = l;
        }

        return (ret);
    }


    /**
     *  Adds a weekday.
     *
     *  @param weekday a weekday
     *  @throws org.osid.InvalidArgumentException <code>weekday</code>
     *          is negative
     */

    protected void addWeekday(long weekday) {
        cardinalarg(weekday, "weekday");

        this.weekdays.add(weekday);
        this.fixedInterval = null;       

        return;
    }


    /**
     *  Sets all the weekdays.
     *
     *  @param weekdays a collection of weekdays
     *  @throws org.osid.InvalidArgumentException a weekday is
     *          negative
     *  @throws org.osid.NullArgumentException
     *          <code>weekdays</code> is <code>null</code>
     */

    protected void setWeekdays(java.util.Collection<Long> weekdays) {
        nullarg(weekdays, "weekdays");

        this.weekdays.clear();
        for (long l : weekdays) {
            cardinalarg(l, "weekday");
            this.weekdays.add(l);
        }

        this.fixedInterval = null;
        return;
    }


    /**
     *  Tests if this schedule has a weekly interval based on the week of the 
     *  month. This method must be <code> false </code> if <code> 
     *  hasWeeklyInterval() </code> is <code> false. </code> 
     *
     *  @return <code> true </code> if there is a week of month specified, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasWeekOfMonthInterval() {
        if (!hasWeeklyInterval()) {
            return (false);
        }

        return (this.hasWeekOfMonthInterval);
    }


    /**
     *  Gets the number of weeks of the interval. 1 is every week. 2
     *  is every other week. -1 is every week back in time.
     *
     *  @return <code> the week interval </code> 
     *  @throws org.osid.IllegalStateException <code> hasWeeklyInterval() 
     *          </code> is <code> false </code> or <code> 
     *          hasWeekofMonthInterval() </code> is <code> true </code> 
     */

    @OSID @Override
    public long getWeeklyInterval() {
        if (!hasWeeklyInterval()) {
            throw new org.osid.IllegalStateException("hasWeeklyInterval() is false");
        }

        if (hasWeekOfMonthInterval()) {
            throw new org.osid.IllegalStateException("hasWeekOfMonthInterval() is true");
        }

        return (this.weeklyInterval);
    }


    /**
     *  Sets the weekly interval.
     *
     *  @param weeklyInterval a weekly interval
     */

    protected void setWeeklyInterval(long weeklyInterval) {
        this.weeklyInterval = weeklyInterval;
        this.fixedInterval  = null;

        return;
    }


    /**
     *  Gets the week of the month for the interval. 1 is the first week of 
     *  the month. -1 is the last week of the month. 0 is invalid. 
     *
     *  @return <code> the week interval </code> 
     *  @throws org.osid.IllegalStateException <code> hasWeeklyInterval() 
     *          </code> is <code> false </code> or <code> 
     *          hasWeekofMonthInterval() </code> is <code> false </code> 
     */

    @OSID @Override
    public long getWeekOfMonth() {
        if (!hasWeeklyInterval()) {
            throw new org.osid.IllegalStateException("hasWeeklyInterval() is false");
        }

        if (!hasWeekOfMonthInterval()) {
            throw new org.osid.IllegalStateException("hasWeekOfMonthInterval() is false");
        }

        return (this.weekOfMonth);
    }


    /**
     *  Sets the week of month.
     *
     *  @param week a week of month
     */

    protected void setWeekOfMonth(long week) {
        this.weekOfMonth            = week;
        this.hasWeekOfMonthInterval = true;
        this.fixedInterval          = null;

        return;
    }


    /**
     *  Gets the time of this recurring schedule. 
     *
     *  @return the time 
     *  @throws org.osid.IllegalStateException <code> hasWeeklyInterval() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Time getWeekdayTime() {
        if (!hasWeeklyInterval()) {
            throw new org.osid.IllegalStateException("hasWeeklyInterval() is false");
        }

        return (this.weekdayTime);
    }


    /**
     *  Sets the weekday time.
     *
     *  @param time a weekday time
     *  @throws org.osid.NullArgumentException
     *          <code>time</code> is <code>null</code>
     */

    protected void setWeekdayTime(org.osid.calendaring.Time time) {
        nullarg(time, "weekday time");

        this.weekdayTime   = time;
        this.fixedInterval = null;

        return;
    }


    /**
     *  Tests if this schedule has a fixed time interval. 
     *
     *  @return <code> true </code> if there is a fixed time interval, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasFixedInterval() {
        return (this.fixedInterval != null);
    }


    /**
     *  Gets the repeating interval. 
     *
     *  @return the interval 
     *  @throws org.osid.IllegalStateException <code> hasFixedInterval() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getFixedInterval() {
        if (!hasFixedInterval()) {
            throw new org.osid.IllegalStateException("hasFixedInterval() is false");
        }

        return (this.fixedInterval);
    }


    /**
     *  Sets the fixed interval.
     *
     *  @param interval a fixed interval
     *  @throws org.osid.NullArgumentException
     *          <code>interval</code> is <code>null</code>
     */

    protected void setFixedInterval(org.osid.calendaring.Duration interval) {
        nullarg(interval, "fixed interval");

        this.fixedInterval          = interval;
        this.hasWeekOfMonthInterval = false;
        this.weekdayTime            = null;
        this.weekdays.clear();

        return;

    }


    /**
     *  Gets the duration.
     *
     *  @return the duration 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getDuration() {
        return (this.duration);
    }


    /**
     *  Sets the duration.
     *
     *  @param duration the duration
     *  @throws org.osid.NullArgumentException <code>duration</code> is
     *          <code>null</code>
     */

    protected void setDuration(org.osid.calendaring.Duration duration) {
        nullarg(duration, "duration");
        this.duration = duration;
        return;
    }


    /**
     *  Tests if this schedule slot supports the given record
     *  <code>Type</code>.
     *
     *  @param  scheduleSlotRecordType a schedule slot record type 
     *  @return <code>true</code> if the scheduleSlotRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleSlotRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type scheduleSlotRecordType) {
        for (org.osid.calendaring.records.ScheduleSlotRecord record : this.records) {
            if (record.implementsRecordType(scheduleSlotRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>ScheduleSlot</code> record <code>Type</code>.
     *
     *  @param  scheduleSlotRecordType the schedule slot record type 
     *  @return the schedule slot record 
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleSlotRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(scheduleSlotRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.ScheduleSlotRecord getScheduleSlotRecord(org.osid.type.Type scheduleSlotRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.ScheduleSlotRecord record : this.records) {
            if (record.implementsRecordType(scheduleSlotRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(scheduleSlotRecordType + " is not supported");
    }


    /**
     *  Adds a record to this schedule slot. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param scheduleSlotRecord the schedule slot record
     *  @param scheduleSlotRecordType schedule slot record type
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleSlotRecord</code> or
     *          <code>scheduleSlotRecordType</code> is
     *          <code>null</code>
     */
            
    protected void addScheduleSlotRecord(org.osid.calendaring.records.ScheduleSlotRecord scheduleSlotRecord, 
                                         org.osid.type.Type scheduleSlotRecordType) {

        nullarg(scheduleSlotRecord, "schedule slot record");
        addRecordType(scheduleSlotRecordType);
        this.records.add(scheduleSlotRecord);
        
        return;
    }
}
