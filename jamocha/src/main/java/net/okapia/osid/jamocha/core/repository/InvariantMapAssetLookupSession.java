//
// InvariantMapAssetLookupSession
//
//    Implements an Asset lookup service backed by a fixed collection of
//    assets.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.repository;


/**
 *  Implements an Asset lookup service backed by a fixed
 *  collection of assets. The assets are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapAssetLookupSession
    extends net.okapia.osid.jamocha.core.repository.spi.AbstractMapAssetLookupSession
    implements org.osid.repository.AssetLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapAssetLookupSession</code> with no
     *  assets.
     *  
     *  @param repository the repository
     *  @throws org.osid.NullArgumnetException {@code repository} is
     *          {@code null}
     */

    public InvariantMapAssetLookupSession(org.osid.repository.Repository repository) {
        setRepository(repository);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAssetLookupSession</code> with a single
     *  asset.
     *  
     *  @param repository the repository
     *  @param asset an single asset
     *  @throws org.osid.NullArgumentException {@code repository} or
     *          {@code asset} is <code>null</code>
     */

      public InvariantMapAssetLookupSession(org.osid.repository.Repository repository,
                                               org.osid.repository.Asset asset) {
        this(repository);
        putAsset(asset);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAssetLookupSession</code> using an array
     *  of assets.
     *  
     *  @param repository the repository
     *  @param assets an array of assets
     *  @throws org.osid.NullArgumentException {@code repository} or
     *          {@code assets} is <code>null</code>
     */

      public InvariantMapAssetLookupSession(org.osid.repository.Repository repository,
                                               org.osid.repository.Asset[] assets) {
        this(repository);
        putAssets(assets);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAssetLookupSession</code> using a
     *  collection of assets.
     *
     *  @param repository the repository
     *  @param assets a collection of assets
     *  @throws org.osid.NullArgumentException {@code repository} or
     *          {@code assets} is <code>null</code>
     */

      public InvariantMapAssetLookupSession(org.osid.repository.Repository repository,
                                               java.util.Collection<? extends org.osid.repository.Asset> assets) {
        this(repository);
        putAssets(assets);
        return;
    }
}
