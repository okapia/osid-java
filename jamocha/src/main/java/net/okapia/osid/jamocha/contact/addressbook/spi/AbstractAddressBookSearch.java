//
// AbstractAddressBookSearch.java
//
//     A template for making an AddressBook Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.contact.addressbook.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing address book searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractAddressBookSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.contact.AddressBookSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.contact.records.AddressBookSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.contact.AddressBookSearchOrder addressBookSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of address books. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  addressBookIds list of address books
     *  @throws org.osid.NullArgumentException
     *          <code>addressBookIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongAddressBooks(org.osid.id.IdList addressBookIds) {
        while (addressBookIds.hasNext()) {
            try {
                this.ids.add(addressBookIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongAddressBooks</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of address book Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getAddressBookIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  addressBookSearchOrder address book search order 
     *  @throws org.osid.NullArgumentException
     *          <code>addressBookSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>addressBookSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderAddressBookResults(org.osid.contact.AddressBookSearchOrder addressBookSearchOrder) {
	this.addressBookSearchOrder = addressBookSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.contact.AddressBookSearchOrder getAddressBookSearchOrder() {
	return (this.addressBookSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given address book search
     *  record <code> Type. </code> This method must be used to
     *  retrieve an address book implementing the requested record.
     *
     *  @param addressBookSearchRecordType an address book search record
     *         type
     *  @return the address book search record
     *  @throws org.osid.NullArgumentException
     *          <code>addressBookSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(addressBookSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.contact.records.AddressBookSearchRecord getAddressBookSearchRecord(org.osid.type.Type addressBookSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.contact.records.AddressBookSearchRecord record : this.records) {
            if (record.implementsRecordType(addressBookSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(addressBookSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this address book search. 
     *
     *  @param addressBookSearchRecord address book search record
     *  @param addressBookSearchRecordType addressBook search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAddressBookSearchRecord(org.osid.contact.records.AddressBookSearchRecord addressBookSearchRecord, 
                                           org.osid.type.Type addressBookSearchRecordType) {

        addRecordType(addressBookSearchRecordType);
        this.records.add(addressBookSearchRecord);        
        return;
    }
}
