//
// AbstractIndexedMapProfileLookupSession.java
//
//    A simple framework for providing a Profile lookup service
//    backed by a fixed collection of profiles with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.profile.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Profile lookup service backed by a
 *  fixed collection of profiles. The profiles are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some profiles may be compatible
 *  with more types than are indicated through these profile
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Profiles</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapProfileLookupSession
    extends AbstractMapProfileLookupSession
    implements org.osid.profile.ProfileLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.profile.Profile> profilesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.profile.Profile>());
    private final MultiMap<org.osid.type.Type, org.osid.profile.Profile> profilesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.profile.Profile>());


    /**
     *  Makes a <code>Profile</code> available in this session.
     *
     *  @param  profile a profile
     *  @throws org.osid.NullArgumentException <code>profile<code> is
     *          <code>null</code>
     */

    @Override
    protected void putProfile(org.osid.profile.Profile profile) {
        super.putProfile(profile);

        this.profilesByGenus.put(profile.getGenusType(), profile);
        
        try (org.osid.type.TypeList types = profile.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.profilesByRecord.put(types.getNextType(), profile);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a profile from this session.
     *
     *  @param profileId the <code>Id</code> of the profile
     *  @throws org.osid.NullArgumentException <code>profileId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeProfile(org.osid.id.Id profileId) {
        org.osid.profile.Profile profile;
        try {
            profile = getProfile(profileId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.profilesByGenus.remove(profile.getGenusType());

        try (org.osid.type.TypeList types = profile.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.profilesByRecord.remove(types.getNextType(), profile);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeProfile(profileId);
        return;
    }


    /**
     *  Gets a <code>ProfileList</code> corresponding to the given
     *  profile genus <code>Type</code> which does not include
     *  profiles of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known profiles or an error results. Otherwise,
     *  the returned list may contain only those profiles that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  profileGenusType a profile genus type 
     *  @return the returned <code>Profile</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>profileGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileList getProfilesByGenusType(org.osid.type.Type profileGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.profile.profile.ArrayProfileList(this.profilesByGenus.get(profileGenusType)));
    }


    /**
     *  Gets a <code>ProfileList</code> containing the given
     *  profile record <code>Type</code>. In plenary mode, the
     *  returned list contains all known profiles or an error
     *  results. Otherwise, the returned list may contain only those
     *  profiles that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  profileRecordType a profile record type 
     *  @return the returned <code>profile</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>profileRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileList getProfilesByRecordType(org.osid.type.Type profileRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.profile.profile.ArrayProfileList(this.profilesByRecord.get(profileRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.profilesByGenus.clear();
        this.profilesByRecord.clear();

        super.close();

        return;
    }
}
