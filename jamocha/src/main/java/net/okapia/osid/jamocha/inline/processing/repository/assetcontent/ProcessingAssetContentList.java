//
// ProcessingAssetContentList.java
//
//     Runs a given list through a rules assetContent.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.processing.repository.assetcontent;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.primordium.type.URNType;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Runs a given list through a rules assetContent. The underlying rules
 *  assetContent must support the <code>Condition</code> record
 *  (<code>osid.rules.records.ConditionRecord:osid.repository.AssetContent@okapia.net</code>)
 *  and <code>Result</code>
 *  (<code>osid.rules.records.ResultRecord:osid.repository.AssetContent@okapia.net</code>)
 *  record for the input and output lists if no condition is
 *  specified.
 */

public final class ProcessingAssetContentList
    extends net.okapia.osid.jamocha.adapter.federator.repository.assetcontent.CompositeAssetContentList
    implements org.osid.repository.AssetContentList {

    private static final org.osid.type.Type CONDITION_TYPE = URNType.valueOf("urn:inet:osid.org:types:records:rules:Condition:osid.repository.AssetContent");
    private static final org.osid.type.Type RESULT_TYPE    = URNType.valueOf("urn:inet:osid.org:types:records:rules:Result:osid.repository.AssetContent");


    /*
     *  Creates a new <code>ProcessingAssetContentList</code>.
     *
     *  @param assetContentList an <code>AssetContentList</code> to filter
     *  @param rulesSession a <code>RulesSession</code> to use for
     *         evaluating rules
     *  @param ruleId the <code>Id</code> of a <code>Rule</code> to evaluate
     *  @throws org.osid.NullArgumentException
     *          <code>assetContentList</code>, <code>rulesSession</code>,
     *          or <code>ruleId</code> is <code>null</code>
     *  @throws org.osid.NotFoundException <code>ruleId</code> not found
     *  @throws org.osid.OperationFailedException 
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    public ProcessingAssetContentList(org.osid.repository.AssetContentList assetContentList, 
                                  org.osid.rules.RulesSession rulesSession,
                                  org.osid.id.Id ruleId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.rules.Condition condition = rulesSession.getConditionForRule(ruleId);
        if (!condition.hasRecordType(CONDITION_TYPE)) {
            throw new org.osid.OperationFailedException("record type not supported");
        }

        net.okapia.osid.records.rules.condition.repository.AssetContentConditionRecord crecord = (net.okapia.osid.records.rules.condition.repository.AssetContentConditionRecord) condition.getConditionRecord(CONDITION_TYPE);
        crecord.addAssetContentList(assetContentList);
        
        org.osid.rules.Result result = rulesSession.executeRule(ruleId, condition);
        if (!result.hasRecordType(CONDITION_TYPE)) {
            throw new org.osid.OperationFailedException("record type not supported");
        }

        net.okapia.osid.records.rules.result.repository.AssetContentResultRecord rrecord = (net.okapia.osid.records.rules.result.repository.AssetContentResultRecord) result.getResultRecord(RESULT_TYPE);
        addAssetContentList(rrecord.getAssetContents());
        noMore();

        return;
    }


    /**
     *  Creates a new <code>ProcessingAssetContentList</code>.
     *
     *  @param assetContentList an <code>AssetContentList</code> to filter
     *  @param rulesSession a <code>RulesSession</code> to use for
     *         evaluating rules
     *  @param ruleId the <code>Id</code> of a <code>Rule</code> to evaluate
     *  @param condition a condition to use for the rules evaluation
     *  @throws org.osid.NullArgumentException
     *          <code>assetContentList</code>, <code>rulesSession</code>,
     *          or <code>rules</code> is <code>null</code>
     *  @throws org.osid.NotFoundException <code>ruleId</code> not found
     *  @throws org.osid.OperationFailedException 
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    public ProcessingAssetContentList(org.osid.repository.AssetContentList assetContentList, 
                                  org.osid.rules.RulesSession rulesSession,
                                  org.osid.id.Id ruleId, org.osid.rules.Condition condition)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.rules.Result result = rulesSession.executeRule(ruleId, condition);
        if (!result.hasRecordType(RESULT_TYPE)) {
            throw new org.osid.OperationFailedException("record type not supported");
        }

        net.okapia.osid.records.rules.result.repository.AssetContentResultRecord rrecord = (net.okapia.osid.records.rules.result.repository.AssetContentResultRecord) result.getResultRecord(RESULT_TYPE);
        addAssetContentList(rrecord.getAssetContents());
        noMore();

        return;
    }
}

