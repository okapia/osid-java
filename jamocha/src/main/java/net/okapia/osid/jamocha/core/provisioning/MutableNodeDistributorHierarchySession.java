//
// MutableNodeDistributorHierarchySession.java
//
//     Defines a Distributor hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning;


/**
 *  Defines a distributor hierarchy session for delivering a hierarchy
 *  of distributors using the DistributorNode interface.
 */

public final class MutableNodeDistributorHierarchySession
    extends net.okapia.osid.jamocha.core.provisioning.spi.AbstractNodeDistributorHierarchySession
    implements org.osid.provisioning.DistributorHierarchySession {


    /**
     *  Constructs a new
     *  <code>MutableNodeDistributorHierarchySession</code> with no
     *  nodes.
     *
     *  @param hierarchy the hierarchy for this session
     *  @throws org.osid.NullArgumentException <code>hierarchy</code> 
     *          is <code>null</code>
     */

    public MutableNodeDistributorHierarchySession(org.osid.hierarchy.Hierarchy hierarchy) {
        setHierarchy(hierarchy);
        return;
    }


    /**
     *  Constructs a new
     *  <code>MutableNodeDistributorHierarchySession</code> using the
     *  root node for the hierarchy.
     *
     *  @param root a root node
     *  @throws org.osid.NullArgumentException <code>node</code> 
     *          is <code>null</code>
     */

    public MutableNodeDistributorHierarchySession(org.osid.provisioning.DistributorNode root) {
        setHierarchy(new net.okapia.osid.jamocha.builder.hierarchy.hierarchy.HierarchyBuilder()
                     .id(root.getId())
                     .displayName(root.getDistributor().getDisplayName())
                     .description(root.getDistributor().getDescription())
                     .build());

        addRootDistributor(root);
        return;
    }


    /**
     *  Constructs a new
     *  <code>MutableNodeDistributorHierarchySession</code> using the
     *  given root as the root node.
     *
     *  @param hierarchy the hierarchy for this session
     *  @param root a root node
     *  @throws org.osid.NullArgumentException <code>hierarchy</code>
     *          or <code>root</code> is <code>null</code>
     */

    public MutableNodeDistributorHierarchySession(org.osid.hierarchy.Hierarchy hierarchy, 
                                               org.osid.provisioning.DistributorNode root) {
        setHierarchy(hierarchy);
        addRootDistributor(root);
        return;
    }


    /**
     *  Constructs a new
     *  <code>MutableNodeDistributorHierarchySession</code> using a
     *  collection of nodes as roots in the hierarchy.
     *
     *  @param hierarchy the hierarchy for this session
     *  @param roots a collection of root nodes
     *  @throws org.osid.NullArgumentException <code>hierarchy</code>
     *          or <code>roots</code> is <code>null</code>
     */

    public MutableNodeDistributorHierarchySession(org.osid.hierarchy.Hierarchy hierarchy, 
                                               java.util.Collection<org.osid.provisioning.DistributorNode> roots) {
        setHierarchy(hierarchy);
        addRootDistributors(roots);
        return;
    }


    /**
     *  Adds a root distributor node to the hierarchy.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    @Override
    public void addRootDistributor(org.osid.provisioning.DistributorNode root) {
        super.addRootDistributor(root);
        return;
    }


    /**
     *  Adds a collection of root distributor nodes.
     *
     *  @param roots hierarchy roots
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    @Override
    public void addRootDistributors(java.util.Collection<org.osid.provisioning.DistributorNode> roots) {
        super.addRootDistributors(roots);
        return;
    }


    /**
     *  Removes a root distributor node from the hierarchy.
     *
     *  @param rootId a root node {@code Id}
     *  @throws org.osid.NullArgumentException <code>rootId</code> is
     *          <code>null</code>
     */

    @Override
    public void removeRootDistributor(org.osid.id.Id rootId) {
        super.removeRootDistributor(rootId);
        return;
    }
}
