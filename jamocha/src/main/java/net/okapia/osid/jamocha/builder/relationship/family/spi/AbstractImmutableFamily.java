//
// AbstractImmutableFamily.java
//
//     Wraps a mutable Family to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.relationship.family.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Family</code> to hide modifiers. This
 *  wrapper provides an immutized Family from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying family whose state changes are visible.
 */

public abstract class AbstractImmutableFamily
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidCatalog
    implements org.osid.relationship.Family {

    private final org.osid.relationship.Family family;


    /**
     *  Constructs a new <code>AbstractImmutableFamily</code>.
     *
     *  @param family the family to immutablize
     *  @throws org.osid.NullArgumentException <code>family</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableFamily(org.osid.relationship.Family family) {
        super(family);
        this.family = family;
        return;
    }


    /**
     *  Gets the famly record corresponding to the given <code> Family </code> 
     *  record <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record. The <code> familyRecordType </code> 
     *  may be the <code> Type </code> returned in <code> getRecordTypes() 
     *  </code> or any of its parents in a <code> Type </code> hierarchy where 
     *  <code> hasRecordType(familyRecordType) </code> is <code> true </code> 
     *  . 
     *
     *  @param  familyRecordType the type of family record to retrieve 
     *  @return the family record 
     *  @throws org.osid.NullArgumentException <code> familyRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(familyRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.relationship.records.FamilyRecord getFamilyRecord(org.osid.type.Type familyRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.family.getFamilyRecord(familyRecordType));
    }
}

