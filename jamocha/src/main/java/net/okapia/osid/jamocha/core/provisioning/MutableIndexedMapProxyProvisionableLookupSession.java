//
// MutableIndexedMapProxyProvisionableLookupSession
//
//    Implements a Provisionable lookup service backed by a collection of
//    provisionables indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning;


/**
 *  Implements a Provisionable lookup service backed by a collection of
 *  provisionables. The provisionables are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some provisionables may be compatible
 *  with more types than are indicated through these provisionable
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of provisionables can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyProvisionableLookupSession
    extends net.okapia.osid.jamocha.core.provisioning.spi.AbstractIndexedMapProvisionableLookupSession
    implements org.osid.provisioning.ProvisionableLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyProvisionableLookupSession} with
     *  no provisionable.
     *
     *  @param distributor the distributor
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyProvisionableLookupSession(org.osid.provisioning.Distributor distributor,
                                                       org.osid.proxy.Proxy proxy) {
        setDistributor(distributor);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyProvisionableLookupSession} with
     *  a single provisionable.
     *
     *  @param distributor the distributor
     *  @param  provisionable an provisionable
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributor},
     *          {@code provisionable}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyProvisionableLookupSession(org.osid.provisioning.Distributor distributor,
                                                       org.osid.provisioning.Provisionable provisionable, org.osid.proxy.Proxy proxy) {

        this(distributor, proxy);
        putProvisionable(provisionable);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyProvisionableLookupSession} using
     *  an array of provisionables.
     *
     *  @param distributor the distributor
     *  @param  provisionables an array of provisionables
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributor},
     *          {@code provisionables}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyProvisionableLookupSession(org.osid.provisioning.Distributor distributor,
                                                       org.osid.provisioning.Provisionable[] provisionables, org.osid.proxy.Proxy proxy) {

        this(distributor, proxy);
        putProvisionables(provisionables);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyProvisionableLookupSession} using
     *  a collection of provisionables.
     *
     *  @param distributor the distributor
     *  @param  provisionables a collection of provisionables
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributor},
     *          {@code provisionables}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyProvisionableLookupSession(org.osid.provisioning.Distributor distributor,
                                                       java.util.Collection<? extends org.osid.provisioning.Provisionable> provisionables,
                                                       org.osid.proxy.Proxy proxy) {
        this(distributor, proxy);
        putProvisionables(provisionables);
        return;
    }

    
    /**
     *  Makes a {@code Provisionable} available in this session.
     *
     *  @param  provisionable a provisionable
     *  @throws org.osid.NullArgumentException {@code provisionable{@code 
     *          is {@code null}
     */

    @Override
    public void putProvisionable(org.osid.provisioning.Provisionable provisionable) {
        super.putProvisionable(provisionable);
        return;
    }


    /**
     *  Makes an array of provisionables available in this session.
     *
     *  @param  provisionables an array of provisionables
     *  @throws org.osid.NullArgumentException {@code provisionables{@code 
     *          is {@code null}
     */

    @Override
    public void putProvisionables(org.osid.provisioning.Provisionable[] provisionables) {
        super.putProvisionables(provisionables);
        return;
    }


    /**
     *  Makes collection of provisionables available in this session.
     *
     *  @param  provisionables a collection of provisionables
     *  @throws org.osid.NullArgumentException {@code provisionable{@code 
     *          is {@code null}
     */

    @Override
    public void putProvisionables(java.util.Collection<? extends org.osid.provisioning.Provisionable> provisionables) {
        super.putProvisionables(provisionables);
        return;
    }


    /**
     *  Removes a Provisionable from this session.
     *
     *  @param provisionableId the {@code Id} of the provisionable
     *  @throws org.osid.NullArgumentException {@code provisionableId{@code  is
     *          {@code null}
     */

    @Override
    public void removeProvisionable(org.osid.id.Id provisionableId) {
        super.removeProvisionable(provisionableId);
        return;
    }    
}
