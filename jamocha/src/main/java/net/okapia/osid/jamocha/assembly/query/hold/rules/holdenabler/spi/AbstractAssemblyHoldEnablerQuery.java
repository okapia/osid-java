//
// AbstractAssemblyHoldEnablerQuery.java
//
//     A HoldEnablerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.hold.rules.holdenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A HoldEnablerQuery that stores terms.
 */

public abstract class AbstractAssemblyHoldEnablerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidEnablerQuery
    implements org.osid.hold.rules.HoldEnablerQuery,
               org.osid.hold.rules.HoldEnablerQueryInspector,
               org.osid.hold.rules.HoldEnablerSearchOrder {

    private final java.util.Collection<org.osid.hold.rules.records.HoldEnablerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.hold.rules.records.HoldEnablerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.hold.rules.records.HoldEnablerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyHoldEnablerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyHoldEnablerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches enablers mapped to the hold. 
     *
     *  @param  holdId the hold <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> holdId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchRuledHoldId(org.osid.id.Id holdId, boolean match) {
        getAssembler().addIdTerm(getRuledHoldIdColumn(), holdId, match);
        return;
    }


    /**
     *  Clears the hold <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledHoldIdTerms() {
        getAssembler().clearTerms(getRuledHoldIdColumn());
        return;
    }


    /**
     *  Gets the hold <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledHoldIdTerms() {
        return (getAssembler().getIdTerms(getRuledHoldIdColumn()));
    }


    /**
     *  Gets the RuledHoldId column name.
     *
     * @return the column name
     */

    protected String getRuledHoldIdColumn() {
        return ("ruled_hold_id");
    }


    /**
     *  Tests if a <code> HoldQuery </code> is available. 
     *
     *  @return <code> true </code> if a hold query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledHoldQuery() {
        return (false);
    }


    /**
     *  Gets the query for a hold. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the hold query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledHoldQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.HoldQuery getRuledHoldQuery() {
        throw new org.osid.UnimplementedException("supportsRuledHoldQuery() is false");
    }


    /**
     *  Matches enablers mapped to any hold. 
     *
     *  @param  match <code> true </code> for enablers mapped to any hold, 
     *          <code> false </code> to match enablers mapped to no hold 
     */

    @OSID @Override
    public void matchAnyRuledHold(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledHoldColumn(), match);
        return;
    }


    /**
     *  Clears the hold query terms. 
     */

    @OSID @Override
    public void clearRuledHoldTerms() {
        getAssembler().clearTerms(getRuledHoldColumn());
        return;
    }


    /**
     *  Gets the hold query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.hold.HoldQueryInspector[] getRuledHoldTerms() {
        return (new org.osid.hold.HoldQueryInspector[0]);
    }


    /**
     *  Gets the RuledHold column name.
     *
     * @return the column name
     */

    protected String getRuledHoldColumn() {
        return ("ruled_hold");
    }


    /**
     *  Matches enablers mapped to the oubliette. 
     *
     *  @param  oublietteId the oubliette <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchOublietteId(org.osid.id.Id oublietteId, boolean match) {
        getAssembler().addIdTerm(getOublietteIdColumn(), oublietteId, match);
        return;
    }


    /**
     *  Clears the oubliette <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearOublietteIdTerms() {
        getAssembler().clearTerms(getOublietteIdColumn());
        return;
    }


    /**
     *  Gets the oubliette <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOublietteIdTerms() {
        return (getAssembler().getIdTerms(getOublietteIdColumn()));
    }


    /**
     *  Gets the OublietteId column name.
     *
     * @return the column name
     */

    protected String getOublietteIdColumn() {
        return ("oubliette_id");
    }


    /**
     *  Tests if a <code> OublietteQuery </code> is available. 
     *
     *  @return <code> true </code> if an oubliette query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOublietteQuery() {
        return (false);
    }


    /**
     *  Gets the query for an oubliette. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the oubliette query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOublietteQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.OublietteQuery getOublietteQuery() {
        throw new org.osid.UnimplementedException("supportsOublietteQuery() is false");
    }


    /**
     *  Clears the oubliette query terms. 
     */

    @OSID @Override
    public void clearOublietteTerms() {
        getAssembler().clearTerms(getOublietteColumn());
        return;
    }


    /**
     *  Gets the oubliette query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.hold.OublietteQueryInspector[] getOublietteTerms() {
        return (new org.osid.hold.OublietteQueryInspector[0]);
    }


    /**
     *  Gets the Oubliette column name.
     *
     * @return the column name
     */

    protected String getOublietteColumn() {
        return ("oubliette");
    }


    /**
     *  Tests if this holdEnabler supports the given record
     *  <code>Type</code>.
     *
     *  @param  holdEnablerRecordType a hold enabler record type 
     *  @return <code>true</code> if the holdEnablerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>holdEnablerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type holdEnablerRecordType) {
        for (org.osid.hold.rules.records.HoldEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(holdEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  holdEnablerRecordType the hold enabler record type 
     *  @return the hold enabler query record 
     *  @throws org.osid.NullArgumentException
     *          <code>holdEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(holdEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.hold.rules.records.HoldEnablerQueryRecord getHoldEnablerQueryRecord(org.osid.type.Type holdEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.hold.rules.records.HoldEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(holdEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(holdEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  holdEnablerRecordType the hold enabler record type 
     *  @return the hold enabler query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>holdEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(holdEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.hold.rules.records.HoldEnablerQueryInspectorRecord getHoldEnablerQueryInspectorRecord(org.osid.type.Type holdEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.hold.rules.records.HoldEnablerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(holdEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(holdEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param holdEnablerRecordType the hold enabler record type
     *  @return the hold enabler search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>holdEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(holdEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.hold.rules.records.HoldEnablerSearchOrderRecord getHoldEnablerSearchOrderRecord(org.osid.type.Type holdEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.hold.rules.records.HoldEnablerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(holdEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(holdEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this hold enabler. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param holdEnablerQueryRecord the hold enabler query record
     *  @param holdEnablerQueryInspectorRecord the hold enabler query inspector
     *         record
     *  @param holdEnablerSearchOrderRecord the hold enabler search order record
     *  @param holdEnablerRecordType hold enabler record type
     *  @throws org.osid.NullArgumentException
     *          <code>holdEnablerQueryRecord</code>,
     *          <code>holdEnablerQueryInspectorRecord</code>,
     *          <code>holdEnablerSearchOrderRecord</code> or
     *          <code>holdEnablerRecordTypeholdEnabler</code> is
     *          <code>null</code>
     */
            
    protected void addHoldEnablerRecords(org.osid.hold.rules.records.HoldEnablerQueryRecord holdEnablerQueryRecord, 
                                      org.osid.hold.rules.records.HoldEnablerQueryInspectorRecord holdEnablerQueryInspectorRecord, 
                                      org.osid.hold.rules.records.HoldEnablerSearchOrderRecord holdEnablerSearchOrderRecord, 
                                      org.osid.type.Type holdEnablerRecordType) {

        addRecordType(holdEnablerRecordType);

        nullarg(holdEnablerQueryRecord, "hold enabler query record");
        nullarg(holdEnablerQueryInspectorRecord, "hold enabler query inspector record");
        nullarg(holdEnablerSearchOrderRecord, "hold enabler search odrer record");

        this.queryRecords.add(holdEnablerQueryRecord);
        this.queryInspectorRecords.add(holdEnablerQueryInspectorRecord);
        this.searchOrderRecords.add(holdEnablerSearchOrderRecord);
        
        return;
    }
}
