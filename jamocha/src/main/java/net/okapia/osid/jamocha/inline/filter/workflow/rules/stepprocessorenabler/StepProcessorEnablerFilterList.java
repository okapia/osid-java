//
// StepProcessorEnablerFilterList.java
//
//     Implements a filtering StepProcessorEnablerList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.workflow.rules.stepprocessorenabler;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filtering StepProcessorEnablerList.
 */

public final class StepProcessorEnablerFilterList
    extends net.okapia.osid.jamocha.inline.filter.workflow.rules.stepprocessorenabler.spi.AbstractStepProcessorEnablerFilterList
    implements org.osid.workflow.rules.StepProcessorEnablerList,
               StepProcessorEnablerFilter {

    private final StepProcessorEnablerFilter filter;


    /**
     *  Creates a new <code>StepProcessorEnablerFilterList</code>.
     *
     *  @param filter an inline query filter
     *  @param list a <code>StepProcessorEnablerList</code>
     *  @throws org.osid.NullArgumentException <code>filter</code> or
     *          <code>list</code> is <code>null</code>
     */

    public StepProcessorEnablerFilterList(StepProcessorEnablerFilter filter, org.osid.workflow.rules.StepProcessorEnablerList list) {
        super(list);

        nullarg(filter, "inline query filter");
        this.filter = filter;

        return;
    }    

    
    /**
     *  Filters StepProcessorEnablers.
     *
     *  @param stepProcessorEnabler the step processor enabler to filter
     *  @return <code>true</code> if the step processor enabler passes the filter,
     *          <code>false</code> if the step processor enabler should be filtered
     */

    @Override
    public boolean pass(org.osid.workflow.rules.StepProcessorEnabler stepProcessorEnabler) {
        return (this.filter.pass(stepProcessorEnabler));
    }
}
