//
// AbstractBidSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.bidding.bid.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractBidSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.bidding.BidSearchResults {

    private org.osid.bidding.BidList bids;
    private final org.osid.bidding.BidQueryInspector inspector;
    private final java.util.Collection<org.osid.bidding.records.BidSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractBidSearchResults.
     *
     *  @param bids the result set
     *  @param bidQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>bids</code>
     *          or <code>bidQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractBidSearchResults(org.osid.bidding.BidList bids,
                                            org.osid.bidding.BidQueryInspector bidQueryInspector) {
        nullarg(bids, "bids");
        nullarg(bidQueryInspector, "bid query inspectpr");

        this.bids = bids;
        this.inspector = bidQueryInspector;

        return;
    }


    /**
     *  Gets the bid list resulting from a search.
     *
     *  @return a bid list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.bidding.BidList getBids() {
        if (this.bids == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.bidding.BidList bids = this.bids;
        this.bids = null;
	return (bids);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.bidding.BidQueryInspector getBidQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  bid search record <code> Type. </code> This method must
     *  be used to retrieve a bid implementing the requested
     *  record.
     *
     *  @param bidSearchRecordType a bid search 
     *         record type 
     *  @return the bid search
     *  @throws org.osid.NullArgumentException
     *          <code>bidSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(bidSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.bidding.records.BidSearchResultsRecord getBidSearchResultsRecord(org.osid.type.Type bidSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.bidding.records.BidSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(bidSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(bidSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record bid search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addBidRecord(org.osid.bidding.records.BidSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "bid record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
