//
// AbstractProjectSearch.java
//
//     A template for making a Project Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.construction.project.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing project searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractProjectSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.room.construction.ProjectSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.room.construction.records.ProjectSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.room.construction.ProjectSearchOrder projectSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of projects. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  projectIds list of projects
     *  @throws org.osid.NullArgumentException
     *          <code>projectIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongProjects(org.osid.id.IdList projectIds) {
        while (projectIds.hasNext()) {
            try {
                this.ids.add(projectIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongProjects</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of project Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getProjectIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  projectSearchOrder project search order 
     *  @throws org.osid.NullArgumentException
     *          <code>projectSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>projectSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderProjectResults(org.osid.room.construction.ProjectSearchOrder projectSearchOrder) {
	this.projectSearchOrder = projectSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.room.construction.ProjectSearchOrder getProjectSearchOrder() {
	return (this.projectSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given project search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a project implementing the requested record.
     *
     *  @param projectSearchRecordType a project search record
     *         type
     *  @return the project search record
     *  @throws org.osid.NullArgumentException
     *          <code>projectSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(projectSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.room.construction.records.ProjectSearchRecord getProjectSearchRecord(org.osid.type.Type projectSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.room.construction.records.ProjectSearchRecord record : this.records) {
            if (record.implementsRecordType(projectSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(projectSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this project search. 
     *
     *  @param projectSearchRecord project search record
     *  @param projectSearchRecordType project search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addProjectSearchRecord(org.osid.room.construction.records.ProjectSearchRecord projectSearchRecord, 
                                           org.osid.type.Type projectSearchRecordType) {

        addRecordType(projectSearchRecordType);
        this.records.add(projectSearchRecord);        
        return;
    }
}
