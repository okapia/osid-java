//
// AbstractFederatingBrokerLookupSession.java
//
//     An abstract federating adapter for a BrokerLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.provisioning.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  BrokerLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingBrokerLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.provisioning.BrokerLookupSession>
    implements org.osid.provisioning.BrokerLookupSession {

    private boolean parallel = false;
    private org.osid.provisioning.Distributor distributor = new net.okapia.osid.jamocha.nil.provisioning.distributor.UnknownDistributor();


    /**
     *  Constructs a new <code>AbstractFederatingBrokerLookupSession</code>.
     */

    protected AbstractFederatingBrokerLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.provisioning.BrokerLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Distributor/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Distributor Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDistributorId() {
        return (this.distributor.getId());
    }


    /**
     *  Gets the <code>Distributor</code> associated with this 
     *  session.
     *
     *  @return the <code>Distributor</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Distributor getDistributor()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.distributor);
    }


    /**
     *  Sets the <code>Distributor</code>.
     *
     *  @param  distributor the distributor for this session
     *  @throws org.osid.NullArgumentException <code>distributor</code>
     *          is <code>null</code>
     */

    protected void setDistributor(org.osid.provisioning.Distributor distributor) {
        nullarg(distributor, "distributor");
        this.distributor = distributor;
        return;
    }


    /**
     *  Tests if this user can perform <code>Broker</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupBrokers() {
        for (org.osid.provisioning.BrokerLookupSession session : getSessions()) {
            if (session.canLookupBrokers()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Broker</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeBrokerView() {
        for (org.osid.provisioning.BrokerLookupSession session : getSessions()) {
            session.useComparativeBrokerView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Broker</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryBrokerView() {
        for (org.osid.provisioning.BrokerLookupSession session : getSessions()) {
            session.usePlenaryBrokerView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include brokers in distributors which are children
     *  of this distributor in the distributor hierarchy.
     */

    @OSID @Override
    public void useFederatedDistributorView() {
        for (org.osid.provisioning.BrokerLookupSession session : getSessions()) {
            session.useFederatedDistributorView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this distributor only.
     */

    @OSID @Override
    public void useIsolatedDistributorView() {
        for (org.osid.provisioning.BrokerLookupSession session : getSessions()) {
            session.useIsolatedDistributorView();
        }

        return;
    }


    /**
     *  Only active brokers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveBrokerView() {
        for (org.osid.provisioning.BrokerLookupSession session : getSessions()) {
            session.useActiveBrokerView();
        }

        return;
    }


    /**
     *  Active and inactive brokers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusBrokerView() {
        for (org.osid.provisioning.BrokerLookupSession session : getSessions()) {
            session.useAnyStatusBrokerView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>Broker</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Broker</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Broker</code> and
     *  retained for compatibility.
     *
     *  In active mode, brokers are returned that are currently
     *  active. In any status mode, active and inactive brokers
     *  are returned.
     *
     *  @param  brokerId <code>Id</code> of the
     *          <code>Broker</code>
     *  @return the broker
     *  @throws org.osid.NotFoundException <code>brokerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>brokerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Broker getBroker(org.osid.id.Id brokerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.provisioning.BrokerLookupSession session : getSessions()) {
            try {
                return (session.getBroker(brokerId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(brokerId + " not found");
    }


    /**
     *  Gets a <code>BrokerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  brokers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Brokers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, brokers are returned that are currently
     *  active. In any status mode, active and inactive brokers
     *  are returned.
     *
     *  @param  brokerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Broker</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>brokerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerList getBrokersByIds(org.osid.id.IdList brokerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.provisioning.broker.MutableBrokerList ret = new net.okapia.osid.jamocha.provisioning.broker.MutableBrokerList();

        try (org.osid.id.IdList ids = brokerIds) {
            while (ids.hasNext()) {
                ret.addBroker(getBroker(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>BrokerList</code> corresponding to the given
     *  broker genus <code>Type</code> which does not include
     *  brokers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  brokers or an error results. Otherwise, the returned list
     *  may contain only those brokers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, brokers are returned that are currently
     *  active. In any status mode, active and inactive brokers
     *  are returned.
     *
     *  @param  brokerGenusType a broker genus type 
     *  @return the returned <code>Broker</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>brokerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerList getBrokersByGenusType(org.osid.type.Type brokerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.broker.FederatingBrokerList ret = getBrokerList();

        for (org.osid.provisioning.BrokerLookupSession session : getSessions()) {
            ret.addBrokerList(session.getBrokersByGenusType(brokerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>BrokerList</code> corresponding to the given
     *  broker genus <code>Type</code> and include any additional
     *  brokers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  brokers or an error results. Otherwise, the returned list
     *  may contain only those brokers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, brokers are returned that are currently
     *  active. In any status mode, active and inactive brokers
     *  are returned.
     *
     *  @param  brokerGenusType a broker genus type 
     *  @return the returned <code>Broker</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>brokerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerList getBrokersByParentGenusType(org.osid.type.Type brokerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.broker.FederatingBrokerList ret = getBrokerList();

        for (org.osid.provisioning.BrokerLookupSession session : getSessions()) {
            ret.addBrokerList(session.getBrokersByParentGenusType(brokerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>BrokerList</code> containing the given
     *  broker record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  brokers or an error results. Otherwise, the returned list
     *  may contain only those brokers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, brokers are returned that are currently
     *  active. In any status mode, active and inactive brokers
     *  are returned.
     *
     *  @param  brokerRecordType a broker record type 
     *  @return the returned <code>Broker</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>brokerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerList getBrokersByRecordType(org.osid.type.Type brokerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.broker.FederatingBrokerList ret = getBrokerList();

        for (org.osid.provisioning.BrokerLookupSession session : getSessions()) {
            ret.addBrokerList(session.getBrokersByRecordType(brokerRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>BrokerList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known brokers
     *  or an error results. Otherwise, the returned list may contain
     *  only those brokers that are accessible through this session.
     *
     *  In active mode, brokers are returned that are currently
     *  active. In any status mode, active and inactive brokers
     *  are returned.
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Broker</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerList getBrokersByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        net.okapia.osid.jamocha.adapter.federator.provisioning.broker.FederatingBrokerList ret = getBrokerList();

        for (org.osid.provisioning.BrokerLookupSession session : getSessions()) {
            ret.addBrokerList(session.getBrokersByProvider(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Brokers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  brokers or an error results. Otherwise, the returned list
     *  may contain only those brokers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, brokers are returned that are currently
     *  active. In any status mode, active and inactive brokers
     *  are returned.
     *
     *  @return a list of <code>Brokers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerList getBrokers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.broker.FederatingBrokerList ret = getBrokerList();

        for (org.osid.provisioning.BrokerLookupSession session : getSessions()) {
            ret.addBrokerList(session.getBrokers());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.provisioning.broker.FederatingBrokerList getBrokerList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.provisioning.broker.ParallelBrokerList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.provisioning.broker.CompositeBrokerList());
        }
    }
}
