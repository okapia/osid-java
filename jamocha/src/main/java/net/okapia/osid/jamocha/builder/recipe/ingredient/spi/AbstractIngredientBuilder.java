//
// AbstractIngredient.java
//
//     Defines an Ingredient builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.recipe.ingredient.spi;


/**
 *  Defines an <code>Ingredient</code> builder.
 */

public abstract class AbstractIngredientBuilder<T extends AbstractIngredientBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.recipe.ingredient.IngredientMiter ingredient;


    /**
     *  Constructs a new <code>AbstractIngredientBuilder</code>.
     *
     *  @param ingredient the ingredient to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractIngredientBuilder(net.okapia.osid.jamocha.builder.recipe.ingredient.IngredientMiter ingredient) {
        super(ingredient);
        this.ingredient = ingredient;
        return;
    }


    /**
     *  Builds the ingredient.
     *
     *  @return the new ingredient
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.recipe.Ingredient build() {
        (new net.okapia.osid.jamocha.builder.validator.recipe.ingredient.IngredientValidator(getValidations())).validate(this.ingredient);
        return (new net.okapia.osid.jamocha.builder.recipe.ingredient.ImmutableIngredient(this.ingredient));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the ingredient miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.recipe.ingredient.IngredientMiter getMiter() {
        return (this.ingredient);
    }


    /**
     *  Sets the quantity.
     *
     *  @param quantity a quantity
     *  @return the builder
     *  @throws org.osid.InvalidArgumentException
     *          <code>quantity</code> is negative
     *  @throws org.osid.NullArgumentException <code>quantity</code>
     *          is <code>null</code>
     */

    public T quantity(java.math.BigDecimal quantity) {
        getMiter().setQuantity(quantity);
        return (self());
    }


    /**
     *  Sets the unit type.
     *
     *  @param unitType an unit type
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>unitType</code>
     *          is <code>null</code>
     */

    public T unitType(org.osid.type.Type unitType) {
        getMiter().setUnitType(unitType);
        return (self());
    }


    /**
     *  Sets the stock.
     *
     *  @param stock a stock
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>stock</code> is
     *          <code>null</code>
     */

    public T stock(org.osid.inventory.Stock stock) {
        getMiter().setStock(stock);
        return (self());
    }


    /**
     *  Adds an Ingredient record.
     *
     *  @param record an ingredient record
     *  @param recordType the type of ingredient record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.recipe.records.IngredientRecord record, org.osid.type.Type recordType) {
        getMiter().addIngredientRecord(record, recordType);
        return (self());
    }
}       


