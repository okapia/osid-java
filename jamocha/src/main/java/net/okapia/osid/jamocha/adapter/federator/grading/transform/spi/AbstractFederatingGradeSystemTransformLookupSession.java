//
// AbstractFederatingGradeSystemTransformLookupSession.java
//
//     An abstract federating adapter for a GradeSystemTransformLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.grading.transform.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  GradeSystemTransformLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingGradeSystemTransformLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.grading.transform.GradeSystemTransformLookupSession>
    implements org.osid.grading.transform.GradeSystemTransformLookupSession {

    private boolean parallel = false;
    private org.osid.grading.Gradebook gradebook = new net.okapia.osid.jamocha.nil.grading.gradebook.UnknownGradebook();


    /**
     *  Constructs a new <code>AbstractFederatingGradeSystemTransformLookupSession</code>.
     */

    protected AbstractFederatingGradeSystemTransformLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.grading.transform.GradeSystemTransformLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Gradebook/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Gradebook Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getGradebookId() {
        return (this.gradebook.getId());
    }


    /**
     *  Gets the <code>Gradebook</code> associated with this 
     *  session.
     *
     *  @return the <code>Gradebook</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.Gradebook getGradebook()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.gradebook);
    }


    /**
     *  Sets the <code>Gradebook</code>.
     *
     *  @param  gradebook the gradebook for this session
     *  @throws org.osid.NullArgumentException <code>gradebook</code>
     *          is <code>null</code>
     */

    protected void setGradebook(org.osid.grading.Gradebook gradebook) {
        nullarg(gradebook, "gradebook");
        this.gradebook = gradebook;
        return;
    }


    /**
     *  Tests if this user can perform
     *  <code>GradeSystemTransform</code> lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupGradeSystemTransforms() {
        for (org.osid.grading.transform.GradeSystemTransformLookupSession session : getSessions()) {
            if (session.canLookupGradeSystemTransforms()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>GradeSystemTransform</code>
     *  returns is desired.  Methods will return what is requested or
     *  result in an error. This view is used when greater precision
     *  is desired at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeGradeSystemTransformView() {
        for (org.osid.grading.transform.GradeSystemTransformLookupSession session : getSessions()) {
            session.useComparativeGradeSystemTransformView();
        }

        return;
    }


    /**
     *  A complete view of the <code>GradeSystemTransform</code>
     *  returns is desired.  Methods will return what is requested or
     *  result in an error. This view is used when greater precision
     *  is desired at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryGradeSystemTransformView() {
        for (org.osid.grading.transform.GradeSystemTransformLookupSession session : getSessions()) {
            session.usePlenaryGradeSystemTransformView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include grade system transforms in gradebooks which
     *  are children of this gradebook in the gradebook hierarchy.
     */

    @OSID @Override
    public void useFederatedGradebookView() {
        for (org.osid.grading.transform.GradeSystemTransformLookupSession session : getSessions()) {
            session.useFederatedGradebookView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this gradebook only.
     */

    @OSID @Override
    public void useIsolatedGradebookView() {
        for (org.osid.grading.transform.GradeSystemTransformLookupSession session : getSessions()) {
            session.useIsolatedGradebookView();
        }

        return;
    }


    /**
     *  Only active grade system transforms are returned by methods in
     *  this session.
     */
     
    @OSID @Override
    public void useActiveGradeSystemTransformView() {
        for (org.osid.grading.transform.GradeSystemTransformLookupSession session : getSessions()) {
            session.useActiveGradeSystemTransformView();
        }

        return;
    }


    /**
     *  Active and inactive grade system transforms are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusGradeSystemTransformView() {
        for (org.osid.grading.transform.GradeSystemTransformLookupSession session : getSessions()) {
            session.useAnyStatusGradeSystemTransformView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>GradeSystemTransform</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>GradeSystemTransform</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>GradeSystemTransform</code> and retained for
     *  compatibility.
     *
     *  In active mode, grade system transforms are returned that are
     *  currently active. In any status mode, active and inactive
     *  grade system transforms are returned.
     *
     *  @param  gradeSystemTransformId <code>Id</code> of the
     *          <code>GradeSystemTransform</code>
     *  @return the grade system transform
     *  @throws org.osid.NotFoundException <code>gradeSystemTransformId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>gradeSystemTransformId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.transform.GradeSystemTransform getGradeSystemTransform(org.osid.id.Id gradeSystemTransformId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.grading.transform.GradeSystemTransformLookupSession session : getSessions()) {
            try {
                return (session.getGradeSystemTransform(gradeSystemTransformId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(gradeSystemTransformId + " not found");
    }


    /**
     *  Gets a <code>GradeSystemTransformList</code> corresponding to
     *  the given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  gradeSystemTransforms specified in the <code>Id</code> list,
     *  in the order of the list, including duplicates, or an error
     *  results if an <code>Id</code> in the supplied list is not
     *  found or inaccessible. Otherwise, inaccessible
     *  <code>GradeSystemTransforms</code> may be omitted from the
     *  list and may present the elements in any order including
     *  returning a unique set.
     *
     *  In active mode, grade system transforms are returned that are
     *  currently active. In any status mode, active and inactive
     *  grade system transforms are returned.
     *
     *  @param  gradeSystemTransformIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>GradeSystemTransform</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>gradeSystemTransformIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.transform.GradeSystemTransformList getGradeSystemTransformsByIds(org.osid.id.IdList gradeSystemTransformIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.grading.transform.gradesystemtransform.MutableGradeSystemTransformList ret = new net.okapia.osid.jamocha.grading.transform.gradesystemtransform.MutableGradeSystemTransformList();

        try (org.osid.id.IdList ids = gradeSystemTransformIds) {
            while (ids.hasNext()) {
                ret.addGradeSystemTransform(getGradeSystemTransform(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>GradeSystemTransformList</code> corresponding to
     *  the given grade system transform genus <code>Type</code> which
     *  does not include grade system transforms of types derived from
     *  the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known grade
     *  system transforms or an error results. Otherwise, the returned
     *  list may contain only those grade system transforms that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In active mode, grade system transforms are returned that are
     *  currently active. In any status mode, active and inactive
     *  grade system transforms are returned.
     *
     *  @param  gradeSystemTransformGenusType a gradeSystemTransform genus type 
     *  @return the returned <code>GradeSystemTransform</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>gradeSystemTransformGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.transform.GradeSystemTransformList getGradeSystemTransformsByGenusType(org.osid.type.Type gradeSystemTransformGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.grading.transform.gradesystemtransform.FederatingGradeSystemTransformList ret = getGradeSystemTransformList();

        for (org.osid.grading.transform.GradeSystemTransformLookupSession session : getSessions()) {
            ret.addGradeSystemTransformList(session.getGradeSystemTransformsByGenusType(gradeSystemTransformGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>GradeSystemTransformList</code> corresponding to
     *  the given grade system transform genus <code>Type</code> and
     *  include any additional grade system transforms with genus
     *  types derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known grade
     *  system transforms or an error results. Otherwise, the returned
     *  list may contain only those grade system transforms that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In active mode, grade system transforms are returned that are
     *  currently active. In any status mode, active and inactive
     *  grade system transforms are returned.
     *
     *  @param  gradeSystemTransformGenusType a gradeSystemTransform genus type 
     *  @return the returned <code>GradeSystemTransform</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>gradeSystemTransformGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.transform.GradeSystemTransformList getGradeSystemTransformsByParentGenusType(org.osid.type.Type gradeSystemTransformGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.grading.transform.gradesystemtransform.FederatingGradeSystemTransformList ret = getGradeSystemTransformList();

        for (org.osid.grading.transform.GradeSystemTransformLookupSession session : getSessions()) {
            ret.addGradeSystemTransformList(session.getGradeSystemTransformsByParentGenusType(gradeSystemTransformGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>GradeSystemTransformList</code> containing the given
     *  grade system transform record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  grade system transforms or an error results. Otherwise, the returned list
     *  may contain only those grade system transforms that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, grade system transforms are returned that are currently
     *  active. In any status mode, active and inactive grade system transforms
     *  are returned.
     *
     *  @param  gradeSystemTransformRecordType a gradeSystemTransform record type 
     *  @return the returned <code>GradeSystemTransform</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>gradeSystemTransformRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.transform.GradeSystemTransformList getGradeSystemTransformsByRecordType(org.osid.type.Type gradeSystemTransformRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.grading.transform.gradesystemtransform.FederatingGradeSystemTransformList ret = getGradeSystemTransformList();

        for (org.osid.grading.transform.GradeSystemTransformLookupSession session : getSessions()) {
            ret.addGradeSystemTransformList(session.getGradeSystemTransformsByRecordType(gradeSystemTransformRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets the grade system transforms from the source grade
     *  system. In plenary mode, the returned list contains all known
     *  grade system transforms or an error results. Otherwise, the
     *  returned list may contain only those grade system transforms
     *  that are accessible through this session.
     *
     *  @param  sourceGradeSystemId the source grade system
     *  @return the returned <code>GradeSystemTransform</code> list
     *  @throws org.osid.NotFoundException
     *          <code>sourceGradeSystemId</code> not found
     *  @throws org.osid.NullArgumentException
     *          <code>sourceGradeSystemId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.grading.transform.GradeSystemTransformList getGradeSystemTransformsBySource(org.osid.id.Id sourceGradeSystemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.grading.transform.gradesystemtransform.FederatingGradeSystemTransformList ret = getGradeSystemTransformList();

        for (org.osid.grading.transform.GradeSystemTransformLookupSession session : getSessions()) {
            ret.addGradeSystemTransformList(session.getGradeSystemTransformsBySource(sourceGradeSystemId));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a grade system transform by its source and target grade
     *  systems.  In plenary mode, the returned list contains all
     *  known grade system transforms or an error results. Otherwise,
     *  the returned list may contain only those grade system
     *  transforms that are accessible through this session.
     *
     *  @param  sourceGradeSystemId the source grade system
     *  @param  targetGradeSystemId the target grade system
     *  @return the returned <code>GradeSystemTransform</code>
     *  @throws org.osid.NotFoundException transform not found
     *  @throws org.osid.NullArgumentException
     *          <code>sourceGradeSystemId</code> or
     *          <code>targetGradeSystemId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.grading.transform.GradeSystemTransform getGradeSystemTransformBySystems(org.osid.id.Id sourceGradeSystemId,
                                                                                            org.osid.id.Id targetGradeSystemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.grading.transform.GradeSystemTransformLookupSession session : getSessions()) {
            try {
                return (session.getGradeSystemTransformBySystems(sourceGradeSystemId, targetGradeSystemId));
            } catch (org.osid.NotFoundException nfe) {}
        }

        throw new org.osid.NotFoundException ("no transform for " + sourceGradeSystemId + " and " + 
                                              targetGradeSystemId);
    }

        
    /**
     *  Gets all <code>GradeSystemTransforms</code>. 
     *
     *  In plenary mode, the returned list contains all known grade
     *  system transforms or an error results. Otherwise, the returned
     *  list may contain only those grade system transforms that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In active mode, grade system transforms are returned that are
     *  currently active. In any status mode, active and inactive
     *  grade system transforms are returned.
     *
     *  @return a list of <code>GradeSystemTransforms</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.transform.GradeSystemTransformList getGradeSystemTransforms()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.grading.transform.gradesystemtransform.FederatingGradeSystemTransformList ret = getGradeSystemTransformList();

        for (org.osid.grading.transform.GradeSystemTransformLookupSession session : getSessions()) {
            ret.addGradeSystemTransformList(session.getGradeSystemTransforms());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.grading.transform.gradesystemtransform.FederatingGradeSystemTransformList getGradeSystemTransformList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.grading.transform.gradesystemtransform.ParallelGradeSystemTransformList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.grading.transform.gradesystemtransform.CompositeGradeSystemTransformList());
        }
    }
}
