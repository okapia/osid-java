//
// AbstractIndexedMapStepConstrainerEnablerLookupSession.java
//
//    A simple framework for providing a StepConstrainerEnabler lookup service
//    backed by a fixed collection of step constrainer enablers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.workflow.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a StepConstrainerEnabler lookup service backed by a
 *  fixed collection of step constrainer enablers. The step constrainer enablers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some step constrainer enablers may be compatible
 *  with more types than are indicated through these step constrainer enabler
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>StepConstrainerEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapStepConstrainerEnablerLookupSession
    extends AbstractMapStepConstrainerEnablerLookupSession
    implements org.osid.workflow.rules.StepConstrainerEnablerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.workflow.rules.StepConstrainerEnabler> stepConstrainerEnablersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.workflow.rules.StepConstrainerEnabler>());
    private final MultiMap<org.osid.type.Type, org.osid.workflow.rules.StepConstrainerEnabler> stepConstrainerEnablersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.workflow.rules.StepConstrainerEnabler>());


    /**
     *  Makes a <code>StepConstrainerEnabler</code> available in this session.
     *
     *  @param  stepConstrainerEnabler a step constrainer enabler
     *  @throws org.osid.NullArgumentException <code>stepConstrainerEnabler<code> is
     *          <code>null</code>
     */

    @Override
    protected void putStepConstrainerEnabler(org.osid.workflow.rules.StepConstrainerEnabler stepConstrainerEnabler) {
        super.putStepConstrainerEnabler(stepConstrainerEnabler);

        this.stepConstrainerEnablersByGenus.put(stepConstrainerEnabler.getGenusType(), stepConstrainerEnabler);
        
        try (org.osid.type.TypeList types = stepConstrainerEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.stepConstrainerEnablersByRecord.put(types.getNextType(), stepConstrainerEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a step constrainer enabler from this session.
     *
     *  @param stepConstrainerEnablerId the <code>Id</code> of the step constrainer enabler
     *  @throws org.osid.NullArgumentException <code>stepConstrainerEnablerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeStepConstrainerEnabler(org.osid.id.Id stepConstrainerEnablerId) {
        org.osid.workflow.rules.StepConstrainerEnabler stepConstrainerEnabler;
        try {
            stepConstrainerEnabler = getStepConstrainerEnabler(stepConstrainerEnablerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.stepConstrainerEnablersByGenus.remove(stepConstrainerEnabler.getGenusType());

        try (org.osid.type.TypeList types = stepConstrainerEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.stepConstrainerEnablersByRecord.remove(types.getNextType(), stepConstrainerEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeStepConstrainerEnabler(stepConstrainerEnablerId);
        return;
    }


    /**
     *  Gets a <code>StepConstrainerEnablerList</code> corresponding to the given
     *  step constrainer enabler genus <code>Type</code> which does not include
     *  step constrainer enablers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known step constrainer enablers or an error results. Otherwise,
     *  the returned list may contain only those step constrainer enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  stepConstrainerEnablerGenusType a step constrainer enabler genus type 
     *  @return the returned <code>StepConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stepConstrainerEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerList getStepConstrainerEnablersByGenusType(org.osid.type.Type stepConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.workflow.rules.stepconstrainerenabler.ArrayStepConstrainerEnablerList(this.stepConstrainerEnablersByGenus.get(stepConstrainerEnablerGenusType)));
    }


    /**
     *  Gets a <code>StepConstrainerEnablerList</code> containing the given
     *  step constrainer enabler record <code>Type</code>. In plenary mode, the
     *  returned list contains all known step constrainer enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  step constrainer enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  stepConstrainerEnablerRecordType a step constrainer enabler record type 
     *  @return the returned <code>stepConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stepConstrainerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerList getStepConstrainerEnablersByRecordType(org.osid.type.Type stepConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.workflow.rules.stepconstrainerenabler.ArrayStepConstrainerEnablerList(this.stepConstrainerEnablersByRecord.get(stepConstrainerEnablerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.stepConstrainerEnablersByGenus.clear();
        this.stepConstrainerEnablersByRecord.clear();

        super.close();

        return;
    }
}
