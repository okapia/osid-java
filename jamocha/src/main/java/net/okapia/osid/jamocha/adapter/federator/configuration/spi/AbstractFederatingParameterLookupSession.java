//
// AbstractFederatingParameterLookupSession.java
//
//     An abstract federating adapter for a ParameterLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.configuration.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  ParameterLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingParameterLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.configuration.ParameterLookupSession>
    implements org.osid.configuration.ParameterLookupSession {

    private boolean parallel = false;
    private org.osid.configuration.Configuration configuration = new net.okapia.osid.jamocha.nil.configuration.configuration.UnknownConfiguration();


    /**
     *  Constructs a new <code>AbstractFederatingParameterLookupSession</code>.
     */

    protected AbstractFederatingParameterLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.configuration.ParameterLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Configuration/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Configuration Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getConfigurationId() {
        return (this.configuration.getId());
    }


    /**
     *  Gets the <code>Configuration</code> associated with this 
     *  session.
     *
     *  @return the <code>Configuration</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.Configuration getConfiguration()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.configuration);
    }


    /**
     *  Sets the <code>Configuration</code>.
     *
     *  @param  configuration the configuration for this session
     *  @throws org.osid.NullArgumentException <code>configuration</code>
     *          is <code>null</code>
     */

    protected void setConfiguration(org.osid.configuration.Configuration configuration) {
        nullarg(configuration, "configuration");
        this.configuration = configuration;
        return;
    }


    /**
     *  Tests if this user can perform <code>Parameter</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupParameters() {
        for (org.osid.configuration.ParameterLookupSession session : getSessions()) {
            if (session.canLookupParameters()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Parameter</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeParameterView() {
        for (org.osid.configuration.ParameterLookupSession session : getSessions()) {
            session.useComparativeParameterView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Parameter</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryParameterView() {
        for (org.osid.configuration.ParameterLookupSession session : getSessions()) {
            session.usePlenaryParameterView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include parameters in configurations which are children
     *  of this configuration in the configuration hierarchy.
     */

    @OSID @Override
    public void useFederatedConfigurationView() {
        for (org.osid.configuration.ParameterLookupSession session : getSessions()) {
            session.useFederatedConfigurationView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this configuration only.
     */

    @OSID @Override
    public void useIsolatedConfigurationView() {
        for (org.osid.configuration.ParameterLookupSession session : getSessions()) {
            session.useIsolatedConfigurationView();
        }

        return;
    }


    /**
     *  Only active parameters are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveParameterView() {
        for (org.osid.configuration.ParameterLookupSession session : getSessions()) {
            session.useActiveParameterView();
        }

        return;
    }


    /**
     *  Active and inactive parameters are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusParameterView() {
        for (org.osid.configuration.ParameterLookupSession session : getSessions()) {
            session.useAnyStatusParameterView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>Parameter</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Parameter</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Parameter</code> and
     *  retained for compatibility.
     *
     *  In active mode, parameters are returned that are currently
     *  active. In any status mode, active and inactive parameters
     *  are returned.
     *
     *  @param  parameterId <code>Id</code> of the
     *          <code>Parameter</code>
     *  @return the parameter
     *  @throws org.osid.NotFoundException <code>parameterId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>parameterId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.Parameter getParameter(org.osid.id.Id parameterId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.configuration.ParameterLookupSession session : getSessions()) {
            try {
                return (session.getParameter(parameterId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(parameterId + " not found");
    }


    /**
     *  Gets a <code>ParameterList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  parameters specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Parameters</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, parameters are returned that are currently
     *  active. In any status mode, active and inactive parameters
     *  are returned.
     *
     *  @param  parameterIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Parameter</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>parameterIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ParameterList getParametersByIds(org.osid.id.IdList parameterIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.configuration.parameter.MutableParameterList ret = new net.okapia.osid.jamocha.configuration.parameter.MutableParameterList();

        try (org.osid.id.IdList ids = parameterIds) {
            while (ids.hasNext()) {
                ret.addParameter(getParameter(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>ParameterList</code> corresponding to the given
     *  parameter genus <code>Type</code> which does not include
     *  parameters of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  parameters or an error results. Otherwise, the returned list
     *  may contain only those parameters that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, parameters are returned that are currently
     *  active. In any status mode, active and inactive parameters
     *  are returned.
     *
     *  @param  parameterGenusType a parameter genus type 
     *  @return the returned <code>Parameter</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>parameterGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ParameterList getParametersByGenusType(org.osid.type.Type parameterGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.configuration.parameter.FederatingParameterList ret = getParameterList();

        for (org.osid.configuration.ParameterLookupSession session : getSessions()) {
            ret.addParameterList(session.getParametersByGenusType(parameterGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ParameterList</code> corresponding to the given
     *  parameter genus <code>Type</code> and include any additional
     *  parameters with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  parameters or an error results. Otherwise, the returned list
     *  may contain only those parameters that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, parameters are returned that are currently
     *  active. In any status mode, active and inactive parameters
     *  are returned.
     *
     *  @param  parameterGenusType a parameter genus type 
     *  @return the returned <code>Parameter</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>parameterGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ParameterList getParametersByParentGenusType(org.osid.type.Type parameterGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.configuration.parameter.FederatingParameterList ret = getParameterList();

        for (org.osid.configuration.ParameterLookupSession session : getSessions()) {
            ret.addParameterList(session.getParametersByParentGenusType(parameterGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ParameterList</code> containing the given
     *  parameter record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  parameters or an error results. Otherwise, the returned list
     *  may contain only those parameters that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, parameters are returned that are currently
     *  active. In any status mode, active and inactive parameters
     *  are returned.
     *
     *  @param  parameterRecordType a parameter record type 
     *  @return the returned <code>Parameter</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>parameterRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ParameterList getParametersByRecordType(org.osid.type.Type parameterRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.configuration.parameter.FederatingParameterList ret = getParameterList();

        for (org.osid.configuration.ParameterLookupSession session : getSessions()) {
            ret.addParameterList(session.getParametersByRecordType(parameterRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Parameters</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  parameters or an error results. Otherwise, the returned list
     *  may contain only those parameters that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, parameters are returned that are currently
     *  active. In any status mode, active and inactive parameters
     *  are returned.
     *
     *  @return a list of <code>Parameters</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ParameterList getParameters()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.configuration.parameter.FederatingParameterList ret = getParameterList();

        for (org.osid.configuration.ParameterLookupSession session : getSessions()) {
            ret.addParameterList(session.getParameters());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.configuration.parameter.FederatingParameterList getParameterList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.configuration.parameter.ParallelParameterList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.configuration.parameter.CompositeParameterList());
        }
    }
}
