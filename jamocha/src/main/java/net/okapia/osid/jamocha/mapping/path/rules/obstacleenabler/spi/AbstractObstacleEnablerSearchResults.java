//
// AbstractObstacleEnablerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.path.rules.obstacleenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractObstacleEnablerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.mapping.path.rules.ObstacleEnablerSearchResults {

    private org.osid.mapping.path.rules.ObstacleEnablerList obstacleEnablers;
    private final org.osid.mapping.path.rules.ObstacleEnablerQueryInspector inspector;
    private final java.util.Collection<org.osid.mapping.path.rules.records.ObstacleEnablerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractObstacleEnablerSearchResults.
     *
     *  @param obstacleEnablers the result set
     *  @param obstacleEnablerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>obstacleEnablers</code>
     *          or <code>obstacleEnablerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractObstacleEnablerSearchResults(org.osid.mapping.path.rules.ObstacleEnablerList obstacleEnablers,
                                            org.osid.mapping.path.rules.ObstacleEnablerQueryInspector obstacleEnablerQueryInspector) {
        nullarg(obstacleEnablers, "obstacle enablers");
        nullarg(obstacleEnablerQueryInspector, "obstacle enabler query inspectpr");

        this.obstacleEnablers = obstacleEnablers;
        this.inspector = obstacleEnablerQueryInspector;

        return;
    }


    /**
     *  Gets the obstacle enabler list resulting from a search.
     *
     *  @return an obstacle enabler list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerList getObstacleEnablers() {
        if (this.obstacleEnablers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.mapping.path.rules.ObstacleEnablerList obstacleEnablers = this.obstacleEnablers;
        this.obstacleEnablers = null;
	return (obstacleEnablers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.mapping.path.rules.ObstacleEnablerQueryInspector getObstacleEnablerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  obstacle enabler search record <code> Type. </code> This method must
     *  be used to retrieve an obstacleEnabler implementing the requested
     *  record.
     *
     *  @param obstacleEnablerSearchRecordType an obstacleEnabler search 
     *         record type 
     *  @return the obstacle enabler search
     *  @throws org.osid.NullArgumentException
     *          <code>obstacleEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(obstacleEnablerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.path.rules.records.ObstacleEnablerSearchResultsRecord getObstacleEnablerSearchResultsRecord(org.osid.type.Type obstacleEnablerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.mapping.path.rules.records.ObstacleEnablerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(obstacleEnablerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(obstacleEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record obstacle enabler search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addObstacleEnablerRecord(org.osid.mapping.path.rules.records.ObstacleEnablerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "obstacle enabler record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
