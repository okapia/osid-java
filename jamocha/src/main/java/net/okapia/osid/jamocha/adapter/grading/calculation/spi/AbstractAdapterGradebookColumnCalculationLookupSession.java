//
// AbstractAdapterGradebookColumnCalculationLookupSession.java
//
//    A GradebookColumnCalculation lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.grading.calculation.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A GradebookColumnCalculation lookup session adapter.
 */

public abstract class AbstractAdapterGradebookColumnCalculationLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.grading.calculation.GradebookColumnCalculationLookupSession {

    private final org.osid.grading.calculation.GradebookColumnCalculationLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterGradebookColumnCalculationLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterGradebookColumnCalculationLookupSession(org.osid.grading.calculation.GradebookColumnCalculationLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Gradebook/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Gradebook Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getGradebookId() {
        return (this.session.getGradebookId());
    }


    /**
     *  Gets the {@code Gradebook} associated with this session.
     *
     *  @return the {@code Gradebook} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.Gradebook getGradebook()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getGradebook());
    }


    /**
     *  Tests if this user can perform {@code GradebookColumnCalculation} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupGradebookColumnCalculations() {
        return (this.session.canLookupGradebookColumnCalculations());
    }


    /**
     *  A complete view of the {@code GradebookColumnCalculation} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeGradebookColumnCalculationView() {
        this.session.useComparativeGradebookColumnCalculationView();
        return;
    }


    /**
     *  A complete view of the {@code GradebookColumnCalculation} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryGradebookColumnCalculationView() {
        this.session.usePlenaryGradebookColumnCalculationView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include gradebook column calculations in gradebooks which are children
     *  of this gradebook in the gradebook hierarchy.
     */

    @OSID @Override
    public void useFederatedGradebookView() {
        this.session.useFederatedGradebookView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this gradebook only.
     */

    @OSID @Override
    public void useIsolatedGradebookView() {
        this.session.useIsolatedGradebookView();
        return;
    }
    

    /**
     *  Only active gradebook column calculations are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveGradebookColumnCalculationView() {
        this.session.useActiveGradebookColumnCalculationView();
        return;
    }


    /**
     *  Active and inactive gradebook column calculations are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusGradebookColumnCalculationView() {
        this.session.useAnyStatusGradebookColumnCalculationView();
        return;
    }
    
     
    /**
     *  Gets the {@code GradebookColumnCalculation} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code GradebookColumnCalculation} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code GradebookColumnCalculation} and
     *  retained for compatibility.
     *
     *  In active mode, gradebook column calculations are returned that are currently
     *  active. In any status mode, active and inactive gradebook column calculations
     *  are returned.
     *
     *  @param gradebookColumnCalculationId {@code Id} of the {@code GradebookColumnCalculation}
     *  @return the gradebook column calculation
     *  @throws org.osid.NotFoundException {@code gradebookColumnCalculationId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code gradebookColumnCalculationId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.calculation.GradebookColumnCalculation getGradebookColumnCalculation(org.osid.id.Id gradebookColumnCalculationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGradebookColumnCalculation(gradebookColumnCalculationId));
    }


    /**
     *  Gets a {@code GradebookColumnCalculationList} corresponding to
     *  the given {@code IdList}.
     *
     *  In plenary mode, the returned list contains all of the
     *  gradebookColumnCalculations specified in the {@code Id} list,
     *  in the order of the list, including duplicates, or an error
     *  results if an {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code
     *  GradebookColumnCalculations} may be omitted from the list and
     *  may present the elements in any order including returning a
     *  unique set.
     *
     *  In active mode, gradebook column calculations are returned
     *  that are currently active. In any status mode, active and
     *  inactive gradebook column calculations are returned.
     *
     *  @param  gradebookColumnCalculationIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code GradebookColumnCalculation} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code gradebookColumnCalculationIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.calculation.GradebookColumnCalculationList getGradebookColumnCalculationsByIds(org.osid.id.IdList gradebookColumnCalculationIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGradebookColumnCalculationsByIds(gradebookColumnCalculationIds));
    }


    /**
     *  Gets a {@code GradebookColumnCalculationList} corresponding to
     *  the given gradebook column calculation genus {@code Type}
     *  which does not include gradebook column calculations of types
     *  derived from the specified {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  gradebook column calculations or an error results. Otherwise,
     *  the returned list may contain only those gradebook column
     *  calculations that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  In active mode, gradebook column calculations are returned
     *  that are currently active. In any status mode, active and
     *  inactive gradebook column calculations are returned.
     *
     *  @param  gradebookColumnCalculationGenusType a gradebookColumnCalculation genus type 
     *  @return the returned {@code GradebookColumnCalculation} list
     *  @throws org.osid.NullArgumentException
     *          {@code gradebookColumnCalculationGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.calculation.GradebookColumnCalculationList getGradebookColumnCalculationsByGenusType(org.osid.type.Type gradebookColumnCalculationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGradebookColumnCalculationsByGenusType(gradebookColumnCalculationGenusType));
    }


    /**
     *  Gets a {@code GradebookColumnCalculationList} corresponding to
     *  the given gradebook column calculation genus {@code Type} and
     *  include any additional gradebook column calculations with
     *  genus types derived from the specified {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  gradebook column calculations or an error results. Otherwise,
     *  the returned list may contain only those gradebook column
     *  calculations that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  In active mode, gradebook column calculations are returned
     *  that are currently active. In any status mode, active and
     *  inactive gradebook column calculations are returned.
     *
     *  @param  gradebookColumnCalculationGenusType a gradebookColumnCalculation genus type 
     *  @return the returned {@code GradebookColumnCalculation} list
     *  @throws org.osid.NullArgumentException
     *          {@code gradebookColumnCalculationGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.calculation.GradebookColumnCalculationList getGradebookColumnCalculationsByParentGenusType(org.osid.type.Type gradebookColumnCalculationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGradebookColumnCalculationsByParentGenusType(gradebookColumnCalculationGenusType));
    }


    /**
     *  Gets a {@code GradebookColumnCalculationList} containing the
     *  given gradebook column calculation record {@code Type}.
     * 
     *  In plenary mode, the returned list contains all known
     *  gradebook column calculations or an error results. Otherwise,
     *  the returned list may contain only those gradebook column
     *  calculations that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  In active mode, gradebook column calculations are returned
     *  that are currently active. In any status mode, active and
     *  inactive gradebook column calculations are returned.
     *
     *  @param  gradebookColumnCalculationRecordType a gradebookColumnCalculation record type 
     *  @return the returned {@code GradebookColumnCalculation} list
     *  @throws org.osid.NullArgumentException
     *          {@code gradebookColumnCalculationRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.calculation.GradebookColumnCalculationList getGradebookColumnCalculationsByRecordType(org.osid.type.Type gradebookColumnCalculationRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGradebookColumnCalculationsByRecordType(gradebookColumnCalculationRecordType));
    }


    /**
     *  Gets the {@code GradebookColumnCalculation} mapped to a {@code
     *  GradebookColumn} to which this calculation is applied. In
     *  plenary mode, the exact {@code Id} is found or a {@code
     *  NOT_FOUND} results. Otherwise, the returned {@code
     *  GradebookColumnCalculation} may have a different {@code Id}
     *  than requested, such as the case where a duplicate {@code Id}
     *  was assigned to a {@code GradebookColumnCalculation} and
     *  retained for compatibility.
     *
     *  @param  gradebookColumnId {@code Id} of a {@code 
     *          GradebookColumn} 
     *  @return the gradebook column calculation 
     *  @throws org.osid.NotFoundException {@code gradebookColumnId} 
     *          not found 
     *  @throws org.osid.NullArgumentException {@code
     *         gradebookColumnId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.grading.calculation.GradebookColumnCalculation getGradebookColumnCalculationForGradebookColumn(org.osid.id.Id gradebookColumnId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getGradebookColumnCalculationForGradebookColumn(gradebookColumnId));
    }

    
    /**
     *  Gets a {@code GradebookColumnCalculationList} corresponding to
     *  the given gradebook column {@code Ids} to which this
     *  calculation is applied. In plenary mode, the returned list
     *  contains all of the gradebook column calculations specified in
     *  the {@code Id} list, in the order of the list, including
     *  duplicates, or an error results if a {@code Id} in the
     *  supplied list is not found or inaccessible. Otherwise,
     *  inaccessible gradeboook column calculations may be omitted
     *  from the list.
     *
     *  @param gradebookColumnIds a list of gradebook column {@code
     *         Ids}
     *  @return the returned {@code GradebookColumnCalculation} list 
     *  @throws org.osid.NotFoundException an {@code Id} was not found 
     *  @throws org.osid.NullArgumentException {@code
     *         gradeBookColumnIds} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @compliance mandatory This method must be implemented. 
     */

    @OSID @Override
    public org.osid.grading.calculation.GradebookColumnCalculationList getGradebookColumnCalculationsForGradebookColumns(org.osid.id.IdList gradebookColumnIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGradebookColumnCalculationsForGradebookColumns(gradebookColumnIds));
    }


    /**
     *  Gets all {@code GradebookColumnCalculations}. 
     *
     *  In plenary mode, the returned list contains all known
     *  gradebook column calculations or an error results. Otherwise,
     *  the returned list may contain only those gradebook column
     *  calculations that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  In active mode, gradebook column calculations are returned
     *  that are currently active. In any status mode, active and
     *  inactive gradebook column calculations are returned.
     *
     *  @return a list of {@code GradebookColumnCalculations} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.calculation.GradebookColumnCalculationList getGradebookColumnCalculations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGradebookColumnCalculations());
    }
}
