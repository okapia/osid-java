//
// WorkflowEventMiter.java
//
//     Defines a WorkflowEvent miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.workflow.workflowevent;


/**
 *  Defines a <code>WorkflowEvent</code> miter for use with the builders.
 */

public interface WorkflowEventMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidObjectMiter,
            org.osid.workflow.WorkflowEvent {


    /**
     *  Sets the timestamp.
     *
     *  @param timestamp a timestamp
     *  @throws org.osid.NullArgumentException <code>timestamp</code>
     *          is <code>null</code>
     */

    public void setTimestamp(org.osid.calendaring.DateTime timestamp);


    /**
     *  Sets the process.
     *
     *  @param process a process
     *  @throws org.osid.NullArgumentException <code>process</code> is
     *          <code>null</code>
     */

    public void setProcess(org.osid.workflow.Process process);


    /**
     *  Sets the worker.
     *
     *  @param worker a worker
     *  @throws org.osid.NullArgumentException <code>worker</code> is
     *          <code>null</code>
     */

    public void setWorker(org.osid.resource.Resource worker);


    /**
     *  Sets the working agent.
     *
     *  @param agent a working agent
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    public void setWorkingAgent(org.osid.authentication.Agent agent);


    /**
     *  Sets the work.
     *
     *  @param work a work
     *  @throws org.osid.NullArgumentException <code>work</code> is
     *          <code>null</code>
     */

    public void setWork(org.osid.workflow.Work work);


    /**
     *  Sets the step.
     *
     *  @param step a step
     *  @throws org.osid.NullArgumentException <code>step</code> is
     *          <code>null</code>
     */

    public void setStep(org.osid.workflow.Step step);


    /**
     *  Adds a WorkflowEvent record.
     *
     *  @param record a workflowEvent record
     *  @param recordType the type of workflowEvent record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addWorkflowEventRecord(org.osid.workflow.records.WorkflowEventRecord record, org.osid.type.Type recordType);
}       


