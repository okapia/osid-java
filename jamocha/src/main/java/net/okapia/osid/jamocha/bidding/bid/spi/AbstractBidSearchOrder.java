//
// AbstractBidSearchOdrer.java
//
//     Defines a BidSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.bidding.bid.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code BidSearchOrder}.
 */

public abstract class AbstractBidSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipSearchOrder
    implements org.osid.bidding.BidSearchOrder {

    private final java.util.Collection<org.osid.bidding.records.BidSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Orders the results by auction. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAuction(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an auction search order is available. 
     *
     *  @return <code> true </code> if an auction search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionSearchOrder() {
        return (false);
    }


    /**
     *  Gets the auction search order. 
     *
     *  @return the auction search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsAuctionSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionSearchOrder getAuctionSearchOrder() {
        throw new org.osid.UnimplementedException("supportsAuctionSearchOrder() is false");
    }


    /**
     *  Orders the results by bidder. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByBidder(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a bidder search order is available. 
     *
     *  @return <code> true </code> if a bidder search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBidderSearchOrder() {
        return (false);
    }


    /**
     *  Gets the bidder search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsBidderSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getBidderSearchOrder() {
        throw new org.osid.UnimplementedException("supportsBidderSearchOrder() is false");
    }


    /**
     *  Orders the results by bidding agent. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByBiddingAgent(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a bidding agent search order is available. 
     *
     *  @return <code> true </code> if a bidding agent search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBiddingAgentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the bidding agent search order. 
     *
     *  @return the agent search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsBiddingAgentSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getBiddingAgentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsBiddingAgentSearchOrder() is false");
    }


    /**
     *  Orders the results by the quantity. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByQuantity(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by the current bid. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCurrentBid(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by the maximum bid. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByMaximumBid(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by winning bids. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByWinner(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by the settlement amount. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySettlementAmount(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  bidRecordType a bid record type 
     *  @return {@code true} if the bidRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code bidRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type bidRecordType) {
        for (org.osid.bidding.records.BidSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(bidRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  bidRecordType the bid record type 
     *  @return the bid search order record
     *  @throws org.osid.NullArgumentException
     *          {@code bidRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(bidRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.bidding.records.BidSearchOrderRecord getBidSearchOrderRecord(org.osid.type.Type bidRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.bidding.records.BidSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(bidRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(bidRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this bid. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param bidRecord the bid search odrer record
     *  @param bidRecordType bid record type
     *  @throws org.osid.NullArgumentException
     *          {@code bidRecord} or
     *          {@code bidRecordTypebid} is
     *          {@code null}
     */
            
    protected void addBidRecord(org.osid.bidding.records.BidSearchOrderRecord bidSearchOrderRecord, 
                                     org.osid.type.Type bidRecordType) {

        addRecordType(bidRecordType);
        this.records.add(bidSearchOrderRecord);
        
        return;
    }
}
