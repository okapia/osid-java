//
// AbstractAssemblyReplyQuery.java
//
//     A ReplyQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.forum.reply.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ReplyQuery that stores terms.
 */

public abstract class AbstractAssemblyReplyQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyContainableOsidObjectQuery
    implements org.osid.forum.ReplyQuery,
               org.osid.forum.ReplyQueryInspector,
               org.osid.forum.ReplySearchOrder {

    private final java.util.Collection<org.osid.forum.records.ReplyQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.forum.records.ReplyQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.forum.records.ReplySearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyReplyQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyReplyQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the post <code> Id </code> for this query to match replies to 
     *  posts. 
     *
     *  @param  postId a post <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> postId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchPostId(org.osid.id.Id postId, boolean match) {
        getAssembler().addIdTerm(getPostIdColumn(), postId, match);
        return;
    }


    /**
     *  Clears the post <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearPostIdTerms() {
        getAssembler().clearTerms(getPostIdColumn());
        return;
    }


    /**
     *  Gets the post <code> Id </code> terms. 
     *
     *  @return the post <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPostIdTerms() {
        return (getAssembler().getIdTerms(getPostIdColumn()));
    }


    /**
     *  Gets the PostId column name.
     *
     * @return the column name
     */

    protected String getPostIdColumn() {
        return ("post_id");
    }


    /**
     *  Tests if a <code> PostQuery </code> is available. 
     *
     *  @return <code> true </code> if a post query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostQuery() {
        return (false);
    }


    /**
     *  Gets the query for a post query. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the post query 
     *  @throws org.osid.UnimplementedException <code> supportsPostQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.PostQuery getPostQuery() {
        throw new org.osid.UnimplementedException("supportsPostQuery() is false");
    }


    /**
     *  Clears the post terms. 
     */

    @OSID @Override
    public void clearPostTerms() {
        getAssembler().clearTerms(getPostColumn());
        return;
    }


    /**
     *  Gets the post terms. 
     *
     *  @return the post terms 
     */

    @OSID @Override
    public org.osid.forum.ReplyQueryInspector[] getPostTerms() {
        return (new org.osid.forum.ReplyQueryInspector[0]);
    }


    /**
     *  Gets the Post column name.
     *
     * @return the column name
     */

    protected String getPostColumn() {
        return ("post");
    }


    /**
     *  Matches entries whose sent time is between the supplied range 
     *  inclusive. 
     *
     *  @param  startTime start time 
     *  @param  endTime end time 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> startTime </code> is 
     *          greater than <code> endTime </code> 
     *  @throws org.osid.NullArgumentException <code> startTime </code> or 
     *          <code> endTime </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchTimestamp(org.osid.calendaring.DateTime startTime, 
                               org.osid.calendaring.DateTime endTime, 
                               boolean match) {
        getAssembler().addDateTimeRangeTerm(getTimestampColumn(), startTime, endTime, match);
        return;
    }


    /**
     *  Clears the timestamp terms. 
     */

    @OSID @Override
    public void clearTimestampTerms() {
        getAssembler().clearTerms(getTimestampColumn());
        return;
    }


    /**
     *  Gets the timestamp terms. 
     *
     *  @return the timestamp terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeTerm[] getTimestampTerms() {
        return (getAssembler().getDateTimeTerms(getTimestampColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the timestamp. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTimestamp(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getTimestampColumn(), style);
        return;
    }


    /**
     *  Gets the Timestamp column name.
     *
     * @return the column name
     */

    protected String getTimestampColumn() {
        return ("timestamp");
    }


    /**
     *  Matches the poster of the entry. 
     *
     *  @param  resourceId resource <code> Id </code> to match 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPosterId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getPosterIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the poster resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearPosterIdTerms() {
        getAssembler().clearTerms(getPosterIdColumn());
        return;
    }


    /**
     *  Gets the poster <code> Id </code> terms. 
     *
     *  @return the resource <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPosterIdTerms() {
        return (getAssembler().getIdTerms(getPosterIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the poster. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPoster(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getPosterColumn(), style);
        return;
    }


    /**
     *  Gets the PosterId column name.
     *
     * @return the column name
     */

    protected String getPosterIdColumn() {
        return ("poster_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available for querying 
     *  posters. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPosterQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsPosterQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getPosterQuery() {
        throw new org.osid.UnimplementedException("supportsPosterQuery() is false");
    }


    /**
     *  Clears the poster terms. 
     */

    @OSID @Override
    public void clearPosterTerms() {
        getAssembler().clearTerms(getPosterColumn());
        return;
    }


    /**
     *  Gets the poster terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getPosterTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a poster resource search order interface is available. 
     *
     *  @return <code> true </code> if a resource search order interface is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPosterSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource search order interface. 
     *
     *  @return the resource search order interface 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPosterSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getPosterSearchOrder() {
        throw new org.osid.UnimplementedException("supportsPosterSearchOrder() is false");
    }


    /**
     *  Gets the Poster column name.
     *
     * @return the column name
     */

    protected String getPosterColumn() {
        return ("poster");
    }


    /**
     *  Matches the posting agent of the entry. 
     *
     *  @param  agentId agent <code> Id </code> to match 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPostingAgentId(org.osid.id.Id agentId, boolean match) {
        getAssembler().addIdTerm(getPostingAgentIdColumn(), agentId, match);
        return;
    }


    /**
     *  Clears the posting agent <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearPostingAgentIdTerms() {
        getAssembler().clearTerms(getPostingAgentIdColumn());
        return;
    }


    /**
     *  Gets the posting agent <code> Id </code> terms. 
     *
     *  @return the agent <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPostingAgentIdTerms() {
        return (getAssembler().getIdTerms(getPostingAgentIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the posting 
     *  agent. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPostingAgent(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getPostingAgentColumn(), style);
        return;
    }


    /**
     *  Gets the PostingAgentId column name.
     *
     * @return the column name
     */

    protected String getPostingAgentIdColumn() {
        return ("posting_agent_id");
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available for querying 
     *  posting agents. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostingAgentQuery() {
        return (false);
    }


    /**
     *  Gets the query for an agent. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostingAgentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getPostingAgentQuery() {
        throw new org.osid.UnimplementedException("supportsPostingAgentQuery() is false");
    }


    /**
     *  Clears the posting agent terms. 
     */

    @OSID @Override
    public void clearPostingAgentTerms() {
        getAssembler().clearTerms(getPostingAgentColumn());
        return;
    }


    /**
     *  Gets the posting agent terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authentication.AgentQueryInspector[] getPostingAgentTerms() {
        return (new org.osid.authentication.AgentQueryInspector[0]);
    }


    /**
     *  Tests if a posting agent search order interface is available. 
     *
     *  @return <code> true </code> if a agent search order interface is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostingAgentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the posting agent search order interface. 
     *
     *  @return the agent search order interface 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostingAgentSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getPostingAgentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsPostingAgentSearchOrder() is false");
    }


    /**
     *  Gets the PostingAgent column name.
     *
     * @return the column name
     */

    protected String getPostingAgentColumn() {
        return ("posting_agent");
    }


    /**
     *  Adds a subject line to match. Multiple subject line matches can be 
     *  added to perform a boolean <code> OR </code> among them. 
     *
     *  @param  subject display name to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> subject is </code> 
     *          not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> subject </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchSubjectLine(String subject, 
                                 org.osid.type.Type stringMatchType, 
                                 boolean match) {
        getAssembler().addStringTerm(getSubjectLineColumn(), subject, stringMatchType, match);
        return;
    }


    /**
     *  Matches entries with any subject line. 
     *
     *  @param  match <code> true </code> to match entries with any subject 
     *          line, <code> false </code> to match entries with no subject 
     *          line 
     */

    @OSID @Override
    public void matchAnySubjectLine(boolean match) {
        getAssembler().addStringWildcardTerm(getSubjectLineColumn(), match);
        return;
    }


    /**
     *  Clears the subject line terms. 
     */

    @OSID @Override
    public void clearSubjectLineTerms() {
        getAssembler().clearTerms(getSubjectLineColumn());
        return;
    }


    /**
     *  Gets the subject line terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getSubjectLineTerms() {
        return (getAssembler().getStringTerms(getSubjectLineColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the subject. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySubjectLine(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getSubjectLineColumn(), style);
        return;
    }


    /**
     *  Gets the SubjectLine column name.
     *
     * @return the column name
     */

    protected String getSubjectLineColumn() {
        return ("subject_line");
    }


    /**
     *  Adds text to match. Multiple text matches can be added to perform a 
     *  boolean <code> OR </code> among them. 
     *
     *  @param  text text to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> text is </code> not 
     *          of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> text </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchText(String text, org.osid.type.Type stringMatchType, 
                          boolean match) {
        getAssembler().addStringTerm(getTextColumn(), text, stringMatchType, match);
        return;
    }


    /**
     *  Matches entries with any text. 
     *
     *  @param  match <code> true </code> to match entries with any text, 
     *          <code> false </code> to match entries with no text 
     */

    @OSID @Override
    public void matchAnyText(boolean match) {
        getAssembler().addStringWildcardTerm(getTextColumn(), match);
        return;
    }


    /**
     *  Clears the text terms. 
     */

    @OSID @Override
    public void clearTextTerms() {
        getAssembler().clearTerms(getTextColumn());
        return;
    }


    /**
     *  Gets the text terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getTextTerms() {
        return (getAssembler().getStringTerms(getTextColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the text. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByText(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getTextColumn(), style);
        return;
    }


    /**
     *  Gets the Text column name.
     *
     * @return the column name
     */

    protected String getTextColumn() {
        return ("text");
    }


    /**
     *  Sets the reply <code> Id </code> for this query to match replies that 
     *  have the specified reply as an ancestor. 
     *
     *  @param  replyId a reply <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> replyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchContainingReplyId(org.osid.id.Id replyId, boolean match) {
        getAssembler().addIdTerm(getContainingReplyIdColumn(), replyId, match);
        return;
    }


    /**
     *  Clears the containing reply <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearContainingReplyIdTerms() {
        getAssembler().clearTerms(getContainingReplyIdColumn());
        return;
    }


    /**
     *  Gets the containing reply <code> Id </code> terms. 
     *
     *  @return the containing reply <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getContainingReplyIdTerms() {
        return (getAssembler().getIdTerms(getContainingReplyIdColumn()));
    }


    /**
     *  Gets the ContainingReplyId column name.
     *
     * @return the column name
     */

    protected String getContainingReplyIdColumn() {
        return ("containing_reply_id");
    }


    /**
     *  Tests if a containing reply query is available. 
     *
     *  @return <code> true </code> if a containing reply query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContainingReplyQuery() {
        return (false);
    }


    /**
     *  Gets the query for a containing reply. 
     *
     *  @return the containing reply query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContainingReplyQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ReplyQuery getContainingReplyQuery() {
        throw new org.osid.UnimplementedException("supportsContainingReplyQuery() is false");
    }


    /**
     *  Matches replies with any ancestor reply. 
     *
     *  @param  match <code> true </code> to match replies with any ancestor 
     *          reply, <code> false </code> to match replies with no ancestor 
     *          replies 
     */

    @OSID @Override
    public void matchAnyContainingReply(boolean match) {
        getAssembler().addIdWildcardTerm(getContainingReplyColumn(), match);
        return;
    }


    /**
     *  Clears the containing reply terms. 
     */

    @OSID @Override
    public void clearContainingReplyTerms() {
        getAssembler().clearTerms(getContainingReplyColumn());
        return;
    }


    /**
     *  Gets the containing reply terms. 
     *
     *  @return the containing reply terms 
     */

    @OSID @Override
    public org.osid.forum.ReplyQueryInspector[] getContainingReplyTerms() {
        return (new org.osid.forum.ReplyQueryInspector[0]);
    }


    /**
     *  Gets the ContainingReply column name.
     *
     * @return the column name
     */

    protected String getContainingReplyColumn() {
        return ("containing_reply");
    }


    /**
     *  Sets the reply <code> Id </code> for this query to match replies that 
     *  have the specified reply as a descendant. 
     *
     *  @param  replyId a reply <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> replyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchContainedReplyId(org.osid.id.Id replyId, boolean match) {
        getAssembler().addIdTerm(getContainedReplyIdColumn(), replyId, match);
        return;
    }


    /**
     *  Clears the contained reply <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearContainedReplyIdTerms() {
        getAssembler().clearTerms(getContainedReplyIdColumn());
        return;
    }


    /**
     *  Gets the contained reply <code> Id </code> terms. 
     *
     *  @return the contained reply <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getContainedReplyIdTerms() {
        return (getAssembler().getIdTerms(getContainedReplyIdColumn()));
    }


    /**
     *  Gets the ContainedReplyId column name.
     *
     * @return the column name
     */

    protected String getContainedReplyIdColumn() {
        return ("contained_reply_id");
    }


    /**
     *  Tests if a contained reply query is available. 
     *
     *  @return <code> true </code> if a contained reply query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContainedReplyQuery() {
        return (false);
    }


    /**
     *  Gets the query for a contained reply. 
     *
     *  @return the contained reply query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContainedReplyQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ReplyQuery getContainedReplyQuery() {
        throw new org.osid.UnimplementedException("supportsContainedReplyQuery() is false");
    }


    /**
     *  Matches replies with any descednant reply. 
     *
     *  @param  match <code> true </code> to match replies with any descendant 
     *          reply, <code> false </code> to match replies with no 
     *          descendant replies 
     */

    @OSID @Override
    public void matchAnyContainedReply(boolean match) {
        getAssembler().addIdWildcardTerm(getContainedReplyColumn(), match);
        return;
    }


    /**
     *  Clears the contained reply terms. 
     */

    @OSID @Override
    public void clearContainedReplyTerms() {
        getAssembler().clearTerms(getContainedReplyColumn());
        return;
    }


    /**
     *  Gets the contained reply terms. 
     *
     *  @return the contained reply terms 
     */

    @OSID @Override
    public org.osid.forum.ReplyQueryInspector[] getContainedReplyTerms() {
        return (new org.osid.forum.ReplyQueryInspector[0]);
    }


    /**
     *  Gets the ContainedReply column name.
     *
     * @return the column name
     */

    protected String getContainedReplyColumn() {
        return ("contained_reply");
    }


    /**
     *  Sets the post <code> Id </code> for this query to match replies 
     *  assigned to forums. 
     *
     *  @param  forumId a forum <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> forumId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchForumId(org.osid.id.Id forumId, boolean match) {
        getAssembler().addIdTerm(getForumIdColumn(), forumId, match);
        return;
    }


    /**
     *  Clears the forum <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearForumIdTerms() {
        getAssembler().clearTerms(getForumIdColumn());
        return;
    }


    /**
     *  Gets the forum <code> Id </code> terms. 
     *
     *  @return the forum <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getForumIdTerms() {
        return (getAssembler().getIdTerms(getForumIdColumn()));
    }


    /**
     *  Gets the ForumId column name.
     *
     * @return the column name
     */

    protected String getForumIdColumn() {
        return ("forum_id");
    }


    /**
     *  Tests if a <code> ForumQuery </code> is available. 
     *
     *  @return <code> true </code> if a forum query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsForumQuery() {
        return (false);
    }


    /**
     *  Gets the query for a forum query. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the forum query 
     *  @throws org.osid.UnimplementedException <code> supportsForumQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ForumQuery getForumQuery() {
        throw new org.osid.UnimplementedException("supportsForumQuery() is false");
    }


    /**
     *  Clears the forum terms. 
     */

    @OSID @Override
    public void clearForumTerms() {
        getAssembler().clearTerms(getForumColumn());
        return;
    }


    /**
     *  Gets the forum terms. 
     *
     *  @return the forum terms 
     */

    @OSID @Override
    public org.osid.forum.ForumQueryInspector[] getForumTerms() {
        return (new org.osid.forum.ForumQueryInspector[0]);
    }


    /**
     *  Gets the Forum column name.
     *
     * @return the column name
     */

    protected String getForumColumn() {
        return ("forum");
    }


    /**
     *  Tests if this reply supports the given record
     *  <code>Type</code>.
     *
     *  @param  replyRecordType a reply record type 
     *  @return <code>true</code> if the replyRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>replyRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type replyRecordType) {
        for (org.osid.forum.records.ReplyQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(replyRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  replyRecordType the reply record type 
     *  @return the reply query record 
     *  @throws org.osid.NullArgumentException
     *          <code>replyRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(replyRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.forum.records.ReplyQueryRecord getReplyQueryRecord(org.osid.type.Type replyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.forum.records.ReplyQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(replyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(replyRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  replyRecordType the reply record type 
     *  @return the reply query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>replyRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(replyRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.forum.records.ReplyQueryInspectorRecord getReplyQueryInspectorRecord(org.osid.type.Type replyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.forum.records.ReplyQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(replyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(replyRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param replyRecordType the reply record type
     *  @return the reply search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>replyRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(replyRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.forum.records.ReplySearchOrderRecord getReplySearchOrderRecord(org.osid.type.Type replyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.forum.records.ReplySearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(replyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(replyRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this reply. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param replyQueryRecord the reply query record
     *  @param replyQueryInspectorRecord the reply query inspector
     *         record
     *  @param replySearchOrderRecord the reply search order record
     *  @param replyRecordType reply record type
     *  @throws org.osid.NullArgumentException
     *          <code>replyQueryRecord</code>,
     *          <code>replyQueryInspectorRecord</code>,
     *          <code>replySearchOrderRecord</code> or
     *          <code>replyRecordTypereply</code> is
     *          <code>null</code>
     */
            
    protected void addReplyRecords(org.osid.forum.records.ReplyQueryRecord replyQueryRecord, 
                                      org.osid.forum.records.ReplyQueryInspectorRecord replyQueryInspectorRecord, 
                                      org.osid.forum.records.ReplySearchOrderRecord replySearchOrderRecord, 
                                      org.osid.type.Type replyRecordType) {

        addRecordType(replyRecordType);

        nullarg(replyQueryRecord, "reply query record");
        nullarg(replyQueryInspectorRecord, "reply query inspector record");
        nullarg(replySearchOrderRecord, "reply search odrer record");

        this.queryRecords.add(replyQueryRecord);
        this.queryInspectorRecords.add(replyQueryInspectorRecord);
        this.searchOrderRecords.add(replySearchOrderRecord);
        
        return;
    }
}
