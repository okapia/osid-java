//
// AbstractAdapterBrokerLookupSession.java
//
//    A Broker lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.provisioning.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Broker lookup session adapter.
 */

public abstract class AbstractAdapterBrokerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.provisioning.BrokerLookupSession {

    private final org.osid.provisioning.BrokerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterBrokerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterBrokerLookupSession(org.osid.provisioning.BrokerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Distributor/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Distributor Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDistributorId() {
        return (this.session.getDistributorId());
    }


    /**
     *  Gets the {@code Distributor} associated with this session.
     *
     *  @return the {@code Distributor} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Distributor getDistributor()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getDistributor());
    }


    /**
     *  Tests if this user can perform {@code Broker} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupBrokers() {
        return (this.session.canLookupBrokers());
    }


    /**
     *  A complete view of the {@code Broker} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeBrokerView() {
        this.session.useComparativeBrokerView();
        return;
    }


    /**
     *  A complete view of the {@code Broker} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryBrokerView() {
        this.session.usePlenaryBrokerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include brokers in distributors which are children
     *  of this distributor in the distributor hierarchy.
     */

    @OSID @Override
    public void useFederatedDistributorView() {
        this.session.useFederatedDistributorView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this distributor only.
     */

    @OSID @Override
    public void useIsolatedDistributorView() {
        this.session.useIsolatedDistributorView();
        return;
    }
    

    /**
     *  Only active brokers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveBrokerView() {
        this.session.useActiveBrokerView();
        return;
    }


    /**
     *  Active and inactive brokers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusBrokerView() {
        this.session.useAnyStatusBrokerView();
        return;
    }
    
     
    /**
     *  Gets the {@code Broker} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Broker} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Broker} and
     *  retained for compatibility.
     *
     *  In active mode, brokers are returned that are currently
     *  active. In any status mode, active and inactive brokers
     *  are returned.
     *
     *  @param brokerId {@code Id} of the {@code Broker}
     *  @return the broker
     *  @throws org.osid.NotFoundException {@code brokerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code brokerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Broker getBroker(org.osid.id.Id brokerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBroker(brokerId));
    }


    /**
     *  Gets a {@code BrokerList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  brokers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Brokers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, brokers are returned that are currently
     *  active. In any status mode, active and inactive brokers
     *  are returned.
     *
     *  @param  brokerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Broker} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code brokerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerList getBrokersByIds(org.osid.id.IdList brokerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBrokersByIds(brokerIds));
    }


    /**
     *  Gets a {@code BrokerList} corresponding to the given
     *  broker genus {@code Type} which does not include
     *  brokers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  brokers or an error results. Otherwise, the returned list
     *  may contain only those brokers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, brokers are returned that are currently
     *  active. In any status mode, active and inactive brokers
     *  are returned.
     *
     *  @param  brokerGenusType a broker genus type 
     *  @return the returned {@code Broker} list
     *  @throws org.osid.NullArgumentException
     *          {@code brokerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerList getBrokersByGenusType(org.osid.type.Type brokerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBrokersByGenusType(brokerGenusType));
    }


    /**
     *  Gets a {@code BrokerList} corresponding to the given
     *  broker genus {@code Type} and include any additional
     *  brokers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  brokers or an error results. Otherwise, the returned list
     *  may contain only those brokers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, brokers are returned that are currently
     *  active. In any status mode, active and inactive brokers
     *  are returned.
     *
     *  @param  brokerGenusType a broker genus type 
     *  @return the returned {@code Broker} list
     *  @throws org.osid.NullArgumentException
     *          {@code brokerGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerList getBrokersByParentGenusType(org.osid.type.Type brokerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBrokersByParentGenusType(brokerGenusType));
    }


    /**
     *  Gets a {@code BrokerList} containing the given
     *  broker record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  brokers or an error results. Otherwise, the returned list
     *  may contain only those brokers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, brokers are returned that are currently
     *  active. In any status mode, active and inactive brokers
     *  are returned.
     *
     *  @param  brokerRecordType a broker record type 
     *  @return the returned {@code Broker} list
     *  @throws org.osid.NullArgumentException
     *          {@code brokerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerList getBrokersByRecordType(org.osid.type.Type brokerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBrokersByRecordType(brokerRecordType));
    }


    /**
     *  Gets a {@code BrokerList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  brokers or an error results. Otherwise, the returned list
     *  may contain only those brokers that are accessible through
     *  this session.
     *
     *  In active mode, brokers are returned that are currently
     *  active. In any status mode, active and inactive brokers
     *  are returned.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Broker} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerList getBrokersByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBrokersByProvider(resourceId));
    }


    /**
     *  Gets all {@code Brokers}. 
     *
     *  In plenary mode, the returned list contains all known
     *  brokers or an error results. Otherwise, the returned list
     *  may contain only those brokers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, brokers are returned that are currently
     *  active. In any status mode, active and inactive brokers
     *  are returned.
     *
     *  @return a list of {@code Brokers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerList getBrokers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBrokers());
    }
}
