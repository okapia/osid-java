//
// AbstractConfigurationSearch.java
//
//     A template for making a Configuration Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.configuration.configuration.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing configuration searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractConfigurationSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.configuration.ConfigurationSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.configuration.records.ConfigurationSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.configuration.ConfigurationSearchOrder configurationSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of configurations. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  configurationIds list of configurations
     *  @throws org.osid.NullArgumentException
     *          <code>configurationIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongConfigurations(org.osid.id.IdList configurationIds) {
        while (configurationIds.hasNext()) {
            try {
                this.ids.add(configurationIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongConfigurations</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of configuration Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getConfigurationIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  configurationSearchOrder configuration search order 
     *  @throws org.osid.NullArgumentException
     *          <code>configurationSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>configurationSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderConfigurationResults(org.osid.configuration.ConfigurationSearchOrder configurationSearchOrder) {
	this.configurationSearchOrder = configurationSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.configuration.ConfigurationSearchOrder getConfigurationSearchOrder() {
	return (this.configurationSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given configuration search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a configuration implementing the requested record.
     *
     *  @param configurationSearchRecordType a configuration search record
     *         type
     *  @return the configuration search record
     *  @throws org.osid.NullArgumentException
     *          <code>configurationSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(configurationSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.configuration.records.ConfigurationSearchRecord getConfigurationSearchRecord(org.osid.type.Type configurationSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.configuration.records.ConfigurationSearchRecord record : this.records) {
            if (record.implementsRecordType(configurationSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(configurationSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this configuration search. 
     *
     *  @param configurationSearchRecord configuration search record
     *  @param configurationSearchRecordType configuration search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addConfigurationSearchRecord(org.osid.configuration.records.ConfigurationSearchRecord configurationSearchRecord, 
                                           org.osid.type.Type configurationSearchRecordType) {

        addRecordType(configurationSearchRecordType);
        this.records.add(configurationSearchRecord);        
        return;
    }
}
