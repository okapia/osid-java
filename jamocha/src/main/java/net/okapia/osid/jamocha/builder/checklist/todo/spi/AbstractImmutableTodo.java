//
// AbstractImmutableTodo.java
//
//     Wraps a mutable Todo to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.checklist.todo.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Todo</code> to hide modifiers. This
 *  wrapper provides an immutized Todo from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying todo whose state changes are visible.
 */

public abstract class AbstractImmutableTodo
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableTemporalOsidObject
    implements org.osid.checklist.Todo {

    private final org.osid.checklist.Todo todo;


    /**
     *  Constructs a new <code>AbstractImmutableTodo</code>.
     *
     *  @param todo the todo to immutablize
     *  @throws org.osid.NullArgumentException <code>todo</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableTodo(org.osid.checklist.Todo todo) {
        super(todo);
        this.todo = todo;
        return;
    }


    /**
     *  Tests if this <code> Containable </code> is sequestered in
     *  that it should not appear outside of its aggregated
     *  composition.
     *
     *  @return <code> true </code> if this containable is
     *          sequestered, <code> false </code> if this containable
     *          may appear outside its aggregate
     */

    @OSID @Override
    public boolean isSequestered() {
        return (this.todo.isSequestered());
    }


    /**
     *  Tests if this todo has been checked off. 
     *
     *  @return <code> true </code> if this todo is complete, <code> false 
     *          </code> if not complete 
     */

    @OSID @Override
    public boolean isComplete() {
        return (this.todo.isComplete());
    }


    /**
     *  Gets a priority of this item. 
     *
     *  @return a priority type 
     */

    @OSID @Override
    public org.osid.type.Type getPriority() {
        return (this.todo.getPriority());
    }


    /**
     *  Gets the due date. 
     *
     *  @return a date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getDueDate() {
        return (this.todo.getDueDate());
    }


    /**
     *  Gets a list of todo <code> Ids </code> on which this todo is 
     *  dependent. 
     *
     *  @return a list of todo <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getDependencyIds() {
        return (this.todo.getDependencyIds());
    }


    /**
     *  Gets a list of todos on which this todo is dependent. 
     *
     *  @return a list of todos 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.checklist.TodoList getDependencies()
        throws org.osid.OperationFailedException {

        return (this.todo.getDependencies());
    }


    /**
     *  Gets the todo record corresponding to the given <code> Todo </code> 
     *  record <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record. The <code> todoRecordType </code> 
     *  may be the <code> Type </code> returned in <code> getRecordTypes() 
     *  </code> or any of its parents in a <code> Type </code> hierarchy where 
     *  <code> hasRecordType(todoRecordType) </code> is <code> true </code> . 
     *
     *  @param  todoRecordType the type of todo record to retrieve 
     *  @return the todo record 
     *  @throws org.osid.NullArgumentException <code> todoRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(todoRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.records.TodoRecord getTodoRecord(org.osid.type.Type todoRecordType)
        throws org.osid.OperationFailedException {

        return (this.todo.getTodoRecord(todoRecordType));
    }
}

