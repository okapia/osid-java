//
// MutableMapProxyPollsLookupSession
//
//    Implements a Polls lookup service backed by a collection of
//    pollses that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.voting;


/**
 *  Implements a Polls lookup service backed by a collection of
 *  pollses. The pollses are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of pollses can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyPollsLookupSession
    extends net.okapia.osid.jamocha.core.voting.spi.AbstractMapPollsLookupSession
    implements org.osid.voting.PollsLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableMapProxyPollsLookupSession} with no
     *  pollses.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public MutableMapProxyPollsLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyPollsLookupSession} with a
     *  single polls.
     *
     *  @param polls a polls
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code polls} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyPollsLookupSession(org.osid.voting.Polls polls, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putPolls(polls);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyPollsLookupSession} using an
     *  array of pollses.
     *
     *  @param pollses an array of pollses
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code pollses} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyPollsLookupSession(org.osid.voting.Polls[] pollses, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putPollses(pollses);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyPollsLookupSession} using
     *  a collection of pollses.
     *
     *  @param pollses a collection of pollses
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code pollses} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyPollsLookupSession(java.util.Collection<? extends org.osid.voting.Polls> pollses,
                                                org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putPollses(pollses);
        return;
    }

    
    /**
     *  Makes a {@code Polls} available in this session.
     *
     *  @param polls an polls
     *  @throws org.osid.NullArgumentException {@code polls{@code 
     *          is {@code null}
     */

    @Override
    public void putPolls(org.osid.voting.Polls polls) {
        super.putPolls(polls);
        return;
    }


    /**
     *  Makes an array of pollses available in this session.
     *
     *  @param pollses an array of pollses
     *  @throws org.osid.NullArgumentException {@code pollses{@code 
     *          is {@code null}
     */

    @Override
    public void putPollses(org.osid.voting.Polls[] pollses) {
        super.putPollses(pollses);
        return;
    }


    /**
     *  Makes collection of pollses available in this session.
     *
     *  @param pollses
     *  @throws org.osid.NullArgumentException {@code polls{@code 
     *          is {@code null}
     */

    @Override
    public void putPollses(java.util.Collection<? extends org.osid.voting.Polls> pollses) {
        super.putPollses(pollses);
        return;
    }


    /**
     *  Removes a Polls from this session.
     *
     *  @param pollsId the {@code Id} of the polls
     *  @throws org.osid.NullArgumentException {@code pollsId{@code  is
     *          {@code null}
     */

    @Override
    public void removePolls(org.osid.id.Id pollsId) {
        super.removePolls(pollsId);
        return;
    }    
}
