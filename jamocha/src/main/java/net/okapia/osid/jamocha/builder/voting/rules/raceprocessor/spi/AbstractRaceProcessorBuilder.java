//
// AbstractRaceProcessor.java
//
//     Defines a RaceProcessor builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.voting.rules.raceprocessor.spi;


/**
 *  Defines a <code>RaceProcessor</code> builder.
 */

public abstract class AbstractRaceProcessorBuilder<T extends AbstractRaceProcessorBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidProcessorBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.voting.rules.raceprocessor.RaceProcessorMiter raceProcessor;


    /**
     *  Constructs a new <code>AbstractRaceProcessorBuilder</code>.
     *
     *  @param raceProcessor the race processor to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractRaceProcessorBuilder(net.okapia.osid.jamocha.builder.voting.rules.raceprocessor.RaceProcessorMiter raceProcessor) {
        super(raceProcessor);
        this.raceProcessor = raceProcessor;
        return;
    }


    /**
     *  Builds the race processor.
     *
     *  @return the new race processor
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.voting.rules.RaceProcessor build() {
        (new net.okapia.osid.jamocha.builder.validator.voting.rules.raceprocessor.RaceProcessorValidator(getValidations())).validate(this.raceProcessor);
        return (new net.okapia.osid.jamocha.builder.voting.rules.raceprocessor.ImmutableRaceProcessor(this.raceProcessor));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the race processor miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.voting.rules.raceprocessor.RaceProcessorMiter getMiter() {
        return (this.raceProcessor);
    }


    /**
     *  Sets the maximum winners.
     *
     *  @param maximumWinner a maximum winners
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>maximumWinner</code> is <code>null</code>
     */

    public T maximumWinner(long maximumWinner) {
        getMiter().setMaximumWinners(maximumWinner);
        return (self());
    }


    /**
     *  Sets the minimum percentage to win.
     *
     *  @param percentage a minimum percentage to win
     *  @return the builder
     *  @throws org.osid.InvalidArgumentException
     *          <code>percentage</code> is out of range
     */

    public T minimumPercentageToWin(long percentage) {
        getMiter().setMinimumPercentageToWin(percentage);
        return (self());
    }


    /**
     *  Sets the minimum votes to win.
     *
     *  @param votes a minimum votes to win
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>votes</code> is
     *          <code>null</code>
     */

    public T minimumVotesToWin(long votes) {
        getMiter().setMinimumVotesToWin(votes);
        return (self());
    }


    /**
     *  Adds a RaceProcessor record.
     *
     *  @param record a race processor record
     *  @param recordType the type of race processor record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.voting.rules.records.RaceProcessorRecord record, org.osid.type.Type recordType) {
        getMiter().addRaceProcessorRecord(record, recordType);
        return (self());
    }
}       


