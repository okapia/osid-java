//
// AbstractCourse.java
//
//     Defines a Course.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.course.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Course</code>.
 */

public abstract class AbstractCourse
    extends net.okapia.osid.jamocha.spi.AbstractOperableOsidObject
    implements org.osid.course.Course {

    private org.osid.locale.DisplayText title;
    private String number;
    private org.osid.locale.DisplayText prerequisitesInfo;

    private boolean isGraded              = false;
    private boolean hasSponsors           = false;
    private boolean hasPrerequisites      = false;
    private boolean hasLearningObjectives = false;

    private final java.util.Collection<org.osid.resource.Resource> sponsors = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.grading.Grade> credits = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.requisite.Requisite> prerequisites = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.grading.Grade> levels = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.grading.GradeSystem> gradingOptions = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.learning.Objective> learningObjectives = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.records.CourseRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the formal title of this course. It may be the same as
     *  the display name or it may be used to more formally label the
     *  course. A display name might be Physics 102 where the title is
     *  Introduction to Electromagentism.
     *
     *  @return the course title 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getTitle() {
        return (this.title);
    }


    /**
     *  Sets the title.
     *
     *  @param title a title
     *  @throws org.osid.NullArgumentException <code>title</code> is
     *          <code>null</code>
     */

    protected void setTitle(org.osid.locale.DisplayText title) {
        nullarg(title, "title");
        this.title = title;
        return;
    }


    /**
     *  Gets the course number which is a label generally used to
     *  index the course in a catalog, such as T101 or 16.004.
     *
     *  @return the course number 
     */

    @OSID @Override
    public String getNumber() {
        return (this.number);
    }


    /**
     *  Sets the number.
     *
     *  @param number a number
     *  @throws org.osid.NullArgumentException <code>number</code> is
     *          <code>null</code>
     */

    protected void setNumber(String number) {
        nullarg(number, "course number");
        this.number = number;
        return;
    }


    /**
     *  Tests if this course has sponsors. 
     *
     *  @return <code> true </code> if this course has sponsors,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean hasSponsors() {
        return (this.hasSponsors);
    }


    /**
     *  Gets the sponsor <code> Ids. </code> 
     *
     *  @return the sponsor <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> hasSponsors() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getSponsorIds() {
        if (!hasSponsors()) {
            throw new org.osid.IllegalStateException("hasSponsors() is false");
        }

        try {
            org.osid.resource.ResourceList sponsors = getSponsors();
            return (new net.okapia.osid.jamocha.adapter.converter.resource.resource.ResourceToIdList(sponsors));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the sponsors. 
     *
     *  @return the sponsors 
     *  @throws org.osid.IllegalStateException <code> hasSponsors()
     *          </code> is <code> false </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getSponsors()
        throws org.osid.OperationFailedException {

        if (!hasSponsors()) {
            throw new org.osid.IllegalStateException("hasSponsors() is false");
        }

        return (new net.okapia.osid.jamocha.resource.resource.ArrayResourceList(this.sponsors));
    }


    /**
     *  Adds a sponsor.
     *
     *  @param sponsor a sponsor
     *  @throws org.osid.NullArgumentException <code>sponsor</code> is
     *          <code>null</code>
     */

    protected void addSponsor(org.osid.resource.Resource sponsor) {
        nullarg(sponsor, "sponsor");

        this.sponsors.add(sponsor);
        this.hasSponsors = true;

        return;
    }


    /**
     *  Sets all the sponsors.
     *
     *  @param sponsors a collection of sponsors
     *  @throws org.osid.NullArgumentException <code>sponsors</code>
     *          is <code>null</code>
     */

    protected void setSponsors(java.util.Collection<org.osid.resource.Resource> sponsors) {
        nullarg(sponsors, "sponsors");

        this.sponsors.clear();
        this.sponsors.addAll(sponsors);
        this.hasSponsors = true;

        return;
    }


    /**
     *  Gets the credit amount <code>Ids</code>.
     *
     *  @return the grading system <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getCreditAmountIds() {
        try {
            org.osid.grading.GradeList credits = getCreditAmounts();
            return (new net.okapia.osid.jamocha.adapter.converter.grading.grade.GradeToIdList(credits));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the credit amounts. 
     *
     *  @return the grade system 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradeList getCreditAmounts()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.grading.grade.ArrayGradeList(this.credits));
    }

   
    /**
     *  Adds a credit smount.
     *
     *  @param credit a credit amount
     *  @throws org.osid.NullArgumentException <code>amount</code> is
     *          <code>null</code>
     */

    protected void addCreditAmount(org.osid.grading.Grade amount) {
        nullarg(amount, "credit amount");
        this.credits.add(amount);
        return;
    }


    /**
     *  Sets all the credit amount options.
     *
     *  @param amounts a collection of credit amounts
     *  @throws org.osid.NullArgumentException <code>amounts</code>
     *          is <code>null</code>
     */

    protected void setCreditAmounts(java.util.Collection<org.osid.grading.Grade> amounts) {
        nullarg(amounts, "credit amounts");

        this.credits.clear();
        this.credits.addAll(amounts);

        return;
    }


    /**
     *  Gets the an informational string for the course prerequisites. 
     *
     *  @return the course prerequisites 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getPrerequisitesInfo() {
        return (this.prerequisitesInfo);
    }


    /**
     *  Sets the prerequisites info.
     *
     *  @param info a prerequisites info
     *  @throws org.osid.NullArgumentException <code>info</code> is
     *          <code>null</code>
     */

    protected void setPrerequisitesInfo(org.osid.locale.DisplayText info) {
        nullarg(info, "prerequisites info");
        this.prerequisitesInfo = info;
        return;
    }


    /**
     *  Tests if this course has <code> Requisites </code> for the
     *  course prerequisites.
     *
     *  @return <code> true </code> if this course has prerequisites
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean hasPrerequisites() {
        return (this.hasPrerequisites);
    }


    /**
     *  Gets the requisite <code> Ids </code> for the course prerequisites. 
     *
     *  @return the requisite <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> hasPrerequisites() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getPrerequisiteIds() {
        if (!hasPrerequisites()) {
            throw new org.osid.IllegalStateException("hasPrerequisites() is false");
        }

        try {
            org.osid.course.requisite.RequisiteList prerequisites = getPrerequisites();
            return (new net.okapia.osid.jamocha.adapter.converter.course.requisite.requisite.RequisiteToIdList(prerequisites));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the requisites for the course prerequisites. Each <code> 
     *  Requisite </code> is an <code> AND </code> term such that all <code> 
     *  Requisites </code> must be true for the prerequisites to be satisifed. 
     *
     *  @return the requisites 
     *  @throws org.osid.IllegalStateException <code> hasPrerequisites() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteList getPrerequisites()
        throws org.osid.OperationFailedException {

        if (!hasPrerequisites()) {
            throw new org.osid.IllegalStateException("hasPrerequisites() is false");
        }

        return (new net.okapia.osid.jamocha.course.requisite.requisite.ArrayRequisiteList(this.prerequisites));
    }


    /**
     *  Adds a prerequisite.
     *
     *  @param prerequisite a prerequisite
     *  @throws org.osid.NullArgumentException
     *          <code>prerequisite</code> is <code>null</code>
     */

    protected void addPrerequisite(org.osid.course.requisite.Requisite prerequisite) {
        nullarg(prerequisite, "prerequisite");

        this.prerequisites.add(prerequisite);
        this.hasPrerequisites = true;

        return;
    }


    /**
     *  Sets all the prerequisites.
     *
     *  @param prerequisites a collection of prerequisites
     *  @throws org.osid.NullArgumentException
     *          <code>prerequisites</code> is <code>null</code>
     */

    protected void setPrerequisites(java.util.Collection<org.osid.course.requisite.Requisite> prerequisites) {
        nullarg(prerequisites, "prerequisites");

        this.prerequisites.clear();
        this.prerequisites.addAll(prerequisites);
        this.hasPrerequisites = true;

        return;
    }


    /**
     *  Gets the grade level <code> Ids </code> of this course. Multiple 
     *  levels may exist for different systems. 
     *
     *  @return the returned list of grade level <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getLevelIds() {
        try {
            org.osid.grading.GradeList levels = getLevels();
            return (new net.okapia.osid.jamocha.adapter.converter.grading.grade.GradeToIdList(levels));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the grade levels of this course. Multiple levels may exist for 
     *  different systems. 
     *
     *  @return the returned list of grade levels 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradeList getLevels()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.grading.grade.ArrayGradeList(this.levels));
    }


    /**
     *  Adds a level.
     *
     *  @param level a level
     *  @throws org.osid.NullArgumentException
     *          <code>level</code> is <code>null</code>
     */

    protected void addLevel(org.osid.grading.Grade level) {
        nullarg(level, "grade level");
        this.levels.add(level);
        return;
    }


    /**
     *  Sets all the levels.
     *
     *  @param levels a collection of levels
     *  @throws org.osid.NullArgumentException
     *          <code>levels</code> is <code>null</code>
     */

    protected void setLevels(java.util.Collection<org.osid.grading.Grade> levels) {
        nullarg(levels, "grade levels");

        this.levels.clear();
        this.levels.addAll(levels);

        return;
    }


    /**
     *  Tests if this course is graded. 
     *
     *  @return <code> true </code> if this course is graded, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isGraded() {
        return (this.isGraded);
    }


    /**
     *  Gets the various grading option <code> Ids </code> available to 
     *  register in this course. 
     *
     *  @return the returned list of grading option <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> isGraded()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.id.IdList getGradingOptionIds() {
        if (!isGraded()) {
            throw new org.osid.IllegalStateException("isGraded() is false");
        }
        
        try {
            org.osid.grading.GradeSystemList gradingOptions = getGradingOptions();
            return (new net.okapia.osid.jamocha.adapter.converter.grading.gradesystem.GradeSystemToIdList(gradingOptions));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the various grading options available to register in this course. 
     *
     *  @return the returned list of grading options 
     *  @throws org.osid.IllegalStateException <code> isGraded() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemList getGradingOptions()
        throws org.osid.OperationFailedException {

        if (!isGraded()) {
            throw new org.osid.IllegalStateException("isGraded() is false");
        }

        return (new net.okapia.osid.jamocha.grading.gradesystem.ArrayGradeSystemList(this.gradingOptions));
    }


    /**
     *  Adds a grading option.
     *
     *  @param option a grading option
     *  @throws org.osid.NullArgumentException
     *          <code>option</code> is <code>null</code>
     */

    protected void addGradingOption(org.osid.grading.GradeSystem option) {
        nullarg(option, "grading option");

        this.gradingOptions.add(option);
        this.isGraded = true;

        return;
    }


    /**
     *  Sets all the grading options.
     *
     *  @param options a collection of grading options
     *  @throws org.osid.NullArgumentException <code>options</code> is
     *          <code>null</code>
     */

    protected void setGradingOptions(java.util.Collection<org.osid.grading.GradeSystem> options) {
        nullarg(options, "grading options");

        this.gradingOptions.clear();
        this.gradingOptions.addAll(options);
        this.isGraded = true;

        return;
    }


    /**
     *  Tests if this course has associated learning objectives. 
     *
     *  @return <code> true </code> if this course has a learning objective, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasLearningObjectives() {
        return (this.hasLearningObjectives);
    }


    /**
     *  Gets the overall learning objective <code> Ids </code> for this 
     *  course. 
     *
     *  @return <code> Ids </code> of the <code> l </code> earning objectives 
     *  @throws org.osid.IllegalStateException <code>
     *          hasLearningObjectives() </code> is <code> false
     *          </code>
     */

    @OSID @Override
    public org.osid.id.IdList getLearningObjectiveIds() {
        if (!hasLearningObjectives()) {
            throw new org.osid.IllegalStateException("hasLearningObjectives() is false");
        }

        try {
            org.osid.learning.ObjectiveList learningObjectives = getLearningObjectives();
            return (new net.okapia.osid.jamocha.adapter.converter.learning.objective.ObjectiveToIdList(learningObjectives));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the overall learning objectives for this course. 
     *
     *  @return the learning objectives 
     *  @throws org.osid.IllegalStateException <code> hasLearningObjectives() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveList getLearningObjectives()
        throws org.osid.OperationFailedException {

        if (!hasLearningObjectives()) {
            throw new org.osid.IllegalStateException("hasLearningObjectives() is false");
        }

        return (new net.okapia.osid.jamocha.learning.objective.ArrayObjectiveList(this.learningObjectives));
    }


    /**
     *  Adds a learning objective.
     *
     *  @param objective a learning objective
     *  @throws org.osid.NullArgumentException
     *          <code>objective</code> is <code>null</code>
     */

    protected void addLearningObjective(org.osid.learning.Objective objective) {
        nullarg(objective, "objective");

        this.learningObjectives.add(objective);
        this.hasLearningObjectives = true;

        return;
    }


    /**
     *  Sets all the learning objectives.
     *
     *  @param objectives a collection of learning objectives
     *  @throws org.osid.NullArgumentException
     *          <code>objectives</code> is <code>null</code>
     */

    protected void setLearningObjectives(java.util.Collection<org.osid.learning.Objective> objectives) {
        nullarg(objectives, "objectives");

        this.learningObjectives.clear();
        this.learningObjectives.addAll(objectives);
        this.hasLearningObjectives = true;

        return;
    }


    /**
     *  Tests if this course supports the given record
     *  <code>Type</code>.
     *
     *  @param  courseRecordType a course record type 
     *  @return <code>true</code> if the courseRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>courseRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type courseRecordType) {
        for (org.osid.course.records.CourseRecord record : this.records) {
            if (record.implementsRecordType(courseRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given <code>Course</code>
     *  record <code>Type</code>.
     *
     *  @param  courseRecordType the course record type 
     *  @return the course record 
     *  @throws org.osid.NullArgumentException
     *          <code>courseRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(courseRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.records.CourseRecord getCourseRecord(org.osid.type.Type courseRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.records.CourseRecord record : this.records) {
            if (record.implementsRecordType(courseRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(courseRecordType + " is not supported");
    }


    /**
     *  Adds a record to this course. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param courseRecord the course record
     *  @param courseRecordType course record type
     *  @throws org.osid.NullArgumentException
     *          <code>courseRecord</code> or
     *          <code>courseRecordTypecourse</code> is
     *          <code>null</code>
     */
            
    protected void addCourseRecord(org.osid.course.records.CourseRecord courseRecord, 
                                   org.osid.type.Type courseRecordType) {

        nullarg(courseRecord, "course record");
        addRecordType(courseRecordType);
        this.records.add(courseRecord);
        
        return;
    }
}
