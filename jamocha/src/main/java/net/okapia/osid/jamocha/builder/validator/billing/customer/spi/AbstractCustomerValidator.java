//
// AbstractCustomerValidator.java
//
//     Validates a Customer.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.billing.customer.spi;


/**
 *  Validates a Customer.
 */

public abstract class AbstractCustomerValidator
    extends net.okapia.osid.jamocha.builder.validator.spi.AbstractTemporalOsidObjectValidator {


    /**
     *  Constructs a new <code>AbstractCustomerValidator</code>.
     */

    protected AbstractCustomerValidator() {
        return;
    }


    /**
     *  Constructs a new <code>AbstractCustomerValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    protected AbstractCustomerValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates a Customer.
     *
     *  @param customer a customer to validate
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>customer</code>
     *          is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in
     *          assembly
     */
    
    public void validate(org.osid.billing.Customer customer) {
        super.validate(customer);

        testNestedObject(customer, "getResource");
        test(customer.getCustomerNumber(), "getCustomerNumber()");

        testConditionalMethod(customer, "getActivityId", customer.hasActivity(), "hasActivity()");
        testConditionalMethod(customer, "getActivity", customer.hasActivity(), "hasActivity()");
        if (customer.hasActivity()) {
            testNestedObject(customer, "getActivity");
        }

        return;
    }
}
