//
// AbstractEnrollmentQueryInspector.java
//
//     A template for making an EnrollmentQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.program.enrollment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for enrollments.
 */

public abstract class AbstractEnrollmentQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQueryInspector
    implements org.osid.course.program.EnrollmentQueryInspector {

    private final java.util.Collection<org.osid.course.program.records.EnrollmentQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the program offering <code> Id </code> query terms. 
     *
     *  @return the program offering <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProgramOfferingIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the program offering query terms. 
     *
     *  @return the program offering query terms 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingQueryInspector[] getProgramOfferingTerms() {
        return (new org.osid.course.program.ProgramOfferingQueryInspector[0]);
    }


    /**
     *  Gets the student <code> Id </code> query terms. 
     *
     *  @return the resource <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getStudentIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the student query terms. 
     *
     *  @return the student query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getStudentTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the course catalog <code> Id </code> query terms. 
     *
     *  @return the course catalog <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseCatalogIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the course catalog query terms. 
     *
     *  @return the course catalog query terms 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQueryInspector[] getCourseCatalogTerms() {
        return (new org.osid.course.CourseCatalogQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given enrollment query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an enrollment implementing the requested record.
     *
     *  @param enrollmentRecordType an enrollment record type
     *  @return the enrollment query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>enrollmentRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(enrollmentRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.program.records.EnrollmentQueryInspectorRecord getEnrollmentQueryInspectorRecord(org.osid.type.Type enrollmentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.program.records.EnrollmentQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(enrollmentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(enrollmentRecordType + " is not supported");
    }


    /**
     *  Adds a record to this enrollment query. 
     *
     *  @param enrollmentQueryInspectorRecord enrollment query inspector
     *         record
     *  @param enrollmentRecordType enrollment record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addEnrollmentQueryInspectorRecord(org.osid.course.program.records.EnrollmentQueryInspectorRecord enrollmentQueryInspectorRecord, 
                                                   org.osid.type.Type enrollmentRecordType) {

        addRecordType(enrollmentRecordType);
        nullarg(enrollmentRecordType, "enrollment record type");
        this.records.add(enrollmentQueryInspectorRecord);        
        return;
    }
}
