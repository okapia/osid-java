//
// AbstractTodoSearchOdrer.java
//
//     Defines a TodoSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.checklist.todo.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code TodoSearchOrder}.
 */

public abstract class AbstractTodoSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractTemporalOsidObjectSearchOrder
    implements org.osid.checklist.TodoSearchOrder {

    private final java.util.Collection<org.osid.checklist.records.TodoSearchOrderRecord> records = new java.util.LinkedHashSet<>();

    private final OsidContainableSearchOrder order = new OsidContainableSearchOrder();


    /**
     *  Specifies a preference for ordering the result set by the
     *  sequestered flag.
     *
     *  @param  style the search order style 
     *  @throws org.osid.NullArgumentException {@code style} is {@code
     *          null}
     */
    
    @OSID @Override
    public void orderBySequestered(org.osid.SearchOrderStyle style) {
        this.order.orderBySequestered(style);
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the completed 
     *  status. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByComplete(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the priority. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPriority(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the due date. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDueDate(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  todoRecordType a todo record type 
     *  @return {@code true} if the todoRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code todoRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type todoRecordType) {
        for (org.osid.checklist.records.TodoSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(todoRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  todoRecordType the todo record type 
     *  @return the todo search order record
     *  @throws org.osid.NullArgumentException
     *          {@code todoRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(todoRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.checklist.records.TodoSearchOrderRecord getTodoSearchOrderRecord(org.osid.type.Type todoRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.checklist.records.TodoSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(todoRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(todoRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this todo. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param todoRecord the todo search odrer record
     *  @param todoRecordType todo record type
     *  @throws org.osid.NullArgumentException
     *          {@code todoRecord} or
     *          {@code todoRecordTypetodo} is
     *          {@code null}
     */
            
    protected void addTodoRecord(org.osid.checklist.records.TodoSearchOrderRecord todoSearchOrderRecord, 
                                     org.osid.type.Type todoRecordType) {

        addRecordType(todoRecordType);
        this.records.add(todoSearchOrderRecord);
        
        return;
    }


    protected class OsidContainableSearchOrder
        extends net.okapia.osid.jamocha.spi.AbstractOsidContainableSearchOrder
        implements org.osid.OsidContainableSearchOrder {
    }
}
