//
// MutableIndexedMapObjectiveBankLookupSession
//
//    Implements an ObjectiveBank lookup service backed by a collection of
//    objectiveBanks indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.learning;


/**
 *  Implements an ObjectiveBank lookup service backed by a collection of
 *  objective banks. The objective banks are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some objective banks may be compatible
 *  with more types than are indicated through these objective bank
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of objective banks can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapObjectiveBankLookupSession
    extends net.okapia.osid.jamocha.core.learning.spi.AbstractIndexedMapObjectiveBankLookupSession
    implements org.osid.learning.ObjectiveBankLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapObjectiveBankLookupSession} with no
     *  objective banks.
     */

    public MutableIndexedMapObjectiveBankLookupSession() {
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapObjectiveBankLookupSession} with a
     *  single objective bank.
     *  
     *  @param  objectiveBank an single objectiveBank
     *  @throws org.osid.NullArgumentException {@code objectiveBank}
     *          is {@code null}
     */

    public MutableIndexedMapObjectiveBankLookupSession(org.osid.learning.ObjectiveBank objectiveBank) {
        putObjectiveBank(objectiveBank);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapObjectiveBankLookupSession} using an
     *  array of objective banks.
     *
     *  @param  objectiveBanks an array of objective banks
     *  @throws org.osid.NullArgumentException {@code objectiveBanks}
     *          is {@code null}
     */

    public MutableIndexedMapObjectiveBankLookupSession(org.osid.learning.ObjectiveBank[] objectiveBanks) {
        putObjectiveBanks(objectiveBanks);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapObjectiveBankLookupSession} using a
     *  collection of objective banks.
     *
     *  @param  objectiveBanks a collection of objective banks
     *  @throws org.osid.NullArgumentException {@code objectiveBanks} is
     *          {@code null}
     */

    public MutableIndexedMapObjectiveBankLookupSession(java.util.Collection<? extends org.osid.learning.ObjectiveBank> objectiveBanks) {
        putObjectiveBanks(objectiveBanks);
        return;
    }
    

    /**
     *  Makes an {@code ObjectiveBank} available in this session.
     *
     *  @param  objectiveBank an objective bank
     *  @throws org.osid.NullArgumentException {@code objectiveBank{@code  is
     *          {@code null}
     */

    @Override
    public void putObjectiveBank(org.osid.learning.ObjectiveBank objectiveBank) {
        super.putObjectiveBank(objectiveBank);
        return;
    }


    /**
     *  Makes an array of objective banks available in this session.
     *
     *  @param  objectiveBanks an array of objective banks
     *  @throws org.osid.NullArgumentException {@code objectiveBanks{@code 
     *          is {@code null}
     */

    @Override
    public void putObjectiveBanks(org.osid.learning.ObjectiveBank[] objectiveBanks) {
        super.putObjectiveBanks(objectiveBanks);
        return;
    }


    /**
     *  Makes collection of objective banks available in this session.
     *
     *  @param  objectiveBanks a collection of objective banks
     *  @throws org.osid.NullArgumentException {@code objectiveBank{@code  is
     *          {@code null}
     */

    @Override
    public void putObjectiveBanks(java.util.Collection<? extends org.osid.learning.ObjectiveBank> objectiveBanks) {
        super.putObjectiveBanks(objectiveBanks);
        return;
    }


    /**
     *  Removes an ObjectiveBank from this session.
     *
     *  @param objectiveBankId the {@code Id} of the objective bank
     *  @throws org.osid.NullArgumentException {@code objectiveBankId{@code  is
     *          {@code null}
     */

    @Override
    public void removeObjectiveBank(org.osid.id.Id objectiveBankId) {
        super.removeObjectiveBank(objectiveBankId);
        return;
    }    
}
