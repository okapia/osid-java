//
// AbstractFederatingCanonicalUnitProcessorLookupSession.java
//
//     An abstract federating adapter for a
//     CanonicalUnitProcessorLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.offering.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  CanonicalUnitProcessorLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingCanonicalUnitProcessorLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.offering.rules.CanonicalUnitProcessorLookupSession>
    implements org.osid.offering.rules.CanonicalUnitProcessorLookupSession {

    private boolean parallel = false;
    private org.osid.offering.Catalogue catalogue = new net.okapia.osid.jamocha.nil.offering.catalogue.UnknownCatalogue();


    /**
     *  Constructs a new <code>AbstractFederatingCanonicalUnitProcessorLookupSession</code>.
     */

    protected AbstractFederatingCanonicalUnitProcessorLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.offering.rules.CanonicalUnitProcessorLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Catalogue/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Catalogue Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCatalogueId() {
        return (this.catalogue.getId());
    }


    /**
     *  Gets the <code>Catalogue</code> associated with this 
     *  session.
     *
     *  @return the <code>Catalogue</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.Catalogue getCatalogue()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.catalogue);
    }


    /**
     *  Sets the <code>Catalogue</code>.
     *
     *  @param  catalogue the catalogue for this session
     *  @throws org.osid.NullArgumentException <code>catalogue</code>
     *          is <code>null</code>
     */

    protected void setCatalogue(org.osid.offering.Catalogue catalogue) {
        nullarg(catalogue, "catalogue");
        this.catalogue = catalogue;
        return;
    }


    /**
     *  Tests if this user can perform <code>CanonicalUnitProcessor</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCanonicalUnitProcessors() {
        for (org.osid.offering.rules.CanonicalUnitProcessorLookupSession session : getSessions()) {
            if (session.canLookupCanonicalUnitProcessors()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>CanonicalUnitProcessor</code>
     *  returns is desired.  Methods will return what is requested or
     *  result in an error. This view is used when greater precision
     *  is desired at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCanonicalUnitProcessorView() {
        for (org.osid.offering.rules.CanonicalUnitProcessorLookupSession session : getSessions()) {
            session.useComparativeCanonicalUnitProcessorView();
        }

        return;
    }


    /**
     *  A complete view of the <code>CanonicalUnitProcessor</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCanonicalUnitProcessorView() {
        for (org.osid.offering.rules.CanonicalUnitProcessorLookupSession session : getSessions()) {
            session.usePlenaryCanonicalUnitProcessorView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include canonical unit processors in catalogues which are children
     *  of this catalogue in the catalogue hierarchy.
     */

    @OSID @Override
    public void useFederatedCatalogueView() {
        for (org.osid.offering.rules.CanonicalUnitProcessorLookupSession session : getSessions()) {
            session.useFederatedCatalogueView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this catalogue only.
     */

    @OSID @Override
    public void useIsolatedCatalogueView() {
        for (org.osid.offering.rules.CanonicalUnitProcessorLookupSession session : getSessions()) {
            session.useIsolatedCatalogueView();
        }

        return;
    }


    /**
     *  Only active canonical unit processors are returned by methods
     *  in this session.
     */
     
    @OSID @Override
    public void useActiveCanonicalUnitProcessorView() {
        for (org.osid.offering.rules.CanonicalUnitProcessorLookupSession session : getSessions()) {
            session.useActiveCanonicalUnitProcessorView();
        }

        return;
    }


    /**
     *  Active and inactive canonical unit processors are returned by
     *  methods in this session.
     */
    
    @OSID @Override
    public void useAnyStatusCanonicalUnitProcessorView() {
        for (org.osid.offering.rules.CanonicalUnitProcessorLookupSession session : getSessions()) {
            session.useAnyStatusCanonicalUnitProcessorView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>CanonicalUnitProcessor</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>CanonicalUnitProcessor</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>CanonicalUnitProcessor</code> and retained for
     *  compatibility.
     *
     *  In active mode, canonical unit processors are returned that
     *  are currently active. In any status mode, active and inactive
     *  canonical unit processors are returned.
     *
     *  @param  canonicalUnitProcessorId <code>Id</code> of the
     *          <code>CanonicalUnitProcessor</code>
     *  @return the canonical unit processor
     *  @throws org.osid.NotFoundException <code>canonicalUnitProcessorId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>canonicalUnitProcessorId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessor getCanonicalUnitProcessor(org.osid.id.Id canonicalUnitProcessorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.offering.rules.CanonicalUnitProcessorLookupSession session : getSessions()) {
            try {
                return (session.getCanonicalUnitProcessor(canonicalUnitProcessorId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(canonicalUnitProcessorId + " not found");
    }


    /**
     *  Gets a <code>CanonicalUnitProcessorList</code> corresponding
     *  to the given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  canonicalUnitProcessors specified in the <code>Id</code> list,
     *  in the order of the list, including duplicates, or an error
     *  results if an <code>Id</code> in the supplied list is not
     *  found or inaccessible. Otherwise, inaccessible
     *  <code>CanonicalUnitProcessors</code> may be omitted from the
     *  list and may present the elements in any order including
     *  returning a unique set.
     *
     *  In active mode, canonical unit processors are returned that
     *  are currently active. In any status mode, active and inactive
     *  canonical unit processors are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getCanonicalUnitProcessors()</code>.
     *
     *  @param  canonicalUnitProcessorIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>CanonicalUnitProcessor</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitProcessorIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorList getCanonicalUnitProcessorsByIds(org.osid.id.IdList canonicalUnitProcessorIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.offering.rules.canonicalunitprocessor.MutableCanonicalUnitProcessorList ret = new net.okapia.osid.jamocha.offering.rules.canonicalunitprocessor.MutableCanonicalUnitProcessorList();

        try (org.osid.id.IdList ids = canonicalUnitProcessorIds) {
            while (ids.hasNext()) {
                ret.addCanonicalUnitProcessor(getCanonicalUnitProcessor(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>CanonicalUnitProcessorList</code> corresponding
     *  to the given canonical unit processor genus <code>Type</code>
     *  which does not include canonical unit processors of types
     *  derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  canonical unit processors or an error results. Otherwise, the
     *  returned list may contain only those canonical unit processors
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, canonical unit processors are returned that
     *  are currently active. In any status mode, active and inactive
     *  canonical unit processors are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from
     *  <code>getCanonicalUnitProcessors()</code>.
     *
     *  @param canonicalUnitProcessorGenusType a
     *         canonicalUnitProcessor genus type
     *  @return the returned <code>CanonicalUnitProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitProcessorGenusType</code> is
     *          <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorList getCanonicalUnitProcessorsByGenusType(org.osid.type.Type canonicalUnitProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.rules.canonicalunitprocessor.FederatingCanonicalUnitProcessorList ret = getCanonicalUnitProcessorList();

        for (org.osid.offering.rules.CanonicalUnitProcessorLookupSession session : getSessions()) {
            ret.addCanonicalUnitProcessorList(session.getCanonicalUnitProcessorsByGenusType(canonicalUnitProcessorGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CanonicalUnitProcessorList</code> corresponding
     *  to the given canonical unit processor genus <code>Type</code>
     *  and include any additional canonical unit processors with
     *  genus types derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  canonical unit processors or an error results. Otherwise, the
     *  returned list may contain only those canonical unit processors
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, canonical unit processors are returned that
     *  are currently active. In any status mode, active and inactive
     *  canonical unit processors are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getCanonicalUnitProcessors()</code>.
     *
     *  @param canonicalUnitProcessorGenusType a
     *         canonicalUnitProcessor genus type
     *  @return the returned <code>CanonicalUnitProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitProcessorGenusType</code> is
     *          <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorList getCanonicalUnitProcessorsByParentGenusType(org.osid.type.Type canonicalUnitProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.rules.canonicalunitprocessor.FederatingCanonicalUnitProcessorList ret = getCanonicalUnitProcessorList();

        for (org.osid.offering.rules.CanonicalUnitProcessorLookupSession session : getSessions()) {
            ret.addCanonicalUnitProcessorList(session.getCanonicalUnitProcessorsByParentGenusType(canonicalUnitProcessorGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CanonicalUnitProcessorList</code> containing the
     *  given canonical unit processor record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known
     *  canonical unit processors or an error results. Otherwise, the
     *  returned list may contain only those canonical unit processors
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, canonical unit processors are returned that
     *  are currently active. In any status mode, active and inactive
     *  canonical unit processors are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from
     *  <code>getCanonicalUnitProcessors()</code>.
     *
     *  @param canonicalUnitProcessorRecordType a
     *         canonicalUnitProcessor record type
     *  @return the returned <code>CanonicalUnitProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitProcessorRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorList getCanonicalUnitProcessorsByRecordType(org.osid.type.Type canonicalUnitProcessorRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.rules.canonicalunitprocessor.FederatingCanonicalUnitProcessorList ret = getCanonicalUnitProcessorList();

        for (org.osid.offering.rules.CanonicalUnitProcessorLookupSession session : getSessions()) {
            ret.addCanonicalUnitProcessorList(session.getCanonicalUnitProcessorsByRecordType(canonicalUnitProcessorRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>CanonicalUnitProcessors</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  canonical unit processors or an error results. Otherwise, the
     *  returned list may contain only those canonical unit processors
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, canonical unit processors are returned that
     *  are currently active. In any status mode, active and inactive
     *  canonical unit processors are returned.
     *
     *  @return a list of <code>CanonicalUnitProcessors</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorList getCanonicalUnitProcessors()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.rules.canonicalunitprocessor.FederatingCanonicalUnitProcessorList ret = getCanonicalUnitProcessorList();

        for (org.osid.offering.rules.CanonicalUnitProcessorLookupSession session : getSessions()) {
            ret.addCanonicalUnitProcessorList(session.getCanonicalUnitProcessors());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.offering.rules.canonicalunitprocessor.FederatingCanonicalUnitProcessorList getCanonicalUnitProcessorList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.offering.rules.canonicalunitprocessor.ParallelCanonicalUnitProcessorList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.offering.rules.canonicalunitprocessor.CompositeCanonicalUnitProcessorList());
        }
    }
}
