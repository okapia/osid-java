//
// AbstractBallotConstrainer.java
//
//     Defines a BallotConstrainer builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.voting.rules.ballotconstrainer.spi;


/**
 *  Defines a <code>BallotConstrainer</code> builder.
 */

public abstract class AbstractBallotConstrainerBuilder<T extends AbstractBallotConstrainerBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidConstrainerBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.voting.rules.ballotconstrainer.BallotConstrainerMiter ballotConstrainer;


    /**
     *  Constructs a new <code>AbstractBallotConstrainerBuilder</code>.
     *
     *  @param ballotConstrainer the ballot constrainer to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractBallotConstrainerBuilder(net.okapia.osid.jamocha.builder.voting.rules.ballotconstrainer.BallotConstrainerMiter ballotConstrainer) {
        super(ballotConstrainer);
        this.ballotConstrainer = ballotConstrainer;
        return;
    }


    /**
     *  Builds the ballot constrainer.
     *
     *  @return the new ballot constrainer
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.voting.rules.BallotConstrainer build() {
        (new net.okapia.osid.jamocha.builder.validator.voting.rules.ballotconstrainer.BallotConstrainerValidator(getValidations())).validate(this.ballotConstrainer);
        return (new net.okapia.osid.jamocha.builder.voting.rules.ballotconstrainer.ImmutableBallotConstrainer(this.ballotConstrainer));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the ballot constrainer miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.voting.rules.ballotconstrainer.BallotConstrainerMiter getMiter() {
        return (this.ballotConstrainer);
    }


    /**
     *  Adds a BallotConstrainer record.
     *
     *  @param record a ballot constrainer record
     *  @param recordType the type of ballot constrainer record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.voting.rules.records.BallotConstrainerRecord record, org.osid.type.Type recordType) {
        getMiter().addBallotConstrainerRecord(record, recordType);
        return (self());
    }
}       


