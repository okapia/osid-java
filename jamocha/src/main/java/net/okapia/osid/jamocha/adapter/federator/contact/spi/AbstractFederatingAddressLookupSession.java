//
// AbstractFederatingAddressLookupSession.java
//
//     An abstract federating adapter for an AddressLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.contact.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for an
 *  AddressLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingAddressLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.contact.AddressLookupSession>
    implements org.osid.contact.AddressLookupSession {

    private boolean parallel = false;
    private org.osid.contact.AddressBook addressBook = new net.okapia.osid.jamocha.nil.contact.addressbook.UnknownAddressBook();


    /**
     *  Constructs a new <code>AbstractFederatingAddressLookupSession</code>.
     */

    protected AbstractFederatingAddressLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.contact.AddressLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>AddressBook/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>AddressBook Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAddressBookId() {
        return (this.addressBook.getId());
    }


    /**
     *  Gets the <code>AddressBook</code> associated with this 
     *  session.
     *
     *  @return the <code>AddressBook</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressBook getAddressBook()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.addressBook);
    }


    /**
     *  Sets the <code>AddressBook</code>.
     *
     *  @param  addressBook the address book for this session
     *  @throws org.osid.NullArgumentException <code>addressBook</code>
     *          is <code>null</code>
     */

    protected void setAddressBook(org.osid.contact.AddressBook addressBook) {
        nullarg(addressBook, "address book");
        this.addressBook = addressBook;
        return;
    }


    /**
     *  Tests if this user can perform <code>Address</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAddresses() {
        for (org.osid.contact.AddressLookupSession session : getSessions()) {
            if (session.canLookupAddresses()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Address</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAddressView() {
        for (org.osid.contact.AddressLookupSession session : getSessions()) {
            session.useComparativeAddressView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Address</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAddressView() {
        for (org.osid.contact.AddressLookupSession session : getSessions()) {
            session.usePlenaryAddressView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include addresses in address books which are children
     *  of this address book in the address book hierarchy.
     */

    @OSID @Override
    public void useFederatedAddressBookView() {
        for (org.osid.contact.AddressLookupSession session : getSessions()) {
            session.useFederatedAddressBookView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this address book only.
     */

    @OSID @Override
    public void useIsolatedAddressBookView() {
        for (org.osid.contact.AddressLookupSession session : getSessions()) {
            session.useIsolatedAddressBookView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Address</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Address</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Address</code> and
     *  retained for compatibility.
     *
     *  @param  addressId <code>Id</code> of the
     *          <code>Address</code>
     *  @return the address
     *  @throws org.osid.NotFoundException <code>addressId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>addressId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.Address getAddress(org.osid.id.Id addressId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.contact.AddressLookupSession session : getSessions()) {
            try {
                return (session.getAddress(addressId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(addressId + " not found");
    }


    /**
     *  Gets an <code>AddressList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  addresses specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Addresses</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  addressIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Address</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>addressIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressList getAddressesByIds(org.osid.id.IdList addressIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.contact.address.MutableAddressList ret = new net.okapia.osid.jamocha.contact.address.MutableAddressList();

        try (org.osid.id.IdList ids = addressIds) {
            while (ids.hasNext()) {
                ret.addAddress(getAddress(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets an <code>AddressList</code> corresponding to the given
     *  address genus <code>Type</code> which does not include
     *  addresses of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  addresses or an error results. Otherwise, the returned list
     *  may contain only those addresses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  addressGenusType an address genus type 
     *  @return the returned <code>Address</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>addressGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressList getAddressesByGenusType(org.osid.type.Type addressGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.contact.address.FederatingAddressList ret = getAddressList();

        for (org.osid.contact.AddressLookupSession session : getSessions()) {
            ret.addAddressList(session.getAddressesByGenusType(addressGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AddressList</code> corresponding to the given
     *  address genus <code>Type</code> and include any additional
     *  addresses with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  addresses or an error results. Otherwise, the returned list
     *  may contain only those addresses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  addressGenusType an address genus type 
     *  @return the returned <code>Address</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>addressGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressList getAddressesByParentGenusType(org.osid.type.Type addressGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.contact.address.FederatingAddressList ret = getAddressList();

        for (org.osid.contact.AddressLookupSession session : getSessions()) {
            ret.addAddressList(session.getAddressesByParentGenusType(addressGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AddressList</code> containing the given
     *  address record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  addresses or an error results. Otherwise, the returned list
     *  may contain only those addresses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  addressRecordType an address record type 
     *  @return the returned <code>Address</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>addressRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressList getAddressesByRecordType(org.osid.type.Type addressRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.contact.address.FederatingAddressList ret = getAddressList();

        for (org.osid.contact.AddressLookupSession session : getSessions()) {
            ret.addAddressList(session.getAddressesByRecordType(addressRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AddressList</code> for the given resource. In
     *  plenary mode, the returned list contains all known addresses
     *  or an error results. Otherwise, the returned list may contain
     *  only those addresses that are accessible through this session.
     *
     *  @param  resourceId a resource <code>Id</code>
     *  @return the returned <code>Address</code> list
     *  @throws org.osid.NullArgumentException <code>resourceId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.contact.AddressList getAddressesByResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.contact.address.FederatingAddressList ret = getAddressList();

        for (org.osid.contact.AddressLookupSession session : getSessions()) {
            ret.addAddressList(session.getAddressesByResource(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Addresses</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  addresses or an error results. Otherwise, the returned list
     *  may contain only those addresses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Addresses</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressList getAddresses()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.contact.address.FederatingAddressList ret = getAddressList();

        for (org.osid.contact.AddressLookupSession session : getSessions()) {
            ret.addAddressList(session.getAddresses());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.contact.address.FederatingAddressList getAddressList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.contact.address.ParallelAddressList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.contact.address.CompositeAddressList());
        }
    }
}
