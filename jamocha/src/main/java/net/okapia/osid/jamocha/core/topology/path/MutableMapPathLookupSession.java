//
// MutableMapPathLookupSession
//
//    Implements a Path lookup service backed by a collection of
//    paths that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.topology.path;


/**
 *  Implements a Path lookup service backed by a collection of
 *  paths. The paths are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of paths can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapPathLookupSession
    extends net.okapia.osid.jamocha.core.topology.path.spi.AbstractMapPathLookupSession
    implements org.osid.topology.path.PathLookupSession {


    /**
     *  Constructs a new {@code MutableMapPathLookupSession}
     *  with no paths.
     *
     *  @param graph the graph
     *  @throws org.osid.NullArgumentException {@code graph} is
     *          {@code null}
     */

      public MutableMapPathLookupSession(org.osid.topology.Graph graph) {
        setGraph(graph);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapPathLookupSession} with a
     *  single path.
     *
     *  @param graph the graph  
     *  @param path a path
     *  @throws org.osid.NullArgumentException {@code graph} or
     *          {@code path} is {@code null}
     */

    public MutableMapPathLookupSession(org.osid.topology.Graph graph,
                                           org.osid.topology.path.Path path) {
        this(graph);
        putPath(path);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapPathLookupSession}
     *  using an array of paths.
     *
     *  @param graph the graph
     *  @param paths an array of paths
     *  @throws org.osid.NullArgumentException {@code graph} or
     *          {@code paths} is {@code null}
     */

    public MutableMapPathLookupSession(org.osid.topology.Graph graph,
                                           org.osid.topology.path.Path[] paths) {
        this(graph);
        putPaths(paths);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapPathLookupSession}
     *  using a collection of paths.
     *
     *  @param graph the graph
     *  @param paths a collection of paths
     *  @throws org.osid.NullArgumentException {@code graph} or
     *          {@code paths} is {@code null}
     */

    public MutableMapPathLookupSession(org.osid.topology.Graph graph,
                                           java.util.Collection<? extends org.osid.topology.path.Path> paths) {

        this(graph);
        putPaths(paths);
        return;
    }

    
    /**
     *  Makes a {@code Path} available in this session.
     *
     *  @param path a path
     *  @throws org.osid.NullArgumentException {@code path{@code  is
     *          {@code null}
     */

    @Override
    public void putPath(org.osid.topology.path.Path path) {
        super.putPath(path);
        return;
    }


    /**
     *  Makes an array of paths available in this session.
     *
     *  @param paths an array of paths
     *  @throws org.osid.NullArgumentException {@code paths{@code 
     *          is {@code null}
     */

    @Override
    public void putPaths(org.osid.topology.path.Path[] paths) {
        super.putPaths(paths);
        return;
    }


    /**
     *  Makes collection of paths available in this session.
     *
     *  @param paths a collection of paths
     *  @throws org.osid.NullArgumentException {@code paths{@code  is
     *          {@code null}
     */

    @Override
    public void putPaths(java.util.Collection<? extends org.osid.topology.path.Path> paths) {
        super.putPaths(paths);
        return;
    }


    /**
     *  Removes a Path from this session.
     *
     *  @param pathId the {@code Id} of the path
     *  @throws org.osid.NullArgumentException {@code pathId{@code 
     *          is {@code null}
     */

    @Override
    public void removePath(org.osid.id.Id pathId) {
        super.removePath(pathId);
        return;
    }    
}
