//
// AbstractVoterAllocation.java
//
//     Defines a VoterAllocation.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.voterallocation.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;


/**
 *  Defines a <code>VoterAllocation</code>.
 */

public abstract class AbstractVoterAllocation
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.voting.VoterAllocation {

    private org.osid.voting.Race race;
    private org.osid.resource.Resource voter;
    private long totalVotes;
    private long maxVotesPerCandidate;
    private long maxCandidates = -1;
    private boolean reallocate = false;

    private final java.util.Collection<org.osid.voting.records.VoterAllocationRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the race <code> Id </code> of the vote. 
     *
     *  @return the ballot item <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getRaceId() {
        return (this.race.getId());
    }


    /**
     *  Gets the race of the vote. 
     *
     *  @return the race 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.voting.Race getRace()
        throws org.osid.OperationFailedException {

        return (this.race);
    }


    /**
     *  Sets the race.
     *
     *  @param race a race
     *  @throws org.osid.NullArgumentException <code>race</code> is
     *          <code>null</code>
     */

    protected void setRace(org.osid.voting.Race race) {
        nullarg(race, "race");
        this.race = race;
        return;
    }


    /**
     *  Gets the resource <code> Id. </code> 
     *
     *  @return a resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getVoterId() {
        return (this.voter.getId());
    }


    /**
     *  Gets the <code> Resource. </code> 
     *
     *  @return the resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getVoter()
        throws org.osid.OperationFailedException {

        return (this.voter);
    }


    /**
     *  Sets the voter.
     *
     *  @param voter a voter
     *  @throws org.osid.NullArgumentException <code>voter</code> is
     *          <code>null</code>
     */

    protected void setVoter(org.osid.resource.Resource voter) {
        nullarg(voter, "voter");
        this.voter = voter;
        return;
    }


    /**
     *  Gets the total number of votes the resource can cast in this race. 
     *
     *  @return the number of votes 
     */

    @OSID @Override
    public long getTotalVotes() {
        return (this.totalVotes);
    }


    /**
     *  Sets the total votes.
     *
     *  @param votes the total votes
     *  @throws org.osid.InvalidArgumentException <code>votes</code>
     *          is negative
     */

    protected void setTotalVotes(long votes) {
        cardinalarg(votes, "total votes");
        this.totalVotes = votes;
        return;
    }


    /**
     *  Gets the maximum votes per candidate the resource can cast in
     *  this race.
     *
     *  @return the max votes per candidate 
     */

    @OSID @Override
    public long getMaxVotesPerCandidate() {
        return (this.maxVotesPerCandidate);
    }


    /**
     *  Sets the max votes per candidate.
     *
     *  @param votes the max votes per candidate
     *  @throws org.osid.InvalidArgumentException <code>votes</code>
     *          is negative
     */

    protected void setMaxVotesPerCandidate(long votes) {
        cardinalarg(votes, "max votes per candidate");
        this.maxVotesPerCandidate = votes;
        return;
    }


    /**
     *  Tests if this resource can vote for a limited number of
     *  candidates.
     *
     *  @return <code> true </code> if a limit on the number of
     *          candidates exists, <code> false </code> if no limit
     *          exists
     */

    @OSID @Override
    public boolean hasMaxCandidates() {
        if (this.maxCandidates < 0) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the maximum number of candidates for which this resource can 
     *  vote. 
     *
     *  @return the maximum candidates 
     *  @throws org.osid.IllegalStateException <code> hasMaxCandidates() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public long getMaxCandidates() {
        if (!hasMaxCandidates()) {
            throw new org.osid.IllegalStateException("hasMaxCandidates() is false");
        }

        return (this.maxCandidates);
    }


    /**
     *  Sets the max candidates.
     *
     *  @param candidates the max candidates
     *  @throws org.osid.InvalidArgumentException
     *          <code>candidates</code> is negative
     */

    protected void setMaxCandidates(long candidates) {
        cardinalarg(candidates, "max candidates");
        this.maxCandidates = candidates;
        return;
    }


    /**
     *  Tests if this resource gets the number of votes cast for a
     *  candidate when the candidate is no longer running in a race.
     *
     *  @return <code> true </code> if votes are returned to the
     *          resource when the candidate drops from the race,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean reallocatesVotesWhenCandidateDrops() {
        return (this.reallocate);
    }


    /**
     *  Sets the reallocates votes when candidate drops.
     *
     *  @param reallocate <code> true </code> if votes are returned to
     *         the resource when the candidate drops from the race,
     *         <code> false </code> otherwise
     */

    protected void setReallocatesVotesWhenCandidateDrops(boolean reallocate) {
        this.reallocate = reallocate;
        return;
    }


    /**
     *  Tests if this voter allocation supports the given record
     *  <code>Type</code>.
     *
     *  @param  voterAllocationRecordType a voter allocation record type 
     *  @return <code>true</code> if the voterAllocationRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>voterAllocationRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type voterAllocationRecordType) {
        for (org.osid.voting.records.VoterAllocationRecord record : this.records) {
            if (record.implementsRecordType(voterAllocationRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>VoterAllocation</code> record <code>Type</code>.
     *
     *  @param voterAllocationRecordType the voter allocation record
     *         type
     *  @return the voter allocation record 
     *  @throws org.osid.NullArgumentException
     *          <code>voterAllocationRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(voterAllocationRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.records.VoterAllocationRecord getVoterAllocationRecord(org.osid.type.Type voterAllocationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.records.VoterAllocationRecord record : this.records) {
            if (record.implementsRecordType(voterAllocationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(voterAllocationRecordType + " is not supported");
    }


    /**
     *  Adds a record to this voter allocation. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param voterAllocationRecord the voter allocation record
     *  @param voterAllocationRecordType voter allocation record type
     *  @throws org.osid.NullArgumentException
     *          <code>voterAllocationRecord</code> or
     *          <code>voterAllocationRecordTypevoterAllocation</code> is
     *          <code>null</code>
     */
            
    protected void addVoterAllocationRecord(org.osid.voting.records.VoterAllocationRecord voterAllocationRecord, 
                                     org.osid.type.Type voterAllocationRecordType) {

        addRecordType(voterAllocationRecordType);
        this.records.add(voterAllocationRecord);
        
        return;
    }
}
