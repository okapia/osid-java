//
// AbstractIndexedMapAvailabilityEnablerLookupSession.java
//
//    A simple framework for providing an AvailabilityEnabler lookup service
//    backed by a fixed collection of availability enablers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an AvailabilityEnabler lookup service backed by a
 *  fixed collection of availability enablers. The availability enablers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some availability enablers may be compatible
 *  with more types than are indicated through these availability enabler
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>AvailabilityEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapAvailabilityEnablerLookupSession
    extends AbstractMapAvailabilityEnablerLookupSession
    implements org.osid.resourcing.rules.AvailabilityEnablerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.resourcing.rules.AvailabilityEnabler> availabilityEnablersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.resourcing.rules.AvailabilityEnabler>());
    private final MultiMap<org.osid.type.Type, org.osid.resourcing.rules.AvailabilityEnabler> availabilityEnablersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.resourcing.rules.AvailabilityEnabler>());


    /**
     *  Makes an <code>AvailabilityEnabler</code> available in this session.
     *
     *  @param  availabilityEnabler an availability enabler
     *  @throws org.osid.NullArgumentException <code>availabilityEnabler<code> is
     *          <code>null</code>
     */

    @Override
    protected void putAvailabilityEnabler(org.osid.resourcing.rules.AvailabilityEnabler availabilityEnabler) {
        super.putAvailabilityEnabler(availabilityEnabler);

        this.availabilityEnablersByGenus.put(availabilityEnabler.getGenusType(), availabilityEnabler);
        
        try (org.osid.type.TypeList types = availabilityEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.availabilityEnablersByRecord.put(types.getNextType(), availabilityEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an availability enabler from this session.
     *
     *  @param availabilityEnablerId the <code>Id</code> of the availability enabler
     *  @throws org.osid.NullArgumentException <code>availabilityEnablerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeAvailabilityEnabler(org.osid.id.Id availabilityEnablerId) {
        org.osid.resourcing.rules.AvailabilityEnabler availabilityEnabler;
        try {
            availabilityEnabler = getAvailabilityEnabler(availabilityEnablerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.availabilityEnablersByGenus.remove(availabilityEnabler.getGenusType());

        try (org.osid.type.TypeList types = availabilityEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.availabilityEnablersByRecord.remove(types.getNextType(), availabilityEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeAvailabilityEnabler(availabilityEnablerId);
        return;
    }


    /**
     *  Gets an <code>AvailabilityEnablerList</code> corresponding to the given
     *  availability enabler genus <code>Type</code> which does not include
     *  availability enablers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known availability enablers or an error results. Otherwise,
     *  the returned list may contain only those availability enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  availabilityEnablerGenusType an availability enabler genus type 
     *  @return the returned <code>AvailabilityEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>availabilityEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerList getAvailabilityEnablersByGenusType(org.osid.type.Type availabilityEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.resourcing.rules.availabilityenabler.ArrayAvailabilityEnablerList(this.availabilityEnablersByGenus.get(availabilityEnablerGenusType)));
    }


    /**
     *  Gets an <code>AvailabilityEnablerList</code> containing the given
     *  availability enabler record <code>Type</code>. In plenary mode, the
     *  returned list contains all known availability enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  availability enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  availabilityEnablerRecordType an availability enabler record type 
     *  @return the returned <code>availabilityEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>availabilityEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerList getAvailabilityEnablersByRecordType(org.osid.type.Type availabilityEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.resourcing.rules.availabilityenabler.ArrayAvailabilityEnablerList(this.availabilityEnablersByRecord.get(availabilityEnablerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.availabilityEnablersByGenus.clear();
        this.availabilityEnablersByRecord.clear();

        super.close();

        return;
    }
}
