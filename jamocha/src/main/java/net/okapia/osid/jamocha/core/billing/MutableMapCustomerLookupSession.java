//
// MutableMapCustomerLookupSession
//
//    Implements a Customer lookup service backed by a collection of
//    customers that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.billing;


/**
 *  Implements a Customer lookup service backed by a collection of
 *  customers. The customers are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of customers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapCustomerLookupSession
    extends net.okapia.osid.jamocha.core.billing.spi.AbstractMapCustomerLookupSession
    implements org.osid.billing.CustomerLookupSession {


    /**
     *  Constructs a new {@code MutableMapCustomerLookupSession}
     *  with no customers.
     *
     *  @param business the business
     *  @throws org.osid.NullArgumentException {@code business} is
     *          {@code null}
     */

      public MutableMapCustomerLookupSession(org.osid.billing.Business business) {
        setBusiness(business);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapCustomerLookupSession} with a
     *  single customer.
     *
     *  @param business the business  
     *  @param customer a customer
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code customer} is {@code null}
     */

    public MutableMapCustomerLookupSession(org.osid.billing.Business business,
                                           org.osid.billing.Customer customer) {
        this(business);
        putCustomer(customer);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapCustomerLookupSession}
     *  using an array of customers.
     *
     *  @param business the business
     *  @param customers an array of customers
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code customers} is {@code null}
     */

    public MutableMapCustomerLookupSession(org.osid.billing.Business business,
                                           org.osid.billing.Customer[] customers) {
        this(business);
        putCustomers(customers);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapCustomerLookupSession}
     *  using a collection of customers.
     *
     *  @param business the business
     *  @param customers a collection of customers
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code customers} is {@code null}
     */

    public MutableMapCustomerLookupSession(org.osid.billing.Business business,
                                           java.util.Collection<? extends org.osid.billing.Customer> customers) {

        this(business);
        putCustomers(customers);
        return;
    }

    
    /**
     *  Makes a {@code Customer} available in this session.
     *
     *  @param customer a customer
     *  @throws org.osid.NullArgumentException {@code customer{@code  is
     *          {@code null}
     */

    @Override
    public void putCustomer(org.osid.billing.Customer customer) {
        super.putCustomer(customer);
        return;
    }


    /**
     *  Makes an array of customers available in this session.
     *
     *  @param customers an array of customers
     *  @throws org.osid.NullArgumentException {@code customers{@code 
     *          is {@code null}
     */

    @Override
    public void putCustomers(org.osid.billing.Customer[] customers) {
        super.putCustomers(customers);
        return;
    }


    /**
     *  Makes collection of customers available in this session.
     *
     *  @param customers a collection of customers
     *  @throws org.osid.NullArgumentException {@code customers{@code  is
     *          {@code null}
     */

    @Override
    public void putCustomers(java.util.Collection<? extends org.osid.billing.Customer> customers) {
        super.putCustomers(customers);
        return;
    }


    /**
     *  Removes a Customer from this session.
     *
     *  @param customerId the {@code Id} of the customer
     *  @throws org.osid.NullArgumentException {@code customerId{@code 
     *          is {@code null}
     */

    @Override
    public void removeCustomer(org.osid.id.Id customerId) {
        super.removeCustomer(customerId);
        return;
    }    
}
