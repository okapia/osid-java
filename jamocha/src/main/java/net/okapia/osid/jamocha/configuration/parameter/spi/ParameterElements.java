//
// ParameterElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.configuration.parameter.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class ParameterElements
    extends net.okapia.osid.jamocha.spi.OsidRuleElements {


    /**
     *  Gets the ParameterElement Id.
     *
     *  @return the parameter element Id
     */

    public static org.osid.id.Id getParameterEntityId() {
        return (makeEntityId("osid.configuration.Parameter"));
    }


    /**
     *  Gets the ValueSyntax element Id.
     *
     *  @return the ValueSyntax element Id
     */

    public static org.osid.id.Id getValueSyntax() {
        return (makeElementId("osid.configuration.parameter.ValueSyntax"));
    }


    /**
     *  Gets the ValueCoordinateType element Id.
     *
     *  @return the ValueCoordinateType element Id
     */

    public static org.osid.id.Id getValueCoordinateType() {
        return (makeElementId("osid.configuration.parameter.ValueCoordinateType"));
    }


    /**
     *  Gets the ValueHeadingType element Id.
     *
     *  @return the ValueHeadingType element Id
     */

    public static org.osid.id.Id getValueHeadingType() {
        return (makeElementId("osid.configuration.parameter.ValueHeadingType"));
    }


    /**
     *  Gets the ValueObjectType element Id.
     *
     *  @return the ValueObjectType element Id
     */

    public static org.osid.id.Id getValueObjectType() {
        return (makeElementId("osid.configuration.parameter.ValueObjectType"));
    }


    /**
     *  Gets the ValueSpatialUnitRecordType element Id.
     *
     *  @return the ValueSpatialUnitRecordType element Id
     */

    public static org.osid.id.Id getValueSpatialUnitRecordType() {
        return (makeElementId("osid.configuration.parameter.ValueSpatialUnitRecordType"));
    }


    /**
     *  Gets the ValueVersionScheme element Id.
     *
     *  @return the ValueVersionScheme element Id
     */

    public static org.osid.id.Id getValueVersionScheme() {
        return (makeElementId("osid.configuration.parameter.ValueVersionScheme"));
    }


    /**
     *  Gets the Value element Id.
     *
     *  @return the Value element Id
     */

    public static org.osid.id.Id getValue() {
        return (makeQueryElementId("osid.configuration.parameter.Value"));
    }


    /**
     *  Gets the ValuesShuffles element Id.
     *
     *  @return the ValuesShuffled element Id
     */

    public static org.osid.id.Id getValuesShuffled() {
        return (makeQueryElementId("osid.configuration.parameter.ValuesShuffled"));
    }


    /**
     *  Gets the ConfigurationId element Id.
     *
     *  @return the ConfigurationId element Id
     */

    public static org.osid.id.Id getConfigurationId() {
        return (makeQueryElementId("osid.configuration.parameter.ConfigurationId"));
    }


    /**
     *  Gets the Configuration element Id.
     *
     *  @return the Configuration element Id
     */

    public static org.osid.id.Id getConfiguration() {
        return (makeQueryElementId("osid.configuration.parameter.Configuration"));
    }
}
