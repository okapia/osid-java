//
// AbstractVaultQuery.java
//
//     A template for making a Vault Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authorization.vault.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for vaults.
 */

public abstract class AbstractVaultQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQuery
    implements org.osid.authorization.VaultQuery {

    private final java.util.Collection<org.osid.authorization.records.VaultQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the function <code> Id </code> for this query. 
     *
     *  @param  functionId a function <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> functionId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchFunctionId(org.osid.id.Id functionId, boolean match) {
        return;
    }


    /**
     *  Clears the function <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearFunctionIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> FunctionQuery </code> is available. 
     *
     *  @return <code> true </code> if a function query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFunctionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a function. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the function query 
     *  @throws org.osid.UnimplementedException <code> supportsFunctionQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.FunctionQuery getFunctionQuery() {
        throw new org.osid.UnimplementedException("supportsFunctionQuery() is false");
    }


    /**
     *  Matches vaults that have any function. 
     *
     *  @param  match <code> true </code> to match vaults with any function 
     *          mapping, <code> false </code> to match vaults with no function 
     *          mapping 
     */

    @OSID @Override
    public void matchAnyFunction(boolean match) {
        return;
    }


    /**
     *  Clears the function query terms. 
     */

    @OSID @Override
    public void clearFunctionTerms() {
        return;
    }


    /**
     *  Sets the qualifier <code> Id </code> for this query. 
     *
     *  @param  qualifierId a qualifier <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> qualifierId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchQualifierId(org.osid.id.Id qualifierId, boolean match) {
        return;
    }


    /**
     *  Clears the qualifier <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearQualifierIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> QualifierQuery </code> is available. 
     *
     *  @return <code> true </code> if a qualifier query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQualifierQuery() {
        return (false);
    }


    /**
     *  Gets the query for a qualifier. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the qualifier query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierQuery getQualifierQuery() {
        throw new org.osid.UnimplementedException("supportsQualifierQuery() is false");
    }


    /**
     *  Matches vaults that have any qualifier. 
     *
     *  @param  match <code> true </code> to match vaults with any qualifier 
     *          mapping, <code> false </code> to match vaults with no 
     *          qualifier mapping 
     */

    @OSID @Override
    public void matchAnyQualifier(boolean match) {
        return;
    }


    /**
     *  Clears the qualifier query terms. 
     */

    @OSID @Override
    public void clearQualifierTerms() {
        return;
    }


    /**
     *  Sets the authorization <code> Id </code> for this query. 
     *
     *  @param  authorizationId an authorization <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> authorizationId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchAuthorizationId(org.osid.id.Id authorizationId, 
                                     boolean match) {
        return;
    }


    /**
     *  Clears the authorization <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAuthorizationIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AuthorizationQuery </code> is available. 
     *
     *  @return <code> true </code> if an authorization query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthorizationQuery() {
        return (false);
    }


    /**
     *  Gets the query for an authorization. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the authorization query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationQuery getAuthorizationQuery() {
        throw new org.osid.UnimplementedException("supportsAuthorizationQuery() is false");
    }


    /**
     *  Matches vaults that have any authorization. 
     *
     *  @param  match <code> true </code> to match vaults with any 
     *          authorization mapping, <code> false </code> to match vaults 
     *          with no authorization mapping 
     */

    @OSID @Override
    public void matchAnyAuthorization(boolean match) {
        return;
    }


    /**
     *  Clears the authorization query terms. 
     */

    @OSID @Override
    public void clearAuthorizationTerms() {
        return;
    }


    /**
     *  Sets the vault <code> Id </code> for this query to match vaults that 
     *  have the specified vault as an ancestor. 
     *
     *  @param  vaultId a vault <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorVaultId(org.osid.id.Id vaultId, boolean match) {
        return;
    }


    /**
     *  Clears the ancestor vault <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorVaultIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> VaultQuery </code> is available. 
     *
     *  @return <code> true </code> if a vault query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorVaultQuery() {
        return (false);
    }


    /**
     *  Gets the query for a vault. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the vault query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorVaultQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.VaultQuery getAncestorVaultQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorVaultQuery() is false");
    }


    /**
     *  Matches vaults that have any ancestor. 
     *
     *  @param  match <code> true </code> to match vaults with any ancestor, 
     *          <code> false </code> to match root vaults 
     */

    @OSID @Override
    public void matchAnyAncestorVault(boolean match) {
        return;
    }


    /**
     *  Clears the ancestor vault query terms. 
     */

    @OSID @Override
    public void clearAncestorVaultTerms() {
        return;
    }


    /**
     *  Sets the vault <code> Id </code> for this query to match vaults that 
     *  have the specified vault as a descendant. 
     *
     *  @param  vaultId a vault <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantVaultId(org.osid.id.Id vaultId, boolean match) {
        return;
    }


    /**
     *  Clears the descendant vault <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantVaultIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> VaultQuery </code> is available. 
     *
     *  @return <code> true </code> if a vault query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantVaultQuery() {
        return (false);
    }


    /**
     *  Gets the query for a vault. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the vault query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantVaultQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.VaultQuery getDescendantVaultQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantVaultQuery() is false");
    }


    /**
     *  Matches vaults that have any descendant. 
     *
     *  @param  match <code> true </code> to match vaults with any Ddscendant, 
     *          <code> false </code> to match leaf vaults 
     */

    @OSID @Override
    public void matchAnyDescendantVault(boolean match) {
        return;
    }


    /**
     *  Clears the descendant vault query terms. 
     */

    @OSID @Override
    public void clearDescendantVaultTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given vault query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a vault implementing the requested record.
     *
     *  @param vaultRecordType a vault record type
     *  @return the vault query record
     *  @throws org.osid.NullArgumentException
     *          <code>vaultRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(vaultRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.authorization.records.VaultQueryRecord getVaultQueryRecord(org.osid.type.Type vaultRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.authorization.records.VaultQueryRecord record : this.records) {
            if (record.implementsRecordType(vaultRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(vaultRecordType + " is not supported");
    }


    /**
     *  Adds a record to this vault query. 
     *
     *  @param vaultQueryRecord vault query record
     *  @param vaultRecordType vault record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addVaultQueryRecord(org.osid.authorization.records.VaultQueryRecord vaultQueryRecord, 
                                          org.osid.type.Type vaultRecordType) {

        addRecordType(vaultRecordType);
        nullarg(vaultQueryRecord, "vault query record");
        this.records.add(vaultQueryRecord);        
        return;
    }
}
