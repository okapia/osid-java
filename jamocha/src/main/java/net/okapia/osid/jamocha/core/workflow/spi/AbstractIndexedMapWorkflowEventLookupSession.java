//
// AbstractIndexedMapWorkflowEventLookupSession.java
//
//    A simple framework for providing a WorkflowEvent lookup service
//    backed by a fixed collection of workflow events with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.workflow.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a WorkflowEvent lookup service backed by a
 *  fixed collection of workflow events. The workflow events are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some workflow events may be compatible
 *  with more types than are indicated through these workflow event
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>WorkflowEvents</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapWorkflowEventLookupSession
    extends AbstractMapWorkflowEventLookupSession
    implements org.osid.workflow.WorkflowEventLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.workflow.WorkflowEvent> workflowEventsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.workflow.WorkflowEvent>());
    private final MultiMap<org.osid.type.Type, org.osid.workflow.WorkflowEvent> workflowEventsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.workflow.WorkflowEvent>());


    /**
     *  Makes a <code>WorkflowEvent</code> available in this session.
     *
     *  @param  workflowEvent a workflow event
     *  @throws org.osid.NullArgumentException <code>workflowEvent<code> is
     *          <code>null</code>
     */

    @Override
    protected void putWorkflowEvent(org.osid.workflow.WorkflowEvent workflowEvent) {
        super.putWorkflowEvent(workflowEvent);

        this.workflowEventsByGenus.put(workflowEvent.getGenusType(), workflowEvent);
        
        try (org.osid.type.TypeList types = workflowEvent.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.workflowEventsByRecord.put(types.getNextType(), workflowEvent);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a workflow event from this session.
     *
     *  @param workflowEventId the <code>Id</code> of the workflow event
     *  @throws org.osid.NullArgumentException <code>workflowEventId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeWorkflowEvent(org.osid.id.Id workflowEventId) {
        org.osid.workflow.WorkflowEvent workflowEvent;
        try {
            workflowEvent = getWorkflowEvent(workflowEventId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.workflowEventsByGenus.remove(workflowEvent.getGenusType());

        try (org.osid.type.TypeList types = workflowEvent.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.workflowEventsByRecord.remove(types.getNextType(), workflowEvent);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeWorkflowEvent(workflowEventId);
        return;
    }


    /**
     *  Gets a <code>WorkflowEventList</code> corresponding to the given
     *  workflow event genus <code>Type</code> which does not include
     *  workflow events of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known workflow events or an error results. Otherwise,
     *  the returned list may contain only those workflow events that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  workflowEventGenusType a workflow event genus type 
     *  @return the returned <code>WorkflowEvent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>workflowEventGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsByGenusType(org.osid.type.Type workflowEventGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.workflow.workflowevent.ArrayWorkflowEventList(this.workflowEventsByGenus.get(workflowEventGenusType)));
    }


    /**
     *  Gets a <code>WorkflowEventList</code> containing the given
     *  workflow event record <code>Type</code>. In plenary mode, the
     *  returned list contains all known workflow events or an error
     *  results. Otherwise, the returned list may contain only those
     *  workflow events that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  workflowEventRecordType a workflow event record type 
     *  @return the returned <code>workflowEvent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>workflowEventRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsByRecordType(org.osid.type.Type workflowEventRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.workflow.workflowevent.ArrayWorkflowEventList(this.workflowEventsByRecord.get(workflowEventRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.workflowEventsByGenus.clear();
        this.workflowEventsByRecord.clear();

        super.close();

        return;
    }
}
