//
// AbstractPackage.java
//
//     Defines a Package builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.installation.pkg.spi;


/**
 *  Defines a <code>Package</code> builder.
 */

public abstract class AbstractPackageBuilder<T extends AbstractPackageBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractSourceableOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.installation.pkg.PackageMiter pkg;


    /**
     *  Constructs a new <code>AbstractPackageBuilder</code>.
     *
     *  @param pkg the package to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractPackageBuilder(net.okapia.osid.jamocha.builder.installation.pkg.PackageMiter pkg) {
        super(pkg);
        this.pkg = pkg;
        return;
    }


    /**
     *  Builds the package.
     *
     *  @return the new package
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.installation.Package build() {
        (new net.okapia.osid.jamocha.builder.validator.installation.pkg.PackageValidator(getValidations())).validate(this.pkg);
        return (new net.okapia.osid.jamocha.builder.installation.pkg.ImmutablePackage(this.pkg));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the pkg miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.installation.pkg.PackageMiter getMiter() {
        return (this.pkg);
    }


    /**
     *  Sets the version.
     *
     *  @param version a version
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>version</code> is
     *          <code>null</code>
     */

    public T version(org.osid.installation.Version version) {
        getMiter().setVersion(version);
        return (self());
    }


    /**
     *  Sets the copyright.
     *
     *  @param copyright a copyright
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>copyright</code>
     *          is <code>null</code>
     */

    public T copyright(org.osid.locale.DisplayText copyright) {
        getMiter().setCopyright(copyright);
        return (self());
    }


    /**
     *  Sets the creator.
     *
     *  @param creator a creator
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>creator</code> is
     *          <code>null</code>
     */

    public T creator(org.osid.resource.Resource creator) {
        getMiter().setCreator(creator);
        return (self());
    }


    /**
     *  Sets the release date.
     *
     *  @param date a release date
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public T releaseDate(org.osid.calendaring.DateTime date) {
        getMiter().setReleaseDate(date);
        return (self());
    }



    /**
     *  Adds a dependency.
     *
     *  @param pkg a dependency
     *  @throws org.osid.NullArgumentException <code>pkg</code> is
     *          <code>null</code>
     */

    public T dependency(org.osid.installation.Package pkg) {
        getMiter().addDependency(pkg);
        return (self());
    }


    /**
     *  Sets all the package dependencies.
     *
     *  @param packages a collection of packages
     *  @throws org.osid.NullArgumentException <code>packages</code>
     *          is <code>null</code>
     */

    public T dependencies(java.util.Collection<org.osid.installation.Package> packages) {
        getMiter().setDependencies(packages);
        return (self());
    }


    /**
     *  Sets the url.
     *
     *  @param url a url
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>url</code> is
     *          <code>null</code>
     */

    public T url(String url) {
        getMiter().setURL(url);
        return (self());
    }



    /**
     *  Adds an installation content.
     *
     *  @param content an installation content
     *  @throws org.osid.NullArgumentException <code>content</code> is
     *          <code>null</code>
     */

    public T installationContent(org.osid.installation.InstallationContent content) {
        getMiter().addInstallationContent(content);        
        return (self());
    }


    /**
     *  Sets all the installation contents.
     *
     *  @param contents a collection of contents
     *  @throws org.osid.NullArgumentException <code>contents</code>
     *          is <code>null</code>
     */

    public T installationContents(java.util.Collection<org.osid.installation.InstallationContent> contents) {
        getMiter().setInstallationContents(contents);
        return (self());
    }


    /**
     *  Adds a Package record.
     *
     *  @param record a package record
     *  @param recordType the type of package record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.installation.records.PackageRecord record, org.osid.type.Type recordType) {
        getMiter().addPackageRecord(record, recordType);
        return (self());
    }
}       


