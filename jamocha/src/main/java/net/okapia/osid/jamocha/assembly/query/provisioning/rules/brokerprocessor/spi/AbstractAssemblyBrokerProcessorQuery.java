//
// AbstractAssemblyBrokerProcessorQuery.java
//
//     A BrokerProcessorQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.provisioning.rules.brokerprocessor.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A BrokerProcessorQuery that stores terms.
 */

public abstract class AbstractAssemblyBrokerProcessorQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidProcessorQuery
    implements org.osid.provisioning.rules.BrokerProcessorQuery,
               org.osid.provisioning.rules.BrokerProcessorQueryInspector,
               org.osid.provisioning.rules.BrokerProcessorSearchOrder {

    private final java.util.Collection<org.osid.provisioning.rules.records.BrokerProcessorQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.provisioning.rules.records.BrokerProcessorQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.provisioning.rules.records.BrokerProcessorSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyBrokerProcessorQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyBrokerProcessorQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches brokers that issue leases. 
     *
     *  @param  match <code> true </code> to match brokers that issue leases, 
     *          <code> false </code> to match brokers that issue permanent 
     *          provisions 
     */

    @OSID @Override
    public void matchLeasing(boolean match) {
        getAssembler().addBooleanTerm(getLeasingColumn(), match);
        return;
    }


    /**
     *  Clears the leasing query terms. 
     */

    @OSID @Override
    public void clearLeasingTerms() {
        getAssembler().clearTerms(getLeasingColumn());
        return;
    }


    /**
     *  Gets the leasing query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getLeasingTerms() {
        return (getAssembler().getBooleanTerms(getLeasingColumn()));
    }


    /**
     *  Orders the results by the leasing flag. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByLeasing(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getLeasingColumn(), style);
        return;
    }


    /**
     *  Gets the Leasing column name.
     *
     * @return the column name
     */

    protected String getLeasingColumn() {
        return ("leasing");
    }


    /**
     *  Matches brokers that issue fixed duration leases between the given 
     *  durations inclusive. 
     *
     *  @param  from starting duration range 
     *  @param  to ending duration range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchFixedLeaseDuration(org.osid.calendaring.Duration from, 
                                        org.osid.calendaring.Duration to, 
                                        boolean match) {
        getAssembler().addDurationRangeTerm(getFixedLeaseDurationColumn(), from, to, match);
        return;
    }


    /**
     *  Matches brokers with any fixed lease duration. 
     *
     *  @param  match <code> true </code> to match brokers with any fixed 
     *          lease duration, returns, <code> false </code> otherwise 
     */

    @OSID @Override
    public void matchAnyFixedLeaseDuration(boolean match) {
        getAssembler().addDurationRangeWildcardTerm(getFixedLeaseDurationColumn(), match);
        return;
    }


    /**
     *  Clears the fixed lease duration query terms. 
     */

    @OSID @Override
    public void clearFixedLeaseDurationTerms() {
        getAssembler().clearTerms(getFixedLeaseDurationColumn());
        return;
    }


    /**
     *  Gets the fixed lease duration query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getFixedLeaseDurationTerms() {
        return (getAssembler().getDurationRangeTerms(getFixedLeaseDurationColumn()));
    }


    /**
     *  Orders the results by the fixed lease duration. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByFixedLeaseDuration(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getFixedLeaseDurationColumn(), style);
        return;
    }


    /**
     *  Gets the FixedLeaseDuration column name.
     *
     * @return the column name
     */

    protected String getFixedLeaseDurationColumn() {
        return ("fixed_lease_duration");
    }


    /**
     *  Matches brokers that require provisions to be returned. 
     *
     *  @param  match <code> true </code> to match brokers that require 
     *          provision returns,, <code> false </code> otherwise 
     */

    @OSID @Override
    public void matchMustReturnProvisions(boolean match) {
        getAssembler().addBooleanTerm(getMustReturnProvisionsColumn(), match);
        return;
    }


    /**
     *  Clears the must return provisions query terms. 
     */

    @OSID @Override
    public void clearMustReturnProvisionsTerms() {
        getAssembler().clearTerms(getMustReturnProvisionsColumn());
        return;
    }


    /**
     *  Gets the must return provisions query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getMustReturnProvisionsTerms() {
        return (getAssembler().getBooleanTerms(getMustReturnProvisionsColumn()));
    }


    /**
     *  Orders the results by the must return provisions flag. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByMustReturnProvisions(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getMustReturnProvisionsColumn(), style);
        return;
    }


    /**
     *  Gets the MustReturnProvisions column name.
     *
     * @return the column name
     */

    protected String getMustReturnProvisionsColumn() {
        return ("must_return_provisions");
    }


    /**
     *  Matches brokers that allow provision exchange. 
     *
     *  @param  match <code> true </code> to match brokers that permit 
     *          provision exchange, <code> false </code> otherwise 
     */

    @OSID @Override
    public void matchAllowsProvisionExchange(boolean match) {
        getAssembler().addBooleanTerm(getAllowsProvisionExchangeColumn(), match);
        return;
    }


    /**
     *  Clears the allows provision exchange query terms. 
     */

    @OSID @Override
    public void clearAllowsProvisionExchangeTerms() {
        getAssembler().clearTerms(getAllowsProvisionExchangeColumn());
        return;
    }


    /**
     *  Gets the allows provision exchange query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getAllowsProvisionExchangeTerms() {
        return (getAssembler().getBooleanTerms(getAllowsProvisionExchangeColumn()));
    }


    /**
     *  Orders the results by the allows provision exchange flag. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAllowsProvisionExchange(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getAllowsProvisionExchangeColumn(), style);
        return;
    }


    /**
     *  Gets the AllowsProvisionExchange column name.
     *
     * @return the column name
     */

    protected String getAllowsProvisionExchangeColumn() {
        return ("allows_provision_exchange");
    }


    /**
     *  Matches brokers that allow comound requests. 
     *
     *  @param  match <code> true </code> to match brokers that permit 
     *          compound requests, <code> false </code> otherwise 
     */

    @OSID @Override
    public void matchAllowsCompoundRequests(boolean match) {
        getAssembler().addBooleanTerm(getAllowsCompoundRequestsColumn(), match);
        return;
    }


    /**
     *  Clears the allows compound requests query terms. 
     */

    @OSID @Override
    public void clearAllowsCompoundRequestsTerms() {
        getAssembler().clearTerms(getAllowsCompoundRequestsColumn());
        return;
    }


    /**
     *  Gets the allows compound requests query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getAllowsCompoundRequestsTerms() {
        return (getAssembler().getBooleanTerms(getAllowsCompoundRequestsColumn()));
    }


    /**
     *  Orders the results by the allows compound requests flag. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAllowsCompoundRequests(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getAllowsCompoundRequestsColumn(), style);
        return;
    }


    /**
     *  Gets the AllowsCompoundRequests column name.
     *
     * @return the column name
     */

    protected String getAllowsCompoundRequestsColumn() {
        return ("allows_compound_requests");
    }


    /**
     *  Matches mapped to the broker. 
     *
     *  @param  distributorId the broker <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledBrokerId(org.osid.id.Id distributorId, boolean match) {
        getAssembler().addIdTerm(getRuledBrokerIdColumn(), distributorId, match);
        return;
    }


    /**
     *  Clears the broker <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledBrokerIdTerms() {
        getAssembler().clearTerms(getRuledBrokerIdColumn());
        return;
    }


    /**
     *  Gets the broker <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledBrokerIdTerms() {
        return (getAssembler().getIdTerms(getRuledBrokerIdColumn()));
    }


    /**
     *  Gets the RuledBrokerId column name.
     *
     * @return the column name
     */

    protected String getRuledBrokerIdColumn() {
        return ("ruled_broker_id");
    }


    /**
     *  Tests if a <code> BrokerQuery </code> is available. 
     *
     *  @return <code> true </code> if a broker query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledBrokerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a broker. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the broker query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledBrokerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerQuery getRuledBrokerQuery() {
        throw new org.osid.UnimplementedException("supportsRuledBrokerQuery() is false");
    }


    /**
     *  Matches mapped to any broker. 
     *
     *  @param  match <code> true </code> for mapped to any broker, <code> 
     *          false </code> to match mapped to no broker 
     */

    @OSID @Override
    public void matchAnyRuledBroker(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledBrokerColumn(), match);
        return;
    }


    /**
     *  Clears the broker query terms. 
     */

    @OSID @Override
    public void clearRuledBrokerTerms() {
        getAssembler().clearTerms(getRuledBrokerColumn());
        return;
    }


    /**
     *  Gets the broker query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerQueryInspector[] getRuledBrokerTerms() {
        return (new org.osid.provisioning.BrokerQueryInspector[0]);
    }


    /**
     *  Gets the RuledBroker column name.
     *
     * @return the column name
     */

    protected String getRuledBrokerColumn() {
        return ("ruled_broker");
    }


    /**
     *  Matches mapped to the distributor. 
     *
     *  @param  distributorId the distributor <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDistributorId(org.osid.id.Id distributorId, boolean match) {
        getAssembler().addIdTerm(getDistributorIdColumn(), distributorId, match);
        return;
    }


    /**
     *  Clears the distributor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDistributorIdTerms() {
        getAssembler().clearTerms(getDistributorIdColumn());
        return;
    }


    /**
     *  Gets the distributor <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDistributorIdTerms() {
        return (getAssembler().getIdTerms(getDistributorIdColumn()));
    }


    /**
     *  Gets the DistributorId column name.
     *
     * @return the column name
     */

    protected String getDistributorIdColumn() {
        return ("distributor_id");
    }


    /**
     *  Tests if a <code> DistributorQuery </code> is available. 
     *
     *  @return <code> true </code> if a distributor query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDistributorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a distributor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the distributor query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDistributorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQuery getDistributorQuery() {
        throw new org.osid.UnimplementedException("supportsDistributorQuery() is false");
    }


    /**
     *  Clears the distributor query terms. 
     */

    @OSID @Override
    public void clearDistributorTerms() {
        getAssembler().clearTerms(getDistributorColumn());
        return;
    }


    /**
     *  Gets the distributor query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQueryInspector[] getDistributorTerms() {
        return (new org.osid.provisioning.DistributorQueryInspector[0]);
    }


    /**
     *  Gets the Distributor column name.
     *
     * @return the column name
     */

    protected String getDistributorColumn() {
        return ("distributor");
    }


    /**
     *  Tests if this brokerProcessor supports the given record
     *  <code>Type</code>.
     *
     *  @param  brokerProcessorRecordType a broker processor record type 
     *  @return <code>true</code> if the brokerProcessorRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>brokerProcessorRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type brokerProcessorRecordType) {
        for (org.osid.provisioning.rules.records.BrokerProcessorQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(brokerProcessorRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  brokerProcessorRecordType the broker processor record type 
     *  @return the broker processor query record 
     *  @throws org.osid.NullArgumentException
     *          <code>brokerProcessorRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(brokerProcessorRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.BrokerProcessorQueryRecord getBrokerProcessorQueryRecord(org.osid.type.Type brokerProcessorRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.rules.records.BrokerProcessorQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(brokerProcessorRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(brokerProcessorRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  brokerProcessorRecordType the broker processor record type 
     *  @return the broker processor query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>brokerProcessorRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(brokerProcessorRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.BrokerProcessorQueryInspectorRecord getBrokerProcessorQueryInspectorRecord(org.osid.type.Type brokerProcessorRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.rules.records.BrokerProcessorQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(brokerProcessorRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(brokerProcessorRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param brokerProcessorRecordType the broker processor record type
     *  @return the broker processor search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>brokerProcessorRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(brokerProcessorRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.BrokerProcessorSearchOrderRecord getBrokerProcessorSearchOrderRecord(org.osid.type.Type brokerProcessorRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.rules.records.BrokerProcessorSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(brokerProcessorRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(brokerProcessorRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this broker processor. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param brokerProcessorQueryRecord the broker processor query record
     *  @param brokerProcessorQueryInspectorRecord the broker processor query inspector
     *         record
     *  @param brokerProcessorSearchOrderRecord the broker processor search order record
     *  @param brokerProcessorRecordType broker processor record type
     *  @throws org.osid.NullArgumentException
     *          <code>brokerProcessorQueryRecord</code>,
     *          <code>brokerProcessorQueryInspectorRecord</code>,
     *          <code>brokerProcessorSearchOrderRecord</code> or
     *          <code>brokerProcessorRecordTypebrokerProcessor</code> is
     *          <code>null</code>
     */
            
    protected void addBrokerProcessorRecords(org.osid.provisioning.rules.records.BrokerProcessorQueryRecord brokerProcessorQueryRecord, 
                                      org.osid.provisioning.rules.records.BrokerProcessorQueryInspectorRecord brokerProcessorQueryInspectorRecord, 
                                      org.osid.provisioning.rules.records.BrokerProcessorSearchOrderRecord brokerProcessorSearchOrderRecord, 
                                      org.osid.type.Type brokerProcessorRecordType) {

        addRecordType(brokerProcessorRecordType);

        nullarg(brokerProcessorQueryRecord, "broker processor query record");
        nullarg(brokerProcessorQueryInspectorRecord, "broker processor query inspector record");
        nullarg(brokerProcessorSearchOrderRecord, "broker processor search odrer record");

        this.queryRecords.add(brokerProcessorQueryRecord);
        this.queryInspectorRecords.add(brokerProcessorQueryInspectorRecord);
        this.searchOrderRecords.add(brokerProcessorSearchOrderRecord);
        
        return;
    }
}
