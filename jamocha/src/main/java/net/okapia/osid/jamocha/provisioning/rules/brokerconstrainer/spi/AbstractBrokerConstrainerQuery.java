//
// AbstractBrokerConstrainerQuery.java
//
//     A template for making a BrokerConstrainer Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.rules.brokerconstrainer.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for broker constrainers.
 */

public abstract class AbstractBrokerConstrainerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidConstrainerQuery
    implements org.osid.provisioning.rules.BrokerConstrainerQuery {

    private final java.util.Collection<org.osid.provisioning.rules.records.BrokerConstrainerQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches mapped to a broker. 
     *
     *  @param  distributorId the broker <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledBrokerId(org.osid.id.Id distributorId, boolean match) {
        return;
    }


    /**
     *  Clears the broker <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledBrokerIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BrokerQuery </code> is available. 
     *
     *  @return <code> true </code> if a broker query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledBrokerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a broker. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the broker query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledBrokerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerQuery getRuledBrokerQuery() {
        throw new org.osid.UnimplementedException("supportsRuledBrokerQuery() is false");
    }


    /**
     *  Matches mapped to any broker. 
     *
     *  @param  match <code> true </code> for mapped to any broker, <code> 
     *          false </code> to match mapped to no brokers 
     */

    @OSID @Override
    public void matchAnyRuledBroker(boolean match) {
        return;
    }


    /**
     *  Clears the broker query terms. 
     */

    @OSID @Override
    public void clearRuledBrokerTerms() {
        return;
    }


    /**
     *  Matches mapped to the distributor. 
     *
     *  @param  distributorId the distributor <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDistributorId(org.osid.id.Id distributorId, boolean match) {
        return;
    }


    /**
     *  Clears the distributor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDistributorIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> DistributorQuery </code> is available. 
     *
     *  @return <code> true </code> if a distributor query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDistributorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a distributor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the distributor query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDistributorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQuery getDistributorQuery() {
        throw new org.osid.UnimplementedException("supportsDistributorQuery() is false");
    }


    /**
     *  Clears the distributor query terms. 
     */

    @OSID @Override
    public void clearDistributorTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given broker constrainer query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a broker constrainer implementing the requested record.
     *
     *  @param brokerConstrainerRecordType a broker constrainer record type
     *  @return the broker constrainer query record
     *  @throws org.osid.NullArgumentException
     *          <code>brokerConstrainerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(brokerConstrainerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.BrokerConstrainerQueryRecord getBrokerConstrainerQueryRecord(org.osid.type.Type brokerConstrainerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.rules.records.BrokerConstrainerQueryRecord record : this.records) {
            if (record.implementsRecordType(brokerConstrainerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(brokerConstrainerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this broker constrainer query. 
     *
     *  @param brokerConstrainerQueryRecord broker constrainer query record
     *  @param brokerConstrainerRecordType brokerConstrainer record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addBrokerConstrainerQueryRecord(org.osid.provisioning.rules.records.BrokerConstrainerQueryRecord brokerConstrainerQueryRecord, 
                                          org.osid.type.Type brokerConstrainerRecordType) {

        addRecordType(brokerConstrainerRecordType);
        nullarg(brokerConstrainerQueryRecord, "broker constrainer query record");
        this.records.add(brokerConstrainerQueryRecord);        
        return;
    }
}
