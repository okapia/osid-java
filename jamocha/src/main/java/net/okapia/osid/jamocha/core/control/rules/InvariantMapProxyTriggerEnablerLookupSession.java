//
// InvariantMapProxyTriggerEnablerLookupSession
//
//    Implements a TriggerEnabler lookup service backed by a fixed
//    collection of triggerEnablers. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.control.rules;


/**
 *  Implements a TriggerEnabler lookup service backed by a fixed
 *  collection of trigger enablers. The trigger enablers are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyTriggerEnablerLookupSession
    extends net.okapia.osid.jamocha.core.control.rules.spi.AbstractMapTriggerEnablerLookupSession
    implements org.osid.control.rules.TriggerEnablerLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyTriggerEnablerLookupSession} with no
     *  trigger enablers.
     *
     *  @param system the system
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code system} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyTriggerEnablerLookupSession(org.osid.control.System system,
                                                  org.osid.proxy.Proxy proxy) {
        setSystem(system);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyTriggerEnablerLookupSession} with a single
     *  trigger enabler.
     *
     *  @param system the system
     *  @param triggerEnabler a single trigger enabler
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code system},
     *          {@code triggerEnabler} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyTriggerEnablerLookupSession(org.osid.control.System system,
                                                  org.osid.control.rules.TriggerEnabler triggerEnabler, org.osid.proxy.Proxy proxy) {

        this(system, proxy);
        putTriggerEnabler(triggerEnabler);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyTriggerEnablerLookupSession} using
     *  an array of trigger enablers.
     *
     *  @param system the system
     *  @param triggerEnablers an array of trigger enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code system},
     *          {@code triggerEnablers} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyTriggerEnablerLookupSession(org.osid.control.System system,
                                                  org.osid.control.rules.TriggerEnabler[] triggerEnablers, org.osid.proxy.Proxy proxy) {

        this(system, proxy);
        putTriggerEnablers(triggerEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyTriggerEnablerLookupSession} using a
     *  collection of trigger enablers.
     *
     *  @param system the system
     *  @param triggerEnablers a collection of trigger enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code system},
     *          {@code triggerEnablers} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyTriggerEnablerLookupSession(org.osid.control.System system,
                                                  java.util.Collection<? extends org.osid.control.rules.TriggerEnabler> triggerEnablers,
                                                  org.osid.proxy.Proxy proxy) {

        this(system, proxy);
        putTriggerEnablers(triggerEnablers);
        return;
    }
}
