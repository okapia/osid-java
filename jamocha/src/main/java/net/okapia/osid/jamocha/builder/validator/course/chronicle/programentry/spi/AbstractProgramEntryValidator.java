//
// AbstractProgramEntryValidator.java
//
//     Validates a ProgramEntry.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.course.chronicle.programentry.spi;


/**
 *  Validates a ProgramEntry.
 */

public abstract class AbstractProgramEntryValidator
    extends net.okapia.osid.jamocha.builder.validator.spi.AbstractOsidRelationshipValidator {


    /**
     *  Constructs a new <code>AbstractProgramEntryValidator</code>.
     */

    protected AbstractProgramEntryValidator() {
        return;
    }


    /**
     *  Constructs a new <code>AbstractProgramEntryValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    protected AbstractProgramEntryValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates a ProgramEntry.
     *
     *  @param programEntry a program entry to validate
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>programEntry</code>
     *          is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in
     *          assembly
     */
    
    public void validate(org.osid.course.chronicle.ProgramEntry programEntry) {
        super.validate(programEntry);

        testNestedObject(programEntry, "getStudent");
        testNestedObject(programEntry, "getProgram");
        test(programEntry.getAdmissionDate(), "getAdmissionDate()");

        testConditionalMethod(programEntry, "getTermId", programEntry.isForTerm(), "isForTerm()");
        testConditionalMethod(programEntry, "getTerm", programEntry.isForTerm(), "isForTerm()");
        if (programEntry.isForTerm()) {
            testNestedObject(programEntry, "getTerm");
        }

        testNestedObject(programEntry, "getCreditScale");
        test(programEntry.getCreditsEarned(), "getCreditsEarned()");

        testConditionalMethod(programEntry, "getGPAScaleId", programEntry.hasGPA(), "hasGPA()");
        testConditionalMethod(programEntry, "getGPAScale", programEntry.hasGPA(), "hasGPA()");
        testConditionalMethod(programEntry, "getGPA", programEntry.hasGPA(), "hasGPA()");
        if (programEntry.hasGPA()) {
            testNestedObject(programEntry, "getGPAScale");
        }

        testConditionalMethod(programEntry, "getEnrollmentIds", programEntry.hasEnrollments(), "hasEnrollments()");
        testConditionalMethod(programEntry, "getEnrollments", programEntry.hasEnrollments(), "hasEnrollments()");
        if (programEntry.hasEnrollments()) {
            testNestedObjects(programEntry, "getEnrollmentIds", "getEnrollments");
        }

        return;
    }
}
