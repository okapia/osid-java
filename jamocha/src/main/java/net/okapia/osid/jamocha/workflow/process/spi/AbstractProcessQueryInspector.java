//
// AbstractProcessQueryInspector.java
//
//     A template for making a ProcessQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.workflow.process.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for processes.
 */

public abstract class AbstractProcessQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidGovernatorQueryInspector
    implements org.osid.workflow.ProcessQueryInspector {

    private final java.util.Collection<org.osid.workflow.records.ProcessQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the enabled query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getEnabledTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the initial step <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getInitialStepIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the initial step query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.workflow.StepQueryInspector[] getInitialStepTerms() {
        return (new org.osid.workflow.StepQueryInspector[0]);
    }


    /**
     *  Gets the initial state <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getInitialStateIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the initial state query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.process.StateQueryInspector[] getInitialStateTerms() {
        return (new org.osid.process.StateQueryInspector[0]);
    }


    /**
     *  Gets the step <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getStepIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the step query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.workflow.StepQueryInspector[] getStepTerms() {
        return (new org.osid.workflow.StepQueryInspector[0]);
    }


    /**
     *  Gets the work <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getWorkIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the work query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.workflow.WorkQueryInspector[] getWorkTerms() {
        return (new org.osid.workflow.WorkQueryInspector[0]);
    }


    /**
     *  Gets the office <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOfficeIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the office query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.workflow.OfficeQueryInspector[] getOfficeTerms() {
        return (new org.osid.workflow.OfficeQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given process query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a process implementing the requested record.
     *
     *  @param processRecordType a process record type
     *  @return the process query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>processRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(processRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.workflow.records.ProcessQueryInspectorRecord getProcessQueryInspectorRecord(org.osid.type.Type processRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.workflow.records.ProcessQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(processRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(processRecordType + " is not supported");
    }


    /**
     *  Adds a record to this process query. 
     *
     *  @param processQueryInspectorRecord process query inspector
     *         record
     *  @param processRecordType process record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addProcessQueryInspectorRecord(org.osid.workflow.records.ProcessQueryInspectorRecord processQueryInspectorRecord, 
                                                   org.osid.type.Type processRecordType) {

        addRecordType(processRecordType);
        nullarg(processRecordType, "process record type");
        this.records.add(processQueryInspectorRecord);        
        return;
    }
}
