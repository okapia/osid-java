//
// AbstractMutableResponse.java
//
//     Defines a mutable Response.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.assessment.response.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>Response</code>.
 */

public abstract class AbstractMutableResponse
    extends net.okapia.osid.jamocha.assessment.response.spi.AbstractResponse
    implements org.osid.assessment.Response,
               net.okapia.osid.jamocha.builder.assessment.response.ResponseMiter {


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this response. 
     *
     *  @param record response record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addResponseRecord(org.osid.assessment.records.ResponseRecord record, org.osid.type.Type recordType) {
        super.addResponseRecord(record, recordType);     
        return;
    }


    /**
     *  Sets the item.
     *
     *  @param item the item
     *  @throws org.osid.NullArgumentException <code>item</code> is
     *          <code>null</code>
     */

    @Override
    public void setItem(org.osid.assessment.Item item) {
        super.setItem(item);
        return;
    }
}

