//
// AbstractQueryBlockLookupSession.java
//
//    An inline adapter that maps a BlockLookupSession to
//    a BlockQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.hold.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a BlockLookupSession to
 *  a BlockQuerySession.
 */

public abstract class AbstractQueryBlockLookupSession
    extends net.okapia.osid.jamocha.hold.spi.AbstractBlockLookupSession
    implements org.osid.hold.BlockLookupSession {

    private final org.osid.hold.BlockQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryBlockLookupSession.
     *
     *  @param querySession the underlying block query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryBlockLookupSession(org.osid.hold.BlockQuerySession querySession) {
        nullarg(querySession, "block query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Oubliette</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Oubliette Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getOublietteId() {
        return (this.session.getOublietteId());
    }


    /**
     *  Gets the <code>Oubliette</code> associated with this 
     *  session.
     *
     *  @return the <code>Oubliette</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.Oubliette getOubliette()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getOubliette());
    }


    /**
     *  Tests if this user can perform <code>Block</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupBlocks() {
        return (this.session.canSearchBlocks());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include blocks in oubliettes which are children
     *  of this oubliette in the oubliette hierarchy.
     */

    @OSID @Override
    public void useFederatedOublietteView() {
        this.session.useFederatedOublietteView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this oubliette only.
     */

    @OSID @Override
    public void useIsolatedOublietteView() {
        this.session.useIsolatedOublietteView();
        return;
    }
    
     
    /**
     *  Gets the <code>Block</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Block</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Block</code> and
     *  retained for compatibility.
     *
     *  @param  blockId <code>Id</code> of the
     *          <code>Block</code>
     *  @return the block
     *  @throws org.osid.NotFoundException <code>blockId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>blockId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.Block getBlock(org.osid.id.Id blockId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.hold.BlockQuery query = getQuery();
        query.matchId(blockId, true);
        org.osid.hold.BlockList blocks = this.session.getBlocksByQuery(query);
        if (blocks.hasNext()) {
            return (blocks.getNextBlock());
        } 
        
        throw new org.osid.NotFoundException(blockId + " not found");
    }


    /**
     *  Gets a <code>BlockList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  blocks specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Blocks</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  blockIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Block</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>blockIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.BlockList getBlocksByIds(org.osid.id.IdList blockIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.hold.BlockQuery query = getQuery();

        try (org.osid.id.IdList ids = blockIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getBlocksByQuery(query));
    }


    /**
     *  Gets a <code>BlockList</code> corresponding to the given
     *  block genus <code>Type</code> which does not include
     *  blocks of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  blocks or an error results. Otherwise, the returned list
     *  may contain only those blocks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  blockGenusType a block genus type 
     *  @return the returned <code>Block</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>blockGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.BlockList getBlocksByGenusType(org.osid.type.Type blockGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.hold.BlockQuery query = getQuery();
        query.matchGenusType(blockGenusType, true);
        return (this.session.getBlocksByQuery(query));
    }


    /**
     *  Gets a <code>BlockList</code> corresponding to the given
     *  block genus <code>Type</code> and include any additional
     *  blocks with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  blocks or an error results. Otherwise, the returned list
     *  may contain only those blocks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  blockGenusType a block genus type 
     *  @return the returned <code>Block</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>blockGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.BlockList getBlocksByParentGenusType(org.osid.type.Type blockGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.hold.BlockQuery query = getQuery();
        query.matchParentGenusType(blockGenusType, true);
        return (this.session.getBlocksByQuery(query));
    }


    /**
     *  Gets a <code>BlockList</code> containing the given
     *  block record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  blocks or an error results. Otherwise, the returned list
     *  may contain only those blocks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  blockRecordType a block record type 
     *  @return the returned <code>Block</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>blockRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.BlockList getBlocksByRecordType(org.osid.type.Type blockRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.hold.BlockQuery query = getQuery();
        query.matchRecordType(blockRecordType, true);
        return (this.session.getBlocksByQuery(query));
    }

    
    /**
     *  Gets a <code> BlockList </code> mapped to the given <code>
     *  Issue </code> In plenary mode, the returned list contains all
     *  known blocks or an error results. Otherwise, the returned list
     *  may contain only those blocks that are accessible through this
     *  session.
     *
     *  @param  issueId an issue <code> Id </code> 
     *  @return the returned <code> Block </code> list 
     *  @throws org.osid.NullArgumentException <code> issueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.hold.BlockList getBlocksForIssue(org.osid.id.Id issueId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.hold.BlockQuery query = getQuery();
        query.matchIssueId(issueId, true);
        return (this.session.getBlocksByQuery(query));
    }


    /**
     *  Gets all <code>Blocks</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  blocks or an error results. Otherwise, the returned list
     *  may contain only those blocks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Blocks</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.BlockList getBlocks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.hold.BlockQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getBlocksByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.hold.BlockQuery getQuery() {
        org.osid.hold.BlockQuery query = this.session.getBlockQuery();
        return (query);
    }
}
