//
// AbstractDictionarySearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.dictionary.dictionary.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractDictionarySearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.dictionary.DictionarySearchResults {

    private org.osid.dictionary.DictionaryList dictionaries;
    private final org.osid.dictionary.DictionaryQueryInspector inspector;
    private final java.util.Collection<org.osid.dictionary.records.DictionarySearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractDictionarySearchResults.
     *
     *  @param dictionaries the result set
     *  @param dictionaryQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>dictionaries</code>
     *          or <code>dictionaryQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractDictionarySearchResults(org.osid.dictionary.DictionaryList dictionaries,
                                            org.osid.dictionary.DictionaryQueryInspector dictionaryQueryInspector) {
        nullarg(dictionaries, "dictionaries");
        nullarg(dictionaryQueryInspector, "dictionary query inspectpr");

        this.dictionaries = dictionaries;
        this.inspector = dictionaryQueryInspector;

        return;
    }


    /**
     *  Gets the dictionary list resulting from a search.
     *
     *  @return a dictionary list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryList getDictionaries() {
        if (this.dictionaries == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.dictionary.DictionaryList dictionaries = this.dictionaries;
        this.dictionaries = null;
	return (dictionaries);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.dictionary.DictionaryQueryInspector getDictionaryQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  dictionary search record <code> Type. </code> This method must
     *  be used to retrieve a dictionary implementing the requested
     *  record.
     *
     *  @param dictionarySearchRecordType a dictionary search 
     *         record type 
     *  @return the dictionary search
     *  @throws org.osid.NullArgumentException
     *          <code>dictionarySearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(dictionarySearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.dictionary.records.DictionarySearchResultsRecord getDictionarySearchResultsRecord(org.osid.type.Type dictionarySearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.dictionary.records.DictionarySearchResultsRecord record : this.records) {
            if (record.implementsRecordType(dictionarySearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(dictionarySearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record dictionary search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addDictionaryRecord(org.osid.dictionary.records.DictionarySearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "dictionary record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
