//
// AbstractMutableRegistration.java
//
//     Defines a mutable Registration.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.registration.registration.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>Registration</code>.
 */

public abstract class AbstractMutableRegistration
    extends net.okapia.osid.jamocha.course.registration.registration.spi.AbstractRegistration
    implements org.osid.course.registration.Registration,
               net.okapia.osid.jamocha.builder.course.registration.registration.RegistrationMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this registration. 
     *
     *  @param record registration record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addRegistrationRecord(org.osid.course.registration.records.RegistrationRecord record, org.osid.type.Type recordType) {
        super.addRegistrationRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Sets the start date for when this registration is effective.
     *
     *  @param date the start date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setStartDate(org.osid.calendaring.DateTime date) {
        super.setStartDate(date);
        return;
    }


    /**
     *  Sets the end date for when this registration ceases to be
     *  effective.
     *
     *  @param date the end date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setEndDate(org.osid.calendaring.DateTime date) {
        super.setEndDate(date);
        return;
    }


    /**
     *  Sets the display name for this registration.
     *
     *  @param displayName the name for this registration
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this registration.
     *
     *  @param description the description of this registration
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException
     *          <code>genusType</code> is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the end state for the expiration of this registration
     *  relationship.
     *
     *  @param state the end state
     *  @throws org.osid.NullArgumentException <code>state</code> is
     *          <code>null</code>
     */

    @Override
    public void setEndReason(org.osid.process.State state) {
        super.setEndReason(state);
        return;
    }


    /**
     *  Sets the activity bundle.
     *
     *  @param activityBundle an activity bundle
     *  @throws org.osid.NullArgumentException
     *          <code>activityBundle</code> is <code>null</code>
     */

    @Override
    public void setActivityBundle(org.osid.course.registration.ActivityBundle activityBundle) {
        super.setActivityBundle(activityBundle);
        return;
    }


    /**
     *  Sets the student.
     *
     *  @param student a student
     *  @throws org.osid.NullArgumentException <code>student</code> is
     *          <code>null</code>
     */

    @Override
    public void setStudent(org.osid.resource.Resource student) {
        super.setStudent(student);
        return;
    }


    /**
     *  Adds a credit option.
     *
     *  @param credits a credit option
     *  @throws org.osid.NullArgumentException <code>credits</code> is
     *          <code>null</code>
     */

    @Override
    public void addCredits(java.math.BigDecimal credits) {
        super.addCredits(credits);
        return;
    }


    /**
     *  Sets all the credit options.
     *
     *  @param credits a collection of credits
     *  @throws org.osid.NullArgumentException <code>credits</code> is
     *          <code>null</code>
     */

    @Override
    public void setCredits(java.util.Collection<java.math.BigDecimal> credits) {
        super.setCredits(credits);
        return;
    }


    /**
     *  Sets the grading option.
     *
     *  @param option a grading option
     *  @throws org.osid.NullArgumentException <code>option</code> is
     *          <code>null</code>
     */

    @Override
    public void setGradingOption(org.osid.grading.GradeSystem option) {
        super.setGradingOption(option);
        return;
    }
}

