//
// AbstractGradebookQuery.java
//
//     A template for making a Gradebook Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.gradebook.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for gradebooks.
 */

public abstract class AbstractGradebookQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQuery
    implements org.osid.grading.GradebookQuery {

    private final java.util.Collection<org.osid.grading.records.GradebookQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the grade system <code> Id </code> for this query. 
     *
     *  @param  gradeSystemId a grade system <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeSystemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGradeSystemId(org.osid.id.Id gradeSystemId, boolean match) {
        return;
    }


    /**
     *  Clears the grade system <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGradeSystemIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> GradeSystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade system query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeSystemQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grade system. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the grade system query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQuery getGradeSystemQuery() {
        throw new org.osid.UnimplementedException("supportsGradeSystemQuery() is false");
    }


    /**
     *  Matches gradebooks that have any grade system. 
     *
     *  @param  match <code> true </code> to match gradebooks with any grade 
     *          system, <code> false </code> to match gradebooks with no grade 
     *          system 
     */

    @OSID @Override
    public void matchAnyGradeSystem(boolean match) {
        return;
    }


    /**
     *  Clears the grade system terms. 
     */

    @OSID @Override
    public void clearGradeSystemTerms() {
        return;
    }


    /**
     *  Sets the grade entry <code> Id </code> for this query. 
     *
     *  @param  gradeEntryId a grade entry <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeEntryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGradeEntryId(org.osid.id.Id gradeEntryId, boolean match) {
        return;
    }


    /**
     *  Clears the grade entry <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGradeEntryIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> GradeEntryQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade entry query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeEntryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grade entry. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the grade entry query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeEntryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryQuery getGradeEntryQuery() {
        throw new org.osid.UnimplementedException("supportsGradeEntryQuery() is false");
    }


    /**
     *  Matches gradebooks that have any grade entry. 
     *
     *  @param  match <code> true </code> to match gradebooks with any grade 
     *          entry, <code> false </code> to match gradebooks with no grade 
     *          entry 
     */

    @OSID @Override
    public void matchAnyGradeEntry(boolean match) {
        return;
    }


    /**
     *  Clears the grade entry terms. 
     */

    @OSID @Override
    public void clearGradeEntryTerms() {
        return;
    }


    /**
     *  Sets the gradebook column <code> Id </code> for this query. 
     *
     *  @param  gradebookColumnId a gradebook column <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradebookColumnId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchGradebookColumnId(org.osid.id.Id gradebookColumnId, 
                                       boolean match) {
        return;
    }


    /**
     *  Clears the gradebook column <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGradebookColumnIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> GradebookColumnQuery </code> is available. 
     *
     *  @return <code> true </code> if a gradebook column query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookColumnQuery() {
        return (false);
    }


    /**
     *  Gets the query for a gradebook column. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the gradebook column query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnQuery getGradebookColumnQuery() {
        throw new org.osid.UnimplementedException("supportsGradebookColumnQuery() is false");
    }


    /**
     *  Matches gradebooks that have any column. 
     *
     *  @param  match <code> true </code> to match gradebooks with any column, 
     *          <code> false </code> to match gradebooks with no column 
     */

    @OSID @Override
    public void matchAnyGradebookColumn(boolean match) {
        return;
    }


    /**
     *  Clears the gradebook column terms. 
     */

    @OSID @Override
    public void clearGradebookColumnTerms() {
        return;
    }


    /**
     *  Sets the gradebook <code> Id </code> for this query to match 
     *  gradebooks that have the specified gradebook as an ancestor. 
     *
     *  @param  gradebookId a gradebook <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorGradebookId(org.osid.id.Id gradebookId, 
                                         boolean match) {
        return;
    }


    /**
     *  Clears the ancestor gradebook <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorGradebookIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> GradebookQuery </code> is available. 
     *
     *  @return <code> true </code> if a gradebook query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorGradebookQuery() {
        return (false);
    }


    /**
     *  Gets the query for a gradebook. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the gradebook query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorGradebookQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookQuery getAncestorGradebookQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorGradebookQuery() is false");
    }


    /**
     *  Matches gradebook with any ancestor. 
     *
     *  @param  match <code> true </code> to match gradebooks with any 
     *          ancestor, <code> false </code> to match root gradebooks 
     */

    @OSID @Override
    public void matchAnyAncestorGradebook(boolean match) {
        return;
    }


    /**
     *  Clears the ancestor gradebook terms. 
     */

    @OSID @Override
    public void clearAncestorGradebookTerms() {
        return;
    }


    /**
     *  Sets the gradebook <code> Id </code> for this query to match 
     *  gradebooks that have the specified gradebook as a descendant. 
     *
     *  @param  gradebookId a gradebook <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantGradebookId(org.osid.id.Id gradebookId, 
                                           boolean match) {
        return;
    }


    /**
     *  Clears the descendant gradebook <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantGradebookIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> GradebookQuery </code> is available. 
     *
     *  @return <code> true </code> if a gradebook query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantGradebookQuery() {
        return (false);
    }


    /**
     *  Gets the query for a gradebook. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the gradebook query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantGradebookQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookQuery getDescendantGradebookQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantGradebookQuery() is false");
    }


    /**
     *  Matches gradebook with any descendant. 
     *
     *  @param  match <code> true </code> to match gradebooks with any 
     *          descendant, <code> false </code> to match leaf gradebooks 
     */

    @OSID @Override
    public void matchAnyDescendantGradebook(boolean match) {
        return;
    }


    /**
     *  Clears the descendant gradebook terms. 
     */

    @OSID @Override
    public void clearDescendantGradebookTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given gradebook query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a gradebook implementing the requested record.
     *
     *  @param gradebookRecordType a gradebook record type
     *  @return the gradebook query record
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(gradebookRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.grading.records.GradebookQueryRecord getGradebookQueryRecord(org.osid.type.Type gradebookRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.grading.records.GradebookQueryRecord record : this.records) {
            if (record.implementsRecordType(gradebookRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(gradebookRecordType + " is not supported");
    }


    /**
     *  Adds a record to this gradebook query. 
     *
     *  @param gradebookQueryRecord gradebook query record
     *  @param gradebookRecordType gradebook record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addGradebookQueryRecord(org.osid.grading.records.GradebookQueryRecord gradebookQueryRecord, 
                                          org.osid.type.Type gradebookRecordType) {

        addRecordType(gradebookRecordType);
        nullarg(gradebookQueryRecord, "gradebook query record");
        this.records.add(gradebookQueryRecord);        
        return;
    }
}
