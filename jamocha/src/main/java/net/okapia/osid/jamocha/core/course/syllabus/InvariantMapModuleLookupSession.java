//
// InvariantMapModuleLookupSession
//
//    Implements a Module lookup service backed by a fixed collection of
//    modules.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.syllabus;


/**
 *  Implements a Module lookup service backed by a fixed
 *  collection of modules. The modules are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapModuleLookupSession
    extends net.okapia.osid.jamocha.core.course.syllabus.spi.AbstractMapModuleLookupSession
    implements org.osid.course.syllabus.ModuleLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapModuleLookupSession</code> with no
     *  modules.
     *  
     *  @param courseCatalog the course catalog
     *  @throws org.osid.NullArgumnetException {@code courseCatalog} is
     *          {@code null}
     */

    public InvariantMapModuleLookupSession(org.osid.course.CourseCatalog courseCatalog) {
        setCourseCatalog(courseCatalog);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapModuleLookupSession</code> with a single
     *  module.
     *  
     *  @param courseCatalog the course catalog
     *  @param module a single module
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code module} is <code>null</code>
     */

      public InvariantMapModuleLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                               org.osid.course.syllabus.Module module) {
        this(courseCatalog);
        putModule(module);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapModuleLookupSession</code> using an array
     *  of modules.
     *  
     *  @param courseCatalog the course catalog
     *  @param modules an array of modules
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code modules} is <code>null</code>
     */

      public InvariantMapModuleLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                               org.osid.course.syllabus.Module[] modules) {
        this(courseCatalog);
        putModules(modules);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapModuleLookupSession</code> using a
     *  collection of modules.
     *
     *  @param courseCatalog the course catalog
     *  @param modules a collection of modules
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code modules} is <code>null</code>
     */

      public InvariantMapModuleLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                               java.util.Collection<? extends org.osid.course.syllabus.Module> modules) {
        this(courseCatalog);
        putModules(modules);
        return;
    }
}
