//
// AbstractCommitmentNotificationSession.java
//
//     A template for making CommitmentNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code Commitment} objects. This session is
 *  intended for consumers needing to synchronize their state with
 *  this service without the use of polling. Notifications are
 *  cancelled when this session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code Commitment} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for commitment entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractCommitmentNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.calendaring.CommitmentNotificationSession {

    private boolean federated = false;
    private org.osid.calendaring.Calendar calendar = new net.okapia.osid.jamocha.nil.calendaring.calendar.UnknownCalendar();


    /**
     *  Gets the {@code Calendar/code> {@code Id} associated with this
     *  session.
     *
     *  @return the {@code Calendar Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getCalendarId() {
        return (this.calendar.getId());
    }

    
    /**
     *  Gets the {@code Calendar} associated with this session.
     *
     *  @return the {@code Calendar} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.calendar);
    }


    /**
     *  Sets the {@code Calendar}.
     *
     *  @param calendar the calendar for this session
     *  @throws org.osid.NullArgumentException {@code calendar}
     *          is {@code null}
     */

    protected void setCalendar(org.osid.calendaring.Calendar calendar) {
        nullarg(calendar, "calendar");
        this.calendar = calendar;
        return;
    }


    /**
     *  Tests if this user can register for {@code Commitment}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForCommitmentNotifications() {
        return (true);
    }


    /**
     *  Reliable notifications are desired. In reliable mode,
     *  notifications are to be acknowledged using <code>
     *  acknowledgeCommitmentNotification() </code>.
     */

    @OSID @Override
    public void reliableCommitmentNotifications() {
        return;
    }


    /**
     *  Unreliable notifications are desired. In unreliable mode,
     *  notifications do not need to be acknowledged.
     */

    @OSID @Override
    public void unreliableCommitmentNotifications() {
        return;
    }


    /**
     *  Acknowledge a commitment notification.
     *
     *  @param  notificationId the <code> Id </code> of the notification
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void acknowledgeCommitmentNotification(org.osid.id.Id notificationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include notifications for commitments in calendars
     *  which are children of this calendar in the calendar hierarchy.
     */

    @OSID @Override
    public void useFederatedCalendarView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts notifications to this calendar only.
     */

    @OSID @Override
    public void useIsolatedCalendarView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return {@codetrue</code> if federated view,
     *          {@codefalse</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications of new commitments. {@code
     *  CommitmentReceiver.newCommitment()} is invoked when a new
     *  {@code Commitment} is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewCommitments()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new commitments by commitment
     *  genus type. {@code CommitmentReceiver.newCommitment()} is
     *  invoked when a new commitment is created.
     *
     *  @param  commitmentGenusType the commitment genus type to monitor 
     *  @throws org.osid.NullArgumentException {@code
     *          commitmentGenusType is null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForNewCommitmentsByGenusType(org.osid.id.Id commitmentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new commitments for the given
     *  event {@code Id}. {@code CommitmentReceiver.newCommitment()}
     *  is invoked when a new {@code Commitment} is created.
     *
     *  @param  eventId the {@code Id} of the event to monitor
     *  @throws org.osid.NullArgumentException {@code eventId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForNewCommitmentsForEvent(org.osid.id.Id eventId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /** 
     *  Register for notifications of new commitments for the given
     *  resource {@code Id}. {@code
     *  CommitmentReceiver.newCommitment()} is invoked when a new
     *  {@code Commitment} is created.
     *
     *  @param  resourceId the {@code Id} of the resource to monitor
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForNewCommitmentsForResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of updated commitments. {@code
     *  CommitmentReceiver.changedCommitment()} is invoked when a
     *  commitment is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedCommitments()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of changed commitments by
     *  commitment genus type. {@code
     *  CommitmentReceiver.changedCommitment()} is invoked when a
     *  commitment is changed.
     *
     *  @param commitmentGenusType the commitment genus type to
     *         monitor
     *  @throws org.osid.NullArgumentException {@code commitmentGenusType is 
     *          null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForChangedCommitmentsByGenusType(org.osid.type.Type commitmentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of updated commitments for the
     *  given event {@code Id}. {@code
     *  CommitmentReceiver.changedCommitment()} is invoked when a
     *  {@code Commitment} in this calendar is changed.
     *
     *  @param  eventId the {@code Id} of the event to monitor
     *  @throws org.osid.NullArgumentException {@code eventId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForChangedCommitmentsForEvent(org.osid.id.Id eventId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of updated commitments for the
     *  given resource {@code Id}. {@code
     *  CommitmentReceiver.changedCommitment()} is invoked when a
     *  {@code Commitment} in this calendar is changed.
     *
     *  @param  resourceId the {@code Id} of the resource to monitor
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForChangedCommitmentsForResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated commitment. {@code
     *  CommitmentReceiver.changedCommitment()} is invoked when the
     *  specified commitment is changed.
     *
     *  @param commitmentId the {@code Id} of the {@code Commitment} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code commitmentId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedCommitment(org.osid.id.Id commitmentId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted commitments. {@code
     *  CommitmentReceiver.deletedCommitment()} is invoked when a
     *  commitment is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedCommitments()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }

    
    /**
     *  Register for notifications of deleted commitments by
     *  commitment genus type. {@code
     *  CommitmentReceiver.deletedCommitment()} is invoked when a
     *  commitment is deleted.
     *
     *  @param  commitmentGenusType the commitment genus type to monitor 
     *  @throws org.osid.NullArgumentException {@code commitmentGenusType is 
     *          null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForDeletedCommitmentsByGenusType(org.osid.id.Id commitmentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of deleted commitments for the
     *  given event {@code Id}. {@code
     *  CommitmentReceiver.deletedCommitment()} is invoked when a
     *  {@code Commitment} is deleted or removed from this calendar.
     *
     *  @param  eventId the {@code Id} of the event to monitor
     *  @throws org.osid.NullArgumentException {@code eventId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */
      
    @OSID @Override
    public void registerForDeletedCommitmentsForEvent(org.osid.id.Id eventId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of deleted commitments for the
     *  given resource {@code Id}. {@code
     *  CommitmentReceiver.deletedCommitment()} is invoked when a
     *  {@code Commitment} is deleted or removed from this calendar.
     *
     *  @param  resourceId the {@code Id} of the resource to monitor
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForDeletedCommitmentsForResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of a deleted commitment. {@code
     *  CommitmentReceiver.deletedCommitment()} is invoked when the
     *  specified commitment is deleted.
     *
     *  @param commitmentId the {@code Id} of the
     *          {@code Commitment} to monitor
     *  @throws org.osid.NullArgumentException {@code commitmentId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedCommitment(org.osid.id.Id commitmentId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
