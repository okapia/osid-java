//
// AbstractProgramSearch.java
//
//     A template for making a Program Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.program.program.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing program searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractProgramSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.course.program.ProgramSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.course.program.records.ProgramSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.course.program.ProgramSearchOrder programSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of programs. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  programIds list of programs
     *  @throws org.osid.NullArgumentException
     *          <code>programIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongPrograms(org.osid.id.IdList programIds) {
        while (programIds.hasNext()) {
            try {
                this.ids.add(programIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongPrograms</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of program Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getProgramIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  programSearchOrder program search order 
     *  @throws org.osid.NullArgumentException
     *          <code>programSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>programSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderProgramResults(org.osid.course.program.ProgramSearchOrder programSearchOrder) {
	this.programSearchOrder = programSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.course.program.ProgramSearchOrder getProgramSearchOrder() {
	return (this.programSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given program search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a program implementing the requested record.
     *
     *  @param programSearchRecordType a program search record
     *         type
     *  @return the program search record
     *  @throws org.osid.NullArgumentException
     *          <code>programSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(programSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.program.records.ProgramSearchRecord getProgramSearchRecord(org.osid.type.Type programSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.course.program.records.ProgramSearchRecord record : this.records) {
            if (record.implementsRecordType(programSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(programSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this program search. 
     *
     *  @param programSearchRecord program search record
     *  @param programSearchRecordType program search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addProgramSearchRecord(org.osid.course.program.records.ProgramSearchRecord programSearchRecord, 
                                           org.osid.type.Type programSearchRecordType) {

        addRecordType(programSearchRecordType);
        this.records.add(programSearchRecord);        
        return;
    }
}
