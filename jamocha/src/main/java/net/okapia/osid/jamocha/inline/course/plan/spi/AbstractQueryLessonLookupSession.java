//
// AbstractQueryLessonLookupSession.java
//
//    An inline adapter that maps a LessonLookupSession to
//    a LessonQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.course.plan.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a LessonLookupSession to
 *  a LessonQuerySession.
 */

public abstract class AbstractQueryLessonLookupSession
    extends net.okapia.osid.jamocha.course.plan.spi.AbstractLessonLookupSession
    implements org.osid.course.plan.LessonLookupSession {

    private boolean effectiveonly = false;
    private final org.osid.course.plan.LessonQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryLessonLookupSession.
     *
     *  @param querySession the underlying lesson query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryLessonLookupSession(org.osid.course.plan.LessonQuerySession querySession) {
        nullarg(querySession, "lesson query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>CourseCatalog</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>CourseCatalog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.session.getCourseCatalogId());
    }


    /**
     *  Gets the <code>CourseCatalog</code> associated with this 
     *  session.
     *
     *  @return the <code>CourseCatalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getCourseCatalog());
    }


    /**
     *  Tests if this user can perform <code>Lesson</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupLessons() {
        return (this.session.canSearchLessons());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include lessons in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.session.useFederatedCourseCatalogView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.session.useIsolatedCourseCatalogView();
        return;
    }
    

    /**
     *  Only lessons whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveLessonView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All lessons of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveLessonView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Lesson</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Lesson</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Lesson</code> and
     *  retained for compatibility.
     *
     *  In effective mode, lessons are returned that are currently
     *  effective.  In any effective mode, effective lessons and
     *  those currently expired are returned.
     *
     *  @param  lessonId <code>Id</code> of the
     *          <code>Lesson</code>
     *  @return the lesson
     *  @throws org.osid.NotFoundException <code>lessonId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>lessonId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.plan.Lesson getLesson(org.osid.id.Id lessonId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.plan.LessonQuery query = getQuery();
        query.matchId(lessonId, true);
        org.osid.course.plan.LessonList lessons = this.session.getLessonsByQuery(query);
        if (lessons.hasNext()) {
            return (lessons.getNextLesson());
        } 
        
        throw new org.osid.NotFoundException(lessonId + " not found");
    }


    /**
     *  Gets a <code>LessonList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  lessons specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Lessons</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, lessons are returned that are currently effective.
     *  In any effective mode, effective lessons and those currently expired
     *  are returned.
     *
     *  @param  lessonIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Lesson</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>lessonIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.plan.LessonList getLessonsByIds(org.osid.id.IdList lessonIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.plan.LessonQuery query = getQuery();

        try (org.osid.id.IdList ids = lessonIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getLessonsByQuery(query));
    }


    /**
     *  Gets a <code>LessonList</code> corresponding to the given
     *  lesson genus <code>Type</code> which does not include
     *  lessons of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  lessons or an error results. Otherwise, the returned list
     *  may contain only those lessons that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, lessons are returned that are currently effective.
     *  In any effective mode, effective lessons and those currently expired
     *  are returned.
     *
     *  @param  lessonGenusType a lesson genus type 
     *  @return the returned <code>Lesson</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>lessonGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.plan.LessonList getLessonsByGenusType(org.osid.type.Type lessonGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.plan.LessonQuery query = getQuery();
        query.matchGenusType(lessonGenusType, true);
        return (this.session.getLessonsByQuery(query));
    }


    /**
     *  Gets a <code>LessonList</code> corresponding to the given
     *  lesson genus <code>Type</code> and include any additional
     *  lessons with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  lessons or an error results. Otherwise, the returned list
     *  may contain only those lessons that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, lessons are returned that are currently
     *  effective.  In any effective mode, effective lessons and
     *  those currently expired are returned.
     *
     *  @param  lessonGenusType a lesson genus type 
     *  @return the returned <code>Lesson</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>lessonGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.plan.LessonList getLessonsByParentGenusType(org.osid.type.Type lessonGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.plan.LessonQuery query = getQuery();
        query.matchParentGenusType(lessonGenusType, true);
        return (this.session.getLessonsByQuery(query));
    }


    /**
     *  Gets a <code>LessonList</code> containing the given
     *  lesson record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  lessons or an error results. Otherwise, the returned list
     *  may contain only those lessons that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, lessons are returned that are currently
     *  effective.  In any effective mode, effective lessons and
     *  those currently expired are returned.
     *
     *  @param  lessonRecordType a lesson record type 
     *  @return the returned <code>Lesson</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>lessonRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.plan.LessonList getLessonsByRecordType(org.osid.type.Type lessonRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.plan.LessonQuery query = getQuery();
        query.matchRecordType(lessonRecordType, true);
        return (this.session.getLessonsByQuery(query));
    }


    /**
     *  Gets a <code>LessonList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  lessons or an error results. Otherwise, the returned list
     *  may contain only those lessons that are accessible
     *  through this session.
     *  
     *  In effective mode, lessons are returned that are currently
     *  effective.  In any effective mode, effective lessons and
     *  those currently expired are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Lesson</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.plan.LessonList getLessonsOnDate(org.osid.calendaring.DateTime from, 
                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.plan.LessonQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getLessonsByQuery(query));
    }
        

    /**
     *  Gets a list of lessons corresponding to a plan
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  lessons or an error results. Otherwise, the returned list
     *  may contain only those lessons that are accessible
     *  through this session.
     *
     *  In effective mode, lessons are returned that are
     *  currently effective.  In any effective mode, effective
     *  lessons and those currently expired are returned.
     *
     *  @param  planId the <code>Id</code> of the plan
     *  @return the returned <code>LessonList</code>
     *  @throws org.osid.NullArgumentException <code>planId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.course.plan.LessonList getLessonsForPlan(org.osid.id.Id planId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.course.plan.LessonQuery query = getQuery();
        query.matchPlanId(planId, true);
        return (this.session.getLessonsByQuery(query));
    }


    /**
     *  Gets a list of lessons corresponding to a plan
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  lessons or an error results. Otherwise, the returned list
     *  may contain only those lessons that are accessible
     *  through this session.
     *
     *  In effective mode, lessons are returned that are
     *  currently effective.  In any effective mode, effective
     *  lessons and those currently expired are returned.
     *
     *  @param  planId the <code>Id</code> of the plan
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>LessonList</code>
     *  @throws org.osid.NullArgumentException <code>planId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.plan.LessonList getLessonsForPlanOnDate(org.osid.id.Id planId,
                                                                   org.osid.calendaring.DateTime from,
                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.plan.LessonQuery query = getQuery();
        query.matchPlanId(planId, true);
        query.matchDate(from, to, true);
        return (this.session.getLessonsByQuery(query));
    }


    /**
     *  Gets a list of lessons corresponding to a docet
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  lessons or an error results. Otherwise, the returned list
     *  may contain only those lessons that are accessible
     *  through this session.
     *
     *  In effective mode, lessons are returned that are
     *  currently effective.  In any effective mode, effective
     *  lessons and those currently expired are returned.
     *
     *  @param  docetId the <code>Id</code> of the docet
     *  @return the returned <code>LessonList</code>
     *  @throws org.osid.NullArgumentException <code>docetId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.course.plan.LessonList getLessonsForDocet(org.osid.id.Id docetId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.course.plan.LessonQuery query = getQuery();
        query.matchDocetId(docetId, true);
        return (this.session.getLessonsByQuery(query));
    }


    /**
     *  Gets a list of lessons corresponding to a docet
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  lessons or an error results. Otherwise, the returned list
     *  may contain only those lessons that are accessible
     *  through this session.
     *
     *  In effective mode, lessons are returned that are
     *  currently effective.  In any effective mode, effective
     *  lessons and those currently expired are returned.
     *
     *  @param  docetId the <code>Id</code> of the docet
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>LessonList</code>
     *  @throws org.osid.NullArgumentException <code>docetId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.plan.LessonList getLessonsForDocetOnDate(org.osid.id.Id docetId,
                                                                    org.osid.calendaring.DateTime from,
                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.plan.LessonQuery query = getQuery();
        query.matchDocetId(docetId, true);
        query.matchDate(from, to, true);
        return (this.session.getLessonsByQuery(query));
    }


    /**
     *  Gets a list of lessons corresponding to plan and docet
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  lessons or an error results. Otherwise, the returned list
     *  may contain only those lessons that are accessible
     *  through this session.
     *
     *  In effective mode, lessons are returned that are
     *  currently effective.  In any effective mode, effective
     *  lessons and those currently expired are returned.
     *
     *  @param  planId the <code>Id</code> of the plan
     *  @param  docetId the <code>Id</code> of the docet
     *  @return the returned <code>LessonList</code>
     *  @throws org.osid.NullArgumentException <code>planId</code>,
     *          <code>docetId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.plan.LessonList getLessonsForPlanAndDocet(org.osid.id.Id planId,
                                                                     org.osid.id.Id docetId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.plan.LessonQuery query = getQuery();
        query.matchPlanId(planId, true);
        query.matchDocetId(docetId, true);
        return (this.session.getLessonsByQuery(query));
    }


    /**
     *  Gets a list of lessons corresponding to plan and docet
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  lessons or an error results. Otherwise, the returned list
     *  may contain only those lessons that are accessible
     *  through this session.
     *
     *  In effective mode, lessons are returned that are
     *  currently effective.  In any effective mode, effective
     *  lessons and those currently expired are returned.
     *
     *  @param  planId the <code>Id</code> of the plan
     *  @param  docetId the <code>Id</code> of the docet
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>LessonList</code>
     *  @throws org.osid.NullArgumentException <code>planId</code>,
     *          <code>docetId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.plan.LessonList getLessonsForPlanAndDocetOnDate(org.osid.id.Id planId,
                                                                           org.osid.id.Id docetId,
                                                                           org.osid.calendaring.DateTime from,
                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.course.plan.LessonQuery query = getQuery();
        query.matchPlanId(planId, true);
        query.matchDocetId(docetId, true);
        query.matchDate(from, to, true);
        return (this.session.getLessonsByQuery(query));
    }

    
    /**
     *  Gets all <code>Lessons</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  lessons or an error results. Otherwise, the returned list
     *  may contain only those lessons that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, lessons are returned that are currently
     *  effective.  In any effective mode, effective lessons and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Lessons</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.plan.LessonList getLessons()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.plan.LessonQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getLessonsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.course.plan.LessonQuery getQuery() {
        org.osid.course.plan.LessonQuery query = this.session.getLessonQuery();
        
        if (isEffectiveOnly()) {
            query.matchEffective(true);
        }

        return (query);
    }
}
