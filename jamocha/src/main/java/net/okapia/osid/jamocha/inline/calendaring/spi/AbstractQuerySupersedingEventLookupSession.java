//
// AbstractQuerySupersedingEventLookupSession.java
//
//    An inline adapter that maps a SupersedingEventLookupSession to
//    a SupersedingEventQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a SupersedingEventLookupSession to
 *  a SupersedingEventQuerySession.
 */

public abstract class AbstractQuerySupersedingEventLookupSession
    extends net.okapia.osid.jamocha.calendaring.spi.AbstractSupersedingEventLookupSession
    implements org.osid.calendaring.SupersedingEventLookupSession {

    private boolean activeonly    = false;
    private final org.osid.calendaring.SupersedingEventQuerySession session;
    

    /**
     *  Constructs a new AbstractQuerySupersedingEventLookupSession.
     *
     *  @param querySession the underlying superseding event query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQuerySupersedingEventLookupSession(org.osid.calendaring.SupersedingEventQuerySession querySession) {
        nullarg(querySession, "superseding event query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Calendar</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Calendar Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCalendarId() {
        return (this.session.getCalendarId());
    }


    /**
     *  Gets the <code>Calendar</code> associated with this 
     *  session.
     *
     *  @return the <code>Calendar</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getCalendar());
    }


    /**
     *  Tests if this user can perform <code>SupersedingEvent</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupSupersedingEvents() {
        return (this.session.canSearchSupersedingEvents());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include superseding events in calendars which are children
     *  of this calendar in the calendar hierarchy.
     */

    @OSID @Override
    public void useFederatedCalendarView() {
        this.session.useFederatedCalendarView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this calendar only.
     */

    @OSID @Override
    public void useIsolatedCalendarView() {
        this.session.useIsolatedCalendarView();
        return;
    }
    

    /**
     *  Only active superseding events are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveSupersedingEventView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive superseding events are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusSupersedingEventView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>SupersedingEvent</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>SupersedingEvent</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>SupersedingEvent</code> and
     *  retained for compatibility.
     *
     *  In active mode, superseding events are returned that are currently
     *  active. In any status mode, active and inactive superseding events
     *  are returned.
     *
     *  @param  supersedingEventId <code>Id</code> of the
     *          <code>SupersedingEvent</code>
     *  @return the superseding event
     *  @throws org.osid.NotFoundException <code>supersedingEventId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>supersedingEventId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEvent getSupersedingEvent(org.osid.id.Id supersedingEventId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.SupersedingEventQuery query = getQuery();
        query.matchId(supersedingEventId, true);
        org.osid.calendaring.SupersedingEventList supersedingEvents = this.session.getSupersedingEventsByQuery(query);
        if (supersedingEvents.hasNext()) {
            return (supersedingEvents.getNextSupersedingEvent());
        } 
        
        throw new org.osid.NotFoundException(supersedingEventId + " not found");
    }


    /**
     *  Gets a <code>SupersedingEventList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  supersedingEvents specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>SupersedingEvents</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, superseding events are returned that are currently
     *  active. In any status mode, active and inactive superseding events
     *  are returned.
     *
     *  @param  supersedingEventIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>SupersedingEvent</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>supersedingEventIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventList getSupersedingEventsByIds(org.osid.id.IdList supersedingEventIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.SupersedingEventQuery query = getQuery();

        try (org.osid.id.IdList ids = supersedingEventIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getSupersedingEventsByQuery(query));
    }


    /**
     *  Gets a <code>SupersedingEventList</code> corresponding to the given
     *  superseding event genus <code>Type</code> which does not include
     *  superseding events of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  superseding events or an error results. Otherwise, the returned list
     *  may contain only those superseding events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, superseding events are returned that are currently
     *  active. In any status mode, active and inactive superseding events
     *  are returned.
     *
     *  @param  supersedingEventGenusType a supersedingEvent genus type 
     *  @return the returned <code>SupersedingEvent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>supersedingEventGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventList getSupersedingEventsByGenusType(org.osid.type.Type supersedingEventGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.SupersedingEventQuery query = getQuery();
        query.matchGenusType(supersedingEventGenusType, true);
        return (this.session.getSupersedingEventsByQuery(query));
    }


    /**
     *  Gets a <code>SupersedingEventList</code> corresponding to the given
     *  superseding event genus <code>Type</code> and include any additional
     *  superseding events with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  superseding events or an error results. Otherwise, the returned list
     *  may contain only those superseding events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, superseding events are returned that are currently
     *  active. In any status mode, active and inactive superseding events
     *  are returned.
     *
     *  @param  supersedingEventGenusType a supersedingEvent genus type 
     *  @return the returned <code>SupersedingEvent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>supersedingEventGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventList getSupersedingEventsByParentGenusType(org.osid.type.Type supersedingEventGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.SupersedingEventQuery query = getQuery();
        query.matchParentGenusType(supersedingEventGenusType, true);
        return (this.session.getSupersedingEventsByQuery(query));
    }


    /**
     *  Gets a <code>SupersedingEventList</code> containing the given
     *  superseding event record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  superseding events or an error results. Otherwise, the returned list
     *  may contain only those superseding events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, superseding events are returned that are currently
     *  active. In any status mode, active and inactive superseding events
     *  are returned.
     *
     *  @param  supersedingEventRecordType a supersedingEvent record type 
     *  @return the returned <code>SupersedingEvent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>supersedingEventRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventList getSupersedingEventsByRecordType(org.osid.type.Type supersedingEventRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.SupersedingEventQuery query = getQuery();
        query.matchRecordType(supersedingEventRecordType, true);
        return (this.session.getSupersedingEventsByQuery(query));
    }


    /**
     *  Gets the <code> SupersedingEvents </code> related to the
     *  relative event <code> Id. </code>
     *  
     *  In plenary mode, the returned list contains all known
     *  superseding events or an error results. Otherwise, the
     *  returned list may contain only those superseding events that
     *  are accessible through this session.
     *  
     *  In active mode, supersdeing events are returned that are
     *  currently active. In any status mode, active and inactive
     *  superseding events are returned.
     *
     *  @param  supersededEventId <code> Id </code> of the related event 
     *  @return the superseding events 
     *  @throws org.osid.NotFoundException <code> supersededEventId </code> 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> supersededEventId 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventList getSupersedingEventsBySupersededEvent(org.osid.id.Id supersededEventId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.SupersedingEventQuery query = query = getQuery();
        query.matchSupersededEventId(supersededEventId, true);
        return (this.session.getSupersedingEventsByQuery(query));
    }
        

    /**
     *  Gets all <code>SupersedingEvents</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  superseding events or an error results. Otherwise, the returned list
     *  may contain only those superseding events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, superseding events are returned that are currently
     *  active. In any status mode, active and inactive superseding events
     *  are returned.
     *
     *  @return a list of <code>SupersedingEvents</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventList getSupersedingEvents()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.SupersedingEventQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getSupersedingEventsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.calendaring.SupersedingEventQuery getQuery() {
        org.osid.calendaring.SupersedingEventQuery query = this.session.getSupersedingEventQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
