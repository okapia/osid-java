//
// AbstractOntologySearch.java
//
//     A template for making an Ontology Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ontology.ontology.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing ontology searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractOntologySearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.ontology.OntologySearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.ontology.records.OntologySearchRecord> records = new java.util.ArrayList<>();
    private org.osid.ontology.OntologySearchOrder ontologySearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of ontologies. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  ontologyIds list of ontologies
     *  @throws org.osid.NullArgumentException
     *          <code>ontologyIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongOntologies(org.osid.id.IdList ontologyIds) {
        while (ontologyIds.hasNext()) {
            try {
                this.ids.add(ontologyIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongOntologies</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of ontology Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getOntologyIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  ontologySearchOrder ontology search order 
     *  @throws org.osid.NullArgumentException
     *          <code>ontologySearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>ontologySearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderOntologyResults(org.osid.ontology.OntologySearchOrder ontologySearchOrder) {
	this.ontologySearchOrder = ontologySearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.ontology.OntologySearchOrder getOntologySearchOrder() {
	return (this.ontologySearchOrder);
    }


    /**
     *  Gets the record corresponding to the given ontology search
     *  record <code> Type. </code> This method must be used to
     *  retrieve an ontology implementing the requested record.
     *
     *  @param ontologySearchRecordType an ontology search record
     *         type
     *  @return the ontology search record
     *  @throws org.osid.NullArgumentException
     *          <code>ontologySearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(ontologySearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.ontology.records.OntologySearchRecord getOntologySearchRecord(org.osid.type.Type ontologySearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.ontology.records.OntologySearchRecord record : this.records) {
            if (record.implementsRecordType(ontologySearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(ontologySearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this ontology search. 
     *
     *  @param ontologySearchRecord ontology search record
     *  @param ontologySearchRecordType ontology search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addOntologySearchRecord(org.osid.ontology.records.OntologySearchRecord ontologySearchRecord, 
                                           org.osid.type.Type ontologySearchRecordType) {

        addRecordType(ontologySearchRecordType);
        this.records.add(ontologySearchRecord);        
        return;
    }
}
