//
// AbstractItem.java
//
//     Defines an Item builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.billing.item.spi;


/**
 *  Defines an <code>Item</code> builder.
 */

public abstract class AbstractItemBuilder<T extends AbstractItemBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.billing.item.ItemMiter item;


    /**
     *  Constructs a new <code>AbstractItemBuilder</code>.
     *
     *  @param item the item to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractItemBuilder(net.okapia.osid.jamocha.builder.billing.item.ItemMiter item) {
        super(item);
        this.item = item;
        return;
    }


    /**
     *  Builds the item.
     *
     *  @return the new item
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.billing.Item build() {
        (new net.okapia.osid.jamocha.builder.validator.billing.item.ItemValidator(getValidations())).validate(this.item);
        return (new net.okapia.osid.jamocha.builder.billing.item.ImmutableItem(this.item));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the item miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.billing.item.ItemMiter getMiter() {
        return (this.item);
    }


    /**
     *  Sets the category.
     *
     *  @param category a category
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>category</code> is <code>null</code>
     */

    public T category(org.osid.billing.Category category) {
        getMiter().setCategory(category);
        return (self());
    }


    /**
     *  Sets the account.
     *
     *  @param account an account
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>account</code> is <code>null</code>
     */

    public T account(org.osid.financials.Account account) {
        getMiter().setAccount(account);
        return (self());
    }


    /**
     *  Sets the product.
     *
     *  @param product a product
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>product</code> is <code>null</code>
     */

    public T product(org.osid.ordering.Product product) {
        getMiter().setProduct(product);
        return (self());
    }


    /**
     *  Sets the amount.
     *
     *  @param amount an amount
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>amount</code> is <code>null</code>
     */

    public T amount(org.osid.financials.Currency amount) {
        getMiter().setAmount(amount);
        return (self());
    }


    /**
     *  Sets the debit flag.
     *
     *  @return the builder
     */

    public T debit() {
        getMiter().setDebit(true);
        return (self());
    }


    /**
     *  Unsets the debit flag.
     *
     *  @return the builder
     */

    public T credit() {
        getMiter().setDebit(false);
        return (self());
    }


    /**
     *  Sets the recurring interval.
     *
     *  @param recurringInterval a recurring interval
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>recurringInterval</code> is <code>null</code>
     */

    public T recurringInterval(org.osid.calendaring.Duration recurringInterval) {
        getMiter().setRecurringInterval(recurringInterval);
        return (self());
    }


    /**
     *  Adds an Item record.
     *
     *  @param record an item record
     *  @param recordType the type of item record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.billing.records.ItemRecord record, org.osid.type.Type recordType) {
        getMiter().addItemRecord(record, recordType);
        return (self());
    }
}       


