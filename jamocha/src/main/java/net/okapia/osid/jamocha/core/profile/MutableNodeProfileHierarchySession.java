//
// MutableNodeProfileHierarchySession.java
//
//     Defines a Profile hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.profile;


/**
 *  Defines a profile hierarchy session for delivering a hierarchy
 *  of profiles using the ProfileNode interface.
 */

public final class MutableNodeProfileHierarchySession
    extends net.okapia.osid.jamocha.core.profile.spi.AbstractNodeProfileHierarchySession
    implements org.osid.profile.ProfileHierarchySession {


    /**
     *  Constructs a new
     *  <code>MutableNodeProfileHierarchySession</code> with no
     *  nodes.
     *
     *  @param hierarchy the hierarchy for this session
     *  @throws org.osid.NullArgumentException <code>hierarchy</code> 
     *          is <code>null</code>
     */

    public MutableNodeProfileHierarchySession(org.osid.hierarchy.Hierarchy hierarchy) {
        setHierarchy(hierarchy);
        return;
    }


    /**
     *  Constructs a new
     *  <code>MutableNodeProfileHierarchySession</code> using the
     *  root node for the hierarchy.
     *
     *  @param root a root node
     *  @throws org.osid.NullArgumentException <code>node</code> 
     *          is <code>null</code>
     */

    public MutableNodeProfileHierarchySession(org.osid.profile.ProfileNode root) {
        setHierarchy(new net.okapia.osid.jamocha.builder.hierarchy.hierarchy.HierarchyBuilder()
                     .id(root.getId())
                     .displayName(root.getProfile().getDisplayName())
                     .description(root.getProfile().getDescription())
                     .build());

        addRootProfile(root);
        return;
    }


    /**
     *  Constructs a new
     *  <code>MutableNodeProfileHierarchySession</code> using the
     *  given root as the root node.
     *
     *  @param hierarchy the hierarchy for this session
     *  @param root a root node
     *  @throws org.osid.NullArgumentException <code>hierarchy</code>
     *          or <code>root</code> is <code>null</code>
     */

    public MutableNodeProfileHierarchySession(org.osid.hierarchy.Hierarchy hierarchy, 
                                               org.osid.profile.ProfileNode root) {
        setHierarchy(hierarchy);
        addRootProfile(root);
        return;
    }


    /**
     *  Constructs a new
     *  <code>MutableNodeProfileHierarchySession</code> using a
     *  collection of nodes as roots in the hierarchy.
     *
     *  @param hierarchy the hierarchy for this session
     *  @param roots a collection of root nodes
     *  @throws org.osid.NullArgumentException <code>hierarchy</code>
     *          or <code>roots</code> is <code>null</code>
     */

    public MutableNodeProfileHierarchySession(org.osid.hierarchy.Hierarchy hierarchy, 
                                               java.util.Collection<org.osid.profile.ProfileNode> roots) {
        setHierarchy(hierarchy);
        addRootProfiles(roots);
        return;
    }


    /**
     *  Adds a root profile node to the hierarchy.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    @Override
    public void addRootProfile(org.osid.profile.ProfileNode root) {
        super.addRootProfile(root);
        return;
    }


    /**
     *  Adds a collection of root profile nodes.
     *
     *  @param roots hierarchy roots
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    @Override
    public void addRootProfiles(java.util.Collection<org.osid.profile.ProfileNode> roots) {
        super.addRootProfiles(roots);
        return;
    }


    /**
     *  Removes a root profile node from the hierarchy.
     *
     *  @param rootId a root node {@code Id}
     *  @throws org.osid.NullArgumentException <code>rootId</code> is
     *          <code>null</code>
     */

    @Override
    public void removeRootProfile(org.osid.id.Id rootId) {
        super.removeRootProfile(rootId);
        return;
    }
}
