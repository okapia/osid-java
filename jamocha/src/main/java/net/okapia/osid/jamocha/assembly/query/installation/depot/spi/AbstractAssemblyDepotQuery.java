//
// AbstractAssemblyDepotQuery.java
//
//     A DepotQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.installation.depot.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A DepotQuery that stores terms.
 */

public abstract class AbstractAssemblyDepotQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCatalogQuery
    implements org.osid.installation.DepotQuery,
               org.osid.installation.DepotQueryInspector,
               org.osid.installation.DepotSearchOrder {

    private final java.util.Collection<org.osid.installation.records.DepotQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.installation.records.DepotQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.installation.records.DepotSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyDepotQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyDepotQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the package <code> Id </code> for this query. 
     *
     *  @param  packageId a package <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> packageId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPackageId(org.osid.id.Id packageId, boolean match) {
        getAssembler().addIdTerm(getPackageIdColumn(), packageId, match);
        return;
    }


    /**
     *  Clears the package <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearPackageIdTerms() {
        getAssembler().clearTerms(getPackageIdColumn());
        return;
    }


    /**
     *  Gets the package <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPackageIdTerms() {
        return (getAssembler().getIdTerms(getPackageIdColumn()));
    }


    /**
     *  Gets the PackageId column name.
     *
     * @return the column name
     */

    protected String getPackageIdColumn() {
        return ("package_id");
    }


    /**
     *  Tests if a <code> PackageQuery </code> is available. 
     *
     *  @return <code> true </code> if a package query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPackageQuery() {
        return (false);
    }


    /**
     *  Gets the query for a package. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the package query 
     *  @throws org.osid.UnimplementedException <code> supportsPackageQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.PackageQuery getPackageQuery() {
        throw new org.osid.UnimplementedException("supportsPackageQuery() is false");
    }


    /**
     *  Matches depots that have any package. 
     *
     *  @param  match <code> true </code> to match depots with any packages, 
     *          <code> false </code> to match depots with no packages 
     */

    @OSID @Override
    public void matchAnyPackage(boolean match) {
        getAssembler().addIdWildcardTerm(getPackageColumn(), match);
        return;
    }


    /**
     *  Clears the package query terms. 
     */

    @OSID @Override
    public void clearPackageTerms() {
        getAssembler().clearTerms(getPackageColumn());
        return;
    }


    /**
     *  Gets the package query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.installation.PackageQueryInspector[] getPackageTerms() {
        return (new org.osid.installation.PackageQueryInspector[0]);
    }


    /**
     *  Gets the Package column name.
     *
     * @return the column name
     */

    protected String getPackageColumn() {
        return ("package");
    }


    /**
     *  Sets the depot <code> Id </code> for this query to match depots that 
     *  have the specified depot as an ancestor. 
     *
     *  @param  depotId a depot <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> depotId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorDepotId(org.osid.id.Id depotId, boolean match) {
        getAssembler().addIdTerm(getAncestorDepotIdColumn(), depotId, match);
        return;
    }


    /**
     *  Clears the ancestor depot <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorDepotIdTerms() {
        getAssembler().clearTerms(getAncestorDepotIdColumn());
        return;
    }


    /**
     *  Gets the ancestor depot <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorDepotIdTerms() {
        return (getAssembler().getIdTerms(getAncestorDepotIdColumn()));
    }


    /**
     *  Gets the AncestorDepotId column name.
     *
     * @return the column name
     */

    protected String getAncestorDepotIdColumn() {
        return ("ancestor_depot_id");
    }


    /**
     *  Tests if a <code> DepotQuery </code> is available. 
     *
     *  @return <code> true </code> if a depot query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorDepotQuery() {
        return (false);
    }


    /**
     *  Gets the query for a Depot. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the depot query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorDepotQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.DepotQuery getAncestorDepotQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorDepotQuery() is false");
    }


    /**
     *  Matches depots with any ancestor. 
     *
     *  @param  match <code> true </code> to match depots with any ancestor, 
     *          <code> false </code> to match root depots 
     */

    @OSID @Override
    public void matchAnyAncestorDepot(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorDepotColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor depot query terms. 
     */

    @OSID @Override
    public void clearAncestorDepotTerms() {
        getAssembler().clearTerms(getAncestorDepotColumn());
        return;
    }


    /**
     *  Gets the ancestor depot query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.installation.DepotQueryInspector[] getAncestorDepotTerms() {
        return (new org.osid.installation.DepotQueryInspector[0]);
    }


    /**
     *  Gets the AncestorDepot column name.
     *
     * @return the column name
     */

    protected String getAncestorDepotColumn() {
        return ("ancestor_depot");
    }


    /**
     *  Sets the depot <code> Id </code> for this query to match depots that 
     *  have the specified depot as a descendant. 
     *
     *  @param  depotId a depot <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> depotId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantDepotId(org.osid.id.Id depotId, boolean match) {
        getAssembler().addIdTerm(getDescendantDepotIdColumn(), depotId, match);
        return;
    }


    /**
     *  Clears the descendant depot <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantDepotIdTerms() {
        getAssembler().clearTerms(getDescendantDepotIdColumn());
        return;
    }


    /**
     *  Gets the descendant depot <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantDepotIdTerms() {
        return (getAssembler().getIdTerms(getDescendantDepotIdColumn()));
    }


    /**
     *  Gets the DescendantDepotId column name.
     *
     * @return the column name
     */

    protected String getDescendantDepotIdColumn() {
        return ("descendant_depot_id");
    }


    /**
     *  Tests if a <code> DepotQuery </code> is available. 
     *
     *  @return <code> true </code> if a depot query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantDepotQuery() {
        return (false);
    }


    /**
     *  Gets the query for a Depot. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the depot query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantDepotQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.DepotQuery getDescendantDepotQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantDepotQuery() is false");
    }


    /**
     *  Matches depots with any descendant. 
     *
     *  @param  match <code> true </code> to match depots with any descendant, 
     *          <code> false </code> to match leaf depots 
     */

    @OSID @Override
    public void matchAnyDescendantDepot(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantDepotColumn(), match);
        return;
    }


    /**
     *  Clears the descendant depot query terms. 
     */

    @OSID @Override
    public void clearDescendantDepotTerms() {
        getAssembler().clearTerms(getDescendantDepotColumn());
        return;
    }


    /**
     *  Gets the descendant depot query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.installation.DepotQueryInspector[] getDescendantDepotTerms() {
        return (new org.osid.installation.DepotQueryInspector[0]);
    }


    /**
     *  Gets the DescendantDepot column name.
     *
     * @return the column name
     */

    protected String getDescendantDepotColumn() {
        return ("descendant_depot");
    }


    /**
     *  Tests if this depot supports the given record
     *  <code>Type</code>.
     *
     *  @param  depotRecordType a depot record type 
     *  @return <code>true</code> if the depotRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>depotRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type depotRecordType) {
        for (org.osid.installation.records.DepotQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(depotRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  depotRecordType the depot record type 
     *  @return the depot query record 
     *  @throws org.osid.NullArgumentException
     *          <code>depotRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(depotRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.installation.records.DepotQueryRecord getDepotQueryRecord(org.osid.type.Type depotRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.installation.records.DepotQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(depotRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(depotRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  depotRecordType the depot record type 
     *  @return the depot query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>depotRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(depotRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.installation.records.DepotQueryInspectorRecord getDepotQueryInspectorRecord(org.osid.type.Type depotRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.installation.records.DepotQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(depotRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(depotRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param depotRecordType the depot record type
     *  @return the depot search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>depotRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(depotRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.installation.records.DepotSearchOrderRecord getDepotSearchOrderRecord(org.osid.type.Type depotRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.installation.records.DepotSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(depotRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(depotRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this depot. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param depotQueryRecord the depot query record
     *  @param depotQueryInspectorRecord the depot query inspector
     *         record
     *  @param depotSearchOrderRecord the depot search order record
     *  @param depotRecordType depot record type
     *  @throws org.osid.NullArgumentException
     *          <code>depotQueryRecord</code>,
     *          <code>depotQueryInspectorRecord</code>,
     *          <code>depotSearchOrderRecord</code> or
     *          <code>depotRecordTypedepot</code> is
     *          <code>null</code>
     */
            
    protected void addDepotRecords(org.osid.installation.records.DepotQueryRecord depotQueryRecord, 
                                      org.osid.installation.records.DepotQueryInspectorRecord depotQueryInspectorRecord, 
                                      org.osid.installation.records.DepotSearchOrderRecord depotSearchOrderRecord, 
                                      org.osid.type.Type depotRecordType) {

        addRecordType(depotRecordType);

        nullarg(depotQueryRecord, "depot query record");
        nullarg(depotQueryInspectorRecord, "depot query inspector record");
        nullarg(depotSearchOrderRecord, "depot search odrer record");

        this.queryRecords.add(depotQueryRecord);
        this.queryInspectorRecords.add(depotQueryInspectorRecord);
        this.searchOrderRecords.add(depotSearchOrderRecord);
        
        return;
    }
}
