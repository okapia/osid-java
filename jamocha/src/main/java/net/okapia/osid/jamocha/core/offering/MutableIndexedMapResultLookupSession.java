//
// MutableIndexedMapResultLookupSession
//
//    Implements a Result lookup service backed by a collection of
//    results indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.offering;


/**
 *  Implements a Result lookup service backed by a collection of
 *  results. The results are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some results may be compatible
 *  with more types than are indicated through these result
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of results can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapResultLookupSession
    extends net.okapia.osid.jamocha.core.offering.spi.AbstractIndexedMapResultLookupSession
    implements org.osid.offering.ResultLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapResultLookupSession} with no results.
     *
     *  @param catalogue the catalogue
     *  @throws org.osid.NullArgumentException {@code catalogue}
     *          is {@code null}
     */

      public MutableIndexedMapResultLookupSession(org.osid.offering.Catalogue catalogue) {
        setCatalogue(catalogue);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapResultLookupSession} with a
     *  single result.
     *  
     *  @param catalogue the catalogue
     *  @param  result a single result
     *  @throws org.osid.NullArgumentException {@code catalogue} or
     *          {@code result} is {@code null}
     */

    public MutableIndexedMapResultLookupSession(org.osid.offering.Catalogue catalogue,
                                                  org.osid.offering.Result result) {
        this(catalogue);
        putResult(result);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapResultLookupSession} using an
     *  array of results.
     *
     *  @param catalogue the catalogue
     *  @param  results an array of results
     *  @throws org.osid.NullArgumentException {@code catalogue} or
     *          {@code results} is {@code null}
     */

    public MutableIndexedMapResultLookupSession(org.osid.offering.Catalogue catalogue,
                                                  org.osid.offering.Result[] results) {
        this(catalogue);
        putResults(results);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapResultLookupSession} using a
     *  collection of results.
     *
     *  @param catalogue the catalogue
     *  @param  results a collection of results
     *  @throws org.osid.NullArgumentException {@code catalogue} or
     *          {@code results} is {@code null}
     */

    public MutableIndexedMapResultLookupSession(org.osid.offering.Catalogue catalogue,
                                                  java.util.Collection<? extends org.osid.offering.Result> results) {

        this(catalogue);
        putResults(results);
        return;
    }
    

    /**
     *  Makes a {@code Result} available in this session.
     *
     *  @param  result a result
     *  @throws org.osid.NullArgumentException {@code result{@code  is
     *          {@code null}
     */

    @Override
    public void putResult(org.osid.offering.Result result) {
        super.putResult(result);
        return;
    }


    /**
     *  Makes an array of results available in this session.
     *
     *  @param  results an array of results
     *  @throws org.osid.NullArgumentException {@code results{@code 
     *          is {@code null}
     */

    @Override
    public void putResults(org.osid.offering.Result[] results) {
        super.putResults(results);
        return;
    }


    /**
     *  Makes collection of results available in this session.
     *
     *  @param  results a collection of results
     *  @throws org.osid.NullArgumentException {@code result{@code  is
     *          {@code null}
     */

    @Override
    public void putResults(java.util.Collection<? extends org.osid.offering.Result> results) {
        super.putResults(results);
        return;
    }


    /**
     *  Removes a Result from this session.
     *
     *  @param resultId the {@code Id} of the result
     *  @throws org.osid.NullArgumentException {@code resultId{@code  is
     *          {@code null}
     */

    @Override
    public void removeResult(org.osid.id.Id resultId) {
        super.removeResult(resultId);
        return;
    }    
}
