//
// AbstractAdapterFrontOfficeLookupSession.java
//
//    A FrontOffice lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.tracking.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A FrontOffice lookup session adapter.
 */

public abstract class AbstractAdapterFrontOfficeLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.tracking.FrontOfficeLookupSession {

    private final org.osid.tracking.FrontOfficeLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterFrontOfficeLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterFrontOfficeLookupSession(org.osid.tracking.FrontOfficeLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@code FrontOffice} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupFrontOffices() {
        return (this.session.canLookupFrontOffices());
    }


    /**
     *  A complete view of the {@code FrontOffice} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeFrontOfficeView() {
        this.session.useComparativeFrontOfficeView();
        return;
    }


    /**
     *  A complete view of the {@code FrontOffice} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryFrontOfficeView() {
        this.session.usePlenaryFrontOfficeView();
        return;
    }

     
    /**
     *  Gets the {@code FrontOffice} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code FrontOffice} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code FrontOffice} and
     *  retained for compatibility.
     *
     *  @param frontOfficeId {@code Id} of the {@code FrontOffice}
     *  @return the front office
     *  @throws org.osid.NotFoundException {@code frontOfficeId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code frontOfficeId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.FrontOffice getFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getFrontOffice(frontOfficeId));
    }


    /**
     *  Gets a {@code FrontOfficeList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  frontOffices specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code FrontOffices} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  frontOfficeIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code FrontOffice} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code frontOfficeIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeList getFrontOfficesByIds(org.osid.id.IdList frontOfficeIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getFrontOfficesByIds(frontOfficeIds));
    }


    /**
     *  Gets a {@code FrontOfficeList} corresponding to the given
     *  front office genus {@code Type} which does not include
     *  front offices of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  front offices or an error results. Otherwise, the returned list
     *  may contain only those front offices that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  frontOfficeGenusType a frontOffice genus type 
     *  @return the returned {@code FrontOffice} list
     *  @throws org.osid.NullArgumentException
     *          {@code frontOfficeGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeList getFrontOfficesByGenusType(org.osid.type.Type frontOfficeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getFrontOfficesByGenusType(frontOfficeGenusType));
    }


    /**
     *  Gets a {@code FrontOfficeList} corresponding to the given
     *  front office genus {@code Type} and include any additional
     *  front offices with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  front offices or an error results. Otherwise, the returned list
     *  may contain only those front offices that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  frontOfficeGenusType a frontOffice genus type 
     *  @return the returned {@code FrontOffice} list
     *  @throws org.osid.NullArgumentException
     *          {@code frontOfficeGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeList getFrontOfficesByParentGenusType(org.osid.type.Type frontOfficeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getFrontOfficesByParentGenusType(frontOfficeGenusType));
    }


    /**
     *  Gets a {@code FrontOfficeList} containing the given
     *  front office record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  front offices or an error results. Otherwise, the returned list
     *  may contain only those front offices that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  frontOfficeRecordType a frontOffice record type 
     *  @return the returned {@code FrontOffice} list
     *  @throws org.osid.NullArgumentException
     *          {@code frontOfficeRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeList getFrontOfficesByRecordType(org.osid.type.Type frontOfficeRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getFrontOfficesByRecordType(frontOfficeRecordType));
    }


    /**
     *  Gets a {@code FrontOfficeList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  front offices or an error results. Otherwise, the returned list
     *  may contain only those front offices that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code FrontOffice} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeList getFrontOfficesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getFrontOfficesByProvider(resourceId));
    }


    /**
     *  Gets all {@code FrontOffices}. 
     *
     *  In plenary mode, the returned list contains all known
     *  front offices or an error results. Otherwise, the returned list
     *  may contain only those front offices that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code FrontOffices} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeList getFrontOffices()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getFrontOffices());
    }
}
