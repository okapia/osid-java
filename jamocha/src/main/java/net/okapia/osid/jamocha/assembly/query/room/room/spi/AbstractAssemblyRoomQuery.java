//
// AbstractAssemblyRoomQuery.java
//
//     A RoomQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.room.room.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A RoomQuery that stores terms.
 */

public abstract class AbstractAssemblyRoomQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyTemporalOsidObjectQuery
    implements org.osid.room.RoomQuery,
               org.osid.room.RoomQueryInspector,
               org.osid.room.RoomSearchOrder {

    private final java.util.Collection<org.osid.room.records.RoomQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.room.records.RoomQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.room.records.RoomSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyRoomQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyRoomQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the building <code> Id </code> for this query to match rooms 
     *  assigned to buildings. 
     *
     *  @param  buildingId a building <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> buildingId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBuildingId(org.osid.id.Id buildingId, boolean match) {
        getAssembler().addIdTerm(getBuildingIdColumn(), buildingId, match);
        return;
    }


    /**
     *  Clears the building <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBuildingIdTerms() {
        getAssembler().clearTerms(getBuildingIdColumn());
        return;
    }


    /**
     *  Gets the building <code> Id </code> terms. 
     *
     *  @return the building <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBuildingIdTerms() {
        return (getAssembler().getIdTerms(getBuildingIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the building. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByBuilding(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getBuildingColumn(), style);
        return;
    }


    /**
     *  Gets the BuildingId column name.
     *
     * @return the column name
     */

    protected String getBuildingIdColumn() {
        return ("building_id");
    }


    /**
     *  Tests if a <code> BuildingQuery </code> is available. 
     *
     *  @return <code> true </code> if a building query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBuildingQuery() {
        return (false);
    }


    /**
     *  Gets the query for a building query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the building query 
     *  @throws org.osid.UnimplementedException <code> supportsBuildingQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingQuery getBuildingQuery() {
        throw new org.osid.UnimplementedException("supportsBuildingQuery() is false");
    }


    /**
     *  Clears the building terms. 
     */

    @OSID @Override
    public void clearBuildingTerms() {
        getAssembler().clearTerms(getBuildingColumn());
        return;
    }


    /**
     *  Gets the building terms. 
     *
     *  @return the building terms 
     */

    @OSID @Override
    public org.osid.room.BuildingQueryInspector[] getBuildingTerms() {
        return (new org.osid.room.BuildingQueryInspector[0]);
    }


    /**
     *  Tests if a building search order is available. 
     *
     *  @return <code> true </code> if a building search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBuildingSearchOrder() {
        return (false);
    }


    /**
     *  Gets the building search order. 
     *
     *  @return the building search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBuildingSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingSearchOrder getBuildingSearchOrder() {
        throw new org.osid.UnimplementedException("supportsBuildingSearchOrder() is false");
    }


    /**
     *  Gets the Building column name.
     *
     * @return the column name
     */

    protected String getBuildingColumn() {
        return ("building");
    }


    /**
     *  Sets the floor <code> Id </code> for this query to match rooms 
     *  assigned to campuses. 
     *
     *  @param  floorId a floor <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> floorId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchFloorId(org.osid.id.Id floorId, boolean match) {
        getAssembler().addIdTerm(getFloorIdColumn(), floorId, match);
        return;
    }


    /**
     *  Clears the floor <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearFloorIdTerms() {
        getAssembler().clearTerms(getFloorIdColumn());
        return;
    }


    /**
     *  Gets the floor <code> Id </code> terms. 
     *
     *  @return the floor <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getFloorIdTerms() {
        return (getAssembler().getIdTerms(getFloorIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the floor. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByFloor(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getFloorColumn(), style);
        return;
    }


    /**
     *  Gets the FloorId column name.
     *
     * @return the column name
     */

    protected String getFloorIdColumn() {
        return ("floor_id");
    }


    /**
     *  Tests if a floor query is available. 
     *
     *  @return <code> true </code> if a room query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFloorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a floor. 
     *
     *  @return the floor query 
     *  @throws org.osid.UnimplementedException <code> supportsFloorQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.FloorQuery getFloorQuery() {
        throw new org.osid.UnimplementedException("supportsFloorQuery() is false");
    }


    /**
     *  Clears the floor terms. 
     */

    @OSID @Override
    public void clearFloorTerms() {
        getAssembler().clearTerms(getFloorColumn());
        return;
    }


    /**
     *  Gets the floor terms. 
     *
     *  @return the floor terms 
     */

    @OSID @Override
    public org.osid.room.FloorQueryInspector[] getFloorTerms() {
        return (new org.osid.room.FloorQueryInspector[0]);
    }


    /**
     *  Tests if a floor order is available. 
     *
     *  @return <code> true </code> if a floor search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFloorSearchOrder() {
        return (false);
    }


    /**
     *  Gets the floor search order. 
     *
     *  @return the floor search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFloorSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.FloorSearchOrder getFloorSearchOrder() {
        throw new org.osid.UnimplementedException("supportsFloorSearchOrder() is false");
    }


    /**
     *  Gets the Floor column name.
     *
     * @return the column name
     */

    protected String getFloorColumn() {
        return ("floor");
    }


    /**
     *  Sets an enclosing room <code> Id. </code> 
     *
     *  @param  roomId an enclosing room <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> roomId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchEnclosingRoomId(org.osid.id.Id roomId, boolean match) {
        getAssembler().addIdTerm(getEnclosingRoomIdColumn(), roomId, match);
        return;
    }


    /**
     *  Clears the enclosing room <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearEnclosingRoomIdTerms() {
        getAssembler().clearTerms(getEnclosingRoomIdColumn());
        return;
    }


    /**
     *  Gets the enclosing room <code> Id </code> terms. 
     *
     *  @return the enclosing room <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEnclosingRoomIdTerms() {
        return (getAssembler().getIdTerms(getEnclosingRoomIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the enclosing 
     *  room. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByEnclosingRoom(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getEnclosingRoomColumn(), style);
        return;
    }


    /**
     *  Gets the EnclosingRoomId column name.
     *
     * @return the column name
     */

    protected String getEnclosingRoomIdColumn() {
        return ("enclosing_room_id");
    }


    /**
     *  Tests if an <code> EnclosingRoomQuery </code> is available. 
     *
     *  @return <code> true </code> if an enclosing room query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEnclosingRoomQuery() {
        return (false);
    }


    /**
     *  Gets the query for an enclosing room query. Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the room query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEnclosingRoomQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomQuery getEnclosingRoomQuery() {
        throw new org.osid.UnimplementedException("supportsEnclosingRoomQuery() is false");
    }


    /**
     *  Matches any enclosing room. 
     *
     *  @param  match <code> true </code> for a to match any room enclosed in 
     *          another room,, <code> false </code> otherwise 
     */

    @OSID @Override
    public void matchAnyEnclosingRoom(boolean match) {
        getAssembler().addIdWildcardTerm(getEnclosingRoomColumn(), match);
        return;
    }


    /**
     *  Clears the enclosing room terms. 
     */

    @OSID @Override
    public void clearEnclosingRoomTerms() {
        getAssembler().clearTerms(getEnclosingRoomColumn());
        return;
    }


    /**
     *  Gets the enclosing room terms. 
     *
     *  @return the enclosing room terms 
     */

    @OSID @Override
    public org.osid.room.RoomQueryInspector[] getEnclosingRoomTerms() {
        return (new org.osid.room.RoomQueryInspector[0]);
    }


    /**
     *  Tests if a room search order is available. 
     *
     *  @return <code> true </code> if a room order is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEnclosingRoomSearchOrder() {
        return (false);
    }


    /**
     *  Gets the room search order. 
     *
     *  @return the enclosing room search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEnclosingRoomSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.room.RoomSearchOrder getEnclosingRoomSearchOrder() {
        throw new org.osid.UnimplementedException("supportsEnclosingRoomSearchOrder() is false");
    }


    /**
     *  Gets the EnclosingRoom column name.
     *
     * @return the column name
     */

    protected String getEnclosingRoomColumn() {
        return ("enclosing_room");
    }


    /**
     *  Sets a subdivision room <code> Id. </code> 
     *
     *  @param  roomId a subdivision room <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> roomId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchSubdivisionId(org.osid.id.Id roomId, boolean match) {
        getAssembler().addIdTerm(getSubdivisionIdColumn(), roomId, match);
        return;
    }


    /**
     *  Clears the subdivision room <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSubdivisionIdTerms() {
        getAssembler().clearTerms(getSubdivisionIdColumn());
        return;
    }


    /**
     *  Gets the subdivision room <code> Id </code> terms. 
     *
     *  @return the subdivision room <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSubdivisionIdTerms() {
        return (getAssembler().getIdTerms(getSubdivisionIdColumn()));
    }


    /**
     *  Gets the SubdivisionId column name.
     *
     * @return the column name
     */

    protected String getSubdivisionIdColumn() {
        return ("subdivision_id");
    }


    /**
     *  Tests if a <code> RoomQuery </code> is available. 
     *
     *  @return <code> true </code> if a subdivision room query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubdivisionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a subdivision room query. Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the room query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubdivisionRoomQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomQuery getSubdivisionQuery() {
        throw new org.osid.UnimplementedException("supportsSubdivisionQuery() is false");
    }


    /**
     *  Matches any subdivision room. 
     *
     *  @param  match <code> true </code> for a to match any room containg 
     *          another room,, <code> false </code> otherwise 
     */

    @OSID @Override
    public void matchAnySubdivision(boolean match) {
        getAssembler().addIdWildcardTerm(getSubdivisionColumn(), match);
        return;
    }


    /**
     *  Clears the subdivision room terms. 
     */

    @OSID @Override
    public void clearSubdivisionTerms() {
        getAssembler().clearTerms(getSubdivisionColumn());
        return;
    }


    /**
     *  Gets the subdivision room terms. 
     *
     *  @return the subdivision room terms 
     */

    @OSID @Override
    public org.osid.room.RoomQueryInspector[] getSubdivisionTerms() {
        return (new org.osid.room.RoomQueryInspector[0]);
    }


    /**
     *  Gets the Subdivision column name.
     *
     * @return the column name
     */

    protected String getSubdivisionColumn() {
        return ("subdivision");
    }


    /**
     *  Sets a name. 
     *
     *  @param  name an official name 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> name </code> is not 
     *          of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> name </code> is <code> 
     *          null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchDesignatedName(String name, 
                                    org.osid.type.Type stringMatchType, 
                                    boolean match) {
        getAssembler().addStringTerm(getDesignatedNameColumn(), name, stringMatchType, match);
        return;
    }


    /**
     *  Matches any designated name. 
     *
     *  @param  match <code> true </code> to match rooms with any designated 
     *          name, <code> false </code> to match rooms with no designated 
     *          name 
     */

    @OSID @Override
    public void matchAnyDesignatedName(boolean match) {
        getAssembler().addStringWildcardTerm(getDesignatedNameColumn(), match);
        return;
    }


    /**
     *  Clears the designated name terms. 
     */

    @OSID @Override
    public void clearDesignatedNameTerms() {
        getAssembler().clearTerms(getDesignatedNameColumn());
        return;
    }


    /**
     *  Gets the offical name terms. 
     *
     *  @return the name terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getDesignatedNameTerms() {
        return (getAssembler().getStringTerms(getDesignatedNameColumn()));
    }


    /**
     *  Gets the DesignatedName column name.
     *
     * @return the column name
     */

    protected String getDesignatedNameColumn() {
        return ("designated_name");
    }


    /**
     *  Sets a room number. 
     *
     *  @param  number a number 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> number </code> is not 
     *          of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> number </code> is <code> 
     *          null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchRoomNumber(String number, 
                                org.osid.type.Type stringMatchType, 
                                boolean match) {
        getAssembler().addStringTerm(getRoomNumberColumn(), number, stringMatchType, match);
        return;
    }


    /**
     *  Matches any room number. 
     *
     *  @param  match <code> true </code> to match rooms with any number, 
     *          <code> false </code> to match rooms with no number 
     */

    @OSID @Override
    public void matchAnyRoomNumber(boolean match) {
        getAssembler().addStringWildcardTerm(getRoomNumberColumn(), match);
        return;
    }


    /**
     *  Clears the room number terms. 
     */

    @OSID @Override
    public void clearRoomNumberTerms() {
        getAssembler().clearTerms(getRoomNumberColumn());
        return;
    }


    /**
     *  Gets the room building number terms. 
     *
     *  @return the room number terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getRoomNumberTerms() {
        return (getAssembler().getStringTerms(getRoomNumberColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the room number. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRoomNumber(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getRoomNumberColumn(), style);
        return;
    }


    /**
     *  Gets the RoomNumber column name.
     *
     * @return the column name
     */

    protected String getRoomNumberColumn() {
        return ("room_number");
    }


    /**
     *  Sets a room code. 
     *
     *  @param  code a room code 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> code </code> is not 
     *          of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> code </code> is <code> 
     *          null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchCode(String code, org.osid.type.Type stringMatchType, 
                          boolean match) {
        getAssembler().addStringTerm(getCodeColumn(), code, stringMatchType, match);
        return;
    }


    /**
     *  Matches any room code. 
     *
     *  @param  match <code> true </code> to match rooms with any code, <code> 
     *          false </code> to match rooms with no code 
     */

    @OSID @Override
    public void matchAnyCode(boolean match) {
        getAssembler().addStringWildcardTerm(getCodeColumn(), match);
        return;
    }


    /**
     *  Clears the code terms. 
     */

    @OSID @Override
    public void clearCodeTerms() {
        getAssembler().clearTerms(getCodeColumn());
        return;
    }


    /**
     *  Gets the room code number terms. 
     *
     *  @return the room code terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getCodeTerms() {
        return (getAssembler().getStringTerms(getCodeColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the room code. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCode(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCodeColumn(), style);
        return;
    }


    /**
     *  Gets the Code column name.
     *
     * @return the column name
     */

    protected String getCodeColumn() {
        return ("code");
    }


    /**
     *  Matches an area within the given range inclusive. 
     *
     *  @param  low start of range 
     *  @param  high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     *  @throws org.osid.NullArgumentException <code> low </code> or <code> 
     *          high </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchArea(java.math.BigDecimal low, java.math.BigDecimal high, 
                          boolean match) {
        getAssembler().addDecimalRangeTerm(getAreaColumn(), low, high, match);
        return;
    }


    /**
     *  Matches any area. 
     *
     *  @param  match <code> true </code> to match rooms with any area, <code> 
     *          false </code> to match rooms with no area assigned 
     */

    @OSID @Override
    public void matchAnyArea(boolean match) {
        getAssembler().addDecimalRangeWildcardTerm(getAreaColumn(), match);
        return;
    }


    /**
     *  Clears the area terms. 
     */

    @OSID @Override
    public void clearAreaTerms() {
        getAssembler().clearTerms(getAreaColumn());
        return;
    }


    /**
     *  Gets the area terms. 
     *
     *  @return the area terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getAreaTerms() {
        return (getAssembler().getDecimalRangeTerms(getAreaColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the room area. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByArea(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getAreaColumn(), style);
        return;
    }


    /**
     *  Gets the Area column name.
     *
     * @return the column name
     */

    protected String getAreaColumn() {
        return ("area");
    }


    /**
     *  Matches an occupancy limit within the given range inclusive. 
     *
     *  @param  low start of range 
     *  @param  high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     */

    @OSID @Override
    public void matchOccupancyLimit(long low, long high, boolean match) {
        getAssembler().addCardinalRangeTerm(getOccupancyLimitColumn(), low, high, match);
        return;
    }


    /**
     *  Matches rooms with any occupancy limit. 
     *
     *  @param  match <code> true </code> to match rooms with any occupancy 
     *          limit, <code> false </code> to match rooms with no limit 
     *          assigned 
     */

    @OSID @Override
    public void matchAnyOccupancyLimit(boolean match) {
        getAssembler().addCardinalRangeWildcardTerm(getOccupancyLimitColumn(), match);
        return;
    }


    /**
     *  Clears the occupancy limit terms. 
     */

    @OSID @Override
    public void clearOccupancyLimitTerms() {
        getAssembler().clearTerms(getOccupancyLimitColumn());
        return;
    }


    /**
     *  Gets the occupancy limit terms. 
     *
     *  @return the occupancy limit terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getOccupancyLimitTerms() {
        return (getAssembler().getCardinalRangeTerms(getOccupancyLimitColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the room 
     *  occupancy limit. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByOccupancyLimit(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getOccupancyLimitColumn(), style);
        return;
    }


    /**
     *  Gets the OccupancyLimit column name.
     *
     * @return the column name
     */

    protected String getOccupancyLimitColumn() {
        return ("occupancy_limit");
    }


    /**
     *  Sets the room <code> Id </code> for this query to match resources 
     *  assigned to rooms. 
     *
     *  @param  roomId a room <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> roomId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchResourceId(org.osid.id.Id roomId, boolean match) {
        getAssembler().addIdTerm(getResourceIdColumn(), roomId, match);
        return;
    }


    /**
     *  Clears the resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearResourceIdTerms() {
        getAssembler().clearTerms(getResourceIdColumn());
        return;
    }


    /**
     *  Gets the resource <code> Id </code> terms. 
     *
     *  @return the resource <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResourceIdTerms() {
        return (getAssembler().getIdTerms(getResourceIdColumn()));
    }


    /**
     *  Gets the ResourceId column name.
     *
     * @return the column name
     */

    protected String getResourceIdColumn() {
        return ("resource_id");
    }


    /**
     *  Tests if a resource query is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a room resource. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsResourceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getResourceQuery() {
        throw new org.osid.UnimplementedException("supportsResourceQuery() is false");
    }


    /**
     *  Matches rooms with any resource. 
     *
     *  @param  match <code> true </code> to match rooms with any resource, 
     *          <code> false </code> to match rooms with no resources 
     */

    @OSID @Override
    public void matchAnyResource(boolean match) {
        getAssembler().addIdWildcardTerm(getResourceColumn(), match);
        return;
    }


    /**
     *  Clears the resource terms. 
     */

    @OSID @Override
    public void clearResourceTerms() {
        getAssembler().clearTerms(getResourceColumn());
        return;
    }


    /**
     *  Gets the resource terms. 
     *
     *  @return the resource terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getResourceTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the Resource column name.
     *
     * @return the column name
     */

    protected String getResourceColumn() {
        return ("resource");
    }


    /**
     *  Sets the building <code> Id </code> for this query to match rooms 
     *  assigned to campuses. 
     *
     *  @param  campusId a campus <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCampusId(org.osid.id.Id campusId, boolean match) {
        getAssembler().addIdTerm(getCampusIdColumn(), campusId, match);
        return;
    }


    /**
     *  Clears the campus <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCampusIdTerms() {
        getAssembler().clearTerms(getCampusIdColumn());
        return;
    }


    /**
     *  Gets the campus <code> Id </code> terms. 
     *
     *  @return the campus <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCampusIdTerms() {
        return (getAssembler().getIdTerms(getCampusIdColumn()));
    }


    /**
     *  Gets the CampusId column name.
     *
     * @return the column name
     */

    protected String getCampusIdColumn() {
        return ("campus_id");
    }


    /**
     *  Tests if a <code> CampusQuery </code> is available. 
     *
     *  @return <code> true </code> if a campus query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCampusQuery() {
        return (false);
    }


    /**
     *  Gets the query for a campus query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the campus query 
     *  @throws org.osid.UnimplementedException <code> supportsCampusQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.CampusQuery getCampusQuery() {
        throw new org.osid.UnimplementedException("supportsCampusQuery() is false");
    }


    /**
     *  Clears the campus terms. 
     */

    @OSID @Override
    public void clearCampusTerms() {
        getAssembler().clearTerms(getCampusColumn());
        return;
    }


    /**
     *  Gets the campus terms. 
     *
     *  @return the campus terms 
     */

    @OSID @Override
    public org.osid.room.CampusQueryInspector[] getCampusTerms() {
        return (new org.osid.room.CampusQueryInspector[0]);
    }


    /**
     *  Gets the Campus column name.
     *
     * @return the column name
     */

    protected String getCampusColumn() {
        return ("campus");
    }


    /**
     *  Tests if this room supports the given record
     *  <code>Type</code>.
     *
     *  @param  roomRecordType a room record type 
     *  @return <code>true</code> if the roomRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>roomRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type roomRecordType) {
        for (org.osid.room.records.RoomQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(roomRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  roomRecordType the room record type 
     *  @return the room query record 
     *  @throws org.osid.NullArgumentException
     *          <code>roomRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(roomRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.room.records.RoomQueryRecord getRoomQueryRecord(org.osid.type.Type roomRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.room.records.RoomQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(roomRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(roomRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  roomRecordType the room record type 
     *  @return the room query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>roomRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(roomRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.room.records.RoomQueryInspectorRecord getRoomQueryInspectorRecord(org.osid.type.Type roomRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.room.records.RoomQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(roomRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(roomRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param roomRecordType the room record type
     *  @return the room search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>roomRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(roomRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.room.records.RoomSearchOrderRecord getRoomSearchOrderRecord(org.osid.type.Type roomRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.room.records.RoomSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(roomRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(roomRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this room. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param roomQueryRecord the room query record
     *  @param roomQueryInspectorRecord the room query inspector
     *         record
     *  @param roomSearchOrderRecord the room search order record
     *  @param roomRecordType room record type
     *  @throws org.osid.NullArgumentException
     *          <code>roomQueryRecord</code>,
     *          <code>roomQueryInspectorRecord</code>,
     *          <code>roomSearchOrderRecord</code> or
     *          <code>roomRecordTyperoom</code> is
     *          <code>null</code>
     */
            
    protected void addRoomRecords(org.osid.room.records.RoomQueryRecord roomQueryRecord, 
                                      org.osid.room.records.RoomQueryInspectorRecord roomQueryInspectorRecord, 
                                      org.osid.room.records.RoomSearchOrderRecord roomSearchOrderRecord, 
                                      org.osid.type.Type roomRecordType) {

        addRecordType(roomRecordType);

        nullarg(roomQueryRecord, "room query record");
        nullarg(roomQueryInspectorRecord, "room query inspector record");
        nullarg(roomSearchOrderRecord, "room search odrer record");

        this.queryRecords.add(roomQueryRecord);
        this.queryInspectorRecords.add(roomQueryInspectorRecord);
        this.searchOrderRecords.add(roomSearchOrderRecord);
        
        return;
    }
}
