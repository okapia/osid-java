//
// AbstractQueryProjectLookupSession.java
//
//    An inline adapter that maps a ProjectLookupSession to
//    a ProjectQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.room.construction.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a ProjectLookupSession to
 *  a ProjectQuerySession.
 */

public abstract class AbstractQueryProjectLookupSession
    extends net.okapia.osid.jamocha.room.construction.spi.AbstractProjectLookupSession
    implements org.osid.room.construction.ProjectLookupSession {

    private boolean effectiveonly = false;
    private final org.osid.room.construction.ProjectQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryProjectLookupSession.
     *
     *  @param querySession the underlying project query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryProjectLookupSession(org.osid.room.construction.ProjectQuerySession querySession) {
        nullarg(querySession, "project query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Campus</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Campus Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCampusId() {
        return (this.session.getCampusId());
    }


    /**
     *  Gets the <code>Campus</code> associated with this 
     *  session.
     *
     *  @return the <code>Campus</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.Campus getCampus()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getCampus());
    }


    /**
     *  Tests if this user can perform <code>Project</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupProjects() {
        return (this.session.canSearchProjects());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include projects in campuses which are children
     *  of this campus in the campus hierarchy.
     */

    @OSID @Override
    public void useFederatedCampusView() {
        this.session.useFederatedCampusView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this campus only.
     */

    @OSID @Override
    public void useIsolatedCampusView() {
        this.session.useIsolatedCampusView();
        return;
    }
    

    /**
     *  Only projects whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveProjectView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All projects of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveProjectView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Project</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Project</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Project</code> and
     *  retained for compatibility.
     *
     *  In effective mode, projects are returned that are currently
     *  effective.  In any effective mode, effective projects and
     *  those currently expired are returned.
     *
     *  @param  projectId <code>Id</code> of the
     *          <code>Project</code>
     *  @return the project
     *  @throws org.osid.NotFoundException <code>projectId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>projectId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.construction.Project getProject(org.osid.id.Id projectId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.construction.ProjectQuery query = getQuery();
        query.matchId(projectId, true);
        org.osid.room.construction.ProjectList projects = this.session.getProjectsByQuery(query);
        if (projects.hasNext()) {
            return (projects.getNextProject());
        } 
        
        throw new org.osid.NotFoundException(projectId + " not found");
    }


    /**
     *  Gets a <code>ProjectList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  projects specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Projects</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, projects are returned that are currently effective.
     *  In any effective mode, effective projects and those currently expired
     *  are returned.
     *
     *  @param  projectIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Project</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>projectIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectList getProjectsByIds(org.osid.id.IdList projectIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.construction.ProjectQuery query = getQuery();

        try (org.osid.id.IdList ids = projectIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getProjectsByQuery(query));
    }


    /**
     *  Gets a <code>ProjectList</code> corresponding to the given
     *  project genus <code>Type</code> which does not include
     *  projects of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  projects or an error results. Otherwise, the returned list
     *  may contain only those projects that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, projects are returned that are currently effective.
     *  In any effective mode, effective projects and those currently expired
     *  are returned.
     *
     *  @param  projectGenusType a project genus type 
     *  @return the returned <code>Project</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>projectGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectList getProjectsByGenusType(org.osid.type.Type projectGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.construction.ProjectQuery query = getQuery();
        query.matchGenusType(projectGenusType, true);
        return (this.session.getProjectsByQuery(query));
    }


    /**
     *  Gets a <code>ProjectList</code> corresponding to the given
     *  project genus <code>Type</code> and include any additional
     *  projects with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  projects or an error results. Otherwise, the returned list
     *  may contain only those projects that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, projects are returned that are currently
     *  effective.  In any effective mode, effective projects and
     *  those currently expired are returned.
     *
     *  @param  projectGenusType a project genus type 
     *  @return the returned <code>Project</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>projectGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectList getProjectsByParentGenusType(org.osid.type.Type projectGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.construction.ProjectQuery query = getQuery();
        query.matchParentGenusType(projectGenusType, true);
        return (this.session.getProjectsByQuery(query));
    }


    /**
     *  Gets a <code>ProjectList</code> containing the given
     *  project record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  projects or an error results. Otherwise, the returned list
     *  may contain only those projects that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, projects are returned that are currently
     *  effective.  In any effective mode, effective projects and
     *  those currently expired are returned.
     *
     *  @param  projectRecordType a project record type 
     *  @return the returned <code>Project</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>projectRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectList getProjectsByRecordType(org.osid.type.Type projectRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.construction.ProjectQuery query = getQuery();
        query.matchRecordType(projectRecordType, true);
        return (this.session.getProjectsByQuery(query));
    }


    /**
     *  Gets a <code>ProjectList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  projects or an error results. Otherwise, the returned list
     *  may contain only those projects that are accessible
     *  through this session.
     *  
     *  In effective mode, projects are returned that are currently
     *  effective.  In any effective mode, effective projects and
     *  those currently expired are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Project</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.room.construction.ProjectList getProjectsOnDate(org.osid.calendaring.DateTime from, 
                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.construction.ProjectQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getProjectsByQuery(query));
    }
        

    /**
     *  Gets a list of all projects with a genus type and effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known projects
     *  or an error results. Otherwise, the returned list may contain
     *  only those projects that are accessible through this session.
     *  
     *  In effective mode, projects are returned that are currently
     *  effective.  In any effective mode, effective projects and
     *  those currently expired are returned.
     *
     *  @param  projectGenusType a project genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code> Project </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code>
     *          projectGenusType, from </code> or <code> to </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectList getProjectsByGenusTypeOnDate(org.osid.type.Type projectGenusType, 
                                                                               org.osid.calendaring.DateTime from, 
                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.construction.ProjectQuery query = getQuery();
        query.matchGenusType(projectGenusType, true);
        query.matchDate(from, to, true);
        return (this.session.getProjectsByQuery(query));
    }


    /**
     *  Gets a <code> ProjectList </code> containing the given
     *  building.
     *  
     *  In plenary mode, the returned list contains all known projects
     *  or an error results. Otherwise, the returned list may contain
     *  only those projects that are accessible through this session.
     *  
     *  In effective mode, projects are returned that are currently
     *  effective.  In any effective mode, effective projects and
     *  those currently expired are returned.
     *
     *  @param  buildingId a building <code> Id </code> 
     *  @return the returned <code> Project </code> list 
     *  @throws org.osid.NullArgumentException <code> buildingId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectList getProjectsForBuilding(org.osid.id.Id buildingId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.construction.ProjectQuery query = getQuery();
        query.matchBuildingId(buildingId, true);
        return (this.session.getProjectsByQuery(query));
    }

    
    /**
     *  Gets a <code> ProjectList </code> containing the given
     *  building and genus type.
     *  
     *  In plenary mode, the returned list contains all known projects
     *  or an error results. Otherwise, the returned list may contain
     *  only those projects that are accessible through this session.
     *  
     *  In effective mode, projects are returned that are currently
     *  effective.  In any effective mode, effective projects and
     *  those currently expired are returned.
     *
     *  @param  buildingId a building <code> Id </code> 
     *  @param  projectGenusType a project genus type 
     *  @return the returned <code> Project </code> list 
     *  @throws org.osid.NullArgumentException <code> buildingId
     *          </code> or <code> projectGenusType </code> is <code>
     *          null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectList getProjectsByGenusTypeForBuilding(org.osid.id.Id buildingId, 
                                                                                    org.osid.type.Type projectGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.construction.ProjectQuery query = getQuery();
        query.matchBuildingId(buildingId, true);
        query.matchGenusType(projectGenusType, true);
        return (this.session.getProjectsByQuery(query));
    }


    /**
     *  Gets a list of all projects for a building effective during
     *  the entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known projects
     *  or an error results. Otherwise, the returned list may contain
     *  only those projects that are accessible through this session.
     *  
     *  In effective mode, projects are returned that are currently
     *  effective.  In any effective mode, effective projects and
     *  those currently expired are returned.
     *
     *  @param  buildingId a building <code> Id </code> 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code> Project </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> buildingId, from
     *          </code> or <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectList getProjectsForBuildingOnDate(org.osid.id.Id buildingId, 
                                                                               org.osid.calendaring.DateTime from, 
                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.construction.ProjectQuery query = getQuery();
        query.matchBuildingId(buildingId, true);
        query.matchDate(from, to, true);
        return (this.session.getProjectsByQuery(query));
    }


    /**
     *  Gets a list of all projects for a building with a genus type
     *  and effective during the entire given date range inclusive but
     *  not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known projects
     *  or an error results. Otherwise, the returned list may contain
     *  only those projects that are accessible through this session.
     *  
     *  In effective mode, projects are returned that are currently
     *  effective.  In any effective mode, effective projects and
     *  those currently expired are returned.
     *
     *  @param  buildingId a building <code> Id </code> 
     *  @param  projectGenusType a project genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code> Project </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> buildingId,
     *          projectGenusType, from </code> or <code> to </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectList getProjectsByGenusTypeForBuildingOnDate(org.osid.id.Id buildingId, 
                                                                                          org.osid.type.Type projectGenusType, 
                                                                                          org.osid.calendaring.DateTime from, 
                                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.construction.ProjectQuery query = getQuery();
        query.matchBuildingId(buildingId, true);
        query.matchGenusType(projectGenusType, true);
        query.matchDate(from, to, true);
        return (this.session.getProjectsByQuery(query));
    }

    
    /**
     *  Gets all <code>Projects</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  projects or an error results. Otherwise, the returned list
     *  may contain only those projects that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, projects are returned that are currently
     *  effective.  In any effective mode, effective projects and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Projects</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectList getProjects()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.construction.ProjectQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getProjectsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.room.construction.ProjectQuery getQuery() {
        org.osid.room.construction.ProjectQuery query = this.session.getProjectQuery();
        
        if (isEffectiveOnly()) {
            query.matchEffective(true);
        }

        return (query);
    }
}
