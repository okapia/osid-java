//
// AbstractConferralQueryInspector.java
//
//     A template for making a ConferralQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recognition.conferral.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for conferrals.
 */

public abstract class AbstractConferralQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQueryInspector
    implements org.osid.recognition.ConferralQueryInspector {

    private final java.util.Collection<org.osid.recognition.records.ConferralQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the award <code> Id </code> terms. 
     *
     *  @return the award <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAwardIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the award terms. 
     *
     *  @return the award terms 
     */

    @OSID @Override
    public org.osid.recognition.AwardQueryInspector[] getAwardTerms() {
        return (new org.osid.recognition.AwardQueryInspector[0]);
    }


    /**
     *  Gets the recipient <code> Id </code> terms. 
     *
     *  @return the recipient <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRecipientIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the recipient terms. 
     *
     *  @return the recipient terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getRecipientTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the reference <code> Id </code> terms. 
     *
     *  @return the reference <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getReferenceIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the convocation <code> Id </code> terms. 
     *
     *  @return the convocation <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getConvocationIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the convocation terms. 
     *
     *  @return the convocation terms 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationQueryInspector[] getConvocationTerms() {
        return (new org.osid.recognition.ConvocationQueryInspector[0]);
    }


    /**
     *  Gets the academy <code> Id </code> terms. 
     *
     *  @return the academy <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAcademyIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the academy terms. 
     *
     *  @return the academy terms 
     */

    @OSID @Override
    public org.osid.recognition.AcademyQueryInspector[] getAcademyTerms() {
        return (new org.osid.recognition.AcademyQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given conferral query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a conferral implementing the requested record.
     *
     *  @param conferralRecordType a conferral record type
     *  @return the conferral query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>conferralRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(conferralRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.recognition.records.ConferralQueryInspectorRecord getConferralQueryInspectorRecord(org.osid.type.Type conferralRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.recognition.records.ConferralQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(conferralRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(conferralRecordType + " is not supported");
    }


    /**
     *  Adds a record to this conferral query. 
     *
     *  @param conferralQueryInspectorRecord conferral query inspector
     *         record
     *  @param conferralRecordType conferral record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addConferralQueryInspectorRecord(org.osid.recognition.records.ConferralQueryInspectorRecord conferralQueryInspectorRecord, 
                                                   org.osid.type.Type conferralRecordType) {

        addRecordType(conferralRecordType);
        nullarg(conferralRecordType, "conferral record type");
        this.records.add(conferralQueryInspectorRecord);        
        return;
    }
}
