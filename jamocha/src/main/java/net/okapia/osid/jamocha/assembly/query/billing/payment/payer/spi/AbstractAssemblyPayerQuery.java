//
// AbstractAssemblyPayerQuery.java
//
//     A PayerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.billing.payment.payer.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A PayerQuery that stores terms.
 */

public abstract class AbstractAssemblyPayerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyTemporalOsidObjectQuery
    implements org.osid.billing.payment.PayerQuery,
               org.osid.billing.payment.PayerQueryInspector,
               org.osid.billing.payment.PayerSearchOrder {

    private final java.util.Collection<org.osid.billing.payment.records.PayerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.billing.payment.records.PayerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.billing.payment.records.PayerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyPayerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyPayerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the resource <code> Id </code> for this query. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResourceId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getResourceIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearResourceIdTerms() {
        getAssembler().clearTerms(getResourceIdColumn());
        return;
    }


    /**
     *  Gets the resource <code> Id </code> query terms. 
     *
     *  @return the resource <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResourceIdTerms() {
        return (getAssembler().getIdTerms(getResourceIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the resource. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByResource(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getResourceColumn(), style);
        return;
    }


    /**
     *  Gets the ResourceId column name.
     *
     * @return the column name
     */

    protected String getResourceIdColumn() {
        return ("resource_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return a resource query 
     *  @throws org.osid.UnimplementedException <code> supportsResourceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getResourceQuery() {
        throw new org.osid.UnimplementedException("supportsResourceQuery() is false");
    }


    /**
     *  Clears the resource terms. 
     */

    @OSID @Override
    public void clearResourceTerms() {
        getAssembler().clearTerms(getResourceColumn());
        return;
    }


    /**
     *  Gets the resource query terms. 
     *
     *  @return the resource query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getResourceTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a resource search order is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceSearchOrder() {
        return (false);
    }


    /**
     *  Specifies a preference for ordering the result set by the resource. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getResourceSearchOrder() {
        throw new org.osid.UnimplementedException("supportsResourceSearchOrder() is false");
    }


    /**
     *  Gets the Resource column name.
     *
     * @return the column name
     */

    protected String getResourceColumn() {
        return ("resource");
    }


    /**
     *  Sets the customer <code> Id </code> for this query. 
     *
     *  @param  customerId a customer <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> customerId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCustomerId(org.osid.id.Id customerId, boolean match) {
        getAssembler().addIdTerm(getCustomerIdColumn(), customerId, match);
        return;
    }


    /**
     *  Clears the customer <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCustomerIdTerms() {
        getAssembler().clearTerms(getCustomerIdColumn());
        return;
    }


    /**
     *  Gets the customer <code> Id </code> query terms. 
     *
     *  @return the customer <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCustomerIdTerms() {
        return (getAssembler().getIdTerms(getCustomerIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the customer. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCustomer(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCustomerColumn(), style);
        return;
    }


    /**
     *  Gets the CustomerId column name.
     *
     * @return the column name
     */

    protected String getCustomerIdColumn() {
        return ("customer_id");
    }


    /**
     *  Tests if a <code> CustomereQuery </code> is available. 
     *
     *  @return <code> true </code> if a customer query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCustomerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a customer. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return a customer query 
     *  @throws org.osid.UnimplementedException <code> supportsCustomerQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerQuery getCustomerQuery() {
        throw new org.osid.UnimplementedException("supportsCustomerQuery() is false");
    }


    /**
     *  Matches payers with any related customer. 
     *
     *  @param  match <code> true </code> to match payers with any related 
     *          customer, <code> false </code> to match payers with no 
     *          customer 
     */

    @OSID @Override
    public void matchAnyCustomer(boolean match) {
        getAssembler().addIdWildcardTerm(getCustomerColumn(), match);
        return;
    }


    /**
     *  Clears the customer terms. 
     */

    @OSID @Override
    public void clearCustomerTerms() {
        getAssembler().clearTerms(getCustomerColumn());
        return;
    }


    /**
     *  Gets the customer query terms. 
     *
     *  @return the customer query terms 
     */

    @OSID @Override
    public org.osid.billing.CustomerQueryInspector[] getCustomerTerms() {
        return (new org.osid.billing.CustomerQueryInspector[0]);
    }


    /**
     *  Tests if a customer search order is available. 
     *
     *  @return <code> true </code> if a customer search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCustomerSearchOrder() {
        return (false);
    }


    /**
     *  Specifies a preference for ordering the result set by the customer. 
     *
     *  @return the customer search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCustomerSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerSearchOrder getCustomerSearchOrder() {
        throw new org.osid.UnimplementedException("supportsCustomerSearchOrder() is false");
    }


    /**
     *  Gets the Customer column name.
     *
     * @return the column name
     */

    protected String getCustomerColumn() {
        return ("customer");
    }


    /**
     *  Matches payers using their activity account. 
     *
     *  @param  match <code> true </code> to match payers using an activity, 
     *          false otherwise 
     */

    @OSID @Override
    public void matchUsesActivity(boolean match) {
        getAssembler().addBooleanTerm(getUsesActivityColumn(), match);
        return;
    }


    /**
     *  Clears the activity terms. 
     */

    @OSID @Override
    public void clearUsesActivityTerms() {
        getAssembler().clearTerms(getUsesActivityColumn());
        return;
    }


    /**
     *  Gets the activity terms. 
     *
     *  @return the activity terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getUsesActivityTerms() {
        return (getAssembler().getBooleanTerms(getUsesActivityColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by activity based 
     *  customers. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByUsesActivity(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getUsesActivityColumn(), style);
        return;
    }


    /**
     *  Gets the UsesActivity column name.
     *
     *  @return the column name
     */

    protected String getUsesActivityColumn() {
        return ("uses_activity");
    }


    /**
     *  Matches payers using cash. 
     *
     *  @param  match <code> true </code> to match payers using cash, false 
     *          otherwise 
     */

    @OSID @Override
    public void matchUsesCash(boolean match) {
        getAssembler().addBooleanTerm(getUsesCashColumn(), match);
        return;
    }


    /**
     *  Clears the cash terms. 
     */

    @OSID @Override
    public void clearUsesCashTerms() {
        getAssembler().clearTerms(getUsesCashColumn());
        return;
    }


    /**
     *  Gets the cash terms. 
     *
     *  @return the cash terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getUsesCashTerms() {
        return (getAssembler().getBooleanTerms(getUsesCashColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by cash customers. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByUsesCash(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getUsesCashColumn(), style);
        return;
    }


    /**
     *  Gets the Cash column name.
     *
     * @return the column name
     */

    protected String getUsesCashColumn() {
        return ("uses_cash");
    }


    /**
     *  Matches credit card numbers. 
     *
     *  @param  number a credit card number 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> number </code> not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> number </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchCreditCardNumber(String number, 
                                      org.osid.type.Type stringMatchType, 
                                      boolean match) {
        getAssembler().addStringTerm(getCreditCardNumberColumn(), number, stringMatchType, match);
        return;
    }


    /**
     *  Matches payers with any credit card. 
     *
     *  @param  match <code> true </code> to match payers with any credit 
     *          card, <code> false </code> to match payers with no credit card 
     */

    @OSID @Override
    public void matchAnyCreditCardNumber(boolean match) {
        getAssembler().addStringWildcardTerm(getCreditCardNumberColumn(), match);
        return;
    }


    /**
     *  Clears the credit card number terms. 
     */

    @OSID @Override
    public void clearCreditCardNumberTerms() {
        getAssembler().clearTerms(getCreditCardNumberColumn());
        return;
    }


    /**
     *  Gets the credit card number terms. 
     *
     *  @return the credit card number terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getCreditCardNumberTerms() {
        return (getAssembler().getStringTerms(getCreditCardNumberColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the credit card 
     *  number. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCreditCardNumber(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCreditCardNumberColumn(), style);
        return;
    }


    /**
     *  Gets the CreditCardNumber column name.
     *
     * @return the column name
     */

    protected String getCreditCardNumberColumn() {
        return ("credit_card_number");
    }


    /**
     *  Matches credit card expirations between the given date range 
     *  inclusive. 
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchCreditCardExpiration(org.osid.calendaring.DateTime from, 
                                          org.osid.calendaring.DateTime to, 
                                          boolean match) {
        getAssembler().addDateTimeRangeTerm(getCreditCardExpirationColumn(), from, to, match);
        return;
    }


    /**
     *  Matches payers with any credit card expiration date. 
     *
     *  @param  match <code> true </code> to match payers with any credit card 
     *          expiration, <code> false </code> to match payers with no 
     *          credit card expiration 
     */

    @OSID @Override
    public void matchAnyCreditCardExpiration(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getCreditCardExpirationColumn(), match);
        return;
    }


    /**
     *  Clears the credit card expiration terms. 
     */

    @OSID @Override
    public void clearCreditCardExpirationTerms() {
        getAssembler().clearTerms(getCreditCardExpirationColumn());
        return;
    }


    /**
     *  Gets the credit card expiration terms. 
     *
     *  @return the credit card expiration terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getCreditCardExpirationTerms() {
        return (getAssembler().getDateTimeRangeTerms(getCreditCardExpirationColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the credit card 
     *  expiration. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCreditCardExpiration(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCreditCardExpirationColumn(), style);
        return;
    }


    /**
     *  Gets the CreditCardExpiration column name.
     *
     * @return the column name
     */

    protected String getCreditCardExpirationColumn() {
        return ("credit_card_expiration");
    }


    /**
     *  Matches credit card security codes. 
     *
     *  @param  code a credit card code 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> code </code> not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> code </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchCreditCardCode(String code, 
                                    org.osid.type.Type stringMatchType, 
                                    boolean match) {
        getAssembler().addStringTerm(getCreditCardCodeColumn(), code, stringMatchType, match);
        return;
    }


    /**
     *  Matches payers with any credit card security code. 
     *
     *  @param  match <code> true </code> to match payers with any credit card 
     *          security code, <code> false </code> to match payers with no 
     *          credit card security code 
     */

    @OSID @Override
    public void matchAnyCreditCardCode(boolean match) {
        getAssembler().addStringWildcardTerm(getCreditCardCodeColumn(), match);
        return;
    }


    /**
     *  Clears the credit card code terms. 
     */

    @OSID @Override
    public void clearCreditCardCodeTerms() {
        getAssembler().clearTerms(getCreditCardCodeColumn());
        return;
    }


    /**
     *  Gets the credit card code terms. 
     *
     *  @return the credit card code terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getCreditCardCodeTerms() {
        return (getAssembler().getStringTerms(getCreditCardCodeColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the credit card 
     *  code. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCreditCardCode(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCreditCardCodeColumn(), style);
        return;
    }


    /**
     *  Gets the CreditCardCode column name.
     *
     * @return the column name
     */

    protected String getCreditCardCodeColumn() {
        return ("credit_card_code");
    }


    /**
     *  Matches bank routing numbers. 
     *
     *  @param  number a bank routing number 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> number </code> not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> number </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchBankRoutingNumber(String number, 
                                       org.osid.type.Type stringMatchType, 
                                       boolean match) {
        getAssembler().addStringTerm(getBankRoutingNumberColumn(), number, stringMatchType, match);
        return;
    }


    /**
     *  Matches payers with any bank routing number. 
     *
     *  @param  match <code> true </code> to match payers with any bank 
     *          routing number, <code> false </code> to match payers with no 
     *          bank routing number 
     */

    @OSID @Override
    public void matchAnyBankRoutingNumber(boolean match) {
        getAssembler().addStringWildcardTerm(getBankRoutingNumberColumn(), match);
        return;
    }


    /**
     *  Clears the bank routing number terms. 
     */

    @OSID @Override
    public void clearBankRoutingNumberTerms() {
        getAssembler().clearTerms(getBankRoutingNumberColumn());
        return;
    }


    /**
     *  Gets the bank routing number terms. 
     *
     *  @return the bank routing number terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getBankRoutingNumberTerms() {
        return (getAssembler().getStringTerms(getBankRoutingNumberColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the bank routing 
     *  number. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByBankRoutingNumber(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getBankRoutingNumberColumn(), style);
        return;
    }


    /**
     *  Gets the BankRoutingNumber column name.
     *
     * @return the column name
     */

    protected String getBankRoutingNumberColumn() {
        return ("bank_routing_number");
    }


    /**
     *  Matches bank account numbers. 
     *
     *  @param  number a bank account number 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> number </code> not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> number </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchBankAccountNumber(String number, 
                                       org.osid.type.Type stringMatchType, 
                                       boolean match) {
        getAssembler().addStringTerm(getBankAccountNumberColumn(), number, stringMatchType, match);
        return;
    }


    /**
     *  Matches payers with any bank account number. 
     *
     *  @param  match <code> true </code> to match payers with any bank 
     *          account number, <code> false </code> to match payers with no 
     *          bank account number 
     */

    @OSID @Override
    public void matchAnyBankAccountNumber(boolean match) {
        getAssembler().addStringWildcardTerm(getBankAccountNumberColumn(), match);
        return;
    }


    /**
     *  Clears the bank account number terms. 
     */

    @OSID @Override
    public void clearBankAccountNumberTerms() {
        getAssembler().clearTerms(getBankAccountNumberColumn());
        return;
    }


    /**
     *  Gets the bank account number terms. 
     *
     *  @return the bank account number terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getBankAccountNumberTerms() {
        return (getAssembler().getStringTerms(getBankAccountNumberColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the bank account 
     *  number. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByBankAccountNumber(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getBankAccountNumberColumn(), style);
        return;
    }


    /**
     *  Gets the BankAccountNumber column name.
     *
     * @return the column name
     */

    protected String getBankAccountNumberColumn() {
        return ("bank_account_number");
    }


    /**
     *  Sets the business <code> Id </code> for this query to match payers 
     *  assigned to businesses. 
     *
     *  @param  businessId the business <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBusinessId(org.osid.id.Id businessId, boolean match) {
        getAssembler().addIdTerm(getBusinessIdColumn(), businessId, match);
        return;
    }


    /**
     *  Clears the business <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBusinessIdTerms() {
        getAssembler().clearTerms(getBusinessIdColumn());
        return;
    }


    /**
     *  Gets the business <code> Id </code> query terms. 
     *
     *  @return the business <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBusinessIdTerms() {
        return (getAssembler().getIdTerms(getBusinessIdColumn()));
    }


    /**
     *  Gets the BusinessId column name.
     *
     * @return the column name
     */

    protected String getBusinessIdColumn() {
        return ("business_id");
    }


    /**
     *  Tests if a <code> BusinessQuery </code> is available. 
     *
     *  @return <code> true </code> if a business query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessQuery() {
        return (false);
    }


    /**
     *  Gets the query for a business. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the business query 
     *  @throws org.osid.UnimplementedException <code> supportsBusinessQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.BusinessQuery getBusinessQuery() {
        throw new org.osid.UnimplementedException("supportsBusinessQuery() is false");
    }


    /**
     *  Clears the business terms. 
     */

    @OSID @Override
    public void clearBusinessTerms() {
        getAssembler().clearTerms(getBusinessColumn());
        return;
    }


    /**
     *  Gets the business query terms. 
     *
     *  @return the business query terms 
     */

    @OSID @Override
    public org.osid.billing.BusinessQueryInspector[] getBusinessTerms() {
        return (new org.osid.billing.BusinessQueryInspector[0]);
    }


    /**
     *  Gets the Business column name.
     *
     * @return the column name
     */

    protected String getBusinessColumn() {
        return ("business");
    }


    /**
     *  Tests if this payer supports the given record
     *  <code>Type</code>.
     *
     *  @param  payerRecordType a payer record type 
     *  @return <code>true</code> if the payerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>payerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type payerRecordType) {
        for (org.osid.billing.payment.records.PayerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(payerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  payerRecordType the payer record type 
     *  @return the payer query record 
     *  @throws org.osid.NullArgumentException
     *          <code>payerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(payerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.payment.records.PayerQueryRecord getPayerQueryRecord(org.osid.type.Type payerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.billing.payment.records.PayerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(payerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(payerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  payerRecordType the payer record type 
     *  @return the payer query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>payerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(payerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.payment.records.PayerQueryInspectorRecord getPayerQueryInspectorRecord(org.osid.type.Type payerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.billing.payment.records.PayerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(payerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(payerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param payerRecordType the payer record type
     *  @return the payer search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>payerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(payerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.payment.records.PayerSearchOrderRecord getPayerSearchOrderRecord(org.osid.type.Type payerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.billing.payment.records.PayerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(payerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(payerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this payer. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param payerQueryRecord the payer query record
     *  @param payerQueryInspectorRecord the payer query inspector
     *         record
     *  @param payerSearchOrderRecord the payer search order record
     *  @param payerRecordType payer record type
     *  @throws org.osid.NullArgumentException
     *          <code>payerQueryRecord</code>,
     *          <code>payerQueryInspectorRecord</code>,
     *          <code>payerSearchOrderRecord</code> or
     *          <code>payerRecordTypepayer</code> is
     *          <code>null</code>
     */
            
    protected void addPayerRecords(org.osid.billing.payment.records.PayerQueryRecord payerQueryRecord, 
                                      org.osid.billing.payment.records.PayerQueryInspectorRecord payerQueryInspectorRecord, 
                                      org.osid.billing.payment.records.PayerSearchOrderRecord payerSearchOrderRecord, 
                                      org.osid.type.Type payerRecordType) {

        addRecordType(payerRecordType);

        nullarg(payerQueryRecord, "payer query record");
        nullarg(payerQueryInspectorRecord, "payer query inspector record");
        nullarg(payerSearchOrderRecord, "payer search odrer record");

        this.queryRecords.add(payerQueryRecord);
        this.queryInspectorRecords.add(payerQueryInspectorRecord);
        this.searchOrderRecords.add(payerSearchOrderRecord);
        
        return;
    }
}
