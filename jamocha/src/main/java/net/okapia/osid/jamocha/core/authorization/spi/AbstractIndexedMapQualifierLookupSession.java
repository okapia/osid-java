//
// AbstractIndexedMapQualifierLookupSession.java
//
//    A simple framework for providing a Qualifier lookup service
//    backed by a fixed collection of qualifiers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.authorization.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Qualifier lookup service backed by a
 *  fixed collection of qualifiers. The qualifiers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some qualifiers may be compatible
 *  with more types than are indicated through these qualifier
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Qualifiers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapQualifierLookupSession
    extends AbstractMapQualifierLookupSession
    implements org.osid.authorization.QualifierLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.authorization.Qualifier> qualifiersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.authorization.Qualifier>());
    private final MultiMap<org.osid.type.Type, org.osid.authorization.Qualifier> qualifiersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.authorization.Qualifier>());


    /**
     *  Makes a <code>Qualifier</code> available in this session.
     *
     *  @param  qualifier a qualifier
     *  @throws org.osid.NullArgumentException <code>qualifier<code> is
     *          <code>null</code>
     */

    @Override
    protected void putQualifier(org.osid.authorization.Qualifier qualifier) {
        super.putQualifier(qualifier);

        this.qualifiersByGenus.put(qualifier.getGenusType(), qualifier);
        
        try (org.osid.type.TypeList types = qualifier.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.qualifiersByRecord.put(types.getNextType(), qualifier);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a qualifier from this session.
     *
     *  @param qualifierId the <code>Id</code> of the qualifier
     *  @throws org.osid.NullArgumentException <code>qualifierId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeQualifier(org.osid.id.Id qualifierId) {
        org.osid.authorization.Qualifier qualifier;
        try {
            qualifier = getQualifier(qualifierId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.qualifiersByGenus.remove(qualifier.getGenusType());

        try (org.osid.type.TypeList types = qualifier.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.qualifiersByRecord.remove(types.getNextType(), qualifier);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeQualifier(qualifierId);
        return;
    }


    /**
     *  Gets a <code>QualifierList</code> corresponding to the given
     *  qualifier genus <code>Type</code> which does not include
     *  qualifiers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known qualifiers or an error results. Otherwise,
     *  the returned list may contain only those qualifiers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  qualifierGenusType a qualifier genus type 
     *  @return the returned <code>Qualifier</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>qualifierGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.QualifierList getQualifiersByGenusType(org.osid.type.Type qualifierGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.authorization.qualifier.ArrayQualifierList(this.qualifiersByGenus.get(qualifierGenusType)));
    }


    /**
     *  Gets a <code>QualifierList</code> containing the given
     *  qualifier record <code>Type</code>. In plenary mode, the
     *  returned list contains all known qualifiers or an error
     *  results. Otherwise, the returned list may contain only those
     *  qualifiers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  qualifierRecordType a qualifier record type 
     *  @return the returned <code>qualifier</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>qualifierRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.QualifierList getQualifiersByRecordType(org.osid.type.Type qualifierRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.authorization.qualifier.ArrayQualifierList(this.qualifiersByRecord.get(qualifierRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.qualifiersByGenus.clear();
        this.qualifiersByRecord.clear();

        super.close();

        return;
    }
}
