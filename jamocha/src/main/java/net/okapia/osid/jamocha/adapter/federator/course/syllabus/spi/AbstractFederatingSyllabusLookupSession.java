//
// AbstractFederatingSyllabusLookupSession.java
//
//     An abstract federating adapter for a SyllabusLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.course.syllabus.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  SyllabusLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingSyllabusLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.course.syllabus.SyllabusLookupSession>
    implements org.osid.course.syllabus.SyllabusLookupSession {

    private boolean parallel = false;
    private org.osid.course.CourseCatalog courseCatalog = new net.okapia.osid.jamocha.nil.course.coursecatalog.UnknownCourseCatalog();


    /**
     *  Constructs a new <code>AbstractFederatingSyllabusLookupSession</code>.
     */

    protected AbstractFederatingSyllabusLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.course.syllabus.SyllabusLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>CourseCatalog/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>CourseCatalog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.courseCatalog.getId());
    }


    /**
     *  Gets the <code>CourseCatalog</code> associated with this 
     *  session.
     *
     *  @return the <code>CourseCatalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.courseCatalog);
    }


    /**
     *  Sets the <code>CourseCatalog</code>.
     *
     *  @param  courseCatalog the course catalog for this session
     *  @throws org.osid.NullArgumentException <code>courseCatalog</code>
     *          is <code>null</code>
     */

    protected void setCourseCatalog(org.osid.course.CourseCatalog courseCatalog) {
        nullarg(courseCatalog, "course catalog");
        this.courseCatalog = courseCatalog;
        return;
    }


    /**
     *  Tests if this user can perform <code>Syllabus</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupSyllabi() {
        for (org.osid.course.syllabus.SyllabusLookupSession session : getSessions()) {
            if (session.canLookupSyllabi()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Syllabus</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeSyllabusView() {
        for (org.osid.course.syllabus.SyllabusLookupSession session : getSessions()) {
            session.useComparativeSyllabusView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Syllabus</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenarySyllabusView() {
        for (org.osid.course.syllabus.SyllabusLookupSession session : getSessions()) {
            session.usePlenarySyllabusView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include syllabi in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        for (org.osid.course.syllabus.SyllabusLookupSession session : getSessions()) {
            session.useFederatedCourseCatalogView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        for (org.osid.course.syllabus.SyllabusLookupSession session : getSessions()) {
            session.useIsolatedCourseCatalogView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Syllabus</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Syllabus</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Syllabus</code> and
     *  retained for compatibility.
     *
     *  @param  syllabusId <code>Id</code> of the
     *          <code>Syllabus</code>
     *  @return the syllabus
     *  @throws org.osid.NotFoundException <code>syllabusId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>syllabusId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.Syllabus getSyllabus(org.osid.id.Id syllabusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.course.syllabus.SyllabusLookupSession session : getSessions()) {
            try {
                return (session.getSyllabus(syllabusId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(syllabusId + " not found");
    }


    /**
     *  Gets a <code>SyllabusList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  syllabi specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Syllabi</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  syllabusIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Syllabus</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>syllabusIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusList getSyllabiByIds(org.osid.id.IdList syllabusIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.course.syllabus.syllabus.MutableSyllabusList ret = new net.okapia.osid.jamocha.course.syllabus.syllabus.MutableSyllabusList();

        try (org.osid.id.IdList ids = syllabusIds) {
            while (ids.hasNext()) {
                ret.addSyllabus(getSyllabus(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>SyllabusList</code> corresponding to the given
     *  syllabus genus <code>Type</code> which does not include
     *  syllabi of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  syllabi or an error results. Otherwise, the returned list
     *  may contain only those syllabi that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  syllabusGenusType a syllabus genus type 
     *  @return the returned <code>Syllabus</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>syllabusGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusList getSyllabiByGenusType(org.osid.type.Type syllabusGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.syllabus.syllabus.FederatingSyllabusList ret = getSyllabusList();

        for (org.osid.course.syllabus.SyllabusLookupSession session : getSessions()) {
            ret.addSyllabusList(session.getSyllabiByGenusType(syllabusGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>SyllabusList</code> corresponding to the given
     *  syllabus genus <code>Type</code> and include any additional
     *  syllabi with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  syllabi or an error results. Otherwise, the returned list
     *  may contain only those syllabi that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  syllabusGenusType a syllabus genus type 
     *  @return the returned <code>Syllabus</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>syllabusGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusList getSyllabiByParentGenusType(org.osid.type.Type syllabusGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.syllabus.syllabus.FederatingSyllabusList ret = getSyllabusList();

        for (org.osid.course.syllabus.SyllabusLookupSession session : getSessions()) {
            ret.addSyllabusList(session.getSyllabiByParentGenusType(syllabusGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>SyllabusList</code> containing the given
     *  syllabus record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  syllabi or an error results. Otherwise, the returned list
     *  may contain only those syllabi that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  syllabusRecordType a syllabus record type 
     *  @return the returned <code>Syllabus</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>syllabusRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusList getSyllabiByRecordType(org.osid.type.Type syllabusRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.syllabus.syllabus.FederatingSyllabusList ret = getSyllabusList();

        for (org.osid.course.syllabus.SyllabusLookupSession session : getSessions()) {
            ret.addSyllabusList(session.getSyllabiByRecordType(syllabusRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code> SyllabusList </code> for the given course.  In
     *  plenary mode, the returned list contains all known syllabi or
     *  an error results. Otherwise, the returned list may contain
     *  only those syllabi that are accessible through this session.
     *
     *  @param  courseId a course <code> Id </code>
     *  @return the returned <code> Syllabus </code> list
     *  @throws org.osid.NullArgumentException <code> courseId </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusList getSyllabiForCourse(org.osid.id.Id courseId)
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.syllabus.syllabus.FederatingSyllabusList ret = getSyllabusList();

        for (org.osid.course.syllabus.SyllabusLookupSession session : getSessions()) {
            ret.addSyllabusList(session.getSyllabiForCourse(courseId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Syllabi</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  syllabi or an error results. Otherwise, the returned list
     *  may contain only those syllabi that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Syllabi</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusList getSyllabi()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.syllabus.syllabus.FederatingSyllabusList ret = getSyllabusList();

        for (org.osid.course.syllabus.SyllabusLookupSession session : getSessions()) {
            ret.addSyllabusList(session.getSyllabi());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.course.syllabus.syllabus.FederatingSyllabusList getSyllabusList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.course.syllabus.syllabus.ParallelSyllabusList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.course.syllabus.syllabus.CompositeSyllabusList());
        }
    }
}
