//
// AbstractAdapterLeaseLookupSession.java
//
//    A Lease lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.room.squatting.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Lease lookup session adapter.
 */

public abstract class AbstractAdapterLeaseLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.room.squatting.LeaseLookupSession {

    private final org.osid.room.squatting.LeaseLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterLeaseLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterLeaseLookupSession(org.osid.room.squatting.LeaseLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Campus/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Campus Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCampusId() {
        return (this.session.getCampusId());
    }


    /**
     *  Gets the {@code Campus} associated with this session.
     *
     *  @return the {@code Campus} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.Campus getCampus()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCampus());
    }


    /**
     *  Tests if this user can perform {@code Lease} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupLeases() {
        return (this.session.canLookupLeases());
    }


    /**
     *  A complete view of the {@code Lease} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeLeaseView() {
        this.session.useComparativeLeaseView();
        return;
    }


    /**
     *  A complete view of the {@code Lease} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryLeaseView() {
        this.session.usePlenaryLeaseView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include leases in campuses which are children
     *  of this campus in the campus hierarchy.
     */

    @OSID @Override
    public void useFederatedCampusView() {
        this.session.useFederatedCampusView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this campus only.
     */

    @OSID @Override
    public void useIsolatedCampusView() {
        this.session.useIsolatedCampusView();
        return;
    }
    

    /**
     *  Only leases whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveLeaseView() {
        this.session.useEffectiveLeaseView();
        return;
    }
    

    /**
     *  All leases of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveLeaseView() {
        this.session.useAnyEffectiveLeaseView();
        return;
    }

     
    /**
     *  Gets the {@code Lease} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Lease} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Lease} and
     *  retained for compatibility.
     *
     *  In effective mode, leases are returned that are currently
     *  effective.  In any effective mode, effective leases and
     *  those currently expired are returned.
     *
     *  @param leaseId {@code Id} of the {@code Lease}
     *  @return the lease
     *  @throws org.osid.NotFoundException {@code leaseId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code leaseId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.squatting.Lease getLease(org.osid.id.Id leaseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLease(leaseId));
    }


    /**
     *  Gets a {@code LeaseList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  leases specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Leases} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, leases are returned that are currently
     *  effective.  In any effective mode, effective leases and
     *  those currently expired are returned.
     *
     *  @param  leaseIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Lease} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code leaseIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesByIds(org.osid.id.IdList leaseIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLeasesByIds(leaseIds));
    }


    /**
     *  Gets a {@code LeaseList} corresponding to the given
     *  lease genus {@code Type} which does not include
     *  leases of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  leases or an error results. Otherwise, the returned list
     *  may contain only those leases that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, leases are returned that are currently
     *  effective.  In any effective mode, effective leases and
     *  those currently expired are returned.
     *
     *  @param  leaseGenusType a lease genus type 
     *  @return the returned {@code Lease} list
     *  @throws org.osid.NullArgumentException
     *          {@code leaseGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesByGenusType(org.osid.type.Type leaseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLeasesByGenusType(leaseGenusType));
    }


    /**
     *  Gets a {@code LeaseList} corresponding to the given lease
     *  genus {@code Type} and include any additional leases with
     *  genus types derived from the specified {@code Type}.
     *
     *  In plenary mode, the returned list contains all known leases
     *  or an error results. Otherwise, the returned list may contain
     *  only those leases that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In effective mode, leases are returned that are currently
     *  effective.  In any effective mode, effective leases and those
     *  currently expired are returned.
     *
     *  @param  leaseGenusType a lease genus type 
     *  @return the returned {@code Lease} list
     *  @throws org.osid.NullArgumentException
     *          {@code leaseGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesByParentGenusType(org.osid.type.Type leaseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLeasesByParentGenusType(leaseGenusType));
    }


    /**
     *  Gets a {@code LeaseList} containing the given lease record
     *  {@code Type}.
     * 
     *  In plenary mode, the returned list contains all known leases
     *  or an error results. Otherwise, the returned list may contain
     *  only those leases that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In effective mode, leases are returned that are currently
     *  effective.  In any effective mode, effective leases and those
     *  currently expired are returned.
     *
     *  @param  leaseRecordType a lease record type 
     *  @return the returned {@code Lease} list
     *  @throws org.osid.NullArgumentException
     *          {@code leaseRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesByRecordType(org.osid.type.Type leaseRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLeasesByRecordType(leaseRecordType));
    }


    /**
     *  Gets a {@code LeaseList} effective during the entire given
     *  date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known leases
     *  or an error results. Otherwise, the returned list may contain
     *  only those leases that are accessible through this session.
     *  
     *  In active mode, leases are returned that are currently
     *  active. In any status mode, active and inactive leases are
     *  returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Lease} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesOnDate(org.osid.calendaring.DateTime from, 
                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLeasesOnDate(from, to));
    }
        

    /**
     *  Gets a list of all leases of a genus type effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *  
     *  In plenary mode, the returned list contains all known leases
     *  or an error results. Otherwise, the returned list may contain
     *  only those leases that are accessible through this session.
     *  
     *  In effective mode, leases are returned that are currently
     *  effective.  In any effective mode, effective leases and those
     *  currently expired are returned.
     *
     *  @param  leaseGenusType a lease genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code LeaseList} 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code leaseGenusType,
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesByGenusTypeOnDate(org.osid.type.Type leaseGenusType, 
                                                                        org.osid.calendaring.DateTime from, 
                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLeasesByGenusTypeOnDate(leaseGenusType, from, to));
    }


    /**
     *  Gets a list of leases corresponding to a room
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  leases or an error results. Otherwise, the returned list
     *  may contain only those leases that are accessible through
     *  this session.
     *
     *  In effective mode, leases are returned that are
     *  currently effective.  In any effective mode, effective
     *  leases and those currently expired are returned.
     *
     *  @param  roomId the {@code Id} of the room
     *  @return the returned {@code LeaseList}
     *  @throws org.osid.NullArgumentException {@code roomId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesForRoom(org.osid.id.Id roomId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLeasesForRoom(roomId));
    }


    /**
     *  Gets a list of all leases for a room and a lease genus type.
     *  
     *  In plenary mode, the returned list contains all known leases
     *  or an error results. Otherwise, the returned list may contain
     *  only those leases that are accessible through this session.
     *  
     *  In effective mode, leases are returned that are currently
     *  effective.  In any effective mode, effective leases and those
     *  currently expired are returned.
     *
     *  @param  roomId a room {@code Id} 
     *  @param  leaseGenusType a lease genus type 
     *  @return the returned {@code LeaseList} 
     *  @throws org.osid.InvalidArgumentException {@code to} is less
     *          than {@code from}
     *  @throws org.osid.NullArgumentException {@code roomId} or {@code 
     *          leaseGenusType} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesByGenusTypeForRoom(org.osid.id.Id roomId, 
                                                                         org.osid.type.Type leaseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLeasesByGenusTypeForRoom(roomId, leaseGenusType));
    }


    /**
     *  Gets a list of leases corresponding to a room {@code Id} and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *
     *  In plenary mode, the returned list contains all known leases
     *  or an error results. Otherwise, the returned list may contain
     *  only those leases that are accessible through this session.
     *
     *  In effective mode, leases are returned that are currently
     *  effective.  In any effective mode, effective leases and those
     *  currently expired are returned.
     *
     *  @param  roomId the {@code Id} of the room
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code LeaseList}
     *  @throws org.osid.NullArgumentException {@code roomId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesForRoomOnDate(org.osid.id.Id roomId,
                                                                    org.osid.calendaring.DateTime from,
                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLeasesForRoomOnDate(roomId, from, to));
    }


    /**
     *  Gets a list of all leases for a room and of a lease genus type
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known leases
     *  or an error results. Otherwise, the returned list may contain
     *  only those leases that are accessible through this session.
     *  
     *  In effective mode, leases are returned that are currently
     *  effective.  In any effective mode, effective leases and those
     *  currently expired are returned.
     *
     *  @param  roomId a room {@code Id} 
     *  @param  leaseGenusType a lease genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code LeaseList} 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code roomId, leaseGenusType, 
     *          from} or {@code to} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesByGenusTypeForRoomOnDate(org.osid.id.Id roomId, 
                                                                               org.osid.type.Type leaseGenusType, 
                                                                               org.osid.calendaring.DateTime from, 
                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLeasesByGenusTypeForRoomOnDate(roomId, leaseGenusType, from, to));
    }


    /**
     *  Gets a list of leases corresponding to a tenant
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  leases or an error results. Otherwise, the returned list
     *  may contain only those leases that are accessible
     *  through this session.
     *
     *  In effective mode, leases are returned that are
     *  currently effective.  In any effective mode, effective
     *  leases and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the tenant
     *  @return the returned {@code LeaseList}
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesForTenant(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLeasesForTenant(resourceId));
    }


    /**
     *  Gets a list of all leases corresponding to a tenant {@code Id}
     *  and of a lease genus type.
     *  
     *  In plenary mode, the returned list contains all known leases
     *  or an error results. Otherwise, the returned list may contain
     *  only those leases that are accessible through this session.
     *  
     *  In effective mode, leases are returned that are currently
     *  effective.  In any effective mode, effective leases and those
     *  currently expired are returned.
     *
     *  @param  resourceId a resourceId {@code Id} 
     *  @param  leaseGenusType a lease genus type 
     *  @return the returned {@code LeaseList} 
     *  @throws org.osid.InvalidArgumentException {@code to} is less
     *          than {@code from}
     *  @throws org.osid.NullArgumentException {@code resourceId} or 
     *          {@code leaseGenusType} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesByGenusTypeForTenant(org.osid.id.Id resourceId, 
                                                                           org.osid.type.Type leaseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLeasesByGenusTypeForTenant(resourceId, leaseGenusType));
    }


    /**
     *  Gets a list of leases corresponding to a tenant
     *  {@code Id} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  leases or an error results. Otherwise, the returned list
     *  may contain only those leases that are accessible
     *  through this session.
     *
     *  In effective mode, leases are returned that are
     *  currently effective.  In any effective mode, effective
     *  leases and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the tenant
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code LeaseList}
     *  @throws org.osid.NullArgumentException {@code resourceId}, {@code
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesForTenantOnDate(org.osid.id.Id resourceId,
                                                                      org.osid.calendaring.DateTime from,
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLeasesForTenantOnDate(resourceId, from, to));
    }


    /**
     *  Gets a list of all leases for a tenant and of a lease genus
     *  type effective during the entire given date range inclusive
     *  but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known leases
     *  or an error results. Otherwise, the returned list may contain
     *  only those leases that are accessible through this session.
     *  
     *  In effective mode, leases are returned that are currently
     *  effective.  In any effective mode, effective leases and those
     *  currently expired are returned.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @param  leaseGenusType a lease genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code LeaseList} 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code resourceId,
     *          leaseGenusType, from}, or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
    
    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesByGenusTypeForTenantOnDate(org.osid.id.Id resourceId, 
                                                                                 org.osid.type.Type leaseGenusType, 
                                                                                 org.osid.calendaring.DateTime from, 
                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLeasesByGenusTypeForTenantOnDate(resourceId, leaseGenusType, from, to));
    }


    /**
     *  Gets a list of leases corresponding to room and tenant
     *  {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known leases
     *  or an error results. Otherwise, the returned list may contain
     *  only those leases that are accessible through this session.
     *
     *  In effective mode, leases are returned that are currently
     *  effective.  In any effective mode, effective leases and those
     *  currently expired are returned.
     *
     *  @param  roomId the {@code Id} of the room
     *  @param  resourceId the {@code Id} of the tenant
     *  @return the returned {@code LeaseList}
     *  @throws org.osid.NullArgumentException {@code roomId},
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesForRoomAndTenant(org.osid.id.Id roomId,
                                                                       org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLeasesForRoomAndTenant(roomId, resourceId));
    }


    /**
     *  Gets a list of all leases for a room, resource, and of a lease
     *  genus type.
     *  
     *  In plenary mode, the returned list contains all known leases
     *  or an error results. Otherwise, the returned list may contain
     *  only those leases that are accessible through this session.
     *  
     *  In effective mode, leases are returned that are currently
     *  effective.  In any effective mode, effective leases and those
     *  currently expired are returned.
     *
     *  @param  roomId a room {@code Id} 
     *  @param  resourceId a tenant {@code Id} 
     *  @param  leaseGenusType a lease genus type 
     *  @return the returned {@code LeaseList} 
     *  @throws org.osid.InvalidArgumentException {@code to} is less 
     *          than {@code from} 
     *  @throws org.osid.NullArgumentException {@code roomId,
     *          resourceId}, or {@code leaseGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesByGenusTypeForRoomAndTenant(org.osid.id.Id roomId, 
                                                                                  org.osid.id.Id resourceId, 
                                                                                  org.osid.type.Type leaseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLeasesByGenusTypeForRoomAndTenant(roomId, resourceId, leaseGenusType));
    }


    /**
     *  Gets a list of leases corresponding to room and tenant {@code
     *  Ids} and effective during the entire given date range
     *  inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known leases
     *  or an error results. Otherwise, the returned list may contain
     *  only those leases that are accessible through this session.
     *
     *  In effective mode, leases are returned that are currently
     *  effective. In any effective mode, effective leases and those
     *  currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the tenant
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code LeaseList}
     *  @throws org.osid.NullArgumentException {@code roomId},
     *          {@code resourceId}, {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesForRoomAndTenantOnDate(org.osid.id.Id roomId,
                                                                             org.osid.id.Id resourceId,
                                                                             org.osid.calendaring.DateTime from,
                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLeasesForRoomAndTenantOnDate(roomId, resourceId, from, to));
    }

    
    /**
     *  Gets a list of all leases for a room, tenant, and of a lease
     *  genus type effective during the entire given date range
     *  inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known leases
     *  or an error results. Otherwise, the returned list may contain
     *  only those leases that are accessible through this session.
     *  
     *  In effective mode, leases are returned that are currently
     *  effective.  In any effective mode, effective leases and those
     *  currently expired are returned.
     *
     *  @param  roomId a room {@code Id} 
     *  @param  resourceId a resource {@code Id} 
     *  @param  leaseGenusType a lease genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code LeaseList} 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code roomId,
     *          resourceId, leaseGenusType, from,} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
    
    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesByGenusTypeForRoomAndTenantOnDate(org.osid.id.Id roomId, 
                                                                                        org.osid.id.Id resourceId, 
                                                                                        org.osid.type.Type leaseGenusType, 
                                                                                        org.osid.calendaring.DateTime from, 
                                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLeasesByGenusTypeForRoomAndTenantOnDate(roomId, resourceId, leaseGenusType, from, to));
    }


    /**
     *  Gets all {@code Leases}. 
     *
     *  In plenary mode, the returned list contains all known
     *  leases or an error results. Otherwise, the returned list
     *  may contain only those leases that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, leases are returned that are currently
     *  effective.  In any effective mode, effective leases and
     *  those currently expired are returned.
     *
     *  @return a list of {@code Leases} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeases()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLeases());
    }
}
