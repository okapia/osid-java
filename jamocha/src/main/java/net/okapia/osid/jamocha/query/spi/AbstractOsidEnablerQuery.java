//
// AbstractOsidEnablerQuery.java
//
//     A OsidEnablerQuery with stored terms.
//
//
// Tom Coppeto
// OnTapSolutions
// 20 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.query.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A OsidEnablerQuery with stored terms.
 */

public abstract class AbstractOsidEnablerQuery
    extends AbstractOsidRuleQuery
    implements org.osid.OsidEnablerQuery {

    private final java.util.Collection<org.osid.search.terms.IdTerm> scheduleIdTerms = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.search.terms.IdTerm> eventIdTerms = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.search.terms.IdTerm> cyclicEventIdTerms = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.search.terms.IdTerm> demographicIdTerms = new java.util.LinkedHashSet<>();

    private final OsidTemporalQuery query;


    /**
     *  Constructs a new <code>AbstractOsidEnablerQuery</code>.
     *
     *  @param factory the term factory
     *  @throws org.osid.NullArgumentException <code>factory</code> is
     *          <code>null</code>
     */

    protected AbstractOsidEnablerQuery(net.okapia.osid.jamocha.query.TermFactory factory) {
        super(factory);
        this.query = new OsidTemporalQuery(factory);
        return;
    }


    /**
     *  Match the <code> Id </code> of an associated schedule. 
     *
     *  @param  scheduleId <code> Id </code> to match 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> scheduleId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchScheduleId(org.osid.id.Id scheduleId, boolean match) {
        this.scheduleIdTerms.add(getTermFactory().createIdTerm(scheduleId, match));
        return;
    }


    /**
     *  Clears all schedule <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearScheduleIdTerms() {
        this.scheduleIdTerms.clear();
        return;
    }

    
    /**
     *  Tests if a <code> ScheduleQuery </code> for the rule is available. 
     *
     *  @return <code> true </code> if a schedule query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScheduleQuery() {
        return (false);
    }


    /**
     *  Gets the query for the schedule. Each retrieval performs a boolean 
     *  <code> OR. </code> 
     *
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @return the schedule query 
     *  @throws org.osid.UnimplementedException <code> supportsScheduleQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleQuery getScheduleQuery(boolean match) {
        throw new org.osid.UnimplementedException("supportsScheduleQuery() is false");
    }


    /**
     *  Match any associated schedule. 
     *
     *  @param  match <code> true </code> to match any schedule, <code> false 
     *          </code> to match no schedules 
     */

    @OSID @Override
    public void matchAnySchedule(boolean match) {
        this.scheduleIdTerms.add(getTermFactory().createIdWildcardTerm(match));
        return;
    }


    /**
     *  Clears all schedule terms. 
     */

    @OSID @Override
    public void clearScheduleTerms() {
        clearWildcardTerms(this.scheduleIdTerms);
        return;
    }


    /**
     *  Gets all the schedule Id query terms.
     *
     *  @return a collection of the schedule Id query terms
     */

    protected java.util.Collection<org.osid.search.terms.IdTerm> getScheduleIdTerms() {
        return (java.util.Collections.unmodifiableCollection(this.scheduleIdTerms));
    }


    /**
     *  Match the <code> Id </code> of an associated event. 
     *
     *  @param  eventId <code> Id </code> to match 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> eventId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchEventId(org.osid.id.Id eventId, boolean match) {
        this.eventIdTerms.add(getTermFactory().createIdTerm(eventId, match));
        return;
    }
    
    
    /**
     *  Clears all event <code> Id </code> terms. 
     */
    
    @OSID @Override
    public void clearEventIdTerms() {
        this.eventIdTerms.clear();
        return;
    }


    /**
     *  Tests if a <code> EventQuery </code> for the rule is available. 
     *
     *  @return <code> true </code> if an event query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEventQuery() {
        return (false);
    }


    /**
     *  Gets the query for the event. Each retrieval performs a boolean <code> 
     *  OR. </code> 
     *
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @return the event query 
     *  @throws org.osid.UnimplementedException <code> supportsEventQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventQuery getEventQuery(boolean match) {
        throw new org.osid.UnimplementedException("supportsEventQuery() is false");
    }


    /**
     *  Match any associated event. 
     *
     *  @param  match <code> true </code> to match any event, <code> false 
     *          </code> to match no events 
     */

    @OSID @Override
    public void matchAnyEvent(boolean match) {
        this.eventIdTerms.add(getTermFactory().createIdWildcardTerm(match));
        return;
    }


    /**
     *  Clears all recurirng event terms. 
     */

    @OSID @Override
    public void clearEventTerms() {
        clearWildcardTerms(this.eventIdTerms);
        return;
    }


    /**
     *  Gets all the event Id query terms.
     *
     *  @return a collection of the event Id query terms
     */

    protected java.util.Collection<org.osid.search.terms.IdTerm> getEventIdTerms() {
        return (java.util.Collections.unmodifiableCollection(this.eventIdTerms));
    }


    /**
     *  Sets the cyclic event <code> Id </code> for this query. 
     *
     *  @param  cyclicEventId the cyclic event <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> cyclicEventId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCyclicEventId(org.osid.id.Id cyclicEventId, boolean match) {
        this.cyclicEventIdTerms.add(getTermFactory().createIdTerm(cyclicEventId, match));
        return;
    }


    /**
     *  Clears the cyclic event <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCyclicEventIdTerms() {
        this.cyclicEventIdTerms.clear();
        return;
    }


    /**
     *  Tests if a <code> CyclicEventQuery </code> is available. 
     *
     *  @return <code> true </code> if a cyclic event query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCyclicEventQuery() {
        return (false);
    }


    /**
     *  Gets the query for a cyclic event. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the cyclic event query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicEventQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventQuery getCyclicEventQuery() {
        throw new org.osid.UnimplementedException("supportsCyclicEventQuery() is false");
    }


    /**
     *  Matches any enabler with a cyclic event. 
     *
     *  @param  match <code> true </code> to match any enablers with a cyclic 
     *          event, <code> false </code> to match enablers with no cyclic 
     *          events 
     */

    @OSID @Override
    public void matchAnyCyclicEvent(boolean match) {
        this.cyclicEventIdTerms.add(getTermFactory().createIdWildcardTerm(match));
        return;
    }


    /**
     *  Clears the cyclic event query terms. 
     */

    @OSID @Override
    public void clearCyclicEventTerms() {
        clearWildcardTerms(this.cyclicEventIdTerms);
        return;
    }


    /**
     *  Gets all the cyclic event Id query terms.
     *
     *  @return a collection of the cyclic event Id query terms
     */

    protected java.util.Collection<org.osid.search.terms.IdTerm> getCyclicEventIdTerms() {
        return (java.util.Collections.unmodifiableCollection(this.cyclicEventIdTerms));
    }


    /**
     *  Match the <code> Id </code> of the demographic resource. 
     *
     *  @param  resourceId <code> Id </code> to match 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDemographicId(org.osid.id.Id resourceId, boolean match) {
        this.demographicIdTerms.add(getTermFactory().createIdTerm(resourceId, match));
        return;
    }


    /**
     *  Clears all resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDemographicIdTerms() {
        this.demographicIdTerms.clear();
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> for the demographic is 
     *  available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDemographicQuery() {
        return (false);
    }


    /**
     *  Gets the query for the resource. Each retrieval performs a boolean 
     *  <code> OR. </code> 
     *
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsResourceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getDemographicQuery(boolean match) {
        throw new org.osid.UnimplementedException("supportsDemographicQuery() is false");
    }


    /**
     *  Match any associated resource. 
     *
     *  @param  match <code> true </code> to match any demographic, <code> 
     *          false </code> to match no rules 
     */

    @OSID @Override
    public void matchAnyDemographic(boolean match) {
        this.demographicIdTerms.add(getTermFactory().createIdWildcardTerm(match));
        return;
    }

    
    /**
     *  Clears all demographic terms. 
     */

    @OSID @Override
    public void clearDemographicTerms() {
        clearWildcardTerms(this.demographicIdTerms);
        return;
    }


    /**
     *  Gets all the demographic Id query terms.
     *
     *  @return a collection of the demographic Id query terms
     */

    protected java.util.Collection<org.osid.search.terms.IdTerm> getDemographicIdTerms() {
        return (java.util.Collections.unmodifiableCollection(this.demographicIdTerms));
    }


    /**
     *  Match effective objects where the current date falls within
     *  the start and end dates inclusive.
     *
     *  @param  match <code> true </code> to match any effective, <code> false 
     *          </code> to match ineffective 
     */

    @OSID @Override
    public void matchEffective(boolean match) {
        this.query.matchEffective(match);
        return;
    }


    /**
     *  Clears the effective query terms. 
     */

    @OSID @Override
    public void clearEffectiveTerms() {
        this.query.clearEffectiveTerms();
        return;
    }


    /**
     *  Gets all the effective query terms.
     *
     *  @return a collection of the effective query terms
     */

    protected java.util.Collection<org.osid.search.terms.BooleanTerm> getEffectiveTerms() {
        return (this.query.getEffectiveTerms());
    }


    /**
     *  Matches temporals whose start date falls in between the given dates 
     *  inclusive. 
     *
     *  @param  start start of date range 
     *  @param  end end of date range 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is less 
     *          than <code> end </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchStartDate(org.osid.calendaring.DateTime start, org.osid.calendaring.DateTime end, 
                               boolean match) {
        this.query.matchStartDate(start, end, match);
        return;
    }


    /**
     *  Matches temporals with any start date set. 
     *
     *  @param  match <code> true </code> to match any start date, <code> 
     *          false </code> to match no start date 
     */

    @OSID @Override
    public void matchAnyStartDate(boolean match) {
        this.query.matchAnyStartDate(match);
        return;
    }


    /**
     *  Clears the start date query terms. 
     */

    @OSID @Override
    public void clearStartDateTerms() {
        this.query.clearStartDateTerms();
        return;
    }


    /**
     *  Gets all the start date query terms.
     *
     *  @return a collection of the start date query terms
     */

    protected java.util.Collection<org.osid.search.terms.DateTimeRangeTerm> getStartDateTerms() {
        return (this.getStartDateTerms());
    }


    /**
     *  Matches temporals whose end date falls in between the given dates 
     *  inclusive. 
     *
     *  @param  start the start of date range 
     *  @param  end the end of date range 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> end </code> is less 
     *          than <code> end </code> 
     *  @throws org.osid.NullArgumentException <code> end </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchEndDate(org.osid.calendaring.DateTime start, org.osid.calendaring.DateTime end, 
                               boolean match) {
        this.query.matchEndDate(start, end, match);
        return;
    }


    /**
     *  Matches temporals with any end date set. 
     *
     *  @param  match <code> true </code> to match any end date, <code> 
     *          false </code> to match no end date 
     */

    @OSID @Override
    public void matchAnyEndDate(boolean match) {
        this.query.matchAnyEndDate(match);
        return;
    }


    /**
     *  Clears the end date query terms. 
     */

    @OSID @Override
    public void clearEndDateTerms() {
        this.query.clearEndDateTerms();
        return;
    }


    /**
     *  Gets all the end date query terms.
     *
     *  @return a collection of the end date query terms
     */

    protected java.util.Collection<org.osid.search.terms.DateTimeRangeTerm> getEndDateTerms() {
        return (this.query.getEndDateTerms());
    }


    /**
     *  Matches temporals where the given date is included within the start 
     *  and end dates inclusive. 
     *
     *  @param from start date
     *  @param to end date
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException
     *          <code>from</code> is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code>
     *          or <code>to</code> is <code> null </code>
     */

    @OSID @Override
    public void matchDate(org.osid.calendaring.DateTime from, 
                          org.osid.calendaring.DateTime to,
                          boolean match) {
        this.query.matchDate(from, to, match);
        return;
    }


    /**
     *  Clears the date query terms. 
     */

    @OSID @Override
    public void clearDateTerms() {
        this.query.clearDateTerms();
        return;
    }


    /**
     *  Gets all the date query terms.
     *
     *  @return a collection of the date query terms
     */

    protected java.util.Collection<org.osid.search.terms.DateTimeRangeTerm> getDateTerms() {
        return (this.query.getDateTerms());
    }

    
    protected class OsidTemporalQuery
        extends AbstractOsidTemporalQuery
        implements org.osid.OsidTemporalQuery {


        /**
         *  Constructs a new <code>OsidTemporalQuery</code>.
         *
         *  @param factory the term factory
         *  @throws org.osid.NullArgumentException <code>factory</code> is
         *          <code>null</code>
         */

        protected OsidTemporalQuery(net.okapia.osid.jamocha.query.TermFactory factory) {
            super(factory);
            return;
        }


        /**
         *  Match effective objects where the current date falls within
         *  the start and end dates inclusive.
         *
         *  @param  match <code> true </code> to match any effective, <code> false 
         *          </code> to match ineffective 
         */

        @OSID @Override
        public void matchEffective(boolean match) {
            super.matchEffective(match);
            return;
        }


        /**
         *  Clears the effective query terms. 
         */

        @OSID @Override
        public void clearEffectiveTerms() {
            super.clearEffectiveTerms();
            return;
        }


        /**
         *  Gets all the effective query terms.
         *
         *  @return a collection of the effective query terms
         */

        @Override
        protected java.util.Collection<org.osid.search.terms.BooleanTerm> getEffectiveTerms() {
            return (super.getEffectiveTerms());
        }


        /**
         *  Matches temporals whose start date falls in between the given dates 
         *  inclusive. 
         *
         *  @param  start start of date range 
         *  @param  end end of date range 
         *  @param  match <code> true </code> if a positive match, <code> false 
         *          </code> for a negative match 
         *  @throws org.osid.InvalidArgumentException <code> start </code> is less 
         *          than <code> end </code> 
         *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
         *          end </code> is <code> null </code> 
         */

        @OSID @Override
        public void matchStartDate(org.osid.calendaring.DateTime start, org.osid.calendaring.DateTime end, 
                                   boolean match) {
            super.matchStartDate(start, end, match);
            return;
        }


        /**
         *  Matches temporals with any start date set. 
         *
         *  @param  match <code> true </code> to match any start date, <code> 
         *          false </code> to match no start date 
         */

        @OSID @Override
        public void matchAnyStartDate(boolean match) {
            super.matchAnyStartDate(match);
            return;
        }


        /**
         *  Clears the start date query terms. 
         */

        @OSID @Override
        public void clearStartDateTerms() {
            super.clearStartDateTerms();
            return;
        }


        /**
         *  Gets all the start date query terms.
         *
         *  @return a collection of the start date query terms
         */

        @Override
        protected java.util.Collection<org.osid.search.terms.DateTimeRangeTerm> getStartDateTerms() {
            return (this.getStartDateTerms());
        }


        /**
         *  Matches temporals whose end date falls in between the given dates 
         *  inclusive. 
         *
         *  @param  start the startof date range 
         *  @param  end the end of date range 
         *  @param  match <code> true </code> if a positive match, <code> false 
         *          </code> for a negative match 
         *  @throws org.osid.InvalidArgumentException <code> end </code> is less 
         *          than <code> end </code> 
         *  @throws org.osid.NullArgumentException <code> end </code> or <code> 
         *          end </code> is <code> null </code> 
         */

        @OSID @Override
        public void matchEndDate(org.osid.calendaring.DateTime start, org.osid.calendaring.DateTime end, 
                                 boolean match) {
            super.matchEndDate(start, end, match);
            return;
        }


        /**
         *  Matches temporals with any end date set. 
         *
         *  @param  match <code> true </code> to match any end date, <code> 
         *          false </code> to match no end date 
         */

        @OSID @Override
        public void matchAnyEndDate(boolean match) {
            super.matchAnyEndDate(match);
            return;
        }


        /**
         *  Clears the end date query terms. 
         */

        @OSID @Override
        public void clearEndDateTerms() {
            super.clearEndDateTerms();
            return;
        }


        /**
         *  Gets all the end date query terms.
         *
         *  @return a collection of the end date query terms
         */

        @Override
        protected java.util.Collection<org.osid.search.terms.DateTimeRangeTerm> getEndDateTerms() {
            return (super.getEndDateTerms());
        }


        /**
         *  Matches temporals where the given date is included within
         *  the start and end dates inclusive.
         *
         *  @param from start date
         *  @param to end date
         *  @param  match <code> true </code> if a positive match, <code> false 
         *          </code> for a negative match 
         *  @throws org.osid.InvalidArgumentException
         *          <code>from</code> is greater than <code>to</code>
         *  @throws org.osid.NullArgumentException <code>from</code>
         *          or <code>to</code> is <code> null </code>
         */

        @OSID @Override
        public void matchDate(org.osid.calendaring.DateTime from, 
                              org.osid.calendaring.DateTime to,
                              boolean match) {
            super.matchDate(from, to, match);
            return;
        }


        /**
         *  Clears the date query terms. 
         */

        @OSID @Override
        public void clearDateTerms() {
            super.clearDateTerms();
            return;
        }


        /**
         *  Gets all the date query terms.
         *
         *  @return a collection of the date query terms
         */

        @Override
        protected java.util.Collection<org.osid.search.terms.DateTimeRangeTerm> getDateTerms() {
            return (super.getDateTerms());
        }
    }
}
