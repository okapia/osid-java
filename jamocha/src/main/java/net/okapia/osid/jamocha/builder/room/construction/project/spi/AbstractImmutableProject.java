//
// AbstractImmutableProject.java
//
//     Wraps a mutable Project to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.room.construction.project.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Project</code> to hide modifiers. This
 *  wrapper provides an immutized Project from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying project whose state changes are visible.
 */

public abstract class AbstractImmutableProject
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableTemporalOsidObject
    implements org.osid.room.construction.Project {

    private final org.osid.room.construction.Project project;


    /**
     *  Constructs a new <code>AbstractImmutableProject</code>.
     *
     *  @param project the project to immutablize
     *  @throws org.osid.NullArgumentException <code>project</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableProject(org.osid.room.construction.Project project) {
        super(project);
        this.project = project;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the building. 
     *
     *  @return the building <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getBuildingId() {
        return (this.project.getBuildingId());
    }


    /**
     *  Gets the building. 
     *
     *  @return the building 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.room.Building getBuilding()
        throws org.osid.OperationFailedException {

        return (this.project.getBuilding());
    }


    /**
     *  Tests if this renovation has a cost. 
     *
     *  @return <code> true </code> if a cost is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasCost() {
        return (this.project.hasCost());
    }


    /**
     *  Gets the cost for this renovation. 
     *
     *  @return the cost 
     *  @throws org.osid.IllegalStateException <code> hasCost() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.Currency getCost() {
        return (this.project.getCost());
    }


    /**
     *  Gets the project record corresponding to the given <code> Project 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record. The <code> ProjectRecordType 
     *  </code> may be the <code> Type </code> returned in <code> 
     *  getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(projectRecordType) 
     *  </code> is <code> true </code> . 
     *
     *  @param  projectRecordType the type of project record to retrieve 
     *  @return the project record 
     *  @throws org.osid.NullArgumentException <code> projectRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(projectRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.room.construction.records.ProjectRecord getProjectRecord(org.osid.type.Type projectRecordType)
        throws org.osid.OperationFailedException {

        return (this.project.getProjectRecord(projectRecordType));
    }
}

