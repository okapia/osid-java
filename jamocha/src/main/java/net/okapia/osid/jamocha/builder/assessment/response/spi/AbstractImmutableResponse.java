//
// AbstractImmutableResponse.java
//
//     Wraps a mutable Response to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.assessment.response.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Response</code> to hide modifiers. This
 *  wrapper provides an immutized Response from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying response whose state changes are visible.
 */

public abstract class AbstractImmutableResponse
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidCondition
    implements org.osid.assessment.Response {

    private final org.osid.assessment.Response response;


    /**
     *  Constructs a new <code>AbstractImmutableResponse</code>.
     *
     *  @param response the response to immutablize
     *  @throws org.osid.NullArgumentException <code>response</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableResponse(org.osid.assessment.Response response) {
        super(response);
        this.response = response;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the <code>Item</code>.
     *
     *  @return the assessment item <code> Id </code>
     */

    @OSID @Override
    public org.osid.id.Id getItemId() {
        return (this.response.getItemId());
    }


    /**
     *  Gets the <code> Item. </code>
     *
     *  @return the assessment item
     */

    @OSID @Override
    public org.osid.assessment.Item getItem() {
        return (this.response.getItem());
    }


    /**
     *  Gets the item record corresponding to the given <code> Item </code> 
     *  record <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record. The <code> itemRecordType </code> 
     *  may be the <code> Type </code> returned in <code> getRecordTypes() 
     *  </code> or any of its parents in a <code> Type </code> hierarchy where 
     *  <code> hasRecordType(itemRecordType) </code> is <code> true </code> . 
     *
     *  @param  itemRecordType the type of the record to retrieve 
     *  @return the response record 
     *  @throws org.osid.NullArgumentException <code> itemRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(itemRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.records.ResponseRecord getResponseRecord(org.osid.type.Type itemRecordType)
        throws org.osid.OperationFailedException {

        return (this.response.getResponseRecord(itemRecordType));
    }
}

