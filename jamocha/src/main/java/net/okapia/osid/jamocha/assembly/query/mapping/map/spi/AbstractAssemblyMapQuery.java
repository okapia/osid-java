//
// AbstractAssemblyMapQuery.java
//
//     A MapQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.mapping.map.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A MapQuery that stores terms.
 */

public abstract class AbstractAssemblyMapQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCatalogQuery
    implements org.osid.mapping.MapQuery,
               org.osid.mapping.MapQueryInspector,
               org.osid.mapping.MapSearchOrder {

    private final java.util.Collection<org.osid.mapping.records.MapQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.mapping.records.MapQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.mapping.records.MapSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyMapQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyMapQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the location <code> Id </code> for this query to match maps that 
     *  have a related location. 
     *
     *  @param  locationId a location <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> locationId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchLocationId(org.osid.id.Id locationId, boolean match) {
        getAssembler().addIdTerm(getLocationIdColumn(), locationId, match);
        return;
    }


    /**
     *  Clears the location <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearLocationIdTerms() {
        getAssembler().clearTerms(getLocationIdColumn());
        return;
    }


    /**
     *  Gets the location <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getLocationIdTerms() {
        return (getAssembler().getIdTerms(getLocationIdColumn()));
    }


    /**
     *  Gets the LocationId column name.
     *
     * @return the column name
     */

    protected String getLocationIdColumn() {
        return ("location_id");
    }


    /**
     *  Tests if a <code> LocationQuery </code> is available. 
     *
     *  @return <code> true </code> if a location query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocationQuery() {
        return (false);
    }


    /**
     *  Gets the query for a location. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the location query 
     *  @throws org.osid.UnimplementedException <code> supportsLocationQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationQuery getLocationQuery() {
        throw new org.osid.UnimplementedException("supportsLocationQuery() is false");
    }


    /**
     *  Matches maps that have any location. 
     *
     *  @param  match <code> true </code> to match maps with any location, 
     *          <code> false </code> to match maps with no location 
     */

    @OSID @Override
    public void matchAnyLocation(boolean match) {
        getAssembler().addIdWildcardTerm(getLocationColumn(), match);
        return;
    }


    /**
     *  Clears the location query terms. 
     */

    @OSID @Override
    public void clearLocationTerms() {
        getAssembler().clearTerms(getLocationColumn());
        return;
    }


    /**
     *  Gets the location query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.LocationQueryInspector[] getLocationTerms() {
        return (new org.osid.mapping.LocationQueryInspector[0]);
    }


    /**
     *  Gets the Location column name.
     *
     * @return the column name
     */

    protected String getLocationColumn() {
        return ("location");
    }


    /**
     *  Sets the path <code> Id </code> for this query to match maps 
     *  containing paths. 
     *
     *  @param  pathId the path <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> pathId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchPathId(org.osid.id.Id pathId, boolean match) {
        getAssembler().addIdTerm(getPathIdColumn(), pathId, match);
        return;
    }


    /**
     *  Clears the path <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearPathIdTerms() {
        getAssembler().clearTerms(getPathIdColumn());
        return;
    }


    /**
     *  Gets the path <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPathIdTerms() {
        return (getAssembler().getIdTerms(getPathIdColumn()));
    }


    /**
     *  Gets the PathId column name.
     *
     * @return the column name
     */

    protected String getPathIdColumn() {
        return ("path_id");
    }


    /**
     *  Tests if a <code> PathQuery </code> is available. 
     *
     *  @return <code> true </code> if a path query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathQuery() {
        return (false);
    }


    /**
     *  Gets the query for a path. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the path query 
     *  @throws org.osid.UnimplementedException <code> supportsPathQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathQuery getPathQuery() {
        throw new org.osid.UnimplementedException("supportsPathQuery() is false");
    }


    /**
     *  Matches maps that have any path. 
     *
     *  @param  match <code> true </code> to match maps with any path, <code> 
     *          false </code> to match maps with no path 
     */

    @OSID @Override
    public void matchAnyPath(boolean match) {
        getAssembler().addIdWildcardTerm(getPathColumn(), match);
        return;
    }


    /**
     *  Clears the path query terms. 
     */

    @OSID @Override
    public void clearPathTerms() {
        getAssembler().clearTerms(getPathColumn());
        return;
    }


    /**
     *  Gets the path query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.path.PathQueryInspector[] getPathTerms() {
        return (new org.osid.mapping.path.PathQueryInspector[0]);
    }


    /**
     *  Gets the Path column name.
     *
     * @return the column name
     */

    protected String getPathColumn() {
        return ("path");
    }


    /**
     *  Sets the path <code> Id </code> for this query to match maps 
     *  containing paths. 
     *
     *  @param  pathId the path <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> pathId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchRouteId(org.osid.id.Id pathId, boolean match) {
        getAssembler().addIdTerm(getRouteIdColumn(), pathId, match);
        return;
    }


    /**
     *  Clears the route <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRouteIdTerms() {
        getAssembler().clearTerms(getRouteIdColumn());
        return;
    }


    /**
     *  Gets the route <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRouteIdTerms() {
        return (getAssembler().getIdTerms(getRouteIdColumn()));
    }


    /**
     *  Gets the RouteId column name.
     *
     * @return the column name
     */

    protected String getRouteIdColumn() {
        return ("route_id");
    }


    /**
     *  Tests if a <code> RouteQuery </code> is available. 
     *
     *  @return <code> true </code> if a route query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRouteQuery() {
        return (false);
    }


    /**
     *  Gets the query for a route. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the route query 
     *  @throws org.osid.UnimplementedException <code> supportsRouteQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteQuery getRouteQuery() {
        throw new org.osid.UnimplementedException("supportsRouteQuery() is false");
    }


    /**
     *  Matches maps that have any route. 
     *
     *  @param  match <code> true </code> to match maps with any route, <code> 
     *          false </code> to match maps with no route 
     */

    @OSID @Override
    public void matchAnyRoute(boolean match) {
        getAssembler().addIdWildcardTerm(getRouteColumn(), match);
        return;
    }


    /**
     *  Clears the route query terms. 
     */

    @OSID @Override
    public void clearRouteTerms() {
        getAssembler().clearTerms(getRouteColumn());
        return;
    }


    /**
     *  Gets the route query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteQueryInspector[] getRouteTerms() {
        return (new org.osid.mapping.route.RouteQueryInspector[0]);
    }


    /**
     *  Gets the Route column name.
     *
     * @return the column name
     */

    protected String getRouteColumn() {
        return ("route");
    }


    /**
     *  Sets the map <code> Id </code> for this query to match maps that have 
     *  the specified map as an ancestor. 
     *
     *  @param  mapId a map <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchAncestorMapId(org.osid.id.Id mapId, boolean match) {
        getAssembler().addIdTerm(getAncestorMapIdColumn(), mapId, match);
        return;
    }


    /**
     *  Clears the ancestor map <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorMapIdTerms() {
        getAssembler().clearTerms(getAncestorMapIdColumn());
        return;
    }


    /**
     *  Gets the ancestor map <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorMapIdTerms() {
        return (getAssembler().getIdTerms(getAncestorMapIdColumn()));
    }


    /**
     *  Gets the AncestorMapId column name.
     *
     * @return the column name
     */

    protected String getAncestorMapIdColumn() {
        return ("ancestor_map_id");
    }


    /**
     *  Tests if a <code> MapQuery </code> is available. 
     *
     *  @return <code> true </code> if a map query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorMapQuery() {
        return (false);
    }


    /**
     *  Gets the query for a map. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the map query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorMapQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.MapQuery getAncestorMapQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorMapQuery() is false");
    }


    /**
     *  Matches maps with any ancestor. 
     *
     *  @param  match <code> true </code> to match maps with any ancestor, 
     *          <code> false </code> to match root maps 
     */

    @OSID @Override
    public void matchAnyAncestorMap(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorMapColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor map query terms. 
     */

    @OSID @Override
    public void clearAncestorMapTerms() {
        getAssembler().clearTerms(getAncestorMapColumn());
        return;
    }


    /**
     *  Gets the ancestor map query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.MapQueryInspector[] getAncestorMapTerms() {
        return (new org.osid.mapping.MapQueryInspector[0]);
    }


    /**
     *  Gets the AncestorMap column name.
     *
     * @return the column name
     */

    protected String getAncestorMapColumn() {
        return ("ancestor_map");
    }


    /**
     *  Sets the map <code> Id </code> for this query to match maps that have 
     *  the specified map as a descendant. 
     *
     *  @param  mapId a map <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchDescendantMapId(org.osid.id.Id mapId, boolean match) {
        getAssembler().addIdTerm(getDescendantMapIdColumn(), mapId, match);
        return;
    }


    /**
     *  Clears the descendant map <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantMapIdTerms() {
        getAssembler().clearTerms(getDescendantMapIdColumn());
        return;
    }


    /**
     *  Gets the descendant map <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantMapIdTerms() {
        return (getAssembler().getIdTerms(getDescendantMapIdColumn()));
    }


    /**
     *  Gets the DescendantMapId column name.
     *
     * @return the column name
     */

    protected String getDescendantMapIdColumn() {
        return ("descendant_map_id");
    }


    /**
     *  Tests if a <code> MapQuery </code> is available. 
     *
     *  @return <code> true </code> if a map query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantMapQuery() {
        return (false);
    }


    /**
     *  Gets the query for a map. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the map query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantMapQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.MapQuery getDescendantMapQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantMapQuery() is false");
    }


    /**
     *  Matches maps with any descendant. 
     *
     *  @param  match <code> true </code> to match maps with any descendant, 
     *          <code> false </code> to match leaf maps 
     */

    @OSID @Override
    public void matchAnyDescendantMap(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantMapColumn(), match);
        return;
    }


    /**
     *  Clears the descendant map query terms. 
     */

    @OSID @Override
    public void clearDescendantMapTerms() {
        getAssembler().clearTerms(getDescendantMapColumn());
        return;
    }


    /**
     *  Gets the descendant map query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.MapQueryInspector[] getDescendantMapTerms() {
        return (new org.osid.mapping.MapQueryInspector[0]);
    }


    /**
     *  Gets the DescendantMap column name.
     *
     * @return the column name
     */

    protected String getDescendantMapColumn() {
        return ("descendant_map");
    }


    /**
     *  Tests if this map supports the given record
     *  <code>Type</code>.
     *
     *  @param  mapRecordType a map record type 
     *  @return <code>true</code> if the mapRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>mapRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type mapRecordType) {
        for (org.osid.mapping.records.MapQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(mapRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  mapRecordType the map record type 
     *  @return the map query record 
     *  @throws org.osid.NullArgumentException
     *          <code>mapRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(mapRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.records.MapQueryRecord getMapQueryRecord(org.osid.type.Type mapRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.records.MapQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(mapRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(mapRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  mapRecordType the map record type 
     *  @return the map query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>mapRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(mapRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.records.MapQueryInspectorRecord getMapQueryInspectorRecord(org.osid.type.Type mapRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.records.MapQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(mapRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(mapRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param mapRecordType the map record type
     *  @return the map search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>mapRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(mapRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.records.MapSearchOrderRecord getMapSearchOrderRecord(org.osid.type.Type mapRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.records.MapSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(mapRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(mapRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this map. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param mapQueryRecord the map query record
     *  @param mapQueryInspectorRecord the map query inspector
     *         record
     *  @param mapSearchOrderRecord the map search order record
     *  @param mapRecordType map record type
     *  @throws org.osid.NullArgumentException
     *          <code>mapQueryRecord</code>,
     *          <code>mapQueryInspectorRecord</code>,
     *          <code>mapSearchOrderRecord</code> or
     *          <code>mapRecordTypemap</code> is
     *          <code>null</code>
     */
            
    protected void addMapRecords(org.osid.mapping.records.MapQueryRecord mapQueryRecord, 
                                      org.osid.mapping.records.MapQueryInspectorRecord mapQueryInspectorRecord, 
                                      org.osid.mapping.records.MapSearchOrderRecord mapSearchOrderRecord, 
                                      org.osid.type.Type mapRecordType) {

        addRecordType(mapRecordType);

        nullarg(mapQueryRecord, "map query record");
        nullarg(mapQueryInspectorRecord, "map query inspector record");
        nullarg(mapSearchOrderRecord, "map search odrer record");

        this.queryRecords.add(mapQueryRecord);
        this.queryInspectorRecords.add(mapQueryInspectorRecord);
        this.searchOrderRecords.add(mapSearchOrderRecord);
        
        return;
    }
}
