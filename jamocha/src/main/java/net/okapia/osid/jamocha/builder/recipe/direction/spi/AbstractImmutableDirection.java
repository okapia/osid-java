//
// AbstractImmutableDirection.java
//
//     Wraps a mutable Direction to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.recipe.direction.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Direction</code> to hide modifiers. This
 *  wrapper provides an immutized Direction from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying direction whose state changes are visible.
 */

public abstract class AbstractImmutableDirection
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.recipe.Direction {

    private final org.osid.recipe.Direction direction;


    /**
     *  Constructs a new <code>AbstractImmutableDirection</code>.
     *
     *  @param direction the direction to immutablize
     *  @throws org.osid.NullArgumentException <code>direction</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableDirection(org.osid.recipe.Direction direction) {
        super(direction);
        this.direction = direction;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the recipe. 
     *
     *  @return the recipe <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getRecipeId() {
        return (this.direction.getRecipeId());
    }


    /**
     *  Gets the recipe. 
     *
     *  @return the recipe 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.recipe.Recipe getRecipe()
        throws org.osid.OperationFailedException {

        return (this.direction.getRecipe());
    }


    /**
     *  Gets the <code> Ids </code> of the procedures to execute in
     *  this direction.
     *
     *  @return the procedure <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getProcedureIds() {
        return (this.direction.getProcedureIds());
    }


    /**
     *  Gets the procedures to execute in this direction. 
     *
     *  @return the procedures 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureList getProcedures()
        throws org.osid.OperationFailedException {

        return (this.direction.getProcedures());
    }


    /**
     *  Gets the <code> Ids </code> of the ingredients.
     *
     *  @return the ingredient <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getIngredientIds() {
        return (this.direction.getIngredientIds());
    }


    /**
     *  Gets the required ingredients. 
     *
     *  @return the ingredients 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.recipe.IngredientList getIngredients()
        throws org.osid.OperationFailedException {

        return (this.direction.getIngredients());
    }


    /**
     *  Gets the estimated time required for this direction. 
     *
     *  @return the estimated duration 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getEstimatedDuration() {
        return (this.direction.getEstimatedDuration());
    }


    /**
     *  Gets any asset <code> Ids </code> to assist in carrying out this 
     *  direction. 
     *
     *  @return the asset <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getAssetIds() {
        return (this.direction.getAssetIds());
    }


    /**
     *  Gets any assets to assist in carrying out this direction. 
     *
     *  @return the assets 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.repository.AssetList getAssets()
        throws org.osid.OperationFailedException {

        return (this.direction.getAssets());
    }


    /**
     *  Gets the direction record corresponding to the given <code> Direction 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record. The <code> 
     *  directionRecordType </code> may be the <code> Type </code> returned in 
     *  <code> getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(directionRecordType) 
     *  </code> is <code> true </code> . 
     *
     *  @param  directionRecordType the type of direction record to retrieve 
     *  @return the direction record 
     *  @throws org.osid.NullArgumentException <code> directionRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(directionRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recipe.records.DirectionRecord getDirectionRecord(org.osid.type.Type directionRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.direction.getDirectionRecord(directionRecordType));
    }
}

