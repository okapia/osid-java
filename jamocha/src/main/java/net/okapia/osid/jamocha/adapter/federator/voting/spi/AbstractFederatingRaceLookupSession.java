//
// AbstractFederatingRaceLookupSession.java
//
//     An abstract federating adapter for a RaceLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.voting.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  RaceLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingRaceLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.voting.RaceLookupSession>
    implements org.osid.voting.RaceLookupSession {

    private boolean parallel = false;
    private org.osid.voting.Polls polls = new net.okapia.osid.jamocha.nil.voting.polls.UnknownPolls();


    /**
     *  Constructs a new <code>AbstractFederatingRaceLookupSession</code>.
     */

    protected AbstractFederatingRaceLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.voting.RaceLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Polls/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Polls Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getPollsId() {
        return (this.polls.getId());
    }


    /**
     *  Gets the <code>Polls</code> associated with this 
     *  session.
     *
     *  @return the <code>Polls</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Polls getPolls()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.polls);
    }


    /**
     *  Sets the <code>Polls</code>.
     *
     *  @param  polls the polls for this session
     *  @throws org.osid.NullArgumentException <code>polls</code>
     *          is <code>null</code>
     */

    protected void setPolls(org.osid.voting.Polls polls) {
        nullarg(polls, "polls");
        this.polls = polls;
        return;
    }


    /**
     *  Tests if this user can perform <code>Race</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupRaces() {
        for (org.osid.voting.RaceLookupSession session : getSessions()) {
            if (session.canLookupRaces()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Race</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeRaceView() {
        for (org.osid.voting.RaceLookupSession session : getSessions()) {
            session.useComparativeRaceView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Race</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryRaceView() {
        for (org.osid.voting.RaceLookupSession session : getSessions()) {
            session.usePlenaryRaceView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include races in pollses which are children
     *  of this polls in the polls hierarchy.
     */

    @OSID @Override
    public void useFederatedPollsView() {
        for (org.osid.voting.RaceLookupSession session : getSessions()) {
            session.useFederatedPollsView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this polls only.
     */

    @OSID @Override
    public void useIsolatedPollsView() {
        for (org.osid.voting.RaceLookupSession session : getSessions()) {
            session.useIsolatedPollsView();
        }

        return;
    }


    /**
     *  Only active races are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveRaceView() {
        for (org.osid.voting.RaceLookupSession session : getSessions()) {
            session.useActiveRaceView();
        }

        return;
    }


    /**
     *  Active and inactive races are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusRaceView() {
        for (org.osid.voting.RaceLookupSession session : getSessions()) {
            session.useAnyStatusRaceView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>Race</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Race</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Race</code> and
     *  retained for compatibility.
     *
     *  In active mode, races are returned that are currently
     *  active. In any status mode, active and inactive races
     *  are returned.
     *
     *  @param  raceId <code>Id</code> of the
     *          <code>Race</code>
     *  @return the race
     *  @throws org.osid.NotFoundException <code>raceId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>raceId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Race getRace(org.osid.id.Id raceId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.voting.RaceLookupSession session : getSessions()) {
            try {
                return (session.getRace(raceId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(raceId + " not found");
    }


    /**
     *  Gets a <code>RaceList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  races specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Races</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, races are returned that are currently
     *  active. In any status mode, active and inactive races
     *  are returned.
     *
     *  @param  raceIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Race</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>raceIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.RaceList getRacesByIds(org.osid.id.IdList raceIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.voting.race.MutableRaceList ret = new net.okapia.osid.jamocha.voting.race.MutableRaceList();

        try (org.osid.id.IdList ids = raceIds) {
            while (ids.hasNext()) {
                ret.addRace(getRace(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>RaceList</code> corresponding to the given
     *  race genus <code>Type</code> which does not include
     *  races of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  races or an error results. Otherwise, the returned list
     *  may contain only those races that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, races are returned that are currently
     *  active. In any status mode, active and inactive races
     *  are returned.
     *
     *  @param  raceGenusType a race genus type 
     *  @return the returned <code>Race</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>raceGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.RaceList getRacesByGenusType(org.osid.type.Type raceGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.race.FederatingRaceList ret = getRaceList();

        for (org.osid.voting.RaceLookupSession session : getSessions()) {
            ret.addRaceList(session.getRacesByGenusType(raceGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>RaceList</code> corresponding to the given
     *  race genus <code>Type</code> and include any additional
     *  races with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  races or an error results. Otherwise, the returned list
     *  may contain only those races that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, races are returned that are currently
     *  active. In any status mode, active and inactive races
     *  are returned.
     *
     *  @param  raceGenusType a race genus type 
     *  @return the returned <code>Race</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>raceGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.RaceList getRacesByParentGenusType(org.osid.type.Type raceGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.race.FederatingRaceList ret = getRaceList();

        for (org.osid.voting.RaceLookupSession session : getSessions()) {
            ret.addRaceList(session.getRacesByParentGenusType(raceGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>RaceList</code> containing the given
     *  race record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  races or an error results. Otherwise, the returned list
     *  may contain only those races that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, races are returned that are currently
     *  active. In any status mode, active and inactive races
     *  are returned.
     *
     *  @param  raceRecordType a race record type 
     *  @return the returned <code>Race</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>raceRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.RaceList getRacesByRecordType(org.osid.type.Type raceRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.race.FederatingRaceList ret = getRaceList();

        for (org.osid.voting.RaceLookupSession session : getSessions()) {
            ret.addRaceList(session.getRacesByRecordType(raceRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>RaceList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known races or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  races that are accessible through this session. 
     *
     *  In active mode, races are returned that are currently
     *  active. In any status mode, active and inactive races
     *  are returned.
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Race</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.voting.RaceList getRacesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        net.okapia.osid.jamocha.adapter.federator.voting.race.FederatingRaceList ret = getRaceList();

        for (org.osid.voting.RaceLookupSession session : getSessions()) {
            ret.addRaceList(session.getRacesByProvider(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>RaceList</code> for the given
     *  <code>Ballot</code>.
     *
     *  In plenary mode, the returned list contains all known races or
     *  an error results. Otherwise, the returned list may contain
     *  only those races that are accessible through this session.
     *
     *  In active mode, races are returned that are currently
     *  active. In any status mode, active and inactive races are
     *  returned.
     *
     *  @param  ballotId a ballot <code>Id</code>
     *  @return the returned <code>Race</code> list
     *  @throws org.osid.NullArgumentException <code>ballotId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.RaceList getRacesForBallot(org.osid.id.Id ballotId)
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.race.FederatingRaceList ret = getRaceList();

        for (org.osid.voting.RaceLookupSession session : getSessions()) {
            ret.addRaceList(session.getRacesForBallot(ballotId));
        }

        ret.noMore();
        return (ret);
    }

    
    /**
     *  Gets all <code>Races</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  races or an error results. Otherwise, the returned list
     *  may contain only those races that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, races are returned that are currently
     *  active. In any status mode, active and inactive races
     *  are returned.
     *
     *  @return a list of <code>Races</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.RaceList getRaces()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.race.FederatingRaceList ret = getRaceList();

        for (org.osid.voting.RaceLookupSession session : getSessions()) {
            ret.addRaceList(session.getRaces());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.voting.race.FederatingRaceList getRaceList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.voting.race.ParallelRaceList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.voting.race.CompositeRaceList());
        }
    }
}
