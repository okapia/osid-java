//
// InlineHierarchyNotifier.java
//
//     A callback interface for performing hierarchy
//     notifications.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.hierarchy.spi;


/**
 *  A callback interface for performing hierarchy
 *  notifications.
 */

public interface InlineHierarchyNotifier {



    /**
     *  Notifies the creation of a new hierarchy.
     *
     *  @param hierarchyId the {@code Id} of the new 
     *         hierarchy
     *  @throws org.osid.NullArgumentException {@code [objectId]}
     *          is {@code null}
     */

    public void newHierarchy(org.osid.id.Id hierarchyId);


    /**
     *  Notifies the change of an updated hierarchy.
     *
     *  @param hierarchyId the {@code Id} of the changed
     *         hierarchy
     *  @throws org.osid.NullArgumentException {@code [objectId]}
     *          is {@code null}
     */
      
    public void changedHierarchy(org.osid.id.Id hierarchyId);


    /**
     *  Notifies the deletion of an hierarchy.
     *
     *  @param hierarchyId the {@code Id} of the deleted
     *         hierarchy
     *  @throws org.osid.NullArgumentException {@code [objectId]} is
     *          {@code null}
     */

    public void deletedHierarchy(org.osid.id.Id hierarchyId);

}
