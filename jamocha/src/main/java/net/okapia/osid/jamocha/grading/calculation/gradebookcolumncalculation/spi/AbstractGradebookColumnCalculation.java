//
// AbstractGradebookColumnCalculation.java
//
//     Defines a GradebookColumnCalculation.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.calculation.gradebookcolumncalculation.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>GradebookColumnCalculation</code>.
 */

public abstract class AbstractGradebookColumnCalculation
    extends net.okapia.osid.jamocha.spi.AbstractOsidRule
    implements org.osid.grading.calculation.GradebookColumnCalculation {

    private org.osid.grading.GradebookColumn gradebookColumn;
    private final java.util.Collection<org.osid.grading.GradebookColumn> inputGradebookColumns = new java.util.LinkedHashSet<>();
    private org.osid.grading.calculation.CalculationOperation operation;
    private java.math.BigDecimal tweakedCenter;
    private java.math.BigDecimal tweakedStandardDeviation;

    private final java.util.Collection<org.osid.grading.calculation.records.GradebookColumnCalculationRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> GradebookColumn Ids </code> to which this column 
     *  applies. 
     *
     *  @return the gradebook column <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getGradebookColumnId() {
        return (this.gradebookColumn.getId());
    }


    /**
     *  Gets the <code> GradebookColumn </code> to which this calculation 
     *  applies. 
     *
     *  @return the gradebook column 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumn getGradebookColumn()
        throws org.osid.OperationFailedException {

        return (this.gradebookColumn);
    }


    /**
     *  Sets the gradebook column.
     *
     *  @param gradebookColumn a gradebook column
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumn</code> is <code>null</code>
     */

    protected void setGradebookColumn(org.osid.grading.GradebookColumn gradebookColumn) {
        nullarg(gradebookColumn, "gradebook column");
        this.gradebookColumn = gradebookColumn;
        return;
    }


    /**
     *  Gets the <code> GradebookColumn Ids </code> from which this column is 
     *  derived. 
     *
     *  @return the derived column <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getInputGradebookColumnIds() {
        try {
            org.osid.grading.GradebookColumnList inputGradebookColumns = getInputGradebookColumns();
            return (new net.okapia.osid.jamocha.adapter.converter.grading.gradebookcolumn.GradebookColumnToIdList(inputGradebookColumns));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the <code> GradebookColumns </code> from which this column is 
     *  derived. 
     *
     *  @return the derived columns 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnList getInputGradebookColumns()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.grading.gradebookcolumn.ArrayGradebookColumnList(this.inputGradebookColumns));
    }


    /**
     *  Adds an input gradebook column.
     *
     *  @param gradebookColumn an input gradebook column
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumn</code> is <code>null</code>
     */

    protected void addInputGradebookColumn(org.osid.grading.GradebookColumn gradebookColumn) {
        nullarg(gradebookColumn, "input gradebook column");
        this.inputGradebookColumns.add(gradebookColumn);
        return;
    }


    /**
     *  Sets all the input gradebook columns.
     *
     *  @param gradebookColumns a collection of input gradebook columns
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumns</code> is <code>null</code>
     */

    protected void setInputGradebookColumns(java.util.Collection<org.osid.grading.GradebookColumn> gradebookColumns) {
        nullarg(gradebookColumns, "input gradebook columns");
        this.inputGradebookColumns.clear();
        this.inputGradebookColumns.addAll(gradebookColumns);
        return;
    }


    /**
     *  Gets the operation to perform for deriving this column. The
     *  output of the operation determines the score or grade for the
     *  column entries.
     *
     *  @return the calculation operation 
     */

    @OSID @Override
    public org.osid.grading.calculation.CalculationOperation getOperation() {
        return (this.operation);
    }


    /**
     *  Sets the operation.
     *
     *  @param operation an operation
     *  @throws org.osid.NullArgumentException <code>operation</code>
     *          is <code>null</code>
     */

    protected void setOperation(org.osid.grading.calculation.CalculationOperation operation) {
        nullarg(operation, "operation");
        this.operation = operation;
        return;
    }


    /**
     *  Gets the tweaked midpoint input value of the grading system for 
     *  determining how to center calculated enries in this column. This tweak 
     *  is applied before calculating standard deviation offsets for entries 
     *  in this derived column. 
     *
     *  @return the tweaked center 
     *  @throws org.osid.IllegalStateException <code> getCalculation() </code> 
     *          is not <code> ColumnCalculation.STD_DEVIATION_OFFSET </code> 
     */

    @OSID @Override
    public java.math.BigDecimal getTweakedCenter() {
        if (getOperation() != org.osid.grading.calculation.CalculationOperation.STD_DEVIATION_OFFSET) {
            throw new org.osid.IllegalStateException("operation not offset");
        }

        return (this.tweakedCenter);
    }


    /**
     *  Sets the tweaked center.
     *
     *  @param center a tweaked center
     *  @throws org.osid.NullArgumentException <code>center</code> is
     *          <code>null</code>
     */

    protected void setTweakedCenter(java.math.BigDecimal center) {
        nullarg(center, "tweaked center");
        this.tweakedCenter = center;
        return;
    }


    /**
     *  Gets the tweaked standard deviation. This tweak is applied before 
     *  calculating standard deviation offsets for entries in this derived 
     *  column. 
     *
     *  @return the tweaked standard deviation 
     *  @throws org.osid.IllegalStateException <code> getCalculation() </code> 
     *          is not <code> ColumnCalculation.STD_DEVIATION_OFFSET </code> 
     */

    @OSID @Override
    public java.math.BigDecimal getTweakedStandardDeviation() {
        if (getOperation() != org.osid.grading.calculation.CalculationOperation.STD_DEVIATION_OFFSET) {
            throw new org.osid.IllegalStateException("operation not offset");
        }

        return (this.tweakedStandardDeviation);
    }


    /**
     *  Sets the tweaked standard deviation.
     *
     *  @param standardDeviation a tweaked standard deviation
     *  @throws org.osid.NullArgumentException
     *          <code>standardDeviation</code> is <code>null</code>
     */

    protected void setTweakedStandardDeviation(java.math.BigDecimal standardDeviation) {
        nullarg(standardDeviation, "tweaked standard deviation");
        this.tweakedStandardDeviation = standardDeviation;
        return;
    }


    /**
     *  Tests if this gradebookColumnCalculation supports the given record
     *  <code>Type</code>.
     *
     *  @param  gradebookColumnCalculationRecordType a gradebook column calculation record type 
     *  @return <code>true</code> if the gradebookColumnCalculationRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumnCalculationRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type gradebookColumnCalculationRecordType) {
        for (org.osid.grading.calculation.records.GradebookColumnCalculationRecord record : this.records) {
            if (record.implementsRecordType(gradebookColumnCalculationRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>GradebookColumnCalculation</code> record
     *  <code>Type</code>.
     *
     *  @param  gradebookColumnCalculationRecordType the gradebook column calculation record type 
     *  @return the gradebook column calculation record 
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumnCalculationRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(gradebookColumnCalculationRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.grading.calculation.records.GradebookColumnCalculationRecord getGradebookColumnCalculationRecord(org.osid.type.Type gradebookColumnCalculationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.grading.calculation.records.GradebookColumnCalculationRecord record : this.records) {
            if (record.implementsRecordType(gradebookColumnCalculationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(gradebookColumnCalculationRecordType + " is not supported");
    }


    /**
     *  Adds a record to this gradebook column calculation. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param gradebookColumnCalculationRecord the gradebook column
     *         calculation record
     *  @param gradebookColumnCalculationRecordType gradebook column
     *         calculation record type
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumnCalculationRecord</code> or
     *          <code>gradebookColumnCalculationRecordTypegradebookColumnCalculation</code>
     *          is <code>null</code>
     */
            
    protected void addGradebookColumnCalculationRecord(org.osid.grading.calculation.records.GradebookColumnCalculationRecord gradebookColumnCalculationRecord, 
                                                       org.osid.type.Type gradebookColumnCalculationRecordType) {

        nullarg(gradebookColumnCalculationRecord, "gradebook column calculation record");
        addRecordType(gradebookColumnCalculationRecordType);
        this.records.add(gradebookColumnCalculationRecord);
        
        return;
    }
}
