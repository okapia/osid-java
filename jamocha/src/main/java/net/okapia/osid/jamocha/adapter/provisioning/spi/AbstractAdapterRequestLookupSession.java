//
// AbstractAdapterRequestLookupSession.java
//
//    A Request lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.provisioning.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Request lookup session adapter.
 */

public abstract class AbstractAdapterRequestLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.provisioning.RequestLookupSession {

    private final org.osid.provisioning.RequestLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterRequestLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterRequestLookupSession(org.osid.provisioning.RequestLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Distributor/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Distributor Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDistributorId() {
        return (this.session.getDistributorId());
    }


    /**
     *  Gets the {@code Distributor} associated with this session.
     *
     *  @return the {@code Distributor} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Distributor getDistributor()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getDistributor());
    }


    /**
     *  Tests if this user can perform {@code Request} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupRequests() {
        return (this.session.canLookupRequests());
    }


    /**
     *  A complete view of the {@code Request} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeRequestView() {
        this.session.useComparativeRequestView();
        return;
    }


    /**
     *  A complete view of the {@code Request} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryRequestView() {
        this.session.usePlenaryRequestView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include requests in distributors which are children
     *  of this distributor in the distributor hierarchy.
     */

    @OSID @Override
    public void useFederatedDistributorView() {
        this.session.useFederatedDistributorView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this distributor only.
     */

    @OSID @Override
    public void useIsolatedDistributorView() {
        this.session.useIsolatedDistributorView();
        return;
    }
    

    /**
     *  Only requests whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveRequestView() {
        this.session.useEffectiveRequestView();
        return;
    }
    

    /**
     *  All requests of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveRequestView() {
        this.session.useAnyEffectiveRequestView();
        return;
    }

     
    /**
     *  Gets the {@code Request} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Request} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Request} and
     *  retained for compatibility.
     *
     *  In effective mode, requests are returned that are currently
     *  effective.  In any effective mode, effective requests and
     *  those currently expired are returned.
     *
     *  @param requestId {@code Id} of the {@code Request}
     *  @return the request
     *  @throws org.osid.NotFoundException {@code requestId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code requestId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Request getRequest(org.osid.id.Id requestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRequest(requestId));
    }


    /**
     *  Gets a {@code RequestList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  requests specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Requests} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, requests are returned that are currently
     *  effective.  In any effective mode, effective requests and
     *  those currently expired are returned.
     *
     *  @param  requestIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Request} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code requestIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.RequestList getRequestsByIds(org.osid.id.IdList requestIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRequestsByIds(requestIds));
    }


    /**
     *  Gets a {@code RequestList} corresponding to the given
     *  request genus {@code Type} which does not include
     *  requests of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  requests or an error results. Otherwise, the returned list
     *  may contain only those requests that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, requests are returned that are currently
     *  effective.  In any effective mode, effective requests and
     *  those currently expired are returned.
     *
     *  @param  requestGenusType a request genus type 
     *  @return the returned {@code Request} list
     *  @throws org.osid.NullArgumentException
     *          {@code requestGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.RequestList getRequestsByGenusType(org.osid.type.Type requestGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRequestsByGenusType(requestGenusType));
    }


    /**
     *  Gets a {@code RequestList} corresponding to the given
     *  request genus {@code Type} and include any additional
     *  requests with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  requests or an error results. Otherwise, the returned list
     *  may contain only those requests that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, requests are returned that are currently
     *  effective.  In any effective mode, effective requests and
     *  those currently expired are returned.
     *
     *  @param  requestGenusType a request genus type 
     *  @return the returned {@code Request} list
     *  @throws org.osid.NullArgumentException
     *          {@code requestGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.RequestList getRequestsByParentGenusType(org.osid.type.Type requestGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRequestsByParentGenusType(requestGenusType));
    }


    /**
     *  Gets a {@code RequestList} containing the given
     *  request record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  requests or an error results. Otherwise, the returned list
     *  may contain only those requests that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, requests are returned that are currently
     *  effective.  In any effective mode, effective requests and
     *  those currently expired are returned.
     *
     *  @param  requestRecordType a request record type 
     *  @return the returned {@code Request} list
     *  @throws org.osid.NullArgumentException
     *          {@code requestRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.RequestList getRequestsByRecordType(org.osid.type.Type requestRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRequestsByRecordType(requestRecordType));
    }


    /**
     *  Gets a {@code RequestList} effective during the entire given
     *  date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known requests
     *  or an error results. Otherwise, the returned list may contain
     *  only those requests that are accessible through this session.
     *  
     *  In active mode, requests are returned that are currently
     *  active. In any status mode, active and inactive requests are
     *  returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Request} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.provisioning.RequestList getRequestsOnDate(org.osid.calendaring.DateTime from, 
                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRequestsOnDate(from, to));
    }
        

    /**
     *  Gets a list of requests corresponding to a requester
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known requests
     *  or an error results. Otherwise, the returned list may contain
     *  only those requests that are accessible through this session.
     *
     *  In effective mode, requests are returned that are currently
     *  effective.  In any effective mode, effective requests and
     *  those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the requester
     *  @return the returned {@code RequestList}
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.RequestList getRequestsForRequester(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRequestsForRequester(resourceId));
    }


    /**
     *  Gets a list of requests corresponding to a requester {@code
     *  Id} and effective during the entire given date range inclusive
     *  but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known requests
     *  or an error results. Otherwise, the returned list may contain
     *  only those requests that are accessible through this session.
     *
     *  In effective mode, requests are returned that are currently
     *  effective.  In any effective mode, effective requests and
     *  those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the requester
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code RequestList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.RequestList getRequestsForRequesterOnDate(org.osid.id.Id resourceId,
                                                                           org.osid.calendaring.DateTime from,
                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRequestsForRequesterOnDate(resourceId, from, to));
    }


    /**
     *  Gets a list of requests corresponding to a queue {@code Id}.
     *
     *  In plenary mode, the returned list contains all known requests
     *  or an error results. Otherwise, the returned list may contain
     *  only those requests that are accessible through this session.
     *
     *  In effective mode, requests are returned that are currently
     *  effective.  In any effective mode, effective requests and
     *  those currently expired are returned.
     *
     *  @param  queueId the {@code Id} of the queue
     *  @return the returned {@code RequestList}
     *  @throws org.osid.NullArgumentException {@code queueId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.RequestList getRequestsForQueue(org.osid.id.Id queueId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRequestsForQueue(queueId));
    }


    /**
     *  Gets a list of requests corresponding to a queue {@code Id}
     *  and effective during the entire given date range inclusive but
     *  not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known requests
     *  or an error results. Otherwise, the returned list may contain
     *  only those requests that are accessible through this session.
     *
     *  In effective mode, requests are returned that are currently
     *  effective.  In any effective mode, effective requests and
     *  those currently expired are returned.
     *
     *  @param  queueId the {@code Id} of the queue
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code RequestList}
     *  @throws org.osid.NullArgumentException {@code queueId}, {@code
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.RequestList getRequestsForQueueOnDate(org.osid.id.Id queueId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRequestsForQueueOnDate(queueId, from, to));
    }


    /**
     *  Gets a list of requests corresponding to requester and queue
     *  {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known requests
     *  or an error results. Otherwise, the returned list may contain
     *  only those requests that are accessible through this session.
     *
     *  In effective mode, requests are returned that are currently
     *  effective.  In any effective mode, effective requests and
     *  those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the requester
     *  @param  queueId the {@code Id} of the queue
     *  @return the returned {@code RequestList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code queueId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.RequestList getRequestsForRequesterAndQueue(org.osid.id.Id resourceId,
                                                                             org.osid.id.Id queueId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRequestsForRequesterAndQueue(resourceId, queueId));
    }


    /**
     *  Gets a list of requests corresponding to requester and queue
     *  {@code Ids} and effective during the entire given date range
     *  inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known requests
     *  or an error results. Otherwise, the returned list may contain
     *  only those requests that are accessible through this session.
     *
     *  In effective mode, requests are returned that are currently
     *  effective. In any effective mode, effective requests and those
     *  currently expired are returned.
     *
     *  @param  queueId the {@code Id} of the queue
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code RequestList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code queueId}, {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.RequestList getRequestsForRequesterAndQueueOnDate(org.osid.id.Id resourceId,
                                                                                   org.osid.id.Id queueId,
                                                                                   org.osid.calendaring.DateTime from,
                                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRequestsForRequesterAndQueueOnDate(resourceId, queueId, from, to));
    }


    /**
     *  Gets all {@code Requests}. 
     *
     *  In plenary mode, the returned list contains all known
     *  requests or an error results. Otherwise, the returned list
     *  may contain only those requests that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, requests are returned that are currently
     *  effective.  In any effective mode, effective requests and
     *  those currently expired are returned.
     *
     *  @return a list of {@code Requests} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.RequestList getRequests()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRequests());
    }
}
