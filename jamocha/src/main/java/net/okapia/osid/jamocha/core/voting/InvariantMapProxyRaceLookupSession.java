//
// InvariantMapProxyRaceLookupSession
//
//    Implements a Race lookup service backed by a fixed
//    collection of races. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.voting;


/**
 *  Implements a Race lookup service backed by a fixed
 *  collection of races. The races are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyRaceLookupSession
    extends net.okapia.osid.jamocha.core.voting.spi.AbstractMapRaceLookupSession
    implements org.osid.voting.RaceLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyRaceLookupSession} with no
     *  races.
     *
     *  @param polls the polls
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code polls} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyRaceLookupSession(org.osid.voting.Polls polls,
                                                  org.osid.proxy.Proxy proxy) {
        setPolls(polls);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyRaceLookupSession} with a single
     *  race.
     *
     *  @param polls the polls
     *  @param race a single race
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code polls},
     *          {@code race} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyRaceLookupSession(org.osid.voting.Polls polls,
                                                  org.osid.voting.Race race, org.osid.proxy.Proxy proxy) {

        this(polls, proxy);
        putRace(race);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyRaceLookupSession} using
     *  an array of races.
     *
     *  @param polls the polls
     *  @param races an array of races
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code polls},
     *          {@code races} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyRaceLookupSession(org.osid.voting.Polls polls,
                                                  org.osid.voting.Race[] races, org.osid.proxy.Proxy proxy) {

        this(polls, proxy);
        putRaces(races);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyRaceLookupSession} using a
     *  collection of races.
     *
     *  @param polls the polls
     *  @param races a collection of races
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code polls},
     *          {@code races} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyRaceLookupSession(org.osid.voting.Polls polls,
                                                  java.util.Collection<? extends org.osid.voting.Race> races,
                                                  org.osid.proxy.Proxy proxy) {

        this(polls, proxy);
        putRaces(races);
        return;
    }
}
