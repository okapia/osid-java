IGES ("model/iges", "Initial Graphics Exchange Specification", "igs", "iges"),
    GDL ("model/vnd.gdl", "Archicad Geometric Description Language (GDL)", "gdl"),
    MOML ("model/vnd.moml+xml", "XML Modeling Markup Language"),
    VRML ("model/vrml", "Virtual Reality Modeling Language", "wrl", "vrml"),
    X3DB ("model/x3d+binary", "Web3D Binary", "x3db", "x3dbz"),
    X3DV ("model/x3d+vrml", "Web 3D Virtual Reality Modeling Language (VRML)", "x3dv", "x3dvz"),
    X3D ("model/x3d+xml", "X3D XML", "x3d", "x3dz");
