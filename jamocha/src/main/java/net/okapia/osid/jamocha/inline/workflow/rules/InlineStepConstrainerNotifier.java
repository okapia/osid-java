//
// InlineStepConstrainerNotifier.java
//
//     A callback interface for performing step constrainer
//     notifications.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.workflow.rules.spi;


/**
 *  A callback interface for performing step constrainer
 *  notifications.
 */

public interface InlineStepConstrainerNotifier {



    /**
     *  Notifies the creation of a new step constrainer.
     *
     *  @param stepConstrainerId the {@code Id} of the new 
     *         step constrainer
     *  @throws org.osid.NullArgumentException {@code [objectId]}
     *          is {@code null}
     */

    public void newStepConstrainer(org.osid.id.Id stepConstrainerId);


    /**
     *  Notifies the change of an updated step constrainer.
     *
     *  @param stepConstrainerId the {@code Id} of the changed
     *         step constrainer
     *  @throws org.osid.NullArgumentException {@code [objectId]}
     *          is {@code null}
     */
      
    public void changedStepConstrainer(org.osid.id.Id stepConstrainerId);


    /**
     *  Notifies the deletion of an step constrainer.
     *
     *  @param stepConstrainerId the {@code Id} of the deleted
     *         step constrainer
     *  @throws org.osid.NullArgumentException {@code [objectId]} is
     *          {@code null}
     */

    public void deletedStepConstrainer(org.osid.id.Id stepConstrainerId);

}
