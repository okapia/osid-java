//
// AbstractPlan.java
//
//     Defines a Plan builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.plan.plan.spi;


/**
 *  Defines a <code>Plan</code> builder.
 */

public abstract class AbstractPlanBuilder<T extends AbstractPlanBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRelationshipBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.course.plan.plan.PlanMiter plan;


    /**
     *  Constructs a new <code>AbstractPlanBuilder</code>.
     *
     *  @param plan the plan to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractPlanBuilder(net.okapia.osid.jamocha.builder.course.plan.plan.PlanMiter plan) {
        super(plan);
        this.plan = plan;
        return;
    }


    /**
     *  Builds the plan.
     *
     *  @return the new plan
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.course.plan.Plan build() {
        (new net.okapia.osid.jamocha.builder.validator.course.plan.plan.PlanValidator(getValidations())).validate(this.plan);
        return (new net.okapia.osid.jamocha.builder.course.plan.plan.ImmutablePlan(this.plan));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the plan miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.course.plan.plan.PlanMiter getMiter() {
        return (this.plan);
    }


    /**
     *  Sets the syllabus.
     *
     *  @param syllabus a syllabus
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>syllabus</code>
     *          is <code>null</code>
     */

    public T syllabus(org.osid.course.syllabus.Syllabus syllabus) {
        getMiter().setSyllabus(syllabus);
        return (self());
    }


    /**
     *  Sets the course offering.
     *
     *  @param courseOffering a course offering
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>courseOffering</code> is <code>null</code>
     */

    public T courseOffering(org.osid.course.CourseOffering courseOffering) {
        getMiter().setCourseOffering(courseOffering);
        return (self());
    }


    /**
     *  Adds a module.
     *
     *  @param module a module
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>module</code> is
     *          <code>null</code>
     */

    public T module(org.osid.course.syllabus.Module module) {
        getMiter().addModule(module);
        return (self());
    }


    /**
     *  Sets all the modules.
     *
     *  @param modules a collection of modules
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>modules</code> is
     *          <code>null</code>
     */

    public T modules(java.util.Collection<org.osid.course.syllabus.Module> modules) {
        getMiter().setModules(modules);
        return (self());
    }


    /**
     *  Adds a Plan record.
     *
     *  @param record a plan record
     *  @param recordType the type of plan record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.course.plan.records.PlanRecord record, org.osid.type.Type recordType) {
        getMiter().addPlanRecord(record, recordType);
        return (self());
    }
}       


