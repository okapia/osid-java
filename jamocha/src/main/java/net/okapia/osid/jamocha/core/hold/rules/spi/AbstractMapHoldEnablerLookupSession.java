//
// AbstractMapHoldEnablerLookupSession
//
//    A simple framework for providing a HoldEnabler lookup service
//    backed by a fixed collection of hold enablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.hold.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a HoldEnabler lookup service backed by a
 *  fixed collection of hold enablers. The hold enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>HoldEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapHoldEnablerLookupSession
    extends net.okapia.osid.jamocha.hold.rules.spi.AbstractHoldEnablerLookupSession
    implements org.osid.hold.rules.HoldEnablerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.hold.rules.HoldEnabler> holdEnablers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.hold.rules.HoldEnabler>());


    /**
     *  Makes a <code>HoldEnabler</code> available in this session.
     *
     *  @param  holdEnabler a hold enabler
     *  @throws org.osid.NullArgumentException <code>holdEnabler<code>
     *          is <code>null</code>
     */

    protected void putHoldEnabler(org.osid.hold.rules.HoldEnabler holdEnabler) {
        this.holdEnablers.put(holdEnabler.getId(), holdEnabler);
        return;
    }


    /**
     *  Makes an array of hold enablers available in this session.
     *
     *  @param  holdEnablers an array of hold enablers
     *  @throws org.osid.NullArgumentException <code>holdEnablers<code>
     *          is <code>null</code>
     */

    protected void putHoldEnablers(org.osid.hold.rules.HoldEnabler[] holdEnablers) {
        putHoldEnablers(java.util.Arrays.asList(holdEnablers));
        return;
    }


    /**
     *  Makes a collection of hold enablers available in this session.
     *
     *  @param  holdEnablers a collection of hold enablers
     *  @throws org.osid.NullArgumentException <code>holdEnablers<code>
     *          is <code>null</code>
     */

    protected void putHoldEnablers(java.util.Collection<? extends org.osid.hold.rules.HoldEnabler> holdEnablers) {
        for (org.osid.hold.rules.HoldEnabler holdEnabler : holdEnablers) {
            this.holdEnablers.put(holdEnabler.getId(), holdEnabler);
        }

        return;
    }


    /**
     *  Removes a HoldEnabler from this session.
     *
     *  @param  holdEnablerId the <code>Id</code> of the hold enabler
     *  @throws org.osid.NullArgumentException <code>holdEnablerId<code> is
     *          <code>null</code>
     */

    protected void removeHoldEnabler(org.osid.id.Id holdEnablerId) {
        this.holdEnablers.remove(holdEnablerId);
        return;
    }


    /**
     *  Gets the <code>HoldEnabler</code> specified by its <code>Id</code>.
     *
     *  @param  holdEnablerId <code>Id</code> of the <code>HoldEnabler</code>
     *  @return the holdEnabler
     *  @throws org.osid.NotFoundException <code>holdEnablerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>holdEnablerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnabler getHoldEnabler(org.osid.id.Id holdEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.hold.rules.HoldEnabler holdEnabler = this.holdEnablers.get(holdEnablerId);
        if (holdEnabler == null) {
            throw new org.osid.NotFoundException("holdEnabler not found: " + holdEnablerId);
        }

        return (holdEnabler);
    }


    /**
     *  Gets all <code>HoldEnablers</code>. In plenary mode, the returned
     *  list contains all known holdEnablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  holdEnablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>HoldEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerList getHoldEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.hold.rules.holdenabler.ArrayHoldEnablerList(this.holdEnablers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.holdEnablers.clear();
        super.close();
        return;
    }
}
