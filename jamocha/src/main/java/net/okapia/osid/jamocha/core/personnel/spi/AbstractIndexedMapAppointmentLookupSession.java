//
// AbstractIndexedMapAppointmentLookupSession.java
//
//    A simple framework for providing an Appointment lookup service
//    backed by a fixed collection of appointments with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.personnel.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an Appointment lookup service backed by a
 *  fixed collection of appointments. The appointments are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some appointments may be compatible
 *  with more types than are indicated through these appointment
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Appointments</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapAppointmentLookupSession
    extends AbstractMapAppointmentLookupSession
    implements org.osid.personnel.AppointmentLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.personnel.Appointment> appointmentsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.personnel.Appointment>());
    private final MultiMap<org.osid.type.Type, org.osid.personnel.Appointment> appointmentsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.personnel.Appointment>());


    /**
     *  Makes an <code>Appointment</code> available in this session.
     *
     *  @param  appointment an appointment
     *  @throws org.osid.NullArgumentException <code>appointment<code> is
     *          <code>null</code>
     */

    @Override
    protected void putAppointment(org.osid.personnel.Appointment appointment) {
        super.putAppointment(appointment);

        this.appointmentsByGenus.put(appointment.getGenusType(), appointment);
        
        try (org.osid.type.TypeList types = appointment.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.appointmentsByRecord.put(types.getNextType(), appointment);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an appointment from this session.
     *
     *  @param appointmentId the <code>Id</code> of the appointment
     *  @throws org.osid.NullArgumentException <code>appointmentId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeAppointment(org.osid.id.Id appointmentId) {
        org.osid.personnel.Appointment appointment;
        try {
            appointment = getAppointment(appointmentId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.appointmentsByGenus.remove(appointment.getGenusType());

        try (org.osid.type.TypeList types = appointment.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.appointmentsByRecord.remove(types.getNextType(), appointment);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeAppointment(appointmentId);
        return;
    }


    /**
     *  Gets an <code>AppointmentList</code> corresponding to the given
     *  appointment genus <code>Type</code> which does not include
     *  appointments of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known appointments or an error results. Otherwise,
     *  the returned list may contain only those appointments that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  appointmentGenusType an appointment genus type 
     *  @return the returned <code>Appointment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>appointmentGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentList getAppointmentsByGenusType(org.osid.type.Type appointmentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.personnel.appointment.ArrayAppointmentList(this.appointmentsByGenus.get(appointmentGenusType)));
    }


    /**
     *  Gets an <code>AppointmentList</code> containing the given
     *  appointment record <code>Type</code>. In plenary mode, the
     *  returned list contains all known appointments or an error
     *  results. Otherwise, the returned list may contain only those
     *  appointments that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  appointmentRecordType an appointment record type 
     *  @return the returned <code>appointment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>appointmentRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentList getAppointmentsByRecordType(org.osid.type.Type appointmentRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.personnel.appointment.ArrayAppointmentList(this.appointmentsByRecord.get(appointmentRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.appointmentsByGenus.clear();
        this.appointmentsByRecord.clear();

        super.close();

        return;
    }
}
