//
// AbstractEffortSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.effort.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractEffortSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.resourcing.EffortSearchResults {

    private org.osid.resourcing.EffortList efforts;
    private final org.osid.resourcing.EffortQueryInspector inspector;
    private final java.util.Collection<org.osid.resourcing.records.EffortSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractEffortSearchResults.
     *
     *  @param efforts the result set
     *  @param effortQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>efforts</code>
     *          or <code>effortQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractEffortSearchResults(org.osid.resourcing.EffortList efforts,
                                            org.osid.resourcing.EffortQueryInspector effortQueryInspector) {
        nullarg(efforts, "efforts");
        nullarg(effortQueryInspector, "effort query inspectpr");

        this.efforts = efforts;
        this.inspector = effortQueryInspector;

        return;
    }


    /**
     *  Gets the effort list resulting from a search.
     *
     *  @return an effort list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.resourcing.EffortList getEfforts() {
        if (this.efforts == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.resourcing.EffortList efforts = this.efforts;
        this.efforts = null;
	return (efforts);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.resourcing.EffortQueryInspector getEffortQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  effort search record <code> Type. </code> This method must
     *  be used to retrieve an effort implementing the requested
     *  record.
     *
     *  @param effortSearchRecordType an effort search 
     *         record type 
     *  @return the effort search
     *  @throws org.osid.NullArgumentException
     *          <code>effortSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(effortSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.records.EffortSearchResultsRecord getEffortSearchResultsRecord(org.osid.type.Type effortSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.resourcing.records.EffortSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(effortSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(effortSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record effort search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addEffortRecord(org.osid.resourcing.records.EffortSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "effort record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
