//
// AbstractRecipeQuery.java
//
//     A template for making a Recipe Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recipe.recipe.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for recipes.
 */

public abstract class AbstractRecipeQuery    
    extends net.okapia.osid.jamocha.spi.AbstractSourceableOsidObjectQuery
    implements org.osid.recipe.RecipeQuery {

    private final java.util.Collection<org.osid.recipe.records.RecipeQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches recipies with an estimated duration between the given range 
     *  inclusive. 
     *
     *  @param  start starting duration 
     *  @param  end ending duration 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchTotalEstimatedDuration(org.osid.calendaring.Duration start, 
                                            org.osid.calendaring.Duration end, 
                                            boolean match) {
        return;
    }


    /**
     *  Matches recipes with any estimated duration. 
     *
     *  @param  match <code> true </code> to match directions with any 
     *          estimated duration, <code> false </code> to match directions 
     *          with no estimated duration 
     */

    @OSID @Override
    public void matchAnyTotalEstimatedDuration(boolean match) {
        return;
    }


    /**
     *  Clears the duration query terms. 
     */

    @OSID @Override
    public void clearTotalEstimatedDurationTerms() {
        return;
    }


    /**
     *  Sets the asset <code> Id </code> for this query. 
     *
     *  @param  assetId the asset <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assetId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAssetId(org.osid.id.Id assetId, boolean match) {
        return;
    }


    /**
     *  Clears the asset <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAssetIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AssetQuery </code> is available. 
     *
     *  @return <code> true </code> if an asset query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssetQuery() {
        return (false);
    }


    /**
     *  Gets the query for an asset. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the asset query 
     *  @throws org.osid.UnimplementedException <code> supportsAssetQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetQuery getAssetQuery() {
        throw new org.osid.UnimplementedException("supportsAssetQuery() is false");
    }


    /**
     *  Matches directions with any asset. 
     *
     *  @param  match <code> true </code> to match directions with any asset, 
     *          <code> false </code> to match directions with no assets 
     */

    @OSID @Override
    public void matchAnyAsset(boolean match) {
        return;
    }


    /**
     *  Clears the asset query terms. 
     */

    @OSID @Override
    public void clearAssetTerms() {
        return;
    }


    /**
     *  Sets the direction <code> Id </code> for this query. 
     *
     *  @param  qualifierId the direction <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> directionId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDirectionId(org.osid.id.Id qualifierId, boolean match) {
        return;
    }


    /**
     *  Clears the direction <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDirectionIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> DirectionQuery </code> is available. 
     *
     *  @return <code> true </code> if a direction query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDirectionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a direction. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the direction query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.DirectionQuery getDirectionQuery() {
        throw new org.osid.UnimplementedException("supportsDirectionQuery() is false");
    }


    /**
     *  Matches recipes that have any direction. 
     *
     *  @param  match <code> true </code> to match recipes with any direction, 
     *          <code> false </code> to match recipes with no direction 
     */

    @OSID @Override
    public void matchAnyDirection(boolean match) {
        return;
    }


    /**
     *  Clears the direction query terms. 
     */

    @OSID @Override
    public void clearDirectionTerms() {
        return;
    }


    /**
     *  Sets the cook book <code> Id </code> for this query to match recipes 
     *  assigned to foundries. 
     *
     *  @param  cookbookId the cook book <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCookbookId(org.osid.id.Id cookbookId, boolean match) {
        return;
    }


    /**
     *  Matches recipies with an estimated duration between the given range 
     *  inclusive. 
     *
     *  @param  start starting duration 
     *  @param  end ending duration 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchTotalDuration(org.osid.calendaring.Duration start, 
                                   org.osid.calendaring.Duration end, 
                                   boolean match) {
        return;
    }


    /**
     *  Clears the duration query terms. 
     */

    @OSID @Override
    public void clearTotalDurationTerms() {
        return;
    }


    /**
     *  Clears the cook book <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCookbookIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CookbookQuery </code> is available. 
     *
     *  @return <code> true </code> if a cook book query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCookbookQuery() {
        return (false);
    }


    /**
     *  Gets the query for a cookbook. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the cook book query 
     *  @throws org.osid.UnimplementedException <code> supportsCookbookQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.CookbookQuery getCookbookQuery() {
        throw new org.osid.UnimplementedException("supportsCookbookQuery() is false");
    }


    /**
     *  Clears the cook book query terms. 
     */

    @OSID @Override
    public void clearCookbookTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given recipe query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a recipe implementing the requested record.
     *
     *  @param recipeRecordType a recipe record type
     *  @return the recipe query record
     *  @throws org.osid.NullArgumentException
     *          <code>recipeRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(recipeRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.recipe.records.RecipeQueryRecord getRecipeQueryRecord(org.osid.type.Type recipeRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.recipe.records.RecipeQueryRecord record : this.records) {
            if (record.implementsRecordType(recipeRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(recipeRecordType + " is not supported");
    }


    /**
     *  Adds a record to this recipe query. 
     *
     *  @param recipeQueryRecord recipe query record
     *  @param recipeRecordType recipe record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRecipeQueryRecord(org.osid.recipe.records.RecipeQueryRecord recipeQueryRecord, 
                                          org.osid.type.Type recipeRecordType) {

        addRecordType(recipeRecordType);
        nullarg(recipeQueryRecord, "recipe query record");
        this.records.add(recipeQueryRecord);        
        return;
    }
}
