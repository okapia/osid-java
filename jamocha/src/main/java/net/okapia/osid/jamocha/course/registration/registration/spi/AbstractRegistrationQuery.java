//
// AbstractRegistrationQuery.java
//
//     A template for making a Registration Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.registration.registration.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for registrations.
 */

public abstract class AbstractRegistrationQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQuery
    implements org.osid.course.registration.RegistrationQuery {

    private final java.util.Collection<org.osid.course.registration.records.RegistrationQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the activity bundle <code> Id </code> for this query to match 
     *  registrations that have a related course. 
     *
     *  @param  activityBundleId an activity bundle <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> activityBundleId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchActivityBundleId(org.osid.id.Id activityBundleId, 
                                      boolean match) {
        return;
    }


    /**
     *  Clears the activity bundle <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearActivityBundleIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> ActivityBundleQuery </code> is available. 
     *
     *  @return <code> true </code> if an activity bundle query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityBundleQuery() {
        return (false);
    }


    /**
     *  Gets the query for an activity bundle. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the activity bundle query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBundleQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleQuery getActivityBundleQuery() {
        throw new org.osid.UnimplementedException("supportsActivityBundleQuery() is false");
    }


    /**
     *  Clears the activity bundle terms. 
     */

    @OSID @Override
    public void clearActivityBundleTerms() {
        return;
    }


    /**
     *  Sets the student resource <code> Id </code> for this query. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchStudentId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the student resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearStudentIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStudentQuery() {
        return (false);
    }


    /**
     *  Gets the query for a student. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsStudentQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getStudentQuery() {
        throw new org.osid.UnimplementedException("supportsStudentQuery() is false");
    }


    /**
     *  Clears the student resource terms. 
     */

    @OSID @Override
    public void clearStudentTerms() {
        return;
    }


    /**
     *  Matches registrations with credits between the given numbers 
     *  inclusive. 
     *
     *  @param  min low number 
     *  @param  max high number 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> max </code> is less 
     *          than <code> min </code> 
     */

    @OSID @Override
    public void matchCredits(java.math.BigDecimal min, 
                             java.math.BigDecimal max, boolean match) {
        return;
    }


    /**
     *  Matches a registration that has any credits assigned. 
     *
     *  @param  match <code> true </code> to match registrations with any 
     *          credits, <code> false </code> to match registrations with no 
     *          credits 
     */

    @OSID @Override
    public void matchAnyCredits(boolean match) {
        return;
    }


    /**
     *  Clears the credit terms. 
     */

    @OSID @Override
    public void clearCreditsTerms() {
        return;
    }


    /**
     *  Sets the grade system <code> Id </code> for this query. 
     *
     *  @param  gradeSystemId a grade system <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeSystemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGradingOptionId(org.osid.id.Id gradeSystemId, 
                                     boolean match) {
        return;
    }


    /**
     *  Clears the grade system <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGradingOptionIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> GradeSystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade system query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradingOptionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grading option. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return a grade system query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradingOptionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQuery getGradingOptionQuery() {
        throw new org.osid.UnimplementedException("supportsGradingOptionQuery() is false");
    }


    /**
     *  Matches registrations that have any grading option. 
     *
     *  @param  match <code> true </code> to match registrations with any 
     *          grading option, <code> false </code> to match registrations 
     *          with no grading options 
     */

    @OSID @Override
    public void matchAnyGradingOption(boolean match) {
        return;
    }


    /**
     *  Clears the grading option terms. 
     */

    @OSID @Override
    public void clearGradingOptionTerms() {
        return;
    }


    /**
     *  Sets the course catalog <code> Id </code> for this query to match 
     *  registrations assigned to course catalogs. 
     *
     *  @param  courseCatalogId the course catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseCatalogId(org.osid.id.Id courseCatalogId, 
                                     boolean match) {
        return;
    }


    /**
     *  Clears the course catalog <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseCatalogIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CourseCatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a course catalog query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course catalog. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the course catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQuery getCourseCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsCourseCatalogQuery() is false");
    }


    /**
     *  Clears the course catalog terms. 
     */

    @OSID @Override
    public void clearCourseCatalogTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given registration query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a registration implementing the requested record.
     *
     *  @param registrationRecordType a registration record type
     *  @return the registration query record
     *  @throws org.osid.NullArgumentException
     *          <code>registrationRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(registrationRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.registration.records.RegistrationQueryRecord getRegistrationQueryRecord(org.osid.type.Type registrationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.registration.records.RegistrationQueryRecord record : this.records) {
            if (record.implementsRecordType(registrationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(registrationRecordType + " is not supported");
    }


    /**
     *  Adds a record to this registration query. 
     *
     *  @param registrationQueryRecord registration query record
     *  @param registrationRecordType registration record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRegistrationQueryRecord(org.osid.course.registration.records.RegistrationQueryRecord registrationQueryRecord, 
                                          org.osid.type.Type registrationRecordType) {

        addRecordType(registrationRecordType);
        nullarg(registrationQueryRecord, "registration query record");
        this.records.add(registrationQueryRecord);        
        return;
    }
}
