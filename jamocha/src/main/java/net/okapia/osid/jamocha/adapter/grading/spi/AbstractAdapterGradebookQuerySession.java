//
// AbstractQueryGradebookLookupSession.java
//
//    A GradebookQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.grading.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A GradebookQuerySession adapter.
 */

public abstract class AbstractAdapterGradebookQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.grading.GradebookQuerySession {

    private final org.osid.grading.GradebookQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterGradebookQuerySession.
     *
     *  @param session the underlying gradebook query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterGradebookQuerySession(org.osid.grading.GradebookQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@codeGradebook</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchGradebooks() {
        return (this.session.canSearchGradebooks());
    }

      
    /**
     *  Gets a gradebook query. The returned query will not have an
     *  extension query.
     *
     *  @return the gradebook query 
     */
      
    @OSID @Override
    public org.osid.grading.GradebookQuery getGradebookQuery() {
        return (this.session.getGradebookQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  gradebookQuery the gradebook query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code gradebookQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code gradebookQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.grading.GradebookList getGradebooksByQuery(org.osid.grading.GradebookQuery gradebookQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getGradebooksByQuery(gradebookQuery));
    }
}
