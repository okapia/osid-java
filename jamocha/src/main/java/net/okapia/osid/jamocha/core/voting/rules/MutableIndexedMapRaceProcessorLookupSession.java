//
// MutableIndexedMapRaceProcessorLookupSession
//
//    Implements a RaceProcessor lookup service backed by a collection of
//    raceProcessors indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.voting.rules;


/**
 *  Implements a RaceProcessor lookup service backed by a collection of
 *  race processors. The race processors are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some race processors may be compatible
 *  with more types than are indicated through these race processor
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of race processors can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapRaceProcessorLookupSession
    extends net.okapia.osid.jamocha.core.voting.rules.spi.AbstractIndexedMapRaceProcessorLookupSession
    implements org.osid.voting.rules.RaceProcessorLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapRaceProcessorLookupSession} with no race processors.
     *
     *  @param polls the polls
     *  @throws org.osid.NullArgumentException {@code polls}
     *          is {@code null}
     */

      public MutableIndexedMapRaceProcessorLookupSession(org.osid.voting.Polls polls) {
        setPolls(polls);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapRaceProcessorLookupSession} with a
     *  single race processor.
     *  
     *  @param polls the polls
     *  @param  raceProcessor a single raceProcessor
     *  @throws org.osid.NullArgumentException {@code polls} or
     *          {@code raceProcessor} is {@code null}
     */

    public MutableIndexedMapRaceProcessorLookupSession(org.osid.voting.Polls polls,
                                                  org.osid.voting.rules.RaceProcessor raceProcessor) {
        this(polls);
        putRaceProcessor(raceProcessor);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapRaceProcessorLookupSession} using an
     *  array of race processors.
     *
     *  @param polls the polls
     *  @param  raceProcessors an array of race processors
     *  @throws org.osid.NullArgumentException {@code polls} or
     *          {@code raceProcessors} is {@code null}
     */

    public MutableIndexedMapRaceProcessorLookupSession(org.osid.voting.Polls polls,
                                                  org.osid.voting.rules.RaceProcessor[] raceProcessors) {
        this(polls);
        putRaceProcessors(raceProcessors);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapRaceProcessorLookupSession} using a
     *  collection of race processors.
     *
     *  @param polls the polls
     *  @param  raceProcessors a collection of race processors
     *  @throws org.osid.NullArgumentException {@code polls} or
     *          {@code raceProcessors} is {@code null}
     */

    public MutableIndexedMapRaceProcessorLookupSession(org.osid.voting.Polls polls,
                                                  java.util.Collection<? extends org.osid.voting.rules.RaceProcessor> raceProcessors) {

        this(polls);
        putRaceProcessors(raceProcessors);
        return;
    }
    

    /**
     *  Makes a {@code RaceProcessor} available in this session.
     *
     *  @param  raceProcessor a race processor
     *  @throws org.osid.NullArgumentException {@code raceProcessor{@code  is
     *          {@code null}
     */

    @Override
    public void putRaceProcessor(org.osid.voting.rules.RaceProcessor raceProcessor) {
        super.putRaceProcessor(raceProcessor);
        return;
    }


    /**
     *  Makes an array of race processors available in this session.
     *
     *  @param  raceProcessors an array of race processors
     *  @throws org.osid.NullArgumentException {@code raceProcessors{@code 
     *          is {@code null}
     */

    @Override
    public void putRaceProcessors(org.osid.voting.rules.RaceProcessor[] raceProcessors) {
        super.putRaceProcessors(raceProcessors);
        return;
    }


    /**
     *  Makes collection of race processors available in this session.
     *
     *  @param  raceProcessors a collection of race processors
     *  @throws org.osid.NullArgumentException {@code raceProcessor{@code  is
     *          {@code null}
     */

    @Override
    public void putRaceProcessors(java.util.Collection<? extends org.osid.voting.rules.RaceProcessor> raceProcessors) {
        super.putRaceProcessors(raceProcessors);
        return;
    }


    /**
     *  Removes a RaceProcessor from this session.
     *
     *  @param raceProcessorId the {@code Id} of the race processor
     *  @throws org.osid.NullArgumentException {@code raceProcessorId{@code  is
     *          {@code null}
     */

    @Override
    public void removeRaceProcessor(org.osid.id.Id raceProcessorId) {
        super.removeRaceProcessor(raceProcessorId);
        return;
    }    
}
