//
// BusinessElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.billing.business.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class BusinessElements
    extends net.okapia.osid.jamocha.spi.OsidCatalogElements {


    /**
     *  Gets the BusinessElement Id.
     *
     *  @return the business element Id
     */

    public static org.osid.id.Id getBusinessEntityId() {
        return (makeEntityId("osid.billing.Business"));
    }


    /**
     *  Gets the CustomerId element Id.
     *
     *  @return the CustomerId element Id
     */

    public static org.osid.id.Id getCustomerId() {
        return (makeQueryElementId("osid.billing.business.CustomerId"));
    }


    /**
     *  Gets the Customer element Id.
     *
     *  @return the Customer element Id
     */

    public static org.osid.id.Id getCustomer() {
        return (makeQueryElementId("osid.billing.business.Customer"));
    }


    /**
     *  Gets the ItemId element Id.
     *
     *  @return the ItemId element Id
     */

    public static org.osid.id.Id getItemId() {
        return (makeQueryElementId("osid.billing.business.ItemId"));
    }


    /**
     *  Gets the Item element Id.
     *
     *  @return the Item element Id
     */

    public static org.osid.id.Id getItem() {
        return (makeQueryElementId("osid.billing.business.Item"));
    }


    /**
     *  Gets the CategoryId element Id.
     *
     *  @return the CategoryId element Id
     */

    public static org.osid.id.Id getCategoryId() {
        return (makeQueryElementId("osid.billing.business.CategoryId"));
    }


    /**
     *  Gets the Category element Id.
     *
     *  @return the Category element Id
     */

    public static org.osid.id.Id getCategory() {
        return (makeQueryElementId("osid.billing.business.Category"));
    }


    /**
     *  Gets the EntryId element Id.
     *
     *  @return the EntryId element Id
     */

    public static org.osid.id.Id getEntryId() {
        return (makeQueryElementId("osid.billing.business.EntryId"));
    }


    /**
     *  Gets the Entry element Id.
     *
     *  @return the Entry element Id
     */

    public static org.osid.id.Id getEntry() {
        return (makeQueryElementId("osid.billing.business.Entry"));
    }


    /**
     *  Gets the PeriodId element Id.
     *
     *  @return the PeriodId element Id
     */

    public static org.osid.id.Id getPeriodId() {
        return (makeQueryElementId("osid.billing.business.PeriodId"));
    }


    /**
     *  Gets the Period element Id.
     *
     *  @return the Period element Id
     */

    public static org.osid.id.Id getPeriod() {
        return (makeQueryElementId("osid.billing.business.Period"));
    }


    /**
     *  Gets the AncestorBusinessId element Id.
     *
     *  @return the AncestorBusinessId element Id
     */

    public static org.osid.id.Id getAncestorBusinessId() {
        return (makeQueryElementId("osid.billing.business.AncestorBusinessId"));
    }


    /**
     *  Gets the AncestorBusiness element Id.
     *
     *  @return the AncestorBusiness element Id
     */

    public static org.osid.id.Id getAncestorBusiness() {
        return (makeQueryElementId("osid.billing.business.AncestorBusiness"));
    }


    /**
     *  Gets the DescendantBusinessId element Id.
     *
     *  @return the DescendantBusinessId element Id
     */

    public static org.osid.id.Id getDescendantBusinessId() {
        return (makeQueryElementId("osid.billing.business.DescendantBusinessId"));
    }


    /**
     *  Gets the DescendantBusiness element Id.
     *
     *  @return the DescendantBusiness element Id
     */

    public static org.osid.id.Id getDescendantBusiness() {
        return (makeQueryElementId("osid.billing.business.DescendantBusiness"));
    }
}
