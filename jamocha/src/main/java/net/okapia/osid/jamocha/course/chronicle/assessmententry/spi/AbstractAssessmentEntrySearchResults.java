//
// AbstractAssessmentEntrySearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.chronicle.assessmententry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractAssessmentEntrySearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.course.chronicle.AssessmentEntrySearchResults {

    private org.osid.course.chronicle.AssessmentEntryList assessmentEntries;
    private final org.osid.course.chronicle.AssessmentEntryQueryInspector inspector;
    private final java.util.Collection<org.osid.course.chronicle.records.AssessmentEntrySearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractAssessmentEntrySearchResults.
     *
     *  @param assessmentEntries the result set
     *  @param assessmentEntryQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>assessmentEntries</code>
     *          or <code>assessmentEntryQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractAssessmentEntrySearchResults(org.osid.course.chronicle.AssessmentEntryList assessmentEntries,
                                            org.osid.course.chronicle.AssessmentEntryQueryInspector assessmentEntryQueryInspector) {
        nullarg(assessmentEntries, "assessment entries");
        nullarg(assessmentEntryQueryInspector, "assessment entry query inspectpr");

        this.assessmentEntries = assessmentEntries;
        this.inspector = assessmentEntryQueryInspector;

        return;
    }


    /**
     *  Gets the assessment entry list resulting from a search.
     *
     *  @return an assessment entry list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryList getAssessmentEntries() {
        if (this.assessmentEntries == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.course.chronicle.AssessmentEntryList assessmentEntries = this.assessmentEntries;
        this.assessmentEntries = null;
	return (assessmentEntries);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.course.chronicle.AssessmentEntryQueryInspector getAssessmentEntryQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  assessment entry search record <code> Type. </code> This method must
     *  be used to retrieve an assessmentEntry implementing the requested
     *  record.
     *
     *  @param assessmentEntrySearchRecordType an assessmentEntry search 
     *         record type 
     *  @return the assessment entry search
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentEntrySearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(assessmentEntrySearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.chronicle.records.AssessmentEntrySearchResultsRecord getAssessmentEntrySearchResultsRecord(org.osid.type.Type assessmentEntrySearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.course.chronicle.records.AssessmentEntrySearchResultsRecord record : this.records) {
            if (record.implementsRecordType(assessmentEntrySearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(assessmentEntrySearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record assessment entry search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addAssessmentEntryRecord(org.osid.course.chronicle.records.AssessmentEntrySearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "assessment entry record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
