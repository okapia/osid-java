//
// AbstractCatalogEnablerQuerySession.java
//
//     A template for making CatalogEnablerQuerySessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.cataloging.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A basic template for query sessions.
 */

public abstract class AbstractCatalogEnablerQuerySession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.cataloging.rules.CatalogEnablerQuerySession {

    private boolean federated = false;
    private org.osid.cataloging.Catalog catalog = new net.okapia.osid.jamocha.nil.cataloging.catalog.UnknownCatalog();


      /**
       *  Gets the <code>Catalog/code> <code>Id</code> associated
       *  with this session.
       *
       *  @return the <code>Catalog Id</code> associated with
       *          this session
       *  @throws org.osid.IllegalStateException this session has been
       *          closed
       */

    @OSID @Override
      public org.osid.id.Id getCatalogId() {
        return (this.catalog.getId());
    }


    /**
     *  Gets the <code>Catalog</code> associated with this 
     *  session.
     *
     *  @return the <code>Catalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.cataloging.Catalog getCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.catalog);
    }


    /**
     *  Sets the <code>Catalog</code>.
     *
     *  @param  catalog the catalog for this session
     *  @throws org.osid.NullArgumentException <code>catalog</code>
     *          is <code>null</code>
     */

    protected void setCatalog(org.osid.cataloging.Catalog catalog) {
        nullarg(catalog, "catalog");
        this.catalog = catalog;
        return;
    }

    /**
     *  Federates the view for methods in this session. A federated
     *  view will include catalog enablers in catalogs which are children
     *  of this catalog in the catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCatalogView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this catalog only.
     */

    @OSID @Override
    public void useIsolatedCatalogView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Tests if this user can perform <code> CatalogEnabler </code>
     *  searches. A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a <code>
     *  PERMISSION_DENIED. </code> This is intended as a hint to an
     *  application that may opt not to offer search operations to
     *  unauthorized users.
     *
     *  @return <code> false </code> if search methods are not authorized, 
     *          <code> true </code> otherwise 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canSearchCatalogEnablers() {
        return (true);
    }
}
