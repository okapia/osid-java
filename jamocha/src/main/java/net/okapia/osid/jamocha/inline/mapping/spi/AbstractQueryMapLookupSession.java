//
// AbstractQueryMapLookupSession.java
//
//    An inline adapter that maps a MapLookupSession to
//    a MapQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.mapping.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a MapLookupSession to
 *  a MapQuerySession.
 */

public abstract class AbstractQueryMapLookupSession
    extends net.okapia.osid.jamocha.mapping.spi.AbstractMapLookupSession
    implements org.osid.mapping.MapLookupSession {

    private final org.osid.mapping.MapQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryMapLookupSession.
     *
     *  @param querySession the underlying map query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryMapLookupSession(org.osid.mapping.MapQuerySession querySession) {
        nullarg(querySession, "map query session");
        this.session = querySession;
        return;
    }



    /**
     *  Tests if this user can perform <code>Map</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupMaps() {
        return (this.session.canSearchMaps());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }

     
    /**
     *  Gets the <code>Map</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Map</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Map</code> and
     *  retained for compatibility.
     *
     *  @param  mapId <code>Id</code> of the
     *          <code>Map</code>
     *  @return the map
     *  @throws org.osid.NotFoundException <code>mapId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>mapId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.Map getMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.mapping.MapQuery query = getQuery();
        query.matchId(mapId, true);
        org.osid.mapping.MapList maps = this.session.getMapsByQuery(query);
        if (maps.hasNext()) {
            return (maps.getNextMap());
        } 
        
        throw new org.osid.NotFoundException(mapId + " not found");
    }


    /**
     *  Gets a <code>MapList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  maps specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Maps</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  mapIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Map</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>mapIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.MapList getMapsByIds(org.osid.id.IdList mapIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.mapping.MapQuery query = getQuery();

        try (org.osid.id.IdList ids = mapIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getMapsByQuery(query));
    }


    /**
     *  Gets a <code>MapList</code> corresponding to the given
     *  map genus <code>Type</code> which does not include
     *  maps of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  maps or an error results. Otherwise, the returned list
     *  may contain only those maps that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  mapGenusType a map genus type 
     *  @return the returned <code>Map</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>mapGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.MapList getMapsByGenusType(org.osid.type.Type mapGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.mapping.MapQuery query = getQuery();
        query.matchGenusType(mapGenusType, true);
        return (this.session.getMapsByQuery(query));
    }


    /**
     *  Gets a <code>MapList</code> corresponding to the given
     *  map genus <code>Type</code> and include any additional
     *  maps with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  maps or an error results. Otherwise, the returned list
     *  may contain only those maps that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  mapGenusType a map genus type 
     *  @return the returned <code>Map</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>mapGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.MapList getMapsByParentGenusType(org.osid.type.Type mapGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.mapping.MapQuery query = getQuery();
        query.matchParentGenusType(mapGenusType, true);
        return (this.session.getMapsByQuery(query));
    }


    /**
     *  Gets a <code>MapList</code> containing the given
     *  map record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  maps or an error results. Otherwise, the returned list
     *  may contain only those maps that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  mapRecordType a map record type 
     *  @return the returned <code>Map</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>mapRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.MapList getMapsByRecordType(org.osid.type.Type mapRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.mapping.MapQuery query = getQuery();
        query.matchRecordType(mapRecordType, true);
        return (this.session.getMapsByQuery(query));
    }


    /**
     *  Gets a <code>MapList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known maps or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  maps that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Map</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.mapping.MapList getMapsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.mapping.MapQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getMapsByQuery(query));        
    }

    
    /**
     *  Gets all <code>Maps</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  maps or an error results. Otherwise, the returned list
     *  may contain only those maps that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Maps</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.MapList getMaps()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.mapping.MapQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getMapsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.mapping.MapQuery getQuery() {
        org.osid.mapping.MapQuery query = this.session.getMapQuery();
        return (query);
    }
}
