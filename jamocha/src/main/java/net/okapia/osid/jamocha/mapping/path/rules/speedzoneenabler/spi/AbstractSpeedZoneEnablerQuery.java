//
// AbstractSpeedZoneEnablerQuery.java
//
//     A template for making a SpeedZoneEnabler Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.path.rules.speedzoneenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for speed zone enablers.
 */

public abstract class AbstractSpeedZoneEnablerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQuery
    implements org.osid.mapping.path.rules.SpeedZoneEnablerQuery {

    private final java.util.Collection<org.osid.mapping.path.rules.records.SpeedZoneEnablerQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches enablers mapped to the speed zone. 
     *
     *  @param  speedZoneId the speed zone <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> speedZoneId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledSpeedZoneId(org.osid.id.Id speedZoneId, 
                                      boolean match) {
        return;
    }


    /**
     *  Clears the speed zone <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledSpeedZoneIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> SpeedZoneQuery </code> is available. 
     *
     *  @return <code> true </code> if a speed zone query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledSpeedZoneQuery() {
        return (false);
    }


    /**
     *  Gets the query for a speed zone. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the speed zone query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledSpeedZoneQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneQuery getRuledSpeedZoneQuery() {
        throw new org.osid.UnimplementedException("supportsRuledSpeedZoneQuery() is false");
    }


    /**
     *  Matches enablers mapped to any speed zone. 
     *
     *  @param  match <code> true </code> for enablers mapped to any speed 
     *          zone, <code> false </code> to match enablers mapped to no 
     *          speed zones 
     */

    @OSID @Override
    public void matchAnyRuledSpeedZone(boolean match) {
        return;
    }


    /**
     *  Clears the speed zone query terms. 
     */

    @OSID @Override
    public void clearRuledSpeedZoneTerms() {
        return;
    }


    /**
     *  Matches enablers mapped to the map. 
     *
     *  @param  mapId the map <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchMapId(org.osid.id.Id mapId, boolean match) {
        return;
    }


    /**
     *  Clears the map <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearMapIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> MapQuery </code> is available. 
     *
     *  @return <code> true </code> if an map query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMapQuery() {
        return (false);
    }


    /**
     *  Gets the query for an map. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the map query 
     *  @throws org.osid.UnimplementedException <code> supportsMapQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.MapQuery getMapQuery() {
        throw new org.osid.UnimplementedException("supportsMapQuery() is false");
    }


    /**
     *  Clears the map query terms. 
     */

    @OSID @Override
    public void clearMapTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given speed zone enabler query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a speed zone enabler implementing the requested record.
     *
     *  @param speedZoneEnablerRecordType a speed zone enabler record type
     *  @return the speed zone enabler query record
     *  @throws org.osid.NullArgumentException
     *          <code>speedZoneEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(speedZoneEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.path.rules.records.SpeedZoneEnablerQueryRecord getSpeedZoneEnablerQueryRecord(org.osid.type.Type speedZoneEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.path.rules.records.SpeedZoneEnablerQueryRecord record : this.records) {
            if (record.implementsRecordType(speedZoneEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(speedZoneEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this speed zone enabler query. 
     *
     *  @param speedZoneEnablerQueryRecord speed zone enabler query record
     *  @param speedZoneEnablerRecordType speedZoneEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addSpeedZoneEnablerQueryRecord(org.osid.mapping.path.rules.records.SpeedZoneEnablerQueryRecord speedZoneEnablerQueryRecord, 
                                          org.osid.type.Type speedZoneEnablerRecordType) {

        addRecordType(speedZoneEnablerRecordType);
        nullarg(speedZoneEnablerQueryRecord, "speed zone enabler query record");
        this.records.add(speedZoneEnablerQueryRecord);        
        return;
    }
}
