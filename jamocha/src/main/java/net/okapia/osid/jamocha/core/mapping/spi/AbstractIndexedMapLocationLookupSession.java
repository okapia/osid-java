//
// AbstractIndexedMapLocationLookupSession.java
//
//    A simple framework for providing a Location lookup service
//    backed by a fixed collection of locations with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Location lookup service backed by a
 *  fixed collection of locations. The locations are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some locations may be compatible
 *  with more types than are indicated through these location
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Locations</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapLocationLookupSession
    extends AbstractMapLocationLookupSession
    implements org.osid.mapping.LocationLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.mapping.Location> locationsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.mapping.Location>());
    private final MultiMap<org.osid.type.Type, org.osid.mapping.Location> locationsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.mapping.Location>());


    /**
     *  Makes a <code>Location</code> available in this session.
     *
     *  @param  location a location
     *  @throws org.osid.NullArgumentException <code>location<code> is
     *          <code>null</code>
     */

    @Override
    protected void putLocation(org.osid.mapping.Location location) {
        super.putLocation(location);

        this.locationsByGenus.put(location.getGenusType(), location);
        
        try (org.osid.type.TypeList types = location.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.locationsByRecord.put(types.getNextType(), location);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a location from this session.
     *
     *  @param locationId the <code>Id</code> of the location
     *  @throws org.osid.NullArgumentException <code>locationId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeLocation(org.osid.id.Id locationId) {
        org.osid.mapping.Location location;
        try {
            location = getLocation(locationId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.locationsByGenus.remove(location.getGenusType());

        try (org.osid.type.TypeList types = location.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.locationsByRecord.remove(types.getNextType(), location);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeLocation(locationId);
        return;
    }


    /**
     *  Gets a <code>LocationList</code> corresponding to the given
     *  location genus <code>Type</code> which does not include
     *  locations of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known locations or an error results. Otherwise,
     *  the returned list may contain only those locations that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  locationGenusType a location genus type 
     *  @return the returned <code>Location</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>locationGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.LocationList getLocationsByGenusType(org.osid.type.Type locationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.mapping.location.ArrayLocationList(this.locationsByGenus.get(locationGenusType)));
    }


    /**
     *  Gets a <code>LocationList</code> containing the given
     *  location record <code>Type</code>. In plenary mode, the
     *  returned list contains all known locations or an error
     *  results. Otherwise, the returned list may contain only those
     *  locations that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  locationRecordType a location record type 
     *  @return the returned <code>location</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>locationRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.LocationList getLocationsByRecordType(org.osid.type.Type locationRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.mapping.location.ArrayLocationList(this.locationsByRecord.get(locationRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.locationsByGenus.clear();
        this.locationsByRecord.clear();

        super.close();

        return;
    }
}
