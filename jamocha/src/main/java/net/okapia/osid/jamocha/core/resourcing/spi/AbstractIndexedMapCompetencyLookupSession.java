//
// AbstractIndexedMapCompetencyLookupSession.java
//
//    A simple framework for providing a Competency lookup service
//    backed by a fixed collection of competencies with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Competency lookup service backed by a
 *  fixed collection of competencies. The competencies are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some competencies may be compatible
 *  with more types than are indicated through these competency
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Competencies</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapCompetencyLookupSession
    extends AbstractMapCompetencyLookupSession
    implements org.osid.resourcing.CompetencyLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.resourcing.Competency> competenciesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.resourcing.Competency>());
    private final MultiMap<org.osid.type.Type, org.osid.resourcing.Competency> competenciesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.resourcing.Competency>());


    /**
     *  Makes a <code>Competency</code> available in this session.
     *
     *  @param  competency a competency
     *  @throws org.osid.NullArgumentException <code>competency<code> is
     *          <code>null</code>
     */

    @Override
    protected void putCompetency(org.osid.resourcing.Competency competency) {
        super.putCompetency(competency);

        this.competenciesByGenus.put(competency.getGenusType(), competency);
        
        try (org.osid.type.TypeList types = competency.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.competenciesByRecord.put(types.getNextType(), competency);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a competency from this session.
     *
     *  @param competencyId the <code>Id</code> of the competency
     *  @throws org.osid.NullArgumentException <code>competencyId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeCompetency(org.osid.id.Id competencyId) {
        org.osid.resourcing.Competency competency;
        try {
            competency = getCompetency(competencyId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.competenciesByGenus.remove(competency.getGenusType());

        try (org.osid.type.TypeList types = competency.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.competenciesByRecord.remove(types.getNextType(), competency);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeCompetency(competencyId);
        return;
    }


    /**
     *  Gets a <code>CompetencyList</code> corresponding to the given
     *  competency genus <code>Type</code> which does not include
     *  competencies of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known competencies or an error results. Otherwise,
     *  the returned list may contain only those competencies that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  competencyGenusType a competency genus type 
     *  @return the returned <code>Competency</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>competencyGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyList getCompetenciesByGenusType(org.osid.type.Type competencyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.resourcing.competency.ArrayCompetencyList(this.competenciesByGenus.get(competencyGenusType)));
    }


    /**
     *  Gets a <code>CompetencyList</code> containing the given
     *  competency record <code>Type</code>. In plenary mode, the
     *  returned list contains all known competencies or an error
     *  results. Otherwise, the returned list may contain only those
     *  competencies that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  competencyRecordType a competency record type 
     *  @return the returned <code>competency</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>competencyRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyList getCompetenciesByRecordType(org.osid.type.Type competencyRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.resourcing.competency.ArrayCompetencyList(this.competenciesByRecord.get(competencyRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.competenciesByGenus.clear();
        this.competenciesByRecord.clear();

        super.close();

        return;
    }
}
