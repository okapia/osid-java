//
// InvariantMapProxyWorkLookupSession
//
//    Implements a Work lookup service backed by a fixed
//    collection of works. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing;


/**
 *  Implements a Work lookup service backed by a fixed
 *  collection of works. The works are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyWorkLookupSession
    extends net.okapia.osid.jamocha.core.resourcing.spi.AbstractMapWorkLookupSession
    implements org.osid.resourcing.WorkLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyWorkLookupSession} with no
     *  works.
     *
     *  @param foundry the foundry
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyWorkLookupSession(org.osid.resourcing.Foundry foundry,
                                                  org.osid.proxy.Proxy proxy) {
        setFoundry(foundry);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyWorkLookupSession} with a single
     *  work.
     *
     *  @param foundry the foundry
     *  @param work a single work
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code foundry},
     *          {@code work} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyWorkLookupSession(org.osid.resourcing.Foundry foundry,
                                                  org.osid.resourcing.Work work, org.osid.proxy.Proxy proxy) {

        this(foundry, proxy);
        putWork(work);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyWorkLookupSession} using
     *  an array of works.
     *
     *  @param foundry the foundry
     *  @param works an array of works
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code foundry},
     *          {@code works} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyWorkLookupSession(org.osid.resourcing.Foundry foundry,
                                                  org.osid.resourcing.Work[] works, org.osid.proxy.Proxy proxy) {

        this(foundry, proxy);
        putWorks(works);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyWorkLookupSession} using a
     *  collection of works.
     *
     *  @param foundry the foundry
     *  @param works a collection of works
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code foundry},
     *          {@code works} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyWorkLookupSession(org.osid.resourcing.Foundry foundry,
                                                  java.util.Collection<? extends org.osid.resourcing.Work> works,
                                                  org.osid.proxy.Proxy proxy) {

        this(foundry, proxy);
        putWorks(works);
        return;
    }
}
