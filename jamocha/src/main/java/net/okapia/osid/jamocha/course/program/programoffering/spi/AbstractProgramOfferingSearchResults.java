//
// AbstractProgramOfferingSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.program.programoffering.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractProgramOfferingSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.course.program.ProgramOfferingSearchResults {

    private org.osid.course.program.ProgramOfferingList programOfferings;
    private final org.osid.course.program.ProgramOfferingQueryInspector inspector;
    private final java.util.Collection<org.osid.course.program.records.ProgramOfferingSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractProgramOfferingSearchResults.
     *
     *  @param programOfferings the result set
     *  @param programOfferingQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>programOfferings</code>
     *          or <code>programOfferingQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractProgramOfferingSearchResults(org.osid.course.program.ProgramOfferingList programOfferings,
                                            org.osid.course.program.ProgramOfferingQueryInspector programOfferingQueryInspector) {
        nullarg(programOfferings, "program offerings");
        nullarg(programOfferingQueryInspector, "program offering query inspectpr");

        this.programOfferings = programOfferings;
        this.inspector = programOfferingQueryInspector;

        return;
    }


    /**
     *  Gets the program offering list resulting from a search.
     *
     *  @return a program offering list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingList getProgramOfferings() {
        if (this.programOfferings == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.course.program.ProgramOfferingList programOfferings = this.programOfferings;
        this.programOfferings = null;
	return (programOfferings);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.course.program.ProgramOfferingQueryInspector getProgramOfferingQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  program offering search record <code> Type. </code> This method must
     *  be used to retrieve a programOffering implementing the requested
     *  record.
     *
     *  @param programOfferingSearchRecordType a programOffering search 
     *         record type 
     *  @return the program offering search
     *  @throws org.osid.NullArgumentException
     *          <code>programOfferingSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(programOfferingSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.program.records.ProgramOfferingSearchResultsRecord getProgramOfferingSearchResultsRecord(org.osid.type.Type programOfferingSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.course.program.records.ProgramOfferingSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(programOfferingSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(programOfferingSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record program offering search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addProgramOfferingRecord(org.osid.course.program.records.ProgramOfferingSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "program offering record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
