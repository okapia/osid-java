//
// GradebookElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.gradebook.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class GradebookElements
    extends net.okapia.osid.jamocha.spi.OsidCatalogElements {


    /**
     *  Gets the GradebookElement Id.
     *
     *  @return the gradebook element Id
     */

    public static org.osid.id.Id getGradebookEntityId() {
        return (makeEntityId("osid.grading.Gradebook"));
    }


    /**
     *  Gets the GradeSystemId element Id.
     *
     *  @return the GradeSystemId element Id
     */

    public static org.osid.id.Id getGradeSystemId() {
        return (makeQueryElementId("osid.grading.gradebook.GradeSystemId"));
    }


    /**
     *  Gets the GradeSystem element Id.
     *
     *  @return the GradeSystem element Id
     */

    public static org.osid.id.Id getGradeSystem() {
        return (makeQueryElementId("osid.grading.gradebook.GradeSystem"));
    }


    /**
     *  Gets the GradeEntryId element Id.
     *
     *  @return the GradeEntryId element Id
     */

    public static org.osid.id.Id getGradeEntryId() {
        return (makeQueryElementId("osid.grading.gradebook.GradeEntryId"));
    }


    /**
     *  Gets the GradeEntry element Id.
     *
     *  @return the GradeEntry element Id
     */

    public static org.osid.id.Id getGradeEntry() {
        return (makeQueryElementId("osid.grading.gradebook.GradeEntry"));
    }


    /**
     *  Gets the GradebookColumnId element Id.
     *
     *  @return the GradebookColumnId element Id
     */

    public static org.osid.id.Id getGradebookColumnId() {
        return (makeQueryElementId("osid.grading.gradebook.GradebookColumnId"));
    }


    /**
     *  Gets the GradebookColumn element Id.
     *
     *  @return the GradebookColumn element Id
     */

    public static org.osid.id.Id getGradebookColumn() {
        return (makeQueryElementId("osid.grading.gradebook.GradebookColumn"));
    }


    /**
     *  Gets the AncestorGradebookId element Id.
     *
     *  @return the AncestorGradebookId element Id
     */

    public static org.osid.id.Id getAncestorGradebookId() {
        return (makeQueryElementId("osid.grading.gradebook.AncestorGradebookId"));
    }


    /**
     *  Gets the AncestorGradebook element Id.
     *
     *  @return the AncestorGradebook element Id
     */

    public static org.osid.id.Id getAncestorGradebook() {
        return (makeQueryElementId("osid.grading.gradebook.AncestorGradebook"));
    }


    /**
     *  Gets the DescendantGradebookId element Id.
     *
     *  @return the DescendantGradebookId element Id
     */

    public static org.osid.id.Id getDescendantGradebookId() {
        return (makeQueryElementId("osid.grading.gradebook.DescendantGradebookId"));
    }


    /**
     *  Gets the DescendantGradebook element Id.
     *
     *  @return the DescendantGradebook element Id
     */

    public static org.osid.id.Id getDescendantGradebook() {
        return (makeQueryElementId("osid.grading.gradebook.DescendantGradebook"));
    }
}
