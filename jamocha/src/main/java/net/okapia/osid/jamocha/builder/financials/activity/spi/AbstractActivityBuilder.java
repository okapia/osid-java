//
// AbstractActivity.java
//
//     Defines an Activity builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.financials.activity.spi;


/**
 *  Defines an <code>Activity</code> builder.
 */

public abstract class AbstractActivityBuilder<T extends AbstractActivityBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractTemporalOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.financials.activity.ActivityMiter activity;


    /**
     *  Constructs a new <code>AbstractActivityBuilder</code>.
     *
     *  @param activity the activity to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractActivityBuilder(net.okapia.osid.jamocha.builder.financials.activity.ActivityMiter activity) {
        super(activity);
        this.activity = activity;
        return;
    }


    /**
     *  Builds the activity.
     *
     *  @return the new activity
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.financials.Activity build() {
        (new net.okapia.osid.jamocha.builder.validator.financials.activity.ActivityValidator(getValidations())).validate(this.activity);
        return (new net.okapia.osid.jamocha.builder.financials.activity.ImmutableActivity(this.activity));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the activity miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.financials.activity.ActivityMiter getMiter() {
        return (this.activity);
    }


    /**
     *  Sets the organization.
     *
     *  @param organization an organization
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>organization</code> is <code>null</code>
     */

    public T organization(org.osid.resource.Resource organization) {
        getMiter().setOrganization(organization);
        return (self());
    }


    /**
     *  Sets the supervisor.
     *
     *  @param supervisor a supervisor
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>supervisor</code>
     *          is <code>null</code>
     */

    public T supervisor(org.osid.resource.Resource supervisor) {
        getMiter().setSupervisor(supervisor);
        return (self());
    }


    /**
     *  Sets the code.
     *
     *  @param code a code
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>code</code> is
     *          <code>null</code>
     */

    public T code(String code) {
        getMiter().setCode(code);
        return (self());
    }


    /**
     *  Adds an Activity record.
     *
     *  @param record an activity record
     *  @param recordType the type of activity record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.financials.records.ActivityRecord record, org.osid.type.Type recordType) {
        getMiter().addActivityRecord(record, recordType);
        return (self());
    }
}       


