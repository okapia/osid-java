//
// AbstractSiteLookupSession.java
//
//    A starter implementation framework for providing a Site
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.installation.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Site
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getSites(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractSiteLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.installation.SiteLookupSession {

    private boolean pedantic  = false;


    /**
     *  Tests if this user can perform <code>Site</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupSites() {
        return (true);
    }


    /**
     *  A complete view of the <code>Site</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeSiteView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Site</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenarySiteView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }

     
    /**
     *  Gets the <code>Site</code> specified by its <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Site</code> may have a different <code>Id</code> than
     *  requested, such as the case where a duplicate <code>Id</code>
     *  was assigned to a <code>Site</code> and retained for
     *  compatibility.
     *
     *  @param  siteId <code>Id</code> of the
     *          <code>Site</code>
     *  @return the site
     *  @throws org.osid.NotFoundException <code>siteId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>siteId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.Site getSite(org.osid.id.Id siteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.installation.SiteList sites = getSites()) {
            while (sites.hasNext()) {
                org.osid.installation.Site site = sites.getNextSite();
                if (site.getId().equals(siteId)) {
                    return (site);
                }
            }
        } 

        throw new org.osid.NotFoundException(siteId + " not found");
    }


    /**
     *  Gets all <code>Sites</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  sites or an error results. Otherwise, the returned list
     *  may contain only those sites that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Sites</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.installation.SiteList getSites()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;
}
