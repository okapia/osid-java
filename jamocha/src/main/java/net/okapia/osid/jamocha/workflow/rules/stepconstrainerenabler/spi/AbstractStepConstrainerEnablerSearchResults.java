//
// AbstractStepConstrainerEnablerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.workflow.rules.stepconstrainerenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractStepConstrainerEnablerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.workflow.rules.StepConstrainerEnablerSearchResults {

    private org.osid.workflow.rules.StepConstrainerEnablerList stepConstrainerEnablers;
    private final org.osid.workflow.rules.StepConstrainerEnablerQueryInspector inspector;
    private final java.util.Collection<org.osid.workflow.rules.records.StepConstrainerEnablerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractStepConstrainerEnablerSearchResults.
     *
     *  @param stepConstrainerEnablers the result set
     *  @param stepConstrainerEnablerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>stepConstrainerEnablers</code>
     *          or <code>stepConstrainerEnablerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractStepConstrainerEnablerSearchResults(org.osid.workflow.rules.StepConstrainerEnablerList stepConstrainerEnablers,
                                            org.osid.workflow.rules.StepConstrainerEnablerQueryInspector stepConstrainerEnablerQueryInspector) {
        nullarg(stepConstrainerEnablers, "step constrainer enablers");
        nullarg(stepConstrainerEnablerQueryInspector, "step constrainer enabler query inspectpr");

        this.stepConstrainerEnablers = stepConstrainerEnablers;
        this.inspector = stepConstrainerEnablerQueryInspector;

        return;
    }


    /**
     *  Gets the step constrainer enabler list resulting from a search.
     *
     *  @return a step constrainer enabler list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerList getStepConstrainerEnablers() {
        if (this.stepConstrainerEnablers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.workflow.rules.StepConstrainerEnablerList stepConstrainerEnablers = this.stepConstrainerEnablers;
        this.stepConstrainerEnablers = null;
	return (stepConstrainerEnablers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.workflow.rules.StepConstrainerEnablerQueryInspector getStepConstrainerEnablerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  step constrainer enabler search record <code> Type. </code> This method must
     *  be used to retrieve a stepConstrainerEnabler implementing the requested
     *  record.
     *
     *  @param stepConstrainerEnablerSearchRecordType a stepConstrainerEnabler search 
     *         record type 
     *  @return the step constrainer enabler search
     *  @throws org.osid.NullArgumentException
     *          <code>stepConstrainerEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(stepConstrainerEnablerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.workflow.rules.records.StepConstrainerEnablerSearchResultsRecord getStepConstrainerEnablerSearchResultsRecord(org.osid.type.Type stepConstrainerEnablerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.workflow.rules.records.StepConstrainerEnablerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(stepConstrainerEnablerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(stepConstrainerEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record step constrainer enabler search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addStepConstrainerEnablerRecord(org.osid.workflow.rules.records.StepConstrainerEnablerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "step constrainer enabler record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
