//
// AbstractMapInstallationLookupSession
//
//    A simple framework for providing an Installation lookup service
//    backed by a fixed collection of installations.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.installation.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an Installation lookup service backed by a
 *  fixed collection of installations. The installations are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Installations</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapInstallationLookupSession
    extends net.okapia.osid.jamocha.installation.spi.AbstractInstallationLookupSession
    implements org.osid.installation.InstallationLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.installation.Installation> installations = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.installation.Installation>());


    /**
     *  Makes an <code>Installation</code> available in this session.
     *
     *  @param  installation an installation
     *  @throws org.osid.NullArgumentException <code>installation<code>
     *          is <code>null</code>
     */

    protected void putInstallation(org.osid.installation.Installation installation) {
        this.installations.put(installation.getId(), installation);
        return;
    }


    /**
     *  Makes an array of installations available in this session.
     *
     *  @param  installations an array of installations
     *  @throws org.osid.NullArgumentException <code>installations<code>
     *          is <code>null</code>
     */

    protected void putInstallations(org.osid.installation.Installation[] installations) {
        putInstallations(java.util.Arrays.asList(installations));
        return;
    }


    /**
     *  Makes a collection of installations available in this session.
     *
     *  @param  installations a collection of installations
     *  @throws org.osid.NullArgumentException <code>installations<code>
     *          is <code>null</code>
     */

    protected void putInstallations(java.util.Collection<? extends org.osid.installation.Installation> installations) {
        for (org.osid.installation.Installation installation : installations) {
            this.installations.put(installation.getId(), installation);
        }

        return;
    }


    /**
     *  Removes an Installation from this session.
     *
     *  @param  installationId the <code>Id</code> of the installation
     *  @throws org.osid.NullArgumentException <code>installationId<code> is
     *          <code>null</code>
     */

    protected void removeInstallation(org.osid.id.Id installationId) {
        this.installations.remove(installationId);
        return;
    }


    /**
     *  Gets the <code>Installation</code> specified by its <code>Id</code>.
     *
     *  @param  installationId <code>Id</code> of the <code>Installation</code>
     *  @return the installation
     *  @throws org.osid.NotFoundException <code>installationId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>installationId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.Installation getInstallation(org.osid.id.Id installationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.installation.Installation installation = this.installations.get(installationId);
        if (installation == null) {
            throw new org.osid.NotFoundException("installation not found: " + installationId);
        }

        return (installation);
    }


    /**
     *  Gets all <code>Installations</code>. In plenary mode, the returned
     *  list contains all known installations or an error
     *  results. Otherwise, the returned list may contain only those
     *  installations that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Installations</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.InstallationList getInstallations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.installation.installation.ArrayInstallationList(this.installations.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.installations.clear();
        super.close();
        return;
    }
}
