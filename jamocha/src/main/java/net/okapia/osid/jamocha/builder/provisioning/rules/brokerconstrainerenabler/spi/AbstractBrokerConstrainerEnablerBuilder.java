//
// AbstractBrokerConstrainerEnabler.java
//
//     Defines a BrokerConstrainerEnabler builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.provisioning.rules.brokerconstrainerenabler.spi;


/**
 *  Defines a <code>BrokerConstrainerEnabler</code> builder.
 */

public abstract class AbstractBrokerConstrainerEnablerBuilder<T extends AbstractBrokerConstrainerEnablerBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidEnablerBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.provisioning.rules.brokerconstrainerenabler.BrokerConstrainerEnablerMiter brokerConstrainerEnabler;


    /**
     *  Constructs a new <code>AbstractBrokerConstrainerEnablerBuilder</code>.
     *
     *  @param brokerConstrainerEnabler the broker constrainer enabler to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractBrokerConstrainerEnablerBuilder(net.okapia.osid.jamocha.builder.provisioning.rules.brokerconstrainerenabler.BrokerConstrainerEnablerMiter brokerConstrainerEnabler) {
        super(brokerConstrainerEnabler);
        this.brokerConstrainerEnabler = brokerConstrainerEnabler;
        return;
    }


    /**
     *  Builds the broker constrainer enabler.
     *
     *  @return the new broker constrainer enabler
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.provisioning.rules.BrokerConstrainerEnabler build() {
        (new net.okapia.osid.jamocha.builder.validator.provisioning.rules.brokerconstrainerenabler.BrokerConstrainerEnablerValidator(getValidations())).validate(this.brokerConstrainerEnabler);
        return (new net.okapia.osid.jamocha.builder.provisioning.rules.brokerconstrainerenabler.ImmutableBrokerConstrainerEnabler(this.brokerConstrainerEnabler));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the broker constrainer enabler miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.provisioning.rules.brokerconstrainerenabler.BrokerConstrainerEnablerMiter getMiter() {
        return (this.brokerConstrainerEnabler);
    }


    /**
     *  Adds a BrokerConstrainerEnabler record.
     *
     *  @param record a broker constrainer enabler record
     *  @param recordType the type of broker constrainer enabler record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.provisioning.rules.records.BrokerConstrainerEnablerRecord record, org.osid.type.Type recordType) {
        getMiter().addBrokerConstrainerEnablerRecord(record, recordType);
        return (self());
    }
}       


