//
// MutableIndexedMapRuleLookupSession
//
//    Implements a Rule lookup service backed by a collection of
//    rules indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.rules;


/**
 *  Implements a Rule lookup service backed by a collection of
 *  rules. The rules are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some rules may be compatible
 *  with more types than are indicated through these rule
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of rules can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapRuleLookupSession
    extends net.okapia.osid.jamocha.core.rules.spi.AbstractIndexedMapRuleLookupSession
    implements org.osid.rules.RuleLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapRuleLookupSession} with no rules.
     *
     *  @param engine the engine
     *  @throws org.osid.NullArgumentException {@code engine}
     *          is {@code null}
     */

      public MutableIndexedMapRuleLookupSession(org.osid.rules.Engine engine) {
        setEngine(engine);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapRuleLookupSession} with a
     *  single rule.
     *  
     *  @param engine the engine
     *  @param  rule a single rule
     *  @throws org.osid.NullArgumentException {@code engine} or
     *          {@code rule} is {@code null}
     */

    public MutableIndexedMapRuleLookupSession(org.osid.rules.Engine engine,
                                                  org.osid.rules.Rule rule) {
        this(engine);
        putRule(rule);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapRuleLookupSession} using an
     *  array of rules.
     *
     *  @param engine the engine
     *  @param  rules an array of rules
     *  @throws org.osid.NullArgumentException {@code engine} or
     *          {@code rules} is {@code null}
     */

    public MutableIndexedMapRuleLookupSession(org.osid.rules.Engine engine,
                                                  org.osid.rules.Rule[] rules) {
        this(engine);
        putRules(rules);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapRuleLookupSession} using a
     *  collection of rules.
     *
     *  @param engine the engine
     *  @param  rules a collection of rules
     *  @throws org.osid.NullArgumentException {@code engine} or
     *          {@code rules} is {@code null}
     */

    public MutableIndexedMapRuleLookupSession(org.osid.rules.Engine engine,
                                                  java.util.Collection<? extends org.osid.rules.Rule> rules) {

        this(engine);
        putRules(rules);
        return;
    }
    

    /**
     *  Makes a {@code Rule} available in this session.
     *
     *  @param  rule a rule
     *  @throws org.osid.NullArgumentException {@code rule{@code  is
     *          {@code null}
     */

    @Override
    public void putRule(org.osid.rules.Rule rule) {
        super.putRule(rule);
        return;
    }


    /**
     *  Makes an array of rules available in this session.
     *
     *  @param  rules an array of rules
     *  @throws org.osid.NullArgumentException {@code rules{@code 
     *          is {@code null}
     */

    @Override
    public void putRules(org.osid.rules.Rule[] rules) {
        super.putRules(rules);
        return;
    }


    /**
     *  Makes collection of rules available in this session.
     *
     *  @param  rules a collection of rules
     *  @throws org.osid.NullArgumentException {@code rule{@code  is
     *          {@code null}
     */

    @Override
    public void putRules(java.util.Collection<? extends org.osid.rules.Rule> rules) {
        super.putRules(rules);
        return;
    }


    /**
     *  Removes a Rule from this session.
     *
     *  @param ruleId the {@code Id} of the rule
     *  @throws org.osid.NullArgumentException {@code ruleId{@code  is
     *          {@code null}
     */

    @Override
    public void removeRule(org.osid.id.Id ruleId) {
        super.removeRule(ruleId);
        return;
    }    
}
