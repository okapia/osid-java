//
// AbstractQueryAgentLookupSession.java
//
//    An inline adapter that maps an AgentLookupSession to
//    an AgentQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.authentication.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps an AgentLookupSession to
 *  an AgentQuerySession.
 */

public abstract class AbstractQueryAgentLookupSession
    extends net.okapia.osid.jamocha.authentication.spi.AbstractAgentLookupSession
    implements org.osid.authentication.AgentLookupSession {

    private final org.osid.authentication.AgentQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryAgentLookupSession.
     *
     *  @param querySession the underlying agent query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryAgentLookupSession(org.osid.authentication.AgentQuerySession querySession) {
        nullarg(querySession, "agent query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Agency</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Agency Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAgencyId() {
        return (this.session.getAgencyId());
    }


    /**
     *  Gets the <code>Agency</code> associated with this 
     *  session.
     *
     *  @return the <code>Agency</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.Agency getAgency()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getAgency());
    }


    /**
     *  Tests if this user can perform <code>Agent</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAgents() {
        return (this.session.canSearchAgents());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include agents in agencies which are children
     *  of this agency in the agency hierarchy.
     */

    @OSID @Override
    public void useFederatedAgencyView() {
        this.session.useFederatedAgencyView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this agency only.
     */

    @OSID @Override
    public void useIsolatedAgencyView() {
        this.session.useIsolatedAgencyView();
        return;
    }
    
     
    /**
     *  Gets the <code>Agent</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Agent</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Agent</code> and
     *  retained for compatibility.
     *
     *  @param  agentId <code>Id</code> of the
     *          <code>Agent</code>
     *  @return the agent
     *  @throws org.osid.NotFoundException <code>agentId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>agentId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.Agent getAgent(org.osid.id.Id agentId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authentication.AgentQuery query = getQuery();
        query.matchId(agentId, true);
        org.osid.authentication.AgentList agents = this.session.getAgentsByQuery(query);
        if (agents.hasNext()) {
            return (agents.getNextAgent());
        } 
        
        throw new org.osid.NotFoundException(agentId + " not found");
    }


    /**
     *  Gets an <code>AgentList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  agents specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Agents</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  agentIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Agent</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>agentIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.AgentList getAgentsByIds(org.osid.id.IdList agentIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authentication.AgentQuery query = getQuery();

        try (org.osid.id.IdList ids = agentIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getAgentsByQuery(query));
    }


    /**
     *  Gets an <code>AgentList</code> corresponding to the given
     *  agent genus <code>Type</code> which does not include
     *  agents of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  agents or an error results. Otherwise, the returned list
     *  may contain only those agents that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  agentGenusType an agent genus type 
     *  @return the returned <code>Agent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>agentGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.AgentList getAgentsByGenusType(org.osid.type.Type agentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authentication.AgentQuery query = getQuery();
        query.matchGenusType(agentGenusType, true);
        return (this.session.getAgentsByQuery(query));
    }


    /**
     *  Gets an <code>AgentList</code> corresponding to the given
     *  agent genus <code>Type</code> and include any additional
     *  agents with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  agents or an error results. Otherwise, the returned list
     *  may contain only those agents that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  agentGenusType an agent genus type 
     *  @return the returned <code>Agent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>agentGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.AgentList getAgentsByParentGenusType(org.osid.type.Type agentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authentication.AgentQuery query = getQuery();
        query.matchParentGenusType(agentGenusType, true);
        return (this.session.getAgentsByQuery(query));
    }


    /**
     *  Gets an <code>AgentList</code> containing the given
     *  agent record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  agents or an error results. Otherwise, the returned list
     *  may contain only those agents that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  agentRecordType an agent record type 
     *  @return the returned <code>Agent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>agentRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.AgentList getAgentsByRecordType(org.osid.type.Type agentRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authentication.AgentQuery query = getQuery();
        query.matchRecordType(agentRecordType, true);
        return (this.session.getAgentsByQuery(query));
    }

    
    /**
     *  Gets all <code>Agents</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  agents or an error results. Otherwise, the returned list
     *  may contain only those agents that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Agents</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.AgentList getAgents()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authentication.AgentQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getAgentsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.authentication.AgentQuery getQuery() {
        org.osid.authentication.AgentQuery query = this.session.getAgentQuery();
        return (query);
    }
}
