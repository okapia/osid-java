//
// AbstractMapCourseLookupSession
//
//    A simple framework for providing a Course lookup service
//    backed by a fixed collection of courses.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Course lookup service backed by a
 *  fixed collection of courses. The courses are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Courses</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapCourseLookupSession
    extends net.okapia.osid.jamocha.course.spi.AbstractCourseLookupSession
    implements org.osid.course.CourseLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.course.Course> courses = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.course.Course>());


    /**
     *  Makes a <code>Course</code> available in this session.
     *
     *  @param  course a course
     *  @throws org.osid.NullArgumentException <code>course<code>
     *          is <code>null</code>
     */

    protected void putCourse(org.osid.course.Course course) {
        this.courses.put(course.getId(), course);
        return;
    }


    /**
     *  Makes an array of courses available in this session.
     *
     *  @param  courses an array of courses
     *  @throws org.osid.NullArgumentException <code>courses<code>
     *          is <code>null</code>
     */

    protected void putCourses(org.osid.course.Course[] courses) {
        putCourses(java.util.Arrays.asList(courses));
        return;
    }


    /**
     *  Makes a collection of courses available in this session.
     *
     *  @param  courses a collection of courses
     *  @throws org.osid.NullArgumentException <code>courses<code>
     *          is <code>null</code>
     */

    protected void putCourses(java.util.Collection<? extends org.osid.course.Course> courses) {
        for (org.osid.course.Course course : courses) {
            this.courses.put(course.getId(), course);
        }

        return;
    }


    /**
     *  Removes a Course from this session.
     *
     *  @param  courseId the <code>Id</code> of the course
     *  @throws org.osid.NullArgumentException <code>courseId<code> is
     *          <code>null</code>
     */

    protected void removeCourse(org.osid.id.Id courseId) {
        this.courses.remove(courseId);
        return;
    }


    /**
     *  Gets the <code>Course</code> specified by its <code>Id</code>.
     *
     *  @param  courseId <code>Id</code> of the <code>Course</code>
     *  @return the course
     *  @throws org.osid.NotFoundException <code>courseId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>courseId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.Course getCourse(org.osid.id.Id courseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.course.Course course = this.courses.get(courseId);
        if (course == null) {
            throw new org.osid.NotFoundException("course not found: " + courseId);
        }

        return (course);
    }


    /**
     *  Gets all <code>Courses</code>. In plenary mode, the returned
     *  list contains all known courses or an error
     *  results. Otherwise, the returned list may contain only those
     *  courses that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Courses</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseList getCourses()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.course.ArrayCourseList(this.courses.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.courses.clear();
        super.close();
        return;
    }
}
