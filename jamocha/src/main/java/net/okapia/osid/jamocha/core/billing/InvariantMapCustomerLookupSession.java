//
// InvariantMapCustomerLookupSession
//
//    Implements a Customer lookup service backed by a fixed collection of
//    customers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.billing;


/**
 *  Implements a Customer lookup service backed by a fixed
 *  collection of customers. The customers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapCustomerLookupSession
    extends net.okapia.osid.jamocha.core.billing.spi.AbstractMapCustomerLookupSession
    implements org.osid.billing.CustomerLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapCustomerLookupSession</code> with no
     *  customers.
     *  
     *  @param business the business
     *  @throws org.osid.NullArgumnetException {@code business} is
     *          {@code null}
     */

    public InvariantMapCustomerLookupSession(org.osid.billing.Business business) {
        setBusiness(business);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapCustomerLookupSession</code> with a single
     *  customer.
     *  
     *  @param business the business
     *  @param customer a single customer
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code customer} is <code>null</code>
     */

      public InvariantMapCustomerLookupSession(org.osid.billing.Business business,
                                               org.osid.billing.Customer customer) {
        this(business);
        putCustomer(customer);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapCustomerLookupSession</code> using an array
     *  of customers.
     *  
     *  @param business the business
     *  @param customers an array of customers
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code customers} is <code>null</code>
     */

      public InvariantMapCustomerLookupSession(org.osid.billing.Business business,
                                               org.osid.billing.Customer[] customers) {
        this(business);
        putCustomers(customers);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapCustomerLookupSession</code> using a
     *  collection of customers.
     *
     *  @param business the business
     *  @param customers a collection of customers
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code customers} is <code>null</code>
     */

      public InvariantMapCustomerLookupSession(org.osid.billing.Business business,
                                               java.util.Collection<? extends org.osid.billing.Customer> customers) {
        this(business);
        putCustomers(customers);
        return;
    }
}
