//
// BidMiter.java
//
//     Defines a Bid miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.bidding.bid;


/**
 *  Defines a <code>Bid</code> miter for use with the builders.
 */

public interface BidMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidRelationshipMiter,
            org.osid.bidding.Bid {


    /**
     *  Sets the auction.
     *
     *  @param auction an auction
     *  @throws org.osid.NullArgumentException <code>auction</code> is
     *          <code>null</code>
     */

    public void setAuction(org.osid.bidding.Auction auction);


    /**
     *  Sets the bidder.
     *
     *  @param bidder a bidder
     *  @throws org.osid.NullArgumentException <code>bidder</code> is
     *          <code>null</code>
     */

    public void setBidder(org.osid.resource.Resource bidder);


    /**
     *  Sets the bidding agent.
     *
     *  @param agent a bidding agent
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    public void setBiddingAgent(org.osid.authentication.Agent agent);


    /**
     *  Sets the quantity.
     *
     *  @param quantity a quantity
     *  @throws org.osid.InvalidArgumentException
     *          <code>quantity</code> is negative
     */

    public void setQuantity(long quantity);


    /**
     *  Sets the current bid.
     *
     *  @param bid a current bid
     *  @throws org.osid.NullArgumentException <code>bid</code> is
     *          <code>null</code>
     */

    public void setCurrentBid(org.osid.financials.Currency bid);


    /**
     *  Sets the maximum bid.
     *
     *  @param bid a maximum bid
     *  @throws org.osid.NullArgumentException <code>bid</code> is
     *          <code>null</code>
     */

    public void setMaximumBid(org.osid.financials.Currency bid);


    /**
     *  Sets the winner flag.
     *
     *  @param winner <code> true </code> if this was a winnign bid,
     *         <code> false </code> otherwise
     */

    public void setWinner(boolean winner);


    /**
     *  Sets the settlement amount.
     *
     *  @param amount a settlement amount
     *  @throws org.osid.NullArgumentException <code>amount</code> is
     *          <code>null</code>
     */

    public void setSettlementAmount(org.osid.financials.Currency amount);


    /**
     *  Adds a Bid record.
     *
     *  @param record a bid record
     *  @param recordType the type of bid record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addBidRecord(org.osid.bidding.records.BidRecord record, org.osid.type.Type recordType);
}       


