//
// AbstractUnknownInventory.java
//
//     Defines an unknown Inventory.
//
//
// Tom Coppeto
// Okapia
// 8 December 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.nil.inventory.inventory.spi;


/**
 *  Defines an unknown <code>Inventory</code>.
 */

public abstract class AbstractUnknownInventory
    extends net.okapia.osid.jamocha.inventory.inventory.spi.AbstractInventory
    implements org.osid.inventory.Inventory {

    protected static final String OBJECT = "osid.inventory.Inventory";


    /**
     *  Constructs a new <code>AbstractUnknownInventory</code>.
     */

    public AbstractUnknownInventory() {
        setId(net.okapia.osid.jamocha.nil.privateutil.UnknownId.valueOf(OBJECT));
        setDisplayName(net.okapia.osid.jamocha.nil.privateutil.DisplayName.valueOf(OBJECT));
        setDescription(net.okapia.osid.jamocha.nil.privateutil.Description.valueOf(OBJECT));

        setStock(new net.okapia.osid.jamocha.nil.inventory.stock.UnknownStock());
        setDate(net.okapia.osid.primordium.calendaring.GregorianUTCDateTime.unknown());
        setQuantity(new java.math.BigDecimal(0));

        return;
    }


    /**
     *  Constructs a new <code>AbstractUnknownInventory</code> with all
     *  the optional methods enabled.
     *
     *  @param optional <code>true</code> to enable the optional
     *         methods
     */

    public AbstractUnknownInventory(boolean optional) {
        this();
        return;
    }
}
