//
// AbstractFederatingAssetLookupSession.java
//
//     An abstract federating adapter for an AssetLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.repository.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for an
 *  AssetLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingAssetLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.repository.AssetLookupSession>
    implements org.osid.repository.AssetLookupSession {

    private boolean parallel = false;
    private org.osid.repository.Repository repository = new net.okapia.osid.jamocha.nil.repository.repository.UnknownRepository();


    /**
     *  Constructs a new <code>AbstractFederatingAssetLookupSession</code>.
     */

    protected AbstractFederatingAssetLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.repository.AssetLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Repository/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Repository Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getRepositoryId() {
        return (this.repository.getId());
    }


    /**
     *  Gets the <code>Repository</code> associated with this 
     *  session.
     *
     *  @return the <code>Repository</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.Repository getRepository()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.repository);
    }


    /**
     *  Sets the <code>Repository</code>.
     *
     *  @param  repository the repository for this session
     *  @throws org.osid.NullArgumentException <code>repository</code>
     *          is <code>null</code>
     */

    protected void setRepository(org.osid.repository.Repository repository) {
        nullarg(repository, "repository");
        this.repository = repository;
        return;
    }


    /**
     *  Tests if this user can perform <code>Asset</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAssets() {
        for (org.osid.repository.AssetLookupSession session : getSessions()) {
            if (session.canLookupAssets()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Asset</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAssetView() {
        for (org.osid.repository.AssetLookupSession session : getSessions()) {
            session.useComparativeAssetView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Asset</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAssetView() {
        for (org.osid.repository.AssetLookupSession session : getSessions()) {
            session.usePlenaryAssetView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include assets in repositories which are children
     *  of this repository in the repository hierarchy.
     */

    @OSID @Override
    public void useFederatedRepositoryView() {
        for (org.osid.repository.AssetLookupSession session : getSessions()) {
            session.useFederatedRepositoryView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this repository only.
     */

    @OSID @Override
    public void useIsolatedRepositoryView() {
        for (org.osid.repository.AssetLookupSession session : getSessions()) {
            session.useIsolatedRepositoryView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Asset</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Asset</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Asset</code> and
     *  retained for compatibility.
     *
     *  @param  assetId <code>Id</code> of the
     *          <code>Asset</code>
     *  @return the asset
     *  @throws org.osid.NotFoundException <code>assetId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>assetId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.Asset getAsset(org.osid.id.Id assetId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.repository.AssetLookupSession session : getSessions()) {
            try {
                return (session.getAsset(assetId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(assetId + " not found");
    }


    /**
     *  Gets an <code>AssetList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  assets specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Assets</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  assetIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Asset</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>assetIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.AssetList getAssetsByIds(org.osid.id.IdList assetIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.repository.asset.MutableAssetList ret = new net.okapia.osid.jamocha.repository.asset.MutableAssetList();

        try (org.osid.id.IdList ids = assetIds) {
            while (ids.hasNext()) {
                ret.addAsset(getAsset(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets an <code>AssetList</code> corresponding to the given
     *  asset genus <code>Type</code> which does not include
     *  assets of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  assets or an error results. Otherwise, the returned list
     *  may contain only those assets that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  assetGenusType an asset genus type 
     *  @return the returned <code>Asset</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assetGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.AssetList getAssetsByGenusType(org.osid.type.Type assetGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.repository.asset.FederatingAssetList ret = getAssetList();

        for (org.osid.repository.AssetLookupSession session : getSessions()) {
            ret.addAssetList(session.getAssetsByGenusType(assetGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AssetList</code> corresponding to the given
     *  asset genus <code>Type</code> and include any additional
     *  assets with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  assets or an error results. Otherwise, the returned list
     *  may contain only those assets that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  assetGenusType an asset genus type 
     *  @return the returned <code>Asset</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assetGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.AssetList getAssetsByParentGenusType(org.osid.type.Type assetGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.repository.asset.FederatingAssetList ret = getAssetList();

        for (org.osid.repository.AssetLookupSession session : getSessions()) {
            ret.addAssetList(session.getAssetsByParentGenusType(assetGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AssetList</code> containing the given
     *  asset record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  assets or an error results. Otherwise, the returned list
     *  may contain only those assets that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  assetRecordType an asset record type 
     *  @return the returned <code>Asset</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assetRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.AssetList getAssetsByRecordType(org.osid.type.Type assetRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.repository.asset.FederatingAssetList ret = getAssetList();

        for (org.osid.repository.AssetLookupSession session : getSessions()) {
            ret.addAssetList(session.getAssetsByRecordType(assetRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AssetList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known assets or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  assets that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Asset</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.repository.AssetList getAssetsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        net.okapia.osid.jamocha.adapter.federator.repository.asset.FederatingAssetList ret = getAssetList();

        for (org.osid.repository.AssetLookupSession session : getSessions()) {
            ret.addAssetList(session.getAssetsByProvider(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Assets</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  assets or an error results. Otherwise, the returned list
     *  may contain only those assets that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Assets</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.AssetList getAssets()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.repository.asset.FederatingAssetList ret = getAssetList();

        for (org.osid.repository.AssetLookupSession session : getSessions()) {
            ret.addAssetList(session.getAssets());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.repository.asset.FederatingAssetList getAssetList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.repository.asset.ParallelAssetList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.repository.asset.CompositeAssetList());
        }
    }
}
