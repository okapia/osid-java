//
// AbstractAssemblyDirectoryQuery.java
//
//     A DirectoryQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.filing.directory.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A DirectoryQuery that stores terms.
 */

public abstract class AbstractAssemblyDirectoryQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCatalogQuery
    implements org.osid.filing.DirectoryQuery,
               org.osid.filing.DirectoryQueryInspector,
               org.osid.filing.DirectorySearchOrder {

    private final java.util.Collection<org.osid.filing.records.DirectoryQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.filing.records.DirectoryQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.filing.records.DirectorySearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyDirectoryQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyDirectoryQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches entry names. Supplying multiple strings behaves like a
     *  boolean <code> AND </code> among the elements each which must
     *  correspond to the <code> stringMatchType. </code> An <code> OR
     *  </code> can be performed with multiple queries.
     *
     *  @param  name name to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> name </code> not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> name </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchName(String name, org.osid.type.Type stringMatchType, 
                          boolean match) {
        getAssembler().addStringTerm(getNameColumn(), name, stringMatchType, match);
        return;
    }


    /**
     *  Clears the name terms. 
     */

    @OSID @Override
    public void clearNameTerms() {
        getAssembler().clearTerms(getNameColumn());
        return;
    }


    /**
     *  Gets the name query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getNameTerms() {
        return (getAssembler().getStringTerms(getNameColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the entry name. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByName(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getNameColumn(), style);
        return;
    }


    /**
     *  Gets the Name column name.
     *
     * @return the column name
     */

    protected String getNameColumn() {
        return ("name");
    }


    /**
     *  Matches an absolute pathname of a directory entry. Supplying multiple 
     *  strings behaves like a boolean <code> AND </code> among the elements 
     *  each which must correspond to the <code> stringMatchType. </code> An 
     *  <code> OR </code> can be performed with multiple queries. 
     *
     *  @param  path path to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> name </code> not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> path </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchPath(String path, org.osid.type.Type stringMatchType, 
                          boolean match) {
        getAssembler().addStringTerm(getPathColumn(), path, stringMatchType, match);
        return;
    }


    /**
     *  Clears the path terms. 
     */

    @OSID @Override
    public void clearPathTerms() {
        getAssembler().clearTerms(getPathColumn());
        return;
    }


    /**
     *  Gets the path query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getPathTerms() {
        return (getAssembler().getStringTerms(getPathColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the entry path. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPath(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getPathColumn(), style);
        return;
    }


    /**
     *  Gets the Path column name.
     *
     * @return the column name
     */

    protected String getPathColumn() {
        return ("path");
    }


    /**
     *  Tests if a <code> DirectoryQuery </code> is available. 
     *
     *  @return <code> true </code> if a directory query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDirectoryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a directory to match the parent directory. There is 
     *  only one <code> DirectoryQuery </code> per <code> 
     *  DifrectoryEntryQuery. </code> Multiple retrievals return the same 
     *  object. 
     *
     *  @return the directory query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectoryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.DirectoryQuery getDirectoryQuery() {
        throw new org.osid.UnimplementedException("supportsDirectoryQuery() is false");
    }


    /**
     *  Clears the directory terms. 
     */

    @OSID @Override
    public void clearDirectoryTerms() {
        getAssembler().clearTerms(getDirectoryColumn());
        return;
    }


    /**
     *  Gets the directory query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.filing.DirectoryQueryInspector[] getDirectoryTerms() {
        return (new org.osid.filing.DirectoryQueryInspector[0]);
    }


    /**
     *  Gets the Directory column name.
     *
     * @return the column name
     */

    protected String getDirectoryColumn() {
        return ("directory");
    }


    /**
     *  Matches aliases only. 
     *
     *  @param  match <code> true </code> to match aliases, <code> false 
     *          </code> to match target files 
     */

    @OSID @Override
    public void matchAliases(boolean match) {
        getAssembler().addBooleanTerm(getAliasesColumn(), match);
        return;
    }


    /**
     *  Matches a file that has any aliases. 
     *
     *  @param  match <code> true </code> to match any alias, <code> false 
     *          </code> to match objects with no aliases 
     */

    @OSID @Override
    public void matchAnyAliases(boolean match) {
        getAssembler().addBooleanWildcardTerm(getAliasesColumn(), match);
        return;
    }


    /**
     *  Clears the aliases terms. 
     */

    @OSID @Override
    public void clearAliasesTerms() {
        getAssembler().clearTerms(getAliasesColumn());
        return;
    }


    /**
     *  Gets the aliases query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getAliasesTerms() {
        return (getAssembler().getBooleanTerms(getAliasesColumn()));
    }


    /**
     *  Gets the Aliases column name.
     *
     * @return the column name
     */

    protected String getAliasesColumn() {
        return ("aliases");
    }


    /**
     *  Matches files whose entries are owned by the given agent id. 
     *
     *  @param  agentId the agent <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchOwnerId(org.osid.id.Id agentId, boolean match) {
        getAssembler().addIdTerm(getOwnerIdColumn(), agentId, match);
        return;
    }


    /**
     *  Clears the owner <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearOwnerIdTerms() {
        getAssembler().clearTerms(getOwnerIdColumn());
        return;
    }


    /**
     *  Gets the owner <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOwnerIdTerms() {
        return (getAssembler().getIdTerms(getOwnerIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the entry owner. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByOwner(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getOwnerColumn(), style);
        return;
    }


    /**
     *  Gets the OwnerId column name.
     *
     * @return the column name
     */

    protected String getOwnerIdColumn() {
        return ("owner_id");
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available for querying 
     *  agents. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOwnerQuery() {
        return (false);
    }


    /**
     *  Gets the query for an agent. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> supportsAgentQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getOwnerQuery() {
        throw new org.osid.UnimplementedException("supportsOwnerQuery() is false");
    }


    /**
     *  Clears the owner terms. 
     */

    @OSID @Override
    public void clearOwnerTerms() {
        getAssembler().clearTerms(getOwnerColumn());
        return;
    }


    /**
     *  Gets the owner query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authentication.AgentQueryInspector[] getOwnerTerms() {
        return (new org.osid.authentication.AgentQueryInspector[0]);
    }


    /**
     *  Tests if an agent search order is available. 
     *
     *  @return <code> true </code> if an agent search order interface is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOwnerSearchOrder() {
        return (false);
    }


    /**
     *  Gets an agent search order interface. 
     *
     *  @return an agent search order interface 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOwnerSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getOwnerSearchOrder() {
        throw new org.osid.UnimplementedException("supportsOwnerSearchOrder() is false");
    }


    /**
     *  Gets the Owner column name.
     *
     * @return the column name
     */

    protected String getOwnerColumn() {
        return ("owner");
    }


    /**
     *  Match directory entries that are created between the specified time 
     *  period inclusive. 
     *
     *  @param  start start time of the query 
     *  @param  end end time of the query 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> end </code> is les 
     *          than <code> start </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchCreatedTime(org.osid.calendaring.DateTime start, 
                                 org.osid.calendaring.DateTime end, 
                                 boolean match) {
        getAssembler().addDateTimeRangeTerm(getCreatedTimeColumn(), start, end, match);
        return;
    }


    /**
     *  Clears the created time terms. 
     */

    @OSID @Override
    public void clearCreatedTimeTerms() {
        getAssembler().clearTerms(getCreatedTimeColumn());
        return;
    }


    /**
     *  Gets the created time query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getCreatedTimeTerms() {
        return (getAssembler().getDateTimeRangeTerms(getCreatedTimeColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the entry 
     *  creation time. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCreatedTime(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCreatedTimeColumn(), style);
        return;
    }


    /**
     *  Gets the CreatedTime column name.
     *
     * @return the column name
     */

    protected String getCreatedTimeColumn() {
        return ("created_time");
    }


    /**
     *  Match directory entries that are modified between the
     *  specified time period inclusive.
     *
     *  @param  start start time of the query 
     *  @param  end end time of the query 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> end </code> is les 
     *          than <code> start </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchModifiedTime(org.osid.calendaring.DateTime start, 
                                  org.osid.calendaring.DateTime end, 
                                  boolean match) {
        getAssembler().addDateTimeRangeTerm(getModifiedTimeColumn(), start, end, match);
        return;
    }


    /**
     *  Clears the modified time terms. 
     */

    @OSID @Override
    public void clearModifiedTimeTerms() {
        getAssembler().clearTerms(getModifiedTimeColumn());
        return;
    }


    /**
     *  Gets the modified time query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getModifiedTimeTerms() {
        return (getAssembler().getDateTimeRangeTerms(getModifiedTimeColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the
     *  entry modification time.
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByModifiedTime(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getModifiedTimeColumn(), style);
        return;
    }


    /**
     *  Gets the ModifiedTime column name.
     *
     * @return the column name
     */

    protected String getModifiedTimeColumn() {
        return ("modified_time");
    }


    /**
     *  Match directory entries that were last accessed between the
     *  specified time period.
     *
     *  @param  start start time of the query 
     *  @param  end end time of the query 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> end </code> is les 
     *          than <code> start </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchLastAccessTime(org.osid.calendaring.DateTime start, 
                                    org.osid.calendaring.DateTime end, 
                                    boolean match) {
        getAssembler().addDateTimeRangeTerm(getLastAccessTimeColumn(), start, end, match);
        return;
    }


    /**
     *  Clears the last access time terms. 
     */

    @OSID @Override
    public void clearLastAccessTimeTerms() {
        getAssembler().clearTerms(getLastAccessTimeColumn());
        return;
    }


    /**
     *  Gets the last access time query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getLastAccessTimeTerms() {
        return (getAssembler().getDateTimeRangeTerms(getLastAccessTimeColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the entry last 
     *  access time. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByLastAccessTime(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getLastAccessTimeColumn(), style);
        return;
    }


    /**
     *  Gets the LastAccessTime column name.
     *
     * @return the column name
     */

    protected String getLastAccessTimeColumn() {
        return ("last_access_time");
    }


    /**
     *  Matches directories that contain the specified file name. 
     *
     *  @param  name a file name 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> name </code> not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> name </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchFileName(String name, org.osid.type.Type stringMatchType, 
                              boolean match) {
        getAssembler().addStringTerm(getFileNameColumn(), name, stringMatchType, match);
        return;
    }


    /**
     *  Clears all file name terms. 
     */

    @OSID @Override
    public void clearFileNameTerms() {
        getAssembler().clearTerms(getFileNameColumn());
        return;
    }


    /**
     *  Gets the file name query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getFileNameTerms() {
        return (getAssembler().getStringTerms(getFileNameColumn()));
    }


    /**
     *  Gets the FileName column name.
     *
     * @return the column name
     */

    protected String getFileNameColumn() {
        return ("file_name");
    }


    /**
     *  Tests if a <code> FileQuery </code> is available. 
     *
     *  @return <code> true </code> if a file query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFileQuery() {
        return (false);
    }


    /**
     *  Gets the query for a file contained within the directory. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @return the directory query 
     *  @throws org.osid.UnimplementedException <code> supportsFileQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileQuery getFileQuery(boolean match) {
        throw new org.osid.UnimplementedException("supportsFileQuery() is false");
    }


    /**
     *  Matches directories with any file. 
     *
     *  @param  match <code> true </code> to match directories with any file,, 
     *          <code> false </code> to match directories with no file. 
     */

    @OSID @Override
    public void matchAnyFile(boolean match) {
        getAssembler().addBooleanWildcardTerm(getFileColumn(), match);
        return;
    }


    /**
     *  Clears all file terms. 
     */

    @OSID @Override
    public void clearFileTerms() {
        getAssembler().clearTerms(getFileColumn());
        return;
    }


    /**
     *  Gets the file query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.filing.FileQueryInspector[] getFileTerms() {
        return (new org.osid.filing.FileQueryInspector[0]);
    }


    /**
     *  Gets the File column name.
     *
     * @return the column name
     */

    protected String getFileColumn() {
        return ("file");
    }


    /**
     *  Tests if this directory supports the given record
     *  <code>Type</code>.
     *
     *  @param  directoryRecordType a directory record type 
     *  @return <code>true</code> if the directoryRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>directoryRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type directoryRecordType) {
        for (org.osid.filing.records.DirectoryQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(directoryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  directoryRecordType the directory record type 
     *  @return the directory query record 
     *  @throws org.osid.NullArgumentException
     *          <code>directoryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(directoryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.filing.records.DirectoryQueryRecord getDirectoryQueryRecord(org.osid.type.Type directoryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.filing.records.DirectoryQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(directoryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(directoryRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  directoryRecordType the directory record type 
     *  @return the directory query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>directoryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(directoryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.filing.records.DirectoryQueryInspectorRecord getDirectoryQueryInspectorRecord(org.osid.type.Type directoryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.filing.records.DirectoryQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(directoryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(directoryRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param directoryRecordType the directory record type
     *  @return the directory search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>directoryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(directoryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.filing.records.DirectorySearchOrderRecord getDirectorySearchOrderRecord(org.osid.type.Type directoryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.filing.records.DirectorySearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(directoryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(directoryRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this directory. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param directoryQueryRecord the directory query record
     *  @param directoryQueryInspectorRecord the directory query inspector
     *         record
     *  @param directorySearchOrderRecord the directory search order record
     *  @param directoryRecordType directory record type
     *  @throws org.osid.NullArgumentException
     *          <code>directoryQueryRecord</code>,
     *          <code>directoryQueryInspectorRecord</code>,
     *          <code>directorySearchOrderRecord</code> or
     *          <code>directoryRecordTypedirectory</code> is
     *          <code>null</code>
     */
            
    protected void addDirectoryRecords(org.osid.filing.records.DirectoryQueryRecord directoryQueryRecord, 
                                      org.osid.filing.records.DirectoryQueryInspectorRecord directoryQueryInspectorRecord, 
                                      org.osid.filing.records.DirectorySearchOrderRecord directorySearchOrderRecord, 
                                      org.osid.type.Type directoryRecordType) {

        addRecordType(directoryRecordType);

        nullarg(directoryQueryRecord, "directory query record");
        nullarg(directoryQueryInspectorRecord, "directory query inspector record");
        nullarg(directorySearchOrderRecord, "directory search odrer record");

        this.queryRecords.add(directoryQueryRecord);
        this.queryInspectorRecords.add(directoryQueryInspectorRecord);
        this.searchOrderRecords.add(directorySearchOrderRecord);
        
        return;
    }


    protected class AssemblyOsidExtensibleQuery
        extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidExtensibleQuery
        implements org.osid.OsidExtensibleQuery,
                   org.osid.OsidExtensibleQueryInspector,
                   org.osid.OsidExtensibleSearchOrder {
        
        
        /** 
         *  Constructs a new
         *  <code>AssemblyOsidExtensibleQuery</code>.
         *
         *  @param assembler the query assembler
         *  @throws org.osid.NullArgumentException <code>assembler</code>
         *          is <code>null</code>
         */
        
        protected AssemblyOsidExtensibleQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
            super(assembler);
            return;
        }


        /**
         *  Adds a record type.
         *
         *  @param recordType
         *  @throws org.osid.NullArgumentException <code>recordType</code>
         *          is <code>null</code>
         */

        @Override
        protected void addRecordType(org.osid.type.Type recordType) {
            super.addRecordType(recordType);
            return;
        }
        
            
        /**
         *  Gets the column name for the record type.
         *
         *  @return the column name
         */
        
        @Override
        protected String getRecordTypeColumn() {
            return (super.getRecordTypeColumn());
        }    
    }
}
