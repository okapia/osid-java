//
// InventoryElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inventory.inventory.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class InventoryElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the InventoryElement Id.
     *
     *  @return the inventory element Id
     */

    public static org.osid.id.Id getInventoryEntityId() {
        return (makeEntityId("osid.inventory.Inventory"));
    }


    /**
     *  Gets the StockId element Id.
     *
     *  @return the StockId element Id
     */

    public static org.osid.id.Id getStockId() {
        return (makeElementId("osid.inventory.inventory.StockId"));
    }


    /**
     *  Gets the Stock element Id.
     *
     *  @return the Stock element Id
     */

    public static org.osid.id.Id getStock() {
        return (makeElementId("osid.inventory.inventory.Stock"));
    }


    /**
     *  Gets the Date element Id.
     *
     *  @return the Date element Id
     */

    public static org.osid.id.Id getDate() {
        return (makeElementId("osid.inventory.inventory.Date"));
    }


    /**
     *  Gets the Quantity element Id.
     *
     *  @return the Quantity element Id
     */

    public static org.osid.id.Id getQuantity() {
        return (makeElementId("osid.inventory.inventory.Quantity"));
    }


    /**
     *  Gets the DateInclusive element Id.
     *
     *  @return the DateInclusive element Id
     */

    public static org.osid.id.Id getDateInclusive() {
        return (makeQueryElementId("osid.inventory.inventory.DateInclusive"));
    }


    /**
     *  Gets the WarehouseId element Id.
     *
     *  @return the WarehouseId element Id
     */

    public static org.osid.id.Id getWarehouseId() {
        return (makeQueryElementId("osid.inventory.inventory.WarehouseId"));
    }


    /**
     *  Gets the Warehouse element Id.
     *
     *  @return the Warehouse element Id
     */

    public static org.osid.id.Id getWarehouse() {
        return (makeQueryElementId("osid.inventory.inventory.Warehouse"));
    }
}
