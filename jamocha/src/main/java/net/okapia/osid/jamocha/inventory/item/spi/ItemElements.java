//
// ItemElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inventory.item.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class ItemElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the ItemElement Id.
     *
     *  @return the item element Id
     */

    public static org.osid.id.Id getItemEntityId() {
        return (makeEntityId("osid.inventory.Item"));
    }


    /**
     *  Gets the StockId element Id.
     *
     *  @return the StockId element Id
     */

    public static org.osid.id.Id getStockId() {
        return (makeElementId("osid.inventory.item.StockId"));
    }


    /**
     *  Gets the Stock element Id.
     *
     *  @return the Stock element Id
     */

    public static org.osid.id.Id getStock() {
        return (makeElementId("osid.inventory.item.Stock"));
    }


    /**
     *  Gets the PropertyTag element Id.
     *
     *  @return the PropertyTag element Id
     */

    public static org.osid.id.Id getPropertyTag() {
        return (makeElementId("osid.inventory.item.PropertyTag"));
    }


    /**
     *  Gets the SerialNumber element Id.
     *
     *  @return the SerialNumber element Id
     */

    public static org.osid.id.Id getSerialNumber() {
        return (makeElementId("osid.inventory.item.SerialNumber"));
    }


    /**
     *  Gets the LocationDescription element Id.
     *
     *  @return the LocationDescription element Id
     */

    public static org.osid.id.Id getLocationDescription() {
        return (makeElementId("osid.inventory.item.LocationDescription"));
    }


    /**
     *  Gets the LocationId element Id.
     *
     *  @return the LocationId element Id
     */

    public static org.osid.id.Id getLocationId() {
        return (makeElementId("osid.inventory.item.LocationId"));
    }


    /**
     *  Gets the Location element Id.
     *
     *  @return the Location element Id
     */

    public static org.osid.id.Id getLocation() {
        return (makeElementId("osid.inventory.item.Location"));
    }


    /**
     *  Gets the ItemId element Id.
     *
     *  @return the ItemId element Id
     */

    public static org.osid.id.Id getItemId() {
        return (makeElementId("osid.inventory.item.ItemId"));
    }


    /**
     *  Gets the Item element Id.
     *
     *  @return the Item element Id
     */

    public static org.osid.id.Id getItem() {
        return (makeElementId("osid.inventory.item.Item"));
    }


    /**
     *  Gets the WarehouseId element Id.
     *
     *  @return the WarehouseId element Id
     */

    public static org.osid.id.Id getWarehouseId() {
        return (makeQueryElementId("osid.inventory.item.WarehouseId"));
    }


    /**
     *  Gets the Warehouse element Id.
     *
     *  @return the Warehouse element Id
     */

    public static org.osid.id.Id getWarehouse() {
        return (makeQueryElementId("osid.inventory.item.Warehouse"));
    }
}
