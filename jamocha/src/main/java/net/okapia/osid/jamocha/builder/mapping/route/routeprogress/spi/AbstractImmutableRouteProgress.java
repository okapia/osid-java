//
// AbstractImmutableRouteProgress.java
//
//     Wraps a mutable RouteProgress to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.mapping.route.routeprogress.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>RouteProgress</code> to hide modifiers. This
 *  wrapper provides an immutized RouteProgress from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying routeProgress whose state changes are visible.
 */

public abstract class AbstractImmutableRouteProgress
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidCompendium
    implements org.osid.mapping.route.RouteProgress {

    private final org.osid.mapping.route.RouteProgress routeProgress;


    /**
     *  Constructs a new <code>AbstractImmutableRouteProgress</code>.
     *
     *  @param routeProgress the route progress to immutablize
     *  @throws org.osid.NullArgumentException <code>routeProgress</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableRouteProgress(org.osid.mapping.route.RouteProgress routeProgress) {
        super(routeProgress);
        this.routeProgress = routeProgress;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the resource on the route. 
     *
     *  @return the resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getResourceId() {
        return (this.routeProgress.getResourceId());
    }


    /**
     *  Gets the resource on the route. 
     *
     *  @return the resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getResource()
        throws org.osid.OperationFailedException {

        return (this.routeProgress.getResource());
    }


    /**
     *  Gets the route <code> Id. </code> 
     *
     *  @return the route <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getRouteId() {
        return (this.routeProgress.getRouteId());
    }


    /**
     *  Gets the route. 
     *
     *  @return the route 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.mapping.route.Route getRoute()
        throws org.osid.OperationFailedException {

        return (this.routeProgress.getRoute());
    }


    /**
     *  Gets the starting time on this route. 
     *
     *  @return the starting time 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getTimeStarted() {
        return (this.routeProgress.getTimeStarted());
    }


    /**
     *  Gets the total distance traveled. 
     *
     *  @return the distance 
     */

    @OSID @Override
    public org.osid.mapping.Distance getTotalDistanceTraveled() {
        return (this.routeProgress.getTotalDistanceTraveled());
    }


    /**
     *  Gets the total travel time. 
     *
     *  @return total travel time 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getTotalTravelTime() {
        return (this.routeProgress.getTotalTravelTime());
    }


    /**
     *  Tests if the object is in motion. 
     *
     *  @return <code> true </code> if the object is in motion, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isInMotion() {
        return (this.routeProgress.isInMotion());
    }


    /**
     *  Gets the total idle time before completion of the route. 
     *
     *  @return total idle time 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getTotalIdleTime() {
        return (this.routeProgress.getTotalIdleTime());
    }


    /**
     *  Get sthe time the object last moved. 
     *
     *  @return last moved time 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getTimeLastMoved() {
        return (this.routeProgress.getTimeLastMoved());
    }


    /**
     *  Tests if the route has been completed. 
     *
     *  @return <code> true </code> if the route has been completed, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean isComplete() {
        return (this.routeProgress.isComplete());
    }


    /**
     *  Gets the current route segment <code> Id. </code> 
     *
     *  @return the route segment <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isComplete() </code> is 
     *          <code> true </code> 
     */

    @OSID @Override
    public org.osid.id.Id getRouteSegmentId() {
        return (this.routeProgress.getRouteSegmentId());
    }


    /**
     *  Gets the current route segment. 
     *
     *  @return the route segment 
     *  @throws org.osid.IllegalStateException <code> isComplete() </code> is 
     *          <code> true </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteSegment getRouteSegment()
        throws org.osid.OperationFailedException {

        return (this.routeProgress.getRouteSegment());
    }


    /**
     *  Gets the estimated travel time to the next route segment. 
     *
     *  @return the estimated travel time 
     *  @throws org.osid.IllegalStateException <code> isComplete() </code> is 
     *          <code> true </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getETAToNextSegment() {
        return (this.routeProgress.getETAToNextSegment());
    }


    /**
     *  Gets the distance along the current route segment traveled. 
     *
     *  @return distance along the current segment traveled 
     *  @throws org.osid.IllegalStateException <code> isComplete() </code> is 
     *          <code> true </code> 
     */

    @OSID @Override
    public org.osid.mapping.Distance getRouteSegmentTraveled() {
        return (this.routeProgress.getRouteSegmentTraveled());
    }


    /**
     *  Gets the ending time for this route. 
     *
     *  @return the ending time 
     *  @throws org.osid.IllegalStateException <code> isComplete() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getTimeCompleted() {
        return (this.routeProgress.getTimeCompleted());
    }


    /**
     *  Gets the route progress record corresponding to the given <code> 
     *  RouteProgresst </code> record <code> Type. </code> 
     *
     *  @param  routeProgressRecordType a route progress record type 
     *  @return the route progress record 
     *  @throws org.osid.NullArgumentException <code> progressRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasDomainType(progressSegmentRecordType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.records.RouteProgressRecord getRouteProgressRecord(org.osid.type.Type routeProgressRecordType)
        throws org.osid.OperationFailedException {

        return (this.routeProgress.getRouteProgressRecord(routeProgressRecordType));
    }
}

