//
// AbstractImmutableMeetingTime.java
//
//     Wraps a mutable MeetingTime to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.calendaring.meetingtime.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>MeetingTime</code> to hide modifiers. This
 *  wrapper provides an immutized MeetingTime from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying meetingTime whose state changes are visible.
 */

public abstract class AbstractImmutableMeetingTime
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutable
    implements org.osid.calendaring.MeetingTime {

    private final org.osid.calendaring.MeetingTime meetingTime;


    /**
     *  Constructs a new <code>AbstractImmutableMeetingTime</code>.
     *
     *  @param meetingTime the recurring event to immutablize
     *  @throws org.osid.NullArgumentException <code>meetingTime</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableMeetingTime(org.osid.calendaring.MeetingTime meetingTime) {
        super(meetingTime);
        this.meetingTime = meetingTime;
        return;
    }


    /**
     *  Gets the date. 
     *
     *  @return the date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getDate() {
        return (this.meetingTime.getDate());
    }


    /**
     *  Gets a descriptive text for the location. 
     *
     *  @return a location description 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getLocationDescription() {
        return (this.meetingTime.getLocationDescription());
    }


    /**
     *  Tests if a location is available. 
     *
     *  @return <code> true </code> if a location is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasLocation() {
        return (this.meetingTime.hasLocation());
    }


    /**
     *  Gets the location <code>Id</code>. 
     *
     *  @return a location <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasLocation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getLocationId() {
        return (this.meetingTime.getLocationId());
    }


    /**
     *  Gets the <code>Location</code>.
     *
     *  @return a location 
     *  @throws org.osid.IllegalStateException <code> hasLocation() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.mapping.Location getLocation()
        throws org.osid.OperationFailedException {

        return (this.meetingTime.getLocation());
    }
}

