//
// AbstractChecklist.java
//
//     Defines a Checklist.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.checklist.checklist.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Checklist</code>.
 */

public abstract class AbstractChecklist
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalog
    implements org.osid.checklist.Checklist {

    private final java.util.Collection<org.osid.checklist.records.ChecklistRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Tests if this checklist supports the given record
     *  <code>Type</code>.
     *
     *  @param  checklistRecordType a checklist record type 
     *  @return <code>true</code> if the checklistRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>checklistRecordType</code> is <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type checklistRecordType) {
        for (org.osid.checklist.records.ChecklistRecord record : this.records) {
            if (record.implementsRecordType(checklistRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Checklist</code> record <code>Type</code>.
     *
     *  @param  checklistRecordType the checklist record type 
     *  @return the checklist record 
     *  @throws org.osid.NullArgumentException
     *          <code>checklistRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(checklistRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.checklist.records.ChecklistRecord getChecklistRecord(org.osid.type.Type checklistRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.checklist.records.ChecklistRecord record : this.records) {
            if (record.implementsRecordType(checklistRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(checklistRecordType + " is not supported");
    }


    /**
     *  Adds a record to this checklist. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param checklistRecord the checklist record
     *  @param checklistRecordType checklist record type
     *  @throws org.osid.NullArgumentException
     *          <code>checklistRecord</code> or
     *          <code>checklistRecordType</code> is <code>null</code>
     */
            
    protected void addChecklistRecord(org.osid.checklist.records.ChecklistRecord checklistRecord, 
                                      org.osid.type.Type checklistRecordType) {

        nullarg(checklistRecord, "checklist record");
        addRecordType(checklistRecordType);
        this.records.add(checklistRecord);
        
        return;
    }
}
