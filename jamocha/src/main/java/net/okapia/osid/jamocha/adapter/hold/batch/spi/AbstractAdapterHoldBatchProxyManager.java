//
// AbstractHoldBatchProxyManager.java
//
//     An adapter for a HoldBatchProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.hold.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a HoldBatchProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterHoldBatchProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.hold.batch.HoldBatchProxyManager>
    implements org.osid.hold.batch.HoldBatchProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterHoldBatchProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterHoldBatchProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterHoldBatchProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterHoldBatchProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if bulk administration of blocks is available. 
     *
     *  @return <code> true </code> if a block bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBlockBatchAdmin() {
        return (getAdapteeManager().supportsBlockBatchAdmin());
    }


    /**
     *  Tests if bulk administration of issues is available. 
     *
     *  @return <code> true </code> if an issue bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueBatchAdmin() {
        return (getAdapteeManager().supportsIssueBatchAdmin());
    }


    /**
     *  Tests if bulk administration of holds is available. 
     *
     *  @return <code> true </code> if a hold bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldBatchAdmin() {
        return (getAdapteeManager().supportsHoldBatchAdmin());
    }


    /**
     *  Tests if bulk administration of oubliettes is available. 
     *
     *  @return <code> true </code> if a oubliette bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOublietteBatchAdmin() {
        return (getAdapteeManager().supportsOublietteBatchAdmin());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk block 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BlockBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBlockBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.batch.BlockBatchAdminSession getBlockBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBlockBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk block 
     *  administration service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BlockBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBlockBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.batch.BlockBatchAdminSession getBlockBatchAdminSessionForOubliette(org.osid.id.Id oublietteId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBlockBatchAdminSessionForOubliette(oublietteId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk issue 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> IssueBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIssueBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.batch.IssueBatchAdminSession getIssueBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getIssueBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk issue 
     *  administration service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> IssueBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIssueBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.batch.IssueBatchAdminSession getIssueBatchAdminSessionForOubliette(org.osid.id.Id oublietteId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getIssueBatchAdminSessionForOubliette(oublietteId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk hold 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> HoldBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.batch.HoldBatchAdminSession getHoldBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getHoldBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk hold 
     *  administration service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> HoldBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.batch.HoldBatchAdminSession getHoldBatchAdminSessionForOubliette(org.osid.id.Id oublietteId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getHoldBatchAdminSessionForOubliette(oublietteId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk oubliette 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> OublietteBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOublietteBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.batch.OublietteBatchAdminSession getOublietteBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOublietteBatchAdminSession(proxy));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
