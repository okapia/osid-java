//
// AbstractIndexedMapAuditEnablerLookupSession.java
//
//    A simple framework for providing an AuditEnabler lookup service
//    backed by a fixed collection of audit enablers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.inquiry.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an AuditEnabler lookup service backed by a
 *  fixed collection of audit enablers. The audit enablers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some audit enablers may be compatible
 *  with more types than are indicated through these audit enabler
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>AuditEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapAuditEnablerLookupSession
    extends AbstractMapAuditEnablerLookupSession
    implements org.osid.inquiry.rules.AuditEnablerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.inquiry.rules.AuditEnabler> auditEnablersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.inquiry.rules.AuditEnabler>());
    private final MultiMap<org.osid.type.Type, org.osid.inquiry.rules.AuditEnabler> auditEnablersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.inquiry.rules.AuditEnabler>());


    /**
     *  Makes an <code>AuditEnabler</code> available in this session.
     *
     *  @param  auditEnabler an audit enabler
     *  @throws org.osid.NullArgumentException <code>auditEnabler<code> is
     *          <code>null</code>
     */

    @Override
    protected void putAuditEnabler(org.osid.inquiry.rules.AuditEnabler auditEnabler) {
        super.putAuditEnabler(auditEnabler);

        this.auditEnablersByGenus.put(auditEnabler.getGenusType(), auditEnabler);
        
        try (org.osid.type.TypeList types = auditEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.auditEnablersByRecord.put(types.getNextType(), auditEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }

    
    /**
     *  Makes an array of audit enablers available in this session.
     *
     *  @param  auditEnablers an array of audit enablers
     *  @throws org.osid.NullArgumentException <code>auditEnablers<code>
     *          is <code>null</code>
     */

    @Override
    protected void putAuditEnablers(org.osid.inquiry.rules.AuditEnabler[] auditEnablers) {
        for (org.osid.inquiry.rules.AuditEnabler auditEnabler : auditEnablers) {
            putAuditEnabler(auditEnabler);
        }

        return;
    }


    /**
     *  Makes a collection of audit enablers available in this session.
     *
     *  @param  auditEnablers a collection of audit enablers
     *  @throws org.osid.NullArgumentException <code>auditEnablers<code>
     *          is <code>null</code>
     */

    @Override
    protected void putAuditEnablers(java.util.Collection<? extends org.osid.inquiry.rules.AuditEnabler> auditEnablers) {
        for (org.osid.inquiry.rules.AuditEnabler auditEnabler : auditEnablers) {
            putAuditEnabler(auditEnabler);
        }

        return;
    }


    /**
     *  Removes an audit enabler from this session.
     *
     *  @param auditEnablerId the <code>Id</code> of the audit enabler
     *  @throws org.osid.NullArgumentException <code>auditEnablerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeAuditEnabler(org.osid.id.Id auditEnablerId) {
        org.osid.inquiry.rules.AuditEnabler auditEnabler;
        try {
            auditEnabler = getAuditEnabler(auditEnablerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.auditEnablersByGenus.remove(auditEnabler.getGenusType());

        try (org.osid.type.TypeList types = auditEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.auditEnablersByRecord.remove(types.getNextType(), auditEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeAuditEnabler(auditEnablerId);
        return;
    }


    /**
     *  Gets an <code>AuditEnablerList</code> corresponding to the given
     *  audit enabler genus <code>Type</code> which does not include
     *  audit enablers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known audit enablers or an error results. Otherwise,
     *  the returned list may contain only those audit enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  auditEnablerGenusType an audit enabler genus type 
     *  @return the returned <code>AuditEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auditEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerList getAuditEnablersByGenusType(org.osid.type.Type auditEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inquiry.rules.auditenabler.ArrayAuditEnablerList(this.auditEnablersByGenus.get(auditEnablerGenusType)));
    }


    /**
     *  Gets an <code>AuditEnablerList</code> containing the given
     *  audit enabler record <code>Type</code>. In plenary mode, the
     *  returned list contains all known audit enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  audit enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  auditEnablerRecordType an audit enabler record type 
     *  @return the returned <code>auditEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auditEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerList getAuditEnablersByRecordType(org.osid.type.Type auditEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inquiry.rules.auditenabler.ArrayAuditEnablerList(this.auditEnablersByRecord.get(auditEnablerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.auditEnablersByGenus.clear();
        this.auditEnablersByRecord.clear();

        super.close();

        return;
    }
}
