//
// AbstractAssemblyCyclicTimePeriodQuery.java
//
//     A CyclicTimePeriodQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.calendaring.cycle.cyclictimeperiod.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A CyclicTimePeriodQuery that stores terms.
 */

public abstract class AbstractAssemblyCyclicTimePeriodQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.calendaring.cycle.CyclicTimePeriodQuery,
               org.osid.calendaring.cycle.CyclicTimePeriodQueryInspector,
               org.osid.calendaring.cycle.CyclicTimePeriodSearchOrder {

    private final java.util.Collection<org.osid.calendaring.cycle.records.CyclicTimePeriodQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.calendaring.cycle.records.CyclicTimePeriodQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.calendaring.cycle.records.CyclicTimePeriodSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyCyclicTimePeriodQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyCyclicTimePeriodQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the time period <code> Id </code> for this query to match cyclic 
     *  time periods with the given mapped time period. 
     *
     *  @param  timePeriodId a tiem period <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> timePeriodId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchTimePeriodId(org.osid.id.Id timePeriodId, boolean match) {
        getAssembler().addIdTerm(getTimePeriodIdColumn(), timePeriodId, match);
        return;
    }


    /**
     *  Clears the cyclic time period <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearTimePeriodIdTerms() {
        getAssembler().clearTerms(getTimePeriodIdColumn());
        return;
    }


    /**
     *  Gets the time period <code> Id </code> terms. 
     *
     *  @return the time period <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getTimePeriodIdTerms() {
        return (getAssembler().getIdTerms(getTimePeriodIdColumn()));
    }


    /**
     *  Gets the TimePeriodId column name.
     *
     * @return the column name
     */

    protected String getTimePeriodIdColumn() {
        return ("time_period_id");
    }


    /**
     *  Tests if a <code> TimePeriodQuery </code> is available for querying 
     *  time periods. 
     *
     *  @return <code> true </code> if a time period query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTimePeriodQuery() {
        return (false);
    }


    /**
     *  Gets the query for a time period. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the time period query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodQuery getTimePeriodQuery() {
        throw new org.osid.UnimplementedException("supportsTimePeriodQuery() is false");
    }


    /**
     *  Matches a cyclic time period that has any time period assigned. 
     *
     *  @param  match <code> true </code> to match cyclic time periods with 
     *          any tiem period, <code> false </code> to match cyclic time 
     *          periods with no tiem periods 
     */

    @OSID @Override
    public void matchAnyTimePeriod(boolean match) {
        getAssembler().addIdWildcardTerm(getTimePeriodColumn(), match);
        return;
    }


    /**
     *  Clears the time period terms. 
     */

    @OSID @Override
    public void clearTimePeriodTerms() {
        getAssembler().clearTerms(getTimePeriodColumn());
        return;
    }


    /**
     *  Gets the time period terms. 
     *
     *  @return the time period terms 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodQueryInspector[] getTimePeriodTerms() {
        return (new org.osid.calendaring.TimePeriodQueryInspector[0]);
    }


    /**
     *  Gets the TimePeriod column name.
     *
     * @return the column name
     */

    protected String getTimePeriodColumn() {
        return ("time_period");
    }


    /**
     *  Sets the calendar <code> Id </code> for this query. 
     *
     *  @param  calendarId a calendar <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCalendarId(org.osid.id.Id calendarId, boolean match) {
        getAssembler().addIdTerm(getCalendarIdColumn(), calendarId, match);
        return;
    }


    /**
     *  Clears the calendar <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCalendarIdTerms() {
        getAssembler().clearTerms(getCalendarIdColumn());
        return;
    }


    /**
     *  Gets the calendar <code> Id </code> terms. 
     *
     *  @return the calendar <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCalendarIdTerms() {
        return (getAssembler().getIdTerms(getCalendarIdColumn()));
    }


    /**
     *  Gets the CalendarId column name.
     *
     * @return the column name
     */

    protected String getCalendarIdColumn() {
        return ("calendar_id");
    }


    /**
     *  Tests if a <code> CalendarQuery </code> is available for querying 
     *  resources. 
     *
     *  @return <code> true </code> if a calendar query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendarQuery() {
        return (false);
    }


    /**
     *  Gets the query for a calendar. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the calendar query 
     *  @throws org.osid.UnimplementedException <code> supportsCalendarQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQuery getCalendarQuery() {
        throw new org.osid.UnimplementedException("supportsCalendarQuery() is false");
    }


    /**
     *  Clears the calendar terms. 
     */

    @OSID @Override
    public void clearCalendarTerms() {
        getAssembler().clearTerms(getCalendarColumn());
        return;
    }


    /**
     *  Gets the calendar terms. 
     *
     *  @return the calendar terms 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQueryInspector[] getCalendarTerms() {
        return (new org.osid.calendaring.CalendarQueryInspector[0]);
    }


    /**
     *  Gets the Calendar column name.
     *
     * @return the column name
     */

    protected String getCalendarColumn() {
        return ("calendar");
    }


    /**
     *  Tests if this cyclicTimePeriod supports the given record
     *  <code>Type</code>.
     *
     *  @param  cyclicTimePeriodRecordType a cyclic time period record type 
     *  @return <code>true</code> if the cyclicTimePeriodRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicTimePeriodRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type cyclicTimePeriodRecordType) {
        for (org.osid.calendaring.cycle.records.CyclicTimePeriodQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(cyclicTimePeriodRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  cyclicTimePeriodRecordType the cyclic time period record type 
     *  @return the cyclic time period query record 
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicTimePeriodRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(cyclicTimePeriodRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.cycle.records.CyclicTimePeriodQueryRecord getCyclicTimePeriodQueryRecord(org.osid.type.Type cyclicTimePeriodRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.cycle.records.CyclicTimePeriodQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(cyclicTimePeriodRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(cyclicTimePeriodRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  cyclicTimePeriodRecordType the cyclic time period record type 
     *  @return the cyclic time period query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicTimePeriodRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(cyclicTimePeriodRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.cycle.records.CyclicTimePeriodQueryInspectorRecord getCyclicTimePeriodQueryInspectorRecord(org.osid.type.Type cyclicTimePeriodRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.cycle.records.CyclicTimePeriodQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(cyclicTimePeriodRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(cyclicTimePeriodRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param cyclicTimePeriodRecordType the cyclic time period record type
     *  @return the cyclic time period search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicTimePeriodRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(cyclicTimePeriodRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.cycle.records.CyclicTimePeriodSearchOrderRecord getCyclicTimePeriodSearchOrderRecord(org.osid.type.Type cyclicTimePeriodRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.cycle.records.CyclicTimePeriodSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(cyclicTimePeriodRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(cyclicTimePeriodRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this cyclic time period. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param cyclicTimePeriodQueryRecord the cyclic time period query record
     *  @param cyclicTimePeriodQueryInspectorRecord the cyclic time period query inspector
     *         record
     *  @param cyclicTimePeriodSearchOrderRecord the cyclic time period search order record
     *  @param cyclicTimePeriodRecordType cyclic time period record type
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicTimePeriodQueryRecord</code>,
     *          <code>cyclicTimePeriodQueryInspectorRecord</code>,
     *          <code>cyclicTimePeriodSearchOrderRecord</code> or
     *          <code>cyclicTimePeriodRecordTypecyclicTimePeriod</code> is
     *          <code>null</code>
     */
            
    protected void addCyclicTimePeriodRecords(org.osid.calendaring.cycle.records.CyclicTimePeriodQueryRecord cyclicTimePeriodQueryRecord, 
                                      org.osid.calendaring.cycle.records.CyclicTimePeriodQueryInspectorRecord cyclicTimePeriodQueryInspectorRecord, 
                                      org.osid.calendaring.cycle.records.CyclicTimePeriodSearchOrderRecord cyclicTimePeriodSearchOrderRecord, 
                                      org.osid.type.Type cyclicTimePeriodRecordType) {

        addRecordType(cyclicTimePeriodRecordType);

        nullarg(cyclicTimePeriodQueryRecord, "cyclic time period query record");
        nullarg(cyclicTimePeriodQueryInspectorRecord, "cyclic time period query inspector record");
        nullarg(cyclicTimePeriodSearchOrderRecord, "cyclic time period search odrer record");

        this.queryRecords.add(cyclicTimePeriodQueryRecord);
        this.queryInspectorRecords.add(cyclicTimePeriodQueryInspectorRecord);
        this.searchOrderRecords.add(cyclicTimePeriodSearchOrderRecord);
        
        return;
    }
}
