//
// InvariantMapPostEntryLookupSession
//
//    Implements a PostEntry lookup service backed by a fixed collection of
//    postEntries.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.financials.posting;


/**
 *  Implements a PostEntry lookup service backed by a fixed
 *  collection of post entries. The post entries are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapPostEntryLookupSession
    extends net.okapia.osid.jamocha.core.financials.posting.spi.AbstractMapPostEntryLookupSession
    implements org.osid.financials.posting.PostEntryLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapPostEntryLookupSession</code> with no
     *  post entries.
     *  
     *  @param business the business
     *  @throws org.osid.NullArgumnetException {@code business} is
     *          {@code null}
     */

    public InvariantMapPostEntryLookupSession(org.osid.financials.Business business) {
        setBusiness(business);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapPostEntryLookupSession</code> with a single
     *  post entry.
     *  
     *  @param business the business
     *  @param postEntry a single post entry
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code postEntry} is <code>null</code>
     */

      public InvariantMapPostEntryLookupSession(org.osid.financials.Business business,
                                               org.osid.financials.posting.PostEntry postEntry) {
        this(business);
        putPostEntry(postEntry);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapPostEntryLookupSession</code> using an array
     *  of post entries.
     *  
     *  @param business the business
     *  @param postEntries an array of post entries
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code postEntries} is <code>null</code>
     */

      public InvariantMapPostEntryLookupSession(org.osid.financials.Business business,
                                               org.osid.financials.posting.PostEntry[] postEntries) {
        this(business);
        putPostEntries(postEntries);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapPostEntryLookupSession</code> using a
     *  collection of post entries.
     *
     *  @param business the business
     *  @param postEntries a collection of post entries
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code postEntries} is <code>null</code>
     */

      public InvariantMapPostEntryLookupSession(org.osid.financials.Business business,
                                               java.util.Collection<? extends org.osid.financials.posting.PostEntry> postEntries) {
        this(business);
        putPostEntries(postEntries);
        return;
    }
}
