//
// JournalElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.journaling.journal.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class JournalElements
    extends net.okapia.osid.jamocha.spi.OsidCatalogElements {


    /**
     *  Gets the JournalElement Id.
     *
     *  @return the journal element Id
     */

    public static org.osid.id.Id getJournalEntityId() {
        return (makeEntityId("osid.journaling.Journal"));
    }


    /**
     *  Gets the JournalEntryId element Id.
     *
     *  @return the JournalEntryId element Id
     */

    public static org.osid.id.Id getJournalEntryId() {
        return (makeQueryElementId("osid.journaling.journal.JournalEntryId"));
    }


    /**
     *  Gets the JournalEntry element Id.
     *
     *  @return the JournalEntry element Id
     */

    public static org.osid.id.Id getJournalEntry() {
        return (makeQueryElementId("osid.journaling.journal.JournalEntry"));
    }


    /**
     *  Gets the BranchId element Id.
     *
     *  @return the BranchId element Id
     */

    public static org.osid.id.Id getBranchId() {
        return (makeQueryElementId("osid.journaling.journal.BranchId"));
    }


    /**
     *  Gets the Branch element Id.
     *
     *  @return the Branch element Id
     */

    public static org.osid.id.Id getBranch() {
        return (makeQueryElementId("osid.journaling.journal.Branch"));
    }


    /**
     *  Gets the AncestorJournalId element Id.
     *
     *  @return the AncestorJournalId element Id
     */

    public static org.osid.id.Id getAncestorJournalId() {
        return (makeQueryElementId("osid.journaling.journal.AncestorJournalId"));
    }


    /**
     *  Gets the AncestorJournal element Id.
     *
     *  @return the AncestorJournal element Id
     */

    public static org.osid.id.Id getAncestorJournal() {
        return (makeQueryElementId("osid.journaling.journal.AncestorJournal"));
    }


    /**
     *  Gets the DescendantJournalId element Id.
     *
     *  @return the DescendantJournalId element Id
     */

    public static org.osid.id.Id getDescendantJournalId() {
        return (makeQueryElementId("osid.journaling.journal.DescendantJournalId"));
    }


    /**
     *  Gets the DescendantJournal element Id.
     *
     *  @return the DescendantJournal element Id
     */

    public static org.osid.id.Id getDescendantJournal() {
        return (makeQueryElementId("osid.journaling.journal.DescendantJournal"));
    }
}
