//
// AbstractReading.java
//
//     Defines a Reading builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.metering.reading.spi;


/**
 *  Defines a <code>Reading</code> builder.
 */

public abstract class AbstractReadingBuilder<T extends AbstractReadingBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.metering.reading.ReadingMiter reading;


    /**
     *  Constructs a new <code>AbstractReadingBuilder</code>.
     *
     *  @param reading the reading to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractReadingBuilder(net.okapia.osid.jamocha.builder.metering.reading.ReadingMiter reading) {
        this.reading = reading;
        return;
    }


    /**
     *  Builds the reading.
     *
     *  @return the new reading
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.metering.Reading build() {
        (new net.okapia.osid.jamocha.builder.validator.metering.reading.ReadingValidator(getValidations())).validate(this.reading);
        return (new net.okapia.osid.jamocha.builder.metering.reading.ImmutableReading(this.reading));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the reading miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.metering.reading.ReadingMiter getMiter() {
        return (this.reading);
    }


    /**
     *  Sets the meter.
     *
     *  @param meter the meter
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>meter</code> is
     *          <code>null</code>
     */

    public T meter(org.osid.metering.Meter meter) {
        getMiter().setMeter(meter);
        return(self());
    }


    /**
     *  Sets the metered object Id.
     *
     *  @param objectId the metered object Id
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>object</code> is
     *          <code>null</code>
     */

    public T meteredObjectId(org.osid.id.Id objectId) {
        getMiter().setMeteredObjectId(objectId);
        return(self());
    }
    

    /**
     *  Sets the amount.
     *
     *  @param amount the amount
     *  @return  the builder
     *  @throws org.osid.NullArgumentException <code>amount</code> is
     *          <code>null</code>
     */

    public T amount(java.math.BigDecimal amount) {
        getMiter().setAmount(amount);
        return(self());
    }
}





