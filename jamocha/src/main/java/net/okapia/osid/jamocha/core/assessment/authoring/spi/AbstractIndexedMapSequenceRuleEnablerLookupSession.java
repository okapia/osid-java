//
// AbstractIndexedMapSequenceRuleEnablerLookupSession.java
//
//    A simple framework for providing a SequenceRuleEnabler lookup service
//    backed by a fixed collection of sequence rule enablers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.assessment.authoring.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a SequenceRuleEnabler lookup service backed by a
 *  fixed collection of sequence rule enablers. The sequence rule enablers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some sequence rule enablers may be compatible
 *  with more types than are indicated through these sequence rule enabler
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>SequenceRuleEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapSequenceRuleEnablerLookupSession
    extends AbstractMapSequenceRuleEnablerLookupSession
    implements org.osid.assessment.authoring.SequenceRuleEnablerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.assessment.authoring.SequenceRuleEnabler> sequenceRuleEnablersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.assessment.authoring.SequenceRuleEnabler>());
    private final MultiMap<org.osid.type.Type, org.osid.assessment.authoring.SequenceRuleEnabler> sequenceRuleEnablersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.assessment.authoring.SequenceRuleEnabler>());


    /**
     *  Makes a <code>SequenceRuleEnabler</code> available in this session.
     *
     *  @param  sequenceRuleEnabler a sequence rule enabler
     *  @throws org.osid.NullArgumentException <code>sequenceRuleEnabler<code> is
     *          <code>null</code>
     */

    @Override
    protected void putSequenceRuleEnabler(org.osid.assessment.authoring.SequenceRuleEnabler sequenceRuleEnabler) {
        super.putSequenceRuleEnabler(sequenceRuleEnabler);

        this.sequenceRuleEnablersByGenus.put(sequenceRuleEnabler.getGenusType(), sequenceRuleEnabler);
        
        try (org.osid.type.TypeList types = sequenceRuleEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.sequenceRuleEnablersByRecord.put(types.getNextType(), sequenceRuleEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a sequence rule enabler from this session.
     *
     *  @param sequenceRuleEnablerId the <code>Id</code> of the sequence rule enabler
     *  @throws org.osid.NullArgumentException <code>sequenceRuleEnablerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeSequenceRuleEnabler(org.osid.id.Id sequenceRuleEnablerId) {
        org.osid.assessment.authoring.SequenceRuleEnabler sequenceRuleEnabler;
        try {
            sequenceRuleEnabler = getSequenceRuleEnabler(sequenceRuleEnablerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.sequenceRuleEnablersByGenus.remove(sequenceRuleEnabler.getGenusType());

        try (org.osid.type.TypeList types = sequenceRuleEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.sequenceRuleEnablersByRecord.remove(types.getNextType(), sequenceRuleEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeSequenceRuleEnabler(sequenceRuleEnablerId);
        return;
    }


    /**
     *  Gets a <code>SequenceRuleEnablerList</code> corresponding to the given
     *  sequence rule enabler genus <code>Type</code> which does not include
     *  sequence rule enablers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known sequence rule enablers or an error results. Otherwise,
     *  the returned list may contain only those sequence rule enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  sequenceRuleEnablerGenusType a sequence rule enabler genus type 
     *  @return the returned <code>SequenceRuleEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>sequenceRuleEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerList getSequenceRuleEnablersByGenusType(org.osid.type.Type sequenceRuleEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.assessment.authoring.sequenceruleenabler.ArraySequenceRuleEnablerList(this.sequenceRuleEnablersByGenus.get(sequenceRuleEnablerGenusType)));
    }


    /**
     *  Gets a <code>SequenceRuleEnablerList</code> containing the given
     *  sequence rule enabler record <code>Type</code>. In plenary mode, the
     *  returned list contains all known sequence rule enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  sequence rule enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  sequenceRuleEnablerRecordType a sequence rule enabler record type 
     *  @return the returned <code>sequenceRuleEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>sequenceRuleEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerList getSequenceRuleEnablersByRecordType(org.osid.type.Type sequenceRuleEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.assessment.authoring.sequenceruleenabler.ArraySequenceRuleEnablerList(this.sequenceRuleEnablersByRecord.get(sequenceRuleEnablerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.sequenceRuleEnablersByGenus.clear();
        this.sequenceRuleEnablersByRecord.clear();

        super.close();

        return;
    }
}
