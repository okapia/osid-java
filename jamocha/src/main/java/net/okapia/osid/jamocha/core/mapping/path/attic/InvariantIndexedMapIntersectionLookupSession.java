//
// InvariantIndexedMapIntersectionLookupSession
//
//    Implements an Intersection lookup service backed by a fixed
//    collection of intersections indexed by their types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping.path;


/**
 *  Implements an Intersection lookup service backed by a fixed
 *  collection of intersections. The intersections are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some intersections may be compatible
 *  with more types than are indicated through these intersection
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 */

public final class InvariantIndexedMapIntersectionLookupSession
    extends net.okapia.osid.jamocha.core.mapping.path.spi.AbstractIndexedMapIntersectionLookupSession
    implements org.osid.mapping.path.IntersectionLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantIndexedMapIntersectionLookupSession</code> using an
     *  array of intersections.
     *
     *  @param  intersections an array of intersections
     *  @throws org.osid.NullArgumentException <code>intersections</code> is
     *          <code>null</code>
     */

    public InvariantIndexedMapIntersectionLookupSession(org.osid.mapping.path.Intersection[] intersections) {
        putIntersections(intersections);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantIndexedMapIntersectionLookupSession</code> using a
     *  collection of intersections.
     *
     *  @param  intersections a collection of intersections
     *  @throws org.osid.NullArgumentException <code>intersections</code> is
     *          <code>null</code>
     */

    public InvariantIndexedMapIntersectionLookupSession(java.util.Collection<? extends org.osid.mapping.path.Intersection> intersections) {
        putIntersections(intersections);
        return;
    }
}
