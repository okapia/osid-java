//
// AbstractImmutableRelationship.java
//
//     Wraps a mutable Relationship to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.relationship.relationship.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Relationship</code> to hide modifiers. This
 *  wrapper provides an immutized Relationship from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying relationship whose state changes are visible.
 */

public abstract class AbstractImmutableRelationship
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRelationship
    implements org.osid.relationship.Relationship {

    private final org.osid.relationship.Relationship relationship;


    /**
     *  Constructs a new <code>AbstractImmutableRelationship</code>.
     *
     *  @param relationship the relationship to immutablize
     *  @throws org.osid.NullArgumentException <code>relationship</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableRelationship(org.osid.relationship.Relationship relationship) {
        super(relationship);
        this.relationship = relationship;
        return;
    }


    /**
     *  Gets the from peer <code> Id </code> in this relationship. 
     *
     *  @return the peer 
     */

    @OSID @Override
    public org.osid.id.Id getSourceId() {
        return (this.relationship.getSourceId());
    }


    /**
     *  Gets the to peer <code> Id </code> in this relationship. 
     *
     *  @return the related peer 
     */

    @OSID @Override
    public org.osid.id.Id getDestinationId() {
        return (this.relationship.getDestinationId());
    }


    /**
     *  Gets the relationshop record corresponding to the given <code> 
     *  Relationship </code> record <code> Type. </code> This method is used 
     *  to retrieve an object implementing the requested record. The <code> 
     *  relationshipRecordType </code> may be the <code> Type </code> returned 
     *  in <code> getRecordTypes() </code> or any of its parents in a <code> 
     *  Type </code> hierarchy where <code> 
     *  hasRecordType(relationshipRecordType) </code> is <code> true </code> . 
     *
     *  @param  relationshipRecordType the type of relationship record to 
     *          retrieve 
     *  @return the relationship record 
     *  @throws org.osid.NullArgumentException <code> relationshipRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(relationshipRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.relationship.records.RelationshipRecord getRelationshipRecord(org.osid.type.Type relationshipRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.relationship.getRelationshipRecord(relationshipRecordType));
    }
}

