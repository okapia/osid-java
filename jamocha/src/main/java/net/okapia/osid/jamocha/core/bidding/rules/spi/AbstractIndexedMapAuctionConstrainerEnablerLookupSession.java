//
// AbstractIndexedMapAuctionConstrainerEnablerLookupSession.java
//
//    A simple framework for providing an AuctionConstrainerEnabler lookup service
//    backed by a fixed collection of auction constrainer enablers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.bidding.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an AuctionConstrainerEnabler lookup service backed by a
 *  fixed collection of auction constrainer enablers. The auction constrainer enablers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some auction constrainer enablers may be compatible
 *  with more types than are indicated through these auction constrainer enabler
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>AuctionConstrainerEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapAuctionConstrainerEnablerLookupSession
    extends AbstractMapAuctionConstrainerEnablerLookupSession
    implements org.osid.bidding.rules.AuctionConstrainerEnablerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.bidding.rules.AuctionConstrainerEnabler> auctionConstrainerEnablersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.bidding.rules.AuctionConstrainerEnabler>());
    private final MultiMap<org.osid.type.Type, org.osid.bidding.rules.AuctionConstrainerEnabler> auctionConstrainerEnablersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.bidding.rules.AuctionConstrainerEnabler>());


    /**
     *  Makes an <code>AuctionConstrainerEnabler</code> available in this session.
     *
     *  @param  auctionConstrainerEnabler an auction constrainer enabler
     *  @throws org.osid.NullArgumentException <code>auctionConstrainerEnabler<code> is
     *          <code>null</code>
     */

    @Override
    protected void putAuctionConstrainerEnabler(org.osid.bidding.rules.AuctionConstrainerEnabler auctionConstrainerEnabler) {
        super.putAuctionConstrainerEnabler(auctionConstrainerEnabler);

        this.auctionConstrainerEnablersByGenus.put(auctionConstrainerEnabler.getGenusType(), auctionConstrainerEnabler);
        
        try (org.osid.type.TypeList types = auctionConstrainerEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.auctionConstrainerEnablersByRecord.put(types.getNextType(), auctionConstrainerEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an auction constrainer enabler from this session.
     *
     *  @param auctionConstrainerEnablerId the <code>Id</code> of the auction constrainer enabler
     *  @throws org.osid.NullArgumentException <code>auctionConstrainerEnablerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeAuctionConstrainerEnabler(org.osid.id.Id auctionConstrainerEnablerId) {
        org.osid.bidding.rules.AuctionConstrainerEnabler auctionConstrainerEnabler;
        try {
            auctionConstrainerEnabler = getAuctionConstrainerEnabler(auctionConstrainerEnablerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.auctionConstrainerEnablersByGenus.remove(auctionConstrainerEnabler.getGenusType());

        try (org.osid.type.TypeList types = auctionConstrainerEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.auctionConstrainerEnablersByRecord.remove(types.getNextType(), auctionConstrainerEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeAuctionConstrainerEnabler(auctionConstrainerEnablerId);
        return;
    }


    /**
     *  Gets an <code>AuctionConstrainerEnablerList</code> corresponding to the given
     *  auction constrainer enabler genus <code>Type</code> which does not include
     *  auction constrainer enablers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known auction constrainer enablers or an error results. Otherwise,
     *  the returned list may contain only those auction constrainer enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  auctionConstrainerEnablerGenusType an auction constrainer enabler genus type 
     *  @return the returned <code>AuctionConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionConstrainerEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerList getAuctionConstrainerEnablersByGenusType(org.osid.type.Type auctionConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.bidding.rules.auctionconstrainerenabler.ArrayAuctionConstrainerEnablerList(this.auctionConstrainerEnablersByGenus.get(auctionConstrainerEnablerGenusType)));
    }


    /**
     *  Gets an <code>AuctionConstrainerEnablerList</code> containing the given
     *  auction constrainer enabler record <code>Type</code>. In plenary mode, the
     *  returned list contains all known auction constrainer enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  auction constrainer enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  auctionConstrainerEnablerRecordType an auction constrainer enabler record type 
     *  @return the returned <code>auctionConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionConstrainerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerList getAuctionConstrainerEnablersByRecordType(org.osid.type.Type auctionConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.bidding.rules.auctionconstrainerenabler.ArrayAuctionConstrainerEnablerList(this.auctionConstrainerEnablersByRecord.get(auctionConstrainerEnablerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.auctionConstrainerEnablersByGenus.clear();
        this.auctionConstrainerEnablersByRecord.clear();

        super.close();

        return;
    }
}
