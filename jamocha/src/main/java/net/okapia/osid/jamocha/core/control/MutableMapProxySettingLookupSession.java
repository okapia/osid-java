//
// MutableMapProxySettingLookupSession
//
//    Implements a Setting lookup service backed by a collection of
//    settings that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.control;


/**
 *  Implements a Setting lookup service backed by a collection of
 *  settings. The settings are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of settings can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxySettingLookupSession
    extends net.okapia.osid.jamocha.core.control.spi.AbstractMapSettingLookupSession
    implements org.osid.control.SettingLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxySettingLookupSession}
     *  with no settings.
     *
     *  @param system the system
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code system} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxySettingLookupSession(org.osid.control.System system,
                                                  org.osid.proxy.Proxy proxy) {
        setSystem(system);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxySettingLookupSession} with a
     *  single setting.
     *
     *  @param system the system
     *  @param setting a setting
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code system},
     *          {@code setting}, or {@code proxy} is {@code null}
     */

    public MutableMapProxySettingLookupSession(org.osid.control.System system,
                                                org.osid.control.Setting setting, org.osid.proxy.Proxy proxy) {
        this(system, proxy);
        putSetting(setting);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxySettingLookupSession} using an
     *  array of settings.
     *
     *  @param system the system
     *  @param settings an array of settings
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code system},
     *          {@code settings}, or {@code proxy} is {@code null}
     */

    public MutableMapProxySettingLookupSession(org.osid.control.System system,
                                                org.osid.control.Setting[] settings, org.osid.proxy.Proxy proxy) {
        this(system, proxy);
        putSettings(settings);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxySettingLookupSession} using a
     *  collection of settings.
     *
     *  @param system the system
     *  @param settings a collection of settings
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code system},
     *          {@code settings}, or {@code proxy} is {@code null}
     */

    public MutableMapProxySettingLookupSession(org.osid.control.System system,
                                                java.util.Collection<? extends org.osid.control.Setting> settings,
                                                org.osid.proxy.Proxy proxy) {
   
        this(system, proxy);
        setSessionProxy(proxy);
        putSettings(settings);
        return;
    }

    
    /**
     *  Makes a {@code Setting} available in this session.
     *
     *  @param setting an setting
     *  @throws org.osid.NullArgumentException {@code setting{@code 
     *          is {@code null}
     */

    @Override
    public void putSetting(org.osid.control.Setting setting) {
        super.putSetting(setting);
        return;
    }


    /**
     *  Makes an array of settings available in this session.
     *
     *  @param settings an array of settings
     *  @throws org.osid.NullArgumentException {@code settings{@code 
     *          is {@code null}
     */

    @Override
    public void putSettings(org.osid.control.Setting[] settings) {
        super.putSettings(settings);
        return;
    }


    /**
     *  Makes collection of settings available in this session.
     *
     *  @param settings
     *  @throws org.osid.NullArgumentException {@code setting{@code 
     *          is {@code null}
     */

    @Override
    public void putSettings(java.util.Collection<? extends org.osid.control.Setting> settings) {
        super.putSettings(settings);
        return;
    }


    /**
     *  Removes a Setting from this session.
     *
     *  @param settingId the {@code Id} of the setting
     *  @throws org.osid.NullArgumentException {@code settingId{@code  is
     *          {@code null}
     */

    @Override
    public void removeSetting(org.osid.id.Id settingId) {
        super.removeSetting(settingId);
        return;
    }    
}
