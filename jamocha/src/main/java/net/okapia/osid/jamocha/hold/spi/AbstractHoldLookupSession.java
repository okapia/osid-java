//
// AbstractHoldLookupSession.java
//
//    A starter implementation framework for providing a Hold
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.hold.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Hold
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getHolds(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractHoldLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.hold.HoldLookupSession {

    private boolean pedantic      = false;
    private boolean effectiveonly = false;
    private boolean federated     = false;
    private org.osid.hold.Oubliette oubliette = new net.okapia.osid.jamocha.nil.hold.oubliette.UnknownOubliette();
    

    /**
     *  Gets the <code>Oubliette/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Oubliette Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getOublietteId() {
        return (this.oubliette.getId());
    }


    /**
     *  Gets the <code>Oubliette</code> associated with this 
     *  session.
     *
     *  @return the <code>Oubliette</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.Oubliette getOubliette()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.oubliette);
    }


    /**
     *  Sets the <code>Oubliette</code>.
     *
     *  @param  oubliette the oubliette for this session
     *  @throws org.osid.NullArgumentException <code>oubliette</code>
     *          is <code>null</code>
     */

    protected void setOubliette(org.osid.hold.Oubliette oubliette) {
        nullarg(oubliette, "oubliette");
        this.oubliette = oubliette;
        return;
    }


    /**
     *  Tests if this user can perform <code>Hold</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupHolds() {
        return (true);
    }


    /**
     *  A complete view of the <code>Hold</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeHoldView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Hold</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryHoldView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include holds in oubliettes which are children
     *  of this oubliette in the oubliette hierarchy.
     */

    @OSID @Override
    public void useFederatedOublietteView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this oubliette only.
     */

    @OSID @Override
    public void useIsolatedOublietteView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only holds whose effective dates are current are returned by
     *  methods in this session.
     */

    @OSID @Override
    public void useEffectiveHoldView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All holds of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveHoldView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Hold</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Hold</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Hold</code> and
     *  retained for compatibility.
     *
     *  In effective mode, holds are returned that are currently
     *  effective.  In any effective mode, effective holds and
     *  those currently expired are returned.
     *
     *  @param  holdId <code>Id</code> of the
     *          <code>Hold</code>
     *  @return the hold
     *  @throws org.osid.NotFoundException <code>holdId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>holdId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.Hold getHold(org.osid.id.Id holdId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.hold.HoldList holds = getHolds()) {
            while (holds.hasNext()) {
                org.osid.hold.Hold hold = holds.getNextHold();
                if (hold.getId().equals(holdId)) {
                    return (hold);
                }
            }
        } 

        throw new org.osid.NotFoundException(holdId + " not found");
    }


    /**
     *  Gets a <code>HoldList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  holds specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Holds</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, holds are returned that are currently effective.
     *  In any effective mode, effective holds and those currently expired
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getHolds()</code>.
     *
     *  @param  holdIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Hold</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>holdIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.HoldList getHoldsByIds(org.osid.id.IdList holdIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.hold.Hold> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = holdIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getHold(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("hold " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.hold.hold.LinkedHoldList(ret));
    }


    /**
     *  Gets a <code>HoldList</code> corresponding to the given
     *  hold genus <code>Type</code> which does not include
     *  holds of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  holds or an error results. Otherwise, the returned list
     *  may contain only those holds that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, holds are returned that are currently effective.
     *  In any effective mode, effective holds and those currently expired
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getHolds()</code>.
     *
     *  @param  holdGenusType a hold genus type 
     *  @return the returned <code>Hold</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>holdGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.HoldList getHoldsByGenusType(org.osid.type.Type holdGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.hold.hold.HoldGenusFilterList(getHolds(), holdGenusType));
    }


    /**
     *  Gets a <code>HoldList</code> corresponding to the given
     *  hold genus <code>Type</code> and include any additional
     *  holds with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  holds or an error results. Otherwise, the returned list
     *  may contain only those holds that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, holds are returned that are currently
     *  effective.  In any effective mode, effective holds and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getHolds()</code>.
     *
     *  @param  holdGenusType a hold genus type 
     *  @return the returned <code>Hold</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>holdGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.HoldList getHoldsByParentGenusType(org.osid.type.Type holdGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getHoldsByGenusType(holdGenusType));
    }


    /**
     *  Gets a <code>HoldList</code> containing the given
     *  hold record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  holds or an error results. Otherwise, the returned list
     *  may contain only those holds that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, holds are returned that are currently
     *  effective.  In any effective mode, effective holds and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getHolds()</code>.
     *
     *  @param  holdRecordType a hold record type 
     *  @return the returned <code>Hold</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>holdRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.HoldList getHoldsByRecordType(org.osid.type.Type holdRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.hold.hold.HoldRecordFilterList(getHolds(), holdRecordType));
    }


    /**
     *  Gets a <code>HoldList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  holds or an error results. Otherwise, the returned list
     *  may contain only those holds that are accessible
     *  through this session.
     *  
     *  In active mode, holds are returned that are currently
     *  active. In any status mode, active and inactive holds
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Hold</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.hold.HoldList getHoldsOnDate(org.osid.calendaring.DateTime from, 
                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.hold.hold.TemporalHoldFilterList(getHolds(), from, to));
    }
        

    /**
     *  Gets a list of holds corresponding to a resource
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  holds or an error results. Otherwise, the returned list
     *  may contain only those holds that are accessible
     *  through this session.
     *
     *  In effective mode, holds are returned that are
     *  currently effective.  In any effective mode, effective
     *  holds and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @return the returned <code>HoldList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.hold.HoldList getHoldsForResource(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.hold.hold.HoldFilterList(new ResourceFilter(resourceId), getHolds()));
    }


    /**
     *  Gets a list of holds corresponding to a resource
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  holds or an error results. Otherwise, the returned list
     *  may contain only those holds that are accessible
     *  through this session.
     *
     *  In effective mode, holds are returned that are
     *  currently effective.  In any effective mode, effective
     *  holds and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>HoldList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.hold.HoldList getHoldsForResourceOnDate(org.osid.id.Id resourceId,
                                                            org.osid.calendaring.DateTime from,
                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.hold.hold.TemporalHoldFilterList(getHoldsForResource(resourceId), from, to));
    }


    /**
     *  Gets a list of holds for an agent.
     *  
     *  In plenary mode, the returned list contains all known holds or
     *  an error results. Otherwise, the returned list may contain
     *  only those holds that are accessible through this session.
     *  
     *  In effective mode, holds are returned that are currently
     *  effective. In any effective mode, effective holds and those
     *  currently expired are returned.
     *
     *  @param  agentId an agent <code>Id</code> 
     *  @return the returned <code>Hold</code> list 
     *  @throws org.osid.NullArgumentException <code>agentId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.hold.HoldList getHoldsForAgent(org.osid.id.Id agentId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.hold.hold.HoldFilterList(new AgentFilter(agentId), getHolds()));
    }


    /**
     *  Gets a list of holds for an agent and effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *  
     *  In plenary mode, the returned list contains all known holds or
     *  an error results. Otherwise, the returned list may contain
     *  only those holds that are accessible through this session. In
     *  both cases, the order of the set is by the start of the
     *  effective date.
     *  
     *  In effective mode, holds are returned that are currently
     *  effective. In any effective mode, effective holds and those
     *  currently expired are returned.
     *
     *  @param  agentId an agent <code>Id</code> 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Hold</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is 
     *          greater than <code>to</code> 
     *  @throws org.osid.NullArgumentException <code>agentId</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.hold.HoldList getHoldsForAgentOnDate(org.osid.id.Id agentId, 
                                                         org.osid.calendaring.DateTime from, 
                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.hold.hold.TemporalHoldFilterList(getHoldsForAgent(agentId), from, to));
    }


    /**
     *  Gets a list of holds corresponding to a issue
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  holds or an error results. Otherwise, the returned list
     *  may contain only those holds that are accessible
     *  through this session.
     *
     *  In effective mode, holds are returned that are
     *  currently effective.  In any effective mode, effective
     *  holds and those currently expired are returned.
     *
     *  @param  issueId the <code>Id</code> of the issue
     *  @return the returned <code>HoldList</code>
     *  @throws org.osid.NullArgumentException <code>issueId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.hold.HoldList getHoldsForIssue(org.osid.id.Id issueId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.hold.hold.HoldFilterList(new IssueFilter(issueId), getHolds()));
    }


    /**
     *  Gets a list of holds corresponding to a issue
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  holds or an error results. Otherwise, the returned list
     *  may contain only those holds that are accessible
     *  through this session.
     *
     *  In effective mode, holds are returned that are
     *  currently effective.  In any effective mode, effective
     *  holds and those currently expired are returned.
     *
     *  @param  issueId the <code>Id</code> of the issue
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>HoldList</code>
     *  @throws org.osid.NullArgumentException <code>issueId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.hold.HoldList getHoldsForIssueOnDate(org.osid.id.Id issueId,
                                                         org.osid.calendaring.DateTime from,
                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.hold.hold.TemporalHoldFilterList(getHoldsForIssue(issueId), from, to));
    }


    /**
     *  Gets a list of holds corresponding to resource and issue
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  holds or an error results. Otherwise, the returned list
     *  may contain only those holds that are accessible
     *  through this session.
     *
     *  In effective mode, holds are returned that are
     *  currently effective.  In any effective mode, effective
     *  holds and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  issueId the <code>Id</code> of the issue
     *  @return the returned <code>HoldList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>issueId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.hold.HoldList getHoldsForResourceAndIssue(org.osid.id.Id resourceId,
                                                              org.osid.id.Id issueId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.hold.hold.HoldFilterList(new IssueFilter(issueId), getHoldsForResource(resourceId)));
    }


    /**
     *  Gets a list of holds corresponding to resource and issue
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  holds or an error results. Otherwise, the returned list
     *  may contain only those holds that are accessible
     *  through this session.
     *
     *  In effective mode, holds are returned that are
     *  currently effective.  In any effective mode, effective
     *  holds and those currently expired are returned.
     *
     *  @param  issueId the <code>Id</code> of the issue
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>HoldList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>issueId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.hold.HoldList getHoldsForResourceAndIssueOnDate(org.osid.id.Id resourceId,
                                                                    org.osid.id.Id issueId,
                                                                    org.osid.calendaring.DateTime from,
                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.hold.hold.TemporalHoldFilterList(getHoldsForResourceAndIssue(resourceId, issueId), from, to));
    }


    /**
     *  Gets a list of holds corresponding to agent and issue
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  holds or an error results. Otherwise, the returned list
     *  may contain only those holds that are accessible
     *  through this session.
     *
     *  In effective mode, holds are returned that are
     *  currently effective.  In any effective mode, effective
     *  holds and those currently expired are returned.
     *
     *  @param  agentId the <code>Id</code> of the agent
     *  @param  issueId the <code>Id</code> of the issue
     *  @return the returned <code>HoldList</code>
     *  @throws org.osid.NullArgumentException <code>agentId</code>,
     *          <code>issueId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.hold.HoldList getHoldsForAgentAndIssue(org.osid.id.Id agentId,
                                                           org.osid.id.Id issueId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.hold.hold.HoldFilterList(new IssueFilter(issueId), getHoldsForAgent(agentId)));
    }


    /**
     *  Gets a list of holds corresponding to agent and issue
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  holds or an error results. Otherwise, the returned list
     *  may contain only those holds that are accessible
     *  through this session.
     *
     *  In effective mode, holds are returned that are
     *  currently effective.  In any effective mode, effective
     *  holds and those currently expired are returned.
     *
     *  @param  issueId the <code>Id</code> of the issue
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>HoldList</code>
     *  @throws org.osid.NullArgumentException <code>agentId</code>,
     *          <code>issueId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.hold.HoldList getHoldsForAgentAndIssueOnDate(org.osid.id.Id agentId,
                                                                 org.osid.id.Id issueId,
                                                                 org.osid.calendaring.DateTime from,
                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.hold.hold.TemporalHoldFilterList(getHoldsForAgentAndIssue(agentId, issueId), from, to));
    }


    /**
     *  Gets all <code>Holds</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  holds or an error results. Otherwise, the returned list
     *  may contain only those holds that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, holds are returned that are currently
     *  effective.  In any effective mode, effective holds and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Holds</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.hold.HoldList getHolds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the hold list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of holds
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.hold.HoldList filterHoldsOnViews(org.osid.hold.HoldList list)
        throws org.osid.OperationFailedException {

        org.osid.hold.HoldList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.hold.hold.EffectiveHoldFilterList(ret);
        }

        return (ret);
    }


    public static class ResourceFilter
        implements net.okapia.osid.jamocha.inline.filter.hold.hold.HoldFilter {
         
        private final org.osid.id.Id resourceId;
         
         
        /**
         *  Constructs a new <code>ResourceFilter</code>.
         *
         *  @param resourceId the resource to filter
         *  @throws org.osid.NullArgumentException
         *          <code>resourceId</code> is <code>null</code>
         */
        
        public ResourceFilter(org.osid.id.Id resourceId) {
            nullarg(resourceId, "resource Id");
            this.resourceId = resourceId;
            return;
        }

         
        /**
         *  Used by the HoldFilterList to filter the 
         *  hold list based on resource.
         *
         *  @param hold the hold
         *  @return <code>true</code> to pass the hold,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.hold.Hold hold) {
            if (hold.hasResource()) {
                return (hold.getResourceId().equals(this.resourceId));
            } else {
                return (false);
            }
        }
    }


    public static class AgentFilter
        implements net.okapia.osid.jamocha.inline.filter.hold.hold.HoldFilter {
         
        private final org.osid.id.Id agentId;
         
         
        /**
         *  Constructs a new <code>AgentFilter</code>.
         *
         *  @param agentId the agent to filter
         *  @throws org.osid.NullArgumentException
         *          <code>agentId</code> is <code>null</code>
         */
        
        public AgentFilter(org.osid.id.Id agentId) {
            nullarg(agentId, "agent Id");
            this.agentId = agentId;
            return;
        }

         
        /**
         *  Used by the HoldFilterList to filter the 
         *  hold list based on agent.
         *
         *  @param hold the hold
         *  @return <code>true</code> to pass the hold,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.hold.Hold hold) {
            if (hold.hasAgent()) {
                return (hold.getAgentId().equals(this.agentId));
            } else {
                return (false);
            }
        }
    }


    public static class IssueFilter
        implements net.okapia.osid.jamocha.inline.filter.hold.hold.HoldFilter {
         
        private final org.osid.id.Id issueId;
         
         
        /**
         *  Constructs a new <code>IssueFilter</code>.
         *
         *  @param issueId the issue to filter
         *  @throws org.osid.NullArgumentException
         *          <code>issueId</code> is <code>null</code>
         */
        
        public IssueFilter(org.osid.id.Id issueId) {
            nullarg(issueId, "issue Id");
            this.issueId = issueId;
            return;
        }

         
        /**
         *  Used by the HoldFilterList to filter the 
         *  hold list based on issue.
         *
         *  @param hold the hold
         *  @return <code>true</code> to pass the hold,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.hold.Hold hold) {
            return (hold.getIssueId().equals(this.issueId));
        }
    }
}
