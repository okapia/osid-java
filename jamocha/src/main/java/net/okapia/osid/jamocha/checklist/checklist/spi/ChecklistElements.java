//
// ChecklistElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.checklist.checklist.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class ChecklistElements
    extends net.okapia.osid.jamocha.spi.OsidCatalogElements {


    /**
     *  Gets the ChecklistElement Id.
     *
     *  @return the checklist element Id
     */

    public static org.osid.id.Id getChecklistEntityId() {
        return (makeEntityId("osid.checklist.Checklist"));
    }


    /**
     *  Gets the TodoId element Id.
     *
     *  @return the TodoId element Id
     */

    public static org.osid.id.Id getTodoId() {
        return (makeQueryElementId("osid.checklist.checklist.TodoId"));
    }


    /**
     *  Gets the Todo element Id.
     *
     *  @return the Todo element Id
     */

    public static org.osid.id.Id getTodo() {
        return (makeQueryElementId("osid.checklist.checklist.Todo"));
    }


    /**
     *  Gets the AncestorChecklistId element Id.
     *
     *  @return the AncestorChecklistId element Id
     */

    public static org.osid.id.Id getAncestorChecklistId() {
        return (makeQueryElementId("osid.checklist.checklist.AncestorChecklistId"));
    }


    /**
     *  Gets the AncestorChecklist element Id.
     *
     *  @return the AncestorChecklist element Id
     */

    public static org.osid.id.Id getAncestorChecklist() {
        return (makeQueryElementId("osid.checklist.checklist.AncestorChecklist"));
    }


    /**
     *  Gets the DescendantChecklistId element Id.
     *
     *  @return the DescendantChecklistId element Id
     */

    public static org.osid.id.Id getDescendantChecklistId() {
        return (makeQueryElementId("osid.checklist.checklist.DescendantChecklistId"));
    }


    /**
     *  Gets the DescendantChecklist element Id.
     *
     *  @return the DescendantChecklist element Id
     */

    public static org.osid.id.Id getDescendantChecklist() {
        return (makeQueryElementId("osid.checklist.checklist.DescendantChecklist"));
    }
}
