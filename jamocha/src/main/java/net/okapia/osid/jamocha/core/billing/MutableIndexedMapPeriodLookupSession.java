//
// MutableIndexedMapPeriodLookupSession
//
//    Implements a Period lookup service backed by a collection of
//    periods indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.billing;


/**
 *  Implements a Period lookup service backed by a collection of
 *  periods. The periods are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some periods may be compatible
 *  with more types than are indicated through these period
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of periods can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapPeriodLookupSession
    extends net.okapia.osid.jamocha.core.billing.spi.AbstractIndexedMapPeriodLookupSession
    implements org.osid.billing.PeriodLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapPeriodLookupSession} with no periods.
     *
     *  @param business the business
     *  @throws org.osid.NullArgumentException {@code business}
     *          is {@code null}
     */

      public MutableIndexedMapPeriodLookupSession(org.osid.billing.Business business) {
        setBusiness(business);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapPeriodLookupSession} with a
     *  single period.
     *  
     *  @param business the business
     *  @param  period a single period
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code period} is {@code null}
     */

    public MutableIndexedMapPeriodLookupSession(org.osid.billing.Business business,
                                                  org.osid.billing.Period period) {
        this(business);
        putPeriod(period);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapPeriodLookupSession} using an
     *  array of periods.
     *
     *  @param business the business
     *  @param  periods an array of periods
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code periods} is {@code null}
     */

    public MutableIndexedMapPeriodLookupSession(org.osid.billing.Business business,
                                                  org.osid.billing.Period[] periods) {
        this(business);
        putPeriods(periods);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapPeriodLookupSession} using a
     *  collection of periods.
     *
     *  @param business the business
     *  @param  periods a collection of periods
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code periods} is {@code null}
     */

    public MutableIndexedMapPeriodLookupSession(org.osid.billing.Business business,
                                                  java.util.Collection<? extends org.osid.billing.Period> periods) {

        this(business);
        putPeriods(periods);
        return;
    }
    

    /**
     *  Makes a {@code Period} available in this session.
     *
     *  @param  period a period
     *  @throws org.osid.NullArgumentException {@code period{@code  is
     *          {@code null}
     */

    @Override
    public void putPeriod(org.osid.billing.Period period) {
        super.putPeriod(period);
        return;
    }


    /**
     *  Makes an array of periods available in this session.
     *
     *  @param  periods an array of periods
     *  @throws org.osid.NullArgumentException {@code periods{@code 
     *          is {@code null}
     */

    @Override
    public void putPeriods(org.osid.billing.Period[] periods) {
        super.putPeriods(periods);
        return;
    }


    /**
     *  Makes collection of periods available in this session.
     *
     *  @param  periods a collection of periods
     *  @throws org.osid.NullArgumentException {@code period{@code  is
     *          {@code null}
     */

    @Override
    public void putPeriods(java.util.Collection<? extends org.osid.billing.Period> periods) {
        super.putPeriods(periods);
        return;
    }


    /**
     *  Removes a Period from this session.
     *
     *  @param periodId the {@code Id} of the period
     *  @throws org.osid.NullArgumentException {@code periodId{@code  is
     *          {@code null}
     */

    @Override
    public void removePeriod(org.osid.id.Id periodId) {
        super.removePeriod(periodId);
        return;
    }    
}
