//
// AbstractAssetQueryInspector.java
//
//     A template for making an AssetQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.repository.asset.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for assets.
 */

public abstract class AbstractAssetQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractSourceableOsidObjectQueryInspector
    implements org.osid.repository.AssetQueryInspector {

    private final java.util.Collection<org.osid.repository.records.AssetQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the title query terms. 
     *
     *  @return the title terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getTitleTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the public domain query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getPublicDomainTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the copyright query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getCopyrightTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the copyright registration query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getCopyrightRegistrationTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the verbatim distribution query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getDistributeVerbatimTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the alteration distribution query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getDistributeAlterationsTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the composition distribution query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getDistributeCompositionsTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the source <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSourceIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the source query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getSourceTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the created time query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getCreatedDateTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the published query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getPublishedTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the published time query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getPublishedDateTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the principal credit string query terms. 
     *
     *  @return the principal credit string terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getPrincipalCreditStringTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the temporal coverage query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getTemporalCoverageTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the location <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getLocationIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the location query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.LocationQueryInspector[] getLocationTerms() {
        return (new org.osid.mapping.LocationQueryInspector[0]);
    }


    /**
     *  Gets the spatial coverage query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.SpatialUnitTerm[] getSpatialCoverageTerms() {
        return (new org.osid.search.terms.SpatialUnitTerm[0]);
    }


    /**
     *  Gets the spatial coverage overlap query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.SpatialUnitTerm[] getSpatialCoverageOverlapTerms() {
        return (new org.osid.search.terms.SpatialUnitTerm[0]);
    }


    /**
     *  Gets the asset content <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAssetContentIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the asset content query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.repository.AssetContentQueryInspector[] getAssetContentTerms() {
        return (new org.osid.repository.AssetContentQueryInspector[0]);
    }


    /**
     *  Gets the composition <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCompositionIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the composition query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.repository.CompositionQueryInspector[] getCompositionTerms() {
        return (new org.osid.repository.CompositionQueryInspector[0]);
    }


    /**
     *  Gets the repository <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRepositoryIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the repository query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.repository.RepositoryQueryInspector[] getRepositoryTerms() {
        return (new org.osid.repository.RepositoryQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given asset query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an asset implementing the requested record.
     *
     *  @param assetRecordType an asset record type
     *  @return the asset query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>assetRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(assetRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.repository.records.AssetQueryInspectorRecord getAssetQueryInspectorRecord(org.osid.type.Type assetRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.repository.records.AssetQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(assetRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assetRecordType + " is not supported");
    }


    /**
     *  Adds a record to this asset query. 
     *
     *  @param assetQueryInspectorRecord asset query inspector
     *         record
     *  @param assetRecordType asset record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAssetQueryInspectorRecord(org.osid.repository.records.AssetQueryInspectorRecord assetQueryInspectorRecord, 
                                                   org.osid.type.Type assetRecordType) {

        addRecordType(assetRecordType);
        nullarg(assetRecordType, "asset record type");
        this.records.add(assetQueryInspectorRecord);        
        return;
    }
}
