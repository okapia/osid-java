//
// DistributorElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.distributor.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class DistributorElements
    extends net.okapia.osid.jamocha.spi.OsidCatalogElements {


    /**
     *  Gets the DistributorElement Id.
     *
     *  @return the distributor element Id
     */

    public static org.osid.id.Id getDistributorEntityId() {
        return (makeEntityId("osid.provisioning.Distributor"));
    }


    /**
     *  Gets the BrokerId element Id.
     *
     *  @return the BrokerId element Id
     */

    public static org.osid.id.Id getBrokerId() {
        return (makeQueryElementId("osid.provisioning.distributor.BrokerId"));
    }


    /**
     *  Gets the Broker element Id.
     *
     *  @return the Broker element Id
     */

    public static org.osid.id.Id getBroker() {
        return (makeQueryElementId("osid.provisioning.distributor.Broker"));
    }


    /**
     *  Gets the QueueId element Id.
     *
     *  @return the QueueId element Id
     */

    public static org.osid.id.Id getQueueId() {
        return (makeQueryElementId("osid.provisioning.distributor.QueueId"));
    }


    /**
     *  Gets the Queue element Id.
     *
     *  @return the Queue element Id
     */

    public static org.osid.id.Id getQueue() {
        return (makeQueryElementId("osid.provisioning.distributor.Queue"));
    }


    /**
     *  Gets the PoolId element Id.
     *
     *  @return the PoolId element Id
     */

    public static org.osid.id.Id getPoolId() {
        return (makeQueryElementId("osid.provisioning.distributor.PoolId"));
    }


    /**
     *  Gets the Pool element Id.
     *
     *  @return the Pool element Id
     */

    public static org.osid.id.Id getPool() {
        return (makeQueryElementId("osid.provisioning.distributor.Pool"));
    }


    /**
     *  Gets the ProvisionId element Id.
     *
     *  @return the ProvisionId element Id
     */

    public static org.osid.id.Id getProvisionId() {
        return (makeQueryElementId("osid.provisioning.distributor.ProvisionId"));
    }


    /**
     *  Gets the Provision element Id.
     *
     *  @return the Provision element Id
     */

    public static org.osid.id.Id getProvision() {
        return (makeQueryElementId("osid.provisioning.distributor.Provision"));
    }


    /**
     *  Gets the AncestorDistributorId element Id.
     *
     *  @return the AncestorDistributorId element Id
     */

    public static org.osid.id.Id getAncestorDistributorId() {
        return (makeQueryElementId("osid.provisioning.distributor.AncestorDistributorId"));
    }


    /**
     *  Gets the AncestorDistributor element Id.
     *
     *  @return the AncestorDistributor element Id
     */

    public static org.osid.id.Id getAncestorDistributor() {
        return (makeQueryElementId("osid.provisioning.distributor.AncestorDistributor"));
    }


    /**
     *  Gets the DescendantDistributorId element Id.
     *
     *  @return the DescendantDistributorId element Id
     */

    public static org.osid.id.Id getDescendantDistributorId() {
        return (makeQueryElementId("osid.provisioning.distributor.DescendantDistributorId"));
    }


    /**
     *  Gets the DescendantDistributor element Id.
     *
     *  @return the DescendantDistributor element Id
     */

    public static org.osid.id.Id getDescendantDistributor() {
        return (makeQueryElementId("osid.provisioning.distributor.DescendantDistributor"));
    }
}
