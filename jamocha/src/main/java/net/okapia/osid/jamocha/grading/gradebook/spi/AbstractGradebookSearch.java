//
// AbstractGradebookSearch.java
//
//     A template for making a Gradebook Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.gradebook.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing gradebook searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractGradebookSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.grading.GradebookSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.grading.records.GradebookSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.grading.GradebookSearchOrder gradebookSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of gradebooks. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  gradebookIds list of gradebooks
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongGradebooks(org.osid.id.IdList gradebookIds) {
        while (gradebookIds.hasNext()) {
            try {
                this.ids.add(gradebookIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongGradebooks</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of gradebook Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getGradebookIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  gradebookSearchOrder gradebook search order 
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>gradebookSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderGradebookResults(org.osid.grading.GradebookSearchOrder gradebookSearchOrder) {
	this.gradebookSearchOrder = gradebookSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.grading.GradebookSearchOrder getGradebookSearchOrder() {
	return (this.gradebookSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given gradebook search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a gradebook implementing the requested record.
     *
     *  @param gradebookSearchRecordType a gradebook search record
     *         type
     *  @return the gradebook search record
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(gradebookSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.grading.records.GradebookSearchRecord getGradebookSearchRecord(org.osid.type.Type gradebookSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.grading.records.GradebookSearchRecord record : this.records) {
            if (record.implementsRecordType(gradebookSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(gradebookSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this gradebook search. 
     *
     *  @param gradebookSearchRecord gradebook search record
     *  @param gradebookSearchRecordType gradebook search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addGradebookSearchRecord(org.osid.grading.records.GradebookSearchRecord gradebookSearchRecord, 
                                           org.osid.type.Type gradebookSearchRecordType) {

        addRecordType(gradebookSearchRecordType);
        this.records.add(gradebookSearchRecord);        
        return;
    }
}
