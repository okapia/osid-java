//
// AbstractMapCatalogLookupSession
//
//    A simple framework for providing a Catalog lookup service
//    backed by a fixed collection of catalogs.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.cataloging.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Catalog lookup service backed by a
 *  fixed collection of catalogs. The catalogs are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Catalogs</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapCatalogLookupSession
    extends net.okapia.osid.jamocha.cataloging.spi.AbstractCatalogLookupSession
    implements org.osid.cataloging.CatalogLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.cataloging.Catalog> catalogs = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.cataloging.Catalog>());


    /**
     *  Makes a <code>Catalog</code> available in this session.
     *
     *  @param  catalog a catalog
     *  @throws org.osid.NullArgumentException <code>catalog<code>
     *          is <code>null</code>
     */

    protected void putCatalog(org.osid.cataloging.Catalog catalog) {
        this.catalogs.put(catalog.getId(), catalog);
        return;
    }


    /**
     *  Makes an array of catalogs available in this session.
     *
     *  @param  catalogs an array of catalogs
     *  @throws org.osid.NullArgumentException <code>catalogs<code>
     *          is <code>null</code>
     */

    protected void putCatalogs(org.osid.cataloging.Catalog[] catalogs) {
        putCatalogs(java.util.Arrays.asList(catalogs));
        return;
    }


    /**
     *  Makes a collection of catalogs available in this session.
     *
     *  @param  catalogs a collection of catalogs
     *  @throws org.osid.NullArgumentException <code>catalogs<code>
     *          is <code>null</code>
     */

    protected void putCatalogs(java.util.Collection<? extends org.osid.cataloging.Catalog> catalogs) {
        for (org.osid.cataloging.Catalog catalog : catalogs) {
            this.catalogs.put(catalog.getId(), catalog);
        }

        return;
    }


    /**
     *  Removes a Catalog from this session.
     *
     *  @param  catalogId the <code>Id</code> of the catalog
     *  @throws org.osid.NullArgumentException <code>catalogId<code> is
     *          <code>null</code>
     */

    protected void removeCatalog(org.osid.id.Id catalogId) {
        this.catalogs.remove(catalogId);
        return;
    }


    /**
     *  Gets the <code>Catalog</code> specified by its <code>Id</code>.
     *
     *  @param  catalogId <code>Id</code> of the <code>Catalog</code>
     *  @return the catalog
     *  @throws org.osid.NotFoundException <code>catalogId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>catalogId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.cataloging.Catalog getCatalog(org.osid.id.Id catalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.cataloging.Catalog catalog = this.catalogs.get(catalogId);
        if (catalog == null) {
            throw new org.osid.NotFoundException("catalog not found: " + catalogId);
        }

        return (catalog);
    }


    /**
     *  Gets all <code>Catalogs</code>. In plenary mode, the returned
     *  list contains all known catalogs or an error
     *  results. Otherwise, the returned list may contain only those
     *  catalogs that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Catalogs</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogList getCatalogs()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.cataloging.catalog.ArrayCatalogList(this.catalogs.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.catalogs.clear();
        super.close();
        return;
    }
}
