//
// AbstractQuerySequenceRuleEnablerLookupSession.java
//
//    A SequenceRuleEnablerQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.assessment.authoring.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A SequenceRuleEnablerQuerySession adapter.
 */

public abstract class AbstractAdapterSequenceRuleEnablerQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.assessment.authoring.SequenceRuleEnablerQuerySession {

    private final org.osid.assessment.authoring.SequenceRuleEnablerQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterSequenceRuleEnablerQuerySession.
     *
     *  @param session the underlying sequence rule enabler query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterSequenceRuleEnablerQuerySession(org.osid.assessment.authoring.SequenceRuleEnablerQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeBank</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeBank Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBankId() {
        return (this.session.getBankId());
    }


    /**
     *  Gets the {@codeBank</code> associated with this 
     *  session.
     *
     *  @return the {@codeBank</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.Bank getBank()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getBank());
    }


    /**
     *  Tests if this user can perform {@codeSequenceRuleEnabler</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchSequenceRuleEnablers() {
        return (this.session.canSearchSequenceRuleEnablers());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include sequence rule enablers in banks which are children
     *  of this bank in the bank hierarchy.
     */

    @OSID @Override
    public void useFederatedBankView() {
        this.session.useFederatedBankView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this bank only.
     */
    
    @OSID @Override
    public void useIsolatedBankView() {
        this.session.useIsolatedBankView();
        return;
    }
    
      
    /**
     *  Gets a sequence rule enabler query. The returned query will not have an
     *  extension query.
     *
     *  @return the sequence rule enabler query 
     */
      
    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerQuery getSequenceRuleEnablerQuery() {
        return (this.session.getSequenceRuleEnablerQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  sequenceRuleEnablerQuery the sequence rule enabler query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code sequenceRuleEnablerQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code sequenceRuleEnablerQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerList getSequenceRuleEnablersByQuery(org.osid.assessment.authoring.SequenceRuleEnablerQuery sequenceRuleEnablerQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getSequenceRuleEnablersByQuery(sequenceRuleEnablerQuery));
    }
}
