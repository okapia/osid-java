//
// InlineCatalogEnablerNotifier.java
//
//     A callback interface for performing catalog enabler
//     notifications.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.cataloging.rules.spi;


/**
 *  A callback interface for performing catalog enabler
 *  notifications.
 */

public interface InlineCatalogEnablerNotifier {



    /**
     *  Notifies the creation of a new catalog enabler.
     *
     *  @param catalogEnablerId the {@code Id} of the new 
     *         catalog enabler
     *  @throws org.osid.NullArgumentException {@code [objectId]}
     *          is {@code null}
     */

    public void newCatalogEnabler(org.osid.id.Id catalogEnablerId);


    /**
     *  Notifies the change of an updated catalog enabler.
     *
     *  @param catalogEnablerId the {@code Id} of the changed
     *         catalog enabler
     *  @throws org.osid.NullArgumentException {@code [objectId]}
     *          is {@code null}
     */
      
    public void changedCatalogEnabler(org.osid.id.Id catalogEnablerId);


    /**
     *  Notifies the deletion of an catalog enabler.
     *
     *  @param catalogEnablerId the {@code Id} of the deleted
     *         catalog enabler
     *  @throws org.osid.NullArgumentException {@code [objectId]} is
     *          {@code null}
     */

    public void deletedCatalogEnabler(org.osid.id.Id catalogEnablerId);

}
