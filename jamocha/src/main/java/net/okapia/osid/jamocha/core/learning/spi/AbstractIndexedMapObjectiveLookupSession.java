//
// AbstractIndexedMapObjectiveLookupSession.java
//
//    A simple framework for providing an Objective lookup service
//    backed by a fixed collection of objectives with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.learning.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an Objective lookup service backed by a
 *  fixed collection of objectives. The objectives are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some objectives may be compatible
 *  with more types than are indicated through these objective
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Objectives</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapObjectiveLookupSession
    extends AbstractMapObjectiveLookupSession
    implements org.osid.learning.ObjectiveLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.learning.Objective> objectivesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.learning.Objective>());
    private final MultiMap<org.osid.type.Type, org.osid.learning.Objective> objectivesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.learning.Objective>());


    /**
     *  Makes an <code>Objective</code> available in this session.
     *
     *  @param  objective an objective
     *  @throws org.osid.NullArgumentException <code>objective<code> is
     *          <code>null</code>
     */

    @Override
    protected void putObjective(org.osid.learning.Objective objective) {
        super.putObjective(objective);

        this.objectivesByGenus.put(objective.getGenusType(), objective);
        
        try (org.osid.type.TypeList types = objective.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.objectivesByRecord.put(types.getNextType(), objective);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an objective from this session.
     *
     *  @param objectiveId the <code>Id</code> of the objective
     *  @throws org.osid.NullArgumentException <code>objectiveId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeObjective(org.osid.id.Id objectiveId) {
        org.osid.learning.Objective objective;
        try {
            objective = getObjective(objectiveId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.objectivesByGenus.remove(objective.getGenusType());

        try (org.osid.type.TypeList types = objective.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.objectivesByRecord.remove(types.getNextType(), objective);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeObjective(objectiveId);
        return;
    }


    /**
     *  Gets an <code>ObjectiveList</code> corresponding to the given
     *  objective genus <code>Type</code> which does not include
     *  objectives of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known objectives or an error results. Otherwise,
     *  the returned list may contain only those objectives that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  objectiveGenusType an objective genus type 
     *  @return the returned <code>Objective</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveList getObjectivesByGenusType(org.osid.type.Type objectiveGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.learning.objective.ArrayObjectiveList(this.objectivesByGenus.get(objectiveGenusType)));
    }


    /**
     *  Gets an <code>ObjectiveList</code> containing the given
     *  objective record <code>Type</code>. In plenary mode, the
     *  returned list contains all known objectives or an error
     *  results. Otherwise, the returned list may contain only those
     *  objectives that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  objectiveRecordType an objective record type 
     *  @return the returned <code>objective</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveList getObjectivesByRecordType(org.osid.type.Type objectiveRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.learning.objective.ArrayObjectiveList(this.objectivesByRecord.get(objectiveRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.objectivesByGenus.clear();
        this.objectivesByRecord.clear();

        super.close();

        return;
    }
}
