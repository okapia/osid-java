//
// AbstractBillingManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.billing.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractBillingManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.billing.BillingManager,
               org.osid.billing.BillingProxyManager {

    private final Types customerRecordTypes                = new TypeRefSet();
    private final Types customerSearchRecordTypes          = new TypeRefSet();

    private final Types itemRecordTypes                    = new TypeRefSet();
    private final Types itemSearchRecordTypes              = new TypeRefSet();

    private final Types categoryRecordTypes                = new TypeRefSet();
    private final Types categorySearchRecordTypes          = new TypeRefSet();

    private final Types entryRecordTypes                   = new TypeRefSet();
    private final Types entrySearchRecordTypes             = new TypeRefSet();

    private final Types periodRecordTypes                  = new TypeRefSet();
    private final Types periodSearchRecordTypes            = new TypeRefSet();

    private final Types businessRecordTypes                = new TypeRefSet();
    private final Types businessSearchRecordTypes          = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractBillingManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractBillingManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any business federation is exposed. Federation is exposed 
     *  when a specific business may be identified, selected and used to 
     *  create a lookup or admin session. Federation is not exposed when a set 
     *  of catalogs appears as a single catalog. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if looking up customers is supported. 
     *
     *  @return <code> true </code> if customer lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCustomerLookup() {
        return (false);
    }


    /**
     *  Tests if querying customers is supported. 
     *
     *  @return <code> true </code> if customer query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCustomerQuery() {
        return (false);
    }


    /**
     *  Tests if searching customers is supported. 
     *
     *  @return <code> true </code> if customer search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCustomerSearch() {
        return (false);
    }


    /**
     *  Tests if customer <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if customer administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCustomerAdmin() {
        return (false);
    }


    /**
     *  Tests if a customer <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if customer notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCustomerNotification() {
        return (false);
    }


    /**
     *  Tests if a businessing service is supported. 
     *
     *  @return <code> true </code> if businessing is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCustomerBusiness() {
        return (false);
    }


    /**
     *  Tests if a businessing service is supported. A businessing service 
     *  maps customers to catalogs. 
     *
     *  @return <code> true </code> if businessing is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCustomerBusinessAssignment() {
        return (false);
    }


    /**
     *  Tests if a customer smart business session is available. 
     *
     *  @return <code> true </code> if a customer smart business session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCustomerSmartBusiness() {
        return (false);
    }


    /**
     *  Tests if looking up items is supported. 
     *
     *  @return <code> true </code> if item lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemLookup() {
        return (false);
    }


    /**
     *  Tests if querying items is supported. 
     *
     *  @return <code> true </code> if item query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemQuery() {
        return (false);
    }


    /**
     *  Tests if searching items is supported. 
     *
     *  @return <code> true </code> if item search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemSearch() {
        return (false);
    }


    /**
     *  Tests if an item <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if item administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemAdmin() {
        return (false);
    }


    /**
     *  Tests if an item <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if item notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemNotification() {
        return (false);
    }


    /**
     *  Tests if an item cataloging service is supported. 
     *
     *  @return <code> true </code> if item catalog is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemBusiness() {
        return (false);
    }


    /**
     *  Tests if an item cataloging service is supported. A cataloging service 
     *  maps items to catalogs. 
     *
     *  @return <code> true </code> if item cataloging is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemBusinessAssignment() {
        return (false);
    }


    /**
     *  Tests if an item smart business session is available. 
     *
     *  @return <code> true </code> if an item smart business session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemSmartBusiness() {
        return (false);
    }


    /**
     *  Tests if looking up categories is supported. 
     *
     *  @return <code> true </code> if category lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCategoryLookup() {
        return (false);
    }


    /**
     *  Tests if querying categories is supported. 
     *
     *  @return <code> true </code> if category query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCategoryQuery() {
        return (false);
    }


    /**
     *  Tests if searching categories is supported. 
     *
     *  @return <code> true </code> if category search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCategorySearch() {
        return (false);
    }


    /**
     *  Tests if category <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if category administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCategoryAdmin() {
        return (false);
    }


    /**
     *  Tests if a category <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if category notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCategoryNotification() {
        return (false);
    }


    /**
     *  Tests if a category cataloging service is supported. 
     *
     *  @return <code> true </code> if category catalog is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCategoryBusiness() {
        return (false);
    }


    /**
     *  Tests if a category cataloging service is supported. A cataloging 
     *  service maps categories to catalogs. 
     *
     *  @return <code> true </code> if category cataloging is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCategoryBusinessAssignment() {
        return (false);
    }


    /**
     *  Tests if a category smart business session is available. 
     *
     *  @return <code> true </code> if a category smart business session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCategorySmartBusiness() {
        return (false);
    }


    /**
     *  Tests if looking up entries is supported. 
     *
     *  @return <code> true </code> if entry lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryLookup() {
        return (false);
    }


    /**
     *  Tests if querying entries is supported. 
     *
     *  @return <code> true </code> if entry query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryQuery() {
        return (false);
    }


    /**
     *  Tests if searching entries is supported. 
     *
     *  @return <code> true </code> if entry search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntrySearch() {
        return (false);
    }


    /**
     *  Tests if entry administrative service is supported. 
     *
     *  @return <code> true </code> if entry administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryAdmin() {
        return (false);
    }


    /**
     *  Tests if an entry <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if entry notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryNotification() {
        return (false);
    }


    /**
     *  Tests if an entry cataloging service is supported. 
     *
     *  @return <code> true </code> if entry catalog is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryBusiness() {
        return (false);
    }


    /**
     *  Tests if an entry cataloging service is supported. A cataloging 
     *  service maps entries to catalogs. 
     *
     *  @return <code> true </code> if entry cataloging is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryBusinessAssignment() {
        return (false);
    }


    /**
     *  Tests if an entry smart business session is available. 
     *
     *  @return <code> true </code> if an entry smart business session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntrySmartBusiness() {
        return (false);
    }


    /**
     *  Tests if looking up periods is supported. 
     *
     *  @return <code> true </code> if period lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPeriodLookup() {
        return (false);
    }


    /**
     *  Tests if querying periods is supported. 
     *
     *  @return <code> true </code> if period query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPeriodQuery() {
        return (false);
    }


    /**
     *  Tests if searching periods is supported. 
     *
     *  @return <code> true </code> if period search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPeriodSearch() {
        return (false);
    }


    /**
     *  Tests if period <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if period administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPeriodAdmin() {
        return (false);
    }


    /**
     *  Tests if a period <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if period notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPeriodNotification() {
        return (false);
    }


    /**
     *  Tests if a period cataloging service is supported. 
     *
     *  @return <code> true </code> if period catalog is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPeriodBusiness() {
        return (false);
    }


    /**
     *  Tests if a period cataloging service is supported. A cataloging 
     *  service maps periods to catalogs. 
     *
     *  @return <code> true </code> if period cataloging is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPeriodBusinessAssignment() {
        return (false);
    }


    /**
     *  Tests if a period smart business session is available. 
     *
     *  @return <code> true </code> if a period smart business session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPeriodSmartBusiness() {
        return (false);
    }


    /**
     *  Tests if looking up businesses is supported. 
     *
     *  @return <code> true </code> if business lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessLookup() {
        return (false);
    }


    /**
     *  Tests if searching businesses is supported. 
     *
     *  @return <code> true </code> if business search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessSearch() {
        return (false);
    }


    /**
     *  Tests if querying businesses is supported. 
     *
     *  @return <code> true </code> if business query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessQuery() {
        return (false);
    }


    /**
     *  Tests if business administrative service is supported. 
     *
     *  @return <code> true </code> if business administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessAdmin() {
        return (false);
    }


    /**
     *  Tests if a business <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if business notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessNotification() {
        return (false);
    }


    /**
     *  Tests for the availability of a business hierarchy traversal service. 
     *
     *  @return <code> true </code> if business hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessHierarchy() {
        return (false);
    }


    /**
     *  Tests for the availability of a business hierarchy design service. 
     *
     *  @return <code> true </code> if business hierarchy design is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests for the availability of a billing batch service. 
     *
     *  @return <code> true </code> if a billing batch service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBillingBatch() {
        return (false);
    }


    /**
     *  Tests for the availability of a billing payment service. 
     *
     *  @return <code> true </code> if a billing payment service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBillingPayment() {
        return (false);
    }


    /**
     *  Gets the supported <code> Customer </code> record types. 
     *
     *  @return a list containing the supported <code> Customer </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCustomerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.customerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Customer </code> record type is supported. 
     *
     *  @param  customerRecordType a <code> Type </code> indicating a <code> 
     *          Customer </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> customerRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCustomerRecordType(org.osid.type.Type customerRecordType) {
        return (this.customerRecordTypes.contains(customerRecordType));
    }


    /**
     *  Adds support for a customer record type.
     *
     *  @param customerRecordType a customer record type
     *  @throws org.osid.NullArgumentException
     *  <code>customerRecordType</code> is <code>null</code>
     */

    protected void addCustomerRecordType(org.osid.type.Type customerRecordType) {
        this.customerRecordTypes.add(customerRecordType);
        return;
    }


    /**
     *  Removes support for a customer record type.
     *
     *  @param customerRecordType a customer record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>customerRecordType</code> is <code>null</code>
     */

    protected void removeCustomerRecordType(org.osid.type.Type customerRecordType) {
        this.customerRecordTypes.remove(customerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Customer </code> search record types. 
     *
     *  @return a list containing the supported <code> Customer </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCustomerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.customerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Customer </code> search record type is 
     *  supported. 
     *
     *  @param  customerSearchRecordType a <code> Type </code> indicating a 
     *          <code> Customer </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> customerSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCustomerSearchRecordType(org.osid.type.Type customerSearchRecordType) {
        return (this.customerSearchRecordTypes.contains(customerSearchRecordType));
    }


    /**
     *  Adds support for a customer search record type.
     *
     *  @param customerSearchRecordType a customer search record type
     *  @throws org.osid.NullArgumentException
     *  <code>customerSearchRecordType</code> is <code>null</code>
     */

    protected void addCustomerSearchRecordType(org.osid.type.Type customerSearchRecordType) {
        this.customerSearchRecordTypes.add(customerSearchRecordType);
        return;
    }


    /**
     *  Removes support for a customer search record type.
     *
     *  @param customerSearchRecordType a customer search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>customerSearchRecordType</code> is <code>null</code>
     */

    protected void removeCustomerSearchRecordType(org.osid.type.Type customerSearchRecordType) {
        this.customerSearchRecordTypes.remove(customerSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Item </code> record types. 
     *
     *  @return a list containing the supported <code> Item </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getItemRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.itemRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Item </code> record type is supported. 
     *
     *  @param  itemRecordType a <code> Type </code> indicating an <code> Item 
     *          </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> itemRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsItemRecordType(org.osid.type.Type itemRecordType) {
        return (this.itemRecordTypes.contains(itemRecordType));
    }


    /**
     *  Adds support for an item record type.
     *
     *  @param itemRecordType an item record type
     *  @throws org.osid.NullArgumentException
     *  <code>itemRecordType</code> is <code>null</code>
     */

    protected void addItemRecordType(org.osid.type.Type itemRecordType) {
        this.itemRecordTypes.add(itemRecordType);
        return;
    }


    /**
     *  Removes support for an item record type.
     *
     *  @param itemRecordType an item record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>itemRecordType</code> is <code>null</code>
     */

    protected void removeItemRecordType(org.osid.type.Type itemRecordType) {
        this.itemRecordTypes.remove(itemRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Item </code> search record types. 
     *
     *  @return a list containing the supported <code> Item </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getItemSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.itemSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Item </code> search record type is 
     *  supported. 
     *
     *  @param  itemSearchRecordType a <code> Type </code> indicating an 
     *          <code> Item </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> itemSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsItemSearchRecordType(org.osid.type.Type itemSearchRecordType) {
        return (this.itemSearchRecordTypes.contains(itemSearchRecordType));
    }


    /**
     *  Adds support for an item search record type.
     *
     *  @param itemSearchRecordType an item search record type
     *  @throws org.osid.NullArgumentException
     *  <code>itemSearchRecordType</code> is <code>null</code>
     */

    protected void addItemSearchRecordType(org.osid.type.Type itemSearchRecordType) {
        this.itemSearchRecordTypes.add(itemSearchRecordType);
        return;
    }


    /**
     *  Removes support for an item search record type.
     *
     *  @param itemSearchRecordType an item search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>itemSearchRecordType</code> is <code>null</code>
     */

    protected void removeItemSearchRecordType(org.osid.type.Type itemSearchRecordType) {
        this.itemSearchRecordTypes.remove(itemSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Category </code> record types. 
     *
     *  @return a list containing the supported <code> Category </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCategoryRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.categoryRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Category </code> record type is supported. 
     *
     *  @param  categoryRecordType a <code> Type </code> indicating an <code> 
     *          Category </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> categoryRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCategoryRecordType(org.osid.type.Type categoryRecordType) {
        return (this.categoryRecordTypes.contains(categoryRecordType));
    }


    /**
     *  Adds support for a category record type.
     *
     *  @param categoryRecordType a category record type
     *  @throws org.osid.NullArgumentException
     *  <code>categoryRecordType</code> is <code>null</code>
     */

    protected void addCategoryRecordType(org.osid.type.Type categoryRecordType) {
        this.categoryRecordTypes.add(categoryRecordType);
        return;
    }


    /**
     *  Removes support for a category record type.
     *
     *  @param categoryRecordType a category record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>categoryRecordType</code> is <code>null</code>
     */

    protected void removeCategoryRecordType(org.osid.type.Type categoryRecordType) {
        this.categoryRecordTypes.remove(categoryRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Category </code> search record types. 
     *
     *  @return a list containing the supported <code> Category </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCategorySearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.categorySearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Category </code> search record type is 
     *  supported. 
     *
     *  @param  categorySearchRecordType a <code> Type </code> indicating an 
     *          <code> Category </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> categorySearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCategorySearchRecordType(org.osid.type.Type categorySearchRecordType) {
        return (this.categorySearchRecordTypes.contains(categorySearchRecordType));
    }


    /**
     *  Adds support for a category search record type.
     *
     *  @param categorySearchRecordType a category search record type
     *  @throws org.osid.NullArgumentException
     *  <code>categorySearchRecordType</code> is <code>null</code>
     */

    protected void addCategorySearchRecordType(org.osid.type.Type categorySearchRecordType) {
        this.categorySearchRecordTypes.add(categorySearchRecordType);
        return;
    }


    /**
     *  Removes support for a category search record type.
     *
     *  @param categorySearchRecordType a category search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>categorySearchRecordType</code> is <code>null</code>
     */

    protected void removeCategorySearchRecordType(org.osid.type.Type categorySearchRecordType) {
        this.categorySearchRecordTypes.remove(categorySearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Entry </code> record types. 
     *
     *  @return a list containing the supported <code> Entry </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getEntryRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.entryRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Entry </code> record type is supported. 
     *
     *  @param  entryRecordType a <code> Type </code> indicating an <code> 
     *          Entry </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> entryRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsEntryRecordType(org.osid.type.Type entryRecordType) {
        return (this.entryRecordTypes.contains(entryRecordType));
    }


    /**
     *  Adds support for an entry record type.
     *
     *  @param entryRecordType an entry record type
     *  @throws org.osid.NullArgumentException
     *  <code>entryRecordType</code> is <code>null</code>
     */

    protected void addEntryRecordType(org.osid.type.Type entryRecordType) {
        this.entryRecordTypes.add(entryRecordType);
        return;
    }


    /**
     *  Removes support for an entry record type.
     *
     *  @param entryRecordType an entry record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>entryRecordType</code> is <code>null</code>
     */

    protected void removeEntryRecordType(org.osid.type.Type entryRecordType) {
        this.entryRecordTypes.remove(entryRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Entry </code> search record types. 
     *
     *  @return a list containing the supported <code> Entry </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getEntrySearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.entrySearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Entry </code> search record type is 
     *  supported. 
     *
     *  @param  entrySearchRecordType a <code> Type </code> indicating an 
     *          <code> Entry </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> entrySearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsEntrySearchRecordType(org.osid.type.Type entrySearchRecordType) {
        return (this.entrySearchRecordTypes.contains(entrySearchRecordType));
    }


    /**
     *  Adds support for an entry search record type.
     *
     *  @param entrySearchRecordType an entry search record type
     *  @throws org.osid.NullArgumentException
     *  <code>entrySearchRecordType</code> is <code>null</code>
     */

    protected void addEntrySearchRecordType(org.osid.type.Type entrySearchRecordType) {
        this.entrySearchRecordTypes.add(entrySearchRecordType);
        return;
    }


    /**
     *  Removes support for an entry search record type.
     *
     *  @param entrySearchRecordType an entry search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>entrySearchRecordType</code> is <code>null</code>
     */

    protected void removeEntrySearchRecordType(org.osid.type.Type entrySearchRecordType) {
        this.entrySearchRecordTypes.remove(entrySearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Period </code> record types. 
     *
     *  @return a list containing the supported <code> Period </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPeriodRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.periodRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Period </code> record type is supported. 
     *
     *  @param  periodRecordType a <code> Type </code> indicating a <code> 
     *          Period </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> periodRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPeriodRecordType(org.osid.type.Type periodRecordType) {
        return (this.periodRecordTypes.contains(periodRecordType));
    }


    /**
     *  Adds support for a period record type.
     *
     *  @param periodRecordType a period record type
     *  @throws org.osid.NullArgumentException
     *  <code>periodRecordType</code> is <code>null</code>
     */

    protected void addPeriodRecordType(org.osid.type.Type periodRecordType) {
        this.periodRecordTypes.add(periodRecordType);
        return;
    }


    /**
     *  Removes support for a period record type.
     *
     *  @param periodRecordType a period record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>periodRecordType</code> is <code>null</code>
     */

    protected void removePeriodRecordType(org.osid.type.Type periodRecordType) {
        this.periodRecordTypes.remove(periodRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Period </code> search record types. 
     *
     *  @return a list containing the supported <code> Period </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPeriodSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.periodSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Period </code> search record type is 
     *  supported. 
     *
     *  @param  periodSearchRecordType a <code> Type </code> indicating a 
     *          <code> Period </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> periodSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPeriodSearchRecordType(org.osid.type.Type periodSearchRecordType) {
        return (this.periodSearchRecordTypes.contains(periodSearchRecordType));
    }


    /**
     *  Adds support for a period search record type.
     *
     *  @param periodSearchRecordType a period search record type
     *  @throws org.osid.NullArgumentException
     *  <code>periodSearchRecordType</code> is <code>null</code>
     */

    protected void addPeriodSearchRecordType(org.osid.type.Type periodSearchRecordType) {
        this.periodSearchRecordTypes.add(periodSearchRecordType);
        return;
    }


    /**
     *  Removes support for a period search record type.
     *
     *  @param periodSearchRecordType a period search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>periodSearchRecordType</code> is <code>null</code>
     */

    protected void removePeriodSearchRecordType(org.osid.type.Type periodSearchRecordType) {
        this.periodSearchRecordTypes.remove(periodSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Business </code> record types. 
     *
     *  @return a list containing the supported <code> Business </code> types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBusinessRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.businessRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Business </code> record type is supported. 
     *
     *  @param  businessRecordType a <code> Type </code> indicating an <code> 
     *          Business </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> businessRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBusinessRecordType(org.osid.type.Type businessRecordType) {
        return (this.businessRecordTypes.contains(businessRecordType));
    }


    /**
     *  Adds support for a business record type.
     *
     *  @param businessRecordType a business record type
     *  @throws org.osid.NullArgumentException
     *  <code>businessRecordType</code> is <code>null</code>
     */

    protected void addBusinessRecordType(org.osid.type.Type businessRecordType) {
        this.businessRecordTypes.add(businessRecordType);
        return;
    }


    /**
     *  Removes support for a business record type.
     *
     *  @param businessRecordType a business record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>businessRecordType</code> is <code>null</code>
     */

    protected void removeBusinessRecordType(org.osid.type.Type businessRecordType) {
        this.businessRecordTypes.remove(businessRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Business </code> search record types. 
     *
     *  @return a list containing the supported <code> Business </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBusinessSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.businessSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Business </code> search record type is 
     *  supported. 
     *
     *  @param  businessSearchRecordType a <code> Type </code> indicating an 
     *          <code> Business </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> businessSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBusinessSearchRecordType(org.osid.type.Type businessSearchRecordType) {
        return (this.businessSearchRecordTypes.contains(businessSearchRecordType));
    }


    /**
     *  Adds support for a business search record type.
     *
     *  @param businessSearchRecordType a business search record type
     *  @throws org.osid.NullArgumentException
     *  <code>businessSearchRecordType</code> is <code>null</code>
     */

    protected void addBusinessSearchRecordType(org.osid.type.Type businessSearchRecordType) {
        this.businessSearchRecordTypes.add(businessSearchRecordType);
        return;
    }


    /**
     *  Removes support for a business search record type.
     *
     *  @param businessSearchRecordType a business search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>businessSearchRecordType</code> is <code>null</code>
     */

    protected void removeBusinessSearchRecordType(org.osid.type.Type businessSearchRecordType) {
        this.businessSearchRecordTypes.remove(businessSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the customer 
     *  lookup service. 
     *
     *  @return a <code> CustomerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCustomerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerLookupSession getCustomerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getCustomerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the customer 
     *  lookup service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CustomerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCustomerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerLookupSession getCustomerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getCustomerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the customer 
     *  lookup service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the business 
     *  @return a <code> CustomerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCustomerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerLookupSession getCustomerLookupSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getCustomerLookupSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the customer 
     *  lookup service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the business 
     *  @param  proxy proxy 
     *  @return a <code> CustomerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCustomerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerLookupSession getCustomerLookupSessionForBusiness(org.osid.id.Id businessId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getCustomerLookupSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the customer query 
     *  service. 
     *
     *  @return a <code> CustomerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCustomerQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerQuerySession getCustomerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getCustomerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the customer query 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CustomerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCustomerQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerQuerySession getCustomerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getCustomerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the customer query 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> CustomerQuerySession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCustomerQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerQuerySession getCustomerQuerySessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getCustomerQuerySessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the customer query 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> CustomerQuerySession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCustomerQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerQuerySession getCustomerQuerySessionForBusiness(org.osid.id.Id businessId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getCustomerQuerySessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the customer 
     *  search service. 
     *
     *  @return a <code> CustomerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCustomerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerSearchSession getCustomerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getCustomerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the customer 
     *  search service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CustomerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCustomerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerSearchSession getCustomerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getCustomerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the customer 
     *  search service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> CustomerSearchSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCustomerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerSearchSession getCustomerSearchSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getCustomerSearchSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the customer 
     *  search service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> CustomerSearchSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCustomerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerSearchSession getCustomerSearchSessionForBusiness(org.osid.id.Id businessId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getCustomerSearchSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the customer 
     *  administration service. 
     *
     *  @return a <code> CustomerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCustomerAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerAdminSession getCustomerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getCustomerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the customer 
     *  administration service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CustomerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCustomerAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerAdminSession getCustomerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getCustomerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the customer 
     *  administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> CustomerAdminSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCustomerAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerAdminSession getCustomerAdminSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getCustomerAdminSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the customer 
     *  administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> CustomerAdminSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCustomerAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerAdminSession getCustomerAdminSessionForBusiness(org.osid.id.Id businessId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getCustomerAdminSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the customer 
     *  notification service. 
     *
     *  @param  customerReceiver the notification callback 
     *  @return a <code> CustomerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> customerReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCustomerNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerNotificationSession getCustomerNotificationSession(org.osid.billing.CustomerReceiver customerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getCustomerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the customer 
     *  notification service. 
     *
     *  @param  customerReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return a <code> CustomerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> customerReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCustomerNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerNotificationSession getCustomerNotificationSession(org.osid.billing.CustomerReceiver customerReceiver, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getCustomerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the customer 
     *  notification service for the given business. 
     *
     *  @param  customerReceiver the notification callback 
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> CustomerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> customerReceiver </code> 
     *          or <code> businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCustomerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerNotificationSession getCustomerNotificationSessionForBusiness(org.osid.billing.CustomerReceiver customerReceiver, 
                                                                                                  org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getCustomerNotificationSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the customer 
     *  notification service for the given business. 
     *
     *  @param  customerReceiver the notification callback 
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> CustomerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> customerReceiver, 
     *          businessId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCustomerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerNotificationSession getCustomerNotificationSessionForBusiness(org.osid.billing.CustomerReceiver customerReceiver, 
                                                                                                  org.osid.id.Id businessId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getCustomerNotificationSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup customer/catalog 
     *  mappings. 
     *
     *  @return a <code> CustomerCustomerBusinessSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCustomerBusiness() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerBusinessSession getCustomerBusinessSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getCustomerBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup customer/catalog 
     *  mappings. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CustomerBusinessSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCustomerBusiness() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerBusinessSession getCustomerBusinessSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getCustomerBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  customers to businesses. 
     *
     *  @return a <code> CustomerBusinessAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCustomerBusinessAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerBusinessAssignmentSession getCustomerBusinessAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getCustomerBusinessAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  customers to businesses. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CustomerBusinessAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCustomerBusinessAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerBusinessAssignmentSession getCustomerBusinessAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getCustomerBusinessAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the customer smart 
     *  business service. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> CustomerSmartBusinessSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCustomerSmartBusiness() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerSmartBusinessSession getCustomerSmartBusinessSession(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getCustomerSmartBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the customer smart 
     *  business service. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> CustomerSmartBusinessSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCustomerSmartBusiness() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerSmartBusinessSession getCustomerSmartBusinessSession(org.osid.id.Id businessId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getCustomerSmartBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item lookup 
     *  service. 
     *
     *  @return an <code> ItemSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.ItemLookupSession getItemLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getItemLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item lookup 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> ItemSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.ItemLookupSession getItemLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getItemLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item lookup 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the business 
     *  @return an <code> ItemLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.ItemLookupSession getItemLookupSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getItemLookupSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item lookup 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the business 
     *  @param  proxy proxy 
     *  @return an <code> ItemLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.ItemLookupSession getItemLookupSessionForBusiness(org.osid.id.Id businessId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getItemLookupSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item query 
     *  service. 
     *
     *  @return an <code> ItemQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerQuerySession getItemQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getItemQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item query 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> ItemQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.ItemQuerySession getItemQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getItemQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item query 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return an <code> ItemQuerySession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerQuerySession getItemQuerySessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getItemQuerySessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item query 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return an <code> ItemQuerySession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.ItemQuerySession getItemQuerySessionForBusiness(org.osid.id.Id businessId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getItemQuerySessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item search 
     *  service. 
     *
     *  @return an <code> ItemSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.ItemSearchSession getItemSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getItemSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item search 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> ItemSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.ItemSearchSession getItemSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getItemSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item search 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return an <code> ItemSearchSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.ItemSearchSession getItemSearchSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getItemSearchSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item search 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return an <code> ItemSearchSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.ItemSearchSession getItemSearchSessionForBusiness(org.osid.id.Id businessId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getItemSearchSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item 
     *  administration service. 
     *
     *  @return an <code> ItemAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.ItemAdminSession getItemAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getItemAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item 
     *  administration service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> ItemAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.ItemAdminSession getItemAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getItemAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item 
     *  administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return an <code> ItemAdminSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.ItemAdminSession getItemAdminSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getItemAdminSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item 
     *  administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return an <code> ItemAdminSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.ItemAdminSession getItemAdminSessionForBusiness(org.osid.id.Id businessId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getItemAdminSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item 
     *  notification service. 
     *
     *  @param  itemReceiver the notification callback 
     *  @return an <code> ItemNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> itemReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsItemNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.ItemNotificationSession getItemNotificationSession(org.osid.billing.ItemReceiver itemReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getItemNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item 
     *  notification service. 
     *
     *  @param  itemReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return an <code> ItemNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> itemReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsItemNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.ItemNotificationSession getItemNotificationSession(org.osid.billing.ItemReceiver itemReceiver, 
                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getItemNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item 
     *  notification service for the given business. 
     *
     *  @param  itemReceiver the notification callback 
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return an <code> ItemNotificationSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> itemReceiver </code> or 
     *          <code> businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsItemNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.ItemNotificationSession getItemNotificationSessionForBusiness(org.osid.billing.ItemReceiver itemReceiver, 
                                                                                          org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getItemNotificationSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item 
     *  notification service for the given business. 
     *
     *  @param  itemReceiver the notification callback 
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return an <code> ItemNotificationSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> itemReceiver, businessId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsItemNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.ItemNotificationSession getItemNotificationSessionForBusiness(org.osid.billing.ItemReceiver itemReceiver, 
                                                                                          org.osid.id.Id businessId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getItemNotificationSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup item/catalog mappings. 
     *
     *  @return an <code> ItemBusinessSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemBusiness() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.ItemBusinessSession getItemBusinessSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getItemBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup item/catalog mappings. 
     *
     *  @param  proxy proxy 
     *  @return an <code> ItemCatalogSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemCatalog() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.ItemBusinessSession getItemBusinessSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getItemBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning items to 
     *  businesses. 
     *
     *  @return an <code> ItemBusinessAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsItemBusinessAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.billing.ItemBusinessAssignmentSession getItemBusinessAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getItemBusinessAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning items to 
     *  businesses. 
     *
     *  @param  proxy proxy 
     *  @return an <code> ItemCatalogAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsItemCatalogAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.billing.ItemBusinessAssignmentSession getItemBusinessAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getItemBusinessAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item smart 
     *  business service. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return an <code> ItemSmartBusinessSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsItemSmartBusiness() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.ItemSmartBusinessSession getItemSmartBusinessSession(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getItemSmartBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item smart 
     *  business service. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return an <code> ItemSmartBusinessSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsItemSmartBusiness() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.ItemSmartBusinessSession getItemSmartBusinessSession(org.osid.id.Id businessId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getItemSmartBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the category 
     *  lookup service. 
     *
     *  @return a <code> CategorySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCategoryLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CategoryLookupSession getCategoryLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getCategoryLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the category 
     *  lookup service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CategorySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCategoryLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CategoryLookupSession getCategoryLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getCategoryLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the category 
     *  lookup service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the business 
     *  @return a <code> CategoryLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCategoryLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CategoryLookupSession getCategoryLookupSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getCategoryLookupSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the category 
     *  lookup service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the business 
     *  @param  proxy proxy 
     *  @return a <code> CategoryLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCategoryLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CategoryLookupSession getCategoryLookupSessionForBusiness(org.osid.id.Id businessId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getCategoryLookupSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the category query 
     *  service. 
     *
     *  @return a <code> CategoryQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCategoryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerQuerySession getCategoryQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getCategoryQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the category query 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CategoryQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCategoryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CategoryQuerySession getCategoryQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getCategoryQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the category query 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> CategoryQuerySession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCategoryQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerQuerySession getCategoryQuerySessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getCategoryQuerySessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the category query 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> CategoryQuerySession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCategoryQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CategoryQuerySession getCategoryQuerySessionForBusiness(org.osid.id.Id businessId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getCategoryQuerySessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the category 
     *  search service. 
     *
     *  @return a <code> CategorySearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCategorySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CategorySearchSession getCategorySearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getCategorySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the category 
     *  search service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CategorySearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCategorySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CategorySearchSession getCategorySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getCategorySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the category 
     *  search service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> CategorySearchSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCategorySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CategorySearchSession getCategorySearchSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getCategorySearchSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the category 
     *  search service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> CategorySearchSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCategorySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CategorySearchSession getCategorySearchSessionForBusiness(org.osid.id.Id businessId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getCategorySearchSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the category 
     *  administration service. 
     *
     *  @return a <code> CategoryAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCategoryAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CategoryAdminSession getCategoryAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getCategoryAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the category 
     *  administration service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CategoryAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCategoryAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CategoryAdminSession getCategoryAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getCategoryAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the category 
     *  administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> CategoryAdminSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCategoryAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CategoryAdminSession getCategoryAdminSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getCategoryAdminSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the category 
     *  administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> CategoryAdminSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCategoryAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CategoryAdminSession getCategoryAdminSessionForBusiness(org.osid.id.Id businessId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getCategoryAdminSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the category 
     *  notification service. 
     *
     *  @param  categoryReceiver the notification callback 
     *  @return a <code> CategoryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> categoryReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCategoryNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CategoryNotificationSession getCategoryNotificationSession(org.osid.billing.CategoryReceiver categoryReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getCategoryNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the category 
     *  notification service. 
     *
     *  @param  categoryReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return a <code> CategoryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> categoryReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCategoryNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CategoryNotificationSession getCategoryNotificationSession(org.osid.billing.CategoryReceiver categoryReceiver, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getCategoryNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the category 
     *  notification service for the given business. 
     *
     *  @param  categoryReceiver the notification callback 
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> CategoryNotificationSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> categoryReceiver </code> 
     *          or <code> businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCategoryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CategoryNotificationSession getCategoryNotificationSessionForBusiness(org.osid.billing.CategoryReceiver categoryReceiver, 
                                                                                                  org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getCategoryNotificationSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the category 
     *  notification service for the given business. 
     *
     *  @param  categoryReceiver the notification callback 
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> CategoryNotificationSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> categoryReceiver, 
     *          businessId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCategoryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CategoryNotificationSession getCategoryNotificationSessionForBusiness(org.osid.billing.CategoryReceiver categoryReceiver, 
                                                                                                  org.osid.id.Id businessId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getCategoryNotificationSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup category/catalog 
     *  mappings. 
     *
     *  @return a <code> CategoryBusinessSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCategoryBusiness() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CategoryBusinessSession getCategoryBusinessSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getCategoryBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup category/catalog 
     *  mappings. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CategoryBusinessSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCategoryBusiness() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CategoryBusinessSession getCategoryBusinessSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getCategoryBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  categories to businesses. 
     *
     *  @return a <code> CategoryBusinessAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCategoryBusinessAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.billing.CategoryBusinessAssignmentSession getCategoryBusinessAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getCategoryBusinessAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  categories to businesses. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CategoryBusinessAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCategoryBusinessAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.billing.CategoryBusinessAssignmentSession getCategoryBusinessAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getCategoryBusinessAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the category smart 
     *  business service. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> CategorySmartBusinessSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCategorySmartBusiness() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CategorySmartBusinessSession getCategorySmartBusinessSession(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getCategorySmartBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the category smart 
     *  business service. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> CategorySmartBusinessSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCategorySmartBusiness() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CategorySmartBusinessSession getCategorySmartBusinessSession(org.osid.id.Id businessId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getCategorySmartBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the entry lookup 
     *  service. 
     *
     *  @return an <code> EntryLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.EntryLookupSession getEntryLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getEntryLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the entry lookup 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> EntryLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.EntryLookupSession getEntryLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getEntryLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the entry lookup 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return an <code> EntryLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.EntryLookupSession getEntryLookupSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getEntryLookupSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the entry lookup 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return an <code> EntryLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.EntryLookupSession getEntryLookupSessionForBusiness(org.osid.id.Id businessId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getEntryLookupSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the entry query 
     *  service. 
     *
     *  @return an <code> EntryQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.EntryQuerySession getEntryQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getEntryQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the entry query 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> EntryQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.EntryQuerySession getEntryQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getEntryQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the entry query 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return an <code> EntryQuerySession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.EntryQuerySession getEntryQuerySessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getEntryQuerySessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the entry query 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return an <code> EntryQuerySession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.EntryQuerySession getEntryQuerySessionForBusiness(org.osid.id.Id businessId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getEntryQuerySessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the entry search 
     *  service. 
     *
     *  @return an <code> EntrySearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntrySearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.EntrySearchSession getEntrySearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getEntrySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the entry search 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> EntrySearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntrySearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.EntrySearchSession getEntrySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getEntrySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the entry search 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return an <code> EntrySearchSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntrySearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.EntrySearchSession getEntrySearchSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getEntrySearchSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the entry search 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return an <code> EntrySearchSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntrySearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.EntrySearchSession getEntrySearchSessionForBusiness(org.osid.id.Id businessId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getEntrySearchSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the entry 
     *  administration service. 
     *
     *  @return an <code> EntryAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.EntryAdminSession getEntryAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getEntryAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the entry 
     *  administration service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> EntryAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.EntryAdminSession getEntryAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getEntryAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the entry 
     *  administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return an <code> EntryAdminSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.EntryAdminSession getEntryAdminSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getEntryAdminSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the entry 
     *  administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return an <code> EntryAdminSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.EntryAdminSession getEntryAdminSessionForBusiness(org.osid.id.Id businessId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getEntryAdminSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the entry 
     *  notification service. 
     *
     *  @param  entryReceiver the notification callback 
     *  @return an <code> EntryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> entryReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntryNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.EntryNotificationSession getEntryNotificationSession(org.osid.billing.EntryReceiver entryReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getEntryNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the entry 
     *  notification service. 
     *
     *  @param  entryReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return an <code> EntryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> entryReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntryNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.EntryNotificationSession getEntryNotificationSession(org.osid.billing.EntryReceiver entryReceiver, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getEntryNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the entry 
     *  notification service for the given business. 
     *
     *  @param  entryReceiver the notification callback 
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return an <code> EntryNotificationSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> entryReceiver </code> or 
     *          <code> businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.EntryNotificationSession getEntryNotificationSessionForBusiness(org.osid.billing.EntryReceiver entryReceiver, 
                                                                                            org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getEntryNotificationSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the entry 
     *  notification service for the given business. 
     *
     *  @param  entryReceiver the notification callback 
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return an <code> EntryNotificationSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> entryReceiver, 
     *          businessId, </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.EntryNotificationSession getEntryNotificationSessionForBusiness(org.osid.billing.EntryReceiver entryReceiver, 
                                                                                            org.osid.id.Id businessId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getEntryNotificationSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup entry/catalog mappings. 
     *
     *  @return an <code> EntryBusinessSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryBusiness() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.EntryBusinessSession getEntryBusinessSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getEntryBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup entry/catalog mappings. 
     *
     *  @param  proxy proxy 
     *  @return an <code> EntryCatalogSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryCatalog() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.EntryBusinessSession getEntryBusinessSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getEntryBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning entries 
     *  to businesses. 
     *
     *  @return an <code> EntryBusinessAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntryBusinessAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.billing.EntryBusinessAssignmentSession getEntryBusinessAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getEntryBusinessAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning entries 
     *  to businesses. 
     *
     *  @param  proxy proxy 
     *  @return an <code> EntryCatalogAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntryCatalogAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.billing.EntryBusinessAssignmentSession getEntryBusinessAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getEntryBusinessAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the entry smart 
     *  business service. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return an <code> EntrySmartBusinessSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntrySmartBusiness() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.EntrySmartBusinessSession getEntrySmartBusinessSession(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getEntrySmartBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the entry smart 
     *  business service. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return an <code> EntrySmartBusinessSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntrySmartBusiness() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.EntrySmartBusinessSession getEntrySmartBusinessSession(org.osid.id.Id businessId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getEntrySmartBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the period lookup 
     *  service. 
     *
     *  @return a <code> PeriodLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPeriodLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.PeriodLookupSession getPeriodLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getPeriodLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the period lookup 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> PeriodLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPeriodLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.PeriodLookupSession getPeriodLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getPeriodLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the period lookup 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> PeriodLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPeriodLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.PeriodLookupSession getPeriodLookupSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getPeriodLookupSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the period lookup 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> PeriodLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPeriodLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.PeriodLookupSession getPeriodLookupSessionForBusiness(org.osid.id.Id businessId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getPeriodLookupSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the period query 
     *  service. 
     *
     *  @return a <code> PeriodQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPeriodQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.PeriodQuerySession getPeriodQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getPeriodQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the period query 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> PeriodQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPeriodQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.PeriodQuerySession getPeriodQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getPeriodQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the period query 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> PeriodQuerySession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPeriodQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.PeriodQuerySession getPeriodQuerySessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getPeriodQuerySessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the period query 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> PeriodQuerySession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPeriodQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.PeriodQuerySession getPeriodQuerySessionForBusiness(org.osid.id.Id businessId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getPeriodQuerySessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the period search 
     *  service. 
     *
     *  @return a <code> PeriodSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPeriodSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.PeriodSearchSession getPeriodSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getPeriodSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the period search 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> PeriodSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPeriodSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.PeriodSearchSession getPeriodSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getPeriodSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the period search 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> PeriodSearchSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPeriodSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.PeriodSearchSession getPeriodSearchSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getPeriodSearchSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the period search 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> PeriodSearchSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPeriodSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.PeriodSearchSession getPeriodSearchSessionForBusiness(org.osid.id.Id businessId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getPeriodSearchSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the period 
     *  administration service. 
     *
     *  @return a <code> PeriodAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPeriodAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.PeriodAdminSession getPeriodAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getPeriodAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the period 
     *  administration service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> PeriodAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPeriodAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.PeriodAdminSession getPeriodAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getPeriodAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the period 
     *  administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> PeriodAdminSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPeriodAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.PeriodAdminSession getPeriodAdminSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getPeriodAdminSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the period 
     *  administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> PeriodAdminSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPeriodAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.PeriodAdminSession getPeriodAdminSessionForBusiness(org.osid.id.Id businessId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getPeriodAdminSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the period 
     *  notification service. 
     *
     *  @param  periodReceiver the notification callback 
     *  @return a <code> PeriodNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> periodReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPeriodNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.PeriodNotificationSession getPeriodNotificationSession(org.osid.billing.PeriodReceiver periodReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getPeriodNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the period 
     *  notification service. 
     *
     *  @param  periodReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return a <code> PeriodNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> periodReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPeriodNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.PeriodNotificationSession getPeriodNotificationSession(org.osid.billing.PeriodReceiver periodReceiver, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getPeriodNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the period 
     *  notification service for the given business. 
     *
     *  @param  periodReceiver the notification callback 
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> PeriodNotificationSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> periodReceiver </code> 
     *          or <code> businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPeriodNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.PeriodNotificationSession getPeriodNotificationSessionForBusiness(org.osid.billing.PeriodReceiver periodReceiver, 
                                                                                              org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getPeriodNotificationSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the period 
     *  notification service for the given business. 
     *
     *  @param  periodReceiver the notification callback 
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> PeriodNotificationSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> periodReceiver, 
     *          businessId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPeriodNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.PeriodNotificationSession getPeriodNotificationSessionForBusiness(org.osid.billing.PeriodReceiver periodReceiver, 
                                                                                              org.osid.id.Id businessId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getPeriodNotificationSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup period/catalog mappings. 
     *
     *  @return a <code> PeriodBusinessSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPeriodBusiness() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.PeriodBusinessSession getPeriodBusinessSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getPeriodBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup period/catalog mappings. 
     *
     *  @param  proxy proxy 
     *  @return a <code> PeriodCatalogSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPeriodCatalog() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.PeriodBusinessSession getPeriodBusinessSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getPeriodBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning periods 
     *  to businesses. 
     *
     *  @return a <code> PeriodBusinessAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPeriodBusinessAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.billing.PeriodBusinessAssignmentSession getPeriodBusinessAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getPeriodBusinessAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning periods 
     *  to businesses. 
     *
     *  @param  proxy proxy 
     *  @return a <code> PeriodCatalogAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPeriodCatalogAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.billing.PeriodBusinessAssignmentSession getPeriodBusinessAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getPeriodBusinessAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the period smart 
     *  business service. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> PeriodSmartBusinessSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPeriodSmartBusiness() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.PeriodSmartBusinessSession getPeriodSmartBusinessSession(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getPeriodSmartBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the period smart 
     *  business service. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> PeriodSmartBusinessSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPeriodSmartBusiness() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.PeriodSmartBusinessSession getPeriodSmartBusinessSession(org.osid.id.Id businessId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getPeriodSmartBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the business 
     *  lookup service. 
     *
     *  @return a <code> BusinessLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBusinessLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.BusinessLookupSession getBusinessLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getBusinessLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the business 
     *  lookup service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> BusinessLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBusinessLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.BusinessLookupSession getBusinessLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getBusinessLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the business query 
     *  service. 
     *
     *  @return a <code> BusinessQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBusinessQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.BusinessQuerySession getBusinessQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getBusinessQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the business query 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> BusinessQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBusinessQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.BusinessQuerySession getBusinessQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getBusinessQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the business 
     *  search service. 
     *
     *  @return a <code> BusinessSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBusinessSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.BusinessSearchSession getBusinessSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getBusinessSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the business 
     *  search service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> BusinessSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBusinessSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.BusinessSearchSession getBusinessSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getBusinessSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the business 
     *  administrative service. 
     *
     *  @return a <code> BusinessAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBusinessAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.BusinessAdminSession getBusinessAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getBusinessAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the business 
     *  administrative service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> BusinessAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBusinessAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.BusinessAdminSession getBusinessAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getBusinessAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the business 
     *  notification service. 
     *
     *  @param  businessReceiver the notification callback 
     *  @return a <code> BusinessNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> businessReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBusinessNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.BusinessNotificationSession getBusinessNotificationSession(org.osid.billing.BusinessReceiver businessReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getBusinessNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the business 
     *  notification service. 
     *
     *  @param  businessReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return a <code> BusinessNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> businessReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBusinessNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.BusinessNotificationSession getBusinessNotificationSession(org.osid.billing.BusinessReceiver businessReceiver, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getBusinessNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the business 
     *  hierarchy service. 
     *
     *  @return a <code> BusinessHierarchySession </code> for businesses 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBusinessHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.BusinessHierarchySession getBusinessHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getBusinessHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the business 
     *  hierarchy service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> BusinessHierarchySession </code> for businesses 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBusinessHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.BusinessHierarchySession getBusinessHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getBusinessHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the business 
     *  hierarchy design service. 
     *
     *  @return a <code> HierarchyDesignSession </code> for businesses 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBusinessHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.billing.BusinessHierarchyDesignSession getBusinessHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getBusinessHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the business 
     *  hierarchy design service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> HierarchyDesignSession </code> for businesses 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBusinessHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.billing.BusinessHierarchyDesignSession getBusinessHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getBusinessHierarchyDesignSession not implemented");
    }


    /**
     *  Gets a <code> BillingBatchManager. </code> 
     *
     *  @return a <code> BillingBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBillingBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.batch.BillingBatchManager getBillingBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getBillingBatchManager not implemented");
    }


    /**
     *  Gets a <code> BillingBatchProxyManager. </code> 
     *
     *  @return a <code> BillingBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBillingBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.batch.BillingBatchProxyManager getBillingBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getBillingBatchProxyManager not implemented");
    }


    /**
     *  Gets a <code> BillingPaymentManager. </code> 
     *
     *  @return a <code> BillingPaymentManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBillingPayment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.BillingPaymentManager getBillingPaymentManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingManager.getBillingPaymentManager not implemented");
    }


    /**
     *  Gets a <code> BillingPaymentProxyManager. </code> 
     *
     *  @return a <code> BillingPaymentProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBillingPayment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.BillingPaymentProxyManager getBillingPaymentProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.BillingProxyManager.getBillingPaymentProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.customerRecordTypes.clear();
        this.customerRecordTypes.clear();

        this.customerSearchRecordTypes.clear();
        this.customerSearchRecordTypes.clear();

        this.itemRecordTypes.clear();
        this.itemRecordTypes.clear();

        this.itemSearchRecordTypes.clear();
        this.itemSearchRecordTypes.clear();

        this.categoryRecordTypes.clear();
        this.categoryRecordTypes.clear();

        this.categorySearchRecordTypes.clear();
        this.categorySearchRecordTypes.clear();

        this.entryRecordTypes.clear();
        this.entryRecordTypes.clear();

        this.entrySearchRecordTypes.clear();
        this.entrySearchRecordTypes.clear();

        this.periodRecordTypes.clear();
        this.periodRecordTypes.clear();

        this.periodSearchRecordTypes.clear();
        this.periodSearchRecordTypes.clear();

        this.businessRecordTypes.clear();
        this.businessRecordTypes.clear();

        this.businessSearchRecordTypes.clear();
        this.businessSearchRecordTypes.clear();

        return;
    }
}
