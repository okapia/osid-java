//
// AbstractObjectOsidQuery.java
//
//     Defines an OsidObjectQuery.
//
//
// Tom Coppeto
// Okapia
// 20 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines an OsidObjectQuery.
 */

public abstract class AbstractOsidObjectQuery
    extends AbstractOsidIdentifiableQuery
    implements org.osid.OsidObjectQuery {

    private final OsidBrowsableQuery query = new OsidBrowsableQuery();
    

    /**
     *  Adds a display name to match. Multiple display name matches can be 
     *  added to perform a boolean <code> OR </code> among them. 
     *
     *  @param  displayName display name to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> displayName </code> 
     *          is not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> displayName </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchDisplayName(String displayName, 
                                 org.osid.type.Type stringMatchType, 
                                 boolean match) {

        return;
    }


    /**
     *  Matches any object with a display name. 
     *
     *  @param  match <code> true </code> to match any display name, <code> 
     *          false </code> to match objects with no display name 
     */

    @OSID @Override
    public void matchAnyDisplayName(boolean match) {
        return;
    }


    /**
     *  Clears all display name terms. 
     */

    @OSID @Override
    public void clearDisplayNameTerms() {
        return;
    }


    /**
     *  Gets all the display name query terms.
     *
     *  @return a collection of the display name query terms
     */

    protected java.util.Collection<org.osid.search.terms.StringTerm> getDisplayNameTerms() {
        return (new java.util.ArrayList<org.osid.search.terms.StringTerm>(0));
    }


    /**
     *  Adds a description to match. Multiple description matches can be 
     *  added to perform a boolean <code> OR </code> among them. 
     *
     *  @param  description description to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> description </code> 
     *          is not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> description </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchDescription(String description, 
                                 org.osid.type.Type stringMatchType, 
                                 boolean match) {

        return;
    }


    /**
     *  Matches any object with a description. 
     *
     *  @param  match <code> true </code> to match any description, <code> 
     *          false </code> to match objects with no description 
     */

    @OSID @Override
    public void matchAnyDescription(boolean match) {
        return;
    }


    /**
     *  Clears all description terms. 
     */

    @OSID @Override
    public void clearDescriptionTerms() {
        return;
    }


    /**
     *  Gets all the description query terms.
     *
     *  @return a collection of the description query terms
     */

    protected java.util.Collection<org.osid.search.terms.StringTerm> getDescriptionTerms() {
        return (new java.util.ArrayList<org.osid.search.terms.StringTerm>(0));
    }


    /**
     *  Sets a <code> Type </code> for querying objects of a given genus. A 
     *  genus type matches if the specified type is the same genus as the 
     *  object genus type. 
     *
     *  @param  genusType the object genus type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> genusType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGenusType(org.osid.type.Type genusType, boolean match) {
        return;
    }


    /**
     *  Matches an object that has any genus type. 
     *
     *  @param  match <code> true </code> to match any genus type, <code> 
     *          false </code> to match objects with no genus type 
     */

    @OSID @Override
    public void matchAnyGenusType(boolean match) {
        return;
    }


    /**
     *  Clears all genus type terms. 
     */

    @OSID @Override
    public void clearGenusTypeTerms() {
        return;
    }


    /**
     *  Sets a <code> Type </code> for querying objects of a given genus. A 
     *  genus type matches if the specified type is the same genus as the 
     *  object or if the specified type is an ancestor of the object genus in 
     *  a type hierarchy. 
     *
     *  @param  genusType the object genus type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> genusType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchParentGenusType(org.osid.type.Type genusType, boolean match) {
        return;
    }


    /**
     *  Clears all genus type terms. 
     */

    @OSID @Override
    public void clearParentGenusTypeTerms() {
        return;
    }


    /**
     *  Matches an object with a relationship to the given subject. 
     *
     *  @param  subjectId a subject <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> subjectId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSubjectId(org.osid.id.Id subjectId, boolean match) {
        return;
    }


    /**
     *  Clears all subject <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSubjectIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> SubjectQuery </code> is available. 
     *
     *  @return <code> true </code> if a subject query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubjectQuery() {
        return (false);
    }


    /**
     *  Gets the query for a subject. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the subject query 
     *  @throws org.osid.UnimplementedException <code> supportsSubjectQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectQuery getSubjectQuery() {
        throw new org.osid.UnimplementedException("supportsSubjectQuery() is false");
    }


    /**
     *  Matches an object that has any relationship to a <code> Subject. 
     *  </code> 
     *
     *  @param  match <code> true </code> to match any subject, <code> false 
     *          </code> to match objects with no subjects 
     */

    @OSID @Override
    public void matchAnySubject(boolean match) {
        return;
    }


    /**
     *  Clears all subject terms. 
     */

    @OSID @Override
    public void clearSubjectTerms() {
        return;
    }


    /**
     *  Tests if a <code> RelevancyQuery </code> is available to provide
     *  queries about the relationships to <code> Subjects. </code>
     *
     *  @return <code> true </code> if a relevancy entry query is available,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean supportsSubjectRelevancyQuery() {
        return (false);
    }


    /**
     *  Gets the query for a subject relevancy. Multiple retrievals produce a
     *  nested <code> OR </code> term.
     *
     *  @return the relevancy query
     *  @throws org.osid.UnimplementedException <code>
     *          supportsSubjectRelevancyQuery() </code> is <code> false
     *          </code>
     */

    @OSID @Override
    public org.osid.ontology.RelevancyQuery getSubjectRelevancyQuery() {
        throw new org.osid.UnimplementedException("supportsRelevancyQuery() is false");
    }


    /**
     *  Clears all subject relevancy terms.
     */

    @OSID @Override
    public void clearSubjectRelevancyTerms() {
        return;
    }


    /**
     *  Matches an object mapped to the given state.
     *
     *  @param  stateId a state <code> Id </code>
     *  @param  match <code> true </code> for a positive match, <code> false
     *          </code> for a negative match
     *  @throws org.osid.NullArgumentException <code> stateId </code> is
     *          <code> null </code>
     */

    @OSID @Override
    public void matchStateId(org.osid.id.Id stateId, boolean match) {
        return;
    }


    /**
     *  Clears all state <code> Id </code> terms.
     */

    @OSID @Override
    public void clearStateIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> StateQuery </code> is available to provide queries
     *  of processed objects.
     *
     *  @return <code> true </code> if a state query is available, <code>
     *          false </code> otherwise
     */

    @OSID @Override
    public boolean supportsStateQuery() {
        return (false);
    }


    /**
     *  Gets the query for a state. Multiple retrievals produce a nested
     *  <code> OR </code> term.
     *
     *  @return the journal entry query
     *  @throws org.osid.UnimplementedException <code> supportsStateQuery()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.process.StateQuery getStateQuery() {
        throw new org.osid.UnimplementedException("supportsStateQuery() is false");
    }


    /**
     *  Matches an object that has any mapping to a <code> State </code>.
     *
     *  @param  match <code> true </code> to match any state, <code> false
     *          </code> to match objects with no states
     */

    @OSID @Override
    public void matchAnyState(boolean match) {
        return;
    }


    /**
     *  Clears all state terms.
     */

    @OSID @Override
    public void clearStateTerms() {
        return;
    }


    /**
     *  Matches an object that has the given comment.
     *
     *  @param  commentId a comment <code> Id </code>
     *  @param  match <code> true </code> for a positive match, <code> false
     *          </code> for a negative match
     *  @throws org.osid.NullArgumentException <code> commentId </code> is
     *          <code> null </code>
     */

    @OSID @Override
    public void matchCommentId(org.osid.id.Id commentId, boolean match) {
        return;
    }


    /**
     *  Clears all comment <code> Id </code> terms.
     */

    @OSID @Override
    public void clearCommentIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CommentQuery </code> is available.
     *
     *  @return <code> true </code> if a comment query is available, <code>
     *          false </code> otherwise
     */

    @OSID @Override
    public boolean supportsCommentQuery() {
        return (false);
    }


    /**
     *  Gets the query for a comment. Multiple retrievals produce a nested
     *  <code> OR </code> term.
     *
     *  @return the comment query
     *  @throws org.osid.UnimplementedException <code> supportsCommentQuery()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.commenting.CommentQuery getCommentQuery() {
        throw new org.osid.UnimplementedException("supportsCommentQuery() is false");
    }


    /**
     *  Matches an object that has any <code> Comment </code>.
     *
     *  @param  match <code> true </code> to match any comment, <code> false
     *          </code> to match objects with no comments
     */

    @OSID @Override
    public void matchAnyComment(boolean match) {
        return;
    }


    /**
     *  Clears all comment terms.
     */

    @OSID @Override
    public void clearCommentTerms() {
        return;
    }


    /**
     *  Matches an object that has the given journal entry.
     *
     *  @param  journalEntryId a journal entry <code> Id </code>
     *  @param  match <code> true </code> for a positive match, <code> false
     *          </code> for a negative match
     *  @throws org.osid.NullArgumentException <code> journalEntryId </code>
     *          is <code> null </code>
     */

    @OSID @Override
    public void matchJournalEntryId(org.osid.id.Id journalEntryId, boolean match) {
        return;
    }


    /**
     *  Clears all journal entry <code> Id </code> terms.
     */

    @OSID @Override
    public void clearJournalEntryIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> JournalEntry </code> is available to provide queries
     *  of journaled <code> OsidObjects. </code>
     *
     *  @return <code> true </code> if a journal entry query is available,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean supportsJournalEntryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a journal entry. Multiple retrievals produce a
     *  nested <code> OR </code> term.
     *
     *  @return the journal entry query
     *  @throws org.osid.UnimplementedException <code>
     *          supportsJournalEntryQuery() </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryQuery getJournalEntryQuery() {
        throw new org.osid.UnimplementedException("supportsJournalEntryQuery() is false");
    }


    /**
     *  Matches an object that has any <code> JournalEntry </code> in the 
     *  given <code> Journal. </code> 
     *
     *  @param  match <code> true </code> to match any journal entry, <code> 
     *          false </code> to match objects with no journal entries 
     */

    @OSID @Override
    public void matchAnyJournalEntry(boolean match) {
        return;
    }


    /**
     *  Clears all journal entry terms.
     */

    @OSID @Override
    public void clearJournalEntryTerms() {
        return;
    }


    /**
     *  Tests if a <code> StatisticQuery </code> is available to provide
     *  statistical queries.
     *
     *  @return <code> true </code> if a statistic query is available, <code>
     *          false </code> otherwise
     */

    @OSID @Override
    public boolean supportsStatisticQuery() {
        return (false);
    }


    /**
     *  Gets the query for a statistic. Multiple retrievals produce a nested
     *  <code> OR </code> term.
     *
     *  @return the statistic query
     *  @throws org.osid.UnimplementedException <code>
     *          supportsStatisticQuery() </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.metering.StatisticQuery getStatisticQuery() {
        throw new org.osid.UnimplementedException("supportsStatisticQuery() is false");
    }


    /**
     *  Matches an object that has any <code> Statistic </code>.
     *
     *  @param  match <code> true </code> to match any statistics, <code> false
     *          </code> to match objects with no statistics
     */

    @OSID @Override
    public void matchAnyStatistic(boolean match) {
        return;
    }


    /**
     *  Clears all statistic terms.
     */

    @OSID @Override
    public void clearStatisticTerms() {
        return;
    }


    /**
     *  Matches an object that has the given credit.
     *
     *  @param  creditId a credit <code> Id </code>
     *  @param  match <code> true </code> for a positive match, <code> false
     *          </code> for a negative match
     *  @throws org.osid.NullArgumentException <code> creditId </code> is
     *          <code> null </code>
     */

    @OSID @Override
    public void matchCreditId(org.osid.id.Id creditId, boolean match) {
        return;
    }


    /**
     *  Clears all credit <code> Id </code> terms.
     */

    @OSID @Override
    public void clearCreditIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CreditQuery </code> is available to provide queries
     *  of related acknowledgements.
     *
     *  @return <code> true </code> if a credit query is available, <code>
     *          false </code> otherwise
     */

    @OSID @Override
    public boolean supportsCreditQuery() {
        return (false);
    }


    /**
     *  Gets the query for an ackowledgement credit. Multiple retrievals
     *  produce a nested <code> OR </code> term.
     *
     *  @return the credit query
     *  @throws org.osid.UnimplementedException <code> supportsCreditQuery()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditQuery getCreditQuery() {
        throw new org.osid.UnimplementedException("supportsCreditQuery() is false");
    }


    /**
     *  Matches an object that has any <code> Credit </code> for the given
     *  <code> Meter. </code>
     *
     *  @param  match <code> true </code> to match any credit, <code> false
     *          </code> to match objects with no credits
     */

    @OSID @Override
    public void matchAnyCredit(boolean match) {
        return;
    }


    /**
     *  Clears all credit terms.
     */

    @OSID @Override
    public void clearCreditTerms() {
        return;
    }


    /**
     *  Matches an object that has the given relationship.
     *
     *  @param  relationshipId a relationship <code> Id </code>
     *  @param  match <code> true </code> for a positive match, <code> false
     *          </code> for a negative match
     *  @throws org.osid.NullArgumentException <code> relationshipId </code>
     *          is <code> null </code>
     */

    @OSID @Override
    public void matchRelationshipId(org.osid.id.Id relationshipId, boolean match) {
        return;
    }


    /**
     *  Clears all relationship <code> Id </code> terms.
     */

    @OSID @Override
    public void clearRelationshipIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> RelationshipQuery </code> is available.
     *
     *  @return <code> true </code> if a relationship query is available,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean supportsRelationshipQuery() {
        return (false);
    }


    /**
     *  Gets the query for relationship. Multiple retrievals produce a nested
     *  <code> OR </code> term.
     *
     *  @return the relationship query
     *  @throws org.osid.UnimplementedException <code>
     *          supportsRelationshipQuery() </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.relationship.RelationshipQuery getRelationshipQuery() {
        throw new org.osid.UnimplementedException("supportsRelationshipQuery() is false");
    }


    /**
     *  Matches an object that has any <code> Relationship </code>.
     *
     *  @param  match <code> true </code> to match any state, <code> false
     *          </code> to match objects with no states
     */

    @OSID @Override
    public void matchAnyRelationship(boolean match) {
        return;
    }


    /**
     *  Clears all relationship terms.
     */

    @OSID @Override
    public void clearRelationshipTerms() {
        return;
    }


    /**
     *  Matches an object that has a relationship to the given peer <code> Id. 
     *  </code> 
     *
     *  @param  peerId a relationship peer <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> peerId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchRelationshipPeerId(org.osid.id.Id peerId, boolean match) {
        return;
    }


    /**
     *  Clears all relationship <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRelationshipPeerIdTerms() {
        return;
    }


    /**
     *  Sets a <code> Type </code> for querying objects having records 
     *  implementing a given record type. 
     *
     *  @param  recordType a record type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> recordType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRecordType(org.osid.type.Type recordType, boolean match) {
        this.query.matchRecordType(recordType, match);
        return;
    }


    /**
     *  Matches an object that has any record. 
     *
     *  @param  match <code> true </code> to match any record, <code> false 
     *          </code> to match objects with no records 
     */

    @OSID @Override
    public void matchAnyRecord(boolean match) {
        this.query.matchAnyRecord(match);
        return;
    }

    
    /**
     *  Clears all record <code> Type </code> terms. 
     */
    
    @OSID @Override
    public void clearRecordTerms() {
        this.query.clearRecordTerms();
        return;
    }
    

    /**
     *  Gets the record types available in this object.
     *
     *  @return the record types
     */

    @OSID @Override
    public org.osid.type.TypeList getRecordTypes() {
        return (this.query.getRecordTypes());
    }


    /**
     *  Tests if this object supports the given record <code>
     *  Type. </code>
     *
     *  @param  recordType a type 
     *  @return <code> false </code>
     *  @throws org.osid.NullArgumentException <code> recordType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type recordType) {
        return (this.query.hasRecordType(recordType));
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    protected void addRecordType(org.osid.type.Type recordType) {
        this.query.addRecordType(recordType);
        return;
    }


    protected class OsidBrowsableQuery
        extends AbstractOsidBrowsableQuery
        implements org.osid.OsidBrowsableQuery {


        /**
         *  Sets a <code> Type </code> for querying objects having records 
         *  implementing a given record type. 
         *
         *  @param  recordType a record type 
         *  @param  match <code> true </code> for a positive match, <code> false 
         *          </code> for a negative match 
         *  @throws org.osid.NullArgumentException <code> recordType </code> is 
         *          <code> null </code> 
         */

        @OSID @Override
        public void matchRecordType(org.osid.type.Type recordType, boolean match) {
            super.matchRecordType(recordType, match);
            return;
        }


        /**
         *  Matches an object that has any record. 
         *
         *  @param  match <code> true </code> to match any record, <code> false 
         *          </code> to match objects with no records 
         */

        @OSID @Override
        public void matchAnyRecord(boolean match) {
            super.matchAnyRecord(match);
            return;
        }

    
        /**
         *  Clears all record <code> Type </code> terms. 
         */
    
        @OSID @Override
        public void clearRecordTerms() {
            super.clearRecordTerms();
            return;
        }


        /**
         *  Adds a record type.
         *
         *  @param recordType
         *  @throws org.osid.NullArgumentException <code>recordType</code>
         *          is <code>null</code>
         */

        @Override
        protected void addRecordType(org.osid.type.Type recordType) {
            super.addRecordType(recordType);
            return;
        }
    }
}