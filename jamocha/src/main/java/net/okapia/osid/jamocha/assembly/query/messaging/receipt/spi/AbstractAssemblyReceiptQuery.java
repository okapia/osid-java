//
// AbstractAssemblyReceiptQuery.java
//
//     A ReceiptQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.messaging.receipt.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ReceiptQuery that stores terms.
 */

public abstract class AbstractAssemblyReceiptQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.messaging.ReceiptQuery,
               org.osid.messaging.ReceiptQueryInspector,
               org.osid.messaging.ReceiptSearchOrder {

    private final java.util.Collection<org.osid.messaging.records.ReceiptQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.messaging.records.ReceiptQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.messaging.records.ReceiptSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyReceiptQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyReceiptQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the message <code> Id </code> for this query. 
     *
     *  @param  messageId a message <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> messageId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchMessageId(org.osid.id.Id messageId, boolean match) {
        getAssembler().addIdTerm(getMessageIdColumn(), messageId, match);
        return;
    }


    /**
     *  Clears the message <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearMessageIdTerms() {
        getAssembler().clearTerms(getMessageIdColumn());
        return;
    }


    /**
     *  Gets the message <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getMessageIdTerms() {
        return (getAssembler().getIdTerms(getMessageIdColumn()));
    }


    /**
     *  Gets the MessageId column name.
     *
     * @return the column name
     */

    protected String getMessageIdColumn() {
        return ("message_id");
    }


    /**
     *  Tests if a <code> MessageQuery </code> is available. 
     *
     *  @return <code> true </code> if a message query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMessageQuery() {
        return (false);
    }


    /**
     *  Gets the query for a message. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the message query 
     *  @throws org.osid.UnimplementedException <code> supportsMessageQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageQuery getMessageQuery() {
        throw new org.osid.UnimplementedException("supportsMessageQuery() is false");
    }


    /**
     *  Clears the message terms. 
     */

    @OSID @Override
    public void clearMessageTerms() {
        getAssembler().clearTerms(getMessageColumn());
        return;
    }


    /**
     *  Gets the message query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.messaging.MessageQueryInspector[] getMessageTerms() {
        return (new org.osid.messaging.MessageQueryInspector[0]);
    }


    /**
     *  Gets the Message column name.
     *
     * @return the column name
     */

    protected String getMessageColumn() {
        return ("message");
    }


    /**
     *  Matches messages whose received time is between the supplied range 
     *  inclusive. 
     *
     *  @param  startTime start time 
     *  @param  endTime end time 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> startTime </code> is 
     *          greater than <code> endTime </code> 
     *  @throws org.osid.NullArgumentException <code> startTime </code> or 
     *          <code> endTime </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchReceivedTime(org.osid.calendaring.DateTime startTime, 
                                  org.osid.calendaring.DateTime endTime, 
                                  boolean match) {
        getAssembler().addDateTimeRangeTerm(getReceivedTimeColumn(), startTime, endTime, match);
        return;
    }


    /**
     *  Clears the received time terms. 
     */

    @OSID @Override
    public void clearReceivedTimeTerms() {
        getAssembler().clearTerms(getReceivedTimeColumn());
        return;
    }


    /**
     *  Gets the received time query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getReceivedTimeTerms() {
        return (getAssembler().getDateTimeRangeTerms(getReceivedTimeColumn()));
    }


    /**
     *  Specifies a preference for ordering messages by the received time. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByReceivedTime(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getReceivedTimeColumn(), style);
        return;
    }


    /**
     *  Gets the ReceivedTime column name.
     *
     * @return the column name
     */

    protected String getReceivedTimeColumn() {
        return ("received_time");
    }


    /**
     *  Matches the endpoint recipient of the message. 
     *
     *  @param  agentId <code> Id </code> to match 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchReceivingAgentId(org.osid.id.Id agentId, boolean match) {
        getAssembler().addIdTerm(getReceivingAgentIdColumn(), agentId, match);
        return;
    }


    /**
     *  Clears the receiving agent <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearReceivingAgentIdTerms() {
        getAssembler().clearTerms(getReceivingAgentIdColumn());
        return;
    }


    /**
     *  Gets the receiving agent <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getReceivingAgentIdTerms() {
        return (getAssembler().getIdTerms(getReceivingAgentIdColumn()));
    }


    /**
     *  Specifies a preference for ordering messages by receiving agent. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByReceivingAgent(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getReceivingAgentColumn(), style);
        return;
    }


    /**
     *  Gets the ReceivingAgentId column name.
     *
     * @return the column name
     */

    protected String getReceivingAgentIdColumn() {
        return ("receiving_agent_id");
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available for querying 
     *  receiving agents. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsReceivingAgentQuery() {
        return (false);
    }


    /**
     *  Gets the query for the receiving agent. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsReceivingQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getReceivingAgentQuery() {
        throw new org.osid.UnimplementedException("supportsReceivingAgentQuery() is false");
    }


    /**
     *  Clears the receiving agent terms. 
     */

    @OSID @Override
    public void clearReceivingAgentTerms() {
        getAssembler().clearTerms(getReceivingAgentColumn());
        return;
    }


    /**
     *  Gets the receiving agent query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authentication.AgentQueryInspector[] getReceivingAgentTerms() {
        return (new org.osid.authentication.AgentQueryInspector[0]);
    }


    /**
     *  Tests if an agent order is available. 
     *
     *  @return <code> true </code> if an agent order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsReceivingAgentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the receiving agent order. 
     *
     *  @return the agent search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsReceivingAgentSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getReceivingAgentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsReceivingAgentSearchOrder() is false");
    }


    /**
     *  Gets the ReceivingAgent column name.
     *
     * @return the column name
     */

    protected String getReceivingAgentColumn() {
        return ("receiving_agent");
    }


    /**
     *  Matches any recipient of the message. 
     *
     *  @param  resourceId <code> Id </code> to match 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRecipientId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getRecipientIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the recipient <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRecipientIdTerms() {
        getAssembler().clearTerms(getRecipientIdColumn());
        return;
    }


    /**
     *  Gets the recipient <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRecipientIdTerms() {
        return (getAssembler().getIdTerms(getRecipientIdColumn()));
    }


    /**
     *  Specifies a preference for ordering messages by recipient. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRecipient(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getRecipientColumn(), style);
        return;
    }


    /**
     *  Gets the RecipientId column name.
     *
     * @return the column name
     */

    protected String getRecipientIdColumn() {
        return ("recipient_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available for querying 
     *  recipients. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecipientQuery() {
        return (false);
    }


    /**
     *  Gets the query for a recipient resource. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecipientQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getRecipientQuery() {
        throw new org.osid.UnimplementedException("supportsRecipientQuery() is false");
    }


    /**
     *  Clears the recipient terms. 
     */

    @OSID @Override
    public void clearRecipientTerms() {
        getAssembler().clearTerms(getRecipientColumn());
        return;
    }


    /**
     *  Gets the recipient query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getRecipientTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a resource order is available. 
     *
     *  @return <code> true </code> if a resource order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecipientSearchOrder() {
        return (false);
    }


    /**
     *  Gets the recipient order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecipienttSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getRecipientSearchOrder() {
        throw new org.osid.UnimplementedException("supportsRecipientSearchOrder() is false");
    }


    /**
     *  Gets the Recipient column name.
     *
     * @return the column name
     */

    protected String getRecipientColumn() {
        return ("recipient");
    }


    /**
     *  Sets the mailbox <code> Id </code> for this query. 
     *
     *  @param  mailboxId the mailbox <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> mailboxId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchMailboxId(org.osid.id.Id mailboxId, boolean match) {
        getAssembler().addIdTerm(getMailboxIdColumn(), mailboxId, match);
        return;
    }


    /**
     *  Clears the mailbox <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearMailboxIdTerms() {
        getAssembler().clearTerms(getMailboxIdColumn());
        return;
    }


    /**
     *  Gets the mailbox <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getMailboxIdTerms() {
        return (getAssembler().getIdTerms(getMailboxIdColumn()));
    }


    /**
     *  Gets the MailboxId column name.
     *
     * @return the column name
     */

    protected String getMailboxIdColumn() {
        return ("mailbox_id");
    }


    /**
     *  Tests if a <code> MailboxQuery </code> is available. 
     *
     *  @return <code> true </code> if a mailbox query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMailboxQuery() {
        return (false);
    }


    /**
     *  Gets the query for a mailbox. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the mailbox query 
     *  @throws org.osid.UnimplementedException <code> supportsMailboxQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MailboxQuery getMailboxQuery() {
        throw new org.osid.UnimplementedException("supportsMailboxQuery() is false");
    }


    /**
     *  Clears the mailbox terms. 
     */

    @OSID @Override
    public void clearMailboxTerms() {
        getAssembler().clearTerms(getMailboxColumn());
        return;
    }


    /**
     *  Gets the mailbox query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.messaging.MailboxQueryInspector[] getMailboxTerms() {
        return (new org.osid.messaging.MailboxQueryInspector[0]);
    }


    /**
     *  Gets the Mailbox column name.
     *
     * @return the column name
     */

    protected String getMailboxColumn() {
        return ("mailbox");
    }


    /**
     *  Tests if this receipt supports the given record
     *  <code>Type</code>.
     *
     *  @param  receiptRecordType a receipt record type 
     *  @return <code>true</code> if the receiptRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>receiptRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type receiptRecordType) {
        for (org.osid.messaging.records.ReceiptQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(receiptRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  receiptRecordType the receipt record type 
     *  @return the receipt query record 
     *  @throws org.osid.NullArgumentException
     *          <code>receiptRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(receiptRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.messaging.records.ReceiptQueryRecord getReceiptQueryRecord(org.osid.type.Type receiptRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.messaging.records.ReceiptQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(receiptRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(receiptRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  receiptRecordType the receipt record type 
     *  @return the receipt query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>receiptRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(receiptRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.messaging.records.ReceiptQueryInspectorRecord getReceiptQueryInspectorRecord(org.osid.type.Type receiptRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.messaging.records.ReceiptQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(receiptRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(receiptRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param receiptRecordType the receipt record type
     *  @return the receipt search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>receiptRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(receiptRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.messaging.records.ReceiptSearchOrderRecord getReceiptSearchOrderRecord(org.osid.type.Type receiptRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.messaging.records.ReceiptSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(receiptRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(receiptRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this receipt. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param receiptQueryRecord the receipt query record
     *  @param receiptQueryInspectorRecord the receipt query inspector
     *         record
     *  @param receiptSearchOrderRecord the receipt search order record
     *  @param receiptRecordType receipt record type
     *  @throws org.osid.NullArgumentException
     *          <code>receiptQueryRecord</code>,
     *          <code>receiptQueryInspectorRecord</code>,
     *          <code>receiptSearchOrderRecord</code> or
     *          <code>receiptRecordTypereceipt</code> is
     *          <code>null</code>
     */
            
    protected void addReceiptRecords(org.osid.messaging.records.ReceiptQueryRecord receiptQueryRecord, 
                                      org.osid.messaging.records.ReceiptQueryInspectorRecord receiptQueryInspectorRecord, 
                                      org.osid.messaging.records.ReceiptSearchOrderRecord receiptSearchOrderRecord, 
                                      org.osid.type.Type receiptRecordType) {

        addRecordType(receiptRecordType);

        nullarg(receiptQueryRecord, "receipt query record");
        nullarg(receiptQueryInspectorRecord, "receipt query inspector record");
        nullarg(receiptSearchOrderRecord, "receipt search odrer record");

        this.queryRecords.add(receiptQueryRecord);
        this.queryInspectorRecords.add(receiptQueryInspectorRecord);
        this.searchOrderRecords.add(receiptSearchOrderRecord);
        
        return;
    }
}
