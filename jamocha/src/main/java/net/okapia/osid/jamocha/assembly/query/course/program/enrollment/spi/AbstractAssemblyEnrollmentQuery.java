//
// AbstractAssemblyEnrollmentQuery.java
//
//     An EnrollmentQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.course.program.enrollment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An EnrollmentQuery that stores terms.
 */

public abstract class AbstractAssemblyEnrollmentQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRelationshipQuery
    implements org.osid.course.program.EnrollmentQuery,
               org.osid.course.program.EnrollmentQueryInspector,
               org.osid.course.program.EnrollmentSearchOrder {

    private final java.util.Collection<org.osid.course.program.records.EnrollmentQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.program.records.EnrollmentQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.program.records.EnrollmentSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyEnrollmentQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyEnrollmentQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the program offering <code> Id </code> for this query. 
     *
     *  @param  programOfferingId a program offering <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> programOfferingId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchProgramOfferingId(org.osid.id.Id programOfferingId, 
                                       boolean match) {
        getAssembler().addIdTerm(getProgramOfferingIdColumn(), programOfferingId, match);
        return;
    }


    /**
     *  Clears the program offering <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearProgramOfferingIdTerms() {
        getAssembler().clearTerms(getProgramOfferingIdColumn());
        return;
    }


    /**
     *  Gets the program offering <code> Id </code> query terms. 
     *
     *  @return the program offering <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProgramOfferingIdTerms() {
        return (getAssembler().getIdTerms(getProgramOfferingIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by program 
     *  offering. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByProgramOffering(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getProgramOfferingColumn(), style);
        return;
    }


    /**
     *  Gets the ProgramOfferingId column name.
     *
     * @return the column name
     */

    protected String getProgramOfferingIdColumn() {
        return ("program_offering_id");
    }


    /**
     *  Tests if a <code> ProgramOffering </code> is available. 
     *
     *  @return <code> true </code> if a program offering query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramOfferingQuery() {
        return (false);
    }


    /**
     *  Gets the query for a program offering. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the program offering query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramOfferingQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingQuery getProgramOfferingQuery() {
        throw new org.osid.UnimplementedException("supportsProgramOfferingQuery() is false");
    }


    /**
     *  Clears the program offering terms. 
     */

    @OSID @Override
    public void clearProgramOfferingTerms() {
        getAssembler().clearTerms(getProgramOfferingColumn());
        return;
    }


    /**
     *  Gets the program offering query terms. 
     *
     *  @return the program offering query terms 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingQueryInspector[] getProgramOfferingTerms() {
        return (new org.osid.course.program.ProgramOfferingQueryInspector[0]);
    }


    /**
     *  Tests if a program offering search order is available. 
     *
     *  @return <code> true </code> if a program offering order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramOfferingSearchOrder() {
        return (false);
    }


    /**
     *  Gets the program offering search order. 
     *
     *  @return the program offering search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramOfferingSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingSearchOrder getProgramOfferingSearchOrder() {
        throw new org.osid.UnimplementedException("supportsProgramOfferingSearchOrder() is false");
    }


    /**
     *  Gets the ProgramOffering column name.
     *
     * @return the column name
     */

    protected String getProgramOfferingColumn() {
        return ("program_offering");
    }


    /**
     *  Sets the student resource <code> Id </code> for this query. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchStudentId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getStudentIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the student resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearStudentIdTerms() {
        getAssembler().clearTerms(getStudentIdColumn());
        return;
    }


    /**
     *  Gets the student <code> Id </code> query terms. 
     *
     *  @return the resource <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getStudentIdTerms() {
        return (getAssembler().getIdTerms(getStudentIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by student. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByStudent(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getStudentColumn(), style);
        return;
    }


    /**
     *  Gets the StudentId column name.
     *
     * @return the column name
     */

    protected String getStudentIdColumn() {
        return ("student_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStudentQuery() {
        return (false);
    }


    /**
     *  Gets the query for a student. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsStudentQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getStudentQuery() {
        throw new org.osid.UnimplementedException("supportsStudentQuery() is false");
    }


    /**
     *  Clears the student resource terms. 
     */

    @OSID @Override
    public void clearStudentTerms() {
        getAssembler().clearTerms(getStudentColumn());
        return;
    }


    /**
     *  Gets the student query terms. 
     *
     *  @return the student query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getStudentTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a student search order is available. 
     *
     *  @return <code> true </code> if a student order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStudentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the student search order. 
     *
     *  @return the student search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStudentSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getStudentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsStudentSearchOrder() is false");
    }


    /**
     *  Gets the Student column name.
     *
     * @return the column name
     */

    protected String getStudentColumn() {
        return ("student");
    }


    /**
     *  Sets the course catalog <code> Id </code> for this query. 
     *
     *  @param  courseCatalogId the course catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseCatalogId(org.osid.id.Id courseCatalogId, 
                                     boolean match) {
        getAssembler().addIdTerm(getCourseCatalogIdColumn(), courseCatalogId, match);
        return;
    }


    /**
     *  Clears the course catalog <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseCatalogIdTerms() {
        getAssembler().clearTerms(getCourseCatalogIdColumn());
        return;
    }


    /**
     *  Gets the course catalog <code> Id </code> query terms. 
     *
     *  @return the course catalog <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseCatalogIdTerms() {
        return (getAssembler().getIdTerms(getCourseCatalogIdColumn()));
    }


    /**
     *  Gets the CourseCatalogId column name.
     *
     * @return the column name
     */

    protected String getCourseCatalogIdColumn() {
        return ("course_catalog_id");
    }


    /**
     *  Tests if a <code> CourseCatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a course catalog query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course catalog. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the course catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQuery getCourseCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsCourseCatalogQuery() is false");
    }


    /**
     *  Clears the course catalog terms. 
     */

    @OSID @Override
    public void clearCourseCatalogTerms() {
        getAssembler().clearTerms(getCourseCatalogColumn());
        return;
    }


    /**
     *  Gets the course catalog query terms. 
     *
     *  @return the course catalog query terms 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQueryInspector[] getCourseCatalogTerms() {
        return (new org.osid.course.CourseCatalogQueryInspector[0]);
    }


    /**
     *  Gets the CourseCatalog column name.
     *
     * @return the column name
     */

    protected String getCourseCatalogColumn() {
        return ("course_catalog");
    }


    /**
     *  Tests if this enrollment supports the given record
     *  <code>Type</code>.
     *
     *  @param  enrollmentRecordType an enrollment record type 
     *  @return <code>true</code> if the enrollmentRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>enrollmentRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type enrollmentRecordType) {
        for (org.osid.course.program.records.EnrollmentQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(enrollmentRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  enrollmentRecordType the enrollment record type 
     *  @return the enrollment query record 
     *  @throws org.osid.NullArgumentException
     *          <code>enrollmentRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(enrollmentRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.program.records.EnrollmentQueryRecord getEnrollmentQueryRecord(org.osid.type.Type enrollmentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.program.records.EnrollmentQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(enrollmentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(enrollmentRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  enrollmentRecordType the enrollment record type 
     *  @return the enrollment query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>enrollmentRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(enrollmentRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.program.records.EnrollmentQueryInspectorRecord getEnrollmentQueryInspectorRecord(org.osid.type.Type enrollmentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.program.records.EnrollmentQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(enrollmentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(enrollmentRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param enrollmentRecordType the enrollment record type
     *  @return the enrollment search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>enrollmentRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(enrollmentRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.program.records.EnrollmentSearchOrderRecord getEnrollmentSearchOrderRecord(org.osid.type.Type enrollmentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.program.records.EnrollmentSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(enrollmentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(enrollmentRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this enrollment. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param enrollmentQueryRecord the enrollment query record
     *  @param enrollmentQueryInspectorRecord the enrollment query inspector
     *         record
     *  @param enrollmentSearchOrderRecord the enrollment search order record
     *  @param enrollmentRecordType enrollment record type
     *  @throws org.osid.NullArgumentException
     *          <code>enrollmentQueryRecord</code>,
     *          <code>enrollmentQueryInspectorRecord</code>,
     *          <code>enrollmentSearchOrderRecord</code> or
     *          <code>enrollmentRecordTypeenrollment</code> is
     *          <code>null</code>
     */
            
    protected void addEnrollmentRecords(org.osid.course.program.records.EnrollmentQueryRecord enrollmentQueryRecord, 
                                      org.osid.course.program.records.EnrollmentQueryInspectorRecord enrollmentQueryInspectorRecord, 
                                      org.osid.course.program.records.EnrollmentSearchOrderRecord enrollmentSearchOrderRecord, 
                                      org.osid.type.Type enrollmentRecordType) {

        addRecordType(enrollmentRecordType);

        nullarg(enrollmentQueryRecord, "enrollment query record");
        nullarg(enrollmentQueryInspectorRecord, "enrollment query inspector record");
        nullarg(enrollmentSearchOrderRecord, "enrollment search odrer record");

        this.queryRecords.add(enrollmentQueryRecord);
        this.queryInspectorRecords.add(enrollmentQueryInspectorRecord);
        this.searchOrderRecords.add(enrollmentSearchOrderRecord);
        
        return;
    }
}
