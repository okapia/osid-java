//
// AbstractNodeOntologyHierarchySession.java
//
//     Defines an Ontology hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.ontology.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an ontology hierarchy session for delivering a hierarchy
 *  of ontologies using the OntologyNode interface.
 */

public abstract class AbstractNodeOntologyHierarchySession
    extends net.okapia.osid.jamocha.ontology.spi.AbstractOntologyHierarchySession
    implements org.osid.ontology.OntologyHierarchySession {

    private java.util.Collection<org.osid.ontology.OntologyNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root ontology <code> Ids </code> in this hierarchy.
     *
     *  @return the root ontology <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootOntologyIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.ontology.ontologynode.OntologyNodeToIdList(this.roots));
    }


    /**
     *  Gets the root ontologies in the ontology hierarchy. A node
     *  with no parents is an orphan. While all ontology <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root ontologies 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.OntologyList getRootOntologies()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.ontology.ontologynode.OntologyNodeToOntologyList(new net.okapia.osid.jamocha.ontology.ontologynode.ArrayOntologyNodeList(this.roots)));
    }


    /**
     *  Adds a root ontology node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootOntology(org.osid.ontology.OntologyNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root ontology nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootOntologies(java.util.Collection<org.osid.ontology.OntologyNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root ontology node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootOntology(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.ontology.OntologyNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Ontology </code> has any parents. 
     *
     *  @param  ontologyId an ontology <code> Id </code> 
     *  @return <code> true </code> if the ontology has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> ontologyId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> ontologyId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentOntologies(org.osid.id.Id ontologyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getOntologyNode(ontologyId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of an
     *  ontology.
     *
     *  @param  id an <code> Id </code> 
     *  @param  ontologyId the <code> Id </code> of an ontology 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> ontologyId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> ontologyId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> ontologyId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfOntology(org.osid.id.Id id, org.osid.id.Id ontologyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.ontology.OntologyNodeList parents = getOntologyNode(ontologyId).getParentOntologyNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextOntologyNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given ontology. 
     *
     *  @param  ontologyId an ontology <code> Id </code> 
     *  @return the parent <code> Ids </code> of the ontology 
     *  @throws org.osid.NotFoundException <code> ontologyId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> ontologyId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentOntologyIds(org.osid.id.Id ontologyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.ontology.ontology.OntologyToIdList(getParentOntologies(ontologyId)));
    }


    /**
     *  Gets the parents of the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> to query 
     *  @return the parents of the ontology 
     *  @throws org.osid.NotFoundException <code> ontologyId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> ontologyId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.OntologyList getParentOntologies(org.osid.id.Id ontologyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.ontology.ontologynode.OntologyNodeToOntologyList(getOntologyNode(ontologyId).getParentOntologyNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of an
     *  ontology.
     *
     *  @param  id an <code> Id </code> 
     *  @param  ontologyId the Id of an ontology 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> ontologyId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> ontologyId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> ontologyId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfOntology(org.osid.id.Id id, org.osid.id.Id ontologyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfOntology(id, ontologyId)) {
            return (true);
        }

        try (org.osid.ontology.OntologyList parents = getParentOntologies(ontologyId)) {
            while (parents.hasNext()) {
                if (isAncestorOfOntology(id, parents.getNextOntology().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if an ontology has any children. 
     *
     *  @param  ontologyId an ontology <code> Id </code> 
     *  @return <code> true </code> if the <code> ontologyId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> ontologyId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> ontologyId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildOntologies(org.osid.id.Id ontologyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getOntologyNode(ontologyId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of an
     *  ontology.
     *
     *  @param  id an <code> Id </code> 
     *  @param ontologyId the <code> Id </code> of an 
     *         ontology
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> ontologyId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> ontologyId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> ontologyId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfOntology(org.osid.id.Id id, org.osid.id.Id ontologyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfOntology(ontologyId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  ontology.
     *
     *  @param  ontologyId the <code> Id </code> to query 
     *  @return the children of the ontology 
     *  @throws org.osid.NotFoundException <code> ontologyId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> ontologyId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildOntologyIds(org.osid.id.Id ontologyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.ontology.ontology.OntologyToIdList(getChildOntologies(ontologyId)));
    }


    /**
     *  Gets the children of the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> to query 
     *  @return the children of the ontology 
     *  @throws org.osid.NotFoundException <code> ontologyId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> ontologyId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.OntologyList getChildOntologies(org.osid.id.Id ontologyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.ontology.ontologynode.OntologyNodeToOntologyList(getOntologyNode(ontologyId).getChildOntologyNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of an
     *  ontology.
     *
     *  @param  id an <code> Id </code> 
     *  @param ontologyId the <code> Id </code> of an 
     *         ontology
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> ontologyId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> ontologyId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> ontologyId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfOntology(org.osid.id.Id id, org.osid.id.Id ontologyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfOntology(ontologyId, id)) {
            return (true);
        }

        try (org.osid.ontology.OntologyList children = getChildOntologies(ontologyId)) {
            while (children.hasNext()) {
                if (isDescendantOfOntology(id, children.getNextOntology().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  ontology.
     *
     *  @param  ontologyId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified ontology node 
     *  @throws org.osid.NotFoundException <code> ontologyId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> ontologyId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getOntologyNodeIds(org.osid.id.Id ontologyId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.ontology.ontologynode.OntologyNodeToNode(getOntologyNode(ontologyId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given ontology.
     *
     *  @param  ontologyId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified ontology node 
     *  @throws org.osid.NotFoundException <code> ontologyId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> ontologyId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.OntologyNode getOntologyNodes(org.osid.id.Id ontologyId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getOntologyNode(ontologyId));
    }


    /**
     *  Closes this <code>OntologyHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets an ontology node.
     *
     *  @param ontologyId the id of the ontology node
     *  @throws org.osid.NotFoundException <code>ontologyId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>ontologyId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.ontology.OntologyNode getOntologyNode(org.osid.id.Id ontologyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(ontologyId, "ontology Id");
        for (org.osid.ontology.OntologyNode ontology : this.roots) {
            if (ontology.getId().equals(ontologyId)) {
                return (ontology);
            }

            org.osid.ontology.OntologyNode r = findOntology(ontology, ontologyId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(ontologyId + " is not found");
    }


    protected org.osid.ontology.OntologyNode findOntology(org.osid.ontology.OntologyNode node, 
                                                          org.osid.id.Id ontologyId) 
	throws org.osid.OperationFailedException {

        try (org.osid.ontology.OntologyNodeList children = node.getChildOntologyNodes()) {
            while (children.hasNext()) {
                org.osid.ontology.OntologyNode ontology = children.getNextOntologyNode();
                if (ontology.getId().equals(ontologyId)) {
                    return (ontology);
                }
                
                ontology = findOntology(ontology, ontologyId);
                if (ontology != null) {
                    return (ontology);
                }
            }
        }

        return (null);
    }
}
