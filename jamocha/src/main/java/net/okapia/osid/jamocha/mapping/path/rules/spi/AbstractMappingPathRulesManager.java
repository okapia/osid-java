//
// AbstractMappingPathRulesManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.path.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractMappingPathRulesManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.mapping.path.rules.MappingPathRulesManager,
               org.osid.mapping.path.rules.MappingPathRulesProxyManager {

    private final Types speedZoneEnablerRecordTypes        = new TypeRefSet();
    private final Types speedZoneEnablerSearchRecordTypes  = new TypeRefSet();

    private final Types signalEnablerRecordTypes           = new TypeRefSet();
    private final Types signalEnablerSearchRecordTypes     = new TypeRefSet();

    private final Types obstacleEnablerRecordTypes         = new TypeRefSet();
    private final Types obstacleEnablerSearchRecordTypes   = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractMappingPathRulesManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractMappingPathRulesManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any broker federation is exposed. Federation is exposed when 
     *  a specific broker may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  brokers appears as a single broker. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if looking up speed zone enablers is supported. 
     *
     *  @return <code> true </code> if speed zone enabler lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSpeedZoneEnablerLookup() {
        return (false);
    }


    /**
     *  Tests if querying speed zone enablers is supported. 
     *
     *  @return <code> true </code> if speed zone enabler query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSpeedZoneEnablerQuery() {
        return (false);
    }


    /**
     *  Tests if searching speed zone enablers is supported. 
     *
     *  @return <code> true </code> if speed zone enabler search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSpeedZoneEnablerSearch() {
        return (false);
    }


    /**
     *  Tests if a speed zone enabler administrative service is supported. 
     *
     *  @return <code> true </code> if speed zone enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSpeedZoneEnablerAdmin() {
        return (false);
    }


    /**
     *  Tests if a speed zone enabler notification service is supported. 
     *
     *  @return <code> true </code> if speed zone enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSpeedZoneEnablerNotification() {
        return (false);
    }


    /**
     *  Tests if a speed zone enabler map lookup service is supported. 
     *
     *  @return <code> true </code> if a speed zone enabler map lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSpeedZoneEnablerMap() {
        return (false);
    }


    /**
     *  Tests if a speed zone enabler map service is supported. 
     *
     *  @return <code> true </code> if speed zone enabler map assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSpeedZoneEnablerMapAssignment() {
        return (false);
    }


    /**
     *  Tests if a speed zone enabler map lookup service is supported. 
     *
     *  @return <code> true </code> if a speed zone enabler map service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSpeedZoneEnablerSmartMap() {
        return (false);
    }


    /**
     *  Tests if a speed zone enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if a speed zone enabler rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSpeedZoneEnablerRuleLookup() {
        return (false);
    }


    /**
     *  Tests if a speed zone enabler rule application service is supported. 
     *
     *  @return <code> true </code> if speed zone enabler rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSpeedZoneEnablerRuleApplication() {
        return (false);
    }


    /**
     *  Tests if looking up signal enabler is supported. 
     *
     *  @return <code> true </code> if signal enabler lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSignalEnablerLookup() {
        return (false);
    }


    /**
     *  Tests if querying signal enabler is supported. 
     *
     *  @return <code> true </code> if signal enabler query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSignalEnablerQuery() {
        return (false);
    }


    /**
     *  Tests if searching signal enabler is supported. 
     *
     *  @return <code> true </code> if signal enabler search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSignalEnablerSearch() {
        return (false);
    }


    /**
     *  Tests if a signal enabler administrative service is supported. 
     *
     *  @return <code> true </code> if signal enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSignalEnablerAdmin() {
        return (false);
    }


    /**
     *  Tests if a signal enabler notification service is supported. 
     *
     *  @return <code> true </code> if signal enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSignalEnablerNotification() {
        return (false);
    }


    /**
     *  Tests if a signal enabler map lookup service is supported. 
     *
     *  @return <code> true </code> if a signal enabler map lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSignalEnablerMap() {
        return (false);
    }


    /**
     *  Tests if a signal enabler map service is supported. 
     *
     *  @return <code> true </code> if signal enabler map assignment service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSignalEnablerMapAssignment() {
        return (false);
    }


    /**
     *  Tests if a signal enabler map lookup service is supported. 
     *
     *  @return <code> true </code> if a signal enabler map service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSignalEnablerSmartMap() {
        return (false);
    }


    /**
     *  Tests if a signal enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if a signal enabler rule lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSignalEnablerRuleLookup() {
        return (false);
    }


    /**
     *  Tests if a signal enabler rule application service is supported. 
     *
     *  @return <code> true </code> if a signal enabler rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSignalEnablerRuleApplication() {
        return (false);
    }


    /**
     *  Tests if looking up obstacle enabler is supported. 
     *
     *  @return <code> true </code> if obstacle enabler lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObstacleEnablerLookup() {
        return (false);
    }


    /**
     *  Tests if querying obstacle enabler is supported. 
     *
     *  @return <code> true </code> if obstacle enabler query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObstacleEnablerQuery() {
        return (false);
    }


    /**
     *  Tests if searching obstacle enabler is supported. 
     *
     *  @return <code> true </code> if obstacle enabler search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObstacleEnablerSearch() {
        return (false);
    }


    /**
     *  Tests if an obstacle enabler administrative service is supported. 
     *
     *  @return <code> true </code> if obstacle enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObstacleEnablerAdmin() {
        return (false);
    }


    /**
     *  Tests if an obstacle enabler notification service is supported. 
     *
     *  @return <code> true </code> if obstacle enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObstacleEnablerNotification() {
        return (false);
    }


    /**
     *  Tests if an obstacle enabler map lookup service is supported. 
     *
     *  @return <code> true </code> if an obstacle enabler map lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObstacleEnablerMap() {
        return (false);
    }


    /**
     *  Tests if an obstacle enabler map service is supported. 
     *
     *  @return <code> true </code> if obstacle enabler map assignment service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObstacleEnablerMapAssignment() {
        return (false);
    }


    /**
     *  Tests if an obstacle enabler map lookup service is supported. 
     *
     *  @return <code> true </code> if an obstacle enabler map service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObstacleEnablerSmartMap() {
        return (false);
    }


    /**
     *  Tests if an obstacle enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if an obstacle enabler rule lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObstacleEnablerRuleLookup() {
        return (false);
    }


    /**
     *  Tests if an obstacle enabler rule application service is supported. 
     *
     *  @return <code> true </code> if an obstacle enabler rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObstacleEnablerRuleApplication() {
        return (false);
    }


    /**
     *  Gets the supported <code> SpeedZoneEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> SpeedZoneEnabler 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSpeedZoneEnablerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.speedZoneEnablerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> SpeedZoneEnabler </code> record type is 
     *  supported. 
     *
     *  @param  speedZoneEnablerRecordType a <code> Type </code> indicating a 
     *          <code> SpeedZoneEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          speedZoneEnablerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSpeedZoneEnablerRecordType(org.osid.type.Type speedZoneEnablerRecordType) {
        return (this.speedZoneEnablerRecordTypes.contains(speedZoneEnablerRecordType));
    }


    /**
     *  Adds support for a speed zone enabler record type.
     *
     *  @param speedZoneEnablerRecordType a speed zone enabler record type
     *  @throws org.osid.NullArgumentException
     *  <code>speedZoneEnablerRecordType</code> is <code>null</code>
     */

    protected void addSpeedZoneEnablerRecordType(org.osid.type.Type speedZoneEnablerRecordType) {
        this.speedZoneEnablerRecordTypes.add(speedZoneEnablerRecordType);
        return;
    }


    /**
     *  Removes support for a speed zone enabler record type.
     *
     *  @param speedZoneEnablerRecordType a speed zone enabler record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>speedZoneEnablerRecordType</code> is <code>null</code>
     */

    protected void removeSpeedZoneEnablerRecordType(org.osid.type.Type speedZoneEnablerRecordType) {
        this.speedZoneEnablerRecordTypes.remove(speedZoneEnablerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> SpeedZoneEnabler </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> SpeedZoneEnabler 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSpeedZoneEnablerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.speedZoneEnablerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> SpeedZoneEnabler </code> search record type 
     *  is supported. 
     *
     *  @param  speedZoneEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> SpeedZoneEnabler </code> search record 
     *          type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          speedZoneEnablerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsSpeedZoneEnablerSearchRecordType(org.osid.type.Type speedZoneEnablerSearchRecordType) {
        return (this.speedZoneEnablerSearchRecordTypes.contains(speedZoneEnablerSearchRecordType));
    }


    /**
     *  Adds support for a speed zone enabler search record type.
     *
     *  @param speedZoneEnablerSearchRecordType a speed zone enabler search record type
     *  @throws org.osid.NullArgumentException
     *  <code>speedZoneEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void addSpeedZoneEnablerSearchRecordType(org.osid.type.Type speedZoneEnablerSearchRecordType) {
        this.speedZoneEnablerSearchRecordTypes.add(speedZoneEnablerSearchRecordType);
        return;
    }


    /**
     *  Removes support for a speed zone enabler search record type.
     *
     *  @param speedZoneEnablerSearchRecordType a speed zone enabler search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>speedZoneEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void removeSpeedZoneEnablerSearchRecordType(org.osid.type.Type speedZoneEnablerSearchRecordType) {
        this.speedZoneEnablerSearchRecordTypes.remove(speedZoneEnablerSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> SignalEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> SignalEnabler </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSignalEnablerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.signalEnablerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> SignalEnabler </code> record type is 
     *  supported. 
     *
     *  @param  signalEnablerRecordType a <code> Type </code> indicating a 
     *          <code> SignalEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> signalEnablerRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSignalEnablerRecordType(org.osid.type.Type signalEnablerRecordType) {
        return (this.signalEnablerRecordTypes.contains(signalEnablerRecordType));
    }


    /**
     *  Adds support for a signal enabler record type.
     *
     *  @param signalEnablerRecordType a signal enabler record type
     *  @throws org.osid.NullArgumentException
     *  <code>signalEnablerRecordType</code> is <code>null</code>
     */

    protected void addSignalEnablerRecordType(org.osid.type.Type signalEnablerRecordType) {
        this.signalEnablerRecordTypes.add(signalEnablerRecordType);
        return;
    }


    /**
     *  Removes support for a signal enabler record type.
     *
     *  @param signalEnablerRecordType a signal enabler record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>signalEnablerRecordType</code> is <code>null</code>
     */

    protected void removeSignalEnablerRecordType(org.osid.type.Type signalEnablerRecordType) {
        this.signalEnablerRecordTypes.remove(signalEnablerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> SignalEnabler </code> search record types. 
     *
     *  @return a list containing the supported <code> SignalEnabler </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSignalEnablerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.signalEnablerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> SignalEnabler </code> search record type is 
     *  supported. 
     *
     *  @param  signalEnablerSearchRecordType a <code> Type </code> indicating 
     *          a <code> SignalEnabler </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          signalEnablerSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSignalEnablerSearchRecordType(org.osid.type.Type signalEnablerSearchRecordType) {
        return (this.signalEnablerSearchRecordTypes.contains(signalEnablerSearchRecordType));
    }


    /**
     *  Adds support for a signal enabler search record type.
     *
     *  @param signalEnablerSearchRecordType a signal enabler search record type
     *  @throws org.osid.NullArgumentException
     *  <code>signalEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void addSignalEnablerSearchRecordType(org.osid.type.Type signalEnablerSearchRecordType) {
        this.signalEnablerSearchRecordTypes.add(signalEnablerSearchRecordType);
        return;
    }


    /**
     *  Removes support for a signal enabler search record type.
     *
     *  @param signalEnablerSearchRecordType a signal enabler search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>signalEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void removeSignalEnablerSearchRecordType(org.osid.type.Type signalEnablerSearchRecordType) {
        this.signalEnablerSearchRecordTypes.remove(signalEnablerSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> ObstacleEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> ObstacleEnabler </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getObstacleEnablerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.obstacleEnablerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> ObstacleEnabler </code> record type is 
     *  supported. 
     *
     *  @param  obstacleEnablerRecordType a <code> Type </code> indicating an 
     *          <code> ObstacleEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          obstacleEnablerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsObstacleEnablerRecordType(org.osid.type.Type obstacleEnablerRecordType) {
        return (this.obstacleEnablerRecordTypes.contains(obstacleEnablerRecordType));
    }


    /**
     *  Adds support for an obstacle enabler record type.
     *
     *  @param obstacleEnablerRecordType an obstacle enabler record type
     *  @throws org.osid.NullArgumentException
     *  <code>obstacleEnablerRecordType</code> is <code>null</code>
     */

    protected void addObstacleEnablerRecordType(org.osid.type.Type obstacleEnablerRecordType) {
        this.obstacleEnablerRecordTypes.add(obstacleEnablerRecordType);
        return;
    }


    /**
     *  Removes support for an obstacle enabler record type.
     *
     *  @param obstacleEnablerRecordType an obstacle enabler record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>obstacleEnablerRecordType</code> is <code>null</code>
     */

    protected void removeObstacleEnablerRecordType(org.osid.type.Type obstacleEnablerRecordType) {
        this.obstacleEnablerRecordTypes.remove(obstacleEnablerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> ObstacleEnabler </code> search record types. 
     *
     *  @return a list containing the supported <code> ObstacleEnabler </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getObstacleEnablerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.obstacleEnablerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> ObstacleEnabler </code> search record type 
     *  is supported. 
     *
     *  @param  obstacleEnablerSearchRecordType a <code> Type </code> 
     *          indicating an <code> ObstacleEnabler </code> search record 
     *          type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          obstacleEnablerSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsObstacleEnablerSearchRecordType(org.osid.type.Type obstacleEnablerSearchRecordType) {
        return (this.obstacleEnablerSearchRecordTypes.contains(obstacleEnablerSearchRecordType));
    }


    /**
     *  Adds support for an obstacle enabler search record type.
     *
     *  @param obstacleEnablerSearchRecordType an obstacle enabler search record type
     *  @throws org.osid.NullArgumentException
     *  <code>obstacleEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void addObstacleEnablerSearchRecordType(org.osid.type.Type obstacleEnablerSearchRecordType) {
        this.obstacleEnablerSearchRecordTypes.add(obstacleEnablerSearchRecordType);
        return;
    }


    /**
     *  Removes support for an obstacle enabler search record type.
     *
     *  @param obstacleEnablerSearchRecordType an obstacle enabler search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>obstacleEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void removeObstacleEnablerSearchRecordType(org.osid.type.Type obstacleEnablerSearchRecordType) {
        this.obstacleEnablerSearchRecordTypes.remove(obstacleEnablerSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  enabler lookup service. 
     *
     *  @return a <code> SpeedZoneEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerLookupSession getSpeedZoneEnablerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesManager.getSpeedZoneEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  enabler lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerLookupSession getSpeedZoneEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesProxyManager.getSpeedZoneEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  enabler lookup service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> SpeedZoneEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerLookupSession getSpeedZoneEnablerLookupSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesManager.getSpeedZoneEnablerLookupSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  enabler lookup service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerLookupSession getSpeedZoneEnablerLookupSessionForMap(org.osid.id.Id mapId, 
                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesProxyManager.getSpeedZoneEnablerLookupSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  enabler query service. 
     *
     *  @return a <code> SpeedZoneEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerQuerySession getSpeedZoneEnablerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesManager.getSpeedZoneEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  enabler query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerQuerySession getSpeedZoneEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesProxyManager.getSpeedZoneEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  enabler query service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> SpeedZoneEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerQuerySession getSpeedZoneEnablerQuerySessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesManager.getSpeedZoneEnablerQuerySessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  enabler query service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerQuerySession getSpeedZoneEnablerQuerySessionForMap(org.osid.id.Id mapId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesProxyManager.getSpeedZoneEnablerQuerySessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  enabler search service. 
     *
     *  @return a <code> SpeedZoneEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerSearchSession getSpeedZoneEnablerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesManager.getSpeedZoneEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  enabler search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerSearchSession getSpeedZoneEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesProxyManager.getSpeedZoneEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  enablers earch service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> SpeedZoneEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerSearchSession getSpeedZoneEnablerSearchSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesManager.getSpeedZoneEnablerSearchSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  enablers earch service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerSearchSession getSpeedZoneEnablerSearchSessionForMap(org.osid.id.Id mapId, 
                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesProxyManager.getSpeedZoneEnablerSearchSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  enabler administration service. 
     *
     *  @return a <code> SpeedZoneEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerAdminSession getSpeedZoneEnablerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesManager.getSpeedZoneEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  enabler administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerAdminSession getSpeedZoneEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesProxyManager.getSpeedZoneEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  enabler administration service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> SpeedZoneEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerAdminSession getSpeedZoneEnablerAdminSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesManager.getSpeedZoneEnablerAdminSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  enabler administration service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerAdminSession getSpeedZoneEnablerAdminSessionForMap(org.osid.id.Id mapId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesProxyManager.getSpeedZoneEnablerAdminSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  enabler notification service. 
     *
     *  @param  speedZoneEnablerReceiver the notification callback 
     *  @return a <code> SpeedZoneEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> speedZoneEnablerReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerNotificationSession getSpeedZoneEnablerNotificationSession(org.osid.mapping.path.rules.SpeedZoneEnablerReceiver speedZoneEnablerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesManager.getSpeedZoneEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  enabler notification service. 
     *
     *  @param  speedZoneEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> speedZoneEnablerReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerNotificationSession getSpeedZoneEnablerNotificationSession(org.osid.mapping.path.rules.SpeedZoneEnablerReceiver speedZoneEnablerReceiver, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesProxyManager.getSpeedZoneEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  enabler notification service for the given map. 
     *
     *  @param  speedZoneEnablerReceiver the notification callback 
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> SpeedZoneEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> speedZoneEnablerReceiver 
     *          </code> or <code> mapId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerNotificationSession getSpeedZoneEnablerNotificationSessionForMap(org.osid.mapping.path.rules.SpeedZoneEnablerReceiver speedZoneEnablerReceiver, 
                                                                                                                        org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesManager.getSpeedZoneEnablerNotificationSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  enabler notification service for the given map. 
     *
     *  @param  speedZoneEnablerReceiver the notification callback 
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          speedZoneEnablerReceiver, mapId </code> or <code> proxy 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerNotificationSession getSpeedZoneEnablerNotificationSessionForMap(org.osid.mapping.path.rules.SpeedZoneEnablerReceiver speedZoneEnablerReceiver, 
                                                                                                                        org.osid.id.Id mapId, 
                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesProxyManager.getSpeedZoneEnablerNotificationSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup speed zone enabler/map 
     *  mappings for speed zone enablers. 
     *
     *  @return a <code> SpeedZoneEnablerMapSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneEnablerMap() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerMapSession getSpeedZoneEnablerMapSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesManager.getSpeedZoneEnablerMapSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup speed zone enabler/map 
     *  mappings for speed zone enablers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneEnablerMapSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneEnablerMap() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerMapSession getSpeedZoneEnablerMapSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesProxyManager.getSpeedZoneEnablerMapSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning speed 
     *  zone enablers to map. 
     *
     *  @return a <code> SpeedZoneEnablerMapAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneEnablerMapAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerMapAssignmentSession getSpeedZoneEnablerMapAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesManager.getSpeedZoneEnablerMapAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning speed 
     *  zone enablers to map. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneEnablerMapAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneEnablerMapAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerMapAssignmentSession getSpeedZoneEnablerMapAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesProxyManager.getSpeedZoneEnablerMapAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage speed zone enabler smart 
     *  map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> SpeedZoneEnablerSmartMapSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneEnablerSmartMap() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerSmartMapSession getSpeedZoneEnablerSmartMapSession(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesManager.getSpeedZoneEnablerSmartMapSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage speed zone enabler smart 
     *  map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneEnablerSmartMapSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneEnablerSmartMap() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerSmartMapSession getSpeedZoneEnablerSmartMapSession(org.osid.id.Id mapId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesProxyManager.getSpeedZoneEnablerSmartMapSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  enabler mapping lookup service. 
     *
     *  @return a <code> SpeedZoneEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerRuleLookupSession getSpeedZoneEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesManager.getSpeedZoneEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  enabler mapping lookup service . 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerRuleLookupSession getSpeedZoneEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesProxyManager.getSpeedZoneEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  enabler mapping lookup service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> SpeedZoneEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerRuleLookupSession getSpeedZoneEnablerRuleLookupSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesManager.getSpeedZoneEnablerRuleLookupSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  enabler mapping lookup service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerRuleLookupSession getSpeedZoneEnablerRuleLookupSessionForMap(org.osid.id.Id mapId, 
                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesProxyManager.getSpeedZoneEnablerRuleLookupSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  enabler assignment service. 
     *
     *  @return a <code> SpeedZoneEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerRuleApplicationSession getSpeedZoneEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesManager.getSpeedZoneEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  enabler assignment service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerRuleApplicationSession getSpeedZoneEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesProxyManager.getSpeedZoneEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  enabler assignment service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> SpeedZoneEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerRuleApplicationSession getSpeedZoneEnablerRuleApplicationSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesManager.getSpeedZoneEnablerRuleApplicationSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  enabler assignment service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerRuleApplicationSession getSpeedZoneEnablerRuleApplicationSessionForMap(org.osid.id.Id mapId, 
                                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesProxyManager.getSpeedZoneEnablerRuleApplicationSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal enabler 
     *  lookup service. 
     *
     *  @return a <code> SignalEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalEnablerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerLookupSession getSignalEnablerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesManager.getSignalEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal enabler 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SignalEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalEnablerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerLookupSession getSignalEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesProxyManager.getSignalEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal enabler 
     *  lookup service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> SignalEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerLookupSession getSignalEnablerLookupSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesManager.getSignalEnablerLookupSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal enabler 
     *  lookup service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SignalEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerLookupSession getSignalEnablerLookupSessionForMap(org.osid.id.Id mapId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesProxyManager.getSignalEnablerLookupSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal enabler 
     *  query service. 
     *
     *  @return a <code> SignalEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalEnablerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerQuerySession getSignalEnablerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesManager.getSignalEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal enabler 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SignalEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalEnablerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerQuerySession getSignalEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesProxyManager.getSignalEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal enabler 
     *  query service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> SignalEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerQuerySession getSignalEnablerQuerySessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesManager.getSignalEnablerQuerySessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal enabler 
     *  query service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SignalEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerQuerySession getSignalEnablerQuerySessionForMap(org.osid.id.Id mapId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesProxyManager.getSignalEnablerQuerySessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal enabler 
     *  search service. 
     *
     *  @return a <code> SignalEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalEnablerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerSearchSession getSignalEnablerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesManager.getSignalEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal enabler 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SignalEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException a <code> 
     *          SignalEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalEnablerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerSearchSession getSignalEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesProxyManager.getSignalEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal enabler 
     *  earch service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> SignalEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerSearchSession getSignalEnablerSearchSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesManager.getSignalEnablerSearchSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal enabler 
     *  earch service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SignalEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerSearchSession getSignalEnablerSearchSessionForMap(org.osid.id.Id mapId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesProxyManager.getSignalEnablerSearchSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal enabler 
     *  administration service. 
     *
     *  @return a <code> SignalEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalEnablerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerAdminSession getSignalEnablerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesManager.getSignalEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal enabler 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SignalEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalEnablerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerAdminSession getSignalEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesProxyManager.getSignalEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal enabler 
     *  administration service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> SignalEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerAdminSession getSignalEnablerAdminSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesManager.getSignalEnablerAdminSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal enabler 
     *  administration service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SignalEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerAdminSession getSignalEnablerAdminSessionForMap(org.osid.id.Id mapId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesProxyManager.getSignalEnablerAdminSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal enabler 
     *  notification service. 
     *
     *  @param  signalEnablerReceiver the notification callback 
     *  @return a <code> SignalEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> signalEnablerReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerNotificationSession getSignalEnablerNotificationSession(org.osid.mapping.path.rules.SignalEnablerReceiver signalEnablerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesManager.getSignalEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal enabler 
     *  notification service. 
     *
     *  @param  signalEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> SignalEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> signalEnablerReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerNotificationSession getSignalEnablerNotificationSession(org.osid.mapping.path.rules.SignalEnablerReceiver signalEnablerReceiver, 
                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesProxyManager.getSignalEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal enabler 
     *  notification service for the given map. 
     *
     *  @param  signalEnablerReceiver the notification callback 
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> SignalEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> signalEnablerReceiver 
     *          </code> or <code> mapId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerNotificationSession getSignalEnablerNotificationSessionForMap(org.osid.mapping.path.rules.SignalEnablerReceiver signalEnablerReceiver, 
                                                                                                                  org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesManager.getSignalEnablerNotificationSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal enabler 
     *  notification service for the given map. 
     *
     *  @param  signalEnablerReceiver the notification callback 
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SignalEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> signalEnablerReceiver, 
     *          mapId </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerNotificationSession getSignalEnablerNotificationSessionForMap(org.osid.mapping.path.rules.SignalEnablerReceiver signalEnablerReceiver, 
                                                                                                                  org.osid.id.Id mapId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesProxyManager.getSignalEnablerNotificationSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup signal enabler/map 
     *  mappings for signal enablers. 
     *
     *  @return a <code> SignalEnablerMapSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalEnablerMap() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerMapSession getSignalEnablerMapSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesManager.getSignalEnablerMapSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup signal enabler/map 
     *  mappings for signal enablers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SignalEnablerMapSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalEnablerMap() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerMapSession getSignalEnablerMapSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesProxyManager.getSignalEnablerMapSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning signal 
     *  enabler to map. 
     *
     *  @return a <code> SignalEnablerMapAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalEnablerMapAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerMapAssignmentSession getSignalEnablerMapAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesManager.getSignalEnablerMapAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning signal 
     *  enabler to map. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SignalEnablerMapAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalEnablerMapAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerMapAssignmentSession getSignalEnablerMapAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesProxyManager.getSignalEnablerMapAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage signal enabler smart 
     *  map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> SignalEnablerSmartMapSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalEnablerSmartMap() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerSmartMapSession getSignalEnablerSmartMapSession(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesManager.getSignalEnablerSmartMapSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage signal enabler smart 
     *  map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SignalEnablerSmartMapSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalEnablerSmartMap() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerSmartMapSession getSignalEnablerSmartMapSession(org.osid.id.Id mapId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesProxyManager.getSignalEnablerSmartMapSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal enabler 
     *  mapping lookup service for looking up the rules applied to the signal. 
     *
     *  @return a <code> SignalEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerRuleLookupSession getSignalEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesManager.getSignalEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal enabler 
     *  mapping lookup service for looking up the rules applied to a signal. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SignalEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerRuleLookupSession getSignalEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesProxyManager.getSignalEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal enabler 
     *  mapping lookup service for the given map for looking up rules applied 
     *  to a signal. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> SignalEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerRuleLookupSession getSignalEnablerRuleLookupSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesManager.getSignalEnablerRuleLookupSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal enabler 
     *  mapping lookup service for the given map for looking up rules applied 
     *  to a signal. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SignalEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerRuleLookupSession getSignalEnablerRuleLookupSessionForMap(org.osid.id.Id mapId, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesProxyManager.getSignalEnablerRuleLookupSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal enabler 
     *  assignment service to apply to signals. 
     *
     *  @return a <code> SignalEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalEnablerRuleApplication() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerRuleApplicationSession getSignalEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesManager.getSignalEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal enabler 
     *  assignment service to apply to signals. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SignalEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalEnablerRuleApplication() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerRuleApplicationSession getSignalEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesProxyManager.getSignalEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal enabler 
     *  assignment service for the given map to apply to signals. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> SignalEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerRuleApplicationSession getSignalEnablerRuleApplicationSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesManager.getSignalEnablerRuleApplicationSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal enabler 
     *  assignment service for the given map to apply to signals. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SignalEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerRuleApplicationSession getSignalEnablerRuleApplicationSessionForMap(org.osid.id.Id mapId, 
                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesProxyManager.getSignalEnablerRuleApplicationSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  enabler lookup service. 
     *
     *  @return an <code> ObstacleEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerLookupSession getObstacleEnablerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesManager.getObstacleEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  enabler lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerLookupSession getObstacleEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesProxyManager.getObstacleEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  enabler lookup service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return an <code> ObstacleEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerLookupSession getObstacleEnablerLookupSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesManager.getObstacleEnablerLookupSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  enabler lookup service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerLookupSession getObstacleEnablerLookupSessionForMap(org.osid.id.Id mapId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesProxyManager.getObstacleEnablerLookupSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  enabler query service. 
     *
     *  @return an <code> ObstacleEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleEnablerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerQuerySession getObstacleEnablerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesManager.getObstacleEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  enabler query service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleEnablerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerQuerySession getObstacleEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesProxyManager.getObstacleEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  enabler query service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return an <code> ObstacleEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerQuerySession getObstacleEnablerQuerySessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesManager.getObstacleEnablerQuerySessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  enabler query service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerQuerySession getObstacleEnablerQuerySessionForMap(org.osid.id.Id mapId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesProxyManager.getObstacleEnablerQuerySessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  enabler search service. 
     *
     *  @return an <code> ObstacleEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerSearchSession getObstacleEnablerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesManager.getObstacleEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  enabler search service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerSearchSession getObstacleEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesProxyManager.getObstacleEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  enabler earch service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return an <code> ObstacleEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerSearchSession getObstacleEnablerSearchSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesManager.getObstacleEnablerSearchSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  enabler earch service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerSearchSession getObstacleEnablerSearchSessionForMap(org.osid.id.Id mapId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesProxyManager.getObstacleEnablerSearchSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  enabler administration service. 
     *
     *  @return an <code> ObstacleEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleEnablerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerAdminSession getObstacleEnablerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesManager.getObstacleEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  enabler administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleEnablerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerAdminSession getObstacleEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesProxyManager.getObstacleEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  enabler administration service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return an <code> ObstacleEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerAdminSession getObstacleEnablerAdminSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesManager.getObstacleEnablerAdminSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  enabler administration service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerAdminSession getObstacleEnablerAdminSessionForMap(org.osid.id.Id mapId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesProxyManager.getObstacleEnablerAdminSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  enabler notification service. 
     *
     *  @param  obstacleEnablerReceiver the notification callback 
     *  @return an <code> ObstacleEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> obstacleEnablerReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerNotificationSession getObstacleEnablerNotificationSession(org.osid.mapping.path.rules.ObstacleEnablerReceiver obstacleEnablerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesManager.getObstacleEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  enabler notification service. 
     *
     *  @param  obstacleEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> obstacleEnablerReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerNotificationSession getObstacleEnablerNotificationSession(org.osid.mapping.path.rules.ObstacleEnablerReceiver obstacleEnablerReceiver, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesProxyManager.getObstacleEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  enabler notification service for the given map. 
     *
     *  @param  obstacleEnablerReceiver the notification callback 
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return an <code> ObstacleEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> obstacleEnablerReceiver 
     *          </code> or <code> mapId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerNotificationSession getObstacleEnablerNotificationSessionForMap(org.osid.mapping.path.rules.ObstacleEnablerReceiver obstacleEnablerReceiver, 
                                                                                                                      org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesManager.getObstacleEnablerNotificationSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  enabler notification service for the given map. 
     *
     *  @param  obstacleEnablerReceiver the notification callback 
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> obstacleEnablerReceiver, 
     *          mapId </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerNotificationSession getObstacleEnablerNotificationSessionForMap(org.osid.mapping.path.rules.ObstacleEnablerReceiver obstacleEnablerReceiver, 
                                                                                                                      org.osid.id.Id mapId, 
                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesProxyManager.getObstacleEnablerNotificationSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup obstacle enabler/map 
     *  mappings for obstacle enablers. 
     *
     *  @return an <code> ObstacleEnablerMapSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleEnablerMap() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerMapSession getObstacleEnablerMapSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesManager.getObstacleEnablerMapSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup obstacle enabler/map 
     *  mappings for obstacle enablers. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleEnablerMapSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleEnablerMap() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerMapSession getObstacleEnablerMapSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesProxyManager.getObstacleEnablerMapSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning obstacle 
     *  enabler to map. 
     *
     *  @return an <code> ObstacleEnablerMapAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleEnablerMapAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerMapAssignmentSession getObstacleEnablerMapAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesManager.getObstacleEnablerMapAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning obstacle 
     *  enabler to map. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleEnablerMapAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleEnablerMapAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerMapAssignmentSession getObstacleEnablerMapAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesProxyManager.getObstacleEnablerMapAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage obstacle enabler smart 
     *  map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return an <code> ObstacleEnablerSmartMapSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleEnablerSmartMap() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerSmartMapSession getObstacleEnablerSmartMapSession(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesManager.getObstacleEnablerSmartMapSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage obstacle enabler smart 
     *  map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleEnablerSmartMapSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleEnablerSmartMap() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerSmartMapSession getObstacleEnablerSmartMapSession(org.osid.id.Id mapId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesProxyManager.getObstacleEnablerSmartMapSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  enabler mapping lookup service for looking up the rules applied to the 
     *  obstacle. 
     *
     *  @return an <code> ObstacleEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerRuleLookupSession getObstacleEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesManager.getObstacleEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  enabler mapping lookup service for looking up the rules applied to an 
     *  obstacle. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerRuleLookupSession getObstacleEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesProxyManager.getObstacleEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  enabler mapping lookup service for the given map for looking up rules 
     *  applied to an obstacle. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return an <code> ObstacleEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerRuleLookupSession getObstacleEnablerRuleLookupSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesManager.getObstacleEnablerRuleLookupSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  enabler mapping lookup service for the given map for looking up rules 
     *  applied to an obstacle. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerRuleLookupSession getObstacleEnablerRuleLookupSessionForMap(org.osid.id.Id mapId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesProxyManager.getObstacleEnablerRuleLookupSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  enabler assignment service to apply to obstacles. 
     *
     *  @return an <code> ObstacleEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerRuleApplicationSession getObstacleEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesManager.getObstacleEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  enabler assignment service to apply to obstacles. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerRuleApplicationSession getObstacleEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesProxyManager.getObstacleEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  enabler assignment service for the given map to apply to obstacles. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return an <code> ObstacleEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerRuleApplicationSession getObstacleEnablerRuleApplicationSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesManager.getObstacleEnablerRuleApplicationSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  enabler assignment service for the given map to apply to obstacles. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerRuleApplicationSession getObstacleEnablerRuleApplicationSessionForMap(org.osid.id.Id mapId, 
                                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.rules.MappingPathRulesProxyManager.getObstacleEnablerRuleApplicationSessionForMap not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.speedZoneEnablerRecordTypes.clear();
        this.speedZoneEnablerRecordTypes.clear();

        this.speedZoneEnablerSearchRecordTypes.clear();
        this.speedZoneEnablerSearchRecordTypes.clear();

        this.signalEnablerRecordTypes.clear();
        this.signalEnablerRecordTypes.clear();

        this.signalEnablerSearchRecordTypes.clear();
        this.signalEnablerSearchRecordTypes.clear();

        this.obstacleEnablerRecordTypes.clear();
        this.obstacleEnablerRecordTypes.clear();

        this.obstacleEnablerSearchRecordTypes.clear();
        this.obstacleEnablerSearchRecordTypes.clear();

        return;
    }
}
