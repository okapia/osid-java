//
// AbstractValueRetrievalSession.java
//
//    A starter implementation framework for providing a Value
//    retrieval service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.configuration.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Value
 *  retrieval service.
 *
 *  Although this abstract class requires only the implementation of
 *  getValues(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractValueRetrievalSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.configuration.ValueRetrievalSession {

    private boolean pedantic    = false;
    private boolean federated   = false;
    private boolean conditional = false;
    private org.osid.configuration.Configuration configuration = new net.okapia.osid.jamocha.nil.configuration.configuration.UnknownConfiguration();
    

    /**
     *  Gets the <code>Configuration/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Configuration Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getConfigurationId() {
        return (this.configuration.getId());
    }


    /**
     *  Gets the <code>Configuration</code> associated with this 
     *  session.
     *
     *  @return the <code>Configuration</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.Configuration getConfiguration()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.configuration);
    }


    /**
     *  Sets the <code>Configuration</code>.
     *
     *  @param  configuration the configuration for this session
     *  @throws org.osid.NullArgumentException <code>configuration</code>
     *          is <code>null</code>
     */

    protected void setConfiguration(org.osid.configuration.Configuration configuration) {
        nullarg(configuration, "configuration");
        this.configuration = configuration;
        return;
    }


    /**
     *  Tests if this user can perform <code>Value</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupValues() {
        return (true);
    }


    /**
     *  A complete view of the <code>Value</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeValueView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Value</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryValueView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include values in configurations which are children
     *  of this configuration in the configuration hierarchy.
     */

    @OSID @Override
    public void useFederatedConfigurationView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts retrievals to this configuration only.
     */

    @OSID @Override
    public void useIsolatedConfigurationView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Returns only values that pass the defined parameter
     *  condition. Some parameter conditions do not require explicit
     *  conditional data to be passed and the <code> Values </code>
     *  returned from any method in this session are filtered on an
     *  implicit condition.
     */

    @OSID @Override
    public void useConditionalView() {
        this.conditional = true;
        return;
    }


    /**
     *  Values that are filtered based on an implicit condition are
     *  not filtered out from methods in this session. Methods that
     *  take an explicit condition as a parameter are filtered on only
     *  those conditions that are specified.
     */

    @OSID @Override
    public void useUnconditionalView() {
        this.conditional = false;
        return;
    }


    /**
     *  Tests if a conditional or unconditional view is set.
     *
     *  @return <code>true</code> if conditional</code>,
     *          <code>false</code> if both unconditional
     */
    
    protected boolean isConditional() {
        return (this.conditional);
    }
    

    /**
     *  Gets a <code> Value </code> for the given parameter <code>
     *  Id. </code> If more than one value exists for the given
     *  parameter, the most preferred value is returned. This method
     *  can be used as a convenience when only one value is
     *  expected. <code> getValuesByParameters() </code> should be
     *  used for getting all the active values.
     *
     *  @param parameterId the <code> Id </code> of the <code>
     *         Parameter </code> to retrieve
     *  @return the value 
     *  @throws org.osid.NotFoundException the <code> parameterId </code> not 
     *          found or no value available 
     *  @throws org.osid.NullArgumentException the <code> parameterId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.configuration.Value getValueByParameter(org.osid.id.Id parameterId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        try (org.osid.configuration.ValueList values = getValuesByParameter(parameterId)) {
            if (values.hasNext()) {
                return (values.getNextValue());
            }

            throw new org.osid.NotFoundException("no value for " + parameterId);
        }
    }


    /**
     *  Gets all the <code> Values </code> for the given parameter <code> Id. 
     *  </code> 
     *
     *  @param  parameterId the <code> Id </code> of the <code> Parameter 
     *          </code> to retrieve 
     *  @return the value list 
     *  @throws org.osid.NotFoundException the <code> parameterId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException the <code> parameterId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public abstract org.osid.configuration.ValueList getValuesByParameter(org.osid.id.Id parameterId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Gets the <code> Values </code> for the given parameter <code>
     *  Ids.  </code> In plenary mode, the values for all parameters
     *  are returned in the order requested or an error results. In
     *  comparative mode, inaccessible values may be omitted or the
     *  values reordered.
     *
     *  @param  parameterIds the <code> Id </code> of the <code> Parameter 
     *          </code> to retrieve 
     *  @return the value list 
     *  @throws org.osid.NotFoundException a parameter <code> Id </code> is 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> parameterIds </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.configuration.ValueList getValuesByParameters(org.osid.id.IdList parameterIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        net.okapia.osid.jamocha.adapter.federator.configuration.value.CompositeValueList values = new net.okapia.osid.jamocha.adapter.federator.configuration.value.CompositeValueList();

        try (org.osid.id.IdList ids = parameterIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    values.addValueList(getValuesByParameter(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("parameter " + id + " not found");
                    } 
                }
            }
        }
            
        values.noMore();
        return (values);
    }


    /**
     *  Gets a value condition for the given parameter. 
     *
     *  @param  parameterId the <code> Id </code> of a <code> Parameter 
     *          </code> 
     *  @return a value condition 
     *  @throws org.osid.NullArgumentException <code> parameterId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public org.osid.configuration.ValueCondition getValueCondition(org.osid.id.Id parameterId) {
        return (new net.okapia.osid.jamocha.nil.configuration.valuecondition.UnknownValueCondition());
    }


    /**
     *  Gets a value in this configuration based on a condition. If multiple 
     *  values are available the most preferred one is returned. The condition 
     *  specified is applied to any or all parameters in this configuration as 
     *  applicable. 
     *
     *  @param  parameterId the <code> Id </code> of a <code> Parameter 
     *          </code> 
     *  @param  valueCondition the condition 
     *  @return the value 
     *  @throws org.osid.NotFoundException parameter <code> Id </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> parameterId </code> or 
     *          <code> valueCondition </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException <code> valueCondition </code> 
     *          not of this service 
     */

    @OSID @Override
    public org.osid.configuration.Value getValueByParameterOnCondition(org.osid.id.Id parameterId, 
                                                                       org.osid.configuration.ValueCondition valueCondition)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        if (!(valueCondition instanceof net.okapia.osid.jamocha.nil.configuration.valuecondition.UnknownValueCondition)) {
            throw new org.osid.UnsupportedException("valueCondition did not originate from getValueCondition()");
        }

        return (getValueByParameter(parameterId));
    }


    /**
     *  Gets all the values for a parameter based on a condition. In plenary 
     *  mode, all values are returned or an error results. In comparative 
     *  mode, inaccessible values may be omitted. 
     *
     *  @param  parameterId the <code> Id </code> of a <code> Parameter 
     *          </code> 
     *  @param  valueCondition the condition 
     *  @return the value list 
     *  @throws org.osid.NotFoundException parameter <code> Id </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> parameterId </code> or 
     *          <code> valueCondition </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException <code> valueCondition </code> is 
     *          not of this service 
     */

    @OSID @Override
    public org.osid.configuration.ValueList getValuesByParameterOnCondition(org.osid.id.Id parameterId, 
                                                                            org.osid.configuration.ValueCondition valueCondition)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (!(valueCondition instanceof net.okapia.osid.jamocha.nil.configuration.valuecondition.UnknownValueCondition)) {
            throw new org.osid.UnsupportedException("valueCondition did not originate from getValueCondition()");
        }

        return (getValuesByParameter(parameterId));
    }


    /**
     *  Gets the values for parameters based on a condition. The specified 
     *  condition is applied to any or all of the parameters as applicable. In 
     *  plenary mode, all values are returned or an error results. In 
     *  comparative mode, inaccessible values may be omitted. 
     *
     *  @param  parameterIds the <code> Id </code> of a <code> Parameter 
     *          </code> 
     *  @param  valueCondition the condition 
     *  @return the value list 
     *  @throws org.osid.NotFoundException a parameter <code> Id </code> is 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> parameterIds </code> or 
     *          <code> valueCondition </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException <code> valueCondition </code> 
     *          not of this service 
     */
    
    @OSID @Override
    public org.osid.configuration.ValueList getValuesByParametersOnCondition(org.osid.id.IdList parameterIds, 
                                                                             org.osid.configuration.ValueCondition valueCondition)
               throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
                      org.osid.PermissionDeniedException {

        if (!(valueCondition instanceof net.okapia.osid.jamocha.nil.configuration.valuecondition.UnknownValueCondition)) {
            throw new org.osid.UnsupportedException("valueCondition did not originate from getValueCondition()");
        }

        return (getValuesByParameters(parameterIds));
    }
}
