//
// AbstractProvisionableSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.provisionable.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractProvisionableSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.provisioning.ProvisionableSearchResults {

    private org.osid.provisioning.ProvisionableList provisionables;
    private final org.osid.provisioning.ProvisionableQueryInspector inspector;
    private final java.util.Collection<org.osid.provisioning.records.ProvisionableSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractProvisionableSearchResults.
     *
     *  @param provisionables the result set
     *  @param provisionableQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>provisionables</code>
     *          or <code>provisionableQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractProvisionableSearchResults(org.osid.provisioning.ProvisionableList provisionables,
                                            org.osid.provisioning.ProvisionableQueryInspector provisionableQueryInspector) {
        nullarg(provisionables, "provisionables");
        nullarg(provisionableQueryInspector, "provisionable query inspectpr");

        this.provisionables = provisionables;
        this.inspector = provisionableQueryInspector;

        return;
    }


    /**
     *  Gets the provisionable list resulting from a search.
     *
     *  @return a provisionable list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionableList getProvisionables() {
        if (this.provisionables == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.provisioning.ProvisionableList provisionables = this.provisionables;
        this.provisionables = null;
	return (provisionables);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.provisioning.ProvisionableQueryInspector getProvisionableQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  provisionable search record <code> Type. </code> This method must
     *  be used to retrieve a provisionable implementing the requested
     *  record.
     *
     *  @param provisionableSearchRecordType a provisionable search 
     *         record type 
     *  @return the provisionable search
     *  @throws org.osid.NullArgumentException
     *          <code>provisionableSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(provisionableSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.ProvisionableSearchResultsRecord getProvisionableSearchResultsRecord(org.osid.type.Type provisionableSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.provisioning.records.ProvisionableSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(provisionableSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(provisionableSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record provisionable search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addProvisionableRecord(org.osid.provisioning.records.ProvisionableSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "provisionable record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
