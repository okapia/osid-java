//
// AbstractQueryBallotConstrainerLookupSession.java
//
//    A BallotConstrainerQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.voting.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A BallotConstrainerQuerySession adapter.
 */

public abstract class AbstractAdapterBallotConstrainerQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.voting.rules.BallotConstrainerQuerySession {

    private final org.osid.voting.rules.BallotConstrainerQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterBallotConstrainerQuerySession.
     *
     *  @param session the underlying ballot constrainer query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterBallotConstrainerQuerySession(org.osid.voting.rules.BallotConstrainerQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codePolls</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codePolls Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getPollsId() {
        return (this.session.getPollsId());
    }


    /**
     *  Gets the {@codePolls</code> associated with this 
     *  session.
     *
     *  @return the {@codePolls</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Polls getPolls()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getPolls());
    }


    /**
     *  Tests if this user can perform {@codeBallotConstrainer</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchBallotConstrainers() {
        return (this.session.canSearchBallotConstrainers());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include ballot constrainers in pollses which are children
     *  of this polls in the polls hierarchy.
     */

    @OSID @Override
    public void useFederatedPollsView() {
        this.session.useFederatedPollsView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this polls only.
     */
    
    @OSID @Override
    public void useIsolatedPollsView() {
        this.session.useIsolatedPollsView();
        return;
    }
    
      
    /**
     *  Gets a ballot constrainer query. The returned query will not have an
     *  extension query.
     *
     *  @return the ballot constrainer query 
     */
      
    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerQuery getBallotConstrainerQuery() {
        return (this.session.getBallotConstrainerQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  ballotConstrainerQuery the ballot constrainer query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code ballotConstrainerQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code ballotConstrainerQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerList getBallotConstrainersByQuery(org.osid.voting.rules.BallotConstrainerQuery ballotConstrainerQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getBallotConstrainersByQuery(ballotConstrainerQuery));
    }
}
