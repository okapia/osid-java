//
// AbstractQueryTimePeriodLookupSession.java
//
//    An inline adapter that maps a TimePeriodLookupSession to
//    a TimePeriodQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a TimePeriodLookupSession to
 *  a TimePeriodQuerySession.
 */

public abstract class AbstractQueryTimePeriodLookupSession
    extends net.okapia.osid.jamocha.calendaring.spi.AbstractTimePeriodLookupSession
    implements org.osid.calendaring.TimePeriodLookupSession {

    private final org.osid.calendaring.TimePeriodQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryTimePeriodLookupSession.
     *
     *  @param querySession the underlying time period query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryTimePeriodLookupSession(org.osid.calendaring.TimePeriodQuerySession querySession) {
        nullarg(querySession, "time period query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Calendar</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Calendar Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCalendarId() {
        return (this.session.getCalendarId());
    }


    /**
     *  Gets the <code>Calendar</code> associated with this 
     *  session.
     *
     *  @return the <code>Calendar</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getCalendar());
    }


    /**
     *  Tests if this user can perform <code>TimePeriod</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupTimePeriods() {
        return (this.session.canSearchTimePeriods());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include time periods in calendars which are children
     *  of this calendar in the calendar hierarchy.
     */

    @OSID @Override
    public void useFederatedCalendarView() {
        this.session.useFederatedCalendarView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this calendar only.
     */

    @OSID @Override
    public void useIsolatedCalendarView() {
        this.session.useIsolatedCalendarView();
        return;
    }
    
     
    /**
     *  Gets the <code>TimePeriod</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>TimePeriod</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>TimePeriod</code> and
     *  retained for compatibility.
     *
     *  @param  timePeriodId <code>Id</code> of the
     *          <code>TimePeriod</code>
     *  @return the time period
     *  @throws org.osid.NotFoundException <code>timePeriodId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>timePeriodId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriod getTimePeriod(org.osid.id.Id timePeriodId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.TimePeriodQuery query = getQuery();
        query.matchId(timePeriodId, true);
        org.osid.calendaring.TimePeriodList timePeriods = this.session.getTimePeriodsByQuery(query);
        if (timePeriods.hasNext()) {
            return (timePeriods.getNextTimePeriod());
        } 
        
        throw new org.osid.NotFoundException(timePeriodId + " not found");
    }


    /**
     *  Gets a <code>TimePeriodList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  timePeriods specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>TimePeriods</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  timePeriodIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>TimePeriod</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>timePeriodIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodList getTimePeriodsByIds(org.osid.id.IdList timePeriodIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.TimePeriodQuery query = getQuery();

        try (org.osid.id.IdList ids = timePeriodIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getTimePeriodsByQuery(query));
    }


    /**
     *  Gets a <code>TimePeriodList</code> corresponding to the given
     *  time period genus <code>Type</code> which does not include
     *  time periods of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  time periods or an error results. Otherwise, the returned list
     *  may contain only those time periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  timePeriodGenusType a timePeriod genus type 
     *  @return the returned <code>TimePeriod</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>timePeriodGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodList getTimePeriodsByGenusType(org.osid.type.Type timePeriodGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.TimePeriodQuery query = getQuery();
        query.matchGenusType(timePeriodGenusType, true);
        return (this.session.getTimePeriodsByQuery(query));
    }


    /**
     *  Gets a <code>TimePeriodList</code> corresponding to the given
     *  time period genus <code>Type</code> and include any additional
     *  time periods with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  time periods or an error results. Otherwise, the returned list
     *  may contain only those time periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  timePeriodGenusType a timePeriod genus type 
     *  @return the returned <code>TimePeriod</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>timePeriodGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodList getTimePeriodsByParentGenusType(org.osid.type.Type timePeriodGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.TimePeriodQuery query = getQuery();
        query.matchParentGenusType(timePeriodGenusType, true);
        return (this.session.getTimePeriodsByQuery(query));
    }


    /**
     *  Gets a <code>TimePeriodList</code> containing the given
     *  time period record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  time periods or an error results. Otherwise, the returned list
     *  may contain only those time periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  timePeriodRecordType a timePeriod record type 
     *  @return the returned <code>TimePeriod</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>timePeriodRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodList getTimePeriodsByRecordType(org.osid.type.Type timePeriodRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.TimePeriodQuery query = getQuery();
        query.matchRecordType(timePeriodRecordType, true);
        return (this.session.getTimePeriodsByQuery(query));
    }


    /**
     *  Gets a <code> TimePeriodList </code> containing the given
     *  <code> DateTime. </code> Time periods containing the given
     *  date are matched.  In plenary mode, the returned list contains
     *  all of the time periods specified in the <code> Id </code>
     *  list, in the order of the list, including duplicates, or an
     *  error results if an <code> Id </code> in the supplied list is
     *  not found or inaccessible. Otherwise, inaccessible <code>
     *  TimePeriods </code> may be omitted from the list and may
     *  present the elements in any order including returning a unique
     *  set.
     *
     *  @param  datetime a date 
     *  @return the returned <code> TimePeriod </code> list 
     *  @throws org.osid.NullArgumentException <code> datetime </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodList getTimePeriodsByDate(org.osid.calendaring.DateTime datetime)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.TimePeriodQuery query = getQuery();
        query.matchTime(datetime, true);
        return (this.session.getTimePeriodsByQuery(query));
    }
        

    /**
     *  Gets a <code> TimePeriodList </code> corresponding to the
     *  given <code> DateTime. </code> Time periods whose start end
     *  end times are included in the given date range are matched.In
     *  plenary mode, the returned list contains all of the time
     *  periods specified in the <code> Id </code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code> Id </code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code> TimePeriods
     *  </code> may be omitted from the list and may present the
     *  elements in any order including returning a unique set.
     *
     *  @param  start start of daterange 
     *  @param  end end of date range 
     *  @return the returned <code> TimePeriod </code> list 
     *  @throws org.osid.InvalidArgumentException <code> end </code> is less 
     *          than <code> start </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodList getTimePeriodsInDateRange(org.osid.calendaring.DateTime start, 
                                                                         org.osid.calendaring.DateTime end)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.TimePeriodQuery query = getQuery();
        query.matchStart(start, end, true);
        query.matchEnd(start, end, true);
        return (this.session.getTimePeriodsByQuery(query));
    }


    /**
     *  Gets all <code>TimePeriods</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  time periods or an error results. Otherwise, the returned list
     *  may contain only those time periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>TimePeriods</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodList getTimePeriods()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.TimePeriodQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getTimePeriodsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.calendaring.TimePeriodQuery getQuery() {
        org.osid.calendaring.TimePeriodQuery query = this.session.getTimePeriodQuery();
        return (query);
    }
}
