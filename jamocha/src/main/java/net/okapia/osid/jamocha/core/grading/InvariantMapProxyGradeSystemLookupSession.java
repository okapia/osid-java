//
// InvariantMapProxyGradeSystemLookupSession
//
//    Implements a GradeSystem lookup service backed by a fixed
//    collection of gradeSystems. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.grading;


/**
 *  Implements a GradeSystem lookup service backed by a fixed
 *  collection of grade systems. The grade systems are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyGradeSystemLookupSession
    extends net.okapia.osid.jamocha.core.grading.spi.AbstractMapGradeSystemLookupSession
    implements org.osid.grading.GradeSystemLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyGradeSystemLookupSession} with no
     *  grade systems.
     *
     *  @param gradebook the gradebook
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code gradebook} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyGradeSystemLookupSession(org.osid.grading.Gradebook gradebook,
                                                  org.osid.proxy.Proxy proxy) {
        setGradebook(gradebook);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyGradeSystemLookupSession} with a single
     *  grade system.
     *
     *  @param gradebook the gradebook
     *  @param gradeSystem a single grade system
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code gradebook},
     *          {@code gradeSystem} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyGradeSystemLookupSession(org.osid.grading.Gradebook gradebook,
                                                  org.osid.grading.GradeSystem gradeSystem, org.osid.proxy.Proxy proxy) {

        this(gradebook, proxy);
        putGradeSystem(gradeSystem);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyGradeSystemLookupSession} using
     *  an array of grade systems.
     *
     *  @param gradebook the gradebook
     *  @param gradeSystems an array of grade systems
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code gradebook},
     *          {@code gradeSystems} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyGradeSystemLookupSession(org.osid.grading.Gradebook gradebook,
                                                  org.osid.grading.GradeSystem[] gradeSystems, org.osid.proxy.Proxy proxy) {

        this(gradebook, proxy);
        putGradeSystems(gradeSystems);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyGradeSystemLookupSession} using a
     *  collection of grade systems.
     *
     *  @param gradebook the gradebook
     *  @param gradeSystems a collection of grade systems
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code gradebook},
     *          {@code gradeSystems} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyGradeSystemLookupSession(org.osid.grading.Gradebook gradebook,
                                                  java.util.Collection<? extends org.osid.grading.GradeSystem> gradeSystems,
                                                  org.osid.proxy.Proxy proxy) {

        this(gradebook, proxy);
        putGradeSystems(gradeSystems);
        return;
    }
}
