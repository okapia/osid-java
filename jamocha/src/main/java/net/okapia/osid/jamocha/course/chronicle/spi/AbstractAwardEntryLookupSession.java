//
// AbstractAwardEntryLookupSession.java
//
//    A starter implementation framework for providing an AwardEntry
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.chronicle.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing an AwardEntry
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getAwardEntries(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractAwardEntryLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.course.chronicle.AwardEntryLookupSession {

    private boolean pedantic      = false;
    private boolean effectiveonly = false;
    private boolean federated     = false;
    private org.osid.course.CourseCatalog courseCatalog = new net.okapia.osid.jamocha.nil.course.coursecatalog.UnknownCourseCatalog();
    

    /**
     *  Gets the <code>CourseCatalog/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>CourseCatalog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.courseCatalog.getId());
    }


    /**
     *  Gets the <code>CourseCatalog</code> associated with this 
     *  session.
     *
     *  @return the <code>CourseCatalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.courseCatalog);
    }


    /**
     *  Sets the <code>CourseCatalog</code>.
     *
     *  @param  courseCatalog the course catalog for this session
     *  @throws org.osid.NullArgumentException <code>courseCatalog</code>
     *          is <code>null</code>
     */

    protected void setCourseCatalog(org.osid.course.CourseCatalog courseCatalog) {
        nullarg(courseCatalog, "course catalog");
        this.courseCatalog = courseCatalog;
        return;
    }


    /**
     *  Tests if this user can perform <code>AwardEntry</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAwardEntries() {
        return (true);
    }


    /**
     *  A complete view of the <code>AwardEntry</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAwardEntryView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>AwardEntry</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAwardEntryView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include award entries in course catalogs which are
     *  children of this course catalog in the course catalog
     *  hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only award entries whose effective dates are current are
     *  returned by methods in this session.
     */

    @OSID @Override
    public void useEffectiveAwardEntryView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All award entries of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveAwardEntryView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>AwardEntry</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>AwardEntry</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>AwardEntry</code> and
     *  retained for compatibility.
     *
     *  In effective mode, award entries are returned that are currently
     *  effective.  In any effective mode, effective award entries and
     *  those currently expired are returned.
     *
     *  @param  awardEntryId <code>Id</code> of the
     *          <code>AwardEntry</code>
     *  @return the award entry
     *  @throws org.osid.NotFoundException <code>awardEntryId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>awardEntryId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntry getAwardEntry(org.osid.id.Id awardEntryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.course.chronicle.AwardEntryList awardEntries = getAwardEntries()) {
            while (awardEntries.hasNext()) {
                org.osid.course.chronicle.AwardEntry awardEntry = awardEntries.getNextAwardEntry();
                if (awardEntry.getId().equals(awardEntryId)) {
                    return (awardEntry);
                }
            }
        } 

        throw new org.osid.NotFoundException(awardEntryId + " not found");
    }


    /**
     *  Gets an <code>AwardEntryList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  awardEntries specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>AwardEntries</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, award entries are returned that are currently effective.
     *  In any effective mode, effective award entries and those currently expired
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getAwardEntries()</code>.
     *
     *  @param  awardEntryIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>AwardEntry</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>awardEntryIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntryList getAwardEntriesByIds(org.osid.id.IdList awardEntryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.course.chronicle.AwardEntry> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = awardEntryIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getAwardEntry(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("award entry " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.course.chronicle.awardentry.LinkedAwardEntryList(ret));
    }


    /**
     *  Gets an <code>AwardEntryList</code> corresponding to the given
     *  award entry genus <code>Type</code> which does not include
     *  award entries of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  award entries or an error results. Otherwise, the returned list
     *  may contain only those award entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, award entries are returned that are currently effective.
     *  In any effective mode, effective award entries and those currently expired
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getAwardEntries()</code>.
     *
     *  @param  awardEntryGenusType an awardEntry genus type 
     *  @return the returned <code>AwardEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>awardEntryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntryList getAwardEntriesByGenusType(org.osid.type.Type awardEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.chronicle.awardentry.AwardEntryGenusFilterList(getAwardEntries(), awardEntryGenusType));
    }


    /**
     *  Gets an <code>AwardEntryList</code> corresponding to the given
     *  award entry genus <code>Type</code> and include any additional
     *  award entries with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  award entries or an error results. Otherwise, the returned list
     *  may contain only those award entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, award entries are returned that are currently
     *  effective.  In any effective mode, effective award entries and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getAwardEntries()</code>.
     *
     *  @param  awardEntryGenusType an awardEntry genus type 
     *  @return the returned <code>AwardEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>awardEntryGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntryList getAwardEntriesByParentGenusType(org.osid.type.Type awardEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getAwardEntriesByGenusType(awardEntryGenusType));
    }


    /**
     *  Gets an <code>AwardEntryList</code> containing the given
     *  award entry record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  award entries or an error results. Otherwise, the returned list
     *  may contain only those award entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, award entries are returned that are currently
     *  effective.  In any effective mode, effective award entries and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getAwardEntries()</code>.
     *
     *  @param  awardEntryRecordType an awardEntry record type 
     *  @return the returned <code>AwardEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>awardEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntryList getAwardEntriesByRecordType(org.osid.type.Type awardEntryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.chronicle.awardentry.AwardEntryRecordFilterList(getAwardEntries(), awardEntryRecordType));
    }


    /**
     *  Gets an <code>AwardEntryList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  award entries or an error results. Otherwise, the returned list
     *  may contain only those award entries that are accessible
     *  through this session.
     *  
     *  In active mode, award entries are returned that are currently
     *  active. In any status mode, active and inactive award entries
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>AwardEntry</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.chronicle.AwardEntryList getAwardEntriesOnDate(org.osid.calendaring.DateTime from, 
                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.chronicle.awardentry.TemporalAwardEntryFilterList(getAwardEntries(), from, to));
    }
        

    /**
     *  Gets a list of award entries corresponding to a student
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  award entries or an error results. Otherwise, the returned list
     *  may contain only those award entries that are accessible
     *  through this session.
     *
     *  In effective mode, award entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  award entries and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the student
     *  @return the returned <code>AwardEntryList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.course.chronicle.AwardEntryList getAwardEntriesForStudent(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.chronicle.awardentry.AwardEntryFilterList(new StudentFilter(resourceId), getAwardEntries()));
    }


    /**
     *  Gets a list of award entries corresponding to a student
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known award
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those award entries that are accessible through
     *  this session.
     *
     *  In effective mode, award entries are returned that are
     *  currently effective.  In any effective mode, effective award
     *  entries and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the student
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>AwardEntryList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntryList getAwardEntriesForStudentOnDate(org.osid.id.Id resourceId,
                                                                                    org.osid.calendaring.DateTime from,
                                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.chronicle.awardentry.TemporalAwardEntryFilterList(getAwardEntriesForStudent(resourceId), from, to));
    }


    /**
     *  Gets a list of award entries corresponding to a award
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known award
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those award entries that are accessible through
     *  this session.
     *
     *  In effective mode, award entries are returned that are
     *  currently effective.  In any effective mode, effective award
     *  entries and those currently expired are returned.
     *
     *  @param  awardId the <code>Id</code> of the award
     *  @return the returned <code>AwardEntryList</code>
     *  @throws org.osid.NullArgumentException <code>awardId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.course.chronicle.AwardEntryList getAwardEntriesForAward(org.osid.id.Id awardId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.chronicle.awardentry.AwardEntryFilterList(new AwardFilter(awardId), getAwardEntries()));
    }


    /**
     *  Gets a list of award entries corresponding to a award
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  award entries or an error results. Otherwise, the returned list
     *  may contain only those award entries that are accessible
     *  through this session.
     *
     *  In effective mode, award entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  award entries and those currently expired are returned.
     *
     *  @param  awardId the <code>Id</code> of the award
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>AwardEntryList</code>
     *  @throws org.osid.NullArgumentException <code>awardId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntryList getAwardEntriesForAwardOnDate(org.osid.id.Id awardId,
                                                                                  org.osid.calendaring.DateTime from,
                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.chronicle.awardentry.TemporalAwardEntryFilterList(getAwardEntriesForAward(awardId), from, to));
    }


    /**
     *  Gets a list of award entries corresponding to student and award
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  award entries or an error results. Otherwise, the returned list
     *  may contain only those award entries that are accessible
     *  through this session.
     *
     *  In effective mode, award entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  award entries and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the student
     *  @param  awardId the <code>Id</code> of the award
     *  @return the returned <code>AwardEntryList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>awardId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.course.chronicle.AwardEntryList getAwardEntriesForStudentAndAward(org.osid.id.Id resourceId,
                                                                                      org.osid.id.Id awardId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.chronicle.awardentry.AwardEntryFilterList(new AwardFilter(awardId), getAwardEntriesForStudent(resourceId)));
    }


    /**
     *  Gets a list of award entries corresponding to student and award
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  award entries or an error results. Otherwise, the returned list
     *  may contain only those award entries that are accessible
     *  through this session.
     *
     *  In effective mode, award entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  award entries and those currently expired are returned.
     *
     *  @param  awardId the <code>Id</code> of the award
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>AwardEntryList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>awardId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntryList getAwardEntriesForStudentAndAwardOnDate(org.osid.id.Id resourceId,
                                                                                            org.osid.id.Id awardId,
                                                                                            org.osid.calendaring.DateTime from,
                                                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.chronicle.awardentry.TemporalAwardEntryFilterList(getAwardEntriesForStudentAndAward(resourceId, awardId), from, to));
    }


    /**
     *  Gets all <code>AwardEntries</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  award entries or an error results. Otherwise, the returned list
     *  may contain only those award entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, award entries are returned that are currently
     *  effective.  In any effective mode, effective award entries and
     *  those currently expired are returned.
     *
     *  @return a list of <code>AwardEntries</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.course.chronicle.AwardEntryList getAwardEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the award entry list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of award entries
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.course.chronicle.AwardEntryList filterAwardEntriesOnViews(org.osid.course.chronicle.AwardEntryList list)
        throws org.osid.OperationFailedException {

        org.osid.course.chronicle.AwardEntryList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.course.chronicle.awardentry.EffectiveAwardEntryFilterList(ret);
        }

        return (ret);
    }

    public static class StudentFilter
        implements net.okapia.osid.jamocha.inline.filter.course.chronicle.awardentry.AwardEntryFilter {
         
        private final org.osid.id.Id resourceId;
         
         
        /**
         *  Constructs a new <code>StudentFilter</code>.
         *
         *  @param resourceId the student to filter
         *  @throws org.osid.NullArgumentException
         *          <code>resourceId</code> is <code>null</code>
         */
        
        public StudentFilter(org.osid.id.Id resourceId) {
            nullarg(resourceId, "student Id");
            this.resourceId = resourceId;
            return;
        }

         
        /**
         *  Used by the AwardEntryFilterList to filter the 
         *  award entry list based on student.
         *
         *  @param awardEntry the award entry
         *  @return <code>true</code> to pass the award entry,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.course.chronicle.AwardEntry awardEntry) {
            return (awardEntry.getStudentId().equals(this.resourceId));
        }
    }


    public static class AwardFilter
        implements net.okapia.osid.jamocha.inline.filter.course.chronicle.awardentry.AwardEntryFilter {
         
        private final org.osid.id.Id awardId;
         
         
        /**
         *  Constructs a new <code>AwardFilter</code>.
         *
         *  @param awardId the award to filter
         *  @throws org.osid.NullArgumentException
         *          <code>awardId</code> is <code>null</code>
         */
        
        public AwardFilter(org.osid.id.Id awardId) {
            nullarg(awardId, "award Id");
            this.awardId = awardId;
            return;
        }

         
        /**
         *  Used by the AwardEntryFilterList to filter the 
         *  award entry list based on award.
         *
         *  @param awardEntry the award entry
         *  @return <code>true</code> to pass the award entry,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.course.chronicle.AwardEntry awardEntry) {
            return (awardEntry.getAwardId().equals(this.awardId));
        }
    }
}
