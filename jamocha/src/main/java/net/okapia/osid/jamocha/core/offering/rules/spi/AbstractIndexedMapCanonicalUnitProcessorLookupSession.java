//
// AbstractIndexedMapCanonicalUnitProcessorLookupSession.java
//
//    A simple framework for providing a CanonicalUnitProcessor lookup service
//    backed by a fixed collection of canonical unit processors with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.offering.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a CanonicalUnitProcessor lookup service backed by a
 *  fixed collection of canonical unit processors. The canonical unit processors are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some canonical unit processors may be compatible
 *  with more types than are indicated through these canonical unit processor
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>CanonicalUnitProcessors</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapCanonicalUnitProcessorLookupSession
    extends AbstractMapCanonicalUnitProcessorLookupSession
    implements org.osid.offering.rules.CanonicalUnitProcessorLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.offering.rules.CanonicalUnitProcessor> canonicalUnitProcessorsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.offering.rules.CanonicalUnitProcessor>());
    private final MultiMap<org.osid.type.Type, org.osid.offering.rules.CanonicalUnitProcessor> canonicalUnitProcessorsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.offering.rules.CanonicalUnitProcessor>());


    /**
     *  Makes a <code>CanonicalUnitProcessor</code> available in this session.
     *
     *  @param  canonicalUnitProcessor a canonical unit processor
     *  @throws org.osid.NullArgumentException <code>canonicalUnitProcessor<code> is
     *          <code>null</code>
     */

    @Override
    protected void putCanonicalUnitProcessor(org.osid.offering.rules.CanonicalUnitProcessor canonicalUnitProcessor) {
        super.putCanonicalUnitProcessor(canonicalUnitProcessor);

        this.canonicalUnitProcessorsByGenus.put(canonicalUnitProcessor.getGenusType(), canonicalUnitProcessor);
        
        try (org.osid.type.TypeList types = canonicalUnitProcessor.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.canonicalUnitProcessorsByRecord.put(types.getNextType(), canonicalUnitProcessor);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }

    
    /**
     *  Makes an array of canonical unit processors available in this session.
     *
     *  @param  canonicalUnitProcessors an array of canonical unit processors
     *  @throws org.osid.NullArgumentException <code>canonicalUnitProcessors<code>
     *          is <code>null</code>
     */

    @Override
    protected void putCanonicalUnitProcessors(org.osid.offering.rules.CanonicalUnitProcessor[] canonicalUnitProcessors) {
        for (org.osid.offering.rules.CanonicalUnitProcessor canonicalUnitProcessor : canonicalUnitProcessors) {
            putCanonicalUnitProcessor(canonicalUnitProcessor);
        }

        return;
    }


    /**
     *  Makes a collection of canonical unit processors available in this session.
     *
     *  @param  canonicalUnitProcessors a collection of canonical unit processors
     *  @throws org.osid.NullArgumentException <code>canonicalUnitProcessors<code>
     *          is <code>null</code>
     */

    @Override
    protected void putCanonicalUnitProcessors(java.util.Collection<? extends org.osid.offering.rules.CanonicalUnitProcessor> canonicalUnitProcessors) {
        for (org.osid.offering.rules.CanonicalUnitProcessor canonicalUnitProcessor : canonicalUnitProcessors) {
            putCanonicalUnitProcessor(canonicalUnitProcessor);
        }

        return;
    }


    /**
     *  Removes a canonical unit processor from this session.
     *
     *  @param canonicalUnitProcessorId the <code>Id</code> of the canonical unit processor
     *  @throws org.osid.NullArgumentException <code>canonicalUnitProcessorId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeCanonicalUnitProcessor(org.osid.id.Id canonicalUnitProcessorId) {
        org.osid.offering.rules.CanonicalUnitProcessor canonicalUnitProcessor;
        try {
            canonicalUnitProcessor = getCanonicalUnitProcessor(canonicalUnitProcessorId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.canonicalUnitProcessorsByGenus.remove(canonicalUnitProcessor.getGenusType());

        try (org.osid.type.TypeList types = canonicalUnitProcessor.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.canonicalUnitProcessorsByRecord.remove(types.getNextType(), canonicalUnitProcessor);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeCanonicalUnitProcessor(canonicalUnitProcessorId);
        return;
    }


    /**
     *  Gets a <code>CanonicalUnitProcessorList</code> corresponding to the given
     *  canonical unit processor genus <code>Type</code> which does not include
     *  canonical unit processors of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known canonical unit processors or an error results. Otherwise,
     *  the returned list may contain only those canonical unit processors that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  canonicalUnitProcessorGenusType a canonical unit processor genus type 
     *  @return the returned <code>CanonicalUnitProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitProcessorGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorList getCanonicalUnitProcessorsByGenusType(org.osid.type.Type canonicalUnitProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.offering.rules.canonicalunitprocessor.ArrayCanonicalUnitProcessorList(this.canonicalUnitProcessorsByGenus.get(canonicalUnitProcessorGenusType)));
    }


    /**
     *  Gets a <code>CanonicalUnitProcessorList</code> containing the given
     *  canonical unit processor record <code>Type</code>. In plenary mode, the
     *  returned list contains all known canonical unit processors or an error
     *  results. Otherwise, the returned list may contain only those
     *  canonical unit processors that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  canonicalUnitProcessorRecordType a canonical unit processor record type 
     *  @return the returned <code>canonicalUnitProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitProcessorRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorList getCanonicalUnitProcessorsByRecordType(org.osid.type.Type canonicalUnitProcessorRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.offering.rules.canonicalunitprocessor.ArrayCanonicalUnitProcessorList(this.canonicalUnitProcessorsByRecord.get(canonicalUnitProcessorRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.canonicalUnitProcessorsByGenus.clear();
        this.canonicalUnitProcessorsByRecord.clear();

        super.close();

        return;
    }
}
