//
// AbstractAdapterObstacleEnablerLookupSession.java
//
//    An ObstacleEnabler lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.mapping.path.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An ObstacleEnabler lookup session adapter.
 */

public abstract class AbstractAdapterObstacleEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.mapping.path.rules.ObstacleEnablerLookupSession {

    private final org.osid.mapping.path.rules.ObstacleEnablerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterObstacleEnablerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterObstacleEnablerLookupSession(org.osid.mapping.path.rules.ObstacleEnablerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Map/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Map Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getMapId() {
        return (this.session.getMapId());
    }


    /**
     *  Gets the {@code Map} associated with this session.
     *
     *  @return the {@code Map} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.Map getMap()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getMap());
    }


    /**
     *  Tests if this user can perform {@code ObstacleEnabler} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupObstacleEnablers() {
        return (this.session.canLookupObstacleEnablers());
    }


    /**
     *  A complete view of the {@code ObstacleEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeObstacleEnablerView() {
        this.session.useComparativeObstacleEnablerView();
        return;
    }


    /**
     *  A complete view of the {@code ObstacleEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryObstacleEnablerView() {
        this.session.usePlenaryObstacleEnablerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include obstacle enablers in maps which are children
     *  of this map in the map hierarchy.
     */

    @OSID @Override
    public void useFederatedMapView() {
        this.session.useFederatedMapView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this map only.
     */

    @OSID @Override
    public void useIsolatedMapView() {
        this.session.useIsolatedMapView();
        return;
    }
    

    /**
     *  Only active obstacle enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveObstacleEnablerView() {
        this.session.useActiveObstacleEnablerView();
        return;
    }


    /**
     *  Active and inactive obstacle enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusObstacleEnablerView() {
        this.session.useAnyStatusObstacleEnablerView();
        return;
    }
    
     
    /**
     *  Gets the {@code ObstacleEnabler} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code ObstacleEnabler} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code ObstacleEnabler} and
     *  retained for compatibility.
     *
     *  In active mode, obstacle enablers are returned that are currently
     *  active. In any status mode, active and inactive obstacle enablers
     *  are returned.
     *
     *  @param obstacleEnablerId {@code Id} of the {@code ObstacleEnabler}
     *  @return the obstacle enabler
     *  @throws org.osid.NotFoundException {@code obstacleEnablerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code obstacleEnablerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnabler getObstacleEnabler(org.osid.id.Id obstacleEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getObstacleEnabler(obstacleEnablerId));
    }


    /**
     *  Gets an {@code ObstacleEnablerList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  obstacleEnablers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code ObstacleEnablers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, obstacle enablers are returned that are currently
     *  active. In any status mode, active and inactive obstacle enablers
     *  are returned.
     *
     *  @param  obstacleEnablerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code ObstacleEnabler} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code obstacleEnablerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerList getObstacleEnablersByIds(org.osid.id.IdList obstacleEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getObstacleEnablersByIds(obstacleEnablerIds));
    }


    /**
     *  Gets an {@code ObstacleEnablerList} corresponding to the given
     *  obstacle enabler genus {@code Type} which does not include
     *  obstacle enablers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  obstacle enablers or an error results. Otherwise, the returned list
     *  may contain only those obstacle enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, obstacle enablers are returned that are currently
     *  active. In any status mode, active and inactive obstacle enablers
     *  are returned.
     *
     *  @param  obstacleEnablerGenusType an obstacleEnabler genus type 
     *  @return the returned {@code ObstacleEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code obstacleEnablerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerList getObstacleEnablersByGenusType(org.osid.type.Type obstacleEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getObstacleEnablersByGenusType(obstacleEnablerGenusType));
    }


    /**
     *  Gets an {@code ObstacleEnablerList} corresponding to the given
     *  obstacle enabler genus {@code Type} and include any additional
     *  obstacle enablers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  obstacle enablers or an error results. Otherwise, the returned list
     *  may contain only those obstacle enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, obstacle enablers are returned that are currently
     *  active. In any status mode, active and inactive obstacle enablers
     *  are returned.
     *
     *  @param  obstacleEnablerGenusType an obstacleEnabler genus type 
     *  @return the returned {@code ObstacleEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code obstacleEnablerGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerList getObstacleEnablersByParentGenusType(org.osid.type.Type obstacleEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getObstacleEnablersByParentGenusType(obstacleEnablerGenusType));
    }


    /**
     *  Gets an {@code ObstacleEnablerList} containing the given
     *  obstacle enabler record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  obstacle enablers or an error results. Otherwise, the returned list
     *  may contain only those obstacle enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, obstacle enablers are returned that are currently
     *  active. In any status mode, active and inactive obstacle enablers
     *  are returned.
     *
     *  @param  obstacleEnablerRecordType an obstacleEnabler record type 
     *  @return the returned {@code ObstacleEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code obstacleEnablerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerList getObstacleEnablersByRecordType(org.osid.type.Type obstacleEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getObstacleEnablersByRecordType(obstacleEnablerRecordType));
    }


    /**
     *  Gets an {@code ObstacleEnablerList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  obstacle enablers or an error results. Otherwise, the returned list
     *  may contain only those obstacle enablers that are accessible
     *  through this session.
     *  
     *  In active mode, obstacle enablers are returned that are currently
     *  active. In any status mode, active and inactive obstacle enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code ObstacleEnabler} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerList getObstacleEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getObstacleEnablersOnDate(from, to));
    }
        

    /**
     *  Gets an {@code ObstacleEnablerList } which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  obstacle enablers or an error results. Otherwise, the returned list
     *  may contain only those obstacle enablers that are accessible
     *  through this session.
     *
     *  In active mode, obstacle enablers are returned that are currently
     *  active. In any status mode, active and inactive obstacle enablers
     *  are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned {@code ObstacleEnabler} list
     *  @throws org.osid.InvalidArgumentException {@code from} is
     *          greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code agent},
     *          {@code from}, or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerList getObstacleEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (this.session.getObstacleEnablersOnDateWithAgent(agentId, from, to));
    }


    /**
     *  Gets all {@code ObstacleEnablers}. 
     *
     *  In plenary mode, the returned list contains all known
     *  obstacle enablers or an error results. Otherwise, the returned list
     *  may contain only those obstacle enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, obstacle enablers are returned that are currently
     *  active. In any status mode, active and inactive obstacle enablers
     *  are returned.
     *
     *  @return a list of {@code ObstacleEnablers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerList getObstacleEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getObstacleEnablers());
    }
}
