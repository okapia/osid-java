//
// AbstractMapFamilyLookupSession
//
//    A simple framework for providing a Family lookup service
//    backed by a fixed collection of families.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.relationship.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Family lookup service backed by a
 *  fixed collection of families. The families are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Families</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapFamilyLookupSession
    extends net.okapia.osid.jamocha.relationship.spi.AbstractFamilyLookupSession
    implements org.osid.relationship.FamilyLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.relationship.Family> families = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.relationship.Family>());


    /**
     *  Makes a <code>Family</code> available in this session.
     *
     *  @param  family a family
     *  @throws org.osid.NullArgumentException <code>family<code>
     *          is <code>null</code>
     */

    protected void putFamily(org.osid.relationship.Family family) {
        this.families.put(family.getId(), family);
        return;
    }


    /**
     *  Makes an array of families available in this session.
     *
     *  @param  families an array of families
     *  @throws org.osid.NullArgumentException <code>families<code>
     *          is <code>null</code>
     */

    protected void putFamilies(org.osid.relationship.Family[] families) {
        putFamilies(java.util.Arrays.asList(families));
        return;
    }


    /**
     *  Makes a collection of families available in this session.
     *
     *  @param  families a collection of families
     *  @throws org.osid.NullArgumentException <code>families<code>
     *          is <code>null</code>
     */

    protected void putFamilies(java.util.Collection<? extends org.osid.relationship.Family> families) {
        for (org.osid.relationship.Family family : families) {
            this.families.put(family.getId(), family);
        }

        return;
    }


    /**
     *  Removes a Family from this session.
     *
     *  @param  familyId the <code>Id</code> of the family
     *  @throws org.osid.NullArgumentException <code>familyId<code> is
     *          <code>null</code>
     */

    protected void removeFamily(org.osid.id.Id familyId) {
        this.families.remove(familyId);
        return;
    }


    /**
     *  Gets the <code>Family</code> specified by its <code>Id</code>.
     *
     *  @param  familyId <code>Id</code> of the <code>Family</code>
     *  @return the family
     *  @throws org.osid.NotFoundException <code>familyId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>familyId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.Family getFamily(org.osid.id.Id familyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.relationship.Family family = this.families.get(familyId);
        if (family == null) {
            throw new org.osid.NotFoundException("family not found: " + familyId);
        }

        return (family);
    }


    /**
     *  Gets all <code>Families</code>. In plenary mode, the returned
     *  list contains all known families or an error
     *  results. Otherwise, the returned list may contain only those
     *  families that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Families</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.FamilyList getFamilies()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.relationship.family.ArrayFamilyList(this.families.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.families.clear();
        super.close();
        return;
    }
}
