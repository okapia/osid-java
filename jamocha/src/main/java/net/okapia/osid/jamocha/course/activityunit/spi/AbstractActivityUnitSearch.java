//
// AbstractActivityUnitSearch.java
//
//     A template for making an ActivityUnit Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.activityunit.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing activity unit searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractActivityUnitSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.course.ActivityUnitSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.course.records.ActivityUnitSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.course.ActivityUnitSearchOrder activityUnitSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of activity units. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  activityUnitIds list of activity units
     *  @throws org.osid.NullArgumentException
     *          <code>activityUnitIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongActivityUnits(org.osid.id.IdList activityUnitIds) {
        while (activityUnitIds.hasNext()) {
            try {
                this.ids.add(activityUnitIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongActivityUnits</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of activity unit Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getActivityUnitIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  activityUnitSearchOrder activity unit search order 
     *  @throws org.osid.NullArgumentException
     *          <code>activityUnitSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>activityUnitSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderActivityUnitResults(org.osid.course.ActivityUnitSearchOrder activityUnitSearchOrder) {
	this.activityUnitSearchOrder = activityUnitSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.course.ActivityUnitSearchOrder getActivityUnitSearchOrder() {
	return (this.activityUnitSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given activity unit search
     *  record <code> Type. </code> This method must be used to
     *  retrieve an activity unit implementing the requested record.
     *
     *  @param activityUnitSearchRecordType an activity unit search record
     *         type
     *  @return the activity unit search record
     *  @throws org.osid.NullArgumentException
     *          <code>activityUnitSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(activityUnitSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.records.ActivityUnitSearchRecord getActivityUnitSearchRecord(org.osid.type.Type activityUnitSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.course.records.ActivityUnitSearchRecord record : this.records) {
            if (record.implementsRecordType(activityUnitSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(activityUnitSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this activity unit search. 
     *
     *  @param activityUnitSearchRecord activity unit search record
     *  @param activityUnitSearchRecordType activityUnit search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addActivityUnitSearchRecord(org.osid.course.records.ActivityUnitSearchRecord activityUnitSearchRecord, 
                                           org.osid.type.Type activityUnitSearchRecordType) {

        addRecordType(activityUnitSearchRecordType);
        this.records.add(activityUnitSearchRecord);        
        return;
    }
}
