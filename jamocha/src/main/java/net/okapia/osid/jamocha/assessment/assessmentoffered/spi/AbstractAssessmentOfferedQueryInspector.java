//
// AbstractAssessmentOfferedQueryInspector.java
//
//     A template for making an AssessmentOfferedQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.assessmentoffered.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for assessments offered.
 */

public abstract class AbstractAssessmentOfferedQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.assessment.AssessmentOfferedQueryInspector {

    private final java.util.Collection<org.osid.assessment.records.AssessmentOfferedQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the assessment <code> Id </code> query terms. 
     *
     *  @return the assessment <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAssessmentIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the assessment query terms. 
     *
     *  @return the assessment terms 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentQueryInspector[] getAssessmentTerms() {
        return (new org.osid.assessment.AssessmentQueryInspector[0]);
    }


    /**
     *  Gets the level <code> Id </code> query terms. 
     *
     *  @return the level <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getLevelIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the level query terms. 
     *
     *  @return the level terms 
     */

    @OSID @Override
    public org.osid.grading.GradeQueryInspector[] getLevelTerms() {
        return (new org.osid.grading.GradeQueryInspector[0]);
    }


    /**
     *  Gets the sequential query terms. 
     *
     *  @return the boolean terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getItemsSequentialTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the shuffled query terms. 
     *
     *  @return the boolean terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getItemsShuffledTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the start time query terms. 
     *
     *  @return the start time terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeTerm[] getStartTimeTerms() {
        return (new org.osid.search.terms.DateTimeTerm[0]);
    }


    /**
     *  Gets the deadline query terms. 
     *
     *  @return the deadline terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeTerm[] getDeadlineTerms() {
        return (new org.osid.search.terms.DateTimeTerm[0]);
    }


    /**
     *  Gets the deadline query terms. 
     *
     *  @return the duration terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationTerm[] getDurationTerms() {
        return (new org.osid.search.terms.DurationTerm[0]);
    }


    /**
     *  Gets the grade system <code> Id </code> query terms. 
     *
     *  @return the grade system <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getScoreSystemIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the grade system query terms. 
     *
     *  @return the grade system terms 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQueryInspector[] getScoreSystemTerms() {
        return (new org.osid.grading.GradeSystemQueryInspector[0]);
    }


    /**
     *  Gets the grade system <code> Id </code> query terms. 
     *
     *  @return the grade system <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGradeSystemIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the grade system query terms. 
     *
     *  @return the grade system terms 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQueryInspector[] getGradeSystemTerms() {
        return (new org.osid.grading.GradeSystemQueryInspector[0]);
    }


    /**
     *  Gets the assessment offered <code> Id </code> query terms. 
     *
     *  @return the assessment offered <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRubricIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the assessment query terms. 
     *
     *  @return the assessment offered terms 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedQueryInspector[] getRubricTerms() {
        return (new org.osid.assessment.AssessmentOfferedQueryInspector[0]);
    }


    /**
     *  Gets the assessment taken <code> Id </code> query terms. 
     *
     *  @return the assessment taken <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAssessmentTakenIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the assessment taken query terms. 
     *
     *  @return the assessment taken terms 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenQueryInspector[] getAssessmentTakenTerms() {
        return (new org.osid.assessment.AssessmentTakenQueryInspector[0]);
    }


    /**
     *  Gets the bank <code> Id </code> query terms. 
     *
     *  @return the bank <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBankIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the bank query terms. 
     *
     *  @return the bank terms 
     */

    @OSID @Override
    public org.osid.assessment.BankQueryInspector[] getBankTerms() {
        return (new org.osid.assessment.BankQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given assessment offered query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an assessment offered implementing the requested record.
     *
     *  @param assessmentOfferedRecordType an assessment offered record type
     *  @return the assessment offered query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentOfferedRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(assessmentOfferedRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.records.AssessmentOfferedQueryInspectorRecord getAssessmentOfferedQueryInspectorRecord(org.osid.type.Type assessmentOfferedRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.records.AssessmentOfferedQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(assessmentOfferedRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assessmentOfferedRecordType + " is not supported");
    }


    /**
     *  Adds a record to this assessment offered query. 
     *
     *  @param assessmentOfferedQueryInspectorRecord assessment offered query inspector
     *         record
     *  @param assessmentOfferedRecordType assessmentOffered record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAssessmentOfferedQueryInspectorRecord(org.osid.assessment.records.AssessmentOfferedQueryInspectorRecord assessmentOfferedQueryInspectorRecord, 
                                                   org.osid.type.Type assessmentOfferedRecordType) {

        addRecordType(assessmentOfferedRecordType);
        nullarg(assessmentOfferedRecordType, "assessment offered record type");
        this.records.add(assessmentOfferedQueryInspectorRecord);        
        return;
    }
}
