//
// AbstractIndexedMapBallotConstrainerLookupSession.java
//
//    A simple framework for providing a BallotConstrainer lookup service
//    backed by a fixed collection of ballot constrainers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.voting.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a BallotConstrainer lookup service backed by a
 *  fixed collection of ballot constrainers. The ballot constrainers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some ballot constrainers may be compatible
 *  with more types than are indicated through these ballot constrainer
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>BallotConstrainers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapBallotConstrainerLookupSession
    extends AbstractMapBallotConstrainerLookupSession
    implements org.osid.voting.rules.BallotConstrainerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.voting.rules.BallotConstrainer> ballotConstrainersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.voting.rules.BallotConstrainer>());
    private final MultiMap<org.osid.type.Type, org.osid.voting.rules.BallotConstrainer> ballotConstrainersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.voting.rules.BallotConstrainer>());


    /**
     *  Makes a <code>BallotConstrainer</code> available in this session.
     *
     *  @param  ballotConstrainer a ballot constrainer
     *  @throws org.osid.NullArgumentException <code>ballotConstrainer<code> is
     *          <code>null</code>
     */

    @Override
    protected void putBallotConstrainer(org.osid.voting.rules.BallotConstrainer ballotConstrainer) {
        super.putBallotConstrainer(ballotConstrainer);

        this.ballotConstrainersByGenus.put(ballotConstrainer.getGenusType(), ballotConstrainer);
        
        try (org.osid.type.TypeList types = ballotConstrainer.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.ballotConstrainersByRecord.put(types.getNextType(), ballotConstrainer);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a ballot constrainer from this session.
     *
     *  @param ballotConstrainerId the <code>Id</code> of the ballot constrainer
     *  @throws org.osid.NullArgumentException <code>ballotConstrainerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeBallotConstrainer(org.osid.id.Id ballotConstrainerId) {
        org.osid.voting.rules.BallotConstrainer ballotConstrainer;
        try {
            ballotConstrainer = getBallotConstrainer(ballotConstrainerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.ballotConstrainersByGenus.remove(ballotConstrainer.getGenusType());

        try (org.osid.type.TypeList types = ballotConstrainer.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.ballotConstrainersByRecord.remove(types.getNextType(), ballotConstrainer);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeBallotConstrainer(ballotConstrainerId);
        return;
    }


    /**
     *  Gets a <code>BallotConstrainerList</code> corresponding to the given
     *  ballot constrainer genus <code>Type</code> which does not include
     *  ballot constrainers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known ballot constrainers or an error results. Otherwise,
     *  the returned list may contain only those ballot constrainers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  ballotConstrainerGenusType a ballot constrainer genus type 
     *  @return the returned <code>BallotConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>ballotConstrainerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerList getBallotConstrainersByGenusType(org.osid.type.Type ballotConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.voting.rules.ballotconstrainer.ArrayBallotConstrainerList(this.ballotConstrainersByGenus.get(ballotConstrainerGenusType)));
    }


    /**
     *  Gets a <code>BallotConstrainerList</code> containing the given
     *  ballot constrainer record <code>Type</code>. In plenary mode, the
     *  returned list contains all known ballot constrainers or an error
     *  results. Otherwise, the returned list may contain only those
     *  ballot constrainers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  ballotConstrainerRecordType a ballot constrainer record type 
     *  @return the returned <code>ballotConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>ballotConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerList getBallotConstrainersByRecordType(org.osid.type.Type ballotConstrainerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.voting.rules.ballotconstrainer.ArrayBallotConstrainerList(this.ballotConstrainersByRecord.get(ballotConstrainerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.ballotConstrainersByGenus.clear();
        this.ballotConstrainersByRecord.clear();

        super.close();

        return;
    }
}
