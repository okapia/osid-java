//
// AbstractActivity.java
//
//     Defines an Activity.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.learning.activity.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>Activity</code>.
 */

public abstract class AbstractActivity
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.learning.Activity {

    private org.osid.learning.Objective objective;
    private final java.util.Collection<org.osid.repository.Asset> assets = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.Course> courses = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.assessment.Assessment> assessments = new java.util.LinkedHashSet<>();

    private boolean assetBasedActivity      = false;
    private boolean courseBasedActivity     = false;
    private boolean assessmentBasedActivity = false;

    private final java.util.Collection<org.osid.learning.records.ActivityRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the related objective. 
     *
     *  @return the objective <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getObjectiveId() {
        return (this.objective.getId());
    }


    /**
     *  Gets the related objective. 
     *
     *  @return the related objective 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.learning.Objective getObjective()
        throws org.osid.OperationFailedException {

        return (this.objective);
    }


    /**
     *  Sets the objective.
     *
     *  @param objective an objective
     *  @throws org.osid.NullArgumentException
     *          <code>objective</code> is <code>null</code>
     */

    protected void setObjective(org.osid.learning.Objective objective) {
        nullarg(objective, "objective");
        this.objective = objective;
        return;
    }


    /**
     *  Tests if this is an asset based activity. 
     *
     *  @return <code> true </code> if this activity is based on assets, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean isAssetBasedActivity() {
        return (this.assetBasedActivity);
    }


    /**
     *  Gets the <code> Ids </code> of any assets associated with this 
     *  activity. 
     *
     *  @return list of asset <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> isAssetBasedActivity() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getAssetIds() {
        if (!isAssetBasedActivity()) {
            throw new org.osid.IllegalStateException("isAssetBasedActivity() is false");
        }

        try {
            org.osid.repository.AssetList assets = getAssets();
            return (new net.okapia.osid.jamocha.adapter.converter.repository.asset.AssetToIdList(assets));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets any assets associated with this activity. 
     *
     *  @return list of assets 
     *  @throws org.osid.IllegalStateException <code> isAssetBasedActivity() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.repository.AssetList getAssets()
        throws org.osid.OperationFailedException {

        if (!isAssetBasedActivity()) {
            throw new org.osid.IllegalStateException("isAssetBasedActivity() is false");
        }

        return (new net.okapia.osid.jamocha.repository.asset.ArrayAssetList(this.assets));
    }


    /**
     *  Adds an asset.
     *
     *  @param asset an asset
     *  @throws org.osid.NullArgumentException
     *          <code>asset</code> is <code>null</code>
     */

    protected void addAsset(org.osid.repository.Asset asset) {
        nullarg(asset, "asset");

        this.assets.add(asset);
        this.assetBasedActivity = true;

        return;
    }


    /**
     *  Sets all the assets.
     *
     *  @param assets a collection of assets
     *  @throws org.osid.NullArgumentException
     *          <code>assets</code> is <code>null</code>
     */

    protected void setAssets(java.util.Collection<org.osid.repository.Asset> assets) {
        nullarg(assets, "assets");

        this.assets.clear();
        this.assets.addAll(assets);
        this.assetBasedActivity = true;

        return;
    }


    /**
     *  Tests if this is a course based activity. 
     *
     *  @return <code> true </code> if this activity is based on courses, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean isCourseBasedActivity() {
        return (this.courseBasedActivity);
    }


    /**
     *  Gets the <code> Ids </code> of any courses associated with this 
     *  activity. 
     *
     *  @return list of course <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> isCourseBasedActivity() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getCourseIds() {
        if (!isCourseBasedActivity()) {
            throw new org.osid.IllegalStateException("isCourseBasedActivity() is false");
        }

        try {
            org.osid.course.CourseList courses = getCourses();
            return (new net.okapia.osid.jamocha.adapter.converter.course.course.CourseToIdList(courses));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets any courses associated with this activity. 
     *
     *  @return list of courses 
     *  @throws org.osid.IllegalStateException <code> isCourseBasedActivity() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.CourseList getCourses()
        throws org.osid.OperationFailedException {

        if (!isCourseBasedActivity()) {
            throw new org.osid.IllegalStateException("isCourseBasedActivity() is false");
        }

        return (new net.okapia.osid.jamocha.course.course.ArrayCourseList(this.courses));
    }


    /**
     *  Adds a course.
     *
     *  @param course a course
     *  @throws org.osid.NullArgumentException
     *          <code>course</code> is <code>null</code>
     */

    protected void addCourse(org.osid.course.Course course) {
        nullarg(course, "course");

        this.courses.add(course);
        this.courseBasedActivity = true;

        return;
    }


    /**
     *  Sets all the courses.
     *
     *  @param courses a collection of courses
     *  @throws org.osid.NullArgumentException
     *          <code>courses</code> is <code>null</code>
     */

    protected void setCourses(java.util.Collection<org.osid.course.Course> courses) {
        nullarg(courses, "courses");

        this.courses.clear();
        this.courses.addAll(courses);
        this.courseBasedActivity = true;

        return;
    }


    /**
     *  Tests if this is an assessment based activity. These
     *  assessments are for learning the objective and not for
     *  assessing prodiciency in the objective.
     *
     *  @return <code> true </code> if this activity is based on
     *          assessments, <code> false </code> otherwise
     */

    @OSID @Override
    public boolean isAssessmentBasedActivity() {
        return (this.assessmentBasedActivity);
    }


    /**
     *  Gets the <code> Ids </code> of any assessments associated with this 
     *  activity. 
     *
     *  @return list of assessment <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> 
     *          isAssessmentBasedActivity() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getAssessmentIds() {
        if (!isAssessmentBasedActivity()) {
            throw new org.osid.IllegalStateException("isAssessmentBasedActivity() is false");
        }

        try {
            org.osid.assessment.AssessmentList assessments = getAssessments();
            return (new net.okapia.osid.jamocha.adapter.converter.assessment.assessment.AssessmentToIdList(assessments));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets any assessments associated with this activity. 
     *
     *  @return list of assessments 
     *  @throws org.osid.IllegalStateException <code> 
     *          isAssessmentBasedActivity() </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentList getAssessments()
        throws org.osid.OperationFailedException {

        if (!isAssessmentBasedActivity()) {
            throw new org.osid.IllegalStateException("isAssessmentBasedActivity() is false");
        }

        return (new net.okapia.osid.jamocha.assessment.assessment.ArrayAssessmentList(this.assessments));
    }


    /**
     *  Adds an assessment.
     *
     *  @param assessment an assessment
     *  @throws org.osid.NullArgumentException <code>assessment</code>
     *          is <code>null</code>
     */

    protected void addAssessment(org.osid.assessment.Assessment assessment) {
        nullarg(assessment, "assessment");

        this.assessments.add(assessment);
        this.assessmentBasedActivity = true;

        return;
    }


    /**
     *  Sets all the assessments.
     *
     *  @param assessments a collection of assessments
     *  @throws org.osid.NullArgumentException
     *          <code>assessments</code> is <code>null</code>
     */

    protected void setAssessments(java.util.Collection<org.osid.assessment.Assessment> assessments) {
        nullarg(assessments, "assessments");

        this.assessments.clear();
        this.assessments.addAll(assessments);
        this.assessmentBasedActivity = true;

        return;
    }


    /**
     *  Tests if this activity supports the given record
     *  <code>Type</code>.
     *
     *  @param  activityRecordType an activity record type 
     *  @return <code>true</code> if the activityRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>activityRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type activityRecordType) {
        for (org.osid.learning.records.ActivityRecord record : this.records) {
            if (record.implementsRecordType(activityRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Activity</code> record <code>Type</code>.
     *
     *  @param  activityRecordType the activity record type 
     *  @return the activity record 
     *  @throws org.osid.NullArgumentException
     *          <code>activityRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(activityRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.learning.records.ActivityRecord getActivityRecord(org.osid.type.Type activityRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.learning.records.ActivityRecord record : this.records) {
            if (record.implementsRecordType(activityRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(activityRecordType + " is not supported");
    }


    /**
     *  Adds a record to this activity. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param activityRecord the activity record
     *  @param activityRecordType activity record type
     *  @throws org.osid.NullArgumentException
     *          <code>activityRecord</code> or
     *          <code>activityRecordTypeactivity</code> is
     *          <code>null</code>
     */
            
    protected void addActivityRecord(org.osid.learning.records.ActivityRecord activityRecord, 
                                     org.osid.type.Type activityRecordType) {

        nullarg(activityRecord, "activity record");
        addRecordType(activityRecordType);
        this.records.add(activityRecord);
        
        return;
    }
}
