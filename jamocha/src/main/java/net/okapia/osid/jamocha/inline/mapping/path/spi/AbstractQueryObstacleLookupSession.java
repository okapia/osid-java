//
// AbstractQueryObstacleLookupSession.java
//
//    An inline adapter that maps an ObstacleLookupSession to
//    an ObstacleQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.mapping.path.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps an ObstacleLookupSession to
 *  an ObstacleQuerySession.
 */

public abstract class AbstractQueryObstacleLookupSession
    extends net.okapia.osid.jamocha.mapping.path.spi.AbstractObstacleLookupSession
    implements org.osid.mapping.path.ObstacleLookupSession {

    private boolean activeonly    = false;
    private final org.osid.mapping.path.ObstacleQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryObstacleLookupSession.
     *
     *  @param querySession the underlying obstacle query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryObstacleLookupSession(org.osid.mapping.path.ObstacleQuerySession querySession) {
        nullarg(querySession, "obstacle query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Map</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Map Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getMapId() {
        return (this.session.getMapId());
    }


    /**
     *  Gets the <code>Map</code> associated with this 
     *  session.
     *
     *  @return the <code>Map</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.Map getMap()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getMap());
    }


    /**
     *  Tests if this user can perform <code>Obstacle</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupObstacles() {
        return (this.session.canSearchObstacles());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include obstacles in maps which are children
     *  of this map in the map hierarchy.
     */

    @OSID @Override
    public void useFederatedMapView() {
        this.session.useFederatedMapView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this map only.
     */

    @OSID @Override
    public void useIsolatedMapView() {
        this.session.useIsolatedMapView();
        return;
    }
    

    /**
     *  Only active obstacles are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveObstacleView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive obstacles are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusObstacleView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>Obstacle</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Obstacle</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Obstacle</code> and
     *  retained for compatibility.
     *
     *  In active mode, obstacles are returned that are currently
     *  active. In any status mode, active and inactive obstacles
     *  are returned.
     *
     *  @param  obstacleId <code>Id</code> of the
     *          <code>Obstacle</code>
     *  @return the obstacle
     *  @throws org.osid.NotFoundException <code>obstacleId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>obstacleId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.Obstacle getObstacle(org.osid.id.Id obstacleId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.mapping.path.ObstacleQuery query = getQuery();
        query.matchId(obstacleId, true);
        org.osid.mapping.path.ObstacleList obstacles = this.session.getObstaclesByQuery(query);
        if (obstacles.hasNext()) {
            return (obstacles.getNextObstacle());
        } 
        
        throw new org.osid.NotFoundException(obstacleId + " not found");
    }


    /**
     *  Gets an <code>ObstacleList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  obstacles specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Obstacles</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, obstacles are returned that are currently
     *  active. In any status mode, active and inactive obstacles
     *  are returned.
     *
     *  @param  obstacleIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Obstacle</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>obstacleIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleList getObstaclesByIds(org.osid.id.IdList obstacleIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.mapping.path.ObstacleQuery query = getQuery();

        try (org.osid.id.IdList ids = obstacleIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getObstaclesByQuery(query));
    }


    /**
     *  Gets an <code>ObstacleList</code> corresponding to the given
     *  obstacle genus <code>Type</code> which does not include
     *  obstacles of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  obstacles or an error results. Otherwise, the returned list
     *  may contain only those obstacles that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, obstacles are returned that are currently
     *  active. In any status mode, active and inactive obstacles
     *  are returned.
     *
     *  @param  obstacleGenusType an obstacle genus type 
     *  @return the returned <code>Obstacle</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>obstacleGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleList getObstaclesByGenusType(org.osid.type.Type obstacleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.mapping.path.ObstacleQuery query = getQuery();
        query.matchGenusType(obstacleGenusType, true);
        return (this.session.getObstaclesByQuery(query));
    }


    /**
     *  Gets an <code>ObstacleList</code> corresponding to the given
     *  obstacle genus <code>Type</code> and include any additional
     *  obstacles with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  obstacles or an error results. Otherwise, the returned list
     *  may contain only those obstacles that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, obstacles are returned that are currently
     *  active. In any status mode, active and inactive obstacles
     *  are returned.
     *
     *  @param  obstacleGenusType an obstacle genus type 
     *  @return the returned <code>Obstacle</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>obstacleGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleList getObstaclesByParentGenusType(org.osid.type.Type obstacleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.mapping.path.ObstacleQuery query = getQuery();
        query.matchParentGenusType(obstacleGenusType, true);
        return (this.session.getObstaclesByQuery(query));
    }


    /**
     *  Gets an <code>ObstacleList</code> containing the given
     *  obstacle record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  obstacles or an error results. Otherwise, the returned list
     *  may contain only those obstacles that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, obstacles are returned that are currently
     *  active. In any status mode, active and inactive obstacles
     *  are returned.
     *
     *  @param  obstacleRecordType an obstacle record type 
     *  @return the returned <code>Obstacle</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>obstacleRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleList getObstaclesByRecordType(org.osid.type.Type obstacleRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.mapping.path.ObstacleQuery query = getQuery();
        query.matchRecordType(obstacleRecordType, true);
        return (this.session.getObstaclesByQuery(query));
    }


    /**
     *  Gets an <code> ObstacleList </code> containing the given path.
     *  
     *  In plenary mode, the returned list contains all known
     *  obstacles or an error results. Otherwise, the returned list
     *  may contain only those obstacles that are accessible through
     *  this session.
     *  
     *  In active mode, obstacles are returned that are currently
     *  active. In any status mode, active and inactive obstacles are
     *  returned.
     *
     *  @param  pathId a path <code> Id </code> 
     *  @return the returned <code> Obstacle </code> list 
     *  @throws org.osid.NullArgumentException <code> pathId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleList getObstaclesForPath(org.osid.id.Id pathId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.mapping.path.ObstacleQuery query = getQuery();
        query.matchPathId(pathId, true);
        return (this.session.getObstaclesByQuery(query));
    }


    /**
     *  Gets an <code> ObstacleList </code> containing the given path
     *  between the given coordinates inclusive.
     *  
     *  In plenary mode, the returned list contains all known
     *  obstacles or an error results. Otherwise, the returned list
     *  may contain only those obstacles that are accessible through
     *  this session.
     *  
     *  In active mode, obstacles are returned that are currently
     *  active. In any status mode, active and inactive obstacles are
     *  returned.
     *
     *  @param  pathId a path <code> Id </code> 
     *  @param  coordinate starting coordinate 
     *  @param  distance a distance from coordinate 
     *  @return the returned <code> Obstacle </code> list 
     *  @throws org.osid.NullArgumentException <code> pathId, coordinate 
     *          </code> or <code> distance </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleList getObstaclesForPathAtCoordinate(org.osid.id.Id pathId, 
                                                                              org.osid.mapping.Coordinate coordinate, 
                                                                              org.osid.mapping.Distance distance)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.nil.mapping.path.obstacle.EmptyObstacleList());
    }


    /**
     *  Gets all <code>Obstacles</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  obstacles or an error results. Otherwise, the returned list
     *  may contain only those obstacles that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, obstacles are returned that are currently
     *  active. In any status mode, active and inactive obstacles
     *  are returned.
     *
     *  @return a list of <code>Obstacles</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleList getObstacles()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.mapping.path.ObstacleQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getObstaclesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.mapping.path.ObstacleQuery getQuery() {
        org.osid.mapping.path.ObstacleQuery query = this.session.getObstacleQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
