//
// AbstractQueryAssessmentPartLookupSession.java
//
//    An AssessmentPartQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.assessment.authoring.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An AssessmentPartQuerySession adapter.
 */

public abstract class AbstractAdapterAssessmentPartQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.assessment.authoring.AssessmentPartQuerySession {

    private final org.osid.assessment.authoring.AssessmentPartQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterAssessmentPartQuerySession.
     *
     *  @param session the underlying assessment part query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterAssessmentPartQuerySession(org.osid.assessment.authoring.AssessmentPartQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeBank</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeBank Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBankId() {
        return (this.session.getBankId());
    }


    /**
     *  Gets the {@codeBank</code> associated with this 
     *  session.
     *
     *  @return the {@codeBank</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.Bank getBank()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getBank());
    }


    /**
     *  Tests if this user can perform {@codeAssessmentPart</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchAssessmentParts() {
        return (this.session.canSearchAssessmentParts());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include assessment parts in banks which are children
     *  of this bank in the bank hierarchy.
     */

    @OSID @Override
    public void useFederatedBankView() {
        this.session.useFederatedBankView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this bank only.
     */
    
    @OSID @Override
    public void useIsolatedBankView() {
        this.session.useIsolatedBankView();
        return;
    }
    
      
    /**
     *  The returns from the lookup methods omit sequestered
     *  assessment parts.
     */

    @OSID @Override
    public void useSequesteredAssessmentPartView() {
        this.session.useSequesteredAssessmentPartView();
        return;
    }


    /**
     *  All assessment parts are returned including sequestered
     *  assessment parts.
     */

    @OSID @Override
    public void useUnsequesteredAssessmentPartView() {
        this.session.useUnsequesteredAssessmentPartView();
        return;
    }


    /**
     *  Gets an assessment part query. The returned query will not have an
     *  extension query.
     *
     *  @return the assessment part query 
     */
      
    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartQuery getAssessmentPartQuery() {
        return (this.session.getAssessmentPartQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  assessmentPartQuery the assessment part query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code assessmentPartQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code assessmentPartQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartList getAssessmentPartsByQuery(org.osid.assessment.authoring.AssessmentPartQuery assessmentPartQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getAssessmentPartsByQuery(assessmentPartQuery));
    }
}
