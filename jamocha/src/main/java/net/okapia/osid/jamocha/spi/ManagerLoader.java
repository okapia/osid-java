//
// ManagerLoader.java
//
//     Handles loader in the managers.
//
//
// Tom Coppeto
// Okapia
// 27 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.spi;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Handles loader in the managers.
 */

public class ManagerLoader {
    private final org.osid.OsidRuntimeManager runtime;
    

    /**
     *  Constructs a new <code>ManagerLoader</code>.
     *
     *  @param runtime
     *  @throws org.osid.NullArgumentException <code>runtime</code> is
     *          <code>null</code>
     */

    public ManagerLoader(org.osid.OsidRuntimeManager runtime) {
        nullarg(runtime, "runtime");
        this.runtime = runtime;
        return;
    }


    /**
     *  Instantiates an OsidManager.
     *
     *  @param osid the OSID to load
     *  @param  implClassName the name of the implementation 
     *  @return the OsidManager
     *  @throws org.osid.ConfigurationErrorException an error in configuring 
     *          the implementation
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.NullArgumentException <code> implClassName </code> or 
     *          <code> version </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete 
     *  @throws org.osid.UnsupportedException <code> implClassName </code> 
     *          does not support the requested OSID 
     */

    public org.osid.OsidManager getOsidManager(org.osid.OSID osid, String implClassName)
        throws org.osid.ConfigurationErrorException,
               org.osid.OperationFailedException {

        try {
            return (this.runtime.getManager(osid, implClassName,
                                            net.okapia.osid.OsidVersions.V3_0_0.getVersion()));
        } catch (org.osid.NotFoundException nfe) {
            throw new org.osid.ConfigurationErrorException(implClassName + " is not found", nfe);
        }
    }


    /**
     *  Instantiates an OsidProxyManager.
     *
     *  @param osid the OSID to load
     *  @param  implClassName the name of the implementation 
     *  @return the OsidManager
     *  @throws org.osid.ConfigurationErrorException an error in configuring 
     *          the implementation
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.NullArgumentException <code> implClassName </code> or 
     *          <code> version </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete 
     *  @throws org.osid.UnsupportedException <code> implClassName </code> 
     *          does not support the requested OSID 
     */

    public org.osid.OsidProxyManager getOsidProxyManager(org.osid.OSID osid, String implClassName)
        throws org.osid.ConfigurationErrorException,
               org.osid.OperationFailedException {

        try {
            return (this.runtime.getProxyManager(osid, implClassName, 
                                                 net.okapia.osid.OsidVersions.V3_0_0.getVersion()));
        } catch (org.osid.NotFoundException nfe) {
            throw new org.osid.ConfigurationErrorException(implClassName + " is not found", nfe);
        }
    }
}
