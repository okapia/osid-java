//
// AbstractInventoryShipmentManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inventory.shipment.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractInventoryShipmentManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.inventory.shipment.InventoryShipmentManager,
               org.osid.inventory.shipment.InventoryShipmentProxyManager {

    private final Types shipmentRecordTypes                = new TypeRefSet();
    private final Types shipmentSearchRecordTypes          = new TypeRefSet();

    private final Types entryRecordTypes                   = new TypeRefSet();
    private final Types entrySearchRecordTypes             = new TypeRefSet();


    /**
     *  Constructs a new
     *  <code>AbstractInventoryShipmentManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractInventoryShipmentManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any warehouse federation is exposed. Federation is exposed 
     *  when a specific warehouse may be identified, selected and used to 
     *  create a lookup or admin session. Federation is not exposed when a set 
     *  of catalogs appears as a single catalog. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if looking up shipments is supported. 
     *
     *  @return <code> true </code> if shipment lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsShipmentLookup() {
        return (false);
    }


    /**
     *  Tests if querying shipments is supported. 
     *
     *  @return <code> true </code> if shipment query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsShipmentQuery() {
        return (false);
    }


    /**
     *  Tests if searching shipments is supported. 
     *
     *  @return <code> true </code> if shipment search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsShipmentSearch() {
        return (false);
    }


    /**
     *  Tests if shipment <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if shipment administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsShipmentAdmin() {
        return (false);
    }


    /**
     *  Tests if a shipment <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if shipment notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsShipmentNotification() {
        return (false);
    }


    /**
     *  Tests if a warehouseing service is supported. 
     *
     *  @return <code> true </code> if warehouseing is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsShipmentWarehouse() {
        return (false);
    }


    /**
     *  Tests if a warehouseing service is supported. A warehouseing service 
     *  maps shipments to catalogs. 
     *
     *  @return <code> true </code> if warehouseing is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsShipmentWarehouseAssignment() {
        return (false);
    }


    /**
     *  Tests if a shipment smart warehouse session is available. 
     *
     *  @return <code> true </code> if a shipment smart warehouse session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsShipmentSmartWarehouse() {
        return (false);
    }


    /**
     *  Tests for the availability of a inventory shipment batch service. 
     *
     *  @return <code> true </code> if an inventory shipment batch service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInventoryShipmentBatchBatch() {
        return (false);
    }


    /**
     *  Gets the supported <code> Shipment </code> record types. 
     *
     *  @return a list containing the supported <code> Shipment </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getShipmentRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.shipmentRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Shipment </code> record type is supported. 
     *
     *  @param  shipmentRecordType a <code> Type </code> indicating a <code> 
     *          Shipment </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> shipmentRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsShipmentRecordType(org.osid.type.Type shipmentRecordType) {
        return (this.shipmentRecordTypes.contains(shipmentRecordType));
    }


    /**
     *  Adds support for a shipment record type.
     *
     *  @param shipmentRecordType a shipment record type
     *  @throws org.osid.NullArgumentException
     *  <code>shipmentRecordType</code> is <code>null</code>
     */

    protected void addShipmentRecordType(org.osid.type.Type shipmentRecordType) {
        this.shipmentRecordTypes.add(shipmentRecordType);
        return;
    }


    /**
     *  Removes support for a shipment record type.
     *
     *  @param shipmentRecordType a shipment record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>shipmentRecordType</code> is <code>null</code>
     */

    protected void removeShipmentRecordType(org.osid.type.Type shipmentRecordType) {
        this.shipmentRecordTypes.remove(shipmentRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Shipment </code> search record types. 
     *
     *  @return a list containing the supported <code> Shipment </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getShipmentSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.shipmentSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Shipment </code> search record type is 
     *  supported. 
     *
     *  @param  shipmentSearchRecordType a <code> Type </code> indicating a 
     *          <code> Shipment </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> shipmentSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsShipmentSearchRecordType(org.osid.type.Type shipmentSearchRecordType) {
        return (this.shipmentSearchRecordTypes.contains(shipmentSearchRecordType));
    }


    /**
     *  Adds support for a shipment search record type.
     *
     *  @param shipmentSearchRecordType a shipment search record type
     *  @throws org.osid.NullArgumentException
     *  <code>shipmentSearchRecordType</code> is <code>null</code>
     */

    protected void addShipmentSearchRecordType(org.osid.type.Type shipmentSearchRecordType) {
        this.shipmentSearchRecordTypes.add(shipmentSearchRecordType);
        return;
    }


    /**
     *  Removes support for a shipment search record type.
     *
     *  @param shipmentSearchRecordType a shipment search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>shipmentSearchRecordType</code> is <code>null</code>
     */

    protected void removeShipmentSearchRecordType(org.osid.type.Type shipmentSearchRecordType) {
        this.shipmentSearchRecordTypes.remove(shipmentSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Entry </code> record types. 
     *
     *  @return a list containing the supported <code> Entry </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getEntryRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.entryRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Entry </code> record type is supported. 
     *
     *  @param  entryRecordType a <code> Type </code> indicating an <code> 
     *          Entry </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> entryRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsEntryRecordType(org.osid.type.Type entryRecordType) {
        return (this.entryRecordTypes.contains(entryRecordType));
    }


    /**
     *  Adds support for an entry record type.
     *
     *  @param entryRecordType an entry record type
     *  @throws org.osid.NullArgumentException
     *  <code>entryRecordType</code> is <code>null</code>
     */

    protected void addEntryRecordType(org.osid.type.Type entryRecordType) {
        this.entryRecordTypes.add(entryRecordType);
        return;
    }


    /**
     *  Removes support for an entry record type.
     *
     *  @param entryRecordType an entry record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>entryRecordType</code> is <code>null</code>
     */

    protected void removeEntryRecordType(org.osid.type.Type entryRecordType) {
        this.entryRecordTypes.remove(entryRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Entry </code> search record types. 
     *
     *  @return a list containing the supported <code> Entry </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getEntrySearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.entrySearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Entry </code> search record type is 
     *  supported. 
     *
     *  @param  entrySearchRecordType a <code> Type </code> indicating an 
     *          <code> Entry </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> entrySearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsEntrySearchRecordType(org.osid.type.Type entrySearchRecordType) {
        return (this.entrySearchRecordTypes.contains(entrySearchRecordType));
    }


    /**
     *  Adds support for an entry search record type.
     *
     *  @param entrySearchRecordType an entry search record type
     *  @throws org.osid.NullArgumentException
     *  <code>entrySearchRecordType</code> is <code>null</code>
     */

    protected void addEntrySearchRecordType(org.osid.type.Type entrySearchRecordType) {
        this.entrySearchRecordTypes.add(entrySearchRecordType);
        return;
    }


    /**
     *  Removes support for an entry search record type.
     *
     *  @param entrySearchRecordType an entry search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>entrySearchRecordType</code> is <code>null</code>
     */

    protected void removeEntrySearchRecordType(org.osid.type.Type entrySearchRecordType) {
        this.entrySearchRecordTypes.remove(entrySearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the shipment 
     *  lookup service. 
     *
     *  @return a <code> ShipmentLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsShipmentLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentLookupSession getShipmentLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.shipment.InventoryShipmentManager.getShipmentLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the shipment 
     *  lookup service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> ShipmentLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsShipmentLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentLookupSession getShipmentLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.shipment.InventoryShipmentProxyManager.getShipmentLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the shipment 
     *  lookup service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the warehouse 
     *  @return a <code> ShipmentLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Warehouse </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsShipmentLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentLookupSession getShipmentLookupSessionForWarehouse(org.osid.id.Id warehouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.shipment.InventoryShipmentManager.getShipmentLookupSessionForWarehouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the shipment 
     *  lookup service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the warehouse 
     *  @param  proxy proxy 
     *  @return a <code> ShipmentLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Warehouse </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          warehouseId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsShipmentLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentLookupSession getShipmentLookupSessionForWarehouse(org.osid.id.Id warehouseId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.shipment.InventoryShipmentProxyManager.getShipmentLookupSessionForWarehouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the shipment query 
     *  service. 
     *
     *  @return a <code> ShipmentQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsShipmentQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentQuerySession getShipmentQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.shipment.InventoryShipmentManager.getShipmentQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the shipment query 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> ShipmentQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsShipmentQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentQuerySession getShipmentQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.shipment.InventoryShipmentProxyManager.getShipmentQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the shipment query 
     *  service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @return a <code> ShipmentQuerySession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsShipmentQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentQuerySession getShipmentQuerySessionForWarehouse(org.osid.id.Id warehouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.shipment.InventoryShipmentManager.getShipmentQuerySessionForWarehouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the shipment query 
     *  service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> ShipmentQuerySession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          warehouseId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsShipmentQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentQuerySession getShipmentQuerySessionForWarehouse(org.osid.id.Id warehouseId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.shipment.InventoryShipmentProxyManager.getShipmentQuerySessionForWarehouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the shipment 
     *  search service. 
     *
     *  @return a <code> ShipmentSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsShipmentSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentSearchSession getShipmentSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.shipment.InventoryShipmentManager.getShipmentSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the shipment 
     *  search service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> ShipmentSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsShipmentSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentSearchSession getShipmentSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.shipment.InventoryShipmentProxyManager.getShipmentSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the shipment 
     *  search service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @return a <code> ShipmentSearchSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsShipmentSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentSearchSession getShipmentSearchSessionForWarehouse(org.osid.id.Id warehouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.shipment.InventoryShipmentManager.getShipmentSearchSessionForWarehouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the shipment 
     *  search service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> ShipmentSearchSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          warehouseId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsShipmentSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentSearchSession getShipmentSearchSessionForWarehouse(org.osid.id.Id warehouseId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.shipment.InventoryShipmentProxyManager.getShipmentSearchSessionForWarehouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the shipment 
     *  administration service. 
     *
     *  @return a <code> ShipmentAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsShipmentAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentAdminSession getShipmentAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.shipment.InventoryShipmentManager.getShipmentAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the shipment 
     *  administration service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> ShipmentAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsShipmentAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentAdminSession getShipmentAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.shipment.InventoryShipmentProxyManager.getShipmentAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the shipment 
     *  administration service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @return a <code> ShipmentAdminSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsShipmentAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentAdminSession getShipmentAdminSessionForWarehouse(org.osid.id.Id warehouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.shipment.InventoryShipmentManager.getShipmentAdminSessionForWarehouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the shipment 
     *  administration service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> ShipmentAdminSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          warehouseId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsShipmentAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentAdminSession getShipmentAdminSessionForWarehouse(org.osid.id.Id warehouseId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.shipment.InventoryShipmentProxyManager.getShipmentAdminSessionForWarehouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the shipment 
     *  notification service. 
     *
     *  @param  shipmentReceiver the notification callback 
     *  @return a <code> ShipmentNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> shipmentReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsShipmentNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentNotificationSession getShipmentNotificationSession(org.osid.inventory.shipment.ShipmentReceiver shipmentReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.shipment.InventoryShipmentManager.getShipmentNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the shipment 
     *  notification service. 
     *
     *  @param  shipmentReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return a <code> ShipmentNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> shipmentReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsShipmentNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentNotificationSession getShipmentNotificationSession(org.osid.inventory.shipment.ShipmentReceiver shipmentReceiver, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.shipment.InventoryShipmentProxyManager.getShipmentNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the shipment 
     *  notification service for the given warehouse. 
     *
     *  @param  shipmentReceiver the notification callback 
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @return a <code> ShipmentNotificationSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> shipmentReceiver </code> 
     *          or <code> warehouseId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsShipmentNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentNotificationSession getShipmentNotificationSessionForWarehouse(org.osid.inventory.shipment.ShipmentReceiver shipmentReceiver, 
                                                                                                              org.osid.id.Id warehouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.shipment.InventoryShipmentManager.getShipmentNotificationSessionForWarehouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the shipment 
     *  notification service for the given warehouse. 
     *
     *  @param  shipmentReceiver the notification callback 
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> ShipmentNotificationSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> shipmentReceiver, 
     *          warehouseId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsShipmentNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentNotificationSession getShipmentNotificationSessionForWarehouse(org.osid.inventory.shipment.ShipmentReceiver shipmentReceiver, 
                                                                                                              org.osid.id.Id warehouseId, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.shipment.InventoryShipmentProxyManager.getShipmentNotificationSessionForWarehouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup shipment/catalog 
     *  mappings. 
     *
     *  @return a <code> ShipmentShipmentWarehouseSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsShipmentWarehouse() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentWarehouseSession getShipmentWarehouseSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.shipment.InventoryShipmentManager.getShipmentWarehouseSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup shipment/catalog 
     *  mappings. 
     *
     *  @param  proxy proxy 
     *  @return a <code> ShipmentWarehouseSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsShipmentWarehouse() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentWarehouseSession getShipmentWarehouseSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.shipment.InventoryShipmentProxyManager.getShipmentWarehouseSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  shipments to warehouses. 
     *
     *  @return a <code> ShipmentWarehouseAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsShipmentWarehouseAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentWarehouseAssignmentSession getShipmentWarehouseAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.shipment.InventoryShipmentManager.getShipmentWarehouseAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  shipments to warehouses. 
     *
     *  @param  proxy proxy 
     *  @return a <code> ShipmentWarehouseAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsShipmentWarehouseAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentWarehouseAssignmentSession getShipmentWarehouseAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.shipment.InventoryShipmentProxyManager.getShipmentWarehouseAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the shipment smart 
     *  warehouse service. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @return a <code> ShipmentSmartWarehouseSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsShipmentSmartWarehouse() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentSmartWarehouseSession getShipmentSmartWarehouseSession(org.osid.id.Id warehouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.shipment.InventoryShipmentManager.getShipmentSmartWarehouseSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the shipment smart 
     *  warehouse service. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> ShipmentSmartWarehouseSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsShipmentSmartWarehouse() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentSmartWarehouseSession getShipmentSmartWarehouseSession(org.osid.id.Id warehouseId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.shipment.InventoryShipmentProxyManager.getShipmentSmartWarehouseSession not implemented");
    }


    /**
     *  Gets the <code> InventoryShipmentBatchManager. </code> 
     *
     *  @return an <code> InventoryShipmentBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventoryShipmentBatch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inventory.shipment.batch.InventoryShipmentBatchManager getInventoryShipmentBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.shipment.InventoryShipmentManager.getInventoryShipmentBatchManager not implemented");
    }


    /**
     *  Gets the <code> InventoryShipmentBatchProxyManager. </code> 
     *
     *  @return an <code> InventoryShipmentBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventoryShipmentBatch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inventory.shipment.batch.InventoryShipmentBatchProxyManager getInventoryShipmentBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.shipment.InventoryShipmentProxyManager.getInventoryShipmentBatchProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.shipmentRecordTypes.clear();
        this.shipmentRecordTypes.clear();

        this.shipmentSearchRecordTypes.clear();
        this.shipmentSearchRecordTypes.clear();

        this.entryRecordTypes.clear();
        this.entryRecordTypes.clear();

        this.entrySearchRecordTypes.clear();
        this.entrySearchRecordTypes.clear();

        return;
    }
}
