//
// AbstractAssemblyActionQuery.java
//
//     An ActionQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.control.action.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An ActionQuery that stores terms.
 */

public abstract class AbstractAssemblyActionQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRuleQuery
    implements org.osid.control.ActionQuery,
               org.osid.control.ActionQueryInspector {

    private final java.util.Collection<org.osid.control.records.ActionQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.control.records.ActionQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyActionQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyActionQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the action group <code> Id </code> for this query. 
     *
     *  @param  actionGroupId an action group <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> actionGroupId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchActionGroupId(org.osid.id.Id actionGroupId, boolean match) {
        getAssembler().addIdTerm(getActionGroupIdColumn(), actionGroupId, match);
        return;
    }


    /**
     *  Clears the action group <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearActionGroupIdTerms() {
        getAssembler().clearTerms(getActionGroupIdColumn());
        return;
    }


    /**
     *  Gets the action group <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getActionGroupIdTerms() {
        return (getAssembler().getIdTerms(getActionGroupIdColumn()));
    }


    /**
     *  Gets the ActionGroupId column name.
     *
     * @return the column name
     */

    protected String getActionGroupIdColumn() {
        return ("action_group_id");
    }


    /**
     *  Tests if an <code> ActionGroupQuery </code> is available. 
     *
     *  @return <code> true </code> if an action group query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActionGroupQuery() {
        return (false);
    }


    /**
     *  Gets the query for an action group. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the action group query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionGroupQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ActionGroupQuery getActionGroupQuery() {
        throw new org.osid.UnimplementedException("supportsActionGroupQuery() is false");
    }


    /**
     *  Clears the action group query terms. 
     */

    @OSID @Override
    public void clearActionGroupTerms() {
        getAssembler().clearTerms(getActionGroupColumn());
        return;
    }


    /**
     *  Gets the action group query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.ActionGroupQueryInspector[] getActionGroupTerms() {
        return (new org.osid.control.ActionGroupQueryInspector[0]);
    }


    /**
     *  Gets the ActionGroup column name.
     *
     * @return the column name
     */

    protected String getActionGroupColumn() {
        return ("action_group");
    }


    /**
     *  Matches delays between the given range inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     *  @throws org.osid.NullArgumentException <code> start or end </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDelay(org.osid.calendaring.Duration start, 
                           org.osid.calendaring.Duration end, boolean match) {
        getAssembler().addDurationRangeTerm(getDelayColumn(), start, end, match);
        return;
    }


    /**
     *  Matches any delay. 
     *
     *  @param  match <code> true </code> to match actions with delays, <code> 
     *          false </code> to match actions with no delays 
     */

    @OSID @Override
    public void matchAnyDelay(boolean match) {
        getAssembler().addDurationRangeWildcardTerm(getDelayColumn(), match);
        return;
    }


    /**
     *  Clears the delay query terms. 
     */

    @OSID @Override
    public void clearDelayTerms() {
        getAssembler().clearTerms(getDelayColumn());
        return;
    }


    /**
     *  Gets the delay query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getDelayTerms() {
        return (getAssembler().getDurationRangeTerms(getDelayColumn()));
    }


    /**
     *  Gets the Delay column name.
     *
     * @return the column name
     */

    protected String getDelayColumn() {
        return ("delay");
    }


    /**
     *  Matches blocking actions. 
     *
     *  @param  match <code> true </code> to match blocking actions, <code> 
     *          false </code> to match unblocking actions 
     */

    @OSID @Override
    public void matchBlocking(boolean match) {
        getAssembler().addBooleanTerm(getBlockingColumn(), match);
        return;
    }


    /**
     *  Clears the blocking query terms. 
     */

    @OSID @Override
    public void clearBlockingTerms() {
        getAssembler().clearTerms(getBlockingColumn());
        return;
    }


    /**
     *  Gets the blocking query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getBlockingTerms() {
        return (getAssembler().getBooleanTerms(getBlockingColumn()));
    }


    /**
     *  Gets the Blocking column name.
     *
     * @return the column name
     */

    protected String getBlockingColumn() {
        return ("blocking");
    }


    /**
     *  Sets the executed action group <code> Id </code> for this query. 
     *
     *  @param  actionGroupId an action group <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> actionGroupId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchNextActionGroupId(org.osid.id.Id actionGroupId, 
                                       boolean match) {
        getAssembler().addIdTerm(getNextActionGroupIdColumn(), actionGroupId, match);
        return;
    }


    /**
     *  Clears the executed action group <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearNextActionGroupIdTerms() {
        getAssembler().clearTerms(getNextActionGroupIdColumn());
        return;
    }


    /**
     *  Gets the action group <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getNextActionGroupIdTerms() {
        return (getAssembler().getIdTerms(getNextActionGroupIdColumn()));
    }


    /**
     *  Gets the NextActionGroupId column name.
     *
     * @return the column name
     */

    protected String getNextActionGroupIdColumn() {
        return ("next_action_group_id");
    }


    /**
     *  Tests if an <code> ActionGroupQuery </code> is available. 
     *
     *  @return <code> true </code> if an action group query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsNextActionGroupQuery() {
        return (false);
    }


    /**
     *  Gets the query for the action group to execute.. Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the action group query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsNextActionGroupQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ActionGroupQuery getNextActionGroupQuery() {
        throw new org.osid.UnimplementedException("supportsNextActionGroupQuery() is false");
    }


    /**
     *  Matches actions with any executable action group. 
     *
     *  @param  match <code> true </code> to match actions with any action 
     *          group, <code> false </code> to match actions with no action 
     *          groups 
     */

    @OSID @Override
    public void matchAnyNextActionGroup(boolean match) {
        getAssembler().addIdWildcardTerm(getNextActionGroupColumn(), match);
        return;
    }


    /**
     *  Clears the action group query terms. 
     */

    @OSID @Override
    public void clearNextActionGroupTerms() {
        getAssembler().clearTerms(getNextActionGroupColumn());
        return;
    }


    /**
     *  Gets the action group query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.ActionGroupQueryInspector[] getNextActionGroupTerms() {
        return (new org.osid.control.ActionGroupQueryInspector[0]);
    }


    /**
     *  Gets the NextActionGroup column name.
     *
     * @return the column name
     */

    protected String getNextActionGroupColumn() {
        return ("next_action_group");
    }


    /**
     *  Sets the scene <code> Id </code> for this query. 
     *
     *  @param  sceneId a scene <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> sceneId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSceneId(org.osid.id.Id sceneId, boolean match) {
        getAssembler().addIdTerm(getSceneIdColumn(), sceneId, match);
        return;
    }


    /**
     *  Clears the scene <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearSceneIdTerms() {
        getAssembler().clearTerms(getSceneIdColumn());
        return;
    }


    /**
     *  Gets the scene <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSceneIdTerms() {
        return (getAssembler().getIdTerms(getSceneIdColumn()));
    }


    /**
     *  Gets the SceneId column name.
     *
     * @return the column name
     */

    protected String getSceneIdColumn() {
        return ("scene_id");
    }


    /**
     *  Tests if a <code> SceneQuery </code> is available. 
     *
     *  @return <code> true </code> if a scene query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSceneQuery() {
        return (false);
    }


    /**
     *  Gets the query for a scene. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the scene query 
     *  @throws org.osid.UnimplementedException <code> supportsSceneQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SceneQuery getSceneQuery() {
        throw new org.osid.UnimplementedException("supportsSceneQuery() is false");
    }


    /**
     *  Matches actions with any scene. 
     *
     *  @param  match <code> true </code> to match actions with any scene, 
     *          <code> false </code> to match actions with no scene 
     */

    @OSID @Override
    public void matchAnyScene(boolean match) {
        getAssembler().addIdWildcardTerm(getSceneColumn(), match);
        return;
    }


    /**
     *  Clears the scene query terms. 
     */

    @OSID @Override
    public void clearSceneTerms() {
        getAssembler().clearTerms(getSceneColumn());
        return;
    }


    /**
     *  Gets the scene query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.SceneQueryInspector[] getSceneTerms() {
        return (new org.osid.control.SceneQueryInspector[0]);
    }


    /**
     *  Gets the Scene column name.
     *
     * @return the column name
     */

    protected String getSceneColumn() {
        return ("scene");
    }


    /**
     *  Sets the setting <code> Id </code> for this query. 
     *
     *  @param  settingId a setting <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> settingId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSettingId(org.osid.id.Id settingId, boolean match) {
        getAssembler().addIdTerm(getSettingIdColumn(), settingId, match);
        return;
    }


    /**
     *  Clears the setting <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearSettingIdTerms() {
        getAssembler().clearTerms(getSettingIdColumn());
        return;
    }


    /**
     *  Gets the setting <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSettingIdTerms() {
        return (getAssembler().getIdTerms(getSettingIdColumn()));
    }


    /**
     *  Gets the SettingId column name.
     *
     * @return the column name
     */

    protected String getSettingIdColumn() {
        return ("setting_id");
    }


    /**
     *  Tests if a <code> SettingQuery </code> is available. 
     *
     *  @return <code> true </code> if a setting query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSettingQuery() {
        return (false);
    }


    /**
     *  Gets the query for a setting. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the setting query 
     *  @throws org.osid.UnimplementedException <code> supportsSettingQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SettingQuery getSettingQuery() {
        throw new org.osid.UnimplementedException("supportsSettingQuery() is false");
    }


    /**
     *  Matches actions with any setting. 
     *
     *  @param  match <code> true </code> to match actions with any setting, 
     *          <code> false </code> to match actions with no setting 
     */

    @OSID @Override
    public void matchAnySetting(boolean match) {
        getAssembler().addIdWildcardTerm(getSettingColumn(), match);
        return;
    }


    /**
     *  Clears the setting query terms. 
     */

    @OSID @Override
    public void clearSettingTerms() {
        getAssembler().clearTerms(getSettingColumn());
        return;
    }


    /**
     *  Gets the setting query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.SettingQueryInspector[] getSettingTerms() {
        return (new org.osid.control.SettingQueryInspector[0]);
    }


    /**
     *  Gets the Setting column name.
     *
     * @return the column name
     */

    protected String getSettingColumn() {
        return ("setting");
    }


    /**
     *  Tests if this action supports the given record
     *  <code>Type</code>.
     *
     *  @param  actionRecordType an action record type 
     *  @return <code>true</code> if the actionRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>actionRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type actionRecordType) {
        for (org.osid.control.records.ActionQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(actionRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  actionRecordType the action record type 
     *  @return the action query record 
     *  @throws org.osid.NullArgumentException
     *          <code>actionRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(actionRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.ActionQueryRecord getActionQueryRecord(org.osid.type.Type actionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.records.ActionQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(actionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(actionRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  actionRecordType the action record type 
     *  @return the action query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>actionRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(actionRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.ActionQueryInspectorRecord getActionQueryInspectorRecord(org.osid.type.Type actionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.records.ActionQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(actionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(actionRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this action. 
     *
     *  This method registers the query, and query inspector,
     *  Additional types may be registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param actionQueryRecord the action query record
     *  @param actionQueryInspectorRecord the action query inspector
     *         record
     *  @param actionRecordType action record type
     *  @throws org.osid.NullArgumentException
     *          <code>actionQueryRecord</code>,
     *          <code>actionQueryInspectorRecord</code>, or
     *          <code>actionRecordType</code> is
     *          <code>null</code>
     */
            
    protected void addActionRecords(org.osid.control.records.ActionQueryRecord actionQueryRecord, 
                                      org.osid.control.records.ActionQueryInspectorRecord actionQueryInspectorRecord, 
                                      org.osid.type.Type actionRecordType) {

        addRecordType(actionRecordType);

        nullarg(actionQueryRecord, "action query record");
        nullarg(actionQueryInspectorRecord, "action query inspector record");

        this.queryRecords.add(actionQueryRecord);
        this.queryInspectorRecords.add(actionQueryInspectorRecord);
        
        return;
    }
}
