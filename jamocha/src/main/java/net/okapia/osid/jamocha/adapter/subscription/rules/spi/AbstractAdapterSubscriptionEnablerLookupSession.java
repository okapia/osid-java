//
// AbstractAdapterSubscriptionEnablerLookupSession.java
//
//    A SubscriptionEnabler lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.subscription.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A SubscriptionEnabler lookup session adapter.
 */

public abstract class AbstractAdapterSubscriptionEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.subscription.rules.SubscriptionEnablerLookupSession {

    private final org.osid.subscription.rules.SubscriptionEnablerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterSubscriptionEnablerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterSubscriptionEnablerLookupSession(org.osid.subscription.rules.SubscriptionEnablerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Publisher/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Publisher Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getPublisherId() {
        return (this.session.getPublisherId());
    }


    /**
     *  Gets the {@code Publisher} associated with this session.
     *
     *  @return the {@code Publisher} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.Publisher getPublisher()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getPublisher());
    }


    /**
     *  Tests if this user can perform {@code SubscriptionEnabler} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupSubscriptionEnablers() {
        return (this.session.canLookupSubscriptionEnablers());
    }


    /**
     *  A complete view of the {@code SubscriptionEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeSubscriptionEnablerView() {
        this.session.useComparativeSubscriptionEnablerView();
        return;
    }


    /**
     *  A complete view of the {@code SubscriptionEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenarySubscriptionEnablerView() {
        this.session.usePlenarySubscriptionEnablerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include subscription enablers in publishers which are children
     *  of this publisher in the publisher hierarchy.
     */

    @OSID @Override
    public void useFederatedPublisherView() {
        this.session.useFederatedPublisherView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this publisher only.
     */

    @OSID @Override
    public void useIsolatedPublisherView() {
        this.session.useIsolatedPublisherView();
        return;
    }
    

    /**
     *  Only active subscription enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveSubscriptionEnablerView() {
        this.session.useActiveSubscriptionEnablerView();
        return;
    }


    /**
     *  Active and inactive subscription enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusSubscriptionEnablerView() {
        this.session.useAnyStatusSubscriptionEnablerView();
        return;
    }
    
     
    /**
     *  Gets the {@code SubscriptionEnabler} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code SubscriptionEnabler} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code SubscriptionEnabler} and
     *  retained for compatibility.
     *
     *  In active mode, subscription enablers are returned that are currently
     *  active. In any status mode, active and inactive subscription enablers
     *  are returned.
     *
     *  @param subscriptionEnablerId {@code Id} of the {@code SubscriptionEnabler}
     *  @return the subscription enabler
     *  @throws org.osid.NotFoundException {@code subscriptionEnablerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code subscriptionEnablerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.rules.SubscriptionEnabler getSubscriptionEnabler(org.osid.id.Id subscriptionEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSubscriptionEnabler(subscriptionEnablerId));
    }


    /**
     *  Gets a {@code SubscriptionEnablerList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  subscriptionEnablers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code SubscriptionEnablers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, subscription enablers are returned that are currently
     *  active. In any status mode, active and inactive subscription enablers
     *  are returned.
     *
     *  @param  subscriptionEnablerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code SubscriptionEnabler} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code subscriptionEnablerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.rules.SubscriptionEnablerList getSubscriptionEnablersByIds(org.osid.id.IdList subscriptionEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSubscriptionEnablersByIds(subscriptionEnablerIds));
    }


    /**
     *  Gets a {@code SubscriptionEnablerList} corresponding to the given
     *  subscription enabler genus {@code Type} which does not include
     *  subscription enablers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  subscription enablers or an error results. Otherwise, the returned list
     *  may contain only those subscription enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, subscription enablers are returned that are currently
     *  active. In any status mode, active and inactive subscription enablers
     *  are returned.
     *
     *  @param  subscriptionEnablerGenusType a subscriptionEnabler genus type 
     *  @return the returned {@code SubscriptionEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code subscriptionEnablerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.rules.SubscriptionEnablerList getSubscriptionEnablersByGenusType(org.osid.type.Type subscriptionEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSubscriptionEnablersByGenusType(subscriptionEnablerGenusType));
    }


    /**
     *  Gets a {@code SubscriptionEnablerList} corresponding to the given
     *  subscription enabler genus {@code Type} and include any additional
     *  subscription enablers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  subscription enablers or an error results. Otherwise, the returned list
     *  may contain only those subscription enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, subscription enablers are returned that are currently
     *  active. In any status mode, active and inactive subscription enablers
     *  are returned.
     *
     *  @param  subscriptionEnablerGenusType a subscriptionEnabler genus type 
     *  @return the returned {@code SubscriptionEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code subscriptionEnablerGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.rules.SubscriptionEnablerList getSubscriptionEnablersByParentGenusType(org.osid.type.Type subscriptionEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSubscriptionEnablersByParentGenusType(subscriptionEnablerGenusType));
    }


    /**
     *  Gets a {@code SubscriptionEnablerList} containing the given
     *  subscription enabler record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  subscription enablers or an error results. Otherwise, the returned list
     *  may contain only those subscription enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, subscription enablers are returned that are currently
     *  active. In any status mode, active and inactive subscription enablers
     *  are returned.
     *
     *  @param  subscriptionEnablerRecordType a subscriptionEnabler record type 
     *  @return the returned {@code SubscriptionEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code subscriptionEnablerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.rules.SubscriptionEnablerList getSubscriptionEnablersByRecordType(org.osid.type.Type subscriptionEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSubscriptionEnablersByRecordType(subscriptionEnablerRecordType));
    }


    /**
     *  Gets a {@code SubscriptionEnablerList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  subscription enablers or an error results. Otherwise, the returned list
     *  may contain only those subscription enablers that are accessible
     *  through this session.
     *  
     *  In active mode, subscription enablers are returned that are currently
     *  active. In any status mode, active and inactive subscription enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code SubscriptionEnabler} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.subscription.rules.SubscriptionEnablerList getSubscriptionEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSubscriptionEnablersOnDate(from, to));
    }
        

    /**
     *  Gets a {@code SubscriptionEnablerList } which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  subscription enablers or an error results. Otherwise, the returned list
     *  may contain only those subscription enablers that are accessible
     *  through this session.
     *
     *  In active mode, subscription enablers are returned that are currently
     *  active. In any status mode, active and inactive subscription enablers
     *  are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned {@code SubscriptionEnabler} list
     *  @throws org.osid.InvalidArgumentException {@code from} is
     *          greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code agent},
     *          {@code from}, or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.subscription.rules.SubscriptionEnablerList getSubscriptionEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (this.session.getSubscriptionEnablersOnDateWithAgent(agentId, from, to));
    }


    /**
     *  Gets all {@code SubscriptionEnablers}. 
     *
     *  In plenary mode, the returned list contains all known
     *  subscription enablers or an error results. Otherwise, the returned list
     *  may contain only those subscription enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, subscription enablers are returned that are currently
     *  active. In any status mode, active and inactive subscription enablers
     *  are returned.
     *
     *  @return a list of {@code SubscriptionEnablers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.rules.SubscriptionEnablerList getSubscriptionEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSubscriptionEnablers());
    }
}
