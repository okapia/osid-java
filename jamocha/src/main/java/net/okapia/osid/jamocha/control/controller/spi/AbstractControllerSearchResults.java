//
// AbstractControllerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.controller.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractControllerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.control.ControllerSearchResults {

    private org.osid.control.ControllerList controllers;
    private final org.osid.control.ControllerQueryInspector inspector;
    private final java.util.Collection<org.osid.control.records.ControllerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractControllerSearchResults.
     *
     *  @param controllers the result set
     *  @param controllerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>controllers</code>
     *          or <code>controllerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractControllerSearchResults(org.osid.control.ControllerList controllers,
                                            org.osid.control.ControllerQueryInspector controllerQueryInspector) {
        nullarg(controllers, "controllers");
        nullarg(controllerQueryInspector, "controller query inspectpr");

        this.controllers = controllers;
        this.inspector = controllerQueryInspector;

        return;
    }


    /**
     *  Gets the controller list resulting from a search.
     *
     *  @return a controller list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.control.ControllerList getControllers() {
        if (this.controllers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.control.ControllerList controllers = this.controllers;
        this.controllers = null;
	return (controllers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.control.ControllerQueryInspector getControllerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  controller search record <code> Type. </code> This method must
     *  be used to retrieve a controller implementing the requested
     *  record.
     *
     *  @param controllerSearchRecordType a controller search 
     *         record type 
     *  @return the controller search
     *  @throws org.osid.NullArgumentException
     *          <code>controllerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(controllerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.ControllerSearchResultsRecord getControllerSearchResultsRecord(org.osid.type.Type controllerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.control.records.ControllerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(controllerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(controllerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record controller search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addControllerRecord(org.osid.control.records.ControllerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "controller record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
