//
// AbstractAdapterJournalEntryLookupSession.java
//
//    A JournalEntry lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.journaling.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A JournalEntry lookup session adapter.
 */

public abstract class AbstractAdapterJournalEntryLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.journaling.JournalEntryLookupSession {

    private final org.osid.journaling.JournalEntryLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterJournalEntryLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterJournalEntryLookupSession(org.osid.journaling.JournalEntryLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Journal/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Journal Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getJournalId() {
        return (this.session.getJournalId());
    }


    /**
     *  Gets the {@code Journal} associated with this session.
     *
     *  @return the {@code Journal} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.Journal getJournal()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getJournal());
    }


    /**
     *  Tests if this user can perform {@code JournalEntry} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canReadJournal() {
        return (this.session.canReadJournal());
    }


    /**
     *  A complete view of the {@code JournalEntry} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeJournalEntryView() {
        this.session.useComparativeJournalEntryView();
        return;
    }


    /**
     *  A complete view of the {@code JournalEntry} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryJournalEntryView() {
        this.session.usePlenaryJournalEntryView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include journal entries in journals which are children
     *  of this journal in the journal hierarchy.
     */

    @OSID @Override
    public void useFederatedJournalView() {
        this.session.useFederatedJournalView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this journal only.
     */

    @OSID @Override
    public void useIsolatedJournalView() {
        this.session.useIsolatedJournalView();
        return;
    }
    
     
    /**
     *  Gets the {@code JournalEntry} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code JournalEntry} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code JournalEntry} and
     *  retained for compatibility.
     *
     *  @param journalEntryId {@code Id} of the {@code JournalEntry}
     *  @return the journal entry
     *  @throws org.osid.NotFoundException {@code journalEntryId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code journalEntryId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntry getJournalEntry(org.osid.id.Id journalEntryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJournalEntry(journalEntryId));
    }


    /**
     *  Gets a {@code JournalEntryList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  journalEntries specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code JournalEntries} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  journalEntryIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code JournalEntry} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code journalEntryIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesByIds(org.osid.id.IdList journalEntryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJournalEntriesByIds(journalEntryIds));
    }


    /**
     *  Gets a {@code JournalEntryList} corresponding to the given
     *  journal entry genus {@code Type} which does not include
     *  journal entries of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  journal entries or an error results. Otherwise, the returned list
     *  may contain only those journal entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  journalEntryGenusType a journalEntry genus type 
     *  @return the returned {@code JournalEntry} list
     *  @throws org.osid.NullArgumentException
     *          {@code journalEntryGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesByGenusType(org.osid.type.Type journalEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJournalEntriesByGenusType(journalEntryGenusType));
    }


    /**
     *  Gets a {@code JournalEntryList} corresponding to the given
     *  journal entry genus {@code Type} and include any additional
     *  journal entries with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  journal entries or an error results. Otherwise, the returned list
     *  may contain only those journal entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  journalEntryGenusType a journalEntry genus type 
     *  @return the returned {@code JournalEntry} list
     *  @throws org.osid.NullArgumentException
     *          {@code journalEntryGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesByParentGenusType(org.osid.type.Type journalEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJournalEntriesByParentGenusType(journalEntryGenusType));
    }


    /**
     *  Gets a {@code JournalEntryList} containing the given
     *  journal entry record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  journal entries or an error results. Otherwise, the returned list
     *  may contain only those journal entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  journalEntryRecordType a journalEntry record type 
     *  @return the returned {@code JournalEntry} list
     *  @throws org.osid.NullArgumentException
     *          {@code journalEntryRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesByRecordType(org.osid.type.Type journalEntryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJournalEntriesByRecordType(journalEntryRecordType));
    }


    /**
     *  Gets a list of journal entries corresponding to a branch
     *  {@code Id}. In plenary mode, the returned list contains all
     *  known journal entries or an error results. Otherwise, the
     *  returned list may contain only those journal entries that are
     *  accessible through this session.
     *
     *  @param  branchId the {@code Id} of the branch 
     *  @return the returned {@code JournalEntryList} 
     *  @throws org.osid.NullArgumentException {@code branchId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesForBranch(org.osid.id.Id branchId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJournalEntriesForBranch(branchId));
    }


    /**
     *  Gets the journal entry corresponding to a resource {@code Id}
     *  and date. The entries returned have a date equal to or more
     *  recent than the requested date. In plenary mode, the returned
     *  list contains all known journal entries or an error
     *  results. Otherwise, the returned list may contain only those
     *  journal entries that are accessible through this session.
     *
     *  @param  branchId the {@code Id} of the branch 
     *  @param  date from date 
     *  @return the returned {@code JournalEntryList} 
     *  @throws org.osid.NullArgumentException {@code branchId} or 
     *          {@code date} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesByDateForBranch(org.osid.id.Id branchId, 
                                                                                 org.osid.calendaring.DateTime date)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJournalEntriesByDateForBranch(branchId, date));
    }


    /**
     *  Gets a list of journal entries corresponding to a branch
     *  {@code Id} and date range. Entries are returned with dates
     *  that fall between the requested dates inclusive. In plenary
     *  mode, the returned list contains all known journal entries or
     *  an error results.  Otherwise, the returned list may contain
     *  only those journal entries that are accessible through this
     *  session.
     *
     *  @param  branchId the {@code Id} of the branch 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned {@code JournalEntryList} 
     *  @throws org.osid.InvalidArgumentException {@code to} is less 
     *          than {@code from} 
     *  @throws org.osid.NullArgumentException {@code branchId, from} 
     *          or {@code to} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesByDateRangeForBranch(org.osid.id.Id branchId, 
                                                                                      org.osid.calendaring.DateTime from, 
                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJournalEntriesByDateRangeForBranch(branchId, from, to));
    }


    /**
     *  Gets a list of journal entries corresponding to a source
     *  {@code Id}. A source {@code Id} of any version may be
     *  requested. In plenary mode, the returned list contains all
     *  known journal entries or an error results. Otherwise, the
     *  returned list may contain only those journal entries that are
     *  accessible through this session.
     *
     *  @param  sourceId the {@code Id} of the source 
     *  @return the returned {@code JournalEntryList} 
     *  @throws org.osid.NullArgumentException {@code sourceId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesForSource(org.osid.id.Id sourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJournalEntriesForSource(sourceId));
    }

    
    /**
     *  Gets the journal entry corresponding to a source {@code Id}
     *  and date. The entry returned has a date equal to or more
     *  recent than the requested date. The {@code sourceId} may
     *  correspond to any version. In plenary mode, the returned list
     *  contains all known journal entries or an error
     *  results. Otherwise, the returned list may contain only those
     *  journal entries that are accessible through this session.
     *
     *  @param  sourceId a source {@code Id} 
     *  @param  date from date 
     *  @return the returned {@code JournalEntryList} 
     *  @throws org.osid.NullArgumentException {@code sourceId} or 
     *          {@code to} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesByDateForSource(org.osid.id.Id sourceId, 
                                                                                 org.osid.calendaring.DateTime date)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJournalEntriesByDateForSource(sourceId, date));
    }


    /**
     *  Gets a list of journal entries corresponding to a source
     *  {@code Id} and date range. Entries are returned with dates
     *  that fall between the requested dates inclusive. The {@code
     *  sourceId} may correspond to any version. In plenary mode, the
     *  returned list contains all known journal entries or an error
     *  results. Otherwise, the returned list may contain only those
     *  journal entries that are accessible through this session.
     *
     *  @param  sourceId a source {@code Id} 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned {@code JournalEntryList} 
     *  @throws org.osid.InvalidArgumentException {@code to} is less
     *          than {@code from}
     *  @throws org.osid.NullArgumentException {@code sourceId, from} 
     *          or {@code to} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesByDateRangeForSource(org.osid.id.Id sourceId, 
                                                                                      org.osid.calendaring.DateTime from, 
                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJournalEntriesByDateRangeForSource(sourceId, from, to));
    }


    /**
     *  Gets a list of journal entries corresponding to a branch and
     *  source {@code Id}. A source {@code Id} of any version may be
     *  requested. {@code} In plenary mode, the returned list contains
     *  all known journal entries or an error results. Otherwise, the
     *  returned list may contain only those journal entries that are
     *  accessible through this session.
     *
     *  @param  branchId the {@code Id} of the branch 
     *  @param  sourceId the {@code Id} of the source 
     *  @return the returned {@code JournalEntryList} 
     *  @throws org.osid.NullArgumentException {@code branchId} or 
     *          {@code sourceId} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesForBranchAndSource(org.osid.id.Id branchId, 
                                                                                    org.osid.id.Id sourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJournalEntriesForBranchAndSource(branchId, sourceId));
    }


    /**
     *  Gets the journal entry corresponding to a branch and source
     *  {@code Id} and date. The entry returned has a date equal to or
     *  more recent than the requested date. The {@code sourceId} may
     *  correspond to any version. In plenary mode, the returned list
     *  contains all known journal entries or an error
     *  results. Otherwise, the returned list may contain only those
     *  journal entries that are accessible through this session.
     *
     *  @param  branchId a branch {@code Id} 
     *  @param  sourceId the {@code Id} of the source 
     *  @param  date from date 
     *  @return the returned {@code JournalEntryList} 
     *  @throws org.osid.NullArgumentException {@code branchId,
     *         sourceId} or {@code date} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesByDateForBranchAndSource(org.osid.id.Id branchId, 
                                                                                          org.osid.id.Id sourceId, 
                                                                                          org.osid.calendaring.DateTime date)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJournalEntriesByDateForBranchAndSource(branchId, sourceId, date));
    }


    /**
     *  Gets a list of journal entries corresponding to a branch and
     *  source {@code Id} and date range. Entries are returned with
     *  dates that fall between the requested dates inclusive. The
     *  {@code sourceId} may correspond to any version In plenary
     *  mode, the returned list contains all known journal entries or
     *  an error results.  Otherwise, the returned list may contain
     *  only those journal entries that are accessible through this
     *  session.
     *
     *  @param  branchId a branch {@code Id} 
     *  @param  sourceId the {@code Id} of the source 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned {@code JournalEntryList} 
     *  @throws org.osid.InvalidArgumentException {@code to} is less 
     *          than {@code from} 
     *  @throws org.osid.NullArgumentException {@code branchId,
     *         sourceId, from}, or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesByDateRangeForBranchAndSource(org.osid.id.Id branchId, 
                                                                                               org.osid.id.Id sourceId, 
                                                                                               org.osid.calendaring.DateTime from, 
                                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJournalEntriesByDateRangeForBranchAndSource(branchId, sourceId, from, to));
    }


    /**
     *  Gets a list of journal entries corresponding to a resource
     *  {@code Id}. In plenary mode, the returned list contains all
     *  known journal entries or an error results. Otherwise, the
     *  returned list may contain only those journal entries that are
     *  accessible through this session.
     *
     *  @param  resourceId the {@code Id} of the resource 
     *  @return the returned {@code JournalEntryList} 
     *  @throws org.osid.NullArgumentException {@code resourceId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesForResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJournalEntriesForResource(resourceId));
    }

    
    /**
     *  Gets the journal entry corresponding to a resource {@code Id}
     *  and date. The entry returned has a date equal to or more
     *  recent than the requested date. In plenary mode, the returned
     *  list contains all known journal entries or an error
     *  results. Otherwise, the returned list may contain only those
     *  journal entries that are accessible through this session.
     *
     *  @param  resourceId the {@code Id} of the resource 
     *  @param  date from date 
     *  @return the returned {@code JournalEntryList} 
     *  @throws org.osid.NullArgumentException {@code resourceId} or 
     *          {@code date} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesByDateForResource(org.osid.id.Id resourceId, 
                                                                                   org.osid.calendaring.DateTime date)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJournalEntriesByDateForResource(resourceId, date));
    }


    /**
     *  Gets a list of journal entries corresponding to a resource
     *  {@code Id} and date range. Entries are returned with dates
     *  that fall between the requested dates inclusive. In plenary
     *  mode, the returned list contains all known journal entries or
     *  an error results.  Otherwise, the returned list may contain
     *  only those journal entries that are accessible through this
     *  session.
     *
     *  @param  resourceId the {@code Id} of the resource 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned {@code JournalEntryList} 
     *  @throws org.osid.InvalidArgumentException {@code to} is less 
     *          than {@code from} 
     *  @throws org.osid.NullArgumentException {@code resourceId,
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesByDateRangeForResource(org.osid.id.Id resourceId, 
                                                                                        org.osid.calendaring.DateTime from, 
                                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJournalEntriesByDateRangeForResource(resourceId, from, to));
    }

    
    /**
     *  Gets all {@code JournalEntries}. 
     *
     *  In plenary mode, the returned list contains all known journal
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those journal entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code JournalEntries} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJournalEntries());
    }
}
