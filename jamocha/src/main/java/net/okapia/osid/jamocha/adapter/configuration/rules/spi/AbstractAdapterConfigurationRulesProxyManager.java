//
// AbstractConfigurationRulesProxyManager.java
//
//     An adapter for a ConfigurationRulesProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.configuration.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a ConfigurationRulesProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterConfigurationRulesProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.configuration.rules.ConfigurationRulesProxyManager>
    implements org.osid.configuration.rules.ConfigurationRulesProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterConfigurationRulesProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterConfigurationRulesProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterConfigurationRulesProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterConfigurationRulesProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any broker federation is exposed. Federation is exposed when 
     *  a specific broker may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  brokers appears as a single broker. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if looking up value enablers is supported. 
     *
     *  @return <code> true </code> if value enabler lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsValueEnablerLookup() {
        return (getAdapteeManager().supportsValueEnablerLookup());
    }


    /**
     *  Tests if querying value enablers is supported. 
     *
     *  @return <code> true </code> if value enabler query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsValueEnablerQuery() {
        return (getAdapteeManager().supportsValueEnablerQuery());
    }


    /**
     *  Tests if searching value enablers is supported. 
     *
     *  @return <code> true </code> if value enabler search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsValueEnablerSearch() {
        return (getAdapteeManager().supportsValueEnablerSearch());
    }


    /**
     *  Tests if a value enabler administrative service is supported. 
     *
     *  @return <code> true </code> if value enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsValueEnablerAdmin() {
        return (getAdapteeManager().supportsValueEnablerAdmin());
    }


    /**
     *  Tests if a value enabler notification service is supported. 
     *
     *  @return <code> true </code> if value enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsValueEnablerNotification() {
        return (getAdapteeManager().supportsValueEnablerNotification());
    }


    /**
     *  Tests if a value enabler configuration lookup service is supported. 
     *
     *  @return <code> true </code> if a value enabler configuration lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsValueEnablerConfiguration() {
        return (getAdapteeManager().supportsValueEnablerConfiguration());
    }


    /**
     *  Tests if a value enabler configuration service is supported. 
     *
     *  @return <code> true </code> if value enabler configuration assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsValueEnablerConfigurationAssignment() {
        return (getAdapteeManager().supportsValueEnablerConfigurationAssignment());
    }


    /**
     *  Tests if a value enabler configuration lookup service is supported. 
     *
     *  @return <code> true </code> if a value enabler configuration service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsValueEnablerSmartConfiguration() {
        return (getAdapteeManager().supportsValueEnablerSmartConfiguration());
    }


    /**
     *  Tests if a value enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if a value enabler rule lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsValueEnablerRuleLookup() {
        return (getAdapteeManager().supportsValueEnablerRuleLookup());
    }


    /**
     *  Tests if a value enabler rule application service is supported. 
     *
     *  @return <code> true </code> if value enabler rule application service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsValueEnablerRuleApplication() {
        return (getAdapteeManager().supportsValueEnablerRuleApplication());
    }


    /**
     *  Tests if looking up parameter processor is supported. 
     *
     *  @return <code> true </code> if parameter processor lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterProcessorLookup() {
        return (getAdapteeManager().supportsParameterProcessorLookup());
    }


    /**
     *  Tests if querying parameter processor is supported. 
     *
     *  @return <code> true </code> if parameter processor query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterProcessorQuery() {
        return (getAdapteeManager().supportsParameterProcessorQuery());
    }


    /**
     *  Tests if searching parameter processor is supported. 
     *
     *  @return <code> true </code> if parameter processor search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterProcessorSearch() {
        return (getAdapteeManager().supportsParameterProcessorSearch());
    }


    /**
     *  Tests if a parameter processor administrative service is supported. 
     *
     *  @return <code> true </code> if parameter processor administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterProcessorAdmin() {
        return (getAdapteeManager().supportsParameterProcessorAdmin());
    }


    /**
     *  Tests if a parameter processor notification service is supported. 
     *
     *  @return <code> true </code> if parameter processor notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterProcessorNotification() {
        return (getAdapteeManager().supportsParameterProcessorNotification());
    }


    /**
     *  Tests if a parameter processor configuration lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a parameter processor configuration 
     *          lookup service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterProcessorConfiguration() {
        return (getAdapteeManager().supportsParameterProcessorConfiguration());
    }


    /**
     *  Tests if a parameter processor configuration service is supported. 
     *
     *  @return <code> true </code> if parameter processor configuration 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsParameterProcessorConfigurationAssignment() {
        return (getAdapteeManager().supportsParameterProcessorConfigurationAssignment());
    }


    /**
     *  Tests if a parameter processor configuration lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a parameter processor configuration 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterProcessorSmartConfiguration() {
        return (getAdapteeManager().supportsParameterProcessorSmartConfiguration());
    }


    /**
     *  Tests if a parameter processor rule lookup service is supported. 
     *
     *  @return <code> true </code> if a parameter processor rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterProcessorRuleLookup() {
        return (getAdapteeManager().supportsParameterProcessorRuleLookup());
    }


    /**
     *  Tests if a parameter processor rule application service is supported. 
     *
     *  @return <code> true </code> if parameter processor rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterProcessorRuleApplication() {
        return (getAdapteeManager().supportsParameterProcessorRuleApplication());
    }


    /**
     *  Tests if looking up parameter processor enablers is supported. 
     *
     *  @return <code> true </code> if parameter processor enabler lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterProcessorEnablerLookup() {
        return (getAdapteeManager().supportsParameterProcessorEnablerLookup());
    }


    /**
     *  Tests if querying parameter processor enablers is supported. 
     *
     *  @return <code> true </code> if parameter processor enabler query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterProcessorEnablerQuery() {
        return (getAdapteeManager().supportsParameterProcessorEnablerQuery());
    }


    /**
     *  Tests if searching parameter processor enablers is supported. 
     *
     *  @return <code> true </code> if parameter processor enabler search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterProcessorEnablerSearch() {
        return (getAdapteeManager().supportsParameterProcessorEnablerSearch());
    }


    /**
     *  Tests if a parameter processor enabler administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if parameter processor enabler 
     *          administration is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterProcessorEnablerAdmin() {
        return (getAdapteeManager().supportsParameterProcessorEnablerAdmin());
    }


    /**
     *  Tests if a parameter processor enabler notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if parameter processor enabler 
     *          notification is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterProcessorEnablerNotification() {
        return (getAdapteeManager().supportsParameterProcessorEnablerNotification());
    }


    /**
     *  Tests if a parameter processor enabler configuration lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a parameter processor enabler 
     *          configuration lookup service is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterProcessorEnablerConfiguration() {
        return (getAdapteeManager().supportsParameterProcessorEnablerConfiguration());
    }


    /**
     *  Tests if a parameter processor enabler configuration service is 
     *  supported. 
     *
     *  @return <code> true </code> if parameter processor enabler 
     *          configuration assignment service is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterProcessorEnablerConfigurationAssignment() {
        return (getAdapteeManager().supportsParameterProcessorEnablerConfigurationAssignment());
    }


    /**
     *  Tests if a parameter processor enabler configuration lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a parameter processor enabler 
     *          configuration service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsParameterProcessorEnablerSmartConfiguration() {
        return (getAdapteeManager().supportsParameterProcessorEnablerSmartConfiguration());
    }


    /**
     *  Tests if a parameter processor enabler rule lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a processor enabler rule lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterProcessorEnablerRuleLookup() {
        return (getAdapteeManager().supportsParameterProcessorEnablerRuleLookup());
    }


    /**
     *  Tests if a parameter processor enabler rule application service is 
     *  supported. 
     *
     *  @return <code> true </code> if parameter processor enabler rule 
     *          application service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsParameterProcessorEnablerRuleApplication() {
        return (getAdapteeManager().supportsParameterProcessorEnablerRuleApplication());
    }


    /**
     *  Gets the supported <code> ValueEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> ValueEnabler </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getValueEnablerRecordTypes() {
        return (getAdapteeManager().getValueEnablerRecordTypes());
    }


    /**
     *  Tests if the given <code> ValueEnabler </code> record type is 
     *  supported. 
     *
     *  @param  valueEnablerRecordType a <code> Type </code> indicating a 
     *          <code> ValueEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> valueEnablerRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsValueEnablerRecordType(org.osid.type.Type valueEnablerRecordType) {
        return (getAdapteeManager().supportsValueEnablerRecordType(valueEnablerRecordType));
    }


    /**
     *  Gets the supported <code> ValueEnabler </code> search record types. 
     *
     *  @return a list containing the supported <code> ValueEnabler </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getValueEnablerSearchRecordTypes() {
        return (getAdapteeManager().getValueEnablerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> ValueEnabler </code> search record type is 
     *  supported. 
     *
     *  @param  valueEnablerSearchRecordType a <code> Type </code> indicating 
     *          a <code> ValueEnabler </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          valueEnablerSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsValueEnablerSearchRecordType(org.osid.type.Type valueEnablerSearchRecordType) {
        return (getAdapteeManager().supportsValueEnablerSearchRecordType(valueEnablerSearchRecordType));
    }


    /**
     *  Gets the supported <code> ParameterProcessor </code> record types. 
     *
     *  @return a list containing the supported <code> ParameterProcessor 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getParameterProcessorRecordTypes() {
        return (getAdapteeManager().getParameterProcessorRecordTypes());
    }


    /**
     *  Tests if the given <code> ParameterProcessor </code> record type is 
     *  supported. 
     *
     *  @param  parameterProcessorRecordType a <code> Type </code> indicating 
     *          a <code> ParameterProcessor </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          parameterProcessorRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsParameterProcessorRecordType(org.osid.type.Type parameterProcessorRecordType) {
        return (getAdapteeManager().supportsParameterProcessorRecordType(parameterProcessorRecordType));
    }


    /**
     *  Gets the supported <code> ParameterProcessor </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> ParameterProcessor 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getParameterProcessorSearchRecordTypes() {
        return (getAdapteeManager().getParameterProcessorSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> ParameterProcessor </code> search record 
     *  type is supported. 
     *
     *  @param  parameterProcessorSearchRecordType a <code> Type </code> 
     *          indicating a <code> ParameterProcessor </code> search record 
     *          type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          parameterProcessorSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsParameterProcessorSearchRecordType(org.osid.type.Type parameterProcessorSearchRecordType) {
        return (getAdapteeManager().supportsParameterProcessorSearchRecordType(parameterProcessorSearchRecordType));
    }


    /**
     *  Gets the supported <code> ParameterProcessorEnabler </code> record 
     *  types. 
     *
     *  @return a list containing the supported <code> 
     *          ParameterProcessorEnabler </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getParameterProcessorEnablerRecordTypes() {
        return (getAdapteeManager().getParameterProcessorEnablerRecordTypes());
    }


    /**
     *  Tests if the given <code> ParameterProcessorEnabler </code> record 
     *  type is supported. 
     *
     *  @param  parameterProcessorEnablerRecordType a <code> Type </code> 
     *          indicating a <code> ParameterProcessorEnabler </code> record 
     *          type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          parameterProcessorEnablerRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsParameterProcessorEnablerRecordType(org.osid.type.Type parameterProcessorEnablerRecordType) {
        return (getAdapteeManager().supportsParameterProcessorEnablerRecordType(parameterProcessorEnablerRecordType));
    }


    /**
     *  Gets the supported <code> ParameterProcessorEnabler </code> search 
     *  record types. 
     *
     *  @return a list containing the supported <code> 
     *          ParameterProcessorEnabler </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getParameterProcessorEnablerSearchRecordTypes() {
        return (getAdapteeManager().getParameterProcessorEnablerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> ParameterProcessorEnabler </code> search 
     *  record type is supported. 
     *
     *  @param  parameterProcessorEnablerSearchRecordType a <code> Type 
     *          </code> indicating a <code> ParameterProcessorEnabler </code> 
     *          search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          parameterProcessorEnablerSearchRecordType </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public boolean supportsParameterProcessorEnablerSearchRecordType(org.osid.type.Type parameterProcessorEnablerSearchRecordType) {
        return (getAdapteeManager().supportsParameterProcessorEnablerSearchRecordType(parameterProcessorEnablerSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the value enabler 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ValueEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueEnablerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerLookupSession getValueEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getValueEnablerLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the value enabler 
     *  lookup service for the given configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ValueEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerLookupSession getValueEnablerLookupSessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getValueEnablerLookupSessionForConfiguration(configurationId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the value enabler 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ValueEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueEnablerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerQuerySession getValueEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getValueEnablerQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the value enabler 
     *  query service for the given configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ValueEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerQuerySession getValueEnablerQuerySessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getValueEnablerQuerySessionForConfiguration(configurationId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the value enabler 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ValueEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueEnablerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerSearchSession getValueEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getValueEnablerSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the value enablers 
     *  earch service for the given configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ValueEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerSearchSession getValueEnablerSearchSessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getValueEnablerSearchSessionForConfiguration(configurationId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the value enabler 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ValueEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueEnablerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerAdminSession getValueEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getValueEnablerAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the value enabler 
     *  administration service for the given configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ValueEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerAdminSession getValueEnablerAdminSessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getValueEnablerAdminSessionForConfiguration(configurationId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the value enabler 
     *  notification service. 
     *
     *  @param  valueEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> ValueEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> valueEnablerReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerNotificationSession getValueEnablerNotificationSession(org.osid.configuration.rules.ValueEnablerReceiver valueEnablerReceiver, 
                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getValueEnablerNotificationSession(valueEnablerReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the value enabler 
     *  notification service for the given configuration. 
     *
     *  @param  valueEnablerReceiver the notification callback 
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ValueEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no configuration found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> valueEnablerReceiver, 
     *          configurationId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerNotificationSession getValueEnablerNotificationSessionForConfiguration(org.osid.configuration.rules.ValueEnablerReceiver valueEnablerReceiver, 
                                                                                                                           org.osid.id.Id configurationId, 
                                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getValueEnablerNotificationSessionForConfiguration(valueEnablerReceiver, configurationId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup value 
     *  enabler/configuration mappings for value enablers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ValueEnablerConfigurationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueEnablerConfiguration() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerConfigurationSession getValueEnablerConfigurationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getValueEnablerConfigurationSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning value 
     *  enablers to configurations. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ValueEnablerConfigurationAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueEnablerConfigurationAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerConfigurationAssignmentSession getValueEnablerConfigurationAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getValueEnablerConfigurationAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage value enabler smart 
     *  configurations. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ValueEnablerSmartConfigurationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueEnablerSmartConfiguration() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerSmartConfigurationSession getValueEnablerSmartConfigurationSession(org.osid.id.Id configurationId, 
                                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getValueEnablerSmartConfigurationSession(configurationId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the value enabler 
     *  mapping lookup service . 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ValueEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerRuleLookupSession getValueEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getValueEnablerRuleLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the value enabler 
     *  mapping lookup service for the given configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ValueEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerRuleLookupSession getValueEnablerRuleLookupSessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getValueEnablerRuleLookupSessionForConfiguration(configurationId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the value enabler 
     *  assignment service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ValueEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueEnablerRuleApplication() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerRuleApplicationSession getValueEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getValueEnablerRuleApplicationSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the value enabler 
     *  assignment service for the given configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ValueEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerRuleApplicationSession getValueEnablerRuleApplicationSessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getValueEnablerRuleApplicationSessionForConfiguration(configurationId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorLookupSession getParameterProcessorLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getParameterProcessorLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor lookup service for the given configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorLookupSession getParameterProcessorLookupSessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getParameterProcessorLookupSessionForConfiguration(configurationId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorQuerySession getParameterProcessorQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getParameterProcessorQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor query service for the given configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorQuerySession getParameterProcessorQuerySessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getParameterProcessorQuerySessionForConfiguration(configurationId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorSearchSession getParameterProcessorSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getParameterProcessorSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor earch service for the given configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorSearchSession getParameterProcessorSearchSessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getParameterProcessorSearchSessionForConfiguration(configurationId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorAdminSession getParameterProcessorAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getParameterProcessorAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor administration service for the given configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorAdminSession getParameterProcessorAdminSessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getParameterProcessorAdminSessionForConfiguration(configurationId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor notification service. 
     *
     *  @param  paremeterProcessorReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          paremeterProcessorReceiver </code> or <code> proxy </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorNotificationSession getParameterProcessorNotificationSession(org.osid.configuration.rules.ParameterProcessorReceiver paremeterProcessorReceiver, 
                                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getParameterProcessorNotificationSession(paremeterProcessorReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor notification service for the given configuration. 
     *
     *  @param  paremeterProcessorReceiver the notification callback 
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorNotificationSession </code> 
     *  @throws org.osid.NotFoundException no configuration found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          paremeterProcessorReceiver, configurationId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorNotificationSession getParameterProcessorNotificationSessionForConfiguration(org.osid.configuration.rules.ParameterProcessorReceiver paremeterProcessorReceiver, 
                                                                                                                                       org.osid.id.Id configurationId, 
                                                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getParameterProcessorNotificationSessionForConfiguration(paremeterProcessorReceiver, configurationId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup parameter 
     *  processor/configuration mappings for parameter processors. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorConfigurationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorConfiguration() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorConfigurationSession getParameterProcessorConfigurationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getParameterProcessorConfigurationSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  parameter processor to configurations. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorConfigurationAssignmentSession 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorConfigurationAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorConfigurationAssignmentSession getParameterProcessorConfigurationAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getParameterProcessorConfigurationAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage parameter processor 
     *  smart configurations. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorSmartConfigurationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorSmartConfiguration() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorSmartConfigurationSession getParameterProcessorSmartConfigurationSession(org.osid.id.Id configurationId, 
                                                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getParameterProcessorSmartConfigurationSession(configurationId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor mapping lookup service for looking up the rules applied to 
     *  the configuration. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorRuleLookupSession getParameterProcessorRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getParameterProcessorRuleLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor mapping lookup service for the given configuration for 
     *  looking up rules applied to an configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorRuleLookupSession getParameterProcessorRuleLookupSessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getParameterProcessorRuleLookupSessionForConfiguration(configurationId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor assignment service to apply to configurations. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorRuleApplicationSession getParameterProcessorRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getParameterProcessorRuleApplicationSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor assignment service for the given configuration to apply to 
     *  configurations. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorRuleApplicationSession getParameterProcessorRuleApplicationSessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getParameterProcessorRuleApplicationSessionForConfiguration(configurationId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor enabler lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorEnablerLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerLookupSession getParameterProcessorEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getParameterProcessorEnablerLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor enabler lookup service for the given configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerLookupSession getParameterProcessorEnablerLookupSessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getParameterProcessorEnablerLookupSessionForConfiguration(configurationId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor enabler query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorEnablerQuery() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerQuerySession getParameterProcessorEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getParameterProcessorEnablerQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor enabler query service for the given configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerQuerySession getParameterProcessorEnablerQuerySessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getParameterProcessorEnablerQuerySessionForConfiguration(configurationId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor enabler search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorEnablerSearch() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerSearchSession getParameterProcessorEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getParameterProcessorEnablerSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor enablers earch service for the given configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerSearchSession getParameterProcessorEnablerSearchSessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getParameterProcessorEnablerSearchSessionForConfiguration(configurationId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor enabler administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorEnablerAdmin() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerAdminSession getParameterProcessorEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getParameterProcessorEnablerAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor enabler administration service for the given configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerAdminSession getParameterProcessorEnablerAdminSessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getParameterProcessorEnablerAdminSessionForConfiguration(configurationId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor enabler notification service. 
     *
     *  @param  parameterProcessorEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          parameterProcessorEnablerReceiver </code> or <code> proxy 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorEnablerNotification() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerNotificationSession getParameterProcessorEnablerNotificationSession(org.osid.configuration.rules.ParameterProcessorEnablerReceiver parameterProcessorEnablerReceiver, 
                                                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getParameterProcessorEnablerNotificationSession(parameterProcessorEnablerReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor enabler notification service for the given configuration. 
     *
     *  @param  parameterProcessorEnablerReceiver the notification callback 
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no configuration found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          parameterProcessorEnablerReceiver, configurationId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorEnablerNotification() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerNotificationSession getParameterProcessorEnablerNotificationSessionForConfiguration(org.osid.configuration.rules.ParameterProcessorEnablerReceiver parameterProcessorEnablerReceiver, 
                                                                                                                                                     org.osid.id.Id configurationId, 
                                                                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getParameterProcessorEnablerNotificationSessionForConfiguration(parameterProcessorEnablerReceiver, configurationId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup parameter processor 
     *  enabler/configuration mappings for parameter processor enablers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorEnablerConfigurationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorEnablerConfiguration() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerConfigurationSession getParameterProcessorEnablerConfigurationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getParameterProcessorEnablerConfigurationSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  parameter processor enablers to configurations. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> 
     *          ParameterProcessorEnablerConfigurationAssignmentSession 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorEnablerConfigurationAssignment() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerConfigurationAssignmentSession getParameterProcessorEnablerConfigurationAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getParameterProcessorEnablerConfigurationAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage parameter processor 
     *  enabler smart configurations. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorEnablerSmartConfigurationSession 
     *          </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorEnablerSmartConfiguration() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerSmartConfigurationSession getParameterProcessorEnablerSmartConfigurationSession(org.osid.id.Id configurationId, 
                                                                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getParameterProcessorEnablerSmartConfigurationSession(configurationId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor enabler mapping lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorEnablerRuleLookup() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerRuleLookupSession getParameterProcessorEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getParameterProcessorEnablerRuleLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor enabler mapping lookup service for the given configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorEnablerRuleLookup() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerRuleLookupSession getParameterProcessorEnablerRuleLookupSessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getParameterProcessorEnablerRuleLookupSessionForConfiguration(configurationId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor enabler assignment service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorEnablerRuleApplicationSession 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerRuleApplicationSession getParameterProcessorEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getParameterProcessorEnablerRuleApplicationSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor enabler assignment service for the given configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorEnablerRuleApplicationSession 
     *          </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerRuleApplicationSession getParameterProcessorEnablerRuleApplicationSessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getParameterProcessorEnablerRuleApplicationSessionForConfiguration(configurationId, proxy));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
