//
// AbstractFiscalPeriodLookupSession.java
//
//    A starter implementation framework for providing a FiscalPeriod
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a FiscalPeriod
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getFiscalPeriods(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractFiscalPeriodLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.financials.FiscalPeriodLookupSession {

    private boolean pedantic  = false;
    private boolean federated = false;
    private org.osid.financials.Business business = new net.okapia.osid.jamocha.nil.financials.business.UnknownBusiness();
    

    /**
     *  Gets the <code>Business/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Business Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBusinessId() {
        return (this.business.getId());
    }


    /**
     *  Gets the <code>Business</code> associated with this 
     *  session.
     *
     *  @return the <code>Business</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.Business getBusiness()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.business);
    }


    /**
     *  Sets the <code>Business</code>.
     *
     *  @param  business the business for this session
     *  @throws org.osid.NullArgumentException <code>business</code>
     *          is <code>null</code>
     */

    protected void setBusiness(org.osid.financials.Business business) {
        nullarg(business, "business");
        this.business = business;
        return;
    }


    /**
     *  Tests if this user can perform <code>FiscalPeriod</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupFiscalPeriods() {
        return (true);
    }


    /**
     *  A complete view of the <code>FiscalPeriod</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeFiscalPeriodView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>FiscalPeriod</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryFiscalPeriodView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include fiscal periods in businesses which are
     *  children of this business in the business hierarchy.
     */

    @OSID @Override
    public void useFederatedBusinessView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this business only.
     */

    @OSID @Override
    public void useIsolatedBusinessView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>FiscalPeriod</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>FiscalPeriod</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>FiscalPeriod</code>
     *  and retained for compatibility.
     *
     *  @param  fiscalPeriodId <code>Id</code> of the
     *          <code>FiscalPeriod</code>
     *  @return the fiscal period
     *  @throws org.osid.NotFoundException <code>fiscalPeriodId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>fiscalPeriodId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriod getFiscalPeriod(org.osid.id.Id fiscalPeriodId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.financials.FiscalPeriodList fiscalPeriods = getFiscalPeriods()) {
            while (fiscalPeriods.hasNext()) {
                org.osid.financials.FiscalPeriod fiscalPeriod = fiscalPeriods.getNextFiscalPeriod();
                if (fiscalPeriod.getId().equals(fiscalPeriodId)) {
                    return (fiscalPeriod);
                }
            }
        } 

        throw new org.osid.NotFoundException(fiscalPeriodId + " not found");
    }


    /**
     *  Gets a <code>FiscalPeriodList</code> corresponding to the
     *  given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  fiscalPeriods specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>FiscalPeriods</code> may be omitted from the list and
     *  may present the elements in any order including returning a
     *  unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getFiscalPeriods()</code>.
     *
     *  @param  fiscalPeriodIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>FiscalPeriod</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>fiscalPeriodIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodList getFiscalPeriodsByIds(org.osid.id.IdList fiscalPeriodIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.financials.FiscalPeriod> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = fiscalPeriodIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getFiscalPeriod(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("fiscal period " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.financials.fiscalperiod.LinkedFiscalPeriodList(ret));
    }


    /**
     *  Gets a <code>FiscalPeriodList</code> corresponding to the
     *  given fiscal period genus <code>Type</code> which does not
     *  include fiscal periods of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known fiscal
     *  periods or an error results. Otherwise, the returned list may
     *  contain only those fiscal periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getFiscalPeriods()</code>.
     *
     *  @param  fiscalPeriodGenusType a fiscalPeriod genus type 
     *  @return the returned <code>FiscalPeriod</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>fiscalPeriodGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodList getFiscalPeriodsByGenusType(org.osid.type.Type fiscalPeriodGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.financials.fiscalperiod.FiscalPeriodGenusFilterList(getFiscalPeriods(), fiscalPeriodGenusType));
    }


    /**
     *  Gets a <code>FiscalPeriodList</code> corresponding to the
     *  given fiscal period genus <code>Type</code> and include any
     *  additional fiscal periods with genus types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known fiscal
     *  periods or an error results. Otherwise, the returned list may
     *  contain only those fiscal periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getFiscalPeriods()</code>.
     *
     *  @param  fiscalPeriodGenusType a fiscalPeriod genus type 
     *  @return the returned <code>FiscalPeriod</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>fiscalPeriodGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodList getFiscalPeriodsByParentGenusType(org.osid.type.Type fiscalPeriodGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getFiscalPeriodsByGenusType(fiscalPeriodGenusType));
    }


    /**
     *  Gets a <code>FiscalPeriodList</code> containing the given
     *  fiscal period record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  fiscal periods or an error results. Otherwise, the returned list
     *  may contain only those fiscal periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getFiscalPeriods()</code>.
     *
     *  @param  fiscalPeriodRecordType a fiscalPeriod record type 
     *  @return the returned <code>FiscalPeriod</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>fiscalPeriodRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodList getFiscalPeriodsByRecordType(org.osid.type.Type fiscalPeriodRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.financials.fiscalperiod.FiscalPeriodRecordFilterList(getFiscalPeriods(), fiscalPeriodRecordType));
    }


    /**
     *  Gets a <code> FiscalPeriodList </code> containing the given
     *  date. In plenary mode, the returned list contains all known
     *  fiscal periods or an error results. Otherwise, the returned
     *  list may contain only those fiscal periods that are accessible
     *  through this session.
     *
     *  @param  date a date 
     *  @return the returned <code> FiscalPeriod </code> list 
     *  @throws org.osid.NullArgumentException <code> date </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodList getFiscalPeriodsByDate(org.osid.calendaring.DateTime date)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.financials.fiscalperiod.FiscalPeriodFilterList(new DateFilter(date), getFiscalPeriods()));
    }


    /**
     *  Gets all <code>FiscalPeriods</code>. 
     *
     *  In plenary mode, the returned list contains all known fiscal
     *  periods or an error results. Otherwise, the returned list may
     *  contain only those fiscal periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>FiscalPeriods</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.financials.FiscalPeriodList getFiscalPeriods()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the fiscal period list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of fiscal periods
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.financials.FiscalPeriodList filterFiscalPeriodsOnViews(org.osid.financials.FiscalPeriodList list)
        throws org.osid.OperationFailedException {

        return (list);
    }


    public static class DateFilter
        implements net.okapia.osid.jamocha.inline.filter.financials.fiscalperiod.FiscalPeriodFilter {

        private final org.osid.calendaring.DateTime date;

        
        /**
         *  Constructs a new <code>DateFilter</code>.
         *
         *  @param date the date to filter
         *  @throws org.osid.NullArgumentException <code>date</code>
         *          is <code>null</code>
         */

        public DateFilter(org.osid.calendaring.DateTime date) {
            nullarg(date, "date");
            this.date = date;
            return;
        }


        /**
         *  Used by the FiscalPeriodFilterList to filter the fiscal
         *  period list based on date.
         *
         *  @param fiscalPeriod the fiscal period
         *  @return <code>true</code> to pass the fiscalPeriod,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.financials.FiscalPeriod fiscalPeriod) {
            if (fiscalPeriod.getStartDate().isGreater(this.date)) {
                return (false);
            }

            if (fiscalPeriod.getEndDate().isLess(this.date)) {
                return (false);
            }

            return (true);
        }
    }    
}
