//
// InvariantMapInputLookupSession
//
//    Implements an Input lookup service backed by a fixed collection of
//    inputs.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.control;


/**
 *  Implements an Input lookup service backed by a fixed
 *  collection of inputs. The inputs are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapInputLookupSession
    extends net.okapia.osid.jamocha.core.control.spi.AbstractMapInputLookupSession
    implements org.osid.control.InputLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapInputLookupSession</code> with no
     *  inputs.
     *  
     *  @param system the system
     *  @throws org.osid.NullArgumnetException {@code system} is
     *          {@code null}
     */

    public InvariantMapInputLookupSession(org.osid.control.System system) {
        setSystem(system);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapInputLookupSession</code> with a single
     *  input.
     *  
     *  @param system the system
     *  @param input an single input
     *  @throws org.osid.NullArgumentException {@code system} or
     *          {@code input} is <code>null</code>
     */

      public InvariantMapInputLookupSession(org.osid.control.System system,
                                               org.osid.control.Input input) {
        this(system);
        putInput(input);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapInputLookupSession</code> using an array
     *  of inputs.
     *  
     *  @param system the system
     *  @param inputs an array of inputs
     *  @throws org.osid.NullArgumentException {@code system} or
     *          {@code inputs} is <code>null</code>
     */

      public InvariantMapInputLookupSession(org.osid.control.System system,
                                               org.osid.control.Input[] inputs) {
        this(system);
        putInputs(inputs);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapInputLookupSession</code> using a
     *  collection of inputs.
     *
     *  @param system the system
     *  @param inputs a collection of inputs
     *  @throws org.osid.NullArgumentException {@code system} or
     *          {@code inputs} is <code>null</code>
     */

      public InvariantMapInputLookupSession(org.osid.control.System system,
                                               java.util.Collection<? extends org.osid.control.Input> inputs) {
        this(system);
        putInputs(inputs);
        return;
    }
}
