//
// AbstractImmutableVote.java
//
//     Wraps a mutable Vote to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.voting.vote.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Vote</code> to hide modifiers. This
 *  wrapper provides an immutized Vote from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying vote whose state changes are visible.
 */

public abstract class AbstractImmutableVote
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRelationship
    implements org.osid.voting.Vote {

    private final org.osid.voting.Vote vote;


    /**
     *  Constructs a new <code>AbstractImmutableVote</code>.
     *
     *  @param vote the vote to immutablize
     *  @throws org.osid.NullArgumentException <code>vote</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableVote(org.osid.voting.Vote vote) {
        super(vote);
        this.vote = vote;
        return;
    }


    /**
     *  Gets the candidate <code> Id. </code> 
     *
     *  @return a candidate <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCandidateId() {
        return (this.vote.getCandidateId());
    }


    /**
     *  Gets the <code> Candidate. </code> 
     *
     *  @return the candidate 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.voting.Candidate getCandidate()
        throws org.osid.OperationFailedException {

        return (this.vote.getCandidate());
    }


    /**
     *  Gets the resource <code> Id </code> of the voter. 
     *
     *  @return the resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getVoterId() {
        return (this.vote.getVoterId());
    }


    /**
     *  Gets the resource of the voter. 
     *
     *  @return the resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getVoter()
        throws org.osid.OperationFailedException {

        return (this.vote.getVoter());
    }


    /**
     *  Gets the agent <code> Id </code> of the voter. 
     *
     *  @return the agent <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getVotingAgentId() {
        return (this.vote.getVotingAgentId());
    }


    /**
     *  Gets the agent of the voter. 
     *
     *  @return the agent 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getVotingAgent()
        throws org.osid.OperationFailedException {

        return (this.vote.getVotingAgent());
    }


    /**
     *  Gets the number of votes cast for this candidate. 
     *
     *  @return the number of votes cast 
     */

    @OSID @Override
    public long getVotes() {
        return (this.vote.getVotes());
    }


    /**
     *  Gets the vote record corresponding to the given <code> Vote </code> 
     *  record <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record. The <code> voteRecordType </code> 
     *  may be the <code> Type </code> returned in <code> getRecordTypes() 
     *  </code> or any of its parents in a <code> Type </code> hierarchy where 
     *  <code> hasRecordType(voteRecordType) </code> is <code> true </code> . 
     *
     *  @param  voteRecordType the type of the record to retrieve 
     *  @return the vote record 
     *  @throws org.osid.NullArgumentException <code> voteRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(voteRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.records.VoteRecord getVoteRecord(org.osid.type.Type voteRecordType)
        throws org.osid.OperationFailedException {

        return (this.vote.getVoteRecord(voteRecordType));
    }
}

