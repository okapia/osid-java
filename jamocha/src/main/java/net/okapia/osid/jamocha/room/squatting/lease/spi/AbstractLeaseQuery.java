//
// AbstractLeaseQuery.java
//
//     A template for making a Lease Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.squatting.lease.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for leases.
 */

public abstract class AbstractLeaseQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQuery
    implements org.osid.room.squatting.LeaseQuery {

    private final java.util.Collection<org.osid.room.squatting.records.LeaseQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the room <code> Id </code> for this query to match rooms assigned 
     *  to leases. 
     *
     *  @param  roomId a room <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> roomId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchRoomId(org.osid.id.Id roomId, boolean match) {
        return;
    }


    /**
     *  Clears the room <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRoomIdTerms() {
        return;
    }


    /**
     *  Tests if a room query is available. 
     *
     *  @return <code> true </code> if a room query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRoomQuery() {
        return (false);
    }


    /**
     *  Gets the query for a lease. 
     *
     *  @return the room query 
     *  @throws org.osid.UnimplementedException <code> supportsRoomQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomQuery getRoomQuery() {
        throw new org.osid.UnimplementedException("supportsRoomQuery() is false");
    }


    /**
     *  Clears the room terms. 
     */

    @OSID @Override
    public void clearRoomTerms() {
        return;
    }


    /**
     *  Sets the resource <code> Id </code> for this query to match rooms 
     *  assigned to leases. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchTenantId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the tenant <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearTenantIdTerms() {
        return;
    }


    /**
     *  Tests if a tenant query is available. 
     *
     *  @return <code> true </code> if a tenant query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTenantQuery() {
        return (false);
    }


    /**
     *  Gets the query for a tenant resource. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsTenantQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getTenantQuery() {
        throw new org.osid.UnimplementedException("supportsTenantQuery() is false");
    }


    /**
     *  Clears the tenant terms. 
     */

    @OSID @Override
    public void clearTenantTerms() {
        return;
    }


    /**
     *  Sets the lease <code> Id </code> for this query to match rooms 
     *  assigned to campuses. 
     *
     *  @param  campusId a campus <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCampusId(org.osid.id.Id campusId, boolean match) {
        return;
    }


    /**
     *  Clears the campus <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCampusIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CampusQuery </code> is available. 
     *
     *  @return <code> true </code> if a campus query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCampusQuery() {
        return (false);
    }


    /**
     *  Gets the query for a campus query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the campus query 
     *  @throws org.osid.UnimplementedException <code> supportsCampusQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.CampusQuery getCampusQuery() {
        throw new org.osid.UnimplementedException("supportsCampusQuery() is false");
    }


    /**
     *  Clears the campus terms. 
     */

    @OSID @Override
    public void clearCampusTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given lease query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a lease implementing the requested record.
     *
     *  @param leaseRecordType a lease record type
     *  @return the lease query record
     *  @throws org.osid.NullArgumentException
     *          <code>leaseRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(leaseRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.room.squatting.records.LeaseQueryRecord getLeaseQueryRecord(org.osid.type.Type leaseRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.room.squatting.records.LeaseQueryRecord record : this.records) {
            if (record.implementsRecordType(leaseRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(leaseRecordType + " is not supported");
    }


    /**
     *  Adds a record to this lease query. 
     *
     *  @param leaseQueryRecord lease query record
     *  @param leaseRecordType lease record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addLeaseQueryRecord(org.osid.room.squatting.records.LeaseQueryRecord leaseQueryRecord, 
                                          org.osid.type.Type leaseRecordType) {

        addRecordType(leaseRecordType);
        nullarg(leaseQueryRecord, "lease query record");
        this.records.add(leaseQueryRecord);        
        return;
    }
}
