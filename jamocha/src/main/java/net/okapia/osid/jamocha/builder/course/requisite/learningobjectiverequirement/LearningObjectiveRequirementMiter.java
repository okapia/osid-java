//
// LearningObjectiveRequirementMiter.java
//
//     Defines a LearningObjectiveRequirement miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.requisite.learningobjectiverequirement;


/**
 *  Defines a <code>LearningObjectiveRequirement</code> miter for use with the builders.
 */

public interface LearningObjectiveRequirementMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidRuleMiter,
            org.osid.course.requisite.LearningObjectiveRequirement {


    /**
     *  Adds an alternative requisite.
     *
     *  @param requisite an alternative requisite
     *  @throws org.osid.NullArgumentException <code>requisite</code>
     *          is <code>null</code>
     */

    public void addAltRequisite(org.osid.course.requisite.Requisite requisite);


    /**
     *  Sets all the alternative requisites.
     *
     *  @param requisites a collection of alternative requisites
     *  @throws org.osid.NullArgumentException <code>requisites</code>
     *          is <code>null</code>
     */

    public void setAltRequisites(java.util.Collection<org.osid.course.requisite.Requisite> requisites);


    /**
     *  Sets the learning objective.
     *
     *  @param objective a learning objective
     *  @throws org.osid.NullArgumentException <code>objective</code>
     *          is <code>null</code>
     */

    public void setLearningObjective(org.osid.learning.Objective objective);


    /**
     *  Sets the minimum proficiency.
     *
     *  @param proficiency a minimum proficiency
     *  @throws org.osid.NullArgumentException
     *          <code>proficiency</code> is <code>null</code>
     */

    public void setMinimumProficiency(org.osid.grading.Grade proficiency);


    /**
     *  Adds a LearningObjectiveRequirement record.
     *
     *  @param record a LearningObjectiveRequirement record
     *  @param recordType the type of learningObjectiveRequirement record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addLearningObjectiveRequirementRecord(org.osid.course.requisite.records.LearningObjectiveRequirementRecord record, org.osid.type.Type recordType);
}       


