//
// InvariantMapProxyParameterLookupSession
//
//    Implements a Parameter lookup service backed by a fixed
//    collection of parameters. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.configuration;


/**
 *  Implements a Parameter lookup service backed by a fixed
 *  collection of parameters. The parameters are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyParameterLookupSession
    extends net.okapia.osid.jamocha.core.configuration.spi.AbstractMapParameterLookupSession
    implements org.osid.configuration.ParameterLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyParameterLookupSession} with no
     *  parameters.
     *
     *  @param configuration the configuration
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code configuration} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyParameterLookupSession(org.osid.configuration.Configuration configuration,
                                                  org.osid.proxy.Proxy proxy) {
        setConfiguration(configuration);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyParameterLookupSession} with a single
     *  parameter.
     *
     *  @param configuration the configuration
     *  @param parameter a single parameter
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code configuration},
     *          {@code parameter} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyParameterLookupSession(org.osid.configuration.Configuration configuration,
                                                  org.osid.configuration.Parameter parameter, org.osid.proxy.Proxy proxy) {

        this(configuration, proxy);
        putParameter(parameter);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyParameterLookupSession} using
     *  an array of parameters.
     *
     *  @param configuration the configuration
     *  @param parameters an array of parameters
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code configuration},
     *          {@code parameters} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyParameterLookupSession(org.osid.configuration.Configuration configuration,
                                                  org.osid.configuration.Parameter[] parameters, org.osid.proxy.Proxy proxy) {

        this(configuration, proxy);
        putParameters(parameters);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyParameterLookupSession} using a
     *  collection of parameters.
     *
     *  @param configuration the configuration
     *  @param parameters a collection of parameters
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code configuration},
     *          {@code parameters} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyParameterLookupSession(org.osid.configuration.Configuration configuration,
                                                  java.util.Collection<? extends org.osid.configuration.Parameter> parameters,
                                                  org.osid.proxy.Proxy proxy) {

        this(configuration, proxy);
        putParameters(parameters);
        return;
    }
}
