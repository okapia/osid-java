//
// FederatingResourceRelationshipPeerList.java
//
//     Interface for federating lists.
//
//
// Tom Coppeto
// Okapia
// 17 September 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.resource.batch.resourcerelationshippeer;


/**
 *  Federating resourcerelationshippeer lists.
 */

public interface FederatingResourceRelationshipPeerList
    extends org.osid.resource.batch.ResourceRelationshipPeerList {


    /**
     *  Adds a <code>ResourceRelationshipPeerList</code> to this
     *  <code>ResourceRelationshipPeerList</code> using the default buffer size.
     *
     *  @param resourceRelationshipPeerList the <code>ResourceRelationshipPeerList</code> to add
     *  @throws org.osid.IllegalStateException this list already closed
     *  @throws org.osid.NullArgumentException
     *          <code>resourceRelationshipPeerList</code> is <code>null</code>
     */

    public void addResourceRelationshipPeerList(org.osid.resource.batch.ResourceRelationshipPeerList resourceRelationshipPeerList);


    /**
     *  Adds a collection of <code>ResourceRelationshipPeerLists</code> to this
     *  <code>ResourceRelationshipPeerList</code>.
     *
     *  @param resourceRelationshipPeerLists the <code>ResourceRelationshipPeerList</code> collection
     *         to add
     *  @throws org.osid.IllegalStateException this list already closed
     *  @throws org.osid.NullArgumentException
     *          <code>resourceRelationshipPeerLists</code> is <code>null</code>
     */

    public void addResourceRelationshipPeerLists(java.util.Collection<org.osid.resource.batch.ResourceRelationshipPeerList> resourceRelationshipPeerLists);

        
    /**
     *  No more. Invoked when no more lists are to be added.
     */

    public void noMore();


    /**
     *  Tests if the list is operational. A list is operational if
     *  <code>close()</code> has not been invoked.
     *
     *  @return <code>true</code> if the list is operational,
     *          <code>false</code> otherwise.
     */

    public boolean isOperational();


    /**
     *  Tests if the list is empty.
     *
     *  @return <code>true</code> if the list is empty
     *          retrieved, <code>false</code> otherwise.
     */

    public boolean isEmpty();
}
