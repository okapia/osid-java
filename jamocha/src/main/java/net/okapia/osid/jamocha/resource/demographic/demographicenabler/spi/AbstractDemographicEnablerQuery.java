//
// AbstractDemographicEnablerQuery.java
//
//     A template for making a DemographicEnabler Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resource.demographic.demographicenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for demographic enablers.
 */

public abstract class AbstractDemographicEnablerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQuery
    implements org.osid.resource.demographic.DemographicEnablerQuery {

    private final java.util.Collection<org.osid.resource.demographic.records.DemographicEnablerQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches enablers mapped to the demographic. 
     *
     *  @param  demographicId the demographic <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> demographicId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledDemographicId(org.osid.id.Id demographicId, 
                                        boolean match) {
        return;
    }


    /**
     *  Clears the demographic <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledDemographicIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> DemographicQuery </code> is available. 
     *
     *  @return <code> true </code> if a demographic query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledDemographicQuery() {
        return (false);
    }


    /**
     *  Gets the query for a demographic. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the demographic query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledDemographicQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicQuery getRuledDemographicQuery() {
        throw new org.osid.UnimplementedException("supportsRuledDemographicQuery() is false");
    }


    /**
     *  Matches enablers mapped to any demographic. 
     *
     *  @param  match <code> true </code> for enablers mapped to any 
     *          demographic, <code> false </code> to match enablers mapped to 
     *          no demographics 
     */

    @OSID @Override
    public void matchAnyRuledDemographic(boolean match) {
        return;
    }


    /**
     *  Clears the demographic query terms. 
     */

    @OSID @Override
    public void clearRuledDemographicTerms() {
        return;
    }


    /**
     *  Matches enablers mapped to the bin. 
     *
     *  @param  binId the bin <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchBinId(org.osid.id.Id binId, boolean match) {
        return;
    }


    /**
     *  Clears the bin <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearBinIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BinQuery </code> is available. 
     *
     *  @return <code> true </code> if a bin query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBinQuery() {
        return (false);
    }


    /**
     *  Gets the query for a bin. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the bin query 
     *  @throws org.osid.UnimplementedException <code> supportsBinQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.BinQuery getBinQuery() {
        throw new org.osid.UnimplementedException("supportsBinQuery() is false");
    }


    /**
     *  Clears the bin query terms. 
     */

    @OSID @Override
    public void clearBinTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given demographic enabler query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a demographic enabler implementing the requested record.
     *
     *  @param demographicEnablerRecordType a demographic enabler record type
     *  @return the demographic enabler query record
     *  @throws org.osid.NullArgumentException
     *          <code>demographicEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(demographicEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.resource.demographic.records.DemographicEnablerQueryRecord getDemographicEnablerQueryRecord(org.osid.type.Type demographicEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resource.demographic.records.DemographicEnablerQueryRecord record : this.records) {
            if (record.implementsRecordType(demographicEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(demographicEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this demographic enabler query. 
     *
     *  @param demographicEnablerQueryRecord demographic enabler query record
     *  @param demographicEnablerRecordType demographicEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addDemographicEnablerQueryRecord(org.osid.resource.demographic.records.DemographicEnablerQueryRecord demographicEnablerQueryRecord, 
                                          org.osid.type.Type demographicEnablerRecordType) {

        addRecordType(demographicEnablerRecordType);
        nullarg(demographicEnablerQueryRecord, "demographic enabler query record");
        this.records.add(demographicEnablerQueryRecord);        
        return;
    }
}
