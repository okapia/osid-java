//
// FrontOfficeFilterList.java
//
//     Implements a filtering FrontOfficeList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.tracking.frontoffice;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filtering FrontOfficeList.
 */

public final class FrontOfficeFilterList
    extends net.okapia.osid.jamocha.inline.filter.tracking.frontoffice.spi.AbstractFrontOfficeFilterList
    implements org.osid.tracking.FrontOfficeList,
               FrontOfficeFilter {

    private final FrontOfficeFilter filter;


    /**
     *  Creates a new <code>FrontOfficeFilterList</code>.
     *
     *  @param filter an inline query filter
     *  @param list a <code>FrontOfficeList</code>
     *  @throws org.osid.NullArgumentException <code>filter</code> or
     *          <code>list</code> is <code>null</code>
     */

    public FrontOfficeFilterList(FrontOfficeFilter filter, org.osid.tracking.FrontOfficeList list) {
        super(list);

        nullarg(filter, "inline query filter");
        this.filter = filter;

        return;
    }    

    
    /**
     *  Filters FrontOffices.
     *
     *  @param frontOffice the front office to filter
     *  @return <code>true</code> if the front office passes the filter,
     *          <code>false</code> if the front office should be filtered
     */

    @Override
    public boolean pass(org.osid.tracking.FrontOffice frontOffice) {
        return (this.filter.pass(frontOffice));
    }
}
