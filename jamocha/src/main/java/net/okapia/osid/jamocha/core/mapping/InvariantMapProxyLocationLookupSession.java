//
// InvariantMapProxyLocationLookupSession
//
//    Implements a Location lookup service backed by a fixed
//    collection of locations. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping;


/**
 *  Implements a Location lookup service backed by a fixed
 *  collection of locations. The locations are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyLocationLookupSession
    extends net.okapia.osid.jamocha.core.mapping.spi.AbstractMapLocationLookupSession
    implements org.osid.mapping.LocationLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyLocationLookupSession} with no
     *  locations.
     *
     *  @param map the map
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code map} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyLocationLookupSession(org.osid.mapping.Map map,
                                                  org.osid.proxy.Proxy proxy) {
        setMap(map);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyLocationLookupSession} with a single
     *  location.
     *
     *  @param map the map
     *  @param location a single location
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code map},
     *          {@code location} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyLocationLookupSession(org.osid.mapping.Map map,
                                                  org.osid.mapping.Location location, org.osid.proxy.Proxy proxy) {

        this(map, proxy);
        putLocation(location);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyLocationLookupSession} using
     *  an array of locations.
     *
     *  @param map the map
     *  @param locations an array of locations
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code map},
     *          {@code locations} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyLocationLookupSession(org.osid.mapping.Map map,
                                                  org.osid.mapping.Location[] locations, org.osid.proxy.Proxy proxy) {

        this(map, proxy);
        putLocations(locations);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyLocationLookupSession} using a
     *  collection of locations.
     *
     *  @param map the map
     *  @param locations a collection of locations
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code map},
     *          {@code locations} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyLocationLookupSession(org.osid.mapping.Map map,
                                                  java.util.Collection<? extends org.osid.mapping.Location> locations,
                                                  org.osid.proxy.Proxy proxy) {

        this(map, proxy);
        putLocations(locations);
        return;
    }
}
