//
// AbstractModelQuery.java
//
//     A template for making a Model Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inventory.model.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for models.
 */

public abstract class AbstractModelQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.inventory.ModelQuery {

    private final java.util.Collection<org.osid.inventory.records.ModelQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the inventory <code> Id </code> for this query to match models 
     *  that have a related manufacturer. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchManufacturerId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the manufacturer terms. 
     */

    @OSID @Override
    public void clearManufacturerIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ManufacturerQuery </code> is available for the 
     *  location. 
     *
     *  @return <code> true </code> if a manufacturer query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsManufacturerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a manufacturer. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the manufacturer query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsManufacturerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getManufacturerQuery() {
        throw new org.osid.UnimplementedException("supportsManufacturerQuery() is false");
    }


    /**
     *  Matches any manufacturer. 
     *
     *  @param  match <code> true </code> to match models with any inventory, 
     *          <code> false </code> to match models with no inventories 
     */

    @OSID @Override
    public void matchAnyManufacturer(boolean match) {
        return;
    }


    /**
     *  Clears the manufacturer terms. 
     */

    @OSID @Override
    public void clearManufacturerTerms() {
        return;
    }


    /**
     *  Matches an archetype. 
     *
     *  @param  archetype an archetype for the model 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> archetype </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchArchetype(String archetype, 
                               org.osid.type.Type stringMatchType, 
                               boolean match) {
        return;
    }


    /**
     *  Matches items that have any archetype. 
     *
     *  @param  match <code> true </code> to match items with any archetype, 
     *          <code> false </code> to match items with no archetype 
     */

    @OSID @Override
    public void matchAnyArchetype(boolean match) {
        return;
    }


    /**
     *  Clears the archetype terms. 
     */

    @OSID @Override
    public void clearArchetypeTerms() {
        return;
    }


    /**
     *  Matches a model number. 
     *
     *  @param  number a model number 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> number </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchNumber(String number, org.osid.type.Type stringMatchType, 
                            boolean match) {
        return;
    }


    /**
     *  Matches items that have any model number. 
     *
     *  @param  match <code> true </code> to match items with any model 
     *          number, <code> false </code> to match items with no model 
     *          number 
     */

    @OSID @Override
    public void matchAnyNumber(boolean match) {
        return;
    }


    /**
     *  Clears the model number terms. 
     */

    @OSID @Override
    public void clearNumberTerms() {
        return;
    }


    /**
     *  Sets the warehouse <code> Id </code> for this query to match models 
     *  assigned to warehouses. 
     *
     *  @param  warehouseId the warehouse <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchWarehouseId(org.osid.id.Id warehouseId, boolean match) {
        return;
    }


    /**
     *  Clears the warehouse <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearWarehouseIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> WarehouseQuery </code> is available. 
     *
     *  @return <code> true </code> if a warehouse query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWarehouseQuery() {
        return (false);
    }


    /**
     *  Gets the query for a warehouse. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the warehouse query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWarehouseQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseQuery getWarehouseQuery() {
        throw new org.osid.UnimplementedException("supportsWarehouseQuery() is false");
    }


    /**
     *  Clears the warehouse terms. 
     */

    @OSID @Override
    public void clearWarehouseTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given model query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a model implementing the requested record.
     *
     *  @param modelRecordType a model record type
     *  @return the model query record
     *  @throws org.osid.NullArgumentException
     *          <code>modelRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(modelRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.inventory.records.ModelQueryRecord getModelQueryRecord(org.osid.type.Type modelRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inventory.records.ModelQueryRecord record : this.records) {
            if (record.implementsRecordType(modelRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(modelRecordType + " is not supported");
    }


    /**
     *  Adds a record to this model query. 
     *
     *  @param modelQueryRecord model query record
     *  @param modelRecordType model record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addModelQueryRecord(org.osid.inventory.records.ModelQueryRecord modelQueryRecord, 
                                          org.osid.type.Type modelRecordType) {

        addRecordType(modelRecordType);
        nullarg(modelQueryRecord, "model query record");
        this.records.add(modelQueryRecord);        
        return;
    }
}
