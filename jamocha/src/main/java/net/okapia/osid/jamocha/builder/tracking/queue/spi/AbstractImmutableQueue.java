//
// AbstractImmutableQueue.java
//
//     Wraps a mutable Queue to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.tracking.queue.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Queue</code> to hide modifiers. This
 *  wrapper provides an immutized Queue from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying queue whose state changes are visible.
 */

public abstract class AbstractImmutableQueue
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidGovernator
    implements org.osid.tracking.Queue {

    private final org.osid.tracking.Queue queue;


    /**
     *  Constructs a new <code>AbstractImmutableQueue</code>.
     *
     *  @param queue the queue to immutablize
     *  @throws org.osid.NullArgumentException <code>queue</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableQueue(org.osid.tracking.Queue queue) {
        super(queue);
        this.queue = queue;
        return;
    }


    /**
     *  Gets the queue record corresponding to the given <code> Queue </code> 
     *  record <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record. The <code> queueRecordType </code> 
     *  may be the <code> Type </code> returned in <code> getRecordTypes() 
     *  </code> or any of its parents in a <code> Type </code> hierarchy where 
     *  <code> hasRecordType(queueRecordType) </code> is <code> true </code> . 
     *
     *  @param  queueRecordType the type of queue record to retrieve 
     *  @return the queue record 
     *  @throws org.osid.NullArgumentException <code> queueRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(queueRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.records.QueueRecord getQueueRecord(org.osid.type.Type queueRecordType)
        throws org.osid.OperationFailedException {

        return (this.queue.getQueueRecord(queueRecordType));
    }
}

