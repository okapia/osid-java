//
// AbstractQueryProfileEntryLookupSession.java
//
//    A ProfileEntryQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.profile.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ProfileEntryQuerySession adapter.
 */

public abstract class AbstractAdapterProfileEntryQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.profile.ProfileEntryQuerySession {

    private final org.osid.profile.ProfileEntryQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterProfileEntryQuerySession.
     *
     *  @param session the underlying profile entry query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterProfileEntryQuerySession(org.osid.profile.ProfileEntryQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeProfile</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeProfile Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getProfileId() {
        return (this.session.getProfileId());
    }


    /**
     *  Gets the {@codeProfile</code> associated with this 
     *  session.
     *
     *  @return the {@codeProfile</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.Profile getProfile()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getProfile());
    }


    /**
     *  Tests if this user can perform {@codeProfileEntry</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchProfileEntries() {
        return (this.session.canSearchProfileEntries());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include profile entries in profiles which are children
     *  of this profile in the profile hierarchy.
     */

    @OSID @Override
    public void useFederatedProfileView() {
        this.session.useFederatedProfileView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this profile only.
     */
    
    @OSID @Override
    public void useIsolatedProfileView() {
        this.session.useIsolatedProfileView();
        return;
    }
    
      
    /**
     *  Sets the view for methods in this session to implicit profile
     *  entries.  An implicit view will include profile entries
     *  derived from other entries as a result of the {@code
     *  ProfileItem} or {@code Resource} hierarchies. This method is
     *  the opposite of {@code explicitProfileEntryView()}.
     */

    public void useImplicitProfileEntryView() {
        this.session.useImplicitProfileEntryView();
        return;
    }


    /**
     *  Sets the view for methods in this session to explicit profile
     *  entries.  An explicit view includes only those profile entries
     *  that were explicitly defined and not implied. This method is
     *  the opposite of {@code implicitProfileEntryView()} .
     */

    @OSID @Override
    public void useExplicitProfileEntryView() {
        this.session.useExplicitProfileEntryView();
        return;
    }


    /**
     *  Gets a profile entry query. The returned query will not have an
     *  extension query.
     *
     *  @return the profile entry query 
     */
      
    @OSID @Override
    public org.osid.profile.ProfileEntryQuery getProfileEntryQuery() {
        return (this.session.getProfileEntryQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  profileEntryQuery the profile entry query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code profileEntryQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code profileEntryQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesByQuery(org.osid.profile.ProfileEntryQuery profileEntryQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getProfileEntriesByQuery(profileEntryQuery));
    }
}
