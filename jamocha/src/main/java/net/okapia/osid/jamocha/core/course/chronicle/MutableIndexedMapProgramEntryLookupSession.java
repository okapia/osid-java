//
// MutableIndexedMapProgramEntryLookupSession
//
//    Implements a ProgramEntry lookup service backed by a collection of
//    programEntries indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.chronicle;


/**
 *  Implements a ProgramEntry lookup service backed by a collection of
 *  program entries. The program entries are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some program entries may be compatible
 *  with more types than are indicated through these program entry
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of program entries can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProgramEntryLookupSession
    extends net.okapia.osid.jamocha.core.course.chronicle.spi.AbstractIndexedMapProgramEntryLookupSession
    implements org.osid.course.chronicle.ProgramEntryLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapProgramEntryLookupSession} with no program entries.
     *
     *  @param courseCatalog the course catalog
     *  @throws org.osid.NullArgumentException {@code courseCatalog}
     *          is {@code null}
     */

      public MutableIndexedMapProgramEntryLookupSession(org.osid.course.CourseCatalog courseCatalog) {
        setCourseCatalog(courseCatalog);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProgramEntryLookupSession} with a
     *  single program entry.
     *  
     *  @param courseCatalog the course catalog
     *  @param  programEntry a single programEntry
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code programEntry} is {@code null}
     */

    public MutableIndexedMapProgramEntryLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  org.osid.course.chronicle.ProgramEntry programEntry) {
        this(courseCatalog);
        putProgramEntry(programEntry);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProgramEntryLookupSession} using an
     *  array of program entries.
     *
     *  @param courseCatalog the course catalog
     *  @param  programEntries an array of program entries
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code programEntries} is {@code null}
     */

    public MutableIndexedMapProgramEntryLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  org.osid.course.chronicle.ProgramEntry[] programEntries) {
        this(courseCatalog);
        putProgramEntries(programEntries);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProgramEntryLookupSession} using a
     *  collection of program entries.
     *
     *  @param courseCatalog the course catalog
     *  @param  programEntries a collection of program entries
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code programEntries} is {@code null}
     */

    public MutableIndexedMapProgramEntryLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  java.util.Collection<? extends org.osid.course.chronicle.ProgramEntry> programEntries) {

        this(courseCatalog);
        putProgramEntries(programEntries);
        return;
    }
    

    /**
     *  Makes a {@code ProgramEntry} available in this session.
     *
     *  @param  programEntry a program entry
     *  @throws org.osid.NullArgumentException {@code programEntry{@code  is
     *          {@code null}
     */

    @Override
    public void putProgramEntry(org.osid.course.chronicle.ProgramEntry programEntry) {
        super.putProgramEntry(programEntry);
        return;
    }


    /**
     *  Makes an array of program entries available in this session.
     *
     *  @param  programEntries an array of program entries
     *  @throws org.osid.NullArgumentException {@code programEntries{@code 
     *          is {@code null}
     */

    @Override
    public void putProgramEntries(org.osid.course.chronicle.ProgramEntry[] programEntries) {
        super.putProgramEntries(programEntries);
        return;
    }


    /**
     *  Makes collection of program entries available in this session.
     *
     *  @param  programEntries a collection of program entries
     *  @throws org.osid.NullArgumentException {@code programEntry{@code  is
     *          {@code null}
     */

    @Override
    public void putProgramEntries(java.util.Collection<? extends org.osid.course.chronicle.ProgramEntry> programEntries) {
        super.putProgramEntries(programEntries);
        return;
    }


    /**
     *  Removes a ProgramEntry from this session.
     *
     *  @param programEntryId the {@code Id} of the program entry
     *  @throws org.osid.NullArgumentException {@code programEntryId{@code  is
     *          {@code null}
     */

    @Override
    public void removeProgramEntry(org.osid.id.Id programEntryId) {
        super.removeProgramEntry(programEntryId);
        return;
    }    
}
