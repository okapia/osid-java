//
// AbstractFinancialsBudgetingManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.budgeting.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractFinancialsBudgetingManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.financials.budgeting.FinancialsBudgetingManager,
               org.osid.financials.budgeting.FinancialsBudgetingProxyManager {

    private final Types budgetRecordTypes                  = new TypeRefSet();
    private final Types budgetSearchRecordTypes            = new TypeRefSet();

    private final Types budgetEntryRecordTypes             = new TypeRefSet();
    private final Types budgetEntrySearchRecordTypes       = new TypeRefSet();


    /**
     *  Constructs a new
     *  <code>AbstractFinancialsBudgetingManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractFinancialsBudgetingManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any business federation is exposed. Federation is exposed 
     *  when a specific business may be identified, selected and used to 
     *  create a lookup or admin session. Federation is not exposed when a set 
     *  of catalogs appears as a single catalog. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if looking up budgets is supported. 
     *
     *  @return <code> true </code> if budget lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBudgetLookup() {
        return (false);
    }


    /**
     *  Tests if querying budgets is supported. 
     *
     *  @return <code> true </code> if budget query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBudgetQuery() {
        return (false);
    }


    /**
     *  Tests if searching budgets is supported. 
     *
     *  @return <code> true </code> if budget search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBudgetSearch() {
        return (false);
    }


    /**
     *  Tests if budget <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if budget administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBudgetAdmin() {
        return (false);
    }


    /**
     *  Tests if a budget <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if budget notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBudgetNotification() {
        return (false);
    }


    /**
     *  Tests if a budget cataloging service is supported. 
     *
     *  @return <code> true </code> if budget catalog is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBudgetBusiness() {
        return (false);
    }


    /**
     *  Tests if a budget cataloging service is supported. A cataloging 
     *  service maps budgets to catalogs. 
     *
     *  @return <code> true </code> if budget cataloging is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBudgetBusinessAssignment() {
        return (false);
    }


    /**
     *  Tests if a budget smart business session is available. 
     *
     *  @return <code> true </code> if a budget smart business session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBudgetSmartBusiness() {
        return (false);
    }


    /**
     *  Tests if looking up budget entries is supported. 
     *
     *  @return <code> true </code> if budget entry lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBudgetEntryLookup() {
        return (false);
    }


    /**
     *  Tests if querying budget entries is supported. 
     *
     *  @return <code> true </code> if budget entry query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBudgetEntryQuery() {
        return (false);
    }


    /**
     *  Tests if searching budget entries is supported. 
     *
     *  @return <code> true </code> if budget entry search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBudgetEntrySearch() {
        return (false);
    }


    /**
     *  Tests if budget entry administrative service is supported. 
     *
     *  @return <code> true </code> if budget entry administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBudgetEntryAdmin() {
        return (false);
    }


    /**
     *  Tests if an entry <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if budget entry notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBudgetEntryNotification() {
        return (false);
    }


    /**
     *  Tests if a budget entry cataloging service is supported. 
     *
     *  @return <code> true </code> if budget entry catalog is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBudgetEntryBusiness() {
        return (false);
    }


    /**
     *  Tests if a budget entry cataloging service is supported. 
     *
     *  @return <code> true </code> if budget entry cataloging is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBudgetEntryBusinessAssignment() {
        return (false);
    }


    /**
     *  Tests if a budget entry smart business session is available. 
     *
     *  @return <code> true </code> if a budget entry smart business session 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBudgetEntrySmartBusiness() {
        return (false);
    }


    /**
     *  Tests for the availability of a financials budgeting batch service. 
     *
     *  @return <code> true </code> if a financials budgeting batch service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFinancialsBudgetingBatch() {
        return (false);
    }


    /**
     *  Gets the supported <code> Budget </code> record types. 
     *
     *  @return a list containing the supported <code> Budget </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBudgetRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.budgetRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Budget </code> record type is supported. 
     *
     *  @param  budgetRecordType a <code> Type </code> indicating a <code> 
     *          Budget </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> budgetRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBudgetRecordType(org.osid.type.Type budgetRecordType) {
        return (this.budgetRecordTypes.contains(budgetRecordType));
    }


    /**
     *  Adds support for a budget record type.
     *
     *  @param budgetRecordType a budget record type
     *  @throws org.osid.NullArgumentException
     *  <code>budgetRecordType</code> is <code>null</code>
     */

    protected void addBudgetRecordType(org.osid.type.Type budgetRecordType) {
        this.budgetRecordTypes.add(budgetRecordType);
        return;
    }


    /**
     *  Removes support for a budget record type.
     *
     *  @param budgetRecordType a budget record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>budgetRecordType</code> is <code>null</code>
     */

    protected void removeBudgetRecordType(org.osid.type.Type budgetRecordType) {
        this.budgetRecordTypes.remove(budgetRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Budget </code> search record types. 
     *
     *  @return a list containing the supported <code> Budget </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBudgetSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.budgetSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Budget </code> search record type is 
     *  supported. 
     *
     *  @param  budgetSearchRecordType a <code> Type </code> indicating a 
     *          <code> Budget </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> budgetSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBudgetSearchRecordType(org.osid.type.Type budgetSearchRecordType) {
        return (this.budgetSearchRecordTypes.contains(budgetSearchRecordType));
    }


    /**
     *  Adds support for a budget search record type.
     *
     *  @param budgetSearchRecordType a budget search record type
     *  @throws org.osid.NullArgumentException
     *  <code>budgetSearchRecordType</code> is <code>null</code>
     */

    protected void addBudgetSearchRecordType(org.osid.type.Type budgetSearchRecordType) {
        this.budgetSearchRecordTypes.add(budgetSearchRecordType);
        return;
    }


    /**
     *  Removes support for a budget search record type.
     *
     *  @param budgetSearchRecordType a budget search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>budgetSearchRecordType</code> is <code>null</code>
     */

    protected void removeBudgetSearchRecordType(org.osid.type.Type budgetSearchRecordType) {
        this.budgetSearchRecordTypes.remove(budgetSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> BudgetEntry </code> record types. 
     *
     *  @return a list containing the supported <code> BudgetEntry </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBudgetEntryRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.budgetEntryRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> BudgetEntry </code> record type is 
     *  supported. 
     *
     *  @param  budgetEntryRecordType a <code> Type </code> indicating a 
     *          <code> BudgetEntry </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> budgetEntryRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBudgetEntryRecordType(org.osid.type.Type budgetEntryRecordType) {
        return (this.budgetEntryRecordTypes.contains(budgetEntryRecordType));
    }


    /**
     *  Adds support for a budget entry record type.
     *
     *  @param budgetEntryRecordType a budget entry record type
     *  @throws org.osid.NullArgumentException
     *  <code>budgetEntryRecordType</code> is <code>null</code>
     */

    protected void addBudgetEntryRecordType(org.osid.type.Type budgetEntryRecordType) {
        this.budgetEntryRecordTypes.add(budgetEntryRecordType);
        return;
    }


    /**
     *  Removes support for a budget entry record type.
     *
     *  @param budgetEntryRecordType a budget entry record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>budgetEntryRecordType</code> is <code>null</code>
     */

    protected void removeBudgetEntryRecordType(org.osid.type.Type budgetEntryRecordType) {
        this.budgetEntryRecordTypes.remove(budgetEntryRecordType);
        return;
    }


    /**
     *  Gets the supported <code> BudgetEntry </code> search record types. 
     *
     *  @return a list containing the supported <code> BudgetEntry </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBudgetEntrySearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.budgetEntrySearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> BudgetEntry </code> search record type is 
     *  supported. 
     *
     *  @param  budgetEntrySearchRecordType a <code> Type </code> indicating a 
     *          <code> BudgetEntry </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          budgetEntrySearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBudgetEntrySearchRecordType(org.osid.type.Type budgetEntrySearchRecordType) {
        return (this.budgetEntrySearchRecordTypes.contains(budgetEntrySearchRecordType));
    }


    /**
     *  Adds support for a budget entry search record type.
     *
     *  @param budgetEntrySearchRecordType a budget entry search record type
     *  @throws org.osid.NullArgumentException
     *  <code>budgetEntrySearchRecordType</code> is <code>null</code>
     */

    protected void addBudgetEntrySearchRecordType(org.osid.type.Type budgetEntrySearchRecordType) {
        this.budgetEntrySearchRecordTypes.add(budgetEntrySearchRecordType);
        return;
    }


    /**
     *  Removes support for a budget entry search record type.
     *
     *  @param budgetEntrySearchRecordType a budget entry search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>budgetEntrySearchRecordType</code> is <code>null</code>
     */

    protected void removeBudgetEntrySearchRecordType(org.osid.type.Type budgetEntrySearchRecordType) {
        this.budgetEntrySearchRecordTypes.remove(budgetEntrySearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget lookup 
     *  service. 
     *
     *  @return a <code> BudgetSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBudgetLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetLookupSession getBudgetLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingManager.getBudgetLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget lookup 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> BudgetSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBudgetLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetLookupSession getBudgetLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingProxyManager.getBudgetLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget lookup 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the business 
     *  @return a <code> BudgetLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBudgetLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetLookupSession getBudgetLookupSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingManager.getBudgetLookupSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget lookup 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the business 
     *  @param  proxy proxy 
     *  @return a <code> BudgetLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBudgetLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetLookupSession getBudgetLookupSessionForBusiness(org.osid.id.Id businessId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingProxyManager.getBudgetLookupSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget query 
     *  service. 
     *
     *  @return a <code> BudgetQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBudgetQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetQuerySession getBudgetQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingManager.getBudgetQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget query 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> BudgetQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBudgetQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetQuerySession getBudgetQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingProxyManager.getBudgetQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget query 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> BudgetQuerySession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBudgetQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetQuerySession getBudgetQuerySessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingManager.getBudgetQuerySessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget query 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> BudgetQuerySession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBudgetQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetQuerySession getBudgetQuerySessionForBusiness(org.osid.id.Id businessId, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingProxyManager.getBudgetQuerySessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget search 
     *  service. 
     *
     *  @return a <code> BudgetSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBudgetSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetSearchSession getBudgetSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingManager.getBudgetSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget search 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> BudgetSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBudgetSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetSearchSession getBudgetSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingProxyManager.getBudgetSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget search 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> BudgetSearchSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBudgetSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetSearchSession getBudgetSearchSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingManager.getBudgetSearchSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget search 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> BudgetSearchSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBudgetSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetSearchSession getBudgetSearchSessionForBusiness(org.osid.id.Id businessId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingProxyManager.getBudgetSearchSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget 
     *  administration service. 
     *
     *  @return a <code> BudgetAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBudgetAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetAdminSession getBudgetAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingManager.getBudgetAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget 
     *  administration service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> BudgetAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBudgetAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetAdminSession getBudgetAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingProxyManager.getBudgetAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget 
     *  administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> BudgetAdminSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBudgetAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetAdminSession getBudgetAdminSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingManager.getBudgetAdminSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget 
     *  administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> BudgetAdminSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBudgetAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetAdminSession getBudgetAdminSessionForBusiness(org.osid.id.Id businessId, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingProxyManager.getBudgetAdminSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget 
     *  notification service. 
     *
     *  @param  budgetReceiver the notification callback 
     *  @return a <code> BudgetNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> budgetReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetNotificationSession getBudgetNotificationSession(org.osid.financials.budgeting.BudgetReceiver budgetReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingManager.getBudgetNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget 
     *  notification service. 
     *
     *  @param  budgetReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return a <code> BudgetNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> budgetReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetNotificationSession getBudgetNotificationSession(org.osid.financials.budgeting.BudgetReceiver budgetReceiver, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingProxyManager.getBudgetNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget 
     *  notification service for the given business. 
     *
     *  @param  budgetReceiver the notification callback 
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> BudgetNotificationSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> budgetReceiver </code> 
     *          or <code> businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetNotificationSession getBudgetNotificationSessionForBusiness(org.osid.financials.budgeting.BudgetReceiver budgetReceiver, 
                                                                                                           org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingManager.getBudgetNotificationSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget 
     *  notification service for the given business. 
     *
     *  @param  budgetReceiver the notification callback 
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> BudgetNotificationSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> budgetReceiver, 
     *          businessId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetNotificationSession getBudgetNotificationSessionForBusiness(org.osid.financials.budgeting.BudgetReceiver budgetReceiver, 
                                                                                                           org.osid.id.Id businessId, 
                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingProxyManager.getBudgetNotificationSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup budget/catalog mappings. 
     *
     *  @return a <code> BudgetBusinessSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetBusiness() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetBusinessSession getBudgetBusinessSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingManager.getBudgetBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup budget/catalog mappings. 
     *
     *  @param  proxy proxy 
     *  @return a <code> BudgetBusinessSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetBusiness() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetBusinessSession getBudgetBusinessSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingProxyManager.getBudgetBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning budgets 
     *  to businesses. 
     *
     *  @return a <code> BudgetBusinessAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetBusinessAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetBusinessAssignmentSession getBudgetBusinessAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingManager.getBudgetBusinessAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning budgets 
     *  to businesses. 
     *
     *  @param  proxy proxy 
     *  @return a <code> BudgetBusinessAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetBusinessAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetBusinessAssignmentSession getBudgetBusinessAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingProxyManager.getBudgetBusinessAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget smart 
     *  business service. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> BudgetSmartBusinessSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetSmartBusiness() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetSmartBusinessSession getBudgetSmartBusinessSession(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingManager.getBudgetSmartBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget smart 
     *  business service. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> BudgetSmartBusinessSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetSmartBusiness() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetSmartBusinessSession getBudgetSmartBusinessSession(org.osid.id.Id businessId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingProxyManager.getBudgetSmartBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget entry 
     *  lookup service. 
     *
     *  @return a <code> BudgetEntryLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetEntryLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryLookupSession getBudgetEntryLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingManager.getBudgetEntryLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget entry 
     *  lookup service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> BudgetEntryLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetEntryLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryLookupSession getBudgetEntryLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingProxyManager.getBudgetEntryLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget entry 
     *  lookup service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> BudgetEntryLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetEntryLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryLookupSession getBudgetEntryLookupSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingManager.getBudgetEntryLookupSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget entry 
     *  lookup service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> BudgetEntryLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetEntryLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryLookupSession getBudgetEntryLookupSessionForBusiness(org.osid.id.Id businessId, 
                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingProxyManager.getBudgetEntryLookupSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget entry 
     *  query service. 
     *
     *  @return a <code> BudgetEntryQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetEntryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryQuerySession getBudgetEntryQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingManager.getBudgetEntryQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget entry 
     *  query service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> BudgetEntryQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetEntryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryQuerySession getBudgetEntryQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingProxyManager.getBudgetEntryQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget entry 
     *  query service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> BudgetEntryQuerySession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetEntryQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryQuerySession getBudgetEntryQuerySessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingManager.getBudgetEntryQuerySessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget entry 
     *  query service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> BudgetEntryQuerySession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetEntryQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryQuerySession getBudgetEntryQuerySessionForBusiness(org.osid.id.Id businessId, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingProxyManager.getBudgetEntryQuerySessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget entry 
     *  search service. 
     *
     *  @return a <code> BudgetEntrySearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetEntrySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntrySearchSession getBudgetEntrySearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingManager.getBudgetEntrySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget entry 
     *  search service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> BudgetEntrySearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetEntrySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntrySearchSession getBudgetEntrySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingProxyManager.getBudgetEntrySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget entry 
     *  search service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> BudgetEntrySearchSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetEntrySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntrySearchSession getBudgetEntrySearchSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingManager.getBudgetEntrySearchSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget entry 
     *  search service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> BudgetEntrySearchSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetEntrySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntrySearchSession getBudgetEntrySearchSessionForBusiness(org.osid.id.Id businessId, 
                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingProxyManager.getBudgetEntrySearchSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget entry 
     *  administration service. 
     *
     *  @return a <code> BudgetEntryAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetEntryAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryAdminSession getBudgetEntryAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingManager.getBudgetEntryAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget entry 
     *  administration service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> BudgetEntryAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetEntryAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryAdminSession getBudgetEntryAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingProxyManager.getBudgetEntryAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget entry 
     *  administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> BudgetEntryAdminSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetEntryAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryAdminSession getBudgetEntryAdminSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingManager.getBudgetEntryAdminSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget entry 
     *  administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> BudgetEntryAdminSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetEntryAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryAdminSession getBudgetEntryAdminSessionForBusiness(org.osid.id.Id businessId, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingProxyManager.getBudgetEntryAdminSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget entry 
     *  notification service. 
     *
     *  @param  budgetEntryReceiver the notification callback 
     *  @return a <code> BudgetEntryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> budgetEntryReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetEntryNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryNotificationSession getBudgetEntryNotificationSession(org.osid.financials.budgeting.BudgetEntryReceiver budgetEntryReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingManager.getBudgetEntryNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget entry 
     *  notification service. 
     *
     *  @param  budgetEntryReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return a <code> BudgetEntryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> budgetEntryReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetEntryNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryNotificationSession getBudgetEntryNotificationSession(org.osid.financials.budgeting.BudgetEntryReceiver budgetEntryReceiver, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingProxyManager.getBudgetEntryNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget entry 
     *  notification service for the given business. 
     *
     *  @param  budgetEntryReceiver the notification callback 
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> BudgetEntryNotificationSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> budgetEntryReceiver 
     *          </code> or <code> businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetEntryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryNotificationSession getBudgetEntryNotificationSessionForBusiness(org.osid.financials.budgeting.BudgetEntryReceiver budgetEntryReceiver, 
                                                                                                                     org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingManager.getBudgetEntryNotificationSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget entry 
     *  notification service for the given business. 
     *
     *  @param  budgetEntryReceiver the notification callback 
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> BudgetEntryNotificationSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> budgetEntryReceiver, 
     *          businessId, </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetEntryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryNotificationSession getBudgetEntryNotificationSessionForBusiness(org.osid.financials.budgeting.BudgetEntryReceiver budgetEntryReceiver, 
                                                                                                                     org.osid.id.Id businessId, 
                                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingProxyManager.getBudgetEntryNotificationSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup entry/catalog mappings. 
     *
     *  @return a <code> BudgetEntryBusinessSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetEntryBusiness() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryBusinessSession getBudgetEntryBusinessSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingManager.getBudgetEntryBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup entry/catalog mappings. 
     *
     *  @param  proxy proxy 
     *  @return a <code> BudgetEntryCatalogSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetEntryCatalog() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryBusinessSession getBudgetEntryBusinessSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingProxyManager.getBudgetEntryBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning budget 
     *  entries to businesses. 
     *
     *  @return a <code> BudgetEntryBusinessAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetEntryBusinessAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryBusinessAssignmentSession getBudgetEntryBusinessAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingManager.getBudgetEntryBusinessAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning budget 
     *  entries to businesses. 
     *
     *  @param  proxy proxy 
     *  @return a <code> BudgetEntryCatalogAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetEntryCatalogAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryBusinessAssignmentSession getBudgetEntryBusinessAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingProxyManager.getBudgetEntryBusinessAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget entry 
     *  smart business service. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> BudgetEntrySmartBusinessSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetEntrySmartBusiness() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntrySmartBusinessSession getBudgetEntrySmartBusinessSession(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingManager.getBudgetEntrySmartBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget entry 
     *  smart business service. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> BudgetEntrySmartBusinessSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetEntrySmartBusiness() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntrySmartBusinessSession getBudgetEntrySmartBusinessSession(org.osid.id.Id businessId, 
                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingProxyManager.getBudgetEntrySmartBusinessSession not implemented");
    }


    /**
     *  Gets a <code> FinancialsBudgetingBatchManager. </code> 
     *
     *  @return a <code> FinancialsBudgetingBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFinancialsBudgetingBatch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.batch.FinancialsBudgetingBatchManager getFinancialsBudgetingBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingManager.getFinancialsBudgetingBatchManager not implemented");
    }


    /**
     *  Gets a <code> FinancialsBudgetingBatchProxyManager. </code> 
     *
     *  @return a <code> FinancialsBudgetingBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFinancialsBudgetingBatch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.batch.FinancialsBudgetingBatchProxyManager getFinancialsBudgetingBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.financials.budgeting.FinancialsBudgetingProxyManager.getFinancialsBudgetingBatchProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.budgetRecordTypes.clear();
        this.budgetRecordTypes.clear();

        this.budgetSearchRecordTypes.clear();
        this.budgetSearchRecordTypes.clear();

        this.budgetEntryRecordTypes.clear();
        this.budgetEntryRecordTypes.clear();

        this.budgetEntrySearchRecordTypes.clear();
        this.budgetEntrySearchRecordTypes.clear();

        return;
    }
}
