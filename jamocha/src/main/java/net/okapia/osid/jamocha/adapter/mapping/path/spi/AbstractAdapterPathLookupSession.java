//
// AbstractAdapterPathLookupSession.java
//
//    A Path lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.mapping.path.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Path lookup session adapter.
 */

public abstract class AbstractAdapterPathLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.mapping.path.PathLookupSession {

    private final org.osid.mapping.path.PathLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterPathLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterPathLookupSession(org.osid.mapping.path.PathLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Map/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Map Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getMapId() {
        return (this.session.getMapId());
    }


    /**
     *  Gets the {@code Map} associated with this session.
     *
     *  @return the {@code Map} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.Map getMap()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getMap());
    }


    /**
     *  Tests if this user can perform {@code Path} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupPaths() {
        return (this.session.canLookupPaths());
    }


    /**
     *  A complete view of the {@code Path} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativePathView() {
        this.session.useComparativePathView();
        return;
    }


    /**
     *  A complete view of the {@code Path} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryPathView() {
        this.session.usePlenaryPathView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include paths in maps which are children
     *  of this map in the map hierarchy.
     */

    @OSID @Override
    public void useFederatedMapView() {
        this.session.useFederatedMapView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this map only.
     */

    @OSID @Override
    public void useIsolatedMapView() {
        this.session.useIsolatedMapView();
        return;
    }
    
     
    /**
     *  Gets the {@code Path} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Path} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Path} and
     *  retained for compatibility.
     *
     *  @param pathId {@code Id} of the {@code Path}
     *  @return the path
     *  @throws org.osid.NotFoundException {@code pathId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code pathId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.Path getPath(org.osid.id.Id pathId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPath(pathId));
    }


    /**
     *  Gets a {@code PathList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  paths specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Paths} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  pathIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Path} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code pathIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.PathList getPathsByIds(org.osid.id.IdList pathIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPathsByIds(pathIds));
    }


    /**
     *  Gets a {@code PathList} corresponding to the given
     *  path genus {@code Type} which does not include
     *  paths of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  paths or an error results. Otherwise, the returned list
     *  may contain only those paths that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  pathGenusType a path genus type 
     *  @return the returned {@code Path} list
     *  @throws org.osid.NullArgumentException
     *          {@code pathGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.PathList getPathsByGenusType(org.osid.type.Type pathGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPathsByGenusType(pathGenusType));
    }


    /**
     *  Gets a {@code PathList} corresponding to the given
     *  path genus {@code Type} and include any additional
     *  paths with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  paths or an error results. Otherwise, the returned list
     *  may contain only those paths that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  pathGenusType a path genus type 
     *  @return the returned {@code Path} list
     *  @throws org.osid.NullArgumentException
     *          {@code pathGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.PathList getPathsByParentGenusType(org.osid.type.Type pathGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPathsByParentGenusType(pathGenusType));
    }


    /**
     *  Gets a {@code PathList} containing the given
     *  path record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  paths or an error results. Otherwise, the returned list
     *  may contain only those paths that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  pathRecordType a path record type 
     *  @return the returned {@code Path} list
     *  @throws org.osid.NullArgumentException
     *          {@code pathRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.PathList getPathsByRecordType(org.osid.type.Type pathRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPathsByRecordType(pathRecordType));
    }


    /**
     *  Gets a {@code PathList} connected to all the given {@code
     *  Locations.} In plenary mode, the returned list contains all of
     *  the paths along the locations, or an error results if a path
     *  connected to the location is not found or
     *  inaccessible. Otherwise, inaccessible {@code Paths} may be
     *  omitted from the list.
     *
     *  @param  locationIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Path} list 
     *  @throws org.osid.NullArgumentException {@code locationIds} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.mapping.path.PathList getPathsAlongLocations(org.osid.id.IdList locationIds)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPathsAlongLocations(locationIds));
    }


    /**
     *  Gets all {@code Paths}. 
     *
     *  In plenary mode, the returned list contains all known
     *  paths or an error results. Otherwise, the returned list
     *  may contain only those paths that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Paths} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.PathList getPaths()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPaths());
    }
}
