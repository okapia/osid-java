//
// AbstractModelSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inventory.model.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractModelSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.inventory.ModelSearchResults {

    private org.osid.inventory.ModelList models;
    private final org.osid.inventory.ModelQueryInspector inspector;
    private final java.util.Collection<org.osid.inventory.records.ModelSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractModelSearchResults.
     *
     *  @param models the result set
     *  @param modelQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>models</code>
     *          or <code>modelQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractModelSearchResults(org.osid.inventory.ModelList models,
                                            org.osid.inventory.ModelQueryInspector modelQueryInspector) {
        nullarg(models, "models");
        nullarg(modelQueryInspector, "model query inspectpr");

        this.models = models;
        this.inspector = modelQueryInspector;

        return;
    }


    /**
     *  Gets the model list resulting from a search.
     *
     *  @return a model list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.inventory.ModelList getModels() {
        if (this.models == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.inventory.ModelList models = this.models;
        this.models = null;
	return (models);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.inventory.ModelQueryInspector getModelQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  model search record <code> Type. </code> This method must
     *  be used to retrieve a model implementing the requested
     *  record.
     *
     *  @param modelSearchRecordType a model search 
     *         record type 
     *  @return the model search
     *  @throws org.osid.NullArgumentException
     *          <code>modelSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(modelSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inventory.records.ModelSearchResultsRecord getModelSearchResultsRecord(org.osid.type.Type modelSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.inventory.records.ModelSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(modelSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(modelSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record model search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addModelRecord(org.osid.inventory.records.ModelSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "model record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
