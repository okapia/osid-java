//
// AbstractAdapterModelLookupSession.java
//
//    A Model lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.inventory.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Model lookup session adapter.
 */

public abstract class AbstractAdapterModelLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.inventory.ModelLookupSession {

    private final org.osid.inventory.ModelLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterModelLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterModelLookupSession(org.osid.inventory.ModelLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Warehouse/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Warehouse Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getWarehouseId() {
        return (this.session.getWarehouseId());
    }


    /**
     *  Gets the {@code Warehouse} associated with this session.
     *
     *  @return the {@code Warehouse} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.Warehouse getWarehouse()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getWarehouse());
    }


    /**
     *  Tests if this user can perform {@code Model} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupModels() {
        return (this.session.canLookupModels());
    }


    /**
     *  A complete view of the {@code Model} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeModelView() {
        this.session.useComparativeModelView();
        return;
    }


    /**
     *  A complete view of the {@code Model} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryModelView() {
        this.session.usePlenaryModelView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include models in warehouses which are children
     *  of this warehouse in the warehouse hierarchy.
     */

    @OSID @Override
    public void useFederatedWarehouseView() {
        this.session.useFederatedWarehouseView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this warehouse only.
     */

    @OSID @Override
    public void useIsolatedWarehouseView() {
        this.session.useIsolatedWarehouseView();
        return;
    }
    
     
    /**
     *  Gets the {@code Model} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Model} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Model} and
     *  retained for compatibility.
     *
     *  @param modelId {@code Id} of the {@code Model}
     *  @return the model
     *  @throws org.osid.NotFoundException {@code modelId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code modelId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.Model getModel(org.osid.id.Id modelId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getModel(modelId));
    }


    /**
     *  Gets a {@code ModelList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  models specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Models} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  modelIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Model} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code modelIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.ModelList getModelsByIds(org.osid.id.IdList modelIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getModelsByIds(modelIds));
    }


    /**
     *  Gets a {@code ModelList} corresponding to the given
     *  model genus {@code Type} which does not include
     *  models of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  models or an error results. Otherwise, the returned list
     *  may contain only those models that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  modelGenusType a model genus type 
     *  @return the returned {@code Model} list
     *  @throws org.osid.NullArgumentException
     *          {@code modelGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.ModelList getModelsByGenusType(org.osid.type.Type modelGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getModelsByGenusType(modelGenusType));
    }


    /**
     *  Gets a {@code ModelList} corresponding to the given
     *  model genus {@code Type} and include any additional
     *  models with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  models or an error results. Otherwise, the returned list
     *  may contain only those models that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  modelGenusType a model genus type 
     *  @return the returned {@code Model} list
     *  @throws org.osid.NullArgumentException
     *          {@code modelGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.ModelList getModelsByParentGenusType(org.osid.type.Type modelGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getModelsByParentGenusType(modelGenusType));
    }


    /**
     *  Gets a {@code ModelList} containing the given
     *  model record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  models or an error results. Otherwise, the returned list
     *  may contain only those models that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  modelRecordType a model record type 
     *  @return the returned {@code Model} list
     *  @throws org.osid.NullArgumentException
     *          {@code modelRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.ModelList getModelsByRecordType(org.osid.type.Type modelRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getModelsByRecordType(modelRecordType));
    }


    /**
     *  Gets a {@code ModelList} for the given resource {@code Id}.
     *  In plenary mode, the returned list contains all known models
     *  or an error results. Otherwise, the returned list may contain
     *  only those models that are accessible through this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Model} list 
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inventory.ModelList getModelsByManufacturer(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getModelsByManufacturer(resourceId));
    }


    /**
     *  Gets all {@code Models}. 
     *
     *  In plenary mode, the returned list contains all known
     *  models or an error results. Otherwise, the returned list
     *  may contain only those models that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Models} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.ModelList getModels()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getModels());
    }
}
