//
// HoldEnablerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.hold.rules.holdenabler;


/**
 *  A template for implementing a search results.
 */

public final class HoldEnablerSearchResults
    extends net.okapia.osid.jamocha.hold.rules.holdenabler.spi.AbstractHoldEnablerSearchResults
    implements org.osid.hold.rules.HoldEnablerSearchResults {


    /**
     *  Constructs a new <code>HoldEnablerSearchResults.
     *
     *  @param holdEnablers the result set
     *  @param holdEnablerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>holdEnablers</code>
     *          or <code>holdEnablerQueryInspector</code> is
     *          <code>null</code>
     */

    public HoldEnablerSearchResults(org.osid.hold.rules.HoldEnablerList holdEnablers,
                                 org.osid.hold.rules.HoldEnablerQueryInspector holdEnablerQueryInspector) {
        super(holdEnablers, holdEnablerQueryInspector);
        return;
    }
}
