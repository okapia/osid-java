//
// AssessmentEntryElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.chronicle.assessmententry.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class AssessmentEntryElements
    extends net.okapia.osid.jamocha.spi.OsidRelationshipElements {


    /**
     *  Gets the AssessmentEntryElement Id.
     *
     *  @return the assessment entry element Id
     */

    public static org.osid.id.Id getAssessmentEntryEntityId() {
        return (makeEntityId("osid.course.chronicle.AssessmentEntry"));
    }


    /**
     *  Gets the StudentId element Id.
     *
     *  @return the StudentId element Id
     */

    public static org.osid.id.Id getStudentId() {
        return (makeElementId("osid.course.chronicle.assessmententry.StudentId"));
    }


    /**
     *  Gets the Student element Id.
     *
     *  @return the Student element Id
     */

    public static org.osid.id.Id getStudent() {
        return (makeElementId("osid.course.chronicle.assessmententry.Student"));
    }


    /**
     *  Gets the AssessmentId element Id.
     *
     *  @return the AssessmentId element Id
     */

    public static org.osid.id.Id getAssessmentId() {
        return (makeElementId("osid.course.chronicle.assessmententry.AssessmentId"));
    }


    /**
     *  Gets the Assessment element Id.
     *
     *  @return the Assessment element Id
     */

    public static org.osid.id.Id getAssessment() {
        return (makeElementId("osid.course.chronicle.assessmententry.Assessment"));
    }


    /**
     *  Gets the DateCompleted element Id.
     *
     *  @return the DateCompleted element Id
     */

    public static org.osid.id.Id getDateCompleted() {
        return (makeElementId("osid.course.chronicle.assessmententry.DateCompleted"));
    }


    /**
     *  Gets the ProgramId element Id.
     *
     *  @return the ProgramId element Id
     */

    public static org.osid.id.Id getProgramId() {
        return (makeElementId("osid.course.chronicle.assessmententry.ProgramId"));
    }


    /**
     *  Gets the Program element Id.
     *
     *  @return the Program element Id
     */

    public static org.osid.id.Id getProgram() {
        return (makeElementId("osid.course.chronicle.assessmententry.Program"));
    }


    /**
     *  Gets the CourseId element Id.
     *
     *  @return the CourseId element Id
     */

    public static org.osid.id.Id getCourseId() {
        return (makeElementId("osid.course.chronicle.assessmententry.CourseId"));
    }


    /**
     *  Gets the Course element Id.
     *
     *  @return the Course element Id
     */

    public static org.osid.id.Id getCourse() {
        return (makeElementId("osid.course.chronicle.assessmententry.Course"));
    }


    /**
     *  Gets the GradeId element Id.
     *
     *  @return the GradeId element Id
     */

    public static org.osid.id.Id getGradeId() {
        return (makeElementId("osid.course.chronicle.assessmententry.GradeId"));
    }


    /**
     *  Gets the Grade element Id.
     *
     *  @return the Grade element Id
     */

    public static org.osid.id.Id getGrade() {
        return (makeElementId("osid.course.chronicle.assessmententry.Grade"));
    }


    /**
     *  Gets the ScoreScaleId element Id.
     *
     *  @return the ScoreScaleId element Id
     */

    public static org.osid.id.Id getScoreScaleId() {
        return (makeElementId("osid.course.chronicle.assessmententry.ScoreScaleId"));
    }


    /**
     *  Gets the ScoreScale element Id.
     *
     *  @return the ScoreScale element Id
     */

    public static org.osid.id.Id getScoreScale() {
        return (makeElementId("osid.course.chronicle.assessmententry.ScoreScale"));
    }


    /**
     *  Gets the Score element Id.
     *
     *  @return the Score element Id
     */

    public static org.osid.id.Id getScore() {
        return (makeElementId("osid.course.chronicle.assessmententry.Score"));
    }


    /**
     *  Gets the CourseCatalogId element Id.
     *
     *  @return the CourseCatalogId element Id
     */

    public static org.osid.id.Id getCourseCatalogId() {
        return (makeQueryElementId("osid.course.chronicle.assessmententry.CourseCatalogId"));
    }


    /**
     *  Gets the CourseCatalog element Id.
     *
     *  @return the CourseCatalog element Id
     */

    public static org.osid.id.Id getCourseCatalog() {
        return (makeQueryElementId("osid.course.chronicle.assessmententry.CourseCatalog"));
    }
}
