//
// AbstractPriceEnablerQuery.java
//
//     A template for making a PriceEnabler Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ordering.rules.priceenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for price enablers.
 */

public abstract class AbstractPriceEnablerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQuery
    implements org.osid.ordering.rules.PriceEnablerQuery {

    private final java.util.Collection<org.osid.ordering.rules.records.PriceEnablerQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches enablers mapped to the price. 
     *
     *  @param  priceId the price <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> priceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledPriceId(org.osid.id.Id priceId, boolean match) {
        return;
    }


    /**
     *  Clears the price <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledPriceIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> PriceQuery </code> is available. 
     *
     *  @return <code> true </code> if a price query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledPriceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a price. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the price query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledPriceQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.PriceQuery getRuledPriceQuery() {
        throw new org.osid.UnimplementedException("supportsRuledPriceQuery() is false");
    }


    /**
     *  Matches enablers mapped to any price. 
     *
     *  @param  match <code> true </code> for enablers mapped to any price, 
     *          <code> false </code> to match enablers mapped to no price 
     */

    @OSID @Override
    public void matchAnyRuledPrice(boolean match) {
        return;
    }


    /**
     *  Clears the price query terms. 
     */

    @OSID @Override
    public void clearRuledPriceTerms() {
        return;
    }


    /**
     *  Matches enablers mapped to the store. 
     *
     *  @param  storeId the store <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> storeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchStoreId(org.osid.id.Id storeId, boolean match) {
        return;
    }


    /**
     *  Clears the store <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearStoreIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> StoreQuery </code> is available. 
     *
     *  @return <code> true </code> if a store query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStoreQuery() {
        return (false);
    }


    /**
     *  Gets the query for a store. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the store query 
     *  @throws org.osid.UnimplementedException <code> supportsStoreQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.StoreQuery getStoreQuery() {
        throw new org.osid.UnimplementedException("supportsStoreQuery() is false");
    }


    /**
     *  Clears the store query terms. 
     */

    @OSID @Override
    public void clearStoreTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given price enabler query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a price enabler implementing the requested record.
     *
     *  @param priceEnablerRecordType a price enabler record type
     *  @return the price enabler query record
     *  @throws org.osid.NullArgumentException
     *          <code>priceEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(priceEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.ordering.rules.records.PriceEnablerQueryRecord getPriceEnablerQueryRecord(org.osid.type.Type priceEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ordering.rules.records.PriceEnablerQueryRecord record : this.records) {
            if (record.implementsRecordType(priceEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(priceEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this price enabler query. 
     *
     *  @param priceEnablerQueryRecord price enabler query record
     *  @param priceEnablerRecordType priceEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addPriceEnablerQueryRecord(org.osid.ordering.rules.records.PriceEnablerQueryRecord priceEnablerQueryRecord, 
                                          org.osid.type.Type priceEnablerRecordType) {

        addRecordType(priceEnablerRecordType);
        nullarg(priceEnablerQueryRecord, "price enabler query record");
        this.records.add(priceEnablerQueryRecord);        
        return;
    }
}
