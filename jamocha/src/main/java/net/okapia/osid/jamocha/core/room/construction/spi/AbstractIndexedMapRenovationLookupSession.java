//
// AbstractIndexedMapRenovationLookupSession.java
//
//    A simple framework for providing a Renovation lookup service
//    backed by a fixed collection of renovations with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.room.construction.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Renovation lookup service backed by a
 *  fixed collection of renovations. The renovations are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some renovations may be compatible
 *  with more types than are indicated through these renovation
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Renovations</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapRenovationLookupSession
    extends AbstractMapRenovationLookupSession
    implements org.osid.room.construction.RenovationLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.room.construction.Renovation> renovationsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.room.construction.Renovation>());
    private final MultiMap<org.osid.type.Type, org.osid.room.construction.Renovation> renovationsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.room.construction.Renovation>());


    /**
     *  Makes a <code>Renovation</code> available in this session.
     *
     *  @param  renovation a renovation
     *  @throws org.osid.NullArgumentException <code>renovation<code> is
     *          <code>null</code>
     */

    @Override
    protected void putRenovation(org.osid.room.construction.Renovation renovation) {
        super.putRenovation(renovation);

        this.renovationsByGenus.put(renovation.getGenusType(), renovation);
        
        try (org.osid.type.TypeList types = renovation.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.renovationsByRecord.put(types.getNextType(), renovation);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a renovation from this session.
     *
     *  @param renovationId the <code>Id</code> of the renovation
     *  @throws org.osid.NullArgumentException <code>renovationId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeRenovation(org.osid.id.Id renovationId) {
        org.osid.room.construction.Renovation renovation;
        try {
            renovation = getRenovation(renovationId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.renovationsByGenus.remove(renovation.getGenusType());

        try (org.osid.type.TypeList types = renovation.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.renovationsByRecord.remove(types.getNextType(), renovation);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeRenovation(renovationId);
        return;
    }


    /**
     *  Gets a <code>RenovationList</code> corresponding to the given
     *  renovation genus <code>Type</code> which does not include
     *  renovations of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known renovations or an error results. Otherwise,
     *  the returned list may contain only those renovations that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  renovationGenusType a renovation genus type 
     *  @return the returned <code>Renovation</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>renovationGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsByGenusType(org.osid.type.Type renovationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.room.construction.renovation.ArrayRenovationList(this.renovationsByGenus.get(renovationGenusType)));
    }


    /**
     *  Gets a <code>RenovationList</code> containing the given
     *  renovation record <code>Type</code>. In plenary mode, the
     *  returned list contains all known renovations or an error
     *  results. Otherwise, the returned list may contain only those
     *  renovations that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  renovationRecordType a renovation record type 
     *  @return the returned <code>renovation</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>renovationRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsByRecordType(org.osid.type.Type renovationRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.room.construction.renovation.ArrayRenovationList(this.renovationsByRecord.get(renovationRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.renovationsByGenus.clear();
        this.renovationsByRecord.clear();

        super.close();

        return;
    }
}
