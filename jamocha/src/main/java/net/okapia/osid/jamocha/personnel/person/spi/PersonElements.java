//
// PersonElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.personnel.person.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class PersonElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the PersonElement Id.
     *
     *  @return the person element Id
     */

    public static org.osid.id.Id getPersonEntityId() {
        return (makeEntityId("osid.personnel.Person"));
    }


    /**
     *  Gets the Salutation element Id.
     *
     *  @return the Salutation element Id
     */

    public static org.osid.id.Id getSalutation() {
        return (makeElementId("osid.personnel.person.Salutation"));
    }


    /**
     *  Gets the GivenName element Id.
     *
     *  @return the GivenName element Id
     */

    public static org.osid.id.Id getGivenName() {
        return (makeElementId("osid.personnel.person.GivenName"));
    }


    /**
     *  Gets the PreferredName element Id.
     *
     *  @return the PreferredName element Id
     */

    public static org.osid.id.Id getPreferredName() {
        return (makeElementId("osid.personnel.person.PreferredName"));
    }


    /**
     *  Gets the ForenameAliases element Id.
     *
     *  @return the ForenameAliases element Id
     */

    public static org.osid.id.Id getForenameAliases() {
        return (makeElementId("osid.personnel.person.ForenameAliases"));
    }


    /**
     *  Gets the MiddleNames element Id.
     *
     *  @return the MiddleNames element Id
     */

    public static org.osid.id.Id getMiddleNames() {
        return (makeElementId("osid.personnel.person.MiddleNames"));
    }


    /**
     *  Gets the Surname element Id.
     *
     *  @return the Surname element Id
     */

    public static org.osid.id.Id getSurname() {
        return (makeElementId("osid.personnel.person.Surname"));
    }


    /**
     *  Gets the SurnameAliases element Id.
     *
     *  @return the SurnameAliases element Id
     */

    public static org.osid.id.Id getSurnameAliases() {
        return (makeElementId("osid.personnel.person.SurnameAliases"));
    }


    /**
     *  Gets the GenerationQualifier element Id.
     *
     *  @return the GenerationQualifier element Id
     */

    public static org.osid.id.Id getGenerationQualifier() {
        return (makeElementId("osid.personnel.person.GenerationQualifier"));
    }


    /**
     *  Gets the QualificationSuffix element Id.
     *
     *  @return the QualificationSuffix element Id
     */

    public static org.osid.id.Id getQualificationSuffix() {
        return (makeElementId("osid.personnel.person.QualificationSuffix"));
    }


    /**
     *  Gets the BirthDate element Id.
     *
     *  @return the BirthDate element Id
     */

    public static org.osid.id.Id getBirthDate() {
        return (makeElementId("osid.personnel.person.BirthDate"));
    }


    /**
     *  Gets the DeathDate element Id.
     *
     *  @return the DeathDate element Id
     */

    public static org.osid.id.Id getDeathDate() {
        return (makeElementId("osid.personnel.person.DeathDate"));
    }


    /**
     *  Gets the InstitutionalIdentifier element Id.
     *
     *  @return the InstitutionalIdentifier element Id
     */

    public static org.osid.id.Id getInstitutionalIdentifier() {
        return (makeElementId("osid.personnel.person.InstitutionalIdentifier"));
    }


    /**
     *  Gets the LivingDate element Id.
     *
     *  @return the LivingDate element Id
     */

    public static org.osid.id.Id getLivingDate() {
        return (makeQueryElementId("osid.personnel.person.LivingDate"));
    }


    /**
     *  Gets the RealmId element Id.
     *
     *  @return the RealmId element Id
     */

    public static org.osid.id.Id getRealmId() {
        return (makeQueryElementId("osid.personnel.person.RealmId"));
    }


    /**
     *  Gets the Realm element Id.
     *
     *  @return the Realm element Id
     */

    public static org.osid.id.Id getRealm() {
        return (makeQueryElementId("osid.personnel.person.Realm"));
    }
}
