//
// AbstractFederatingStateLookupSession.java
//
//     An abstract federating adapter for a StateLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.process.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  StateLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingStateLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.process.StateLookupSession>
    implements org.osid.process.StateLookupSession {

    private boolean parallel = false;
    private org.osid.process.Process process = new net.okapia.osid.jamocha.nil.process.process.UnknownProcess();


    /**
     *  Constructs a new <code>AbstractFederatingStateLookupSession</code>.
     */

    protected AbstractFederatingStateLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.process.StateLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Process/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Process Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getProcessId() {
        return (this.process.getId());
    }


    /**
     *  Gets the <code>Process</code> associated with this 
     *  session.
     *
     *  @return the <code>Process</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.process.Process getProcess()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.process);
    }


    /**
     *  Sets the <code>Process</code>.
     *
     *  @param  process the process for this session
     *  @throws org.osid.NullArgumentException <code>process</code>
     *          is <code>null</code>
     */

    protected void setProcess(org.osid.process.Process process) {
        nullarg(process, "process");
        this.process = process;
        return;
    }


    /**
     *  Tests if this user can perform <code>State</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupStates() {
        for (org.osid.process.StateLookupSession session : getSessions()) {
            if (session.canLookupStates()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>State</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeStateView() {
        for (org.osid.process.StateLookupSession session : getSessions()) {
            session.useComparativeStateView();
        }

        return;
    }


    /**
     *  A complete view of the <code>State</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryStateView() {
        for (org.osid.process.StateLookupSession session : getSessions()) {
            session.usePlenaryStateView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include states in processes which are children
     *  of this process in the process hierarchy.
     */

    @OSID @Override
    public void useFederatedProcessView() {
        for (org.osid.process.StateLookupSession session : getSessions()) {
            session.useFederatedProcessView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this process only.
     */

    @OSID @Override
    public void useIsolatedProcessView() {
        for (org.osid.process.StateLookupSession session : getSessions()) {
            session.useIsolatedProcessView();
        }

        return;
    }

     
    /**
     *  Gets the <code>State</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>State</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>State</code> and
     *  retained for compatibility.
     *
     *  @param  stateId <code>Id</code> of the
     *          <code>State</code>
     *  @return the state
     *  @throws org.osid.NotFoundException <code>stateId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>stateId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.process.State getState(org.osid.id.Id stateId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.process.StateLookupSession session : getSessions()) {
            try {
                return (session.getState(stateId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(stateId + " not found");
    }


    /**
     *  Gets a <code>StateList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  states specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>States</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  stateIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>State</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>stateIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.process.StateList getStatesByIds(org.osid.id.IdList stateIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.process.state.MutableStateList ret = new net.okapia.osid.jamocha.process.state.MutableStateList();

        try (org.osid.id.IdList ids = stateIds) {
            while (ids.hasNext()) {
                ret.addState(getState(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>StateList</code> corresponding to the given
     *  state genus <code>Type</code> which does not include
     *  states of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  states or an error results. Otherwise, the returned list
     *  may contain only those states that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  stateGenusType a state genus type 
     *  @return the returned <code>State</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stateGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.process.StateList getStatesByGenusType(org.osid.type.Type stateGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.process.state.FederatingStateList ret = getStateList();

        for (org.osid.process.StateLookupSession session : getSessions()) {
            ret.addStateList(session.getStatesByGenusType(stateGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>StateList</code> corresponding to the given
     *  state genus <code>Type</code> and include any additional
     *  states with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  states or an error results. Otherwise, the returned list
     *  may contain only those states that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  stateGenusType a state genus type 
     *  @return the returned <code>State</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stateGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.process.StateList getStatesByParentGenusType(org.osid.type.Type stateGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.process.state.FederatingStateList ret = getStateList();

        for (org.osid.process.StateLookupSession session : getSessions()) {
            ret.addStateList(session.getStatesByParentGenusType(stateGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>StateList</code> containing the given
     *  state record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  states or an error results. Otherwise, the returned list
     *  may contain only those states that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  stateRecordType a state record type 
     *  @return the returned <code>State</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stateRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.process.StateList getStatesByRecordType(org.osid.type.Type stateRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.process.state.FederatingStateList ret = getStateList();

        for (org.osid.process.StateLookupSession session : getSessions()) {
            ret.addStateList(session.getStatesByRecordType(stateRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>States</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  states or an error results. Otherwise, the returned list
     *  may contain only those states that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>States</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.process.StateList getStates()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.process.state.FederatingStateList ret = getStateList();

        for (org.osid.process.StateLookupSession session : getSessions()) {
            ret.addStateList(session.getStates());
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets the next valid states for the given state.
     *
     *  @param  stateId a state <code>Id</code>
     *  @return the valid next <code>States</code>
     *  @throws org.osid.NotFoundException <code>stateId</code> is not found
     *  @throws org.osid.NullArgumentException <code>stateId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.process.StateList getValidNextStates(org.osid.id.Id stateId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException { 

        net.okapia.osid.jamocha.adapter.federator.process.state.FederatingStateList ret = getStateList();

        for (org.osid.process.StateLookupSession session : getSessions()) {
            ret.addStateList(session.getValidNextStates(stateId));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.process.state.FederatingStateList getStateList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.process.state.ParallelStateList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.process.state.CompositeStateList());
        }
    }
}
