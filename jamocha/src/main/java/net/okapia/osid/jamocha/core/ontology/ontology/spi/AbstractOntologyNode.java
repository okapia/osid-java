//
// AbstractOntology.java
//
//     Defines an OntologyNode within an in core hierarchy.
//
//
// Tom Coppeto
// Okapia
// 8 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.ontology.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract class for managing a hierarchy of ontology
 *  nodes in core.
 */

public abstract class AbstractOntologyNode
    extends net.okapia.osid.jamocha.spi.AbstractOsidNode
    implements org.osid.ontology.OntologyNode,
               org.osid.hierarchy.Node {

    private final org.osid.ontology.Ontology ontology;
    private final java.util.Collection<org.osid.ontology.OntologyNode> parents  = new java.util.HashSet<org.osid.ontology.OntologyNode>();
    private final java.util.Collection<org.osid.ontology.OntologyNode> children = new java.util.HashSet<org.osid.ontology.OntologyNode>();


    /**
     *  Constructs a new <code>AbstractOntologyNode</code> from a
     *  single ontology.
     *
     *  @param ontology the ontology
     *  @throws org.osid.NullArgumentException <code>ontology</code> is 
     *          <code>null</code>.
     */

    protected AbstractOntologyNode(org.osid.ontology.Ontology ontology) {
        setId(ontology.getId());
        this.ontology = ontology;
        return;
    }


    /**
     *  Constructs a new <code>AbstractOntologyNode</code> from a
     *  single ontology.
     *
     *  @param ontology the ontology
     *  @param root <code>true</code> if this node is a root, 
     *         <code>false</code> otherwise
     *  @param leaf <code>true</code> if this node is a leaf, 
     *         <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException <code>ontology</code> is 
     *          <code>null</code>.
     */

    protected AbstractOntologyNode(org.osid.ontology.Ontology ontology, boolean root, boolean leaf) {
        this(ontology);

        if (root) {
            root();
        } else {
            unroot();
        }

        if (leaf) {
            leaf();
        } else {
            unleaf();
        }
        
        return;
    }


    /**
     *  Adds a parent to this ontology.
     *
     *  @param node the parent ontology node to add
     *  @throws org.osid.IllegalStateException this is a root
     *  @throws org.osid.NullArgumentException <code>node</code> is
     *          <code>null</code>
     */

    protected void addParent(org.osid.ontology.OntologyNode node) {
        nullarg(node, "node");

        if (isRoot()) {
            throw new org.osid.IllegalStateException(getId() + " is a root");
        }

        this.parents.add(node);
        return;
    }


    /**
     *  Adds a child to this ontology.
     *
     *  @param node the child ontology node to add
     *  @throws org.osid.NullArgumentException <code>node</code> is
     *          <code>null</code>
     */

    public void addChild(org.osid.ontology.OntologyNode node) {
        nullarg(node, "ontology node");
        this.children.add(node);
        return;
    }


    /**
     *  Gets the <code> Ontology </code> at this node.
     *
     *  @return the ontology represented by this node
     */

    @OSID @Override
    public org.osid.ontology.Ontology getOntology() {
        return (this.ontology);
    }


    /**
     *  Tests if any parents are available in this node structure. There may 
     *  be no more parents in this node structure however there may be parents 
     *  that exist in the hierarchy. 
     *
     *  @return <code> true </code> if this node has parents, <code> false
     *          </code> otherwise
     */

    @OSID @Override
    public boolean hasParents() {
        return (this.parents.size() > 0);
    }


    /**
     *  Tests if any children are available in this node structure. There may 
     *  be no more children available in this node structure but this node may 
     *  have children in the hierarchy. 
     *
     *  @return <code> true </code> if this node has children, <code>
     *          false </code> otherwise
     */
    
    @OSID @Override
    public boolean hasChildren() {
        return (this.children.size() > 0);
    }


    /**
     *  Gets the parents of this node.
     *
     *  @return the parents of this node
     */

    @OSID @Override
    public org.osid.id.IdList getParentIds() {
        return (new net.okapia.osid.jamocha.adapter.converter.ontology.ontologynode.OntologyNodeToIdList(this.parents));
    }


    /**
     *  Gets the parents of this node.
     *
     *  @return the parents of the <code> id </code>
     */

    @OSID @Override
    public org.osid.hierarchy.NodeList getParents() {
        return (new net.okapia.osid.jamocha.adapter.converter.ontology.ontologynode.OntologyNodeToNodeList(getParentOntologyNodes()));
    }


    /**
     *  Gets the parents of this node.
     *
     *  @return the parents of the <code> id </code>
     */

    @OSID @Override
    public org.osid.ontology.OntologyNodeList getParentOntologyNodes() {
        return (new net.okapia.osid.jamocha.ontology.ontologynode.ArrayOntologyNodeList(this.parents));
    }


    /**
     *  Gets the children of this node.
     *
     *  @return the children of this node
     */

    @OSID @Override
    public org.osid.id.IdList getChildIds() {
        return (new net.okapia.osid.jamocha.adapter.converter.ontology.ontologynode.OntologyNodeToIdList(this.children));
    }


    /**
     *  Gets the children of this node.
     *
     *  @return the children of the <code> id </code>
     */

    @OSID @Override
    public org.osid.hierarchy.NodeList getChildren() {
        return (new net.okapia.osid.jamocha.adapter.converter.ontology.ontologynode.OntologyNodeToNodeList(getChildOntologyNodes()));
    }


    /**
     *  Gets the child nodes of this ontology.
     *
     *  @return the child nodes of this ontology
     */

    @OSID @Override
    public org.osid.ontology.OntologyNodeList getChildOntologyNodes() {
        return (new net.okapia.osid.jamocha.ontology.ontologynode.ArrayOntologyNodeList(this.children));
    }
}
