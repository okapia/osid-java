//
// CompositionMiter.java
//
//     Defines a Composition miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.repository.composition;


/**
 *  Defines a <code>Composition</code> miter for use with the builders.
 */

public interface CompositionMiter
    extends net.okapia.osid.jamocha.builder.spi.SourceableOsidObjectMiter,
            net.okapia.osid.jamocha.builder.spi.OperableMiter,
            net.okapia.osid.jamocha.builder.spi.ContainableMiter,
            org.osid.repository.Composition {

    
    /**
     *  Adds a child.
     *
     *  @param child a child
     *  @throws org.osid.NullArgumentException <code>child</code> is
     *          <code>null</code>
     */

    public void addChild(org.osid.repository.Composition child);


    /**
     *  Sets all the children.
     *
     *  @param children a collection of children
     *  @throws org.osid.NullArgumentException <code>children</code>
     *          is <code>null</code>
     */

    public void setChildren(java.util.Collection<org.osid.repository.Composition> children);


    /**
     *  Adds a Composition record.
     *
     *  @param record a composition record
     *  @param recordType the type of composition record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addCompositionRecord(org.osid.repository.records.CompositionRecord record, org.osid.type.Type recordType);
}       


