//
// AbstractAssemblyPlanQuery.java
//
//     A PlanQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.course.plan.plan.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A PlanQuery that stores terms.
 */

public abstract class AbstractAssemblyPlanQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRelationshipQuery
    implements org.osid.course.plan.PlanQuery,
               org.osid.course.plan.PlanQueryInspector,
               org.osid.course.plan.PlanSearchOrder {

    private final java.util.Collection<org.osid.course.plan.records.PlanQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.plan.records.PlanQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.plan.records.PlanSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyPlanQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyPlanQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the syllabus <code> Id </code> for this query. 
     *
     *  @param  syllabusId a syllabus <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> syllabusId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSyllabusId(org.osid.id.Id syllabusId, boolean match) {
        getAssembler().addIdTerm(getSyllabusIdColumn(), syllabusId, match);
        return;
    }


    /**
     *  Clears the syllabus <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSyllabusIdTerms() {
        getAssembler().clearTerms(getSyllabusIdColumn());
        return;
    }


    /**
     *  Gets the syllabus <code> Id </code> terms. 
     *
     *  @return the syllabus <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSyllabusIdTerms() {
        return (getAssembler().getIdTerms(getSyllabusIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the syllabus. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySyllabus(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getSyllabusColumn(), style);
        return;
    }


    /**
     *  Gets the SyllabusId column name.
     *
     * @return the column name
     */

    protected String getSyllabusIdColumn() {
        return ("syllabus_id");
    }


    /**
     *  Tests if a syllabus query is available. 
     *
     *  @return <code> true </code> if a syllabus query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSyllabusQuery() {
        return (false);
    }


    /**
     *  Gets the query for a lesson. 
     *
     *  @return the syllabus query 
     *  @throws org.osid.UnimplementedException <code> supportsSyllabusQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusQuery getSyllabusQuery() {
        throw new org.osid.UnimplementedException("supportsSyllabusQuery() is false");
    }


    /**
     *  Clears the syllabus terms. 
     */

    @OSID @Override
    public void clearSyllabusTerms() {
        getAssembler().clearTerms(getSyllabusColumn());
        return;
    }


    /**
     *  Gets the syllabus terms. 
     *
     *  @return the syllabus terms 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusQueryInspector[] getSyllabusTerms() {
        return (new org.osid.course.syllabus.SyllabusQueryInspector[0]);
    }


    /**
     *  Tests if a syllabus order is available. 
     *
     *  @return <code> true </code> if a syllabus order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSyllabusSearchOrder() {
        return (false);
    }


    /**
     *  Gets the syllabus order. 
     *
     *  @return the syllabus search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSyllabusSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusSearchOrder getSyllabusSearchOrder() {
        throw new org.osid.UnimplementedException("supportsSyllabusSearchOrder() is false");
    }


    /**
     *  Gets the Syllabus column name.
     *
     * @return the column name
     */

    protected String getSyllabusColumn() {
        return ("syllabus");
    }


    /**
     *  Sets the course offering <code> Id </code> for this query. 
     *
     *  @param  courseOfferingId a course offering <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> syllabusId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCourseOfferingId(org.osid.id.Id courseOfferingId, 
                                      boolean match) {
        getAssembler().addIdTerm(getCourseOfferingIdColumn(), courseOfferingId, match);
        return;
    }


    /**
     *  Clears the course offering <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseOfferingIdTerms() {
        getAssembler().clearTerms(getCourseOfferingIdColumn());
        return;
    }


    /**
     *  Gets the course offering <code> Id </code> terms. 
     *
     *  @return the course offering <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseOfferingIdTerms() {
        return (getAssembler().getIdTerms(getCourseOfferingIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the course 
     *  offering. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCourseOffering(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCourseOfferingColumn(), style);
        return;
    }


    /**
     *  Gets the CourseOfferingId column name.
     *
     * @return the column name
     */

    protected String getCourseOfferingIdColumn() {
        return ("course_offering_id");
    }


    /**
     *  Tests if a course offering query is available. 
     *
     *  @return <code> true </code> if a course offering query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseOfferingQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course offering. 
     *
     *  @return the course offering query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingQuery getCourseOfferingQuery() {
        throw new org.osid.UnimplementedException("supportsCourseOfferingQuery() is false");
    }


    /**
     *  Clears the syllabus terms. 
     */

    @OSID @Override
    public void clearCourseOfferingTerms() {
        getAssembler().clearTerms(getCourseOfferingColumn());
        return;
    }


    /**
     *  Gets the course offering terms. 
     *
     *  @return the course offering terms 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingQueryInspector[] getCourseOfferingTerms() {
        return (new org.osid.course.CourseOfferingQueryInspector[0]);
    }


    /**
     *  Tests if a course offering order is available. 
     *
     *  @return <code> true </code> if a course offering order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseOfferingSearchOrder() {
        return (false);
    }


    /**
     *  Gets the course offering order. 
     *
     *  @return the course offering search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingSearchOrder getCourseOfferingSearchOrder() {
        throw new org.osid.UnimplementedException("supportsCourseOfferingSearchOrder() is false");
    }


    /**
     *  Gets the CourseOffering column name.
     *
     * @return the column name
     */

    protected String getCourseOfferingColumn() {
        return ("course_offering");
    }


    /**
     *  Sets a module <code> Id. </code> 
     *
     *  @param  moduleId a module <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> moduleId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchModuleId(org.osid.id.Id moduleId, boolean match) {
        getAssembler().addIdTerm(getModuleIdColumn(), moduleId, match);
        return;
    }


    /**
     *  Clears the module <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearModuleIdTerms() {
        getAssembler().clearTerms(getModuleIdColumn());
        return;
    }


    /**
     *  Gets the module <code> Id </code> terms. 
     *
     *  @return the module <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getModuleIdTerms() {
        return (getAssembler().getIdTerms(getModuleIdColumn()));
    }


    /**
     *  Gets the ModuleId column name.
     *
     * @return the column name
     */

    protected String getModuleIdColumn() {
        return ("module_id");
    }


    /**
     *  Gets the query for a module query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the lesson query 
     *  @throws org.osid.UnimplementedException <code> supportsModuleQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleQuery getModuleQuery() {
        throw new org.osid.UnimplementedException("supportsModuleQuery() is false");
    }


    /**
     *  Matches plans with any module. 
     *
     *  @param  match <code> true </code> to match pans with any module, 
     *          <code> false </code> to match plans with no modules 
     */

    @OSID @Override
    public void matchAnyModule(boolean match) {
        getAssembler().addIdWildcardTerm(getModuleColumn(), match);
        return;
    }


    /**
     *  Clears the module terms. 
     */

    @OSID @Override
    public void clearModuleTerms() {
        getAssembler().clearTerms(getModuleColumn());
        return;
    }


    /**
     *  Gets the module terms. 
     *
     *  @return the module terms 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleQueryInspector[] getModuleTerms() {
        return (new org.osid.course.syllabus.ModuleQueryInspector[0]);
    }


    /**
     *  Gets the Module column name.
     *
     * @return the column name
     */

    protected String getModuleColumn() {
        return ("module");
    }


    /**
     *  Sets a lesson <code> Id. </code> 
     *
     *  @param  lessonId a lesson <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> lessonId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchLessonId(org.osid.id.Id lessonId, boolean match) {
        getAssembler().addIdTerm(getLessonIdColumn(), lessonId, match);
        return;
    }


    /**
     *  Clears the lesson <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearLessonIdTerms() {
        getAssembler().clearTerms(getLessonIdColumn());
        return;
    }


    /**
     *  Gets the lesson <code> Id </code> terms. 
     *
     *  @return the lesson <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getLessonIdTerms() {
        return (getAssembler().getIdTerms(getLessonIdColumn()));
    }


    /**
     *  Gets the LessonId column name.
     *
     * @return the column name
     */

    protected String getLessonIdColumn() {
        return ("lesson_id");
    }


    /**
     *  Tests if an <code> LessonQuery </code> is available. 
     *
     *  @return <code> true </code> if a lesson query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLessonQuery() {
        return (false);
    }


    /**
     *  Gets the query for a lesson query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the lesson query 
     *  @throws org.osid.UnimplementedException <code> supportsLessonQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonQuery getLessonQuery() {
        throw new org.osid.UnimplementedException("supportsLessonQuery() is false");
    }


    /**
     *  Matches plans with any lesson. 
     *
     *  @param  match <code> true </code> to match covocations with any 
     *          lesson, <code> false </code> to match plans with no lessons 
     */

    @OSID @Override
    public void matchAnyLesson(boolean match) {
        getAssembler().addIdWildcardTerm(getLessonColumn(), match);
        return;
    }


    /**
     *  Clears the lesson terms. 
     */

    @OSID @Override
    public void clearLessonTerms() {
        getAssembler().clearTerms(getLessonColumn());
        return;
    }


    /**
     *  Gets the lesson terms. 
     *
     *  @return the lesson terms 
     */

    @OSID @Override
    public org.osid.course.plan.LessonQueryInspector[] getLessonTerms() {
        return (new org.osid.course.plan.LessonQueryInspector[0]);
    }


    /**
     *  Gets the Lesson column name.
     *
     * @return the column name
     */

    protected String getLessonColumn() {
        return ("lesson");
    }


    /**
     *  Sets the lesson <code> Id </code> for this query to match plans 
     *  assigned to course catalogs. 
     *
     *  @param  courseCatalogId a course catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseCatalogId(org.osid.id.Id courseCatalogId, 
                                     boolean match) {
        getAssembler().addIdTerm(getCourseCatalogIdColumn(), courseCatalogId, match);
        return;
    }


    /**
     *  Clears the course catalog <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseCatalogIdTerms() {
        getAssembler().clearTerms(getCourseCatalogIdColumn());
        return;
    }


    /**
     *  Gets the course catalog <code> Id </code> terms. 
     *
     *  @return the course catalog <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseCatalogIdTerms() {
        return (getAssembler().getIdTerms(getCourseCatalogIdColumn()));
    }


    /**
     *  Gets the CourseCatalogId column name.
     *
     * @return the column name
     */

    protected String getCourseCatalogIdColumn() {
        return ("course_catalog_id");
    }


    /**
     *  Tests if an <code> CourseCatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a course catalog query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course catalog query. Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the course catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQuery getCourseCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsCourseCatalogQuery() is false");
    }


    /**
     *  Clears the course catalog terms. 
     */

    @OSID @Override
    public void clearCourseCatalogTerms() {
        getAssembler().clearTerms(getCourseCatalogColumn());
        return;
    }


    /**
     *  Gets the course catalog terms. 
     *
     *  @return the course catalog terms 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQueryInspector[] getCourseCatalogTerms() {
        return (new org.osid.course.CourseCatalogQueryInspector[0]);
    }


    /**
     *  Gets the CourseCatalog column name.
     *
     * @return the column name
     */

    protected String getCourseCatalogColumn() {
        return ("course_catalog");
    }


    /**
     *  Tests if this plan supports the given record
     *  <code>Type</code>.
     *
     *  @param  planRecordType a plan record type 
     *  @return <code>true</code> if the planRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>planRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type planRecordType) {
        for (org.osid.course.plan.records.PlanQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(planRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  planRecordType the plan record type 
     *  @return the plan query record 
     *  @throws org.osid.NullArgumentException
     *          <code>planRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(planRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.plan.records.PlanQueryRecord getPlanQueryRecord(org.osid.type.Type planRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.plan.records.PlanQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(planRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(planRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  planRecordType the plan record type 
     *  @return the plan query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>planRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(planRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.plan.records.PlanQueryInspectorRecord getPlanQueryInspectorRecord(org.osid.type.Type planRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.plan.records.PlanQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(planRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(planRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param planRecordType the plan record type
     *  @return the plan search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>planRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(planRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.plan.records.PlanSearchOrderRecord getPlanSearchOrderRecord(org.osid.type.Type planRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.plan.records.PlanSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(planRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(planRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this plan. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param planQueryRecord the plan query record
     *  @param planQueryInspectorRecord the plan query inspector
     *         record
     *  @param planSearchOrderRecord the plan search order record
     *  @param planRecordType plan record type
     *  @throws org.osid.NullArgumentException
     *          <code>planQueryRecord</code>,
     *          <code>planQueryInspectorRecord</code>,
     *          <code>planSearchOrderRecord</code> or
     *          <code>planRecordTypeplan</code> is
     *          <code>null</code>
     */
            
    protected void addPlanRecords(org.osid.course.plan.records.PlanQueryRecord planQueryRecord, 
                                      org.osid.course.plan.records.PlanQueryInspectorRecord planQueryInspectorRecord, 
                                      org.osid.course.plan.records.PlanSearchOrderRecord planSearchOrderRecord, 
                                      org.osid.type.Type planRecordType) {

        addRecordType(planRecordType);

        nullarg(planQueryRecord, "plan query record");
        nullarg(planQueryInspectorRecord, "plan query inspector record");
        nullarg(planSearchOrderRecord, "plan search odrer record");

        this.queryRecords.add(planQueryRecord);
        this.queryInspectorRecords.add(planQueryInspectorRecord);
        this.searchOrderRecords.add(planSearchOrderRecord);
        
        return;
    }
}
