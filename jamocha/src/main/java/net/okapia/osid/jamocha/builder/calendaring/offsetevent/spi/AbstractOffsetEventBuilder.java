//
// AbstractOffsetEvent.java
//
//     Defines an OffsetEvent builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.calendaring.offsetevent.spi;


/**
 *  Defines an <code>OffsetEvent</code> builder.
 */

public abstract class AbstractOffsetEventBuilder<T extends AbstractOffsetEventBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRuleBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.calendaring.offsetevent.OffsetEventMiter offsetEvent;


    /**
     *  Constructs a new <code>AbstractOffsetEventBuilder</code>.
     *
     *  @param offsetEvent the offset event to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractOffsetEventBuilder(net.okapia.osid.jamocha.builder.calendaring.offsetevent.OffsetEventMiter offsetEvent) {
        super(offsetEvent);
        this.offsetEvent = offsetEvent;
        return;
    }


    /**
     *  Builds the offset event.
     *
     *  @return the new offset event
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.calendaring.OffsetEvent build() {
        (new net.okapia.osid.jamocha.builder.validator.calendaring.offsetevent.OffsetEventValidator(getValidations())).validate(this.offsetEvent);
        return (new net.okapia.osid.jamocha.builder.calendaring.offsetevent.ImmutableOffsetEvent(this.offsetEvent));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the offset event miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.calendaring.offsetevent.OffsetEventMiter getMiter() {
        return (this.offsetEvent);
    }


    /**
     *  Sets the fixed start time.
     *
     *  @param time a fixed start time
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    public T fixedStartTime(org.osid.calendaring.DateTime time) {
        getMiter().setFixedStartTime(time);
        return (self());
    }


    /**
     *  Sets the start reference event.
     *
     *  @param event a start reference event
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>event</code> is
     *          <code>null</code>
     */

    public T startReferenceEvent(org.osid.calendaring.Event event) {
        getMiter().setStartReferenceEvent(event);
        return (self());
    }


    /**
     *  Sets the fixed start offset.
     *
     *  @param offset a fixed start offset
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>offset</code> is
     *          <code>null</code>
     */

    public T fixedStartOffset(org.osid.calendaring.Duration offset) {
        getMiter().setFixedStartOffset(offset);
        return (self());
    }


    /**
     *  Sets the relative weekday start offset.
     *
     *  @param offset a relative weekday start offset
     *  @return the builder
     */

    public T relativeWeekdayStartOffset(long offset) {
        getMiter().setRelativeWeekdayStartOffset(offset);
        return (self());
    }


    /**
     *  Sets the relative start weekday.
     *
     *  @param weekday a relative start weekday
     *  @return the builder
     */

    public T relativeStartWeekday(long weekday) {
        getMiter().setRelativeStartWeekday(weekday);
        return (self());
    }


    /**
     *  Sets the fixedduration.
     *
     *  @param duration a duration
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>duration</code>
     *          is <code>null</code>
     */

    public T fixedDuration(org.osid.calendaring.Duration duration) {
        getMiter().setFixedDuration(duration);
        return (self());
    }


    /**
     *  Sets the end reference event.
     *
     *  @param event an end reference event
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>event</code> is
     *          <code>null</code>
     */

    public T endReferenceEvent(org.osid.calendaring.Event event) {
        getMiter().setEndReferenceEvent(event);
        return (self());
    }


    /**
     *  Sets the fixed end offset.
     *
     *  @param offset a fixed end offset
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>offset</code> is
     *          <code>null</code>
     */

    public T fixedEndOffset(org.osid.calendaring.Duration offset) {
        getMiter().setFixedEndOffset(offset);
        return (self());
    }


    /**
     *  Sets the relative weekday end offset.
     *
     *  @param offset a relative weekday end offset
     *  @return the builder
     */

    public T relativeWeekdayEndOffset(long offset) {
        getMiter().setRelativeWeekdayEndOffset(offset);
        return (self());
    }


    /**
     *  Sets the relative end weekday.
     *
     *  @param weekday a relative end weekday
     *  @return the builder
     */

    public T relativeEndWeekday(long weekday) {
        getMiter().setRelativeEndWeekday(weekday);
        return (self());
    }


    /**
     *  Sets the location description.
     *
     *  @param locationDescription a location description
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>locationDescription</code> is <code>null</code>
     */

    public T locationDescription(org.osid.locale.DisplayText locationDescription) {
        getMiter().setLocationDescription(locationDescription);
        return (self());
    }


    /**
     *  Sets the location.
     *
     *  @param location a location
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>location</code>
     *          is <code>null</code>
     */

    public T location(org.osid.mapping.Location location) {
        getMiter().setLocation(location);
        return (self());
    }


    /**
     *  Adds a sponsor.
     *
     *  @param sponsor a sponsor
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>sponsor</code> is
     *          <code>null</code>
     */

    public T sponsor(org.osid.resource.Resource sponsor) {
        getMiter().addSponsor(sponsor);
        return (self());
    }


    /**
     *  Sets all the sponsors.
     *
     *  @param sponsors a collection of sponsors
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>sponsors</code>
     *          is <code>null</code>
     */

    public T sponsors(java.util.Collection<org.osid.resource.Resource> sponsors) {
        getMiter().setSponsors(sponsors);
        return (self());
    }


    /**
     *  Adds an OffsetEvent record.
     *
     *  @param record an offset event record
     *  @param recordType the type of offset event record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.calendaring.records.OffsetEventRecord record, org.osid.type.Type recordType) {
        getMiter().addOffsetEventRecord(record, recordType);
        return (self());
    }
}       


