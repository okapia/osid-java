//
// AbstractRequisiteQueryInspector.java
//
//     A template for making a RequisiteQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.requisite.requisite.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for requisites.
 */

public abstract class AbstractRequisiteQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQueryInspector
    implements org.osid.course.requisite.RequisiteQueryInspector {

    private final java.util.Collection<org.osid.course.requisite.records.RequisiteQueryInspectorRecord> records = new java.util.ArrayList<>();

    private final OsidContainableQueryInspector inspector = new OsidContainableQueryInspector();


    /**
     *  Gets the sequestered query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getSequesteredTerms() {
        return (this.inspector.getSequesteredTerms());
    }

    
    /**
     *  Gets the requisite option <code> Id </code> query terms. 
     *
     *  @return the requisite <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRequisiteOptionIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the requisite option query terms. 
     *
     *  @return the requisite query terms 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteQueryInspector[] getRequisiteOptionTerms() {
        return (new org.osid.course.requisite.RequisiteQueryInspector[0]);
    }


    /**
     *  Gets the course <code> Id </code> query terms. 
     *
     *  @return the course <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the course query terms. 
     *
     *  @return the course terms 
     */

    @OSID @Override
    public org.osid.course.CourseQueryInspector[] getCourseTerms() {
        return (new org.osid.course.CourseQueryInspector[0]);
    }


    /**
     *  Gets the program <code> Id </code> query terms. 
     *
     *  @return the program <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProgramIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the program query terms. 
     *
     *  @return the program terms 
     */

    @OSID @Override
    public org.osid.course.program.ProgramQueryInspector[] getProgramTerms() {
        return (new org.osid.course.program.ProgramQueryInspector[0]);
    }


    /**
     *  Gets the credential <code> Id </code> query terms. 
     *
     *  @return the credential <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCredentialIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the credential query terms. 
     *
     *  @return the credential terms 
     */

    @OSID @Override
    public org.osid.course.program.CredentialQueryInspector[] getCredentialTerms() {
        return (new org.osid.course.program.CredentialQueryInspector[0]);
    }


    /**
     *  Gets the learning objective <code> Id </code> query terms. 
     *
     *  @return the learning objective <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getLearningObjectiveIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the learning objective query terms. 
     *
     *  @return the learning objective terms 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQueryInspector[] getLearningObjectiveTerms() {
        return (new org.osid.learning.ObjectiveQueryInspector[0]);
    }


    /**
     *  Gets the assessment <code> Id </code> query terms. 
     *
     *  @return the assessment <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAssessmentIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the assessment query terms. 
     *
     *  @return the assessment terms 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentQueryInspector[] getAssessmentTerms() {
        return (new org.osid.assessment.AssessmentQueryInspector[0]);
    }


    /**
     *  Gets the award <code> Id </code> query terms. 
     *
     *  @return the award <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAwardIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the award query terms. 
     *
     *  @return the award terms 
     */

    @OSID @Override
    public org.osid.recognition.AwardQueryInspector[] getAwardTerms() {
        return (new org.osid.recognition.AwardQueryInspector[0]);
    }


    /**
     *  Gets the containing This method must be implemented. <code> Id </code> 
     *  terms. 
     *
     *  @return the This method must be implemented. <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getContainingRequisiteIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the containing This method must be implemented. terms. 
     *
     *  @return the requisite terms 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteQueryInspector[] getContainingRequisiteTerms() {
        return (new org.osid.course.requisite.RequisiteQueryInspector[0]);
    }


    /**
     *  Gets the course catalog <code> Id </code> query terms. 
     *
     *  @return the course catalog <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseCatalogIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the course catalog query terms. 
     *
     *  @return the course catalog query terms 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQueryInspector[] getCourseCatalogTerms() {
        return (new org.osid.course.CourseCatalogQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given requisite query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a requisite implementing the requested record.
     *
     *  @param requisiteRecordType a requisite record type
     *  @return the requisite query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>requisiteRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(requisiteRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.requisite.records.RequisiteQueryInspectorRecord getRequisiteQueryInspectorRecord(org.osid.type.Type requisiteRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.requisite.records.RequisiteQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(requisiteRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(requisiteRecordType + " is not supported");
    }


    /**
     *  Adds a record to this requisite query. 
     *
     *  @param requisiteQueryInspectorRecord requisite query inspector
     *         record
     *  @param requisiteRecordType requisite record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRequisiteQueryInspectorRecord(org.osid.course.requisite.records.RequisiteQueryInspectorRecord requisiteQueryInspectorRecord, 
                                                   org.osid.type.Type requisiteRecordType) {

        addRecordType(requisiteRecordType);
        nullarg(requisiteRecordType, "requisite record type");
        this.records.add(requisiteQueryInspectorRecord);        
        return;
    }


    protected class OsidContainableQueryInspector
        extends net.okapia.osid.jamocha.spi.AbstractOsidContainableQueryInspector
        implements org.osid.OsidContainableQueryInspector {
    }    
}
