//
// MutableMapProxyRoomLookupSession
//
//    Implements a Room lookup service backed by a collection of
//    rooms that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.room;


/**
 *  Implements a Room lookup service backed by a collection of
 *  rooms. The rooms are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of rooms can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyRoomLookupSession
    extends net.okapia.osid.jamocha.core.room.spi.AbstractMapRoomLookupSession
    implements org.osid.room.RoomLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyRoomLookupSession}
     *  with no rooms.
     *
     *  @param campus the campus
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code campus} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyRoomLookupSession(org.osid.room.Campus campus,
                                                  org.osid.proxy.Proxy proxy) {
        setCampus(campus);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyRoomLookupSession} with a
     *  single room.
     *
     *  @param campus the campus
     *  @param room a room
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code campus},
     *          {@code room}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyRoomLookupSession(org.osid.room.Campus campus,
                                                org.osid.room.Room room, org.osid.proxy.Proxy proxy) {
        this(campus, proxy);
        putRoom(room);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyRoomLookupSession} using an
     *  array of rooms.
     *
     *  @param campus the campus
     *  @param rooms an array of rooms
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code campus},
     *          {@code rooms}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyRoomLookupSession(org.osid.room.Campus campus,
                                                org.osid.room.Room[] rooms, org.osid.proxy.Proxy proxy) {
        this(campus, proxy);
        putRooms(rooms);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyRoomLookupSession} using a
     *  collection of rooms.
     *
     *  @param campus the campus
     *  @param rooms a collection of rooms
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code campus},
     *          {@code rooms}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyRoomLookupSession(org.osid.room.Campus campus,
                                                java.util.Collection<? extends org.osid.room.Room> rooms,
                                                org.osid.proxy.Proxy proxy) {
   
        this(campus, proxy);
        setSessionProxy(proxy);
        putRooms(rooms);
        return;
    }

    
    /**
     *  Makes a {@code Room} available in this session.
     *
     *  @param room an room
     *  @throws org.osid.NullArgumentException {@code room{@code 
     *          is {@code null}
     */

    @Override
    public void putRoom(org.osid.room.Room room) {
        super.putRoom(room);
        return;
    }


    /**
     *  Makes an array of rooms available in this session.
     *
     *  @param rooms an array of rooms
     *  @throws org.osid.NullArgumentException {@code rooms{@code 
     *          is {@code null}
     */

    @Override
    public void putRooms(org.osid.room.Room[] rooms) {
        super.putRooms(rooms);
        return;
    }


    /**
     *  Makes collection of rooms available in this session.
     *
     *  @param rooms
     *  @throws org.osid.NullArgumentException {@code room{@code 
     *          is {@code null}
     */

    @Override
    public void putRooms(java.util.Collection<? extends org.osid.room.Room> rooms) {
        super.putRooms(rooms);
        return;
    }


    /**
     *  Removes a Room from this session.
     *
     *  @param roomId the {@code Id} of the room
     *  @throws org.osid.NullArgumentException {@code roomId{@code  is
     *          {@code null}
     */

    @Override
    public void removeRoom(org.osid.id.Id roomId) {
        super.removeRoom(roomId);
        return;
    }    
}
