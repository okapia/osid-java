//
// AbstractEffortSearch.java
//
//     A template for making an Effort Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.effort.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing effort searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractEffortSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.resourcing.EffortSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.resourcing.records.EffortSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.resourcing.EffortSearchOrder effortSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of efforts. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  effortIds list of efforts
     *  @throws org.osid.NullArgumentException
     *          <code>effortIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongEfforts(org.osid.id.IdList effortIds) {
        while (effortIds.hasNext()) {
            try {
                this.ids.add(effortIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongEfforts</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of effort Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getEffortIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  effortSearchOrder effort search order 
     *  @throws org.osid.NullArgumentException
     *          <code>effortSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>effortSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderEffortResults(org.osid.resourcing.EffortSearchOrder effortSearchOrder) {
	this.effortSearchOrder = effortSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.resourcing.EffortSearchOrder getEffortSearchOrder() {
	return (this.effortSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given effort search
     *  record <code> Type. </code> This method must be used to
     *  retrieve an effort implementing the requested record.
     *
     *  @param effortSearchRecordType an effort search record
     *         type
     *  @return the effort search record
     *  @throws org.osid.NullArgumentException
     *          <code>effortSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(effortSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.records.EffortSearchRecord getEffortSearchRecord(org.osid.type.Type effortSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.resourcing.records.EffortSearchRecord record : this.records) {
            if (record.implementsRecordType(effortSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(effortSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this effort search. 
     *
     *  @param effortSearchRecord effort search record
     *  @param effortSearchRecordType effort search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addEffortSearchRecord(org.osid.resourcing.records.EffortSearchRecord effortSearchRecord, 
                                           org.osid.type.Type effortSearchRecordType) {

        addRecordType(effortSearchRecordType);
        this.records.add(effortSearchRecord);        
        return;
    }
}
