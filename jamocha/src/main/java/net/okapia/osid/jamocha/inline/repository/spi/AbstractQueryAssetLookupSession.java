//
// AbstractQueryAssetLookupSession.java
//
//    An inline adapter that maps an AssetLookupSession to
//    an AssetQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.repository.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps an AssetLookupSession to
 *  an AssetQuerySession.
 */

public abstract class AbstractQueryAssetLookupSession
    extends net.okapia.osid.jamocha.repository.spi.AbstractAssetLookupSession
    implements org.osid.repository.AssetLookupSession {

    private final org.osid.repository.AssetQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryAssetLookupSession.
     *
     *  @param querySession the underlying asset query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryAssetLookupSession(org.osid.repository.AssetQuerySession querySession) {
        nullarg(querySession, "asset query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Repository</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Repository Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getRepositoryId() {
        return (this.session.getRepositoryId());
    }


    /**
     *  Gets the <code>Repository</code> associated with this 
     *  session.
     *
     *  @return the <code>Repository</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.Repository getRepository()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getRepository());
    }


    /**
     *  Tests if this user can perform <code>Asset</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAssets() {
        return (this.session.canSearchAssets());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include assets in repositories which are children
     *  of this repository in the repository hierarchy.
     */

    @OSID @Override
    public void useFederatedRepositoryView() {
        this.session.useFederatedRepositoryView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this repository only.
     */

    @OSID @Override
    public void useIsolatedRepositoryView() {
        this.session.useIsolatedRepositoryView();
        return;
    }
    
     
    /**
     *  Gets the <code>Asset</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Asset</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Asset</code> and
     *  retained for compatibility.
     *
     *  @param  assetId <code>Id</code> of the
     *          <code>Asset</code>
     *  @return the asset
     *  @throws org.osid.NotFoundException <code>assetId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>assetId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.Asset getAsset(org.osid.id.Id assetId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.repository.AssetQuery query = getQuery();
        query.matchId(assetId, true);
        org.osid.repository.AssetList assets = this.session.getAssetsByQuery(query);
        if (assets.hasNext()) {
            return (assets.getNextAsset());
        } 
        
        throw new org.osid.NotFoundException(assetId + " not found");
    }


    /**
     *  Gets an <code>AssetList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  assets specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Assets</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  assetIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Asset</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>assetIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.AssetList getAssetsByIds(org.osid.id.IdList assetIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.repository.AssetQuery query = getQuery();

        try (org.osid.id.IdList ids = assetIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getAssetsByQuery(query));
    }


    /**
     *  Gets an <code>AssetList</code> corresponding to the given
     *  asset genus <code>Type</code> which does not include
     *  assets of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  assets or an error results. Otherwise, the returned list
     *  may contain only those assets that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  assetGenusType an asset genus type 
     *  @return the returned <code>Asset</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assetGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.AssetList getAssetsByGenusType(org.osid.type.Type assetGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.repository.AssetQuery query = getQuery();
        query.matchGenusType(assetGenusType, true);
        return (this.session.getAssetsByQuery(query));
    }


    /**
     *  Gets an <code>AssetList</code> corresponding to the given
     *  asset genus <code>Type</code> and include any additional
     *  assets with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  assets or an error results. Otherwise, the returned list
     *  may contain only those assets that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  assetGenusType an asset genus type 
     *  @return the returned <code>Asset</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assetGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.AssetList getAssetsByParentGenusType(org.osid.type.Type assetGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.repository.AssetQuery query = getQuery();
        query.matchParentGenusType(assetGenusType, true);
        return (this.session.getAssetsByQuery(query));
    }


    /**
     *  Gets an <code>AssetList</code> containing the given
     *  asset record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  assets or an error results. Otherwise, the returned list
     *  may contain only those assets that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  assetRecordType an asset record type 
     *  @return the returned <code>Asset</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assetRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.AssetList getAssetsByRecordType(org.osid.type.Type assetRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.repository.AssetQuery query = getQuery();
        query.matchRecordType(assetRecordType, true);
        return (this.session.getAssetsByQuery(query));
    }


    /**
     *  Gets an <code>AssetList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known assets or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  assets that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Asset</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.repository.AssetList getAssetsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.repository.AssetQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getAssetsByQuery(query));        
    }

    
    /**
     *  Gets all <code>Assets</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  assets or an error results. Otherwise, the returned list
     *  may contain only those assets that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Assets</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.AssetList getAssets()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.repository.AssetQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getAssetsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.repository.AssetQuery getQuery() {
        org.osid.repository.AssetQuery query = this.session.getAssetQuery();
        return (query);
    }
}
