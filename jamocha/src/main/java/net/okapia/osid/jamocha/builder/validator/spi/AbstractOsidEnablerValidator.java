//
// AbstractOsidEnablerValidator.java
//
//     Validates OsidEnablers.
//
//
// Tom Coppeto
// Okapia
// 20 Semtember 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.spi;

import net.okapia.osid.jamocha.builder.validator.Validation;


/**
 *  Validates an OsidEnabler.
 */

public abstract class AbstractOsidEnablerValidator
    extends AbstractOsidRuleValidator {

    private final TemporalValidator validator;


    /**
     *  Constructs a new <code>AbstractOsidEnablerValidator</code>.
     */

    protected AbstractOsidEnablerValidator() {
        this.validator = new TemporalValidator();
        return;
    }


    /**
     *  Constructs a new <code>AbstractOsidEnablerValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    protected AbstractOsidEnablerValidator(java.util.EnumSet<Validation> validation) {
        super(validation);
        this.validator = new TemporalValidator(validation);
        return;
    }

    
    /**
     *  Validates an OsidEnabler.
     *
     *  @param enabler the enabler to validate
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.NullArgumentException <code>enabler</code>
     *          is <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred
     */
    
    public void validate(org.osid.OsidEnabler enabler) {
        super.validate(enabler);

        if (!(enabler instanceof org.osid.Temporal)) {
            throw new org.osid.UnsupportedException("enabler not a Temporal");
        }

        this.validator.validate((org.osid.Temporal) enabler);
        
        if (enabler.isEffectiveBySchedule()) {
            org.osid.id.Id scheduleId = enabler.getScheduleId();
            test(scheduleId, "getScheduleId()");
            
            if (perform(Validation.TRAVERSE)) {            
                try {
                    testId(scheduleId, enabler.getSchedule(),
                           "getScheduleId() != getSchedule().getId()");
                } catch (org.osid.OperationFailedException oe) {
                    throw new org.osid.OsidRuntimeException(oe);
                }
            }
        } else {
            try {
                enabler.getScheduleId();
                throw new org.osid.BadLogicException("getScheduleId() did not throw IlegalState");
            } catch (org.osid.IllegalStateException ie) {
            }
            
            if (perform(Validation.TRAVERSE)) {
                try {
                    enabler.getSchedule();
                    throw new org.osid.BadLogicException("getSchedule() did not throw IlegalState");
                } catch (org.osid.IllegalStateException ie) {
                } catch (org.osid.OperationFailedException oe) {
                    throw new org.osid.OsidRuntimeException(oe);
                }
            }
        }

        if (enabler.isEffectiveByEvent()) {
            org.osid.id.Id eventId = enabler.getEventId();
            test(eventId, "getEventId()");

            if (perform(Validation.TRAVERSE)) {            
                try {
                    testId(eventId, enabler.getEvent(), "getEventId() != getEvent().getId()");
                } catch (org.osid.OperationFailedException oe) {
                    throw new org.osid.OsidRuntimeException(oe);
                }
            }
        } else {
            try {
                enabler.getEventId();
                throw new org.osid.BadLogicException("getEventId() did not throw IlegalState");
            } catch (org.osid.IllegalStateException ie) {
            }

            if (perform(Validation.TRAVERSE)) {
                try {
                    enabler.getEvent();
                    throw new org.osid.BadLogicException("getEvent() did not throw IlegalState");
                } catch (org.osid.IllegalStateException ie) {
                } catch (org.osid.OperationFailedException oe) {
                    throw new org.osid.OsidRuntimeException(oe);
                }
            }
        }

        if (enabler.isEffectiveByCyclicEvent()) {
            org.osid.id.Id eventId = enabler.getCyclicEventId();
            test(eventId, "getCyclicEventId()");

            if (perform(Validation.TRAVERSE)) {            
                try {
                    testId(eventId, enabler.getCyclicEvent(), 
                           "getCyclicEventId() != getCyclicEvent().getId()");
                } catch (org.osid.OperationFailedException oe) {
                    throw new org.osid.OsidRuntimeException(oe);
                }
            }
        } else {
            try {
                enabler.getCyclicEventId();
                throw new org.osid.BadLogicException("getCyclicEventId() did not throw IlegalState");
            } catch (org.osid.IllegalStateException ie) {
            }

            if (perform(Validation.TRAVERSE)) {
                try {
                    enabler.getCyclicEvent();
                    throw new org.osid.BadLogicException("getCyclicEvent() did not throw IlegalState");
                } catch (org.osid.IllegalStateException ie) {
                } catch (org.osid.OperationFailedException oe) {
                    throw new org.osid.OsidRuntimeException(oe);
                }
            }
        }

        if (enabler.isEffectiveForDemographic()) {
            org.osid.id.Id demographicId = enabler.getDemographicId();
            test(demographicId, "getDemographicId()");

            if (perform(Validation.TRAVERSE)) {            
                try {
                    testId(demographicId, enabler.getDemographic(), 
                           "getDemographicId() != getDemographic().getId()");
                } catch (org.osid.OperationFailedException oe) {
                    throw new org.osid.OsidRuntimeException(oe);
                }
            }
        } else {
            try {
                enabler.getDemographicId();
                throw new org.osid.BadLogicException("getDemographicId() did not throw IlegalState");
            } catch (org.osid.IllegalStateException ie) {
            }

            if (perform(Validation.TRAVERSE)) {
                try {
                    enabler.getDemographic();
                    throw new org.osid.BadLogicException("getDemographic() did not throw IlegalState");
                } catch (org.osid.IllegalStateException ie) {
                } catch (org.osid.OperationFailedException oe) {
                    throw new org.osid.OsidRuntimeException(oe);
                }
            }
        }

        return;
    }


    protected class TemporalValidator
        extends AbstractTemporalValidator {

        protected TemporalValidator() {
            return;
        }


        protected TemporalValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
            super(validation);
            return;
        }

        
        public void validate(org.osid.Temporal temporal) {
            super.validate(temporal);
            return;
        }
    }
}
