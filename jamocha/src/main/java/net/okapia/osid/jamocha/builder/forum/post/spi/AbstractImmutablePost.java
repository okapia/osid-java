//
// AbstractImmutablePost.java
//
//     Wraps a mutable Post to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.forum.post.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Post</code> to hide modifiers. This
 *  wrapper provides an immutized Post from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying post whose state changes are visible.
 */

public abstract class AbstractImmutablePost
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.forum.Post {

    private final org.osid.forum.Post post;


    /**
     *  Constructs a new <code>AbstractImmutablePost</code>.
     *
     *  @param post the post to immutablize
     *  @throws org.osid.NullArgumentException <code>post</code>
     *          is <code>null</code>
     */

    protected AbstractImmutablePost(org.osid.forum.Post post) {
        super(post);
        this.post = post;
        return;
    }


    /**
     *  Gets the time of this post. 
     *
     *  @return the time 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getTimestamp() {
        return (this.post.getTimestamp());
    }


    /**
     *  Gets the poster <code> Id </code> of this post. 
     *
     *  @return the poster resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getPosterId() {
        return (this.post.getPosterId());
    }


    /**
     *  Gets the poster of this post. 
     *
     *  @return the poster resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getPoster()
        throws org.osid.OperationFailedException {

        return (this.post.getPoster());
    }


    /**
     *  Gets the posting agent <code> Id </code> of this post. 
     *
     *  @return the posting agent <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getPostingAgentId() {
        return (this.post.getPostingAgentId());
    }


    /**
     *  Gets the posting agent of this post. 
     *
     *  @return the posting agent 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getPostingAgent()
        throws org.osid.OperationFailedException {

        return (this.post.getPostingAgent());
    }


    /**
     *  Gets the subject line of this post. 
     *
     *  @return the subject 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getSubjectLine() {
        return (this.post.getSubjectLine());
    }


    /**
     *  Gets the text of the post. 
     *
     *  @return the entry text 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getText() {
        return (this.post.getText());
    }


    /**
     *  Gets the post record corresponding to the given <code> Post </code> 
     *  record <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record. The <code> postRecordType </code> 
     *  may be the <code> Type </code> returned in <code> getRecordTypes() 
     *  </code> or any of its parents in a <code> Type </code> hierarchy where 
     *  <code> hasRecordType(postRecordType) </code> is <code> true </code> . 
     *
     *  @param  postRecordType the type of post record to retrieve 
     *  @return the post record 
     *  @throws org.osid.NullArgumentException <code> postRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(postRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.records.PostRecord getPostRecord(org.osid.type.Type postRecordType)
        throws org.osid.OperationFailedException {

        return (this.post.getPostRecord(postRecordType));
    }
}

