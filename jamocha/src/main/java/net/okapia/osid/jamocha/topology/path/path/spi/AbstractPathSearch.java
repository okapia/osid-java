//
// AbstractPathSearch.java
//
//     A template for making a Path Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.topology.path.path.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing path searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractPathSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.topology.path.PathSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.topology.path.records.PathSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.topology.path.PathSearchOrder pathSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of paths. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  pathIds list of paths
     *  @throws org.osid.NullArgumentException
     *          <code>pathIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongPaths(org.osid.id.IdList pathIds) {
        while (pathIds.hasNext()) {
            try {
                this.ids.add(pathIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongPaths</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of path Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getPathIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  pathSearchOrder path search order 
     *  @throws org.osid.NullArgumentException
     *          <code>pathSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>pathSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderPathResults(org.osid.topology.path.PathSearchOrder pathSearchOrder) {
	this.pathSearchOrder = pathSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.topology.path.PathSearchOrder getPathSearchOrder() {
	return (this.pathSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given path search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a path implementing the requested record.
     *
     *  @param pathSearchRecordType a path search record
     *         type
     *  @return the path search record
     *  @throws org.osid.NullArgumentException
     *          <code>pathSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(pathSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.topology.path.records.PathSearchRecord getPathSearchRecord(org.osid.type.Type pathSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.topology.path.records.PathSearchRecord record : this.records) {
            if (record.implementsRecordType(pathSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(pathSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this path search. 
     *
     *  @param pathSearchRecord path search record
     *  @param pathSearchRecordType path search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addPathSearchRecord(org.osid.topology.path.records.PathSearchRecord pathSearchRecord, 
                                           org.osid.type.Type pathSearchRecordType) {

        addRecordType(pathSearchRecordType);
        this.records.add(pathSearchRecord);        
        return;
    }
}
