//
// StringMetadata.java
//
//     Defines a string Metadata.
//
//
// Tom Coppeto
// Okapia
// 11 January 2023
//
//
// Copyright (c) 2023 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.metadata;


/**
 *  Defines a string Metadata.
 */

public final class StringMetadata
    extends net.okapia.osid.jamocha.metadata.spi.AbstractStringMetadata
    implements org.osid.Metadata {


    /**
     *  Constructs a new single unlinked {@code StringMetadata}.
     *
     *  @param elementId the Id of the element
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    public StringMetadata(org.osid.id.Id elementId) {
        super(elementId);
        return;
    }


    /**
     *  Constructs a new unlinked {@code StringMetadata}.
     *
     *  @param elementId the Id of the element
     *  @param isArray {@code true} if the element is an array another
     *         element, {@code false} if a single element
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    public StringMetadata(org.osid.id.Id elementId, boolean isArray) {
        super(elementId, isArray, false);
        return;
    }


    /**
     *  Constructs a new {@code StringMetadata}.
     *
     *  @param elementId the Id of the element
     *  @param isArray {@code true} if the element is an array another
     *         element, {@code false} if a single element
     *  @param isLinked {@code true} if the element is linked to
     *         another element, {@code false} otherwise
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    public StringMetadata(org.osid.id.Id elementId, boolean isArray, boolean isLinked) {
        super(elementId, isArray, isLinked);
        return;
    }


    /**
     *  Sets the element label.
     *
     *  @param label the new element label
     *  @throws org.osid.NullArgumentException {@code label} is {@code
     *          null}
     */

    public void setLabel(org.osid.locale.DisplayText label) {
        super.setLabel(label);
        return;
    }


    /**
     *  Sets the instructions.
     *
     *  @param instructions the new instructions
     *  @throws org.osid.NullArgumentException {@code instructions}
     *          is {@code null}
     */

    public void setInstructions(org.osid.locale.DisplayText instructions) {
        super.setInstructions(instructions);
        return;
    }

    
    /**
     *  Sets the required flag.
     *
     *  @param required {@code true} if required, {@code false} if
     *         optional
     */

    public void setRequired(boolean required) {
        super.setRequired(required);
        return;
    }

    
    /**
     *  Sets the has value flag.
     *
     *  @param exists {@code true} if has existing value, {@code
     *         false} if no value exists
     */

    public void setValueExists(boolean exists) {
        super.setValueExists(exists);
        return;
    }


    /**
     *  Sets the read only flag.
     *
     *  @param readonly {@code true} if read only, {@code false} if
     *         can be updated
     */

    public void setReadOnly(boolean readonly) {
        super.setReadOnly(readonly);
        return;
    }


    /**
     *  Sets the units.
     *
     *	@param units the new units
     *  @throws org.osid.NullArgumentException {@code units}
     *          is {@code null}
     */

    public void setUnits(org.osid.locale.DisplayText units) {
        super.setUnits(units);
        return;
    }


    /**
     *  Sets the min and max string length.
     *
     *  @param min the minimum value
     *  @param max the maximum value
     *  @throws org.osid.InvalidArgumentException {@code min} is
     *          greater than {@code max}
     *  @throws org.osid.NullArgumentException {@code min} or
     *          {@code max} is {@code null}
     */

    public void setStringLengthRange(long min, long max) {
        super.setStringLengthRange(min, max);
        return;
    }


    /**
     *  Add support for a string match type.
     *
     *  @param stringMatchType the type of string match
     *  @param expression the expression used to validate string
     *  @throws org.osid.NullArgumentException {@code stringMatchType}
     *          or {@code expression} is {@code null}
     */

    public void addStringMatchType(org.osid.type.Type stringMatchType, String expression) {
        super.addStringMatchType(stringMatchType, expression);
        return;
    }


    /**
     *  Add support for a string format type.
     *
     *  @param stringFormatType the type of string format
     *  @throws org.osid.NullArgumentException {@code stringFormatType}
     *          is {@code null}
     */

    public void addStringFormatType(org.osid.type.Type stringFormatType) {
        super.addStringFormatType(stringFormatType);
        return;
    }    

    
    /**
     *  Sets the string set.
     *
     *  @param values a collection of accepted string values
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    public void setStringSet(java.util.Collection<String> values) {
        super.setStringSet(values);
        return;
    }


    /**
     *  Adds a collection of values to the string set.
     *
     *  @param values a collection of accepted string values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    public void addToStringSet(java.util.Collection<String> values) {
        super.addToStringSet(values);
        return;
    }


    /**
     *  Adds a value to the string set.
     *
     *  @param value a string value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void addToStringSet(String value) {
        super.addToStringSet(value);
        return;
    }


    /**
     *  Removes a value from the string set.
     *
     *  @param value a string value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void removeFromStringSet(String value) {
        super.removeFromStringSet(value);
        return;
    }


    /**
     *  Clears the string set.
     */

    public void clearStringSet() {
        super.clearStringSet();
        return;
    }


    /**
     *  Sets the default string set.
     *
     *  @param values a collection of default string values
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    public void setDefaultStringValues(java.util.Collection<String> values) {
        super.setDefaultStringValues(values);
        return;
    }


    /**
     *  Adds a collection of default string values.
     *
     *  @param values a collection of default string values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    public void addDefaultStringValues(java.util.Collection<String> values) {
        super.addDefaultStringValues(values);
        return;
    }


    /**
        return;
     *  Adds a default string value.
     *
     *  @param value a string value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void addDefaultStringValue(String value) {
        super.addDefaultStringValue(value);
        return;
    }


    /**
     *  Removes a default string value.
     *
     *  @param value a string value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void removeDefaultStringValue(String value) {
        super.removeDefaultStringValue(value);
        return;
    }


    /**
     *  Clears the default string values.
     */

    public void clearDefaultStringValues() {
        super.clearDefaultStringValues();
        return;
    }


    /**
     *  Sets the existing string set.
     *
     *  @param values a collection of existing string values
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    public void setExistingStringValues(java.util.Collection<String> values) {
        super.setExistingStringValues(values);
        return;
    }


    /**
     *  Adds a collection of existing string values.
     *
     *  @param values a collection of existing string values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    public void addExistingStringValues(java.util.Collection<String> values) {
        super.addExistingStringValues(values);
        return;
    }


    /**
     *  Adds a existing string value.
     *
     *  @param value a string value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void addExistingStringValue(String value) {
        super.addExistingStringValue(value);
        return;
    }


    /**
     *  Removes a existing string value.
     *
     *  @param value a string value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void removeExistingStringValue(String value) {
        super.removeExistingStringValue(value);
        return;
    }


    /**
     *  Clears the existing string values.
     */

    public void clearExistingStringValues() {
        super.clearExistingStringValues();
        return;
    }            
}
