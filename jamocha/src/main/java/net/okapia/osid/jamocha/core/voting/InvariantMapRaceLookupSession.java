//
// InvariantMapRaceLookupSession
//
//    Implements a Race lookup service backed by a fixed collection of
//    races.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.voting;


/**
 *  Implements a Race lookup service backed by a fixed
 *  collection of races. The races are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapRaceLookupSession
    extends net.okapia.osid.jamocha.core.voting.spi.AbstractMapRaceLookupSession
    implements org.osid.voting.RaceLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapRaceLookupSession</code> with no
     *  races.
     *  
     *  @param polls the polls
     *  @throws org.osid.NullArgumnetException {@code polls} is
     *          {@code null}
     */

    public InvariantMapRaceLookupSession(org.osid.voting.Polls polls) {
        setPolls(polls);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapRaceLookupSession</code> with a single
     *  race.
     *  
     *  @param polls the polls
     *  @param race a single race
     *  @throws org.osid.NullArgumentException {@code polls} or
     *          {@code race} is <code>null</code>
     */

      public InvariantMapRaceLookupSession(org.osid.voting.Polls polls,
                                               org.osid.voting.Race race) {
        this(polls);
        putRace(race);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapRaceLookupSession</code> using an array
     *  of races.
     *  
     *  @param polls the polls
     *  @param races an array of races
     *  @throws org.osid.NullArgumentException {@code polls} or
     *          {@code races} is <code>null</code>
     */

      public InvariantMapRaceLookupSession(org.osid.voting.Polls polls,
                                               org.osid.voting.Race[] races) {
        this(polls);
        putRaces(races);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapRaceLookupSession</code> using a
     *  collection of races.
     *
     *  @param polls the polls
     *  @param races a collection of races
     *  @throws org.osid.NullArgumentException {@code polls} or
     *          {@code races} is <code>null</code>
     */

      public InvariantMapRaceLookupSession(org.osid.voting.Polls polls,
                                               java.util.Collection<? extends org.osid.voting.Race> races) {
        this(polls);
        putRaces(races);
        return;
    }
}
