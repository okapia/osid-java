//
// AbstractAssemblyConferralQuery.java
//
//     A ConferralQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.recognition.conferral.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ConferralQuery that stores terms.
 */

public abstract class AbstractAssemblyConferralQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRelationshipQuery
    implements org.osid.recognition.ConferralQuery,
               org.osid.recognition.ConferralQueryInspector,
               org.osid.recognition.ConferralSearchOrder {

    private final java.util.Collection<org.osid.recognition.records.ConferralQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.recognition.records.ConferralQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.recognition.records.ConferralSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyConferralQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyConferralQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets an award <code> Id. </code> 
     *
     *  @param  awardId an award <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> awardId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAwardId(org.osid.id.Id awardId, boolean match) {
        getAssembler().addIdTerm(getAwardIdColumn(), awardId, match);
        return;
    }


    /**
     *  Clears the award <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAwardIdTerms() {
        getAssembler().clearTerms(getAwardIdColumn());
        return;
    }


    /**
     *  Gets the award <code> Id </code> terms. 
     *
     *  @return the award <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAwardIdTerms() {
        return (getAssembler().getIdTerms(getAwardIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the award. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAward(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getAwardColumn(), style);
        return;
    }


    /**
     *  Gets the AwardId column name.
     *
     * @return the column name
     */

    protected String getAwardIdColumn() {
        return ("award_id");
    }


    /**
     *  Tests if an <code> AwardQuery </code> is available. 
     *
     *  @return <code> true </code> if an award query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAwardQuery() {
        return (false);
    }


    /**
     *  Gets the query for an award query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the award query 
     *  @throws org.osid.UnimplementedException <code> supportsAwardQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AwardQuery getAwardQuery() {
        throw new org.osid.UnimplementedException("supportsAwardQuery() is false");
    }


    /**
     *  Clears the award terms. 
     */

    @OSID @Override
    public void clearAwardTerms() {
        getAssembler().clearTerms(getAwardColumn());
        return;
    }


    /**
     *  Gets the award terms. 
     *
     *  @return the award terms 
     */

    @OSID @Override
    public org.osid.recognition.AwardQueryInspector[] getAwardTerms() {
        return (new org.osid.recognition.AwardQueryInspector[0]);
    }


    /**
     *  Tests if an award order is available. 
     *
     *  @return <code> true </code> if an award order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAwardSearchOrder() {
        return (false);
    }


    /**
     *  Gets the award order. 
     *
     *  @return the award search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AwardSearchOrder getAwardSearchOrder() {
        throw new org.osid.UnimplementedException("supportsAwardSearchOrder() is false");
    }


    /**
     *  Gets the Award column name.
     *
     * @return the column name
     */

    protected String getAwardColumn() {
        return ("award");
    }


    /**
     *  Sets a resource <code> Id. </code> 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRecipientId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getRecipientIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRecipientIdTerms() {
        getAssembler().clearTerms(getRecipientIdColumn());
        return;
    }


    /**
     *  Gets the recipient <code> Id </code> terms. 
     *
     *  @return the recipient <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRecipientIdTerms() {
        return (getAssembler().getIdTerms(getRecipientIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the recipient. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRecipient(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getRecipientColumn(), style);
        return;
    }


    /**
     *  Gets the RecipientId column name.
     *
     * @return the column name
     */

    protected String getRecipientIdColumn() {
        return ("recipient_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecipientQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecipientQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getRecipientQuery() {
        throw new org.osid.UnimplementedException("supportsRecipientQuery() is false");
    }


    /**
     *  Clears the resource terms. 
     */

    @OSID @Override
    public void clearRecipientTerms() {
        getAssembler().clearTerms(getRecipientColumn());
        return;
    }


    /**
     *  Gets the recipient terms. 
     *
     *  @return the recipient terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getRecipientTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a recipient order is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecipientSearchOrder() {
        return (false);
    }


    /**
     *  Gets the recipient order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecipientSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getRecipientSearchOrder() {
        throw new org.osid.UnimplementedException("supportsRecipientSearchOrder() is false");
    }


    /**
     *  Gets the Recipient column name.
     *
     * @return the column name
     */

    protected String getRecipientColumn() {
        return ("recipient");
    }


    /**
     *  Sets a reference <code> Id. </code> 
     *
     *  @param  referenceId a reference <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> referenceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchReferenceId(org.osid.id.Id referenceId, boolean match) {
        getAssembler().addIdTerm(getReferenceIdColumn(), referenceId, match);
        return;
    }


    /**
     *  Matches any reference. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAnyReferenceId(boolean match) {
        getAssembler().addIdWildcardTerm(getReferenceIdColumn(), match);
        return;
    }


    /**
     *  Clears the reference <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearReferenceIdTerms() {
        getAssembler().clearTerms(getReferenceIdColumn());
        return;
    }


    /**
     *  Gets the reference <code> Id </code> terms. 
     *
     *  @return the reference <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getReferenceIdTerms() {
        return (getAssembler().getIdTerms(getReferenceIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the reference. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByReference(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getReferenceIdColumn(), style);
        return;
    }


    /**
     *  Gets the ReferenceId column name.
     *
     * @return the column name
     */

    protected String getReferenceIdColumn() {
        return ("reference_id");
    }


    /**
     *  Sets a convocaton <code> Id. </code> 
     *
     *  @param  convocationId a convocaton <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> convocationId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchConvocationId(org.osid.id.Id convocationId, boolean match) {
        getAssembler().addIdTerm(getConvocationIdColumn(), convocationId, match);
        return;
    }


    /**
     *  Clears the convocaton <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearConvocationIdTerms() {
        getAssembler().clearTerms(getConvocationIdColumn());
        return;
    }


    /**
     *  Gets the convocation <code> Id </code> terms. 
     *
     *  @return the convocation <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getConvocationIdTerms() {
        return (getAssembler().getIdTerms(getConvocationIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the convocation. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByConvocation(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getConvocationColumn(), style);
        return;
    }


    /**
     *  Gets the ConvocationId column name.
     *
     * @return the column name
     */

    protected String getConvocationIdColumn() {
        return ("convocation_id");
    }


    /**
     *  Tests if a <code> ConvocationQuery </code> is available. 
     *
     *  @return <code> true </code> if a convocaton query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConvocationQuery() {
        return (false);
    }


    /**
     *  Gets the query for a convocaton query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the convocaton query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConvocationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationQuery getConvocationQuery() {
        throw new org.osid.UnimplementedException("supportsConvocationQuery() is false");
    }


    /**
     *  Matches any convocaton. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAnyConvocation(boolean match) {
        getAssembler().addIdWildcardTerm(getConvocationColumn(), match);
        return;
    }


    /**
     *  Clears the convocaton terms. 
     */

    @OSID @Override
    public void clearConvocationTerms() {
        getAssembler().clearTerms(getConvocationColumn());
        return;
    }


    /**
     *  Gets the convocation terms. 
     *
     *  @return the convocation terms 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationQueryInspector[] getConvocationTerms() {
        return (new org.osid.recognition.ConvocationQueryInspector[0]);
    }


    /**
     *  Tests if a convocation order is available. 
     *
     *  @return <code> true </code> if a convocation order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConvocationSearchOrder() {
        return (false);
    }


    /**
     *  Gets the convocation order. 
     *
     *  @return the convocation search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConvocationSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationSearchOrder getConvocationSearchOrder() {
        throw new org.osid.UnimplementedException("supportsConvocationSearchOrder() is false");
    }


    /**
     *  Gets the Convocation column name.
     *
     * @return the column name
     */

    protected String getConvocationColumn() {
        return ("convocation");
    }


    /**
     *  Sets the award <code> Id </code> for this query to match conferrals 
     *  assigned to academies. 
     *
     *  @param  academyId an academy <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> academyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAcademyId(org.osid.id.Id academyId, boolean match) {
        getAssembler().addIdTerm(getAcademyIdColumn(), academyId, match);
        return;
    }


    /**
     *  Clears the academy <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAcademyIdTerms() {
        getAssembler().clearTerms(getAcademyIdColumn());
        return;
    }


    /**
     *  Gets the academy <code> Id </code> terms. 
     *
     *  @return the academy <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAcademyIdTerms() {
        return (getAssembler().getIdTerms(getAcademyIdColumn()));
    }


    /**
     *  Gets the AcademyId column name.
     *
     * @return the column name
     */

    protected String getAcademyIdColumn() {
        return ("academy_id");
    }


    /**
     *  Tests if an <code> AcademyQuery </code> is available. 
     *
     *  @return <code> true </code> if an academy query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAcademyQuery() {
        return (false);
    }


    /**
     *  Gets the query for an academy query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the academy query 
     *  @throws org.osid.UnimplementedException <code> supportsAcademyQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AcademyQuery getAcademyQuery() {
        throw new org.osid.UnimplementedException("supportsAcademyQuery() is false");
    }


    /**
     *  Clears the academy terms. 
     */

    @OSID @Override
    public void clearAcademyTerms() {
        getAssembler().clearTerms(getAcademyColumn());
        return;
    }


    /**
     *  Gets the academy terms. 
     *
     *  @return the academy terms 
     */

    @OSID @Override
    public org.osid.recognition.AcademyQueryInspector[] getAcademyTerms() {
        return (new org.osid.recognition.AcademyQueryInspector[0]);
    }


    /**
     *  Gets the Academy column name.
     *
     * @return the column name
     */

    protected String getAcademyColumn() {
        return ("academy");
    }


    /**
     *  Tests if this conferral supports the given record
     *  <code>Type</code>.
     *
     *  @param  conferralRecordType a conferral record type 
     *  @return <code>true</code> if the conferralRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>conferralRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type conferralRecordType) {
        for (org.osid.recognition.records.ConferralQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(conferralRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  conferralRecordType the conferral record type 
     *  @return the conferral query record 
     *  @throws org.osid.NullArgumentException
     *          <code>conferralRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(conferralRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.recognition.records.ConferralQueryRecord getConferralQueryRecord(org.osid.type.Type conferralRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.recognition.records.ConferralQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(conferralRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(conferralRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  conferralRecordType the conferral record type 
     *  @return the conferral query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>conferralRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(conferralRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.recognition.records.ConferralQueryInspectorRecord getConferralQueryInspectorRecord(org.osid.type.Type conferralRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.recognition.records.ConferralQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(conferralRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(conferralRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param conferralRecordType the conferral record type
     *  @return the conferral search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>conferralRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(conferralRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.recognition.records.ConferralSearchOrderRecord getConferralSearchOrderRecord(org.osid.type.Type conferralRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.recognition.records.ConferralSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(conferralRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(conferralRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this conferral. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param conferralQueryRecord the conferral query record
     *  @param conferralQueryInspectorRecord the conferral query inspector
     *         record
     *  @param conferralSearchOrderRecord the conferral search order record
     *  @param conferralRecordType conferral record type
     *  @throws org.osid.NullArgumentException
     *          <code>conferralQueryRecord</code>,
     *          <code>conferralQueryInspectorRecord</code>,
     *          <code>conferralSearchOrderRecord</code> or
     *          <code>conferralRecordTypeconferral</code> is
     *          <code>null</code>
     */
            
    protected void addConferralRecords(org.osid.recognition.records.ConferralQueryRecord conferralQueryRecord, 
                                      org.osid.recognition.records.ConferralQueryInspectorRecord conferralQueryInspectorRecord, 
                                      org.osid.recognition.records.ConferralSearchOrderRecord conferralSearchOrderRecord, 
                                      org.osid.type.Type conferralRecordType) {

        addRecordType(conferralRecordType);

        nullarg(conferralQueryRecord, "conferral query record");
        nullarg(conferralQueryInspectorRecord, "conferral query inspector record");
        nullarg(conferralSearchOrderRecord, "conferral search odrer record");

        this.queryRecords.add(conferralQueryRecord);
        this.queryInspectorRecords.add(conferralQueryInspectorRecord);
        this.searchOrderRecords.add(conferralSearchOrderRecord);
        
        return;
    }
}
