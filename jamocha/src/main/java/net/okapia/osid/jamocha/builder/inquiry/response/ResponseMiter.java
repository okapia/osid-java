//
// ResponseMiter.java
//
//     Defines a Response miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.inquiry.response;


/**
 *  Defines a <code>Response</code> miter for use with the builders.
 */

public interface ResponseMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidRelationshipMiter,
            org.osid.inquiry.Response {


    /**
     *  Sets the inquiry.
     *
     *  @param inquiry an inquiry
     *  @throws org.osid.NullArgumentException
     *          <code>inquiry</code> is <code>null</code>
     */

    public void setInquiry(org.osid.inquiry.Inquiry inquiry);


    /**
     *  Sets the responder.
     *
     *  @param responder a responder
     *  @throws org.osid.NullArgumentException
     *          <code>responder</code> is <code>null</code>
     */

    public void setResponder(org.osid.resource.Resource responder);


    /**
     *  Sets the responding agent.
     *
     *  @param respondingAgent a responding agent
     *  @throws org.osid.NullArgumentException
     *          <code>respondingAgent</code> is <code>null</code>
     */

    public void setRespondingAgent(org.osid.authentication.Agent respondingAgent);


    /**
     *  Sets the affirmative flag.
     *
     *  @param affirmative {@code true} if a positive response, {@code
     *         false} if a negative response
     */

    public void setAffirmative(boolean affirmative);


    /**
     *  Adds a Response record.
     *
     *  @param record a response record
     *  @param recordType the type of response record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addResponseRecord(org.osid.inquiry.records.ResponseRecord record, org.osid.type.Type recordType);
}       


