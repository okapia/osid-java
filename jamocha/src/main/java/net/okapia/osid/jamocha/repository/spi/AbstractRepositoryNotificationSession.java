//
// AbstractRepositoryNotificationSession.java
//
//     A template for making RepositoryNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.repository.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code Repository} objects. This session is
 *  intended for consumers needing to synchronize their state with
 *  this service without the use of polling. Notifications are
 *  cancelled when this session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code Repository} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for repository entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractRepositoryNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.repository.RepositoryNotificationSession {


    /**
     *  Tests if this user can register for {@code Repository}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForRepositoryNotifications() {
        return (true);
    }


    /**
     *  Register for notifications of new repositories. {@code
     *  RepositoryReceiver.newRepository()} is invoked when a new
     *  {@code Repository} is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewRepositories()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of an updated hierarchy structure
     *  that introduces a new ancestor of the specified
     *  repository. {@code RepositoryReceiver.newAncestorRepository()}
     *  is invoked when the specified repository node gets a new
     *  ancestor.
     *
     *  @param repositoryId the {@code Id} of the
     *         {@code Repository} node to monitor
     *  @throws org.osid.NullArgumentException {@code repositoryId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewRepositoryAncestors(org.osid.id.Id repositoryId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of an updated hierarchy structure
     *  that introduces a new descendant of the specified
     *  repository. {@code
     *  RepositoryReceiver.newDescendantRepository()} is invoked when
     *  the specified repository node gets a new descendant.
     *
     *  @param repositoryId the {@code Id} of the
     *         {@code Repository} node to monitor
     *  @throws org.osid.NullArgumentException {@code repositoryId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewRepositoryDescendants(org.osid.id.Id repositoryId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of updated repositories. {@code
     *  RepositoryReceiver.changedRepository()} is invoked when a
     *  repository is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedRepositories()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated repository. {@code
     *  RepositoryReceiver.changedRepository()} is invoked when the
     *  specified repository is changed.
     *
     *  @param repositoryId the {@code Id} of the {@code Repository} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code repositoryId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedRepository(org.osid.id.Id repositoryId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted repositories. {@code
     *  RepositoryReceiver.deletedRepository()} is invoked when a
     *  repository is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedRepositories()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of a deleted repository. {@code
     *  RepositoryReceiver.deletedRepository()} is invoked when the
     *  specified repository is deleted.
     *
     *  @param repositoryId the {@code Id} of the
     *          {@code Repository} to monitor
     *  @throws org.osid.NullArgumentException {@code repositoryId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedRepository(org.osid.id.Id repositoryId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated hierarchy structure
     *  that removes an ancestor of the specified repository. {@code
     *  RepositoryReceiver.deletedAncestor()} is invoked when the
     *  specified repository node loses an ancestor.
     *
     *  @param repositoryId the {@code Id} of the
     *         {@code Repository} node to monitor
     *  @throws org.osid.NullArgumentException {@code repositoryId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedRepositoryAncestors(org.osid.id.Id repositoryId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated hierarchy structure
     *  that removes a descendant of the specified repository. {@code
     *  RepositoryReceiver.deletedDescendant()} is invoked when the
     *  specified repository node loses a descendant.
     *
     *  @param repositoryId the {@code Id} of the
     *          {@code Repository} node to monitor
     *  @throws org.osid.NullArgumentException {@code repositoryId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedRepositoryDescendants(org.osid.id.Id repositoryId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
