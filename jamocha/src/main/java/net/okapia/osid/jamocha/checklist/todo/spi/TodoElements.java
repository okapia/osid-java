//
// TodoElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.checklist.todo.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class TodoElements
    extends net.okapia.osid.jamocha.spi.TemporalOsidObjectElements {


    /**
     *  Gets the sequestered element Id.
     *
     *  @return the sequestered element Id
     */

    public static org.osid.id.Id getSequestered() {
        return (net.okapia.osid.jamocha.spi.ContainableElements.getSequestered());
    }


    /**
     *  Gets the TodoElement Id.
     *
     *  @return the todo element Id
     */

    public static org.osid.id.Id getTodoEntityId() {
        return (makeEntityId("osid.checklist.Todo"));
    }


    /**
     *  Gets the Priority element Id.
     *
     *  @return the Priority element Id
     */

    public static org.osid.id.Id getPriority() {
        return (makeElementId("osid.checklist.todo.Priority"));
    }


    /**
     *  Gets the DueDate element Id.
     *
     *  @return the DueDate element Id
     */

    public static org.osid.id.Id getDueDate() {
        return (makeElementId("osid.checklist.todo.DueDate"));
    }


    /**
     *  Gets the DependencyIds element Id.
     *
     *  @return the DependencyIds element Id
     */

    public static org.osid.id.Id getDependencyIds() {
        return (makeElementId("osid.checklist.todo.DependencyIds"));
    }


    /**
     *  Gets the Dependencies element Id.
     *
     *  @return the Dependencies element Id
     */

    public static org.osid.id.Id getDependencies() {
        return (makeElementId("osid.checklist.todo.Dependencies"));
    }


    /**
     *  Gets the Complete element Id.
     *
     *  @return the Complete element Id
     */

    public static org.osid.id.Id getComplete() {
        return (makeElementId("osid.checklist.todo.Complete"));
    }


    /**
     *  Gets the MinimumPriority element Id.
     *
     *  @return the MinimumPriority element Id
     */

    public static org.osid.id.Id getMinimumPriority() {
        return (makeQueryElementId("osid.checklist.todo.MinimumPriority"));
    }


    /**
     *  Gets the AncestorTodoId element Id.
     *
     *  @return the AncestorTodoId element Id
     */

    public static org.osid.id.Id getAncestorTodoId() {
        return (makeQueryElementId("osid.checklist.todo.AncestorTodoId"));
    }


    /**
     *  Gets the AncestorTodo element Id.
     *
     *  @return the AncestorTodo element Id
     */

    public static org.osid.id.Id getAncestorTodo() {
        return (makeQueryElementId("osid.checklist.todo.AncestorTodo"));
    }


    /**
     *  Gets the DescendantTodoId element Id.
     *
     *  @return the DescendantTodoId element Id
     */

    public static org.osid.id.Id getDescendantTodoId() {
        return (makeQueryElementId("osid.checklist.todo.DescendantTodoId"));
    }


    /**
     *  Gets the DescendantTodo element Id.
     *
     *  @return the DescendantTodo element Id
     */

    public static org.osid.id.Id getDescendantTodo() {
        return (makeQueryElementId("osid.checklist.todo.DescendantTodo"));
    }


    /**
     *  Gets the ChecklistId element Id.
     *
     *  @return the ChecklistId element Id
     */

    public static org.osid.id.Id getChecklistId() {
        return (makeQueryElementId("osid.checklist.todo.ChecklistId"));
    }


    /**
     *  Gets the Checklist element Id.
     *
     *  @return the Checklist element Id
     */

    public static org.osid.id.Id getChecklist() {
        return (makeQueryElementId("osid.checklist.todo.Checklist"));
    }
}
