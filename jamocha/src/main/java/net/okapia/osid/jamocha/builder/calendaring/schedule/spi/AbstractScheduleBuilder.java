//
// AbstractSchedule.java
//
//     Defines a Schedule builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.calendaring.schedule.spi;


/**
 *  Defines a <code>Schedule</code> builder.
 */

public abstract class AbstractScheduleBuilder<T extends AbstractScheduleBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.calendaring.schedule.ScheduleMiter schedule;


    /**
     *  Constructs a new <code>AbstractScheduleBuilder</code>.
     *
     *  @param schedule the schedule to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractScheduleBuilder(net.okapia.osid.jamocha.builder.calendaring.schedule.ScheduleMiter schedule) {
        super(schedule);
        this.schedule = schedule;
        return;
    }


    /**
     *  Builds the schedule.
     *
     *  @return the new schedule
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.calendaring.Schedule build() {
        (new net.okapia.osid.jamocha.builder.validator.calendaring.schedule.ScheduleValidator(getValidations())).validate(this.schedule);
        return (new net.okapia.osid.jamocha.builder.calendaring.schedule.ImmutableSchedule(this.schedule));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the schedule miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.calendaring.schedule.ScheduleMiter getMiter() {
        return (this.schedule);
    }


    /**
     *  Sets the schedule slot.
     *
     *  @param slot a schedule slot
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>slot</code> is <code>null</code>
     */

    public T scheduleSlot(org.osid.calendaring.ScheduleSlot slot) {
        getMiter().setScheduleSlot(slot);
        return (self());
    }


    /**
     *  Sets the time period.
     *
     *  @param period a time period
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>period</code> is <code>null</code>
     */

    public T timePeriod(org.osid.calendaring.TimePeriod period) {
        getMiter().setTimePeriod(period);
        return (self());
    }


    /**
     *  Sets the schedule start.
     *
     *  @param start a schedule start
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>start</code> is <code>null</code>
     */

    public T scheduleStart(org.osid.calendaring.DateTime start) {
        getMiter().setScheduleStart(start);
        return (self());
    }


    /**
     *  Sets the schedule end.
     *
     *  @param end a schedule end
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>end</code> is <code>null</code>
     */

    public T scheduleEnd(org.osid.calendaring.DateTime end) {
        getMiter().setScheduleEnd(end);
        return (self());
    }


    /**
     *  Sets the limit.
     *
     *  @param limit a limit
     *  @return the builder
     *  @throws org.osid.InvalidArgumentException <code>limit</code>
     *          is negative
     */

    public T limit(long limit) {
        getMiter().setLimit(limit);
        return (self());
    }


    /**
     *  Sets the total duration.
     *
     *  @param duration the total duration
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>duration</code> is <code>null</code>
     */

    public T duration(org.osid.calendaring.Duration duration) {
        getMiter().setTotalDuration(duration);
        return (self());
    }


    /**
     *  Sets the location description.
     *
     *  @param description a location description
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    public T locationDescription(org.osid.locale.DisplayText description) {
        getMiter().setLocationDescription(description);
        return (self());
    }


    /**
     *  Sets the location.
     *
     *  @param location a location
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>location</code> is <code>null</code>
     */

    public T location(org.osid.mapping.Location location) {
        getMiter().setLocation(location);
        return (self());
    }


    /**
     *  Adds a Schedule record.
     *
     *  @param record a schedule record
     *  @param recordType the type of schedule record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.calendaring.records.ScheduleRecord record, org.osid.type.Type recordType) {
        getMiter().addScheduleRecord(record, recordType);
        return (self());
    }
}       


