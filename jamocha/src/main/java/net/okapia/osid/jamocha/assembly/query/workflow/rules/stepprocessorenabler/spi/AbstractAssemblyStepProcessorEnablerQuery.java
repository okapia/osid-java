//
// AbstractAssemblyStepProcessorEnablerQuery.java
//
//     A StepProcessorEnablerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.workflow.rules.stepprocessorenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A StepProcessorEnablerQuery that stores terms.
 */

public abstract class AbstractAssemblyStepProcessorEnablerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidEnablerQuery
    implements org.osid.workflow.rules.StepProcessorEnablerQuery,
               org.osid.workflow.rules.StepProcessorEnablerQueryInspector,
               org.osid.workflow.rules.StepProcessorEnablerSearchOrder {

    private final java.util.Collection<org.osid.workflow.rules.records.StepProcessorEnablerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.workflow.rules.records.StepProcessorEnablerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.workflow.rules.records.StepProcessorEnablerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyStepProcessorEnablerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyStepProcessorEnablerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches enablers mapped to the step processor. 
     *
     *  @param  stepProcessorId the step processor <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> stepProcessorId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledStepProcessorId(org.osid.id.Id stepProcessorId, 
                                          boolean match) {
        getAssembler().addIdTerm(getRuledStepProcessorIdColumn(), stepProcessorId, match);
        return;
    }


    /**
     *  Clears the step processor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledStepProcessorIdTerms() {
        getAssembler().clearTerms(getRuledStepProcessorIdColumn());
        return;
    }


    /**
     *  Gets the step processor <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledStepProcessorIdTerms() {
        return (getAssembler().getIdTerms(getRuledStepProcessorIdColumn()));
    }


    /**
     *  Gets the RuledStepProcessorId column name.
     *
     * @return the column name
     */

    protected String getRuledStepProcessorIdColumn() {
        return ("ruled_step_processor_id");
    }


    /**
     *  Tests if a <code> StepProcessorQuery </code> is available. 
     *
     *  @return <code> true </code> if a step processor query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledStepProcessorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a step processor. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the step processor query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledStepProcessorQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorQuery getRuledStepProcessorQuery() {
        throw new org.osid.UnimplementedException("supportsRuledStepProcessorQuery() is false");
    }


    /**
     *  Matches enablers mapped to any step processor. 
     *
     *  @param  match <code> true </code> for enablers mapped to any step 
     *          processor, <code> false </code> to match enablers mapped to no 
     *          step processors 
     */

    @OSID @Override
    public void matchAnyRuledStepProcessor(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledStepProcessorColumn(), match);
        return;
    }


    /**
     *  Clears the step processor query terms. 
     */

    @OSID @Override
    public void clearRuledStepProcessorTerms() {
        getAssembler().clearTerms(getRuledStepProcessorColumn());
        return;
    }


    /**
     *  Gets the step processor query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorQueryInspector[] getRuledStepProcessorTerms() {
        return (new org.osid.workflow.rules.StepProcessorQueryInspector[0]);
    }


    /**
     *  Gets the RuledStepProcessor column name.
     *
     * @return the column name
     */

    protected String getRuledStepProcessorColumn() {
        return ("ruled_step_processor");
    }


    /**
     *  Matches enablers mapped to the office. 
     *
     *  @param  officeId the office <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchOfficeId(org.osid.id.Id officeId, boolean match) {
        getAssembler().addIdTerm(getOfficeIdColumn(), officeId, match);
        return;
    }


    /**
     *  Clears the office <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearOfficeIdTerms() {
        getAssembler().clearTerms(getOfficeIdColumn());
        return;
    }


    /**
     *  Gets the office <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOfficeIdTerms() {
        return (getAssembler().getIdTerms(getOfficeIdColumn()));
    }


    /**
     *  Gets the OfficeId column name.
     *
     * @return the column name
     */

    protected String getOfficeIdColumn() {
        return ("office_id");
    }


    /**
     *  Tests if an <code> OfficeQuery </code> is available. 
     *
     *  @return <code> true </code> if an office query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfficeQuery() {
        return (false);
    }


    /**
     *  Gets the query for an office. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the office query 
     *  @throws org.osid.UnimplementedException <code> supportsOfficeQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.OfficeQuery getOfficeQuery() {
        throw new org.osid.UnimplementedException("supportsOfficeQuery() is false");
    }


    /**
     *  Clears the office query terms. 
     */

    @OSID @Override
    public void clearOfficeTerms() {
        getAssembler().clearTerms(getOfficeColumn());
        return;
    }


    /**
     *  Gets the office query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.workflow.OfficeQueryInspector[] getOfficeTerms() {
        return (new org.osid.workflow.OfficeQueryInspector[0]);
    }


    /**
     *  Gets the Office column name.
     *
     * @return the column name
     */

    protected String getOfficeColumn() {
        return ("office");
    }


    /**
     *  Tests if this stepProcessorEnabler supports the given record
     *  <code>Type</code>.
     *
     *  @param  stepProcessorEnablerRecordType a step processor enabler record type 
     *  @return <code>true</code> if the stepProcessorEnablerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>stepProcessorEnablerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type stepProcessorEnablerRecordType) {
        for (org.osid.workflow.rules.records.StepProcessorEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(stepProcessorEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  stepProcessorEnablerRecordType the step processor enabler record type 
     *  @return the step processor enabler query record 
     *  @throws org.osid.NullArgumentException
     *          <code>stepProcessorEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(stepProcessorEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.workflow.rules.records.StepProcessorEnablerQueryRecord getStepProcessorEnablerQueryRecord(org.osid.type.Type stepProcessorEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.workflow.rules.records.StepProcessorEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(stepProcessorEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(stepProcessorEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  stepProcessorEnablerRecordType the step processor enabler record type 
     *  @return the step processor enabler query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>stepProcessorEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(stepProcessorEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.workflow.rules.records.StepProcessorEnablerQueryInspectorRecord getStepProcessorEnablerQueryInspectorRecord(org.osid.type.Type stepProcessorEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.workflow.rules.records.StepProcessorEnablerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(stepProcessorEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(stepProcessorEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param stepProcessorEnablerRecordType the step processor enabler record type
     *  @return the step processor enabler search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>stepProcessorEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(stepProcessorEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.workflow.rules.records.StepProcessorEnablerSearchOrderRecord getStepProcessorEnablerSearchOrderRecord(org.osid.type.Type stepProcessorEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.workflow.rules.records.StepProcessorEnablerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(stepProcessorEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(stepProcessorEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this step processor enabler. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param stepProcessorEnablerQueryRecord the step processor enabler query record
     *  @param stepProcessorEnablerQueryInspectorRecord the step processor enabler query inspector
     *         record
     *  @param stepProcessorEnablerSearchOrderRecord the step processor enabler search order record
     *  @param stepProcessorEnablerRecordType step processor enabler record type
     *  @throws org.osid.NullArgumentException
     *          <code>stepProcessorEnablerQueryRecord</code>,
     *          <code>stepProcessorEnablerQueryInspectorRecord</code>,
     *          <code>stepProcessorEnablerSearchOrderRecord</code> or
     *          <code>stepProcessorEnablerRecordTypestepProcessorEnabler</code> is
     *          <code>null</code>
     */
            
    protected void addStepProcessorEnablerRecords(org.osid.workflow.rules.records.StepProcessorEnablerQueryRecord stepProcessorEnablerQueryRecord, 
                                      org.osid.workflow.rules.records.StepProcessorEnablerQueryInspectorRecord stepProcessorEnablerQueryInspectorRecord, 
                                      org.osid.workflow.rules.records.StepProcessorEnablerSearchOrderRecord stepProcessorEnablerSearchOrderRecord, 
                                      org.osid.type.Type stepProcessorEnablerRecordType) {

        addRecordType(stepProcessorEnablerRecordType);

        nullarg(stepProcessorEnablerQueryRecord, "step processor enabler query record");
        nullarg(stepProcessorEnablerQueryInspectorRecord, "step processor enabler query inspector record");
        nullarg(stepProcessorEnablerSearchOrderRecord, "step processor enabler search odrer record");

        this.queryRecords.add(stepProcessorEnablerQueryRecord);
        this.queryInspectorRecords.add(stepProcessorEnablerQueryInspectorRecord);
        this.searchOrderRecords.add(stepProcessorEnablerSearchOrderRecord);
        
        return;
    }
}
