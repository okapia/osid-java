//
// MutableIndexedMapProxyOntologyLookupSession
//
//    Implements an Ontology lookup service backed by a collection of
//    ontologies indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.ontology;


/**
 *  Implements an Ontology lookup service backed by a collection of
 *  ontologies. The ontologies are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some ontologies may be compatible
 *  with more types than are indicated through these ontology
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of ontologies can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyOntologyLookupSession
    extends net.okapia.osid.jamocha.core.ontology.spi.AbstractIndexedMapOntologyLookupSession
    implements org.osid.ontology.OntologyLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyOntologyLookupSession} with
     *  no ontology.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public MutableIndexedMapProxyOntologyLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyOntologyLookupSession} with
     *  a single ontology.
     *
     *  @param  ontology an ontology
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code ontology} or
     *          {@code proxy} is {@code null}
     */

    public MutableIndexedMapProxyOntologyLookupSession(org.osid.ontology.Ontology ontology, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putOntology(ontology);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyOntologyLookupSession} using
     *  an array of ontologies.
     *
     *  @param  ontologies an array of ontologies
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code ontologies} or
     *          {@code proxy} is {@code null}
     */

    public MutableIndexedMapProxyOntologyLookupSession(org.osid.ontology.Ontology[] ontologies, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putOntologies(ontologies);
        return;
    }


    /**
     *  Constructs a new {@code MutableIndexedMapProxyOntologyLookupSession} using
     *  a collection of ontologies.
     *
     *  @param  ontologies a collection of ontologies
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code ontologies} or
     *          {@code proxy} is {@code null}
     */

    public MutableIndexedMapProxyOntologyLookupSession(java.util.Collection<? extends org.osid.ontology.Ontology> ontologies,
                                                       org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putOntologies(ontologies);
        return;
    }

    
    /**
     *  Makes an {@code Ontology} available in this session.
     *
     *  @param  ontology an ontology
     *  @throws org.osid.NullArgumentException {@code ontology{@code 
     *          is {@code null}
     */

    @Override
    public void putOntology(org.osid.ontology.Ontology ontology) {
        super.putOntology(ontology);
        return;
    }


    /**
     *  Makes an array of ontologies available in this session.
     *
     *  @param  ontologies an array of ontologies
     *  @throws org.osid.NullArgumentException {@code ontologies{@code 
     *          is {@code null}
     */

    @Override
    public void putOntologies(org.osid.ontology.Ontology[] ontologies) {
        super.putOntologies(ontologies);
        return;
    }


    /**
     *  Makes collection of ontologies available in this session.
     *
     *  @param  ontologies a collection of ontologies
     *  @throws org.osid.NullArgumentException {@code ontology{@code 
     *          is {@code null}
     */

    @Override
    public void putOntologies(java.util.Collection<? extends org.osid.ontology.Ontology> ontologies) {
        super.putOntologies(ontologies);
        return;
    }


    /**
     *  Removes an Ontology from this session.
     *
     *  @param ontologyId the {@code Id} of the ontology
     *  @throws org.osid.NullArgumentException {@code ontologyId{@code  is
     *          {@code null}
     */

    @Override
    public void removeOntology(org.osid.id.Id ontologyId) {
        super.removeOntology(ontologyId);
        return;
    }    
}
