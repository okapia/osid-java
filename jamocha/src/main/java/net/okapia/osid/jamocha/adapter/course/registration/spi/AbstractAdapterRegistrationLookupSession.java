//
// AbstractAdapterRegistrationLookupSession.java
//
//    A Registration lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.course.registration.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Registration lookup session adapter.
 */

public abstract class AbstractAdapterRegistrationLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.course.registration.RegistrationLookupSession {

    private final org.osid.course.registration.RegistrationLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterRegistrationLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterRegistrationLookupSession(org.osid.course.registration.RegistrationLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code CourseCatalog/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code CourseCatalog Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.session.getCourseCatalogId());
    }


    /**
     *  Gets the {@code CourseCatalog} associated with this session.
     *
     *  @return the {@code CourseCatalog} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCourseCatalog());
    }


    /**
     *  Tests if this user can perform {@code Registration} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupRegistrations() {
        return (this.session.canLookupRegistrations());
    }


    /**
     *  A complete view of the {@code Registration} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeRegistrationView() {
        this.session.useComparativeRegistrationView();
        return;
    }


    /**
     *  A complete view of the {@code Registration} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryRegistrationView() {
        this.session.usePlenaryRegistrationView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include registrations in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.session.useFederatedCourseCatalogView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.session.useIsolatedCourseCatalogView();
        return;
    }
    

    /**
     *  Only registrations whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveRegistrationView() {
        this.session.useEffectiveRegistrationView();
        return;
    }
    

    /**
     *  All registrations of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveRegistrationView() {
        this.session.useAnyEffectiveRegistrationView();
        return;
    }

     
    /**
     *  Gets the {@code Registration} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Registration} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Registration} and
     *  retained for compatibility.
     *
     *  In effective mode, registrations are returned that are currently
     *  effective.  In any effective mode, effective registrations and
     *  those currently expired are returned.
     *
     *  @param registrationId {@code Id} of the {@code Registration}
     *  @return the registration
     *  @throws org.osid.NotFoundException {@code registrationId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code registrationId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.Registration getRegistration(org.osid.id.Id registrationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRegistration(registrationId));
    }


    /**
     *  Gets a {@code RegistrationList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  registrations specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Registrations} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, registrations are returned that are currently
     *  effective.  In any effective mode, effective registrations and
     *  those currently expired are returned.
     *
     *  @param  registrationIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Registration} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code registrationIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationList getRegistrationsByIds(org.osid.id.IdList registrationIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRegistrationsByIds(registrationIds));
    }


    /**
     *  Gets a {@code RegistrationList} corresponding to the given
     *  registration genus {@code Type} which does not include
     *  registrations of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  registrations or an error results. Otherwise, the returned list
     *  may contain only those registrations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, registrations are returned that are currently
     *  effective.  In any effective mode, effective registrations and
     *  those currently expired are returned.
     *
     *  @param  registrationGenusType a registration genus type 
     *  @return the returned {@code Registration} list
     *  @throws org.osid.NullArgumentException
     *          {@code registrationGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationList getRegistrationsByGenusType(org.osid.type.Type registrationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRegistrationsByGenusType(registrationGenusType));
    }


    /**
     *  Gets a {@code RegistrationList} corresponding to the given
     *  registration genus {@code Type} and include any additional
     *  registrations with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  registrations or an error results. Otherwise, the returned list
     *  may contain only those registrations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, registrations are returned that are currently
     *  effective.  In any effective mode, effective registrations and
     *  those currently expired are returned.
     *
     *  @param  registrationGenusType a registration genus type 
     *  @return the returned {@code Registration} list
     *  @throws org.osid.NullArgumentException
     *          {@code registrationGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationList getRegistrationsByParentGenusType(org.osid.type.Type registrationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRegistrationsByParentGenusType(registrationGenusType));
    }


    /**
     *  Gets a {@code RegistrationList} containing the given
     *  registration record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  registrations or an error results. Otherwise, the returned list
     *  may contain only those registrations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, registrations are returned that are currently
     *  effective.  In any effective mode, effective registrations and
     *  those currently expired are returned.
     *
     *  @param  registrationRecordType a registration record type 
     *  @return the returned {@code Registration} list
     *  @throws org.osid.NullArgumentException
     *          {@code registrationRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationList getRegistrationsByRecordType(org.osid.type.Type registrationRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRegistrationsByRecordType(registrationRecordType));
    }


    /**
     *  Gets a {@code RegistrationList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  registrations or an error results. Otherwise, the returned list
     *  may contain only those registrations that are accessible
     *  through this session.
     *  
     *  In active mode, registrations are returned that are currently
     *  active. In any status mode, active and inactive registrations
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Registration} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.registration.RegistrationList getRegistrationsOnDate(org.osid.calendaring.DateTime from, 
                                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRegistrationsOnDate(from, to));
    }
        

    /**
     *  Gets a list of registrations corresponding to a activity bundle
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  registrations or an error results. Otherwise, the returned list
     *  may contain only those registrations that are accessible through
     *  this session.
     *
     *  In effective mode, registrations are returned that are
     *  currently effective.  In any effective mode, effective
     *  registrations and those currently expired are returned.
     *
     *  @param  activityBundleId the {@code Id} of the activity bundle
     *  @return the returned {@code RegistrationList}
     *  @throws org.osid.NullArgumentException {@code activityBundleId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationList getRegistrationsForActivityBundle(org.osid.id.Id activityBundleId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRegistrationsForActivityBundle(activityBundleId));
    }


    /**
     *  Gets a list of registrations corresponding to a activity bundle
     *  {@code Id} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  registrations or an error results. Otherwise, the returned list
     *  may contain only those registrations that are accessible
     *  through this session.
     *
     *  In effective mode, registrations are returned that are
     *  currently effective.  In any effective mode, effective
     *  registrations and those currently expired are returned.
     *
     *  @param  activityBundleId the {@code Id} of the activity bundle
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code RegistrationList}
     *  @throws org.osid.NullArgumentException {@code activityBundleId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationList getRegistrationsForActivityBundleOnDate(org.osid.id.Id activityBundleId,
                                                                                                 org.osid.calendaring.DateTime from,
                                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRegistrationsForActivityBundleOnDate(activityBundleId, from, to));
    }


    /**
     *  Gets a list of registrations corresponding to a student
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  registrations or an error results. Otherwise, the returned list
     *  may contain only those registrations that are accessible
     *  through this session.
     *
     *  In effective mode, registrations are returned that are
     *  currently effective.  In any effective mode, effective
     *  registrations and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the student
     *  @return the returned {@code RegistrationList}
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationList getRegistrationsForStudent(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRegistrationsForStudent(resourceId));
    }


    /**
     *  Gets a list of registrations corresponding to a student
     *  {@code Id} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  registrations or an error results. Otherwise, the returned list
     *  may contain only those registrations that are accessible
     *  through this session.
     *
     *  In effective mode, registrations are returned that are
     *  currently effective.  In any effective mode, effective
     *  registrations and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the student
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code RegistrationList}
     *  @throws org.osid.NullArgumentException {@code resourceId}, {@code
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationList getRegistrationsForStudentOnDate(org.osid.id.Id resourceId,
                                                                                          org.osid.calendaring.DateTime from,
                                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRegistrationsForStudentOnDate(resourceId, from, to));
    }


    /**
     *  Gets a list of registrations corresponding to activity bundle and student
     *  {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known
     *  registrations or an error results. Otherwise, the returned list
     *  may contain only those registrations that are accessible
     *  through this session.
     *
     *  In effective mode, registrations are returned that are
     *  currently effective.  In any effective mode, effective
     *  registrations and those currently expired are returned.
     *
     *  @param  activityBundleId the {@code Id} of the activity bundle
     *  @param  resourceId the {@code Id} of the student
     *  @return the returned {@code RegistrationList}
     *  @throws org.osid.NullArgumentException {@code activityBundleId},
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationList getRegistrationsForActivityBundleAndStudent(org.osid.id.Id activityBundleId,
                                                                                                     org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRegistrationsForActivityBundleAndStudent(activityBundleId, resourceId));
    }


    /**
     *  Gets a list of registrations corresponding to activity bundle and student
     *  {@code Ids} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  registrations or an error results. Otherwise, the returned list
     *  may contain only those registrations that are accessible
     *  through this session.
     *
     *  In effective mode, registrations are returned that are currently
     *  effective. In any effective mode, effective registrations and
     *  those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the student
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code RegistrationList}
     *  @throws org.osid.NullArgumentException {@code activityBundleId},
     *          {@code resourceId}, {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationList getRegistrationsForActivityBundleAndStudentOnDate(org.osid.id.Id activityBundleId,
                                                                                                           org.osid.id.Id resourceId,
                                                                                                           org.osid.calendaring.DateTime from,
                                                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRegistrationsForActivityBundleAndStudentOnDate(activityBundleId, resourceId, from, to));
    }


    /**
     *  Gets a list of registrations corresponding to a course offering
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  registrations or an error results. Otherwise, the returned list
     *  may contain only those registrations that are accessible through
     *  this session.
     *
     *  In effective mode, registrations are returned that are
     *  currently effective.  In any effective mode, effective
     *  registrations and those currently expired are returned.
     *
     *  @param  courseOfferingId the {@code Id} of the course offering
     *  @return the returned {@code RegistrationList}
     *  @throws org.osid.NullArgumentException {@code courseOfferingId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationList getRegistrationsForCourseOffering(org.osid.id.Id courseOfferingId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRegistrationsForCourseOffering(courseOfferingId));
    }


    /**
     *  Gets a list of registrations corresponding to a course offering
     *  {@code Id} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  registrations or an error results. Otherwise, the returned list
     *  may contain only those registrations that are accessible
     *  through this session.
     *
     *  In effective mode, registrations are returned that are
     *  currently effective.  In any effective mode, effective
     *  registrations and those currently expired are returned.
     *
     *  @param  courseOfferingId the {@code Id} of the course offering
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code RegistrationList}
     *  @throws org.osid.NullArgumentException {@code courseOfferingId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationList getRegistrationsForCourseOfferingOnDate(org.osid.id.Id courseOfferingId,
                                                                                                 org.osid.calendaring.DateTime from,
                                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRegistrationsForCourseOfferingOnDate(courseOfferingId, from, to));
    }


    /**
     *  Gets a list of registrations corresponding to course offering and student
     *  {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known
     *  registrations or an error results. Otherwise, the returned list
     *  may contain only those registrations that are accessible
     *  through this session.
     *
     *  In effective mode, registrations are returned that are
     *  currently effective.  In any effective mode, effective
     *  registrations and those currently expired are returned.
     *
     *  @param  courseOfferingId the {@code Id} of the course offering
     *  @param  resourceId the {@code Id} of the student
     *  @return the returned {@code RegistrationList}
     *  @throws org.osid.NullArgumentException {@code courseOfferingId},
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationList getRegistrationsForCourseOfferingAndStudent(org.osid.id.Id courseOfferingId,
                                                                                                     org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRegistrationsForCourseOfferingAndStudent(courseOfferingId, resourceId));
    }


    /**
     *  Gets a list of registrations corresponding to course offering and student
     *  {@code Ids} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  registrations or an error results. Otherwise, the returned list
     *  may contain only those registrations that are accessible
     *  through this session.
     *
     *  In effective mode, registrations are returned that are currently
     *  effective. In any effective mode, effective registrations and
     *  those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the student
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code RegistrationList}
     *  @throws org.osid.NullArgumentException {@code courseOfferingId},
     *          {@code resourceId}, {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationList getRegistrationsForCourseOfferingAndStudentOnDate(org.osid.id.Id courseOfferingId,
                                                                                                           org.osid.id.Id resourceId,
                                                                                                           org.osid.calendaring.DateTime from,
                                                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRegistrationsForCourseOfferingAndStudentOnDate(courseOfferingId, resourceId, from, to));
    }

    
    /**
     *  Gets all {@code Registrations}. 
     *
     *  In plenary mode, the returned list contains all known
     *  registrations or an error results. Otherwise, the returned list
     *  may contain only those registrations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, registrations are returned that are currently
     *  effective.  In any effective mode, effective registrations and
     *  those currently expired are returned.
     *
     *  @return a list of {@code Registrations} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationList getRegistrations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRegistrations());
    }
}
