//
// AbstractMapGraphLookupSession
//
//    A simple framework for providing a Graph lookup service
//    backed by a fixed collection of graphs.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.topology.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Graph lookup service backed by a
 *  fixed collection of graphs. The graphs are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Graphs</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapGraphLookupSession
    extends net.okapia.osid.jamocha.topology.spi.AbstractGraphLookupSession
    implements org.osid.topology.GraphLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.topology.Graph> graphs = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.topology.Graph>());


    /**
     *  Makes a <code>Graph</code> available in this session.
     *
     *  @param  graph a graph
     *  @throws org.osid.NullArgumentException <code>graph<code>
     *          is <code>null</code>
     */

    protected void putGraph(org.osid.topology.Graph graph) {
        this.graphs.put(graph.getId(), graph);
        return;
    }


    /**
     *  Makes an array of graphs available in this session.
     *
     *  @param  graphs an array of graphs
     *  @throws org.osid.NullArgumentException <code>graphs<code>
     *          is <code>null</code>
     */

    protected void putGraphs(org.osid.topology.Graph[] graphs) {
        putGraphs(java.util.Arrays.asList(graphs));
        return;
    }


    /**
     *  Makes a collection of graphs available in this session.
     *
     *  @param  graphs a collection of graphs
     *  @throws org.osid.NullArgumentException <code>graphs<code>
     *          is <code>null</code>
     */

    protected void putGraphs(java.util.Collection<? extends org.osid.topology.Graph> graphs) {
        for (org.osid.topology.Graph graph : graphs) {
            this.graphs.put(graph.getId(), graph);
        }

        return;
    }


    /**
     *  Removes a Graph from this session.
     *
     *  @param  graphId the <code>Id</code> of the graph
     *  @throws org.osid.NullArgumentException <code>graphId<code> is
     *          <code>null</code>
     */

    protected void removeGraph(org.osid.id.Id graphId) {
        this.graphs.remove(graphId);
        return;
    }


    /**
     *  Gets the <code>Graph</code> specified by its <code>Id</code>.
     *
     *  @param  graphId <code>Id</code> of the <code>Graph</code>
     *  @return the graph
     *  @throws org.osid.NotFoundException <code>graphId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>graphId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.Graph getGraph(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.topology.Graph graph = this.graphs.get(graphId);
        if (graph == null) {
            throw new org.osid.NotFoundException("graph not found: " + graphId);
        }

        return (graph);
    }


    /**
     *  Gets all <code>Graphs</code>. In plenary mode, the returned
     *  list contains all known graphs or an error
     *  results. Otherwise, the returned list may contain only those
     *  graphs that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Graphs</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.GraphList getGraphs()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.topology.graph.ArrayGraphList(this.graphs.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.graphs.clear();
        super.close();
        return;
    }
}
