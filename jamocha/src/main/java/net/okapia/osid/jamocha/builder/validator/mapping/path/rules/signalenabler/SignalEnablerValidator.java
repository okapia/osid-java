//
// SignalEnablerValidator.java
//
//     Validates a SignalEnabler.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.mapping.path.rules.signalenabler;


/**
 *  Validates a SignalEnabler.
 */

public final class SignalEnablerValidator
    extends net.okapia.osid.jamocha.builder.validator.mapping.path.rules.signalenabler.spi.AbstractSignalEnablerValidator {


    /**
     *  Constructs a new <code>SignalEnablerValidator</code>.
     */

    public SignalEnablerValidator() {
        return;
    }


    /**
     *  Constructs a new <code>SignalEnablerValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    public SignalEnablerValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates a SignalEnabler with a default validation.
     *
     *  @param signalEnabler a signal enabler to validate
     *  @return the signal enabler
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>signalEnabler</code>
     *          is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred
     */

    public static org.osid.mapping.path.rules.SignalEnabler validateSignalEnabler(org.osid.mapping.path.rules.SignalEnabler signalEnabler) {
        SignalEnablerValidator validator = new SignalEnablerValidator();
        validator.validate(signalEnabler);
        return (signalEnabler);
    }


    /**
     *  Validates a SignalEnabler for the given validations.
     *
     *  @param validation an EnumSet of validations
     *  @param signalEnabler a signal enabler to validate
     *  @return the signal enabler
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          or <code>signalEnabler</code> is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred
     */

    public static org.osid.mapping.path.rules.SignalEnabler validateSignalEnabler(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation,
                                                       org.osid.mapping.path.rules.SignalEnabler signalEnabler) {

        SignalEnablerValidator validator = new SignalEnablerValidator(validation);
        validator.validate(signalEnabler);
        return (signalEnabler);
    }
}
