//
// AbstractRecipeBatchManager.java
//
//     An adapter for a RecipeBatchManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.recipe.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a RecipeBatchManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterRecipeBatchManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.recipe.batch.RecipeBatchManager>
    implements org.osid.recipe.batch.RecipeBatchManager {


    /**
     *  Constructs a new {@code AbstractAdapterRecipeBatchManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterRecipeBatchManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterRecipeBatchManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterRecipeBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if bulk administration of recipes is available. 
     *
     *  @return <code> true </code> if a recipe bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecipeBatchAdmin() {
        return (getAdapteeManager().supportsRecipeBatchAdmin());
    }


    /**
     *  Tests if bulk administration of directions is available. 
     *
     *  @return <code> true </code> if an direction bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDirectionBatchAdmin() {
        return (getAdapteeManager().supportsDirectionBatchAdmin());
    }


    /**
     *  Tests if bulk administration of procedures is available. 
     *
     *  @return <code> true </code> if a procedure bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcedureBatchAdmin() {
        return (getAdapteeManager().supportsProcedureBatchAdmin());
    }


    /**
     *  Tests if bulk administration of cookbook is available. 
     *
     *  @return <code> true </code> if a cookbook bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCookbookBatchAdmin() {
        return (getAdapteeManager().supportsCookbookBatchAdmin());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk recipe 
     *  administration service. 
     *
     *  @return a <code> RecipeBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecipeBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.batch.RecipeBatchAdminSession getRecipeBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRecipeBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk recipe 
     *  administration service for the given cookbook. 
     *
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @return a <code> RecipeBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Cookbook </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecipeBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.batch.RecipeBatchAdminSession getRecipeBatchAdminSessionForCookbook(org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRecipeBatchAdminSessionForCookbook(cookbookId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk direction 
     *  administration service. 
     *
     *  @return a <code> DirectionBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectionBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.batch.DirectionBatchAdminSession getDirectionBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDirectionBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk direction 
     *  administration service for the given cookbook. 
     *
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @return a <code> DirectionBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Cookbook </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectionBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.batch.DirectionBatchAdminSession getDirectionBatchAdminSessionForCookbook(org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDirectionBatchAdminSessionForCookbook(cookbookId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk procedure 
     *  administration service. 
     *
     *  @return a <code> ProcedureBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcedureBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.batch.ProcedureBatchAdminSession getProcedureBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProcedureBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk procedure 
     *  administration service for the given cookbook. 
     *
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @return a <code> ProcedureBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Cookbook </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcedureBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.batch.ProcedureBatchAdminSession getProcedureBatchAdminSessionForCookbook(org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProcedureBatchAdminSessionForCookbook(cookbookId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk cookbook 
     *  administration service. 
     *
     *  @return a <code> CookbookBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCookbookBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.batch.CookbookBatchAdminSession getCookbookBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCookbookBatchAdminSession());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
