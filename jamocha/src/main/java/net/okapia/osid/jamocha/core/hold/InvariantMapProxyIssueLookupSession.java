//
// InvariantMapProxyIssueLookupSession
//
//    Implements an Issue lookup service backed by a fixed
//    collection of issues. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.hold;


/**
 *  Implements an Issue lookup service backed by a fixed
 *  collection of issues. The issues are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyIssueLookupSession
    extends net.okapia.osid.jamocha.core.hold.spi.AbstractMapIssueLookupSession
    implements org.osid.hold.IssueLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyIssueLookupSession} with no
     *  issues.
     *
     *  @param oubliette the oubliette
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code oubliette} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyIssueLookupSession(org.osid.hold.Oubliette oubliette,
                                                  org.osid.proxy.Proxy proxy) {
        setOubliette(oubliette);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyIssueLookupSession} with a single
     *  issue.
     *
     *  @param oubliette the oubliette
     *  @param issue an single issue
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code oubliette},
     *          {@code issue} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyIssueLookupSession(org.osid.hold.Oubliette oubliette,
                                                  org.osid.hold.Issue issue, org.osid.proxy.Proxy proxy) {

        this(oubliette, proxy);
        putIssue(issue);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyIssueLookupSession} using
     *  an array of issues.
     *
     *  @param oubliette the oubliette
     *  @param issues an array of issues
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code oubliette},
     *          {@code issues} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyIssueLookupSession(org.osid.hold.Oubliette oubliette,
                                                  org.osid.hold.Issue[] issues, org.osid.proxy.Proxy proxy) {

        this(oubliette, proxy);
        putIssues(issues);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyIssueLookupSession} using a
     *  collection of issues.
     *
     *  @param oubliette the oubliette
     *  @param issues a collection of issues
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code oubliette},
     *          {@code issues} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyIssueLookupSession(org.osid.hold.Oubliette oubliette,
                                                  java.util.Collection<? extends org.osid.hold.Issue> issues,
                                                  org.osid.proxy.Proxy proxy) {

        this(oubliette, proxy);
        putIssues(issues);
        return;
    }
}
