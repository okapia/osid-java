//
// AbstractBlockSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.hold.block.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractBlockSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.hold.BlockSearchResults {

    private org.osid.hold.BlockList blocks;
    private final org.osid.hold.BlockQueryInspector inspector;
    private final java.util.Collection<org.osid.hold.records.BlockSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractBlockSearchResults.
     *
     *  @param blocks the result set
     *  @param blockQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>blocks</code>
     *          or <code>blockQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractBlockSearchResults(org.osid.hold.BlockList blocks,
                                            org.osid.hold.BlockQueryInspector blockQueryInspector) {
        nullarg(blocks, "blocks");
        nullarg(blockQueryInspector, "block query inspectpr");

        this.blocks = blocks;
        this.inspector = blockQueryInspector;

        return;
    }


    /**
     *  Gets the block list resulting from a search.
     *
     *  @return a block list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.hold.BlockList getBlocks() {
        if (this.blocks == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.hold.BlockList blocks = this.blocks;
        this.blocks = null;
	return (blocks);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.hold.BlockQueryInspector getBlockQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  block search record <code> Type. </code> This method must
     *  be used to retrieve a block implementing the requested
     *  record.
     *
     *  @param blockSearchRecordType a block search 
     *         record type 
     *  @return the block search
     *  @throws org.osid.NullArgumentException
     *          <code>blockSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(blockSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.hold.records.BlockSearchResultsRecord getBlockSearchResultsRecord(org.osid.type.Type blockSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.hold.records.BlockSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(blockSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(blockSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record block search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addBlockRecord(org.osid.hold.records.BlockSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "block record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
