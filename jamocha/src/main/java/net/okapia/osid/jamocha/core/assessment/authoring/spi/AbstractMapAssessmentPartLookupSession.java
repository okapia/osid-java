//
// AbstractMapAssessmentPartLookupSession
//
//    A simple framework for providing an AssessmentPart lookup service
//    backed by a fixed collection of assessment parts.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.assessment.authoring.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an AssessmentPart lookup service backed by a
 *  fixed collection of assessment parts. The assessment parts are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>AssessmentParts</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapAssessmentPartLookupSession
    extends net.okapia.osid.jamocha.assessment.authoring.spi.AbstractAssessmentPartLookupSession
    implements org.osid.assessment.authoring.AssessmentPartLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.assessment.authoring.AssessmentPart> assessmentParts = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.assessment.authoring.AssessmentPart>());


    /**
     *  Makes an <code>AssessmentPart</code> available in this session.
     *
     *  @param  assessmentPart an assessment part
     *  @throws org.osid.NullArgumentException <code>assessmentPart<code>
     *          is <code>null</code>
     */

    protected void putAssessmentPart(org.osid.assessment.authoring.AssessmentPart assessmentPart) {
        this.assessmentParts.put(assessmentPart.getId(), assessmentPart);
        return;
    }


    /**
     *  Makes an array of assessment parts available in this session.
     *
     *  @param  assessmentParts an array of assessment parts
     *  @throws org.osid.NullArgumentException <code>assessmentParts<code>
     *          is <code>null</code>
     */

    protected void putAssessmentParts(org.osid.assessment.authoring.AssessmentPart[] assessmentParts) {
        putAssessmentParts(java.util.Arrays.asList(assessmentParts));
        return;
    }


    /**
     *  Makes a collection of assessment parts available in this session.
     *
     *  @param  assessmentParts a collection of assessment parts
     *  @throws org.osid.NullArgumentException <code>assessmentParts<code>
     *          is <code>null</code>
     */

    protected void putAssessmentParts(java.util.Collection<? extends org.osid.assessment.authoring.AssessmentPart> assessmentParts) {
        for (org.osid.assessment.authoring.AssessmentPart assessmentPart : assessmentParts) {
            this.assessmentParts.put(assessmentPart.getId(), assessmentPart);
        }

        return;
    }


    /**
     *  Removes an AssessmentPart from this session.
     *
     *  @param  assessmentPartId the <code>Id</code> of the assessment part
     *  @throws org.osid.NullArgumentException <code>assessmentPartId<code> is
     *          <code>null</code>
     */

    protected void removeAssessmentPart(org.osid.id.Id assessmentPartId) {
        this.assessmentParts.remove(assessmentPartId);
        return;
    }


    /**
     *  Gets the <code>AssessmentPart</code> specified by its <code>Id</code>.
     *
     *  @param  assessmentPartId <code>Id</code> of the <code>AssessmentPart</code>
     *  @return the assessmentPart
     *  @throws org.osid.NotFoundException <code>assessmentPartId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>assessmentPartId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPart getAssessmentPart(org.osid.id.Id assessmentPartId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.assessment.authoring.AssessmentPart assessmentPart = this.assessmentParts.get(assessmentPartId);
        if (assessmentPart == null) {
            throw new org.osid.NotFoundException("assessmentPart not found: " + assessmentPartId);
        }

        return (assessmentPart);
    }


    /**
     *  Gets all <code>AssessmentParts</code>. In plenary mode, the returned
     *  list contains all known assessmentParts or an error
     *  results. Otherwise, the returned list may contain only those
     *  assessmentParts that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>AssessmentParts</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartList getAssessmentParts()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.assessment.authoring.assessmentpart.ArrayAssessmentPartList(this.assessmentParts.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.assessmentParts.clear();
        super.close();
        return;
    }
}
