//
// AcademyNodeFilterList.java
//
//     Implements a filtering AcademyNodeList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.recognition.academynode;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filtering AcademyNodeList.
 */

public final class AcademyNodeFilterList
    extends net.okapia.osid.jamocha.inline.filter.recognition.academynode.spi.AbstractAcademyNodeFilterList
    implements org.osid.recognition.AcademyNodeList,
               AcademyNodeFilter {

    private final AcademyNodeFilter filter;


    /**
     *  Creates a new <code>AcademyNodeFilterList</code>.
     *
     *  @param filter an inline query filter
     *  @param list an <code>AcademyNodeList</code>
     *  @throws org.osid.NullArgumentException <code>filter</code> or
     *          <code>list</code> is <code>null</code>
     */

    public AcademyNodeFilterList(AcademyNodeFilter filter, org.osid.recognition.AcademyNodeList list) {
        super(list);

        nullarg(filter, "inline query filter");
        this.filter = filter;

        return;
    }    

    
    /**
     *  Filters AcademyNodes.
     *
     *  @param academyNode the academy node to filter
     *  @return <code>true</code> if the academy node passes the filter,
     *          <code>false</code> if the academy node should be filtered
     */

    @Override
    public boolean pass(org.osid.recognition.AcademyNode academyNode) {
        return (this.filter.pass(academyNode));
    }
}
