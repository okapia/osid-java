//
// AbstractFederatingPlanLookupSession.java
//
//     An abstract federating adapter for a PlanLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.course.plan.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  PlanLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingPlanLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.course.plan.PlanLookupSession>
    implements org.osid.course.plan.PlanLookupSession {

    private boolean parallel = false;
    private org.osid.course.CourseCatalog courseCatalog = new net.okapia.osid.jamocha.nil.course.coursecatalog.UnknownCourseCatalog();


    /**
     *  Constructs a new <code>AbstractFederatingPlanLookupSession</code>.
     */

    protected AbstractFederatingPlanLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.course.plan.PlanLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>CourseCatalog/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>CourseCatalog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.courseCatalog.getId());
    }


    /**
     *  Gets the <code>CourseCatalog</code> associated with this 
     *  session.
     *
     *  @return the <code>CourseCatalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.courseCatalog);
    }


    /**
     *  Sets the <code>CourseCatalog</code>.
     *
     *  @param  courseCatalog the course catalog for this session
     *  @throws org.osid.NullArgumentException <code>courseCatalog</code>
     *          is <code>null</code>
     */

    protected void setCourseCatalog(org.osid.course.CourseCatalog courseCatalog) {
        nullarg(courseCatalog, "course catalog");
        this.courseCatalog = courseCatalog;
        return;
    }


    /**
     *  Tests if this user can perform <code>Plan</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupPlans() {
        for (org.osid.course.plan.PlanLookupSession session : getSessions()) {
            if (session.canLookupPlans()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Plan</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativePlanView() {
        for (org.osid.course.plan.PlanLookupSession session : getSessions()) {
            session.useComparativePlanView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Plan</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryPlanView() {
        for (org.osid.course.plan.PlanLookupSession session : getSessions()) {
            session.usePlenaryPlanView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include plans in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        for (org.osid.course.plan.PlanLookupSession session : getSessions()) {
            session.useFederatedCourseCatalogView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        for (org.osid.course.plan.PlanLookupSession session : getSessions()) {
            session.useIsolatedCourseCatalogView();
        }

        return;
    }


    /**
     *  Only plans whose effective dates are current are returned by
     *  methods in this session.
     */

    @OSID @Override
    public void useEffectivePlanView() {
        for (org.osid.course.plan.PlanLookupSession session : getSessions()) {
            session.useEffectivePlanView();
        }

        return;
    }


    /**
     *  All plans of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectivePlanView() {
        for (org.osid.course.plan.PlanLookupSession session : getSessions()) {
            session.useAnyEffectivePlanView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Plan</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Plan</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Plan</code> and
     *  retained for compatibility.
     *
     *  In effective mode, plans are returned that are currently
     *  effective.  In any effective mode, effective plans and
     *  those currently expired are returned.
     *
     *  @param  planId <code>Id</code> of the
     *          <code>Plan</code>
     *  @return the plan
     *  @throws org.osid.NotFoundException <code>planId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>planId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.plan.Plan getPlan(org.osid.id.Id planId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.course.plan.PlanLookupSession session : getSessions()) {
            try {
                return (session.getPlan(planId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(planId + " not found");
    }


    /**
     *  Gets a <code>PlanList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  plans specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Plans</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, plans are returned that are currently effective.
     *  In any effective mode, effective plans and those currently expired
     *  are returned.
     *
     *  @param  planIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Plan</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>planIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.plan.PlanList getPlansByIds(org.osid.id.IdList planIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.course.plan.plan.MutablePlanList ret = new net.okapia.osid.jamocha.course.plan.plan.MutablePlanList();

        try (org.osid.id.IdList ids = planIds) {
            while (ids.hasNext()) {
                ret.addPlan(getPlan(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>PlanList</code> corresponding to the given
     *  plan genus <code>Type</code> which does not include
     *  plans of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  plans or an error results. Otherwise, the returned list
     *  may contain only those plans that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, plans are returned that are currently effective.
     *  In any effective mode, effective plans and those currently expired
     *  are returned.
     *
     *  @param  planGenusType a plan genus type 
     *  @return the returned <code>Plan</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>planGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.plan.PlanList getPlansByGenusType(org.osid.type.Type planGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.plan.plan.FederatingPlanList ret = getPlanList();

        for (org.osid.course.plan.PlanLookupSession session : getSessions()) {
            ret.addPlanList(session.getPlansByGenusType(planGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>PlanList</code> corresponding to the given
     *  plan genus <code>Type</code> and include any additional
     *  plans with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  plans or an error results. Otherwise, the returned list
     *  may contain only those plans that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, plans are returned that are currently
     *  effective.  In any effective mode, effective plans and
     *  those currently expired are returned.
     *
     *  @param  planGenusType a plan genus type 
     *  @return the returned <code>Plan</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>planGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.plan.PlanList getPlansByParentGenusType(org.osid.type.Type planGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.plan.plan.FederatingPlanList ret = getPlanList();

        for (org.osid.course.plan.PlanLookupSession session : getSessions()) {
            ret.addPlanList(session.getPlansByParentGenusType(planGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>PlanList</code> containing the given
     *  plan record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  plans or an error results. Otherwise, the returned list
     *  may contain only those plans that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, plans are returned that are currently
     *  effective.  In any effective mode, effective plans and
     *  those currently expired are returned.
     *
     *  @param  planRecordType a plan record type 
     *  @return the returned <code>Plan</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>planRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.plan.PlanList getPlansByRecordType(org.osid.type.Type planRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.plan.plan.FederatingPlanList ret = getPlanList();

        for (org.osid.course.plan.PlanLookupSession session : getSessions()) {
            ret.addPlanList(session.getPlansByRecordType(planRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>PlanList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  plans or an error results. Otherwise, the returned list
     *  may contain only those plans that are accessible
     *  through this session.
     *  
     *  In active mode, plans are returned that are currently
     *  active. In any status mode, active and inactive plans
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Plan</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.plan.PlanList getPlansOnDate(org.osid.calendaring.DateTime from, 
                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.plan.plan.FederatingPlanList ret = getPlanList();

        for (org.osid.course.plan.PlanLookupSession session : getSessions()) {
            ret.addPlanList(session.getPlansOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a list of plans corresponding to a syllabus
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  plans or an error results. Otherwise, the returned list
     *  may contain only those plans that are accessible
     *  through this session.
     *
     *  In effective mode, plans are returned that are
     *  currently effective.  In any effective mode, effective
     *  plans and those currently expired are returned.
     *
     *  @param  syllabusId the <code>Id</code> of the syllabus
     *  @return the returned <code>PlanList</code>
     *  @throws org.osid.NullArgumentException <code>syllabusId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.course.plan.PlanList getPlansForSyllabus(org.osid.id.Id syllabusId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.plan.plan.FederatingPlanList ret = getPlanList();

        for (org.osid.course.plan.PlanLookupSession session : getSessions()) {
            ret.addPlanList(session.getPlansForSyllabus(syllabusId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of plans corresponding to a syllabus
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  plans or an error results. Otherwise, the returned list
     *  may contain only those plans that are accessible
     *  through this session.
     *
     *  In effective mode, plans are returned that are
     *  currently effective.  In any effective mode, effective
     *  plans and those currently expired are returned.
     *
     *  @param  syllabusId the <code>Id</code> of the syllabus
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>PlanList</code>
     *  @throws org.osid.NullArgumentException <code>syllabusId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.plan.PlanList getPlansForSyllabusOnDate(org.osid.id.Id syllabusId,
                                                                   org.osid.calendaring.DateTime from,
                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.plan.plan.FederatingPlanList ret = getPlanList();

        for (org.osid.course.plan.PlanLookupSession session : getSessions()) {
            ret.addPlanList(session.getPlansForSyllabusOnDate(syllabusId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of plans corresponding to a course offering
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  plans or an error results. Otherwise, the returned list
     *  may contain only those plans that are accessible
     *  through this session.
     *
     *  In effective mode, plans are returned that are
     *  currently effective.  In any effective mode, effective
     *  plans and those currently expired are returned.
     *
     *  @param  courseOfferingId the <code>Id</code> of the course offering
     *  @return the returned <code>PlanList</code>
     *  @throws org.osid.NullArgumentException <code>courseOfferingId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.course.plan.PlanList getPlansForCourseOffering(org.osid.id.Id courseOfferingId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.plan.plan.FederatingPlanList ret = getPlanList();

        for (org.osid.course.plan.PlanLookupSession session : getSessions()) {
            ret.addPlanList(session.getPlansForCourseOffering(courseOfferingId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of plans corresponding to a course offering
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  plans or an error results. Otherwise, the returned list
     *  may contain only those plans that are accessible
     *  through this session.
     *
     *  In effective mode, plans are returned that are
     *  currently effective.  In any effective mode, effective
     *  plans and those currently expired are returned.
     *
     *  @param  courseOfferingId the <code>Id</code> of the course offering
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>PlanList</code>
     *  @throws org.osid.NullArgumentException <code>courseOfferingId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.plan.PlanList getPlansForCourseOfferingOnDate(org.osid.id.Id courseOfferingId,
                                                                      org.osid.calendaring.DateTime from,
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.plan.plan.FederatingPlanList ret = getPlanList();

        for (org.osid.course.plan.PlanLookupSession session : getSessions()) {
            ret.addPlanList(session.getPlansForCourseOfferingOnDate(courseOfferingId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of plans corresponding to syllabus and course offering
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  plans or an error results. Otherwise, the returned list
     *  may contain only those plans that are accessible
     *  through this session.
     *
     *  In effective mode, plans are returned that are
     *  currently effective.  In any effective mode, effective
     *  plans and those currently expired are returned.
     *
     *  @param  syllabusId the <code>Id</code> of the syllabus
     *  @param  courseOfferingId the <code>Id</code> of the course offering
     *  @return the returned <code>PlanList</code>
     *  @throws org.osid.NullArgumentException <code>syllabusId</code>,
     *          <code>courseOfferingId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.course.plan.PlanList getPlansForSyllabusAndCourseOffering(org.osid.id.Id syllabusId,
                                                                              org.osid.id.Id courseOfferingId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.plan.plan.FederatingPlanList ret = getPlanList();

        for (org.osid.course.plan.PlanLookupSession session : getSessions()) {
            ret.addPlanList(session.getPlansForSyllabusAndCourseOffering(syllabusId, courseOfferingId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of plans corresponding to syllabus and course
     *  offering <code>Ids</code> and effective during the entire
     *  given date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  plans or an error results. Otherwise, the returned list
     *  may contain only those plans that are accessible
     *  through this session.
     *
     *  In effective mode, plans are returned that are
     *  currently effective.  In any effective mode, effective
     *  plans and those currently expired are returned.
     *
     *  @param  courseOfferingId the <code>Id</code> of the course offering
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>PlanList</code>
     *  @throws org.osid.NullArgumentException <code>syllabusId</code>,
     *          <code>courseOfferingId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.plan.PlanList getPlansForSyllabusAndCourseOfferingOnDate(org.osid.id.Id syllabusId,
                                                                                    org.osid.id.Id courseOfferingId,
                                                                                    org.osid.calendaring.DateTime from,
                                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.plan.plan.FederatingPlanList ret = getPlanList();

        for (org.osid.course.plan.PlanLookupSession session : getSessions()) {
            ret.addPlanList(session.getPlansForSyllabusAndCourseOfferingOnDate(syllabusId, courseOfferingId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Plans</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  plans or an error results. Otherwise, the returned list
     *  may contain only those plans that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, plans are returned that are currently
     *  effective.  In any effective mode, effective plans and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Plans</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.plan.PlanList getPlans()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.plan.plan.FederatingPlanList ret = getPlanList();

        for (org.osid.course.plan.PlanLookupSession session : getSessions()) {
            ret.addPlanList(session.getPlans());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.course.plan.plan.FederatingPlanList getPlanList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.course.plan.plan.ParallelPlanList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.course.plan.plan.CompositePlanList());
        }
    }
}
