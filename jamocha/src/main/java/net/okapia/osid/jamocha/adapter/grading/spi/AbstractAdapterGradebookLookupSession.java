//
// AbstractAdapterGradebookLookupSession.java
//
//    A Gradebook lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.grading.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Gradebook lookup session adapter.
 */

public abstract class AbstractAdapterGradebookLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.grading.GradebookLookupSession {

    private final org.osid.grading.GradebookLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterGradebookLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterGradebookLookupSession(org.osid.grading.GradebookLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@code Gradebook} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupGradebooks() {
        return (this.session.canLookupGradebooks());
    }


    /**
     *  A complete view of the {@code Gradebook} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeGradebookView() {
        this.session.useComparativeGradebookView();
        return;
    }


    /**
     *  A complete view of the {@code Gradebook} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryGradebookView() {
        this.session.usePlenaryGradebookView();
        return;
    }

     
    /**
     *  Gets the {@code Gradebook} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Gradebook} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Gradebook} and
     *  retained for compatibility.
     *
     *  @param gradebookId {@code Id} of the {@code Gradebook}
     *  @return the gradebook
     *  @throws org.osid.NotFoundException {@code gradebookId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code gradebookId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.Gradebook getGradebook(org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGradebook(gradebookId));
    }


    /**
     *  Gets a {@code GradebookList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  gradebooks specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Gradebooks} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  gradebookIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Gradebook} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code gradebookIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradebookList getGradebooksByIds(org.osid.id.IdList gradebookIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGradebooksByIds(gradebookIds));
    }


    /**
     *  Gets a {@code GradebookList} corresponding to the given
     *  gradebook genus {@code Type} which does not include
     *  gradebooks of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  gradebooks or an error results. Otherwise, the returned list
     *  may contain only those gradebooks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  gradebookGenusType a gradebook genus type 
     *  @return the returned {@code Gradebook} list
     *  @throws org.osid.NullArgumentException
     *          {@code gradebookGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradebookList getGradebooksByGenusType(org.osid.type.Type gradebookGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGradebooksByGenusType(gradebookGenusType));
    }


    /**
     *  Gets a {@code GradebookList} corresponding to the given
     *  gradebook genus {@code Type} and include any additional
     *  gradebooks with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  gradebooks or an error results. Otherwise, the returned list
     *  may contain only those gradebooks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  gradebookGenusType a gradebook genus type 
     *  @return the returned {@code Gradebook} list
     *  @throws org.osid.NullArgumentException
     *          {@code gradebookGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradebookList getGradebooksByParentGenusType(org.osid.type.Type gradebookGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGradebooksByParentGenusType(gradebookGenusType));
    }


    /**
     *  Gets a {@code GradebookList} containing the given
     *  gradebook record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  gradebooks or an error results. Otherwise, the returned list
     *  may contain only those gradebooks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  gradebookRecordType a gradebook record type 
     *  @return the returned {@code Gradebook} list
     *  @throws org.osid.NullArgumentException
     *          {@code gradebookRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradebookList getGradebooksByRecordType(org.osid.type.Type gradebookRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGradebooksByRecordType(gradebookRecordType));
    }


    /**
     *  Gets a {@code GradebookList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  gradebooks or an error results. Otherwise, the returned list
     *  may contain only those gradebooks that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Gradebook} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.grading.GradebookList getGradebooksByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGradebooksByProvider(resourceId));
    }


    /**
     *  Gets all {@code Gradebooks}. 
     *
     *  In plenary mode, the returned list contains all known
     *  gradebooks or an error results. Otherwise, the returned list
     *  may contain only those gradebooks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Gradebooks} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradebookList getGradebooks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGradebooks());
    }
}
