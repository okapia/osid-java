//
// AbstractProgramQuery.java
//
//     A template for making a Program Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.program.program.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for programs.
 */

public abstract class AbstractProgramQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOperableOsidObjectQuery
    implements org.osid.course.program.ProgramQuery {

    private final java.util.Collection<org.osid.course.program.records.ProgramQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Adds a title for this query. 
     *
     *  @param  title title string to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> title </code> not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> title </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchTitle(String title, org.osid.type.Type stringMatchType, 
                           boolean match) {
        return;
    }


    /**
     *  Matches a title that has any value. 
     *
     *  @param  match <code> true </code> to match programs with any title, 
     *          <code> false </code> to match programs with no title 
     */

    @OSID @Override
    public void matchAnyTitle(boolean match) {
        return;
    }


    /**
     *  Clears the title terms. 
     */

    @OSID @Override
    public void clearTitleTerms() {
        return;
    }


    /**
     *  Adds a programs number for this query. 
     *
     *  @param  number programs number string to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> number </code> not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> number </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchNumber(String number, org.osid.type.Type stringMatchType, 
                            boolean match) {
        return;
    }


    /**
     *  Matches a programs number that has any value. 
     *
     *  @param  match <code> true </code> to match programs with any number, 
     *          <code> false </code> to match programs with no title 
     */

    @OSID @Override
    public void matchAnyNumber(boolean match) {
        return;
    }


    /**
     *  Clears the number terms. 
     */

    @OSID @Override
    public void clearNumberTerms() {
        return;
    }


    /**
     *  Sets the resource <code> Id </code> for this query to match programs 
     *  that have a sponsor. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSponsorId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the sponsor <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSponsorIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSponsorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a sponsor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return a resource query 
     *  @throws org.osid.UnimplementedException <code> supportsSponsorQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getSponsorQuery() {
        throw new org.osid.UnimplementedException("supportsSponsorQuery() is false");
    }


    /**
     *  Matches programs that have any sponsor. 
     *
     *  @param  match <code> true </code> to match programs with any sponsor, 
     *          <code> false </code> to match programs with no sponsors 
     */

    @OSID @Override
    public void matchAnySponsor(boolean match) {
        return;
    }


    /**
     *  Clears the sponsor terms. 
     */

    @OSID @Override
    public void clearSponsorTerms() {
        return;
    }


    /**
     *  Matches programs with the prerequisites informational string. 
     *
     *  @param  requirementsInfo completion requirements string to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> requirementsInfo 
     *          </code> not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> requirementsInfo </code> 
     *          or <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchCompletionRequirementsInfo(String requirementsInfo, 
                                                org.osid.type.Type stringMatchType, 
                                                boolean match) {
        return;
    }


    /**
     *  Matches a program that has any completion requirements information 
     *  assigned. 
     *
     *  @param  match <code> true </code> to match programs with any 
     *          completion requirements information, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public void matchAnyCompletionRequirementsInfo(boolean match) {
        return;
    }


    /**
     *  Clears the completion requirements info terms. 
     */

    @OSID @Override
    public void clearCompletionRequirementsInfoTerms() {
        return;
    }


    /**
     *  Sets the requisite <code> Id </code> for this query. 
     *
     *  @param  ruleId a rule <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> ruleId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchCompletionRequirementsId(org.osid.id.Id ruleId, 
                                              boolean match) {
        return;
    }


    /**
     *  Clears the requisite <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCompletionRequirementsIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> RequisiteQuery </code> is available. 
     *
     *  @return <code> true </code> if a requisite query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompletionRequirementsQuery() {
        return (false);
    }


    /**
     *  Gets the query for a requisite. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return a requisite query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompletionRequirementsQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteQuery getCompletionRequirementsQuery() {
        throw new org.osid.UnimplementedException("supportsCompletionRequirementsQuery() is false");
    }


    /**
     *  Matches programs that have any completion requirement requisite. 
     *
     *  @param  match <code> true </code> to match programs with any 
     *          requisite, <code> false </code> to match programs with no 
     *          requisites 
     */

    @OSID @Override
    public void matchAnyCompletionRequirements(boolean match) {
        return;
    }


    /**
     *  Clears the requisite terms. 
     */

    @OSID @Override
    public void clearCompletionRequirementsTerms() {
        return;
    }


    /**
     *  Sets the credential <code> Id </code> for this query. 
     *
     *  @param  credentialId a credential <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> credentialId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCredentialId(org.osid.id.Id credentialId, boolean match) {
        return;
    }


    /**
     *  Clears the credential <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCredentialIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CredentialQuery </code> is available. 
     *
     *  @return <code> true </code> if a credential query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCredentialQuery() {
        return (false);
    }


    /**
     *  Gets the query for a credential. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return a credential query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.CredentialQuery getCredentialQuery() {
        throw new org.osid.UnimplementedException("supportsCredentialQuery() is false");
    }


    /**
     *  Matches programs that have any credentials. 
     *
     *  @param  match <code> true </code> to match programs with any 
     *          credentials, <code> false </code> to match programs with no 
     *          credentials 
     */

    @OSID @Override
    public void matchAnyCredential(boolean match) {
        return;
    }


    /**
     *  Clears the credential terms. 
     */

    @OSID @Override
    public void clearCredentialTerms() {
        return;
    }


    /**
     *  Sets the program offering <code> Id </code> for this query to match 
     *  programs that have a related program offering. 
     *
     *  @param  programOfferingId a program offering <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> programOfferingId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchProgramOfferingId(org.osid.id.Id programOfferingId, 
                                       boolean match) {
        return;
    }


    /**
     *  Clears the program offering <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearProgramOfferingIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ProgramOfferingQuery </code> is available. 
     *
     *  @return <code> true </code> if a program offering query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramOfferingQuery() {
        return (false);
    }


    /**
     *  Gets the query for a program offering. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the program offering query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramOfferingQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingQuery getProgramOfferingQuery() {
        throw new org.osid.UnimplementedException("supportsProgramOfferingQuery() is false");
    }


    /**
     *  Matches programs that have any program offering. 
     *
     *  @param  match <code> true </code> to match programs with any program 
     *          offering, <code> false </code> to match programs with no 
     *          program offering 
     */

    @OSID @Override
    public void matchAnyProgramOffering(boolean match) {
        return;
    }


    /**
     *  Clears the program offering terms. 
     */

    @OSID @Override
    public void clearProgramOfferingTerms() {
        return;
    }


    /**
     *  Sets the course catalog <code> Id </code> for this query to match 
     *  programs assigned to course catalogs. 
     *
     *  @param  courseCatalogId the course catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseCatalogId(org.osid.id.Id courseCatalogId, 
                                     boolean match) {
        return;
    }


    /**
     *  Clears the course catalog <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseCatalogIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CourseCatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a course catalog query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course catalog. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the course catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQuery getCourseCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsCourseCatalogQuery() is false");
    }


    /**
     *  Clears the course catalog terms. 
     */

    @OSID @Override
    public void clearCourseCatalogTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given program query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a program implementing the requested record.
     *
     *  @param programRecordType a program record type
     *  @return the program query record
     *  @throws org.osid.NullArgumentException
     *          <code>programRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(programRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.program.records.ProgramQueryRecord getProgramQueryRecord(org.osid.type.Type programRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.program.records.ProgramQueryRecord record : this.records) {
            if (record.implementsRecordType(programRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(programRecordType + " is not supported");
    }


    /**
     *  Adds a record to this program query. 
     *
     *  @param programQueryRecord program query record
     *  @param programRecordType program record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addProgramQueryRecord(org.osid.course.program.records.ProgramQueryRecord programQueryRecord, 
                                          org.osid.type.Type programRecordType) {

        addRecordType(programRecordType);
        nullarg(programQueryRecord, "program query record");
        this.records.add(programQueryRecord);        
        return;
    }
}
