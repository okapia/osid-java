//
// AbstractAdapterAssessmentLookupSession.java
//
//    An Assessment lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.assessment.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An Assessment lookup session adapter.
 */

public abstract class AbstractAdapterAssessmentLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.assessment.AssessmentLookupSession {

    private final org.osid.assessment.AssessmentLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterAssessmentLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterAssessmentLookupSession(org.osid.assessment.AssessmentLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Bank/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Bank Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBankId() {
        return (this.session.getBankId());
    }


    /**
     *  Gets the {@code Bank} associated with this session.
     *
     *  @return the {@code Bank} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.Bank getBank()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getBank());
    }


    /**
     *  Tests if this user can perform {@code Assessment} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupAssessments() {
        return (this.session.canLookupAssessments());
    }


    /**
     *  A complete view of the {@code Assessment} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAssessmentView() {
        this.session.useComparativeAssessmentView();
        return;
    }


    /**
     *  A complete view of the {@code Assessment} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAssessmentView() {
        this.session.usePlenaryAssessmentView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include assessments in banks which are children
     *  of this bank in the bank hierarchy.
     */

    @OSID @Override
    public void useFederatedBankView() {
        this.session.useFederatedBankView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this bank only.
     */

    @OSID @Override
    public void useIsolatedBankView() {
        this.session.useIsolatedBankView();
        return;
    }
    
     
    /**
     *  Gets the {@code Assessment} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Assessment} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Assessment} and
     *  retained for compatibility.
     *
     *  @param assessmentId {@code Id} of the {@code Assessment}
     *  @return the assessment
     *  @throws org.osid.NotFoundException {@code assessmentId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code assessmentId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.Assessment getAssessment(org.osid.id.Id assessmentId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssessment(assessmentId));
    }


    /**
     *  Gets an {@code AssessmentList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  assessments specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Assessments} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *
     *  @param  assessmentIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Assessment} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code assessmentIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentList getAssessmentsByIds(org.osid.id.IdList assessmentIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssessmentsByIds(assessmentIds));
    }


    /**
     *  Gets an {@code AssessmentList} corresponding to the given
     *  assessment genus {@code Type} which does not include
     *  assessments of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  assessments or an error results. Otherwise, the returned list
     *  may contain only those assessments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  assessmentGenusType an assessment genus type 
     *  @return the returned {@code Assessment} list
     *  @throws org.osid.NullArgumentException
     *          {@code assessmentGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentList getAssessmentsByGenusType(org.osid.type.Type assessmentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssessmentsByGenusType(assessmentGenusType));
    }


    /**
     *  Gets an {@code AssessmentList} corresponding to the given
     *  assessment genus {@code Type} and include any additional
     *  assessments with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  assessments or an error results. Otherwise, the returned list
     *  may contain only those assessments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  assessmentGenusType an assessment genus type 
     *  @return the returned {@code Assessment} list
     *  @throws org.osid.NullArgumentException
     *          {@code assessmentGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentList getAssessmentsByParentGenusType(org.osid.type.Type assessmentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssessmentsByParentGenusType(assessmentGenusType));
    }


    /**
     *  Gets an {@code AssessmentList} containing the given
     *  assessment record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  assessments or an error results. Otherwise, the returned list
     *  may contain only those assessments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  assessmentRecordType an assessment record type 
     *  @return the returned {@code Assessment} list
     *  @throws org.osid.NullArgumentException
     *          {@code assessmentRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentList getAssessmentsByRecordType(org.osid.type.Type assessmentRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssessmentsByRecordType(assessmentRecordType));
    }


    /**
     *  Gets all {@code Assessments}. 
     *
     *  In plenary mode, the returned list contains all known
     *  assessments or an error results. Otherwise, the returned list
     *  may contain only those assessments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Assessments} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentList getAssessments()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssessments());
    }
}
