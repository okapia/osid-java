//
// AbstractJobConstrainerEnablerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.rules.jobconstrainerenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractJobConstrainerEnablerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.resourcing.rules.JobConstrainerEnablerSearchResults {

    private org.osid.resourcing.rules.JobConstrainerEnablerList jobConstrainerEnablers;
    private final org.osid.resourcing.rules.JobConstrainerEnablerQueryInspector inspector;
    private final java.util.Collection<org.osid.resourcing.rules.records.JobConstrainerEnablerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractJobConstrainerEnablerSearchResults.
     *
     *  @param jobConstrainerEnablers the result set
     *  @param jobConstrainerEnablerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>jobConstrainerEnablers</code>
     *          or <code>jobConstrainerEnablerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractJobConstrainerEnablerSearchResults(org.osid.resourcing.rules.JobConstrainerEnablerList jobConstrainerEnablers,
                                            org.osid.resourcing.rules.JobConstrainerEnablerQueryInspector jobConstrainerEnablerQueryInspector) {
        nullarg(jobConstrainerEnablers, "job constrainer enablers");
        nullarg(jobConstrainerEnablerQueryInspector, "job constrainer enabler query inspectpr");

        this.jobConstrainerEnablers = jobConstrainerEnablers;
        this.inspector = jobConstrainerEnablerQueryInspector;

        return;
    }


    /**
     *  Gets the job constrainer enabler list resulting from a search.
     *
     *  @return a job constrainer enabler list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerList getJobConstrainerEnablers() {
        if (this.jobConstrainerEnablers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.resourcing.rules.JobConstrainerEnablerList jobConstrainerEnablers = this.jobConstrainerEnablers;
        this.jobConstrainerEnablers = null;
	return (jobConstrainerEnablers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.resourcing.rules.JobConstrainerEnablerQueryInspector getJobConstrainerEnablerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  job constrainer enabler search record <code> Type. </code> This method must
     *  be used to retrieve a jobConstrainerEnabler implementing the requested
     *  record.
     *
     *  @param jobConstrainerEnablerSearchRecordType a jobConstrainerEnabler search 
     *         record type 
     *  @return the job constrainer enabler search
     *  @throws org.osid.NullArgumentException
     *          <code>jobConstrainerEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(jobConstrainerEnablerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.rules.records.JobConstrainerEnablerSearchResultsRecord getJobConstrainerEnablerSearchResultsRecord(org.osid.type.Type jobConstrainerEnablerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.resourcing.rules.records.JobConstrainerEnablerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(jobConstrainerEnablerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(jobConstrainerEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record job constrainer enabler search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addJobConstrainerEnablerRecord(org.osid.resourcing.rules.records.JobConstrainerEnablerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "job constrainer enabler record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
