//
// InvariantIndexedMapProxyObstacleLookupSession
//
//    Implements an Obstacle lookup service backed by a fixed
//    collection of obstacles indexed by their types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping.path;


/**
 *  Implements a Obstacle lookup service backed by a fixed
 *  collection of obstacles. The obstacles are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some obstacles may be compatible
 *  with more types than are indicated through these obstacle
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 */

public final class InvariantIndexedMapProxyObstacleLookupSession
    extends net.okapia.osid.jamocha.core.mapping.path.spi.AbstractIndexedMapObstacleLookupSession
    implements org.osid.mapping.path.ObstacleLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantIndexedMapProxyObstacleLookupSession</code>
     *  using an array of obstacles.
     *
     *  @param  obstacles an array of obstacles
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException <code>obstacles</code> or
     *          <code>proxy</code> is <code>null</code>
     */

    public InvariantIndexedMapProxyObstacleLookupSession(org.osid.mapping.path.Obstacle[] obstacles, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putObstacles(obstacles);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantIndexedMapProxyObstacleLookupSession</code>
     *  using a collection of obstacles.
     *
     *  @param  obstacles a collection of obstacles
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException <code>obstacles</code> or
     *          <code>proxy</code> is <code>null</code>
     */

    public InvariantIndexedMapProxyObstacleLookupSession(java.util.Collection<? extends org.osid.mapping.path.Obstacle> obstacles,
                                                         org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putObstacles(obstacles);
        return;
    }
}
