//
// AbstractSubscriptionQueryInspector.java
//
//     A template for making a SubscriptionQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.subscription.subscription.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for subscriptions.
 */

public abstract class AbstractSubscriptionQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQueryInspector
    implements org.osid.subscription.SubscriptionQueryInspector {

    private final java.util.Collection<org.osid.subscription.records.SubscriptionQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the dispatch <code> Id </code> terms. 
     *
     *  @return the dispatch <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDispatchIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the dispatch terms. 
     *
     *  @return the dispatch terms 
     */

    @OSID @Override
    public org.osid.subscription.DispatchQueryInspector[] getDispatchTerms() {
        return (new org.osid.subscription.DispatchQueryInspector[0]);
    }


    /**
     *  Gets the subscriber <code> Id </code> terms. 
     *
     *  @return the resource <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSubscriberIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the subscriber terms. 
     *
     *  @return the resource terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getSubscriberTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the address <code> Id </code> terms. 
     *
     *  @return the address <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAddressIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the address terms. 
     *
     *  @return the address terms 
     */

    @OSID @Override
    public org.osid.contact.AddressQueryInspector[] getAddressTerms() {
        return (new org.osid.contact.AddressQueryInspector[0]);
    }


    /**
     *  Gets the publisher <code> Id </code> terms. 
     *
     *  @return the publisher <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPublisherIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the publisher terms. 
     *
     *  @return the publisher terms 
     */

    @OSID @Override
    public org.osid.subscription.PublisherQueryInspector[] getPublisherTerms() {
        return (new org.osid.subscription.PublisherQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given subscription query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a subscription implementing the requested record.
     *
     *  @param subscriptionRecordType a subscription record type
     *  @return the subscription query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>subscriptionRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(subscriptionRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.subscription.records.SubscriptionQueryInspectorRecord getSubscriptionQueryInspectorRecord(org.osid.type.Type subscriptionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.subscription.records.SubscriptionQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(subscriptionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(subscriptionRecordType + " is not supported");
    }


    /**
     *  Adds a record to this subscription query. 
     *
     *  @param subscriptionQueryInspectorRecord subscription query inspector
     *         record
     *  @param subscriptionRecordType subscription record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addSubscriptionQueryInspectorRecord(org.osid.subscription.records.SubscriptionQueryInspectorRecord subscriptionQueryInspectorRecord, 
                                                   org.osid.type.Type subscriptionRecordType) {

        addRecordType(subscriptionRecordType);
        nullarg(subscriptionRecordType, "subscription record type");
        this.records.add(subscriptionQueryInspectorRecord);        
        return;
    }
}
