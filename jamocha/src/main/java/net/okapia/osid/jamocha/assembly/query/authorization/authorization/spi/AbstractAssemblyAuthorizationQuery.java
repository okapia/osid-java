//
// AbstractAssemblyAuthorizationQuery.java
//
//     An AuthorizationQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.authorization.authorization.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An AuthorizationQuery that stores terms.
 */

public abstract class AbstractAssemblyAuthorizationQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRelationshipQuery
    implements org.osid.authorization.AuthorizationQuery,
               org.osid.authorization.AuthorizationQueryInspector,
               org.osid.authorization.AuthorizationSearchOrder {

    private final java.util.Collection<org.osid.authorization.records.AuthorizationQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.authorization.records.AuthorizationQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.authorization.records.AuthorizationSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyAuthorizationQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyAuthorizationQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches explciit authorizations. 
     *
     *  @param  match <code> true </code> to match explicit authorizations, 
     *          <code> false </code> to match implciit authorizations 
     */

    @OSID @Override
    public void matchExplicitAuthorizations(boolean match) {
        getAssembler().addBooleanTerm(getExplicitAuthorizationsColumn(), match);
        return;
    }


    /**
     *  Clears the explicit authorization query terms. 
     */

    @OSID @Override
    public void clearExplicitAuthorizationsTerms() {
        getAssembler().clearTerms(getExplicitAuthorizationsColumn());
        return;
    }


    /**
     *  Gets the explicit authorization query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getExplicitAuthorizationsTerms() {
        return (getAssembler().getBooleanTerms(getExplicitAuthorizationsColumn()));
    }


    /**
     *  Gets the ExplicitAuthorizations column name.
     *
     * @return the column name
     */

    protected String getExplicitAuthorizationsColumn() {
        return ("explicit_authorizations");
    }


    /**
     *  Adds an <code> Id </code> to match explicit or implicitly related 
     *  authorizations depending on <code> matchExplicitAuthorizations(). 
     *  </code> Multiple <code> Ids </code> can be added to perform a boolean 
     *  <code> OR </code> among them. 
     *
     *  @param  id <code> Id </code> to match 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> id </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchRelatedAuthorizationId(org.osid.id.Id id, boolean match) {
        getAssembler().addIdTerm(getRelatedAuthorizationIdColumn(), id, match);
        return;
    }


    /**
     *  Clears the related authorization <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRelatedAuthorizationIdTerms() {
        getAssembler().clearTerms(getRelatedAuthorizationIdColumn());
        return;
    }


    /**
     *  Gets the related authorization <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRelatedAuthorizationIdTerms() {
        return (getAssembler().getIdTerms(getRelatedAuthorizationIdColumn()));
    }


    /**
     *  Gets the RelatedAuthorizationId column name.
     *
     * @return the column name
     */

    protected String getRelatedAuthorizationIdColumn() {
        return ("related_authorization_id");
    }


    /**
     *  Tests if an <code> AuthorizationQuery </code> is available. 
     *
     *  @return <code> true </code> if an authorization query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelatedAuthorizationQuery() {
        return (false);
    }


    /**
     *  Gets the authorization query. 
     *
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @return the <code> AuthorizationQuery </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelatedAuthorizationQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationQuery getRelatedAuthorizationQuery(boolean match) {
        throw new org.osid.UnimplementedException("supportsRelatedAuthorizationQuery() is false");
    }


    /**
     *  Clears the related authorization query terms. 
     */

    @OSID @Override
    public void clearRelatedAuthorizationTerms() {
        getAssembler().clearTerms(getRelatedAuthorizationColumn());
        return;
    }


    /**
     *  Gets the related authorization query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationQueryInspector[] getRelatedAuthorizationTerms() {
        return (new org.osid.authorization.AuthorizationQueryInspector[0]);
    }


    /**
     *  Gets the RelatedAuthorization column name.
     *
     * @return the column name
     */

    protected String getRelatedAuthorizationColumn() {
        return ("related_authorization");
    }


    /**
     *  Matches the resource identified by the given <code> Id. </code> 
     *
     *  @param  resourceId the <code> Id </code> of the <code> Resource 
     *          </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResourceId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getResourceIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the resource <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearResourceIdTerms() {
        getAssembler().clearTerms(getResourceIdColumn());
        return;
    }


    /**
     *  Gets the resource <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResourceIdTerms() {
        return (getAssembler().getIdTerms(getResourceIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the resource. 
     *
     *  @param  style the search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByResource(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getResourceColumn(), style);
        return;
    }


    /**
     *  Gets the ResourceId column name.
     *
     * @return the column name
     */

    protected String getResourceIdColumn() {
        return ("resource_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceQuery() {
        return (false);
    }


    /**
     *  Gets the resource query. 
     *
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @return the <code> ResourceQuery </code> 
     *  @throws org.osid.UnimplementedException <code> supportsResourceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getResourceQuery(boolean match) {
        throw new org.osid.UnimplementedException("supportsResourceQuery() is false");
    }


    /**
     *  Matches authorizations that have any resource. 
     *
     *  @param  match <code> true </code> to match authorizations with any 
     *          resource, <code> false </code> to match authorizations with no 
     *          resource 
     */

    @OSID @Override
    public void matchAnyResource(boolean match) {
        getAssembler().addBooleanWildcardTerm(getResourceColumn(), match);
        return;
    }


    /**
     *  Clears the resource query terms. 
     */

    @OSID @Override
    public void clearResourceTerms() {
        getAssembler().clearTerms(getResourceColumn());
        return;
    }


    /**
     *  Gets the resource query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getResourceTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a <code> Resource </code> is available. 
     *
     *  @return <code> true </code> if a resource search order interface is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getResourceSearchOrder() {
        throw new org.osid.UnimplementedException("supportsResourceSearchOrder() is false");
    }


    /**
     *  Gets the Resource column name.
     *
     * @return the column name
     */

    protected String getResourceColumn() {
        return ("resource");
    }


    /**
     *  Matches the trust identified by the given <code> Id. </code> 
     *
     *  @param  trustId the <code> Id </code> of the <code> Trust </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> trustId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchTrustId(org.osid.id.Id trustId, boolean match) {
        getAssembler().addIdTerm(getTrustIdColumn(), trustId, match);
        return;
    }


    /**
     *  Matches authorizations that have any trust defined. 
     *
     *  @param  match <code> true </code> to match authorizations with any 
     *          trust, <code> false </code> to match authorizations with no 
     *          trusts 
     */

    @OSID @Override
    public void matchAnyTrustId(boolean match) {
        getAssembler().addIdWildcardTerm(getTrustIdColumn(), match);
        return;
    }


    /**
     *  Clears the trust <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearTrustIdTerms() {
        getAssembler().clearTerms(getTrustIdColumn());
        return;
    }


    /**
     *  Gets the trust <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getTrustIdTerms() {
        return (getAssembler().getIdTerms(getTrustIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the trust. 
     *
     *  @param  style the search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTrust(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getTrustColumn(), style);
        return;
    }


    /**
     *  Gets the Trust Id column name.
     *
     *  @return the column name
     */

    protected String getTrustIdColumn() {
        return ("trust_id");
    }


    /**
     *  Gets the Trust column name.
     *
     *  @return the column name
     */

    protected String getTrustColumn() {
        return ("trust");
    }


    /**
     *  Matches the agent identified by the given <code> Id. </code> 
     *
     *  @param  agentId the Id of the <code> Agent </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAgentId(org.osid.id.Id agentId, boolean match) {
        getAssembler().addIdTerm(getAgentIdColumn(), agentId, match);
        return;
    }


    /**
     *  Clears the agent <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAgentIdTerms() {
        getAssembler().clearTerms(getAgentIdColumn());
        return;
    }


    /**
     *  Gets the agent <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAgentIdTerms() {
        return (getAssembler().getIdTerms(getAgentIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the agent. 
     *
     *  @param  style the search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAgent(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getAgentColumn(), style);
        return;
    }


    /**
     *  Gets the AgentId column name.
     *
     * @return the column name
     */

    protected String getAgentIdColumn() {
        return ("agent_id");
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgentQuery() {
        return (false);
    }


    /**
     *  Gets the agent query. 
     *
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @return the <code> AgentQuery </code> 
     *  @throws org.osid.UnimplementedException <code> supportsAgentQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getAgentQuery(boolean match) {
        throw new org.osid.UnimplementedException("supportsAgentQuery() is false");
    }


    /**
     *  Matches authorizations that have any agent. 
     *
     *  @param  match <code> true </code> to match authorizations with any 
     *          agent, <code> false </code> to match authorizations with no 
     *          agent 
     */

    @OSID @Override
    public void matchAnyAgent(boolean match) {
        getAssembler().addBooleanWildcardTerm(getAgentColumn(), match);
        return;
    }


    /**
     *  Clears the agent query terms. 
     */

    @OSID @Override
    public void clearAgentTerms() {
        getAssembler().clearTerms(getAgentColumn());
        return;
    }


    /**
     *  Gets the agent query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authentication.AgentQueryInspector[] getAgentTerms() {
        return (new org.osid.authentication.AgentQueryInspector[0]);
    }


    /**
     *  Tests if an <code> Agent </code> is available. 
     *
     *  @return <code> true </code> if an agent search order interface is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the agent search order. 
     *
     *  @return the agent search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAgentSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getAgentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsAgentSearchOrder() is false");
    }


    /**
     *  Gets the Agent column name.
     *
     * @return the column name
     */

    protected String getAgentColumn() {
        return ("agent");
    }


    /**
     *  Matches the function identified by the given <code> Id. </code> 
     *
     *  @param  functionId the Id of the <code> Function </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> functionId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchFunctionId(org.osid.id.Id functionId, boolean match) {
        getAssembler().addIdTerm(getFunctionIdColumn(), functionId, match);
        return;
    }


    /**
     *  Clears the function <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearFunctionIdTerms() {
        getAssembler().clearTerms(getFunctionIdColumn());
        return;
    }


    /**
     *  Gets the function <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getFunctionIdTerms() {
        return (getAssembler().getIdTerms(getFunctionIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the active 
     *  status. 
     *
     *  @param  style the search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByFunction(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getFunctionColumn(), style);
        return;
    }


    /**
     *  Gets the FunctionId column name.
     *
     * @return the column name
     */

    protected String getFunctionIdColumn() {
        return ("function_id");
    }


    /**
     *  Tests if a <code> FunctionQuery </code> is available. 
     *
     *  @return <code> true </code> if a function query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFunctionQuery() {
        return (false);
    }


    /**
     *  Gets the function query. 
     *
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @return the <code> FunctinQuery </code> 
     *  @throws org.osid.UnimplementedException <code> supportsFunctionQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.FunctionQuery getFunctionQuery(boolean match) {
        throw new org.osid.UnimplementedException("supportsFunctionQuery() is false");
    }


    /**
     *  Clears the function query terms. 
     */

    @OSID @Override
    public void clearFunctionTerms() {
        getAssembler().clearTerms(getFunctionColumn());
        return;
    }


    /**
     *  Gets the function query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authorization.FunctionQueryInspector[] getFunctionTerms() {
        return (new org.osid.authorization.FunctionQueryInspector[0]);
    }


    /**
     *  Tests if a <code> Function </code> is available. 
     *
     *  @return <code> true </code> if a function search order interface is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFunctionSearchOrder() {
        return (false);
    }


    /**
     *  Gets the function search order. 
     *
     *  @return the function search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFunctionSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.FunctionSearchOrder getFunctionSearchOrder() {
        throw new org.osid.UnimplementedException("supportsFunctionSearchOrder() is false");
    }


    /**
     *  Gets the Function column name.
     *
     * @return the column name
     */

    protected String getFunctionColumn() {
        return ("function");
    }


    /**
     *  Matches the qualifier identified by the given <code> Id. </code> 
     *
     *  @param  qualifierId the Id of the <code> Qualifier </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> qualifierId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchQualifierId(org.osid.id.Id qualifierId, boolean match) {
        getAssembler().addIdTerm(getQualifierIdColumn(), qualifierId, match);
        return;
    }


    /**
     *  Clears the qualifier <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearQualifierIdTerms() {
        getAssembler().clearTerms(getQualifierIdColumn());
        return;
    }


    /**
     *  Gets the qualifier <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getQualifierIdTerms() {
        return (getAssembler().getIdTerms(getQualifierIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the qualifier, 
     *
     *  @param  style the search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByQualifier(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getQualifierColumn(), style);
        return;
    }


    /**
     *  Gets the QualifierId column name.
     *
     * @return the column name
     */

    protected String getQualifierIdColumn() {
        return ("qualifier_id");
    }


    /**
     *  Tests if a <code> QualifierQuery </code> is available. 
     *
     *  @return <code> true </code> if a qualifier query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQualifierQuery() {
        return (false);
    }


    /**
     *  Gets the qualiier query. 
     *
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @return the <code> QualifierQuery </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierQuery getQualifierQuery(boolean match) {
        throw new org.osid.UnimplementedException("supportsQualifierQuery() is false");
    }


    /**
     *  Clears the qualifier query terms. 
     */

    @OSID @Override
    public void clearQualifierTerms() {
        getAssembler().clearTerms(getQualifierColumn());
        return;
    }


    /**
     *  Gets the qualifier query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authorization.QualifierQueryInspector[] getQualifierTerms() {
        return (new org.osid.authorization.QualifierQueryInspector[0]);
    }


    /**
     *  Tests if a <code> Qualifier </code> is available. 
     *
     *  @return <code> true </code> if a qualifier search order interface is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQualifierSearchOrder() {
        return (false);
    }


    /**
     *  Gets the qualifier search order. 
     *
     *  @return the qualifier search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierSearchOrder getQualifierSearchOrder() {
        throw new org.osid.UnimplementedException("supportsQualifierSearchOrder() is false");
    }


    /**
     *  Gets the Qualifier column name.
     *
     * @return the column name
     */

    protected String getQualifierColumn() {
        return ("qualifier");
    }


    /**
     *  Sets the vault <code> Id </code> for this query. 
     *
     *  @param  vaultId a vault <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchVaultId(org.osid.id.Id vaultId, boolean match) {
        getAssembler().addIdTerm(getVaultIdColumn(), vaultId, match);
        return;
    }


    /**
     *  Clears the vault <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearVaultIdTerms() {
        getAssembler().clearTerms(getVaultIdColumn());
        return;
    }


    /**
     *  Gets the vault <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getVaultIdTerms() {
        return (getAssembler().getIdTerms(getVaultIdColumn()));
    }


    /**
     *  Gets the VaultId column name.
     *
     * @return the column name
     */

    protected String getVaultIdColumn() {
        return ("vault_id");
    }


    /**
     *  Tests if a <code> VaultQuery </code> is available. 
     *
     *  @return <code> true </code> if a vault query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVaultQuery() {
        return (false);
    }


    /**
     *  Gets the query for a vault. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the vault query 
     *  @throws org.osid.UnimplementedException <code> supportsVaultQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.VaultQuery getVaultQuery() {
        throw new org.osid.UnimplementedException("supportsVaultQuery() is false");
    }


    /**
     *  Clears the vault query terms. 
     */

    @OSID @Override
    public void clearVaultTerms() {
        getAssembler().clearTerms(getVaultColumn());
        return;
    }


    /**
     *  Gets the vault query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authorization.VaultQueryInspector[] getVaultTerms() {
        return (new org.osid.authorization.VaultQueryInspector[0]);
    }


    /**
     *  Gets the Vault column name.
     *
     * @return the column name
     */

    protected String getVaultColumn() {
        return ("vault");
    }


    /**
     *  Tests if this authorization supports the given record
     *  <code>Type</code>.
     *
     *  @param  authorizationRecordType an authorization record type 
     *  @return <code>true</code> if the authorizationRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>authorizationRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type authorizationRecordType) {
        for (org.osid.authorization.records.AuthorizationQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(authorizationRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  authorizationRecordType the authorization record type 
     *  @return the authorization query record 
     *  @throws org.osid.NullArgumentException
     *          <code>authorizationRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(authorizationRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.authorization.records.AuthorizationQueryRecord getAuthorizationQueryRecord(org.osid.type.Type authorizationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.authorization.records.AuthorizationQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(authorizationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(authorizationRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  authorizationRecordType the authorization record type 
     *  @return the authorization query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>authorizationRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(authorizationRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.authorization.records.AuthorizationQueryInspectorRecord getAuthorizationQueryInspectorRecord(org.osid.type.Type authorizationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.authorization.records.AuthorizationQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(authorizationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(authorizationRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param authorizationRecordType the authorization record type
     *  @return the authorization search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>authorizationRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(authorizationRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.authorization.records.AuthorizationSearchOrderRecord getAuthorizationSearchOrderRecord(org.osid.type.Type authorizationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.authorization.records.AuthorizationSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(authorizationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(authorizationRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this authorization. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param authorizationQueryRecord the authorization query record
     *  @param authorizationQueryInspectorRecord the authorization query inspector
     *         record
     *  @param authorizationSearchOrderRecord the authorization search order record
     *  @param authorizationRecordType authorization record type
     *  @throws org.osid.NullArgumentException
     *          <code>authorizationQueryRecord</code>,
     *          <code>authorizationQueryInspectorRecord</code>,
     *          <code>authorizationSearchOrderRecord</code> or
     *          <code>authorizationRecordTypeauthorization</code> is
     *          <code>null</code>
     */
            
    protected void addAuthorizationRecords(org.osid.authorization.records.AuthorizationQueryRecord authorizationQueryRecord, 
                                      org.osid.authorization.records.AuthorizationQueryInspectorRecord authorizationQueryInspectorRecord, 
                                      org.osid.authorization.records.AuthorizationSearchOrderRecord authorizationSearchOrderRecord, 
                                      org.osid.type.Type authorizationRecordType) {

        addRecordType(authorizationRecordType);

        nullarg(authorizationQueryRecord, "authorization query record");
        nullarg(authorizationQueryInspectorRecord, "authorization query inspector record");
        nullarg(authorizationSearchOrderRecord, "authorization search odrer record");

        this.queryRecords.add(authorizationQueryRecord);
        this.queryInspectorRecords.add(authorizationQueryInspectorRecord);
        this.searchOrderRecords.add(authorizationSearchOrderRecord);
        
        return;
    }
}
