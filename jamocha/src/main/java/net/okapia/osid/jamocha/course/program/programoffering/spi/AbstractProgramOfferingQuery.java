//
// AbstractProgramOfferingQuery.java
//
//     A template for making a ProgramOffering Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.program.programoffering.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for program offerings.
 */

public abstract class AbstractProgramOfferingQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQuery
    implements org.osid.course.program.ProgramOfferingQuery {

    private final java.util.Collection<org.osid.course.program.records.ProgramOfferingQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the program <code> Id </code> for this query to match program 
     *  offerings that have a related program. 
     *
     *  @param  programId a program <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> programId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchProgramId(org.osid.id.Id programId, boolean match) {
        return;
    }


    /**
     *  Clears the program <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearProgramIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ProgramQuery </code> is available. 
     *
     *  @return <code> true </code> if a program query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramQuery() {
        return (false);
    }


    /**
     *  Gets the query for a program. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the program query 
     *  @throws org.osid.UnimplementedException <code> supportsProgramQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramQuery getProgramQuery() {
        throw new org.osid.UnimplementedException("supportsProgramQuery() is false");
    }


    /**
     *  Clears the program terms. 
     */

    @OSID @Override
    public void clearProgramTerms() {
        return;
    }


    /**
     *  Sets the term <code> Id </code> for this query to match program 
     *  offerings that have a related term. 
     *
     *  @param  termId a term <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> termId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchTermId(org.osid.id.Id termId, boolean match) {
        return;
    }


    /**
     *  Clears the term <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearTermIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> TermQuery </code> is available. 
     *
     *  @return <code> true </code> if a term query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTermQuery() {
        return (false);
    }


    /**
     *  Gets the query for a reporting term. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the term query 
     *  @throws org.osid.UnimplementedException <code> supportsTermQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermQuery getTermQuery() {
        throw new org.osid.UnimplementedException("supportsTermQuery() is false");
    }


    /**
     *  Clears the reporting term terms. 
     */

    @OSID @Override
    public void clearTermTerms() {
        return;
    }


    /**
     *  Adds a title for this query. 
     *
     *  @param  title title string to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> title </code> not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> title </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchTitle(String title, org.osid.type.Type stringMatchType, 
                           boolean match) {
        return;
    }


    /**
     *  Matches a title that has any value. 
     *
     *  @param  match <code> true </code> to match program offerings with any 
     *          title, <code> false </code> to match program offerings with no 
     *          title 
     */

    @OSID @Override
    public void matchAnyTitle(boolean match) {
        return;
    }


    /**
     *  Clears the title terms. 
     */

    @OSID @Override
    public void clearTitleTerms() {
        return;
    }


    /**
     *  Adds a program number for this query. 
     *
     *  @param  number program number string to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> number </code> not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> number </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchNumber(String number, org.osid.type.Type stringMatchType, 
                            boolean match) {
        return;
    }


    /**
     *  Matches a program number that has any value. 
     *
     *  @param  match <code> true </code> to match program offerings with any 
     *          number, <code> false </code> to match program offerings with 
     *          no number 
     */

    @OSID @Override
    public void matchAnyNumber(boolean match) {
        return;
    }


    /**
     *  Clears the number terms. 
     */

    @OSID @Override
    public void clearNumberTerms() {
        return;
    }


    /**
     *  Sets the resource <code> Id </code> for this query to match program 
     *  offerings that have an instructor. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchInstructorId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the instructor <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearInstructorIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInstructorQuery() {
        return (false);
    }


    /**
     *  Gets the query for an instructor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return a resource query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstructorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getInstructorQuery() {
        throw new org.osid.UnimplementedException("supportsInstructorQuery() is false");
    }


    /**
     *  Matches program offerings that have any instructor. 
     *
     *  @param  match <code> true </code> to match program offerings with any 
     *          instructor, <code> false </code> to match program offerings 
     *          with no instructors 
     */

    @OSID @Override
    public void matchAnyInstructor(boolean match) {
        return;
    }


    /**
     *  Clears the instructor terms. 
     */

    @OSID @Override
    public void clearInstructorTerms() {
        return;
    }


    /**
     *  Sets the resource <code> Id </code> for this query to match programs 
     *  that have a sponsor. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSponsorId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the sponsor <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSponsorIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSponsorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a sponsor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return a resource query 
     *  @throws org.osid.UnimplementedException <code> supportsSponsorQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getSponsorQuery() {
        throw new org.osid.UnimplementedException("supportsSponsorQuery() is false");
    }


    /**
     *  Matches programs that have any sponsor. 
     *
     *  @param  match <code> true </code> to match programs with any sponsor, 
     *          <code> false </code> to match programs with no sponsors 
     */

    @OSID @Override
    public void matchAnySponsor(boolean match) {
        return;
    }


    /**
     *  Clears the sponsor terms. 
     */

    @OSID @Override
    public void clearSponsorTerms() {
        return;
    }


    /**
     *  Matches programs with the prerequisites informational string. 
     *
     *  @param  requirementsInfo completion requirements string to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> requirementsInfo 
     *          </code> not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> requirementsInfo </code> 
     *          or <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchCompletionRequirementsInfo(String requirementsInfo, 
                                                org.osid.type.Type stringMatchType, 
                                                boolean match) {
        return;
    }


    /**
     *  Matches a program that has any completion requirements information 
     *  assigned. 
     *
     *  @param  match <code> true </code> to match programs with any 
     *          completion requirements information, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public void matchAnyCompletionRequirementsInfo(boolean match) {
        return;
    }


    /**
     *  Clears the completion requirements info terms. 
     */

    @OSID @Override
    public void clearCompletionRequirementsInfoTerms() {
        return;
    }


    /**
     *  Sets the requisite <code> Id </code> for this query. 
     *
     *  @param  ruleId a rule <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> ruleId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchCompletionRequirementsId(org.osid.id.Id ruleId, 
                                              boolean match) {
        return;
    }


    /**
     *  Clears the requisite <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCompletionRequirementsIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> RequisiteQuery </code> is available. 
     *
     *  @return <code> true </code> if a requisite query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompletionRequirementsQuery() {
        return (false);
    }


    /**
     *  Gets the query for a requisite. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return a requisite query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompletionRequirementsQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteQuery getCompletionRequirementsQuery() {
        throw new org.osid.UnimplementedException("supportsCompletionRequirementsQuery() is false");
    }


    /**
     *  Matches programs that have any completion requirement requisite. 
     *
     *  @param  match <code> true </code> to match programs with any 
     *          requisite, <code> false </code> to match programs with no 
     *          requisites 
     */

    @OSID @Override
    public void matchAnyCompletionRequirements(boolean match) {
        return;
    }


    /**
     *  Clears the requisite terms. 
     */

    @OSID @Override
    public void clearCompletionRequirementsTerms() {
        return;
    }


    /**
     *  Sets the credential <code> Id </code> for this query. 
     *
     *  @param  credentialId a credential <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> credentialId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCredentialId(org.osid.id.Id credentialId, boolean match) {
        return;
    }


    /**
     *  Clears the credential <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCredentialIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CredentialQuery </code> is available. 
     *
     *  @return <code> true </code> if a credential query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCredentialQuery() {
        return (false);
    }


    /**
     *  Gets the query for a credential. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return a credential query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.CredentialQuery getCredentialQuery() {
        throw new org.osid.UnimplementedException("supportsCredentialQuery() is false");
    }


    /**
     *  Matches programs that have any credentials. 
     *
     *  @param  match <code> true </code> to match programs with any 
     *          credentials, <code> false </code> to match programs with no 
     *          credentials 
     */

    @OSID @Override
    public void matchAnyCredential(boolean match) {
        return;
    }


    /**
     *  Clears the credential terms. 
     */

    @OSID @Override
    public void clearCredentialTerms() {
        return;
    }


    /**
     *  Matches program offerings that require registration. 
     *
     *  @param  match <code> true </code> to match program offerings requiring 
     *          registration,, <code> false </code> to match program offerings 
     *          not requiring registration 
     */

    @OSID @Override
    public void matchRequiresRegistration(boolean match) {
        return;
    }


    /**
     *  Clears the requires registration terms. 
     */

    @OSID @Override
    public void clearRequiresRegistrationTerms() {
        return;
    }


    /**
     *  Matches program offerings with minimum seating between the given 
     *  numbers inclusive. 
     *
     *  @param  min low number 
     *  @param  max high number 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> max </code> is less 
     *          than <code> min </code> 
     */

    @OSID @Override
    public void matchMinimumSeats(long min, long max, boolean match) {
        return;
    }


    /**
     *  Matches a program offering that has any minimum seating assigned. 
     *
     *  @param  match <code> true </code> to match program offerings with any 
     *          minimum seats, <code> false </code> to match program offerings 
     *          with no minimum seats 
     */

    @OSID @Override
    public void matchAnyMinimumSeats(boolean match) {
        return;
    }


    /**
     *  Clears the minimum seats terms. 
     */

    @OSID @Override
    public void clearMinimumSeatsTerms() {
        return;
    }


    /**
     *  Matches program offerings with maximum seating between the given 
     *  numbers inclusive. 
     *
     *  @param  min low number 
     *  @param  max high number 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> max </code> is less 
     *          than <code> min </code> 
     */

    @OSID @Override
    public void matchMaximumSeats(long min, long max, boolean match) {
        return;
    }


    /**
     *  Matches a program offering that has any maximum seating assigned. 
     *
     *  @param  match <code> true </code> to match program offerings with any 
     *          maximum seats, <code> false </code> to match program offerings 
     *          with no maximum seats 
     */

    @OSID @Override
    public void matchAnyMaximumSeats(boolean match) {
        return;
    }


    /**
     *  Clears the maximum seats terms. 
     */

    @OSID @Override
    public void clearMaximumSeatsTerms() {
        return;
    }


    /**
     *  Adds a class url for this query. 
     *
     *  @param  url url string to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> url </code> not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> url </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchURL(String url, org.osid.type.Type stringMatchType, 
                         boolean match) {
        return;
    }


    /**
     *  Matches a url that has any value. 
     *
     *  @param  match <code> true </code> to match program offerings with any 
     *          url, <code> false </code> to match program offerings with no 
     *          url 
     */

    @OSID @Override
    public void matchAnyURL(boolean match) {
        return;
    }


    /**
     *  Clears the url terms. 
     */

    @OSID @Override
    public void clearURLTerms() {
        return;
    }


    /**
     *  Sets the course catalog <code> Id </code> for this query to match 
     *  program offerings assigned to course catalogs. 
     *
     *  @param  courseCatalogId the course catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseCatalogId(org.osid.id.Id courseCatalogId, 
                                     boolean match) {
        return;
    }


    /**
     *  Clears the course catalog <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseCatalogIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CourseCatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a course catalog query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course catalog. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the course catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQuery getCourseCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsCourseCatalogQuery() is false");
    }


    /**
     *  Clears the course catalog terms. 
     */

    @OSID @Override
    public void clearCourseCatalogTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given program offering query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a program offering implementing the requested record.
     *
     *  @param programOfferingRecordType a program offering record type
     *  @return the program offering query record
     *  @throws org.osid.NullArgumentException
     *          <code>programOfferingRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(programOfferingRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.program.records.ProgramOfferingQueryRecord getProgramOfferingQueryRecord(org.osid.type.Type programOfferingRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.program.records.ProgramOfferingQueryRecord record : this.records) {
            if (record.implementsRecordType(programOfferingRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(programOfferingRecordType + " is not supported");
    }


    /**
     *  Adds a record to this program offering query. 
     *
     *  @param programOfferingQueryRecord program offering query record
     *  @param programOfferingRecordType programOffering record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addProgramOfferingQueryRecord(org.osid.course.program.records.ProgramOfferingQueryRecord programOfferingQueryRecord, 
                                          org.osid.type.Type programOfferingRecordType) {

        addRecordType(programOfferingRecordType);
        nullarg(programOfferingQueryRecord, "program offering query record");
        this.records.add(programOfferingQueryRecord);        
        return;
    }
}
