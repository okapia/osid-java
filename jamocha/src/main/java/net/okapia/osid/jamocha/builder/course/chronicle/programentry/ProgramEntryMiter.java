//
// ProgramEntryMiter.java
//
//     Defines a ProgramEntry miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.chronicle.programentry;


/**
 *  Defines a <code>ProgramEntry</code> miter for use with the builders.
 */

public interface ProgramEntryMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidRelationshipMiter,
            org.osid.course.chronicle.ProgramEntry {


    /**
     *  Sets the student.
     *
     *  @param student a student
     *  @throws org.osid.NullArgumentException <code>student</code> is
     *          <code>null</code>
     */

    public void setStudent(org.osid.resource.Resource student);


    /**
     *  Sets the program.
     *
     *  @param program a program
     *  @throws org.osid.NullArgumentException <code>program</code> is
     *          <code>null</code>
     */

    public void setProgram(org.osid.course.program.Program program);


    /**
     *  Sets the admission date.
     *
     *  @param date an admission date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public void setAdmissionDate(org.osid.calendaring.DateTime date);


    /**
     *  Sets the term.
     *
     *  @param term a term
     *  @throws org.osid.NullArgumentException <code>term</code> is
     *          <code>null</code>
     */

    public void setTerm(org.osid.course.Term term);


    /**
     *  Sets the credit scale.
     *
     *  @param scale a credit scale
     *  @throws org.osid.NullArgumentException <code>scale</code> is
     *          <code>null</code>
     */

    public void setCreditScale(org.osid.grading.GradeSystem scale);


    /**
     *  Sets the credits earned.
     *
     *  @param earned a credits earned
     *  @throws org.osid.NullArgumentException <code>earned</code> is
     *          <code>null</code>
     */

    public void setCreditsEarned(java.math.BigDecimal earned);


    /**
     *  Sets the g pa scale.
     *
     *  @param scale a gpa scale
     *  @throws org.osid.NullArgumentException <code>scale</code> is
     *          <code>null</code>
     */

    public void setGPAScale(org.osid.grading.GradeSystem scale);


    /**
     *  Sets the GPA.
     *
     *  @param gpa a GPA
     *  @throws org.osid.NullArgumentException <code>gpa</code> is
     *          <code>null</code>
     */

    public void setGPA(java.math.BigDecimal gpa);


    /**
     *  Adds an enrollment.
     *
     *  @param enrollment an enrollment
     *  @throws org.osid.NullArgumentException <code>enrollment</code>
     *          is <code>null</code>
     */

    public void addEnrollment(org.osid.course.program.Enrollment enrollment);


    /**
     *  Sets all the enrollments.
     *
     *  @param enrollments a collection of enrollments
     *  @throws org.osid.NullArgumentException
     *          <code>enrollments</code> is <code>null</code>
     */

    public void setEnrollments(java.util.Collection<org.osid.course.program.Enrollment> enrollments);


    /**
     *  Adds a ProgramEntry record.
     *
     *  @param record a programEntry record
     *  @param recordType the type of programEntry record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addProgramEntryRecord(org.osid.course.chronicle.records.ProgramEntryRecord record, org.osid.type.Type recordType);
}       


