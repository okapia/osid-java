//
// AbstractConfigurationQuery.java
//
//     A template for making a Configuration Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.configuration.configuration.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for configurations.
 */

public abstract class AbstractConfigurationQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQuery
    implements org.osid.configuration.ConfigurationQuery {

    private final java.util.Collection<org.osid.configuration.records.ConfigurationQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches configurations which are parameter registries. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public void matchRegistry(boolean match) {
        return;
    }


    /**
     *  Clears the registry terms. 
     */

    @OSID @Override
    public void clearRegistryTerms() {
        return;
    }


    /**
     *  Adds a parameter <code> Id </code> for this query. 
     *
     *  @param  parameterId a parameter <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> parameterId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchParameterId(org.osid.id.Id parameterId, boolean match) {
        return;
    }


    /**
     *  Clears the parameter <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearParameterIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ParameterQuery </code> is available. 
     *
     *  @return <code> true </code> if a parameter query interface is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterQuery() {
        return (false);
    }


    /**
     *  Gets the query interface for a parameter. 
     *
     *  @return the parameter query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ParameterQuery getParameterQuery() {
        throw new org.osid.UnimplementedException("supportsParameterQuery() is false");
    }


    /**
     *  Matches configurations that have any parameter. 
     *
     *  @param  match <code> true </code> to match configurations with any 
     *          parameter, <code> false </code> to match configurations with 
     *          no parameter 
     */

    @OSID @Override
    public void matchAnyParameter(boolean match) {
        return;
    }


    /**
     *  Clears the parameter terms. 
     */

    @OSID @Override
    public void clearParameterTerms() {
        return;
    }


    /**
     *  Adds a configuration <code> Id </code> for this query to match 
     *  configurations which have as an ancestor the specified configuration. 
     *
     *  @param  configurationId a configuration <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorConfigurationId(org.osid.id.Id configurationId, 
                                             boolean match) {
        return;
    }


    /**
     *  Clears the ancestor configuration <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorConfigurationIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ConfigurationQuery </code> is available. 
     *
     *  @return <code> true </code> if a configuration query interface is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorConfigurationQuery() {
        return (false);
    }


    /**
     *  Gets the query interface for a configuration. 
     *
     *  @return the configuration query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorConfigurationQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationQuery getAncestorConfigurationQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorConfigurationQuery() is false");
    }


    /**
     *  Matches configurations that have any ancestor. 
     *
     *  @param  match <code> true </code> to match configurations with any 
     *          ancestor, <code> false </code> to match root configurations 
     */

    @OSID @Override
    public void matchAnyAncestorConfiguration(boolean match) {
        return;
    }


    /**
     *  Clears the ancestor configuration terms. 
     */

    @OSID @Override
    public void clearAncestorConfigurationTerms() {
        return;
    }


    /**
     *  Adds a configuration <code> Id </code> for this query to match 
     *  configurations which have as a descendant the specified configuration. 
     *
     *  @param  configurationId a configuration <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantConfigurationId(org.osid.id.Id configurationId, 
                                               boolean match) {
        return;
    }


    /**
     *  Clears the descendant configuration <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantConfigurationIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ConfigurationQuery </code> is available. 
     *
     *  @return <code> true </code> if a configuration query interface is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantConfigurationQuery() {
        return (false);
    }


    /**
     *  Gets the query interface for a configuration. 
     *
     *  @return the configuration query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantConfigurationQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationQuery getDescendantConfigurationQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantConfigurationQuery() is false");
    }


    /**
     *  Matches configurations that have any descendant. 
     *
     *  @param  match <code> true </code> to match configurations with any 
     *          descendant, <code> false </code> to match leaf configurations 
     */

    @OSID @Override
    public void matchAnyDescendantConfiguration(boolean match) {
        return;
    }


    /**
     *  Clears the descendant configuration terms. 
     */

    @OSID @Override
    public void clearDescendantConfigurationTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given configuration query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a configuration implementing the requested record.
     *
     *  @param configurationRecordType a configuration record type
     *  @return the configuration query record
     *  @throws org.osid.NullArgumentException
     *          <code>configurationRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(configurationRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.configuration.records.ConfigurationQueryRecord getConfigurationQueryRecord(org.osid.type.Type configurationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.configuration.records.ConfigurationQueryRecord record : this.records) {
            if (record.implementsRecordType(configurationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(configurationRecordType + " is not supported");
    }


    /**
     *  Adds a record to this configuration query. 
     *
     *  @param configurationQueryRecord configuration query record
     *  @param configurationRecordType configuration record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addConfigurationQueryRecord(org.osid.configuration.records.ConfigurationQueryRecord configurationQueryRecord, 
                                          org.osid.type.Type configurationRecordType) {

        addRecordType(configurationRecordType);
        nullarg(configurationQueryRecord, "configuration query record");
        this.records.add(configurationQueryRecord);        
        return;
    }
}
