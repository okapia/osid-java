//
// InputValidator.java
//
//     Validates an Input.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.control.input;


/**
 *  Validates an Input.
 */

public final class InputValidator
    extends net.okapia.osid.jamocha.builder.validator.control.input.spi.AbstractInputValidator {


    /**
     *  Constructs a new <code>InputValidator</code>.
     */

    public InputValidator() {
        return;
    }


    /**
     *  Constructs a new <code>InputValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    public InputValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates an Input with a default validation.
     *
     *  @param input an input to validate
     *  @return the input
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>input</code>
     *          is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred
     */

    public static org.osid.control.Input validateInput(org.osid.control.Input input) {
        InputValidator validator = new InputValidator();
        validator.validate(input);
        return (input);
    }


    /**
     *  Validates an Input for the given validations.
     *
     *  @param validation an EnumSet of validations
     *  @param input an input to validate
     *  @return the input
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          or <code>input</code> is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred
     */

    public static org.osid.control.Input validateInput(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation,
                                                       org.osid.control.Input input) {

        InputValidator validator = new InputValidator(validation);
        validator.validate(input);
        return (input);
    }
}
