//
// AbstractAdapterAuthorizationLookupSession.java
//
//    An Authorization lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.authorization.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An Authorization lookup session adapter.
 */

public abstract class AbstractAdapterAuthorizationLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.authorization.AuthorizationLookupSession {

    private final org.osid.authorization.AuthorizationLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterAuthorizationLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterAuthorizationLookupSession(org.osid.authorization.AuthorizationLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Vault/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Vault Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getVaultId() {
        return (this.session.getVaultId());
    }


    /**
     *  Gets the {@code Vault} associated with this session.
     *
     *  @return the {@code Vault} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.Vault getVault()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getVault());
    }


    /**
     *  Tests if this user can perform {@code Authorization} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupAuthorizations() {
        return (this.session.canLookupAuthorizations());
    }


    /**
     *  A complete view of the {@code Authorization} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAuthorizationView() {
        this.session.useComparativeAuthorizationView();
        return;
    }


    /**
     *  A complete view of the {@code Authorization} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAuthorizationView() {
        this.session.usePlenaryAuthorizationView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include authorizations in vaults which are children
     *  of this vault in the vault hierarchy.
     */

    @OSID @Override
    public void useFederatedVaultView() {
        this.session.useFederatedVaultView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this vault only.
     */

    @OSID @Override
    public void useIsolatedVaultView() {
        this.session.useIsolatedVaultView();
        return;
    }
    

    /**
     *  Only authorizations whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveAuthorizationView() {
        this.session.useEffectiveAuthorizationView();
        return;
    }
    

    /**
     *  All authorizations of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveAuthorizationView() {
        this.session.useAnyEffectiveAuthorizationView();
        return;
    }


    /**
     *  Sets the view for methods in this session to implicit
     *  authorizations.  An implicit view will include authorizations
     *  derived from other authorizations as a result of the {@code
     *  Qualifier}, {@code Function} or {@code Resource}
     *  hierarchies. This method is the opposite of {@code
     *  explicitAuthorizationView()}.
     */

    @OSID @Override
    public void useImplicitAuthorizationView() {
        this.session.useImplicitAuthorizationView();
        return;
    }


    /**
     *  Sets the view for methods in this session to explicit
     *  authorizations.  An explicit view includes only those
     *  authorizations that were explicitly defined and not
     *  implied. This method is the opposite of {@code
     *  implicitAuthorizationView()}.
     */

    public void useExplicitAuthorizationView() {
        this.session.useExplicitAuthorizationView();
        return;
    }

     
    /**
     *  Gets the {@code Authorization} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Authorization} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Authorization} and
     *  retained for compatibility.
     *
     *  In effective mode, authorizations are returned that are currently
     *  effective.  In any effective mode, effective authorizations and
     *  those currently expired are returned.
     *
     *  @param authorizationId {@code Id} of the {@code Authorization}
     *  @return the authorization
     *  @throws org.osid.NotFoundException {@code authorizationId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code authorizationId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.Authorization getAuthorization(org.osid.id.Id authorizationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuthorization(authorizationId));
    }


    /**
     *  Gets an {@code AuthorizationList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  authorizations specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Authorizations} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, authorizations are returned that are
     *  currently effective.  In any effective mode, effective
     *  authorizations and those currently expired are returned.
     *
     *  @param  authorizationIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Authorization} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code authorizationIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsByIds(org.osid.id.IdList authorizationIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuthorizationsByIds(authorizationIds));
    }


    /**
     *  Gets an {@code AuthorizationList} corresponding to the given
     *  authorization genus {@code Type} which does not include
     *  authorizations of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned list
     *  may contain only those authorizations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, authorizations are returned that are currently
     *  effective.  In any effective mode, effective authorizations and
     *  those currently expired are returned.
     *
     *  @param  authorizationGenusType an authorization genus type 
     *  @return the returned {@code Authorization} list
     *  @throws org.osid.NullArgumentException
     *          {@code authorizationGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsByGenusType(org.osid.type.Type authorizationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuthorizationsByGenusType(authorizationGenusType));
    }


    /**
     *  Gets an {@code AuthorizationList} corresponding to the given
     *  authorization genus {@code Type} and include any additional
     *  authorizations with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned list
     *  may contain only those authorizations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, authorizations are returned that are currently
     *  effective.  In any effective mode, effective authorizations and
     *  those currently expired are returned.
     *
     *
     *  @param  authorizationGenusType an authorization genus type 
     *  @return the returned {@code Authorization} list
     *  @throws org.osid.NullArgumentException
     *          {@code authorizationGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsByParentGenusType(org.osid.type.Type authorizationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuthorizationsByParentGenusType(authorizationGenusType));
    }


    /**
     *  Gets an {@code AuthorizationList} containing the given
     *  authorization record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned list
     *  may contain only those authorizations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, authorizations are returned that are
     *  currently effective.  In any effective mode, effective
     *  authorizations and those currently expired are returned.
     *
     *  @param  authorizationRecordType an authorization record type 
     *  @return the returned {@code Authorization} list
     *  @throws org.osid.NullArgumentException
     *          {@code authorizationRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsByRecordType(org.osid.type.Type authorizationRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuthorizationsByRecordType(authorizationRecordType));
    }


    /**
     *  Gets an {@code AuthorizationList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned list
     *  may contain only those authorizations that are accessible
     *  through this session.
     *  
     *  In active mode, authorizations are returned that are currently
     *  active. In any status mode, active and inactive authorizations
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Authorization} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsOnDate(org.osid.calendaring.DateTime from, 
                                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuthorizationsOnDate(from, to));
    }
        

    /**
     *  Gets a list of authorizations corresponding to a resource
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned list
     *  may contain only those authorizations that are accessible through
     *  this session.
     *
     *  In effective mode, authorizations are returned that are
     *  currently effective.  In any effective mode, effective
     *  authorizations and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the resource
     *  @return the returned {@code AuthorizationList}
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsForResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuthorizationsForResource(resourceId));
    }


    /**
     *  Gets a list of authorizations corresponding to a resource
     *  {@code Id} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned list
     *  may contain only those authorizations that are accessible
     *  through this session.
     *
     *  In effective mode, authorizations are returned that are
     *  currently effective.  In any effective mode, effective
     *  authorizations and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code AuthorizationList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsForResourceOnDate(org.osid.id.Id resourceId,
                                                                                       org.osid.calendaring.DateTime from,
                                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuthorizationsForResourceOnDate(resourceId, from, to));
    }


    /**
     *  Gets a list of authorizations corresponding to an agent
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned list
     *  may contain only those authorizations that are accessible through
     *  this session.
     *
     *  In effective mode, authorizations are returned that are
     *  currently effective.  In any effective mode, effective
     *  authorizations and those currently expired are returned.
     *
     *  @param  agentId the {@code Id} of the agent
     *  @return the returned {@code AuthorizationList}
     *  @throws org.osid.NullArgumentException {@code agentId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsForAgent(org.osid.id.Id agentId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuthorizationsForAgent(agentId));
    }


    /**
     *  Gets a list of authorizations corresponding to an agent
     *  {@code Id} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned list
     *  may contain only those authorizations that are accessible
     *  through this session.
     *
     *  In effective mode, authorizations are returned that are
     *  currently effective.  In any effective mode, effective
     *  authorizations and those currently expired are returned.
     *
     *  @param  agentId the {@code Id} of the agent
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code AuthorizationList}
     *  @throws org.osid.NullArgumentException {@code agentId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsForAgentOnDate(org.osid.id.Id agentId,
                                                                                    org.osid.calendaring.DateTime from,
                                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuthorizationsForAgentOnDate(agentId, from, to));
    }


    /**
     *  Gets a list of authorizations corresponding to a function
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned list
     *  may contain only those authorizations that are accessible
     *  through this session.
     *
     *  In effective mode, authorizations are returned that are
     *  currently effective.  In any effective mode, effective
     *  authorizations and those currently expired are returned.
     *
     *  @param  functionId the {@code Id} of the function
     *  @return the returned {@code AuthorizationList}
     *  @throws org.osid.NullArgumentException {@code functionId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsForFunction(org.osid.id.Id functionId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuthorizationsForFunction(functionId));
    }


    /**
     *  Gets a list of authorizations corresponding to a function
     *  {@code Id} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned list
     *  may contain only those authorizations that are accessible
     *  through this session.
     *
     *  In effective mode, authorizations are returned that are
     *  currently effective.  In any effective mode, effective
     *  authorizations and those currently expired are returned.
     *
     *  @param  functionId the {@code Id} of the function
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code AuthorizationList}
     *  @throws org.osid.NullArgumentException {@code functionId}, {@code
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsForFunctionOnDate(org.osid.id.Id functionId,
                                                                                       org.osid.calendaring.DateTime from,
                                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuthorizationsForFunctionOnDate(functionId, from, to));
    }


    /**
     *  Gets a list of authorizations corresponding to resource and
     *  function {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned list
     *  may contain only those authorizations that are accessible
     *  through this session.
     *
     *  In effective mode, authorizations are returned that are
     *  currently effective.  In any effective mode, effective
     *  authorizations and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the resource
     *  @param  functionId the {@code Id} of the function
     *  @return the returned {@code AuthorizationList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code functionId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsForResourceAndFunction(org.osid.id.Id resourceId,
                                                                                            org.osid.id.Id functionId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuthorizationsForResourceAndFunction(resourceId, functionId));
    }


    /**
     *  Gets a list of authorizations corresponding to resource and function
     *  {@code Ids} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned list
     *  may contain only those authorizations that are accessible
     *  through this session.
     *
     *  In effective mode, authorizations are returned that are currently
     *  effective. In any effective mode, effective authorizations and
     *  those currently expired are returned.
     *
     *  @param  functionId the {@code Id} of the function
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code AuthorizationList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code functionId}, {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsForResourceAndFunctionOnDate(org.osid.id.Id resourceId,
                                                                                                  org.osid.id.Id functionId,
                                                                                                  org.osid.calendaring.DateTime from,
                                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuthorizationsForResourceAndFunctionOnDate(resourceId, functionId, from, to));
    }


    /**
     *  Gets a list of authorizations corresponding to agent and
     *  function {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned list
     *  may contain only those authorizations that are accessible
     *  through this session.
     *
     *  In effective mode, authorizations are returned that are
     *  currently effective.  In any effective mode, effective
     *  authorizations and those currently expired are returned.
     *
     *  @param  agentId the {@code Id} of the agent
     *  @param  functionId the {@code Id} of the function
     *  @return the returned {@code AuthorizationList}
     *  @throws org.osid.NullArgumentException {@code agentId},
     *          {@code functionId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsForAgentAndFunction(org.osid.id.Id agentId,
                                                                        org.osid.id.Id functionId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuthorizationsForAgentAndFunction(agentId, functionId));
    }


    /**
     *  Gets a list of authorizations corresponding to agent and function
     *  {@code Ids} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned list
     *  may contain only those authorizations that are accessible
     *  through this session.
     *
     *  In effective mode, authorizations are returned that are currently
     *  effective. In any effective mode, effective authorizations and
     *  those currently expired are returned.
     *
     *  @param  functionId the {@code Id} of the function
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code AuthorizationList}
     *  @throws org.osid.NullArgumentException {@code agentId},
     *          {@code functionId}, {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsForAgentAndFunctionOnDate(org.osid.id.Id agentId,
                                                                                               org.osid.id.Id functionId,
                                                                                               org.osid.calendaring.DateTime from,
                                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuthorizationsForAgentAndFunctionOnDate(agentId, functionId, from, to));
    }


    /**
     *  Gets a list of {@code Authorizations} associated with a given
     *  qualifier. In plenary mode, the returned list contains all
     *  known authorizations or an error results. Otherwise, the
     *  returned list may contain only those authorizations that are
     *  accessible through this session.
     *
     *  @param  qualifierId a qualifier {@code Id} 
     *  @return the returned {@code Authorization list} 
     *  @throws org.osid.NullArgumentException {@code qualifierId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsByQualifier(org.osid.id.Id qualifierId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuthorizationsByQualifier(qualifierId));
    }


    /**
     *  Gets the explicit {@code Authorization} that generated the
     *  given implicit authorization. If the given {@code
     *  Authorization} is explicit, then the same {@code
     *  Authorization} is returned.
     *
     *  @param  authorizationId an authorization 
     *  @return the explicit {@code Authorization} 
     *  @throws org.osid.NotFoundException {@code authorizationId} is 
     *          not found 
     *  @throws org.osid.NullArgumentException {@code authorizationId} 
     *          is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.authorization.Authorization getExplicitAuthorization(org.osid.id.Id authorizationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getExplicitAuthorization(authorizationId));
    }


    /**
     *  Gets all {@code Authorizations}. 
     *
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned
     *  list may contain only those authorizations that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, authorizations are returned that are
     *  currently effective.  In any effective mode, effective
     *  authorizations and those currently expired are returned.
     *
     *  @return a list of {@code Authorizations} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuthorizations());
    }
}
