//
// AbstractOffsetEventEnablerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.rules.offseteventenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractOffsetEventEnablerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.calendaring.rules.OffsetEventEnablerSearchResults {

    private org.osid.calendaring.rules.OffsetEventEnablerList offsetEventEnablers;
    private final org.osid.calendaring.rules.OffsetEventEnablerQueryInspector inspector;
    private final java.util.Collection<org.osid.calendaring.rules.records.OffsetEventEnablerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractOffsetEventEnablerSearchResults.
     *
     *  @param offsetEventEnablers the result set
     *  @param offsetEventEnablerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>offsetEventEnablers</code>
     *          or <code>offsetEventEnablerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractOffsetEventEnablerSearchResults(org.osid.calendaring.rules.OffsetEventEnablerList offsetEventEnablers,
                                            org.osid.calendaring.rules.OffsetEventEnablerQueryInspector offsetEventEnablerQueryInspector) {
        nullarg(offsetEventEnablers, "offset event enablers");
        nullarg(offsetEventEnablerQueryInspector, "offset event enabler query inspectpr");

        this.offsetEventEnablers = offsetEventEnablers;
        this.inspector = offsetEventEnablerQueryInspector;

        return;
    }


    /**
     *  Gets the offset event enabler list resulting from a search.
     *
     *  @return an offset event enabler list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerList getOffsetEventEnablers() {
        if (this.offsetEventEnablers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.calendaring.rules.OffsetEventEnablerList offsetEventEnablers = this.offsetEventEnablers;
        this.offsetEventEnablers = null;
	return (offsetEventEnablers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.calendaring.rules.OffsetEventEnablerQueryInspector getOffsetEventEnablerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  offset event enabler search record <code> Type. </code> This method must
     *  be used to retrieve an offsetEventEnabler implementing the requested
     *  record.
     *
     *  @param offsetEventEnablerSearchRecordType an offsetEventEnabler search 
     *         record type 
     *  @return the offset event enabler search
     *  @throws org.osid.NullArgumentException
     *          <code>offsetEventEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(offsetEventEnablerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.rules.records.OffsetEventEnablerSearchResultsRecord getOffsetEventEnablerSearchResultsRecord(org.osid.type.Type offsetEventEnablerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.calendaring.rules.records.OffsetEventEnablerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(offsetEventEnablerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(offsetEventEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record offset event enabler search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addOffsetEventEnablerRecord(org.osid.calendaring.rules.records.OffsetEventEnablerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "offset event enabler record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
