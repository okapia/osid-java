//
// AbstractIntersectionSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.path.intersection.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractIntersectionSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.mapping.path.IntersectionSearchResults {

    private org.osid.mapping.path.IntersectionList intersections;
    private final org.osid.mapping.path.IntersectionQueryInspector inspector;
    private final java.util.Collection<org.osid.mapping.path.records.IntersectionSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractIntersectionSearchResults.
     *
     *  @param intersections the result set
     *  @param intersectionQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>intersections</code>
     *          or <code>intersectionQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractIntersectionSearchResults(org.osid.mapping.path.IntersectionList intersections,
                                                org.osid.mapping.path.IntersectionQueryInspector intersectionQueryInspector) {
        nullarg(intersections, "intersections");
        nullarg(intersectionQueryInspector, "intersection query inspectpr");

        this.intersections = intersections;
        this.inspector = intersectionQueryInspector;

        return;
    }


    /**
     *  Gets the intersection list resulting from a search.
     *
     *  @return an intersection list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionList getIntersections() {
        if (this.intersections == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.mapping.path.IntersectionList intersections = this.intersections;
        this.intersections = null;
	return (intersections);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.mapping.path.IntersectionQueryInspector getIntersectionQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  intersection search record <code> Type. </code> This method must
     *  be used to retrieve an intersection implementing the requested
     *  record.
     *
     *  @param intersectionSearchRecordType an intersection search 
     *         record type 
     *  @return the intersection search
     *  @throws org.osid.NullArgumentException
     *          <code>intersectionSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(intersectionSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.path.records.IntersectionSearchResultsRecord getIntersectionSearchResultsRecord(org.osid.type.Type intersectionSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.mapping.path.records.IntersectionSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(intersectionSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(intersectionSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record intersection search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addIntersectionRecord(org.osid.mapping.path.records.IntersectionSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "intersection record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
