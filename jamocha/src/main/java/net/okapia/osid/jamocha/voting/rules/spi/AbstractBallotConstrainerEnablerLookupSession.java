//
// AbstractBallotConstrainerEnablerLookupSession.java
//
//    A starter implementation framework for providing a BallotConstrainerEnabler
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a BallotConstrainerEnabler
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getBallotConstrainerEnablers(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractBallotConstrainerEnablerLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.voting.rules.BallotConstrainerEnablerLookupSession {

    private boolean pedantic      = false;
    private boolean activeonly    = false;
    private boolean federated     = false;
    private org.osid.voting.Polls polls = new net.okapia.osid.jamocha.nil.voting.polls.UnknownPolls();
    

    /**
     *  Gets the <code>Polls/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Polls Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getPollsId() {
        return (this.polls.getId());
    }


    /**
     *  Gets the <code>Polls</code> associated with this 
     *  session.
     *
     *  @return the <code>Polls</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Polls getPolls()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.polls);
    }


    /**
     *  Sets the <code>Polls</code>.
     *
     *  @param  polls the polls for this session
     *  @throws org.osid.NullArgumentException <code>polls</code>
     *          is <code>null</code>
     */

    protected void setPolls(org.osid.voting.Polls polls) {
        nullarg(polls, "polls");
        this.polls = polls;
        return;
    }


    /**
     *  Tests if this user can perform <code>BallotConstrainerEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupBallotConstrainerEnablers() {
        return (true);
    }


    /**
     *  A complete view of the <code>BallotConstrainerEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeBallotConstrainerEnablerView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>BallotConstrainerEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryBallotConstrainerEnablerView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include ballot constrainer enablers in pollses which are children
     *  of this polls in the polls hierarchy.
     */

    @OSID @Override
    public void useFederatedPollsView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this polls only.
     */

    @OSID @Override
    public void useIsolatedPollsView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active ballot constrainer enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveBallotConstrainerEnablerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive ballot constrainer enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusBallotConstrainerEnablerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>BallotConstrainerEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>BallotConstrainerEnabler</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>BallotConstrainerEnabler</code> and
     *  retained for compatibility.
     *
     *  In active mode, ballot constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive ballot constrainer enablers
     *  are returned.
     *
     *  @param  ballotConstrainerEnablerId <code>Id</code> of the
     *          <code>BallotConstrainerEnabler</code>
     *  @return the ballot constrainer enabler
     *  @throws org.osid.NotFoundException <code>ballotConstrainerEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>ballotConstrainerEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnabler getBallotConstrainerEnabler(org.osid.id.Id ballotConstrainerEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.voting.rules.BallotConstrainerEnablerList ballotConstrainerEnablers = getBallotConstrainerEnablers()) {
            while (ballotConstrainerEnablers.hasNext()) {
                org.osid.voting.rules.BallotConstrainerEnabler ballotConstrainerEnabler = ballotConstrainerEnablers.getNextBallotConstrainerEnabler();
                if (ballotConstrainerEnabler.getId().equals(ballotConstrainerEnablerId)) {
                    return (ballotConstrainerEnabler);
                }
            }
        } 

        throw new org.osid.NotFoundException(ballotConstrainerEnablerId + " not found");
    }


    /**
     *  Gets a <code>BallotConstrainerEnablerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  ballotConstrainerEnablers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>BallotConstrainerEnablers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, ballot constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive ballot constrainer enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getBallotConstrainerEnablers()</code>.
     *
     *  @param  ballotConstrainerEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>BallotConstrainerEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
*               found
     *  @throws org.osid.NullArgumentException
     *          <code>ballotConstrainerEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerList getBallotConstrainerEnablersByIds(org.osid.id.IdList ballotConstrainerEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.voting.rules.BallotConstrainerEnabler> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = ballotConstrainerEnablerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getBallotConstrainerEnabler(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("ballot constrainer enabler " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.voting.rules.ballotconstrainerenabler.LinkedBallotConstrainerEnablerList(ret));
    }


    /**
     *  Gets a <code>BallotConstrainerEnablerList</code> corresponding to the given
     *  ballot constrainer enabler genus <code>Type</code> which does not include
     *  ballot constrainer enablers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  ballot constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those ballot constrainer enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, ballot constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive ballot constrainer enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getBallotConstrainerEnablers()</code>.
     *
     *  @param  ballotConstrainerEnablerGenusType a ballotConstrainerEnabler genus type 
     *  @return the returned <code>BallotConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>ballotConstrainerEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerList getBallotConstrainerEnablersByGenusType(org.osid.type.Type ballotConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.voting.rules.ballotconstrainerenabler.BallotConstrainerEnablerGenusFilterList(getBallotConstrainerEnablers(), ballotConstrainerEnablerGenusType));
    }


    /**
     *  Gets a <code>BallotConstrainerEnablerList</code> corresponding to the given
     *  ballot constrainer enabler genus <code>Type</code> and include any additional
     *  ballot constrainer enablers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  ballot constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those ballot constrainer enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, ballot constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive ballot constrainer enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getBallotConstrainerEnablers()</code>.
     *
     *  @param  ballotConstrainerEnablerGenusType a ballotConstrainerEnabler genus type 
     *  @return the returned <code>BallotConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>ballotConstrainerEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerList getBallotConstrainerEnablersByParentGenusType(org.osid.type.Type ballotConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getBallotConstrainerEnablersByGenusType(ballotConstrainerEnablerGenusType));
    }


    /**
     *  Gets a <code>BallotConstrainerEnablerList</code> containing the given
     *  ballot constrainer enabler record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  ballot constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those ballot constrainer enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *

     *  In active mode, ballot constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive ballot constrainer enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getBallotConstrainerEnablers()</code>.
     *
     *  @param  ballotConstrainerEnablerRecordType a ballotConstrainerEnabler record type 
     *  @return the returned <code>BallotConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>ballotConstrainerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerList getBallotConstrainerEnablersByRecordType(org.osid.type.Type ballotConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.voting.rules.ballotconstrainerenabler.BallotConstrainerEnablerRecordFilterList(getBallotConstrainerEnablers(), ballotConstrainerEnablerRecordType));
    }


    /**
     *  Gets a <code>BallotConstrainerEnablerList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  ballot constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those ballot constrainer enablers that are accessible
     *  through this session.
     *  
     *  In active mode, ballot constrainer enablers are returned that are currently
     *  active in addition to being effective during the given date
     *  range. In any status mode, active and inactive ballot constrainer enablers are
     *  returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>BallotConstrainerEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerList getBallotConstrainerEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.voting.rules.ballotconstrainerenabler.TemporalBallotConstrainerEnablerFilterList(getBallotConstrainerEnablers(), from, to));
    }
        

    /**
     *  Gets a <code>BallotConstrainerEnablerList </code> which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  ballot constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those ballot constrainer enablers that are accessible
     *  through this session.
     *
     *  In active mode, ballot constrainer enablers are returned that are currently
     *  active in addition to being effective during the date
     *  range. In any status mode, active and inactive ballot constrainer enablers are
     *  returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned <code>BallotConstrainerEnabler</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>agent</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerList getBallotConstrainerEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (getBallotConstrainerEnablersOnDate(from, to));
    }


    /**
     *  Gets all <code>BallotConstrainerEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  ballot constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those ballot constrainer enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, ballot constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive ballot constrainer enablers
     *  are returned.
     *
     *  @return a list of <code>BallotConstrainerEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.voting.rules.BallotConstrainerEnablerList getBallotConstrainerEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the ballot constrainer enabler list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of ballot constrainer enablers
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.voting.rules.BallotConstrainerEnablerList filterBallotConstrainerEnablersOnViews(org.osid.voting.rules.BallotConstrainerEnablerList list)
        throws org.osid.OperationFailedException {

        org.osid.voting.rules.BallotConstrainerEnablerList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.voting.rules.ballotconstrainerenabler.ActiveBallotConstrainerEnablerFilterList(ret);
        }

        return (ret);
    }
}
