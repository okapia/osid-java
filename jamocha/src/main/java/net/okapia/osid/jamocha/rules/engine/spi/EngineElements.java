//
// EngineElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.rules.engine.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class EngineElements
    extends net.okapia.osid.jamocha.spi.OsidCatalogElements {


    /**
     *  Gets the EngineElement Id.
     *
     *  @return the engine element Id
     */

    public static org.osid.id.Id getEngineEntityId() {
        return (makeEntityId("osid.rules.Engine"));
    }


    /**
     *  Gets the RuleId element Id.
     *
     *  @return the RuleId element Id
     */

    public static org.osid.id.Id getRuleId() {
        return (makeQueryElementId("osid.rules.engine.RuleId"));
    }


    /**
     *  Gets the Rule element Id.
     *
     *  @return the Rule element Id
     */

    public static org.osid.id.Id getRule() {
        return (makeQueryElementId("osid.rules.engine.Rule"));
    }


    /**
     *  Gets the AncestorEngineId element Id.
     *
     *  @return the AncestorEngineId element Id
     */

    public static org.osid.id.Id getAncestorEngineId() {
        return (makeQueryElementId("osid.rules.engine.AncestorEngineId"));
    }


    /**
     *  Gets the AncestorEngine element Id.
     *
     *  @return the AncestorEngine element Id
     */

    public static org.osid.id.Id getAncestorEngine() {
        return (makeQueryElementId("osid.rules.engine.AncestorEngine"));
    }


    /**
     *  Gets the DescendantEngineId element Id.
     *
     *  @return the DescendantEngineId element Id
     */

    public static org.osid.id.Id getDescendantEngineId() {
        return (makeQueryElementId("osid.rules.engine.DescendantEngineId"));
    }


    /**
     *  Gets the DescendantEngine element Id.
     *
     *  @return the DescendantEngine element Id
     */

    public static org.osid.id.Id getDescendantEngine() {
        return (makeQueryElementId("osid.rules.engine.DescendantEngine"));
    }
}
