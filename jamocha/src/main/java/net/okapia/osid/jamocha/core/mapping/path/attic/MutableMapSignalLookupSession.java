//
// MutableMapSignalLookupSession
//
//    Implements a Signal lookup service backed by a collection of
//    signals that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping.path;


/**
 *  Implements a Signal lookup service backed by a collection of
 *  signals. The signals are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of signals can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapSignalLookupSession
    extends net.okapia.osid.jamocha.core.mapping.path.spi.AbstractMapSignalLookupSession
    implements org.osid.mapping.path.SignalLookupSession {


    /**
     *  Constructs a new <code>MutableMapSignalLookupSession</code>
     *  with no signals.
     */

    public MutableMapSignalLookupSession() {
        return;
    }


    /**
     *  Constructs a new <code>MutableMapSignalLookupSession</code> with a
     *  single signal.
     *  
     *  @param  signal a signal
     *  @throws org.osid.NullArgumentException <code>signal</code>
     *          is <code>null</code>
     */

    public MutableMapSignalLookupSession(org.osid.mapping.path.Signal signal) {
        putSignal(signal);
        return;
    }


    /**
     *  Constructs a new <code>MutableMapSignalLookupSession</code>
     *  using an array of signals.
     *
     *  @param  signals an array of signals
     *  @throws org.osid.NullArgumentException <code>signals</code>
     *          is <code>null</code>
     */

    public MutableMapSignalLookupSession(org.osid.mapping.path.Signal[] signals) {
        putSignals(signals);
        return;
    }


    /**
     *  Constructs a new <code>MutableMapSignalLookupSession</code>
     *  using a collection of signals.
     *
     *  @param  signals a collection of signals
     *  @throws org.osid.NullArgumentException <code>signals</code>
     *          is <code>null</code>
     */

    public MutableMapSignalLookupSession(java.util.Collection<? extends org.osid.mapping.path.Signal> signals) {
        putSignals(signals);
        return;
    }

    
    /**
     *  Makes a <code>Signal</code> available in this session.
     *
     *  @param  signal a signal
     *  @throws org.osid.NullArgumentException <code>signal<code> is
     *          <code>null</code>
     */

    @Override
    public void putSignal(org.osid.mapping.path.Signal signal) {
        super.putSignal(signal);
        return;
    }


    /**
     *  Makes an array of signals available in this session.
     *
     *  @param  signals an array of signals
     *  @throws org.osid.NullArgumentException <code>signals<code>
     *          is <code>null</code>
     */

    @Override
    public void putSignals(org.osid.mapping.path.Signal[] signals) {
        super.putSignals(signals);
        return;
    }


    /**
     *  Makes collection of signals available in this session.
     *
     *  @param  signals a collection of signals
     *  @throws org.osid.NullArgumentException <code>signals<code> is
     *          <code>null</code>
     */

    @Override
    public void putSignals(java.util.Collection<? extends org.osid.mapping.path.Signal> signals) {
        super.putSignals(signals);
        return;
    }


    /**
     *  Removes a Signal from this session.
     *
     *  @param signalId the <code>Id</code> of the signal
     *  @throws org.osid.NullArgumentException <code>signalId<code>
     *          is <code>null</code>
     */

    @Override
    public void removeSignal(org.osid.id.Id signalId) {
        super.removeSignal(signalId);
        return;
    }    
}
