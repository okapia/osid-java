//
// AbstractImmutableEdge.java
//
//     Wraps a mutable Edge to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.topology.edge.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Edge</code> to hide modifiers. This
 *  wrapper provides an immutized Edge from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying edge whose state changes are visible.
 */

public abstract class AbstractImmutableEdge
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRelationship
    implements org.osid.topology.Edge {

    private final org.osid.topology.Edge edge;


    /**
     *  Constructs a new <code>AbstractImmutableEdge</code>.
     *
     *  @param edge the edge to immutablize
     *  @throws org.osid.NullArgumentException <code>edge</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableEdge(org.osid.topology.Edge edge) {
        super(edge);
        this.edge = edge;
        return;
    }

    
    /**
     *  Tests if this is a directional edge.
     *
     *  @return <code> true </code> if this edge is directional
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean isDirectional() {
        return (this.edge.isDirectional());
    }
    
    
    /**
     *  Tests if this directional edge is bi-directional. 
     *
     *  @return <code> true </code> if this edge is directional and 
     *          bi-directional, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean isBiDirectional() {
        return (this.edge.isBiDirectional());
    }


    /**
     *  Gets the cost of this edge. 
     *
     *  @return a number representing the cost of this edge 
     */

    @OSID @Override
    public java.math.BigDecimal getCost() {
        return (this.edge.getCost());
    }


    /**
     *  Gets the distance of this edge. 
     *
     *  @return a number representing the distance of this edge 
     */

    @OSID @Override
    public java.math.BigDecimal getDistance() {
        return (this.edge.getDistance());
    }


    /**
     *  Gets the source node of this edge. If the edge is uni-directional, the 
     *  source node is the node at the beginning of the edge, otherwise it may 
     *  be relative to the means of traversal of the graph. 
     *
     *  @return the source node <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getSourceNodeId() {
        return (this.edge.getSourceNodeId());
    }


    /**
     *  Gets the <code> Id </code> of the source node of this edge. If the 
     *  edge is uni-directional, the source node is the node at the beginning 
     *  of the edge, otherwise it may be relative to the means of traversal of 
     *  the graph. 
     *
     *  @return the source node 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.topology.Node getSourceNode()
        throws org.osid.OperationFailedException {

        return (this.edge.getSourceNode());
    }


    /**
     *  Gets the <code> Id </code> of the destination node of this edge. If 
     *  the edge is uni-directional, the destination node is the node at the 
     *  end of the edge, otherwise it may be relative to the means of 
     *  traversal of the graph. 
     *
     *  @return the detsination node <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getDestinationNodeId() {
        return (this.edge.getDestinationNodeId());
    }


    /**
     *  Gets the destination node of this edge. If the edge is 
     *  uni-directional, the destination node is the node at the end of the 
     *  edge, otherwise it may be relative to the means of traversal of the 
     *  graph. 
     *
     *  @return the detsination node 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.topology.Node getDestinationNode()
        throws org.osid.OperationFailedException {

        return (this.edge.getDestinationNode());
    }


    /**
     *  Gets the edge record corresponding to the given <code> Edge </code> 
     *  record <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record. The <code> edgeRecordType </code> 
     *  may be the <code> Type </code> returned in <code> getRecordTypes() 
     *  </code> or any of its parents in a <code> Type </code> hierarchy where 
     *  <code> hasRecordType(edgeRecordType) </code> is <code> true </code> . 
     *
     *  @param  edgeRecordType the type of edge record to retrieve 
     *  @return the edge record 
     *  @throws org.osid.NullArgumentException <code> edgeRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(edgeRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.records.EdgeRecord getEdgeRecord(org.osid.type.Type edgeRecordType)
        throws org.osid.OperationFailedException {

        return (this.edge.getEdgeRecord(edgeRecordType));
    }
}

