//
// AbstractChecklistLookupSession.java
//
//    A starter implementation framework for providing a Checklist
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.checklist.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A starter implementation framework for providing a Checklist
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getChecklists(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractChecklistLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.checklist.ChecklistLookupSession {

    private boolean pedantic = false;
    private org.osid.checklist.Checklist checklist = new net.okapia.osid.jamocha.nil.checklist.checklist.UnknownChecklist();
    

    /**
     *  Tests if this user can perform <code>Checklist</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupChecklists() {
        return (true);
    }


    /**
     *  A complete view of the <code>Checklist</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeChecklistView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Checklist</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryChecklistView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }

     
    /**
     *  Gets the <code>Checklist</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Checklist</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Checklist</code> and
     *  retained for compatibility.
     *
     *  @param  checklistId <code>Id</code> of the
     *          <code>Checklist</code>
     *  @return the checklist
     *  @throws org.osid.NotFoundException <code>checklistId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>checklistId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.Checklist getChecklist(org.osid.id.Id checklistId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.checklist.ChecklistList checklists = getChecklists()) {
            while (checklists.hasNext()) {
                org.osid.checklist.Checklist checklist = checklists.getNextChecklist();
                if (checklist.getId().equals(checklistId)) {
                    return (checklist);
                }
            }
        } 

        throw new org.osid.NotFoundException(checklistId + " not found");
    }


    /**
     *  Gets a <code>ChecklistList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  checklists specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Checklists</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getChecklists()</code>.
     *
     *  @param  checklistIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Checklist</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>checklistIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistList getChecklistsByIds(org.osid.id.IdList checklistIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.checklist.Checklist> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = checklistIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getChecklist(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("checklist " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.checklist.checklist.LinkedChecklistList(ret));
    }


    /**
     *  Gets a <code>ChecklistList</code> corresponding to the given
     *  checklist genus <code>Type</code> which does not include
     *  checklists of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  checklists or an error results. Otherwise, the returned list
     *  may contain only those checklists that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getChecklists()</code>.
     *
     *  @param  checklistGenusType a checklist genus type 
     *  @return the returned <code>Checklist</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>checklistGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistList getChecklistsByGenusType(org.osid.type.Type checklistGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.checklist.checklist.ChecklistGenusFilterList(getChecklists(), checklistGenusType));
    }


    /**
     *  Gets a <code>ChecklistList</code> corresponding to the given
     *  checklist genus <code>Type</code> and include any additional
     *  checklists with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  checklists or an error results. Otherwise, the returned list
     *  may contain only those checklists that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getChecklists()</code>.
     *
     *  @param  checklistGenusType a checklist genus type 
     *  @return the returned <code>Checklist</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>checklistGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistList getChecklistsByParentGenusType(org.osid.type.Type checklistGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getChecklistsByGenusType(checklistGenusType));
    }


    /**
     *  Gets a <code>ChecklistList</code> containing the given
     *  checklist record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  checklists or an error results. Otherwise, the returned list
     *  may contain only those checklists that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getChecklists()</code>.
     *
     *  @param  checklistRecordType a checklist record type 
     *  @return the returned <code>Checklist</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>checklistRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistList getChecklistsByRecordType(org.osid.type.Type checklistRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.checklist.checklist.ChecklistRecordFilterList(getChecklists(), checklistRecordType));
    }


    /**
     *  Gets a <code>ChecklistList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known checklists or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  checklists that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Checklist</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistList getChecklistsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.checklist.checklist.ChecklistProviderFilterList(getChecklists(), resourceId));
    }


    /**
     *  Gets all <code>Checklists</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  checklists or an error results. Otherwise, the returned list
     *  may contain only those checklists that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Checklists</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.checklist.ChecklistList getChecklists()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the checklist list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of checklists
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.checklist.ChecklistList filterChecklistsOnViews(org.osid.checklist.ChecklistList list)
        throws org.osid.OperationFailedException {

        return (list);
    }
}
