//
// AssessmentOfferedElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.assessmentoffered.spi;

/**
 *  Ids for object elements for use in forms and queries.
 */

public class AssessmentOfferedElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the AssessmentOfferedElement Id.
     *
     *  @return the assessment offered element Id
     */

    public static org.osid.id.Id getAssessmentOfferedEntityId() {
        return (makeEntityId("osid.assessment.AssessmentOffered"));
    }


    /**
     *  Gets the AssessmentId element Id.
     *
     *  @return the AssessmentId element Id
     */

    public static org.osid.id.Id getAssessmentId() {
        return (makeElementId("osid.assessment.assessmentoffered.AssessmentId"));
    }


    /**
     *  Gets the Assessment element Id.
     *
     *  @return the Assessment element Id
     */

    public static org.osid.id.Id getAssessment() {
        return (makeElementId("osid.assessment.assessmentoffered.Assessment"));
    }


    /**
     *  Gets the LevelId element Id.
     *
     *  @return the LevelId element Id
     */

    public static org.osid.id.Id getLevelId() {
        return (makeElementId("osid.assessment.assessmentoffered.LevelId"));
    }


    /**
     *  Gets the Level element Id.
     *
     *  @return the Level element Id
     */

    public static org.osid.id.Id getLevel() {
        return (makeElementId("osid.assessment.assessmentoffered.Level"));
    }


    /**
     *  Gets the StartTime element Id.
     *
     *  @return the StartTime element Id
     */

    public static org.osid.id.Id getStartTime() {
        return (makeElementId("osid.assessment.assessmentoffered.StartTime"));
    }


    /**
     *  Gets the Deadline element Id.
     *
     *  @return the Deadline element Id
     */

    public static org.osid.id.Id getDeadline() {
        return (makeElementId("osid.assessment.assessmentoffered.Deadline"));
    }


    /**
     *  Gets the Duration element Id.
     *
     *  @return the Duration element Id
     */

    public static org.osid.id.Id getDuration() {
        return (makeElementId("osid.assessment.assessmentoffered.Duration"));
    }


    /**
     *  Gets the RubricId element Id.
     *
     *  @return the RubricId element Id
     */

    public static org.osid.id.Id getRubricId() {
        return (makeElementId("osid.assessment.assessmentoffered.RubricId"));
    }


    /**
     *  Gets the Rubric element Id.
     *
     *  @return the Rubric element Id
     */

    public static org.osid.id.Id getRubric() {
        return (makeElementId("osid.assessment.assessmentoffered.Rubric"));
    }


    /**
     *  Gets the Sequential element Id.
     *
     *  @return the Sequential element Id
     */

    public static org.osid.id.Id getSequential() {
        return (makeElementId("osid.assessment.assessmentoffered.Sequential"));
    }


    /**
     *  Gets the Shuffled element Id.
     *
     *  @return the Shuffled element Id
     */

    public static org.osid.id.Id getShuffled() {
        return (makeQueryElementId("osid.assessment.assessmentoffered.Shuffled"));
    }


    /**
     *  Gets the ScoreSystem element Id.
     *
     *  @return the ScoreSystem element Id
     */

    public static org.osid.id.Id getScoreSystem() {
        return (makeElementId("osid.assessment.assessmentoffered.ScoreSystem"));
    }


    /**
     *  Gets the GradeSystem element Id.
     *
     *  @return the GradeSystem element Id
     */

    public static org.osid.id.Id getGradeSystem() {
        return (makeElementId("osid.assessment.assessmentoffered.GradeSystem"));
    }


    /**
     *  Gets the ScoreSystemId element Id.
     *
     *  @return the ScoreSystemId element Id
     */

    public static org.osid.id.Id getScoreSystemId() {
        return (makeElementId("osid.assessment.assessmentoffered.ScoreSystemId"));
    }


    /**
     *  Gets the GradeSystemId element Id.
     *
     *  @return the GradeSystemId element Id
     */

    public static org.osid.id.Id getGradeSystemId() {
        return (makeElementId("osid.assessment.assessmentoffered.GradeSystemId"));
    }


    /**
     *  Gets the AssessmentTakenId element Id.
     *
     *  @return the AssessmentTakenId element Id
     */

    public static org.osid.id.Id getAssessmentTakenId() {
        return (makeQueryElementId("osid.assessment.assessmentoffered.AssessmentTakenId"));
    }


    /**
     *  Gets the AssessmentTaken element Id.
     *
     *  @return the AssessmentTaken element Id
     */

    public static org.osid.id.Id getAssessmentTaken() {
        return (makeQueryElementId("osid.assessment.assessmentoffered.AssessmentTaken"));
    }


    /**
     *  Gets the BankId element Id.
     *
     *  @return the BankId element Id
     */

    public static org.osid.id.Id getBankId() {
        return (makeQueryElementId("osid.assessment.assessmentoffered.BankId"));
    }


    /**
     *  Gets the Bank element Id.
     *
     *  @return the Bank element Id
     */

    public static org.osid.id.Id getBank() {
        return (makeQueryElementId("osid.assessment.assessmentoffered.Bank"));
    }


    /**
     *  Gets the Shuffle element Id.
     *
     *  @return the Shuffle element Id
     */

    public static org.osid.id.Id getShuffle() {
        return (makeElementId("osid.assessment.assessmentoffered.Shuffle"));
    }
}
