//
// AbstractMutableSupersedingEventList.java
//
//     Implements a mutable SupersedingEventList. This list allows SupersedingEvents
//     to be added after this object has been created.
//
//
// Tom Coppeto
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.supersedingevent.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  <p>Implements a mutable SupersedingEventList. This list allows SupersedingEvents
 *  to be added after this supersedingEvent has been created. One this list
 *  has been returned to the consumer, all subsequent additions occur
 *  in a separate processing thread.  The creator of this supersedingEvent
 *  must invoke <code>eol()</code> when there are no more supersedingEvents to
 *  be added.</p>
 *
 *  <p> If the consumer of the <code>SupersedingEventList</code> interface reaches
 *  the end of the internal buffer before <code>eol()</code>, then
 *  methods will block until more supersedingEvents are added or <code>eol()</code>
 *  is invoked.</p>
 *
 *  <p><code>available()</code> never blocks but may return
 *  <code>0</code> if waiting for more supersedingEvents to be added.</p>
 */

public abstract class AbstractMutableSupersedingEventList
    extends AbstractSupersedingEventList
    implements org.osid.calendaring.SupersedingEventList {

    private final java.util.Queue<org.osid.calendaring.SupersedingEvent> stack = new java.util.concurrent.ConcurrentLinkedQueue<>();
    private volatile boolean eol = false;
    private volatile boolean closed = false;
    private volatile org.osid.OsidException error;
    

    /**
     *  Creates a new empty <code>AbstractMutableSupersedingEventList</code>.
     */

    protected AbstractMutableSupersedingEventList() {
        return;
    }


    /**
     *  Creates a new <code>AbstractMutableSupersedingEventList</code>.
     *
     *  @param supersedingEvent a <code>SupersedingEvent</code>
     *  @throws org.osid.NullArgumentException <code>supersedingEvent</code>
     *          is <code>null</code>
     */

    protected AbstractMutableSupersedingEventList(org.osid.calendaring.SupersedingEvent supersedingEvent) {
        nullarg(supersedingEvent, "supersedingEvent");
        addSupersedingEvent(supersedingEvent);
        return;
    }


    /**
     *  Creates a new <code>AbstractMutableSupersedingEventList</code>.
     *
     *  @param array an array of supersedingevents
     *  @throws org.osid.NullArgumentException <code>array</code>
     *          is <code>null</code>
     */

    protected AbstractMutableSupersedingEventList(org.osid.calendaring.SupersedingEvent[] array) {
        nullarg(array, "array");
        addSupersedingEvents(array);
        return;
    }


    /**
     *  Creates a new <code>AbstractMutableSupersedingEventList</code>.
     *
     *  @param collection a java.util.Collection of supersedingEvents
     *  @throws org.osid.NullArgumentException <code>collection</code>
     *          is <code>null</code>
     */

    protected AbstractMutableSupersedingEventList(java.util.Collection<org.osid.calendaring.SupersedingEvent> collection) {
        nullarg(collection, "collection");
        addSupersedingEventCollection(collection);
        return;
    }


    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in this 
     *          list, <code> false </code> if the end of the list has been 
     *          reached 
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        synchronized (this.stack) {
            while (true) {
                if (this.stack.size() > 0) {
                    return (true);
                }

                if (hasError()) {
                    return (true);
                }

                if (this.eol) {
                    return (false);
                }

                try {
                    this.stack.wait();
                } catch (InterruptedException ie) {}
            }
        }
    }


    /**
     *  Gets the number of elements available for retrieval. The number 
     *  returned by this method may be less than or equal to the total number 
     *  of elements in this list. To determine if the end of the list has been 
     *  reached, the method <code> hasNext() </code> should be used. This 
     *  method conveys what is known about the number of remaining elements at 
     *  a point in time and can be used to determine a minimum size of the 
     *  remaining elements, if known. A valid return is zero even if <code> 
     *  hasNext() </code> is true. 
     *  <p>
     *  This method does not imply asynchronous usage. All OSID methods may 
     *  block. 
     *
     *  @return the number of elements available for retrieval 
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public long available() {
        synchronized (this.stack) {
            return (this.stack.size());
        }
    }


    /**
     *  Skip the specified number of elements in the list. If the number 
     *  skipped is greater than the number of elements in the list, hasNext() 
     *  becomes false and available() returns zero as there are no more 
     *  elements to retrieve. 
     *
     *  @param  n the number of elements to skip 
     *  @throws org.osid.InvalidArgumentException <code>n</code> is less
     *          than zero
     */

    @OSID @Override
    public void skip(long n) {
        synchronized (this.stack) {
            while (n > 0) {
                if (hasError()) {
                    return;
                }

                /* blasted protected removeRange */
                if (this.stack.size() > 0) {
                    this.stack.remove();
                    --n;
                } else if (this.eol) {
                    return;
                } else {
                    try {
                        this.stack.wait();
                    } catch (InterruptedException ie) {}
                }
            }
            
            this.stack.notifyAll();
        }

        return;
    }


    /**
     *  Gets the next <code>SupersedingEvent</code> in this list. 
     *
     *  @return the next <code>SupersedingEvent</code> in this list. The <code> 
     *          hasNext() </code> method should be used to test that a next 
     *          <code>SupersedingEvent</code> is available before calling this method. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEvent getNextSupersedingEvent()
        throws org.osid.OperationFailedException {

        if (hasError()) {
            throw new org.osid.OperationFailedException(getError());
        }

        if (hasNext()) {
            synchronized (this.stack) {
                org.osid.calendaring.SupersedingEvent supersedingEvent = this.stack.remove();
                this.stack.notifyAll();
                return (supersedingEvent);
            }
        } else {
            throw new org.osid.IllegalStateException("no more elements available in supersedingevent list");
        }
    }

        
    /**
     *  Gets the next set of <code>SupersedingEvent</code> elements in this
     *  list. The specified amount must be less than or equal to the
     *  return from <code> available(). </code>
     *
     *  @param  n the number of <code>SupersedingEvent</code> elements requested which 
     *          must be less than or equal to <code> available() </code> 
     *  @return an array of <code>SupersedingEvent</code> elements. <code> </code> The 
     *          length of the array is less than or equal to the number 
     *          specified. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.InvalidArgumentException <code>n</code> is less
     *          than zero
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEvent[] getNextSupersedingEvents(long n)
        throws org.osid.OperationFailedException {
        
        if (n > available()) {
            throw new org.osid.IllegalStateException("insufficient elements available");
        }

        if (hasError()) {
            throw new org.osid.OperationFailedException(getError());
        }

        org.osid.calendaring.SupersedingEvent[] ret = new org.osid.calendaring.SupersedingEvent[(int) n];

        synchronized (this.stack) {
            for (int i = 0; i < n; i++) {
                ret[i] = this.stack.remove();
            }

            this.stack.notifyAll();
        }

        return (ret);
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        synchronized (this.stack) {
            this.stack.notifyAll();
            this.stack.clear();
            this.closed = true;
        }

        return;
    }


    /**
     *  Tests if this list has been closed.
     *
     *  @return <code>true</code> if this list has been closed,
     *          <code>false</code> if still active
     */

    public boolean isClosed() {
        return (this.closed);
    }


    /**
     *  Tests if the list is operational. A list is operational if
     *  <code>done()</code> has not been invoked and no error has
     *  occurred.
     *
     *  @return <code>true</code> if the list is operational,
     *          <code>false</code> otherwise.
     */

    public boolean isOperational() {
        if (hasError()) {
            return (false);
        }

        if (isClosed()) {
            return (false);
        }

        return (true);
    }


    /**
     *  Tests if the list is empty.
     *
     *  @return <code>true</code> if the list is empty
     *          retrieved, <code>false</code> otherwise.
     */

    public boolean isEmpty() {
        synchronized (this.stack) {
            if (this.stack == null) {
                return (true);
            }
            
            if (this.stack.size() == 0) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Adds a <code>SupersedingEvent</code> to this <code>SupersedingEventList</code>.
     *
     *  @param supersedingEvent the <code>SupersedingEvent</code> to add
     *  @throws org.osid.IllegalStateException this list already closed
     *  @throws org.osid.NullArgumentException <code>supersedingEvent</code>
     *          is <code>null</code>
     */

    public void addSupersedingEvent(org.osid.calendaring.SupersedingEvent supersedingEvent) {
        nullarg(supersedingEvent, "supersedingEvent");

        if (isClosed()) {
            return;
        }

        synchronized (this.stack) {
            this.stack.add(supersedingEvent);
            this.stack.notifyAll();
        }

        return;
    }


    /**
     *  Adds a <code>SupersedingEvent</code> to this <code>SupersedingEventList</code>.
     *
     *  @param supersedingEvents <code>SupersedingEvent</code> array to add
     *  @throws org.osid.IllegalStateException this list already closed
     *  @throws org.osid.NullArgumentException <code>supersedingEvents</code>
     *          is <code>null</code>
     */

    public void addSupersedingEvents(org.osid.calendaring.SupersedingEvent[] supersedingEvents) {
        nullarg(supersedingEvents, "supersedingEvents");

        if (isClosed()) {
            return;
        }

        synchronized (this.stack) {
            for (org.osid.calendaring.SupersedingEvent supersedingEvent : supersedingEvents) {
                this.stack.add(supersedingEvent);
            }

            this.stack.notifyAll();
        }

        return;
    }


    /**
     *  Adds a <code>SupersedingEvent</code> collection to this
     *  <code>SupersedingEventList</code>.
     *
     *  @param collection a <code>SupersedingEvent</code> collection
     *  @throws org.osid.IllegalStateException this list already closed
     *  @throws org.osid.NullArgumentException <code>collection</code>
     *          is <code>null</code>
     */

    public void addSupersedingEventCollection(java.util.Collection<org.osid.calendaring.SupersedingEvent> collection) {
        nullarg(collection, "collection");

        if (isClosed()) {
            return;
        }

        synchronized (this.stack) {
            this.stack.addAll(collection);
            this.stack.notifyAll();
        }

        return;
    }


    /**
     *  Set an error to be thrown on the next retrieval.
     *
     *  @param error
     *  @throws org.osid.IllegalStateException this list already closed
     */

    public void error(org.osid.OsidException error) {
        this.error = error;

        if (isClosed()) {
            return;
        }

        synchronized (this.stack) {
            this.stack.notifyAll();
        }

        return;
    }


    /**
     *  Tests if an error exists.
     *
     *  @return <code>true</code> if an error has occurred,
     *          <code>false</code> otherwise
     */

    public boolean hasError() {
        if (this.error == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the error.
     *
     *  @return an error or <code>null</code> if none
     */

    protected org.osid.OsidException getError() {
        return (this.error);
    }


    /**
     *  There are no more elements to be added to this list.
     *
     *  @throws org.osid.IllegalStateException this list already
     *          closed
     */

    public void eol() {
        this.eol = true;
        
        if (isClosed()) {
            return;
        }

        synchronized (this.stack) {
            this.stack.notifyAll();
        }

        return;
    }
}
