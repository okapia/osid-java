//
// AbstractProcedureSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recipe.procedure.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractProcedureSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.recipe.ProcedureSearchResults {

    private org.osid.recipe.ProcedureList procedures;
    private final org.osid.recipe.ProcedureQueryInspector inspector;
    private final java.util.Collection<org.osid.recipe.records.ProcedureSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractProcedureSearchResults.
     *
     *  @param procedures the result set
     *  @param procedureQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>procedures</code>
     *          or <code>procedureQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractProcedureSearchResults(org.osid.recipe.ProcedureList procedures,
                                            org.osid.recipe.ProcedureQueryInspector procedureQueryInspector) {
        nullarg(procedures, "procedures");
        nullarg(procedureQueryInspector, "procedure query inspectpr");

        this.procedures = procedures;
        this.inspector = procedureQueryInspector;

        return;
    }


    /**
     *  Gets the procedure list resulting from a search.
     *
     *  @return a procedure list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.recipe.ProcedureList getProcedures() {
        if (this.procedures == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.recipe.ProcedureList procedures = this.procedures;
        this.procedures = null;
	return (procedures);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.recipe.ProcedureQueryInspector getProcedureQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  procedure search record <code> Type. </code> This method must
     *  be used to retrieve a procedure implementing the requested
     *  record.
     *
     *  @param procedureSearchRecordType a procedure search 
     *         record type 
     *  @return the procedure search
     *  @throws org.osid.NullArgumentException
     *          <code>procedureSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(procedureSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.recipe.records.ProcedureSearchResultsRecord getProcedureSearchResultsRecord(org.osid.type.Type procedureSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.recipe.records.ProcedureSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(procedureSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(procedureSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record procedure search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addProcedureRecord(org.osid.recipe.records.ProcedureSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "procedure record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
