//
// AbstractPostEntryLookupSession.java
//
//    A starter implementation framework for providing a PostEntry
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.posting.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a PostEntry
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getPostEntries(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractPostEntryLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.financials.posting.PostEntryLookupSession {

    private boolean pedantic  = false;
    private boolean federated = false;
    private org.osid.financials.Business business = new net.okapia.osid.jamocha.nil.financials.business.UnknownBusiness();
    

    /**
     *  Gets the <code>Business/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Business Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBusinessId() {
        return (this.business.getId());
    }


    /**
     *  Gets the <code>Business</code> associated with this 
     *  session.
     *
     *  @return the <code>Business</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.Business getBusiness()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.business);
    }


    /**
     *  Sets the <code>Business</code>.
     *
     *  @param  business the business for this session
     *  @throws org.osid.NullArgumentException <code>business</code>
     *          is <code>null</code>
     */

    protected void setBusiness(org.osid.financials.Business business) {
        nullarg(business, "business");
        this.business = business;
        return;
    }


    /**
     *  Tests if this user can perform <code>PostEntry</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupPostEntries() {
        return (true);
    }


    /**
     *  A complete view of the <code>PostEntry</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativePostEntryView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>PostEntry</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryPostEntryView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include post entries in businesses which are children
     *  of this business in the business hierarchy.
     */

    @OSID @Override
    public void useFederatedBusinessView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this business only.
     */

    @OSID @Override
    public void useIsolatedBusinessView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>PostEntry</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>PostEntry</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>PostEntry</code> and
     *  retained for compatibility.
     *
     *  @param  postEntryId <code>Id</code> of the
     *          <code>PostEntry</code>
     *  @return the post entry
     *  @throws org.osid.NotFoundException <code>postEntryId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>postEntryId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntry getPostEntry(org.osid.id.Id postEntryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.financials.posting.PostEntryList postEntries = getPostEntries()) {
            while (postEntries.hasNext()) {
                org.osid.financials.posting.PostEntry postEntry = postEntries.getNextPostEntry();
                if (postEntry.getId().equals(postEntryId)) {
                    return (postEntry);
                }
            }
        } 

        throw new org.osid.NotFoundException(postEntryId + " not found");
    }


    /**
     *  Gets a <code>PostEntryList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  postEntries specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>PostEntries</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getPostEntries()</code>.
     *
     *  @param  postEntryIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>PostEntry</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>postEntryIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesByIds(org.osid.id.IdList postEntryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.financials.posting.PostEntry> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = postEntryIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getPostEntry(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("post entry " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.financials.posting.postentry.LinkedPostEntryList(ret));
    }


    /**
     *  Gets a <code>PostEntryList</code> corresponding to the given
     *  post entry genus <code>Type</code> which does not include
     *  post entries of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  post entries or an error results. Otherwise, the returned list
     *  may contain only those post entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getPostEntries()</code>.
     *
     *  @param  postEntryGenusType a postEntry genus type 
     *  @return the returned <code>PostEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>postEntryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesByGenusType(org.osid.type.Type postEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.financials.posting.postentry.PostEntryGenusFilterList(getPostEntries(), postEntryGenusType));
    }


    /**
     *  Gets a <code>PostEntryList</code> corresponding to the given
     *  post entry genus <code>Type</code> and include any additional
     *  post entries with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  post entries or an error results. Otherwise, the returned list
     *  may contain only those post entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getPostEntries()</code>.
     *
     *  @param  postEntryGenusType a postEntry genus type 
     *  @return the returned <code>PostEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>postEntryGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesByParentGenusType(org.osid.type.Type postEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getPostEntriesByGenusType(postEntryGenusType));
    }


    /**
     *  Gets a <code>PostEntryList</code> containing the given
     *  post entry record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  post entries or an error results. Otherwise, the returned list
     *  may contain only those post entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getPostEntries()</code>.
     *
     *  @param  postEntryRecordType a postEntry record type 
     *  @return the returned <code>PostEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>postEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesByRecordType(org.osid.type.Type postEntryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.financials.posting.postentry.PostEntryRecordFilterList(getPostEntries(), postEntryRecordType));
    }


    /**
     *  Gets a <code>PostEntryList</code> for the given post.
     *  
     *  In plenary mode, the returned list contains all known post
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those post entries that are accessible through
     *  this session.
     *
     *  @param  postId a post <code>Id /code> 
     *  @return the returned <code>PostEntry</code> list 
     *  @throws org.osid.NullArgumentException <code>postId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesForPost(org.osid.id.Id postId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.financials.posting.postentry.PostEntryFilterList(new PostFilter(postId), getPostEntries()));
    }


    /**
     *  Gets a <code>PostEntryList</code> in the given fiscal period.
     *  
     *  In plenary mode, the returned list contains all known post
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those post entries that are accessible through
     *  this session.
     *
     *  @param  fiscalPeriodId a fiscal period <code> Id </code> 
     *  @return the returned <code>PostEntry</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>fiscalPeriodId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesByFiscalPeriod(org.osid.id.Id fiscalPeriodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.financials.posting.PostEntry> ret = new java.util.ArrayList<>();

        try (org.osid.financials.posting.PostEntryList entries = getPostEntries()) {
            while (entries.hasNext()) {
                org.osid.financials.posting.PostEntry entry = entries.getNextPostEntry();
                org.osid.financials.posting.Post post = entry.getPost();
                if (post.getFiscalPeriodId().equals(fiscalPeriodId)) {
                    ret.add(entry);
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.financials.posting.postentry.LinkedPostEntryList(ret));
    }


    /**
     *  Gets a <code>PostEntryList</code> posted within given date
     *  range inclusive.
     *  
     *  In plenary mode, the returned list contains all known post
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those post entries that are accessible through
     *  this session.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>PostEntry</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesByDate(org.osid.calendaring.DateTime from, 
                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.financials.posting.PostEntry> ret = new java.util.ArrayList<>();

        try (org.osid.financials.posting.PostEntryList entries = getPostEntries()) {
            while (entries.hasNext()) {
                org.osid.financials.posting.PostEntry entry = entries.getNextPostEntry();
                org.osid.financials.posting.Post post = entry.getPost();
                if (!(post.getDate().isLess(from) || post.getDate().isGreater(to))) {
                    ret.add(entry);
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.financials.posting.postentry.LinkedPostEntryList(ret));
    }


    /**
     *  Gets a <code> PostEntryList </code> for the given account.
     *  
     *  In plenary mode, the returned list contains all known post
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those post entries that are accessible through
     *  this session.
     *
     *  @param  accountId an account <code>Id</code> 
     *  @return the returned <code>PostEntry</code> list 
     *  @throws org.osid.NullArgumentException <code>accountId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesByAccount(org.osid.id.Id accountId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.financials.posting.postentry.PostEntryFilterList(new AccountFilter(accountId), getPostEntries()));
    }


    /**
     *  Gets a <code>PostEntryList</code> for the given account in a
     *  fiscal period.
     *  
     *  In plenary mode, the returned list contains all known post
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those post entries that are accessible through
     *  this session.
     *
     *  @param  accountId an account <code>Id</code> 
     *  @param  fiscalPeriodId a fiscal period <code>Id</code> 
     *  @return the returned <code>PostEntry</code> list 
     *  @throws org.osid.NullArgumentException <code>accountId</code> or 
     *          <code>fiscalPeriodId</code> is <code>null</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesByAccountAndFiscalPeriod(org.osid.id.Id accountId, 
                                                                                            org.osid.id.Id fiscalPeriodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.financials.posting.postentry.PostEntryFilterList(new AccountFilter(accountId), getPostEntriesByFiscalPeriod(fiscalPeriodId)));
    }


    /**
     *  Gets a <code>PostEntryList</code> for the given activity.
     *  
     *  In plenary mode, the returned list contains all known post
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those post entries that are accessible through
     *  this session.
     *
     *  @param  activityId an activity <code>Id</code> 
     *  @return the returned <code>PostEntry</code> list 
     *  @throws org.osid.NullArgumentException <code>activityId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesByActivity(org.osid.id.Id activityId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.financials.posting.postentry.PostEntryFilterList(new ActivityFilter(activityId), getPostEntries()));
    }


    /**
     *  Gets a <code>PostEntryList</code> for the given activity in a
     *  fiscal period.
     *  
     *  In plenary mode, the returned list contains all known post
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those post entries that are accessible through
     *  this session.
     *
     *  @param  activityId an activity <code>Id</code> 
     *  @param  fiscalPeriodId a fiscal period <code>Id</code> 
     *  @return the returned <code>PostEntry</code> list 
     *  @throws org.osid.NullArgumentException <code>activityId</code>
     *          or <code>fiscalPeriodId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesByActivityAndFiscalPeriod(org.osid.id.Id activityId, 
                                                                                             org.osid.id.Id fiscalPeriodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.financials.posting.postentry.PostEntryFilterList(new ActivityFilter(activityId), getPostEntriesByFiscalPeriod(fiscalPeriodId)));
    }


    /**
     *  Gets a <code>PostEntryList</code> for the given activity and
     *  account.
     *  
     *  In plenary mode, the returned list contains all known post
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those post entries that are accessible through
     *  this session.
     *
     *  @param  accountId an account <code>Id</code> 
     *  @param  activityId an activity <code>Id</code> 
     *  @return the returned <code>PostEntry</code> list 
     *  @throws org.osid.NullArgumentException <code>accountId</code>
     *          or <code>activityId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesByAccountAndActivity(org.osid.id.Id accountId, 
                                                                                        org.osid.id.Id activityId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.financials.posting.postentry.PostEntryFilterList(new ActivityFilter(activityId), getPostEntriesByAccount(accountId)));
    }


    /**
     *  Gets a <code>PostEntryList</code> for the given account in a fiscal 
     *  period.
     *  
     *  In plenary mode, the returned list contains all known post
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those post entries that are accessible through
     *  this session.
     *
     *  @param  accountId an account <code>Id</code> 
     *  @param  activityId an activity <code>Id</code> 
     *  @param  fiscalPeriodId a fiscal period <code>Id</code> 
     *  @return the returned <code>PostEntry</code> list 
     *  @throws org.osid.NullArgumentException <code>accountId</code>,
     *          <code>activityId</code>, or
     *          <code>fiscalPeriodId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesByAccountAndActivityAndFiscalPeriod(org.osid.id.Id accountId, 
                                                                                                       org.osid.id.Id activityId, 
                                                                                                       org.osid.id.Id fiscalPeriodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.financials.posting.postentry.PostEntryFilterList(new AccountFilter(accountId), getPostEntriesByActivityAndFiscalPeriod(activityId, fiscalPeriodId)));
    }


    /**
     *  Gets all <code>PostEntries</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  post entries or an error results. Otherwise, the returned list
     *  may contain only those post entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>PostEntries</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.financials.posting.PostEntryList getPostEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the post entry list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of post entries
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.financials.posting.PostEntryList filterPostEntriesOnViews(org.osid.financials.posting.PostEntryList list)
        throws org.osid.OperationFailedException {

        return (list);
    }


    public static class AccountFilter
        implements net.okapia.osid.jamocha.inline.filter.financials.posting.postentry.PostEntryFilter {

        private final org.osid.id.Id accountId;

        
        /**
         *  Constructs a new <code>AccountFilterList</code>.
         *
         *  @param accountId the account to filter
         *  @throws org.osid.NullArgumentException
         *          <code>accountId</code> is <code>null</code>
         */

        public AccountFilter(org.osid.id.Id accountId) {
            nullarg(accountId, "account Id");
            this.accountId = accountId;
            return;
        }


        /**
         *  Used by the PostEntryFilterList to filter the post entry
         *  list based on account.
         *
         *  @param postEntry the post entry
         *  @return <code>true</code> to pass the post entry,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.financials.posting.PostEntry postEntry) {
            return (postEntry.getAccountId().equals(this.accountId));
        }
    }


    public static class ActivityFilter
        implements net.okapia.osid.jamocha.inline.filter.financials.posting.postentry.PostEntryFilter {

        private final org.osid.id.Id activityId;

        
        /**
         *  Constructs a new <code>ActivityFilterList</code>.
         *
         *  @param activityId the activity to filter
         *  @throws org.osid.NullArgumentException
         *          <code>activityId</code> is <code>null</code>
         */

        public ActivityFilter(org.osid.id.Id activityId) {
            nullarg(activityId, "activity Id");
            this.activityId = activityId;
            return;
        }


        /**
         *  Used by the PostEntryFilterList to filter the post entry
         *  list based on activity.
         *
         *  @param postEntry the post entry
         *  @return <code>true</code> to pass the post entry,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.financials.posting.PostEntry postEntry) {
            return (postEntry.getActivityId().equals(this.activityId));
        }
    }


    public static class PostFilter
        implements net.okapia.osid.jamocha.inline.filter.financials.posting.postentry.PostEntryFilter {

        private final org.osid.id.Id postId;

        
        /**
         *  Constructs a new <code>PostFilterList</code>.
         *
         *  @param postId the post to filter
         *  @throws org.osid.NullArgumentException
         *          <code>postId</code> is <code>null</code>
         */

        public PostFilter(org.osid.id.Id postId) {
            nullarg(postId, "post Id");
            this.postId = postId;
            return;
        }


        /**
         *  Used by the PostEntryFilterList to filter the post entry
         *  list based on post.
         *
         *  @param postEntry the post entry
         *  @return <code>true</code> to pass the post entry,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.financials.posting.PostEntry postEntry) {
            return (postEntry.getPostId().equals(this.postId));
        }
    }
}
