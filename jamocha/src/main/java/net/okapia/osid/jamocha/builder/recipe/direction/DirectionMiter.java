//
// DirectionMiter.java
//
//     Defines a Direction miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.recipe.direction;


/**
 *  Defines a <code>Direction</code> miter for use with the builders.
 */

public interface DirectionMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidObjectMiter,
            org.osid.recipe.Direction {


    /**
     *  Sets the recipe.
     *
     *  @param recipe a recipe
     *  @throws org.osid.NullArgumentException <code>recipe</code> is
     *          <code>null</code>
     */

    public void setRecipe(org.osid.recipe.Recipe recipe);


    /**
     *  Adds a procedure.
     *
     *  @param procedure a procedure
     *  @throws org.osid.NullArgumentException <code>procedure</code>
     *          is <code>null</code>
     */

    public void addProcedure(org.osid.recipe.Procedure procedure);


    /**
     *  Sets all the procedures.
     *
     *  @param procedures a collection of procedures
     *  @throws org.osid.NullArgumentException <code>procedures</code>
     *          is <code>null</code>
     */

    public void setProcedures(java.util.Collection<org.osid.recipe.Procedure> procedures);


    /**
     *  Adds an ingredient.
     *
     *  @param ingredient an ingredient
     *  @throws org.osid.NullArgumentException <code>ingredient</code>
     *          is <code>null</code>
     */

    public void addIngredient(org.osid.recipe.Ingredient ingredient);


    /**
     *  Sets all the ingredients.
     *
     *  @param ingredients a collection of ingredients
     *  @throws org.osid.NullArgumentException
     *          <code>ingredients</code> is <code>null</code>
     */

    public void setIngredients(java.util.Collection<org.osid.recipe.Ingredient> ingredients);


    /**
     *  Sets the estimated duration.
     *
     *  @param duration an estimated duration
     *  @throws org.osid.NullArgumentException <code>duration</code>
     *          is <code>null</code>
     */

    public void setEstimatedDuration(org.osid.calendaring.Duration duration);


    /**
     *  Adds an asset.
     *
     *  @param asset an asset
     *  @throws org.osid.NullArgumentException <code>asset</code> is
     *          <code>null</code>
     */

    public void addAsset(org.osid.repository.Asset asset);


    /**
     *  Sets all the assets.
     *
     *  @param assets a collection of assets
     *  @throws org.osid.NullArgumentException <code>assets</code> is
     *          <code>null</code>
     */

    public void setAssets(java.util.Collection<org.osid.repository.Asset> assets);


    /**
     *  Adds a Direction record.
     *
     *  @param record a direction record
     *  @param recordType the type of direction record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addDirectionRecord(org.osid.recipe.records.DirectionRecord record, org.osid.type.Type recordType);
}       


