//
// AbstractIndexedMapPriceScheduleLookupSession.java
//
//    A simple framework for providing a PriceSchedule lookup service
//    backed by a fixed collection of price schedules with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.ordering.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a PriceSchedule lookup service backed by a
 *  fixed collection of price schedules. The price schedules are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some price schedules may be compatible
 *  with more types than are indicated through these price schedule
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>PriceSchedules</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapPriceScheduleLookupSession
    extends AbstractMapPriceScheduleLookupSession
    implements org.osid.ordering.PriceScheduleLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.ordering.PriceSchedule> priceSchedulesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.ordering.PriceSchedule>());
    private final MultiMap<org.osid.type.Type, org.osid.ordering.PriceSchedule> priceSchedulesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.ordering.PriceSchedule>());


    /**
     *  Makes a <code>PriceSchedule</code> available in this session.
     *
     *  @param  priceSchedule a price schedule
     *  @throws org.osid.NullArgumentException <code>priceSchedule<code> is
     *          <code>null</code>
     */

    @Override
    protected void putPriceSchedule(org.osid.ordering.PriceSchedule priceSchedule) {
        super.putPriceSchedule(priceSchedule);

        this.priceSchedulesByGenus.put(priceSchedule.getGenusType(), priceSchedule);
        
        try (org.osid.type.TypeList types = priceSchedule.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.priceSchedulesByRecord.put(types.getNextType(), priceSchedule);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a price schedule from this session.
     *
     *  @param priceScheduleId the <code>Id</code> of the price schedule
     *  @throws org.osid.NullArgumentException <code>priceScheduleId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removePriceSchedule(org.osid.id.Id priceScheduleId) {
        org.osid.ordering.PriceSchedule priceSchedule;
        try {
            priceSchedule = getPriceSchedule(priceScheduleId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.priceSchedulesByGenus.remove(priceSchedule.getGenusType());

        try (org.osid.type.TypeList types = priceSchedule.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.priceSchedulesByRecord.remove(types.getNextType(), priceSchedule);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removePriceSchedule(priceScheduleId);
        return;
    }


    /**
     *  Gets a <code>PriceScheduleList</code> corresponding to the given
     *  price schedule genus <code>Type</code> which does not include
     *  price schedules of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known price schedules or an error results. Otherwise,
     *  the returned list may contain only those price schedules that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  priceScheduleGenusType a price schedule genus type 
     *  @return the returned <code>PriceSchedule</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>priceScheduleGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleList getPriceSchedulesByGenusType(org.osid.type.Type priceScheduleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.ordering.priceschedule.ArrayPriceScheduleList(this.priceSchedulesByGenus.get(priceScheduleGenusType)));
    }


    /**
     *  Gets a <code>PriceScheduleList</code> containing the given
     *  price schedule record <code>Type</code>. In plenary mode, the
     *  returned list contains all known price schedules or an error
     *  results. Otherwise, the returned list may contain only those
     *  price schedules that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  priceScheduleRecordType a price schedule record type 
     *  @return the returned <code>priceSchedule</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>priceScheduleRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleList getPriceSchedulesByRecordType(org.osid.type.Type priceScheduleRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.ordering.priceschedule.ArrayPriceScheduleList(this.priceSchedulesByRecord.get(priceScheduleRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.priceSchedulesByGenus.clear();
        this.priceSchedulesByRecord.clear();

        super.close();

        return;
    }
}
