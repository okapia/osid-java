//
// ConfigurationElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.configuration.configuration.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class ConfigurationElements
    extends net.okapia.osid.jamocha.spi.OsidCatalogElements {


    /**
     *  Gets the ConfigurationElement Id.
     *
     *  @return the configuration element Id
     */

    public static org.osid.id.Id getConfigurationEntityId() {
        return (makeEntityId("osid.configuration.Configuration"));
    }


    /**
     *  Gets the Registry element Id.
     *
     *  @return the Registry element Id
     */

    public static org.osid.id.Id getRegistry() {
        return (makeElementId("osid.configuration.configuration.Registry"));
    }


    /**
     *  Gets the ParameterId element Id.
     *
     *  @return the ParameterId element Id
     */

    public static org.osid.id.Id getParameterId() {
        return (makeQueryElementId("osid.configuration.configuration.ParameterId"));
    }


    /**
     *  Gets the Parameter element Id.
     *
     *  @return the Parameter element Id
     */

    public static org.osid.id.Id getParameter() {
        return (makeQueryElementId("osid.configuration.configuration.Parameter"));
    }


    /**
     *  Gets the AncestorConfigurationId element Id.
     *
     *  @return the AncestorConfigurationId element Id
     */

    public static org.osid.id.Id getAncestorConfigurationId() {
        return (makeQueryElementId("osid.configuration.configuration.AncestorConfigurationId"));
    }


    /**
     *  Gets the AncestorConfiguration element Id.
     *
     *  @return the AncestorConfiguration element Id
     */

    public static org.osid.id.Id getAncestorConfiguration() {
        return (makeQueryElementId("osid.configuration.configuration.AncestorConfiguration"));
    }


    /**
     *  Gets the DescendantConfigurationId element Id.
     *
     *  @return the DescendantConfigurationId element Id
     */

    public static org.osid.id.Id getDescendantConfigurationId() {
        return (makeQueryElementId("osid.configuration.configuration.DescendantConfigurationId"));
    }


    /**
     *  Gets the DescendantConfiguration element Id.
     *
     *  @return the DescendantConfiguration element Id
     */

    public static org.osid.id.Id getDescendantConfiguration() {
        return (makeQueryElementId("osid.configuration.configuration.DescendantConfiguration"));
    }
}
