//
// AbstractNodeFrontOfficeHierarchySession.java
//
//     Defines a FrontOffice hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.tracking.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a front office hierarchy session for delivering a hierarchy
 *  of front offices using the FrontOfficeNode interface.
 */

public abstract class AbstractNodeFrontOfficeHierarchySession
    extends net.okapia.osid.jamocha.tracking.spi.AbstractFrontOfficeHierarchySession
    implements org.osid.tracking.FrontOfficeHierarchySession {

    private java.util.Collection<org.osid.tracking.FrontOfficeNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root front office <code> Ids </code> in this hierarchy.
     *
     *  @return the root front office <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootFrontOfficeIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.tracking.frontofficenode.FrontOfficeNodeToIdList(this.roots));
    }


    /**
     *  Gets the root front offices in the front office hierarchy. A node
     *  with no parents is an orphan. While all front office <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root front offices 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeList getRootFrontOffices()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.tracking.frontofficenode.FrontOfficeNodeToFrontOfficeList(new net.okapia.osid.jamocha.tracking.frontofficenode.ArrayFrontOfficeNodeList(this.roots)));
    }


    /**
     *  Adds a root front office node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootFrontOffice(org.osid.tracking.FrontOfficeNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root front office nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootFrontOffices(java.util.Collection<org.osid.tracking.FrontOfficeNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root front office node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootFrontOffice(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.tracking.FrontOfficeNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> FrontOffice </code> has any parents. 
     *
     *  @param  frontOfficeId a front office <code> Id </code> 
     *  @return <code> true </code> if the front office has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> frontOfficeId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> frontOfficeId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentFrontOffices(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getFrontOfficeNode(frontOfficeId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of a
     *  front office.
     *
     *  @param  id an <code> Id </code> 
     *  @param  frontOfficeId the <code> Id </code> of a front office 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> frontOfficeId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> frontOfficeId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> frontOfficeId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfFrontOffice(org.osid.id.Id id, org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.tracking.FrontOfficeNodeList parents = getFrontOfficeNode(frontOfficeId).getParentFrontOfficeNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextFrontOfficeNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given front office. 
     *
     *  @param  frontOfficeId a front office <code> Id </code> 
     *  @return the parent <code> Ids </code> of the front office 
     *  @throws org.osid.NotFoundException <code> frontOfficeId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> frontOfficeId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentFrontOfficeIds(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.tracking.frontoffice.FrontOfficeToIdList(getParentFrontOffices(frontOfficeId)));
    }


    /**
     *  Gets the parents of the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> to query 
     *  @return the parents of the front office 
     *  @throws org.osid.NotFoundException <code> frontOfficeId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> frontOfficeId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeList getParentFrontOffices(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.tracking.frontofficenode.FrontOfficeNodeToFrontOfficeList(getFrontOfficeNode(frontOfficeId).getParentFrontOfficeNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of a
     *  front office.
     *
     *  @param  id an <code> Id </code> 
     *  @param  frontOfficeId the Id of a front office 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> frontOfficeId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> frontOfficeId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> frontOfficeId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfFrontOffice(org.osid.id.Id id, org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfFrontOffice(id, frontOfficeId)) {
            return (true);
        }

        try (org.osid.tracking.FrontOfficeList parents = getParentFrontOffices(frontOfficeId)) {
            while (parents.hasNext()) {
                if (isAncestorOfFrontOffice(id, parents.getNextFrontOffice().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if a front office has any children. 
     *
     *  @param  frontOfficeId a front office <code> Id </code> 
     *  @return <code> true </code> if the <code> frontOfficeId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> frontOfficeId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> frontOfficeId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildFrontOffices(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getFrontOfficeNode(frontOfficeId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of a
     *  front office.
     *
     *  @param  id an <code> Id </code> 
     *  @param frontOfficeId the <code> Id </code> of a 
     *         front office
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> frontOfficeId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> frontOfficeId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> frontOfficeId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfFrontOffice(org.osid.id.Id id, org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfFrontOffice(frontOfficeId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  front office.
     *
     *  @param  frontOfficeId the <code> Id </code> to query 
     *  @return the children of the front office 
     *  @throws org.osid.NotFoundException <code> frontOfficeId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> frontOfficeId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildFrontOfficeIds(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.tracking.frontoffice.FrontOfficeToIdList(getChildFrontOffices(frontOfficeId)));
    }


    /**
     *  Gets the children of the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> to query 
     *  @return the children of the front office 
     *  @throws org.osid.NotFoundException <code> frontOfficeId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> frontOfficeId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeList getChildFrontOffices(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.tracking.frontofficenode.FrontOfficeNodeToFrontOfficeList(getFrontOfficeNode(frontOfficeId).getChildFrontOfficeNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of a
     *  front office.
     *
     *  @param  id an <code> Id </code> 
     *  @param frontOfficeId the <code> Id </code> of a 
     *         front office
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> frontOfficeId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> frontOfficeId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> frontOfficeId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfFrontOffice(org.osid.id.Id id, org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfFrontOffice(frontOfficeId, id)) {
            return (true);
        }

        try (org.osid.tracking.FrontOfficeList children = getChildFrontOffices(frontOfficeId)) {
            while (children.hasNext()) {
                if (isDescendantOfFrontOffice(id, children.getNextFrontOffice().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  front office.
     *
     *  @param  frontOfficeId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified front office node 
     *  @throws org.osid.NotFoundException <code> frontOfficeId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> frontOfficeId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getFrontOfficeNodeIds(org.osid.id.Id frontOfficeId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.tracking.frontofficenode.FrontOfficeNodeToNode(getFrontOfficeNode(frontOfficeId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given front office.
     *
     *  @param  frontOfficeId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified front office node 
     *  @throws org.osid.NotFoundException <code> frontOfficeId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> frontOfficeId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeNode getFrontOfficeNodes(org.osid.id.Id frontOfficeId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getFrontOfficeNode(frontOfficeId));
    }


    /**
     *  Closes this <code>FrontOfficeHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets a front office node.
     *
     *  @param frontOfficeId the id of the front office node
     *  @throws org.osid.NotFoundException <code>frontOfficeId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>frontOfficeId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.tracking.FrontOfficeNode getFrontOfficeNode(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(frontOfficeId, "front office Id");
        for (org.osid.tracking.FrontOfficeNode frontOffice : this.roots) {
            if (frontOffice.getId().equals(frontOfficeId)) {
                return (frontOffice);
            }

            org.osid.tracking.FrontOfficeNode r = findFrontOffice(frontOffice, frontOfficeId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(frontOfficeId + " is not found");
    }


    protected org.osid.tracking.FrontOfficeNode findFrontOffice(org.osid.tracking.FrontOfficeNode node, 
                                                                org.osid.id.Id frontOfficeId)
	throws org.osid.OperationFailedException {

        try (org.osid.tracking.FrontOfficeNodeList children = node.getChildFrontOfficeNodes()) {
            while (children.hasNext()) {
                org.osid.tracking.FrontOfficeNode frontOffice = children.getNextFrontOfficeNode();
                if (frontOffice.getId().equals(frontOfficeId)) {
                    return (frontOffice);
                }
                
                frontOffice = findFrontOffice(frontOffice, frontOfficeId);
                if (frontOffice != null) {
                    return (frontOffice);
                }
            }
        }

        return (null);
    }
}
