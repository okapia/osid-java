//
// AbstractExtensibleBuilder.java
//
//     Defines a builder for a Extensible.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.spi;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines the Extensible builder.
 */

public abstract class AbstractExtensibleBuilder<T extends AbstractExtensibleBuilder<? extends T>>
    extends AbstractBuilder<T> {

    private final ExtensibleMiter extensible;


    /**
     *  Creates a new <code>AbstractExtensibleBuilder</code>.
     *
     *  @param extensible an extensible miter interface
     *  @throws org.osid.NullArgumentException <code>extensible</code>
     *          is <code>null</code>
     */

    protected AbstractExtensibleBuilder(ExtensibleMiter extensible) {
        nullarg(extensible, "extensible");
        this.extensible = extensible;
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    public T recordType(org.osid.type.Type recordType) {
        this.extensible.addRecordType(recordType);
        return (self());
    }
}
