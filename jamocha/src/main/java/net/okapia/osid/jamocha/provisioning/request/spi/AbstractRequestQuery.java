//
// AbstractRequestQuery.java
//
//     A template for making a Request Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.request.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for requests.
 */

public abstract class AbstractRequestQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQuery
    implements org.osid.provisioning.RequestQuery {

    private final java.util.Collection<org.osid.provisioning.records.RequestQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the request transaction <code> Id </code> for this query. 
     *
     *  @param  transactionId the request transaction <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> transactionId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRequestTransactionId(org.osid.id.Id transactionId, 
                                          boolean match) {
        return;
    }


    /**
     *  Clears the request transaction <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRequestTransactionIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> RequestTransactionQuery </code> is available. 
     *
     *  @return <code> true </code> if a request transaction query is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequestTransactionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a request transaction. Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the request transaction query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequestTransactionQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransactionQuery getRequestTransactionQuery() {
        throw new org.osid.UnimplementedException("supportsRequestTransactionQuery() is false");
    }


    /**
     *  Clears the request transaction query terms. 
     */

    @OSID @Override
    public void clearRequestTransactionTerms() {
        return;
    }


    /**
     *  Sets the queue <code> Id </code> for this query. 
     *
     *  @param  queueId the queue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> queueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchQueueId(org.osid.id.Id queueId, boolean match) {
        return;
    }


    /**
     *  Clears the queue <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearQueueIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> QueueQuery </code> is available. 
     *
     *  @return <code> true </code> if a queue query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a queue. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the queue query 
     *  @throws org.osid.UnimplementedException <code> supportsQueueQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.QueueQuery getQueueQuery() {
        throw new org.osid.UnimplementedException("supportsQueueQuery() is false");
    }


    /**
     *  Clears the queue query terms. 
     */

    @OSID @Override
    public void clearQueueTerms() {
        return;
    }


    /**
     *  Matches requests with a date in the given range inclusive. 
     *
     *  @param  from the range start 
     *  @param  to the range end 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchRequestDate(org.osid.calendaring.DateTime from, 
                                 org.osid.calendaring.DateTime to, 
                                 boolean match) {
        return;
    }


    /**
     *  Clears the request date query terms. 
     */

    @OSID @Override
    public void clearRequestDateTerms() {
        return;
    }


    /**
     *  Sets the resource <code> Id </code> for this query. 
     *
     *  @param  resourceId the resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRequesterId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the resource <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRequesterIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequesterQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequesterQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getRequesterQuery() {
        throw new org.osid.UnimplementedException("supportsRequesterQuery() is false");
    }


    /**
     *  Clears the resource query terms. 
     */

    @OSID @Override
    public void clearRequesterTerms() {
        return;
    }


    /**
     *  Sets the agent <code> Id </code> for this query. 
     *
     *  @param  agentId the agent <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRequestingAgentId(org.osid.id.Id agentId, boolean match) {
        return;
    }


    /**
     *  Clears the agent <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRequestingAgentIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequestingAgentQuery() {
        return (false);
    }


    /**
     *  Gets the query for an agent. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequestingAgentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getRequestingAgentQuery() {
        throw new org.osid.UnimplementedException("supportsRequestingAgentQuery() is false");
    }


    /**
     *  Clears the agent query terms. 
     */

    @OSID @Override
    public void clearRequestingAgentTerms() {
        return;
    }


    /**
     *  Sets the pool <code> Id </code> for this query. 
     *
     *  @param  poolId the pool <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> poolId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchPoolId(org.osid.id.Id poolId, boolean match) {
        return;
    }


    /**
     *  Clears the pool <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearPoolIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> PoolQuery </code> is available. 
     *
     *  @return <code> true </code> if a pool query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolQuery() {
        return (false);
    }


    /**
     *  Gets the query for a pool. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the pool query 
     *  @throws org.osid.UnimplementedException <code> supportsPoolQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolQuery getPoolQuery() {
        throw new org.osid.UnimplementedException("supportsPoolQuery() is false");
    }


    /**
     *  Matches requests with any pool. 
     *
     *  @param  match <code> true </code> to match requests with any poo, 
     *          <code> false </code> to match requests with no pool 
     */

    @OSID @Override
    public void matchAnyPool(boolean match) {
        return;
    }


    /**
     *  Clears the pool query terms. 
     */

    @OSID @Override
    public void clearPoolTerms() {
        return;
    }


    /**
     *  Sets the requested provisionable <code> Id </code> for this query. 
     *
     *  @param  provisionableId the provisionable <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> provisionableId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchRequestedProvisionableId(org.osid.id.Id provisionableId, 
                                              boolean match) {
        return;
    }


    /**
     *  Clears the requested provisionable <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRequestedProvisionableIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ProvisionableQuery </code> is available. 
     *
     *  @return <code> true </code> if a provisionable query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequestedProvisionableQuery() {
        return (false);
    }


    /**
     *  Gets the query for a requested provisionable. Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the provisionable query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequestedprovisionableQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionableQuery getRequestedProvisionableQuery() {
        throw new org.osid.UnimplementedException("supportsRequestedProvisionableQuery() is false");
    }


    /**
     *  Matches requests with any requested provisionable. 
     *
     *  @param  match <code> true </code> to match requests with any requested 
     *          provisionable, <code> false </code> to match requests with no 
     *          requested provisionables 
     */

    @OSID @Override
    public void matchAnyRequestedProvisionable(boolean match) {
        return;
    }


    /**
     *  Clears the requested provisionable query terms. 
     */

    @OSID @Override
    public void clearRequestedProvisionableTerms() {
        return;
    }


    /**
     *  Sets the exchange provision <code> Id </code> for this query. 
     *
     *  @param  provisionId the provision <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> provisionId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchExchangeProvisionId(org.osid.id.Id provisionId, 
                                         boolean match) {
        return;
    }


    /**
     *  Clears the exchange provision <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearExchangeProvisionIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ProvisionQuery </code> is available. 
     *
     *  @return <code> true </code> if a provision query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsExchangeProvisionQuery() {
        return (false);
    }


    /**
     *  Gets the query for an exchange provision. Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the provision query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsExchangeProvisionQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionQuery getExchangeProvisionQuery() {
        throw new org.osid.UnimplementedException("supportsExchangeProvisionQuery() is false");
    }


    /**
     *  Matches requests with any exchange provision. 
     *
     *  @param  match <code> true </code> to match requests with any exchange 
     *          provision, <code> false </code> to match requests with no 
     *          exchange provision 
     */

    @OSID @Override
    public void matchAnyExchangeProvision(boolean match) {
        return;
    }


    /**
     *  Clears the exchange provision query terms. 
     */

    @OSID @Override
    public void clearExchangeProvisionTerms() {
        return;
    }


    /**
     *  Sets the origin provision <code> Id </code> for this query. 
     *
     *  @param  provisionId the provision <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> provisionId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchOriginProvisionId(org.osid.id.Id provisionId, 
                                       boolean match) {
        return;
    }


    /**
     *  Clears the origin provision <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearOriginProvisionIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ProvisionQuery </code> is available. 
     *
     *  @return <code> true </code> if a provision query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOriginProvisionQuery() {
        return (false);
    }


    /**
     *  Gets the query for an origin provision. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the provision query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOriginProvisionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionQuery getOriginProvisionQuery() {
        throw new org.osid.UnimplementedException("supportsOriginProvisionQuery() is false");
    }


    /**
     *  Matches requests with any originating provision. 
     *
     *  @param  match <code> true </code> to match requests with any 
     *          originating provision, <code> false </code> to match requests 
     *          with no origininating provision 
     */

    @OSID @Override
    public void matchAnyOriginProvision(boolean match) {
        return;
    }


    /**
     *  Clears the origin provision query terms. 
     */

    @OSID @Override
    public void clearOriginProvisionTerms() {
        return;
    }


    /**
     *  Matches requests whose position is in the given range inclusive,. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     */

    @OSID @Override
    public void matchPosition(long start, long end, boolean match) {
        return;
    }


    /**
     *  Matches requests with any position. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAnyPosition(boolean match) {
        return;
    }


    /**
     *  Clears the position query terms. 
     */

    @OSID @Override
    public void clearPositionTerms() {
        return;
    }


    /**
     *  Matches requests whose estimated waiting time is in the given range 
     *  inclusive,. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchEWA(org.osid.calendaring.Duration start, 
                         org.osid.calendaring.Duration end, boolean match) {
        return;
    }


    /**
     *  Matches requests with any ewa. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAnyEWA(boolean match) {
        return;
    }


    /**
     *  Clears the ewa query terms. 
     */

    @OSID @Override
    public void clearEWATerms() {
        return;
    }


    /**
     *  Sets the distributor <code> Id </code> for this query. 
     *
     *  @param  distributorId the distributor <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDistributorId(org.osid.id.Id distributorId, boolean match) {
        return;
    }


    /**
     *  Clears the distributor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDistributorIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> DistributorQuery </code> is available. 
     *
     *  @return <code> true </code> if a distributor query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDistributorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a distributor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the distributor query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportDistributorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQuery getDistributorQuery() {
        throw new org.osid.UnimplementedException("supportsDistributorQuery() is false");
    }


    /**
     *  Clears the distributor query terms. 
     */

    @OSID @Override
    public void clearDistributorTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given request query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a request implementing the requested record.
     *
     *  @param requestRecordType a request record type
     *  @return the request query record
     *  @throws org.osid.NullArgumentException
     *          <code>requestRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(requestRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.RequestQueryRecord getRequestQueryRecord(org.osid.type.Type requestRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.records.RequestQueryRecord record : this.records) {
            if (record.implementsRecordType(requestRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(requestRecordType + " is not supported");
    }


    /**
     *  Adds a record to this request query. 
     *
     *  @param requestQueryRecord request query record
     *  @param requestRecordType request record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRequestQueryRecord(org.osid.provisioning.records.RequestQueryRecord requestQueryRecord, 
                                          org.osid.type.Type requestRecordType) {

        addRecordType(requestRecordType);
        nullarg(requestQueryRecord, "request query record");
        this.records.add(requestQueryRecord);        
        return;
    }
}
