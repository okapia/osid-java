//
// InvariantIndexedMapActivityBundleLookupSession
//
//    Implements an ActivityBundle lookup service backed by a fixed
//    collection of activityBundles indexed by their types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.registration;


/**
 *  Implements an ActivityBundle lookup service backed by a fixed
 *  collection of activity bundles. The activity bundles are indexed by
 *  {@code Id}, genus and record types.
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some activity bundles may be compatible
 *  with more types than are indicated through these activity bundle
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 */

public final class InvariantIndexedMapActivityBundleLookupSession
    extends net.okapia.osid.jamocha.core.course.registration.spi.AbstractIndexedMapActivityBundleLookupSession
    implements org.osid.course.registration.ActivityBundleLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapActivityBundleLookupSession} using an
     *  array of activityBundles.
     *
     *  @param courseCatalog the course catalog
     *  @param activityBundles an array of activity bundles
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code activityBundles} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapActivityBundleLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                    org.osid.course.registration.ActivityBundle[] activityBundles) {

        setCourseCatalog(courseCatalog);
        putActivityBundles(activityBundles);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapActivityBundleLookupSession} using a
     *  collection of activity bundles.
     *
     *  @param courseCatalog the course catalog
     *  @param activityBundles a collection of activity bundles
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code activityBundles} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapActivityBundleLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                    java.util.Collection<? extends org.osid.course.registration.ActivityBundle> activityBundles) {

        setCourseCatalog(courseCatalog);
        putActivityBundles(activityBundles);
        return;
    }
}
