//
// VoteMiter.java
//
//     Defines a Vote miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.voting.vote;


/**
 *  Defines a <code>Vote</code> miter for use with the builders.
 */

public interface VoteMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidRelationshipMiter,
            org.osid.voting.Vote {


    /**
     *  Sets the candidate.
     *
     *  @param candidate a candidate
     *  @throws org.osid.NullArgumentException <code>candidate</code>
     *          is <code>null</code>
     */

    public void setCandidate(org.osid.voting.Candidate candidate);


    /**
     *  Sets the voter.
     *
     *  @param voter a voter
     *  @throws org.osid.NullArgumentException <code>voter</code> is
     *          <code>null</code>
     */

    public void setVoter(org.osid.resource.Resource voter);


    /**
     *  Sets the voting agent.
     *
     *  @param agent a voting agent
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    public void setVotingAgent(org.osid.authentication.Agent agent);


    /**
     *  Sets the votes.
     *
     *  @param vote the number of votes
     */

    public void setVotes(long vote);


    /**
     *  Adds a Vote record.
     *
     *  @param record a vote record
     *  @param recordType the type of vote record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addVoteRecord(org.osid.voting.records.VoteRecord record, org.osid.type.Type recordType);
}       


