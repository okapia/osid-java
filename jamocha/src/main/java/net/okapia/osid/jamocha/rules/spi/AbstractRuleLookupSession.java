//
// AbstractRuleLookupSession.java
//
//    A starter implementation framework for providing a Rule
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Rule
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getRules(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractRuleLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.rules.RuleLookupSession {

    private boolean pedantic   = false;
    private boolean activeonly = false;
    private boolean federated  = false;
    private org.osid.rules.Engine engine = new net.okapia.osid.jamocha.nil.rules.engine.UnknownEngine();
    

    /**
     *  Gets the <code>Engine/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Engine Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getEngineId() {
        return (this.engine.getId());
    }


    /**
     *  Gets the <code>Engine</code> associated with this 
     *  session.
     *
     *  @return the <code>Engine</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.Engine getEngine()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.engine);
    }


    /**
     *  Sets the <code>Engine</code>.
     *
     *  @param  engine the engine for this session
     *  @throws org.osid.NullArgumentException <code>engine</code>
     *          is <code>null</code>
     */

    protected void setEngine(org.osid.rules.Engine engine) {
        nullarg(engine, "engine");
        this.engine = engine;
        return;
    }


    /**
     *  Tests if this user can perform <code>Rule</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupRules() {
        return (true);
    }


    /**
     *  A complete view of the <code>Rule</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeRuleView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Rule</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryRuleView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include rules in engines which are children
     *  of this engine in the engine hierarchy.
     */

    @OSID @Override
    public void useFederatedEngineView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this engine only.
     */

    @OSID @Override
    public void useIsolatedEngineView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active rules are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveRuleView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive rules are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusRuleView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>Rule</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Rule</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Rule</code> and
     *  retained for compatibility.
     *
     *  In active mode, rules are returned that are currently
     *  active. In any status mode, active and inactive rules
     *  are returned.
     *
     *  @param  ruleId <code>Id</code> of the
     *          <code>Rule</code>
     *  @return the rule
     *  @throws org.osid.NotFoundException <code>ruleId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>ruleId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.Rule getRule(org.osid.id.Id ruleId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.rules.RuleList rules = getRules()) {
            while (rules.hasNext()) {
                org.osid.rules.Rule rule = rules.getNextRule();
                if (rule.getId().equals(ruleId)) {
                    return (rule);
                }
            }
        } 

        throw new org.osid.NotFoundException(ruleId + " not found");
    }


    /**
     *  Gets a <code>RuleList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  rules specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Rules</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, rules are returned that are currently
     *  active. In any status mode, active and inactive rules
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getRules()</code>.
     *
     *  @param  ruleIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Rule</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>ruleIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.RuleList getRulesByIds(org.osid.id.IdList ruleIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.rules.Rule> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = ruleIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getRule(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("rule " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.rules.rule.LinkedRuleList(ret));
    }


    /**
     *  Gets a <code>RuleList</code> corresponding to the given
     *  rule genus <code>Type</code> which does not include
     *  rules of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  rules or an error results. Otherwise, the returned list
     *  may contain only those rules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, rules are returned that are currently
     *  active. In any status mode, active and inactive rules
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getRules()</code>.
     *
     *  @param  ruleGenusType a rule genus type 
     *  @return the returned <code>Rule</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>ruleGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.RuleList getRulesByGenusType(org.osid.type.Type ruleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.rules.rule.RuleGenusFilterList(getRules(), ruleGenusType));
    }


    /**
     *  Gets a <code>RuleList</code> corresponding to the given
     *  rule genus <code>Type</code> and include any additional
     *  rules with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  rules or an error results. Otherwise, the returned list
     *  may contain only those rules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, rules are returned that are currently
     *  active. In any status mode, active and inactive rules
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getRules()</code>.
     *
     *  @param  ruleGenusType a rule genus type 
     *  @return the returned <code>Rule</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>ruleGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.RuleList getRulesByParentGenusType(org.osid.type.Type ruleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getRulesByGenusType(ruleGenusType));
    }


    /**
     *  Gets a <code>RuleList</code> containing the given
     *  rule record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  rules or an error results. Otherwise, the returned list
     *  may contain only those rules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, rules are returned that are currently
     *  active. In any status mode, active and inactive rules
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getRules()</code>.
     *
     *  @param  ruleRecordType a rule record type 
     *  @return the returned <code>Rule</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>ruleRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.RuleList getRulesByRecordType(org.osid.type.Type ruleRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.rules.rule.RuleRecordFilterList(getRules(), ruleRecordType));
    }


    /**
     *  Gets all <code>Rules</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  rules or an error results. Otherwise, the returned list
     *  may contain only those rules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, rules are returned that are currently
     *  active. In any status mode, active and inactive rules
     *  are returned.
     *
     *  @return a list of <code>Rules</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.rules.RuleList getRules()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the rule list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of rules
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.rules.RuleList filterRulesOnViews(org.osid.rules.RuleList list)
        throws org.osid.OperationFailedException {

        org.osid.rules.RuleList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.rules.rule.ActiveRuleFilterList(ret);
        }

        return (ret);
    }
}
