//
// LogElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.logging.log.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class LogElements
    extends net.okapia.osid.jamocha.spi.OsidCatalogElements {


    /**
     *  Gets the LogElement Id.
     *
     *  @return the log element Id
     */

    public static org.osid.id.Id getLogEntityId() {
        return (makeEntityId("osid.logging.Log"));
    }


    /**
     *  Gets the LogEntryId element Id.
     *
     *  @return the LogEntryId element Id
     */

    public static org.osid.id.Id getLogEntryId() {
        return (makeQueryElementId("osid.logging.log.LogEntryId"));
    }


    /**
     *  Gets the LogEntry element Id.
     *
     *  @return the LogEntry element Id
     */

    public static org.osid.id.Id getLogEntry() {
        return (makeQueryElementId("osid.logging.log.LogEntry"));
    }


    /**
     *  Gets the AncestorLogId element Id.
     *
     *  @return the AncestorLogId element Id
     */

    public static org.osid.id.Id getAncestorLogId() {
        return (makeQueryElementId("osid.logging.log.AncestorLogId"));
    }


    /**
     *  Gets the AncestorLog element Id.
     *
     *  @return the AncestorLog element Id
     */

    public static org.osid.id.Id getAncestorLog() {
        return (makeQueryElementId("osid.logging.log.AncestorLog"));
    }


    /**
     *  Gets the DescendantLogId element Id.
     *
     *  @return the DescendantLogId element Id
     */

    public static org.osid.id.Id getDescendantLogId() {
        return (makeQueryElementId("osid.logging.log.DescendantLogId"));
    }


    /**
     *  Gets the DescendantLog element Id.
     *
     *  @return the DescendantLog element Id
     */

    public static org.osid.id.Id getDescendantLog() {
        return (makeQueryElementId("osid.logging.log.DescendantLog"));
    }
}
