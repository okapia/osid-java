//
// InvariantMapCompositionLookupSession
//
//    Implements a Composition lookup service backed by a fixed collection of
//    compositions.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.repository;


/**
 *  Implements a Composition lookup service backed by a fixed
 *  collection of compositions. The compositions are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapCompositionLookupSession
    extends net.okapia.osid.jamocha.core.repository.spi.AbstractMapCompositionLookupSession
    implements org.osid.repository.CompositionLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapCompositionLookupSession</code> with no
     *  compositions.
     *  
     *  @param repository the repository
     *  @throws org.osid.NullArgumnetException {@code repository} is
     *          {@code null}
     */

    public InvariantMapCompositionLookupSession(org.osid.repository.Repository repository) {
        setRepository(repository);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapCompositionLookupSession</code> with a single
     *  composition.
     *  
     *  @param repository the repository
     *  @param composition a single composition
     *  @throws org.osid.NullArgumentException {@code repository} or
     *          {@code composition} is <code>null</code>
     */

      public InvariantMapCompositionLookupSession(org.osid.repository.Repository repository,
                                               org.osid.repository.Composition composition) {
        this(repository);
        putComposition(composition);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapCompositionLookupSession</code> using an array
     *  of compositions.
     *  
     *  @param repository the repository
     *  @param compositions an array of compositions
     *  @throws org.osid.NullArgumentException {@code repository} or
     *          {@code compositions} is <code>null</code>
     */

      public InvariantMapCompositionLookupSession(org.osid.repository.Repository repository,
                                               org.osid.repository.Composition[] compositions) {
        this(repository);
        putCompositions(compositions);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapCompositionLookupSession</code> using a
     *  collection of compositions.
     *
     *  @param repository the repository
     *  @param compositions a collection of compositions
     *  @throws org.osid.NullArgumentException {@code repository} or
     *          {@code compositions} is <code>null</code>
     */

      public InvariantMapCompositionLookupSession(org.osid.repository.Repository repository,
                                               java.util.Collection<? extends org.osid.repository.Composition> compositions) {
        this(repository);
        putCompositions(compositions);
        return;
    }
}
