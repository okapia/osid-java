//
// AbstractAssemblySpeedZoneEnablerQuery.java
//
//     A SpeedZoneEnablerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.mapping.path.rules.speedzoneenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A SpeedZoneEnablerQuery that stores terms.
 */

public abstract class AbstractAssemblySpeedZoneEnablerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidEnablerQuery
    implements org.osid.mapping.path.rules.SpeedZoneEnablerQuery,
               org.osid.mapping.path.rules.SpeedZoneEnablerQueryInspector,
               org.osid.mapping.path.rules.SpeedZoneEnablerSearchOrder {

    private final java.util.Collection<org.osid.mapping.path.rules.records.SpeedZoneEnablerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.mapping.path.rules.records.SpeedZoneEnablerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.mapping.path.rules.records.SpeedZoneEnablerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblySpeedZoneEnablerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblySpeedZoneEnablerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches enablers mapped to the speed zone. 
     *
     *  @param  speedZoneId the speed zone <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> speedZoneId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledSpeedZoneId(org.osid.id.Id speedZoneId, 
                                      boolean match) {
        getAssembler().addIdTerm(getRuledSpeedZoneIdColumn(), speedZoneId, match);
        return;
    }


    /**
     *  Clears the speed zone <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledSpeedZoneIdTerms() {
        getAssembler().clearTerms(getRuledSpeedZoneIdColumn());
        return;
    }


    /**
     *  Gets the speed zone <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledSpeedZoneIdTerms() {
        return (getAssembler().getIdTerms(getRuledSpeedZoneIdColumn()));
    }


    /**
     *  Gets the RuledSpeedZoneId column name.
     *
     * @return the column name
     */

    protected String getRuledSpeedZoneIdColumn() {
        return ("ruled_speed_zone_id");
    }


    /**
     *  Tests if a <code> SpeedZoneQuery </code> is available. 
     *
     *  @return <code> true </code> if a speed zone query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledSpeedZoneQuery() {
        return (false);
    }


    /**
     *  Gets the query for a speed zone. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the speed zone query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledSpeedZoneQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneQuery getRuledSpeedZoneQuery() {
        throw new org.osid.UnimplementedException("supportsRuledSpeedZoneQuery() is false");
    }


    /**
     *  Matches enablers mapped to any speed zone. 
     *
     *  @param  match <code> true </code> for enablers mapped to any speed 
     *          zone, <code> false </code> to match enablers mapped to no 
     *          speed zones 
     */

    @OSID @Override
    public void matchAnyRuledSpeedZone(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledSpeedZoneColumn(), match);
        return;
    }


    /**
     *  Clears the speed zone query terms. 
     */

    @OSID @Override
    public void clearRuledSpeedZoneTerms() {
        getAssembler().clearTerms(getRuledSpeedZoneColumn());
        return;
    }


    /**
     *  Gets the speed zone query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneQueryInspector[] getRuledSpeedZoneTerms() {
        return (new org.osid.mapping.path.SpeedZoneQueryInspector[0]);
    }


    /**
     *  Gets the RuledSpeedZone column name.
     *
     * @return the column name
     */

    protected String getRuledSpeedZoneColumn() {
        return ("ruled_speed_zone");
    }


    /**
     *  Matches enablers mapped to the map. 
     *
     *  @param  mapId the map <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchMapId(org.osid.id.Id mapId, boolean match) {
        getAssembler().addIdTerm(getMapIdColumn(), mapId, match);
        return;
    }


    /**
     *  Clears the map <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearMapIdTerms() {
        getAssembler().clearTerms(getMapIdColumn());
        return;
    }


    /**
     *  Gets the map <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getMapIdTerms() {
        return (getAssembler().getIdTerms(getMapIdColumn()));
    }


    /**
     *  Gets the MapId column name.
     *
     * @return the column name
     */

    protected String getMapIdColumn() {
        return ("map_id");
    }


    /**
     *  Tests if an <code> MapQuery </code> is available. 
     *
     *  @return <code> true </code> if an map query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMapQuery() {
        return (false);
    }


    /**
     *  Gets the query for an map. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the map query 
     *  @throws org.osid.UnimplementedException <code> supportsMapQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.MapQuery getMapQuery() {
        throw new org.osid.UnimplementedException("supportsMapQuery() is false");
    }


    /**
     *  Clears the map query terms. 
     */

    @OSID @Override
    public void clearMapTerms() {
        getAssembler().clearTerms(getMapColumn());
        return;
    }


    /**
     *  Gets the map query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.MapQueryInspector[] getMapTerms() {
        return (new org.osid.mapping.MapQueryInspector[0]);
    }


    /**
     *  Gets the Map column name.
     *
     * @return the column name
     */

    protected String getMapColumn() {
        return ("map");
    }


    /**
     *  Tests if this speedZoneEnabler supports the given record
     *  <code>Type</code>.
     *
     *  @param  speedZoneEnablerRecordType a speed zone enabler record type 
     *  @return <code>true</code> if the speedZoneEnablerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>speedZoneEnablerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type speedZoneEnablerRecordType) {
        for (org.osid.mapping.path.rules.records.SpeedZoneEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(speedZoneEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  speedZoneEnablerRecordType the speed zone enabler record type 
     *  @return the speed zone enabler query record 
     *  @throws org.osid.NullArgumentException
     *          <code>speedZoneEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(speedZoneEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.path.rules.records.SpeedZoneEnablerQueryRecord getSpeedZoneEnablerQueryRecord(org.osid.type.Type speedZoneEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.path.rules.records.SpeedZoneEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(speedZoneEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(speedZoneEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  speedZoneEnablerRecordType the speed zone enabler record type 
     *  @return the speed zone enabler query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>speedZoneEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(speedZoneEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.path.rules.records.SpeedZoneEnablerQueryInspectorRecord getSpeedZoneEnablerQueryInspectorRecord(org.osid.type.Type speedZoneEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.path.rules.records.SpeedZoneEnablerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(speedZoneEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(speedZoneEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param speedZoneEnablerRecordType the speed zone enabler record type
     *  @return the speed zone enabler search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>speedZoneEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(speedZoneEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.path.rules.records.SpeedZoneEnablerSearchOrderRecord getSpeedZoneEnablerSearchOrderRecord(org.osid.type.Type speedZoneEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.path.rules.records.SpeedZoneEnablerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(speedZoneEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(speedZoneEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this speed zone enabler. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param speedZoneEnablerQueryRecord the speed zone enabler query record
     *  @param speedZoneEnablerQueryInspectorRecord the speed zone enabler query inspector
     *         record
     *  @param speedZoneEnablerSearchOrderRecord the speed zone enabler search order record
     *  @param speedZoneEnablerRecordType speed zone enabler record type
     *  @throws org.osid.NullArgumentException
     *          <code>speedZoneEnablerQueryRecord</code>,
     *          <code>speedZoneEnablerQueryInspectorRecord</code>,
     *          <code>speedZoneEnablerSearchOrderRecord</code> or
     *          <code>speedZoneEnablerRecordTypespeedZoneEnabler</code> is
     *          <code>null</code>
     */
            
    protected void addSpeedZoneEnablerRecords(org.osid.mapping.path.rules.records.SpeedZoneEnablerQueryRecord speedZoneEnablerQueryRecord, 
                                      org.osid.mapping.path.rules.records.SpeedZoneEnablerQueryInspectorRecord speedZoneEnablerQueryInspectorRecord, 
                                      org.osid.mapping.path.rules.records.SpeedZoneEnablerSearchOrderRecord speedZoneEnablerSearchOrderRecord, 
                                      org.osid.type.Type speedZoneEnablerRecordType) {

        addRecordType(speedZoneEnablerRecordType);

        nullarg(speedZoneEnablerQueryRecord, "speed zone enabler query record");
        nullarg(speedZoneEnablerQueryInspectorRecord, "speed zone enabler query inspector record");
        nullarg(speedZoneEnablerSearchOrderRecord, "speed zone enabler search odrer record");

        this.queryRecords.add(speedZoneEnablerQueryRecord);
        this.queryInspectorRecords.add(speedZoneEnablerQueryInspectorRecord);
        this.searchOrderRecords.add(speedZoneEnablerSearchOrderRecord);
        
        return;
    }
}
