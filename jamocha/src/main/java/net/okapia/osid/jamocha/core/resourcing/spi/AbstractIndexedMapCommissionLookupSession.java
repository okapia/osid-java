//
// AbstractIndexedMapCommissionLookupSession.java
//
//    A simple framework for providing a Commission lookup service
//    backed by a fixed collection of commissions with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Commission lookup service backed by a
 *  fixed collection of commissions. The commissions are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some commissions may be compatible
 *  with more types than are indicated through these commission
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Commissions</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapCommissionLookupSession
    extends AbstractMapCommissionLookupSession
    implements org.osid.resourcing.CommissionLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.resourcing.Commission> commissionsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.resourcing.Commission>());
    private final MultiMap<org.osid.type.Type, org.osid.resourcing.Commission> commissionsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.resourcing.Commission>());


    /**
     *  Makes a <code>Commission</code> available in this session.
     *
     *  @param  commission a commission
     *  @throws org.osid.NullArgumentException <code>commission<code> is
     *          <code>null</code>
     */

    @Override
    protected void putCommission(org.osid.resourcing.Commission commission) {
        super.putCommission(commission);

        this.commissionsByGenus.put(commission.getGenusType(), commission);
        
        try (org.osid.type.TypeList types = commission.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.commissionsByRecord.put(types.getNextType(), commission);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a commission from this session.
     *
     *  @param commissionId the <code>Id</code> of the commission
     *  @throws org.osid.NullArgumentException <code>commissionId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeCommission(org.osid.id.Id commissionId) {
        org.osid.resourcing.Commission commission;
        try {
            commission = getCommission(commissionId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.commissionsByGenus.remove(commission.getGenusType());

        try (org.osid.type.TypeList types = commission.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.commissionsByRecord.remove(types.getNextType(), commission);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeCommission(commissionId);
        return;
    }


    /**
     *  Gets a <code>CommissionList</code> corresponding to the given
     *  commission genus <code>Type</code> which does not include
     *  commissions of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known commissions or an error results. Otherwise,
     *  the returned list may contain only those commissions that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  commissionGenusType a commission genus type 
     *  @return the returned <code>Commission</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>commissionGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionList getCommissionsByGenusType(org.osid.type.Type commissionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.resourcing.commission.ArrayCommissionList(this.commissionsByGenus.get(commissionGenusType)));
    }


    /**
     *  Gets a <code>CommissionList</code> containing the given
     *  commission record <code>Type</code>. In plenary mode, the
     *  returned list contains all known commissions or an error
     *  results. Otherwise, the returned list may contain only those
     *  commissions that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  commissionRecordType a commission record type 
     *  @return the returned <code>commission</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>commissionRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionList getCommissionsByRecordType(org.osid.type.Type commissionRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.resourcing.commission.ArrayCommissionList(this.commissionsByRecord.get(commissionRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.commissionsByGenus.clear();
        this.commissionsByRecord.clear();

        super.close();

        return;
    }
}
