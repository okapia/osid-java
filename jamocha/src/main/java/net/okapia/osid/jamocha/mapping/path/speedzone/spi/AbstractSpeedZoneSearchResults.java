//
// AbstractSpeedZoneSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.path.speedzone.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractSpeedZoneSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.mapping.path.SpeedZoneSearchResults {

    private org.osid.mapping.path.SpeedZoneList speedZones;
    private final org.osid.mapping.path.SpeedZoneQueryInspector inspector;
    private final java.util.Collection<org.osid.mapping.path.records.SpeedZoneSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractSpeedZoneSearchResults.
     *
     *  @param speedZones the result set
     *  @param speedZoneQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>speedZones</code>
     *          or <code>speedZoneQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractSpeedZoneSearchResults(org.osid.mapping.path.SpeedZoneList speedZones,
                                            org.osid.mapping.path.SpeedZoneQueryInspector speedZoneQueryInspector) {
        nullarg(speedZones, "speed zones");
        nullarg(speedZoneQueryInspector, "speed zone query inspectpr");

        this.speedZones = speedZones;
        this.inspector = speedZoneQueryInspector;

        return;
    }


    /**
     *  Gets the speed zone list resulting from a search.
     *
     *  @return a speed zone list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneList getSpeedZones() {
        if (this.speedZones == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.mapping.path.SpeedZoneList speedZones = this.speedZones;
        this.speedZones = null;
	return (speedZones);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.mapping.path.SpeedZoneQueryInspector getSpeedZoneQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  speed zone search record <code> Type. </code> This method must
     *  be used to retrieve a speedZone implementing the requested
     *  record.
     *
     *  @param speedZoneSearchRecordType a speedZone search 
     *         record type 
     *  @return the speed zone search
     *  @throws org.osid.NullArgumentException
     *          <code>speedZoneSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(speedZoneSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.path.records.SpeedZoneSearchResultsRecord getSpeedZoneSearchResultsRecord(org.osid.type.Type speedZoneSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.mapping.path.records.SpeedZoneSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(speedZoneSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(speedZoneSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record speed zone search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addSpeedZoneRecord(org.osid.mapping.path.records.SpeedZoneSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "speed zone record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
