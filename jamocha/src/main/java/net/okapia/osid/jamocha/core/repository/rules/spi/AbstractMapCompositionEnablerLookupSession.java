//
// AbstractMapCompositionEnablerLookupSession
//
//    A simple framework for providing a CompositionEnabler lookup service
//    backed by a fixed collection of composition enablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.repository.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a CompositionEnabler lookup service backed by a
 *  fixed collection of composition enablers. The composition enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>CompositionEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapCompositionEnablerLookupSession
    extends net.okapia.osid.jamocha.repository.rules.spi.AbstractCompositionEnablerLookupSession
    implements org.osid.repository.rules.CompositionEnablerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.repository.rules.CompositionEnabler> compositionEnablers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.repository.rules.CompositionEnabler>());


    /**
     *  Makes a <code>CompositionEnabler</code> available in this session.
     *
     *  @param  compositionEnabler a composition enabler
     *  @throws org.osid.NullArgumentException <code>compositionEnabler<code>
     *          is <code>null</code>
     */

    protected void putCompositionEnabler(org.osid.repository.rules.CompositionEnabler compositionEnabler) {
        this.compositionEnablers.put(compositionEnabler.getId(), compositionEnabler);
        return;
    }


    /**
     *  Makes an array of composition enablers available in this session.
     *
     *  @param  compositionEnablers an array of composition enablers
     *  @throws org.osid.NullArgumentException <code>compositionEnablers<code>
     *          is <code>null</code>
     */

    protected void putCompositionEnablers(org.osid.repository.rules.CompositionEnabler[] compositionEnablers) {
        putCompositionEnablers(java.util.Arrays.asList(compositionEnablers));
        return;
    }


    /**
     *  Makes a collection of composition enablers available in this session.
     *
     *  @param  compositionEnablers a collection of composition enablers
     *  @throws org.osid.NullArgumentException <code>compositionEnablers<code>
     *          is <code>null</code>
     */

    protected void putCompositionEnablers(java.util.Collection<? extends org.osid.repository.rules.CompositionEnabler> compositionEnablers) {
        for (org.osid.repository.rules.CompositionEnabler compositionEnabler : compositionEnablers) {
            this.compositionEnablers.put(compositionEnabler.getId(), compositionEnabler);
        }

        return;
    }


    /**
     *  Removes a CompositionEnabler from this session.
     *
     *  @param  compositionEnablerId the <code>Id</code> of the composition enabler
     *  @throws org.osid.NullArgumentException <code>compositionEnablerId<code> is
     *          <code>null</code>
     */

    protected void removeCompositionEnabler(org.osid.id.Id compositionEnablerId) {
        this.compositionEnablers.remove(compositionEnablerId);
        return;
    }


    /**
     *  Gets the <code>CompositionEnabler</code> specified by its <code>Id</code>.
     *
     *  @param  compositionEnablerId <code>Id</code> of the <code>CompositionEnabler</code>
     *  @return the compositionEnabler
     *  @throws org.osid.NotFoundException <code>compositionEnablerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>compositionEnablerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnabler getCompositionEnabler(org.osid.id.Id compositionEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.repository.rules.CompositionEnabler compositionEnabler = this.compositionEnablers.get(compositionEnablerId);
        if (compositionEnabler == null) {
            throw new org.osid.NotFoundException("compositionEnabler not found: " + compositionEnablerId);
        }

        return (compositionEnabler);
    }


    /**
     *  Gets all <code>CompositionEnablers</code>. In plenary mode, the returned
     *  list contains all known compositionEnablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  compositionEnablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>CompositionEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerList getCompositionEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.repository.rules.compositionenabler.ArrayCompositionEnablerList(this.compositionEnablers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.compositionEnablers.clear();
        super.close();
        return;
    }
}
