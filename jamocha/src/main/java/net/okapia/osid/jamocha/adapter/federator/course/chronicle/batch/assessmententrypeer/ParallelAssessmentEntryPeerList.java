//
// ParallelAssessmentEntryPeerList.java
//
//     Implements an AssessmentEntryPeerList. This list reads from 
//     multiple lists.
//
//
// Tom Coppeto
// Okapia
// 17 September 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.course.chronicle.batch.assessmententrypeer;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements an <code>AssessmentEntryPeerList</code>. This list allows
 *  AssessmentEntryPeerLists to be added a bit at a time and are read off in
 *  parallel. <code>noMore</code> must be invoked when all the lists
 *  have been added.
 *
 *  Lists that are added to added to this list are automatically
 *  wrapped in a <code>BufferedAssessmentEntryPeerList</code>.
 *
 *  Two error handlers are offered. A hard error throws an exception
 *  for any error received by an underlying list. A soft error ignores
 *  errors in underlying lists and proceeds to the next list.
 */

public class ParallelAssessmentEntryPeerList
    extends net.okapia.osid.jamocha.course.chronicle.batch.assessmententrypeer.spi.AbstractAssessmentEntryPeerList
    implements org.osid.course.chronicle.batch.AssessmentEntryPeerList,
               net.okapia.osid.jamocha.adapter.federator.course.chronicle.batch.assessmententrypeer.FederatingAssessmentEntryPeerList,
               net.okapia.osid.jamocha.inline.course.chronicle.batch.assessmententrypeer.AssessmentEntryPeerListCallback {

    private final java.util.Collection<AssessmentEntryPeerListReader> readers = java.util.Collections.synchronizedList(new java.util.LinkedList<AssessmentEntryPeerListReader>());
    private int bufferSize = 10;
    private boolean soft = false;
    private boolean eol = false;


    /**
     *  Creates a new empty <code>ParallelAssessmentEntryPeerList</code>. The
     *  default buffer size is 10 elemets per list and underlying
     *  errors fail hard.
     */

    public ParallelAssessmentEntryPeerList() {
        return;
    }


    /**
     *  Creates a new empty <code>ParallelAssessmentEntryPeerList</code>.
     *
     *  @param bufferSize the number of elements to buffer for each
     *         list
     *  @param soft <code>true</code> to fail soft, <code>false</code>
     *         to fail hard
     */

    public ParallelAssessmentEntryPeerList(int bufferSize, boolean soft) {
        this.bufferSize = bufferSize;
        this.soft = soft;
        return;
    }


    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in this 
     *          list, <code> false </code> if the end of the list has been 
     *          reached 
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        while (true) {
            if (this.eol && (this.readers.size() == 0)) {
                return (false);
            }
            
            for (AssessmentEntryPeerListReader reader : this.readers) {
                if (!reader.isFinished()) {
                    return (true);
                }
            }

            synchronized(this.readers) {
                try {
                    this.readers.wait(100);
                } catch (InterruptedException ie) { }
            }
        }
    }


    /**
     *  Gets the number of elements available for retrieval. The
     *  number returned by this method may be less than or equal to
     *  the total number of elements in this list. To determine if the
     *  end of the list has been reached, the method <code> hasNext()
     *  </code> should be used. This method conveys what is known
     *  about the number of remaining elements at a point in time and
     *  can be used to determine a minimum size of the remaining
     *  elements, if known. A valid return is zero even if <code>
     *  hasNext() </code> is true.
     *  
     *  This method does not imply asynchronous usage. All OSID
     *  methods may block.
     *
     *  @return the number of elements available for retrieval 
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public long available() {
        int total = 0;
        for (AssessmentEntryPeerListReader reader : this.readers) {
            total += reader.available();
        }

        return (total);
    }


    /**
     *  Skip the specified number of elements in the list. If the
     *  number skipped is greater than the number of elements in the
     *  list, hasNext() becomes false and available() returns zero as
     *  there are no more elements to retrieve.
     *  
     *  This method makes no sense in a randomly parallel iterator.
     *
     *  @param n the number of elements to skip
     *  @throws org.osid.InvalidArgumentException <code>n</code> is
     *          less than zero
     */

    @OSID @Override
    public void skip(long n) {
        long skipped = 0;
        
        while (skipped < n) {
            if (this.eol && (this.readers.size() == 0)) {
                return;
            }

            for (AssessmentEntryPeerListReader reader : this.readers) {
                if (!reader.isFinished()) {
                    if (n > skipped) {
                        long s = reader.available();
                        if ((s + skipped) > n) {
                            s = n - skipped;
                        }
                        
                        reader.skip(s);
                        skipped += s;
                    }
                } 
            }

            synchronized (this.readers) {
                try {
                    this.readers.wait(100);
                } catch (InterruptedException ie) { }
            }
        }

        return;
    }


    /**
     *  Gets the next <code> AssessmentEntryPeer </code> in this list. 
     *
     *  @return the next <code> AssessmentEntryPeer </code> in this list. The
     *          <code> hasNext() </code> method should be used to test
     *          that a next <code> AssessmentEntryPeer </code> is available
     *          before calling this method.
     *  @throws org.osid.IllegalStateException no more elements
     *          available in this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     */

    @OSID @Override
    public org.osid.course.chronicle.batch.AssessmentEntryPeer getNextAssessmentEntryPeer()
        throws org.osid.OperationFailedException {

        while (true) {
            if (this.eol && (this.readers.size() == 0)) {
                throw new org.osid.IllegalStateException("no more elements in list");
            }

            for (AssessmentEntryPeerListReader reader : this.readers) {
                try {
                    if (reader.ready()) {
                        return (reader.getNextAssessmentEntryPeer());
                    } 
                } catch (org.osid.OperationFailedException ofe) {
                    if (!reader.getSoftError()) {
                        throw ofe;
                    }
                    
                    this.readers.remove(reader);
                }
            }
                
            synchronized (this.readers) {
                try {
                    this.readers.wait(100);
                } catch (InterruptedException ie) { }
            }
        }
    }

        
    /**
     *  Gets the next set of <code> AssessmentEntryPeer </code> elements in this
     *  list. The specified amount must be less than or equal to the
     *  return from <code> available(). </code>
     *
     *  @param n the number of <code> AssessmentEntryPeer </code> elements
     *          requested which must be less than or equal to <code>
     *          available() </code>
     *  @return an array of <code> AssessmentEntryPeer </code> elements. <code>
     *          </code> The length of the array is less than or equal
     *          to the number specified.
     *  @throws org.osid.IllegalStateException no more elements
     *          available in this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.InvalidArgumentException <code>n</code> is
     *          less than zero
     */

    @OSID @Override
    public org.osid.course.chronicle.batch.AssessmentEntryPeer[] getNextAssessmentEntryPeers(long n)
        throws org.osid.IllegalStateException,
               org.osid.OperationFailedException {
        
        java.util.Collection<org.osid.course.chronicle.batch.AssessmentEntryPeer> ret = new java.util.ArrayList<>((int) n);

        for (AssessmentEntryPeerListReader reader : this.readers) {
            try {
                for (org.osid.course.chronicle.batch.AssessmentEntryPeer assessmentEntryPeer : reader.getNextAssessmentEntryPeers(reader.available())) {
                    ret.add(assessmentEntryPeer);
                }
            } catch (org.osid.OperationFailedException ofe) {
                if (!reader.getSoftError()) {
                    throw ofe;
                }
                
                this.readers.remove(reader);
            }
        }

        return (ret.toArray(new org.osid.course.chronicle.batch.AssessmentEntryPeer[ret.size()]));      
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        for (AssessmentEntryPeerListReader reader : this.readers) {
            reader.close();
        }

        this.readers.clear();
        this.eol     = true;
        return;
    }


    /**
     *  Adds an <code>AssessmentEntryPeerList</code> to this
     *  <code>AssessmentEntryPeerList</code> using the default buffer size.
     *
     *  @param assessmentEntryPeerList the <code>AssessmentEntryPeerList</code> to add
     *  @throws org.osid.IllegalStateException this list already closed
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentEntryPeerList</code> is <code>null</code>
     */

    @Override
    public void addAssessmentEntryPeerList(org.osid.course.chronicle.batch.AssessmentEntryPeerList assessmentEntryPeerList) {
        addAssessmentEntryPeerList(assessmentEntryPeerList, this.bufferSize, this.soft);
        return;
    }


    /**
     *  Adds a collection of <code>AssessmentEntryPeerLists</code> to this
     *  <code>AssessmentEntryPeerList</code>.
     *
     *  @param assessmentEntryPeerLists the <code>AssessmentEntryPeerList</code> collection
     *         to add
     *  @throws org.osid.IllegalStateException this list already closed
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentEntryPeerLists</code> is <code>null</code>
     */

    @Override
    public void addAssessmentEntryPeerLists(java.util.Collection<org.osid.course.chronicle.batch.AssessmentEntryPeerList> assessmentEntryPeerLists) {
        nullarg(assessmentEntryPeerLists, "assessmententrypeer lists");

        for (org.osid.course.chronicle.batch.AssessmentEntryPeerList list : assessmentEntryPeerLists) {
            addAssessmentEntryPeerList(list);
        }

        return;
    }

        
    /**
     *  Adds an <code>AssessmentEntryPeerList</code> to this
     *  <code>AssessmentEntryPeerList</code>.
     *
     *  @param assessmentEntryPeerList the <code>AssessmentEntryPeerList</code> to add
     *  @param bufferSize a buffer size for this list
     *  @param soft <code>true</code> to fail soft, <code>false</code>
     *         to fail hard
     *  @throws org.osid.IllegalStateException this list already closed
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentEntryPeerList</code> is <code>null</code>
     */

    public void addAssessmentEntryPeerList(org.osid.course.chronicle.batch.AssessmentEntryPeerList assessmentEntryPeerList, int bufferSize, boolean soft) {
        nullarg(assessmentEntryPeerList, "assessmententrypeer list");

        net.okapia.osid.jamocha.inline.course.chronicle.batch.assessmententrypeer.BufferedAssessmentEntryPeerList bpl = new net.okapia.osid.jamocha.inline.course.chronicle.batch.assessmententrypeer.BufferedAssessmentEntryPeerList(assessmentEntryPeerList, bufferSize, this);
        AssessmentEntryPeerListReader reader = new AssessmentEntryPeerListReader(bpl, soft);
        this.readers.add(reader);

        synchronized (this.readers) {
            this.readers.notifyAll();
        }

        return;
    }


    /**
     *  No more. Invoked when no more lists are to be added.
     */

    @Override
    public void noMore() {
        this.eol = true;
        synchronized (this.readers) {
            this.readers.notifyAll();
        }

        return;
    }


    /**
     *  Tests if the list is operational. A list is operational if
     *  <code>close()</code> has not been invoked.
     *
     *  @return <code>true</code> if the list is operational,
     *          <code>false</code> otherwise.
     */

    @Override
    public boolean isOperational() {
        if ((this.readers.size() == 0) && this.eol) {
            return (false);
        }

        return (true);
    }


    /**
     *  Tests if the list is empty.
     *
     *  @return <code>true</code> if the list is empty
     *          retrieved, <code>false</code> otherwise.
     */

    @Override
    public boolean isEmpty() {
        if ((this.readers.size() == 0) && this.eol) {
            return (true);
        } else if (this.readers.size() > 0) {
            return (false);
        }

        return (true);
    }


    @Override
    public void newAssessmentEntryPeerInList(org.osid.course.chronicle.batch.AssessmentEntryPeerList list) {
        synchronized (this.readers) {
            this.readers.notifyAll();
        }

        return;
    }


    @Override
    public void errorInList(org.osid.course.chronicle.batch.AssessmentEntryPeerList list) {
        this.readers.remove(list);

        synchronized (this.readers) {
            this.readers.notifyAll();
        }

        return;
    }


    @Override
    public void doneForList(org.osid.course.chronicle.batch.AssessmentEntryPeerList list) {
        this.readers.remove(list);

        synchronized (this.readers) {
            this.readers.notifyAll();
        }

        return;
    }


    class AssessmentEntryPeerListReader {
        private net.okapia.osid.jamocha.inline.course.chronicle.batch.assessmententrypeer.BufferedAssessmentEntryPeerList list;
        private boolean soft = false;


        AssessmentEntryPeerListReader(net.okapia.osid.jamocha.inline.course.chronicle.batch.assessmententrypeer.BufferedAssessmentEntryPeerList list, boolean soft) {
            this.list = list;
            this.soft = soft;
            this.list.run();
            return;
        }


        public net.okapia.osid.jamocha.inline.course.chronicle.batch.assessmententrypeer.BufferedAssessmentEntryPeerList getList() {
            return (this.list);
        }


        public boolean getSoftError() {
            return (this.soft);
        }


        public boolean ready() {            
            if (this.list.available() > 0) {
                return (true);
            } else {
                return (false);
            }
        }

        
        public boolean isFinished() {
            if (this.list.isEmpty() && !this.list.isRunning()) {
                return (true);
            } else {
                return (false);
            }
        }


        public long available() {
            return (this.list.available());
        }


        public void skip(long n) {
            this.list.skip(n);
            return;
        }


        public org.osid.course.chronicle.batch.AssessmentEntryPeer getNextAssessmentEntryPeer()
            throws org.osid.OperationFailedException {

            if (this.list.hasNext()) {
                return (this.list.getNextAssessmentEntryPeer());           
            } else {
                try {
                    this.list.close();
                } catch (Exception e) {}
            }

            throw new org.osid.OperationFailedException("no more elements in buffered list");
        }


        public org.osid.course.chronicle.batch.AssessmentEntryPeer[] getNextAssessmentEntryPeers(long n)
            throws org.osid.OperationFailedException {

            if (n > this.list.available()) {
                n = this.list.available();
            }

            return (this.list.getNextAssessmentEntryPeers(n));
        }


        public void close() {
            try {
                this.list.close();
            } catch (Exception e) {}

            return;
        }
    }       
}
