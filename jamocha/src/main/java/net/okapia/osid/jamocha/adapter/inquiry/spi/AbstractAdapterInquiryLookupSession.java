//
// AbstractAdapterInquiryLookupSession.java
//
//    An Inquiry lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.inquiry.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An Inquiry lookup session adapter.
 */

public abstract class AbstractAdapterInquiryLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.inquiry.InquiryLookupSession {

    private final org.osid.inquiry.InquiryLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterInquiryLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterInquiryLookupSession(org.osid.inquiry.InquiryLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Inquest/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Inquest Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getInquestId() {
        return (this.session.getInquestId());
    }


    /**
     *  Gets the {@code Inquest} associated with this session.
     *
     *  @return the {@code Inquest} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.Inquest getInquest()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getInquest());
    }


    /**
     *  Tests if this user can perform {@code Inquiry} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupInquiries() {
        return (this.session.canLookupInquiries());
    }


    /**
     *  A complete view of the {@code Inquiry} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeInquiryView() {
        this.session.useComparativeInquiryView();
        return;
    }


    /**
     *  A complete view of the {@code Inquiry} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryInquiryView() {
        this.session.usePlenaryInquiryView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include inquiries in inquests which are children
     *  of this inquest in the inquest hierarchy.
     */

    @OSID @Override
    public void useFederatedInquestView() {
        this.session.useFederatedInquestView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this inquest only.
     */

    @OSID @Override
    public void useIsolatedInquestView() {
        this.session.useIsolatedInquestView();
        return;
    }
    

    /**
     *  Only active inquiries are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveInquiryView() {
        this.session.useActiveInquiryView();
        return;
    }


    /**
     *  Active and inactive inquiries are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusInquiryView() {
        this.session.useAnyStatusInquiryView();
        return;
    }
    
     
    /**
     *  Gets the {@code Inquiry} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Inquiry} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Inquiry} and
     *  retained for compatibility.
     *
     *  In active mode, inquiries are returned that are currently
     *  active. In any status mode, active and inactive inquiries
     *  are returned.
     *
     *  @param inquiryId {@code Id} of the {@code Inquiry}
     *  @return the inquiry
     *  @throws org.osid.NotFoundException {@code inquiryId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code inquiryId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.Inquiry getInquiry(org.osid.id.Id inquiryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getInquiry(inquiryId));
    }


    /**
     *  Gets an {@code InquiryList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  inquiries specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Inquiries} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, inquiries are returned that are currently
     *  active. In any status mode, active and inactive inquiries
     *  are returned.
     *
     *  @param  inquiryIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Inquiry} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code inquiryIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryList getInquiriesByIds(org.osid.id.IdList inquiryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getInquiriesByIds(inquiryIds));
    }


    /**
     *  Gets an {@code InquiryList} corresponding to the given
     *  inquiry genus {@code Type} which does not include
     *  inquiries of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  inquiries or an error results. Otherwise, the returned list
     *  may contain only those inquiries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, inquiries are returned that are currently
     *  active. In any status mode, active and inactive inquiries
     *  are returned.
     *
     *  @param  inquiryGenusType an inquiry genus type 
     *  @return the returned {@code Inquiry} list
     *  @throws org.osid.NullArgumentException
     *          {@code inquiryGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryList getInquiriesByGenusType(org.osid.type.Type inquiryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getInquiriesByGenusType(inquiryGenusType));
    }


    /**
     *  Gets an {@code InquiryList} corresponding to the given
     *  inquiry genus {@code Type} and include any additional
     *  inquiries with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  inquiries or an error results. Otherwise, the returned list
     *  may contain only those inquiries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, inquiries are returned that are currently
     *  active. In any status mode, active and inactive inquiries
     *  are returned.
     *
     *  @param  inquiryGenusType an inquiry genus type 
     *  @return the returned {@code Inquiry} list
     *  @throws org.osid.NullArgumentException
     *          {@code inquiryGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryList getInquiriesByParentGenusType(org.osid.type.Type inquiryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getInquiriesByParentGenusType(inquiryGenusType));
    }


    /**
     *  Gets an {@code InquiryList} containing the given
     *  inquiry record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  inquiries or an error results. Otherwise, the returned list
     *  may contain only those inquiries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, inquiries are returned that are currently
     *  active. In any status mode, active and inactive inquiries
     *  are returned.
     *
     *  @param  inquiryRecordType an inquiry record type 
     *  @return the returned {@code Inquiry} list
     *  @throws org.osid.NullArgumentException
     *          {@code inquiryRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryList getInquiriesByRecordType(org.osid.type.Type inquiryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getInquiriesByRecordType(inquiryRecordType));
    }


    /**
     *  Gets a list of inquiries for a supplied audit. 
     *  
     *  In plenary mode, the returned list contains all known
     *  inquiries or an error results. Otherwise, the returned list
     *  may contain only those inquiries that are accessible through
     *  this session.
     *  
     *  In active mode, inquiries are returned that are currently
     *  active. In any status mode, both active and inactive inquiries
     *  are returned.
     *
     *  @param  auditId an audit {@code Id} 
     *  @return the returned {@code Inquiry} list 
     *  @throws org.osid.NullArgumentException {@code auditId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryList getInquiriesForAudit(org.osid.id.Id auditId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getInquiriesForAudit(auditId));
    }


    /**
     *  Gets all {@code Inquiries}. 
     *
     *  In plenary mode, the returned list contains all known
     *  inquiries or an error results. Otherwise, the returned list
     *  may contain only those inquiries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, inquiries are returned that are currently
     *  active. In any status mode, active and inactive inquiries
     *  are returned.
     *
     *  @return a list of {@code Inquiries} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryList getInquiries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getInquiries());
    }
}
