//
// AbstractObjectiveSearchOdrer.java
//
//     Defines an ObjectiveSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.learning.objective.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines an {@code ObjectiveSearchOrder}.
 */

public abstract class AbstractObjectiveSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectSearchOrder
    implements org.osid.learning.ObjectiveSearchOrder {

    private final java.util.Collection<org.osid.learning.records.ObjectiveSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specified a preference for ordering results by the assessment. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAssessment(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an <code> AssessmentSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if an assessment search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for an assessment. 
     *
     *  @return the assessment search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentSearchOrder getAssessmentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsAssessmentSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the knowledge 
     *  category. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByKnowledgeCategory(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a grade search order is available. 
     *
     *  @return <code> true </code> if a grade search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsKnowledgeCategorySearchOrder() {
        return (false);
    }


    /**
     *  Gets a grade search order to order on knolwgedge category. 
     *
     *  @return a grade search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsKnowledgeCategorySearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSearchOrder getKnowledgeCategorySearchOrder() {
        throw new org.osid.UnimplementedException("supportsKnowledgeCategorySearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the cognitive 
     *  process. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCognitiveProcess(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a grade search order is available. 
     *
     *  @return <code> true </code> if a grade search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCognitiveProcessSearchOrder() {
        return (false);
    }


    /**
     *  Gets a grade search order to order on cognitive process. 
     *
     *  @return a grade search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCognitiveProcessSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSearchOrder getCognitiveProcessSearchOrder() {
        throw new org.osid.UnimplementedException("supportsCognitiveProcessSearchOrder() is false");
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  objectiveRecordType an objective record type 
     *  @return {@code true} if the objectiveRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code objectiveRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type objectiveRecordType) {
        for (org.osid.learning.records.ObjectiveSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(objectiveRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  objectiveRecordType the objective record type 
     *  @return the objective search order record
     *  @throws org.osid.NullArgumentException
     *          {@code objectiveRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(objectiveRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.learning.records.ObjectiveSearchOrderRecord getObjectiveSearchOrderRecord(org.osid.type.Type objectiveRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.learning.records.ObjectiveSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(objectiveRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(objectiveRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this objective. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param objectiveRecord the objective search odrer record
     *  @param objectiveRecordType objective record type
     *  @throws org.osid.NullArgumentException
     *          {@code objectiveRecord} or
     *          {@code objectiveRecordTypeobjective} is
     *          {@code null}
     */
            
    protected void addObjectiveRecord(org.osid.learning.records.ObjectiveSearchOrderRecord objectiveSearchOrderRecord, 
                                     org.osid.type.Type objectiveRecordType) {

        addRecordType(objectiveRecordType);
        this.records.add(objectiveSearchOrderRecord);
        
        return;
    }
}
