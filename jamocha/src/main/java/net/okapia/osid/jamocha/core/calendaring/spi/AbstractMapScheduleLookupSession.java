//
// AbstractMapScheduleLookupSession
//
//    A simple framework for providing a Schedule lookup service
//    backed by a fixed collection of schedules.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Schedule lookup service backed by a
 *  fixed collection of schedules. The schedules are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Schedules</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapScheduleLookupSession
    extends net.okapia.osid.jamocha.calendaring.spi.AbstractScheduleLookupSession
    implements org.osid.calendaring.ScheduleLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.calendaring.Schedule> schedules = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.calendaring.Schedule>());


    /**
     *  Makes a <code>Schedule</code> available in this session.
     *
     *  @param  schedule a schedule
     *  @throws org.osid.NullArgumentException <code>schedule<code>
     *          is <code>null</code>
     */

    protected void putSchedule(org.osid.calendaring.Schedule schedule) {
        this.schedules.put(schedule.getId(), schedule);
        return;
    }


    /**
     *  Makes an array of schedules available in this session.
     *
     *  @param  schedules an array of schedules
     *  @throws org.osid.NullArgumentException <code>schedules<code>
     *          is <code>null</code>
     */

    protected void putSchedules(org.osid.calendaring.Schedule[] schedules) {
        putSchedules(java.util.Arrays.asList(schedules));
        return;
    }


    /**
     *  Makes a collection of schedules available in this session.
     *
     *  @param  schedules a collection of schedules
     *  @throws org.osid.NullArgumentException <code>schedules<code>
     *          is <code>null</code>
     */

    protected void putSchedules(java.util.Collection<? extends org.osid.calendaring.Schedule> schedules) {
        for (org.osid.calendaring.Schedule schedule : schedules) {
            this.schedules.put(schedule.getId(), schedule);
        }

        return;
    }


    /**
     *  Removes a Schedule from this session.
     *
     *  @param  scheduleId the <code>Id</code> of the schedule
     *  @throws org.osid.NullArgumentException <code>scheduleId<code> is
     *          <code>null</code>
     */

    protected void removeSchedule(org.osid.id.Id scheduleId) {
        this.schedules.remove(scheduleId);
        return;
    }


    /**
     *  Gets the <code>Schedule</code> specified by its <code>Id</code>.
     *
     *  @param  scheduleId <code>Id</code> of the <code>Schedule</code>
     *  @return the schedule
     *  @throws org.osid.NotFoundException <code>scheduleId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>scheduleId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Schedule getSchedule(org.osid.id.Id scheduleId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.calendaring.Schedule schedule = this.schedules.get(scheduleId);
        if (schedule == null) {
            throw new org.osid.NotFoundException("schedule not found: " + scheduleId);
        }

        return (schedule);
    }


    /**
     *  Gets all <code>Schedules</code>. In plenary mode, the returned
     *  list contains all known schedules or an error
     *  results. Otherwise, the returned list may contain only those
     *  schedules that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Schedules</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleList getSchedules()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.calendaring.schedule.ArrayScheduleList(this.schedules.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.schedules.clear();
        super.close();
        return;
    }
}
