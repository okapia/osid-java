//
// AbstractIndexedMapDeviceLookupSession.java
//
//    A simple framework for providing a Device lookup service
//    backed by a fixed collection of devices with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.control.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Device lookup service backed by a
 *  fixed collection of devices. The devices are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some devices may be compatible
 *  with more types than are indicated through these device
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Devices</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapDeviceLookupSession
    extends AbstractMapDeviceLookupSession
    implements org.osid.control.DeviceLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.control.Device> devicesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.control.Device>());
    private final MultiMap<org.osid.type.Type, org.osid.control.Device> devicesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.control.Device>());


    /**
     *  Makes a <code>Device</code> available in this session.
     *
     *  @param  device a device
     *  @throws org.osid.NullArgumentException <code>device<code> is
     *          <code>null</code>
     */

    @Override
    protected void putDevice(org.osid.control.Device device) {
        super.putDevice(device);

        this.devicesByGenus.put(device.getGenusType(), device);
        
        try (org.osid.type.TypeList types = device.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.devicesByRecord.put(types.getNextType(), device);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }

    
    /**
     *  Makes an array of devices available in this session.
     *
     *  @param  devices an array of devices
     *  @throws org.osid.NullArgumentException <code>devices<code>
     *          is <code>null</code>
     */

    @Override
    protected void putDevices(org.osid.control.Device[] devices) {
        for (org.osid.control.Device device : devices) {
            putDevice(device);
        }

        return;
    }


    /**
     *  Makes a collection of devices available in this session.
     *
     *  @param  devices a collection of devices
     *  @throws org.osid.NullArgumentException <code>devices<code>
     *          is <code>null</code>
     */

    @Override
    protected void putDevices(java.util.Collection<? extends org.osid.control.Device> devices) {
        for (org.osid.control.Device device : devices) {
            putDevice(device);
        }

        return;
    }


    /**
     *  Removes a device from this session.
     *
     *  @param deviceId the <code>Id</code> of the device
     *  @throws org.osid.NullArgumentException <code>deviceId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeDevice(org.osid.id.Id deviceId) {
        org.osid.control.Device device;
        try {
            device = getDevice(deviceId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.devicesByGenus.remove(device.getGenusType());

        try (org.osid.type.TypeList types = device.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.devicesByRecord.remove(types.getNextType(), device);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeDevice(deviceId);
        return;
    }


    /**
     *  Gets a <code>DeviceList</code> corresponding to the given
     *  device genus <code>Type</code> which does not include
     *  devices of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known devices or an error results. Otherwise,
     *  the returned list may contain only those devices that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  deviceGenusType a device genus type 
     *  @return the returned <code>Device</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>deviceGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.DeviceList getDevicesByGenusType(org.osid.type.Type deviceGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.control.device.ArrayDeviceList(this.devicesByGenus.get(deviceGenusType)));
    }


    /**
     *  Gets a <code>DeviceList</code> containing the given
     *  device record <code>Type</code>. In plenary mode, the
     *  returned list contains all known devices or an error
     *  results. Otherwise, the returned list may contain only those
     *  devices that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  deviceRecordType a device record type 
     *  @return the returned <code>device</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>deviceRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.DeviceList getDevicesByRecordType(org.osid.type.Type deviceRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.control.device.ArrayDeviceList(this.devicesByRecord.get(deviceRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.devicesByGenus.clear();
        this.devicesByRecord.clear();

        super.close();

        return;
    }
}
