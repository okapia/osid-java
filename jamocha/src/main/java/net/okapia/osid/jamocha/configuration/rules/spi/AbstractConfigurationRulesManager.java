//
// AbstractConfigurationRulesManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.configuration.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractConfigurationRulesManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.configuration.rules.ConfigurationRulesManager,
               org.osid.configuration.rules.ConfigurationRulesProxyManager {

    private final Types valueEnablerRecordTypes            = new TypeRefSet();
    private final Types valueEnablerSearchRecordTypes      = new TypeRefSet();

    private final Types parameterProcessorRecordTypes      = new TypeRefSet();
    private final Types parameterProcessorSearchRecordTypes= new TypeRefSet();

    private final Types parameterProcessorEnablerRecordTypes= new TypeRefSet();
    private final Types parameterProcessorEnablerSearchRecordTypes= new TypeRefSet();


    /**
     *  Constructs a new
     *  <code>AbstractConfigurationRulesManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractConfigurationRulesManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any broker federation is exposed. Federation is exposed when 
     *  a specific broker may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  brokers appears as a single broker. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if looking up value enablers is supported. 
     *
     *  @return <code> true </code> if value enabler lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsValueEnablerLookup() {
        return (false);
    }


    /**
     *  Tests if querying value enablers is supported. 
     *
     *  @return <code> true </code> if value enabler query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsValueEnablerQuery() {
        return (false);
    }


    /**
     *  Tests if searching value enablers is supported. 
     *
     *  @return <code> true </code> if value enabler search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsValueEnablerSearch() {
        return (false);
    }


    /**
     *  Tests if a value enabler administrative service is supported. 
     *
     *  @return <code> true </code> if value enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsValueEnablerAdmin() {
        return (false);
    }


    /**
     *  Tests if a value enabler notification service is supported. 
     *
     *  @return <code> true </code> if value enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsValueEnablerNotification() {
        return (false);
    }


    /**
     *  Tests if a value enabler configuration lookup service is supported. 
     *
     *  @return <code> true </code> if a value enabler configuration lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsValueEnablerConfiguration() {
        return (false);
    }


    /**
     *  Tests if a value enabler configuration service is supported. 
     *
     *  @return <code> true </code> if value enabler configuration assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsValueEnablerConfigurationAssignment() {
        return (false);
    }


    /**
     *  Tests if a value enabler configuration lookup service is supported. 
     *
     *  @return <code> true </code> if a value enabler configuration service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsValueEnablerSmartConfiguration() {
        return (false);
    }


    /**
     *  Tests if a value enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if a value enabler rule lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsValueEnablerRuleLookup() {
        return (false);
    }


    /**
     *  Tests if a value enabler rule application service is supported. 
     *
     *  @return <code> true </code> if value enabler rule application service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsValueEnablerRuleApplication() {
        return (false);
    }


    /**
     *  Tests if looking up parameter processor is supported. 
     *
     *  @return <code> true </code> if parameter processor lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterProcessorLookup() {
        return (false);
    }


    /**
     *  Tests if querying parameter processor is supported. 
     *
     *  @return <code> true </code> if parameter processor query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterProcessorQuery() {
        return (false);
    }


    /**
     *  Tests if searching parameter processor is supported. 
     *
     *  @return <code> true </code> if parameter processor search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterProcessorSearch() {
        return (false);
    }


    /**
     *  Tests if a parameter processor administrative service is supported. 
     *
     *  @return <code> true </code> if parameter processor administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterProcessorAdmin() {
        return (false);
    }


    /**
     *  Tests if a parameter processor notification service is supported. 
     *
     *  @return <code> true </code> if parameter processor notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterProcessorNotification() {
        return (false);
    }


    /**
     *  Tests if a parameter processor configuration lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a parameter processor configuration 
     *          lookup service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterProcessorConfiguration() {
        return (false);
    }


    /**
     *  Tests if a parameter processor configuration service is supported. 
     *
     *  @return <code> true </code> if parameter processor configuration 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsParameterProcessorConfigurationAssignment() {
        return (false);
    }


    /**
     *  Tests if a parameter processor configuration lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a parameter processor configuration 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterProcessorSmartConfiguration() {
        return (false);
    }


    /**
     *  Tests if a parameter processor rule lookup service is supported. 
     *
     *  @return <code> true </code> if a parameter processor rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterProcessorRuleLookup() {
        return (false);
    }


    /**
     *  Tests if a parameter processor rule application service is supported. 
     *
     *  @return <code> true </code> if parameter processor rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterProcessorRuleApplication() {
        return (false);
    }


    /**
     *  Tests if looking up parameter processor enablers is supported. 
     *
     *  @return <code> true </code> if parameter processor enabler lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterProcessorEnablerLookup() {
        return (false);
    }


    /**
     *  Tests if querying parameter processor enablers is supported. 
     *
     *  @return <code> true </code> if parameter processor enabler query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterProcessorEnablerQuery() {
        return (false);
    }


    /**
     *  Tests if searching parameter processor enablers is supported. 
     *
     *  @return <code> true </code> if parameter processor enabler search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterProcessorEnablerSearch() {
        return (false);
    }


    /**
     *  Tests if a parameter processor enabler administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if parameter processor enabler 
     *          administration is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterProcessorEnablerAdmin() {
        return (false);
    }


    /**
     *  Tests if a parameter processor enabler notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if parameter processor enabler 
     *          notification is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterProcessorEnablerNotification() {
        return (false);
    }


    /**
     *  Tests if a parameter processor enabler configuration lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a parameter processor enabler 
     *          configuration lookup service is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterProcessorEnablerConfiguration() {
        return (false);
    }


    /**
     *  Tests if a parameter processor enabler configuration service is 
     *  supported. 
     *
     *  @return <code> true </code> if parameter processor enabler 
     *          configuration assignment service is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterProcessorEnablerConfigurationAssignment() {
        return (false);
    }


    /**
     *  Tests if a parameter processor enabler configuration lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a parameter processor enabler 
     *          configuration service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsParameterProcessorEnablerSmartConfiguration() {
        return (false);
    }


    /**
     *  Tests if a parameter processor enabler rule lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a processor enabler rule lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterProcessorEnablerRuleLookup() {
        return (false);
    }


    /**
     *  Tests if a parameter processor enabler rule application service is 
     *  supported. 
     *
     *  @return <code> true </code> if parameter processor enabler rule 
     *          application service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsParameterProcessorEnablerRuleApplication() {
        return (false);
    }


    /**
     *  Gets the supported <code> ValueEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> ValueEnabler </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getValueEnablerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.valueEnablerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> ValueEnabler </code> record type is 
     *  supported. 
     *
     *  @param  valueEnablerRecordType a <code> Type </code> indicating a 
     *          <code> ValueEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> valueEnablerRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsValueEnablerRecordType(org.osid.type.Type valueEnablerRecordType) {
        return (this.valueEnablerRecordTypes.contains(valueEnablerRecordType));
    }


    /**
     *  Adds support for a value enabler record type.
     *
     *  @param valueEnablerRecordType a value enabler record type
     *  @throws org.osid.NullArgumentException
     *  <code>valueEnablerRecordType</code> is <code>null</code>
     */

    protected void addValueEnablerRecordType(org.osid.type.Type valueEnablerRecordType) {
        this.valueEnablerRecordTypes.add(valueEnablerRecordType);
        return;
    }


    /**
     *  Removes support for a value enabler record type.
     *
     *  @param valueEnablerRecordType a value enabler record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>valueEnablerRecordType</code> is <code>null</code>
     */

    protected void removeValueEnablerRecordType(org.osid.type.Type valueEnablerRecordType) {
        this.valueEnablerRecordTypes.remove(valueEnablerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> ValueEnabler </code> search record types. 
     *
     *  @return a list containing the supported <code> ValueEnabler </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getValueEnablerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.valueEnablerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> ValueEnabler </code> search record type is 
     *  supported. 
     *
     *  @param  valueEnablerSearchRecordType a <code> Type </code> indicating 
     *          a <code> ValueEnabler </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          valueEnablerSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsValueEnablerSearchRecordType(org.osid.type.Type valueEnablerSearchRecordType) {
        return (this.valueEnablerSearchRecordTypes.contains(valueEnablerSearchRecordType));
    }


    /**
     *  Adds support for a value enabler search record type.
     *
     *  @param valueEnablerSearchRecordType a value enabler search record type
     *  @throws org.osid.NullArgumentException
     *  <code>valueEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void addValueEnablerSearchRecordType(org.osid.type.Type valueEnablerSearchRecordType) {
        this.valueEnablerSearchRecordTypes.add(valueEnablerSearchRecordType);
        return;
    }


    /**
     *  Removes support for a value enabler search record type.
     *
     *  @param valueEnablerSearchRecordType a value enabler search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>valueEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void removeValueEnablerSearchRecordType(org.osid.type.Type valueEnablerSearchRecordType) {
        this.valueEnablerSearchRecordTypes.remove(valueEnablerSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> ParameterProcessor </code> record types. 
     *
     *  @return a list containing the supported <code> ParameterProcessor 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getParameterProcessorRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.parameterProcessorRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> ParameterProcessor </code> record type is 
     *  supported. 
     *
     *  @param  parameterProcessorRecordType a <code> Type </code> indicating 
     *          a <code> ParameterProcessor </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          parameterProcessorRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsParameterProcessorRecordType(org.osid.type.Type parameterProcessorRecordType) {
        return (this.parameterProcessorRecordTypes.contains(parameterProcessorRecordType));
    }


    /**
     *  Adds support for a parameter processor record type.
     *
     *  @param parameterProcessorRecordType a parameter processor record type
     *  @throws org.osid.NullArgumentException
     *  <code>parameterProcessorRecordType</code> is <code>null</code>
     */

    protected void addParameterProcessorRecordType(org.osid.type.Type parameterProcessorRecordType) {
        this.parameterProcessorRecordTypes.add(parameterProcessorRecordType);
        return;
    }


    /**
     *  Removes support for a parameter processor record type.
     *
     *  @param parameterProcessorRecordType a parameter processor record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>parameterProcessorRecordType</code> is <code>null</code>
     */

    protected void removeParameterProcessorRecordType(org.osid.type.Type parameterProcessorRecordType) {
        this.parameterProcessorRecordTypes.remove(parameterProcessorRecordType);
        return;
    }


    /**
     *  Gets the supported <code> ParameterProcessor </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> ParameterProcessor 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getParameterProcessorSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.parameterProcessorSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> ParameterProcessor </code> search record 
     *  type is supported. 
     *
     *  @param  parameterProcessorSearchRecordType a <code> Type </code> 
     *          indicating a <code> ParameterProcessor </code> search record 
     *          type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          parameterProcessorSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsParameterProcessorSearchRecordType(org.osid.type.Type parameterProcessorSearchRecordType) {
        return (this.parameterProcessorSearchRecordTypes.contains(parameterProcessorSearchRecordType));
    }


    /**
     *  Adds support for a parameter processor search record type.
     *
     *  @param parameterProcessorSearchRecordType a parameter processor search record type
     *  @throws org.osid.NullArgumentException
     *  <code>parameterProcessorSearchRecordType</code> is <code>null</code>
     */

    protected void addParameterProcessorSearchRecordType(org.osid.type.Type parameterProcessorSearchRecordType) {
        this.parameterProcessorSearchRecordTypes.add(parameterProcessorSearchRecordType);
        return;
    }


    /**
     *  Removes support for a parameter processor search record type.
     *
     *  @param parameterProcessorSearchRecordType a parameter processor search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>parameterProcessorSearchRecordType</code> is <code>null</code>
     */

    protected void removeParameterProcessorSearchRecordType(org.osid.type.Type parameterProcessorSearchRecordType) {
        this.parameterProcessorSearchRecordTypes.remove(parameterProcessorSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> ParameterProcessorEnabler </code> record 
     *  types. 
     *
     *  @return a list containing the supported <code> 
     *          ParameterProcessorEnabler </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getParameterProcessorEnablerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.parameterProcessorEnablerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> ParameterProcessorEnabler </code> record 
     *  type is supported. 
     *
     *  @param  parameterProcessorEnablerRecordType a <code> Type </code> 
     *          indicating a <code> ParameterProcessorEnabler </code> record 
     *          type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          parameterProcessorEnablerRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsParameterProcessorEnablerRecordType(org.osid.type.Type parameterProcessorEnablerRecordType) {
        return (this.parameterProcessorEnablerRecordTypes.contains(parameterProcessorEnablerRecordType));
    }


    /**
     *  Adds support for a parameter processor enabler record type.
     *
     *  @param parameterProcessorEnablerRecordType a parameter processor enabler record type
     *  @throws org.osid.NullArgumentException
     *  <code>parameterProcessorEnablerRecordType</code> is <code>null</code>
     */

    protected void addParameterProcessorEnablerRecordType(org.osid.type.Type parameterProcessorEnablerRecordType) {
        this.parameterProcessorEnablerRecordTypes.add(parameterProcessorEnablerRecordType);
        return;
    }


    /**
     *  Removes support for a parameter processor enabler record type.
     *
     *  @param parameterProcessorEnablerRecordType a parameter processor enabler record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>parameterProcessorEnablerRecordType</code> is <code>null</code>
     */

    protected void removeParameterProcessorEnablerRecordType(org.osid.type.Type parameterProcessorEnablerRecordType) {
        this.parameterProcessorEnablerRecordTypes.remove(parameterProcessorEnablerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> ParameterProcessorEnabler </code> search 
     *  record types. 
     *
     *  @return a list containing the supported <code> 
     *          ParameterProcessorEnabler </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getParameterProcessorEnablerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.parameterProcessorEnablerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> ParameterProcessorEnabler </code> search 
     *  record type is supported. 
     *
     *  @param  parameterProcessorEnablerSearchRecordType a <code> Type 
     *          </code> indicating a <code> ParameterProcessorEnabler </code> 
     *          search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          parameterProcessorEnablerSearchRecordType </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public boolean supportsParameterProcessorEnablerSearchRecordType(org.osid.type.Type parameterProcessorEnablerSearchRecordType) {
        return (this.parameterProcessorEnablerSearchRecordTypes.contains(parameterProcessorEnablerSearchRecordType));
    }


    /**
     *  Adds support for a parameter processor enabler search record type.
     *
     *  @param parameterProcessorEnablerSearchRecordType a parameter processor enabler search record type
     *  @throws org.osid.NullArgumentException
     *  <code>parameterProcessorEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void addParameterProcessorEnablerSearchRecordType(org.osid.type.Type parameterProcessorEnablerSearchRecordType) {
        this.parameterProcessorEnablerSearchRecordTypes.add(parameterProcessorEnablerSearchRecordType);
        return;
    }


    /**
     *  Removes support for a parameter processor enabler search record type.
     *
     *  @param parameterProcessorEnablerSearchRecordType a parameter processor enabler search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>parameterProcessorEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void removeParameterProcessorEnablerSearchRecordType(org.osid.type.Type parameterProcessorEnablerSearchRecordType) {
        this.parameterProcessorEnablerSearchRecordTypes.remove(parameterProcessorEnablerSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the value enabler 
     *  lookup service. 
     *
     *  @return a <code> ValueEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueEnablerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerLookupSession getValueEnablerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesManager.getValueEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the value enabler 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ValueEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueEnablerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerLookupSession getValueEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesProxyManager.getValueEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the value enabler 
     *  lookup service for the given configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @return a <code> ValueEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerLookupSession getValueEnablerLookupSessionForConfiguration(org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesManager.getValueEnablerLookupSessionForConfiguration not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the value enabler 
     *  lookup service for the given configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ValueEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerLookupSession getValueEnablerLookupSessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesProxyManager.getValueEnablerLookupSessionForConfiguration not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the value enabler 
     *  query service. 
     *
     *  @return a <code> ValueEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueEnablerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerQuerySession getValueEnablerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesManager.getValueEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the value enabler 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ValueEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueEnablerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerQuerySession getValueEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesProxyManager.getValueEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the value enabler 
     *  query service for the given configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @return a <code> ValueEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerQuerySession getValueEnablerQuerySessionForConfiguration(org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesManager.getValueEnablerQuerySessionForConfiguration not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the value enabler 
     *  query service for the given configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ValueEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerQuerySession getValueEnablerQuerySessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesProxyManager.getValueEnablerQuerySessionForConfiguration not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the value enabler 
     *  search service. 
     *
     *  @return a <code> ValueEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueEnablerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerSearchSession getValueEnablerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesManager.getValueEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the value enabler 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ValueEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueEnablerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerSearchSession getValueEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesProxyManager.getValueEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the value enablers 
     *  earch service for the given configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @return a <code> ValueEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerSearchSession getValueEnablerSearchSessionForConfiguration(org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesManager.getValueEnablerSearchSessionForConfiguration not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the value enablers 
     *  earch service for the given configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ValueEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerSearchSession getValueEnablerSearchSessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesProxyManager.getValueEnablerSearchSessionForConfiguration not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the value enabler 
     *  administration service. 
     *
     *  @return a <code> ValueEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueEnablerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerAdminSession getValueEnablerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesManager.getValueEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the value enabler 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ValueEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueEnablerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerAdminSession getValueEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesProxyManager.getValueEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the value enabler 
     *  administration service for the given configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @return a <code> ValueEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerAdminSession getValueEnablerAdminSessionForConfiguration(org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesManager.getValueEnablerAdminSessionForConfiguration not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the value enabler 
     *  administration service for the given configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ValueEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerAdminSession getValueEnablerAdminSessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesProxyManager.getValueEnablerAdminSessionForConfiguration not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the value enabler 
     *  notification service. 
     *
     *  @param  valueEnablerReceiver the notification callback 
     *  @return a <code> ValueEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> valueEnablerReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerNotificationSession getValueEnablerNotificationSession(org.osid.configuration.rules.ValueEnablerReceiver valueEnablerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesManager.getValueEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the value enabler 
     *  notification service. 
     *
     *  @param  valueEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> ValueEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> valueEnablerReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerNotificationSession getValueEnablerNotificationSession(org.osid.configuration.rules.ValueEnablerReceiver valueEnablerReceiver, 
                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesProxyManager.getValueEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the value enabler 
     *  notification service for the given configuration. 
     *
     *  @param  valueEnablerReceiver the notification callback 
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @return a <code> ValueEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no configuration found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> valueEnablerReceiver 
     *          </code> or <code> configurationId </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerNotificationSession getValueEnablerNotificationSessionForConfiguration(org.osid.configuration.rules.ValueEnablerReceiver valueEnablerReceiver, 
                                                                                                                           org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesManager.getValueEnablerNotificationSessionForConfiguration not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the value enabler 
     *  notification service for the given configuration. 
     *
     *  @param  valueEnablerReceiver the notification callback 
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ValueEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no configuration found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> valueEnablerReceiver, 
     *          configurationId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerNotificationSession getValueEnablerNotificationSessionForConfiguration(org.osid.configuration.rules.ValueEnablerReceiver valueEnablerReceiver, 
                                                                                                                           org.osid.id.Id configurationId, 
                                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesProxyManager.getValueEnablerNotificationSessionForConfiguration not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup value 
     *  enabler/configuration mappings for value enablers. 
     *
     *  @return a <code> ValueEnablerConfigurationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueEnablerConfiguration() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerConfigurationSession getValueEnablerConfigurationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesManager.getValueEnablerConfigurationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup value 
     *  enabler/configuration mappings for value enablers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ValueEnablerConfigurationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueEnablerConfiguration() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerConfigurationSession getValueEnablerConfigurationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesProxyManager.getValueEnablerConfigurationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning value 
     *  enablers to configurations. 
     *
     *  @return a <code> ValueEnablerConfigurationAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueEnablerConfigurationAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerConfigurationAssignmentSession getValueEnablerConfigurationAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesManager.getValueEnablerConfigurationAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning value 
     *  enablers to configurations. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ValueEnablerConfigurationAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueEnablerConfigurationAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerConfigurationAssignmentSession getValueEnablerConfigurationAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesProxyManager.getValueEnablerConfigurationAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage value enabler smart 
     *  configurations. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @return a <code> ValueEnablerSmartConfigurationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueEnablerSmartConfiguration() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerSmartConfigurationSession getValueEnablerSmartConfigurationSession(org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesManager.getValueEnablerSmartConfigurationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage value enabler smart 
     *  configurations. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ValueEnablerSmartConfigurationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueEnablerSmartConfiguration() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerSmartConfigurationSession getValueEnablerSmartConfigurationSession(org.osid.id.Id configurationId, 
                                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesProxyManager.getValueEnablerSmartConfigurationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the value enabler 
     *  mapping lookup service. 
     *
     *  @return a <code> ValueEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerRuleLookupSession getValueEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesManager.getValueEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the value enabler 
     *  mapping lookup service . 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ValueEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerRuleLookupSession getValueEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesProxyManager.getValueEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the value enabler 
     *  mapping lookup service for the given configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @return a <code> ValueEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerRuleLookupSession getValueEnablerRuleLookupSessionForConfiguration(org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesManager.getValueEnablerRuleLookupSessionForConfiguration not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the value enabler 
     *  mapping lookup service for the given configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ValueEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerRuleLookupSession getValueEnablerRuleLookupSessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesProxyManager.getValueEnablerRuleLookupSessionForConfiguration not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the value enabler 
     *  assignment service. 
     *
     *  @return a <code> ValueEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueEnablerRuleApplication() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerRuleApplicationSession getValueEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesManager.getValueEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the value enabler 
     *  assignment service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ValueEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueEnablerRuleApplication() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerRuleApplicationSession getValueEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesProxyManager.getValueEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the value enabler 
     *  assignment service for the given configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @return a <code> ValueEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerRuleApplicationSession getValueEnablerRuleApplicationSessionForConfiguration(org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesManager.getValueEnablerRuleApplicationSessionForConfiguration not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the value enabler 
     *  assignment service for the given configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ValueEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerRuleApplicationSession getValueEnablerRuleApplicationSessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesProxyManager.getValueEnablerRuleApplicationSessionForConfiguration not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor lookup service. 
     *
     *  @return a <code> ParameterProcessorLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorLookupSession getParameterProcessorLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesManager.getParameterProcessorLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorLookupSession getParameterProcessorLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesProxyManager.getParameterProcessorLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor lookup service for the given configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @return a <code> ParameterProcessorLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorLookupSession getParameterProcessorLookupSessionForConfiguration(org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesManager.getParameterProcessorLookupSessionForConfiguration not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor lookup service for the given configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorLookupSession getParameterProcessorLookupSessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesProxyManager.getParameterProcessorLookupSessionForConfiguration not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor query service. 
     *
     *  @return a <code> ParameterProcessorQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorQuerySession getParameterProcessorQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesManager.getParameterProcessorQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorQuerySession getParameterProcessorQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesProxyManager.getParameterProcessorQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor query service for the given configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @return a <code> ParameterProcessorQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorQuerySession getParameterProcessorQuerySessionForConfiguration(org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesManager.getParameterProcessorQuerySessionForConfiguration not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor query service for the given configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorQuerySession getParameterProcessorQuerySessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesProxyManager.getParameterProcessorQuerySessionForConfiguration not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor search service. 
     *
     *  @return a <code> ParameterProcessorSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorSearchSession getParameterProcessorSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesManager.getParameterProcessorSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorSearchSession getParameterProcessorSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesProxyManager.getParameterProcessorSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor earch service for the given configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @return a <code> ParameterProcessorSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorSearchSession getParameterProcessorSearchSessionForConfiguration(org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesManager.getParameterProcessorSearchSessionForConfiguration not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor earch service for the given configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorSearchSession getParameterProcessorSearchSessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesProxyManager.getParameterProcessorSearchSessionForConfiguration not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor administration service. 
     *
     *  @return a <code> ParameterProcessorAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorAdminSession getParameterProcessorAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesManager.getParameterProcessorAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorAdminSession getParameterProcessorAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesProxyManager.getParameterProcessorAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor administration service for the given configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @return a <code> ParameterProcessorAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorAdminSession getParameterProcessorAdminSessionForConfiguration(org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesManager.getParameterProcessorAdminSessionForConfiguration not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor administration service for the given configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorAdminSession getParameterProcessorAdminSessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesProxyManager.getParameterProcessorAdminSessionForConfiguration not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor notification service. 
     *
     *  @param  parameterProcessorReceiver the notification callback 
     *  @return a <code> ParameterProcessorNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          parameterProcessorReceiver </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorNotificationSession getParameterProcessorNotificationSession(org.osid.configuration.rules.ParameterProcessorReceiver parameterProcessorReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesManager.getParameterProcessorNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor notification service. 
     *
     *  @param  paremeterProcessorReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          paremeterProcessorReceiver </code> or <code> proxy </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorNotificationSession getParameterProcessorNotificationSession(org.osid.configuration.rules.ParameterProcessorReceiver paremeterProcessorReceiver, 
                                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesProxyManager.getParameterProcessorNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor notification service for the given configuration. 
     *
     *  @param  parameterProcessorReceiver the notification callback 
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @return a <code> ParameterProcessorNotificationSession </code> 
     *  @throws org.osid.NotFoundException no configuration found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          parameterProcessorReceiver </code> or <code> configurationId 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorNotificationSession getParameterProcessorNotificationSessionForConfiguration(org.osid.configuration.rules.ParameterProcessorReceiver parameterProcessorReceiver, 
                                                                                                                                       org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesManager.getParameterProcessorNotificationSessionForConfiguration not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor notification service for the given configuration. 
     *
     *  @param  paremeterProcessorReceiver the notification callback 
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorNotificationSession </code> 
     *  @throws org.osid.NotFoundException no configuration found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          paremeterProcessorReceiver, configurationId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorNotificationSession getParameterProcessorNotificationSessionForConfiguration(org.osid.configuration.rules.ParameterProcessorReceiver paremeterProcessorReceiver, 
                                                                                                                                       org.osid.id.Id configurationId, 
                                                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesProxyManager.getParameterProcessorNotificationSessionForConfiguration not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup parameter 
     *  processor/configuration mappings for parameter processors. 
     *
     *  @return a <code> ParameterProcessorConfigurationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorConfiguration() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorConfigurationSession getParameterProcessorConfigurationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesManager.getParameterProcessorConfigurationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup parameter 
     *  processor/configuration mappings for parameter processors. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorConfigurationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorConfiguration() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorConfigurationSession getParameterProcessorConfigurationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesProxyManager.getParameterProcessorConfigurationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  parameter processor to configurations. 
     *
     *  @return a <code> ParameterProcessorConfigurationAssignmentSession 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorConfigurationAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorConfigurationAssignmentSession getParameterProcessorConfigurationAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesManager.getParameterProcessorConfigurationAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  parameter processor to configurations. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorConfigurationAssignmentSession 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorConfigurationAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorConfigurationAssignmentSession getParameterProcessorConfigurationAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesProxyManager.getParameterProcessorConfigurationAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage parameter processor 
     *  smart configurations. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @return a <code> ParameterProcessorSmartConfigurationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorSmartConfiguration() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorSmartConfigurationSession getParameterProcessorSmartConfigurationSession(org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesManager.getParameterProcessorSmartConfigurationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage parameter processor 
     *  smart configurations. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorSmartConfigurationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorSmartConfiguration() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorSmartConfigurationSession getParameterProcessorSmartConfigurationSession(org.osid.id.Id configurationId, 
                                                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesProxyManager.getParameterProcessorSmartConfigurationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor mapping lookup service for looking up the rules applied to 
     *  the configuration. 
     *
     *  @return a <code> ParameterProcessorRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorRuleLookupSession getParameterProcessorRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesManager.getParameterProcessorRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor mapping lookup service for looking up the rules applied to 
     *  the configuration. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorRuleLookupSession getParameterProcessorRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesProxyManager.getParameterProcessorRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor mapping lookup service for the given configuration for 
     *  looking up rules applied to an configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @return a <code> ParameterProcessorRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorRuleLookupSession getParameterProcessorRuleLookupSessionForConfiguration(org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesManager.getParameterProcessorRuleLookupSessionForConfiguration not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor mapping lookup service for the given configuration for 
     *  looking up rules applied to an configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorRuleLookupSession getParameterProcessorRuleLookupSessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesProxyManager.getParameterProcessorRuleLookupSessionForConfiguration not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor assignment service. 
     *
     *  @return a <code> ParameterProcessorRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorRuleApplicationSession getParameterProcessorRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesManager.getParameterProcessorRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor assignment service to apply to configurations. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorRuleApplicationSession getParameterProcessorRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesProxyManager.getParameterProcessorRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor assignment service for the given configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @return a <code> ParameterProcessorRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorRuleApplicationSession getParameterProcessorRuleApplicationSessionForConfiguration(org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesManager.getParameterProcessorRuleApplicationSessionForConfiguration not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor assignment service for the given configuration to apply to 
     *  configurations. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorRuleApplicationSession getParameterProcessorRuleApplicationSessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesProxyManager.getParameterProcessorRuleApplicationSessionForConfiguration not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor enabler lookup service. 
     *
     *  @return a <code> ParameterProcessorEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorEnablerLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerLookupSession getParameterProcessorEnablerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesManager.getParameterProcessorEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor enabler lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorEnablerLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerLookupSession getParameterProcessorEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesProxyManager.getParameterProcessorEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor enabler lookup service for the given configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @return a <code> ParameterProcessorEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerLookupSession getParameterProcessorEnablerLookupSessionForConfiguration(org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesManager.getParameterProcessorEnablerLookupSessionForConfiguration not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor enabler lookup service for the given configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerLookupSession getParameterProcessorEnablerLookupSessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesProxyManager.getParameterProcessorEnablerLookupSessionForConfiguration not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor enabler query service. 
     *
     *  @return a <code> ParameterProcessorEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorEnablerQuery() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerQuerySession getParameterProcessorEnablerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesManager.getParameterProcessorEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor enabler query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorEnablerQuery() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerQuerySession getParameterProcessorEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesProxyManager.getParameterProcessorEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor enabler query service for the given configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @return a <code> ParameterProcessorEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerQuerySession getParameterProcessorEnablerQuerySessionForConfiguration(org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesManager.getParameterProcessorEnablerQuerySessionForConfiguration not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor enabler query service for the given configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerQuerySession getParameterProcessorEnablerQuerySessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesProxyManager.getParameterProcessorEnablerQuerySessionForConfiguration not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor enabler search service. 
     *
     *  @return a <code> ParameterProcessorEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorEnablerSearch() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerSearchSession getParameterProcessorEnablerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesManager.getParameterProcessorEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor enabler search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorEnablerSearch() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerSearchSession getParameterProcessorEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesProxyManager.getParameterProcessorEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor enablers earch service for the given configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @return a <code> ParameterProcessorEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerSearchSession getParameterProcessorEnablerSearchSessionForConfiguration(org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesManager.getParameterProcessorEnablerSearchSessionForConfiguration not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor enablers earch service for the given configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerSearchSession getParameterProcessorEnablerSearchSessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesProxyManager.getParameterProcessorEnablerSearchSessionForConfiguration not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor enabler administration service. 
     *
     *  @return a <code> ParameterProcessorEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorEnablerAdmin() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerAdminSession getParameterProcessorEnablerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesManager.getParameterProcessorEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor enabler administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorEnablerAdmin() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerAdminSession getParameterProcessorEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesProxyManager.getParameterProcessorEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor enabler administration service for the given configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @return a <code> ParameterProcessorEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerAdminSession getParameterProcessorEnablerAdminSessionForConfiguration(org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesManager.getParameterProcessorEnablerAdminSessionForConfiguration not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor enabler administration service for the given configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerAdminSession getParameterProcessorEnablerAdminSessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesProxyManager.getParameterProcessorEnablerAdminSessionForConfiguration not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor enabler notification service. 
     *
     *  @param  parameterProcessorEnablerReceiver the notification callback 
     *  @return a <code> ParameterProcessorEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          parameterProcessorEnablerReceiver </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorEnablerNotification() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerNotificationSession getParameterProcessorEnablerNotificationSession(org.osid.configuration.rules.ParameterProcessorEnablerReceiver parameterProcessorEnablerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesManager.getParameterProcessorEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor enabler notification service. 
     *
     *  @param  parameterProcessorEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          parameterProcessorEnablerReceiver </code> or <code> proxy 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorEnablerNotification() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerNotificationSession getParameterProcessorEnablerNotificationSession(org.osid.configuration.rules.ParameterProcessorEnablerReceiver parameterProcessorEnablerReceiver, 
                                                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesProxyManager.getParameterProcessorEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor enabler notification service for the given configuration. 
     *
     *  @param  parameterProcessorEnablerReceiver the notification callback 
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @return a <code> ParameterProcessorEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no configuration found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          parameterProcessorEnablerReceiver </code> or <code> 
     *          configurationId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorEnablerNotification() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerNotificationSession getParameterProcessorEnablerNotificationSessionForConfiguration(org.osid.configuration.rules.ParameterProcessorEnablerReceiver parameterProcessorEnablerReceiver, 
                                                                                                                                                     org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesManager.getParameterProcessorEnablerNotificationSessionForConfiguration not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor enabler notification service for the given configuration. 
     *
     *  @param  parameterProcessorEnablerReceiver the notification callback 
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no configuration found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          parameterProcessorEnablerReceiver, configurationId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorEnablerNotification() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerNotificationSession getParameterProcessorEnablerNotificationSessionForConfiguration(org.osid.configuration.rules.ParameterProcessorEnablerReceiver parameterProcessorEnablerReceiver, 
                                                                                                                                                     org.osid.id.Id configurationId, 
                                                                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesProxyManager.getParameterProcessorEnablerNotificationSessionForConfiguration not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup parameter processor 
     *  enabler/configuration mappings for parameter processor enablers. 
     *
     *  @return a <code> ParameterProcessorEnablerConfigurationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorEnablerConfiguration() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerConfigurationSession getParameterProcessorEnablerConfigurationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesManager.getParameterProcessorEnablerConfigurationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup parameter processor 
     *  enabler/configuration mappings for parameter processor enablers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorEnablerConfigurationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorEnablerConfiguration() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerConfigurationSession getParameterProcessorEnablerConfigurationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesProxyManager.getParameterProcessorEnablerConfigurationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  parameter processor enablers to configurations. 
     *
     *  @return a <code> 
     *          ParameterProcessorEnablerConfigurationAssignmentSession 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorEnablerConfigurationAssignment() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerConfigurationAssignmentSession getParameterProcessorEnablerConfigurationAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesManager.getParameterProcessorEnablerConfigurationAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  parameter processor enablers to configurations. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> 
     *          ParameterProcessorEnablerConfigurationAssignmentSession 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorEnablerConfigurationAssignment() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerConfigurationAssignmentSession getParameterProcessorEnablerConfigurationAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesProxyManager.getParameterProcessorEnablerConfigurationAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage parameter processor 
     *  enabler smart configurations. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @return a <code> ParameterProcessorEnablerSmartConfigurationSession 
     *          </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorEnablerSmartConfiguration() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerSmartConfigurationSession getParameterProcessorEnablerSmartConfigurationSession(org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesManager.getParameterProcessorEnablerSmartConfigurationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage parameter processor 
     *  enabler smart configurations. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorEnablerSmartConfigurationSession 
     *          </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorEnablerSmartConfiguration() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerSmartConfigurationSession getParameterProcessorEnablerSmartConfigurationSession(org.osid.id.Id configurationId, 
                                                                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesProxyManager.getParameterProcessorEnablerSmartConfigurationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor enabler mapping lookup service. 
     *
     *  @return a <code> ParameterProcessorEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorEnablerRuleLookup() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerRuleLookupSession getParameterProcessorEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesManager.getParameterProcessorEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor enabler mapping lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorEnablerRuleLookup() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerRuleLookupSession getParameterProcessorEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesProxyManager.getParameterProcessorEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor enabler mapping lookup service. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @return a <code> ParameterProcessorEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorEnablerRuleLookup() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerRuleLookupSession getParameterProcessorEnablerRuleLookupSessionForConfiguration(org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesManager.getParameterProcessorEnablerRuleLookupSessionForConfiguration not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor enabler mapping lookup service for the given configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorEnablerRuleLookup() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerRuleLookupSession getParameterProcessorEnablerRuleLookupSessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesProxyManager.getParameterProcessorEnablerRuleLookupSessionForConfiguration not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor enabler assignment service. 
     *
     *  @return a <code> ParameterProcessorEnablerRuleApplicationSession 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerRuleApplicationSession getParameterProcessorEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesManager.getParameterProcessorEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor enabler assignment service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorEnablerRuleApplicationSession 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerRuleApplicationSession getParameterProcessorEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesProxyManager.getParameterProcessorEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor enabler assignment service for the given configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @return a <code> ParameterProcessorEnablerRuleApplicationSession 
     *          </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerRuleApplicationSession getParameterProcessorEnablerRuleApplicationSessionForConfiguration(org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesManager.getParameterProcessorEnablerRuleApplicationSessionForConfiguration not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the parameter 
     *  processor enabler assignment service for the given configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ParameterProcessorEnablerRuleApplicationSession 
     *          </code> 
     *  @throws org.osid.NotFoundException no <code> Configuration </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterProcessorEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerRuleApplicationSession getParameterProcessorEnablerRuleApplicationSessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.rules.ConfigurationRulesProxyManager.getParameterProcessorEnablerRuleApplicationSessionForConfiguration not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.valueEnablerRecordTypes.clear();
        this.valueEnablerRecordTypes.clear();

        this.valueEnablerSearchRecordTypes.clear();
        this.valueEnablerSearchRecordTypes.clear();

        this.parameterProcessorRecordTypes.clear();
        this.parameterProcessorRecordTypes.clear();

        this.parameterProcessorSearchRecordTypes.clear();
        this.parameterProcessorSearchRecordTypes.clear();

        this.parameterProcessorEnablerRecordTypes.clear();
        this.parameterProcessorEnablerRecordTypes.clear();

        this.parameterProcessorEnablerSearchRecordTypes.clear();
        this.parameterProcessorEnablerSearchRecordTypes.clear();

        return;
    }
}
