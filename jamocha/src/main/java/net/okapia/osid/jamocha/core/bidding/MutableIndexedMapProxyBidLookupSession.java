//
// MutableIndexedMapProxyBidLookupSession
//
//    Implements a Bid lookup service backed by a collection of
//    bids indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.bidding;


/**
 *  Implements a Bid lookup service backed by a collection of
 *  bids. The bids are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some bids may be compatible
 *  with more types than are indicated through these bid
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of bids can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyBidLookupSession
    extends net.okapia.osid.jamocha.core.bidding.spi.AbstractIndexedMapBidLookupSession
    implements org.osid.bidding.BidLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyBidLookupSession} with
     *  no bid.
     *
     *  @param auctionHouse the auction house
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code auctionHouse} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyBidLookupSession(org.osid.bidding.AuctionHouse auctionHouse,
                                                       org.osid.proxy.Proxy proxy) {
        setAuctionHouse(auctionHouse);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyBidLookupSession} with
     *  a single bid.
     *
     *  @param auctionHouse the auction house
     *  @param  bid an bid
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code auctionHouse},
     *          {@code bid}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyBidLookupSession(org.osid.bidding.AuctionHouse auctionHouse,
                                                       org.osid.bidding.Bid bid, org.osid.proxy.Proxy proxy) {

        this(auctionHouse, proxy);
        putBid(bid);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyBidLookupSession} using
     *  an array of bids.
     *
     *  @param auctionHouse the auction house
     *  @param  bids an array of bids
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code auctionHouse},
     *          {@code bids}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyBidLookupSession(org.osid.bidding.AuctionHouse auctionHouse,
                                                       org.osid.bidding.Bid[] bids, org.osid.proxy.Proxy proxy) {

        this(auctionHouse, proxy);
        putBids(bids);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyBidLookupSession} using
     *  a collection of bids.
     *
     *  @param auctionHouse the auction house
     *  @param  bids a collection of bids
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code auctionHouse},
     *          {@code bids}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyBidLookupSession(org.osid.bidding.AuctionHouse auctionHouse,
                                                       java.util.Collection<? extends org.osid.bidding.Bid> bids,
                                                       org.osid.proxy.Proxy proxy) {
        this(auctionHouse, proxy);
        putBids(bids);
        return;
    }

    
    /**
     *  Makes a {@code Bid} available in this session.
     *
     *  @param  bid a bid
     *  @throws org.osid.NullArgumentException {@code bid{@code 
     *          is {@code null}
     */

    @Override
    public void putBid(org.osid.bidding.Bid bid) {
        super.putBid(bid);
        return;
    }


    /**
     *  Makes an array of bids available in this session.
     *
     *  @param  bids an array of bids
     *  @throws org.osid.NullArgumentException {@code bids{@code 
     *          is {@code null}
     */

    @Override
    public void putBids(org.osid.bidding.Bid[] bids) {
        super.putBids(bids);
        return;
    }


    /**
     *  Makes collection of bids available in this session.
     *
     *  @param  bids a collection of bids
     *  @throws org.osid.NullArgumentException {@code bid{@code 
     *          is {@code null}
     */

    @Override
    public void putBids(java.util.Collection<? extends org.osid.bidding.Bid> bids) {
        super.putBids(bids);
        return;
    }


    /**
     *  Removes a Bid from this session.
     *
     *  @param bidId the {@code Id} of the bid
     *  @throws org.osid.NullArgumentException {@code bidId{@code  is
     *          {@code null}
     */

    @Override
    public void removeBid(org.osid.id.Id bidId) {
        super.removeBid(bidId);
        return;
    }    
}
