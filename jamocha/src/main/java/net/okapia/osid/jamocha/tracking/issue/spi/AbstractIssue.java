//
// AbstractIssue.java
//
//     Defines an Issue.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.tracking.issue.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>Issue</code>.
 */

public abstract class AbstractIssue
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.tracking.Issue {

    private org.osid.tracking.Queue queue;
    private org.osid.resource.Resource customer;
    private org.osid.ontology.Subject topic;
    private org.osid.tracking.Issue masterIssue;
    private org.osid.tracking.Issue branchedIssue;
    private org.osid.tracking.Issue rootIssue;
    private org.osid.type.Type priorityType;
    private org.osid.resource.Resource creator;
    private org.osid.authentication.Agent creatingAgent;
    private org.osid.calendaring.DateTime createdDate;
    private org.osid.resource.Resource reopener;
    private org.osid.authentication.Agent reopeningAgent;
    private org.osid.calendaring.DateTime reopenedDate;
    private org.osid.calendaring.DateTime dueDate;
    private org.osid.resource.Resource resolver;
    private org.osid.authentication.Agent resolvingAgent;
    private org.osid.calendaring.DateTime resolvedDate;
    private org.osid.type.Type resolutionType;
    private org.osid.resource.Resource closer;
    private org.osid.authentication.Agent closingAgent;
    private org.osid.calendaring.DateTime closedDate;
    private org.osid.resource.Resource assignedResource;

    private boolean duplicate = false;
    private boolean pending   = false;
    private boolean blocked   = false;

    private final java.util.Collection<org.osid.tracking.Issue> duplicateIssues = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.tracking.Issue> blockingIssues = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.tracking.records.IssueRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the queue <code> Id. </code> 
     *
     *  @return the queue <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getQueueId() {
        return (this.queue.getId());
    }


    /**
     *  Gets the queue. 
     *
     *  @return the queue 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.tracking.Queue getQueue()
        throws org.osid.OperationFailedException {

        return (this.queue);
    }


    /**
     *  Sets the queue.
     *
     *  @param queue a queue
     *  @throws org.osid.NullArgumentException
     *          <code>queue</code> is <code>null</code>
     */

    protected void setQueue(org.osid.tracking.Queue queue) {
        nullarg(queue, "queue");
        this.queue = queue;
        return;
    }


    /**
     *  Gets the customer <code> Id</code>.
     *
     *  @return the customer <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCustomerId() {
        return (this.customer.getId());
    }


    /**
     *  Gets the customer. 
     *
     *  @return the customer 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getCustomer()
        throws org.osid.OperationFailedException {

        return (this.customer);
    }


    /**
     *  Sets the customer.
     *
     *  @param customer a customer
     *  @throws org.osid.NullArgumentException
     *          <code>customer</code> is <code>null</code>
     */

    protected void setCustomer(org.osid.resource.Resource customer) {
        nullarg(customer, "customer");
        this.customer = customer;
        return;
    }


    /**
     *  Gets the topic <code> Id</code>.
     *
     *  @return the topic <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getTopicId() {
        return (this.topic.getId());
    }


    /**
     *  Gets the topic. 
     *
     *  @return the topic 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.ontology.Subject getTopic()
        throws org.osid.OperationFailedException {

        return (this.topic);
    }


    /**
     *  Sets the topic.
     *
     *  @param topic a topic
     *  @throws org.osid.NullArgumentException
     *          <code>topic</code> is <code>null</code>
     */

    protected void setTopic(org.osid.ontology.Subject topic) {
        nullarg(topic, "topic");
        this.topic = topic;
        return;
    }


    /**
     *  Tests if this issue is a subtask of another issue. 
     *
     *  @return <code> true </code> if this issue is a subtask, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isSubTask() {
        return (this.masterIssue != null);
    }


    /**
     *  Gets the master issue <code> Id </code> . If <code> isSubtask() 
     *  </code> is <code> false </code> then the <code> Id </code> of this 
     *  <code> Issue </code> is returned. 
     *
     *  @return the master issue <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getMasterIssueId() {
        if (!isSubTask()) {
            return (getId());
        } else {
            return (this.masterIssue.getId());
        }
    }


    /**
     *  Gets the master issue. If <code> isSubtask() </code> is <code> false 
     *  </code> then this <code> Issue </code> is returned. 
     *
     *  @return the master issue 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.tracking.Issue getMasterIssue()
        throws org.osid.OperationFailedException {

        if (!isSubTask()) {
            return (this);
        } else {
            return (this.masterIssue);
        }
    }


    /**
     *  Sets the master issue.
     *
     *  @param issue a master issue
     *  @throws org.osid.NullArgumentException
     *          <code>issue</code> is <code>null</code>
     */

    protected void setMasterIssue(org.osid.tracking.Issue issue) {
        nullarg(issue, "master issue");
        this.masterIssue = issue;
        return;
    }


    /**
     *  Tests if this issue is a duplicate of another issue. 
     *
     *  @return <code> true </code> if this issue is a duplicate, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isDuplicate() {
        return (this.duplicate);
    }


    /**
     *  Gets the duplicate issue <code> Ids. </code> 
     *
     *  @return the duplicate issue <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> isDuplicate() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getDuplicateIssueIds() {
        if (!isDuplicate()) {
            throw new org.osid.IllegalStateException("isDuplicate() is false");
        }

        return (new net.okapia.osid.jamocha.adapter.converter.tracking.issue.IssueToIdList(this.duplicateIssues));
    }


    /**
     *  Gets the duplicate issues. 
     *
     *  @return the duplicate issues 
     *  @throws org.osid.IllegalStateException <code> isDuplicate() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */
    
    @OSID @Override
    public org.osid.tracking.IssueList getDuplicateIssues()
        throws org.osid.OperationFailedException {

        if (!isDuplicate()) {
            throw new org.osid.IllegalStateException("isDuplicate() is false");
        }

        return (new net.okapia.osid.jamocha.tracking.issue.ArrayIssueList(this.duplicateIssues));
    }


    /**
     *  Adds a duplicate issue.
     *
     *  @param issue a duplicate issue
     *  @throws org.osid.NullArgumentException <code>issue</code>
     *          is <code>null</code>
     */

    protected void addDuplicateIssue(org.osid.tracking.Issue issue) {
        nullarg(issue, "duplicate issue");

        this.duplicateIssues.add(issue);
        this.duplicate = true;

        return;
    }


    /**
     *  Sets all the duplicate issues.
     *
     *  @param issues a collection of duplicate issues
     *  @throws org.osid.NullArgumentException <code>issues</code>
     *          is <code>null</code>
     */

    protected void setDuplicateIssues(java.util.Collection<org.osid.tracking.Issue> issues) {
        nullarg(issues, "duplicate issues");

        this.duplicateIssues.addAll(issues);
        this.duplicate = true;

        return;
    }


    /**
     *  Tests if this issue is a branch of another issue. 
     *
     *  @return <code> true </code> if this issue is a branch, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isBranched() {
        return (this.branchedIssue != null);
    }


    /**
     *  Gets the branched issue <code> Id. </code> 
     *
     *  @return the branched issue 
     *  @throws org.osid.IllegalStateException <code> isBranched() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getBranchedIssueId() {
        if (!isBranched()) {
            throw new org.osid.IllegalStateException("isBranched() is false");
        }

        return (this.branchedIssue.getId());
    }


    /**
     *  Gets the branched issue. 
     *
     *  @return the branched issue 
     *  @throws org.osid.IllegalStateException <code> isBranched() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */
    
    @OSID @Override
    public org.osid.tracking.Issue getBranchedIssue()
        throws org.osid.OperationFailedException {

        if (!isBranched()) {
            throw new org.osid.IllegalStateException("isBranched() is false");
        }

        return (this.branchedIssue);
    }


    /**
     *  Sets the branched issue.
     *
     *  @param issue a branched issue
     *  @throws org.osid.NullArgumentException
     *          <code>issue</code> is <code>null</code>
     */

    protected void setBranchedIssue(org.osid.tracking.Issue issue) {
        nullarg(issue, "branched issue");
        this.branchedIssue = issue;
        return;
    }


    /**
     *  Gets the root issue <code> Id </code> . If <code> isBranched()
     *  </code> is <code> false </code> then the <code> Id </code> of
     *  this <code> Issue </code> is returned.
     *
     *  @return the root issue <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getRootIssueId() {
        if (isBranched()) {
            return (this.rootIssue.getId());
        } else {
            return (getId());
        }
    }


    /**
     *  Gets the root issue. If <code> isBranched() </code> is <code>
     *  false </code> then this <code> Issue </code> is returned.
     *
     *  @return the root issue 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.tracking.Issue getRootIssue()
        throws org.osid.OperationFailedException {

        if (isBranched()) {
            return (this.rootIssue);
        } else {
            return (this);
        }
    }


    /**
     *  Sets the root issue.
     *
     *  @param issue a root issue
     *  @throws org.osid.NullArgumentException
     *          <code>issue</code> is <code>null</code>
     */

    protected void setRootIssue(org.osid.tracking.Issue issue) {
        nullarg(issue, "root issue");
        this.rootIssue = issue;
        return;
    }


    /**
     *  Gets the priority type of this issue. 
     *
     *  @return the priority type 
     */

    @OSID @Override
    public org.osid.type.Type getPriorityType() {
        return (this.priorityType);
    }


    /**
     *  Sets the priority type.
     *
     *  @param priorityType a priority type
     *  @throws org.osid.NullArgumentException
     *          <code>priorityType</code> is <code>null</code>
     */

    protected void setPriorityType(org.osid.type.Type priorityType) {
        nullarg(priorityType, "priority type");
        this.priorityType = priorityType;
        return;
    }


    /**
     *  Gets the creator <code> Id. </code> 
     *
     *  @return the creator <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCreatorId() {
        return (this.creator.getId());
    }


    /**
     *  Gets the creator of this issue. 
     *
     *  @return the creator 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getCreator()
        throws org.osid.OperationFailedException {

        return (this.creator);
    }


    /**
     *  Sets the creator.
     *
     *  @param creator a creator
     *  @throws org.osid.NullArgumentException
     *          <code>creator</code> is <code>null</code>
     */

    protected void setCreator(org.osid.resource.Resource creator) {
        nullarg(creator, "creator");
        this.creator = creator;
        return;
    }


    /**
     *  Gets the creating agent <code> Id. </code> 
     *
     *  @return the agent <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCreatingAgentId() {
        return (this.creatingAgent.getId());
    }


    /**
     *  Gets the creating agent of this issue. 
     *
     *  @return the agent
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getCreatingAgent()
        throws org.osid.OperationFailedException {

        return (this.creatingAgent);
    }


    /**
     *  Sets the creating agent.
     *
     *  @param agent the agent
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    protected void setCreatingAgent(org.osid.authentication.Agent agent) {
        nullarg(agent, "creating agent");
        this.creatingAgent = agent;;
        return;
    }


    /**
     *  Gets the created date. 
     *
     *  @return the created date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getCreatedDate() {
        return (this.createdDate);
    }


    /**
     *  Sets the created date. 
     *
     *  @param date the created date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    protected void setCreatedDate(org.osid.calendaring.DateTime date) {
        nullarg(date, "created date");
        this.createdDate = date;
        return;
    }


    /**
     *  Tests if this issue has been resurrected after a close. A
     *  reopened <code> Issue </code> extends the effective dates
     *  through to when it is closed again. The details of when it was
     *  opened or closed is maintained in the log.
     *
     *  @return <code> true </code> if this issue is reopened, <code>
     *          false </code> otherwise
     */

    @OSID @Override
    public boolean isReopened() {
        return ((this.reopener != null) && (this.reopenedDate != null));
    }


    /**
     *  Gets the reopener <code> Id. </code> 
     *
     *  @return the reopener <code> Id </code> 
     *  @throws org.osid.IllegalStateException
     *          <code>isReopened()</code> is <code>false</code>
     */

    @OSID @Override
    public org.osid.id.Id getReopenerId() {
        if (!isReopened()) {
            throw new org.osid.IllegalStateException("isReopened() is false");
        }

        return (this.reopener.getId());
    }


    /**
     *  Gets the reopener of this issue. 
     *
     *  @return the reopener 
     *  @throws org.osid.IllegalStateException
     *          <code>isReopened()</code> is <code>false</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getReopener()
        throws org.osid.OperationFailedException {

        if (!isReopened()) {
            throw new org.osid.IllegalStateException("isReopened() is false");
        }

        return (this.reopener);
    }


    /**
     *  Sets the reopener.
     *
     *  @param reopener a reopener
     *  @throws org.osid.NullArgumentException
     *          <code>reopener</code> is <code>null</code>
     */

    protected void setReopener(org.osid.resource.Resource reopener) {
        nullarg(reopener, "reopener");
        this.reopener = reopener;
        return;
    }


    /**
     *  Gets the reopening agent <code> Id. </code> 
     *
     *  @return the agent <code> Id </code> 
     *  @throws org.osid.IllegalStateException
     *          <code>isReopened()</code> is <code>false</code>
     */

    @OSID @Override
    public org.osid.id.Id getReopeningAgentId() {
        if (!isReopened()) {
            throw new org.osid.IllegalStateException("isReopened() is false");
        }

        return (this.reopeningAgent.getId());
    }


    /**
     *  Gets the reopening agemt of this issue. 
     *
     *  @return the agent
     *  @throws org.osid.IllegalStateException
     *          <code>isReopened()</code> is <code>false</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getReopeningAgent()
        throws org.osid.OperationFailedException {

        if (!isReopened()) {
            throw new org.osid.IllegalStateException("isReopened() is false");
        }

        return (this.reopeningAgent);
    }


    /**
     *  Sets the reopening agent.
     *
     *  @param agent the agent
     *  @throws org.osid.NullArgumentException <code>agent</code>
     *          is <code>null</code>
     */

    protected void setReopeningAgent(org.osid.authentication.Agent agent) {
        nullarg(agent, "reopening agent");
        this.reopeningAgent = reopeningAgent;
        return;
    }


    /**
     *  Gets last the reopened date. 
     *
     *  @return last the reopened date 
     *  @throws org.osid.IllegalStateException <code> hasReopened() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getReopenedDate() {
        if (!isReopened()) {
            throw new org.osid.IllegalStateException("isReopened() is false");
        }

        return (this.reopenedDate);
    }


    /**
     *  Sets last the reopened date. 
     *
     *  @param date the last reopened date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    protected void setReopenedDate(org.osid.calendaring.DateTime date) {
        nullarg(date, "reopened date");
        this.reopenedDate = date;
        return;
    }


    /**
     *  Tests if this issue has a due date. 
     *
     *  @return <code> true </code> if this issue has a due date,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean hasDueDate() {
        return (this.dueDate != null);
    }


    /**
     *  Gets the due date. 
     *
     *  @return the due date 
     *  @throws org.osid.IllegalStateException <code> hasDueDate() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getDueDate() {
        if (!hasDueDate()) {
            throw new org.osid.IllegalStateException("hasDueDate() is false");
        }

        return (this.dueDate);
    }


    /**
     *  Sets the due date.
     *
     *  @param date a due date
     *  @throws org.osid.NullArgumentException
     *          <code>date</code> is <code>null</code>
     */

    protected void setDueDate(org.osid.calendaring.DateTime date) {
        nullarg(date, "due date");
        this.dueDate = dueDate;
        return;
    }


    /**
     *  Tests if this issue is pending a response from the customer. 
     *
     *  @return <code> true </code> if this issue is pending, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isPending() {
        return (this.pending);
    }


    /**
     *  Sets the pending flag.
     *
     *  @param pending <code>true</code> if pending,
     *         <code>false</code> otherwise
     */

    protected void setPending(boolean pending) {
        this.pending = pending;
        return;
    }


    /**
     *  Tests if this issue is blocked on another issue. 
     *
     *  @return <code> true </code> if this issue is blocked, <code>
     *          false </code> otherwise
     */

    @OSID @Override
    public boolean isBlocked() {
        return (this.blocked);
    }


    /**
     *  Gets the blocker issue <code> Ids. </code> 
     *
     *  @return the blocking issue <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> isBlocked() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getBlockerIds() {
        if (!isBlocked()) {
            throw new org.osid.IllegalStateException("isBlocked() is false");
        }

        return (new net.okapia.osid.jamocha.adapter.converter.tracking.issue.IssueToIdList(this.blockingIssues));
    }


    /**
     *  Gets the blocking issues. 
     *
     *  @return the blocking issues 
     *  @throws org.osid.IllegalStateException <code> isBlocked() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.tracking.IssueList getBlockers()
        throws org.osid.OperationFailedException {

        if (!isBlocked()) {
            throw new org.osid.IllegalStateException("isBlocked() is false");
        }

        return (new net.okapia.osid.jamocha.tracking.issue.ArrayIssueList(this.blockingIssues));
    }


    /**
     *  Adds a blocking issue.
     *
     *  @param issue a blocking issue
     *  @throws org.osid.NullArgumentException <code>issue</code>
     *          is <code>null</code>
     */

    protected void addBlocker(org.osid.tracking.Issue issue) {
        nullarg(issue, "blocking issue");

        this.blockingIssues.add(issue);
        this.blocked = true;

        return;
    }


    /**
     *  Sets all the blocking issues.
     *
     *  @param issues a collection of blocking issues
     *  @throws org.osid.NullArgumentException <code>issues</code>
     *          is <code>null</code>
     */

    protected void setBlockers(java.util.Collection<org.osid.tracking.Issue> issues) {
        nullarg(issues, "blocking issues");

        this.blockingIssues.addAll(issues);
        this.blocked = true;

        return;
    }

        
    /**
     *  Tests if this issue is resolved. 
     *
     *  @return <code> true </code> if this issue is resolved, <code> false 
     *          </code> if unresolved 
     */

    @OSID @Override
    public boolean isResolved() {
        return ((this.resolver != null) && (this.resolvedDate != null) && 
                (this.resolutionType != null));
    }


    /**
     *  Gets the resolver <code> Id. </code> 
     *
     *  @return the resolver <code> Id </code> 
     *  @throws org.osid.IllegalStateException
     *          <code>isResolved()</code> is <code>false</code>
     */

    @OSID @Override
    public org.osid.id.Id getResolverId() {
        if (!isResolved()) {
            throw new org.osid.IllegalStateException("isResolved() is false");
        }

        return (this.resolver.getId());
    }


    /**
     *  Gets the resolver of this issue. 
     *
     *  @return the resolver 
     *  @throws org.osid.IllegalStateException
     *          <code>isResolved()</code> is <code>false</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getResolver()
        throws org.osid.OperationFailedException {

        if (!isResolved()) {
            throw new org.osid.IllegalStateException("isResolved() is false");
        }

        return (this.resolver);
    }


    /**
     *  Sets the resolver.
     *
     *  @param resolver a resolver
     *  @throws org.osid.NullArgumentException
     *          <code>resolver</code> is <code>null</code>
     */

    protected void setResolver(org.osid.resource.Resource resolver) {
        nullarg(resolver, "resolver");
        this.resolver = resolver;
        return;
    }


    /**
     *  Gets the resolving agent <code> Id. </code> 
     *
     *  @return the agent <code> Id </code> 
     *  @throws org.osid.IllegalStateException
     *          <code>isResolved()</code> is <code>false</code>
     */

    @OSID @Override
    public org.osid.id.Id getResolvingAgentId() {
        if (!isResolved()) {
            throw new org.osid.IllegalStateException("isResolved() is false");
        }

        return (this.resolvingAgent.getId());
    }


    /**
     *  Gets the resolving agemt of this issue. 
     *
     *  @return the agent
     *  @throws org.osid.IllegalStateException
     *          <code>isResolved()</code> is <code>false</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getResolvingAgent()
        throws org.osid.OperationFailedException {

        if (!isResolved()) {
            throw new org.osid.IllegalStateException("isResolved() is false");
        }

        return (this.resolvingAgent);
    }


    /**
     *  Sets the resolving agent.
     *
     *  @param agent the agent
     *  @throws org.osid.NullArgumentException <code>agent</code>
     *          is <code>null</code>
     */

    protected void setResolvingAgent(org.osid.authentication.Agent agent) {
        nullarg(agent, "resolving agent");
        this.resolvingAgent = resolvingAgent;
        return;
    }


    /**
     *  Gets the resolved date. A resolved issue is still open until it is 
     *  closed. 
     *
     *  @return the resolved date 
     *  @throws org.osid.IllegalStateException <code> isresolved() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getResolvedDate() {
        if (!isResolved()) {
            throw new org.osid.IllegalStateException("isResolved() is false");
        }

        return (this.resolvedDate);
    }


    /**
     *  Sets the resolved date.
     *
     *  @param date a resolved date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    protected void setResolvedDate(org.osid.calendaring.DateTime date) {
        nullarg(date, "resolved date");
        this.resolvedDate = date;
        return;
    }


    /**
     *  Gets a type indicating the resolution; "fixed," "canceled", "cannot 
     *  reproduce." 
     *
     *  @return the resolution type 
     *  @throws org.osid.IllegalStateException <code> isResolved() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.type.Type getResolutionType() {
        if (!isResolved()) {
            throw new org.osid.IllegalStateException("isResolved() is false");
        }

        return (this.resolutionType);
    }


    /**
     *  Sets the resolution type.
     *
     *  @param resolutionType a resolution type
     *  @throws org.osid.NullArgumentException
     *          <code>resolutionType</code> is <code>null</code>
     */

    protected void setResolutionType(org.osid.type.Type resolutionType) {
        nullarg(resolutionType, "resolution type");
        this.resolutionType = resolutionType;
        return;
    }


    /**
     *  Tests if this issue is closed. An issue may be left opened
     *  after being resolved for acknowledgement or review.
     *
     *  @return <code> true </code> if this issue is closed, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isClosed() {
        return ((this.closer != null) && (this.closedDate != null));
    }


    /**
     *  Gets the resource <code> Id </code> of the closer.
     *
     *  @return the closing agent <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isClosed() </code> is 
     *          <code> true </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCloserId() {
        if (!isClosed()) {
            throw new org.osid.IllegalStateException("isClosed() is false");
        }

        return (this.closer.getId());
    }


    /**
     *  Gets the resource of the closer. 
     *
     *  @return the closer
     *  @throws org.osid.IllegalStateException <code> isClosed()
     *          </code> is <code> true </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getCloser()
        throws org.osid.OperationFailedException {

        if (!isClosed()) {
            throw new org.osid.IllegalStateException("isClosed() is false");
        }

        return (this.closer);
    }


    /**
     *  Sets the closer.
     *
     *  @param closer a closer
     *  @throws org.osid.NullArgumentException
     *          <code>closer</code> is <code>null</code>
     */

    protected void setCloser(org.osid.resource.Resource closer) {
        nullarg(closer, "closer");
        this.closer = closer;
        return;
    }


    /**
     *  Gets the agent <code> Id </code> of the closer. 
     *
     *  @return the closing agent <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isClosed() </code> is 
     *          <code> true </code> 
     */

    @OSID @Override
    public org.osid.id.Id getClosingAgentId() {
        if (!isClosed()) {
            throw new org.osid.IllegalStateException("isClosed() is false");
        }

        return (this.closingAgent.getId());
    }


    /**
     *  Gets the agent of the closer. 
     *
     *  @return the closing agent 
     *  @throws org.osid.IllegalStateException <code> isClosed() </code> is 
     *          <code> true </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getClosingAgent()
        throws org.osid.OperationFailedException {

        if (!isClosed()) {
            throw new org.osid.IllegalStateException("isClosed() is false");
        }

        return (this.closingAgent);
    }


    /**
     *  Sets the closing agent.
     *
     *  @param agent the closing agent
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    protected void setClosingAgent(org.osid.authentication.Agent agent) {
        nullarg(agent, "closing agent");
        this.closingAgent = agent;
        return;
    }


    /**
     *  Gets the closed date. A closed issue is still open until it is 
     *  closed. 
     *
     *  @return the closed date 
     *  @throws org.osid.IllegalStateException <code> isclosed() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getClosedDate() {
        if (!isClosed()) {
            throw new org.osid.IllegalStateException("isClosed() is false");
        }

        return (this.closedDate);
    }


    /**
     *  Sets the closed date.
     *
     *  @param date a closed date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    protected void setClosedDate(org.osid.calendaring.DateTime date) {
        nullarg(date, "closed date");
        this.closedDate = date;
        return;
    }


    /**
     *  Tests if this issue is assigned. 
     *
     *  @return <code> true </code> if this issue is assigned, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isAssigned() {
        return (this.assignedResource != null);
    }


    /**
     *  Gets the assigned resource <code> Id. </code> 
     *
     *  @return the assigned resource <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isAssigned() </code> is 
     *          <code> false </code> or <code> isClosed() </code> is <code> 
     *          true </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAssignedResourceId() {
        if (!isAssigned()) {
            throw new org.osid.IllegalStateException("isAssigned() is false");
        }

        return (this.assignedResource.getId());
    }


    /**
     *  Gets the assigned resource. 
     *
     *  @return the assigned resource 
     *  @throws org.osid.IllegalStateException <code> isAssigned() </code> is 
     *          <code> false </code> or <code> isClosed() </code> is <code> 
     *          true </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getAssignedResource()
        throws org.osid.OperationFailedException {

        if (!isAssigned()) {
            throw new org.osid.IllegalStateException("isAssigned() is false");
        }

        return (this.assignedResource);
    }


    /**
     *  Sets the assigned resource.
     *
     *  @param resource an assigned resource
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    protected void setAssignedResource(org.osid.resource.Resource resource) {
        nullarg(resource, "assigned resource");
        this.assignedResource = assignedResource;
        return;
    }


    /**
     *  Tests if this issue supports the given record
     *  <code>Type</code>.
     *
     *  @param  issueRecordType an issue record type 
     *  @return <code>true</code> if the issueRecordType is supported,
     *          <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>issueRecordType</code> is <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type issueRecordType) {
        for (org.osid.tracking.records.IssueRecord record : this.records) {
            if (record.implementsRecordType(issueRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given <code>Issue</code>
     *  record <code>Type</code>.
     *
     *  @param  issueRecordType the issue record type 
     *  @return the issue record 
     *  @throws org.osid.NullArgumentException
     *          <code>issueRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(issueRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.tracking.records.IssueRecord getIssueRecord(org.osid.type.Type issueRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.tracking.records.IssueRecord record : this.records) {
            if (record.implementsRecordType(issueRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(issueRecordType + " is not supported");
    }


    /**
     *  Adds a record to this issue. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param issueRecord the issue record
     *  @param issueRecordType issue record type
     *  @throws org.osid.NullArgumentException
     *          <code>issueRecord</code> or
     *          <code>issueRecordType</code> is <code>null</code>
     */
            
    protected void addIssueRecord(org.osid.tracking.records.IssueRecord issueRecord, 
                                  org.osid.type.Type issueRecordType) {

        nullarg(issueRecord, "issue record");
        addRecordType(issueRecordType);
        this.records.add(issueRecord);
        
        return;
    }
}
