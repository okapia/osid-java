//
// AbstractIndexedMapCalendarLookupSession.java
//
//    A simple framework for providing a Calendar lookup service
//    backed by a fixed collection of calendars with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Calendar lookup service backed by a
 *  fixed collection of calendars. The calendars are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some calendars may be compatible
 *  with more types than are indicated through these calendar
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Calendars</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapCalendarLookupSession
    extends AbstractMapCalendarLookupSession
    implements org.osid.calendaring.CalendarLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.calendaring.Calendar> calendarsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.calendaring.Calendar>());
    private final MultiMap<org.osid.type.Type, org.osid.calendaring.Calendar> calendarsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.calendaring.Calendar>());


    /**
     *  Makes a <code>Calendar</code> available in this session.
     *
     *  @param  calendar a calendar
     *  @throws org.osid.NullArgumentException <code>calendar<code> is
     *          <code>null</code>
     */

    @Override
    protected void putCalendar(org.osid.calendaring.Calendar calendar) {
        super.putCalendar(calendar);

        this.calendarsByGenus.put(calendar.getGenusType(), calendar);
        
        try (org.osid.type.TypeList types = calendar.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.calendarsByRecord.put(types.getNextType(), calendar);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a calendar from this session.
     *
     *  @param calendarId the <code>Id</code> of the calendar
     *  @throws org.osid.NullArgumentException <code>calendarId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeCalendar(org.osid.id.Id calendarId) {
        org.osid.calendaring.Calendar calendar;
        try {
            calendar = getCalendar(calendarId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.calendarsByGenus.remove(calendar.getGenusType());

        try (org.osid.type.TypeList types = calendar.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.calendarsByRecord.remove(types.getNextType(), calendar);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeCalendar(calendarId);
        return;
    }


    /**
     *  Gets a <code>CalendarList</code> corresponding to the given
     *  calendar genus <code>Type</code> which does not include
     *  calendars of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known calendars or an error results. Otherwise,
     *  the returned list may contain only those calendars that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  calendarGenusType a calendar genus type 
     *  @return the returned <code>Calendar</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>calendarGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarList getCalendarsByGenusType(org.osid.type.Type calendarGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.calendaring.calendar.ArrayCalendarList(this.calendarsByGenus.get(calendarGenusType)));
    }


    /**
     *  Gets a <code>CalendarList</code> containing the given
     *  calendar record <code>Type</code>. In plenary mode, the
     *  returned list contains all known calendars or an error
     *  results. Otherwise, the returned list may contain only those
     *  calendars that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  calendarRecordType a calendar record type 
     *  @return the returned <code>calendar</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>calendarRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarList getCalendarsByRecordType(org.osid.type.Type calendarRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.calendaring.calendar.ArrayCalendarList(this.calendarsByRecord.get(calendarRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.calendarsByGenus.clear();
        this.calendarsByRecord.clear();

        super.close();

        return;
    }
}
