//
// AbstractAdapterSceneLookupSession.java
//
//    A Scene lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.control.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Scene lookup session adapter.
 */

public abstract class AbstractAdapterSceneLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.control.SceneLookupSession {

    private final org.osid.control.SceneLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterSceneLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterSceneLookupSession(org.osid.control.SceneLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code System/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code System Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getSystemId() {
        return (this.session.getSystemId());
    }


    /**
     *  Gets the {@code System} associated with this session.
     *
     *  @return the {@code System} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.System getSystem()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getSystem());
    }


    /**
     *  Tests if this user can perform {@code Scene} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupScenes() {
        return (this.session.canLookupScenes());
    }


    /**
     *  A complete view of the {@code Scene} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeSceneView() {
        this.session.useComparativeSceneView();
        return;
    }


    /**
     *  A complete view of the {@code Scene} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenarySceneView() {
        this.session.usePlenarySceneView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include scenes in systems which are children
     *  of this system in the system hierarchy.
     */

    @OSID @Override
    public void useFederatedSystemView() {
        this.session.useFederatedSystemView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this system only.
     */

    @OSID @Override
    public void useIsolatedSystemView() {
        this.session.useIsolatedSystemView();
        return;
    }
    
     
    /**
     *  Gets the {@code Scene} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Scene} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Scene} and
     *  retained for compatibility.
     *
     *  @param sceneId {@code Id} of the {@code Scene}
     *  @return the scene
     *  @throws org.osid.NotFoundException {@code sceneId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code sceneId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.Scene getScene(org.osid.id.Id sceneId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getScene(sceneId));
    }


    /**
     *  Gets a {@code SceneList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  scenes specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Scenes} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  sceneIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Scene} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code sceneIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.SceneList getScenesByIds(org.osid.id.IdList sceneIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getScenesByIds(sceneIds));
    }


    /**
     *  Gets a {@code SceneList} corresponding to the given
     *  scene genus {@code Type} which does not include
     *  scenes of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  scenes or an error results. Otherwise, the returned list
     *  may contain only those scenes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  sceneGenusType a scene genus type 
     *  @return the returned {@code Scene} list
     *  @throws org.osid.NullArgumentException
     *          {@code sceneGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.SceneList getScenesByGenusType(org.osid.type.Type sceneGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getScenesByGenusType(sceneGenusType));
    }


    /**
     *  Gets a {@code SceneList} corresponding to the given
     *  scene genus {@code Type} and include any additional
     *  scenes with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  scenes or an error results. Otherwise, the returned list
     *  may contain only those scenes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  sceneGenusType a scene genus type 
     *  @return the returned {@code Scene} list
     *  @throws org.osid.NullArgumentException
     *          {@code sceneGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.SceneList getScenesByParentGenusType(org.osid.type.Type sceneGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getScenesByParentGenusType(sceneGenusType));
    }


    /**
     *  Gets a {@code SceneList} containing the given
     *  scene record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  scenes or an error results. Otherwise, the returned list
     *  may contain only those scenes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  sceneRecordType a scene record type 
     *  @return the returned {@code Scene} list
     *  @throws org.osid.NullArgumentException
     *          {@code sceneRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.SceneList getScenesByRecordType(org.osid.type.Type sceneRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getScenesByRecordType(sceneRecordType));
    }


    /**
     *  Gets a list of scenes by a setting.
     *  
     *  In plenary mode, the returned list contains all known scenes
     *  or an error results. Otherwise, the returned list may contain
     *  only those scenes that are accessible through this session.
     *
     *  @param  settingId a setting {@code Id} 
     *  @return the returned {@code Scene} list 
     *  @throws org.osid.NullArgumentException {@code settingId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @compliance mandatory This method must be implemented. 
     */

    @OSID @Override
    public org.osid.control.SceneList getScenesBySetting(org.osid.id.Id settingId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getScenesBySetting(settingId));
    }


    /**
     *  Gets all {@code Scenes}. 
     *
     *  In plenary mode, the returned list contains all known
     *  scenes or an error results. Otherwise, the returned list
     *  may contain only those scenes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Scenes} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.SceneList getScenes()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getScenes());
    }
}
