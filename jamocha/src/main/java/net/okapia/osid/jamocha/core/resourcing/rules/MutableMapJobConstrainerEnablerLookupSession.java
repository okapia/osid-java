//
// MutableMapJobConstrainerEnablerLookupSession
//
//    Implements a JobConstrainerEnabler lookup service backed by a collection of
//    jobConstrainerEnablers that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing.rules;


/**
 *  Implements a JobConstrainerEnabler lookup service backed by a collection of
 *  job constrainer enablers. The job constrainer enablers are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of job constrainer enablers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapJobConstrainerEnablerLookupSession
    extends net.okapia.osid.jamocha.core.resourcing.rules.spi.AbstractMapJobConstrainerEnablerLookupSession
    implements org.osid.resourcing.rules.JobConstrainerEnablerLookupSession {


    /**
     *  Constructs a new {@code MutableMapJobConstrainerEnablerLookupSession}
     *  with no job constrainer enablers.
     *
     *  @param foundry the foundry
     *  @throws org.osid.NullArgumentException {@code foundry} is
     *          {@code null}
     */

      public MutableMapJobConstrainerEnablerLookupSession(org.osid.resourcing.Foundry foundry) {
        setFoundry(foundry);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapJobConstrainerEnablerLookupSession} with a
     *  single jobConstrainerEnabler.
     *
     *  @param foundry the foundry  
     *  @param jobConstrainerEnabler a job constrainer enabler
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code jobConstrainerEnabler} is {@code null}
     */

    public MutableMapJobConstrainerEnablerLookupSession(org.osid.resourcing.Foundry foundry,
                                           org.osid.resourcing.rules.JobConstrainerEnabler jobConstrainerEnabler) {
        this(foundry);
        putJobConstrainerEnabler(jobConstrainerEnabler);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapJobConstrainerEnablerLookupSession}
     *  using an array of job constrainer enablers.
     *
     *  @param foundry the foundry
     *  @param jobConstrainerEnablers an array of job constrainer enablers
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code jobConstrainerEnablers} is {@code null}
     */

    public MutableMapJobConstrainerEnablerLookupSession(org.osid.resourcing.Foundry foundry,
                                           org.osid.resourcing.rules.JobConstrainerEnabler[] jobConstrainerEnablers) {
        this(foundry);
        putJobConstrainerEnablers(jobConstrainerEnablers);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapJobConstrainerEnablerLookupSession}
     *  using a collection of job constrainer enablers.
     *
     *  @param foundry the foundry
     *  @param jobConstrainerEnablers a collection of job constrainer enablers
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code jobConstrainerEnablers} is {@code null}
     */

    public MutableMapJobConstrainerEnablerLookupSession(org.osid.resourcing.Foundry foundry,
                                           java.util.Collection<? extends org.osid.resourcing.rules.JobConstrainerEnabler> jobConstrainerEnablers) {

        this(foundry);
        putJobConstrainerEnablers(jobConstrainerEnablers);
        return;
    }

    
    /**
     *  Makes a {@code JobConstrainerEnabler} available in this session.
     *
     *  @param jobConstrainerEnabler a job constrainer enabler
     *  @throws org.osid.NullArgumentException {@code jobConstrainerEnabler{@code  is
     *          {@code null}
     */

    @Override
    public void putJobConstrainerEnabler(org.osid.resourcing.rules.JobConstrainerEnabler jobConstrainerEnabler) {
        super.putJobConstrainerEnabler(jobConstrainerEnabler);
        return;
    }


    /**
     *  Makes an array of job constrainer enablers available in this session.
     *
     *  @param jobConstrainerEnablers an array of job constrainer enablers
     *  @throws org.osid.NullArgumentException {@code jobConstrainerEnablers{@code 
     *          is {@code null}
     */

    @Override
    public void putJobConstrainerEnablers(org.osid.resourcing.rules.JobConstrainerEnabler[] jobConstrainerEnablers) {
        super.putJobConstrainerEnablers(jobConstrainerEnablers);
        return;
    }


    /**
     *  Makes collection of job constrainer enablers available in this session.
     *
     *  @param jobConstrainerEnablers a collection of job constrainer enablers
     *  @throws org.osid.NullArgumentException {@code jobConstrainerEnablers{@code  is
     *          {@code null}
     */

    @Override
    public void putJobConstrainerEnablers(java.util.Collection<? extends org.osid.resourcing.rules.JobConstrainerEnabler> jobConstrainerEnablers) {
        super.putJobConstrainerEnablers(jobConstrainerEnablers);
        return;
    }


    /**
     *  Removes a JobConstrainerEnabler from this session.
     *
     *  @param jobConstrainerEnablerId the {@code Id} of the job constrainer enabler
     *  @throws org.osid.NullArgumentException {@code jobConstrainerEnablerId{@code 
     *          is {@code null}
     */

    @Override
    public void removeJobConstrainerEnabler(org.osid.id.Id jobConstrainerEnablerId) {
        super.removeJobConstrainerEnabler(jobConstrainerEnablerId);
        return;
    }    
}
