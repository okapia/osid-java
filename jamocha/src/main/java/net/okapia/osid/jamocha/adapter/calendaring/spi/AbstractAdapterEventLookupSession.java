//
// AbstractAdapterEventLookupSession.java
//
//    An Event lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.calendaring.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An Event lookup session adapter.
 */

public abstract class AbstractAdapterEventLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.calendaring.EventLookupSession {

    private final org.osid.calendaring.EventLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterEventLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterEventLookupSession(org.osid.calendaring.EventLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Calendar/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Calendar Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCalendarId() {
        return (this.session.getCalendarId());
    }


    /**
     *  Gets the {@code Calendar} associated with this session.
     *
     *  @return the {@code Calendar} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCalendar());
    }


    /**
     *  Tests if this user can perform {@code Event} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupEvents() {
        return (this.session.canLookupEvents());
    }


    /**
     *  A complete view of the {@code Event} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeEventView() {
        this.session.useComparativeEventView();
        return;
    }


    /**
     *  A complete view of the {@code Event} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryEventView() {
        this.session.usePlenaryEventView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include events in calendars which are children
     *  of this calendar in the calendar hierarchy.
     */

    @OSID @Override
    public void useFederatedCalendarView() {
        this.session.useFederatedCalendarView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this calendar only.
     */

    @OSID @Override
    public void useIsolatedCalendarView() {
        this.session.useIsolatedCalendarView();
        return;
    }
    

    /**
     *  Only events whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveEventView() {
        this.session.useEffectiveEventView();
        return;
    }
    

    /**
     *  All events of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveEventView() {
        this.session.useAnyEffectiveEventView();
        return;
    }


    /**
     *  A normalized view uses a single {@code Event} to
     *  represent a set of recurring events.
     */

    @OSID @Override
    public void useNormalizedEventView() {
        this.session.useNormalizedEventView();
        return;
    }


    /**
     *  A denormalized view expands recurring events into a series of
     *  {@code Events.}
     */

    @OSID @Override
    public void useDenormalizedEventView() {
        this.session.useDenormalizedEventView();
        return;
    }


    /**
     *  The returns from the lookup methods omit sequestered
     *  events.
     */

    @OSID @Override
    public void useSequesteredEventView() {
        this.session.useSequesteredEventView();
        return;
    }


    /**
     *  All events are returned including sequestered events.
     */

    @OSID @Override
    public void useUnsequesteredEventView() {
        this.session.useUnsequesteredEventView();
        return;
    }


    /**
     *  Gets the {@code Event} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Event} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Event} and
     *  retained for compatibility.
     *
     *  In effective mode, events are returned that are currently
     *  effective.  In any effective mode, effective events and
     *  those currently expired are returned.
     *
     *  @param eventId {@code Id} of the {@code Event}
     *  @return the event
     *  @throws org.osid.NotFoundException {@code eventId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code eventId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Event getEvent(org.osid.id.Id eventId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEvent(eventId));
    }


    /**
     *  Gets an {@code EventList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  events specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Events} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, events are returned that are currently
     *  effective.  In any effective mode, effective events and
     *  those currently expired are returned.
     *
     *  @param  eventIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Event} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code eventIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.EventList getEventsByIds(org.osid.id.IdList eventIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEventsByIds(eventIds));
    }


    /**
     *  Gets an {@code EventList} corresponding to the given
     *  event genus {@code Type} which does not include
     *  events of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  events or an error results. Otherwise, the returned list
     *  may contain only those events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, events are returned that are currently
     *  effective.  In any effective mode, effective events and
     *  those currently expired are returned.
     *
     *  @param  eventGenusType an event genus type 
     *  @return the returned {@code Event} list
     *  @throws org.osid.NullArgumentException
     *          {@code eventGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.EventList getEventsByGenusType(org.osid.type.Type eventGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEventsByGenusType(eventGenusType));
    }


    /**
     *  Gets an {@code EventList} corresponding to the given
     *  event genus {@code Type} and include any additional
     *  events with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  events or an error results. Otherwise, the returned list
     *  may contain only those events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, events are returned that are currently
     *  effective.  In any effective mode, effective events and
     *  those currently expired are returned.
     *
     *  @param  eventGenusType an event genus type 
     *  @return the returned {@code Event} list
     *  @throws org.osid.NullArgumentException
     *          {@code eventGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.EventList getEventsByParentGenusType(org.osid.type.Type eventGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEventsByParentGenusType(eventGenusType));
    }


    /**
     *  Gets an {@code EventList} containing the given
     *  event record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  events or an error results. Otherwise, the returned list
     *  may contain only those events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, events are returned that are currently
     *  effective.  In any effective mode, effective events and
     *  those currently expired are returned.
     *
     *  @param  eventRecordType an event record type 
     *  @return the returned {@code Event} list
     *  @throws org.osid.NullArgumentException
     *          {@code eventRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.EventList getEventsByRecordType(org.osid.type.Type eventRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEventsByRecordType(eventRecordType));
    }


    /**
     *  Gets an {@code EventList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  events or an error results. Otherwise, the returned list
     *  may contain only those events that are accessible
     *  through this session.
     *  
     *  In active mode, events are returned that are currently
     *  active. In any status mode, active and inactive events
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Event} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.calendaring.EventList getEventsOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEventsOnDate(from, to));
    }
        

    /**
     *  Gets an {@code EventList} that fall within the given
     *  range inclusive.
     *  
     *  In plenary mode, the returned list contains all known events
     *  or an error results. Otherwise, the returned list may contain
     *  only those events that are accessible through this session.
     *  
     *  In normalized mode, recurring events appear as a single
     *  event. In denormalized mode, each element in the recurring
     *  series appears as a separate event.
     *  
     *  In effective mode, events are returned that are currently
     *  effective.  In any effective mode, effective events and those
     *  currently expired are returned.
     *  
     *  In sequestered mode, no sequestered events are returned. In
     *  unsequestered mode, all events are returned.
     *
     *  @param  from start date 
     *  @param  to end date 
     *  @return the returned {@code Event} list 
     *  @throws org.osid.InvalidArgumentException {@code to} is less 
     *          than {@code from} 
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.calendaring.EventList getEventsInDateRange(org.osid.calendaring.DateTime from, 
                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEventsInDateRange(from, to));
    }


    /**
     *  Gets the next upcoming events on this calendar. 
     *  
     *  In plenary mode, the returned list contains all known events
     *  or an error results. Otherwise, the returned list may contain
     *  only those events that are accessible through this session.
     *  
     *  In normalized mode, recurring events appear as a single
     *  event. In denormalized mode, each element in the recurring
     *  series appears as a separate event.
     *  
     *  In effective mode, events are returned that are currently
     *  effective.  In any effective mode, effective events and those
     *  currently expired are returned.
     *  
     *  In sequestered mode, no sequestered events are returned. In 
     *  unsequestered mode, all events are returned. 
     *
     *  @param  number the number of events 
     *  @return the returned {@code Event} list 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.calendaring.EventList getUpcomingEvents(long number)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getUpcomingEvents(number));
    }


    /**
     *  Gets a list of events with the given location.
     *  
     *  In plenary mode, the returned list contains all known events
     *  or an error results. Otherwise, the returned list may contain
     *  only those events that are accessible through this session.
     *  
     *  In normalized mode, recurring events appear as a single event. In 
     *  denormalized mode, each element in the recurring series appears as a 
     *  separate event. 
     *  
     *  In effective mode, events are returned that are currently
     *  effective.  In any effective mode, effective events and those
     *  currently expired are returned.
     *  
     *  In sequestered mode, no sequestered events are returned. In
     *  unsequestered mode, all events are returned.
     *
     *  @param  locationId a location 
     *  @return the returned {@code Event} list 
     *  @throws org.osid.NullArgumentException {@code locationId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.calendaring.EventList getEventsByLocation(org.osid.id.Id locationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEventsByLocation(locationId));
    }


    /**
     *  Gets an {@code EventList} at the given location where
     *  the given date range falls within the event dates inclusive.
     *  
     *  In plenary mode, the returned list contains all known events
     *  or an error results. Otherwise, the returned list may contain
     *  only those events that are accessible through this session.
     *  
     *  In normalized mode, recurring events appear as a single
     *  event. In denormalized mode, each element in the recurring
     *  series appears as a separate event.
     *  
     *  In effective mode, events are returned that are currently
     *  effective.  In any effective mode, effective events and those
     *  currently expired are returned.
     *  
     *  In sequestered mode, no sequestered events are returned. In
     *  unsequestered mode, all events are returned.
     *
     *  @param  locationId a location 
     *  @param  from start date 
     *  @param  to end date 
     *  @return the returned {@code Event} list 
     *  @throws org.osid.InvalidArgumentException {@code to} is less 
     *          than {@code from} 
     *  @throws org.osid.NullArgumentException {@code locationId,
     *          from}, or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.calendaring.EventList getEventsByLocationOnDate(org.osid.id.Id locationId, 
                                                                    org.osid.calendaring.DateTime from, 
                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEventsByLocationOnDate(locationId, from, to));
    }        


    /**
     *  Gets an {@code EventList} that fall within the given
     *  range inclusive at the given location.
     *  
     *  In plenary mode, the returned list contains all known events
     *  or an error results. Otherwise, the returned list may contain
     *  only those events that are accessible through this session.
     *  
     *  In normalized mode, recurring events appear as a single
     *  event. In denormalized mode, each element in the recurring
     *  series appears as a separate event.
     *  
     *  In effective mode, events are returned that are currently
     *  effective.  In any effective mode, effective events and those
     *  currently expired are returned.
     *  
     *  In sequestered mode, no sequestered events are returned. In
     *  unsequestered mode, all events are returned.
     *
     *  @param  locationId a location 
     *  @param  from start date 
     *  @param  to end date 
     *  @return the returned {@code Event} list 
     *  @throws org.osid.InvalidArgumentException {@code to} is less 
     *          than {@code from} 
     *  @throws org.osid.NullArgumentException {@code locationId,
     *          from}, or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    public org.osid.calendaring.EventList getEventsByLocationInDateRange(org.osid.id.Id locationId, 
                                                                         org.osid.calendaring.DateTime from, 
                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEventsByLocationInDateRange(locationId, from, to));
    }


    /**
     *  Gets a list of events with the given sponsor.
     *  
     *  In plenary mode, the returned list contains all known events
     *  or an error results. Otherwise, the returned list may contain
     *  only those events that are accessible through this session.
     *  
     *  In normalized mode, recurring events appear as a single
     *  event. In denormalized mode, each element in the recurring
     *  series appears as a separate event.
     *  
     *  In effective mode, events are returned that are currently
     *  effective.  In any effective mode, effective events and those
     *  currently expired are returned.
     *  
     *  In sequestered mode, no sequestered events are returned. In
     *  unsequestered mode, all events are returned.
     *
     *  @param  resourceId a sponsor 
     *  @return the returned {@code Event} list 
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    public org.osid.calendaring.EventList getEventsBySponsor(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEventsBySponsor(resourceId));
    }


    /**
     *  Gets an {@code EventList} with the given sponsor where
     *  the given date range falls within the event dates inclusive.
     *  
     *  In plenary mode, the returned list contains all known events
     *  or an error results. Otherwise, the returned list may contain
     *  only those events that are accessible through this session.
     *  
     *  In normalized mode, recurring events appear as a single
     *  event. In denormalized mode, each element in the recurring
     *  series appears as a separate event.
     *  
     *  In effective mode, events are returned that are currently
     *  effective.  In any effective mode, effective events and those
     *  currently expired are returned.
     *  
     *  In sequestered mode, no sequestered events are returned. In
     *  unsequestered mode, all events are returned.
     *
     *  @param  resourceId a sponsor 
     *  @param  from start date 
     *  @param  to end date 
     *  @return the returned {@code Event} list 
     *  @throws org.osid.InvalidArgumentException {@code to} is less 
     *          than {@code from} 
     *  @throws org.osid.NullArgumentException {@code resourceId,
     *          from}, or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.calendaring.EventList getEventsBySponsorOnDate(org.osid.id.Id resourceId, 
                                                                   org.osid.calendaring.DateTime from, 
                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEventsBySponsorOnDate(resourceId, from, to));
    }


    /**
     *  Gets an {@code EventList} that fall within the given
     *  range inclusive at the given location.
     *  
     *  In plenary mode, the returned list contains all known events
     *  or an error results. Otherwise, the returned list may contain
     *  only those events that are accessible through this session.
     *  
     *  In normalized mode, recurring events appear as a single
     *  event. In denormalized mode, each element in the recurring
     *  series appears as a separate event.
     *  
     *  In effective mode, events are returned that are currently
     *  effective.  In any effective mode, effective events and those
     *  currently expired are returned.
     *  
     *  In sequestered mode, no sequestered events are returned. In
     *  unsequestered mode, all events are returned.
     *
     *  @param  resourceId a sponsor 
     *  @param  from start date 
     *  @param  to end date 
     *  @return the returned {@code Event} list 
     *  @throws org.osid.InvalidArgumentException {@code to} is less 
     *          than {@code from} 
     *  @throws org.osid.NullArgumentException {@code resourceId,
     *         from}, or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.calendaring.EventList getEventsBySponsorInDateRange(org.osid.id.Id resourceId, 
                                                                        org.osid.calendaring.DateTime from, 
                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEventsBySponsorInDateRange(resourceId, from, to));
    }


    /**
     *  Gets all {@code Events}. 
     *
     *  In plenary mode, the returned list contains all known
     *  events or an error results. Otherwise, the returned list
     *  may contain only those events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, events are returned that are currently
     *  effective.  In any effective mode, effective events and
     *  those currently expired are returned.
     *
     *  @return a list of {@code Events} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.EventList getEvents()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEvents());
    }
}
