//
// AbstractAssemblyPersonQuery.java
//
//     A PersonQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.personnel.person.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A PersonQuery that stores terms.
 */

public abstract class AbstractAssemblyPersonQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.personnel.PersonQuery,
               org.osid.personnel.PersonQueryInspector,
               org.osid.personnel.PersonSearchOrder {

    private final java.util.Collection<org.osid.personnel.records.PersonQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.personnel.records.PersonQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.personnel.records.PersonSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyPersonQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyPersonQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches a salutation. 
     *
     *  @param  salutation a salutation 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> salutation </code> is 
     *          not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> salutation </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchSalutation(String salutation, 
                                org.osid.type.Type stringMatchType, 
                                boolean match) {
        getAssembler().addStringTerm(getSalutationColumn(), salutation, stringMatchType, match);
        return;
    }


    /**
     *  Matches persons with any salutation. 
     *
     *  @param  match <code> true </code> to match persons with any 
     *          salutation, <code> false </code> to match persons with no 
     *          salutation 
     */

    @OSID @Override
    public void matchAnySalutation(boolean match) {
        getAssembler().addStringWildcardTerm(getSalutationColumn(), match);
        return;
    }


    /**
     *  Clears all salutation terms. 
     */

    @OSID @Override
    public void clearSalutationTerms() {
        getAssembler().clearTerms(getSalutationColumn());
        return;
    }


    /**
     *  Gets the salutation query terms. 
     *
     *  @return the salutation terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getSalutationTerms() {
        return (getAssembler().getStringTerms(getSalutationColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the salutation. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySalutation(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getSalutationColumn(), style);
        return;
    }


    /**
     *  Gets the Salutation column name.
     *
     * @return the column name
     */

    protected String getSalutationColumn() {
        return ("salutation");
    }


    /**
     *  Matches a given name. 
     *
     *  @param  givenName a given name 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> givenName </code> is 
     *          not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> givenName </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchGivenName(String givenName, 
                               org.osid.type.Type stringMatchType, 
                               boolean match) {
        getAssembler().addStringTerm(getGivenNameColumn(), givenName, stringMatchType, match);
        return;
    }


    /**
     *  Matches persons with any given name. 
     *
     *  @param  match <code> true </code> to match persons with any given 
     *          name, <code> false </code> to match persons with no given name 
     */

    @OSID @Override
    public void matchAnyGivenName(boolean match) {
        getAssembler().addStringWildcardTerm(getGivenNameColumn(), match);
        return;
    }


    /**
     *  Clears all given name terms. 
     */

    @OSID @Override
    public void clearGivenNameTerms() {
        getAssembler().clearTerms(getGivenNameColumn());
        return;
    }


    /**
     *  Gets the given name query terms. 
     *
     *  @return the given name terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getGivenNameTerms() {
        return (getAssembler().getStringTerms(getGivenNameColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the given name. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByGivenName(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getGivenNameColumn(), style);
        return;
    }


    /**
     *  Gets the GivenName column name.
     *
     * @return the column name
     */

    protected String getGivenNameColumn() {
        return ("given_name");
    }


    /**
     *  Matches a preferred name. 
     *
     *  @param  preferredName a preferred name 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> preferredName </code> 
     *          is not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> preferredName </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchPreferredName(String preferredName, 
                                   org.osid.type.Type stringMatchType, 
                                   boolean match) {
        getAssembler().addStringTerm(getPreferredNameColumn(), preferredName, stringMatchType, match);
        return;
    }


    /**
     *  Matches persons with any preferred name. 
     *
     *  @param  match <code> true </code> to match persons with any preferred 
     *          name, <code> false </code> to match persons with no preferred 
     *          name 
     */

    @OSID @Override
    public void matchAnyPreferredName(boolean match) {
        getAssembler().addStringWildcardTerm(getPreferredNameColumn(), match);
        return;
    }


    /**
     *  Clears all preferred name terms. 
     */

    @OSID @Override
    public void clearPreferredNameTerms() {
        getAssembler().clearTerms(getPreferredNameColumn());
        return;
    }


    /**
     *  Gets the preferred name query terms. 
     *
     *  @return the preferred name terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getPreferredNameTerms() {
        return (getAssembler().getStringTerms(getPreferredNameColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the
     *  preferred name.
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPreferredName(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getPreferredNameColumn(), style);
        return;
    }


    /**
     *  Gets the PreferredName column name.
     *
     * @return the column name
     */

    protected String getPreferredNameColumn() {
        return ("preferred_name");
    }


    /**
     *  Matches a forename alias. 
     *
     *  @param  forenameAlias a forename alias 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> forenameAlias </code> 
     *          is not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> forenameAlias </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchForenameAlias(String forenameAlias, 
                                   org.osid.type.Type stringMatchType, 
                                   boolean match) {
        getAssembler().addStringTerm(getForenameAliasColumn(), forenameAlias, stringMatchType, match);
        return;
    }


    /**
     *  Matches persons with any forename alias. 
     *
     *  @param  match <code> true </code> to match persons with any forename 
     *          alias, <code> false </code> to match persons with no forename 
     *          aliases 
     */

    @OSID @Override
    public void matchAnyForenameAlias(boolean match) {
        getAssembler().addStringWildcardTerm(getForenameAliasColumn(), match);
        return;
    }


    /**
     *  Clears all forename alias terms. 
     */

    @OSID @Override
    public void clearForenameAliasTerms() {
        getAssembler().clearTerms(getForenameAliasColumn());
        return;
    }


    /**
     *  Gets the forename alias query terms. 
     *
     *  @return the forename alias terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getForenameAliasTerms() {
        return (getAssembler().getStringTerms(getForenameAliasColumn()));
    }


    /**
     *  Gets the ForenameAlias column name.
     *
     * @return the column name
     */

    protected String getForenameAliasColumn() {
        return ("forename_alias");
    }


    /**
     *  Matches a middle name. 
     *
     *  @param  middleName a middle name 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> middleName </code> is 
     *          not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> middleName </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchMiddleName(String middleName, 
                                org.osid.type.Type stringMatchType, 
                                boolean match) {
        getAssembler().addStringTerm(getMiddleNameColumn(), middleName, stringMatchType, match);
        return;
    }


    /**
     *  Matches persons with any middle name. 
     *
     *  @param  match <code> true </code> to match persons with any middle 
     *          name, <code> false </code> to match persons with no middle 
     *          name 
     */

    @OSID @Override
    public void matchAnyMiddleName(boolean match) {
        getAssembler().addStringWildcardTerm(getMiddleNameColumn(), match);
        return;
    }


    /**
     *  Clears all middle name terms. 
     */

    @OSID @Override
    public void clearMiddleNameTerms() {
        getAssembler().clearTerms(getMiddleNameColumn());
        return;
    }


    /**
     *  Gets the middle name query terms. 
     *
     *  @return the middle name terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getMiddleNameTerms() {
        return (getAssembler().getStringTerms(getMiddleNameColumn()));
    }


    /**
     *  Gets the MiddleName column name.
     *
     * @return the column name
     */

    protected String getMiddleNameColumn() {
        return ("middle_name");
    }


    /**
     *  Matches a surname. 
     *
     *  @param  surName a surname 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> surName </code> is 
     *          not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> surName </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchSurname(String surName, 
                             org.osid.type.Type stringMatchType, boolean match) {
        getAssembler().addStringTerm(getSurnameColumn(), surName, stringMatchType, match);
        return;
    }


    /**
     *  Matches persons with any surname. 
     *
     *  @param  match <code> true </code> to match persons with any surname, 
     *          <code> false </code> to match persons with no surname 
     */

    @OSID @Override
    public void matchAnySurname(boolean match) {
        getAssembler().addStringWildcardTerm(getSurnameColumn(), match);
        return;
    }


    /**
     *  Clears all surname terms. 
     */

    @OSID @Override
    public void clearSurnameTerms() {
        getAssembler().clearTerms(getSurnameColumn());
        return;
    }


    /**
     *  Gets the surname query terms. 
     *
     *  @return the surname terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getSurnameTerms() {
        return (getAssembler().getStringTerms(getSurnameColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the surname. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySurname(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getSurnameColumn(), style);
        return;
    }


    /**
     *  Gets the Surname column name.
     *
     * @return the column name
     */

    protected String getSurnameColumn() {
        return ("surname");
    }


    /**
     *  Matches a surname alias. 
     *
     *  @param  surnameAlias a surname alias 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> surnameAlias </code> 
     *          is not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> surnameAlias </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchSurnameAlias(String surnameAlias, 
                                  org.osid.type.Type stringMatchType, 
                                  boolean match) {
        getAssembler().addStringTerm(getSurnameAliasColumn(), surnameAlias, stringMatchType, match);
        return;
    }


    /**
     *  Matches persons with any surname alias. 
     *
     *  @param  match <code> true </code> to match persons with any surname 
     *          alias, <code> false </code> to match persons with no surname 
     *          alias 
     */

    @OSID @Override
    public void matchAnySurnameAlias(boolean match) {
        getAssembler().addStringWildcardTerm(getSurnameAliasColumn(), match);
        return;
    }


    /**
     *  Clears all surname alias terms. 
     */

    @OSID @Override
    public void clearSurnameAliasTerms() {
        getAssembler().clearTerms(getSurnameAliasColumn());
        return;
    }


    /**
     *  Gets the surname alias query terms. 
     *
     *  @return the surname alias terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getSurnameAliasTerms() {
        return (getAssembler().getStringTerms(getSurnameAliasColumn()));
    }


    /**
     *  Gets the SurnameAlias column name.
     *
     * @return the column name
     */

    protected String getSurnameAliasColumn() {
        return ("surname_alias");
    }


    /**
     *  Matches a generation qualifier. 
     *
     *  @param  generationQualifier a generation qualifier 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> generationQualifier 
     *          </code> is not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> generationQualifier 
     *          </code> or <code> stringMatchType </code> is <code> null 
     *          </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchGenerationQualifier(String generationQualifier, 
                                         org.osid.type.Type stringMatchType, 
                                         boolean match) {
        getAssembler().addStringTerm(getGenerationQualifierColumn(), generationQualifier, stringMatchType, match);
        return;
    }


    /**
     *  Matches persons with any generation qualifier. 
     *
     *  @param  match <code> true </code> to match persons with any generation 
     *          qualifier, <code> false </code> to match persons with no 
     *          generation qualifier 
     */

    @OSID @Override
    public void matchAnyGenerationQualifier(boolean match) {
        getAssembler().addStringWildcardTerm(getGenerationQualifierColumn(), match);
        return;
    }


    /**
     *  Clears all generation qualifier terms. 
     */

    @OSID @Override
    public void clearGenerationQualifierTerms() {
        getAssembler().clearTerms(getGenerationQualifierColumn());
        return;
    }


    /**
     *  Gets the generation qualifier query terms. 
     *
     *  @return the generation qualifier terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getGenerationQualifierTerms() {
        return (getAssembler().getStringTerms(getGenerationQualifierColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the generation 
     *  qualifier. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByGenerationQualifier(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getGenerationQualifierColumn(), style);
        return;
    }


    /**
     *  Gets the GenerationQualifier column name.
     *
     * @return the column name
     */

    protected String getGenerationQualifierColumn() {
        return ("generation_qualifier");
    }


    /**
     *  Matches a qualification suffix. 
     *
     *  @param  qualificationSuffix a qualification suffix 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> qualificationSuffix 
     *          </code> is not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> qualificationSuffix 
     *          </code> or <code> stringMatchType </code> is <code> null 
     *          </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchQualificationSuffix(String qualificationSuffix, 
                                         org.osid.type.Type stringMatchType, 
                                         boolean match) {
        getAssembler().addStringTerm(getQualificationSuffixColumn(), qualificationSuffix, stringMatchType, match);
        return;
    }


    /**
     *  Matches persons with any qualification suffix. 
     *
     *  @param  match <code> true </code> to match persons with any 
     *          qualification sufix, <code> false </code> to match persons 
     *          with no qualification suffix 
     */

    @OSID @Override
    public void matchAnyQualificationSuffix(boolean match) {
        getAssembler().addStringWildcardTerm(getQualificationSuffixColumn(), match);
        return;
    }


    /**
     *  Clears all qualification suffix terms. 
     */

    @OSID @Override
    public void clearQualificationSuffixTerms() {
        getAssembler().clearTerms(getQualificationSuffixColumn());
        return;
    }


    /**
     *  Gets the qualifier suffix query terms. 
     *
     *  @return the qualifier suffix terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getQualificationSuffixTerms() {
        return (getAssembler().getStringTerms(getQualificationSuffixColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the 
     *  qualification suffix. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByQualificationSuffix(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getQualificationSuffixColumn(), style);
        return;
    }


    /**
     *  Gets the QualificationSuffix column name.
     *
     * @return the column name
     */

    protected String getQualificationSuffixColumn() {
        return ("qualification_suffix");
    }


    /**
     *  Matches a birth date bwteen the given range inclusive. 
     *
     *  @param  from a starting date 
     *  @param  to an ending date 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchBirthDate(org.osid.calendaring.DateTime from, 
                               org.osid.calendaring.DateTime to, boolean match) {
        getAssembler().addDateTimeRangeTerm(getBirthDateColumn(), from, to, match);
        return;
    }


    /**
     *  Matches persons with any birth date. 
     *
     *  @param  match <code> true </code> to match persons with any date of 
     *          birth, <code> false </code> to match persons with no date of 
     *          birth 
     */

    @OSID @Override
    public void matchAnyBirthDate(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getBirthDateColumn(), match);
        return;
    }


    /**
     *  Clears all birth date terms. 
     */

    @OSID @Override
    public void clearBirthDateTerms() {
        getAssembler().clearTerms(getBirthDateColumn());
        return;
    }


    /**
     *  Gets the birth date query terms. 
     *
     *  @return the birth date terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getBirthDateTerms() {
        return (getAssembler().getDateTimeRangeTerms(getBirthDateColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the birth date. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByBirthDate(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getBirthDateColumn(), style);
        return;
    }


    /**
     *  Gets the BirthDate column name.
     *
     * @return the column name
     */

    protected String getBirthDateColumn() {
        return ("birth_date");
    }


    /**
     *  Matches a death date between the given range inclusive. 
     *
     *  @param  from a starting date 
     *  @param  to an ending date 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchDeathDate(org.osid.calendaring.DateTime from, 
                               org.osid.calendaring.DateTime to, boolean match) {
        getAssembler().addDateTimeRangeTerm(getDeathDateColumn(), from, to, match);
        return;
    }


    /**
     *  Matches persons with any death date. 
     *
     *  @param  match <code> true </code> to match persons with any date of 
     *          death, <code> false </code> to match persons who might still 
     *          be alive 
     */

    @OSID @Override
    public void matchAnyDeathDate(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getDeathDateColumn(), match);
        return;
    }


    /**
     *  Clears all death date terms. 
     */

    @OSID @Override
    public void clearDeathDateTerms() {
        getAssembler().clearTerms(getDeathDateColumn());
        return;
    }


    /**
     *  Gets the death date query terms. 
     *
     *  @return the death date terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getDeathDateTerms() {
        return (getAssembler().getDateTimeRangeTerms(getDeathDateColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the death date. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDeathDate(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDeathDateColumn(), style);
        return;
    }


    /**
     *  Gets the DeathDate column name.
     *
     * @return the column name
     */

    protected String getDeathDateColumn() {
        return ("death_date");
    }


    /**
     *  Matches a persons alive within the given dates inclusive. 
     *
     *  @param  from a starting date 
     *  @param  to an ending date 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchLivingDate(org.osid.calendaring.DateTime from, 
                                org.osid.calendaring.DateTime to, 
                                boolean match) {
        getAssembler().addDateTimeRangeTerm(getLivingDateColumn(), from, to, match);
        return;
    }


    /**
     *  Clears all living date terms. 
     */

    @OSID @Override
    public void clearLivingDateTerms() {
        getAssembler().clearTerms(getLivingDateColumn());
        return;
    }


    /**
     *  Gets the living date query terms. 
     *
     *  @return the living date terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getLivingDateTerms() {
        return (getAssembler().getDateTimeRangeTerms(getLivingDateColumn()));
    }


    /**
     *  Gets the LivingDate column name.
     *
     * @return the column name
     */

    protected String getLivingDateColumn() {
        return ("living_date");
    }


    /**
     *  Matches an institutional identifier. 
     *
     *  @param  identifier an institutional identifier 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> identifier </code> is 
     *          not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> identifier </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchInstitutionalIdentifier(String identifier, 
                                             org.osid.type.Type stringMatchType, 
                                             boolean match) {
        getAssembler().addStringTerm(getInstitutionalIdentifierColumn(), identifier, stringMatchType, match);
        return;
    }


    /**
     *  Matches persons with any institutional identifier. 
     *
     *  @param  match <code> true </code> to match persons with any 
     *          institutional identifier, <code> false </code> to match 
     *          persons with no instituional identifier 
     */

    @OSID @Override
    public void matchAnyInstitutionalIdentifier(boolean match) {
        getAssembler().addStringWildcardTerm(getInstitutionalIdentifierColumn(), match);
        return;
    }


    /**
     *  Clears all institutional identifier terms. 
     */

    @OSID @Override
    public void clearInstitutionalIdentifierTerms() {
        getAssembler().clearTerms(getInstitutionalIdentifierColumn());
        return;
    }


    /**
     *  Gets the identifier query terms. 
     *
     *  @return the identifier terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getInstitutionalIdentifierTerms() {
        return (getAssembler().getStringTerms(getInstitutionalIdentifierColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the 
     *  institutional identifier. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByInstitutionalIdentifier(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getInstitutionalIdentifierColumn(), style);
        return;
    }


    /**
     *  Gets the InstitutionalIdentifier column name.
     *
     * @return the column name
     */

    protected String getInstitutionalIdentifierColumn() {
        return ("institutional_identifier");
    }


    /**
     *  Sets the realm <code> Id </code> for this query to match persons 
     *  assigned to realms. 
     *
     *  @param  realmId a realm <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> realmId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRealmId(org.osid.id.Id realmId, boolean match) {
        getAssembler().addIdTerm(getRealmIdColumn(), realmId, match);
        return;
    }


    /**
     *  Clears all realm <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRealmIdTerms() {
        getAssembler().clearTerms(getRealmIdColumn());
        return;
    }


    /**
     *  Gets the realm <code> Id </code> query terms. 
     *
     *  @return the realm <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRealmIdTerms() {
        return (getAssembler().getIdTerms(getRealmIdColumn()));
    }


    /**
     *  Gets the RealmId column name.
     *
     * @return the column name
     */

    protected String getRealmIdColumn() {
        return ("realm_id");
    }


    /**
     *  Tests if a <code> RealmQuery </code> is available. 
     *
     *  @return <code> true </code> if a realm query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRealmQuery() {
        return (false);
    }


    /**
     *  Gets the query for a realm query. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the realm query 
     *  @throws org.osid.UnimplementedException <code> supportsRealmQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.RealmQuery getRealmQuery() {
        throw new org.osid.UnimplementedException("supportsRealmQuery() is false");
    }


    /**
     *  Clears all realm terms. 
     */

    @OSID @Override
    public void clearRealmTerms() {
        getAssembler().clearTerms(getRealmColumn());
        return;
    }


    /**
     *  Gets the realm query terms. 
     *
     *  @return the realm terms 
     */

    @OSID @Override
    public org.osid.personnel.RealmQueryInspector[] getRealmTerms() {
        return (new org.osid.personnel.RealmQueryInspector[0]);
    }


    /**
     *  Gets the Realm column name.
     *
     * @return the column name
     */

    protected String getRealmColumn() {
        return ("realm");
    }


    /**
     *  Tests if this person supports the given record
     *  <code>Type</code>.
     *
     *  @param  personRecordType a person record type 
     *  @return <code>true</code> if the personRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>personRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type personRecordType) {
        for (org.osid.personnel.records.PersonQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(personRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  personRecordType the person record type 
     *  @return the person query record 
     *  @throws org.osid.NullArgumentException
     *          <code>personRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(personRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.personnel.records.PersonQueryRecord getPersonQueryRecord(org.osid.type.Type personRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.personnel.records.PersonQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(personRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(personRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  personRecordType the person record type 
     *  @return the person query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>personRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(personRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.personnel.records.PersonQueryInspectorRecord getPersonQueryInspectorRecord(org.osid.type.Type personRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.personnel.records.PersonQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(personRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(personRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param personRecordType the person record type
     *  @return the person search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>personRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(personRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.personnel.records.PersonSearchOrderRecord getPersonSearchOrderRecord(org.osid.type.Type personRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.personnel.records.PersonSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(personRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(personRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this person. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param personQueryRecord the person query record
     *  @param personQueryInspectorRecord the person query inspector
     *         record
     *  @param personSearchOrderRecord the person search order record
     *  @param personRecordType person record type
     *  @throws org.osid.NullArgumentException
     *          <code>personQueryRecord</code>,
     *          <code>personQueryInspectorRecord</code>,
     *          <code>personSearchOrderRecord</code> or
     *          <code>personRecordTypeperson</code> is
     *          <code>null</code>
     */
            
    protected void addPersonRecords(org.osid.personnel.records.PersonQueryRecord personQueryRecord, 
                                      org.osid.personnel.records.PersonQueryInspectorRecord personQueryInspectorRecord, 
                                      org.osid.personnel.records.PersonSearchOrderRecord personSearchOrderRecord, 
                                      org.osid.type.Type personRecordType) {

        addRecordType(personRecordType);

        nullarg(personQueryRecord, "person query record");
        nullarg(personQueryInspectorRecord, "person query inspector record");
        nullarg(personSearchOrderRecord, "person search odrer record");

        this.queryRecords.add(personQueryRecord);
        this.queryInspectorRecords.add(personQueryInspectorRecord);
        this.searchOrderRecords.add(personSearchOrderRecord);
        
        return;
    }
}
