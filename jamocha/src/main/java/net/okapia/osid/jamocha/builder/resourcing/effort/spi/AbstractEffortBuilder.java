//
// AbstractEffort.java
//
//     Defines an Effort builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.resourcing.effort.spi;


/**
 *  Defines an <code>Effort</code> builder.
 */

public abstract class AbstractEffortBuilder<T extends AbstractEffortBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRelationshipBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.resourcing.effort.EffortMiter effort;


    /**
     *  Constructs a new <code>AbstractEffortBuilder</code>.
     *
     *  @param effort the effort to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractEffortBuilder(net.okapia.osid.jamocha.builder.resourcing.effort.EffortMiter effort) {
        super(effort);
        this.effort = effort;
        return;
    }


    /**
     *  Builds the effort.
     *
     *  @return the new effort
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.resourcing.Effort build() {
        (new net.okapia.osid.jamocha.builder.validator.resourcing.effort.EffortValidator(getValidations())).validate(this.effort);
        return (new net.okapia.osid.jamocha.builder.resourcing.effort.ImmutableEffort(this.effort));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the effort miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.resourcing.effort.EffortMiter getMiter() {
        return (this.effort);
    }


    /**
     *  Sets the resource.
     *
     *  @param resource a resource
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    public T resource(org.osid.resource.Resource resource) {
        getMiter().setResource(resource);
        return (self());
    }


    /**
     *  Sets the commission.
     *
     *  @param commission a commission
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>commission</code>
     *          is <code>null</code>
     */

    public T commission(org.osid.resourcing.Commission commission) {
        getMiter().setCommission(commission);
        return (self());
    }


    /**
     *  Sets the time spent.
     *
     *  @param timeSpent a time spent
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>timeSpent</code>
     *          is <code>null</code>
     */

    public T timeSpent(org.osid.calendaring.Duration timeSpent) {
        getMiter().setTimeSpent(timeSpent);
        return (self());
    }


    /**
     *  Adds an Effort record.
     *
     *  @param record an effort record
     *  @param recordType the type of effort record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.resourcing.records.EffortRecord record, org.osid.type.Type recordType) {
        getMiter().addEffortRecord(record, recordType);
        return (self());
    }
}       


