//
// AbstractIndexedMapParameterProcessorEnablerLookupSession.java
//
//    A simple framework for providing a ParameterProcessorEnabler lookup service
//    backed by a fixed collection of parameter processor enablers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.configuration.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a ParameterProcessorEnabler lookup service backed by a
 *  fixed collection of parameter processor enablers. The parameter processor enablers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some parameter processor enablers may be compatible
 *  with more types than are indicated through these parameter processor enabler
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>ParameterProcessorEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapParameterProcessorEnablerLookupSession
    extends AbstractMapParameterProcessorEnablerLookupSession
    implements org.osid.configuration.rules.ParameterProcessorEnablerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.configuration.rules.ParameterProcessorEnabler> parameterProcessorEnablersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.configuration.rules.ParameterProcessorEnabler>());
    private final MultiMap<org.osid.type.Type, org.osid.configuration.rules.ParameterProcessorEnabler> parameterProcessorEnablersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.configuration.rules.ParameterProcessorEnabler>());


    /**
     *  Makes a <code>ParameterProcessorEnabler</code> available in this session.
     *
     *  @param  parameterProcessorEnabler a parameter processor enabler
     *  @throws org.osid.NullArgumentException <code>parameterProcessorEnabler<code> is
     *          <code>null</code>
     */

    @Override
    protected void putParameterProcessorEnabler(org.osid.configuration.rules.ParameterProcessorEnabler parameterProcessorEnabler) {
        super.putParameterProcessorEnabler(parameterProcessorEnabler);

        this.parameterProcessorEnablersByGenus.put(parameterProcessorEnabler.getGenusType(), parameterProcessorEnabler);
        
        try (org.osid.type.TypeList types = parameterProcessorEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.parameterProcessorEnablersByRecord.put(types.getNextType(), parameterProcessorEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a parameter processor enabler from this session.
     *
     *  @param parameterProcessorEnablerId the <code>Id</code> of the parameter processor enabler
     *  @throws org.osid.NullArgumentException <code>parameterProcessorEnablerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeParameterProcessorEnabler(org.osid.id.Id parameterProcessorEnablerId) {
        org.osid.configuration.rules.ParameterProcessorEnabler parameterProcessorEnabler;
        try {
            parameterProcessorEnabler = getParameterProcessorEnabler(parameterProcessorEnablerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.parameterProcessorEnablersByGenus.remove(parameterProcessorEnabler.getGenusType());

        try (org.osid.type.TypeList types = parameterProcessorEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.parameterProcessorEnablersByRecord.remove(types.getNextType(), parameterProcessorEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeParameterProcessorEnabler(parameterProcessorEnablerId);
        return;
    }


    /**
     *  Gets a <code>ParameterProcessorEnablerList</code> corresponding to the given
     *  parameter processor enabler genus <code>Type</code> which does not include
     *  parameter processor enablers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known parameter processor enablers or an error results. Otherwise,
     *  the returned list may contain only those parameter processor enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  parameterProcessorEnablerGenusType a parameter processor enabler genus type 
     *  @return the returned <code>ParameterProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>parameterProcessorEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerList getParameterProcessorEnablersByGenusType(org.osid.type.Type parameterProcessorEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.configuration.rules.parameterprocessorenabler.ArrayParameterProcessorEnablerList(this.parameterProcessorEnablersByGenus.get(parameterProcessorEnablerGenusType)));
    }


    /**
     *  Gets a <code>ParameterProcessorEnablerList</code> containing the given
     *  parameter processor enabler record <code>Type</code>. In plenary mode, the
     *  returned list contains all known parameter processor enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  parameter processor enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  parameterProcessorEnablerRecordType a parameter processor enabler record type 
     *  @return the returned <code>parameterProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>parameterProcessorEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerList getParameterProcessorEnablersByRecordType(org.osid.type.Type parameterProcessorEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.configuration.rules.parameterprocessorenabler.ArrayParameterProcessorEnablerList(this.parameterProcessorEnablersByRecord.get(parameterProcessorEnablerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.parameterProcessorEnablersByGenus.clear();
        this.parameterProcessorEnablersByRecord.clear();

        super.close();

        return;
    }
}
