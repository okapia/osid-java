//
// AbstractActionGroupSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.actiongroup.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractActionGroupSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.control.ActionGroupSearchResults {

    private org.osid.control.ActionGroupList actionGroups;
    private final org.osid.control.ActionGroupQueryInspector inspector;
    private final java.util.Collection<org.osid.control.records.ActionGroupSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractActionGroupSearchResults.
     *
     *  @param actionGroups the result set
     *  @param actionGroupQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>actionGroups</code>
     *          or <code>actionGroupQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractActionGroupSearchResults(org.osid.control.ActionGroupList actionGroups,
                                            org.osid.control.ActionGroupQueryInspector actionGroupQueryInspector) {
        nullarg(actionGroups, "action groups");
        nullarg(actionGroupQueryInspector, "action group query inspectpr");

        this.actionGroups = actionGroups;
        this.inspector = actionGroupQueryInspector;

        return;
    }


    /**
     *  Gets the action group list resulting from a search.
     *
     *  @return an action group list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.control.ActionGroupList getActionGroups() {
        if (this.actionGroups == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.control.ActionGroupList actionGroups = this.actionGroups;
        this.actionGroups = null;
	return (actionGroups);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.control.ActionGroupQueryInspector getActionGroupQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  action group search record <code> Type. </code> This method must
     *  be used to retrieve an actionGroup implementing the requested
     *  record.
     *
     *  @param actionGroupSearchRecordType an actionGroup search 
     *         record type 
     *  @return the action group search
     *  @throws org.osid.NullArgumentException
     *          <code>actionGroupSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(actionGroupSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.ActionGroupSearchResultsRecord getActionGroupSearchResultsRecord(org.osid.type.Type actionGroupSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.control.records.ActionGroupSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(actionGroupSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(actionGroupSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record action group search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addActionGroupRecord(org.osid.control.records.ActionGroupSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "action group record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
