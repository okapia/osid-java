//
// AbstractPlan.java
//
//     Defines a Plan.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.plan.plan.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Plan</code>.
 */

public abstract class AbstractPlan
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.course.plan.Plan {

    private org.osid.course.syllabus.Syllabus syllabus;
    private org.osid.course.CourseOffering courseOffering;

    private final java.util.Collection<org.osid.course.syllabus.Module> modules = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.plan.records.PlanRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the syllabus. 
     *
     *  @return the syllabus <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getSyllabusId() {
        return (this.syllabus.getId());
    }


    /**
     *  Gets the syllabus. 
     *
     *  @return the syllabus 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.syllabus.Syllabus getSyllabus()
        throws org.osid.OperationFailedException {

        return (this.syllabus);
    }


    /**
     *  Sets the syllabus.
     *
     *  @param syllabus a syllabus
     *  @throws org.osid.NullArgumentException <code>syllabus</code>
     *          is <code>null</code>
     */

    protected void setSyllabus(org.osid.course.syllabus.Syllabus syllabus) {
        nullarg(syllabus, "syllabus");
        this.syllabus = syllabus;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the course offering. 
     *
     *  @return the course offering <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCourseOfferingId() {
        return (this.courseOffering.getId());
    }


    /**
     *  Gets the course offering. 
     *
     *  @return the course offering 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.CourseOffering getCourseOffering()
        throws org.osid.OperationFailedException {

        return (this.courseOffering);
    }


    /**
     *  Sets the course offering.
     *
     *  @param courseOffering a course offering
     *  @throws org.osid.NullArgumentException
     *          <code>courseOffering</code> is <code>null</code>
     */

    protected void setCourseOffering(org.osid.course.CourseOffering courseOffering) {
        nullarg(courseOffering, "course offering");
        this.courseOffering = courseOffering;
        return;
    }


    /**
     *  Gets the modules to apply to this plan. 
     *
     *  @return the module <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getModuleIds() {
        try {
            org.osid.course.syllabus.ModuleList modules = getModules();
            return (new net.okapia.osid.jamocha.adapter.converter.course.syllabus.module.ModuleToIdList(modules));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the modules to apply to this plan. 
     *
     *  @return the module 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleList getModules()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.course.syllabus.module.ArrayModuleList(this.modules));
    }


    /**
     *  Adds a module.
     *
     *  @param module a module
     *  @throws org.osid.NullArgumentException <code>module</code> is
     *          <code>null</code>
     */

    protected void addModule(org.osid.course.syllabus.Module module) {
        nullarg(module, "module");
        this.modules.add(module);
        return;
    }


    /**
     *  Sets all the modules.
     *
     *  @param modules a collection of modules
     *  @throws org.osid.NullArgumentException
     *          <code>modules</code> is <code>null</code>
     */

    protected void setModules(java.util.Collection<org.osid.course.syllabus.Module> modules) {
        nullarg(modules, "modules");

        this.modules.clear();
        this.modules.addAll(modules);

        return;
    }


    /**
     *  Tests if this plan supports the given record
     *  <code>Type</code>.
     *
     *  @param  planRecordType a plan record type 
     *  @return <code>true</code> if the planRecordType is supported,
     *          <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>planRecordType</code> is <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type planRecordType) {
        for (org.osid.course.plan.records.PlanRecord record : this.records) {
            if (record.implementsRecordType(planRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Plan</code> record <code>Type</code>.
     *
     *  @param  planRecordType the plan record type 
     *  @return the plan record 
     *  @throws org.osid.NullArgumentException
     *          <code>planRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(planRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.plan.records.PlanRecord getPlanRecord(org.osid.type.Type planRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.plan.records.PlanRecord record : this.records) {
            if (record.implementsRecordType(planRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(planRecordType + " is not supported");
    }


    /**
     *  Adds a record to this plan. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param planRecord the plan record
     *  @param planRecordType plan record type
     *  @throws org.osid.NullArgumentException
     *          <code>planRecord</code> or
     *          <code>planRecordTypeplan</code> is
     *          <code>null</code>
     */
            
    protected void addPlanRecord(org.osid.course.plan.records.PlanRecord planRecord, 
                                 org.osid.type.Type planRecordType) {
        
        nullarg(planRecord, "plan record");
        addRecordType(planRecordType);
        this.records.add(planRecord);
        
        return;
    }
}
