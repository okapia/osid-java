//
// AbstractORMOsidObjectForm.java
//
//     Defines a simple OSID form to draw from.
//
//
// Tom Coppeto
// Okapia
// 15 March 2013
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A basic OsidObjectForm.
 */

public abstract class AbstractORMOsidObjectForm
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectForm
    implements org.osid.OsidObjectForm {
    
    
    
    
    /** 
     *  Constructs a new {@code AbstractOsidObjectForm}.
     *
     *  @param locale this serves as the default Locale for this form
     *         which generally is the Locale of the associated
     *         session. Additional locales may be set.
     *  @param update {@code true} if for update, {@code false} if for
     *         create
     */

    protected AbstractOsidObjectForm(org.osid.locale.Locale locale, boolean update) {
        super(locale, update);
        return;
    }


    /**
     *  Gets the metadata for the display name.
     *
     *  @return metadata for the display name 
     */

    @OSID @Override
    public org.osid.Metadata getDisplayNameMetadata() {
        return (this.displayNameMetadata);
    }


    /**
     *  Sets the display name metadata.
     *
     *  @param metadata the display name metadata
     *  @throws org.osid.NullArgumentException {@code metadata} is
     *          {@code null}
     */

    protected void setDisplayNameMetadata(org.osid.Metadata metadata) {
        nullarg(metadata, "display name metadata");
        this.displayNameMetadata = metadata;
        return;
    }


    /**
     *  Gets the metadata for the description.
     *
     *  @return metadata for the description 
     */

    @OSID @Override
    public org.osid.Metadata getDescriptionMetadata() {
        return (this.descriptionMetadata);
    }


    /**
     *  Sets the description metadata.
     *
     *  @param metadata the description metadata
     *  @throws org.osid.NullArgumentException {@code metadata} is
     *          {@code null}
     */

    protected void setDescriptionMetadata(org.osid.Metadata metadata) {
        nullarg(metadata, "description metadata");
        this.descriptionMetadata = metadata;
        return;
    }


    /**
     *  Gets the metadata for the genus type.
     *
     *  @return metadata for the genus type 
     */

    @OSID @Override
    public org.osid.Metadata getGenusTypeMetadata() {
        return (this.genusTypeMetadata);
    }


    /**
     *  Sets the genus type metadata.
     *
     *  @param metadata the genus type metadata
     *  @throws org.osid.NullArgumentException {@code metadata} is
     *          {@code null}
     */

    protected void setGenusTypeMetadata(org.osid.Metadata metadata) {
        nullarg(metadata, "genus type metadata");
        this.genusTypeMetadata = metadata;
        return;
    }

}
