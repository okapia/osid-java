//
// ProvisionElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.provision.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class ProvisionElements
    extends net.okapia.osid.jamocha.spi.OsidRelationshipElements {


    /**
     *  Gets the ProvisionElement Id.
     *
     *  @return the provision element Id
     */

    public static org.osid.id.Id getProvisionEntityId() {
        return (makeEntityId("osid.provisioning.Provision"));
    }


    /**
     *  Gets the BrokerId element Id.
     *
     *  @return the BrokerId element Id
     */

    public static org.osid.id.Id getBrokerId() {
        return (makeElementId("osid.provisioning.provision.BrokerId"));
    }


    /**
     *  Gets the Broker element Id.
     *
     *  @return the Broker element Id
     */

    public static org.osid.id.Id getBroker() {
        return (makeElementId("osid.provisioning.provision.Broker"));
    }


    /**
     *  Gets the ProvisionableId element Id.
     *
     *  @return the ProvisionableId element Id
     */

    public static org.osid.id.Id getProvisionableId() {
        return (makeElementId("osid.provisioning.provision.ProvisionableId"));
    }


    /**
     *  Gets the Provisionable element Id.
     *
     *  @return the Provisionable element Id
     */

    public static org.osid.id.Id getProvisionable() {
        return (makeElementId("osid.provisioning.provision.Provisionable"));
    }


    /**
     *  Gets the RecipientId element Id.
     *
     *  @return the RecipientId element Id
     */

    public static org.osid.id.Id getRecipientId() {
        return (makeElementId("osid.provisioning.provision.RecipientId"));
    }


    /**
     *  Gets the Recipient element Id.
     *
     *  @return the Recipient element Id
     */

    public static org.osid.id.Id getRecipient() {
        return (makeElementId("osid.provisioning.provision.Recipient"));
    }


    /**
     *  Gets the RequestId element Id.
     *
     *  @return the RequestId element Id
     */

    public static org.osid.id.Id getRequestId() {
        return (makeElementId("osid.provisioning.provision.RequestId"));
    }


    /**
     *  Gets the Request element Id.
     *
     *  @return the Request element Id
     */

    public static org.osid.id.Id getRequest() {
        return (makeElementId("osid.provisioning.provision.Request"));
    }


    /**
     *  Gets the ProvisionDate element Id.
     *
     *  @return the ProvisionDate element Id
     */

    public static org.osid.id.Id getProvisionDate() {
        return (makeElementId("osid.provisioning.provision.ProvisionDate"));
    }


    /**
     *  Gets the DueDate element Id.
     *
     *  @return the DueDate element Id
     */

    public static org.osid.id.Id getDueDate() {
        return (makeElementId("osid.provisioning.provision.DueDate"));
    }


    /**
     *  Gets the Cost element Id.
     *
     *  @return the Cost element Id
     */

    public static org.osid.id.Id getCost() {
        return (makeElementId("osid.provisioning.provision.Cost"));
    }


    /**
     *  Gets the RateAmount element Id.
     *
     *  @return the RateAmount element Id
     */

    public static org.osid.id.Id getRateAmount() {
        return (makeElementId("osid.provisioning.provision.RateAmount"));
    }


    /**
     *  Gets the RatePeriod element Id.
     *
     *  @return the RatePeriod element Id
     */

    public static org.osid.id.Id getRatePeriod() {
        return (makeElementId("osid.provisioning.provision.RatePeriod"));
    }


    /**
     *  Gets the ProvisionReturn element Id.
     *
     *  @return the ProvisionReturn element Id
     */

    public static org.osid.id.Id getProvisionReturn() {
        return (makeElementId("osid.provisioning.provision.ProvisionReturn"));
    }


    /**
     *  Gets the Leased element Id.
     *
     *  @return the Leased element Id
     */

    public static org.osid.id.Id getLeased() {
        return (makeElementId("osid.provisioning.provision.Leased"));
    }


    /**
     *  Gets the MustReturn element Id.
     *
     *  @return the MustReturn element Id
     */

    public static org.osid.id.Id getMustReturn() {
        return (makeElementId("osid.provisioning.provision.MustReturn"));
    }


    /**
     *  Gets the DistributorId element Id.
     *
     *  @return the DistributorId element Id
     */

    public static org.osid.id.Id getDistributorId() {
        return (makeQueryElementId("osid.provisioning.provision.DistributorId"));
    }


    /**
     *  Gets the Distributor element Id.
     *
     *  @return the Distributor element Id
     */

    public static org.osid.id.Id getDistributor() {
        return (makeQueryElementId("osid.provisioning.provision.Distributor"));
    }


    /**
     *  Gets the Rate element Id.
     *
     *  @return the Rate element Id
     */

    public static org.osid.id.Id getRate() {
        return (makeSearchOrderElementId("osid.provisioning.provision.Rate"));
    }
}
