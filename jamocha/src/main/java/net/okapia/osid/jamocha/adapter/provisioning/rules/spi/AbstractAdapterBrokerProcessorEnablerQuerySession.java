//
// AbstractQueryBrokerProcessorEnablerLookupSession.java
//
//    A BrokerProcessorEnablerQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.provisioning.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A BrokerProcessorEnablerQuerySession adapter.
 */

public abstract class AbstractAdapterBrokerProcessorEnablerQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.provisioning.rules.BrokerProcessorEnablerQuerySession {

    private final org.osid.provisioning.rules.BrokerProcessorEnablerQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterBrokerProcessorEnablerQuerySession.
     *
     *  @param session the underlying broker processor enabler query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterBrokerProcessorEnablerQuerySession(org.osid.provisioning.rules.BrokerProcessorEnablerQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeDistributor</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeDistributor Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDistributorId() {
        return (this.session.getDistributorId());
    }


    /**
     *  Gets the {@codeDistributor</code> associated with this 
     *  session.
     *
     *  @return the {@codeDistributor</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Distributor getDistributor()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getDistributor());
    }


    /**
     *  Tests if this user can perform {@codeBrokerProcessorEnabler</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchBrokerProcessorEnablers() {
        return (this.session.canSearchBrokerProcessorEnablers());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include broker processor enablers in distributors which are children
     *  of this distributor in the distributor hierarchy.
     */

    @OSID @Override
    public void useFederatedDistributorView() {
        this.session.useFederatedDistributorView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this distributor only.
     */
    
    @OSID @Override
    public void useIsolatedDistributorView() {
        this.session.useIsolatedDistributorView();
        return;
    }
    
      
    /**
     *  Gets a broker processor enabler query. The returned query will not have an
     *  extension query.
     *
     *  @return the broker processor enabler query 
     */
      
    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorEnablerQuery getBrokerProcessorEnablerQuery() {
        return (this.session.getBrokerProcessorEnablerQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  brokerProcessorEnablerQuery the broker processor enabler query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code brokerProcessorEnablerQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code brokerProcessorEnablerQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorEnablerList getBrokerProcessorEnablersByQuery(org.osid.provisioning.rules.BrokerProcessorEnablerQuery brokerProcessorEnablerQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getBrokerProcessorEnablersByQuery(brokerProcessorEnablerQuery));
    }
}
