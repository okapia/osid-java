//
// AbstractAssemblyContactQuery.java
//
//     A ContactQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.contact.contact.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ContactQuery that stores terms.
 */

public abstract class AbstractAssemblyContactQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRelationshipQuery
    implements org.osid.contact.ContactQuery,
               org.osid.contact.ContactQueryInspector,
               org.osid.contact.ContactSearchOrder {

    private final java.util.Collection<org.osid.contact.records.ContactQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.contact.records.ContactQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.contact.records.ContactSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyContactQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyContactQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets a reference <code> Id. </code> 
     *
     *  @param  referenceId a reference <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> referenceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchReferenceId(org.osid.id.Id referenceId, boolean match) {
        getAssembler().addIdTerm(getReferenceIdColumn(), referenceId, match);
        return;
    }


    /**
     *  Clears the reference <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearReferenceIdTerms() {
        getAssembler().clearTerms(getReferenceIdColumn());
        return;
    }


    /**
     *  Gets the reference <code> Id </code> terms. 
     *
     *  @return the reference <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getReferenceIdTerms() {
        return (getAssembler().getIdTerms(getReferenceIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the reference. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByReference(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getReferenceIdColumn(), style);
        return;
    }


    /**
     *  Gets the ReferenceId column name.
     *
     * @return the column name
     */

    protected String getReferenceIdColumn() {
        return ("reference_id");
    }


    /**
     *  Sets a resource <code> Id. </code> 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAddresseeId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getAddresseeIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAddresseeIdTerms() {
        getAssembler().clearTerms(getAddresseeIdColumn());
        return;
    }


    /**
     *  Gets the resource <code> Id </code> terms. 
     *
     *  @return the resource <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAddresseeIdTerms() {
        return (getAssembler().getIdTerms(getAddresseeIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the resource. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAddressee(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getAddresseeColumn(), style);
        return;
    }


    /**
     *  Gets the AddresseeId column name.
     *
     * @return the column name
     */

    protected String getAddresseeIdColumn() {
        return ("addressee_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAddresseeQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAddresseeQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getAddresseeQuery() {
        throw new org.osid.UnimplementedException("supportsAddresseeQuery() is false");
    }


    /**
     *  Clears the resource terms. 
     */

    @OSID @Override
    public void clearAddresseeTerms() {
        getAssembler().clearTerms(getAddresseeColumn());
        return;
    }


    /**
     *  Gets the resource terms. 
     *
     *  @return the resource terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getAddresseeTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a resource order is available. 
     *
     *  @return <code> true </code> if a resource order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAddresseeSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAddresseeSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getAddresseeSearchOrder() {
        throw new org.osid.UnimplementedException("supportsAddresseeSearchOrder() is false");
    }


    /**
     *  Gets the Addressee column name.
     *
     * @return the column name
     */

    protected String getAddresseeColumn() {
        return ("addressee");
    }


    /**
     *  Sets an address <code> Id. </code> 
     *
     *  @param  addressId an address <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> addressId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAddressId(org.osid.id.Id addressId, boolean match) {
        getAssembler().addIdTerm(getAddressIdColumn(), addressId, match);
        return;
    }


    /**
     *  Clears the address <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAddressIdTerms() {
        getAssembler().clearTerms(getAddressIdColumn());
        return;
    }


    /**
     *  Gets the address <code> Id </code> terms. 
     *
     *  @return the address <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAddressIdTerms() {
        return (getAssembler().getIdTerms(getAddressIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the address. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAddress(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getAddressColumn(), style);
        return;
    }


    /**
     *  Gets the AddressId column name.
     *
     * @return the column name
     */

    protected String getAddressIdColumn() {
        return ("address_id");
    }


    /**
     *  Tests if an <code> AddressQuery </code> is available. 
     *
     *  @return <code> true </code> if an address query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAddressQuery() {
        return (false);
    }


    /**
     *  Gets the query for an address query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the address query 
     *  @throws org.osid.UnimplementedException <code> supportsAddressQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.AddressQuery getAddressQuery() {
        throw new org.osid.UnimplementedException("supportsAddressQuery() is false");
    }


    /**
     *  Clears the address terms. 
     */

    @OSID @Override
    public void clearAddressTerms() {
        getAssembler().clearTerms(getAddressColumn());
        return;
    }


    /**
     *  Gets the address terms. 
     *
     *  @return the address terms 
     */

    @OSID @Override
    public org.osid.contact.AddressQueryInspector[] getAddressTerms() {
        return (new org.osid.contact.AddressQueryInspector[0]);
    }


    /**
     *  Tests if an address order interface is available. 
     *
     *  @return <code> true </code> if an address order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAddressSearchOrder() {
        return (false);
    }


    /**
     *  Gets the address order. 
     *
     *  @return the address search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAddressSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.AddressSearchOrder getAddressSearchOrder() {
        throw new org.osid.UnimplementedException("supportsAddressSearchOrder() is false");
    }


    /**
     *  Gets the Address column name.
     *
     * @return the column name
     */

    protected String getAddressColumn() {
        return ("address");
    }


    /**
     *  Sets the address <code> Id </code> for this query to match contacts 
     *  assigned to address books. 
     *
     *  @param  addressBookId an address book <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> addressBookId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAddressBookId(org.osid.id.Id addressBookId, boolean match) {
        getAssembler().addIdTerm(getAddressBookIdColumn(), addressBookId, match);
        return;
    }


    /**
     *  Clears the address book <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAddressBookIdTerms() {
        getAssembler().clearTerms(getAddressBookIdColumn());
        return;
    }


    /**
     *  Gets the address book <code> Id </code> terms. 
     *
     *  @return the address book <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAddressBookIdTerms() {
        return (getAssembler().getIdTerms(getAddressBookIdColumn()));
    }


    /**
     *  Gets the AddressBookId column name.
     *
     * @return the column name
     */

    protected String getAddressBookIdColumn() {
        return ("address_book_id");
    }


    /**
     *  Tests if an <code> AddressBookQuery </code> is available. 
     *
     *  @return <code> true </code> if an address book query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAddressBookQuery() {
        return (false);
    }


    /**
     *  Gets the query for an address book query. Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the address book query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAddressBookQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.AddressBookQuery getAddressBookQuery() {
        throw new org.osid.UnimplementedException("supportsAddressBookQuery() is false");
    }


    /**
     *  Clears the address book terms. 
     */

    @OSID @Override
    public void clearAddressBookTerms() {
        getAssembler().clearTerms(getAddressBookColumn());
        return;
    }


    /**
     *  Gets the address book terms. 
     *
     *  @return the address book terms 
     */

    @OSID @Override
    public org.osid.contact.AddressBookQueryInspector[] getAddressBookTerms() {
        return (new org.osid.contact.AddressBookQueryInspector[0]);
    }


    /**
     *  Gets the AddressBook column name.
     *
     * @return the column name
     */

    protected String getAddressBookColumn() {
        return ("address_book");
    }


    /**
     *  Tests if this contact supports the given record
     *  <code>Type</code>.
     *
     *  @param  contactRecordType a contact record type 
     *  @return <code>true</code> if the contactRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>contactRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type contactRecordType) {
        for (org.osid.contact.records.ContactQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(contactRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  contactRecordType the contact record type 
     *  @return the contact query record 
     *  @throws org.osid.NullArgumentException
     *          <code>contactRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(contactRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.contact.records.ContactQueryRecord getContactQueryRecord(org.osid.type.Type contactRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.contact.records.ContactQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(contactRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(contactRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  contactRecordType the contact record type 
     *  @return the contact query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>contactRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(contactRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.contact.records.ContactQueryInspectorRecord getContactQueryInspectorRecord(org.osid.type.Type contactRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.contact.records.ContactQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(contactRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(contactRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param contactRecordType the contact record type
     *  @return the contact search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>contactRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(contactRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.contact.records.ContactSearchOrderRecord getContactSearchOrderRecord(org.osid.type.Type contactRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.contact.records.ContactSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(contactRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(contactRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this contact. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param contactQueryRecord the contact query record
     *  @param contactQueryInspectorRecord the contact query inspector
     *         record
     *  @param contactSearchOrderRecord the contact search order record
     *  @param contactRecordType contact record type
     *  @throws org.osid.NullArgumentException
     *          <code>contactQueryRecord</code>,
     *          <code>contactQueryInspectorRecord</code>,
     *          <code>contactSearchOrderRecord</code> or
     *          <code>contactRecordTypecontact</code> is
     *          <code>null</code>
     */
            
    protected void addContactRecords(org.osid.contact.records.ContactQueryRecord contactQueryRecord, 
                                      org.osid.contact.records.ContactQueryInspectorRecord contactQueryInspectorRecord, 
                                      org.osid.contact.records.ContactSearchOrderRecord contactSearchOrderRecord, 
                                      org.osid.type.Type contactRecordType) {

        addRecordType(contactRecordType);

        nullarg(contactQueryRecord, "contact query record");
        nullarg(contactQueryInspectorRecord, "contact query inspector record");
        nullarg(contactSearchOrderRecord, "contact search odrer record");

        this.queryRecords.add(contactQueryRecord);
        this.queryInspectorRecords.add(contactQueryInspectorRecord);
        this.searchOrderRecords.add(contactSearchOrderRecord);
        
        return;
    }
}
