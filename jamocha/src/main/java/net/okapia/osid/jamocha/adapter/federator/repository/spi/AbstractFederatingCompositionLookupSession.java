//
// AbstractFederatingCompositionLookupSession.java
//
//     An abstract federating adapter for a CompositionLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.repository.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  CompositionLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingCompositionLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.repository.CompositionLookupSession>
    implements org.osid.repository.CompositionLookupSession {

    private boolean parallel = false;
    private org.osid.repository.Repository repository = new net.okapia.osid.jamocha.nil.repository.repository.UnknownRepository();


    /**
     *  Constructs a new <code>AbstractFederatingCompositionLookupSession</code>.
     */

    protected AbstractFederatingCompositionLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.repository.CompositionLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Repository/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Repository Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getRepositoryId() {
        return (this.repository.getId());
    }


    /**
     *  Gets the <code>Repository</code> associated with this 
     *  session.
     *
     *  @return the <code>Repository</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.Repository getRepository()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.repository);
    }


    /**
     *  Sets the <code>Repository</code>.
     *
     *  @param  repository the repository for this session
     *  @throws org.osid.NullArgumentException <code>repository</code>
     *          is <code>null</code>
     */

    protected void setRepository(org.osid.repository.Repository repository) {
        nullarg(repository, "repository");
        this.repository = repository;
        return;
    }


    /**
     *  Tests if this user can perform <code>Composition</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCompositions() {
        for (org.osid.repository.CompositionLookupSession session : getSessions()) {
            if (session.canLookupCompositions()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Composition</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCompositionView() {
        for (org.osid.repository.CompositionLookupSession session : getSessions()) {
            session.useComparativeCompositionView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Composition</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCompositionView() {
        for (org.osid.repository.CompositionLookupSession session : getSessions()) {
            session.usePlenaryCompositionView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include compositions in repositories which are children
     *  of this repository in the repository hierarchy.
     */

    @OSID @Override
    public void useFederatedRepositoryView() {
        for (org.osid.repository.CompositionLookupSession session : getSessions()) {
            session.useFederatedRepositoryView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this repository only.
     */

    @OSID @Override
    public void useIsolatedRepositoryView() {
        for (org.osid.repository.CompositionLookupSession session : getSessions()) {
            session.useIsolatedRepositoryView();
        }

        return;
    }


    /**
     *  Only active compositions are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveCompositionView() {
        for (org.osid.repository.CompositionLookupSession session : getSessions()) {
            session.useActiveCompositionView();
        }

        return;
    }


    /**
     *  Active and inactive compositions are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusCompositionView() {
        for (org.osid.repository.CompositionLookupSession session : getSessions()) {
            session.useAnyStatusCompositionView();
        }

        return;
    }
    

    /**
     *  The returns from the lookup methods omit sequestered
     *  compositions.
     */

    @OSID @Override
    public void useSequesteredCompositionView() {
        for (org.osid.repository.CompositionLookupSession session : getSessions()) {
            session.useSequesteredCompositionView();
        }

        return;
    }


    /**
     *  All compositions are returned including sequestered compositions.
     */

    @OSID @Override
    public void useUnsequesteredCompositionView() {
        for (org.osid.repository.CompositionLookupSession session : getSessions()) {
            session.useUnsequesteredCompositionView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Composition</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Composition</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Composition</code> and
     *  retained for compatibility.
     *
     *  In active mode, compositions are returned that are currently
     *  active. In any status mode, active and inactive compositions
     *  are returned.
     *
     *  @param  compositionId <code>Id</code> of the
     *          <code>Composition</code>
     *  @return the composition
     *  @throws org.osid.NotFoundException <code>compositionId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>compositionId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.Composition getComposition(org.osid.id.Id compositionId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.repository.CompositionLookupSession session : getSessions()) {
            try {
                return (session.getComposition(compositionId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(compositionId + " not found");
    }


    /**
     *  Gets a <code>CompositionList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  compositions specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Compositions</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, compositions are returned that are currently
     *  active. In any status mode, active and inactive compositions
     *  are returned.
     *
     *  @param  compositionIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Composition</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>compositionIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.CompositionList getCompositionsByIds(org.osid.id.IdList compositionIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.repository.composition.MutableCompositionList ret = new net.okapia.osid.jamocha.repository.composition.MutableCompositionList();

        try (org.osid.id.IdList ids = compositionIds) {
            while (ids.hasNext()) {
                ret.addComposition(getComposition(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>CompositionList</code> corresponding to the given
     *  composition genus <code>Type</code> which does not include
     *  compositions of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  compositions or an error results. Otherwise, the returned list
     *  may contain only those compositions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, compositions are returned that are currently
     *  active. In any status mode, active and inactive compositions
     *  are returned.
     *
     *  @param  compositionGenusType a composition genus type 
     *  @return the returned <code>Composition</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>compositionGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.CompositionList getCompositionsByGenusType(org.osid.type.Type compositionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.repository.composition.FederatingCompositionList ret = getCompositionList();

        for (org.osid.repository.CompositionLookupSession session : getSessions()) {
            ret.addCompositionList(session.getCompositionsByGenusType(compositionGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CompositionList</code> corresponding to the given
     *  composition genus <code>Type</code> and include any additional
     *  compositions with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  compositions or an error results. Otherwise, the returned list
     *  may contain only those compositions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, compositions are returned that are currently
     *  active. In any status mode, active and inactive compositions
     *  are returned.
     *
     *  @param  compositionGenusType a composition genus type 
     *  @return the returned <code>Composition</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>compositionGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.CompositionList getCompositionsByParentGenusType(org.osid.type.Type compositionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.repository.composition.FederatingCompositionList ret = getCompositionList();

        for (org.osid.repository.CompositionLookupSession session : getSessions()) {
            ret.addCompositionList(session.getCompositionsByParentGenusType(compositionGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CompositionList</code> containing the given
     *  composition record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  compositions or an error results. Otherwise, the returned list
     *  may contain only those compositions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, compositions are returned that are currently
     *  active. In any status mode, active and inactive compositions
     *  are returned.
     *
     *  @param  compositionRecordType a composition record type 
     *  @return the returned <code>Composition</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>compositionRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.CompositionList getCompositionsByRecordType(org.osid.type.Type compositionRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.repository.composition.FederatingCompositionList ret = getCompositionList();

        for (org.osid.repository.CompositionLookupSession session : getSessions()) {
            ret.addCompositionList(session.getCompositionsByRecordType(compositionRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CompositionList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known compositions or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  compositions that are accessible through this session. 
     *
     *  In active mode, compositions are returned that are currently
     *  active. In any status mode, active and inactive compositions
     *  are returned.
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Composition</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.repository.CompositionList getCompositionsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        net.okapia.osid.jamocha.adapter.federator.repository.composition.FederatingCompositionList ret = getCompositionList();

        for (org.osid.repository.CompositionLookupSession session : getSessions()) {
            ret.addCompositionList(session.getCompositionsByProvider(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Compositions</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  compositions or an error results. Otherwise, the returned list
     *  may contain only those compositions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, compositions are returned that are currently
     *  active. In any status mode, active and inactive compositions
     *  are returned.
     *
     *  @return a list of <code>Compositions</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.CompositionList getCompositions()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.repository.composition.FederatingCompositionList ret = getCompositionList();

        for (org.osid.repository.CompositionLookupSession session : getSessions()) {
            ret.addCompositionList(session.getCompositions());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.repository.composition.FederatingCompositionList getCompositionList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.repository.composition.ParallelCompositionList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.repository.composition.CompositeCompositionList());
        }
    }
}
