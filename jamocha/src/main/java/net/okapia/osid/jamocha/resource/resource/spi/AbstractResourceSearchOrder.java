//
// AbstractResourceSearchOdrer.java
//
//     Defines a ResourceSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resource.resource.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code ResourceSearchOrder}.
 */

public abstract class AbstractResourceSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectSearchOrder
    implements org.osid.resource.ResourceSearchOrder {

    private final java.util.Collection<org.osid.resource.records.ResourceSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Groups the search results by resources that are groups. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByGroup(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Groups the search results by resources that are demographics. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDemographic(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the result set by avatar. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAvatar(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an <code> AssetSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if an asset search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAvatarSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for an asset. 
     *
     *  @return the asset search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvatarSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetSearchOrder getAvatarSearchOrder() {
        throw new org.osid.UnimplementedException("supportsAvatarSearchOrder() is false");
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  resourceRecordType a resource record type 
     *  @return {@code true} if the resourceRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code resourceRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type resourceRecordType) {
        for (org.osid.resource.records.ResourceSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(resourceRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  resourceRecordType the resource record type 
     *  @return the resource search order record
     *  @throws org.osid.NullArgumentException
     *          {@code resourceRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(resourceRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.resource.records.ResourceSearchOrderRecord getResourceSearchOrderRecord(org.osid.type.Type resourceRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resource.records.ResourceSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(resourceRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(resourceRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this resource. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param resourceRecord the resource search odrer record
     *  @param resourceRecordType resource record type
     *  @throws org.osid.NullArgumentException
     *          {@code resourceRecord} or
     *          {@code resourceRecordTyperesource} is
     *          {@code null}
     */
            
    protected void addResourceRecord(org.osid.resource.records.ResourceSearchOrderRecord resourceSearchOrderRecord, 
                                     org.osid.type.Type resourceRecordType) {

        addRecordType(resourceRecordType);
        this.records.add(resourceSearchOrderRecord);
        
        return;
    }
}
