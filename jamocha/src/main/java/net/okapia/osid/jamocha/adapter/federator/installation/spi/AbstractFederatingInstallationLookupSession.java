//
// AbstractFederatingInstallationLookupSession.java
//
//     An abstract federating adapter for an InstallationLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.installation.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for an
 *  InstallationLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingInstallationLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.installation.InstallationLookupSession>
    implements org.osid.installation.InstallationLookupSession {

    private boolean parallel = false;
    private org.osid.installation.Site site = new net.okapia.osid.jamocha.nil.installation.site.UnknownSite();


    /**
     *  Constructs a new <code>AbstractFederatingInstallationLookupSession</code>.
     */

    protected AbstractFederatingInstallationLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.installation.InstallationLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Site/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Site Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getSiteId() {
        return (this.site.getId());
    }


    /**
     *  Gets the <code>Site</code> associated with this 
     *  session.
     *
     *  @return the <code>Site</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.Site getSite()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.site);
    }


    /**
     *  Sets the <code>Site</code>.
     *
     *  @param  site the site for this session
     *  @throws org.osid.NullArgumentException <code>site</code>
     *          is <code>null</code>
     */

    protected void setSite(org.osid.installation.Site site) {
        nullarg(site, "site");
        this.site = site;
        return;
    }


    /**
     *  Tests if this user can perform <code>Installation</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupInstallations() {
        for (org.osid.installation.InstallationLookupSession session : getSessions()) {
            if (session.canLookupInstallations()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Installation</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeInstallationView() {
        for (org.osid.installation.InstallationLookupSession session : getSessions()) {
            session.useComparativeInstallationView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Installation</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryInstallationView() {
        for (org.osid.installation.InstallationLookupSession session : getSessions()) {
            session.usePlenaryInstallationView();
        }

        return;
    }


    /**
     * The returns from the lookup methods may omit multiple versions
     *  of the same installation.
     */

    @OSID @Override
    public void useNormalizedVersionView() {
        for (org.osid.installation.InstallationLookupSession session : getSessions()) {
            session.useNormalizedVersionView();
        }

        return;
    }


    /**
     *  All versions of the same installation are returned.
     */

    @OSID @Override
    public void useDenormalizedVersionView() {
        for (org.osid.installation.InstallationLookupSession session : getSessions()) {
            session.useDenormalizedVersionView();
        }

        return;
    }


    /**
     *  Gets the <code>Installation</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Installation</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Installation</code> and
     *  retained for compatibility.
     *
     *  @param  installationId <code>Id</code> of the
     *          <code>Installation</code>
     *  @return the installation
     *  @throws org.osid.NotFoundException <code>installationId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>installationId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.Installation getInstallation(org.osid.id.Id installationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.installation.InstallationLookupSession session : getSessions()) {
            try {
                return (session.getInstallation(installationId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(installationId + " not found");
    }


    /**
     *  Gets an <code>InstallationList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  installations specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Installations</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  installationIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Installation</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>installationIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.InstallationList getInstallationsByIds(org.osid.id.IdList installationIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.installation.installation.MutableInstallationList ret = new net.okapia.osid.jamocha.installation.installation.MutableInstallationList();

        try (org.osid.id.IdList ids = installationIds) {
            while (ids.hasNext()) {
                ret.addInstallation(getInstallation(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets an <code>InstallationList</code> corresponding to the given
     *  installation genus <code>Type</code> which does not include
     *  installations of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  installations or an error results. Otherwise, the returned list
     *  may contain only those installations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  installationGenusType an installation genus type 
     *  @return the returned <code>Installation</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>installationGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.InstallationList getInstallationsByGenusType(org.osid.type.Type installationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.installation.installation.FederatingInstallationList ret = getInstallationList();

        for (org.osid.installation.InstallationLookupSession session : getSessions()) {
            ret.addInstallationList(session.getInstallationsByGenusType(installationGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>InstallationList</code> corresponding to the given
     *  installation genus <code>Type</code> and include any additional
     *  installations with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  installations or an error results. Otherwise, the returned list
     *  may contain only those installations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  installationGenusType an installation genus type 
     *  @return the returned <code>Installation</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>installationGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.InstallationList getInstallationsByParentGenusType(org.osid.type.Type installationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.installation.installation.FederatingInstallationList ret = getInstallationList();

        for (org.osid.installation.InstallationLookupSession session : getSessions()) {
            ret.addInstallationList(session.getInstallationsByParentGenusType(installationGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>InstallationList</code> containing the given
     *  installation record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  installations or an error results. Otherwise, the returned list
     *  may contain only those installations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  installationRecordType an installation record type 
     *  @return the returned <code>Installation</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>installationRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.InstallationList getInstallationsByRecordType(org.osid.type.Type installationRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.installation.installation.FederatingInstallationList ret = getInstallationList();

        for (org.osid.installation.InstallationLookupSession session : getSessions()) {
            ret.addInstallationList(session.getInstallationsByRecordType(installationRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>InstallationList</code> corresponding to the
     *  given <code>Package</code>.
     *
     *In plenary mode, the returned list contains all of the
     *  installations for the specified package, in the order of the
     *  list, including duplicates, or an error results if an <code>
     *  Id </code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code> Installations
     *  </code> may be omitted from the list and may present the
     *  elements in any order including returning a unique set.
     *
     *  @param  packageId <code>Id</code> of a <code>Package</code>
     *  @return the returned <code>Installation</code> list
     *  @throws org.osid.NullArgumentException <code>packageId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.installation.InstallationList getInstallationsByPackage(org.osid.id.Id packageId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.installation.installation.FederatingInstallationList ret = getInstallationList();

        for (org.osid.installation.InstallationLookupSession session : getSessions()) {
            ret.addInstallationList(session.getInstallationsByPackage(packageId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Installations</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  installations or an error results. Otherwise, the returned list
     *  may contain only those installations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Installations</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.InstallationList getInstallations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.installation.installation.FederatingInstallationList ret = getInstallationList();

        for (org.osid.installation.InstallationLookupSession session : getSessions()) {
            ret.addInstallationList(session.getInstallations());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.installation.installation.FederatingInstallationList getInstallationList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.installation.installation.ParallelInstallationList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.installation.installation.CompositeInstallationList());
        }
    }
}
