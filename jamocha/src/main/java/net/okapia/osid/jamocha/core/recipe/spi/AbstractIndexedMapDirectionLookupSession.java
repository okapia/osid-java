//
// AbstractIndexedMapDirectionLookupSession.java
//
//    A simple framework for providing a Direction lookup service
//    backed by a fixed collection of directions with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.recipe.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Direction lookup service backed by a
 *  fixed collection of directions. The directions are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some directions may be compatible
 *  with more types than are indicated through these direction
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Directions</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapDirectionLookupSession
    extends AbstractMapDirectionLookupSession
    implements org.osid.recipe.DirectionLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.recipe.Direction> directionsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.recipe.Direction>());
    private final MultiMap<org.osid.type.Type, org.osid.recipe.Direction> directionsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.recipe.Direction>());


    /**
     *  Makes a <code>Direction</code> available in this session.
     *
     *  @param  direction a direction
     *  @throws org.osid.NullArgumentException <code>direction<code> is
     *          <code>null</code>
     */

    @Override
    protected void putDirection(org.osid.recipe.Direction direction) {
        super.putDirection(direction);

        this.directionsByGenus.put(direction.getGenusType(), direction);
        
        try (org.osid.type.TypeList types = direction.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.directionsByRecord.put(types.getNextType(), direction);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a direction from this session.
     *
     *  @param directionId the <code>Id</code> of the direction
     *  @throws org.osid.NullArgumentException <code>directionId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeDirection(org.osid.id.Id directionId) {
        org.osid.recipe.Direction direction;
        try {
            direction = getDirection(directionId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.directionsByGenus.remove(direction.getGenusType());

        try (org.osid.type.TypeList types = direction.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.directionsByRecord.remove(types.getNextType(), direction);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeDirection(directionId);
        return;
    }


    /**
     *  Gets a <code>DirectionList</code> corresponding to the given
     *  direction genus <code>Type</code> which does not include
     *  directions of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known directions or an error results. Otherwise,
     *  the returned list may contain only those directions that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  directionGenusType a direction genus type 
     *  @return the returned <code>Direction</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>directionGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.DirectionList getDirectionsByGenusType(org.osid.type.Type directionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.recipe.direction.ArrayDirectionList(this.directionsByGenus.get(directionGenusType)));
    }


    /**
     *  Gets a <code>DirectionList</code> containing the given
     *  direction record <code>Type</code>. In plenary mode, the
     *  returned list contains all known directions or an error
     *  results. Otherwise, the returned list may contain only those
     *  directions that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  directionRecordType a direction record type 
     *  @return the returned <code>direction</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>directionRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.DirectionList getDirectionsByRecordType(org.osid.type.Type directionRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.recipe.direction.ArrayDirectionList(this.directionsByRecord.get(directionRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.directionsByGenus.clear();
        this.directionsByRecord.clear();

        super.close();

        return;
    }
}
