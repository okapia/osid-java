//
// AbstractRepositorySearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.repository.repository.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractRepositorySearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.repository.RepositorySearchResults {

    private org.osid.repository.RepositoryList repositories;
    private final org.osid.repository.RepositoryQueryInspector inspector;
    private final java.util.Collection<org.osid.repository.records.RepositorySearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractRepositorySearchResults.
     *
     *  @param repositories the result set
     *  @param repositoryQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>repositories</code>
     *          or <code>repositoryQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractRepositorySearchResults(org.osid.repository.RepositoryList repositories,
                                            org.osid.repository.RepositoryQueryInspector repositoryQueryInspector) {
        nullarg(repositories, "repositories");
        nullarg(repositoryQueryInspector, "repository query inspectpr");

        this.repositories = repositories;
        this.inspector = repositoryQueryInspector;

        return;
    }


    /**
     *  Gets the repository list resulting from a search.
     *
     *  @return a repository list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.repository.RepositoryList getRepositories() {
        if (this.repositories == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.repository.RepositoryList repositories = this.repositories;
        this.repositories = null;
	return (repositories);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.repository.RepositoryQueryInspector getRepositoryQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  repository search record <code> Type. </code> This method must
     *  be used to retrieve a repository implementing the requested
     *  record.
     *
     *  @param repositorySearchRecordType a repository search 
     *         record type 
     *  @return the repository search
     *  @throws org.osid.NullArgumentException
     *          <code>repositorySearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(repositorySearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.repository.records.RepositorySearchResultsRecord getRepositorySearchResultsRecord(org.osid.type.Type repositorySearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.repository.records.RepositorySearchResultsRecord record : this.records) {
            if (record.implementsRecordType(repositorySearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(repositorySearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record repository search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addRepositoryRecord(org.osid.repository.records.RepositorySearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "repository record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
