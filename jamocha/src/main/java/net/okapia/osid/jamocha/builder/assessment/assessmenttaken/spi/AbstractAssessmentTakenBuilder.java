//
// AbstractAssessmentTaken.java
//
//     Defines an AssessmentTaken builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.assessment.assessmenttaken.spi;


/**
 *  Defines an <code>AssessmentTaken</code> builder.
 */

public abstract class AbstractAssessmentTakenBuilder<T extends AbstractAssessmentTakenBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.assessment.assessmenttaken.AssessmentTakenMiter assessmentTaken;


    /**
     *  Constructs a new <code>AbstractAssessmentTakenBuilder</code>.
     *
     *  @param assessmentTaken the assessment taken to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractAssessmentTakenBuilder(net.okapia.osid.jamocha.builder.assessment.assessmenttaken.AssessmentTakenMiter assessmentTaken) {
        super(assessmentTaken);
        this.assessmentTaken = assessmentTaken;
        return;
    }


    /**
     *  Builds the assessment taken.
     *
     *  @return the new assessment taken
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.assessment.AssessmentTaken build() {
        (new net.okapia.osid.jamocha.builder.validator.assessment.assessmenttaken.AssessmentTakenValidator(getValidations())).validate(this.assessmentTaken);
        return (new net.okapia.osid.jamocha.builder.assessment.assessmenttaken.ImmutableAssessmentTaken(this.assessmentTaken));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the assessment taken miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.assessment.assessmenttaken.AssessmentTakenMiter getMiter() {
        return (this.assessmentTaken);
    }


    /**
     *  Sets the assessment offered.
     *
     *  @param assessmentOffered an assessment offered
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentOffered</code> is <code>null</code>
     */

    public T assessmentOffered(org.osid.assessment.AssessmentOffered assessmentOffered) {
        getMiter().setAssessmentOffered(assessmentOffered);
        return (self());
    }


    /**
     *  Sets the taker.
     *
     *  @param taker a taker
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>taker</code> is <code>null</code>
     */

    public T taker(org.osid.resource.Resource taker) {
        getMiter().setTaker(taker);
        return (self());
    }


    /**
     *  Sets the taking agent.
     *
     *  @param agent a taking agent
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>agent</code> is <code>null</code>
     */

    public T takingAgent(org.osid.authentication.Agent agent) {
        getMiter().setTakingAgent(agent);
        return (self());
    }


    /**
     *  Sets the actual start time.
     *
     *  @param time an actual start time
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    public T startTime(org.osid.calendaring.DateTime time) {
        getMiter().setActualStartTime(time);
        return (self());
    }


    /**
     *  Sets the completion time.
     *
     *  @param time a completion time
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    public T completionTime(org.osid.calendaring.DateTime time) {
        getMiter().setCompletionTime(time);
        return (self());
    }


    /**
     *  Sets the time spent.
     *
     *  @param duration a time spent
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>duration</code>
     *          is <code>null</code>
     */

    public T timeSpent(org.osid.calendaring.Duration duration) {
        getMiter().setTimeSpent(duration);
        return (self());
    }


    /**
     *  Sets the completion.
     *
     *  @param completion a completion
     *  @return the builder
     *  @throws org.osid.InvalidArgumentException
     *          <code>completion</code> is out of range
     */

    public T completion(long completion) {
        getMiter().setCompletion(completion);
        return (self());
    }


    /**
     *  Sets the score system.
     *
     *  @param system a score system
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>system</code> is
     *          <code>null</code>
     */

    public T scoreSystem(org.osid.grading.GradeSystem system) {
        getMiter().setScoreSystem(system);
        return (self());
    }


    /**
     *  Sets the score.
     *
     *  @param score a score
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>score</code> is <code>null</code>
     */

    public T score(java.math.BigDecimal score) {
        getMiter().setScore(score);
        return (self());
    }


    /**
     *  Sets the grade.
     *
     *  @param grade a grade
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>grade</code> is <code>null</code>
     */

    public T grade(org.osid.grading.Grade grade) {
        getMiter().setGrade(grade);
        return (self());
    }


    /**
     *  Sets the feedback.
     *
     *  @param feedback the feedback
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>feedback</code> is <code>null</code>
     */

    public T feedback(org.osid.locale.DisplayText feedback) {
        getMiter().setFeedback(feedback);
        return (self());
    }


    /**
     *  Sets the rubric.
     *
     *  @param assessmentTaken the rubric assessment taken
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentTaken</code> is <code>null</code>
     */

    public T rubric(org.osid.assessment.AssessmentTaken assessmentTaken) {
        getMiter().setRubric(assessmentTaken);
        return (self());
    }


    /**
     *  Adds an AssessmentTaken record.
     *
     *  @param record an assessment taken record
     *  @param recordType the type of assessment taken record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.assessment.records.AssessmentTakenRecord record, org.osid.type.Type recordType) {
        getMiter().addAssessmentTakenRecord(record, recordType);
        return (self());
    }
}       


