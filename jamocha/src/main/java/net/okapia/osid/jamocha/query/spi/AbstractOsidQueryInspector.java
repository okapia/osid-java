//
// AbstractOsidQueryInspector.java
//
//     Defines an OsidQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 1 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.query.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an OsidQueryInspector to extend. 
 */

public abstract class AbstractOsidQueryInspector
    implements org.osid.OsidQueryInspector {

    private final java.util.Collection<org.osid.search.terms.StringTerm> keywordTerms = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.search.terms.BooleanTerm> anyTerms = new java.util.LinkedHashSet<>();


    /**
     *  Gets the keyword query terms.
     *
     *  @return the keyword string terms
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getKeywordTerms() {
        return (this.keywordTerms.toArray(new org.osid.search.terms.StringTerm[this.keywordTerms.size()]));
    }

    
    /**
     *  Adds a keyword term.
     *
     *  @param term a string term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addKeywordTerm(org.osid.search.terms.StringTerm term) {
        nullarg(term, "string term");
        this.keywordTerms.add(term);
        return;
    }


    /**
     *  Adds a collection of keyword terms.
     *
     *  @param terms a collection of string terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addKeywordTerms(java.util.Collection<org.osid.search.terms.StringTerm> terms) {
        nullarg(terms, "string terms");
        this.keywordTerms.addAll(terms);
        return;
    }


    /**
     *  Adds a keyword term.
     *
     *  @param keyword the keyword
     *  @param stringMatchType the string match type
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>keyword</code> or
     *          <code>stringMatchType</code> is <code>null</code>
     */

    protected void addKeywordTerm(String keyword, org.osid.type.Type stringMatchType, boolean match) {
        this.keywordTerms.add(new net.okapia.osid.primordium.terms.StringTerm(keyword, stringMatchType, match));
        return;
    }

    
    /**
     *  Gets the any query terms. 
     *
     *  @return the any terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getAnyTerms() {
        return (this.anyTerms.toArray(new org.osid.search.terms.BooleanTerm[this.anyTerms.size()]));
    }


    /**
     *  Adds an any term.
     *
     *  @param term the any term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addAnyTerm(org.osid.search.terms.BooleanTerm term) {
        nullarg(term, "boolean term");
        this.anyTerms.add(term);
        return;
    }


    /**
     *  Adds a collection of any terms.
     *
     *  @param terms a collection of boolean terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is
     *          <code>null</code>
     */

    protected void addAnyTerms(java.util.Collection<org.osid.search.terms.BooleanTerm> terms) {
        nullarg(terms, "boolean terms");
        this.anyTerms.addAll(terms);
        return;
    }


    /**
     *  Adds an any term.
     *
     *  @param any boolean value
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     */

    protected void addAnyTerm(boolean any, boolean match) {
        this.anyTerms.add(new net.okapia.osid.primordium.terms.BooleanTerm(any, match));
        return;
    }
}
