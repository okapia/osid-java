//
// PostEntryElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.posting.postentry.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class PostEntryElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the PostEntryElement Id.
     *
     *  @return the post entry element Id
     */

    public static org.osid.id.Id getPostEntryEntityId() {
        return (makeEntityId("osid.financials.posting.PostEntry"));
    }


    /**
     *  Gets the PostId element Id.
     *
     *  @return the PostId element Id
     */

    public static org.osid.id.Id getPostId() {
        return (makeElementId("osid.financials.posting.postentry.PostId"));
    }


    /**
     *  Gets the Post element Id.
     *
     *  @return the Post element Id
     */

    public static org.osid.id.Id getPost() {
        return (makeElementId("osid.financials.posting.postentry.Post"));
    }


    /**
     *  Gets the AccountId element Id.
     *
     *  @return the AccountId element Id
     */

    public static org.osid.id.Id getAccountId() {
        return (makeElementId("osid.financials.posting.postentry.AccountId"));
    }


    /**
     *  Gets the Account element Id.
     *
     *  @return the Account element Id
     */

    public static org.osid.id.Id getAccount() {
        return (makeElementId("osid.financials.posting.postentry.Account"));
    }


    /**
     *  Gets the ActivityId element Id.
     *
     *  @return the ActivityId element Id
     */

    public static org.osid.id.Id getActivityId() {
        return (makeElementId("osid.financials.posting.postentry.ActivityId"));
    }


    /**
     *  Gets the Activity element Id.
     *
     *  @return the Activity element Id
     */

    public static org.osid.id.Id getActivity() {
        return (makeElementId("osid.financials.posting.postentry.Activity"));
    }


    /**
     *  Gets the Amount element Id.
     *
     *  @return the Amount element Id
     */

    public static org.osid.id.Id getAmount() {
        return (makeElementId("osid.financials.posting.postentry.Amount"));
    }


    /**
     *  Gets the Debit element Id.
     *
     *  @return the Debit element Id
     */

    public static org.osid.id.Id getDebit() {
        return (makeElementId("osid.financials.posting.postentry.Debit"));
    }


    /**
     *  Gets the BusinessId element Id.
     *
     *  @return the BusinessId element Id
     */

    public static org.osid.id.Id getBusinessId() {
        return (makeQueryElementId("osid.financials.posting.postentry.BusinessId"));
    }


    /**
     *  Gets the Business element Id.
     *
     *  @return the Business element Id
     */

    public static org.osid.id.Id getBusiness() {
        return (makeQueryElementId("osid.financials.posting.postentry.Business"));
    }
}
