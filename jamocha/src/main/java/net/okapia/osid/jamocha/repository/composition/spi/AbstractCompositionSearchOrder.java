//
// AbstractCompositionSearchOdrer.java
//
//     Defines a CompositionSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.repository.composition.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code CompositionSearchOrder}.
 */

public abstract class AbstractCompositionSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractSourceableOsidObjectSearchOrder
    implements org.osid.repository.CompositionSearchOrder {

    private final java.util.Collection<org.osid.repository.records.CompositionSearchOrderRecord> records = new java.util.LinkedHashSet<>();

    private final OsidOperableSearchOrder operable = new OsidOperableSearchOrder();
    private final OsidContainableSearchOrder containable = new OsidContainableSearchOrder();


    /**
     *  Specifies a preference for ordering the result set by the
     *  active status.
     *
     *  @param  style the search order style 
     *  @throws org.osid.NullArgumentException {@code  style} is {@code  
     *          null} 
     */

    @OSID @Override
    public void orderByActive(org.osid.SearchOrderStyle style) {
        this.operable.orderByActive(style);
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the
     *  administratively enabled status.
     *
     *  @param  style the search order style 
     *  @throws org.osid.NullArgumentException {@code style} is {@code
     *          null}
     */

    @OSID @Override
    public void orderByEnabled(org.osid.SearchOrderStyle style) {
        this.operable.orderByEnabled(style);
        return;
    }        


    /**
     *  Specifies a preference for ordering the result set by the
     *  administratively disabled status.
     *
     *  @param  style the search order style 
     *  @throws org.osid.NullArgumentException {@code  style} is
     *          {@code  null}
     */

    @OSID @Override
    public void orderByDisabled(org.osid.SearchOrderStyle style) {
        this.operable.orderByDisabled(style);
        return;
    }


    /**
     *  Specifies a preference for ordering the results by the
     *  operational status.
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException {@code  style} is {@code  
     *          null} 
     */

    @OSID @Override
    public void orderByOperational(org.osid.SearchOrderStyle style) {
        this.operable.orderByOperational(style);
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the
     *  sequestered flag.
     *
     *  @param  style the search order style 
     *  @throws org.osid.NullArgumentException {@code style} is {@code
     *          null}
     */
    
    @OSID @Override
    public void orderBySequestered(org.osid.SearchOrderStyle style) {
        this.containable.orderBySequestered(style);
        return;
    }


    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  compositionRecordType a composition record type 
     *  @return {@code true} if the compositionRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code compositionRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type compositionRecordType) {
        for (org.osid.repository.records.CompositionSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(compositionRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  compositionRecordType the composition record type 
     *  @return the composition search order record
     *  @throws org.osid.NullArgumentException
     *          {@code compositionRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(compositionRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.repository.records.CompositionSearchOrderRecord getCompositionSearchOrderRecord(org.osid.type.Type compositionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.repository.records.CompositionSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(compositionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(compositionRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this composition. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param compositionRecord the composition search odrer record
     *  @param compositionRecordType composition record type
     *  @throws org.osid.NullArgumentException
     *          {@code compositionRecord} or
     *          {@code compositionRecordTypecomposition} is
     *          {@code null}
     */
            
    protected void addCompositionRecord(org.osid.repository.records.CompositionSearchOrderRecord compositionSearchOrderRecord, 
                                     org.osid.type.Type compositionRecordType) {

        addRecordType(compositionRecordType);
        this.records.add(compositionSearchOrderRecord);
        
        return;
    }


    protected class OsidOperableSearchOrder
        extends net.okapia.osid.jamocha.spi.AbstractOsidOperableSearchOrder
        implements org.osid.OsidOperableSearchOrder {
    }


    protected class OsidContainableSearchOrder
        extends net.okapia.osid.jamocha.spi.AbstractOsidContainableSearchOrder
        implements org.osid.OsidContainableSearchOrder {
    }
}
