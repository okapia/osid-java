//
// AbstractMapPathLookupSession
//
//    A simple framework for providing a Path lookup service
//    backed by a fixed collection of paths.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping.path.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Path lookup service backed by a
 *  fixed collection of paths. The paths are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Paths</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapPathLookupSession
    extends net.okapia.osid.jamocha.mapping.path.spi.AbstractPathLookupSession
    implements org.osid.mapping.path.PathLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.mapping.path.Path> paths = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.mapping.path.Path>());


    /**
     *  Makes a <code>Path</code> available in this session.
     *
     *  @param  path a path
     *  @throws org.osid.NullArgumentException <code>path<code>
     *          is <code>null</code>
     */

    protected void putPath(org.osid.mapping.path.Path path) {
        this.paths.put(path.getId(), path);
        return;
    }


    /**
     *  Makes an array of paths available in this session.
     *
     *  @param  paths an array of paths
     *  @throws org.osid.NullArgumentException <code>paths<code>
     *          is <code>null</code>
     */

    protected void putPaths(org.osid.mapping.path.Path[] paths) {
        putPaths(java.util.Arrays.asList(paths));
        return;
    }


    /**
     *  Makes a collection of paths available in this session.
     *
     *  @param  paths a collection of paths
     *  @throws org.osid.NullArgumentException <code>paths<code>
     *          is <code>null</code>
     */

    protected void putPaths(java.util.Collection<? extends org.osid.mapping.path.Path> paths) {
        for (org.osid.mapping.path.Path path : paths) {
            this.paths.put(path.getId(), path);
        }

        return;
    }


    /**
     *  Removes a Path from this session.
     *
     *  @param  pathId the <code>Id</code> of the path
     *  @throws org.osid.NullArgumentException <code>pathId<code> is
     *          <code>null</code>
     */

    protected void removePath(org.osid.id.Id pathId) {
        this.paths.remove(pathId);
        return;
    }


    /**
     *  Gets the <code>Path</code> specified by its <code>Id</code>.
     *
     *  @param  pathId <code>Id</code> of the <code>Path</code>
     *  @return the path
     *  @throws org.osid.NotFoundException <code>pathId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>pathId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.Path getPath(org.osid.id.Id pathId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.mapping.path.Path path = this.paths.get(pathId);
        if (path == null) {
            throw new org.osid.NotFoundException("path not found: " + pathId);
        }

        return (path);
    }


    /**
     *  Gets all <code>Paths</code>. In plenary mode, the returned
     *  list contains all known paths or an error
     *  results. Otherwise, the returned list may contain only those
     *  paths that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Paths</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.PathList getPaths()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.mapping.path.path.ArrayPathList(this.paths.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.paths.clear();
        super.close();
        return;
    }
}
