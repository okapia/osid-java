//
// InvariantMapProxyJobLookupSession
//
//    Implements a Job lookup service backed by a fixed
//    collection of jobs. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing;


/**
 *  Implements a Job lookup service backed by a fixed
 *  collection of jobs. The jobs are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyJobLookupSession
    extends net.okapia.osid.jamocha.core.resourcing.spi.AbstractMapJobLookupSession
    implements org.osid.resourcing.JobLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyJobLookupSession} with no
     *  jobs.
     *
     *  @param foundry the foundry
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyJobLookupSession(org.osid.resourcing.Foundry foundry,
                                                  org.osid.proxy.Proxy proxy) {
        setFoundry(foundry);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyJobLookupSession} with a single
     *  job.
     *
     *  @param foundry the foundry
     *  @param job a single job
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code foundry},
     *          {@code job} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyJobLookupSession(org.osid.resourcing.Foundry foundry,
                                                  org.osid.resourcing.Job job, org.osid.proxy.Proxy proxy) {

        this(foundry, proxy);
        putJob(job);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyJobLookupSession} using
     *  an array of jobs.
     *
     *  @param foundry the foundry
     *  @param jobs an array of jobs
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code foundry},
     *          {@code jobs} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyJobLookupSession(org.osid.resourcing.Foundry foundry,
                                                  org.osid.resourcing.Job[] jobs, org.osid.proxy.Proxy proxy) {

        this(foundry, proxy);
        putJobs(jobs);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyJobLookupSession} using a
     *  collection of jobs.
     *
     *  @param foundry the foundry
     *  @param jobs a collection of jobs
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code foundry},
     *          {@code jobs} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyJobLookupSession(org.osid.resourcing.Foundry foundry,
                                                  java.util.Collection<? extends org.osid.resourcing.Job> jobs,
                                                  org.osid.proxy.Proxy proxy) {

        this(foundry, proxy);
        putJobs(jobs);
        return;
    }
}
