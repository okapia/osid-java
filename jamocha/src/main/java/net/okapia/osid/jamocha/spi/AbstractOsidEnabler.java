//
// AbstractOsidEnabler.java
//
//     Defines a simple OSID enabler to draw from.
//
//
// Tom Coppeto
// Okapia
// 22 January 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a simple OSID enabler to draw from. using this
 *  abstract class requires that <code>setId()</code> must be done
 *  before passing this interface to a consumer in order to maintain
 *  compliance with the OSID specification.
 */

public abstract class AbstractOsidEnabler
    extends AbstractOsidRule
    implements org.osid.OsidEnabler {

    private org.osid.calendaring.Schedule schedule;
    private org.osid.calendaring.Event event;
    private org.osid.calendaring.cycle.CyclicEvent cyclic;
    private org.osid.resource.Resource demographic;

    private final Temporal temporal = new Temporal();


    /**
     *  Tests if the current date is within the start end end dates
     *  inclusive.
     *
     *  @return <code> true </code> if this is effective, <code> false
     *          </code> otherwise
     */

    @OSID @Override
    public boolean isEffective() {
        return (this.temporal.isEffective());
    }


    /**
     *  Gets the start date. 
     *
     *  @return the start date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getStartDate() {
        return (this.temporal.getStartDate());
    }


    /**
     *  Sets the start date.
     *
     *  @param date the start date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    protected void setStartDate(org.osid.calendaring.DateTime date) {
        this.temporal.setStartDate(date);
        return;
    }


    /**
     *  Gets the end date. 
     *
     *  @return the end date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getEndDate() {
        return (this.temporal.getEndDate());
    }


    /**
     *  Sets the end date.
     *
     *  @param date the end date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    protected void setEndDate(org.osid.calendaring.DateTime date) {
        this.temporal.setEndDate(date);
        return;
    }


    /**
     *  Tests if the effectiveness of the enabler is governed by a <code> 
     *  Schedule. </code> If a schedule exists, it is bounded by the effective 
     *  dates of this enabler. If <code> isEffectiveBySchedule() </code> is 
     *  <code> true, </code> <code> isEffectiveByEvent() </code> and <code> 
     *  isEffectiveByCyclicEvent() </code> must be <code> false. </code> 
     *
     *  @return <code> true </code> if the enabler is governed by schedule, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean isEffectiveBySchedule() {
        return (this.schedule != null);
    }


    /**
     *  Gets the schedule <code> Id </code>. 
     *
     *  @return the schedule <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isEffectiveBySchedule() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getScheduleId() {
        if (!isEffectiveBySchedule()) {
            throw new org.osid.IllegalStateException("isEffectiveBySchedule() is false");
        }

        return (this.schedule.getId());
    }


    /**
     *  Gets the schedule. 
     *
     *  @return the schedule 
     *  @throws org.osid.IllegalStateException <code> isEffectiveBySchedule() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.Schedule getSchedule()
        throws org.osid.OperationFailedException {

        if (!isEffectiveBySchedule()) {
            throw new org.osid.IllegalStateException("isEffectiveBySchedule() is false");
        }

        return (this.schedule);
    }


    /**
     *  Sets the schedule and makes this enabler effective by
     *  schedule.
     *
     *  @param schedule the schedule
     *  @throws org.osid.NullArgumentException <code>schedule</code>
     *          is <code>null</code>
     */

    protected void setSchedule(org.osid.calendaring.Schedule schedule) {
        nullarg(schedule, "schedule");
        this.schedule = schedule;
        this.event    = null;
        this.cyclic   = null;
        return;
    }


    /**
     *  Tests if the effectiveness of the enabler is governed by an
     *  <code> Event </code> such that the start and end dates of the
     *  event govern the effectiveness. The event may also be a <code>
     *  RecurringEvent </code> in which case the enabler is effective
     *  for start and end dates of each event in the series If an
     *  event exists, it is bounded by the effective dates of this
     *  enabler. If <code> isEffectiveByEvent() </code> is <code>
     *  true, </code> <code> isEffectiveBySchedule() </code> and
     *  <code> isEffectiveByCyclicEvent() </code> must be <code>
     *  false.  </code>
     *
     *  @return <code> true </code> if the enabler is governed by an event, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean isEffectiveByEvent() {
        return (this.event != null);
    }


    /**
     *  Gets the event <code> Id </code>. 
     *
     *  @return the event <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isEffectiveByEvent() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getEventId() {
        if (!isEffectiveByEvent()) {
            throw new org.osid.IllegalStateException("isEffectiveByEvent() is false");
        }

        return (this.event.getId());
    }


    /**
     *  Gets the event. 
     *
     *  @return the event 
     *  @throws org.osid.IllegalStateException <code> isEffectiveByEvent() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.Event getEvent()
        throws org.osid.OperationFailedException {

        if (!isEffectiveByEvent()) {
            throw new org.osid.IllegalStateException("isEffectiveByEvent() is false");
        }

        return (this.event);
    }


    /**
     *  Sets the event and makes this enabler effective by
     *  event.
     *
     *  @param event the event
     *  @throws org.osid.NullArgumentException <code>event</code>
     *          is <code>null</code>
     */

    protected void setEvent(org.osid.calendaring.Event event) {
        nullarg(event, "event");
        this.schedule = null;
        this.event    = event;
        this.cyclic   = null;
        return;
    }


    /**
     *  Tests if the effectiveness of the enabler is governed by a
     *  <code> CyclicEvent. </code> If a cyclic event exists, it is
     *  evaluated by the accompanying cyclic time period.. If <code>
     *  isEffectiveByCyclicEvent() </code> is <code> true, </code>
     *  <code> isEffectiveBySchedule() </code> and <code>
     *  isEffectiveByEvent() </code> must be <code> false. </code>
     *
     *  @return <code> true </code> if the enabler is governed by a cyclic 
     *          event, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean isEffectiveByCyclicEvent() {
        return (this.cyclic != null);
    }


    /**
     *  Gets the cyclic event <code> Id </code> . 
     *
     *  @return the cyclic event <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> 
     *          isEffectiveByCyclicEvent() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCyclicEventId() {
        if (!isEffectiveByCyclicEvent()) {
            throw new org.osid.IllegalStateException("isEffectiveByCyclicEvent() is false");
        }

        return (this.cyclic.getId());
    }


    /**
     *  Gets the cyclic event. 
     *
     *  @return the cyclic event 
     *  @throws org.osid.IllegalStateException <code> 
     *          isEffectiveByCyclicEvent() </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEvent getCyclicEvent()
        throws org.osid.OperationFailedException {
        
        if (!isEffectiveByCyclicEvent()) {
            throw new org.osid.IllegalStateException("isEffectiveByCyclicEvent() is false");
        }

        return (this.cyclic);
    }


    /**
     *  Sets the cyclic event and makes this enabler effective by
     *  cyclic event.
     *
     *  @param cyclicEvent the cyclic event
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicEvent</code> is <code>null</code>
     */

    protected void setCyclicEvent(org.osid.calendaring.cycle.CyclicEvent cyclicEvent) {
        nullarg(cyclicEvent, "cyclic event");
        this.schedule = null;
        this.event    = null;
        this.cyclic   = cyclicEvent;
        return;
    }


    /**
     *  Tests if the effectiveness of the enabler applies to a demographic 
     *  resource. 
     *
     *  @return <code> true </code> if the rule apples to a demographic. 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean isEffectiveForDemographic() {
        return (this.demographic != null);
    }


    /**
     *  Gets the demographic resource <code> Id </code>. 
     *
     *  @return the resource <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> 
     *          isEffectiveForDemographic() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getDemographicId() {
        if (!isEffectiveForDemographic()) {
            throw new org.osid.IllegalStateException("isEffectiveForDemographic is false");
        }

        return (this.demographic.getId());
    }


    /**
     *  Gets the demographic resource. 
     *
     *  @return the resource representing the demographic 
     *  @throws org.osid.IllegalStateException <code> 
     *          isEffectiveForDemographic() </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getDemographic()
        throws org.osid.OperationFailedException {
        
        if (!isEffectiveForDemographic()) {
            throw new org.osid.IllegalStateException("isEffectiveForDemographic is false");
        }

        return (this.demographic);
    }


    /**
     *  Sets the resource demographic.
     *
     *  @param demographic the resource
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    protected void setDemographic(org.osid.resource.Resource demographic) {
        nullarg(demographic, "demographic");
        this.demographic = demographic;
        return;
    }


    protected class Temporal        
        extends AbstractTemporal
        implements org.osid.Temporal {


        /**
         *  Sets the start date.
         *
         *  @param date the start date
         *  @throws org.osid.NullArgumentException <code>date</code> is
         *          <code>null</code>
         */
        
        protected void setStartDate(org.osid.calendaring.DateTime date) {
            super.setStartDate(date);
            return;
        }


        /**
         *  Sets the end date.
         *
         *  @param date the end date
         *  @throws org.osid.NullArgumentException <code>date</code> is
         *          <code>null</code>
         */
        
        protected void setEndDate(org.osid.calendaring.DateTime date) {
            super.setEndDate(date);
            return;
        }
    }
}
