//
// AbstractRealmQueryInspector.java
//
//     A template for making a RealmQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.personnel.realm.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for realms.
 */

public abstract class AbstractRealmQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQueryInspector
    implements org.osid.personnel.RealmQueryInspector {

    private final java.util.Collection<org.osid.personnel.records.RealmQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the person <code> Id </code> query terms. 
     *
     *  @return the person <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPersonIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the person query terms. 
     *
     *  @return the person terms 
     */

    @OSID @Override
    public org.osid.personnel.PersonQueryInspector[] getPersonTerms() {
        return (new org.osid.personnel.PersonQueryInspector[0]);
    }


    /**
     *  Gets the organziation <code> Id </code> query terms. 
     *
     *  @return the organziation <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOrganizationIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the organziation query terms. 
     *
     *  @return the organziation terms 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationQueryInspector[] getOrganizationTerms() {
        return (new org.osid.personnel.OrganizationQueryInspector[0]);
    }


    /**
     *  Gets the position <code> Id </code> query terms. 
     *
     *  @return the position <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPositionIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the position query terms. 
     *
     *  @return the position terms 
     */

    @OSID @Override
    public org.osid.personnel.PositionQueryInspector[] getPositionTerms() {
        return (new org.osid.personnel.PositionQueryInspector[0]);
    }


    /**
     *  Gets the appointment <code> Id </code> query terms. 
     *
     *  @return the appointment <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAppointmentIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the appointment query terms. 
     *
     *  @return the appointment terms 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentQueryInspector[] getAppointmentTerms() {
        return (new org.osid.personnel.AppointmentQueryInspector[0]);
    }


    /**
     *  Gets the ancestor realm <code> Id </code> query terms. 
     *
     *  @return the ancestor realm <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorRealmIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor realm query terms. 
     *
     *  @return the ancestor realm terms 
     */

    @OSID @Override
    public org.osid.personnel.RealmQueryInspector[] getAncestorRealmTerms() {
        return (new org.osid.personnel.RealmQueryInspector[0]);
    }


    /**
     *  Gets the descendant realm <code> Id </code> query terms. 
     *
     *  @return the descendant realm <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantRealmIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant realm query terms. 
     *
     *  @return the descendant realm terms 
     */

    @OSID @Override
    public org.osid.personnel.RealmQueryInspector[] getDescendantRealmTerms() {
        return (new org.osid.personnel.RealmQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given realm query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a realm implementing the requested record.
     *
     *  @param realmRecordType a realm record type
     *  @return the realm query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>realmRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(realmRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.personnel.records.RealmQueryInspectorRecord getRealmQueryInspectorRecord(org.osid.type.Type realmRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.personnel.records.RealmQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(realmRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(realmRecordType + " is not supported");
    }


    /**
     *  Adds a record to this realm query. 
     *
     *  @param realmQueryInspectorRecord realm query inspector
     *         record
     *  @param realmRecordType realm record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRealmQueryInspectorRecord(org.osid.personnel.records.RealmQueryInspectorRecord realmQueryInspectorRecord, 
                                                   org.osid.type.Type realmRecordType) {

        addRecordType(realmRecordType);
        nullarg(realmRecordType, "realm record type");
        this.records.add(realmQueryInspectorRecord);        
        return;
    }
}
