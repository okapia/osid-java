//
// OfferingConstrainerMiter.java
//
//     Defines an OfferingConstrainer miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.offering.rules.offeringconstrainer;


/**
 *  Defines an <code>OfferingConstrainer</code> miter for use with the builders.
 */

public interface OfferingConstrainerMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidConstrainerMiter,
            org.osid.offering.rules.OfferingConstrainer {


    /**
     *  Sets the can override description.
     *
     *  @param canOverrideDescription <code>true</code> if the
     *         description can be overriden at offering,
     *         <code>false</code> otherwise
     */

    public void setCanOverrideDescription(boolean canOverrideDescription);


    /**
     *  Sets the can override title.
     *
     *  @param canOverrideTitle <code>true</code> if the title can be
     *         overriden at offering, <code>false</code> otherwise
     */

    public void setCanOverrideTitle(boolean canOverrideTitle);


    /**
     *  Sets the can override code.
     *
     *  @param canOverrideCode <code>true</code> if the code can be
     *         overriden at offering, <code>false</code> otherwise
     */

    public void setCanOverrideCode(boolean canOverrideCode);


    /**
     *  Sets the can override time periods.
     *
     *  @param canOverrideTimePeriods <code>true</code> if the time
     *         periods can be overriden at offering,
     *         <code>false</code> otherwise
     */

    public void setCanOverrideTimePeriods(boolean canOverrideTimePeriods);


    /**
     *  Sets the can constrain time periods.
     *
     *  @param canConstrainTimePeriods <code>true</code> if the time
     *         periods can be constrained at offering,
     *         <code>false</code> otherwise
     */

    public void setCanConstrainTimePeriods(boolean canConstrainTimePeriods);


    /**
     *  Sets the can override result options.
     *
     *  @param canOverrideResultOptions <code>true</code> if the
     *         result options can be overidden at offering,
     *         <code>false</code> otherwise
     */

    public void setCanOverrideResultOptions(boolean canOverrideResultOptions);


    /**
     *  Sets the can constrain result options.
     *
     *  @param canConstrainResultOptions <code>true</code> if the
     *         result options can be constrained at offering,
     *         <code>false</code> otherwise
     */

    public void setCanConstrainResultOptions(boolean canConstrainResultOptions);


    /**
     *  Sets the can override sponsors.
     *
     *  @param canOverrideSponsors <code>true</code> if the sponsors
     *         can be overidden at offering, <code>false</code>
     *         otherwise
     */

    public void setCanOverrideSponsors(boolean canOverrideSponsors);


    /**
     *  Sets the can constrain sponsors.
     *
     *  @param canConstrainSponsors <code>true</code> if the
     *         sponsors can be constrained at offering,
     *         <code>false</code> otherwise
     */

    public void setCanConstrainSponsors(boolean canConstrainSponsors);


    /**
     *  Adds an OfferingConstrainer record.
     *
     *  @param record an offeringConstrainer record
     *  @param recordType the type of offeringConstrainer record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addOfferingConstrainerRecord(org.osid.offering.rules.records.OfferingConstrainerRecord record, org.osid.type.Type recordType);
}       


