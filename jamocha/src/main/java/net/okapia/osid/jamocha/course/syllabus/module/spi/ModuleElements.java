//
// ModuleElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.syllabus.module.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class ModuleElements
    extends net.okapia.osid.jamocha.spi.OsidGovernatorElements {


    /**
     *  Gets the ModuleElement Id.
     *
     *  @return the module element Id
     */

    public static org.osid.id.Id getModuleEntityId() {
        return (makeEntityId("osid.course.syllabus.Module"));
    }


    /**
     *  Gets the SyllabusId element Id.
     *
     *  @return the SyllabusId element Id
     */

    public static org.osid.id.Id getSyllabusId() {
        return (makeElementId("osid.course.syllabus.module.SyllabusId"));
    }


    /**
     *  Gets the Syllabus element Id.
     *
     *  @return the Syllabus element Id
     */

    public static org.osid.id.Id getSyllabus() {
        return (makeElementId("osid.course.syllabus.module.Syllabus"));
    }


    /**
     *  Gets the DocetId element Id.
     *
     *  @return the DocetId element Id
     */

    public static org.osid.id.Id getDocetId() {
        return (makeQueryElementId("osid.course.syllabus.module.DocetId"));
    }


    /**
     *  Gets the Docet element Id.
     *
     *  @return the Docet element Id
     */

    public static org.osid.id.Id getDocet() {
        return (makeQueryElementId("osid.course.syllabus.module.Docet"));
    }


    /**
     *  Gets the CourseCatalogId element Id.
     *
     *  @return the CourseCatalogId element Id
     */

    public static org.osid.id.Id getCourseCatalogId() {
        return (makeQueryElementId("osid.course.syllabus.module.CourseCatalogId"));
    }


    /**
     *  Gets the CourseCatalog element Id.
     *
     *  @return the CourseCatalog element Id
     */

    public static org.osid.id.Id getCourseCatalog() {
        return (makeQueryElementId("osid.course.syllabus.module.CourseCatalog"));
    }
}
