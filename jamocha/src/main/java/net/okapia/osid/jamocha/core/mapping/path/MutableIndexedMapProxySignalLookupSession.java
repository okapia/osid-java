//
// MutableIndexedMapProxySignalLookupSession
//
//    Implements a Signal lookup service backed by a collection of
//    signals indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping.path;


/**
 *  Implements a Signal lookup service backed by a collection of
 *  signals. The signals are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some signals may be compatible
 *  with more types than are indicated through these signal
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of signals can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxySignalLookupSession
    extends net.okapia.osid.jamocha.core.mapping.path.spi.AbstractIndexedMapSignalLookupSession
    implements org.osid.mapping.path.SignalLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxySignalLookupSession} with
     *  no signal.
     *
     *  @param map the map
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code map} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxySignalLookupSession(org.osid.mapping.Map map,
                                                       org.osid.proxy.Proxy proxy) {
        setMap(map);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxySignalLookupSession} with
     *  a single signal.
     *
     *  @param map the map
     *  @param  signal an signal
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code map},
     *          {@code signal}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxySignalLookupSession(org.osid.mapping.Map map,
                                                       org.osid.mapping.path.Signal signal, org.osid.proxy.Proxy proxy) {

        this(map, proxy);
        putSignal(signal);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxySignalLookupSession} using
     *  an array of signals.
     *
     *  @param map the map
     *  @param  signals an array of signals
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code map},
     *          {@code signals}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxySignalLookupSession(org.osid.mapping.Map map,
                                                       org.osid.mapping.path.Signal[] signals, org.osid.proxy.Proxy proxy) {

        this(map, proxy);
        putSignals(signals);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxySignalLookupSession} using
     *  a collection of signals.
     *
     *  @param map the map
     *  @param  signals a collection of signals
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code map},
     *          {@code signals}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxySignalLookupSession(org.osid.mapping.Map map,
                                                       java.util.Collection<? extends org.osid.mapping.path.Signal> signals,
                                                       org.osid.proxy.Proxy proxy) {
        this(map, proxy);
        putSignals(signals);
        return;
    }

    
    /**
     *  Makes a {@code Signal} available in this session.
     *
     *  @param  signal a signal
     *  @throws org.osid.NullArgumentException {@code signal{@code 
     *          is {@code null}
     */

    @Override
    public void putSignal(org.osid.mapping.path.Signal signal) {
        super.putSignal(signal);
        return;
    }


    /**
     *  Makes an array of signals available in this session.
     *
     *  @param  signals an array of signals
     *  @throws org.osid.NullArgumentException {@code signals{@code 
     *          is {@code null}
     */

    @Override
    public void putSignals(org.osid.mapping.path.Signal[] signals) {
        super.putSignals(signals);
        return;
    }


    /**
     *  Makes collection of signals available in this session.
     *
     *  @param  signals a collection of signals
     *  @throws org.osid.NullArgumentException {@code signal{@code 
     *          is {@code null}
     */

    @Override
    public void putSignals(java.util.Collection<? extends org.osid.mapping.path.Signal> signals) {
        super.putSignals(signals);
        return;
    }


    /**
     *  Removes a Signal from this session.
     *
     *  @param signalId the {@code Id} of the signal
     *  @throws org.osid.NullArgumentException {@code signalId{@code  is
     *          {@code null}
     */

    @Override
    public void removeSignal(org.osid.id.Id signalId) {
        super.removeSignal(signalId);
        return;
    }    
}
