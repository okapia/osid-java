//
// MutableIndexedMapRaceConstrainerLookupSession
//
//    Implements a RaceConstrainer lookup service backed by a collection of
//    raceConstrainers indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.voting.rules;


/**
 *  Implements a RaceConstrainer lookup service backed by a collection of
 *  race constrainers. The race constrainers are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some race constrainers may be compatible
 *  with more types than are indicated through these race constrainer
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of race constrainers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapRaceConstrainerLookupSession
    extends net.okapia.osid.jamocha.core.voting.rules.spi.AbstractIndexedMapRaceConstrainerLookupSession
    implements org.osid.voting.rules.RaceConstrainerLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapRaceConstrainerLookupSession} with no race constrainers.
     *
     *  @param polls the polls
     *  @throws org.osid.NullArgumentException {@code polls}
     *          is {@code null}
     */

      public MutableIndexedMapRaceConstrainerLookupSession(org.osid.voting.Polls polls) {
        setPolls(polls);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapRaceConstrainerLookupSession} with a
     *  single race constrainer.
     *  
     *  @param polls the polls
     *  @param  raceConstrainer a single raceConstrainer
     *  @throws org.osid.NullArgumentException {@code polls} or
     *          {@code raceConstrainer} is {@code null}
     */

    public MutableIndexedMapRaceConstrainerLookupSession(org.osid.voting.Polls polls,
                                                  org.osid.voting.rules.RaceConstrainer raceConstrainer) {
        this(polls);
        putRaceConstrainer(raceConstrainer);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapRaceConstrainerLookupSession} using an
     *  array of race constrainers.
     *
     *  @param polls the polls
     *  @param  raceConstrainers an array of race constrainers
     *  @throws org.osid.NullArgumentException {@code polls} or
     *          {@code raceConstrainers} is {@code null}
     */

    public MutableIndexedMapRaceConstrainerLookupSession(org.osid.voting.Polls polls,
                                                  org.osid.voting.rules.RaceConstrainer[] raceConstrainers) {
        this(polls);
        putRaceConstrainers(raceConstrainers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapRaceConstrainerLookupSession} using a
     *  collection of race constrainers.
     *
     *  @param polls the polls
     *  @param  raceConstrainers a collection of race constrainers
     *  @throws org.osid.NullArgumentException {@code polls} or
     *          {@code raceConstrainers} is {@code null}
     */

    public MutableIndexedMapRaceConstrainerLookupSession(org.osid.voting.Polls polls,
                                                  java.util.Collection<? extends org.osid.voting.rules.RaceConstrainer> raceConstrainers) {

        this(polls);
        putRaceConstrainers(raceConstrainers);
        return;
    }
    

    /**
     *  Makes a {@code RaceConstrainer} available in this session.
     *
     *  @param  raceConstrainer a race constrainer
     *  @throws org.osid.NullArgumentException {@code raceConstrainer{@code  is
     *          {@code null}
     */

    @Override
    public void putRaceConstrainer(org.osid.voting.rules.RaceConstrainer raceConstrainer) {
        super.putRaceConstrainer(raceConstrainer);
        return;
    }


    /**
     *  Makes an array of race constrainers available in this session.
     *
     *  @param  raceConstrainers an array of race constrainers
     *  @throws org.osid.NullArgumentException {@code raceConstrainers{@code 
     *          is {@code null}
     */

    @Override
    public void putRaceConstrainers(org.osid.voting.rules.RaceConstrainer[] raceConstrainers) {
        super.putRaceConstrainers(raceConstrainers);
        return;
    }


    /**
     *  Makes collection of race constrainers available in this session.
     *
     *  @param  raceConstrainers a collection of race constrainers
     *  @throws org.osid.NullArgumentException {@code raceConstrainer{@code  is
     *          {@code null}
     */

    @Override
    public void putRaceConstrainers(java.util.Collection<? extends org.osid.voting.rules.RaceConstrainer> raceConstrainers) {
        super.putRaceConstrainers(raceConstrainers);
        return;
    }


    /**
     *  Removes a RaceConstrainer from this session.
     *
     *  @param raceConstrainerId the {@code Id} of the race constrainer
     *  @throws org.osid.NullArgumentException {@code raceConstrainerId{@code  is
     *          {@code null}
     */

    @Override
    public void removeRaceConstrainer(org.osid.id.Id raceConstrainerId) {
        super.removeRaceConstrainer(raceConstrainerId);
        return;
    }    
}
