//
// AbstractQueryFamilyLookupSession.java
//
//    An inline adapter that maps a FamilyLookupSession to
//    a FamilyQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.relationship.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a FamilyLookupSession to
 *  a FamilyQuerySession.
 */

public abstract class AbstractQueryFamilyLookupSession
    extends net.okapia.osid.jamocha.relationship.spi.AbstractFamilyLookupSession
    implements org.osid.relationship.FamilyLookupSession {

    private final org.osid.relationship.FamilyQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryFamilyLookupSession.
     *
     *  @param querySession the underlying family query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryFamilyLookupSession(org.osid.relationship.FamilyQuerySession querySession) {
        nullarg(querySession, "family query session");
        this.session = querySession;
        return;
    }



    /**
     *  Tests if this user can perform <code>Family</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupFamilies() {
        return (this.session.canSearchFamilies());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }

     
    /**
     *  Gets the <code>Family</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Family</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Family</code> and
     *  retained for compatibility.
     *
     *  @param  familyId <code>Id</code> of the
     *          <code>Family</code>
     *  @return the family
     *  @throws org.osid.NotFoundException <code>familyId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>familyId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.Family getFamily(org.osid.id.Id familyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.relationship.FamilyQuery query = getQuery();
        query.matchId(familyId, true);
        org.osid.relationship.FamilyList families = this.session.getFamiliesByQuery(query);
        if (families.hasNext()) {
            return (families.getNextFamily());
        } 
        
        throw new org.osid.NotFoundException(familyId + " not found");
    }


    /**
     *  Gets a <code>FamilyList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  families specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Families</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  familyIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Family</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>familyIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.FamilyList getFamiliesByIds(org.osid.id.IdList familyIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.relationship.FamilyQuery query = getQuery();

        try (org.osid.id.IdList ids = familyIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getFamiliesByQuery(query));
    }


    /**
     *  Gets a <code>FamilyList</code> corresponding to the given
     *  family genus <code>Type</code> which does not include
     *  families of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  families or an error results. Otherwise, the returned list
     *  may contain only those families that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  familyGenusType a family genus type 
     *  @return the returned <code>Family</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>familyGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.FamilyList getFamiliesByGenusType(org.osid.type.Type familyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.relationship.FamilyQuery query = getQuery();
        query.matchGenusType(familyGenusType, true);
        return (this.session.getFamiliesByQuery(query));
    }


    /**
     *  Gets a <code>FamilyList</code> corresponding to the given
     *  family genus <code>Type</code> and include any additional
     *  families with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  families or an error results. Otherwise, the returned list
     *  may contain only those families that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  familyGenusType a family genus type 
     *  @return the returned <code>Family</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>familyGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.FamilyList getFamiliesByParentGenusType(org.osid.type.Type familyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.relationship.FamilyQuery query = getQuery();
        query.matchParentGenusType(familyGenusType, true);
        return (this.session.getFamiliesByQuery(query));
    }


    /**
     *  Gets a <code>FamilyList</code> containing the given
     *  family record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  families or an error results. Otherwise, the returned list
     *  may contain only those families that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  familyRecordType a family record type 
     *  @return the returned <code>Family</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>familyRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.FamilyList getFamiliesByRecordType(org.osid.type.Type familyRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.relationship.FamilyQuery query = getQuery();
        query.matchRecordType(familyRecordType, true);
        return (this.session.getFamiliesByQuery(query));
    }


    /**
     *  Gets a <code>FamilyList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known families or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  families that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Family</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.relationship.FamilyList getFamiliesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.relationship.FamilyQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getFamiliesByQuery(query));        
    }

    
    /**
     *  Gets all <code>Families</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  families or an error results. Otherwise, the returned list
     *  may contain only those families that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Families</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.FamilyList getFamilies()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.relationship.FamilyQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getFamiliesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.relationship.FamilyQuery getQuery() {
        org.osid.relationship.FamilyQuery query = this.session.getFamilyQuery();
        return (query);
    }
}
