//
// AbstractHold.java
//
//     Defines a Hold builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.hold.hold.spi;


/**
 *  Defines a <code>Hold</code> builder.
 */

public abstract class AbstractHoldBuilder<T extends AbstractHoldBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRelationshipBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.hold.hold.HoldMiter hold;


    /**
     *  Constructs a new <code>AbstractHoldBuilder</code>.
     *
     *  @param hold the hold to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractHoldBuilder(net.okapia.osid.jamocha.builder.hold.hold.HoldMiter hold) {
        super(hold);
        this.hold = hold;
        return;
    }


    /**
     *  Builds the hold.
     *
     *  @return the new hold
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.hold.Hold build() {
        (new net.okapia.osid.jamocha.builder.validator.hold.hold.HoldValidator(getValidations())).validate(this.hold);
        return (new net.okapia.osid.jamocha.builder.hold.hold.ImmutableHold(this.hold));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the hold miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.hold.hold.HoldMiter getMiter() {
        return (this.hold);
    }


    /**
     *  Sets the issue.
     *
     *  @param issue an issue
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>issue</code> is
     *          <code>null</code>
     */

    public T issue(org.osid.hold.Issue issue) {
        getMiter().setIssue(issue);
        return (self());
    }


    /**
     *  Sets the resource.
     *
     *  @param resource a resource
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    public T resource(org.osid.resource.Resource resource) {
        getMiter().setResource(resource);
        return (self());
    }


    /**
     *  Sets the agent.
     *
     *  @param agent an agent
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    public T agent(org.osid.authentication.Agent agent) {
        getMiter().setAgent(agent);
        return (self());
    }


    /**
     *  Adds a Hold record.
     *
     *  @param record a hold record
     *  @param recordType the type of hold record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.hold.records.HoldRecord record, org.osid.type.Type recordType) {
        getMiter().addHoldRecord(record, recordType);
        return (self());
    }
}       


