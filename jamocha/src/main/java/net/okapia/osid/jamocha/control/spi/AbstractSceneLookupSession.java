//
// AbstractSceneLookupSession.java
//
//    A starter implementation framework for providing a Scene
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Scene
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getScenes(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractSceneLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.control.SceneLookupSession {

    private boolean pedantic      = false;
    private boolean federated     = false;
    private org.osid.control.System system = new net.okapia.osid.jamocha.nil.control.system.UnknownSystem();
    

    /**
     *  Gets the <code>System/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>System Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getSystemId() {
        return (this.system.getId());
    }


    /**
     *  Gets the <code>System</code> associated with this 
     *  session.
     *
     *  @return the <code>System</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.System getSystem()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.system);
    }


    /**
     *  Sets the <code>System</code>.
     *
     *  @param  system the system for this session
     *  @throws org.osid.NullArgumentException <code>system</code>
     *          is <code>null</code>
     */

    protected void setSystem(org.osid.control.System system) {
        nullarg(system, "system");
        this.system = system;
        return;
    }


    /**
     *  Tests if this user can perform <code>Scene</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupScenes() {
        return (true);
    }


    /**
     *  A complete view of the <code>Scene</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeSceneView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Scene</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenarySceneView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include scenes in systems which are children
     *  of this system in the system hierarchy.
     */

    @OSID @Override
    public void useFederatedSystemView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this system only.
     */

    @OSID @Override
    public void useIsolatedSystemView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>Scene</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Scene</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Scene</code> and
     *  retained for compatibility.
     *
     *  @param  sceneId <code>Id</code> of the
     *          <code>Scene</code>
     *  @return the scene
     *  @throws org.osid.NotFoundException <code>sceneId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>sceneId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.Scene getScene(org.osid.id.Id sceneId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.control.SceneList scenes = getScenes()) {
            while (scenes.hasNext()) {
                org.osid.control.Scene scene = scenes.getNextScene();
                if (scene.getId().equals(sceneId)) {
                    return (scene);
                }
            }
        } 

        throw new org.osid.NotFoundException(sceneId + " not found");
    }


    /**
     *  Gets a <code>SceneList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  scenes specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Scenes</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getScenes()</code>.
     *
     *  @param  sceneIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Scene</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>sceneIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.SceneList getScenesByIds(org.osid.id.IdList sceneIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.control.Scene> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = sceneIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getScene(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("scene " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.control.scene.LinkedSceneList(ret));
    }


    /**
     *  Gets a <code>SceneList</code> corresponding to the given
     *  scene genus <code>Type</code> which does not include
     *  scenes of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  scenes or an error results. Otherwise, the returned list
     *  may contain only those scenes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getScenes()</code>.
     *
     *  @param  sceneGenusType a scene genus type 
     *  @return the returned <code>Scene</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>sceneGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.SceneList getScenesByGenusType(org.osid.type.Type sceneGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.control.scene.SceneGenusFilterList(getScenes(), sceneGenusType));
    }


    /**
     *  Gets a <code>SceneList</code> corresponding to the given
     *  scene genus <code>Type</code> and include any additional
     *  scenes with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  scenes or an error results. Otherwise, the returned list
     *  may contain only those scenes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getScenes()</code>.
     *
     *  @param  sceneGenusType a scene genus type 
     *  @return the returned <code>Scene</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>sceneGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.SceneList getScenesByParentGenusType(org.osid.type.Type sceneGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getScenesByGenusType(sceneGenusType));
    }


    /**
     *  Gets a <code>SceneList</code> containing the given
     *  scene record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  scenes or an error results. Otherwise, the returned list
     *  may contain only those scenes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getScenes()</code>.
     *
     *  @param  sceneRecordType a scene record type 
     *  @return the returned <code>Scene</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>sceneRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.SceneList getScenesByRecordType(org.osid.type.Type sceneRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.control.scene.SceneRecordFilterList(getScenes(), sceneRecordType));
    }


    /**
     *  Gets a list of scenes by a setting. <code> </code>
     *
     *  In plenary mode, the returned list contains all known scenes
     *  or an error results. Otherwise, the returned list may contain
     *  only those scenes that are accessible through this session.
     *
     *  @param  settingId a setting <code> Id </code>
     *  @return the returned <code> Scene </code> list
     *  @throws org.osid.NullArgumentException <code> settingId </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.control.SceneList getScenesBySetting(org.osid.id.Id settingId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.control.scene.SceneFilterList(new SettingFilter(settingId), getScenes()));
    }


    /**
     *  Gets all <code>Scenes</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  scenes or an error results. Otherwise, the returned list
     *  may contain only those scenes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Scenes</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.control.SceneList getScenes()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the scene list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of scenes
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.control.SceneList filterScenesOnViews(org.osid.control.SceneList list)
        throws org.osid.OperationFailedException {

        return (list);
    }


    public static class SettingFilter
        implements net.okapia.osid.jamocha.inline.filter.control.scene.SceneFilter {
         
        private final org.osid.id.Id settingId;
         
         
        /**
         *  Constructs a new <code>SettingFilter</code>.
         *
         *  @param settingId the setting to filter
         *  @throws org.osid.NullArgumentException
         *          <code>settingId</code> is <code>null</code>
         */
        
        public SettingFilter(org.osid.id.Id settingId) {
            nullarg(settingId, "setting Id");
            this.settingId = settingId;
            return;
        }

         
        /**
         *  Used by the SceneFilterList to filter the scene list based
         *  on setting.
         *
         *  @param scene the scene
         *  @return <code>true</code> to pass the scene,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.control.Scene scene) {
            try (org.osid.id.IdList settingIds = scene.getSettingIds()) {
                while (settingIds.hasNext()) {
                    try {
                        if (settingIds.getNextId().equals(this.settingId)) {
                            return (true);
                        }
                    } catch (org.osid.OperationFailedException ofe) {}
                }
            }
            
            return (false);
        }
    }
}
