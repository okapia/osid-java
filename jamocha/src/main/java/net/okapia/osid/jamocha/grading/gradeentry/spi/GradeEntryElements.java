//
// GradeEntryElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.gradeentry.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class GradeEntryElements
    extends net.okapia.osid.jamocha.spi.OsidRelationshipElements {


    /**
     *  Gets the GradeEntryElement Id.
     *
     *  @return the grade entry element Id
     */

    public static org.osid.id.Id getGradeEntryEntityId() {
        return (makeEntityId("osid.grading.GradeEntry"));
    }


    /**
     *  Gets the GradebookColumnId element Id.
     *
     *  @return the GradebookColumnId element Id
     */

    public static org.osid.id.Id getGradebookColumnId() {
        return (makeElementId("osid.grading.gradeentry.GradebookColumnId"));
    }


    /**
     *  Gets the GradebookColumn element Id.
     *
     *  @return the GradebookColumn element Id
     */

    public static org.osid.id.Id getGradebookColumn() {
        return (makeElementId("osid.grading.gradeentry.GradebookColumn"));
    }


    /**
     *  Gets the KeyResourceId element Id.
     *
     *  @return the KeyResourceId element Id
     */

    public static org.osid.id.Id getKeyResourceId() {
        return (makeElementId("osid.grading.gradeentry.KeyResourceId"));
    }


    /**
     *  Gets the KeyResource element Id.
     *
     *  @return the KeyResource element Id
     */

    public static org.osid.id.Id getKeyResource() {
        return (makeElementId("osid.grading.gradeentry.KeyResource"));
    }


    /**
     *  Gets the OverriddenCalculatedEntryId element Id.
     *
     *  @return the OverriddenCalculatedEntryId element Id
     */

    public static org.osid.id.Id getOverriddenCalculatedEntryId() {
        return (makeElementId("osid.grading.gradeentry.OverriddenCalculatedEntryId"));
    }


    /**
     *  Gets the OverriddenCalculatedEntry element Id.
     *
     *  @return the OverriddenCalculatedEntry element Id
     */

    public static org.osid.id.Id getOverriddenCalculatedEntry() {
        return (makeElementId("osid.grading.gradeentry.OverriddenCalculatedEntry"));
    }


    /**
     *  Gets the GradeId element Id.
     *
     *  @return the GradeId element Id
     */

    public static org.osid.id.Id getGradeId() {
        return (makeElementId("osid.grading.gradeentry.GradeId"));
    }


    /**
     *  Gets the Grade element Id.
     *
     *  @return the Grade element Id
     */

    public static org.osid.id.Id getGrade() {
        return (makeElementId("osid.grading.gradeentry.Grade"));
    }


    /**
     *  Gets the Score element Id.
     *
     *  @return the Score element Id
     */

    public static org.osid.id.Id getScore() {
        return (makeElementId("osid.grading.gradeentry.Score"));
    }


    /**
     *  Gets the TimeGraded element Id.
     *
     *  @return the TimeGraded element Id
     */

    public static org.osid.id.Id getTimeGraded() {
        return (makeElementId("osid.grading.gradeentry.TimeGraded"));
    }


    /**
     *  Gets the GraderId element Id.
     *
     *  @return the GraderId element Id
     */

    public static org.osid.id.Id getGraderId() {
        return (makeElementId("osid.grading.gradeentry.GraderId"));
    }


    /**
     *  Gets the Grader element Id.
     *
     *  @return the Grader element Id
     */

    public static org.osid.id.Id getGrader() {
        return (makeElementId("osid.grading.gradeentry.Grader"));
    }


    /**
     *  Gets the GradingAgentId element Id.
     *
     *  @return the GradingAgentId element Id
     */

    public static org.osid.id.Id getGradingAgentId() {
        return (makeElementId("osid.grading.gradeentry.GradingAgentId"));
    }


    /**
     *  Gets the GradingAgent element Id.
     *
     *  @return the GradingAgent element Id
     */

    public static org.osid.id.Id getGradingAgent() {
        return (makeElementId("osid.grading.gradeentry.GradingAgent"));
    }


    /**
     *  Gets the Derived element Id.
     *
     *  @return the Derived element Id
     */

    public static org.osid.id.Id getDerived() {
        return (makeElementId("osid.grading.gradeentry.Derived"));
    }


    /**
     *  Gets the OverriddenGradeEntryId element Id.
     *
     *  @return the OverriddenGradeEntryId element Id
     */

    public static org.osid.id.Id getOverriddenGradeEntryId() {
        return (makeQueryElementId("osid.grading.gradeentry.OverriddenGradeEntryId"));
    }


    /**
     *  Gets the OverriddenGradeEntry element Id.
     *
     *  @return the OverriddenGradeEntry element Id
     */

    public static org.osid.id.Id getOverriddenGradeEntry() {
        return (makeQueryElementId("osid.grading.gradeentry.OverriddenGradeEntry"));
    }


    /**
     *  Gets the IgnoredForCalculations element Id.
     *
     *  @return the IgnoredForCalculations element Id
     */

    public static org.osid.id.Id getIgnoredForCalculations() {
        return (makeQueryElementId("osid.grading.gradeentry.IgnoredForCalculations"));
    }


    /**
     *  Gets the GradebookId element Id.
     *
     *  @return the GradebookId element Id
     */

    public static org.osid.id.Id getGradebookId() {
        return (makeQueryElementId("osid.grading.gradeentry.GradebookId"));
    }


    /**
     *  Gets the Gradebook element Id.
     *
     *  @return the Gradebook element Id
     */

    public static org.osid.id.Id getGradebook() {
        return (makeQueryElementId("osid.grading.gradeentry.Gradebook"));
    }
}
