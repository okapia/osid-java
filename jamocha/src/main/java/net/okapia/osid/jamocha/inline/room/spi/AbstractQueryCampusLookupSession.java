//
// AbstractQueryCampusLookupSession.java
//
//    An inline adapter that maps a CampusLookupSession to
//    a CampusQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.room.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a CampusLookupSession to
 *  a CampusQuerySession.
 */

public abstract class AbstractQueryCampusLookupSession
    extends net.okapia.osid.jamocha.room.spi.AbstractCampusLookupSession
    implements org.osid.room.CampusLookupSession {

    private final org.osid.room.CampusQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryCampusLookupSession.
     *
     *  @param querySession the underlying campus query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryCampusLookupSession(org.osid.room.CampusQuerySession querySession) {
        nullarg(querySession, "campus query session");
        this.session = querySession;
        return;
    }



    /**
     *  Tests if this user can perform <code>Campus</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCampuses() {
        return (this.session.canSearchCampuses());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }

     
    /**
     *  Gets the <code>Campus</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Campus</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Campus</code> and
     *  retained for compatibility.
     *
     *  @param  campusId <code>Id</code> of the
     *          <code>Campus</code>
     *  @return the campus
     *  @throws org.osid.NotFoundException <code>campusId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>campusId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.Campus getCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.CampusQuery query = getQuery();
        query.matchId(campusId, true);
        org.osid.room.CampusList campuses = this.session.getCampusesByQuery(query);
        if (campuses.hasNext()) {
            return (campuses.getNextCampus());
        } 
        
        throw new org.osid.NotFoundException(campusId + " not found");
    }


    /**
     *  Gets a <code>CampusList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  campuses specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Campuses</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  campusIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Campus</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>campusIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.CampusList getCampusesByIds(org.osid.id.IdList campusIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.CampusQuery query = getQuery();

        try (org.osid.id.IdList ids = campusIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getCampusesByQuery(query));
    }


    /**
     *  Gets a <code>CampusList</code> corresponding to the given
     *  campus genus <code>Type</code> which does not include
     *  campuses of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  campuses or an error results. Otherwise, the returned list
     *  may contain only those campuses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  campusGenusType a campus genus type 
     *  @return the returned <code>Campus</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>campusGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.CampusList getCampusesByGenusType(org.osid.type.Type campusGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.CampusQuery query = getQuery();
        query.matchGenusType(campusGenusType, true);
        return (this.session.getCampusesByQuery(query));
    }


    /**
     *  Gets a <code>CampusList</code> corresponding to the given
     *  campus genus <code>Type</code> and include any additional
     *  campuses with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  campuses or an error results. Otherwise, the returned list
     *  may contain only those campuses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  campusGenusType a campus genus type 
     *  @return the returned <code>Campus</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>campusGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.CampusList getCampusesByParentGenusType(org.osid.type.Type campusGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.CampusQuery query = getQuery();
        query.matchParentGenusType(campusGenusType, true);
        return (this.session.getCampusesByQuery(query));
    }


    /**
     *  Gets a <code>CampusList</code> containing the given
     *  campus record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  campuses or an error results. Otherwise, the returned list
     *  may contain only those campuses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  campusRecordType a campus record type 
     *  @return the returned <code>Campus</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>campusRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.CampusList getCampusesByRecordType(org.osid.type.Type campusRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.CampusQuery query = getQuery();
        query.matchRecordType(campusRecordType, true);
        return (this.session.getCampusesByQuery(query));
    }


    /**
     *  Gets a <code>CampusList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known campuses or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  campuses that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Campus</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.CampusList getCampusesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.CampusQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getCampusesByQuery(query));        
    }

    
    /**
     *  Gets all <code>Campuses</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  campuses or an error results. Otherwise, the returned list
     *  may contain only those campuses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Campuses</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.CampusList getCampuses()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.CampusQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getCampusesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.room.CampusQuery getQuery() {
        org.osid.room.CampusQuery query = this.session.getCampusQuery();
        return (query);
    }
}
