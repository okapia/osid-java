//
// AbstractMap.java
//
//     Defines a Map builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.mapping.map.spi;


/**
 *  Defines a <code>Map</code> builder.
 */

public abstract class AbstractMapBuilder<T extends AbstractMapBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidCatalogBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.mapping.map.MapMiter map;


    /**
     *  Constructs a new <code>AbstractMapBuilder</code>.
     *
     *  @param map the map to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractMapBuilder(net.okapia.osid.jamocha.builder.mapping.map.MapMiter map) {
        super(map);
        this.map = map;
        return;
    }


    /**
     *  Builds the map.
     *
     *  @return the new map
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.mapping.Map build() {
        (new net.okapia.osid.jamocha.builder.validator.mapping.map.MapValidator(getValidations())).validate(this.map);
        return (new net.okapia.osid.jamocha.builder.mapping.map.ImmutableMap(this.map));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the map miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.mapping.map.MapMiter getMiter() {
        return (this.map);
    }


    /**
     *  Adds a Map record.
     *
     *  @param record a map record
     *  @param recordType the type of map record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.mapping.records.MapRecord record, org.osid.type.Type recordType) {
        getMiter().addMapRecord(record, recordType);
        return (self());
    }
}       


