//
// CookbookElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recipe.cookbook.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class CookbookElements
    extends net.okapia.osid.jamocha.spi.OsidCatalogElements {


    /**
     *  Gets the CookbookElement Id.
     *
     *  @return the cookbook element Id
     */

    public static org.osid.id.Id getCookbookEntityId() {
        return (makeEntityId("osid.recipe.Cookbook"));
    }


    /**
     *  Gets the RecipeId element Id.
     *
     *  @return the RecipeId element Id
     */

    public static org.osid.id.Id getRecipeId() {
        return (makeQueryElementId("osid.recipe.cookbook.RecipeId"));
    }


    /**
     *  Gets the Recipe element Id.
     *
     *  @return the Recipe element Id
     */

    public static org.osid.id.Id getRecipe() {
        return (makeQueryElementId("osid.recipe.cookbook.Recipe"));
    }


    /**
     *  Gets the ProcedureId element Id.
     *
     *  @return the ProcedureId element Id
     */

    public static org.osid.id.Id getProcedureId() {
        return (makeQueryElementId("osid.recipe.cookbook.ProcedureId"));
    }


    /**
     *  Gets the Procedure element Id.
     *
     *  @return the Procedure element Id
     */

    public static org.osid.id.Id getProcedure() {
        return (makeQueryElementId("osid.recipe.cookbook.Procedure"));
    }


    /**
     *  Gets the DirectionId element Id.
     *
     *  @return the DirectionId element Id
     */

    public static org.osid.id.Id getDirectionId() {
        return (makeQueryElementId("osid.recipe.cookbook.DirectionId"));
    }


    /**
     *  Gets the Direction element Id.
     *
     *  @return the Direction element Id
     */

    public static org.osid.id.Id getDirection() {
        return (makeQueryElementId("osid.recipe.cookbook.Direction"));
    }


    /**
     *  Gets the AncestorCookbookId element Id.
     *
     *  @return the AncestorCookbookId element Id
     */

    public static org.osid.id.Id getAncestorCookbookId() {
        return (makeQueryElementId("osid.recipe.cookbook.AncestorCookbookId"));
    }


    /**
     *  Gets the AncestorCookbook element Id.
     *
     *  @return the AncestorCookbook element Id
     */

    public static org.osid.id.Id getAncestorCookbook() {
        return (makeQueryElementId("osid.recipe.cookbook.AncestorCookbook"));
    }


    /**
     *  Gets the DescendantCookbookId element Id.
     *
     *  @return the DescendantCookbookId element Id
     */

    public static org.osid.id.Id getDescendantCookbookId() {
        return (makeQueryElementId("osid.recipe.cookbook.DescendantCookbookId"));
    }


    /**
     *  Gets the DescendantCookbook element Id.
     *
     *  @return the DescendantCookbook element Id
     */

    public static org.osid.id.Id getDescendantCookbook() {
        return (makeQueryElementId("osid.recipe.cookbook.DescendantCookbook"));
    }
}
