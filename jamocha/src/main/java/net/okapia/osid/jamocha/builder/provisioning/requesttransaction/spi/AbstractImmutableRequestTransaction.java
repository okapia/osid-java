//
// AbstractImmutableRequestTransaction.java
//
//     Wraps a mutable RequestTransaction to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.provisioning.requesttransaction.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>RequestTransaction</code> to hide modifiers. This
 *  wrapper provides an immutized RequestTransaction from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying requestTransaction whose state changes are visible.
 */

public abstract class AbstractImmutableRequestTransaction
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRelationship
    implements org.osid.provisioning.RequestTransaction {

    private final org.osid.provisioning.RequestTransaction requestTransaction;


    /**
     *  Constructs a new <code>AbstractImmutableRequestTransaction</code>.
     *
     *  @param requestTransaction the request transaction to immutablize
     *  @throws org.osid.NullArgumentException <code>requestTransaction</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableRequestTransaction(org.osid.provisioning.RequestTransaction requestTransaction) {
        super(requestTransaction);
        this.requestTransaction = requestTransaction;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the broker. 
     *
     *  @return the broker <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getBrokerId() {
        return (this.requestTransaction.getBrokerId());
    }


    /**
     *  Gets the broker. 
     *
     *  @return the broker 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.provisioning.Broker getBroker()
        throws org.osid.OperationFailedException {

        return (this.requestTransaction.getBroker());
    }


    /**
     *  Gets the date this transaction was submitted. The submitted date may 
     *  differ from the effective dates of this provision. 
     *
     *  @return the transaction submit date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getSubmitDate() {
        return (this.requestTransaction.getSubmitDate());
    }


    /**
     *  Gets the <code> Id </code> of the submitter. 
     *
     *  @return the resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getSubmitterId() {
        return (this.requestTransaction.getSubmitterId());
    }


    /**
     *  Gets the submitter. 
     *
     *  @return the resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getSubmitter()
        throws org.osid.OperationFailedException {

        return (this.requestTransaction.getSubmitter());
    }


    /**
     *  Gets the <code> Id </code> of the submitting agent. 
     *
     *  @return the agent <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getSubmittingAgentId() {
        return (this.requestTransaction.getSubmittingAgentId());
    }


    /**
     *  Gets the submitting agent. 
     *
     *  @return the submitting agent 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getSubmittingAgent()
        throws org.osid.OperationFailedException {

        return (this.requestTransaction.getSubmittingAgent());
    }


    /**
     *  Gets the <code> Ids </code> of the requests. 
     *
     *  @return the request <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.id.IdList getRequestIds()
        throws org.osid.OperationFailedException {

        return (this.requestTransaction.getRequestIds());
    }


    /**
     *  Gets the requests. 
     *
     *  @return the requests 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.provisioning.RequestList getRequests()
        throws org.osid.OperationFailedException {

        return (this.requestTransaction.getRequests());
    }


    /**
     *  Gets the request transaction record corresponding to the given <code> 
     *  RequestTransaction </code> record <code> Type. </code> This method is 
     *  used to retrieve an object implementing the requested record. The 
     *  <code> requestTransactionRecordType </code> may be the <code> Type 
     *  </code> returned in <code> getRecordTypes() </code> or any of its 
     *  parents in a <code> Type </code> hierarchy where <code> 
     *  hasRecordType(requestTransactionRecordType) </code> is <code> true 
     *  </code> . 
     *
     *  @param  requestTransactionRecordType the type of request transaction 
     *          record to retrieve 
     *  @return the request transaction record 
     *  @throws org.osid.NullArgumentException <code> 
     *          requestTransactionRecordType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(requestTransactionRecordType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.records.RequestTransactionRecord getRequestTransactionRecord(org.osid.type.Type requestTransactionRecordType)
        throws org.osid.OperationFailedException {

        return (this.requestTransaction.getRequestTransactionRecord(requestTransactionRecordType));
    }
}

