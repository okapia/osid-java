//
// InvariantMapSettingLookupSession
//
//    Implements a Setting lookup service backed by a fixed collection of
//    settings.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.control;


/**
 *  Implements a Setting lookup service backed by a fixed
 *  collection of settings. The settings are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapSettingLookupSession
    extends net.okapia.osid.jamocha.core.control.spi.AbstractMapSettingLookupSession
    implements org.osid.control.SettingLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapSettingLookupSession</code> with no
     *  settings.
     *  
     *  @param system the system
     *  @throws org.osid.NullArgumnetException {@code system} is
     *          {@code null}
     */

    public InvariantMapSettingLookupSession(org.osid.control.System system) {
        setSystem(system);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapSettingLookupSession</code> with a single
     *  setting.
     *  
     *  @param system the system
     *  @param setting a single setting
     *  @throws org.osid.NullArgumentException {@code system} or
     *          {@code setting} is <code>null</code>
     */

      public InvariantMapSettingLookupSession(org.osid.control.System system,
                                               org.osid.control.Setting setting) {
        this(system);
        putSetting(setting);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapSettingLookupSession</code> using an array
     *  of settings.
     *  
     *  @param system the system
     *  @param settings an array of settings
     *  @throws org.osid.NullArgumentException {@code system} or
     *          {@code settings} is <code>null</code>
     */

      public InvariantMapSettingLookupSession(org.osid.control.System system,
                                               org.osid.control.Setting[] settings) {
        this(system);
        putSettings(settings);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapSettingLookupSession</code> using a
     *  collection of settings.
     *
     *  @param system the system
     *  @param settings a collection of settings
     *  @throws org.osid.NullArgumentException {@code system} or
     *          {@code settings} is <code>null</code>
     */

      public InvariantMapSettingLookupSession(org.osid.control.System system,
                                               java.util.Collection<? extends org.osid.control.Setting> settings) {
        this(system);
        putSettings(settings);
        return;
    }
}
