//
// AbstractImmutableOperable
//
//     Defines an immutable wrapper for an Operable OsidObject.
//
//
// Tom Coppeto
// Okapia
// 8 december 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an immutable wrapper for an OSID Object.
 */

public abstract class AbstractImmutableOperable
    implements org.osid.Operable {

    private final org.osid.Operable operable;


    /**
     *  Constructs a new <code>AbstractImmutableOperable</code>.
     *
     *  @param operable
     *  @throws org.osid.NullArgumentException <code>operable</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableOperable(org.osid.Operable operable) {
        nullarg(operable, "operable");
        this.operable = operable;
        return;
    }


    /**
     *  Tests if this rule is active. <code> isActive() </code> is <code> 
     *  true </code> if <code> isEnabled() </code> and <code> isOperational() 
     *  </code> are <code> true </code> and <code> isDisabled() </code> is 
     *  <code> false. </code> 
     *
     *  @return <code> true </code> if this rule is active, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isActive() {
        return (this.operable.isActive());
    }


    /**
     *  Tests if this rule is administravely enabled. Administratively 
     *  enabling overrides any enabling rule which may exist. If this method 
     *  returns <code> true </code> then <code> isDisabled() </code> must 
     *  return <code> false. </code> 
     *
     *  @return <code> true </code> if this rule is enabled, <code> false 
     *          </code> is the active status is determined by other rules 
     */

    @OSID @Override
    public boolean isEnabled() {
        return (this.operable.isEnabled());
    }


    /**
     *  Tests if this rule is administravely disabled. Administratively 
     *  disabling overrides any disabling rule which may exist. If this method 
     *  returns <code> true </code> then <code> isDisabled() </code> must 
     *  return <code> false. </code> 
     *
     *  @return <code> true </code> if this rule is disabled, <code> false 
     *          </code> is the active status is determined by other rules 
     */

    @OSID @Override
    public boolean isDisabled() {
        return (this.operable.isDisabled());
    }


    /**
     *  Tests if this rule is operational in that all rules pertaining to 
     *  this operation except for an administrative disable are <code> true. 
     *  </code> 
     *
     *  @return <code> true </code> if this rule is operational, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean isOperational() {
        return (this.operable.isOperational());
    }


    /**
     *  Determines if the given <code> Id </code> is equal to this
     *  one. Two Ids are equal if the namespace, authority and
     *  identifier components are equal. The identifier is case
     *  sensitive while the namespace and authority strings are not
     *  case sensitive.
     *
     *  @param  obj an operable to compare
     *  @return <code> true </code> if the given operable is equal to
     *          this <code>Id</code>, <code> false </code> otherwise
     */

    @Override
    public boolean equals(Object obj) {
        return (this.operable.equals(obj));
    }


    /**
     *  Returns a hash code value for this <code>Operable</code>
     *  based on the <code>Id</code>.
     *
     *  @return a hash code value for this operable
     */

    @Override
    public int hashCode() {
        return (this.operable.hashCode());
    }


    /**
     *  Returns a string representation of this Operable.
     *
     *  @return a string
     */

    @Override
    public String toString() {
        return (this.operable.toString());
    }
}
