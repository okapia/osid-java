//
// AbstractRequestTransactionLookupSession.java
//
//    A starter implementation framework for providing a RequestTransaction
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a RequestTransaction
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getRequestTransactions(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractRequestTransactionLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.provisioning.RequestTransactionLookupSession {

    private boolean pedantic      = false;
    private boolean effectiveonly = false;
    private boolean federated     = false;
    private org.osid.provisioning.Distributor distributor = new net.okapia.osid.jamocha.nil.provisioning.distributor.UnknownDistributor();
    

    /**
     *  Gets the <code>Distributor/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Distributor Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDistributorId() {
        return (this.distributor.getId());
    }


    /**
     *  Gets the <code>Distributor</code> associated with this
     *  session.
     *
     *  @return the <code>Distributor</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Distributor getDistributor()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.distributor);
    }


    /**
     *  Sets the <code>Distributor</code>.
     *
     *  @param  distributor the distributor for this session
     *  @throws org.osid.NullArgumentException <code>distributor</code>
     *          is <code>null</code>
     */

    protected void setDistributor(org.osid.provisioning.Distributor distributor) {
        nullarg(distributor, "distributor");
        this.distributor = distributor;
        return;
    }


    /**
     *  Tests if this user can perform <code>RequestTransaction</code>
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupRequestTransactions() {
        return (true);
    }


    /**
     *  A complete view of the <code>RequestTransaction</code> returns
     *  is desired.  Methods will return what is requested or result
     *  in an error. This view is used when greater precision is
     *  desired at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeRequestTransactionView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>RequestTransaction</code> returns
     *  is desired.  Methods will return what is requested or result
     *  in an error. This view is used when greater precision is
     *  desired at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryRequestTransactionView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include request transactions in distributors which
     *  are children of this distributor in the distributor hierarchy.
     */

    @OSID @Override
    public void useFederatedDistributorView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this distributor only.
     */

    @OSID @Override
    public void useIsolatedDistributorView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only request transactions whose effective dates are current
     *  are returned by methods in this session.
     */

    @OSID @Override
    public void useEffectiveRequestTransactionView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All request transactions of any effective dates are returned
     *  by all methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveRequestTransactionView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>RequestTransaction</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>RequestTransaction</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>RequestTransaction</code> and retained for
     *  compatibility.
     *
     *  In effective mode, request transactions are returned that are
     *  currently effective.  In any effective mode, effective request
     *  transactions and those currently expired are returned.
     *
     *  @param  requestTransactionId <code>Id</code> of the
     *          <code>RequestTransaction</code>
     *  @return the request transaction
     *  @throws org.osid.NotFoundException <code>requestTransactionId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>requestTransactionId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransaction getRequestTransaction(org.osid.id.Id requestTransactionId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.provisioning.RequestTransactionList requestTransactions = getRequestTransactions()) {
            while (requestTransactions.hasNext()) {
                org.osid.provisioning.RequestTransaction requestTransaction = requestTransactions.getNextRequestTransaction();
                if (requestTransaction.getId().equals(requestTransactionId)) {
                    return (requestTransaction);
                }
            }
        } 

        throw new org.osid.NotFoundException(requestTransactionId + " not found");
    }


    /**
     *  Gets a <code>RequestTransactionList</code> corresponding to
     *  the given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  requestTransactions specified in the <code>Id</code> list, in
     *  the order of the list, including duplicates, or an error
     *  results if an <code>Id</code> in the supplied list is not
     *  found or inaccessible. Otherwise, inaccessible
     *  <code>RequestTransactions</code> may be omitted from the list
     *  and may present the elements in any order including returning
     *  a unique set.
     *
     *  In effective mode, request transactions are returned that are
     *  currently effective.  In any effective mode, effective request
     *  transactions and those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getRequestTransactions()</code>.
     *
     *  @param  requestTransactionIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>RequestTransaction</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>requestTransactionIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransactionList getRequestTransactionsByIds(org.osid.id.IdList requestTransactionIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.provisioning.RequestTransaction> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = requestTransactionIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getRequestTransaction(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("request transaction " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.provisioning.requesttransaction.LinkedRequestTransactionList(ret));
    }


    /**
     *  Gets a <code>RequestTransactionList</code> corresponding to
     *  the given request transaction genus <code>Type</code> which
     *  does not include request transactions of types derived from
     *  the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known request
     *  transactions or an error results. Otherwise, the returned list
     *  may contain only those request transactions that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In effective mode, request transactions are returned that are
     *  currently effective.  In any effective mode, effective request
     *  transactions and those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getRequestTransactions()</code>.
     *
     *  @param  requestTransactionGenusType a requestTransaction genus type 
     *  @return the returned <code>RequestTransaction</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>requestTransactionGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransactionList getRequestTransactionsByGenusType(org.osid.type.Type requestTransactionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.provisioning.requesttransaction.RequestTransactionGenusFilterList(getRequestTransactions(), requestTransactionGenusType));
    }


    /**
     *  Gets a <code>RequestTransactionList</code> corresponding to
     *  the given request transaction genus <code>Type</code> and
     *  include any additional request transactions with genus types
     *  derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known request
     *  transactions or an error results. Otherwise, the returned list
     *  may contain only those request transactions that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In effective mode, request transactions are returned that are
     *  currently effective.  In any effective mode, effective request
     *  transactions and those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getRequestTransactions()</code>.
     *
     *  @param  requestTransactionGenusType a requestTransaction genus type 
     *  @return the returned <code>RequestTransaction</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>requestTransactionGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransactionList getRequestTransactionsByParentGenusType(org.osid.type.Type requestTransactionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getRequestTransactionsByGenusType(requestTransactionGenusType));
    }


    /**
     *  Gets a <code>RequestTransactionList</code> containing the
     *  given request transaction record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known request
     *  transactions or an error results. Otherwise, the returned list
     *  may contain only those request transactions that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In effective mode, request transactions are returned that are
     *  currently effective.  In any effective mode, effective request
     *  transactions and those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getRequestTransactions()</code>.
     *
     *  @param  requestTransactionRecordType a requestTransaction record type 
     *  @return the returned <code>RequestTransaction</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>requestTransactionRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransactionList getRequestTransactionsByRecordType(org.osid.type.Type requestTransactionRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.provisioning.requesttransaction.RequestTransactionRecordFilterList(getRequestTransactions(), requestTransactionRecordType));
    }


    /**
     *  Gets a <code>RequestTransactionList</code> effective during
     *  the entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known request
     *  transactions or an error results. Otherwise, the returned list
     *  may contain only those request transactions that are
     *  accessible through this session.
     *  
     *  In active mode, request transactions are returned that are
     *  currently active. In any status mode, active and inactive
     *  request transactions are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>RequestTransaction</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.provisioning.RequestTransactionList getRequestTransactionsOnDate(org.osid.calendaring.DateTime from, 
                                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.provisioning.requesttransaction.TemporalRequestTransactionFilterList(getRequestTransactions(), from, to));
    }
        

    /**
     *  Gets a list of request transactions corresponding to a request
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known request
     *  transactions or an error results. Otherwise, the returned list
     *  may contain only those request transactions that are
     *  accessible through this session.
     *
     *  In effective mode, request transactions are returned that are
     *  currently effective.  In any effective mode, effective request
     *  transactions and those currently expired are returned.
     *
     *  @param  requestId the <code>Id</code> of the request
     *  @return the returned <code>RequestTransactionList</code>
     *  @throws org.osid.NullArgumentException <code>requestId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.provisioning.RequestTransactionList getRequestTransactionsByRequest(org.osid.id.Id requestId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.provisioning.RequestTransaction> ret = new java.util.ArrayList<>();

        try (org.osid.provisioning.RequestTransactionList transactions = getRequestTransactions()) {
            while (transactions.hasNext()) {
                org.osid.provisioning.RequestTransaction transaction = transactions.getNextRequestTransaction();
                try (org.osid.id.IdList ids = transaction.getRequestIds()) {
                    while (ids.hasNext()) {
                        if (ids.getNextId().equals(requestId)) {
                            ret.add(transaction);
                        }
                    }
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.provisioning.requesttransaction.LinkedRequestTransactionList(ret));
    }


    /**
     *  Gets a list of request transactions corresponding to a
     *  submitter.
     *
     *  In plenary mode, the returned list contains all known request
     *  transactions or an error results. Otherwise, the returned list
     *  may contain only those request transactions that are
     *  accessible through this session.
     *
     *  In effective mode, request transactions are returned that are
     *  currently effective.  In any effective mode, effective request
     *  transactions and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @return the returned <code>RequestTransactionList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.provisioning.RequestTransactionList getRequestTransactionsForSubmitter(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.provisioning.requesttransaction.RequestTransactionFilterList(new SubmitterFilter(resourceId), getRequestTransactions()));
    }


    /**
     *  Gets a list of request transactions corresponding to a
     *  submitter <code>Id</code> and effective during the entire
     *  given date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known request
     *  transactions or an error results. Otherwise, the returned list
     *  may contain only those request transactions that are
     *  accessible through this session.
     *
     *  In effective mode, request transactions are returned that are
     *  currently effective.  In any effective mode, effective request
     *  transactions and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>RequestTransactionList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransactionList getRequestTransactionsForSubmitterOnDate(org.osid.id.Id resourceId,
                                                                                                 org.osid.calendaring.DateTime from,
                                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.provisioning.requesttransaction.TemporalRequestTransactionFilterList(getRequestTransactionsForSubmitter(resourceId), from, to));
    }


    /**
     *  Gets a list of request transactions corresponding to a broker.
     *
     *  In plenary mode, the returned list contains all known request
     *  transactions or an error results. Otherwise, the returned list
     *  may contain only those request transactions that are
     *  accessible through this session.
     *
     *  In effective mode, request transactions are returned that are
     *  currently effective.  In any effective mode, effective request
     *  transactions and those currently expired are returned.
     *
     *  @param  brokerId the <code>Id</code> of the resource
     *  @return the returned <code>RequestTransactionList</code>
     *  @throws org.osid.NullArgumentException <code>brokerId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.provisioning.RequestTransactionList getRequestTransactionsForBroker(org.osid.id.Id brokerId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.provisioning.requesttransaction.RequestTransactionFilterList(new BrokerFilter(brokerId), getRequestTransactions()));
    }


    /**
     *  Gets a list of request transactions corresponding to a
     *  broker <code>Id</code> and effective during the entire
     *  given date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known request
     *  transactions or an error results. Otherwise, the returned list
     *  may contain only those request transactions that are
     *  accessible through this session.
     *
     *  In effective mode, request transactions are returned that are
     *  currently effective.  In any effective mode, effective request
     *  transactions and those currently expired are returned.
     *
     *  @param  brokerId the <code>Id</code> of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>RequestTransactionList</code>
     *  @throws org.osid.NullArgumentException <code>brokerId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransactionList getRequestTransactionsForBrokerOnDate(org.osid.id.Id brokerId,
                                                                                              org.osid.calendaring.DateTime from,
                                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.provisioning.requesttransaction.TemporalRequestTransactionFilterList(getRequestTransactionsForBroker(brokerId), from, to));
    }


    /**
     *  Gets a list of request transactions corresponding to submitter
     *  and broker<code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known request
     *  transactions or an error results. Otherwise, the returned list
     *  may contain only those request transactions that are
     *  accessible through this session.
     *
     *  In effective mode, request transactions are returned that are
     *  currently effective.  In any effective mode, effective request
     *  transactions and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the submitter
     *  @param  brokerId the <code>Id</code> of the broker
     *  @return the returned <code>RequestTransactionList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>brokerId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.provisioning.RequestTransactionList getRequestTransactionsForSubmitterAndBroker(org.osid.id.Id resourceId,
                                                                                                    org.osid.id.Id brokerId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.provisioning.requesttransaction.RequestTransactionFilterList(new BrokerFilter(brokerId), getRequestTransactionsForSubmitter(resourceId)));
    }


    /**
     *  Gets a list of request transactions corresponding to submitter
     *  and broker <code>Ids</code> and effective during the entire
     *  given date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known request
     *  transactions or an error results. Otherwise, the returned list
     *  may contain only those request transactions that are
     *  accessible through this session.
     *
     *  In effective mode, request transactions are returned that are
     *  currently effective.  In any effective mode, effective request
     *  transactions and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  brokerId the <code>Id</code> of the broker
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>RequestTransactionList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code>, <code>brokerId</code>
     *          <code>resourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransactionList getRequestTransactionsForSubmitterAndBrokerOnDate(org.osid.id.Id resourceId,
                                                                                                          org.osid.id.Id brokerId,
                                                                                                          org.osid.calendaring.DateTime from,
                                                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.provisioning.requesttransaction.TemporalRequestTransactionFilterList(getRequestTransactionsForSubmitterAndBroker(resourceId, brokerId), from, to));
    }


    /**
     *  Gets all <code>RequestTransactions</code>. 
     *
     *  In plenary mode, the returned list contains all known request
     *  transactions or an error results. Otherwise, the returned list
     *  may contain only those request transactions that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In effective mode, request transactions are returned that are
     *  currently effective.  In any effective mode, effective request
     *  transactions and those currently expired are returned.
     *
     *  @return a list of <code>RequestTransactions</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.provisioning.RequestTransactionList getRequestTransactions()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the request transaction list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of request transactions
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.provisioning.RequestTransactionList filterRequestTransactionsOnViews(org.osid.provisioning.RequestTransactionList list)
        throws org.osid.OperationFailedException {

        org.osid.provisioning.RequestTransactionList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.provisioning.requesttransaction.EffectiveRequestTransactionFilterList(ret);
        }

        return (ret);
    }


    public static class BrokerFilter
        implements net.okapia.osid.jamocha.inline.filter.provisioning.requesttransaction.RequestTransactionFilter {
         
        private final org.osid.id.Id brokerId;
         
         
        /**
         *  Constructs a new <code>BrokerFilter</code>.
         *
         *  @param brokerId the broker to filter
         *  @throws org.osid.NullArgumentException
         *          <code>brokerId</code> is <code>null</code>
         */
        
        public BrokerFilter(org.osid.id.Id brokerId) {
            nullarg(brokerId, "broker Id");
            this.brokerId = brokerId;
            return;
        }

         
        /**
         *  Used by the RequestTransactionFilterList to filter the 
         *  request transaction list based on broker.
         *
         *  @param requestTransaction the request transaction
         *  @return <code>true</code> to pass the request transaction,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.provisioning.RequestTransaction requestTransaction) {
            return (requestTransaction.getBrokerId().equals(this.brokerId));
        }
    }


    public static class SubmitterFilter
        implements net.okapia.osid.jamocha.inline.filter.provisioning.requesttransaction.RequestTransactionFilter {
         
        private final org.osid.id.Id resourceId;
         
         
        /**
         *  Constructs a new <code>SubmitterFilter</code>.
         *
         *  @param resourceId the resource to filter
         *  @throws org.osid.NullArgumentException
         *          <code>resourceId</code> is <code>null</code>
         */
        
        public SubmitterFilter(org.osid.id.Id resourceId) {
            nullarg(resourceId, "resource Id");
            this.resourceId = resourceId;
            return;
        }

         
        /**
         *  Used by the RequestTransactionFilterList to filter the
         *  request transaction list based on resource.
         *
         *  @param requestTransaction the request transaction
         *  @return <code>true</code> to pass the request transaction,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.provisioning.RequestTransaction requestTransaction) {
            return (requestTransaction.getSubmitterId().equals(this.resourceId));
        }
    }
}
