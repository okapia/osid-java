//
// AbstractResourceLookupSession.java
//
//    A starter implementation framework for providing a Resource
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resource.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Resource lookup
 *  service.
 *
 *  Although this abstract class requires only the implementation of
 *  getResources(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractResourceLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.resource.ResourceLookupSession {

    private boolean pedantic  = false;
    private boolean federated = false;
    private org.osid.resource.Bin bin = new net.okapia.osid.jamocha.nil.resource.bin.UnknownBin();
    

    /**
     *  Gets the <code>Bin/code> <code>Id</code> associated with this
     *  session.
     *
     *  @return the <code>Bin Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBinId() {
        return (this.bin.getId());
    }


    /**
     *  Gets the <code>Bin</code> associated with this session.
     *
     *  @return the <code>Bin</code> associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.Bin getBin()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.bin);
    }


    /**
     *  Sets the <code>Bin</code>.
     *
     *  @param  bin the bin for this session
     *  @throws org.osid.NullArgumentException <code>bin</code>
     *          is <code>null</code>
     */

    protected void setBin(org.osid.resource.Bin bin) {
        nullarg(bin, "bin");
        this.bin = bin;
        return;
    }


    /**
     *  Tests if this user can perform <code>Resource</code> lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupResources() {
        return (true);
    }


    /**
     *  A complete view of the <code>Resource</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeResourceView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Resource</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryResourceView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include resources in bins which are children of this
     *  bin in the bin hierarchy.
     */

    @OSID @Override
    public void useFederatedBinView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this bin only.
     */

    @OSID @Override
    public void useIsolatedBinView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>Resource</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Resource</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Resource</code> and
     *  retained for compatibility.
     *
     *  @param resourceId <code>Id</code> of the <code>Resource</code>
     *  @return the resource
     *  @throws org.osid.NotFoundException <code>resourceId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>resourceId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.Resource getResource(org.osid.id.Id resourceId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.resource.ResourceList resources = getResources()) {
            while (resources.hasNext()) {
                org.osid.resource.Resource resource = resources.getNextResource();
                if (resource.getId().equals(resourceId)) {
                    return (resource);
                }
            }
        } 

        throw new org.osid.NotFoundException(resourceId + " not found");
    }


    /**
     *  Gets a <code>ResourceList</code> corresponding to the given
     *  <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  resources specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Resources</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getResources()</code>.
     *
     *  @param  resourceIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Resource</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getResourcesByIds(org.osid.id.IdList resourceIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.resource.Resource> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = resourceIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getResource(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("resource " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.resource.resource.LinkedResourceList(ret));
    }


    /**
     *  Gets a <code>ResourceList</code> corresponding to the given
     *  resource genus <code>Type</code> which does not include
     *  resources of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  resources or an error results. Otherwise, the returned list
     *  may contain only those resources that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getResources()</code>.
     *
     *  @param  resourceGenusType a resource genus type 
     *  @return the returned <code>Resource</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>resourceGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getResourcesByGenusType(org.osid.type.Type resourceGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resource.resource.ResourceGenusFilterList(getResources(), resourceGenusType));
    }


    /**
     *  Gets a <code>ResourceList</code> corresponding to the given
     *  resource genus <code>Type</code> and include any additional
     *  resources with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  resources or an error results. Otherwise, the returned list
     *  may contain only those resources that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getResources()</code>.
     *
     *  @param  resourceGenusType a resource genus type 
     *  @return the returned <code>Resource</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>resourceGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getResourcesByParentGenusType(org.osid.type.Type resourceGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getResourcesByGenusType(resourceGenusType));
    }


    /**
     *  Gets a <code>ResourceList</code> containing the given resource
     *  record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known
     *  resources or an error results. Otherwise, the returned list
     *  may contain only those resources that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getResources()</code>.
     *
     *  @param  resourceRecordType a resource record type 
     *  @return the returned <code>Resource</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>resourceRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getResourcesByRecordType(org.osid.type.Type resourceRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resource.resource.ResourceRecordFilterList(getResources(), resourceRecordType));
    }


    /**
     *  Gets all <code>Resources</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  resources or an error results. Otherwise, the returned list
     *  may contain only those resources that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Resources</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.resource.ResourceList getResources()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the resource list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of resources
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.resource.ResourceList filterResourcesOnViews(org.osid.resource.ResourceList list)
        throws org.osid.OperationFailedException {

        return (list);
    }
}
