//
// AbstractImmutableAssessmentPart.java
//
//     Wraps a mutable AssessmentPart to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.assessment.authoring.assessmentpart.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>AssessmentPart</code> to hide modifiers. This
 *  wrapper provides an immutized AssessmentPart from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying assessmentPart whose state changes are visible.
 */

public abstract class AbstractImmutableAssessmentPart
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOperableOsidObject
    implements org.osid.assessment.authoring.AssessmentPart {

    private final org.osid.assessment.authoring.AssessmentPart assessmentPart;


    /**
     *  Constructs a new <code>AbstractImmutableAssessmentPart</code>.
     *
     *  @param assessmentPart the assessment part to immutablize
     *  @throws org.osid.NullArgumentException <code>assessmentPart</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableAssessmentPart(org.osid.assessment.authoring.AssessmentPart assessmentPart) {
        super(assessmentPart);
        this.assessmentPart = assessmentPart;
        return;
    }


    /**
     *  Tests if this <code> Containable </code> is sequestered in
     *  that it should not appear outside of its aggregated
     *  assessment part.
     *
     *  @return <code> true </code> if this containable is
     *          sequestered, <code> false </code> if this containable
     *          may appear outside its aggregate
     */

    @OSID @Override
    public boolean isSequestered() {
        return (this.assessmentPart.isSequestered());
    }


    /**
     *  Gets the assessment <code> Id </code> to which this rule belongs. 
     *
     *  @return <code> Id </code> of an assessment 
     */

    @OSID @Override
    public org.osid.id.Id getAssessmentId() {
        return (this.assessmentPart.getAssessmentId());
    }


    /**
     *  Gets the assessment to which this rule belongs. 
     *
     *  @return an assessment 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.assessment.Assessment getAssessment()
        throws org.osid.OperationFailedException {

        return (this.assessmentPart.getAssessment());
    }


    /**
     *  Tests if this assessment part belongs to a parent assessment part. 
     *
     *  @return <code> true </code> if this part has a parent, <code> false 
     *          </code> if a root 
     */

    @OSID @Override
    public boolean hasParentPart() {
        return (this.assessmentPart.hasParentPart());
    }


    /**
     *  Gets the parent assessment <code> Id. </code> 
     *
     *  @return <code> Id </code> of an assessment 
     *  @throws org.osid.IllegalStateException <code> hasParentPart() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAssessmentPartId() {
        return (this.assessmentPart.getAssessmentPartId());
    }


    /**
     *  Gets the parent assessment. 
     *
     *  @return the parent assessment part 
     *  @throws org.osid.IllegalStateException <code> hasParentPart() </code> 
     *          is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPart getAssessmentPart()
        throws org.osid.OperationFailedException {

        return (this.assessmentPart.getAssessmentPart());
    }


    /**
     *  Tests if this part should be visible as a section in an assessment. If 
     *  visible, this part will appear to the user as a separate section of 
     *  the assessment. Typically, a section may not be under a non-sectioned 
     *  part. 
     *
     *  @return <code> true </code> if this part is a section, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isSection() {
        return (this.assessmentPart.isSection());
    }


    /**
     *  Gets an integral weight factor for this assessment part used for 
     *  scoring. The percentage weight for this part is this weight divided by 
     *  the sum total of all the weights in the assessment. 
     *
     *  @return the weight 
     */

    @OSID @Override
    public long getWeight() {
        return (this.assessmentPart.getWeight());
    }


    /**
     *  Gets the allocated time for this part. The allocated time may be used 
     *  to assign fixed time limits to each item or can be used to estimate 
     *  the total assessment time. 
     *
     *  @return allocated time 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getAllocatedTime() {
        return (this.assessmentPart.getAllocatedTime());
    }


    /**
     *  Gets any child assessment part <code> Ids. </code> 
     *
     *  @return <code> Ids </code> of the child assessment parts 
     */

    @OSID @Override
    public org.osid.id.IdList getChildAssessmentPartIds() {
        return (this.assessmentPart.getChildAssessmentPartIds());
    }


    /**
     *  Gets any child assessment parts. 
     *
     *  @return the child assessment parts 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartList getChildAssessmentParts()
        throws org.osid.OperationFailedException {

        return (this.assessmentPart.getChildAssessmentParts());
    }


    /**
     *  Gets the assessment part record corresponding to the given <code> 
     *  AssessmentPart </code> record <code> Type. </code> This method is used 
     *  to retrieve an object implementing the requested record. The <code> 
     *  assessmentPartRecordType </code> may be the <code> Type </code> 
     *  returned in <code> getRecordTypes() </code> or any of its parents in a 
     *  <code> Type </code> hierarchy where <code> 
     *  hasRecordType(assessmentPartRecordType) </code> is <code> true </code> 
     *  . 
     *
     *  @param  assessmentPartRecordType the type of the record to retrieve 
     *  @return the assessment part record 
     *  @throws org.osid.NullArgumentException <code> assessmentPartRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(assessmentPartRecordType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.records.AssessmentPartRecord getAssessmentPartRecord(org.osid.type.Type assessmentPartRecordType)
        throws org.osid.OperationFailedException {

        return (this.assessmentPart.getAssessmentPartRecord(assessmentPartRecordType));
    }
}

