//
// AbstractActivityLookupSession.java
//
//    A starter implementation framework for providing an Activity
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing an Activity
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getActivities(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractActivityLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.course.ActivityLookupSession {

    private boolean pedantic      = false;
    private boolean federated     = false;
    private boolean effectiveonly = false;
    private boolean normalized    = false;
    private org.osid.course.CourseCatalog courseCatalog = new net.okapia.osid.jamocha.nil.course.coursecatalog.UnknownCourseCatalog();
    

    /**
     *  Gets the <code>CourseCatalog/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>CourseCatalog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.courseCatalog.getId());
    }


    /**
     *  Gets the <code>CourseCatalog</code> associated with this 
     *  session.
     *
     *  @return the <code>CourseCatalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.courseCatalog);
    }


    /**
     *  Sets the <code>CourseCatalog</code>.
     *
     *  @param  courseCatalog the course catalog for this session
     *  @throws org.osid.NullArgumentException <code>courseCatalog</code>
     *          is <code>null</code>
     */

    protected void setCourseCatalog(org.osid.course.CourseCatalog courseCatalog) {
        nullarg(courseCatalog, "course catalog");
        this.courseCatalog = courseCatalog;
        return;
    }


    /**
     *  Tests if this user can perform <code>Activity</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupActivities() {
        return (true);
    }


    /**
     *  A complete view of the <code>Activity</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeActivityView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Activity</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryActivityView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include activity entries in course catalogs which are
     *  children of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only activities whose effective dates are current are returned
     *  by methods in this session.
     */

    @OSID @Override
    public void useEffectiveActivityView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All activities of any effective dates are returned by
     *  all methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveActivityView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  A normalized view uses a single <code>Activity</code> to represent a
     *  set of recurring activities.
     */

    @OSID @Override
    public void useNormalizedActivityView() {
        this.normalized = true;
        return;
    }


    /**
     *  A denormalized view expands activities into a series of <code>
     *  Activities.</code>
     */

    @OSID @Override
    public void useDenormalizedActivityView() {
        this.normalized = false;
        return;
    }


    /**
     *  Tests if a normalized or denormalized view is set.
     *
     *  @return <code>true</code> if normalized</code>,
     *          <code>false</code> if denormalized
     */

    protected boolean isNormalized() {
        return (this.normalized);
    }


    /**
     *  Gets the <code>Activity</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Activity</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Activity</code> and
     *  retained for compatibility.
     *
     *  In effective mode, activities are returned that are currently
     *  effective.  In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  @param  activityId <code>Id</code> of the
     *          <code>Activity</code>
     *  @return the activity
     *  @throws org.osid.NotFoundException <code>activityId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>activityId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.Activity getActivity(org.osid.id.Id activityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.course.ActivityList activities = getActivities()) {
            while (activities.hasNext()) {
                org.osid.course.Activity activity = activities.getNextActivity();
                if (activity.getId().equals(activityId)) {
                    return (activity);
                }
            }
        } 

        throw new org.osid.NotFoundException(activityId + " not found");
    }


    /**
     *  Gets an <code>ActivityList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  activities specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Activities</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, activities are returned that are currently
     *  effective.  In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getActivities()</code>.
     *
     *  @param  activityIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Activity</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>activityIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.ActivityList getActivitiesByIds(org.osid.id.IdList activityIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.course.Activity> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = activityIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getActivity(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("activity " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.course.activity.LinkedActivityList(ret));
    }


    /**
     *  Gets an <code>ActivityList</code> corresponding to the given
     *  activity genus <code>Type</code> which does not include
     *  activities of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, activities are returned that are currently
     *  effective.  In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getActivities()</code>.
     *
     *  @param  activityGenusType an activity genus type 
     *  @return the returned <code>Activity</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>activityGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.ActivityList getActivitiesByGenusType(org.osid.type.Type activityGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.activity.ActivityGenusFilterList(getActivities(), activityGenusType));
    }


    /**
     *  Gets an <code>ActivityList</code> corresponding to the given
     *  activity genus <code>Type</code> and include any additional
     *  activities with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, activities are returned that are currently
     *  effective.  In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getActivities()</code>.
     *
     *  @param  activityGenusType an activity genus type 
     *  @return the returned <code>Activity</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>activityGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.ActivityList getActivitiesByParentGenusType(org.osid.type.Type activityGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getActivitiesByGenusType(activityGenusType));
    }


    /**
     *  Gets an <code>ActivityList</code> containing the given
     *  activity record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, activities are returned that are currently
     *  effective.  In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getActivities()</code>.
     *
     *  @param  activityRecordType an activity record type 
     *  @return the returned <code>Activity</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>activityRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.ActivityList getActivitiesByRecordType(org.osid.type.Type activityRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.activity.ActivityRecordFilterList(getActivities(), activityRecordType));
    }


    /**
     *  Gets a <code>ActviityList </code> effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *
     *  In plenary mode, the returned list contains all known activity
     *  or an error results. Otherwise, the returned list may contain
     *  only those activities that are accessible through this
     *  session.
     *
     *  In effective mode, activities are returned that are currently
     *  effective. In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code> Activity </code> list
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> from </code> or <code>
     *          to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.ActivityList getActivitiesOnDate(org.osid.calendaring.DateTime from,
                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.activity.TemporalActivityFilterList(getActivities(), from, to));
    }
    
    
    /**
     *  Gets all <code>Activities</code> associated with a given
     *  <code>ActivityUnit</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session.
     *
     *  In effective mode, activities are returned that are currently
     *  effective.  In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  @param  activityUnitId an activity unit <code>Id</code> 
     *  @return the returned <code>Activity</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>activityUnitId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.ActivityList getActivitiesForActivityUnit(org.osid.id.Id activityUnitId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.activity.ActivityFilterList(new ActivityUnitFilter(activityUnitId), getActivities()));
    }        


    /**
     *  Gets a <code>ActviityList </code> for the given activity unit
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *
     *  In plenary mode, the returned list contains all known activity
     *  or an error results. Otherwise, the returned list may contain
     *  only those activities that are accessible through this
     *  session.
     *
     *  In effective mode, activities are returned that are currently
     *  effective. In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  @param activityUnitId the <code>Id</code> of an activity unit
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code> Activity </code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException
     *          <code>activityUnitId</code>, <code>from</code>, or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.ActivityList getActivitiesForActivityUnitOnDate(org.osid.id.Id activityUnitId, 
                                                                           org.osid.calendaring.DateTime from,
                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.activity.TemporalActivityFilterList(getActivitiesForActivityUnit(activityUnitId), from, to));
    }


    /**
     *  Gets all <code>Activities</code> associated with a given
     *  <code>Term</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session.
     *
     *  In effective mode, activities are returned that are currently
     *  effective.  In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  @param  termId a term <code>Id</code> 
     *  @return the returned <code>Activity</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>termId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.ActivityList getActivitiesForTerm(org.osid.id.Id termId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.activity.ActivityFilterList(new TermFilter(termId), getActivities()));
    }        


    /**
     *  Gets a <code>ActviityList </code> for the given term effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *
     *  In plenary mode, the returned list contains all known activity
     *  or an error results. Otherwise, the returned list may contain
     *  only those activities that are accessible through this
     *  session.
     *
     *  In effective mode, activities are returned that are currently
     *  effective. In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  @param termId the <code>Id</code> of a term
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code> Activity </code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException
     *          <code>termId</code>, <code>from</code>, or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.ActivityList getActivitiesForTermOnDate(org.osid.id.Id termId, 
                                                                   org.osid.calendaring.DateTime from,
                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.activity.TemporalActivityFilterList(getActivitiesForTerm(termId), from, to));
    }


    /**
     *  Gets all <code>Activities</code> associated with a given
     *  <code>ActivityUnit</code> for a term. 
     * 
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session.
     *
     *  In effective mode, activities are returned that are currently
     *  effective.  In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  @param  activityUnitId an activity unit <code>Id</code> 
     *  @param  termId a term <code>Id</code> 
     *  @return the returned <code>Activity</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>activityUnitId</code> or <code>termId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.ActivityList getActivitiesForActivityUnitAndTerm(org.osid.id.Id activityUnitId, 
                                                                            org.osid.id.Id termId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.activity.ActivityFilterList(new TermFilter(termId), getActivitiesForActivityUnit(activityUnitId)));
    }


    /**
     *  Gets a <code>ActviityList </code> for the given activity unit,
     *  term, effective during the entire given date range inclusive
     *  but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known activity
     *  or an error results. Otherwise, the returned list may contain
     *  only those activities that are accessible through this
     *  session.
     *
     *  In effective mode, activities are returned that are currently
     *  effective. In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  @param activityUnitId the <code>Id</code> of an activity unit
     *  @param termId the <code>Id</code> of a term
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code> Activity </code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException
     *          <code>activityUnitId</code>, <code>termId</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.ActivityList getActivitiesForActivityUnitAndTermOnDate(org.osid.id.Id activityUnitId, 
                                                                                  org.osid.id.Id termId,
                                                                                  org.osid.calendaring.DateTime from,
                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.activity.TemporalActivityFilterList(getActivitiesForActivityUnitAndTerm(activityUnitId, termId), from, to));
    }


    /**
     *  Gets all <code>Activities</code> associated with a given
     *  <code>CourseOffering</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session.
     *
     *  In effective mode, activities are returned that are currently
     *  effective.  In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  @param  courseOfferingId a course <code>Id</code> 
     *  @return the returned <code>Activity</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>courseOfferingId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.ActivityList getActivitiesForCourseOffering(org.osid.id.Id courseOfferingId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.activity.ActivityFilterList(new CourseOfferingFilter(courseOfferingId), getActivities()));
    }


    /**
     *  Gets all <code>Activities</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, activities are returned that are currently
     *  effective.  In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Activities</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.course.ActivityList getActivities()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the activity list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of activities
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.course.ActivityList filterActivitiesOnViews(org.osid.course.ActivityList list)
        throws org.osid.OperationFailedException {

        org.osid.course.ActivityList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.course.activity.EffectiveActivityFilterList(ret);
        }

        return (ret);
    }


    public static class ActivityUnitFilter
        implements net.okapia.osid.jamocha.inline.filter.course.activity.ActivityFilter {

        private final org.osid.id.Id activityUnitId;

        
        /**
         *  Constructs a new <code>ActivityUnitFilter</code>.
         *
         *  @param activityUnitId the course offering to filter
         *  @throws org.osid.NullArgumentException <code>activityUnitId</code>
         *          is <code>null</code>
         */

        public ActivityUnitFilter(org.osid.id.Id activityUnitId) {
            nullarg(activityUnitId, "course offering Id");
            this.activityUnitId = activityUnitId;
            return;
        }


        /**
         *  Used by the ActivityFilterList to filter the course
         *  offering list based on activity unit.
         *
         *  @param activity the activity offering
         *  @return <code>true</code> to pass the activity offering,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.course.Activity activity) {
            return (activity.getActivityUnitId().equals(this.activityUnitId));
        }
    }        


    public static class TermFilter
        implements net.okapia.osid.jamocha.inline.filter.course.activity.ActivityFilter {

        private final org.osid.id.Id termId;

        
        /**
         *  Constructs a new <code>TermFilter</code>.
         *
         *  @param termId the course offering to filter
         *  @throws org.osid.NullArgumentException <code>termId</code>
         *          is <code>null</code>
         */

        public TermFilter(org.osid.id.Id termId) {
            nullarg(termId, "course offering Id");
            this.termId = termId;
            return;
        }


        /**
         *  Used by the ActivityFilterList to filter the course offering list
         *  based on course.
         *
         *  @param activity the activity offering
         *  @return <code>true</code> to pass the activity offering,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.course.Activity activity) {
            return (activity.getTermId().equals(this.termId));
        }
    }        


    public static class CourseOfferingFilter
        implements net.okapia.osid.jamocha.inline.filter.course.activity.ActivityFilter {

        private final org.osid.id.Id courseOfferingId;

        
        /**
         *  Constructs a new <code>CourseOfferingFilter</code>.
         *
         *  @param courseOfferingId the course offering to filter
         *  @throws org.osid.NullArgumentException
         *          <code>courseOfferingId</code> is <code>null</code>
         */

        public CourseOfferingFilter(org.osid.id.Id courseOfferingId) {
            nullarg(courseOfferingId, "course offering Id");
            this.courseOfferingId = courseOfferingId;
            return;
        }


        /**
         *  Used by the ActivityFilterList to filter the activity list
         *  based on course offering.
         *
         *  @param activity the activity offering
         *  @return <code>true</code> to pass the activity offering,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.course.Activity activity) {
            return (activity.getCourseOfferingId().equals(this.courseOfferingId));
        }
    }        
}
