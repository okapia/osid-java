//
// AbstractAdapterActivityUnitLookupSession.java
//
//    An ActivityUnit lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.course.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An ActivityUnit lookup session adapter.
 */

public abstract class AbstractAdapterActivityUnitLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.course.ActivityUnitLookupSession {

    private final org.osid.course.ActivityUnitLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterActivityUnitLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterActivityUnitLookupSession(org.osid.course.ActivityUnitLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code CourseCatalog/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code CourseCatalog Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.session.getCourseCatalogId());
    }


    /**
     *  Gets the {@code CourseCatalog} associated with this session.
     *
     *  @return the {@code CourseCatalog} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCourseCatalog());
    }


    /**
     *  Tests if this user can perform {@code ActivityUnit} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupActivityUnits() {
        return (this.session.canLookupActivityUnits());
    }


    /**
     *  A complete view of the {@code ActivityUnit} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeActivityUnitView() {
        this.session.useComparativeActivityUnitView();
        return;
    }


    /**
     *  A complete view of the {@code ActivityUnit} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryActivityUnitView() {
        this.session.usePlenaryActivityUnitView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include activity units in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.session.useFederatedCourseCatalogView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.session.useIsolatedCourseCatalogView();
        return;
    }
    

    /**
     *  Only active activity units are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveActivityUnitView() {
        this.session.useActiveActivityUnitView();
        return;
    }


    /**
     *  Active and inactive activity units are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusActivityUnitView() {
        this.session.useAnyStatusActivityUnitView();
        return;
    }
    
     
    /**
     *  Gets the {@code ActivityUnit} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code ActivityUnit} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code ActivityUnit} and
     *  retained for compatibility.
     *
     *  In active mode, activity units are returned that are currently
     *  active. In any status mode, active and inactive activity units
     *  are returned.
     *
     *  @param activityUnitId {@code Id} of the {@code ActivityUnit}
     *  @return the activity unit
     *  @throws org.osid.NotFoundException {@code activityUnitId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code activityUnitId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.ActivityUnit getActivityUnit(org.osid.id.Id activityUnitId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActivityUnit(activityUnitId));
    }


    /**
     *  Gets an {@code ActivityUnitList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  activityUnits specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code ActivityUnits} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, activity units are returned that are currently
     *  active. In any status mode, active and inactive activity units
     *  are returned.
     *
     *  @param  activityUnitIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code ActivityUnit} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code activityUnitIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitList getActivityUnitsByIds(org.osid.id.IdList activityUnitIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActivityUnitsByIds(activityUnitIds));
    }


    /**
     *  Gets an {@code ActivityUnitList} corresponding to the given
     *  activity unit genus {@code Type} which does not include
     *  activity units of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  activity units or an error results. Otherwise, the returned list
     *  may contain only those activity units that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, activity units are returned that are currently
     *  active. In any status mode, active and inactive activity units
     *  are returned.
     *
     *  @param  activityUnitGenusType an activityUnit genus type 
     *  @return the returned {@code ActivityUnit} list
     *  @throws org.osid.NullArgumentException
     *          {@code activityUnitGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitList getActivityUnitsByGenusType(org.osid.type.Type activityUnitGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActivityUnitsByGenusType(activityUnitGenusType));
    }


    /**
     *  Gets an {@code ActivityUnitList} corresponding to the given
     *  activity unit genus {@code Type} and include any additional
     *  activity units with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  activity units or an error results. Otherwise, the returned list
     *  may contain only those activity units that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, activity units are returned that are currently
     *  active. In any status mode, active and inactive activity units
     *  are returned.
     *
     *  @param  activityUnitGenusType an activityUnit genus type 
     *  @return the returned {@code ActivityUnit} list
     *  @throws org.osid.NullArgumentException
     *          {@code activityUnitGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitList getActivityUnitsByParentGenusType(org.osid.type.Type activityUnitGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActivityUnitsByParentGenusType(activityUnitGenusType));
    }


    /**
     *  Gets an {@code ActivityUnitList} containing the given
     *  activity unit record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  activity units or an error results. Otherwise, the returned list
     *  may contain only those activity units that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, activity units are returned that are currently
     *  active. In any status mode, active and inactive activity units
     *  are returned.
     *
     *  @param  activityUnitRecordType an activityUnit record type 
     *  @return the returned {@code ActivityUnit} list
     *  @throws org.osid.NullArgumentException
     *          {@code activityUnitRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitList getActivityUnitsByRecordType(org.osid.type.Type activityUnitRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActivityUnitsByRecordType(activityUnitRecordType));
    }


    /**
     *  Gets an {@code ActivityUnitList} for the given course.
     *  
     *  In plenary mode, the returned list contains all known activity
     *  units or an error results. Otherwise, the returned list may
     *  contain only those activity units that are accessible through
     *  this session.
     *  
     *  In active mode, activity units are returned that are currently
     *  active.  In any status mode, active and inactive activity
     *  units are returned.
     *
     *  @param  courseId a course {@code Id} 
     *  @return the returned {@code ActivityUnit} list 
     *  @throws org.osid.NullArgumentException {@code courseId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitList getActivityUnitsForCourse(org.osid.id.Id courseId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActivityUnitsForCourse(courseId));
    }


    /**
     *  Gets all {@code ActivityUnits}. 
     *
     *  In plenary mode, the returned list contains all known
     *  activity units or an error results. Otherwise, the returned list
     *  may contain only those activity units that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, activity units are returned that are currently
     *  active. In any status mode, active and inactive activity units
     *  are returned.
     *
     *  @return a list of {@code ActivityUnits} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitList getActivityUnits()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActivityUnits());
    }
}
