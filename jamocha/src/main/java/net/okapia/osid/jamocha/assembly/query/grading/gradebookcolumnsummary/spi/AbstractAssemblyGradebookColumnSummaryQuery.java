//
// AbstractAssemblyGradebookColumnSummaryQuery.java
//
//     A GradebookColumnSummaryQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.grading.gradebookcolumnsummary.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A GradebookColumnSummaryQuery that stores terms.
 */

public abstract class AbstractAssemblyGradebookColumnSummaryQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRuleQuery
    implements org.osid.grading.GradebookColumnSummaryQuery,
               org.osid.grading.GradebookColumnSummaryQueryInspector,
               org.osid.grading.GradebookColumnSummarySearchOrder {

    private final java.util.Collection<org.osid.grading.records.GradebookColumnSummaryQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.grading.records.GradebookColumnSummaryQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.grading.records.GradebookColumnSummarySearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyGradebookColumnSummaryQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyGradebookColumnSummaryQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the gradebook column <code> Id </code> for this query. 
     *
     *  @param  gradebookColumnId a gradeboo column <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradebookColumnId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchGradebookColumnId(org.osid.id.Id gradebookColumnId, 
                                       boolean match) {
        getAssembler().addIdTerm(getGradebookColumnIdColumn(), gradebookColumnId, match);
        return;
    }


    /**
     *  Clears the gradebook column <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGradebookColumnIdTerms() {
        getAssembler().clearTerms(getGradebookColumnIdColumn());
        return;
    }


    /**
     *  Gets the gradebook column <code> Id </code> terms. 
     *
     *  @return the gradebook column <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGradebookColumnIdTerms() {
        return (getAssembler().getIdTerms(getGradebookColumnIdColumn()));
    }


    /**
     *  Gets the GradebookColumnId column name.
     *
     * @return the column name
     */

    protected String getGradebookColumnIdColumn() {
        return ("gradebook_column_id");
    }


    /**
     *  Tests if a <code> GradebookColumnQuery </code> is available for 
     *  querying gradebook column. 
     *
     *  @return <code> true </code> if a gradebook column query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookColumnQuery() {
        return (false);
    }


    /**
     *  Gets the query for a gradebook column. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the gradebook column query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnQuery getGradebookColumnQuery() {
        throw new org.osid.UnimplementedException("supportsGradebookColumnQuery() is false");
    }


    /**
     *  Matches gradebook column derivations with any gradebookc olumn. 
     *
     *  @param  match <code> true </code> to match gradebook column 
     *          derivations with any gradebook column, <code> false </code> to 
     *          match gradebook column derivations with no gradebook columns 
     */

    @OSID @Override
    public void matchAnyGradebookColumn(boolean match) {
        getAssembler().addIdWildcardTerm(getGradebookColumnColumn(), match);
        return;
    }


    /**
     *  Clears the source grade system terms. 
     */

    @OSID @Override
    public void clearGradebookColumnTerms() {
        getAssembler().clearTerms(getGradebookColumnColumn());
        return;
    }


    /**
     *  Gets the gradebook column terms. 
     *
     *  @return the gradebookc column terms 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnQueryInspector[] getGradebookColumnTerms() {
        return (new org.osid.grading.GradebookColumnQueryInspector[0]);
    }


    /**
     *  Gets the GradebookColumn column name.
     *
     * @return the column name
     */

    protected String getGradebookColumnColumn() {
        return ("gradebook_column");
    }


    /**
     *  Matches a mean between the given values inclusive. 
     *
     *  @param  low low end of range 
     *  @param  high high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     */

    @OSID @Override
    public void matchMean(java.math.BigDecimal low, java.math.BigDecimal high, 
                          boolean match) {
        getAssembler().addDecimalRangeTerm(getMeanColumn(), low, high, match);
        return;
    }


    /**
     *  Clears the mean terms. 
     */

    @OSID @Override
    public void clearMeanTerms() {
        getAssembler().clearTerms(getMeanColumn());
        return;
    }


    /**
     *  Gets the mean terms. 
     *
     *  @return the mean terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getMeanTerms() {
        return (getAssembler().getDecimalRangeTerms(getMeanColumn()));
    }


    /**
     *  Specified a preference for ordering results by the mean. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByMean(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getMeanColumn(), style);
        return;
    }


    /**
     *  Gets the Mean column name.
     *
     * @return the column name
     */

    protected String getMeanColumn() {
        return ("mean");
    }


    /**
     *  Matches a mean greater than or equal to the given value. 
     *
     *  @param  value minimum value 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchMinimumMean(java.math.BigDecimal value, boolean match) {
        getAssembler().addDecimalTerm(getMinimumMeanColumn(), value, match);
        return;
    }


    /**
     *  Clears the minimum mean terms. 
     */

    @OSID @Override
    public void clearMinimumMeanTerms() {
        getAssembler().clearTerms(getMinimumMeanColumn());
        return;
    }


    /**
     *  Gets the minimum mean terms. 
     *
     *  @return the minimum mean terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalTerm[] getMinimumMeanTerms() {
        return (getAssembler().getDecimalTerms(getMinimumMeanColumn()));
    }


    /**
     *  Gets the MinimumMean column name.
     *
     * @return the column name
     */

    protected String getMinimumMeanColumn() {
        return ("minimum_mean");
    }


    /**
     *  Matches a median between the given values inclusive. 
     *
     *  @param  low low end of range 
     *  @param  high high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     */

    @OSID @Override
    public void matchMedian(java.math.BigDecimal low, 
                            java.math.BigDecimal high, boolean match) {
        getAssembler().addDecimalRangeTerm(getMedianColumn(), low, high, match);
        return;
    }


    /**
     *  Clears the median terms. 
     */

    @OSID @Override
    public void clearMedianTerms() {
        getAssembler().clearTerms(getMedianColumn());
        return;
    }


    /**
     *  Gets the median terms. 
     *
     *  @return the median terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getMedianTerms() {
        return (getAssembler().getDecimalRangeTerms(getMedianColumn()));
    }


    /**
     *  Specified a preference for ordering results by the median. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByMedian(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getMedianColumn(), style);
        return;
    }


    /**
     *  Gets the Median column name.
     *
     * @return the column name
     */

    protected String getMedianColumn() {
        return ("median");
    }


    /**
     *  Matches a median greater than or equal to the given value. 
     *
     *  @param  value minimum value 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchMinimumMedian(java.math.BigDecimal value, boolean match) {
        getAssembler().addDecimalTerm(getMinimumMedianColumn(), value, match);
        return;
    }


    /**
     *  Clears the minimum median terms. 
     */

    @OSID @Override
    public void clearMinimumMedianTerms() {
        getAssembler().clearTerms(getMinimumMedianColumn());
        return;
    }


    /**
     *  Gets the minimum median terms. 
     *
     *  @return the minimum median terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalTerm[] getMinimumMedianTerms() {
        return (getAssembler().getDecimalTerms(getMinimumMedianColumn()));
    }


    /**
     *  Gets the MinimumMedian column name.
     *
     * @return the column name
     */

    protected String getMinimumMedianColumn() {
        return ("minimum_median");
    }


    /**
     *  Matches a mode between the given values inclusive. 
     *
     *  @param  low low end of range 
     *  @param  high high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     */

    @OSID @Override
    public void matchMode(java.math.BigDecimal low, java.math.BigDecimal high, 
                          boolean match) {
        getAssembler().addDecimalRangeTerm(getModeColumn(), low, high, match);
        return;
    }


    /**
     *  Clears the mode terms. 
     */

    @OSID @Override
    public void clearModeTerms() {
        getAssembler().clearTerms(getModeColumn());
        return;
    }


    /**
     *  Gets the mode terms. 
     *
     *  @return the mode terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getModeTerms() {
        return (getAssembler().getDecimalRangeTerms(getModeColumn()));
    }


    /**
     *  Specified a preference for ordering results by the mode. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByMode(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getModeColumn(), style);
        return;
    }


    /**
     *  Gets the Mode column name.
     *
     * @return the column name
     */

    protected String getModeColumn() {
        return ("mode");
    }


    /**
     *  Matches a mode greater than or equal to the given value. 
     *
     *  @param  value minimum value 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchMinimumMode(java.math.BigDecimal value, boolean match) {
        getAssembler().addDecimalTerm(getMinimumModeColumn(), value, match);
        return;
    }


    /**
     *  Clears the minimum mode terms. 
     */

    @OSID @Override
    public void clearMinimumModeTerms() {
        getAssembler().clearTerms(getMinimumModeColumn());
        return;
    }


    /**
     *  Gets the minimum mode terms. 
     *
     *  @return the minimum mode terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalTerm[] getMinimumModeTerms() {
        return (getAssembler().getDecimalTerms(getMinimumModeColumn()));
    }


    /**
     *  Gets the MinimumMode column name.
     *
     * @return the column name
     */

    protected String getMinimumModeColumn() {
        return ("minimum_mode");
    }


    /**
     *  Matches a root mean square between the given values inclusive. 
     *
     *  @param  low low end of range 
     *  @param  high high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     */

    @OSID @Override
    public void matchRMS(java.math.BigDecimal low, java.math.BigDecimal high, 
                         boolean match) {
        getAssembler().addDecimalRangeTerm(getRMSColumn(), low, high, match);
        return;
    }


    /**
     *  Clears the root mean square terms. 
     */

    @OSID @Override
    public void clearRMSTerms() {
        getAssembler().clearTerms(getRMSColumn());
        return;
    }


    /**
     *  Gets the rms terms. 
     *
     *  @return the rms terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getRMSTerms() {
        return (getAssembler().getDecimalRangeTerms(getRMSColumn()));
    }


    /**
     *  Specified a preference for ordering results by the root mean square. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRMS(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getRMSColumn(), style);
        return;
    }


    /**
     *  Gets the RMS column name.
     *
     * @return the column name
     */

    protected String getRMSColumn() {
        return ("r_ms");
    }


    /**
     *  Matches a root mean square greater than or equal to the given value. 
     *
     *  @param  value minimum value 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchMinimumRMS(java.math.BigDecimal value, boolean match) {
        getAssembler().addDecimalTerm(getMinimumRMSColumn(), value, match);
        return;
    }


    /**
     *  Clears the minimum RMS terms. 
     */

    @OSID @Override
    public void clearMinimumRMSTerms() {
        getAssembler().clearTerms(getMinimumRMSColumn());
        return;
    }


    /**
     *  Gets the minimum rms terms. 
     *
     *  @return the minimum rms terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalTerm[] getMinimumRMSTerms() {
        return (getAssembler().getDecimalTerms(getMinimumRMSColumn()));
    }


    /**
     *  Gets the MinimumRMS column name.
     *
     * @return the column name
     */

    protected String getMinimumRMSColumn() {
        return ("minimum_rms");
    }


    /**
     *  Matches a standard deviation mean square between the given values 
     *  inclusive. 
     *
     *  @param  low low end of range 
     *  @param  high high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     */

    @OSID @Override
    public void matchStandardDeviation(java.math.BigDecimal low, 
                                       java.math.BigDecimal high, 
                                       boolean match) {
        getAssembler().addDecimalRangeTerm(getStandardDeviationColumn(), low, high, match);
        return;
    }


    /**
     *  Clears the standard deviation terms. 
     */

    @OSID @Override
    public void clearStandardDeviationTerms() {
        getAssembler().clearTerms(getStandardDeviationColumn());
        return;
    }


    /**
     *  Gets the standard deviation terms. 
     *
     *  @return the standard deviation terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getStandardDeviationTerms() {
        return (getAssembler().getDecimalRangeTerms(getStandardDeviationColumn()));
    }


    /**
     *  Specified a preference for ordering results by the standard deviation. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByStandardDeviation(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getStandardDeviationColumn(), style);
        return;
    }


    /**
     *  Gets the StandardDeviation column name.
     *
     * @return the column name
     */

    protected String getStandardDeviationColumn() {
        return ("standard_deviation");
    }


    /**
     *  Matches a standard deviation greater than or equal to the given value. 
     *
     *  @param  value minimum value 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchMinimumStandardDeviation(java.math.BigDecimal value, 
                                              boolean match) {
        getAssembler().addDecimalTerm(getMinimumStandardDeviationColumn(), value, match);
        return;
    }


    /**
     *  Clears the minimum standard deviation terms. 
     */

    @OSID @Override
    public void clearMinimumStandardDeviationTerms() {
        getAssembler().clearTerms(getMinimumStandardDeviationColumn());
        return;
    }


    /**
     *  Gets the minimum standard deviation terms. 
     *
     *  @return the minimum standard deviation terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalTerm[] getMinimumStandardDeviationTerms() {
        return (getAssembler().getDecimalTerms(getMinimumStandardDeviationColumn()));
    }


    /**
     *  Gets the MinimumStandardDeviation column name.
     *
     * @return the column name
     */

    protected String getMinimumStandardDeviationColumn() {
        return ("minimum_standard_deviation");
    }


    /**
     *  Matches a sum mean square between the given values inclusive. 
     *
     *  @param  low low end of range 
     *  @param  high high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     */

    @OSID @Override
    public void matchSum(java.math.BigDecimal low, java.math.BigDecimal high, 
                         boolean match) {
        getAssembler().addDecimalRangeTerm(getSumColumn(), low, high, match);
        return;
    }


    /**
     *  Clears the sum terms. 
     */

    @OSID @Override
    public void clearSumTerms() {
        getAssembler().clearTerms(getSumColumn());
        return;
    }


    /**
     *  Gets the sum terms. 
     *
     *  @return the sum terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getSumTerms() {
        return (getAssembler().getDecimalRangeTerms(getSumColumn()));
    }


    /**
     *  Specified a preference for ordering results by the sum. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySum(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getSumColumn(), style);
        return;
    }


    /**
     *  Gets the Sum column name.
     *
     * @return the column name
     */

    protected String getSumColumn() {
        return ("sum");
    }


    /**
     *  Matches a sum greater than or equal to the given value. 
     *
     *  @param  value minimum value 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchMinimumSum(java.math.BigDecimal value, boolean match) {
        getAssembler().addDecimalTerm(getMinimumSumColumn(), value, match);
        return;
    }


    /**
     *  Clears the minimum sum terms. 
     */

    @OSID @Override
    public void clearMinimumSumTerms() {
        getAssembler().clearTerms(getMinimumSumColumn());
        return;
    }


    /**
     *  Gets the minimum sum terms. 
     *
     *  @return the minimum sum terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalTerm[] getMinimumSumTerms() {
        return (getAssembler().getDecimalTerms(getMinimumSumColumn()));
    }


    /**
     *  Gets the MinimumSum column name.
     *
     * @return the column name
     */

    protected String getMinimumSumColumn() {
        return ("minimum_sum");
    }


    /**
     *  Sets the gradebook <code> Id </code> for this query. 
     *
     *  @param  gradebookId a gradebook <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGradebookId(org.osid.id.Id gradebookId, boolean match) {
        getAssembler().addIdTerm(getGradebookIdColumn(), gradebookId, match);
        return;
    }


    /**
     *  Clears the gradebook <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGradebookIdTerms() {
        getAssembler().clearTerms(getGradebookIdColumn());
        return;
    }


    /**
     *  Gets the gradebook <code> Id </code> terms. 
     *
     *  @return the gradebook <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGradebookIdTerms() {
        return (getAssembler().getIdTerms(getGradebookIdColumn()));
    }


    /**
     *  Gets the GradebookId column name.
     *
     * @return the column name
     */

    protected String getGradebookIdColumn() {
        return ("gradebook_id");
    }


    /**
     *  Tests if a <code> GradebookQuery </code> is available . 
     *
     *  @return <code> true </code> if a gradebook query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookQuery() {
        return (false);
    }


    /**
     *  Gets the query for a gradebook. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the gradebook query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookQuery getGradebookQuery() {
        throw new org.osid.UnimplementedException("supportsGradebookQuery() is false");
    }


    /**
     *  Clears the gradebook terms. 
     */

    @OSID @Override
    public void clearGradebookTerms() {
        getAssembler().clearTerms(getGradebookColumn());
        return;
    }


    /**
     *  Gets the gradebook terms. 
     *
     *  @return the gradebook terms 
     */

    @OSID @Override
    public org.osid.grading.GradebookQueryInspector[] getGradebookTerms() {
        return (new org.osid.grading.GradebookQueryInspector[0]);
    }


    /**
     *  Gets the Gradebook column name.
     *
     * @return the column name
     */

    protected String getGradebookColumn() {
        return ("gradebook");
    }


    /**
     *  Tests if this gradebookColumnSummary supports the given record
     *  <code>Type</code>.
     *
     *  @param  gradebookColumnSummaryRecordType a gradebook column summary record type 
     *  @return <code>true</code> if the gradebookColumnSummaryRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumnSummaryRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type gradebookColumnSummaryRecordType) {
        for (org.osid.grading.records.GradebookColumnSummaryQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(gradebookColumnSummaryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  gradebookColumnSummaryRecordType the gradebook column summary record type 
     *  @return the gradebook column summary query record 
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumnSummaryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(gradebookColumnSummaryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.grading.records.GradebookColumnSummaryQueryRecord getGradebookColumnSummaryQueryRecord(org.osid.type.Type gradebookColumnSummaryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.grading.records.GradebookColumnSummaryQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(gradebookColumnSummaryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(gradebookColumnSummaryRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  gradebookColumnSummaryRecordType the gradebook column summary record type 
     *  @return the gradebook column summary query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumnSummaryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(gradebookColumnSummaryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.grading.records.GradebookColumnSummaryQueryInspectorRecord getGradebookColumnSummaryQueryInspectorRecord(org.osid.type.Type gradebookColumnSummaryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.grading.records.GradebookColumnSummaryQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(gradebookColumnSummaryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(gradebookColumnSummaryRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param gradebookColumnSummaryRecordType the gradebook column summary record type
     *  @return the gradebook column summary search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumnSummaryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(gradebookColumnSummaryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.grading.records.GradebookColumnSummarySearchOrderRecord getGradebookColumnSummarySearchOrderRecord(org.osid.type.Type gradebookColumnSummaryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.grading.records.GradebookColumnSummarySearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(gradebookColumnSummaryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(gradebookColumnSummaryRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this gradebook column summary. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param gradebookColumnSummaryQueryRecord the gradebook column summary query record
     *  @param gradebookColumnSummaryQueryInspectorRecord the gradebook column summary query inspector
     *         record
     *  @param gradebookColumnSummarySearchOrderRecord the gradebook column summary search order record
     *  @param gradebookColumnSummaryRecordType gradebook column summary record type
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumnSummaryQueryRecord</code>,
     *          <code>gradebookColumnSummaryQueryInspectorRecord</code>,
     *          <code>gradebookColumnSummarySearchOrderRecord</code> or
     *          <code>gradebookColumnSummaryRecordTypegradebookColumnSummary</code> is
     *          <code>null</code>
     */
            
    protected void addGradebookColumnSummaryRecords(org.osid.grading.records.GradebookColumnSummaryQueryRecord gradebookColumnSummaryQueryRecord, 
                                      org.osid.grading.records.GradebookColumnSummaryQueryInspectorRecord gradebookColumnSummaryQueryInspectorRecord, 
                                      org.osid.grading.records.GradebookColumnSummarySearchOrderRecord gradebookColumnSummarySearchOrderRecord, 
                                      org.osid.type.Type gradebookColumnSummaryRecordType) {

        addRecordType(gradebookColumnSummaryRecordType);

        nullarg(gradebookColumnSummaryQueryRecord, "gradebook column summary query record");
        nullarg(gradebookColumnSummaryQueryInspectorRecord, "gradebook column summary query inspector record");
        nullarg(gradebookColumnSummarySearchOrderRecord, "gradebook column summary search odrer record");

        this.queryRecords.add(gradebookColumnSummaryQueryRecord);
        this.queryInspectorRecords.add(gradebookColumnSummaryQueryInspectorRecord);
        this.searchOrderRecords.add(gradebookColumnSummarySearchOrderRecord);
        
        return;
    }
}
