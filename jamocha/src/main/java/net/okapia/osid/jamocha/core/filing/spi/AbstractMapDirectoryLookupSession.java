//
// AbstractMapDirectoryLookupSession
//
//    A simple framework for providing a Directory lookup service
//    backed by a fixed collection of directories.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.filing.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Directory lookup service backed by a
 *  fixed collection of directories. The directories are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Directories</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapDirectoryLookupSession
    extends net.okapia.osid.jamocha.filing.spi.AbstractDirectoryLookupSession
    implements org.osid.filing.DirectoryLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.filing.Directory> directories = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.filing.Directory>());


    /**
     *  Makes a <code>Directory</code> available in this session.
     *
     *  @param  directory a directory
     *  @throws org.osid.NullArgumentException <code>directory<code>
     *          is <code>null</code>
     */

    protected void putDirectory(org.osid.filing.Directory directory) {
        this.directories.put(directory.getId(), directory);
        return;
    }


    /**
     *  Makes an array of directories available in this session.
     *
     *  @param  directories an array of directories
     *  @throws org.osid.NullArgumentException <code>directories<code>
     *          is <code>null</code>
     */

    protected void putDirectories(org.osid.filing.Directory[] directories) {
        putDirectories(java.util.Arrays.asList(directories));
        return;
    }


    /**
     *  Makes a collection of directories available in this session.
     *
     *  @param  directories a collection of directories
     *  @throws org.osid.NullArgumentException <code>directories<code>
     *          is <code>null</code>
     */

    protected void putDirectories(java.util.Collection<? extends org.osid.filing.Directory> directories) {
        for (org.osid.filing.Directory directory : directories) {
            this.directories.put(directory.getId(), directory);
        }

        return;
    }


    /**
     *  Removes a Directory from this session.
     *
     *  @param  directoryId the <code>Id</code> of the directory
     *  @throws org.osid.NullArgumentException <code>directoryId<code> is
     *          <code>null</code>
     */

    protected void removeDirectory(org.osid.id.Id directoryId) {
        this.directories.remove(directoryId);
        return;
    }


    /**
     *  Gets the <code>Directory</code> specified by its <code>Id</code>.
     *
     *  @param  directoryId <code>Id</code> of the <code>Directory</code>
     *  @return the directory
     *  @throws org.osid.NotFoundException <code>directoryId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>directoryId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.filing.Directory getDirectory(org.osid.id.Id directoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.filing.Directory directory = this.directories.get(directoryId);
        if (directory == null) {
            throw new org.osid.NotFoundException("directory not found: " + directoryId);
        }

        return (directory);
    }


    /**
     *  Gets all <code>Directories</code>. In plenary mode, the returned
     *  list contains all known directories or an error
     *  results. Otherwise, the returned list may contain only those
     *  directories that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Directories</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.filing.DirectoryList getDirectories()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.filing.directory.ArrayDirectoryList(this.directories.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.directories.clear();
        super.close();
        return;
    }
}
