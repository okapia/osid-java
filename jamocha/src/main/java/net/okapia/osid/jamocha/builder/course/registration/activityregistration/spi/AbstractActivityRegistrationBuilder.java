//
// AbstractActivityRegistration.java
//
//     Defines an ActivityRegistration builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.registration.activityregistration.spi;


/**
 *  Defines an <code>ActivityRegistration</code> builder.
 */

public abstract class AbstractActivityRegistrationBuilder<T extends AbstractActivityRegistrationBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRelationshipBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.course.registration.activityregistration.ActivityRegistrationMiter activityRegistration;


    /**
     *  Constructs a new <code>AbstractActivityRegistrationBuilder</code>.
     *
     *  @param activityRegistration the activity registration to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractActivityRegistrationBuilder(net.okapia.osid.jamocha.builder.course.registration.activityregistration.ActivityRegistrationMiter activityRegistration) {
        super(activityRegistration);
        this.activityRegistration = activityRegistration;
        return;
    }


    /**
     *  Builds the activity registration.
     *
     *  @return the new activity registration
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.course.registration.ActivityRegistration build() {
        (new net.okapia.osid.jamocha.builder.validator.course.registration.activityregistration.ActivityRegistrationValidator(getValidations())).validate(this.activityRegistration);
        return (new net.okapia.osid.jamocha.builder.course.registration.activityregistration.ImmutableActivityRegistration(this.activityRegistration));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the activity registration miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.course.registration.activityregistration.ActivityRegistrationMiter getMiter() {
        return (this.activityRegistration);
    }


    /**
     *  Sets the registration.
     *
     *  @param registration the registration
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>registration</code>
     *          is <code>null</code>
     */

    public T registration(org.osid.course.registration.Registration registration) {
        getMiter().setRegistration(registration);
        return (self());
    }


    /**
     *  Sets the activity.
     *
     *  @param activity an activity
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>activity</code>
     *          is <code>null</code>
     */

    public T activity(org.osid.course.Activity activity) {
        getMiter().setActivity(activity);
        return (self());
    }


    /**
     *  Sets the student.
     *
     *  @param student a student
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>student</code> is
     *          <code>null</code>
     */

    public T student(org.osid.resource.Resource student) {
        getMiter().setStudent(student);
        return (self());
    }


    /**
     *  Adds an ActivityRegistration record.
     *
     *  @param record an activity registration record
     *  @param recordType the type of activity registration record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.course.registration.records.ActivityRegistrationRecord record, org.osid.type.Type recordType) {
        getMiter().addActivityRegistrationRecord(record, recordType);
        return (self());
    }
}       


