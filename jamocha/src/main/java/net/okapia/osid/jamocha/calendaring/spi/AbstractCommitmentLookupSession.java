//
// AbstractCommitmentLookupSession.java
//
//    A starter implementation framework for providing a Commitment
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Commitment
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getCommitments(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractCommitmentLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.calendaring.CommitmentLookupSession {

    private boolean pedantic      = false;
    private boolean effectiveonly = false;
    private boolean federated     = false;
    private org.osid.calendaring.Calendar calendar = new net.okapia.osid.jamocha.nil.calendaring.calendar.UnknownCalendar();
    

    /**
     *  Gets the <code>Calendar/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Calendar Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCalendarId() {
        return (this.calendar.getId());
    }


    /**
     *  Gets the <code>Calendar</code> associated with this 
     *  session.
     *
     *  @return the <code>Calendar</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.calendar);
    }


    /**
     *  Sets the <code>Calendar</code>.
     *
     *  @param  calendar the calendar for this session
     *  @throws org.osid.NullArgumentException <code>calendar</code>
     *          is <code>null</code>
     */

    protected void setCalendar(org.osid.calendaring.Calendar calendar) {
        nullarg(calendar, "calendar");
        this.calendar = calendar;
        return;
    }

    /**
     *  Tests if this user can perform <code>Commitment</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCommitments() {
        return (true);
    }


    /**
     *  A complete view of the <code>Commitment</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCommitmentView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Commitment</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCommitmentView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include commitments in calendars which are children
     *  of this calendar in the calendar hierarchy.
     */

    @OSID @Override
    public void useFederatedCalendarView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this calendar only.
     */

    @OSID @Override
    public void useIsolatedCalendarView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only commitments whose effective dates are current are
     *  returned by methods in this session.
     */

    @OSID @Override
    public void useEffectiveCommitmentView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All commitments of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveCommitmentView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Commitment</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Commitment</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Commitment</code> and
     *  retained for compatibility.
     *
     *  In effective mode, commitments are returned that are currently
     *  effective.  In any effective mode, effective commitments and
     *  those currently expired are returned.
     *
     *  @param  commitmentId <code>Id</code> of the
     *          <code>Commitment</code>
     *  @return the commitment
     *  @throws org.osid.NotFoundException <code>commitmentId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>commitmentId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Commitment getCommitment(org.osid.id.Id commitmentId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.calendaring.CommitmentList commitments = getCommitments()) {
            while (commitments.hasNext()) {
                org.osid.calendaring.Commitment commitment = commitments.getNextCommitment();
                if (commitment.getId().equals(commitmentId)) {
                    return (commitment);
                }
            }
        } 

        throw new org.osid.NotFoundException(commitmentId + " not found");
    }


    /**
     *  Gets a <code>CommitmentList</code> corresponding to the given
     *  <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  commitments specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Commitments</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, commitments are returned that are currently
     *  effective.  In any effective mode, effective commitments and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getCommitments()</code>.
     *
     *  @param  commitmentIds the list of <code>Ids</code> to rerieve 
     *  @return the returned <code>Commitment</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>commitmentIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitmentsByIds(org.osid.id.IdList commitmentIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.calendaring.Commitment> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = commitmentIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getCommitment(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("commitment " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.calendaring.commitment.LinkedCommitmentList(ret));
    }


    /**
     *  Gets a <code>CommitmentList</code> corresponding to the given
     *  commitment genus <code>Type</code> which does not include
     *  commitments of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  commitments or an error results. Otherwise, the returned list
     *  may contain only those commitments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, commitments are returned that are currently
     *  effective.  In any effective mode, effective commitments and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getCommitments()</code>.
     *
     *  @param  commitmentGenusType a commitment genus type 
     *  @return the returned <code>Commitment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>commitmentGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitmentsByGenusType(org.osid.type.Type commitmentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.calendaring.commitment.CommitmentGenusFilterList(getCommitments(), commitmentGenusType));
    }


    /**
     *  Gets a <code>CommitmentList</code> corresponding to the given
     *  commitment genus <code>Type</code> and include any additional
     *  commitments with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  commitments or an error results. Otherwise, the returned list
     *  may contain only those commitments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, commitments are returned that are currently
     *  effective.  In any effective mode, effective commitments and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getCommitments()</code>.
     *
     *  @param  commitmentGenusType a commitment genus type 
     *  @return the returned <code>Commitment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>commitmentGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitmentsByParentGenusType(org.osid.type.Type commitmentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getCommitmentsByGenusType(commitmentGenusType));
    }


    /**
     *  Gets a <code>CommitmentList</code> containing the given
     *  commitment record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known
     *  commitments or an error results. Otherwise, the returned list
     *  may contain only those commitments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, commitments are returned that are currently
     *  effective.  In any effective mode, effective commitments and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getCommitments()</code>.
     *
     *  @param  commitmentRecordType a commitment record type 
     *  @return the returned <code>Commitment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>commitmentRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitmentsByRecordType(org.osid.type.Type commitmentRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.calendaring.commitment.CommitmentRecordFilterList(getCommitments(), commitmentRecordType));
    }


    /**
     *  Gets a <code> CommitmentList </code> effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *
     *  In plenary mode, the returned list contains all known
     *  commitments or an error results. Otherwise, the returned list
     *  may contain only those commitments that are accessible through
     *  this session.
     *
     *  In effective mode, commitments are returned that are currently
     *  effective. In any effective mode, effective commitments and those
     *  currently expired are returned.
     *
     *  @param  from starting date
     *  @param  to ending date
     *  @return list of commitments
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> from </code> or <code>
     *          to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitmentsOnDate(org.osid.calendaring.DateTime from,
                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.calendaring.commitment.TemporalCommitmentFilterList(getCommitments(), from, to));
    }


    /**
     *  Gets a <code> CommitmentList </code> effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *
     *  In plenary mode, the returned list contains all known
     *  commitments or an error results. Otherwise, the returned list
     *  may contain only those commitments that are accessible through
     *  this session.
     *
     *  In effective mode, commitments are returned that are currently
     *  effective. In any effective mode, effective commitments and those
     *  currently expired are returned.
     *
     *  @param  commitmentGenusType a commitment genus type 
     *  @param  from starting date
     *  @param  to ending date
     *  @return list of commitments
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException
     *          <code>commitmentGenusType</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitmentsByGenusTypeOnDate(org.osid.type.Type commitmentGenusType,
                                                                               org.osid.calendaring.DateTime from,
                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.calendaring.commitment.TemporalCommitmentFilterList(getCommitmentsByGenusType(commitmentGenusType), from, to));
    }


    /**
     *  Gets a list of commitments corresponding to an event
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  commitments or an error results. Otherwise, the returned list
     *  may contain only those commitments that are accessible
     *  through this session.
     *
     *  In effective mode, commitments are returned that are currently
     *  effective.  In any effective mode, effective commitments and
     *  those currently expired are returned.
     *
     *  @param  eventId the <code>Id</code> of the event
     *  @return the returned <code>CommitmentList</code>
     *  @throws org.osid.NullArgumentException <code>event</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.calendaring.CommitmentList getCommitmentsForEvent(org.osid.id.Id eventId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

         return (new net.okapia.osid.jamocha.inline.filter.calendaring.commitment.CommitmentFilterList(new EventFilter(eventId), getCommitments()));
    }


    /**
     *  Gets a list of commitments corresponding to an event
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  commitments or an error results. Otherwise, the returned list
     *  may contain only those commitments that are accessible through
     *  this session.
     *
     *  In effective mode, commitments are returned that are currently
     *  effective.  In any effective mode, effective commitments and
     *  those currently expired are returned.
     *
     *  @param  eventId the <code>Id</code> of the event
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>CommitmentList</code>
     *  @throws org.osid.NullArgumentException <code>event</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitmentsForEventOnDate(org.osid.id.Id eventId,
                                                                            org.osid.calendaring.DateTime from,
                                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.calendaring.commitment.TemporalCommitmentFilterList(getCommitmentsForEvent(eventId), from, to));
    }


    /**
     *  Gets the commitments for the given event and commitment genus
     *  type including any genus types derived from the given genus
     *  type.
     *
     *  If the event is a recurring event, the commitments are
     *  returned for the recurring event only. In plenary mode, the
     *  returned list contains all of the commitments mapped to the
     *  event <code> Id </code> or an error results if an Id in the
     *  supplied list is not found or inaccessible. Otherwise,
     *  inaccessible commitments may be ommitted.
     *
     *  In effective mode, commitments are returned that are currently
     *  effective. In any effective mode, effective commitments and
     *  those currently expired are returned.
     *
     *  @param  eventId <code> Id </code> of the <code> Event </code>
     *  @param  commitmentGenusType commitment genus type
     *  @return list of commitments
     *  @throws org.osid.NullArgumentException <code> eventId </code> or
     *          <code> commitmentGenusType </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitmentsByGenusTypeForEvent(org.osid.id.Id eventId,
                                                                                 org.osid.type.Type commitmentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.calendaring.commitment.CommitmentGenusFilterList(getCommitmentsForEvent(eventId), commitmentGenusType));        
    }


    /**
     *  Gets a <code> CommitmentList </code> for the given event and
     *  commitment genus type effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  commitments or an error results. Otherwise, the returned list
     *  may contain only those commitments that are accessible through
     *  this session.
     *
     *  In effective mode, commitments are returned that are currently
     *  effective. In any effective mode, effective commitments and
     *  those currently expired are returned.
     *
     *  @param  eventId <code> Id </code> of the <code> Event </code>
     *  @param  commitmentGenusType commitment genus type
     *  @param  from starting date
     *  @param  to ending date
     *  @return list of commitments
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> eventId,
     *          commitmentGenusType, from </code> or <code> to </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitmentsByGenusTypeForEventOnDate(org.osid.id.Id eventId,
                                                                                       org.osid.type.Type commitmentGenusType,
                                                                                       org.osid.calendaring.DateTime from,
                                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.calendaring.commitment.TemporalCommitmentFilterList(getCommitmentsByGenusTypeForEvent(eventId, commitmentGenusType), from, to));
    }


    /**
     *  Gets a list of commitments corresponding to a resource
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  commitments or an error results. Otherwise, the returned list
     *  may contain only those commitments that are accessible through
     *  this session.
     *
     *  In effective mode, commitments are returned that are currently
     *  effective. In any effective mode, effective commitments and
     *  those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @return the returned <code>CommitmentList</code>
     *  @throws org.osid.NullArgumentException <code>resource</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.calendaring.CommitmentList getCommitmentsForResource(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

         return (new net.okapia.osid.jamocha.inline.filter.calendaring.commitment.CommitmentFilterList(new ResourceFilter(resourceId), getCommitments()));
    }


    /**
     *  Gets a list of commitments corresponding to a resource
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  commitments or an error results. Otherwise, the returned list
     *  may contain only those commitments that are accessible
     *  through this session.
     *
     *  In effective mode, commitments are returned that are
     *  currently effective.  In any effective mode, effective
     *  commitments and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>CommitmentList</code>
     *  @throws org.osid.NullArgumentException <code>resource</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitmentsForResourceOnDate(org.osid.id.Id resourceId,
                                                                               org.osid.calendaring.DateTime from,
                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.calendaring.commitment.TemporalCommitmentFilterList(getCommitmentsForResource(resourceId), from, to));
    }


    /**
     *  Gets the commitments for the given resource.
     *
     *  In plenary mode, the returned list contains all of the
     *  commitments mapped to the resource <code> Id </code> or an
     *  error results if an Id in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code> Commitments
     *  </code> may be omitted from the list and may present the
     *  elements in any order including returning a unique set.
     *
     *  In effective mode, commitments are returned that are currently
     *  effective. In any effective mode, effective commitments and
     *  those currently expired are returned.
     *
     *  @param  resourceId <code> Id </code> of a <code> Resource </code>
     *  @param  commitmentGenusType commitment genus type
     *  @return list of commitments
     *  @throws org.osid.NullArgumentException <code> resourceId </code> or
     *          <code> commitmentGenusType </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitmentsByGenusTypeForResource(org.osid.id.Id resourceId,
                                                                                    org.osid.type.Type commitmentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.calendaring.commitment.CommitmentGenusFilterList(getCommitmentsForResource(resourceId), commitmentGenusType));         
    }


    /**
     *  Gets a <code> CommitmentList </code> for the given resource
     *  and effective during the entire given date range inclusive but
     *  not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  commitments or an error results. Otherwise, the returned list
     *  may contain only those commitments that are accessible through
     *  this session.
     *
     *  In effective mode, commitments are returned that are currently
     *  effective. In any effective mode, effective commitments and
     *  those currently expired are returned.
     *
     *  @param  resourceId <code> Id </code> of the <code> Event </code>
     *  @param  commitmentGenusType commitment genus type
     *  @param  from starting date
     *  @param  to ending date
     *  @return list of commitments
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> resourceId,
     *          commitmentGenusType, from </code> or <code> to </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitmentsByGenusTypeForResourceOnDate(org.osid.id.Id resourceId,
                                                                                          org.osid.type.Type commitmentGenusType,
                                                                                          org.osid.calendaring.DateTime from,
                                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.calendaring.commitment.TemporalCommitmentFilterList(getCommitmentsByGenusTypeForResource(resourceId, commitmentGenusType), from, to));
    }


    /**
     *  Gets a list of commitments corresponding to event and resource
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  commitments or an error results. Otherwise, the returned list
     *  may contain only those commitments that are accessible
     *  through this session.
     *
     *  In effective mode, commitments are returned that are
     *  currently effective.  In any effective mode, effective
     *  commitments and those currently expired are returned.
     *
     *  @param  eventId the <code>Id</code> of the event
     *  @param  resourceId the <code>Id</code> of the resource
     *  @return the returned <code>CommitmentList</code>
     *  @throws org.osid.NullArgumentException <code>event</code>,
     *          <code>resource</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitmentsForEventAndResource(org.osid.id.Id eventId,
                                                                                 org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.calendaring.commitment.CommitmentFilterList(new ResourceFilter(resourceId), getCommitmentsForEvent(eventId)));
    }


    /**
     *  Gets a list of commitments corresponding to event and resource
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  commitments or an error results. Otherwise, the returned list
     *  may contain only those commitments that are accessible through
     *  this session.
     *
     *  In effective mode, commitments are returned that are
     *  currently effective.  In any effective mode, effective
     *  commitments and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>CommitmentList</code>
     *  @throws org.osid.NullArgumentException <code>resource</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitmentsForEventAndResourceOnDate(org.osid.id.Id eventId,
                                                                                       org.osid.id.Id resourceId,
                                                                                       org.osid.calendaring.DateTime from,
                                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.calendaring.commitment.TemporalCommitmentFilterList(getCommitmentsForEventAndResource(eventId, resourceId), from, to));
    }


    /**
     *  Gets the commitmentsof the given genus type for the given
     *  event and resource. If the event is a recurring event, the
     *  commitments are returned for the recurring event only.
     *
     *  In plenary mode, the returned list contains all of the
     *  commitments mapped to the event <code> Id </code> or an error
     *  results if an Id in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code> Commitments
     *  </code> may be omitted from the list and may present the
     *  elements in any order including returning a unique set.
     *
     *  In effective mode, commitments are returned that are currently
     *  effective. In any effective mode, effective commitments and
     *  those currently expired are returned.
     *
     *  @param  eventId <code> Id </code> of the <code> Event </code>
     *  @param  resourceId <code> Id </code> of a <code> Resource </code>
     *  @param  commitmentGenusType commitment genus type
     *  @return list of commitments
     *  @throws org.osid.NullArgumentException <code> eventId, resourceId
     *          </code> or <code> commitmentGenusType </code> is <code> null
     *          </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitmentsByGenusTypeForEventAndResource(org.osid.id.Id eventId,
                                                                                            org.osid.id.Id resourceId,
                                                                                            org.osid.type.Type commitmentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.calendaring.commitment.CommitmentGenusFilterList(getCommitmentsForEventAndResource(eventId, resourceId), commitmentGenusType));
    }


    /**
     *  Gets a <code> CommitmentList </code> of the given genus type
     *  for the given event and resource and effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *
     *  In plenary mode, the returned list contains all known
     *  commitments or an error results. Otherwise, the returned list
     *  may contain only those commitments that are accessible through
     *  this session.
     *
     *  In effective mode, commitments are returned that are currently
     *  effective. In any effective mode, effective commitments and
     *  those currently expired are returned.
     *
     *  @param  eventId <code> Id </code> of the <code> Event </code>
     *  @param  resourceId <code> Id </code> of a <code> Resource </code>
     *  @param  commitmentGenusType commitment genus type
     *  @param  from starting date
     *  @param  to ending date
     *  @return list of commitments
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> eventId, resourceId,
     *          commitmentGenusType, from </code> or <code> to </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */
    
    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitmentsByGenusTypeForEventAndResourceOnDate(org.osid.id.Id eventId,
                                                                                                  org.osid.id.Id resourceId,
                                                                                                  org.osid.type.Type commitmentGenusType,
                                                                                                  org.osid.calendaring.DateTime from,
                                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.calendaring.commitment.TemporalCommitmentFilterList(getCommitmentsByGenusTypeForEventAndResource(eventId, resourceId, commitmentGenusType), from, to));
    }


    /**
     *  Gets all <code>Commitments</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  commitments or an error results. Otherwise, the returned list
     *  may contain only those commitments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, commitments are returned that are currently
     *  effective.  In any effective mode, effective commitments and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Commitments</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.calendaring.CommitmentList getCommitments()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the commitment list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of commitments
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.calendaring.CommitmentList filterCommitmentsOnViews(org.osid.calendaring.CommitmentList list)
        throws org.osid.OperationFailedException {
            
        org.osid.calendaring.CommitmentList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.calendaring.commitment.EffectiveCommitmentFilterList(ret);
        }

        return (ret);
    }


    public static class EventFilter
        implements net.okapia.osid.jamocha.inline.filter.calendaring.commitment.CommitmentFilter {
         
        private final org.osid.id.Id eventId;
         
         
        /**
         *  Constructs a new <code>EventFilter</code>.
         *
         *  @param eventId the event to filter
         *  @throws org.osid.NullArgumentException
         *          <code>eventId</code> is <code>null</code>
         */
        
        public EventFilter(org.osid.id.Id eventId) {
            nullarg(eventId, "event Id");
            this.eventId = eventId;
            return;
        }

         
        /**
         *  Used by the CommitmentFilterList to filter the 
         *  commitment list based on event.
         *
         *  @param commitment the commitment
         *  @return <code>true</code> to pass the commitment,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.calendaring.Commitment commitment) {
            return (commitment.getEventId().equals(this.eventId));
        }
    }


    public static class ResourceFilter
        implements net.okapia.osid.jamocha.inline.filter.calendaring.commitment.CommitmentFilter {
         
        private final org.osid.id.Id resourceId;
         
         
        /**
         *  Constructs a new <code>ResourceFilter</code>.
         *
         *  @param resourceId the resource to filter
         *  @throws org.osid.NullArgumentException
         *          <code>resourceId</code> is <code>null</code>
         */
        
        public ResourceFilter(org.osid.id.Id resourceId) {
            nullarg(resourceId, "resource Id");
            this.resourceId = resourceId;
            return;
        }

         
        /**
         *  Used by the CommitmentFilterList to filter the 
         *  commitment list based on resource.
         *
         *  @param commitment the commitment
         *  @return <code>true</code> to pass the commitment,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.calendaring.Commitment commitment) {
            return (commitment.getResourceId().equals(this.resourceId));
        }
    }
}
