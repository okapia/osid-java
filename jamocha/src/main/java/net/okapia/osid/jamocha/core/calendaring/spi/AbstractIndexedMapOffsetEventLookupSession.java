//
// AbstractIndexedMapOffsetEventLookupSession.java
//
//    A simple framework for providing an OffsetEvent lookup service
//    backed by a fixed collection of offset events with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an OffsetEvent lookup service backed by a
 *  fixed collection of offset events. The offset events are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some offset events may be compatible
 *  with more types than are indicated through these offset event
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>OffsetEvents</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapOffsetEventLookupSession
    extends AbstractMapOffsetEventLookupSession
    implements org.osid.calendaring.OffsetEventLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.calendaring.OffsetEvent> offsetEventsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.calendaring.OffsetEvent>());
    private final MultiMap<org.osid.type.Type, org.osid.calendaring.OffsetEvent> offsetEventsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.calendaring.OffsetEvent>());


    /**
     *  Makes an <code>OffsetEvent</code> available in this session.
     *
     *  @param  offsetEvent an offset event
     *  @throws org.osid.NullArgumentException <code>offsetEvent<code> is
     *          <code>null</code>
     */

    @Override
    protected void putOffsetEvent(org.osid.calendaring.OffsetEvent offsetEvent) {
        super.putOffsetEvent(offsetEvent);

        this.offsetEventsByGenus.put(offsetEvent.getGenusType(), offsetEvent);
        
        try (org.osid.type.TypeList types = offsetEvent.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.offsetEventsByRecord.put(types.getNextType(), offsetEvent);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an offset event from this session.
     *
     *  @param offsetEventId the <code>Id</code> of the offset event
     *  @throws org.osid.NullArgumentException <code>offsetEventId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeOffsetEvent(org.osid.id.Id offsetEventId) {
        org.osid.calendaring.OffsetEvent offsetEvent;
        try {
            offsetEvent = getOffsetEvent(offsetEventId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.offsetEventsByGenus.remove(offsetEvent.getGenusType());

        try (org.osid.type.TypeList types = offsetEvent.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.offsetEventsByRecord.remove(types.getNextType(), offsetEvent);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeOffsetEvent(offsetEventId);
        return;
    }


    /**
     *  Gets an <code>OffsetEventList</code> corresponding to the given
     *  offset event genus <code>Type</code> which does not include
     *  offset events of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known offset events or an error results. Otherwise,
     *  the returned list may contain only those offset events that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  offsetEventGenusType an offset event genus type 
     *  @return the returned <code>OffsetEvent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>offsetEventGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventList getOffsetEventsByGenusType(org.osid.type.Type offsetEventGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.calendaring.offsetevent.ArrayOffsetEventList(this.offsetEventsByGenus.get(offsetEventGenusType)));
    }


    /**
     *  Gets an <code>OffsetEventList</code> containing the given
     *  offset event record <code>Type</code>. In plenary mode, the
     *  returned list contains all known offset events or an error
     *  results. Otherwise, the returned list may contain only those
     *  offset events that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  offsetEventRecordType an offset event record type 
     *  @return the returned <code>offsetEvent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>offsetEventRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventList getOffsetEventsByRecordType(org.osid.type.Type offsetEventRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.calendaring.offsetevent.ArrayOffsetEventList(this.offsetEventsByRecord.get(offsetEventRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.offsetEventsByGenus.clear();
        this.offsetEventsByRecord.clear();

        super.close();

        return;
    }
}
