//
// AbstractTriggerEnablerSearch.java
//
//     A template for making a TriggerEnabler Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.rules.triggerenabler.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing trigger enabler searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractTriggerEnablerSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.control.rules.TriggerEnablerSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.control.rules.records.TriggerEnablerSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.control.rules.TriggerEnablerSearchOrder triggerEnablerSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of trigger enablers. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  triggerEnablerIds list of trigger enablers
     *  @throws org.osid.NullArgumentException
     *          <code>triggerEnablerIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongTriggerEnablers(org.osid.id.IdList triggerEnablerIds) {
        while (triggerEnablerIds.hasNext()) {
            try {
                this.ids.add(triggerEnablerIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongTriggerEnablers</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of trigger enabler Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getTriggerEnablerIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  triggerEnablerSearchOrder trigger enabler search order 
     *  @throws org.osid.NullArgumentException
     *          <code>triggerEnablerSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>triggerEnablerSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderTriggerEnablerResults(org.osid.control.rules.TriggerEnablerSearchOrder triggerEnablerSearchOrder) {
	this.triggerEnablerSearchOrder = triggerEnablerSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.control.rules.TriggerEnablerSearchOrder getTriggerEnablerSearchOrder() {
	return (this.triggerEnablerSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given trigger enabler search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a trigger enabler implementing the requested record.
     *
     *  @param triggerEnablerSearchRecordType a trigger enabler search record
     *         type
     *  @return the trigger enabler search record
     *  @throws org.osid.NullArgumentException
     *          <code>triggerEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(triggerEnablerSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.control.rules.records.TriggerEnablerSearchRecord getTriggerEnablerSearchRecord(org.osid.type.Type triggerEnablerSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.control.rules.records.TriggerEnablerSearchRecord record : this.records) {
            if (record.implementsRecordType(triggerEnablerSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(triggerEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this trigger enabler search. 
     *
     *  @param triggerEnablerSearchRecord trigger enabler search record
     *  @param triggerEnablerSearchRecordType triggerEnabler search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addTriggerEnablerSearchRecord(org.osid.control.rules.records.TriggerEnablerSearchRecord triggerEnablerSearchRecord, 
                                           org.osid.type.Type triggerEnablerSearchRecordType) {

        addRecordType(triggerEnablerSearchRecordType);
        this.records.add(triggerEnablerSearchRecord);        
        return;
    }
}
