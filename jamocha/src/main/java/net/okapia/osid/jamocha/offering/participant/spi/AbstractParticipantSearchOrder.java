//
// AbstractParticipantSearchOdrer.java
//
//     Defines a ParticipantSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.participant.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code ParticipantSearchOrder}.
 */

public abstract class AbstractParticipantSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipSearchOrder
    implements org.osid.offering.ParticipantSearchOrder {

    private final java.util.Collection<org.osid.offering.records.ParticipantSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specifies a preference for ordering the result set by the offering. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByOffering(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a offering search order is available. 
     *
     *  @return <code> true </code> if an offering search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingSearchOrder() {
        return (false);
    }


    /**
     *  Gets the offering search order. 
     *
     *  @return the offering search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.OfferingSearchOrder getOfferingSearchOrder() {
        throw new org.osid.UnimplementedException("supportsOfferingSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the resource. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByResource(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a resource search order is available. 
     *
     *  @return <code> true </code> if a offering search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getResourceSearchOrder() {
        throw new org.osid.UnimplementedException("supportsResourceSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by time period. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTimePeriod(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a time period order is available. 
     *
     *  @return <code> true </code> if a time period order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTimePeriodSearchOrder() {
        return (false);
    }


    /**
     *  Gets the time period order. 
     *
     *  @return the time period search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodSearchOrder getTimePeriodSearchOrder() {
        throw new org.osid.UnimplementedException("supportsTimePeriodSearchOrder() is false");
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  participantRecordType a participant record type 
     *  @return {@code true} if the participantRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code participantRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type participantRecordType) {
        for (org.osid.offering.records.ParticipantSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(participantRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  participantRecordType the participant record type 
     *  @return the participant search order record
     *  @throws org.osid.NullArgumentException
     *          {@code participantRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(participantRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.offering.records.ParticipantSearchOrderRecord getParticipantSearchOrderRecord(org.osid.type.Type participantRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.offering.records.ParticipantSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(participantRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(participantRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this participant. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param participantRecord the participant search odrer record
     *  @param participantRecordType participant record type
     *  @throws org.osid.NullArgumentException
     *          {@code participantRecord} or
     *          {@code participantRecordTypeparticipant} is
     *          {@code null}
     */
            
    protected void addParticipantRecord(org.osid.offering.records.ParticipantSearchOrderRecord participantSearchOrderRecord, 
                                     org.osid.type.Type participantRecordType) {

        addRecordType(participantRecordType);
        this.records.add(participantSearchOrderRecord);
        
        return;
    }
}
