//
// PriceScheduleElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ordering.priceschedule.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class PriceScheduleElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the PriceScheduleElement Id.
     *
     *  @return the price schedule element Id
     */

    public static org.osid.id.Id getPriceScheduleEntityId() {
        return (makeEntityId("osid.ordering.PriceSchedule"));
    }


    /**
     *  Gets the PriceIds element Id.
     *
     *  @return the PriceIds element Id
     */

    public static org.osid.id.Id getPriceIds() {
        return (makeElementId("osid.ordering.priceschedule.PriceIds"));
    }


    /**
     *  Gets the Prices element Id.
     *
     *  @return the Prices element Id
     */

    public static org.osid.id.Id getPrices() {
        return (makeElementId("osid.ordering.priceschedule.Prices"));
    }


    /**
     *  Gets the ProductId element Id.
     *
     *  @return the ProductId element Id
     */

    public static org.osid.id.Id getProductId() {
        return (makeQueryElementId("osid.ordering.priceschedule.ProductId"));
    }


    /**
     *  Gets the Product element Id.
     *
     *  @return the Product element Id
     */

    public static org.osid.id.Id getProduct() {
        return (makeQueryElementId("osid.ordering.priceschedule.Product"));
    }


    /**
     *  Gets the StoreId element Id.
     *
     *  @return the StoreId element Id
     */

    public static org.osid.id.Id getStoreId() {
        return (makeQueryElementId("osid.ordering.priceschedule.StoreId"));
    }


    /**
     *  Gets the Store element Id.
     *
     *  @return the Store element Id
     */

    public static org.osid.id.Id getStore() {
        return (makeQueryElementId("osid.ordering.priceschedule.Store"));
    }
}
