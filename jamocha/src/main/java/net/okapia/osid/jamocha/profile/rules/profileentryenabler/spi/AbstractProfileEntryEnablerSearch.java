//
// AbstractProfileEntryEnablerSearch.java
//
//     A template for making a ProfileEntryEnabler Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.profile.rules.profileentryenabler.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing profile entry enabler searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractProfileEntryEnablerSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.profile.rules.ProfileEntryEnablerSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.profile.rules.records.ProfileEntryEnablerSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.profile.rules.ProfileEntryEnablerSearchOrder profileEntryEnablerSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of profile entry enablers. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  profileEntryEnablerIds list of profile entry enablers
     *  @throws org.osid.NullArgumentException
     *          <code>profileEntryEnablerIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongProfileEntryEnablers(org.osid.id.IdList profileEntryEnablerIds) {
        while (profileEntryEnablerIds.hasNext()) {
            try {
                this.ids.add(profileEntryEnablerIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongProfileEntryEnablers</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of profile entry enabler Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getProfileEntryEnablerIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  profileEntryEnablerSearchOrder profile entry enabler search order 
     *  @throws org.osid.NullArgumentException
     *          <code>profileEntryEnablerSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>profileEntryEnablerSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderProfileEntryEnablerResults(org.osid.profile.rules.ProfileEntryEnablerSearchOrder profileEntryEnablerSearchOrder) {
	this.profileEntryEnablerSearchOrder = profileEntryEnablerSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.profile.rules.ProfileEntryEnablerSearchOrder getProfileEntryEnablerSearchOrder() {
	return (this.profileEntryEnablerSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given profile entry enabler search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a profile entry enabler implementing the requested record.
     *
     *  @param profileEntryEnablerSearchRecordType a profile entry enabler search record
     *         type
     *  @return the profile entry enabler search record
     *  @throws org.osid.NullArgumentException
     *          <code>profileEntryEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(profileEntryEnablerSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.profile.rules.records.ProfileEntryEnablerSearchRecord getProfileEntryEnablerSearchRecord(org.osid.type.Type profileEntryEnablerSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.profile.rules.records.ProfileEntryEnablerSearchRecord record : this.records) {
            if (record.implementsRecordType(profileEntryEnablerSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(profileEntryEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this profile entry enabler search. 
     *
     *  @param profileEntryEnablerSearchRecord profile entry enabler search record
     *  @param profileEntryEnablerSearchRecordType profileEntryEnabler search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addProfileEntryEnablerSearchRecord(org.osid.profile.rules.records.ProfileEntryEnablerSearchRecord profileEntryEnablerSearchRecord, 
                                           org.osid.type.Type profileEntryEnablerSearchRecordType) {

        addRecordType(profileEntryEnablerSearchRecordType);
        this.records.add(profileEntryEnablerSearchRecord);        
        return;
    }
}
