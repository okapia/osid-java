//
// AbstractAuthorizationEnablerQueryInspector.java
//
//     A template for making an AuthorizationEnablerQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authorization.rules.authorizationenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for authorization enablers.
 */

public abstract class AbstractAuthorizationEnablerQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQueryInspector
    implements org.osid.authorization.rules.AuthorizationEnablerQueryInspector {

    private final java.util.Collection<org.osid.authorization.rules.records.AuthorizationEnablerQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the authorization <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledAuthorizationIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the authorization query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationQueryInspector[] getRuledAuthorizationTerms() {
        return (new org.osid.authorization.AuthorizationQueryInspector[0]);
    }


    /**
     *  Gets the vault <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getVaultIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the vault query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authorization.VaultQueryInspector[] getVaultTerms() {
        return (new org.osid.authorization.VaultQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given authorization enabler query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an authorization enabler implementing the requested record.
     *
     *  @param authorizationEnablerRecordType an authorization enabler record type
     *  @return the authorization enabler query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>authorizationEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(authorizationEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.authorization.rules.records.AuthorizationEnablerQueryInspectorRecord getAuthorizationEnablerQueryInspectorRecord(org.osid.type.Type authorizationEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.authorization.rules.records.AuthorizationEnablerQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(authorizationEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(authorizationEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this authorization enabler query. 
     *
     *  @param authorizationEnablerQueryInspectorRecord authorization enabler query inspector
     *         record
     *  @param authorizationEnablerRecordType authorizationEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAuthorizationEnablerQueryInspectorRecord(org.osid.authorization.rules.records.AuthorizationEnablerQueryInspectorRecord authorizationEnablerQueryInspectorRecord, 
                                                   org.osid.type.Type authorizationEnablerRecordType) {

        addRecordType(authorizationEnablerRecordType);
        nullarg(authorizationEnablerRecordType, "authorization enabler record type");
        this.records.add(authorizationEnablerQueryInspectorRecord);        
        return;
    }
}
