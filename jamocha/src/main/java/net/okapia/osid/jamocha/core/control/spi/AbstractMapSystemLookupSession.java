//
// AbstractMapSystemLookupSession
//
//    A simple framework for providing a System lookup service
//    backed by a fixed collection of systems.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.control.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a System lookup service backed by a
 *  fixed collection of systems. The systems are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Systems</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapSystemLookupSession
    extends net.okapia.osid.jamocha.control.spi.AbstractSystemLookupSession
    implements org.osid.control.SystemLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.control.System> systems = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.control.System>());


    /**
     *  Makes a <code>System</code> available in this session.
     *
     *  @param  system a system
     *  @throws org.osid.NullArgumentException <code>system<code>
     *          is <code>null</code>
     */

    protected void putSystem(org.osid.control.System system) {
        this.systems.put(system.getId(), system);
        return;
    }


    /**
     *  Makes an array of systems available in this session.
     *
     *  @param  systems an array of systems
     *  @throws org.osid.NullArgumentException <code>systems<code>
     *          is <code>null</code>
     */

    protected void putSystems(org.osid.control.System[] systems) {
        putSystems(java.util.Arrays.asList(systems));
        return;
    }


    /**
     *  Makes a collection of systems available in this session.
     *
     *  @param  systems a collection of systems
     *  @throws org.osid.NullArgumentException <code>systems<code>
     *          is <code>null</code>
     */

    protected void putSystems(java.util.Collection<? extends org.osid.control.System> systems) {
        for (org.osid.control.System system : systems) {
            this.systems.put(system.getId(), system);
        }

        return;
    }


    /**
     *  Removes a System from this session.
     *
     *  @param  systemId the <code>Id</code> of the system
     *  @throws org.osid.NullArgumentException <code>systemId<code> is
     *          <code>null</code>
     */

    protected void removeSystem(org.osid.id.Id systemId) {
        this.systems.remove(systemId);
        return;
    }


    /**
     *  Gets the <code>System</code> specified by its <code>Id</code>.
     *
     *  @param  systemId <code>Id</code> of the <code>System</code>
     *  @return the system
     *  @throws org.osid.NotFoundException <code>systemId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>systemId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.System getSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.control.System system = this.systems.get(systemId);
        if (system == null) {
            throw new org.osid.NotFoundException("system not found: " + systemId);
        }

        return (system);
    }


    /**
     *  Gets all <code>Systems</code>. In plenary mode, the returned
     *  list contains all known systems or an error
     *  results. Otherwise, the returned list may contain only those
     *  systems that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Systems</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.SystemList getSystems()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.control.system.ArraySystemList(this.systems.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.systems.clear();
        super.close();
        return;
    }
}
