//
// AbstractIndexedMapObjectiveBankLookupSession.java
//
//    A simple framework for providing an ObjectiveBank lookup service
//    backed by a fixed collection of objective banks with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.learning.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an ObjectiveBank lookup service backed by a
 *  fixed collection of objective banks. The objective banks are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some objective banks may be compatible
 *  with more types than are indicated through these objective bank
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>ObjectiveBanks</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapObjectiveBankLookupSession
    extends AbstractMapObjectiveBankLookupSession
    implements org.osid.learning.ObjectiveBankLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.learning.ObjectiveBank> objectiveBanksByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.learning.ObjectiveBank>());
    private final MultiMap<org.osid.type.Type, org.osid.learning.ObjectiveBank> objectiveBanksByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.learning.ObjectiveBank>());


    /**
     *  Makes an <code>ObjectiveBank</code> available in this session.
     *
     *  @param  objectiveBank an objective bank
     *  @throws org.osid.NullArgumentException <code>objectiveBank<code> is
     *          <code>null</code>
     */

    @Override
    protected void putObjectiveBank(org.osid.learning.ObjectiveBank objectiveBank) {
        super.putObjectiveBank(objectiveBank);

        this.objectiveBanksByGenus.put(objectiveBank.getGenusType(), objectiveBank);
        
        try (org.osid.type.TypeList types = objectiveBank.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.objectiveBanksByRecord.put(types.getNextType(), objectiveBank);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an objective bank from this session.
     *
     *  @param objectiveBankId the <code>Id</code> of the objective bank
     *  @throws org.osid.NullArgumentException <code>objectiveBankId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeObjectiveBank(org.osid.id.Id objectiveBankId) {
        org.osid.learning.ObjectiveBank objectiveBank;
        try {
            objectiveBank = getObjectiveBank(objectiveBankId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.objectiveBanksByGenus.remove(objectiveBank.getGenusType());

        try (org.osid.type.TypeList types = objectiveBank.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.objectiveBanksByRecord.remove(types.getNextType(), objectiveBank);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeObjectiveBank(objectiveBankId);
        return;
    }


    /**
     *  Gets an <code>ObjectiveBankList</code> corresponding to the given
     *  objective bank genus <code>Type</code> which does not include
     *  objective banks of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known objective banks or an error results. Otherwise,
     *  the returned list may contain only those objective banks that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  objectiveBankGenusType an objective bank genus type 
     *  @return the returned <code>ObjectiveBank</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveBankGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankList getObjectiveBanksByGenusType(org.osid.type.Type objectiveBankGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.learning.objectivebank.ArrayObjectiveBankList(this.objectiveBanksByGenus.get(objectiveBankGenusType)));
    }


    /**
     *  Gets an <code>ObjectiveBankList</code> containing the given
     *  objective bank record <code>Type</code>. In plenary mode, the
     *  returned list contains all known objective banks or an error
     *  results. Otherwise, the returned list may contain only those
     *  objective banks that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  objectiveBankRecordType an objective bank record type 
     *  @return the returned <code>objectiveBank</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveBankRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankList getObjectiveBanksByRecordType(org.osid.type.Type objectiveBankRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.learning.objectivebank.ArrayObjectiveBankList(this.objectiveBanksByRecord.get(objectiveBankRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.objectiveBanksByGenus.clear();
        this.objectiveBanksByRecord.clear();

        super.close();

        return;
    }
}
