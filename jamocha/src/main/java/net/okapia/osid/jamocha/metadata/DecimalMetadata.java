//
// DecimalMetadata.java
//
//     Defines a decimal Metadata.
//
//
// Tom Coppeto
// Okapia
// 11 January 2023
//
//
// Copyright (c) 2023 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.metadata;


/**
 *  Defines a decimal Metadata.
 */

public final class DecimalMetadata
    extends net.okapia.osid.jamocha.metadata.spi.AbstractDecimalMetadata
    implements org.osid.Metadata {


    /**
     *  Constructs a new single unlinked {@code DecimalMetadata}.
     *
     *  @param elementId the Id of the element
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    public DecimalMetadata(org.osid.id.Id elementId) {
        super(elementId);
        return;
    }


    /**
     *  Constructs a new unlinked {@code DecimalMetadata}.
     *
     *  @param elementId the Id of the element
     *  @param isArray {@code true} if the element is an array another
     *         element, {@code false} if a single element
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    public DecimalMetadata(org.osid.id.Id elementId, boolean isArray) {
        super(elementId, isArray, false);
        return;
    }

    
    /**
     *  Constructs a new {@code DecimalMetadata}.
     *
     *  @param elementId the Id of the element
     *  @param isArray {@code true} if the element is an array another
     *         element, {@code false} if a single element
     *  @param isLinked {@code true} if the element is linked to
     *         another element, {@code false} otherwise
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    public DecimalMetadata(org.osid.id.Id elementId, boolean isArray, boolean isLinked) {
        super(elementId, isArray, isLinked);
        return;
    }


    /**
     *  Sets the element label.
     *
     *  @param label the new element label
     *  @throws org.osid.NullArgumentException {@code label} is {@code
     *          null}
     */

    public void setLabel(org.osid.locale.DisplayText label) {
        super.setLabel(label);
	return;
    }


    /**
     *  Sets the instructions.
     *
     *  @param instructions the new instructions
     *  @throws org.osid.NullArgumentException {@code instructions}
     *  	is {@code null}
     */

    public void setInstructions(org.osid.locale.DisplayText instructions) {
        super.setInstructions(instructions);
        return;
    }


    /**
     *  Sets the required flag.
     *
     *  @param required {@code true} if required, {@code false} if
     *         optional
     */

    public void setRequired(boolean required) {
        super.setRequired(required);
        return;
    }


    /**
     *  Sets the has value flag.
     *
     *  @param exists {@code true} if has existing value, {@code
     *         false} if no value exists
     */

    public void setValueExists(boolean exists) {
        super.setValueExists(exists);
        return;
    }


    /**
     *  Sets the read only flag.
     *
     *  @param readonly {@code true} if read only, {@code
     *         false} if can be updated
     */

    public void setReadOnly(boolean readonly) {
        super.setReadOnly(readonly);
        return;
    }


    /**
     *  Sets the units.
     *
     *	@param units the new units
     *  @throws org.osid.NullArgumentException {@code units}
     *          is {@code null}
     */

    public void setUnits(org.osid.locale.DisplayText units) {
        super.setUnits(units);
        return;
    }
   

    /**
     *  Sets the scale.
     *
     *  @param scale the number of digits to the right of the decimal
     *         point
     *  @throws org.osid.InvalidArgumentException {@code scale} is
     *          negative
     */

    public void setScale(long scale) {
        super.setScale(scale);
        return;
    }

    
    /**
     *  Sets the decimal range.
     *
     *  @param min the minimum value
     *  @param max the maximum value
     *  @throws org.osid.InvalidArgumentException {@code min} is
     *          greater than {@code max}
     */

    public void setDecimalRange(java.math.BigDecimal min, java.math.BigDecimal max) {
        super.setDecimalRange(min, max);
        return;
    }


    /**
     *  Sets the decimal set.
     *
     *  @param values a collection of accepted decimal values
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    public void setDecimalSet(java.util.Collection<java.math.BigDecimal> values) {
        super.setDecimalSet(values);
        return;
    }


    /**
     *  Adds a collection of values to the decimal set.
     *
     *  @param values a collection of accepted decimal values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    public void addToDecimalSet(java.util.Collection<java.math.BigDecimal> values) {
        super.addToDecimalSet(values);
        return;
    }


    /**
     *  Adds a value to the decimal set.
     *
     *  @param value a decimal value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *         null}
     */

    public void addToDecimalSet(java.math.BigDecimal value) {
        super.addToDecimalSet(value);
        return;
    }


    /**
     *  Removes a value from the decimal set.
     *
     *  @param value a decimal value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *         null}
     */

    public void removeFromDecimalSet(java.math.BigDecimal value) {
        super.removeFromDecimalSet(value);
        return;
    }


    /**
     *  Clears the decimal set.
     */

    public void clearDecimalSet() {
        super.clearDecimalSet();
        return;
    }


    /**
     *  Sets the default decimal set.
     *
     *  @param values a collection of default decimal values
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    public void setDefaultDecimalValues(java.util.Collection<java.math.BigDecimal> values) {
        super.setDefaultDecimalValues(values);
        return;
    }


    /**
     *  Adds a collection of default decimal values.
     *
     *  @param values a collection of default decimal values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    public void addDefaultDecimalValues(java.util.Collection<java.math.BigDecimal> values) {
        super.addDefaultDecimalValues(values);
        return;
    }


    /**
     *  Adds a default decimal value.
     *
     *  @param value a decimal value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void addDefaultDecimalValue(java.math.BigDecimal value) {
        super.addDefaultDecimalValue(value);
        return;
    }


    /**
     *  Removes a default decimal value.
     *
     *  @param value a decimal value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void removeDefaultDecimalValue(java.math.BigDecimal value) {
        super.removeDefaultDecimalValue(value);
        return;
    }


    /**
     *  Clears the default decimal values.
     */

    public void clearDefaultDecimalValues() {
        super.clearDefaultDecimalValues();
        return;
    }


    /**
     *  Sets the existing decimal set.
     *
     *  @param values a collection of existing decimal values
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    public void setExistingDecimalValues(java.util.Collection<java.math.BigDecimal> values) {
        super.setExistingDecimalValues(values);
        return;
    }


    /**
     *  Adds a collection of existing decimal values.
     *
     *  @param values a collection of existing decimal values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    public void addExistingDecimalValues(java.util.Collection<java.math.BigDecimal> values) {
        super.addExistingDecimalValues(values);
        return;
    }


    /**
     *  Adds a existing decimal value.
     *
     *  @param value a decimal value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void addExistingDecimalValue(java.math.BigDecimal value) {
        super.addExistingDecimalValue(value);
        return;
    }


    /**
     *  Removes a existing decimal value.
     *
     *  @param value a decimal value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void removeExistingDecimalValue(java.math.BigDecimal value) {
        super.removeExistingDecimalValue(value);
        return;
    }


    /**
     *  Clears the existing decimal values.
     */

    public void clearExistingDecimalValues() {
        super.clearExistingDecimalValues();
        return;
    }        
}
