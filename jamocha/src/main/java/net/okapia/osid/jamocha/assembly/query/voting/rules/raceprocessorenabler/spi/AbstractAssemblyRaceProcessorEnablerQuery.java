//
// AbstractAssemblyRaceProcessorEnablerQuery.java
//
//     A RaceProcessorEnablerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.voting.rules.raceprocessorenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A RaceProcessorEnablerQuery that stores terms.
 */

public abstract class AbstractAssemblyRaceProcessorEnablerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidEnablerQuery
    implements org.osid.voting.rules.RaceProcessorEnablerQuery,
               org.osid.voting.rules.RaceProcessorEnablerQueryInspector,
               org.osid.voting.rules.RaceProcessorEnablerSearchOrder {

    private final java.util.Collection<org.osid.voting.rules.records.RaceProcessorEnablerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.voting.rules.records.RaceProcessorEnablerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.voting.rules.records.RaceProcessorEnablerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyRaceProcessorEnablerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyRaceProcessorEnablerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches enablers mapped to the race processor. 
     *
     *  @param  raceProcessorId the race processor <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> raceProcessorId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledRaceProcessorId(org.osid.id.Id raceProcessorId, 
                                          boolean match) {
        getAssembler().addIdTerm(getRuledRaceProcessorIdColumn(), raceProcessorId, match);
        return;
    }


    /**
     *  Clears the race processor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledRaceProcessorIdTerms() {
        getAssembler().clearTerms(getRuledRaceProcessorIdColumn());
        return;
    }


    /**
     *  Gets the race processor <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledRaceProcessorIdTerms() {
        return (getAssembler().getIdTerms(getRuledRaceProcessorIdColumn()));
    }


    /**
     *  Gets the RuledRaceProcessorId column name.
     *
     * @return the column name
     */

    protected String getRuledRaceProcessorIdColumn() {
        return ("ruled_race_processor_id");
    }


    /**
     *  Tests if a <code> RaceProcessorQuery </code> is available. 
     *
     *  @return <code> true </code> if a race processor query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledRaceProcessorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a race processor. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the race processor query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledRaceProcessorQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorQuery getRuledRaceProcessorQuery() {
        throw new org.osid.UnimplementedException("supportsRuledRaceProcessorQuery() is false");
    }


    /**
     *  Matches enablers mapped to any race processor. 
     *
     *  @param  match <code> true </code> for enablers mapped to any race 
     *          processor, <code> false </code> to match enablers mapped to no 
     *          race processors 
     */

    @OSID @Override
    public void matchAnyRuledRaceProcessor(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledRaceProcessorColumn(), match);
        return;
    }


    /**
     *  Clears the race processor query terms. 
     */

    @OSID @Override
    public void clearRuledRaceProcessorTerms() {
        getAssembler().clearTerms(getRuledRaceProcessorColumn());
        return;
    }


    /**
     *  Gets the race processor query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorQueryInspector[] getRuledRaceProcessorTerms() {
        return (new org.osid.voting.rules.RaceProcessorQueryInspector[0]);
    }


    /**
     *  Gets the RuledRaceProcessor column name.
     *
     * @return the column name
     */

    protected String getRuledRaceProcessorColumn() {
        return ("ruled_race_processor");
    }


    /**
     *  Matches enablers mapped to the polls. 
     *
     *  @param  pollsId the polls <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPollsId(org.osid.id.Id pollsId, boolean match) {
        getAssembler().addIdTerm(getPollsIdColumn(), pollsId, match);
        return;
    }


    /**
     *  Clears the polls <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearPollsIdTerms() {
        getAssembler().clearTerms(getPollsIdColumn());
        return;
    }


    /**
     *  Gets the polls <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPollsIdTerms() {
        return (getAssembler().getIdTerms(getPollsIdColumn()));
    }


    /**
     *  Gets the PollsId column name.
     *
     * @return the column name
     */

    protected String getPollsIdColumn() {
        return ("polls_id");
    }


    /**
     *  Tests if a <code> PollsQuery </code> is available. 
     *
     *  @return <code> true </code> if a polls query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPollsQuery() {
        return (false);
    }


    /**
     *  Gets the query for a polls. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the polls query 
     *  @throws org.osid.UnimplementedException <code> supportsPollsQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.PollsQuery getPollsQuery() {
        throw new org.osid.UnimplementedException("supportsPollsQuery() is false");
    }


    /**
     *  Clears the polls query terms. 
     */

    @OSID @Override
    public void clearPollsTerms() {
        getAssembler().clearTerms(getPollsColumn());
        return;
    }


    /**
     *  Gets the polls query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.voting.PollsQueryInspector[] getPollsTerms() {
        return (new org.osid.voting.PollsQueryInspector[0]);
    }


    /**
     *  Gets the Polls column name.
     *
     * @return the column name
     */

    protected String getPollsColumn() {
        return ("polls");
    }


    /**
     *  Tests if this raceProcessorEnabler supports the given record
     *  <code>Type</code>.
     *
     *  @param  raceProcessorEnablerRecordType a race processor enabler record type 
     *  @return <code>true</code> if the raceProcessorEnablerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>raceProcessorEnablerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type raceProcessorEnablerRecordType) {
        for (org.osid.voting.rules.records.RaceProcessorEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(raceProcessorEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  raceProcessorEnablerRecordType the race processor enabler record type 
     *  @return the race processor enabler query record 
     *  @throws org.osid.NullArgumentException
     *          <code>raceProcessorEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(raceProcessorEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.rules.records.RaceProcessorEnablerQueryRecord getRaceProcessorEnablerQueryRecord(org.osid.type.Type raceProcessorEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.rules.records.RaceProcessorEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(raceProcessorEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(raceProcessorEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  raceProcessorEnablerRecordType the race processor enabler record type 
     *  @return the race processor enabler query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>raceProcessorEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(raceProcessorEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.rules.records.RaceProcessorEnablerQueryInspectorRecord getRaceProcessorEnablerQueryInspectorRecord(org.osid.type.Type raceProcessorEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.rules.records.RaceProcessorEnablerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(raceProcessorEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(raceProcessorEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param raceProcessorEnablerRecordType the race processor enabler record type
     *  @return the race processor enabler search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>raceProcessorEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(raceProcessorEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.rules.records.RaceProcessorEnablerSearchOrderRecord getRaceProcessorEnablerSearchOrderRecord(org.osid.type.Type raceProcessorEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.rules.records.RaceProcessorEnablerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(raceProcessorEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(raceProcessorEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this race processor enabler. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param raceProcessorEnablerQueryRecord the race processor enabler query record
     *  @param raceProcessorEnablerQueryInspectorRecord the race processor enabler query inspector
     *         record
     *  @param raceProcessorEnablerSearchOrderRecord the race processor enabler search order record
     *  @param raceProcessorEnablerRecordType race processor enabler record type
     *  @throws org.osid.NullArgumentException
     *          <code>raceProcessorEnablerQueryRecord</code>,
     *          <code>raceProcessorEnablerQueryInspectorRecord</code>,
     *          <code>raceProcessorEnablerSearchOrderRecord</code> or
     *          <code>raceProcessorEnablerRecordTyperaceProcessorEnabler</code> is
     *          <code>null</code>
     */
            
    protected void addRaceProcessorEnablerRecords(org.osid.voting.rules.records.RaceProcessorEnablerQueryRecord raceProcessorEnablerQueryRecord, 
                                      org.osid.voting.rules.records.RaceProcessorEnablerQueryInspectorRecord raceProcessorEnablerQueryInspectorRecord, 
                                      org.osid.voting.rules.records.RaceProcessorEnablerSearchOrderRecord raceProcessorEnablerSearchOrderRecord, 
                                      org.osid.type.Type raceProcessorEnablerRecordType) {

        addRecordType(raceProcessorEnablerRecordType);

        nullarg(raceProcessorEnablerQueryRecord, "race processor enabler query record");
        nullarg(raceProcessorEnablerQueryInspectorRecord, "race processor enabler query inspector record");
        nullarg(raceProcessorEnablerSearchOrderRecord, "race processor enabler search odrer record");

        this.queryRecords.add(raceProcessorEnablerQueryRecord);
        this.queryInspectorRecords.add(raceProcessorEnablerQueryInspectorRecord);
        this.searchOrderRecords.add(raceProcessorEnablerSearchOrderRecord);
        
        return;
    }
}
