//
// AbstractIndexedMapTodoProducerLookupSession.java
//
//    A simple framework for providing a TodoProducer lookup service
//    backed by a fixed collection of todo producers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.checklist.mason.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a TodoProducer lookup service backed by a
 *  fixed collection of todo producers. The todo producers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some todo producers may be compatible
 *  with more types than are indicated through these todo producer
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>TodoProducers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapTodoProducerLookupSession
    extends AbstractMapTodoProducerLookupSession
    implements org.osid.checklist.mason.TodoProducerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.checklist.mason.TodoProducer> todoProducersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.checklist.mason.TodoProducer>());
    private final MultiMap<org.osid.type.Type, org.osid.checklist.mason.TodoProducer> todoProducersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.checklist.mason.TodoProducer>());


    /**
     *  Makes a <code>TodoProducer</code> available in this session.
     *
     *  @param  todoProducer a todo producer
     *  @throws org.osid.NullArgumentException <code>todoProducer<code> is
     *          <code>null</code>
     */

    @Override
    protected void putTodoProducer(org.osid.checklist.mason.TodoProducer todoProducer) {
        super.putTodoProducer(todoProducer);

        this.todoProducersByGenus.put(todoProducer.getGenusType(), todoProducer);
        
        try (org.osid.type.TypeList types = todoProducer.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.todoProducersByRecord.put(types.getNextType(), todoProducer);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a todo producer from this session.
     *
     *  @param todoProducerId the <code>Id</code> of the todo producer
     *  @throws org.osid.NullArgumentException <code>todoProducerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeTodoProducer(org.osid.id.Id todoProducerId) {
        org.osid.checklist.mason.TodoProducer todoProducer;
        try {
            todoProducer = getTodoProducer(todoProducerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.todoProducersByGenus.remove(todoProducer.getGenusType());

        try (org.osid.type.TypeList types = todoProducer.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.todoProducersByRecord.remove(types.getNextType(), todoProducer);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeTodoProducer(todoProducerId);
        return;
    }


    /**
     *  Gets a <code>TodoProducerList</code> corresponding to the given
     *  todo producer genus <code>Type</code> which does not include
     *  todo producers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known todo producers or an error results. Otherwise,
     *  the returned list may contain only those todo producers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  todoProducerGenusType a todo producer genus type 
     *  @return the returned <code>TodoProducer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>todoProducerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerList getTodoProducersByGenusType(org.osid.type.Type todoProducerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.checklist.mason.todoproducer.ArrayTodoProducerList(this.todoProducersByGenus.get(todoProducerGenusType)));
    }


    /**
     *  Gets a <code>TodoProducerList</code> containing the given
     *  todo producer record <code>Type</code>. In plenary mode, the
     *  returned list contains all known todo producers or an error
     *  results. Otherwise, the returned list may contain only those
     *  todo producers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  todoProducerRecordType a todo producer record type 
     *  @return the returned <code>todoProducer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>todoProducerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerList getTodoProducersByRecordType(org.osid.type.Type todoProducerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.checklist.mason.todoproducer.ArrayTodoProducerList(this.todoProducersByRecord.get(todoProducerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.todoProducersByGenus.clear();
        this.todoProducersByRecord.clear();

        super.close();

        return;
    }
}
