//
// AbstractImmutableFiscalPeriod.java
//
//     Wraps a mutable FiscalPeriod to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.financials.fiscalperiod.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>FiscalPeriod</code> to hide modifiers. This
 *  wrapper provides an immutized FiscalPeriod from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying fiscalPeriod whose state changes are visible.
 */

public abstract class AbstractImmutableFiscalPeriod
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.financials.FiscalPeriod {

    private final org.osid.financials.FiscalPeriod fiscalPeriod;


    /**
     *  Constructs a new <code>AbstractImmutableFiscalPeriod</code>.
     *
     *  @param fiscalPeriod the fiscal period to immutablize
     *  @throws org.osid.NullArgumentException <code>fiscalPeriod</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableFiscalPeriod(org.osid.financials.FiscalPeriod fiscalPeriod) {
        super(fiscalPeriod);
        this.fiscalPeriod = fiscalPeriod;
        return;
    }


    /**
     *  Gets a display label for this fiscal period which may be less formal 
     *  than the display name. 
     *
     *  @return the display label 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getDisplayLabel() {
        return (this.fiscalPeriod.getDisplayLabel());
    }


    /**
     *  Ges the fiscal year. 
     *
     *  @return the fiscal year 
     */

    @OSID @Override
    public long getFiscalYear() {
        return (this.fiscalPeriod.getFiscalYear());
    }


    /**
     *  Get sthe start date of this period. 
     *
     *  @return the start date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getStartDate() {
        return (this.fiscalPeriod.getStartDate());
    }


    /**
     *  Get sthe end date of this period. 
     *
     *  @return the end date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getEndDate() {
        return (this.fiscalPeriod.getEndDate());
    }


    /**
     *  Tests if this fiscal period has milestones. 
     *
     *  @return <code> true </code> if milestones are available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasMilestones() {
        return (this.fiscalPeriod.hasMilestones());
    }


    /**
     *  Tests if budgets need to be submiited for this fiscal period. 
     *
     *  @return <code> true </code> if budgets require submission, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.IllegalStateException <code> hasMilestones() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public boolean requiresBudgetSubmission() {
        return (this.fiscalPeriod.requiresBudgetSubmission());
    }


    /**
     *  Gets the budget deadline. 
     *
     *  @return the closing date 
     *  @throws org.osid.IllegalStateException <code> hasMilestones() </code> 
     *          or <code> requiresBudgetSubmission() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getBudgetDeadline() {
        return (this.fiscalPeriod.getBudgetDeadline());
    }


    /**
     *  Tests if this period has a cutoff date for posting transactions. 
     *
     *  @return <code> true </code> if a posting deadline date is available, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.IllegalStateException <code> hasMilestones() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public boolean hasPostingDeadline() {
        return (this.fiscalPeriod.hasPostingDeadline());
    }


    /**
     *  Gets the last date transactions can be posted for this period. 
     *
     *  @return the cutoff date 
     *  @throws org.osid.IllegalStateException <code> hasMilestones() </code> 
     *          or <code> hasPostingDeadline() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getPostingDeadline() {
        return (this.fiscalPeriod.getPostingDeadline());
    }


    /**
     *  Tests if this period has a closing date. 
     *
     *  @return <code> true </code> if a closing date is available, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.IllegalStateException <code> hasMilestones() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public boolean hasClosing() {
        return (this.fiscalPeriod.hasClosing());
    }


    /**
     *  Gets the date of the closing for this period. 
     *
     *  @return the closing date 
     *  @throws org.osid.IllegalStateException <code> hasMilestones() </code> 
     *          or <code> hasClosing() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getClosing() {
        return (this.fiscalPeriod.getClosing());
    }


    /**
     *  Gets the fiscal period record corresponding to the given <code> 
     *  FiscalPeriod </code> record <code> Type. </code> This method is used 
     *  to retrieve an object implementing the requested record. The <code> 
     *  fiscalPeriodRecordType </code> may be the <code> Type </code> returned 
     *  in <code> getRecordTypes() </code> or any of its parents in a <code> 
     *  Type </code> hierarchy where <code> 
     *  hasRecordType(fiscalPeriodRecordType) </code> is <code> true </code> . 
     *
     *  @param  fiscalPeriodRecordType the type of fiscal period record to 
     *          retrieve 
     *  @return the fiscal period record 
     *  @throws org.osid.NullArgumentException <code> fiscalPeriodRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(fiscalPeriodRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.records.FiscalPeriodRecord getFiscalPeriodRecord(org.osid.type.Type fiscalPeriodRecordType)
        throws org.osid.OperationFailedException {

        return (this.fiscalPeriod.getFiscalPeriodRecord(fiscalPeriodRecordType));
    }
}

