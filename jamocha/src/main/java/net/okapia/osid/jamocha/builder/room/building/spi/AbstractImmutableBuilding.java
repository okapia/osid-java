//
// AbstractImmutableBuilding.java
//
//     Wraps a mutable Building to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.room.building.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Building</code> to hide modifiers. This
 *  wrapper provides an immutized Building from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying building whose state changes are visible.
 */

public abstract class AbstractImmutableBuilding
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableTemporalOsidObject
    implements org.osid.room.Building {

    private final org.osid.room.Building building;


    /**
     *  Constructs a new <code>AbstractImmutableBuilding</code>.
     *
     *  @param building the building to immutablize
     *  @throws org.osid.NullArgumentException <code>building</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableBuilding(org.osid.room.Building building) {
        super(building);
        this.building = building;
        return;
    }


    /**
     *  Gets the address of this building. 
     *
     *  @return the building address <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAddressId() {
        return (this.building.getAddressId());
    }


    /**
     *  Gets the building address. 
     *
     *  @return the building address. 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.contact.Address getAddress()
        throws org.osid.OperationFailedException {

        return (this.building.getAddress());
    }


    /**
     *  Gets the formal name for this building (e.g. Guggenheim Laboratory). 
     *  The display name may or may not differ from this name but may be based 
     *  on the code (e.g. Building 35). 
     *
     *  @return the name 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getOfficialName() {
        return (this.building.getOfficialName());
    }


    /**
     *  Gets the building number or code (e.g. 35). 
     *
     *  @return the building number 
     */

    @OSID @Override
    public String getNumber() {
        return (this.building.getNumber());
    }


    /**
     *  Tests if this building is a subdivision of another building. 
     *
     *  @return <code> true </code> if this building is a subdivisiaion, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean isSubdivision() {
        return (this.building.isSubdivision());
    }


    /**
     *  Gets the enclosing building <code> Id </code> if this building is a 
     *  subdivision. 
     *
     *  @return the enclosing building <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isSubdivision() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getEnclosingBuildingId() {
        return (this.building.getEnclosingBuildingId());
    }


    /**
     *  Gets the enclosing building if this building is a subdivision. 
     *
     *  @return the enlcosing building 
     *  @throws org.osid.IllegalStateException <code> isSubdivision() </code> 
     *          is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.room.Building getEnclosingBuilding()
        throws org.osid.OperationFailedException {

        return (this.building.getEnclosingBuilding());
    }


    /**
     *  Gets the subdivision building <code> Ids </code> if this building is 
     *  subdivided. 
     *
     *  @return the subdivision building <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getSubdivisionIds() {
        return (this.building.getSubdivisionIds());
    }


    /**
     *  Gets the subdivision buildings if this building is subdivided. 
     *
     *  @return the subdivision buildings 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.room.BuildingList getSubdivisions()
        throws org.osid.OperationFailedException {

        return (this.building.getSubdivisions());
    }


    /**
     *  Tests if an area is available. 
     *
     *  @return <code> true </code> if an area is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasArea() {
        return (this.building.hasArea());
    }


    /**
     *  Gets the gross square footage of this building. 
     *
     *  @return the gross area 
     *  @throws org.osid.IllegalStateException <code> hasArea() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public java.math.BigDecimal getGrossArea() {
        return (this.building.getGrossArea());
    }


    /**
     *  Gets the building record corresponding to the given <code> Building 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record. The <code> 
     *  buildingRecordType </code> may be the <code> Type </code> returned in 
     *  <code> getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(buildingRecordType) 
     *  </code> is <code> true </code> . 
     *
     *  @param  buildingRecordType the type of building record to retrieve 
     *  @return the building record 
     *  @throws org.osid.NullArgumentException <code> buildingRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(buildingRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.room.records.BuildingRecord getBuildingRecord(org.osid.type.Type buildingRecordType)
        throws org.osid.OperationFailedException {

        return (this.building.getBuildingRecord(buildingRecordType));
    }
}

