//
// AbstractFederatingRecurringEventLookupSession.java
//
//     An abstract federating adapter for a RecurringEventLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  RecurringEventLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingRecurringEventLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.calendaring.RecurringEventLookupSession>
    implements org.osid.calendaring.RecurringEventLookupSession {

    private boolean parallel = false;
    private org.osid.calendaring.Calendar calendar = new net.okapia.osid.jamocha.nil.calendaring.calendar.UnknownCalendar();


    /**
     *  Constructs a new <code>AbstractFederatingRecurringEventLookupSession</code>.
     */

    protected AbstractFederatingRecurringEventLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.calendaring.RecurringEventLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Calendar/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Calendar Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCalendarId() {
        return (this.calendar.getId());
    }


    /**
     *  Gets the <code>Calendar</code> associated with this 
     *  session.
     *
     *  @return the <code>Calendar</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.calendar);
    }


    /**
     *  Sets the <code>Calendar</code>.
     *
     *  @param  calendar the calendar for this session
     *  @throws org.osid.NullArgumentException <code>calendar</code>
     *          is <code>null</code>
     */

    protected void setCalendar(org.osid.calendaring.Calendar calendar) {
        nullarg(calendar, "calendar");
        this.calendar = calendar;
        return;
    }


    /**
     *  Tests if this user can perform <code>RecurringEvent</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupRecurringEvents() {
        for (org.osid.calendaring.RecurringEventLookupSession session : getSessions()) {
            if (session.canLookupRecurringEvents()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>RecurringEvent</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeRecurringEventView() {
        for (org.osid.calendaring.RecurringEventLookupSession session : getSessions()) {
            session.useComparativeRecurringEventView();
        }

        return;
    }


    /**
     *  A complete view of the <code>RecurringEvent</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryRecurringEventView() {
        for (org.osid.calendaring.RecurringEventLookupSession session : getSessions()) {
            session.usePlenaryRecurringEventView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include recurring events in calendars which are children
     *  of this calendar in the calendar hierarchy.
     */

    @OSID @Override
    public void useFederatedCalendarView() {
        for (org.osid.calendaring.RecurringEventLookupSession session : getSessions()) {
            session.useFederatedCalendarView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this calendar only.
     */

    @OSID @Override
    public void useIsolatedCalendarView() {
        for (org.osid.calendaring.RecurringEventLookupSession session : getSessions()) {
            session.useIsolatedCalendarView();
        }

        return;
    }


    /**
     *  Only active recurring events are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveRecurringEventView() {
        for (org.osid.calendaring.RecurringEventLookupSession session : getSessions()) {
            session.useActiveRecurringEventView();
        }

        return;
    }


    /**
     *  Active and inactive recurring events are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusRecurringEventView() {
        for (org.osid.calendaring.RecurringEventLookupSession session : getSessions()) {
            session.useAnyStatusRecurringEventView();
        }

        return;
    }
    

    /**
     *  The returns from the lookup methods omit sequestered
     *  recurring events.
     */

    @OSID @Override
    public void useSequesteredRecurringEventView() {
        for (org.osid.calendaring.RecurringEventLookupSession session : getSessions()) {
            session.useSequesteredRecurringEventView();
        }

        return;
    }


    /**
     *  All recurring events are returned including sequestered recurring events.
     */

    @OSID @Override
    public void useUnsequesteredRecurringEventView() {
        for (org.osid.calendaring.RecurringEventLookupSession session : getSessions()) {
            session.useUnsequesteredRecurringEventView();
        }

        return;
    }

     
    /**
     *  Gets the <code>RecurringEvent</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>RecurringEvent</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>RecurringEvent</code> and
     *  retained for compatibility.
     *
     *  @param  recurringEventId <code>Id</code> of the
     *          <code>RecurringEvent</code>
     *  @return the recurring event
     *  @throws org.osid.NotFoundException <code>recurringEventId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>recurringEventId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEvent getRecurringEvent(org.osid.id.Id recurringEventId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.calendaring.RecurringEventLookupSession session : getSessions()) {
            try {
                return (session.getRecurringEvent(recurringEventId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(recurringEventId + " not found");
    }


    /**
     *  Gets a <code>RecurringEventList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  recurringEvents specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>RecurringEvents</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, recurring events are returned that are currently
     *  active. In any status mode, active and inactive recurring events
     *  are returned.
     *
     *  @param  recurringEventIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>RecurringEvent</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>recurringEventIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventList getRecurringEventsByIds(org.osid.id.IdList recurringEventIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.calendaring.recurringevent.MutableRecurringEventList ret = new net.okapia.osid.jamocha.calendaring.recurringevent.MutableRecurringEventList();

        try (org.osid.id.IdList ids = recurringEventIds) {
            while (ids.hasNext()) {
                ret.addRecurringEvent(getRecurringEvent(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>RecurringEventList</code> corresponding to the given
     *  recurring event genus <code>Type</code> which does not include
     *  recurring events of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  recurring events or an error results. Otherwise, the returned list
     *  may contain only those recurring events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, recurring events are returned that are currently
     *  active. In any status mode, active and inactive recurring events
     *  are returned.
     *
     *  @param  recurringEventGenusType a recurringEvent genus type 
     *  @return the returned <code>RecurringEvent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>recurringEventGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventList getRecurringEventsByGenusType(org.osid.type.Type recurringEventGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.recurringevent.FederatingRecurringEventList ret = getRecurringEventList();

        for (org.osid.calendaring.RecurringEventLookupSession session : getSessions()) {
            ret.addRecurringEventList(session.getRecurringEventsByGenusType(recurringEventGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>RecurringEventList</code> corresponding to the given
     *  recurring event genus <code>Type</code> and include any additional
     *  recurring events with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  recurring events or an error results. Otherwise, the returned list
     *  may contain only those recurring events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, recurring events are returned that are currently
     *  active. In any status mode, active and inactive recurring events
     *  are returned.
     *
     *  @param  recurringEventGenusType a recurringEvent genus type 
     *  @return the returned <code>RecurringEvent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>recurringEventGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventList getRecurringEventsByParentGenusType(org.osid.type.Type recurringEventGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.recurringevent.FederatingRecurringEventList ret = getRecurringEventList();

        for (org.osid.calendaring.RecurringEventLookupSession session : getSessions()) {
            ret.addRecurringEventList(session.getRecurringEventsByParentGenusType(recurringEventGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>RecurringEventList</code> containing the given
     *  recurring event record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  recurring events or an error results. Otherwise, the returned list
     *  may contain only those recurring events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, recurring events are returned that are currently
     *  active. In any status mode, active and inactive recurring events
     *  are returned.
     *
     *  @param  recurringEventRecordType a recurringEvent record type 
     *  @return the returned <code>RecurringEvent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>recurringEventRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventList getRecurringEventsByRecordType(org.osid.type.Type recurringEventRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.recurringevent.FederatingRecurringEventList ret = getRecurringEventList();

        for (org.osid.calendaring.RecurringEventLookupSession session : getSessions()) {
            ret.addRecurringEventList(session.getRecurringEventsByRecordType(recurringEventRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets the <code> RecurringEvents </code> containing the given
     *  schedule slot <code> </code> 
     *
     *  In plenary mode, the returned list contains all matching
     *  recurring events or an error results. Otherwise, the returned
     *  list may contain only those recurring events that are
     *  accessible through this session.
     *
     *  In active mode, recurring events are returned that are currently
     *  active. In any status mode, active and inactive recurring events
     *  are returned.
     *
     *  @param  scheduleSlotId a schedule slot <code> Id </code>
     *  @return the returned <code> RecurringEvent </code> list
     *  @throws org.osid.NullArgumentException <code> scheduleSlotId </code>
     *          is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventList getRecurringEventsByScheduleSlot(org.osid.id.Id scheduleSlotId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.recurringevent.FederatingRecurringEventList ret = getRecurringEventList();

        for (org.osid.calendaring.RecurringEventLookupSession session : getSessions()) {
            ret.addRecurringEventList(session.getRecurringEventsByScheduleSlot(scheduleSlotId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>RecurringEvents</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  recurring events or an error results. Otherwise, the returned
     *  list may contain only those recurring events that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In active mode, recurring events are returned that are currently
     *  active. In any status mode, active and inactive recurring events
     *  are returned.
     *
     *  @return a list of <code>RecurringEvents</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventList getRecurringEvents()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.recurringevent.FederatingRecurringEventList ret = getRecurringEventList();

        for (org.osid.calendaring.RecurringEventLookupSession session : getSessions()) {
            ret.addRecurringEventList(session.getRecurringEvents());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.calendaring.recurringevent.FederatingRecurringEventList getRecurringEventList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.calendaring.recurringevent.ParallelRecurringEventList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.calendaring.recurringevent.CompositeRecurringEventList());
        }
    }
}
