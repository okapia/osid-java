//
// AbstractAdapterBallotConstrainerLookupSession.java
//
//    A BallotConstrainer lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.voting.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A BallotConstrainer lookup session adapter.
 */

public abstract class AbstractAdapterBallotConstrainerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.voting.rules.BallotConstrainerLookupSession {

    private final org.osid.voting.rules.BallotConstrainerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterBallotConstrainerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterBallotConstrainerLookupSession(org.osid.voting.rules.BallotConstrainerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Polls/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Polls Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getPollsId() {
        return (this.session.getPollsId());
    }


    /**
     *  Gets the {@code Polls} associated with this session.
     *
     *  @return the {@code Polls} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Polls getPolls()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getPolls());
    }


    /**
     *  Tests if this user can perform {@code BallotConstrainer} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupBallotConstrainers() {
        return (this.session.canLookupBallotConstrainers());
    }


    /**
     *  A complete view of the {@code BallotConstrainer} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeBallotConstrainerView() {
        this.session.useComparativeBallotConstrainerView();
        return;
    }


    /**
     *  A complete view of the {@code BallotConstrainer} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryBallotConstrainerView() {
        this.session.usePlenaryBallotConstrainerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include ballot constrainers in pollses which are children
     *  of this polls in the polls hierarchy.
     */

    @OSID @Override
    public void useFederatedPollsView() {
        this.session.useFederatedPollsView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this polls only.
     */

    @OSID @Override
    public void useIsolatedPollsView() {
        this.session.useIsolatedPollsView();
        return;
    }
    

    /**
     *  Only active ballot constrainers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveBallotConstrainerView() {
        this.session.useActiveBallotConstrainerView();
        return;
    }


    /**
     *  Active and inactive ballot constrainers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusBallotConstrainerView() {
        this.session.useAnyStatusBallotConstrainerView();
        return;
    }
    
     
    /**
     *  Gets the {@code BallotConstrainer} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code BallotConstrainer} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code BallotConstrainer} and
     *  retained for compatibility.
     *
     *  In active mode, ballot constrainers are returned that are currently
     *  active. In any status mode, active and inactive ballot constrainers
     *  are returned.
     *
     *  @param ballotConstrainerId {@code Id} of the {@code BallotConstrainer}
     *  @return the ballot constrainer
     *  @throws org.osid.NotFoundException {@code ballotConstrainerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code ballotConstrainerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainer getBallotConstrainer(org.osid.id.Id ballotConstrainerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBallotConstrainer(ballotConstrainerId));
    }


    /**
     *  Gets a {@code BallotConstrainerList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  ballotConstrainers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code BallotConstrainers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, ballot constrainers are returned that are currently
     *  active. In any status mode, active and inactive ballot constrainers
     *  are returned.
     *
     *  @param  ballotConstrainerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code BallotConstrainer} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code ballotConstrainerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerList getBallotConstrainersByIds(org.osid.id.IdList ballotConstrainerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBallotConstrainersByIds(ballotConstrainerIds));
    }


    /**
     *  Gets a {@code BallotConstrainerList} corresponding to the given
     *  ballot constrainer genus {@code Type} which does not include
     *  ballot constrainers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  ballot constrainers or an error results. Otherwise, the returned list
     *  may contain only those ballot constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, ballot constrainers are returned that are currently
     *  active. In any status mode, active and inactive ballot constrainers
     *  are returned.
     *
     *  @param  ballotConstrainerGenusType a ballotConstrainer genus type 
     *  @return the returned {@code BallotConstrainer} list
     *  @throws org.osid.NullArgumentException
     *          {@code ballotConstrainerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerList getBallotConstrainersByGenusType(org.osid.type.Type ballotConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBallotConstrainersByGenusType(ballotConstrainerGenusType));
    }


    /**
     *  Gets a {@code BallotConstrainerList} corresponding to the given
     *  ballot constrainer genus {@code Type} and include any additional
     *  ballot constrainers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  ballot constrainers or an error results. Otherwise, the returned list
     *  may contain only those ballot constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, ballot constrainers are returned that are currently
     *  active. In any status mode, active and inactive ballot constrainers
     *  are returned.
     *
     *  @param  ballotConstrainerGenusType a ballotConstrainer genus type 
     *  @return the returned {@code BallotConstrainer} list
     *  @throws org.osid.NullArgumentException
     *          {@code ballotConstrainerGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerList getBallotConstrainersByParentGenusType(org.osid.type.Type ballotConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBallotConstrainersByParentGenusType(ballotConstrainerGenusType));
    }


    /**
     *  Gets a {@code BallotConstrainerList} containing the given
     *  ballot constrainer record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  ballot constrainers or an error results. Otherwise, the returned list
     *  may contain only those ballot constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, ballot constrainers are returned that are currently
     *  active. In any status mode, active and inactive ballot constrainers
     *  are returned.
     *
     *  @param  ballotConstrainerRecordType a ballotConstrainer record type 
     *  @return the returned {@code BallotConstrainer} list
     *  @throws org.osid.NullArgumentException
     *          {@code ballotConstrainerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerList getBallotConstrainersByRecordType(org.osid.type.Type ballotConstrainerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBallotConstrainersByRecordType(ballotConstrainerRecordType));
    }


    /**
     *  Gets all {@code BallotConstrainers}. 
     *
     *  In plenary mode, the returned list contains all known
     *  ballot constrainers or an error results. Otherwise, the returned list
     *  may contain only those ballot constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, ballot constrainers are returned that are currently
     *  active. In any status mode, active and inactive ballot constrainers
     *  are returned.
     *
     *  @return a list of {@code BallotConstrainers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerList getBallotConstrainers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBallotConstrainers());
    }
}
