//
// AbstractFederatingProjectLookupSession.java
//
//     An abstract federating adapter for a ProjectLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.room.construction.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  ProjectLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingProjectLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.room.construction.ProjectLookupSession>
    implements org.osid.room.construction.ProjectLookupSession {

    private boolean parallel = false;
    private org.osid.room.Campus campus = new net.okapia.osid.jamocha.nil.room.campus.UnknownCampus();


    /**
     *  Constructs a new <code>AbstractFederatingProjectLookupSession</code>.
     */

    protected AbstractFederatingProjectLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.room.construction.ProjectLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Campus/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Campus Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCampusId() {
        return (this.campus.getId());
    }


    /**
     *  Gets the <code>Campus</code> associated with this 
     *  session.
     *
     *  @return the <code>Campus</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.Campus getCampus()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.campus);
    }


    /**
     *  Sets the <code>Campus</code>.
     *
     *  @param  campus the campus for this session
     *  @throws org.osid.NullArgumentException <code>campus</code>
     *          is <code>null</code>
     */

    protected void setCampus(org.osid.room.Campus campus) {
        nullarg(campus, "campus");
        this.campus = campus;
        return;
    }


    /**
     *  Tests if this user can perform <code>Project</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupProjects() {
        for (org.osid.room.construction.ProjectLookupSession session : getSessions()) {
            if (session.canLookupProjects()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Project</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeProjectView() {
        for (org.osid.room.construction.ProjectLookupSession session : getSessions()) {
            session.useComparativeProjectView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Project</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryProjectView() {
        for (org.osid.room.construction.ProjectLookupSession session : getSessions()) {
            session.usePlenaryProjectView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include projects in campuses which are children
     *  of this campus in the campus hierarchy.
     */

    @OSID @Override
    public void useFederatedCampusView() {
        for (org.osid.room.construction.ProjectLookupSession session : getSessions()) {
            session.useFederatedCampusView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this campus only.
     */

    @OSID @Override
    public void useIsolatedCampusView() {
        for (org.osid.room.construction.ProjectLookupSession session : getSessions()) {
            session.useIsolatedCampusView();
        }

        return;
    }


    /**
     *  Only projects whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveProjectView() {
        for (org.osid.room.construction.ProjectLookupSession session : getSessions()) {
            session.useEffectiveProjectView();
        }

        return;
    }


    /**
     *  All projects of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveProjectView() {
        for (org.osid.room.construction.ProjectLookupSession session : getSessions()) {
            session.useAnyEffectiveProjectView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Project</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Project</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Project</code> and
     *  retained for compatibility.
     *
     *  In effective mode, projects are returned that are currently
     *  effective.  In any effective mode, effective projects and
     *  those currently expired are returned.
     *
     *  @param  projectId <code>Id</code> of the
     *          <code>Project</code>
     *  @return the project
     *  @throws org.osid.NotFoundException <code>projectId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>projectId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.construction.Project getProject(org.osid.id.Id projectId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.room.construction.ProjectLookupSession session : getSessions()) {
            try {
                return (session.getProject(projectId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(projectId + " not found");
    }


    /**
     *  Gets a <code>ProjectList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  projects specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Projects</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, projects are returned that are currently
     *  effective.  In any effective mode, effective projects and
     *  those currently expired are returned.
     *
     *  @param  projectIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Project</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>projectIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectList getProjectsByIds(org.osid.id.IdList projectIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.room.construction.project.MutableProjectList ret = new net.okapia.osid.jamocha.room.construction.project.MutableProjectList();

        try (org.osid.id.IdList ids = projectIds) {
            while (ids.hasNext()) {
                ret.addProject(getProject(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>ProjectList</code> corresponding to the given
     *  project genus <code>Type</code> which does not include
     *  projects of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  projects or an error results. Otherwise, the returned list
     *  may contain only those projects that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, projects are returned that are currently
     *  effective.  In any effective mode, effective projects and
     *  those currently expired are returned.
     *
     *  @param  projectGenusType a project genus type 
     *  @return the returned <code>Project</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>projectGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectList getProjectsByGenusType(org.osid.type.Type projectGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.construction.project.FederatingProjectList ret = getProjectList();

        for (org.osid.room.construction.ProjectLookupSession session : getSessions()) {
            ret.addProjectList(session.getProjectsByGenusType(projectGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ProjectList</code> corresponding to the given
     *  project genus <code>Type</code> and include any additional
     *  projects with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  projects or an error results. Otherwise, the returned list
     *  may contain only those projects that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, projects are returned that are currently
     *  effective.  In any effective mode, effective projects and
     *  those currently expired are returned.
     *
     *  @param  projectGenusType a project genus type 
     *  @return the returned <code>Project</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>projectGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectList getProjectsByParentGenusType(org.osid.type.Type projectGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.construction.project.FederatingProjectList ret = getProjectList();

        for (org.osid.room.construction.ProjectLookupSession session : getSessions()) {
            ret.addProjectList(session.getProjectsByParentGenusType(projectGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ProjectList</code> containing the given
     *  project record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  projects or an error results. Otherwise, the returned list
     *  may contain only those projects that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, projects are returned that are currently
     *  effective.  In any effective mode, effective projects and
     *  those currently expired are returned.
     *
     *  @param  projectRecordType a project record type 
     *  @return the returned <code>Project</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>projectRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectList getProjectsByRecordType(org.osid.type.Type projectRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.construction.project.FederatingProjectList ret = getProjectList();

        for (org.osid.room.construction.ProjectLookupSession session : getSessions()) {
            ret.addProjectList(session.getProjectsByRecordType(projectRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ProjectList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  projects or an error results. Otherwise, the returned list
     *  may contain only those projects that are accessible
     *  through this session.
     *  
     *  In active mode, projects are returned that are currently
     *  active. In any status mode, active and inactive projects
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Project</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.room.construction.ProjectList getProjectsOnDate(org.osid.calendaring.DateTime from, 
                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.construction.project.FederatingProjectList ret = getProjectList();

        for (org.osid.room.construction.ProjectLookupSession session : getSessions()) {
            ret.addProjectList(session.getProjectsOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a list of all projects with a genus type and effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *
     *  In plenary mode, the returned list contains all known projects
     *  or an error results. Otherwise, the returned list may contain
     *  only those projects that are accessible through this session.
     *
     *  In effective mode, projects are returned that are currently
     *  effective.  In any effective mode, effective projects and
     *  those currently expired are returned.
     *
     *  @param  projectGenusType a project genus type
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code>Project</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException
     *  <code>projectGenusType</code>, <code>from </code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.construction.ProjectList getProjectsByGenusTypeOnDate(org.osid.type.Type projectGenusType,
                                                                               org.osid.calendaring.DateTime from,
                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.construction.project.FederatingProjectList ret = getProjectList();

        for (org.osid.room.construction.ProjectLookupSession session : getSessions()) {
            ret.addProjectList(session.getProjectsByGenusTypeOnDate(projectGenusType, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ProjectList</code> containing the given building.
     *
     *  In plenary mode, the returned list contains all known projects
     *  or an error results. Otherwise, the returned list may contain
     *  only those projects that are accessible through this session.
     *
     *  In effective mode, projects are returned that are currently
     *  effective.  In any effective mode, effective projects and
     *  those currently expired are returned.
     *
     *  @param  buildingId a building <code>Id</code>
     *  @return the returned <code>Project</code> list
     *  @throws org.osid.NullArgumentException <code>buildingId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.construction.ProjectList getProjectsForBuilding(org.osid.id.Id buildingId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.construction.project.FederatingProjectList ret = getProjectList();

        for (org.osid.room.construction.ProjectLookupSession session : getSessions()) {
            ret.addProjectList(session.getProjectsForBuilding(buildingId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ProjectList</code> containing the given
     *  building and genus type.
     *
     *  In plenary mode, the returned list contains all known projects
     *  or an error results. Otherwise, the returned list may contain
     *  only those projects that are accessible through this session.
     *
     *  In effective mode, projects are returned that are currently
     *  effective.  In any effective mode, effective projects and
     *  those currently expired are returned.
     *
     *  @param  buildingId a building <code>Id</code>
     *  @param  projectGenusType a project genus type
     *  @return the returned <code>Project</code> list
     *  @throws org.osid.NullArgumentException <code>buildingId</code>
     *          or <code>projectGenusType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */
    
    @OSID @Override
    public org.osid.room.construction.ProjectList getProjectsByGenusTypeForBuilding(org.osid.id.Id buildingId,
                                                                                    org.osid.type.Type projectGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.construction.project.FederatingProjectList ret = getProjectList();

        for (org.osid.room.construction.ProjectLookupSession session : getSessions()) {
            ret.addProjectList(session.getProjectsByGenusTypeForBuilding(buildingId, projectGenusType));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of all projects for a building effective during
     *  the entire given date range inclusive but not confined to the
     *  date range.
     *
     *  In plenary mode, the returned list contains all known projects
     *  or an error results. Otherwise, the returned list may contain
     *  only those projects that are accessible through this session.
     *
     *  In effective mode, projects are returned that are currently
     *  effective.  In any effective mode, effective projects and
     *  those currently expired are returned.
     *
     *  @param  buildingId a building <code>Id</code>
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code>Project</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException
     *          <code>buildingId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.construction.ProjectList getProjectsForBuildingOnDate(org.osid.id.Id buildingId,
                                                                               org.osid.calendaring.DateTime from,
                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.construction.project.FederatingProjectList ret = getProjectList();

        for (org.osid.room.construction.ProjectLookupSession session : getSessions()) {
            ret.addProjectList(session.getProjectsForBuildingOnDate(buildingId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of all projects for a building with a genus type
     *  and effective during the entire given date range inclusive but
     *  not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known projects
     *  or an error results. Otherwise, the returned list may contain
     *  only those projects that are accessible through this session.
     *
     *  In effective mode, projects are returned that are currently
     *  effective.  In any effective mode, effective projects and
     *  those currently expired are returned.
     *
     *  @param  buildingId a building <code>Id</code>
     *  @param  projectGenusType a project genus type
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code>Project</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException
     *          <code>buildingId</code>,
     *          <code>projectGenusType</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.construction.ProjectList getProjectsByGenusTypeForBuildingOnDate(org.osid.id.Id buildingId,
                                                                                          org.osid.type.Type projectGenusType,
                                                                                          org.osid.calendaring.DateTime from,
                                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.construction.project.FederatingProjectList ret = getProjectList();

        for (org.osid.room.construction.ProjectLookupSession session : getSessions()) {
            ret.addProjectList(session.getProjectsByGenusTypeForBuildingOnDate(buildingId, projectGenusType, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Projects</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  projects or an error results. Otherwise, the returned list
     *  may contain only those projects that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, projects are returned that are currently
     *  effective.  In any effective mode, effective projects and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Projects</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectList getProjects()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.construction.project.FederatingProjectList ret = getProjectList();

        for (org.osid.room.construction.ProjectLookupSession session : getSessions()) {
            ret.addProjectList(session.getProjects());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.room.construction.project.FederatingProjectList getProjectList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.room.construction.project.ParallelProjectList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.room.construction.project.CompositeProjectList());
        }
    }
}
