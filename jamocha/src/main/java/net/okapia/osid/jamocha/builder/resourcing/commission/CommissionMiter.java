//
// CommissionMiter.java
//
//     Defines a Commission miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.resourcing.commission;


/**
 *  Defines a <code>Commission</code> miter for use with the builders.
 */

public interface CommissionMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidRelationshipMiter,
            org.osid.resourcing.Commission {


    /**
     *  Sets the resource.
     *
     *  @param resource a resource
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    public void setResource(org.osid.resource.Resource resource);


    /**
     *  Sets the work.
     *
     *  @param work a work
     *  @throws org.osid.NullArgumentException <code>work</code> is
     *          <code>null</code>
     */

    public void setWork(org.osid.resourcing.Work work);


    /**
     *  Sets the competency.
     *
     *  @param competency a competency
     *  @throws org.osid.NullArgumentException <code>competency</code>
     *          is <code>null</code>
     */

    public void setCompetency(org.osid.resourcing.Competency competency);


    /**
     *  Sets the percentage commitment.
     *
     *  @param percentage a percentage (0-100)
     *  @throws org.osid.InvalidArgumentException
     *          <code>percentage</code> is out of range
     */

    public void setPercentage(long percentage);


    /**
     *  Adds a Commission record.
     *
     *  @param record a commission record
     *  @param recordType the type of commission record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addCommissionRecord(org.osid.resourcing.records.CommissionRecord record, org.osid.type.Type recordType);
}       


