//
// AbstractIndexedMapTrustLookupSession.java
//
//    A simple framework for providing a Trust lookup service
//    backed by a fixed collection of trusts with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.authentication.process.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Trust lookup service backed by a
 *  fixed collection of trusts. The trusts are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some trusts may be compatible
 *  with more types than are indicated through these trust
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Trusts</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapTrustLookupSession
    extends AbstractMapTrustLookupSession
    implements org.osid.authentication.process.TrustLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.authentication.process.Trust> trustsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.authentication.process.Trust>());
    private final MultiMap<org.osid.type.Type, org.osid.authentication.process.Trust> trustsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.authentication.process.Trust>());


    /**
     *  Makes a <code>Trust</code> available in this session.
     *
     *  @param  trust a trust
     *  @throws org.osid.NullArgumentException <code>trust<code> is
     *          <code>null</code>
     */

    @Override
    protected void putTrust(org.osid.authentication.process.Trust trust) {
        super.putTrust(trust);

        this.trustsByGenus.put(trust.getGenusType(), trust);
        
        try (org.osid.type.TypeList types = trust.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.trustsByRecord.put(types.getNextType(), trust);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a trust from this session.
     *
     *  @param trustId the <code>Id</code> of the trust
     *  @throws org.osid.NullArgumentException <code>trustId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeTrust(org.osid.id.Id trustId) {
        org.osid.authentication.process.Trust trust;
        try {
            trust = getTrust(trustId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.trustsByGenus.remove(trust.getGenusType());

        try (org.osid.type.TypeList types = trust.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.trustsByRecord.remove(types.getNextType(), trust);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeTrust(trustId);
        return;
    }


    /**
     *  Gets a <code>TrustList</code> corresponding to the given
     *  trust genus <code>Type</code> which does not include
     *  trusts of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known trusts or an error results. Otherwise,
     *  the returned list may contain only those trusts that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  trustGenusType a trust genus type 
     *  @return the returned <code>Trust</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>trustGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.process.TrustList getTrustsByGenusType(org.osid.type.Type trustGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.authentication.process.trust.ArrayTrustList(this.trustsByGenus.get(trustGenusType)));
    }


    /**
     *  Gets a <code>TrustList</code> containing the given
     *  trust record <code>Type</code>. In plenary mode, the
     *  returned list contains all known trusts or an error
     *  results. Otherwise, the returned list may contain only those
     *  trusts that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  trustRecordType a trust record type 
     *  @return the returned <code>trust</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>trustRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.process.TrustList getTrustsByRecordType(org.osid.type.Type trustRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.authentication.process.trust.ArrayTrustList(this.trustsByRecord.get(trustRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.trustsByGenus.clear();
        this.trustsByRecord.clear();

        super.close();

        return;
    }
}
