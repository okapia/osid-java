//
// MutableMapBlogLookupSession
//
//    Implements a Blog lookup service backed by a collection of
//    blogs that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.blogging;


/**
 *  Implements a Blog lookup service backed by a collection of
 *  blogs. The blogs are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of blogs can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapBlogLookupSession
    extends net.okapia.osid.jamocha.core.blogging.spi.AbstractMapBlogLookupSession
    implements org.osid.blogging.BlogLookupSession {


    /**
     *  Constructs a new {@code MutableMapBlogLookupSession}
     *  with no blogs.
     */

    public MutableMapBlogLookupSession() {
        return;
    }


    /**
     *  Constructs a new {@code MutableMapBlogLookupSession} with a
     *  single blog.
     *  
     *  @param blog a blog
     *  @throws org.osid.NullArgumentException {@code blog}
     *          is {@code null}
     */

    public MutableMapBlogLookupSession(org.osid.blogging.Blog blog) {
        putBlog(blog);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapBlogLookupSession}
     *  using an array of blogs.
     *
     *  @param blogs an array of blogs
     *  @throws org.osid.NullArgumentException {@code blogs}
     *          is {@code null}
     */

    public MutableMapBlogLookupSession(org.osid.blogging.Blog[] blogs) {
        putBlogs(blogs);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapBlogLookupSession}
     *  using a collection of blogs.
     *
     *  @param blogs a collection of blogs
     *  @throws org.osid.NullArgumentException {@code blogs}
     *          is {@code null}
     */

    public MutableMapBlogLookupSession(java.util.Collection<? extends org.osid.blogging.Blog> blogs) {
        putBlogs(blogs);
        return;
    }

    
    /**
     *  Makes a {@code Blog} available in this session.
     *
     *  @param blog a blog
     *  @throws org.osid.NullArgumentException {@code blog{@code  is
     *          {@code null}
     */

    @Override
    public void putBlog(org.osid.blogging.Blog blog) {
        super.putBlog(blog);
        return;
    }


    /**
     *  Makes an array of blogs available in this session.
     *
     *  @param blogs an array of blogs
     *  @throws org.osid.NullArgumentException {@code blogs{@code 
     *          is {@code null}
     */

    @Override
    public void putBlogs(org.osid.blogging.Blog[] blogs) {
        super.putBlogs(blogs);
        return;
    }


    /**
     *  Makes collection of blogs available in this session.
     *
     *  @param blogs a collection of blogs
     *  @throws org.osid.NullArgumentException {@code blogs{@code  is
     *          {@code null}
     */

    @Override
    public void putBlogs(java.util.Collection<? extends org.osid.blogging.Blog> blogs) {
        super.putBlogs(blogs);
        return;
    }


    /**
     *  Removes a Blog from this session.
     *
     *  @param blogId the {@code Id} of the blog
     *  @throws org.osid.NullArgumentException {@code blogId{@code 
     *          is {@code null}
     */

    @Override
    public void removeBlog(org.osid.id.Id blogId) {
        super.removeBlog(blogId);
        return;
    }    
}
