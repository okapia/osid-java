//
// SpeedZoneEnablerElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.path.rules.speedzoneenabler.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class SpeedZoneEnablerElements
    extends net.okapia.osid.jamocha.spi.OsidEnablerElements {


    /**
     *  Gets the SpeedZoneEnablerElement Id.
     *
     *  @return the speed zone enabler element Id
     */

    public static org.osid.id.Id getSpeedZoneEnablerEntityId() {
        return (makeEntityId("osid.mapping.path.rules.SpeedZoneEnabler"));
    }


    /**
     *  Gets the RuledSpeedZoneId element Id.
     *
     *  @return the RuledSpeedZoneId element Id
     */

    public static org.osid.id.Id getRuledSpeedZoneId() {
        return (makeQueryElementId("osid.mapping.path.rules.speedzoneenabler.RuledSpeedZoneId"));
    }


    /**
     *  Gets the RuledSpeedZone element Id.
     *
     *  @return the RuledSpeedZone element Id
     */

    public static org.osid.id.Id getRuledSpeedZone() {
        return (makeQueryElementId("osid.mapping.path.rules.speedzoneenabler.RuledSpeedZone"));
    }


    /**
     *  Gets the MapId element Id.
     *
     *  @return the MapId element Id
     */

    public static org.osid.id.Id getMapId() {
        return (makeQueryElementId("osid.mapping.path.rules.speedzoneenabler.MapId"));
    }


    /**
     *  Gets the Map element Id.
     *
     *  @return the Map element Id
     */

    public static org.osid.id.Id getMap() {
        return (makeQueryElementId("osid.mapping.path.rules.speedzoneenabler.Map"));
    }
}
