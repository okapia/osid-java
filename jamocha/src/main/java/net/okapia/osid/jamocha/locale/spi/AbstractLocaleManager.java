//
// AbstractLocaleManager
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.locale.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Collections;
import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.MultiHashMap;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeHashMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.TypeRefSet;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractLocaleManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.locale.LocaleManager,
               org.osid.locale.LocaleProxyManager {

    private final MultiMap<LanguageType, LanguageType> languageTranslations = Collections.synchronizedMultiMap(new MultiHashMap<LanguageType, LanguageType>());

    private final MultiMap<DateTimeType, DateTimeFormatType> calendarFormats = Collections.synchronizedMultiMap(new MultiHashMap<DateTimeType, DateTimeFormatType>());

    private final MultiMap<org.osid.type.Type, org.osid.type.Type> currencyFormats = Collections.synchronizedMultiMap(new TypeMultiHashMap<org.osid.type.Type>());
    private final MultiMap<org.osid.type.Type, org.osid.type.Type> coordinateFormats = Collections.synchronizedMultiMap(new TypeMultiHashMap<org.osid.type.Type>());

    private final Types numericFormats = new TypeRefSet();

    private final MultiMap<org.osid.type.Type, org.osid.type.Type> unitConversions = Collections.synchronizedMultiMap(new TypeMultiHashMap<org.osid.type.Type>());
    private final MultiMap<org.osid.type.Type, org.osid.type.Type> calendarConversions = Collections.synchronizedMultiMap(new TypeMultiHashMap<org.osid.type.Type>());
    private final MultiMap<org.osid.type.Type, org.osid.type.Type> timeConversions = Collections.synchronizedMultiMap(new TypeMultiHashMap<org.osid.type.Type>());
    private final MultiMap<org.osid.type.Type, org.osid.type.Type> currencyConversions = Collections.synchronizedMultiMap(new TypeMultiHashMap<org.osid.type.Type>());
    private final MultiMap<org.osid.type.Type, org.osid.type.Type> coordinateConversions = Collections.synchronizedMultiMap(new TypeMultiHashMap<org.osid.type.Type>());
    private final MultiMap<org.osid.type.Type, org.osid.type.Type> spatialUnitConversions = Collections.synchronizedMultiMap(new TypeMultiHashMap<org.osid.type.Type>());
    private final MultiMap<org.osid.type.Type, org.osid.type.Type> formatConversions = Collections.synchronizedMultiMap(new TypeMultiHashMap<org.osid.type.Type>());

    private final MultiMap<org.osid.type.Type, org.osid.type.Type> datetimes = Collections.synchronizedMultiMap(new TypeMultiHashMap<org.osid.type.Type>());
    private final MultiMap<org.osid.type.Type, org.osid.type.Type> timedates = Collections.synchronizedMultiMap(new TypeMultiHashMap<org.osid.type.Type>());


    /**
     *  Constructs a new <code>AbstractLocaleManager</code>.
     *
     *  @param provider the provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractLocaleManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if visible federation is supported. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if translation is supported. 
     *
     *  @return <code> true </code> if translation is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTranslation() {
        return (false);
    }


    /**
     *  Tests if translation administration is supported. 
     *
     *  @return <code> true </code> if translation administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTranslationAdmin() {
        return (false);
    }


    /**
     *  Tests if numeric formatting is supported. 
     *
     *  @return <code> true </code> if numeric formatting is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsNumericFormatting() {
        return (false);
    }


    /**
     *  Tests if calendar formatting is supported. 
     *
     *  @return <code> true </code> if calendar formatting is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendarFormatting() {
        return (false);
    }


    /**
     *  Tests if currency formatting is supported. 
     *
     *  @return <code> true </code> if currency formatting is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCurrencyFormatting() {
        return (false);
    }


    /**
     *  Tests if coordinate formatting is supported. 
     *
     *  @return <code> true </code> if coordinate formatting is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCoordinateFormatting() {
        return (false);
    }


    /**
     *  Tests if unit conversion is supported. 
     *
     *  @return <code> true </code> if unit conversion is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsUnitConversion() {
        return (false);
    }


    /**
     *  Tests if currency conversion is supported. 
     *
     *  @return <code> true </code> if currency conversion is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCurrencyConversion() {
        return (false);
    }


    /**
     *  Tests if calendar conversion is supported. 
     *
     *  @return <code> true </code> if calendar conversion is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendarConversion() {
        return (false);
    }


    /**
     *  Tests if coordnate conversion is supported. 
     *
     *  @return <code> true </code> if coordinate conversion is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCoordinateConversion() {
        return (false);
    }


    /**
     *  Tests if spatial unit conversion is supported. 
     *
     *  @return <code> true </code> if spatial unit conversion is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSpatialUnitConversion() {
        return (false);
    }


    /**
     *  Tests if text format conversion is supported. 
     *
     *  @return <code> true </code> if text formatconversion is
     *          supported, <code> false </code> otherwise
     */

    @OSID @Override
    public boolean supportsFormatConversion() {
        return (false);
    }


    /**
     *  Tests if a calendar informational service is supported. 
     *
     *  @return <code> true </code> if calendar info is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendarInfo() {
        return (false);
    }


    /**
     *  Tests if a given language translation is supported. 
     *
     *  @param  sourceLanguageType the type of the source language 
     *  @param  sourceScriptType the type of the source script 
     *  @param  targetLanguageType the type of the target language 
     *  @param  targetScriptType the type of the target script 
     *  @return <code> true </code> if the given source and target translation 
     *          is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> sourceLanguageType, 
     *          sourceScriptType, targetLanguageType </code> or <code> 
     *          targetScriptType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsLanguageTypesForTranslation(org.osid.type.Type sourceLanguageType, org.osid.type.Type sourceScriptType, org.osid.type.Type targetLanguageType, org.osid.type.Type targetScriptType) {

        return (this.languageTranslations.containsKeyValue(new LanguageType(sourceLanguageType, sourceScriptType),
                                                           new LanguageType(targetLanguageType, targetScriptType)));
    }


    /**
     *  Gets the list of target language types for a given source
     *  language type.
     *
     *  @param  sourceLanguageType the type of the source language 
     *  @param  sourceScriptType the type of the source script 
     *  @return the list of supported types for the given source language type 
     *  @throws org.osid.NullArgumentException <code> sourceLanguageType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public org.osid.type.TypeList getLanguageTypesForSource(org.osid.type.Type sourceLanguageType, org.osid.type.Type sourceScriptType) {
        
        net.okapia.osid.jamocha.type.type.MutableTypeList ret = new net.okapia.osid.jamocha.type.type.MutableTypeList();
        
        for (LanguageType lang : this.languageTranslations.get(new LanguageType(sourceLanguageType, sourceScriptType))) {
            ret.addType(lang.getLanguageType());
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets all the source language types supported. 
     *
     *  @return the list of supported language types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSourceLanguageTypes() {

        net.okapia.osid.jamocha.type.type.MutableTypeList ret = new net.okapia.osid.jamocha.type.type.MutableTypeList();
        
        for (LanguageType lang : this.languageTranslations.keySet()) {
            ret.addType(lang.getLanguageType());
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets the list of script types available for a given language type. 
     *
     *  @param  languageType the type of the language 
     *  @return the list of supported script types for the given language type 
     *  @throws org.osid.NullArgumentException <code> languageType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public org.osid.type.TypeList getScriptTypesForLanguageType(org.osid.type.Type languageType) {
        java.util.Collection<LanguageType> sources = this.languageTranslations.keySet();

        net.okapia.osid.jamocha.type.type.MutableTypeList ret = new net.okapia.osid.jamocha.type.type.MutableTypeList();

        for (LanguageType source : sources) {
            if (source.getLanguageType().equals(languageType)) {
                ret.addType(source.getScriptType());
            }

            for (LanguageType target : this.languageTranslations.get(source)) {
                if (target.getLanguageType().equals(languageType)) {
                    ret.addType(target.getScriptType());
                }
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Adds support for a language translation.
     *
     *  @param sourceLanguageType source language type
     *  @param sourceScriptType source script type
     *  @param targetLanguageType destination language type
     *  @param targetScriptType destination script type
     *  @throws org.osid.NullArgumentException
     *          <code>sourceLanguageType</code>,
     *          <code>sourceScriptType</code>,
     *          <code>targetLanguageType</code>,
     *          <code>targetScriptType</code> is <code>null</code>
     */

    protected void addLanguageTranslation(org.osid.type.Type sourceLanguageType,
                                          org.osid.type.Type sourceScriptType,
                                          org.osid.type.Type targetLanguageType,
                                          org.osid.type.Type targetScriptType) {

        this.languageTranslations.put(new LanguageType(sourceLanguageType, sourceScriptType),
                                      new LanguageType(targetLanguageType, targetScriptType));

        return;            
    }


    /**
     *  Removes support for a language translation.
     *
     *  @param sourceLanguageType source language type
     *  @param sourceScriptType source script type
     *  @param targetLanguageType destination language type
     *  @param targetScriptType destination script type
     *  @throws org.osid.NullArgumentException
     *          <code>sourceLanguageType</code>,
     *          <code>sourceScriptType</code>,
     *          <code>targetLanguageType</code>,
     *          <code>targetScriptType</code> is <code>null</code>
     */

    protected void removeLanguageTranslation(org.osid.type.Type sourceLanguageType,
                                             org.osid.type.Type sourceScriptType,
                                             org.osid.type.Type targetLanguageType,
                                             org.osid.type.Type targetScriptType) {
        
        this.languageTranslations.remove(new LanguageType(sourceLanguageType, sourceScriptType),
                                         new LanguageType(targetLanguageType, targetScriptType));           

        return;            
    }


    /**
     *  Tests if a given numeric format type is supported. 
     *
     *  @param  numericFormatType the type of the numeric format 
     *  @return <code> true </code> if the given numeric format type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> numericFormatType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsNumericFormatTypes(org.osid.type.Type numericFormatType) {
        return (this.numericFormats.contains(numericFormatType));        
    }


    /**
     *  Gets all the numeric format types supported. 
     *
     *  @return the list of supported numeric format types 
     */

    @OSID @Override
    public org.osid.type.TypeList getNumericFormatTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.numericFormats.toCollection()));
    }


    /**
     *  Adds support for a numeric format.
     *
     *  @param numericFormatType a numeric format type
     *  @throws org.osid.NullArgumentException
     *          <code>numericFormatType</code> is <code>null</code>
     */

    protected void addNumericFormatType(org.osid.type.Type numericFormatType) {
        this.numericFormats.add(numericFormatType);
        return;            
    }


    /**
     *  Removes support for a numeric format.
     *
     *  @param numericFormatType a numeric format type
     *  @throws org.osid.NullArgumentException
     *          <code>numericFormatType</code> is <code>null</code>
     */

    protected void removeNumericFormatType(org.osid.type.Type numericFormatType) {        
        this.numericFormats.remove(numericFormatType);
        return;            
    }


    /**
     *  Tests if a given calendaring formatting is supported. 
     *
     *  @param  calendarType the type of the calendar 
     *  @param  timeType the type of the time system 
     *  @param  dateFormatType the type of the output calendar format 
     *  @param  timeFormatType the type of the output time format 
     *  @return <code> true </code> if formatting with the given types is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> calendarType, 
     *          dateFormatType, timeType, </code> or <code> timeFormatType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCalendarTypesForFormatting(org.osid.type.Type calendarType, org.osid.type.Type timeType, org.osid.type.Type dateFormatType, org.osid.type.Type timeFormatType) {

        return (this.calendarFormats.containsKeyValue(new DateTimeType(calendarType, timeType),
                                                      new DateTimeFormatType(calendarType, timeType)));
    }


    /**
     *  Gets all the calendar types for which formatting is available.
     *
     *  @return the list of calendar types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCalendarTypesForFormatting() {
        net.okapia.osid.jamocha.type.type.MutableTypeList ret = new net.okapia.osid.jamocha.type.type.MutableTypeList();

        for (DateTimeType type : this.calendarFormats.keySet()) {
            ret.addType(type.getCalendarType());
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets the list of date format types for a given calendar type. 
     *
     *  @param  calendarType the type of the calendar 
     *  @return the list of supported date format types 
     *  @throws org.osid.NullArgumentException <code> calendarType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public org.osid.type.TypeList getDateFormatTypesForCalendarType(org.osid.type.Type calendarType) {
        net.okapia.osid.jamocha.type.type.MutableTypeList ret = new net.okapia.osid.jamocha.type.type.MutableTypeList();

        for (DateTimeType type : this.calendarFormats.keySet()) {
            if (type.getCalendarType().equals(calendarType)) {
                for (DateTimeFormatType format : this.calendarFormats.get(type)) {
                    ret.addType(format.getCalendarFormatType());
                }
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets all the time types for which formatting is available.
     *
     *  @return the list of time types 
     */

    @OSID @Override
    public org.osid.type.TypeList getTimeTypesForFormatting() {
        net.okapia.osid.jamocha.type.type.MutableTypeList ret = new net.okapia.osid.jamocha.type.type.MutableTypeList();

        for (DateTimeType type : this.calendarFormats.keySet()) {
            ret.addType(type.getTimeType());
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets the list of time format types for a given time type. 
     *
     *  @param  timeType the type of the time 
     *  @return the list of supported time format types 
     *  @throws org.osid.NullArgumentException <code> timeType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public org.osid.type.TypeList getTimeFormatTypesForTimeType(org.osid.type.Type timeType) {
        net.okapia.osid.jamocha.type.type.MutableTypeList ret = new net.okapia.osid.jamocha.type.type.MutableTypeList();

        for (DateTimeType type : this.calendarFormats.keySet()) {
            if (type.getTimeType().equals(timeType)) {
                for (DateTimeFormatType format : this.calendarFormats.get(type)) {
                    ret.addType(format.getTimeFormatType());
                }
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Adds support for a calendar format.
     *
     *  @param calendarType a calendar type
     *  @param timeType a time type
     *  @param dateFormatType a date format type
     *  @param timeFormatType a time format type
     *  @throws org.osid.NullArgumentException
     *          <code>calendarType</code>, <code>timeType</code>,
     *          <code>dateFormatType</code>,
     *          <code>timeFormatType</code> is <code>null</code>
     */

    protected void addCalendarFormat(org.osid.type.Type calendarType,
                                     org.osid.type.Type timeType,
                                     org.osid.type.Type dateFormatType,
                                     org.osid.type.Type timeFormatType) {

        this.calendarFormats.put(new DateTimeType(calendarType, timeType),
                                 new DateTimeFormatType(dateFormatType, timeFormatType));

        return;            
    }


    /**
     *  Removes support for a calendar format.
     *
     *  @param calendarType a calendar type
     *  @param timeType a time type
     *  @param dateFormatType a date format type
     *  @param timeFormatType a time format type
     *  @throws org.osid.NullArgumentException
     *          <code>calendarType</code>, <code>timeType</code>,
     *          <code>dateFormatType</code>,
     *          <code>timeFormatType</code> is <code>null</code>
     */

    protected void removeCalendarFormat(org.osid.type.Type calendarType,
                                        org.osid.type.Type timeType,
                                        org.osid.type.Type dateFormatType,
                                        org.osid.type.Type timeFormatType) {
        
        this.calendarFormats.remove(new DateTimeType(calendarType, timeType),
                                    new DateTimeFormatType(dateFormatType, timeFormatType));

        return;            
    }


    /**
     *  Tests if a given currency formatting is supported. 
     *
     *  @param  currencyType the type of the cureny 
     *  @param  numericFormatType the type of the output currency format 
     *  @return <code> true </code> if formatting with the given types is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> currencyType </code> or 
     *          <code> numericFormatType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCurrencyTypesForFormatting(org.osid.type.Type currencyType, org.osid.type.Type numericFormatType) {
        return (this.currencyFormats.containsKeyValue(currencyType, numericFormatType));
    }


    /**
     *  Gets all the currency format types for which formatting is
     *  available.
     *
     *  @return the list of currency types
     */

    @OSID @Override
    public org.osid.type.TypeList getCurrencyTypesForFormatting() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.currencyFormats.keySet()));
    }


    /**
     *  Gets the list of currency format types for a given currency
     *  type.
     *
     *  @param  currencyType the type of the currency 
     *  @return the list of supported currency format types 
     *  @throws org.osid.NullArgumentException <code> currencyType
     *          </code> is <code> null </code>
     */

    @OSID @Override
    public org.osid.type.TypeList getCurrencyFormatTypesForCurrencyType(org.osid.type.Type currencyType) {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.currencyFormats.get(currencyType)));
    }


    /**
     *  Adds support for a currency format.
     *
     *  @param currencyType a currency type
     *  @param numericFormatType a numeric format type
     *  @throws org.osid.NullArgumentException <code>currencyType</code> or 
     *          <code>numericFormatType</code> is <code>null</code>
     */

    protected void addCurrencyFormatType(org.osid.type.Type currencyType, org.osid.type.Type numericFormatType) {
        this.currencyFormats.put(currencyType, numericFormatType);
        return;            
    }


    /**
     *  Removes support for a currency format.
     *
     *  @param currencyType a currency type
     *  @param numericFormatType a numeric format type
     *  @throws org.osid.NullArgumentException <code>currencyType</code> or 
     *          <code>numericFormatType</code> is <code>null</code>
     */

    protected void removeCurrencyFormatType(org.osid.type.Type currencyType, org.osid.type.Type numericFormatType) {
        this.currencyFormats.remove(currencyType, numericFormatType);
        return;            
    }


    /**
     *  Tests if a given coordinate formatting is supported. 
     *
     *  @param  coordinateType the type of the coordinate
     *  @param  coordinateFormatType the type of the output coordinate format 
     *  @return <code> true </code> if formatting with the given types is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> cooridinateType 
     *          </code> or <code> coodinateFormatType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsCoordinateTypesForFormatting(org.osid.type.Type coordinateType, org.osid.type.Type coordinateFormatType) {
        return (this.coordinateFormats.containsKeyValue(coordinateType, coordinateFormatType));
    }


    /**
     *  Gets all the coordinate types for which formatting is avilable.
     *
     *  @return the list of coordinate types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCoordinateTypesForFormatting() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.coordinateFormats.keySet()));
    }


    /**
     *  Gets the list of coordinate format types for a given coordinate type. 
     *
     *  @param  coordinateType the type of the coordinate 
     *  @return the list of supported coordinate format types 
     *  @throws org.osid.NullArgumentException <code> coordinaterType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public org.osid.type.TypeList getCoordinateFormatTypesForCoordinateType(org.osid.type.Type coordinateType) {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.coordinateFormats.get(coordinateType)));
    }


    /**
     *  Adds support for a coordinate format.
     *
     *  @param coordinateType a coordinate type
     *  @param numericFormatType a numeric format type
     *  @throws org.osid.NullArgumentException <code>coordinateType</code> or 
     *          <code>numericFormatType</code> is <code>null</code>
     */

    protected void addCoordinateFormatType(org.osid.type.Type coordinateType, org.osid.type.Type numericFormatType) {
        this.coordinateFormats.put(coordinateType, numericFormatType);
        return;            
    }


    /**
     *  Removes support for a coordinate format.
     *
     *  @param coordinateType a coordinate type
     *  @param numericFormatType a numeric format type
     *  @throws org.osid.NullArgumentException <code>coordinateType</code> or 
     *          <code>numericFormatType</code> is <code>null</code>
     */

    protected void removeCoordinateFormatType(org.osid.type.Type coordinateType, org.osid.type.Type numericFormatType) {
        this.coordinateFormats.remove(coordinateType, numericFormatType);
        return;            
    }


    /**
     *  Tests if a given measure conversion is supported. 
     *
     *  @param  sourceUnitType the type of the source measure 
     *  @param  targetUnitType the type of the target measure 
     *  @return <code> true </code> if the given source and target conversion 
     *          is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> sourceUnitType </code> 
     *          or <code> targetUnitType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsUnitTypesForConversion(org.osid.type.Type sourceUnitType, org.osid.type.Type targetUnitType) {
        return (this.unitConversions.containsKeyValue(sourceUnitType, targetUnitType));
    }


    /**
     *  Gets the list of target measure types for a given source measure type. 
     *
     *  @param  sourceUnitType the type of the source measure 
     *  @return the list of supported measure types 
     *  @throws org.osid.NullArgumentException <code> sourceUnitType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public org.osid.type.TypeList getUnitTypesForSource(org.osid.type.Type sourceUnitType) {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.unitConversions.get(sourceUnitType)));
    }


    /**
     *  Gets all the source unit types supported. 
     *
     *  @return the list of supported unit types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSourceUnitTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.unitConversions.keySet()));
    }


    /**
     *  Adds support for a unit conversion.
     *
     *  @param sourceUnitType the source unit type
     *  @param targetUnitType the target unit type
     *  @throws org.osid.NullArgumentException
     *          <code>sourceUnitType</code> or
     *          <code>targetUnitTypeType</code> is <code>null</code>
     */

    protected void addUnitTypesForConversion(org.osid.type.Type sourceUnitType, org.osid.type.Type targetUnitType) {
        this.unitConversions.put(sourceUnitType, targetUnitType);
        return;            
    }


    /**
     *  Removes support for a unit conversion.
     *
     *  @param sourceUnitType the source unit type
     *  @param targetUnitType the target unit type
     *  @throws org.osid.NullArgumentException
     *          <code>sourceUnitType</code> or
     *          <code>targetUnitTypeType</code> is <code>null</code>
     */

    protected void removeUnitTypesForConversion(org.osid.type.Type sourceUnitType, org.osid.type.Type targetUnitType) {
        this.unitConversions.remove(sourceUnitType, targetUnitType);
        return;            
    }


    /**
     *  Tests if a given currency conversion is supported. 
     *
     *  @param  sourceCurrencyType the type of the source currency 
     *  @param  targetCurrencyType the type of the target currency 
     *  @return <code> true </code> if the given source and target conversion 
     *          is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> sourceCurrencyType 
     *          </code> or <code> targetCurrencyType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsCurrencyTypesForConversion(org.osid.type.Type sourceCurrencyType, org.osid.type.Type targetCurrencyType) {
        return (this.currencyConversions.containsKeyValue(sourceCurrencyType, targetCurrencyType));
    }


    /**
     *  Gets the list of target currency types for a given source currency 
     *  type. 
     *
     *  @param  sourceCurrencyType the type of the source currency 
     *  @return the list of supported currency types 
     *  @throws org.osid.NullArgumentException <code> sourceCurrencyType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public org.osid.type.TypeList getCurrencyTypesForSource(org.osid.type.Type sourceCurrencyType) {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.currencyConversions.get(sourceCurrencyType)));
    }


    /**
     *  Gets the list of source currency types. 
     *
     *  @return the list of supported currency types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSourceCurrencyTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.currencyConversions.keySet()));
    }


    /**
     *  Adds support for a currency conversion.
     *
     *  @param sourceCurrencyType the source currency type
     *  @param targetCurrencyType the target currency type
     *  @throws org.osid.NullArgumentException
     *          <code>sourceCurrencyType</code> or
     *          <code>targetCurrencyTypeType</code> is
     *          <code>null</code>
     */

    protected void addCurrencyTypesForConversion(org.osid.type.Type sourceCurrencyType, org.osid.type.Type targetCurrencyType) {
        this.currencyConversions.put(sourceCurrencyType, targetCurrencyType);
        return;            
    }


    /**
     *  Removes support for a currency conversion.
     *
     *  @param sourceCurrencyType the source currency type
     *  @param targetCurrencyType the target currency type
     *  @throws org.osid.NullArgumentException
     *          <code>sourceCurrencyType</code> or
     *          <code>targetCurrencyTypeType</code> is
     *          <code>null</code>
     */

    protected void removeCurrencyTypesForConversion(org.osid.type.Type sourceCurrencyType, org.osid.type.Type targetCurrencyType) {
        this.currencyConversions.remove(sourceCurrencyType, targetCurrencyType);
        return;            
    }


    /**
     *  Tests if a given calendar conversion is supported. 
     *
     *  @param  sourceCalendarType the type of the source calendar 
     *  @param  targetCalendarType the type of the target calendar 
     *  @return <code> true </code> if the given source and target conversion 
     *          is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> sourceCalendarType 
     *          </code> or <code> targetCalendarType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsCalendarTypesForConversion(org.osid.type.Type sourceCalendarType, org.osid.type.Type targetCalendarType) {
        return (this.calendarConversions.containsKeyValue(sourceCalendarType, targetCalendarType));
    }


    /**
     *  Gets the list of target calendar types for a given source calendar 
     *  type. 
     *
     *  @param  sourceCalendarType the type of the source calendar 
     *  @return the list of supported calendar types 
     *  @throws org.osid.NullArgumentException <code> sourceCalendarType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public org.osid.type.TypeList getCalendarTypesForSource(org.osid.type.Type sourceCalendarType) {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.calendarConversions.get(sourceCalendarType)));
    }


    /**
     *  Gets the list of source calendar types.
     *
     *  @return the list of supported calendar types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSourceCalendarTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.calendarConversions.keySet()));
    }


    /**
     *  Adds support for a calendar conversion.
     *
     *  @param sourceCalendarType the source calendar type
     *  @param targetCalendarType the target calendar type
     *  @throws org.osid.NullArgumentException
     *          <code>sourceCalendarType</code> or
     *          <code>targetCalendarTypeType</code> is
     *          <code>null</code>
     */

    protected void addCalendarTypesForConversion(org.osid.type.Type sourceCalendarType, org.osid.type.Type targetCalendarType) {
        this.calendarConversions.put(sourceCalendarType, targetCalendarType);
        return;            
    }


    /**
     *  Removes support for a calendar conversion.
     *
     *  @param sourceCalendarType the source calendar type
     *  @param targetCalendarType the target calendar type
     *  @throws org.osid.NullArgumentException
     *          <code>sourceCalendarType</code> or
     *          <code>targetCalendarTypeType</code> is
     *          <code>null</code>
     */

    protected void removeCalendarTypesForConversion(org.osid.type.Type sourceCalendarType, org.osid.type.Type targetCalendarType) {
        this.calendarConversions.remove(sourceCalendarType, targetCalendarType);
        return;            
    }


    /**
     *  Tests if a given time conversion is supported. 
     *
     *  @param  sourceTimeType the type of the source time 
     *  @param  targetTimeType the type of the target time 
     *  @return <code> true </code> if the given source and target conversion 
     *          is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> sourceTimeType </code> 
     *          or <code> targetTimeType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsTimeTypesForConversion(org.osid.type.Type sourceTimeType, org.osid.type.Type targetTimeType) {
        return (this.timeConversions.containsKeyValue(sourceTimeType, targetTimeType));
    }


    /**
     *  Gets the list of target time types for a given source time type. 
     *
     *  @param  sourceTimeType the type of the source time 
     *  @return the list of supported time types 
     *  @throws org.osid.NullArgumentException <code> sourceTimeType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public org.osid.type.TypeList getTimeTypesForSource(org.osid.type.Type sourceTimeType) {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.timeConversions.get(sourceTimeType)));
    }


    /**
     *  Gets the list of source time types.
     *
     *  @return the list of supported time types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSourceTimeTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.timeConversions.keySet()));
    }


    /**
     *  Adds support for a time conversion.
     *
     *  @param sourceTimeType the source time type
     *  @param targetTimeType the target time type
     *  @throws org.osid.NullArgumentException
     *          <code>sourceTimeType</code> or
     *          <code>targetTimeTypeType</code> is <code>null</code>
     */

    protected void addTimeTypesForConversion(org.osid.type.Type sourceTimeType, org.osid.type.Type targetTimeType) {
        this.timeConversions.put(sourceTimeType, targetTimeType);
        return;            
    }


    /**
     *  Removes support for a time conversion.
     *
     *  @param sourceTimeType the source time type
     *  @param targetTimeType the target time type
     *  @throws org.osid.NullArgumentException
     *          <code>sourceTimeType</code> or
     *          <code>targetTimeTypeType</code> is <code>null</code>
     */

    protected void removeTimeTypesForConversion(org.osid.type.Type sourceTimeType, org.osid.type.Type targetTimeType) {
        this.timeConversions.remove(sourceTimeType, targetTimeType);
        return;            
    }


    /**
     *  Gets the list of time types supported for a given calendar
     *  type where they are both used in a <code> DateTime. </code>
     *
     *  @param  calendarType the type of the calendar 
     *  @return the list of supported time types 
     *  @throws org.osid.NullArgumentException <code> calendarType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public org.osid.type.TypeList getTimeTypesForCalendarType(org.osid.type.Type calendarType) {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.datetimes.get(calendarType)));
    }


    /**
     *  Gets the list of calendar types supported for a given time
     *  type where they are both used in a <code> DateTime. </code>
     *
     *  @param  timeType the type of the time system 
     *  @return the list of supported calendar types 
     *  @throws org.osid.NullArgumentException <code> timeType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public org.osid.type.TypeList getCalendarTypesForTimeType(org.osid.type.Type timeType) {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.timedates.get(timeType)));
    }


    /**
     *  Tests if a given calendar and time type are used together in a
     *  <code> DateTime. </code>
     *
     *  @param  calendarType the type of the calendar 
     *  @param  timeType the type of the time system 
     *  @return <code> true </code> if the given calendar and time types are 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> calendarType </code> or 
     *          <code> timeType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCalendarTimeTypes(org.osid.type.Type calendarType, org.osid.type.Type timeType) {
        return (this.datetimes.containsKeyValue(calendarType, timeType));
    }


    /**
     *  Adds support for a datetime.
     *
     *  @param calendarType a calendar type
     *  @param timeType a time type
     *  @throws org.osid.NullArgumentException
     *          <code>calendarType</code> or <code>timeType</code> is
     *          <code>null</code>
     */

    protected void addCalendarTimeType(org.osid.type.Type calendarType, org.osid.type.Type timeType) {
        this.datetimes.put(calendarType, timeType);
        this.timedates.put(timeType, calendarType);

        return;            
    }


    /**
     *  Removes support for a datetime.
     *
     *  @param calendarType a calendar type
     *  @param timeType a time type
     *  @throws org.osid.NullArgumentException
     *          <code>calendarType</code> or <code>timeType</code> is
     *          <code>null</code>
     */

    protected void removeCalendarTimeType(org.osid.type.Type calendarType, org.osid.type.Type timeType) {
        this.datetimes.remove(calendarType, timeType);
        this.timedates.remove(timeType, calendarType);

        return;            
    }


    /**
     *  Tests if a given coordinate type for conversion is supported. 
     *
     *  @param  sourceCoordinateType the type of the source coordinate 
     *  @param  targetCoordinateType the type of the target coordinate 
     *  @return <code> true </code> if the given source and target conversion 
     *          is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> sourceCoordinateType 
     *          </code> or <code> targetCoordinateType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsCoordinateTypesForConversion(org.osid.type.Type sourceCoordinateType, org.osid.type.Type targetCoordinateType) {
        return (this.coordinateConversions.containsKeyValue(sourceCoordinateType, targetCoordinateType));
    }


    /**
     *  Gets the list of target coordinate types for a given source coordinate 
     *  type. 
     *
     *  @param  sourceCoordinateType the type of the source coordinate 
     *  @return the list of supported coordinate types 
     *  @throws org.osid.NullArgumentException <code> sourceCoordinateType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public org.osid.type.TypeList getCoordinateTypesForSource(org.osid.type.Type sourceCoordinateType) {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.coordinateConversions.get(sourceCoordinateType)));
    }


    /**
     *  Gets the list of source coordinate types.
     *
     *  @return the list of supported coordinate types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSourceCoordinateTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.coordinateConversions.keySet()));
    }


    /**
     *  Adds support for a coordinate conversion.
     *
     *  @param sourceCoordinateType the source coordinate type
     *  @param targetCoordinateType the target coordinate type
     *  @throws org.osid.NullArgumentException
     *          <code>sourceCoordinateType</code> or
     *          <code>targetCoordinateTypeType</code> is
     *          <code>null</code>
     */

    protected void addCoordinateTypesForConversion(org.osid.type.Type sourceCoordinateType, org.osid.type.Type targetCoordinateType) {
        this.coordinateConversions.put(sourceCoordinateType, targetCoordinateType);
        return;            
    }


    /**
     *  Removes support for a coordinate conversion.
     *
     *  @param sourceCoordinateType the source coordinate type
     *  @param targetCoordinateType the target coordinate type
     *  @throws org.osid.NullArgumentException
     *          <code>sourceCoordinateType</code> or
     *          <code>targetCoordinateTypeType</code> is
     *          <code>null</code>
     */

    protected void removeCoordinateTypesForConversion(org.osid.type.Type sourceCoordinateType, org.osid.type.Type targetCoordinateType) {
        this.coordinateConversions.remove(sourceCoordinateType, targetCoordinateType);
        return;            
    }


    /**
     *  Tests if a given spatial conversion is supported. 
     *
     *  @param  sourceSpatialUnitRecordType the type of the source spatial 
     *          unit record 
     *  @param  targetSpatialUnitRecordType the type of the target spatial 
     *          unit record 
     *  @return <code> true </code> if the given source and target conversion 
     *          is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          sourceSpatialUnitRecordType </code> or <code> 
     *          targetSpatialUnitRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSpatialUnitRecordTypesForConversion(org.osid.type.Type sourceSpatialUnitRecordType, org.osid.type.Type targetSpatialUnitRecordType) {
        return (this.spatialUnitConversions.containsKeyValue(sourceSpatialUnitRecordType, targetSpatialUnitRecordType));        
    }


    /**
     *  Gets the list of target spatial unit record types for a given
     *  source spatial unit type.
     *
     *  @param sourceSpatialUnitRecordType the type of the source
     *          spatial unit record
     *  @return the list of supported spatial unit record types 
     *  @throws org.osid.NullArgumentException <code>
     *          sourceSpatialUnitRecordType </code> is <code> null
     *          </code>
     */

    @OSID @Override
    public org.osid.type.TypeList getSpatialUnitRecordTypesForSource(org.osid.type.Type sourceSpatialUnitRecordType) {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.spatialUnitConversions.get(sourceSpatialUnitRecordType)));
    }


    /**
     *  Gets the list of source spatial unit record types.
     *
     *  @return the list of supported spatial unit record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSourceSpatialUnitRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.spatialUnitConversions.keySet()));
    }


    /**
     *  Adds support for a spatial unit conversion.
     *
     *  @param sourceSpatialUnitType the source spatial unit type
     *  @param targetSpatialUnitType the target spatial unit type
     *  @throws org.osid.NullArgumentException
     *          <code>sourceSpatialUnitType</code> or
     *          <code>targetSpatialUnitTypeType</code> is
     *          <code>null</code>
     */

    protected void addSpatialUnitTypesForConversion(org.osid.type.Type sourceSpatialUnitType, org.osid.type.Type targetSpatialUnitType) {
        this.spatialUnitConversions.put(sourceSpatialUnitType, targetSpatialUnitType);
        return;            
    }


    /**
     *  Removes support for a spatial unit conversion.
     *
     *  @param sourceSpatialUnitType the source spatial unit type
     *  @param targetSpatialUnitType the target spatial unit type
     *  @throws org.osid.NullArgumentException
     *          <code>sourceSpatialUnitType</code> or
     *          <code>targetSpatialUnitTypeType</code> is
     *          <code>null</code>
     */

    protected void removeSpatialUnitTypesForConversion(org.osid.type.Type sourceSpatialUnitType, org.osid.type.Type targetSpatialUnitType) {
        this.spatialUnitConversions.remove(sourceSpatialUnitType, targetSpatialUnitType);
        return;            
    }


    /**
     *  Tests if a given format type for conversion is supported. 
     *
     *  @param  sourceFormatType the type of the source format 
     *  @param  targetFormatType the type of the target format 
     *  @return <code> true </code> if the given source and target conversion 
     *          is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> sourceFormatType 
     *          </code> or <code> targetFormatType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsFormatTypesForConversion(org.osid.type.Type sourceFormatType, org.osid.type.Type targetFormatType) {
        return (this.formatConversions.containsKeyValue(sourceFormatType, targetFormatType));
    }


    /**
     *  Gets the list of target format types for a given source format 
     *  type. 
     *
     *  @param  sourceFormatType the type of the source format 
     *  @return the list of supported format types 
     *  @throws org.osid.NullArgumentException <code> sourceFormatType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public org.osid.type.TypeList getFormatTypesForSource(org.osid.type.Type sourceFormatType) {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.formatConversions.get(sourceFormatType)));
    }


    /**
     *  Gets the list of source format types.
     *
     *  @return the list of supported format types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSourceFormatTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.formatConversions.keySet()));
    }


    /**
     *  Adds support for a format conversion.
     *
     *  @param sourceFormatType the source format type
     *  @param targetFormatType the target format type
     *  @throws org.osid.NullArgumentException
     *          <code>sourceFormatType</code> or
     *          <code>targetFormatTypeType</code> is
     *          <code>null</code>
     */

    protected void addFormatTypesForConversion(org.osid.type.Type sourceFormatType, org.osid.type.Type targetFormatType) {
        this.formatConversions.put(sourceFormatType, targetFormatType);
        return;            
    }


    /**
     *  Removes support for a format conversion.
     *
     *  @param sourceFormatType the source format type
     *  @param targetFormatType the target format type
     *  @throws org.osid.NullArgumentException
     *          <code>sourceFormatType</code> or
     *          <code>targetFormatTypeType</code> is
     *          <code>null</code>
     */

    protected void removeFormatTypesForConversion(org.osid.type.Type sourceFormatType, org.osid.type.Type targetFormatType) {
        this.formatConversions.remove(sourceFormatType, targetFormatType);
        return;            
    }


    /**
     *  Gets an <code> OsidSession </code> associated with the language 
     *  translation service. 
     *
     *  @return a <code> TranslationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTranslation() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.TranslationSession getTranslationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.locale.LocaleManager.getTranslationSession not implemented");
    }


    /**
     *  Gets an <code> OsidSession </code> associated with the language 
     *  translation service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TranslationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTranslation() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.TranslationSession getTranslationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.locale.LocaleProxyManager.getTranslationSession not implemented");
    }


    /**
     *  Gets an <code> OsidSession </code> associated with the language 
     *  translation service and the given language and script types. 
     *
     *  @param  sourceLanguageType the type of the source language 
     *  @param  sourceScriptType the type of the source script 
     *  @param  targetLanguageType the type of the target language 
     *  @param  targetScriptType the type of the target script 
     *  @return a <code> TranslationSession </code> 
     *  @throws org.osid.NullArgumentException <code> sourceLanguageType, 
     *          sourceScriptType, targetLanguageType </code> or <code> 
     *          targetScriptType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTranslation() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsLanguageTypesForTranslation(sourceLanguageType, 
     *          sourceScriptType, targetLanguageType, targetScriptType) 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.TranslationSession getTranslationSessionForType(org.osid.type.Type sourceLanguageType, 
                                                                           org.osid.type.Type sourceScriptType, 
                                                                           org.osid.type.Type targetLanguageType, 
                                                                           org.osid.type.Type targetScriptType)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.locale.LocaleManager.getTranslationSessionForType not implemented");
    }


    /**
     *  Gets an <code> OsidSession </code> associated with the language 
     *  translation service and the given language and script types. 
     *
     *  @param  sourceLanguageType the type of the source language 
     *  @param  sourceScriptType the type of the source script 
     *  @param  targetLanguageType the type of the target language 
     *  @param  targetScriptType the type of the target script 
     *  @param  proxy a proxy 
     *  @return a <code> TranslationSession </code> 
     *  @throws org.osid.NullArgumentException <code> sourceLanguageType, 
     *          sourceScriptType, targetLanguageType, targetScriptType </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTranslation() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsLanguageTypesForTranslation(sourceLanguageType, 
     *          sourceScriptType, targetLanguageType, targetScriptType) 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.TranslationSession getTranslationSessionForType(org.osid.type.Type sourceLanguageType, 
                                                                           org.osid.type.Type sourceScriptType, 
                                                                           org.osid.type.Type targetLanguageType, 
                                                                           org.osid.type.Type targetScriptType, 
                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.locale.LocaleProxyManager.getTranslationSessionForType not implemented");
    }


    /**
     *  Gets a language translation administration service for updating a 
     *  locale dictionary. 
     *
     *  @return a <code> TranslationAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTranslationAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.TranslationAdminSession getTranslationAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.locale.LocaleManager.getTranslationAdminSession not implemented");
    }


    /**
     *  Gets a language translation administration service for updating a 
     *  locale dictionary. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TranslationAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTranslationAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.TranslationAdminSession getTranslationAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.locale.LocaleProxyManager.getTranslationAdminSession not implemented");
    }


    /**
     *  Gets a language trabslation administration service for updating a 
     *  locale dictionary using the given language and script types. 
     *
     *  @param  sourceLanguageType the type of the source language 
     *  @param  sourceScriptType the type of the source script 
     *  @param  targetLanguageType the type of the target language 
     *  @param  targetScriptType the type of the target script 
     *  @return a <code> TranslationAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> sourceLanguageType, 
     *          sourceScriptType, targetLanguageType </code> or <code> 
     *          targetScriptType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTranslationAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsLanguageTypesForTranslation(sourceLanguageType, 
     *          sourceScriptType, targetLanguageType, targetScriptType) 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.TranslationAdminSession getTranslationAdminSessionForType(org.osid.type.Type sourceLanguageType, 
                                                                                     org.osid.type.Type sourceScriptType, 
                                                                                     org.osid.type.Type targetLanguageType, 
                                                                                     org.osid.type.Type targetScriptType)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.locale.LocaleManager.getTranslationAdminSessionForType not implemented");
    }


    /**
     *  Gets a language trabslation administration service for updating a 
     *  locale dictionary using the given language and script types. 
     *
     *  @param  sourceLanguageType the type of the source language 
     *  @param  sourceScriptType the type of the source script 
     *  @param  targetLanguageType the type of the target language 
     *  @param  targetScriptType the type of the target script 
     *  @param  proxy a proxy 
     *  @return a <code> TranslationAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> sourceLanguageType, 
     *          sourceScriptType, targetLanguageType, targetScriptType </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTranslationAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsLanguageTypesForTranslation(sourceLanguageType, 
     *          sourceScriptType, targetLanguageType, targetScriptType) 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.TranslationAdminSession getTranslationAdminSessionForType(org.osid.type.Type sourceLanguageType, 
                                                                                     org.osid.type.Type sourceScriptType, 
                                                                                     org.osid.type.Type targetLanguageType, 
                                                                                     org.osid.type.Type targetScriptType, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.locale.LocaleProxyManager.getTranslationAdminSessionForType not implemented");
    }


    /**
     *  Gets an <code> OsidSession </code> associated with the numeric 
     *  formatting service. 
     *
     *  @return a <code> NumericFormattingSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsNumericFormatting() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.NumericFormattingSession getNumericFormattingSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.locale.LocaleManager.getNumericFormattingSession not implemented");
    }


    /**
     *  Gets an <code> OsidSession </code> associated with the numeric 
     *  formatting service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> NumericFormattingSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsNumericFormatting() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.NumericFormattingSession getNumericFormattingSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.locale.LocaleProxyManager.getNumericFormattingSession not implemented");
    }


    /**
     *  Gets an <code> OsidSession </code> associated with the numeric 
     *  formatting service and the given numeric format type. 
     *
     *  @param  numericFormatType the type of the numeric format 
     *  @return a <code> NumericFormattingSession </code> 
     *  @throws org.osid.NullArgumentException <code> numericFormatType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsNumericFormatting() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsNumericFormatType(numericFormatType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.locale.NumericFormattingSession getNumericFormattingSessionForType(org.osid.type.Type numericFormatType)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.locale.LocaleManager.getNumericFormattingSessionForType not implemented");
    }


    /**
     *  Gets an <code> OsidSession </code> associated with the numeric 
     *  formatting service and the given numeric format type. 
     *
     *  @param  numericFormatType the type of the numeric format 
     *  @param  proxy a proxy 
     *  @return a <code> NumericFormattingSession </code> 
     *  @throws org.osid.NullArgumentException <code> numericFormatType 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsNumericFormatting() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsNumericFormatType(numericFormatType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.locale.NumericFormattingSession getNumericFormattingSessionForType(org.osid.type.Type numericFormatType, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.locale.LocaleProxyManager.getNumericFormattingSessionForType not implemented");
    }


    /**
     *  Gets an <code> OsidSession </code> associated with the calendar 
     *  formatting service. 
     *
     *  @return a <code> CalendarFormattingSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCalendarFormatting() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.CalendarFormattingSession getCalendarFormattingSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.locale.LocaleManager.getCalendarFormattingSession not implemented");
    }


    /**
     *  Gets an <code> OsidSession </code> associated with the calendar 
     *  formatting service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CalendarFormattingSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCalendarFormatting() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.CalendarFormattingSession getCalendarFormattingSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.locale.LocaleProxyManager.getCalendarFormattingSession not implemented");
    }


    /**
     *  Gets an <code> OsidSession </code> associated with the calendar 
     *  formatting service and the given calendar and time types. 
     *
     *  @param  calendarType the type of the calendar 
     *  @param  calendarFormatType the type of the calendar format 
     *  @param  timeType the type of the time system 
     *  @param  timeFormatType the type of the time format 
     *  @return a <code> CalendarFormattingSession </code> 
     *  @throws org.osid.NullArgumentException <code> calendarType, 
     *          calendarFormatType, timeType </code> or <code> timeFormatType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCalendarFormatting() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsCalendarTypesForFormattinge(calendarType, 
     *          calendarFormatType, timeType, timeFormatType) </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.CalendarFormattingSession getCalendarFormattingSessionForType(org.osid.type.Type calendarType, 
                                                                                         org.osid.type.Type calendarFormatType, 
                                                                                         org.osid.type.Type timeType, 
                                                                                         org.osid.type.Type timeFormatType)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.locale.LocaleManager.getCalendarFormattingSessionForType not implemented");
    }


    /**
     *  Gets an <code> OsidSession </code> associated with the calendar 
     *  formatting service and the given calendar and time types. 
     *
     *  @param  calendarType the type of the calendar 
     *  @param  calendarFormatType the type of the calendar format 
     *  @param  timeType the type of the time system 
     *  @param  timeFormatType the type of the time format 
     *  @param  proxy a proxy 
     *  @return a <code> CalendarFormattingSession </code> 
     *  @throws org.osid.NullArgumentException <code> calendarType, 
     *          calendarFormatType, timeTyp, timeFormatType </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCalendarFormatting() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsCalendarTypesForFormattinge(calendarType, 
     *          calendarFormatType, timeType, timeFormatType) </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.CalendarFormattingSession getCalendarFormattingSessionForType(org.osid.type.Type calendarType, 
                                                                                         org.osid.type.Type calendarFormatType, 
                                                                                         org.osid.type.Type timeType, 
                                                                                         org.osid.type.Type timeFormatType, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.locale.LocaleProxyManager.getCalendarFormattingSessionForType not implemented");
    }


    /**
     *  Gets an <code> OsidSession </code> associated with the currency 
     *  formatting service. 
     *
     *  @return a <code> CurrencyFormattingSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCurrencyFormatting() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.CurrencyFormattingSession getCurrencyFormattingSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.locale.LocaleManager.getCurrencyFormattingSession not implemented");
    }


    /**
     *  Gets an <code> OsidSession </code> associated with the currency 
     *  formatting service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CurrencyFormattingSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCurrencyFormatting() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.CurrencyFormattingSession getCurrencyFormattingSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.locale.LocaleProxyManager.getCurrencyFormattingSession not implemented");
    }


    /**
     *  Gets an <code> OsidSession </code> associated with the currency 
     *  formatting service and the given currency and numeric format types. 
     *
     *  @param  currencyType the type of the currency 
     *  @param  numericFormatType the type of the numeric format 
     *  @return a <code> CurrencyFormattingSession </code> 
     *  @throws org.osid.NullArgumentException <code> currencyType </code> or 
     *          <code> numericFormatType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCurrencyFormatting() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsCurrencyTypesForFomatting(currencyType, 
     *          numericFormatType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.CurrencyFormattingSession getCurrencyFormattingSessionForType(org.osid.type.Type currencyType, 
                                                                                         org.osid.type.Type numericFormatType)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.locale.LocaleManager.getCurrencyFormattingSessionForType not implemented");
    }


    /**
     *  Gets an <code> OsidSession </code> associated with the currency 
     *  formatting service and the given currency and numeric format types. 
     *
     *  @param  currencyType the type of the currency 
     *  @param  numericFormatType the type of the numeric format 
     *  @param  proxy a proxy 
     *  @return a <code> CurrencyFormattingSession </code> 
     *  @throws org.osid.NullArgumentException <code> currencyType, 
     *          numericFormatType </code> or <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCurrencyFormatting() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsCurrencyTypesForFomatting(currencyType, 
     *          numericFormatType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.CurrencyFormattingSession getCurrencyFormattingSessionForType(org.osid.type.Type currencyType, 
                                                                                         org.osid.type.Type numericFormatType, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.locale.LocaleProxyManager.getCurrencyFormattingSessionForType not implemented");
    }


    /**
     *  Gets an <code> OsidSession </code> associated with the coordinate 
     *  formatting service. 
     *
     *  @return a <code> CoordinateFormattingSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCoordinateFormatting() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.CoordinateFormattingSession getCoordinateFormattingSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.locale.LocaleManager.getCoordinateFormattingSession not implemented");
    }


    /**
     *  Gets an <code> OsidSession </code> associated with the coordinate 
     *  formatting service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CoordinateFormattingSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCoordinateFormatting() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.CoordinateFormattingSession getCoordinateFormattingSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.locale.LocaleProxyManager.getCoordinateFormattingSession not implemented");
    }


    /**
     *  Gets an <code> OsidSession </code> associated with the coordinate 
     *  formatting service and the given coordinate and format types. 
     *
     *  @param  coordinateType the type of the coordinate 
     *  @param  coordinateFormatType the type of the coordinate format 
     *  @return a <code> CoordinateFormattingSession </code> 
     *  @throws org.osid.NullArgumentException <code> coordinateType </code> 
     *          or <code> coordinateFormatType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCoordinateFormatting() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsCoordinateTypesForFomatting(coordinateType, 
     *          coordinateFormatType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.CoordinateFormattingSession getCoordinateFormattingSessionForType(org.osid.type.Type coordinateType, 
                                                                                             org.osid.type.Type coordinateFormatType)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.locale.LocaleManager.getCoordinateFormattingSessionForType not implemented");
    }


    /**
     *  Gets an <code> OsidSession </code> associated with the coordinate 
     *  formatting service and the given coordinate and format types. 
     *
     *  @param  coordinateType the type of the coordinate 
     *  @param  coordinateFormatType the type of the coordinate format 
     *  @param  proxy a proxy 
     *  @return a <code> CoordinateFormattingSession </code> 
     *  @throws org.osid.NullArgumentException <code> coordinateType, 
     *          coordinateFormatType, </code> or <code> proxy </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCoordinateFormatting() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsCoordinateTypesForFomatting(coordinateType, 
     *          coordinateFormatType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.CoordinateFormattingSession getCoordinateFormattingSessionForType(org.osid.type.Type coordinateType, 
                                                                                             org.osid.type.Type coordinateFormatType, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.locale.LocaleProxyManager.getCoordinateFormattingSessionForType not implemented");
    }


    /**
     *  Gets a unit conversion session. 
     *
     *  @return a <code> UnitConversionSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsUnitConversion() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.UnitConversionSession getUnitConversionSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.locale.LocaleManager.getUnitConversionSession not implemented");
    }


    /**
     *  Gets a unit conversion session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> UnitConversionSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsUnitConversion() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.UnitConversionSession getUnitConversionSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.locale.LocaleProxyManager.getUnitConversionSession not implemented");
    }


    /**
     *  Gets a currency conversion session. 
     *
     *  @return a <code> CurrencyConversionSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCurrencyConversion() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.CurrencyConversionSession getCurrencyConversionSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.locale.LocaleManager.getCurrencyConversionSession not implemented");
    }


    /**
     *  Gets a currency conversion session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CurrencyConversionSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCurrencyConversion() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.CurrencyConversionSession getCurrencyConversionSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.locale.LocaleProxyManager.getCurrencyConversionSession not implemented");
    }


    /**
     *  Gets an <code> OsidSession </code> associated with the currency 
     *  conversion service and the given currency types. 
     *
     *  @param  sourceCurrencyType the type of the source currency 
     *  @param  targetCurrencyType the type of the target currency 
     *  @return a <code> CurrencyConversionSession </code> 
     *  @throws org.osid.NullArgumentException <code> sourceCurrencyType 
     *          </code> or <code> targetCurrencyType </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCurrencyConversion() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsCurrencyTypesForConversion(sourceCurrencyType, 
     *          targetCurrencyType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.CurrencyConversionSession getCurrencyConversionSessionForType(org.osid.type.Type sourceCurrencyType, 
                                                                                         org.osid.type.Type targetCurrencyType)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.locale.LocaleManager.getCurrencyConversionSessionForType not implemented");
    }


    /**
     *  Gets an <code> OsidSession </code> associated with the currency 
     *  conversion service and the given currency types. 
     *
     *  @param  sourceCurrencyType the type of the source currency 
     *  @param  targetCurrencyType the type of the target currency 
     *  @param  proxy a proxy 
     *  @return a <code> CurrencyConversionSession </code> 
     *  @throws org.osid.NullArgumentException <code> sourceCurrencyType, 
     *          targetCurrencyType </code> or <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCurrencyConversion() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsCurrencyTypesForConversion(sourceCurrencyType, 
     *          targetCurrencyType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.CurrencyConversionSession getCurrencyConversionSessionForType(org.osid.type.Type sourceCurrencyType, 
                                                                                         org.osid.type.Type targetCurrencyType, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.locale.LocaleProxyManager.getCurrencyConversionSessionForType not implemented");
    }


    /**
     *  Gets a calendar conversion session. 
     *
     *  @return a <code> CalendarConversionSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCalendarConversion() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.CalendarConversionSession getCalendarConversionSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.locale.LocaleManager.getCalendarConversionSession not implemented");
    }


    /**
     *  Gets a calendar conversion session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CalendarConversionSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCalendarConversion() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.CalendarConversionSession getCalendarConversionSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.locale.LocaleProxyManager.getCalendarConversionSession not implemented");
    }


    /**
     *  Gets an <code> OsidSession </code> associated with the calendar 
     *  conversion service and the given calendar types. 
     *
     *  @param  sourceCalendarType the type of the source calendar 
     *  @param  sourceTimeType the type of the source time 
     *  @param  targetCalendarType the type of the target calendar 
     *  @param  targetTimeType the type of the target time 
     *  @return a <code> CalendarConversionSession </code> 
     *  @throws org.osid.NullArgumentException <code> sourceCalendarType, 
     *          sourceTimeType, </code> <code> targetCalendarType </code> or 
     *          <code> targetTimeType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCalendarConversion() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsCalendarTypesForConversion(sourceCalendarType, 
     *          targetCalendarType) </code> or <code> 
     *          supportsTimeTypesForConversion(sourceTimeType, targetTimeType) 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.CalendarConversionSession getCalendarConversionSessionForType(org.osid.type.Type sourceCalendarType, 
                                                                                         org.osid.type.Type sourceTimeType, 
                                                                                         org.osid.type.Type targetCalendarType, 
                                                                                         org.osid.type.Type targetTimeType)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.locale.LocaleManager.getCalendarConversionSessionForType not implemented");
    }


    /**
     *  Gets an <code> OsidSession </code> associated with the calendar 
     *  conversion service and the given calendar types. 
     *
     *  @param  sourceCalendarType the type of the source calendar 
     *  @param  sourceTimeType the type of the source time 
     *  @param  targetCalendarType the type of the target calendar 
     *  @param  targetTimeType the type of the target time 
     *  @param  proxy a proxy 
     *  @return a <code> CalendarConversionSession </code> 
     *  @throws org.osid.NullArgumentException <code> sourceCalendarType, 
     *          sourceTimeType, targetCalendarType, targetTimeType </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCalendarConversion() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsCalendarTypesForConversion(sourceCalendarType, 
     *          targetCalendarType) </code> or <code> 
     *          supportsTimeTypesForConversion(sourceTimeType, targetTimeType) 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.CalendarConversionSession getCalendarConversionSessionForType(org.osid.type.Type sourceCalendarType, 
                                                                                         org.osid.type.Type sourceTimeType, 
                                                                                         org.osid.type.Type targetCalendarType, 
                                                                                         org.osid.type.Type targetTimeType, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.locale.LocaleProxyManager.getCalendarConversionSessionForType not implemented");
    }


    /**
     *  Gets a coordinate conversion session. 
     *
     *  @return a <code> CoordinateConversionSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCoordinateConversion() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.CoordinateConversionSession getCoordinateConversionSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.locale.LocaleManager.getCoordinateConversionSession not implemented");
    }


    /**
     *  Gets a coordinate conversion session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CoordinateConversionSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCoordinateConversion() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.CoordinateConversionSession getCoordinateConversionSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.locale.LocaleProxyManager.getCoordinateConversionSession not implemented");
    }


    /**
     *  Gets an <code> OsidSession </code> associated with the coordinate 
     *  conversion service and the given coordinate types. 
     *
     *  @param  sourceCoordinateType the type of the source coordinate 
     *  @param  targetCoordinateType the type of the target coordinate 
     *  @return a <code> CoordinateConversionSession </code> 
     *  @throws org.osid.NullArgumentException <code> sourceCoordinateType 
     *          </code> or <code> targetCoordinateType </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCoordinateConversion() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsCoordinateRecordTypesForConversion(sourceCoordinateType, 
     *          targetCoordinateType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.CoordinateConversionSession getCoordinateConversionSessionForType(org.osid.type.Type sourceCoordinateType, 
                                                                                             org.osid.type.Type targetCoordinateType)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.locale.LocaleManager.getCoordinateConversionSessionForType not implemented");
    }


    /**
     *  Gets an <code> OsidSession </code> associated with the coordinate 
     *  conversion service and the given coordinate types. 
     *
     *  @param  sourceCoordinateType the type of the source coordinate 
     *  @param  targetCoordinateType the type of the target coordinate 
     *  @param  proxy a proxy 
     *  @return a <code> CoordinateConversionSession </code> 
     *  @throws org.osid.NullArgumentException <code> sourceCoordinateType, 
     *          </code> <code> targetCoordinateType, </code> or <code> proxy 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCoordinateConversion() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsCoordinateRecordTypesForConversion(sourceCoordinateType, 
     *          targetCoordinateType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.CoordinateConversionSession getCoordinateConversionSessionForType(org.osid.type.Type sourceCoordinateType, 
                                                                                             org.osid.type.Type targetCoordinateType, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.locale.LocaleProxyManager.getCoordinateConversionSessionForType not implemented");
    }


    /**
     *  Gets a spatial unit conversion session. 
     *
     *  @return a <code> SpatialUnitConversionSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpatialUnitConversion() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.locale.SpatialUnitConversionSession getSpatialUnitConversionSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.locale.LocaleManager.getSpatialUnitConversionSession not implemented");
    }


    /**
     *  Gets a spatial unit conversion session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SpatialUnitConversionSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpatialUnitConversion() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.locale.SpatialUnitConversionSession getSpatialUnitConversionSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.locale.LocaleProxyManager.getSpatialUnitConversionSession not implemented");
    }


    /**
     *  Gets an <code> OsidSession </code> associated with the spatial unit 
     *  conversion service and the given coordinate record types. 
     *
     *  @param  sourceSpatialUnitRecordType the type of the source spatial 
     *          unit record 
     *  @param  targetSpatialUnitRecordType the type of the target spatial 
     *          unit record 
     *  @return a <code> SpatialUnitConversionSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          sourceSpatialUnitRecordType </code> or <code> 
     *          targetSpatialUnitRecordType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpatialUnitConversion() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsSpatialUnitRecordTypesForConversion(sourceSpatialUnitRecordType, 
     *          targetSpatialUnitRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.SpatialUnitConversionSession getSpatialUnitConversionSessionForType(org.osid.type.Type sourceSpatialUnitRecordType, 
                                                                                               org.osid.type.Type targetSpatialUnitRecordType)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.locale.LocaleManager.getSpatialUnitConversionSessionForType not implemented");
    }


    /**
     *  Gets an <code> OsidSession </code> associated with the spatial unit 
     *  conversion service and the given coordinate record types. 
     *
     *  @param  sourceSpatialUnitRecordType the type of the source spatial 
     *          unit record 
     *  @param  targetSpatialUnitRecordType the type of the target spatial 
     *          unit record 
     *  @param  proxy a proxy 
     *  @return a <code> SpatialUnitConversionSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          sourceSpatialUnitRecordType, </code> <code> 
     *          targetSpatialUnitRecordType or </code> proxy is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpatialUnitConversion() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsSpatialUnitRecordTypesForConversion(sourceSpatialUnitRecordType, 
     *          targetSpatialUnitRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.SpatialUnitConversionSession getSpatialUnitConversionSessionForType(org.osid.type.Type sourceSpatialUnitRecordType, 
                                                                                               org.osid.type.Type targetSpatialUnitRecordType, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.locale.LocaleProxyManager.getSpatialUnitConversionSessionForType not implemented");
    }


    /**
     *  Gets a format conversion session. 
     *
     *  @return a <code> FormatConversionSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFormatConversion() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.FormatConversionSession getFormatConversionSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.locale.LocaleManager.getFormatConversionSession not implemented");
    }


    /**
     *  Gets a format conversion session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FormatConversionSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFormatConversion() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.FormatConversionSession getFormatConversionSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.locale.LocaleProxyManager.getFormatConversionSession not implemented");
    }


    /**
     *  Gets an <code> OsidSession </code> associated with the format 
     *  conversion service and the given format types. 
     *
     *  @param  sourceFormatType the type of the source format 
     *  @param  targetFormatType the type of the target format 
     *  @return a <code> FormatConversionSession </code> 
     *  @throws org.osid.NullArgumentException <code> sourceFormatType 
     *          </code> or <code> targetFormatType </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFormatConversion() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsFormatRecordTypesForConversion(sourceFormatType, 
     *          targetFormatType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.FormatConversionSession getFormatConversionSessionForType(org.osid.type.Type sourceFormatType, 
                                                                                             org.osid.type.Type targetFormatType)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.locale.LocaleManager.getFormatConversionSessionForType not implemented");
    }


    /**
     *  Gets an <code> OsidSession </code> associated with the format 
     *  conversion service and the given format types. 
     *
     *  @param  sourceFormatType the type of the source format 
     *  @param  targetFormatType the type of the target format 
     *  @param  proxy a proxy 
     *  @return a <code> FormatConversionSession </code> 
     *  @throws org.osid.NullArgumentException <code> sourceFormatType, 
     *          </code> <code> targetFormatType, </code> or <code> proxy 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFormatConversion() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsFormatRecordTypesForConversion(sourceFormatType, 
     *          targetFormatType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.FormatConversionSession getFormatConversionSessionForType(org.osid.type.Type sourceFormatType, 
                                                                                             org.osid.type.Type targetFormatType, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.locale.LocaleProxyManager.getFormatConversionSessionForType not implemented");
    }


    /**
     *  Gets a calendar informational session session. 
     *
     *  @return a <code> CalendarInfoSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCalendarInfo() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.CalendarInfoSession getCalendarInfoSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.locale.LocaleManager.getCalendarInfoSession not implemented");
    }


    /**
     *  Gets a calendar informational session session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CalendarInfoSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCalendarInfo() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.CalendarInfoSession getCalendarInfoSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.locale.LocaleProxyManager.getCalendarInfoSession not implemented");
    }


    /**
     *  Gets an <code> OsidSession </code> associated with the calendar 
     *  informational service and the given calendar and time types. 
     *
     *  @param  calendarType the type of the calendar 
     *  @param  timeType the type of the time system 
     *  @return a <code> CalendarInfoSession </code> 
     *  @throws org.osid.NullArgumentException <code> calendarType </code> or 
     *          <code> timeType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCalendarType() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsCalendarTimeTypes(calendarType, timeType) </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.CalendarInfoSession getCalendarInfoSessionForType(org.osid.type.Type calendarType, 
                                                                             org.osid.type.Type timeType)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.locale.LocaleManager.getCalendarInfoSessionForType not implemented");
    }


    /**
     *  Gets an <code> OsidSession </code> associated with the calendar 
     *  informational service and the given calendar and time types. 
     *
     *  @param  calendarType the type of the calendar 
     *  @param  timeType the type of the time system 
     *  @param  proxy a proxy 
     *  @return a <code> CalendarInfoSession </code> 
     *  @throws org.osid.NullArgumentException <code> calendarType, timeType 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCalendarType() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsCalendarTimeTypes(calendarType, timeType) </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.CalendarInfoSession getCalendarInfoSessionForType(org.osid.type.Type calendarType, 
                                                                             org.osid.type.Type timeType, 
                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.locale.LocaleProxyManager.getCalendarInfoSessionForType not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();

        this.languageTranslations.clear();
        this.numericFormats.clear();
        this.calendarFormats.clear();
        this.currencyFormats.clear();
        this.coordinateFormats.clear();

        this.unitConversions.clear();
        this.calendarConversions.clear();
        this.timeConversions.clear();
        this.currencyConversions.clear();
        this.coordinateConversions.clear();
        this.spatialUnitConversions.clear();
        this.formatConversions.clear();

        this.datetimes.clear();
        this.timedates.clear();

        return;
    }


    protected class TypePair {
        private org.osid.type.Type a;
        private org.osid.type.Type b;


        protected TypePair(org.osid.type.Type a, org.osid.type.Type b) {
            nullarg(a, "a");
            nullarg(b, "b");

            this.a = a;
            this.b = b;

            return;
        }

        
        protected org.osid.type.Type getAType() {
            return (this.a);
        }


        protected org.osid.type.Type getBType() {
            return (this.b);
        }


        @Override 
        public boolean equals(Object obj) {
            if (obj == this) {
                return (true);
            }

            if (!(obj instanceof LanguageType)) {
                return (false);
            }

            LanguageType lt = (LanguageType) obj;
            return (getAType().equals(lt.getAType()) &&
                    getBType().equals(lt.getBType()));
        }

        
        @Override 
        public int hashCode() {
            int code = getAType().hashCode() * 31;
            return (code + getBType().hashCode());
        }
    }


    protected class LanguageType 
        extends TypePair {


        protected LanguageType(org.osid.type.Type language, org.osid.type.Type script) {
            super(language, script);
            return;
        }

        
        public org.osid.type.Type getLanguageType() {
            return (getAType());
        }


        public org.osid.type.Type getScriptType() {
            return (getBType());
        }
    }


    protected class DateTimeType 
        extends TypePair {


        protected DateTimeType(org.osid.type.Type calendar, org.osid.type.Type time) {
            super(calendar, time);
            return;
        }

        
        public org.osid.type.Type getCalendarType() {
            return (getAType());
        }


        public org.osid.type.Type getTimeType() {
            return (getBType());
        }
    }


    protected class DateTimeFormatType 
        extends TypePair {


        protected DateTimeFormatType(org.osid.type.Type calendarFormat, org.osid.type.Type timeFormat) {
            super(calendarFormat, timeFormat);
            return;
        }

        
        public org.osid.type.Type getCalendarFormatType() {
            return (getAType());
        }


        public org.osid.type.Type getTimeFormatType() {
            return (getBType());
        }
    }
}
