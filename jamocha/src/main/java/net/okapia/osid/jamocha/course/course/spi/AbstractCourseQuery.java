//
// AbstractCourseQuery.java
//
//     A template for making a Course Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.course.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for courses.
 */

public abstract class AbstractCourseQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOperableOsidObjectQuery
    implements org.osid.course.CourseQuery {

    private final java.util.Collection<org.osid.course.records.CourseQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Adds a title for this query. 
     *
     *  @param  title title string to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> title </code> not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> title </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchTitle(String title, org.osid.type.Type stringMatchType, 
                           boolean match) {
        return;
    }


    /**
     *  Matches a title that has any value. 
     *
     *  @param  match <code> true </code> to match courses with any title, 
     *          <code> false </code> to match courses with no title 
     */

    @OSID @Override
    public void matchAnyTitle(boolean match) {
        return;
    }


    /**
     *  Clears the title terms. 
     */

    @OSID @Override
    public void clearTitleTerms() {
        return;
    }


    /**
     *  Adds a course number for this query. 
     *
     *  @param  number course number string to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> number </code> not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> number </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchNumber(String number, org.osid.type.Type stringMatchType, 
                            boolean match) {
        return;
    }


    /**
     *  Matches a course number that has any value. 
     *
     *  @param  match <code> true </code> to match courses with any number, 
     *          <code> false </code> to match courses with no title 
     */

    @OSID @Override
    public void matchAnyNumber(boolean match) {
        return;
    }


    /**
     *  Clears the number terms. 
     */

    @OSID @Override
    public void clearNumberTerms() {
        return;
    }


    /**
     *  Sets the resource <code> Id </code> for this query to match courses 
     *  that have a sponsor. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSponsorId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the sponsor <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSponsorIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSponsorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a sponsor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return a resource query 
     *  @throws org.osid.UnimplementedException <code> supportsSponsorQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getSponsorQuery() {
        throw new org.osid.UnimplementedException("supportsSponsorQuery() is false");
    }


    /**
     *  Matches courses that have any sponsor. 
     *
     *  @param  match <code> true </code> to match courses with any sponsor, 
     *          <code> false </code> to match courses with no sponsors 
     */

    @OSID @Override
    public void matchAnySponsor(boolean match) {
        return;
    }


    /**
     *  Clears the sponsor terms. 
     */

    @OSID @Override
    public void clearSponsorTerms() {
        return;
    }


    /**
     *  Sets the grade <code> Id </code> for this query to match
     *  courses that have a credit amount.
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code>gradeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCreditAmountId(org.osid.id.Id gradId, boolean match) {
        return;
    }


    /**
     *  Clears the credit amount <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCreditAmountIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> GradeQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCreditAmountQuery() {
        return (false);
    }


    /**
     *  Gets the query for a credit amount. Multiple retrievals
     *  produce a nested <code> OR </code> term.
     *
     *  @return a grade query 
     *  @throws org.osid.UnimplementedException <code> supportsCreditAmountQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeQuery getCreditAmountQuery() {
        throw new org.osid.UnimplementedException("supportsCreditAmountQuery() is false");
    }


    /**
     *  Matches courses that have any credit amount. 
     *
     *  @param match <code> true </code> to match courses with any
     *         credit amount, <code> false </code> to match courses
     *         with no credit amounts
     */

    @OSID @Override
    public void matchAnyCreditAmount(boolean match) {
        return;
    }


    /**
     *  Clears the credit aount terms. 
     */

    @OSID @Override
    public void clearCreditAmountTerms() {
        return;
    }


    /**
     *  Matches courses with the prerequisites informational string. 
     *
     *  @param  prereqInfo prerequisite informational string to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> prereqInfo </code> 
     *          not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> prereqInfo </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchPrerequisitesInfo(String prereqInfo, 
                                       org.osid.type.Type stringMatchType, 
                                       boolean match) {
        return;
    }


    /**
     *  Matches a course that has any prerequisite information assigned. 
     *
     *  @param  match <code> true </code> to match courses with any 
     *          prerequisite information, <code> false </code> otherwise 
     */

    @OSID @Override
    public void matchAnyPrerequisitesInfo(boolean match) {
        return;
    }


    /**
     *  Clears the prerequisite info terms. 
     */

    @OSID @Override
    public void clearPrerequisitesInfoTerms() {
        return;
    }


    /**
     *  Sets the requisite <code> Id </code> for this query. 
     *
     *  @param  requisiteId a requisite <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> requisiteId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPrerequisitesId(org.osid.id.Id requisiteId, boolean match) {
        return;
    }


    /**
     *  Clears the requisite <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearPrerequisitesIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> RequisiteQuery </code> is available. 
     *
     *  @return <code> true </code> if a requisite query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPrerequisitesQuery() {
        return (false);
    }


    /**
     *  Gets the query for a requisite. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return a prerequisites query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPrerequisitesQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteQuery getPrerequisitesQuery() {
        throw new org.osid.UnimplementedException("supportsPrerequisitesQuery() is false");
    }


    /**
     *  Matches courses that have any prerequisites. 
     *
     *  @param  match <code> true </code> to match courses with any 
     *          prerequisites, <code> false </code> to match courses with no 
     *          prerequisites 
     */

    @OSID @Override
    public void matchAnyPrerequisites(boolean match) {
        return;
    }


    /**
     *  Clears the prerequisites terms. 
     */

    @OSID @Override
    public void clearPrerequisitesTerms() {
        return;
    }


    /**
     *  Sets the grade <code> Id </code> for this query. 
     *
     *  @param  gradeId a grade <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchLevelId(org.osid.id.Id gradeId, boolean match) {
        return;
    }


    /**
     *  Clears the grade <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearLevelIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> GradeQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLevelQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grade level. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return a grade query 
     *  @throws org.osid.UnimplementedException <code> supportsLevelQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeQuery getLevelQuery() {
        throw new org.osid.UnimplementedException("supportsLevelQuery() is false");
    }


    /**
     *  Matches courses that have any grade level. 
     *
     *  @param  match <code> true </code> to match courses with any level, 
     *          <code> false </code> to match courses with no level 
     */

    @OSID @Override
    public void matchAnyLevel(boolean match) {
        return;
    }


    /**
     *  Clears the level terms. 
     */

    @OSID @Override
    public void clearLevelTerms() {
        return;
    }


    /**
     *  Sets the grade system <code> Id </code> for this query. 
     *
     *  @param  gradeSystemId a grade system <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeSystemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGradingOptionId(org.osid.id.Id gradeSystemId, 
                                     boolean match) {
        return;
    }


    /**
     *  Clears the grade system <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGradingOptionIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> GradeSystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade system query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradingOptionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grading option. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return a grade system query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradingOptionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQuery getGradingOptionQuery() {
        throw new org.osid.UnimplementedException("supportsGradingOptionQuery() is false");
    }


    /**
     *  Matches courses that have any grading option. 
     *
     *  @param  match <code> true </code> to match courses with any grading 
     *          option, <code> false </code> to match courses with no grading 
     *          options 
     */

    @OSID @Override
    public void matchAnyGradingOption(boolean match) {
        return;
    }


    /**
     *  Clears the grading option terms. 
     */

    @OSID @Override
    public void clearGradingOptionTerms() {
        return;
    }


    /**
     *  Sets the objective <code> Id </code> for this query. 
     *
     *  @param  objectiveId an objective <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> objectiveId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchLearningObjectiveId(org.osid.id.Id objectiveId, 
                                         boolean match) {
        return;
    }


    /**
     *  Clears the objective <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearLearningObjectiveIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ObjectiveQuery </code> is available. 
     *
     *  @return <code> true </code> if an objective query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLearningObjectiveQuery() {
        return (false);
    }


    /**
     *  Gets the query for a learning objective. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return an objective query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLearningObjectiveQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQuery getLearningObjectiveQuery() {
        throw new org.osid.UnimplementedException("supportsLearningObjectiveQuery() is false");
    }


    /**
     *  Matches courses that have any learning objective. 
     *
     *  @param  match <code> true </code> to match courses with any learning 
     *          objective, <code> false </code> to match courses with no 
     *          learning objectives 
     */

    @OSID @Override
    public void matchAnyLearningObjective(boolean match) {
        return;
    }


    /**
     *  Clears the learning objective terms. 
     */

    @OSID @Override
    public void clearLearningObjectiveTerms() {
        return;
    }


    /**
     *  Sets the course offering <code> Id </code> for this query to match 
     *  courses that have a related course offering. 
     *
     *  @param  courseOfferingId a course offering <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseOfferingId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchActivityUnitId(org.osid.id.Id courseOfferingId, 
                                    boolean match) {
        return;
    }


    /**
     *  Clears the activity unit <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearActivityUnitIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ActivityUnitQuery </code> is available. 
     *
     *  @return <code> true </code> if a activity unit query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityUnitQuery() {
        return (false);
    }


    /**
     *  Gets the query for an activity unit. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the activity unit query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnitQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitQuery getActivityUnitQuery() {
        throw new org.osid.UnimplementedException("supportsActivityUnitQuery() is false");
    }


    /**
     *  Matches courses that have any course offering. 
     *
     *  @param  match <code> true </code> to match courses with any related 
     *          activity unit, <code> false </code> to match courses with no 
     *          activity units 
     */

    @OSID @Override
    public void matchAnyActivityUnit(boolean match) {
        return;
    }


    /**
     *  Clears the activity unit terms. 
     */

    @OSID @Override
    public void clearActivityUnitTerms() {
        return;
    }


    /**
     *  Sets the course offering <code> Id </code> for this query to match 
     *  courses that have a related course offering. 
     *
     *  @param  courseOfferingId a course offering <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseOfferingId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseOfferingId(org.osid.id.Id courseOfferingId, 
                                      boolean match) {
        return;
    }


    /**
     *  Clears the course offering <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseOfferingIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CourseOfferingQuery </code> is available. 
     *
     *  @return <code> true </code> if a course offering query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseOfferingQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course offering. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the course offering query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingQuery getCourseOfferingQuery() {
        throw new org.osid.UnimplementedException("supportsCourseOfferingQuery() is false");
    }


    /**
     *  Matches courses that have any course offering. 
     *
     *  @param  match <code> true </code> to match courses with any course 
     *          offering, <code> false </code> to match courses with no course 
     *          offering 
     */

    @OSID @Override
    public void matchAnyCourseOffering(boolean match) {
        return;
    }


    /**
     *  Clears the course offering terms. 
     */

    @OSID @Override
    public void clearCourseOfferingTerms() {
        return;
    }


    /**
     *  Sets the course catalog <code> Id </code> for this query to match 
     *  courses assigned to course catalogs. 
     *
     *  @param  courseCatalogId the course catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseCatalogId(org.osid.id.Id courseCatalogId, 
                                     boolean match) {
        return;
    }


    /**
     *  Clears the course catalog <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseCatalogIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CourseCatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a course catalog query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course catalog. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the course catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQuery getCourseCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsCourseCatalogQuery() is false");
    }


    /**
     *  Clears the course catalog terms. 
     */

    @OSID @Override
    public void clearCourseCatalogTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given course query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a course implementing the requested record.
     *
     *  @param courseRecordType a course record type
     *  @return the course query record
     *  @throws org.osid.NullArgumentException
     *          <code>courseRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(courseRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.records.CourseQueryRecord getCourseQueryRecord(org.osid.type.Type courseRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.records.CourseQueryRecord record : this.records) {
            if (record.implementsRecordType(courseRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(courseRecordType + " is not supported");
    }


    /**
     *  Adds a record to this course query. 
     *
     *  @param courseQueryRecord course query record
     *  @param courseRecordType course record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCourseQueryRecord(org.osid.course.records.CourseQueryRecord courseQueryRecord, 
                                          org.osid.type.Type courseRecordType) {

        addRecordType(courseRecordType);
        nullarg(courseQueryRecord, "course query record");
        this.records.add(courseQueryRecord);        
        return;
    }
}
