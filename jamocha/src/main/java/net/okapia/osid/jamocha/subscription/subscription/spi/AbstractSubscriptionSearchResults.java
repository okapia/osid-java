//
// AbstractSubscriptionSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.subscription.subscription.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractSubscriptionSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.subscription.SubscriptionSearchResults {

    private org.osid.subscription.SubscriptionList subscriptions;
    private final org.osid.subscription.SubscriptionQueryInspector inspector;
    private final java.util.Collection<org.osid.subscription.records.SubscriptionSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractSubscriptionSearchResults.
     *
     *  @param subscriptions the result set
     *  @param subscriptionQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>subscriptions</code>
     *          or <code>subscriptionQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractSubscriptionSearchResults(org.osid.subscription.SubscriptionList subscriptions,
                                            org.osid.subscription.SubscriptionQueryInspector subscriptionQueryInspector) {
        nullarg(subscriptions, "subscriptions");
        nullarg(subscriptionQueryInspector, "subscription query inspectpr");

        this.subscriptions = subscriptions;
        this.inspector = subscriptionQueryInspector;

        return;
    }


    /**
     *  Gets the subscription list resulting from a search.
     *
     *  @return a subscription list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptions() {
        if (this.subscriptions == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.subscription.SubscriptionList subscriptions = this.subscriptions;
        this.subscriptions = null;
	return (subscriptions);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.subscription.SubscriptionQueryInspector getSubscriptionQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  subscription search record <code> Type. </code> This method must
     *  be used to retrieve a subscription implementing the requested
     *  record.
     *
     *  @param subscriptionSearchRecordType a subscription search 
     *         record type 
     *  @return the subscription search
     *  @throws org.osid.NullArgumentException
     *          <code>subscriptionSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(subscriptionSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.subscription.records.SubscriptionSearchResultsRecord getSubscriptionSearchResultsRecord(org.osid.type.Type subscriptionSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.subscription.records.SubscriptionSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(subscriptionSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(subscriptionSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record subscription search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addSubscriptionRecord(org.osid.subscription.records.SubscriptionSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "subscription record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
