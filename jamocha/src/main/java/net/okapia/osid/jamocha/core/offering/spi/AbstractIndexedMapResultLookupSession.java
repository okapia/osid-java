//
// AbstractIndexedMapResultLookupSession.java
//
//    A simple framework for providing a Result lookup service
//    backed by a fixed collection of results with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.offering.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Result lookup service backed by a
 *  fixed collection of results. The results are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some results may be compatible
 *  with more types than are indicated through these result
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Results</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapResultLookupSession
    extends AbstractMapResultLookupSession
    implements org.osid.offering.ResultLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.offering.Result> resultsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.offering.Result>());
    private final MultiMap<org.osid.type.Type, org.osid.offering.Result> resultsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.offering.Result>());


    /**
     *  Makes a <code>Result</code> available in this session.
     *
     *  @param  result a result
     *  @throws org.osid.NullArgumentException <code>result<code> is
     *          <code>null</code>
     */

    @Override
    protected void putResult(org.osid.offering.Result result) {
        super.putResult(result);

        this.resultsByGenus.put(result.getGenusType(), result);
        
        try (org.osid.type.TypeList types = result.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.resultsByRecord.put(types.getNextType(), result);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }

    
    /**
     *  Makes an array of results available in this session.
     *
     *  @param  results an array of results
     *  @throws org.osid.NullArgumentException <code>results<code>
     *          is <code>null</code>
     */

    @Override
    protected void putResults(org.osid.offering.Result[] results) {
        for (org.osid.offering.Result result : results) {
            putResult(result);
        }

        return;
    }


    /**
     *  Makes a collection of results available in this session.
     *
     *  @param  results a collection of results
     *  @throws org.osid.NullArgumentException <code>results<code>
     *          is <code>null</code>
     */

    @Override
    protected void putResults(java.util.Collection<? extends org.osid.offering.Result> results) {
        for (org.osid.offering.Result result : results) {
            putResult(result);
        }

        return;
    }


    /**
     *  Removes a result from this session.
     *
     *  @param resultId the <code>Id</code> of the result
     *  @throws org.osid.NullArgumentException <code>resultId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeResult(org.osid.id.Id resultId) {
        org.osid.offering.Result result;
        try {
            result = getResult(resultId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.resultsByGenus.remove(result.getGenusType());

        try (org.osid.type.TypeList types = result.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.resultsByRecord.remove(types.getNextType(), result);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeResult(resultId);
        return;
    }


    /**
     *  Gets a <code>ResultList</code> corresponding to the given
     *  result genus <code>Type</code> which does not include
     *  results of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known results or an error results. Otherwise,
     *  the returned list may contain only those results that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  resultGenusType a result genus type 
     *  @return the returned <code>Result</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>resultGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.ResultList getResultsByGenusType(org.osid.type.Type resultGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.offering.result.ArrayResultList(this.resultsByGenus.get(resultGenusType)));
    }


    /**
     *  Gets a <code>ResultList</code> containing the given
     *  result record <code>Type</code>. In plenary mode, the
     *  returned list contains all known results or an error
     *  results. Otherwise, the returned list may contain only those
     *  results that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  resultRecordType a result record type 
     *  @return the returned <code>result</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>resultRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.ResultList getResultsByRecordType(org.osid.type.Type resultRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.offering.result.ArrayResultList(this.resultsByRecord.get(resultRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.resultsByGenus.clear();
        this.resultsByRecord.clear();

        super.close();

        return;
    }
}
