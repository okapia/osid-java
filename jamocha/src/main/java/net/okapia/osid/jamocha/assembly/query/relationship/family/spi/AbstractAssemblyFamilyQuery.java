//
// AbstractAssemblyFamilyQuery.java
//
//     A FamilyQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.relationship.family.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A FamilyQuery that stores terms.
 */

public abstract class AbstractAssemblyFamilyQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCatalogQuery
    implements org.osid.relationship.FamilyQuery,
               org.osid.relationship.FamilyQueryInspector,
               org.osid.relationship.FamilySearchOrder {

    private final java.util.Collection<org.osid.relationship.records.FamilyQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.relationship.records.FamilyQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.relationship.records.FamilySearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyFamilyQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyFamilyQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches a relationship <code> Id. </code> 
     *
     *  @param  relationshipId a relationship <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> relationshipId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchRelationshipId(org.osid.id.Id relationshipId, 
                                    boolean match) {
        getAssembler().addIdTerm(getRelationshipIdColumn(), relationshipId, match);
        return;
    }


    /**
     *  Clears the relationship <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRelationshipIdTerms() {
        getAssembler().clearTerms(getRelationshipIdColumn());
        return;
    }


    /**
     *  Gets the relationship <code> Id </code> terms. 
     *
     *  @return the relationship <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRelationshipIdTerms() {
        return (getAssembler().getIdTerms(getRelationshipIdColumn()));
    }


    /**
     *  Gets the RelationshipId column name.
     *
     * @return the column name
     */

    protected String getRelationshipIdColumn() {
        return ("relationship_id");
    }


    /**
     *  Tests if a relationship query is available. 
     *
     *  @return <code> true </code> if a relationship query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelationshipQuery() {
        return (false);
    }


    /**
     *  Gets the query interface for a relationship. 
     *
     *  @return the relationship query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipQuery getRelationshipQuery() {
        throw new org.osid.UnimplementedException("supportsRelationshipQuery() is false");
    }


    /**
     *  Matches families with any relationship. 
     *
     *  @param  match <code> true </code> to match families with any 
     *          relationship, <code> false </code> to match families with no 
     *          relationship 
     */

    @OSID @Override
    public void matchAnyRelationship(boolean match) {
        getAssembler().addIdWildcardTerm(getRelationshipColumn(), match);
        return;
    }


    /**
     *  Clears the relationship terms. 
     */

    @OSID @Override
    public void clearRelationshipTerms() {
        getAssembler().clearTerms(getRelationshipColumn());
        return;
    }


    /**
     *  Gets the relationship terms. 
     *
     *  @return the relationship terms 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipQueryInspector[] getRelationshipTerms() {
        return (new org.osid.relationship.RelationshipQueryInspector[0]);
    }


    /**
     *  Gets the Relationship column name.
     *
     * @return the column name
     */

    protected String getRelationshipColumn() {
        return ("relationship");
    }


    /**
     *  Sets the family <code> Id </code> for this query to match families 
     *  that have the specified family as an ancestor. 
     *
     *  @param  familyId a family <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> familyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorFamilyId(org.osid.id.Id familyId, boolean match) {
        getAssembler().addIdTerm(getAncestorFamilyIdColumn(), familyId, match);
        return;
    }


    /**
     *  Clears the ancestor family <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorFamilyIdTerms() {
        getAssembler().clearTerms(getAncestorFamilyIdColumn());
        return;
    }


    /**
     *  Gets the ancestor family <code> Id </code> terms. 
     *
     *  @return the ancestor family <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorFamilyIdTerms() {
        return (getAssembler().getIdTerms(getAncestorFamilyIdColumn()));
    }


    /**
     *  Gets the AncestorFamilyId column name.
     *
     * @return the column name
     */

    protected String getAncestorFamilyIdColumn() {
        return ("ancestor_family_id");
    }


    /**
     *  Tests if a <code> FamilyQuery </code> is available. 
     *
     *  @return <code> true </code> if a family query interface is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorFamilyQuery() {
        return (false);
    }


    /**
     *  Gets the query interface for a family. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the family query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorFamilyQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.FamilyQuery getAncestorFamilyQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorFamilyQuery() is false");
    }


    /**
     *  Matches families with any ancestor. 
     *
     *  @param  match <code> true </code> to match families with any ancestor, 
     *          <code> false </code> to match root families 
     */

    @OSID @Override
    public void matchAnyAncestorFamily(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorFamilyColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor family terms. 
     */

    @OSID @Override
    public void clearAncestorFamilyTerms() {
        getAssembler().clearTerms(getAncestorFamilyColumn());
        return;
    }


    /**
     *  Gets the ancestor family terms. 
     *
     *  @return the ancestor family terms 
     */

    @OSID @Override
    public org.osid.relationship.FamilyQueryInspector[] getAncestorFamilyTerms() {
        return (new org.osid.relationship.FamilyQueryInspector[0]);
    }


    /**
     *  Gets the AncestorFamily column name.
     *
     * @return the column name
     */

    protected String getAncestorFamilyColumn() {
        return ("ancestor_family");
    }


    /**
     *  Sets the family <code> Id </code> for this query to match families 
     *  that have the specified family as a descednant. 
     *
     *  @param  familyId a family <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> familyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantFamilyId(org.osid.id.Id familyId, boolean match) {
        getAssembler().addIdTerm(getDescendantFamilyIdColumn(), familyId, match);
        return;
    }


    /**
     *  Clears the descendant family <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantFamilyIdTerms() {
        getAssembler().clearTerms(getDescendantFamilyIdColumn());
        return;
    }


    /**
     *  Gets the descendant family <code> Id </code> terms. 
     *
     *  @return the descendant family <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantFamilyIdTerms() {
        return (getAssembler().getIdTerms(getDescendantFamilyIdColumn()));
    }


    /**
     *  Gets the DescendantFamilyId column name.
     *
     * @return the column name
     */

    protected String getDescendantFamilyIdColumn() {
        return ("descendant_family_id");
    }


    /**
     *  Tests if a <code> FamilyQuery </code> is available. 
     *
     *  @return <code> true </code> if a family query interface is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantFamilyQuery() {
        return (false);
    }


    /**
     *  Gets the query interface for a family. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the family query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantFamilyQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.relationship.FamilyQuery getDescendantFamilyQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantFamilyQuery() is false");
    }


    /**
     *  Matches families with any decendant. 
     *
     *  @param  match <code> true </code> to match families with any 
     *          decendants, <code> false </code> to match leaf families 
     */

    @OSID @Override
    public void matchAnyDescendantFamily(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantFamilyColumn(), match);
        return;
    }


    /**
     *  Clears the descendant family terms. 
     */

    @OSID @Override
    public void clearDescendantFamilyTerms() {
        getAssembler().clearTerms(getDescendantFamilyColumn());
        return;
    }


    /**
     *  Gets the descendant family terms. 
     *
     *  @return the descendant family terms 
     */

    @OSID @Override
    public org.osid.relationship.FamilyQueryInspector[] getDescendantFamilyTerms() {
        return (new org.osid.relationship.FamilyQueryInspector[0]);
    }


    /**
     *  Gets the DescendantFamily column name.
     *
     * @return the column name
     */

    protected String getDescendantFamilyColumn() {
        return ("descendant_family");
    }


    /**
     *  Tests if this family supports the given record
     *  <code>Type</code>.
     *
     *  @param  familyRecordType a family record type 
     *  @return <code>true</code> if the familyRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>familyRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type familyRecordType) {
        for (org.osid.relationship.records.FamilyQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(familyRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  familyRecordType the family record type 
     *  @return the family query record 
     *  @throws org.osid.NullArgumentException
     *          <code>familyRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(familyRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.relationship.records.FamilyQueryRecord getFamilyQueryRecord(org.osid.type.Type familyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.relationship.records.FamilyQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(familyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(familyRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  familyRecordType the family record type 
     *  @return the family query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>familyRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(familyRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.relationship.records.FamilyQueryInspectorRecord getFamilyQueryInspectorRecord(org.osid.type.Type familyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.relationship.records.FamilyQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(familyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(familyRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param familyRecordType the family record type
     *  @return the family search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>familyRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(familyRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.relationship.records.FamilySearchOrderRecord getFamilySearchOrderRecord(org.osid.type.Type familyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.relationship.records.FamilySearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(familyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(familyRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this family. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param familyQueryRecord the family query record
     *  @param familyQueryInspectorRecord the family query inspector
     *         record
     *  @param familySearchOrderRecord the family search order record
     *  @param familyRecordType family record type
     *  @throws org.osid.NullArgumentException
     *          <code>familyQueryRecord</code>,
     *          <code>familyQueryInspectorRecord</code>,
     *          <code>familySearchOrderRecord</code> or
     *          <code>familyRecordTypefamily</code> is
     *          <code>null</code>
     */
            
    protected void addFamilyRecords(org.osid.relationship.records.FamilyQueryRecord familyQueryRecord, 
                                      org.osid.relationship.records.FamilyQueryInspectorRecord familyQueryInspectorRecord, 
                                      org.osid.relationship.records.FamilySearchOrderRecord familySearchOrderRecord, 
                                      org.osid.type.Type familyRecordType) {

        addRecordType(familyRecordType);

        nullarg(familyQueryRecord, "family query record");
        nullarg(familyQueryInspectorRecord, "family query inspector record");
        nullarg(familySearchOrderRecord, "family search odrer record");

        this.queryRecords.add(familyQueryRecord);
        this.queryInspectorRecords.add(familyQueryInspectorRecord);
        this.searchOrderRecords.add(familySearchOrderRecord);
        
        return;
    }
}
