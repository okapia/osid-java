//
// AbstractItem.java
//
//     Defines an Item.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.item.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>Item</code>.
 */

public abstract class AbstractItem
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.assessment.Item {

    private org.osid.assessment.Question question;
    private final java.util.Collection<org.osid.assessment.Answer> answers = new java.util.LinkedHashSet<>();;

    private final java.util.Collection<org.osid.learning.Objective> learningObjectives = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.assessment.records.ItemRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the <code> Question </code>. 
     *
     *  @return the question <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getQuestionId() {
        return (this.question.getId());
    }


    /**
     *  Gets the question. 
     *
     *  @return the question 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.assessment.Question getQuestion()
        throws org.osid.OperationFailedException {

        return (this.question);
    }


    /**
     *  Sets the question.
     * 
     *  @param question the question
     *  @throws org.osid/NullArgumentException <code>question</code>
     *          is <code>null</code>
     */

    protected void setQuestion(org.osid.assessment.Question question) {
        nullarg(question, "question");
        this.question = question;
        return;
    }


    /**
     *  Gets the <code> Ids </code> of the <code> Answers </code>. 
     *
     *  @return the answer <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getAnswerIds() {
        try {
            org.osid.assessment.AnswerList answers = getAnswers();
            return (new net.okapia.osid.jamocha.adapter.converter.assessment.answer.AnswerToIdList(answers));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the answers. 
     *
     *  @return the answers 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.assessment.AnswerList getAnswers()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.assessment.answer.ArrayAnswerList(this.answers));
    }


    /**
     *  Adds an answer.
     * 
     *  @param answer an answer
     *  @throws org.osid/NullArgumentException <code>answer</code>
     *          is <code>null</code>
     */

    protected void addAnswer(org.osid.assessment.Answer answer) {
        nullarg(answer, "answer");
        this.answers.add(answer);
        return;
    }


    /**
     *  Sets all the answers.
     * 
     *  @param answers a collection of answers
     *  @throws org.osid/NullArgumentException <code>answers</code>
     *          is <code>null</code>
     */

    protected void setAnswers(java.util.Collection<org.osid.assessment.Answer> answers) {
        nullarg(answers, "answers");

        this.answers.clear();
        this.answers.addAll(answers);

        return;
    }


    /**
     *  Gets the <code> Id </code> of any <code> Objectives </code> 
     *  corresponding to this item. 
     *
     *  @return learning objective <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getLearningObjectiveIds() {
        try {
            org.osid.learning.ObjectiveList learningObjectives = getLearningObjectives();
            return (new net.okapia.osid.jamocha.adapter.converter.learning.objective.ObjectiveToIdList(learningObjectives));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the any <code> Objectives </code> corresponding to this item. 
     *
     *  @return the learning objectives 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveList getLearningObjectives()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.learning.objective.ArrayObjectiveList(this.learningObjectives));
    }


    /**
     *  Adds a learning objective.
     *
     *  @param objective a learning objective
     *  @throws org.osid.NullArgumentException <code>objective</code>
     *          is <code>null</code>
     */

    protected void addLearningObjective(org.osid.learning.Objective objective) {
        nullarg(objective, "learning objective");
        this.learningObjectives.add(objective);
        return;
    }


    /**
     *  Sets all the learning objectives.
     *
     *  @param objectives a collection of learning objectives
     *  @throws org.osid.NullArgumentException <code>objectives</code>
     *          is <code>null</code>
     */

    protected void setLearningObjectives(java.util.Collection<org.osid.learning.Objective> objectives) {
        nullarg(objectives, "learning objectives");

        this.learningObjectives.clear();
        this.learningObjectives.addAll(objectives);

        return;
    }


    /**
     *  Tests if this item supports the given record
     *  <code>Type</code>.
     *
     *  @param  itemRecordType an item record type 
     *  @return <code>true</code> if the itemRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>itemRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type itemRecordType) {
        for (org.osid.assessment.records.ItemRecord record : this.records) {
            if (record.implementsRecordType(itemRecordType)) {
                return (true);
            }
        }

        return (false);        
    }


    /**
     *  Gets the record corresponding to the given <code>Item</code>
     *  record <code>Type</code>.
     *
     *  @param  itemRecordType the item record type 
     *  @return the item record 
     *  @throws org.osid.NullArgumentException
     *          <code>itemRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable
     *          to complete request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(itemRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.records.ItemRecord getItemRecord(org.osid.type.Type itemRecordType)
        throws org.osid.OperationFailedException {


        for (org.osid.assessment.records.ItemRecord record : this.records) {
            if (record.implementsRecordType(itemRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(itemRecordType + " is not supported");
    }


    /**
     *  Adds a record to this item. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param itemRecord the item record
     *  @param itemRecordType item record type
     *  @throws org.osid.NullArgumentException
     *          <code>itemRecord</code> or
     *          <code>itemRecordType</code> is
     *          <code>null</code>
     */
            
    protected void addItemRecord(org.osid.assessment.records.ItemRecord itemRecord, 
                                 org.osid.type.Type itemRecordType) {

        nullarg(itemRecord, "item record");
        addRecordType(itemRecordType);
        this.records.add(itemRecord);
        
        return;
    }
}
