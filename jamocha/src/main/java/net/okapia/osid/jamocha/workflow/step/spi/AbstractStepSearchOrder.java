//
// AbstractStepSearchOdrer.java
//
//     Defines a StepSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.workflow.step.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code StepSearchOrder}.
 */

public abstract class AbstractStepSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidGovernatorSearchOrder
    implements org.osid.workflow.StepSearchOrder {

    private final java.util.Collection<org.osid.workflow.records.StepSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Orders the results by process. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByProcess(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a process search order is available. 
     *
     *  @return <code> true </code> if a process search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessSearchOrder() {
        return (false);
    }


    /**
     *  Gets the process search order. 
     *
     *  @return the process search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsProcessSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.ProcessSearchOrder getProcessSearchOrder() {
        throw new org.osid.UnimplementedException("supportsProcessSearchOrder() is false");
    }


    /**
     *  Orders the results by initial steps. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByInitial(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by terminal steps. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTerminal(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by next state. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByNextState(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a next state search order is available. 
     *
     *  @return <code> true </code> if a state search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsNextStateSearchOrder() {
        return (false);
    }


    /**
     *  Gets the next state search order. 
     *
     *  @return the state search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsNextStateSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateSearchOrder getNextStateSearchOrder() {
        throw new org.osid.UnimplementedException("supportsNextStateSearchOrder() is false");
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  stepRecordType a step record type 
     *  @return {@code true} if the stepRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code stepRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type stepRecordType) {
        for (org.osid.workflow.records.StepSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(stepRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  stepRecordType the step record type 
     *  @return the step search order record
     *  @throws org.osid.NullArgumentException
     *          {@code stepRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(stepRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.workflow.records.StepSearchOrderRecord getStepSearchOrderRecord(org.osid.type.Type stepRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.workflow.records.StepSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(stepRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(stepRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this step. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param stepSearchOrderRecord the step search odrer record
     *  @param stepRecordType step record type
     *  @throws org.osid.NullArgumentException
     *          {@code stepRecord} or
     *          {@code stepRecordTypestep} is
     *          {@code null}
     */
            
    protected void addStepRecord(org.osid.workflow.records.StepSearchOrderRecord stepSearchOrderRecord, 
                                 org.osid.type.Type stepRecordType) {

        addRecordType(stepRecordType);
        this.records.add(stepSearchOrderRecord);
        
        return;
    }
}
