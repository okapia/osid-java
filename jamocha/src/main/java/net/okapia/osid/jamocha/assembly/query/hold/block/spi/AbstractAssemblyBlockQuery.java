//
// AbstractAssemblyBlockQuery.java
//
//     A BlockQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.hold.block.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A BlockQuery that stores terms.
 */

public abstract class AbstractAssemblyBlockQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.hold.BlockQuery,
               org.osid.hold.BlockQueryInspector,
               org.osid.hold.BlockSearchOrder {

    private final java.util.Collection<org.osid.hold.records.BlockQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.hold.records.BlockQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.hold.records.BlockSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyBlockQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyBlockQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the issue <code> Id </code> for this query. 
     *
     *  @param  issueId the issue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> issueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchIssueId(org.osid.id.Id issueId, boolean match) {
        getAssembler().addIdTerm(getIssueIdColumn(), issueId, match);
        return;
    }


    /**
     *  Clears the issue <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearIssueIdTerms() {
        getAssembler().clearTerms(getIssueIdColumn());
        return;
    }


    /**
     *  Gets the issue <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getIssueIdTerms() {
        return (getAssembler().getIdTerms(getIssueIdColumn()));
    }


    /**
     *  Gets the IssueId column name.
     *
     * @return the column name
     */

    protected String getIssueIdColumn() {
        return ("issue_id");
    }


    /**
     *  Tests if an <code> IssueQuery </code> is available. 
     *
     *  @return <code> true </code> if an issue query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueQuery() {
        return (false);
    }


    /**
     *  Gets the query for an issue. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the issue query 
     *  @throws org.osid.UnimplementedException <code> supportsIssueQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.IssueQuery getIssueQuery() {
        throw new org.osid.UnimplementedException("supportsIssueQuery() is false");
    }


    /**
     *  Matches blocks that have any issue. 
     *
     *  @param  match <code> true </code> to match blocks with any issue, 
     *          <code> false </code> to match blocks with no issue 
     */

    @OSID @Override
    public void matchAnyIssue(boolean match) {
        getAssembler().addIdWildcardTerm(getIssueColumn(), match);
        return;
    }


    /**
     *  Clears the issue query terms. 
     */

    @OSID @Override
    public void clearIssueTerms() {
        getAssembler().clearTerms(getIssueColumn());
        return;
    }


    /**
     *  Gets the issue query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.hold.IssueQueryInspector[] getIssueTerms() {
        return (new org.osid.hold.IssueQueryInspector[0]);
    }


    /**
     *  Gets the Issue column name.
     *
     * @return the column name
     */

    protected String getIssueColumn() {
        return ("issue");
    }


    /**
     *  Sets the oubliette <code> Id </code> for this query to match blocks 
     *  assigned to foundries. 
     *
     *  @param  oublietteId the oubliette <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchOublietteId(org.osid.id.Id oublietteId, boolean match) {
        getAssembler().addIdTerm(getOublietteIdColumn(), oublietteId, match);
        return;
    }


    /**
     *  Clears the oubliette <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearOublietteIdTerms() {
        getAssembler().clearTerms(getOublietteIdColumn());
        return;
    }


    /**
     *  Gets the oubliette <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOublietteIdTerms() {
        return (getAssembler().getIdTerms(getOublietteIdColumn()));
    }


    /**
     *  Gets the OublietteId column name.
     *
     * @return the column name
     */

    protected String getOublietteIdColumn() {
        return ("oubliette_id");
    }


    /**
     *  Tests if a <code> OublietteQuery </code> is available. 
     *
     *  @return <code> true </code> if an oubliette query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOublietteQuery() {
        return (false);
    }


    /**
     *  Gets the query for an oubliette. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the oubliette query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOublietteQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.OublietteQuery getOublietteQuery() {
        throw new org.osid.UnimplementedException("supportsOublietteQuery() is false");
    }


    /**
     *  Clears the oubliette query terms. 
     */

    @OSID @Override
    public void clearOublietteTerms() {
        getAssembler().clearTerms(getOublietteColumn());
        return;
    }


    /**
     *  Gets the oubliette query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.hold.OublietteQueryInspector[] getOublietteTerms() {
        return (new org.osid.hold.OublietteQueryInspector[0]);
    }


    /**
     *  Gets the Oubliette column name.
     *
     * @return the column name
     */

    protected String getOublietteColumn() {
        return ("oubliette");
    }


    /**
     *  Tests if this block supports the given record
     *  <code>Type</code>.
     *
     *  @param  blockRecordType a block record type 
     *  @return <code>true</code> if the blockRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>blockRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type blockRecordType) {
        for (org.osid.hold.records.BlockQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(blockRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  blockRecordType the block record type 
     *  @return the block query record 
     *  @throws org.osid.NullArgumentException
     *          <code>blockRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(blockRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.hold.records.BlockQueryRecord getBlockQueryRecord(org.osid.type.Type blockRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.hold.records.BlockQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(blockRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(blockRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  blockRecordType the block record type 
     *  @return the block query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>blockRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(blockRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.hold.records.BlockQueryInspectorRecord getBlockQueryInspectorRecord(org.osid.type.Type blockRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.hold.records.BlockQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(blockRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(blockRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param blockRecordType the block record type
     *  @return the block search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>blockRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(blockRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.hold.records.BlockSearchOrderRecord getBlockSearchOrderRecord(org.osid.type.Type blockRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.hold.records.BlockSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(blockRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(blockRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this block. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param blockQueryRecord the block query record
     *  @param blockQueryInspectorRecord the block query inspector
     *         record
     *  @param blockSearchOrderRecord the block search order record
     *  @param blockRecordType block record type
     *  @throws org.osid.NullArgumentException
     *          <code>blockQueryRecord</code>,
     *          <code>blockQueryInspectorRecord</code>,
     *          <code>blockSearchOrderRecord</code> or
     *          <code>blockRecordTypeblock</code> is
     *          <code>null</code>
     */
            
    protected void addBlockRecords(org.osid.hold.records.BlockQueryRecord blockQueryRecord, 
                                      org.osid.hold.records.BlockQueryInspectorRecord blockQueryInspectorRecord, 
                                      org.osid.hold.records.BlockSearchOrderRecord blockSearchOrderRecord, 
                                      org.osid.type.Type blockRecordType) {

        addRecordType(blockRecordType);

        nullarg(blockQueryRecord, "block query record");
        nullarg(blockQueryInspectorRecord, "block query inspector record");
        nullarg(blockSearchOrderRecord, "block search odrer record");

        this.queryRecords.add(blockQueryRecord);
        this.queryInspectorRecords.add(blockQueryInspectorRecord);
        this.searchOrderRecords.add(blockSearchOrderRecord);
        
        return;
    }
}
