//
// AbstractPathLookupSession.java
//
//    A starter implementation framework for providing a Path
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.path.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Path
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getPaths(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractPathLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.mapping.path.PathLookupSession {

    private boolean pedantic  = false;
    private boolean federated = false;
    private org.osid.mapping.Map map = new net.okapia.osid.jamocha.nil.mapping.map.UnknownMap();
    

    /**
     *  Gets the <code>Map/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Map Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getMapId() {
        return (this.map.getId());
    }


    /**
     *  Gets the <code>Map</code> associated with this 
     *  session.
     *
     *  @return the <code>Map</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.Map getMap()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.map);
    }


    /**
     *  Sets the <code>Map</code>.
     *
     *  @param  map the map for this session
     *  @throws org.osid.NullArgumentException <code>map</code>
     *          is <code>null</code>
     */

    protected void setMap(org.osid.mapping.Map map) {
        nullarg(map, "map");
        this.map = map;
        return;
    }


    /**
     *  Tests if this user can perform <code>Path</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupPaths() {
        return (true);
    }


    /**
     *  A complete view of the <code>Path</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativePathView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Path</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryPathView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include paths in maps which are children
     *  of this map in the map hierarchy.
     */

    @OSID @Override
    public void useFederatedMapView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this map only.
     */

    @OSID @Override
    public void useIsolatedMapView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>Path</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Path</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Path</code> and
     *  retained for compatibility.
     *
     *  @param  pathId <code>Id</code> of the
     *          <code>Path</code>
     *  @return the path
     *  @throws org.osid.NotFoundException <code>pathId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>pathId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.Path getPath(org.osid.id.Id pathId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.mapping.path.PathList paths = getPaths()) {
            while (paths.hasNext()) {
                org.osid.mapping.path.Path path = paths.getNextPath();
                if (path.getId().equals(pathId)) {
                    return (path);
                }
            }
        } 

        throw new org.osid.NotFoundException(pathId + " not found");
    }


    /**
     *  Gets a <code>PathList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  paths specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Paths</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getPaths()</code>.
     *
     *  @param  pathIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Path</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>pathIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.PathList getPathsByIds(org.osid.id.IdList pathIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.mapping.path.Path> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = pathIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getPath(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("path " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.mapping.path.path.LinkedPathList(ret));
    }


    /**
     *  Gets a <code>PathList</code> corresponding to the given
     *  path genus <code>Type</code> which does not include
     *  paths of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  paths or an error results. Otherwise, the returned list
     *  may contain only those paths that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getPaths()</code>.
     *
     *  @param  pathGenusType a path genus type 
     *  @return the returned <code>Path</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>pathGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.PathList getPathsByGenusType(org.osid.type.Type pathGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.mapping.path.path.PathGenusFilterList(getPaths(), pathGenusType));
    }


    /**
     *  Gets a <code>PathList</code> corresponding to the given
     *  path genus <code>Type</code> and include any additional
     *  paths with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  paths or an error results. Otherwise, the returned list
     *  may contain only those paths that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getPaths()</code>.
     *
     *  @param  pathGenusType a path genus type 
     *  @return the returned <code>Path</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>pathGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.PathList getPathsByParentGenusType(org.osid.type.Type pathGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getPathsByGenusType(pathGenusType));
    }


    /**
     *  Gets a <code>PathList</code> containing the given
     *  path record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  paths or an error results. Otherwise, the returned list
     *  may contain only those paths that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getPaths()</code>.
     *
     *  @param  pathRecordType a path record type 
     *  @return the returned <code>Path</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>pathRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.PathList getPathsByRecordType(org.osid.type.Type pathRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.mapping.path.path.PathRecordFilterList(getPaths(), pathRecordType));
    }


    /**
     *  Gets a <code>PathList</code> connected to all the given
     *  <code>Locations</code.> In plenary mode, the returned list
     *  contains all of the paths along the locations, or an error
     *  results if a path connected to the location is not found or
     *  inaccessible. Otherwise, inaccessible <code>Paths</code> may
     *  be omitted from the list.
     *
     *  This method should be implemented.
     *
     *  @param  locationIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Path</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>locationIds</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.mapping.path.PathList getPathsAlongLocations(org.osid.id.IdList locationIds)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.nil.mapping.path.path.EmptyPathList());
    }


    /**
     *  Gets all <code>Paths</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  paths or an error results. Otherwise, the returned list
     *  may contain only those paths that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Paths</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.mapping.path.PathList getPaths()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the path list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of paths
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.mapping.path.PathList filterPathsOnViews(org.osid.mapping.path.PathList list)
        throws org.osid.OperationFailedException {

        return (list);
    }
}
