//
// SequenceRuleElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.authoring.sequencerule.spi;

/**
 *  Ids for object elements for use in forms and queries.
 */

public class SequenceRuleElements
    extends net.okapia.osid.jamocha.spi.OsidRuleElements {


    /**
     *  Gets the SequenceRuleElement Id.
     *
     *  @return the sequence rule element Id
     */

    public static org.osid.id.Id getSequenceRuleEntityId() {
        return (makeEntityId("osid.assessment.authoring.SequenceRule"));
    }


    /**
     *  Gets the AssessmentPartId element Id.
     *
     *  @return the AssessmentPartId element Id
     */

    public static org.osid.id.Id getAssessmentPartId() {
        return (makeElementId("osid.assessment.authoring.sequencerule.AssessmentPartId"));
    }


    /**
     *  Gets the AssessmentPart element Id.
     *
     *  @return the AssessmentPart element Id
     */

    public static org.osid.id.Id getAssessmentPart() {
        return (makeElementId("osid.assessment.authoring.sequencerule.AssessmentPart"));
    }


    /**
     *  Gets the NextAssessmentPartId element Id.
     *
     *  @return the NextAssessmentPartId element Id
     */

    public static org.osid.id.Id getNextAssessmentPartId() {
        return (makeElementId("osid.assessment.authoring.sequencerule.NextAssessmentPartId"));
    }


    /**
     *  Gets the NextAssessmentPart element Id.
     *
     *  @return the NextAssessmentPart element Id
     */

    public static org.osid.id.Id getNextAssessmentPart() {
        return (makeElementId("osid.assessment.authoring.sequencerule.NextAssessmentPart"));
    }


    /**
     *  Gets the MinimumScore element Id.
     *
     *  @return the MinimumScore element Id
     */

    public static org.osid.id.Id getMinimumScore() {
        return (makeElementId("osid.assessment.authoring.sequencerule.MinimumScore"));
    }


    /**
     *  Gets the MaximumScore element Id.
     *
     *  @return the MaximumScore element Id
     */

    public static org.osid.id.Id getMaximumScore() {
        return (makeElementId("osid.assessment.authoring.sequencerule.MaximumScore"));
    }


    /**
     *  Gets the AppliedAssessmentPartIds element Id.
     *
     *  @return the AppliedAssessmentPartIds element Id
     */

    public static org.osid.id.Id getAppliedAssessmentPartIds() {
        return (makeElementId("osid.assessment.authoring.sequencerule.AppliedAssessmentPartIds"));
    }


    /**
     *  Gets the AppliedAssessmentParts element Id.
     *
     *  @return the AppliedAssessmentParts element Id
     */

    public static org.osid.id.Id getAppliedAssessmentParts() {
        return (makeElementId("osid.assessment.authoring.sequencerule.AppliedAssessmentParts"));
    }


    /**
     *  Gets the Cumulative element Id.
     *
     *  @return the Cumulative element Id
     */

    public static org.osid.id.Id getCumulative() {
        return (makeElementId("osid.assessment.authoring.sequencerule.Cumulative"));
    }


    /**
     *  Gets the BankId element Id.
     *
     *  @return the BankId element Id
     */

    public static org.osid.id.Id getBankId() {
        return (makeQueryElementId("osid.assessment.authoring.sequencerule.BankId"));
    }


    /**
     *  Gets the Bank element Id.
     *
     *  @return the Bank element Id
     */

    public static org.osid.id.Id getBank() {
        return (makeQueryElementId("osid.assessment.authoring.sequencerule.Bank"));
    }
}
