//
// UnknownWorkflowEvent.java
//
//     Defines an unknown WorkflowEvent.
//
//
// Tom Coppeto
// Okapia
// 8 December 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.nil.workflow.workflowevent;


/**
 *  Defines an unknown <code>WorkflowEvent</code>.
 */

public final class UnknownWorkflowEvent
    extends net.okapia.osid.jamocha.nil.workflow.workflowevent.spi.AbstractUnknownWorkflowEvent
    implements org.osid.workflow.WorkflowEvent {


    /**
     *  Constructs a new <code>UnknownWorkflowEvent</code>.
     */

    public UnknownWorkflowEvent() {
        return;
    }


    /**
     *  Constructs a new <code>UnknownWorkflowEvent</code> with all
     *  the optional methods enabled.
     *
     *  @param optional <code>true</code> to enable the optional
     *         methods
     */

    public UnknownWorkflowEvent(boolean optional) {
        super(optional);
        addWorkflowEventRecord(new WorkflowEventRecord(), net.okapia.osid.jamocha.nil.privateutil.UnknownRecordType.valueOf(OBJECT));
        return;
    }


    /**
     *  Gets an unknown WorkflowEvent.
     *
     *  @return an unknown WorkflowEvent
     */

    public static org.osid.workflow.WorkflowEvent create() {
        return (net.okapia.osid.jamocha.builder.validator.workflow.workflowevent.WorkflowEventValidator.validateWorkflowEvent(new UnknownWorkflowEvent()));
    }


    public class WorkflowEventRecord 
        extends net.okapia.osid.jamocha.spi.AbstractOsidRecord
        implements org.osid.workflow.records.WorkflowEventRecord {

        
        protected WorkflowEventRecord() {
            addRecordType(net.okapia.osid.jamocha.nil.privateutil.UnknownRecordType.valueOf(OBJECT));
            return;
        }
    }        
}
