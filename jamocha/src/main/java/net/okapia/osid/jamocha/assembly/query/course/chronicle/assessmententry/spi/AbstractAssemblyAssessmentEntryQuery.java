//
// AbstractAssemblyAssessmentEntryQuery.java
//
//     An AssessmentEntryQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.course.chronicle.assessmententry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An AssessmentEntryQuery that stores terms.
 */

public abstract class AbstractAssemblyAssessmentEntryQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRelationshipQuery
    implements org.osid.course.chronicle.AssessmentEntryQuery,
               org.osid.course.chronicle.AssessmentEntryQueryInspector,
               org.osid.course.chronicle.AssessmentEntrySearchOrder {

    private final java.util.Collection<org.osid.course.chronicle.records.AssessmentEntryQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.chronicle.records.AssessmentEntryQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.chronicle.records.AssessmentEntrySearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyAssessmentEntryQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyAssessmentEntryQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the student <code> Id </code> for this query. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchStudentId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getStudentIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the student <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearStudentIdTerms() {
        getAssembler().clearTerms(getStudentIdColumn());
        return;
    }


    /**
     *  Gets the resource <code> Id </code> query terms. 
     *
     *  @return the resource <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getStudentIdTerms() {
        return (getAssembler().getIdTerms(getStudentIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the resource. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByStudent(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getStudentColumn(), style);
        return;
    }


    /**
     *  Gets the StudentId column name.
     *
     * @return the column name
     */

    protected String getStudentIdColumn() {
        return ("student_id");
    }


    /**
     *  Tests if a <code> StudentQuery </code> is available. 
     *
     *  @return <code> true </code> if a student query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStudentQuery() {
        return (false);
    }


    /**
     *  Gets the query for a student option. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return a student query 
     *  @throws org.osid.UnimplementedException <code> supportsStudentQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getStudentQuery() {
        throw new org.osid.UnimplementedException("supportsStudentQuery() is false");
    }


    /**
     *  Clears the student option terms. 
     */

    @OSID @Override
    public void clearStudentTerms() {
        getAssembler().clearTerms(getStudentColumn());
        return;
    }


    /**
     *  Gets the student query terms. 
     *
     *  @return the resource query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getStudentTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a resource order is available. 
     *
     *  @return <code> true </code> if a resource order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStudentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStudentSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getStudentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsStudentSearchOrder() is false");
    }


    /**
     *  Gets the Student column name.
     *
     * @return the column name
     */

    protected String getStudentColumn() {
        return ("student");
    }


    /**
     *  Sets the assessment <code> Id </code> for this query to match entries 
     *  that have an entry for the given assessment. 
     *
     *  @param  assessmentId an assessment <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assessmentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAssessmentId(org.osid.id.Id assessmentId, boolean match) {
        getAssembler().addIdTerm(getAssessmentIdColumn(), assessmentId, match);
        return;
    }


    /**
     *  Clears the assessment <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAssessmentIdTerms() {
        getAssembler().clearTerms(getAssessmentIdColumn());
        return;
    }


    /**
     *  Gets the assessment <code> Id </code> query terms. 
     *
     *  @return the assessment <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAssessmentIdTerms() {
        return (getAssembler().getIdTerms(getAssessmentIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the assessment. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAssessment(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getAssessmentColumn(), style);
        return;
    }


    /**
     *  Gets the AssessmentId column name.
     *
     * @return the column name
     */

    protected String getAssessmentIdColumn() {
        return ("assessment_id");
    }


    /**
     *  Tests if an <code> AssessmentQuery </code> is available. 
     *
     *  @return <code> true </code> if an assessment query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentQuery() {
        return (false);
    }


    /**
     *  Gets the query for an assessment entry. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return an assessment query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentQuery getAssessmentQuery() {
        throw new org.osid.UnimplementedException("supportsAssessmentQuery() is false");
    }


    /**
     *  Clears the assessment terms. 
     */

    @OSID @Override
    public void clearAssessmentTerms() {
        getAssembler().clearTerms(getAssessmentColumn());
        return;
    }


    /**
     *  Gets the assessment query terms. 
     *
     *  @return the assessment terms 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentQueryInspector[] getAssessmentTerms() {
        return (new org.osid.assessment.AssessmentQueryInspector[0]);
    }


    /**
     *  Tests if an assessment order is available. 
     *
     *  @return <code> true </code> if an assessment order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the assessment order. 
     *
     *  @return the assessment search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentSearchOrder getAssessmentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsAssessmentSearchOrder() is false");
    }


    /**
     *  Gets the Assessment column name.
     *
     * @return the column name
     */

    protected String getAssessmentColumn() {
        return ("assessment");
    }


    /**
     *  Matches completed dates between the given dates inclusive. 
     *
     *  @param  from starting date 
     *  @param  to ending date 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchDateCompleted(org.osid.calendaring.DateTime from, 
                                   org.osid.calendaring.DateTime to, 
                                   boolean match) {
        getAssembler().addDateTimeRangeTerm(getDateCompletedColumn(), from, to, match);
        return;
    }


    /**
     *  Matches entries that have any completed date. 
     *
     *  @param  match <code> true </code> to match entries with any completed 
     *          date, <code> false </code> to match entries with no completed 
     *          date 
     */

    @OSID @Override
    public void matchAnyDateCompleted(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getDateCompletedColumn(), match);
        return;
    }


    /**
     *  Clears the completed date terms. 
     */

    @OSID @Override
    public void clearDateCompletedTerms() {
        getAssembler().clearTerms(getDateCompletedColumn());
        return;
    }


    /**
     *  Gets the completion date query terms. 
     *
     *  @return the date terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getDateCompletedTerms() {
        return (getAssembler().getDateTimeRangeTerms(getDateCompletedColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the completion 
     *  date. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDateCompleted(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDateCompletedColumn(), style);
        return;
    }


    /**
     *  Gets the DateCompleted column name.
     *
     * @return the column name
     */

    protected String getDateCompletedColumn() {
        return ("date_completed");
    }


    /**
     *  Sets the program <code> Id </code> for this query. 
     *
     *  @param  programId a program <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> programId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchProgramId(org.osid.id.Id programId, boolean match) {
        getAssembler().addIdTerm(getProgramIdColumn(), programId, match);
        return;
    }


    /**
     *  Clears the program <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearProgramIdTerms() {
        getAssembler().clearTerms(getProgramIdColumn());
        return;
    }


    /**
     *  Gets the program <code> Id </code> query terms. 
     *
     *  @return the program <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProgramIdTerms() {
        return (getAssembler().getIdTerms(getProgramIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the program. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByProgram(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getProgramColumn(), style);
        return;
    }


    /**
     *  Gets the ProgramId column name.
     *
     * @return the column name
     */

    protected String getProgramIdColumn() {
        return ("program_id");
    }


    /**
     *  Tests if a <code> ProgramQuery </code> is available. 
     *
     *  @return <code> true </code> if a program query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramQuery() {
        return (false);
    }


    /**
     *  Gets the query for a program. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return a program query 
     *  @throws org.osid.UnimplementedException <code> supportsProgramQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramQuery getProgramQuery() {
        throw new org.osid.UnimplementedException("supportsProgramQuery() is false");
    }


    /**
     *  Matches entries that have any program. 
     *
     *  @param  match <code> true </code> to match entries with any program 
     *          <code> false </code> to match entries with no program 
     */

    @OSID @Override
    public void matchAnyProgram(boolean match) {
        getAssembler().addIdWildcardTerm(getProgramColumn(), match);
        return;
    }


    /**
     *  Clears the program terms. 
     */

    @OSID @Override
    public void clearProgramTerms() {
        getAssembler().clearTerms(getProgramColumn());
        return;
    }


    /**
     *  Gets the program query terms. 
     *
     *  @return the program terms 
     */

    @OSID @Override
    public org.osid.course.program.ProgramQueryInspector[] getProgramTerms() {
        return (new org.osid.course.program.ProgramQueryInspector[0]);
    }


    /**
     *  Tests if a program order is available. 
     *
     *  @return <code> true </code> if a program order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramSearchOrder() {
        return (false);
    }


    /**
     *  Gets the program order. 
     *
     *  @return the program search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramSearchOrder getProgramSearchOrder() {
        throw new org.osid.UnimplementedException("supportsProgramSearchOrder() is false");
    }


    /**
     *  Gets the Program column name.
     *
     * @return the column name
     */

    protected String getProgramColumn() {
        return ("program");
    }


    /**
     *  Sets the course <code> Id </code> for this query. 
     *
     *  @param  courseId a course <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCourseId(org.osid.id.Id courseId, boolean match) {
        getAssembler().addIdTerm(getCourseIdColumn(), courseId, match);
        return;
    }


    /**
     *  Clears the course <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseIdTerms() {
        getAssembler().clearTerms(getCourseIdColumn());
        return;
    }


    /**
     *  Gets the course <code> Id </code> query terms. 
     *
     *  @return the course <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseIdTerms() {
        return (getAssembler().getIdTerms(getCourseIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the course. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCourse(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCourseColumn(), style);
        return;
    }


    /**
     *  Gets the CourseId column name.
     *
     * @return the column name
     */

    protected String getCourseIdColumn() {
        return ("course_id");
    }


    /**
     *  Tests if a <code> CourseQuery </code> is available. 
     *
     *  @return <code> true </code> if a course query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course entry. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return a course query 
     *  @throws org.osid.UnimplementedException <code> supportsCourseQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseQuery getCourseQuery() {
        throw new org.osid.UnimplementedException("supportsCourseQuery() is false");
    }


    /**
     *  Matches entries that have any course. 
     *
     *  @param  match <code> true </code> to match entries with any course, 
     *          <code> false </code> to match entries with no course 
     */

    @OSID @Override
    public void matchAnyCourse(boolean match) {
        getAssembler().addIdWildcardTerm(getCourseColumn(), match);
        return;
    }


    /**
     *  Clears the course terms. 
     */

    @OSID @Override
    public void clearCourseTerms() {
        getAssembler().clearTerms(getCourseColumn());
        return;
    }


    /**
     *  Gets the course query terms. 
     *
     *  @return the course terms 
     */

    @OSID @Override
    public org.osid.course.CourseQueryInspector[] getCourseTerms() {
        return (new org.osid.course.CourseQueryInspector[0]);
    }


    /**
     *  Tests if a course order is available. 
     *
     *  @return <code> true </code> if a course order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseSearchOrder() {
        return (false);
    }


    /**
     *  Gets the course order. 
     *
     *  @return the course search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseSearchOrder getCourseSearchOrder() {
        throw new org.osid.UnimplementedException("supportsCourseSearchOrder() is false");
    }


    /**
     *  Gets the Course column name.
     *
     * @return the column name
     */

    protected String getCourseColumn() {
        return ("course");
    }


    /**
     *  Matches a grade <code> Id. </code> 
     *
     *  @param  gradeId a grade <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGradeId(org.osid.id.Id gradeId, boolean match) {
        getAssembler().addIdTerm(getGradeIdColumn(), gradeId, match);
        return;
    }


    /**
     *  Clears the grade <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGradeIdTerms() {
        getAssembler().clearTerms(getGradeIdColumn());
        return;
    }


    /**
     *  Gets the grade <code> Id </code> query terms. 
     *
     *  @return the grade <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGradeIdTerms() {
        return (getAssembler().getIdTerms(getGradeIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the grade system 
     *  for GPAs. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByGrade(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getGradeColumn(), style);
        return;
    }


    /**
     *  Gets the GradeId column name.
     *
     * @return the column name
     */

    protected String getGradeIdColumn() {
        return ("grade_id");
    }


    /**
     *  Tests if a <code> GradeQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grade. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return a grade system query 
     *  @throws org.osid.UnimplementedException <code> supportsGradeQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeQuery getGradeQuery() {
        throw new org.osid.UnimplementedException("supportsGradeQuery() is false");
    }


    /**
     *  Matches entries that have any grade. 
     *
     *  @param  match <code> true </code> to match entries with any grade, 
     *          <code> false </code> to match entries with no grade 
     */

    @OSID @Override
    public void matchAnyGrade(boolean match) {
        getAssembler().addIdWildcardTerm(getGradeColumn(), match);
        return;
    }


    /**
     *  Clears the grade terms. 
     */

    @OSID @Override
    public void clearGradeTerms() {
        getAssembler().clearTerms(getGradeColumn());
        return;
    }


    /**
     *  Gets the grade query terms. 
     *
     *  @return the grade terms 
     */

    @OSID @Override
    public org.osid.grading.GradeQueryInspector[] getGradeTerms() {
        return (new org.osid.grading.GradeQueryInspector[0]);
    }


    /**
     *  Tests if a grade search order is available. 
     *
     *  @return <code> true </code> if a grade order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeSearchOrder() {
        return (false);
    }


    /**
     *  Gets the grade system search order. 
     *
     *  @return the grade search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSearchOrder getGradeSearchOrder() {
        throw new org.osid.UnimplementedException("supportsGradeSearchOrder() is false");
    }


    /**
     *  Gets the Grade column name.
     *
     * @return the column name
     */

    protected String getGradeColumn() {
        return ("grade");
    }


    /**
     *  Matches a score scale <code> Id. </code> 
     *
     *  @param  gradeSystemId a grade system <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeSystemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchScoreScaleId(org.osid.id.Id gradeSystemId, boolean match) {
        getAssembler().addIdTerm(getScoreScaleIdColumn(), gradeSystemId, match);
        return;
    }


    /**
     *  Clears the grade system <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearScoreScaleIdTerms() {
        getAssembler().clearTerms(getScoreScaleIdColumn());
        return;
    }


    /**
     *  Gets the grade system <code> Id </code> query terms. 
     *
     *  @return the grade system <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getScoreScaleIdTerms() {
        return (getAssembler().getIdTerms(getScoreScaleIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the grade system 
     *  for scores. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByScoreScale(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getScoreScaleColumn(), style);
        return;
    }


    /**
     *  Gets the ScoreScaleId column name.
     *
     * @return the column name
     */

    protected String getScoreScaleIdColumn() {
        return ("score_scale_id");
    }


    /**
     *  Tests if a <code> GradeSystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade system query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScoreScaleQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grade system. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return a grade system query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScoreScaleQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQuery getScoreScaleQuery() {
        throw new org.osid.UnimplementedException("supportsScoreScaleQuery() is false");
    }


    /**
     *  Matches entries that have any score scale. 
     *
     *  @param  match <code> true </code> to match entries with any score 
     *          scale, <code> false </code> to match entries with no score 
     *          scale 
     */

    @OSID @Override
    public void matchAnyScoreScale(boolean match) {
        getAssembler().addIdWildcardTerm(getScoreScaleColumn(), match);
        return;
    }


    /**
     *  Clears the score scale terms. 
     */

    @OSID @Override
    public void clearScoreScaleTerms() {
        getAssembler().clearTerms(getScoreScaleColumn());
        return;
    }


    /**
     *  Gets the grade system query terms. 
     *
     *  @return the grade system terms 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQueryInspector[] getScoreScaleTerms() {
        return (new org.osid.grading.GradeSystemQueryInspector[0]);
    }


    /**
     *  Tests if a grade system order is available. 
     *
     *  @return <code> true </code> if a grade system order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScoreScaleSearchOrder() {
        return (false);
    }


    /**
     *  Gets the grade system order. 
     *
     *  @return the grade system search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScoreScaleSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemSearchOrder getScoreScaleSearchOrder() {
        throw new org.osid.UnimplementedException("supportsScoreScaleSearchOrder() is false");
    }


    /**
     *  Gets the ScoreScale column name.
     *
     * @return the column name
     */

    protected String getScoreScaleColumn() {
        return ("score_scale");
    }


    /**
     *  Matches scores between the given range inclusive. 
     *
     *  @param  from starting value 
     *  @param  to ending value 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchScore(java.math.BigDecimal from, java.math.BigDecimal to, 
                           boolean match) {
        getAssembler().addDecimalRangeTerm(getScoreColumn(), from, to, match);
        return;
    }


    /**
     *  Matches entries that have any score. 
     *
     *  @param  match <code> true </code> to match entries with any score, 
     *          <code> false </code> to match entries with no score 
     */

    @OSID @Override
    public void matchAnyScore(boolean match) {
        getAssembler().addDecimalRangeWildcardTerm(getScoreColumn(), match);
        return;
    }


    /**
     *  Clears the score terms. 
     */

    @OSID @Override
    public void clearScoreTerms() {
        getAssembler().clearTerms(getScoreColumn());
        return;
    }


    /**
     *  Gets the score query terms. 
     *
     *  @return the score query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getScoreTerms() {
        return (getAssembler().getDecimalRangeTerms(getScoreColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the score. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByScore(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getScoreColumn(), style);
        return;
    }


    /**
     *  Gets the Score column name.
     *
     * @return the column name
     */

    protected String getScoreColumn() {
        return ("score");
    }


    /**
     *  Sets the course catalog <code> Id </code> for this query to match 
     *  entries assigned to course catalogs. 
     *
     *  @param  courseCatalogId the course catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseCatalogId(org.osid.id.Id courseCatalogId, 
                                     boolean match) {
        getAssembler().addIdTerm(getCourseCatalogIdColumn(), courseCatalogId, match);
        return;
    }


    /**
     *  Clears the course catalog <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseCatalogIdTerms() {
        getAssembler().clearTerms(getCourseCatalogIdColumn());
        return;
    }


    /**
     *  Gets the course catalog <code> Id </code> query terms. 
     *
     *  @return the course catalog <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseCatalogIdTerms() {
        return (getAssembler().getIdTerms(getCourseCatalogIdColumn()));
    }


    /**
     *  Gets the CourseCatalogId column name.
     *
     * @return the column name
     */

    protected String getCourseCatalogIdColumn() {
        return ("course_catalog_id");
    }


    /**
     *  Tests if a <code> CourseCatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a course catalog query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course catalog. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the course catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQuery getCourseCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsCourseCatalogQuery() is false");
    }


    /**
     *  Clears the course catalog terms. 
     */

    @OSID @Override
    public void clearCourseCatalogTerms() {
        getAssembler().clearTerms(getCourseCatalogColumn());
        return;
    }


    /**
     *  Gets the course catalog query terms. 
     *
     *  @return the course catalog query terms 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQueryInspector[] getCourseCatalogTerms() {
        return (new org.osid.course.CourseCatalogQueryInspector[0]);
    }


    /**
     *  Gets the CourseCatalog column name.
     *
     * @return the column name
     */

    protected String getCourseCatalogColumn() {
        return ("course_catalog");
    }


    /**
     *  Tests if this assessmentEntry supports the given record
     *  <code>Type</code>.
     *
     *  @param  assessmentEntryRecordType an assessment entry record type 
     *  @return <code>true</code> if the assessmentEntryRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentEntryRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type assessmentEntryRecordType) {
        for (org.osid.course.chronicle.records.AssessmentEntryQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(assessmentEntryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  assessmentEntryRecordType the assessment entry record type 
     *  @return the assessment entry query record 
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(assessmentEntryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.chronicle.records.AssessmentEntryQueryRecord getAssessmentEntryQueryRecord(org.osid.type.Type assessmentEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.chronicle.records.AssessmentEntryQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(assessmentEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assessmentEntryRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  assessmentEntryRecordType the assessment entry record type 
     *  @return the assessment entry query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(assessmentEntryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.chronicle.records.AssessmentEntryQueryInspectorRecord getAssessmentEntryQueryInspectorRecord(org.osid.type.Type assessmentEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.chronicle.records.AssessmentEntryQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(assessmentEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assessmentEntryRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param assessmentEntryRecordType the assessment entry record type
     *  @return the assessment entry search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(assessmentEntryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.chronicle.records.AssessmentEntrySearchOrderRecord getAssessmentEntrySearchOrderRecord(org.osid.type.Type assessmentEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.chronicle.records.AssessmentEntrySearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(assessmentEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assessmentEntryRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this assessment entry. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param assessmentEntryQueryRecord the assessment entry query record
     *  @param assessmentEntryQueryInspectorRecord the assessment entry query inspector
     *         record
     *  @param assessmentEntrySearchOrderRecord the assessment entry search order record
     *  @param assessmentEntryRecordType assessment entry record type
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentEntryQueryRecord</code>,
     *          <code>assessmentEntryQueryInspectorRecord</code>,
     *          <code>assessmentEntrySearchOrderRecord</code> or
     *          <code>assessmentEntryRecordTypeassessmentEntry</code> is
     *          <code>null</code>
     */
            
    protected void addAssessmentEntryRecords(org.osid.course.chronicle.records.AssessmentEntryQueryRecord assessmentEntryQueryRecord, 
                                      org.osid.course.chronicle.records.AssessmentEntryQueryInspectorRecord assessmentEntryQueryInspectorRecord, 
                                      org.osid.course.chronicle.records.AssessmentEntrySearchOrderRecord assessmentEntrySearchOrderRecord, 
                                      org.osid.type.Type assessmentEntryRecordType) {

        addRecordType(assessmentEntryRecordType);

        nullarg(assessmentEntryQueryRecord, "assessment entry query record");
        nullarg(assessmentEntryQueryInspectorRecord, "assessment entry query inspector record");
        nullarg(assessmentEntrySearchOrderRecord, "assessment entry search odrer record");

        this.queryRecords.add(assessmentEntryQueryRecord);
        this.queryInspectorRecords.add(assessmentEntryQueryInspectorRecord);
        this.searchOrderRecords.add(assessmentEntrySearchOrderRecord);
        
        return;
    }
}
