//
// FederatingReceiptLookupSession.java
//
//     A federating adapter for a ReceiptLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 January 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.messaging;

import org.osid.binding.java.annotation.OSID;


/**
 *  A federating adapter for a ReceiptLookupSession. Sessions are
 *  added to this session through <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code< is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all
 *  sessions. The federating adapter always uses a comparative
 *  view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public final class FederatingReceiptLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.messaging.spi.AbstractFederatingReceiptLookupSession
    implements org.osid.messaging.ReceiptLookupSession {


    /**
     *  Constructs a new <code>FederatingReceiptLookupSession</code>
     *  in serial and all mode.
     *
     *  @param mailbox the mailbox for this session
     *  @throws org.osid.NullArgumentException <code>mailbox</code>
     *          is <code>null</code>
     */

    public FederatingReceiptLookupSession(org.osid.messaging.Mailbox mailbox) {
        setMailbox(mailbox);
        setParallel(true);
        selectAll();
        return;
    }


    /**
     *  Constructs a new <code>FederatingReceiptLookupSession</code>.
     *
     *  @param mailbox the mailbox for this session
     *  @param parallel <code>true</code> to mix the returns from all
     *         sessions, <code>false</code to return results in order
     *         of the sessions
     *  @param all <code>true</code> to merge results from all
     *         providers, <code>false</code> to return the results
     *         from the first session to have them
     *  @throws org.osid.NullArgumentException <code>mailbox</code>
     *          is <code>null</code>
     */

    public FederatingReceiptLookupSession(org.osid.messaging.Mailbox mailbox, 
                                           boolean parallel, boolean all) {
        setMailbox(mailbox);
        setParallel(parallel);

        if (all) {
            selectAll();
        } else {
            selectFirst();
        }

        return;
    }


    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */
    
    @Override
    public void addSession(org.osid.messaging.ReceiptLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Removes a session from this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */
    
    @Override
    public void removeSession(org.osid.messaging.ReceiptLookupSession session) {
        super.removeSession(session);
        return;
    }
}
