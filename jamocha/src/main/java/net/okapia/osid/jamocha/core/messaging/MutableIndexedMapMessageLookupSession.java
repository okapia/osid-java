//
// MutableIndexedMapMessageLookupSession
//
//    Implements a Message lookup service backed by a collection of
//    messages indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.messaging;


/**
 *  Implements a Message lookup service backed by a collection of
 *  messages. The messages are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some messages may be compatible
 *  with more types than are indicated through these message
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of messages can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapMessageLookupSession
    extends net.okapia.osid.jamocha.core.messaging.spi.AbstractIndexedMapMessageLookupSession
    implements org.osid.messaging.MessageLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapMessageLookupSession} with no messages.
     *
     *  @param mailbox the mailbox
     *  @throws org.osid.NullArgumentException {@code mailbox}
     *          is {@code null}
     */

      public MutableIndexedMapMessageLookupSession(org.osid.messaging.Mailbox mailbox) {
        setMailbox(mailbox);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapMessageLookupSession} with a
     *  single message.
     *  
     *  @param mailbox the mailbox
     *  @param  message a single message
     *  @throws org.osid.NullArgumentException {@code mailbox} or
     *          {@code message} is {@code null}
     */

    public MutableIndexedMapMessageLookupSession(org.osid.messaging.Mailbox mailbox,
                                                  org.osid.messaging.Message message) {
        this(mailbox);
        putMessage(message);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapMessageLookupSession} using an
     *  array of messages.
     *
     *  @param mailbox the mailbox
     *  @param  messages an array of messages
     *  @throws org.osid.NullArgumentException {@code mailbox} or
     *          {@code messages} is {@code null}
     */

    public MutableIndexedMapMessageLookupSession(org.osid.messaging.Mailbox mailbox,
                                                  org.osid.messaging.Message[] messages) {
        this(mailbox);
        putMessages(messages);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapMessageLookupSession} using a
     *  collection of messages.
     *
     *  @param mailbox the mailbox
     *  @param  messages a collection of messages
     *  @throws org.osid.NullArgumentException {@code mailbox} or
     *          {@code messages} is {@code null}
     */

    public MutableIndexedMapMessageLookupSession(org.osid.messaging.Mailbox mailbox,
                                                  java.util.Collection<? extends org.osid.messaging.Message> messages) {

        this(mailbox);
        putMessages(messages);
        return;
    }
    

    /**
     *  Makes a {@code Message} available in this session.
     *
     *  @param  message a message
     *  @throws org.osid.NullArgumentException {@code message{@code  is
     *          {@code null}
     */

    @Override
    public void putMessage(org.osid.messaging.Message message) {
        super.putMessage(message);
        return;
    }


    /**
     *  Makes an array of messages available in this session.
     *
     *  @param  messages an array of messages
     *  @throws org.osid.NullArgumentException {@code messages{@code 
     *          is {@code null}
     */

    @Override
    public void putMessages(org.osid.messaging.Message[] messages) {
        super.putMessages(messages);
        return;
    }


    /**
     *  Makes collection of messages available in this session.
     *
     *  @param  messages a collection of messages
     *  @throws org.osid.NullArgumentException {@code message{@code  is
     *          {@code null}
     */

    @Override
    public void putMessages(java.util.Collection<? extends org.osid.messaging.Message> messages) {
        super.putMessages(messages);
        return;
    }


    /**
     *  Removes a Message from this session.
     *
     *  @param messageId the {@code Id} of the message
     *  @throws org.osid.NullArgumentException {@code messageId{@code  is
     *          {@code null}
     */

    @Override
    public void removeMessage(org.osid.id.Id messageId) {
        super.removeMessage(messageId);
        return;
    }    
}
