//
// UnknownAssessmentEntry.java
//
//     Defines an unknown AssessmentEntry.
//
//
// Tom Coppeto
// Okapia
// 8 December 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.nil.course.chronicle.assessmententry;


/**
 *  Defines an unknown <code>AssessmentEntry</code>.
 */

public final class UnknownAssessmentEntry
    extends net.okapia.osid.jamocha.nil.course.chronicle.assessmententry.spi.AbstractUnknownAssessmentEntry
    implements org.osid.course.chronicle.AssessmentEntry {


    /**
     *  Constructs a new <code>UnknownAssessmentEntry</code>.
     */

    public UnknownAssessmentEntry() {
        return;
    }


    /**
     *  Constructs a new <code>UnknownAssessmentEntry</code> with all
     *  the optional methods enabled.
     *
     *  @param optional <code>true</code> to enable the optional
     *         methods
     */

    public UnknownAssessmentEntry(boolean optional) {
        super(optional);
        addAssessmentEntryRecord(new AssessmentEntryRecord(), net.okapia.osid.jamocha.nil.privateutil.UnknownRecordType.valueOf(OBJECT));
        return;
    }


    /**
     *  Gets an unknown AssessmentEntry.
     *
     *  @return an unknown AssessmentEntry
     */

    public static org.osid.course.chronicle.AssessmentEntry create() {
        return (net.okapia.osid.jamocha.builder.validator.course.chronicle.assessmententry.AssessmentEntryValidator.validateAssessmentEntry(new UnknownAssessmentEntry()));
    }


    public class AssessmentEntryRecord 
        extends net.okapia.osid.jamocha.spi.AbstractOsidRecord
        implements org.osid.course.chronicle.records.AssessmentEntryRecord {

        
        protected AssessmentEntryRecord() {
            addRecordType(net.okapia.osid.jamocha.nil.privateutil.UnknownRecordType.valueOf(OBJECT));
            return;
        }
    }        
}
