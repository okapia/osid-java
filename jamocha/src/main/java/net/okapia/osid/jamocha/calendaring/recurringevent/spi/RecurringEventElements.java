//
// RecurringEventElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.recurringevent.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class RecurringEventElements
    extends net.okapia.osid.jamocha.spi.ContainableOsidObjectElements {


    /**
     *  Gets the RecurringEventElement Id.
     *
     *  @return the recurring event element Id
     */

    public static org.osid.id.Id getRecurringEventEntityId() {
        return (makeEntityId("osid.calendaring.RecurringEvent"));
    }


    /**
     *  Gets the ScheduleIds element Id.
     *
     *  @return the ScheduleIds element Id
     */

    public static org.osid.id.Id getScheduleIds() {
        return (makeElementId("osid.calendaring.recurringevent.ScheduleIds"));
    }


    /**
     *  Gets the Schedules element Id.
     *
     *  @return the Schedules element Id
     */

    public static org.osid.id.Id getSchedules() {
        return (makeElementId("osid.calendaring.recurringevent.Schedules"));
    }


    /**
     *  Gets the SupersedingEventIds element Id.
     *
     *  @return the SupersedingEventIds element Id
     */

    public static org.osid.id.Id getSupersedingEventIds() {
        return (makeElementId("osid.calendaring.recurringevent.SupersedingEventIds"));
    }


    /**
     *  Gets the SupersedingEvents element Id.
     *
     *  @return the SupersedingEvents element Id
     */

    public static org.osid.id.Id getSupersedingEvents() {
        return (makeElementId("osid.calendaring.recurringevent.SupersedingEvents"));
    }


    /**
     *  Gets the SpecificMeetingTimes element Id.
     *
     *  @return the SpecificMeetingTimes element Id
     */

    public static org.osid.id.Id getSpecificMeetingTimes() {
        return (makeElementId("osid.calendaring.recurringevent.SpecificMeetingTimes"));
    }


    /**
     *  Gets the EventIds element Id.
     *
     *  @return the EventIds element Id
     */

    public static org.osid.id.Id getEventIds() {
        return (makeElementId("osid.calendaring.recurringevent.EventIds"));
    }


    /**
     *  Gets the Events element Id.
     *
     *  @return the Events element Id
     */

    public static org.osid.id.Id getEvents() {
        return (makeElementId("osid.calendaring.recurringevent.Events"));
    }


    /**
     *  Gets the Blackouts element Id.
     *
     *  @return the Blackouts element Id
     */

    public static org.osid.id.Id getBlackouts() {
        return (makeElementId("osid.calendaring.recurringevent.Blackouts"));
    }


    /**
     *  Gets the SponsorIds element Id.
     *
     *  @return the SponsorIds element Id
     */

    public static org.osid.id.Id getSponsorIds() {
        return (makeElementId("osid.calendaring.recurringevent.SponsorIds"));
    }


    /**
     *  Gets the Sponsors element Id.
     *
     *  @return the Sponsors element Id
     */

    public static org.osid.id.Id getSponsors() {
        return (makeElementId("osid.calendaring.recurringevent.Sponsors"));
    }


    /**
     *  Gets the BlackoutInclusive element Id.
     *
     *  @return the BlackoutInclusive element Id
     */

    public static org.osid.id.Id getBlackoutInclusive() {
        return (makeQueryElementId("osid.calendaring.recurringevent.BlackoutInclusive"));
    }


    /**
     *  Gets the CalendarId element Id.
     *
     *  @return the CalendarId element Id
     */

    public static org.osid.id.Id getCalendarId() {
        return (makeQueryElementId("osid.calendaring.recurringevent.CalendarId"));
    }


    /**
     *  Gets the Calendar element Id.
     *
     *  @return the Calendar element Id
     */

    public static org.osid.id.Id getCalendar() {
        return (makeQueryElementId("osid.calendaring.recurringevent.Calendar"));
    }
}
