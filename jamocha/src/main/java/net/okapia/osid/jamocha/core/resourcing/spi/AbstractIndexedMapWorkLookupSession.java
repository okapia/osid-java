//
// AbstractIndexedMapWorkLookupSession.java
//
//    A simple framework for providing a Work lookup service
//    backed by a fixed collection of works with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Work lookup service backed by a
 *  fixed collection of works. The works are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some works may be compatible
 *  with more types than are indicated through these work
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Works</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapWorkLookupSession
    extends AbstractMapWorkLookupSession
    implements org.osid.resourcing.WorkLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.resourcing.Work> worksByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.resourcing.Work>());
    private final MultiMap<org.osid.type.Type, org.osid.resourcing.Work> worksByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.resourcing.Work>());


    /**
     *  Makes a <code>Work</code> available in this session.
     *
     *  @param  work a work
     *  @throws org.osid.NullArgumentException <code>work<code> is
     *          <code>null</code>
     */

    @Override
    protected void putWork(org.osid.resourcing.Work work) {
        super.putWork(work);

        this.worksByGenus.put(work.getGenusType(), work);
        
        try (org.osid.type.TypeList types = work.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.worksByRecord.put(types.getNextType(), work);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a work from this session.
     *
     *  @param workId the <code>Id</code> of the work
     *  @throws org.osid.NullArgumentException <code>workId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeWork(org.osid.id.Id workId) {
        org.osid.resourcing.Work work;
        try {
            work = getWork(workId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.worksByGenus.remove(work.getGenusType());

        try (org.osid.type.TypeList types = work.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.worksByRecord.remove(types.getNextType(), work);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeWork(workId);
        return;
    }


    /**
     *  Gets a <code>WorkList</code> corresponding to the given
     *  work genus <code>Type</code> which does not include
     *  works of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known works or an error results. Otherwise,
     *  the returned list may contain only those works that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  workGenusType a work genus type 
     *  @return the returned <code>Work</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>workGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.WorkList getWorksByGenusType(org.osid.type.Type workGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.resourcing.work.ArrayWorkList(this.worksByGenus.get(workGenusType)));
    }


    /**
     *  Gets a <code>WorkList</code> containing the given
     *  work record <code>Type</code>. In plenary mode, the
     *  returned list contains all known works or an error
     *  results. Otherwise, the returned list may contain only those
     *  works that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  workRecordType a work record type 
     *  @return the returned <code>work</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>workRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.WorkList getWorksByRecordType(org.osid.type.Type workRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.resourcing.work.ArrayWorkList(this.worksByRecord.get(workRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.worksByGenus.clear();
        this.worksByRecord.clear();

        super.close();

        return;
    }
}
