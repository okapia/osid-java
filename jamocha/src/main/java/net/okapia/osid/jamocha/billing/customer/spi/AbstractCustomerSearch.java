//
// AbstractCustomerSearch.java
//
//     A template for making a Customer Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.billing.customer.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing customer searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractCustomerSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.billing.CustomerSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.billing.records.CustomerSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.billing.CustomerSearchOrder customerSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of customers. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  customerIds list of customers
     *  @throws org.osid.NullArgumentException
     *          <code>customerIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongCustomers(org.osid.id.IdList customerIds) {
        while (customerIds.hasNext()) {
            try {
                this.ids.add(customerIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongCustomers</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of customer Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getCustomerIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  customerSearchOrder customer search order 
     *  @throws org.osid.NullArgumentException
     *          <code>customerSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>customerSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderCustomerResults(org.osid.billing.CustomerSearchOrder customerSearchOrder) {
	this.customerSearchOrder = customerSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.billing.CustomerSearchOrder getCustomerSearchOrder() {
	return (this.customerSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given customer search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a customer implementing the requested record.
     *
     *  @param customerSearchRecordType a customer search record
     *         type
     *  @return the customer search record
     *  @throws org.osid.NullArgumentException
     *          <code>customerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(customerSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.records.CustomerSearchRecord getCustomerSearchRecord(org.osid.type.Type customerSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.billing.records.CustomerSearchRecord record : this.records) {
            if (record.implementsRecordType(customerSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(customerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this customer search. 
     *
     *  @param customerSearchRecord customer search record
     *  @param customerSearchRecordType customer search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCustomerSearchRecord(org.osid.billing.records.CustomerSearchRecord customerSearchRecord, 
                                           org.osid.type.Type customerSearchRecordType) {

        addRecordType(customerSearchRecordType);
        this.records.add(customerSearchRecord);        
        return;
    }
}
