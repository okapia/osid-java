//
// AbstractNodeCampusHierarchySession.java
//
//     Defines a Campus hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.room.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a campus hierarchy session for delivering a hierarchy
 *  of campuses using the CampusNode interface.
 */

public abstract class AbstractNodeCampusHierarchySession
    extends net.okapia.osid.jamocha.room.spi.AbstractCampusHierarchySession
    implements org.osid.room.CampusHierarchySession {

    private java.util.Collection<org.osid.room.CampusNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root campus <code> Ids </code> in this hierarchy.
     *
     *  @return the root campus <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootCampusIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.room.campusnode.CampusNodeToIdList(this.roots));
    }


    /**
     *  Gets the root campuses in the campus hierarchy. A node
     *  with no parents is an orphan. While all campus <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root campuses 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.CampusList getRootCampuses()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.room.campusnode.CampusNodeToCampusList(new net.okapia.osid.jamocha.room.campusnode.ArrayCampusNodeList(this.roots)));
    }


    /**
     *  Adds a root campus node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootCampus(org.osid.room.CampusNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root campus nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootCampuses(java.util.Collection<org.osid.room.CampusNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root campus node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootCampus(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.room.CampusNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Campus </code> has any parents. 
     *
     *  @param  campusId a campus <code> Id </code> 
     *  @return <code> true </code> if the campus has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> campusId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> campusId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentCampuses(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getCampusNode(campusId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of a
     *  campus.
     *
     *  @param  id an <code> Id </code> 
     *  @param  campusId the <code> Id </code> of a campus 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> campusId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> campusId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> campusId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfCampus(org.osid.id.Id id, org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.room.CampusNodeList parents = getCampusNode(campusId).getParentCampusNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextCampusNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given campus. 
     *
     *  @param  campusId a campus <code> Id </code> 
     *  @return the parent <code> Ids </code> of the campus 
     *  @throws org.osid.NotFoundException <code> campusId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> campusId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentCampusIds(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.room.campus.CampusToIdList(getParentCampuses(campusId)));
    }


    /**
     *  Gets the parents of the given campus. 
     *
     *  @param  campusId the <code> Id </code> to query 
     *  @return the parents of the campus 
     *  @throws org.osid.NotFoundException <code> campusId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> campusId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.CampusList getParentCampuses(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.room.campusnode.CampusNodeToCampusList(getCampusNode(campusId).getParentCampusNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of a
     *  campus.
     *
     *  @param  id an <code> Id </code> 
     *  @param  campusId the Id of a campus 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> campusId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> campusId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> campusId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfCampus(org.osid.id.Id id, org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfCampus(id, campusId)) {
            return (true);
        }

        try (org.osid.room.CampusList parents = getParentCampuses(campusId)) {
            while (parents.hasNext()) {
                if (isAncestorOfCampus(id, parents.getNextCampus().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if a campus has any children. 
     *
     *  @param  campusId a campus <code> Id </code> 
     *  @return <code> true </code> if the <code> campusId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> campusId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> campusId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildCampuses(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getCampusNode(campusId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of a
     *  campus.
     *
     *  @param  id an <code> Id </code> 
     *  @param campusId the <code> Id </code> of a 
     *         campus
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> campusId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> campusId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> campusId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfCampus(org.osid.id.Id id, org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfCampus(campusId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  campus.
     *
     *  @param  campusId the <code> Id </code> to query 
     *  @return the children of the campus 
     *  @throws org.osid.NotFoundException <code> campusId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> campusId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildCampusIds(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.room.campus.CampusToIdList(getChildCampuses(campusId)));
    }


    /**
     *  Gets the children of the given campus. 
     *
     *  @param  campusId the <code> Id </code> to query 
     *  @return the children of the campus 
     *  @throws org.osid.NotFoundException <code> campusId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> campusId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.CampusList getChildCampuses(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.room.campusnode.CampusNodeToCampusList(getCampusNode(campusId).getChildCampusNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of a
     *  campus.
     *
     *  @param  id an <code> Id </code> 
     *  @param campusId the <code> Id </code> of a 
     *         campus
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> campusId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> campusId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> campusId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfCampus(org.osid.id.Id id, org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfCampus(campusId, id)) {
            return (true);
        }

        try (org.osid.room.CampusList children = getChildCampuses(campusId)) {
            while (children.hasNext()) {
                if (isDescendantOfCampus(id, children.getNextCampus().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  campus.
     *
     *  @param  campusId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified campus node 
     *  @throws org.osid.NotFoundException <code> campusId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> campusId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getCampusNodeIds(org.osid.id.Id campusId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.room.campusnode.CampusNodeToNode(getCampusNode(campusId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given campus.
     *
     *  @param  campusId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified campus node 
     *  @throws org.osid.NotFoundException <code> campusId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> campusId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.CampusNode getCampusNodes(org.osid.id.Id campusId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getCampusNode(campusId));
    }


    /**
     *  Closes this <code>CampusHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets a campus node.
     *
     *  @param campusId the id of the campus node
     *  @throws org.osid.NotFoundException <code>campusId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>campusId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.room.CampusNode getCampusNode(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(campusId, "campus Id");
        for (org.osid.room.CampusNode campus : this.roots) {
            if (campus.getId().equals(campusId)) {
                return (campus);
            }

            org.osid.room.CampusNode r = findCampus(campus, campusId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(campusId + " is not found");
    }


    protected org.osid.room.CampusNode findCampus(org.osid.room.CampusNode node, 
                                                  org.osid.id.Id campusId)
	throws org.osid.OperationFailedException {

        try (org.osid.room.CampusNodeList children = node.getChildCampusNodes()) {
            while (children.hasNext()) {
                org.osid.room.CampusNode campus = children.getNextCampusNode();
                if (campus.getId().equals(campusId)) {
                    return (campus);
                }
                
                campus = findCampus(campus, campusId);
                if (campus != null) {
                    return (campus);
                }
            }
        }

        return (null);
    }
}
