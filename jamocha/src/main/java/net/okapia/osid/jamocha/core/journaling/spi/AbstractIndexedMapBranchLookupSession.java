//
// AbstractIndexedMapBranchLookupSession.java
//
//    A simple framework for providing a Branch lookup service
//    backed by a fixed collection of branches with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.journaling.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Branch lookup service backed by a
 *  fixed collection of branches. The branches are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some branches may be compatible
 *  with more types than are indicated through these branch
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Branches</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapBranchLookupSession
    extends AbstractMapBranchLookupSession
    implements org.osid.journaling.BranchLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.journaling.Branch> branchesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.journaling.Branch>());
    private final MultiMap<org.osid.type.Type, org.osid.journaling.Branch> branchesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.journaling.Branch>());


    /**
     *  Makes a <code>Branch</code> available in this session.
     *
     *  @param  branch a branch
     *  @throws org.osid.NullArgumentException <code>branch<code> is
     *          <code>null</code>
     */

    @Override
    protected void putBranch(org.osid.journaling.Branch branch) {
        super.putBranch(branch);

        this.branchesByGenus.put(branch.getGenusType(), branch);
        
        try (org.osid.type.TypeList types = branch.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.branchesByRecord.put(types.getNextType(), branch);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a branch from this session.
     *
     *  @param branchId the <code>Id</code> of the branch
     *  @throws org.osid.NullArgumentException <code>branchId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeBranch(org.osid.id.Id branchId) {
        org.osid.journaling.Branch branch;
        try {
            branch = getBranch(branchId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.branchesByGenus.remove(branch.getGenusType());

        try (org.osid.type.TypeList types = branch.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.branchesByRecord.remove(types.getNextType(), branch);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeBranch(branchId);
        return;
    }


    /**
     *  Gets a <code>BranchList</code> corresponding to the given
     *  branch genus <code>Type</code> which does not include
     *  branches of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known branches or an error results. Otherwise,
     *  the returned list may contain only those branches that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  branchGenusType a branch genus type 
     *  @return the returned <code>Branch</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>branchGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.BranchList getBranchesByGenusType(org.osid.type.Type branchGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.journaling.branch.ArrayBranchList(this.branchesByGenus.get(branchGenusType)));
    }


    /**
     *  Gets a <code>BranchList</code> containing the given
     *  branch record <code>Type</code>. In plenary mode, the
     *  returned list contains all known branches or an error
     *  results. Otherwise, the returned list may contain only those
     *  branches that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  branchRecordType a branch record type 
     *  @return the returned <code>branch</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>branchRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.BranchList getBranchesByRecordType(org.osid.type.Type branchRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.journaling.branch.ArrayBranchList(this.branchesByRecord.get(branchRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.branchesByGenus.clear();
        this.branchesByRecord.clear();

        super.close();

        return;
    }
}
