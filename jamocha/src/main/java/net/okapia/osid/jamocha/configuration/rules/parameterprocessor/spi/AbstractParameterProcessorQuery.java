//
// AbstractParameterProcessorQuery.java
//
//     A template for making a ParameterProcessor Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.configuration.rules.parameterprocessor.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for parameter processors.
 */

public abstract class AbstractParameterProcessorQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidProcessorQuery
    implements org.osid.configuration.rules.ParameterProcessorQuery {

    private final java.util.Collection<org.osid.configuration.rules.records.ParameterProcessorQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches mapped to the parameter. 
     *
     *  @param  parameterId the parameter <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> parameterId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledParameterId(org.osid.id.Id parameterId, 
                                      boolean match) {
        return;
    }


    /**
     *  Clears the parameter <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledParameterIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ParameterQuery </code> is available. 
     *
     *  @return <code> true </code> if a parameter query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledParameterQuery() {
        return (false);
    }


    /**
     *  Gets the query for a parameter. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the parameter query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledParameterQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ParameterQuery getRuledParameterQuery() {
        throw new org.osid.UnimplementedException("supportsRuledParameterQuery() is false");
    }


    /**
     *  Matches mapped to any parameter. 
     *
     *  @param  match <code> true </code> for mapped to any parameter, <code> 
     *          false </code> to match mapped to no parameters 
     */

    @OSID @Override
    public void matchAnyRuledParameter(boolean match) {
        return;
    }


    /**
     *  Clears the parameter query terms. 
     */

    @OSID @Override
    public void clearRuledParameterTerms() {
        return;
    }


    /**
     *  Matches mapped to the configuration. 
     *
     *  @param  configurationId the configuration <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchConfigurationId(org.osid.id.Id configurationId, 
                                     boolean match) {
        return;
    }


    /**
     *  Clears the configuration <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearConfigurationIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ConfigurationQuery </code> is available. 
     *
     *  @return <code> true </code> if a configuration query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConfigurationQuery() {
        return (false);
    }


    /**
     *  Gets the query for a configuration. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the configuration query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConfigurationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationQuery getConfigurationQuery() {
        throw new org.osid.UnimplementedException("supportsConfigurationQuery() is false");
    }


    /**
     *  Clears the configuration query terms. 
     */

    @OSID @Override
    public void clearConfigurationTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given parameter processor query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a parameter processor implementing the requested record.
     *
     *  @param parameterProcessorRecordType a parameter processor record type
     *  @return the parameter processor query record
     *  @throws org.osid.NullArgumentException
     *          <code>parameterProcessorRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(parameterProcessorRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.configuration.rules.records.ParameterProcessorQueryRecord getParameterProcessorQueryRecord(org.osid.type.Type parameterProcessorRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.configuration.rules.records.ParameterProcessorQueryRecord record : this.records) {
            if (record.implementsRecordType(parameterProcessorRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(parameterProcessorRecordType + " is not supported");
    }


    /**
     *  Adds a record to this parameter processor query. 
     *
     *  @param parameterProcessorQueryRecord parameter processor query record
     *  @param parameterProcessorRecordType parameterProcessor record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addParameterProcessorQueryRecord(org.osid.configuration.rules.records.ParameterProcessorQueryRecord parameterProcessorQueryRecord, 
                                          org.osid.type.Type parameterProcessorRecordType) {

        addRecordType(parameterProcessorRecordType);
        nullarg(parameterProcessorQueryRecord, "parameter processor query record");
        this.records.add(parameterProcessorQueryRecord);        
        return;
    }
}
