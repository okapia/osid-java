//
// AbstractMutableCheckResult.java
//
//     Defines a mutable CheckResult.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.rules.check.checkresult.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>CheckResult</code>.
 */

public abstract class AbstractMutableCheckResult
    extends net.okapia.osid.jamocha.rules.check.checkresult.spi.AbstractCheckResult
    implements org.osid.rules.check.CheckResult,
               net.okapia.osid.jamocha.builder.rules.check.checkresult.CheckResultMiter {


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this check result. 
     *
     *  @param record check result record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addCheckResultRecord(org.osid.rules.check.records.CheckResultRecord record, org.osid.type.Type recordType) {
        super.addCheckResultRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Sets the instruction.
     *
     *  @param instruction an instruction
     *  @throws org.osid.NullArgumentException
     *          <code>instruction</code> is <code>null</code>
     */

    @Override
    public void setInstruction(org.osid.rules.check.Instruction instruction) {
        super.setInstruction(instruction);
        return;
    }


    /**
     *  Sets the failed flag.
     *
     *  @param failed <code> true </code> if this check failed, <code> false
     *         </code> otherwise
     */

    @Override
    public void setFailed(boolean failed) {
        super.setFailed(failed);
        return;
    }


    /**
     *  Sets the warning flag.
     *
     *  @param warning <code> true </code> if this check is a warning,
     *         <code> false </code> otherwise
     */

    @Override
    public void setWarning(boolean warning) {
        super.setWarning(warning);
        return;
    }


    /**
     *  Sets the message.
     *
     *  @param message a message
     *  @throws org.osid.NullArgumentException
     *          <code>message</code> is <code>null</code>
     */

    @Override
    public void setMessage(org.osid.locale.DisplayText message) {
        super.setMessage(message);
        return;
    }
}

