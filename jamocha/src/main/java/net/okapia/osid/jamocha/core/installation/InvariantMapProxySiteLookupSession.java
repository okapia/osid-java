//
// InvariantMapProxySiteLookupSession
//
//    Implements a Site lookup service backed by a fixed
//    collection of sites. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.installation;


/**
 *  Implements a Site lookup service backed by a fixed
 *  collection of sites. The sites are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxySiteLookupSession
    extends net.okapia.osid.jamocha.core.installation.spi.AbstractMapSiteLookupSession
    implements org.osid.installation.SiteLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxySiteLookupSession} with no
     *  sites.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public InvariantMapProxySiteLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxySiteLookupSession} with a
     *  single site.
     *
     *  @param site a single site
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code site} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxySiteLookupSession(org.osid.installation.Site site, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putSite(site);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxySiteLookupSession} using
     *  an array of sites.
     *
     *  @param sites an array of sites
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code sites} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxySiteLookupSession(org.osid.installation.Site[] sites, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putSites(sites);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxySiteLookupSession} using a
     *  collection of sites.
     *
     *  @param sites a collection of sites
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code sites} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxySiteLookupSession(java.util.Collection<? extends org.osid.installation.Site> sites,
                                                  org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putSites(sites);
        return;
    }
}
