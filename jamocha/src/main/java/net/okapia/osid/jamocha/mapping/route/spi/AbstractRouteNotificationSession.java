//
// AbstractRouteNotificationSession.java
//
//     A template for making RouteNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.route.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code Route} objects. This session is intended
 *  for consumers needing to synchronize their state with this service
 *  without the use of polling. Notifications are cancelled when this
 *  session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code Route} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for route entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractRouteNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.mapping.route.RouteNotificationSession {

    private boolean federated = false;
    private org.osid.mapping.Map map = new net.okapia.osid.jamocha.nil.mapping.map.UnknownMap();


    /**
     *  Gets the {@code Map/code> {@code Id} associated with this
     *  session.
     *
     *  @return the {@code Map Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getMapId() {
        return (this.map.getId());
    }

    
    /**
     *  Gets the {@code Map} associated with this session.
     *
     *  @return the {@code Map} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.Map getMap()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.map);
    }


    /**
     *  Sets the {@code Map}.
     *
     *  @param map the map for this session
     *  @throws org.osid.NullArgumentException {@code map}
     *          is {@code null}
     */

    protected void setMap(org.osid.mapping.Map map) {
        nullarg(map, "map");
        this.map = map;
        return;
    }


    /**
     *  Tests if this user can register for {@code Route}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForRouteNotifications() {
        return (true);
    }


    /**
     *  Reliable notifications are desired. In reliable mode,
     *  notifications are to be acknowledged using <code>
     *  acknowledgeRouteNotification() </code>.
     */

    @OSID @Override
    public void reliableRouteNotifications() {
        return;
    }


    /**
     *  Unreliable notifications are desired. In unreliable mode,
     *  notifications do not need to be acknowledged.
     */

    @OSID @Override
    public void unreliableRouteNotifications() {
        return;
    }


    /**
     *  Acknowledge a route notification.
     *
     *  @param  notificationId the <code> Id </code> of the notification
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void acknowledgeRouteNotification(org.osid.id.Id notificationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include routes in maps which are children of this
     *  map in the map hierarchy.
     */

    @OSID @Override
    public void useFederatedMapView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this map only.
     */

    @OSID @Override
    public void useIsolatedMapView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications of new routes. {@code
     *  RouteReceiver.newRoute()} is invoked when a new {@code Route}
     *  is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewRoutes()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new routes for the given
     *  starting location {@code Id}. {@code RouteReceiver.newRoute()}
     *  is invoked when a new {@code Route} is created.
     *
     *  @param startingLocationId the {@code Id} of the starting
     *         location to monitor
     *  @throws org.osid.NullArgumentException {@code
     *          startingLocationId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForNewRoutesForStartingLocation(org.osid.id.Id startingLocationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /** 
     *  Register for notifications of new routes for the given ending
     *  location {@code Id}. {@code RouteReceiver.newRoute()} is
     *  invoked when a new {@code Route} is created.
     *
     *  @param endingLocationId the {@code Id} of the ending location
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code
     *          endingLocationId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForNewRoutesForEndingLocation(org.osid.id.Id endingLocationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of new routes through the given
     *  location.  {@code RouteReceiver.newRoute()} is invoked when a
     *  new {@code Route} appears in this map.
     *
     *  @param locationId the {@code Id} of the {@code Location} to
     *         monitor
     *  @throws org.osid.NullArgumentException {@code locationId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForNewRoutesThroughLocation(org.osid.id.Id locationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of updated routes. {@code
     *  RouteReceiver.changedRoute()} is invoked when a route is
     *  changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedRoutes()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of updated routes for the given
     *  starting location {@code Id}. {@code
     *  RouteReceiver.changedRoute()} is invoked when a {@code Route}
     *  in this map is changed.
     *
     *  @param startingLocationId the {@code Id} of the starting
     *         location to monitor
     *  @throws org.osid.NullArgumentException {@code
     *          startingLocationId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForChangedRoutesForStartingLocation(org.osid.id.Id startingLocationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of updated routes for the given
     *  ending location {@code Id}. {@code
     *  RouteReceiver.changedRoute()} is invoked when a {@code Route}
     *  in this map is changed.
     *
     *  @param endingLocationId the {@code Id} of the ending location
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code
     *          endingLocationId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForChangedRoutesForEndingLocation(org.osid.id.Id endingLocationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of updated routes through the given
     *  location. {@code RouteReceiver.changedRoute()} is invoked when
     *  a route is changed in this map.
     *
     *  @param locationId the {@code Id} of the {@code Location} to
     *         monitor
     *  @throws org.osid.NullArgumentException {@code locationId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForChangedRoutesThroughLocation(org.osid.id.Id locationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of an updated route. {@code
     *  RouteReceiver.changedRoute()} is invoked when the specified
     *  route is changed.
     *
     *  @param routeId the {@code Id} of the {@code Route} to monitor
     *  @throws org.osid.NullArgumentException {@code routeId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedRoute(org.osid.id.Id routeId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted routes. {@code
     *  RouteReceiver.deletedRoute()} is invoked when a route is
     *  deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedRoutes()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of deleted routes for the given
     *  starting location {@code Id}. {@code
     *  RouteReceiver.deletedRoute()} is invoked when a {@code Route}
     *  is deleted or removed from this map.
     *
     *  @param startingLocationId the {@code Id} of the starting
     *         location to monitor
     *  @throws org.osid.NullArgumentException {@code
     *          startingLocationId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */
      
    @OSID @Override
    public void registerForDeletedRoutesForStartingLocation(org.osid.id.Id startingLocationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of deleted routes for the given
     *  ending location {@code Id}. {@code
     *  RouteReceiver.deletedRoute()} is invoked when a {@code Route}
     *  is deleted or removed from this map.
     *
     *  @param endingLocationId the {@code Id} of the ending location
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code
     *          endingLocationId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForDeletedRoutesForEndingLocation(org.osid.id.Id endingLocationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of deleted routes through the given
     *  location. {@code RouteReceiver.deletedRoute()} is invoked when
     *  a route is removed from this map.
     *
     *  @param locationId the {@code Id} of the {@code Location} to
     *         monitor
     *  @throws org.osid.NullArgumentException {@code locationId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForDeletedRoutesThroughLocation(org.osid.id.Id locationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of a deleted route. {@code
     *  RouteReceiver.deletedRoute()} is invoked when the specified
     *  route is deleted.
     *
     *  @param routeId the {@code Id} of the
     *          {@code Route} to monitor
     *  @throws org.osid.NullArgumentException {@code routeId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedRoute(org.osid.id.Id routeId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
