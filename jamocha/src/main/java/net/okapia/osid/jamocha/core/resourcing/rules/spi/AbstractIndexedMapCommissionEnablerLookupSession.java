//
// AbstractIndexedMapCommissionEnablerLookupSession.java
//
//    A simple framework for providing a CommissionEnabler lookup service
//    backed by a fixed collection of commission enablers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a CommissionEnabler lookup service backed by a
 *  fixed collection of commission enablers. The commission enablers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some commission enablers may be compatible
 *  with more types than are indicated through these commission enabler
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>CommissionEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapCommissionEnablerLookupSession
    extends AbstractMapCommissionEnablerLookupSession
    implements org.osid.resourcing.rules.CommissionEnablerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.resourcing.rules.CommissionEnabler> commissionEnablersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.resourcing.rules.CommissionEnabler>());
    private final MultiMap<org.osid.type.Type, org.osid.resourcing.rules.CommissionEnabler> commissionEnablersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.resourcing.rules.CommissionEnabler>());


    /**
     *  Makes a <code>CommissionEnabler</code> available in this session.
     *
     *  @param  commissionEnabler a commission enabler
     *  @throws org.osid.NullArgumentException <code>commissionEnabler<code> is
     *          <code>null</code>
     */

    @Override
    protected void putCommissionEnabler(org.osid.resourcing.rules.CommissionEnabler commissionEnabler) {
        super.putCommissionEnabler(commissionEnabler);

        this.commissionEnablersByGenus.put(commissionEnabler.getGenusType(), commissionEnabler);
        
        try (org.osid.type.TypeList types = commissionEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.commissionEnablersByRecord.put(types.getNextType(), commissionEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a commission enabler from this session.
     *
     *  @param commissionEnablerId the <code>Id</code> of the commission enabler
     *  @throws org.osid.NullArgumentException <code>commissionEnablerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeCommissionEnabler(org.osid.id.Id commissionEnablerId) {
        org.osid.resourcing.rules.CommissionEnabler commissionEnabler;
        try {
            commissionEnabler = getCommissionEnabler(commissionEnablerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.commissionEnablersByGenus.remove(commissionEnabler.getGenusType());

        try (org.osid.type.TypeList types = commissionEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.commissionEnablersByRecord.remove(types.getNextType(), commissionEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeCommissionEnabler(commissionEnablerId);
        return;
    }


    /**
     *  Gets a <code>CommissionEnablerList</code> corresponding to the given
     *  commission enabler genus <code>Type</code> which does not include
     *  commission enablers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known commission enablers or an error results. Otherwise,
     *  the returned list may contain only those commission enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  commissionEnablerGenusType a commission enabler genus type 
     *  @return the returned <code>CommissionEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>commissionEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerList getCommissionEnablersByGenusType(org.osid.type.Type commissionEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.resourcing.rules.commissionenabler.ArrayCommissionEnablerList(this.commissionEnablersByGenus.get(commissionEnablerGenusType)));
    }


    /**
     *  Gets a <code>CommissionEnablerList</code> containing the given
     *  commission enabler record <code>Type</code>. In plenary mode, the
     *  returned list contains all known commission enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  commission enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  commissionEnablerRecordType a commission enabler record type 
     *  @return the returned <code>commissionEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>commissionEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerList getCommissionEnablersByRecordType(org.osid.type.Type commissionEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.resourcing.rules.commissionenabler.ArrayCommissionEnablerList(this.commissionEnablersByRecord.get(commissionEnablerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.commissionEnablersByGenus.clear();
        this.commissionEnablersByRecord.clear();

        super.close();

        return;
    }
}
