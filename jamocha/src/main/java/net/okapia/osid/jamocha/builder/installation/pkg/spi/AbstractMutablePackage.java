//
// AbstractMutablePackage.java
//
//     Defines a mutable Package.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.installation.pkg.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>Package</code>.
 */

public abstract class AbstractMutablePackage
    extends net.okapia.osid.jamocha.installation.pkg.spi.AbstractPackage
    implements org.osid.installation.Package,
               net.okapia.osid.jamocha.builder.installation.pkg.PackageMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this package. 
     *
     *  @param record package record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addPackageRecord(org.osid.installation.records.PackageRecord record, org.osid.type.Type recordType) {
        super.addPackageRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Sets the provider for this package.
     *
     *  @param provider the new provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    @Override
    public void setProvider(net.okapia.osid.provider.Provider provider) {
        super.setProvider(provider);
        return;
    }


    /**
     *  Sets the provider of this package
     *
     *  @param provider the new provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    @Override
    public void setProvider(org.osid.resource.Resource provider) {
        super.setProvider(provider);
        return;
    }


    /**
     *  Adds an asset for the provider branding.
     *
     *  @param asset an asset to add
     *  @throws org.osid.NullArgumentException <code>asset</code>
     *          is <code>null</code>
     */

    @Override
    public void addAssetToBranding(org.osid.repository.Asset asset) {
        super.addAssetToBranding(asset);
        return;
    }


    /**
     *  Adds assets for the provider branding.
     *
     *  @param assets an array of assets to add
     *  @throws org.osid.NullArgumentException <code>assets</code>
     *          is <code>null</code>
     */

    @Override
    public void setBranding(java.util.Collection<org.osid.repository.Asset> assets) {
        super.setBranding(assets);
        return;
    }


    /**
     *  Sets the license.
     *
     *  @param license the license
     *  @throws org.osid.NullArgumentException <code>license</code>
     *          is <code>null</code>
     */

    @Override
    public void setLicense(org.osid.locale.DisplayText license) {
        super.setLicense(license);
        return;
    }


    /**
     *  Sets the display name for this package.
     *
     *  @param displayName the name for this package
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this package.
     *
     *  @param description the description of this package
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException <code>genusType</code>
     *          is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the version.
     *
     *  @param version a version
     *  @throws org.osid.NullArgumentException <code>version</code> is
     *          <code>null</code>
     */

    @Override
    public void setVersion(org.osid.installation.Version version) {
        super.setVersion(version);
        return;
    }


    /**
     *  Sets the copyright.
     *
     *  @param copyright a copyright
     *  @throws org.osid.NullArgumentException <code>copyright</code>
     *          is <code>null</code>
     */

    @Override
    public void setCopyright(org.osid.locale.DisplayText copyright) {
        super.setCopyright(copyright);
        return;
    }


    /**
     *  Sets the creator.
     *
     *  @param creator a creator
     *  @throws org.osid.NullArgumentException <code>creator</code> is
     *          <code>null</code>
     */

    @Override
    public void setCreator(org.osid.resource.Resource creator) {
        super.setCreator(creator);
        return;
    }


    /**
     *  Sets the release date.
     *
     *  @param date a release date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    @Override
    public void setReleaseDate(org.osid.calendaring.DateTime date) {
        super.setReleaseDate(date);
        return;
    }


    /**
     *  Adds a dependency.
     *
     *  @param pkg a dependency
     *  @throws org.osid.NullArgumentException <code>pkg</code> is
     *          <code>null</code>
     */

    @Override
    public void addDependency(org.osid.installation.Package pkg) {
        super.addDependency(pkg);
        return;
    }


    /**
     *  Sets all the package dependencies.
     *
     *  @param packages a collection of packages
     *  @throws org.osid.NullArgumentException <code>packages</code>
     *          is <code>null</code>
     */

    @Override
    public void setDependencies(java.util.Collection<org.osid.installation.Package> packages) {
        super.setDependencies(packages);
        return;
    }


    /**
     *  Adds an installation content.
     *
     *  @param content an installation content
     *  @throws org.osid.NullArgumentException <code>content</code> is
     *          <code>null</code>
     */

    @Override
    public void addInstallationContent(org.osid.installation.InstallationContent content) {
        super.addInstallationContent(content);
        return;
    }


    /**
     *  Sets all the installation contents.
     *
     *  @param contents a collection of contents
     *  @throws org.osid.NullArgumentException <code>contents</code>
     *          is <code>null</code>
     */

    @Override
    public void setInstallationContents(java.util.Collection<org.osid.installation.InstallationContent> contents) {
        super.setInstallationContents(contents);
        return;
    }

    
    /**
     *  Sets the url.
     *
     *  @param url a url
     *  @throws org.osid.NullArgumentException <code>url</code> is
     *          <code>null</code>
     */

    @Override
    public void setURL(String url) {
        super.setURL(url);
        return;
    }
}

