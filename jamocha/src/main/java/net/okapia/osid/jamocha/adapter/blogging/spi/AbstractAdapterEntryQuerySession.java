//
// AbstractQueryEntryLookupSession.java
//
//    An EntryQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.blogging.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An EntryQuerySession adapter.
 */

public abstract class AbstractAdapterEntryQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.blogging.EntryQuerySession {

    private final org.osid.blogging.EntryQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterEntryQuerySession.
     *
     *  @param session the underlying entry query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterEntryQuerySession(org.osid.blogging.EntryQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeBlog</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeBlog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBlogId() {
        return (this.session.getBlogId());
    }


    /**
     *  Gets the {@codeBlog</code> associated with this 
     *  session.
     *
     *  @return the {@codeBlog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.blogging.Blog getBlog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getBlog());
    }


    /**
     *  Tests if this user can perform {@codeEntry</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchEntries() {
        return (this.session.canSearchEntries());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include entries in blogs which are children
     *  of this blog in the blog hierarchy.
     */

    @OSID @Override
    public void useFederatedBlogView() {
        this.session.useFederatedBlogView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this blog only.
     */
    
    @OSID @Override
    public void useIsolatedBlogView() {
        this.session.useIsolatedBlogView();
        return;
    }
    
      
    /**
     *  Gets an entry query. The returned query will not have an
     *  extension query.
     *
     *  @return the entry query 
     */
      
    @OSID @Override
    public org.osid.blogging.EntryQuery getEntryQuery() {
        return (this.session.getEntryQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  entryQuery the entry query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code entryQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code entryQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.blogging.EntryList getEntriesByQuery(org.osid.blogging.EntryQuery entryQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getEntriesByQuery(entryQuery));
    }
}
