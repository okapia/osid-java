//
// AbstractCanonicalUnitProcessorEnabler.java
//
//     Defines a CanonicalUnitProcessorEnabler builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.offering.rules.canonicalunitprocessorenabler.spi;


/**
 *  Defines a <code>CanonicalUnitProcessorEnabler</code> builder.
 */

public abstract class AbstractCanonicalUnitProcessorEnablerBuilder<T extends AbstractCanonicalUnitProcessorEnablerBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidEnablerBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.offering.rules.canonicalunitprocessorenabler.CanonicalUnitProcessorEnablerMiter canonicalUnitProcessorEnabler;


    /**
     *  Constructs a new
     *  <code>AbstractCanonicalUnitProcessorEnablerBuilder</code>.
     *
     *  @param canonicalUnitProcessorEnabler the canonical unit
     *         processor enabler to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractCanonicalUnitProcessorEnablerBuilder(net.okapia.osid.jamocha.builder.offering.rules.canonicalunitprocessorenabler.CanonicalUnitProcessorEnablerMiter canonicalUnitProcessorEnabler) {
        super(canonicalUnitProcessorEnabler);
        this.canonicalUnitProcessorEnabler = canonicalUnitProcessorEnabler;
        return;
    }


    /**
     *  Builds the canonical unit processor enabler.
     *
     *  @return the new canonical unit processor enabler
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitProcessorEnabler</code> is
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnabler build() {
        (new net.okapia.osid.jamocha.builder.validator.offering.rules.canonicalunitprocessorenabler.CanonicalUnitProcessorEnablerValidator(getValidations())).validate(this.canonicalUnitProcessorEnabler);
        return (new net.okapia.osid.jamocha.builder.offering.rules.canonicalunitprocessorenabler.ImmutableCanonicalUnitProcessorEnabler(this.canonicalUnitProcessorEnabler));
    }


    /**
     *  Gets the canonical unit processor enabler. This method is used
     *  to get the miter interface for further updates. Use
     *  <code>build()</code> to finalize and validate construction.
     *
     *  @return the new canonicalUnitProcessorEnabler
     */

    @Override
    public net.okapia.osid.jamocha.builder.offering.rules.canonicalunitprocessorenabler.CanonicalUnitProcessorEnablerMiter getMiter() {
        return (this.canonicalUnitProcessorEnabler);
    }


    /**
     *  Adds a CanonicalUnitProcessorEnabler record.
     *
     *  @param record a canonical unit processor enabler record
     *  @param recordType the type of canonical unit processor enabler record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.offering.rules.records.CanonicalUnitProcessorEnablerRecord record, org.osid.type.Type recordType) {
        getMiter().addCanonicalUnitProcessorEnablerRecord(record, recordType);
        return (self());
    }
}       


