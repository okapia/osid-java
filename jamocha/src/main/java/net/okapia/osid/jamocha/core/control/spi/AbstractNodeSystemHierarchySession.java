//
// AbstractNodeSystemHierarchySession.java
//
//     Defines a System hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.control.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a system hierarchy session for delivering a hierarchy
 *  of systems using the SystemNode interface.
 */

public abstract class AbstractNodeSystemHierarchySession
    extends net.okapia.osid.jamocha.control.spi.AbstractSystemHierarchySession
    implements org.osid.control.SystemHierarchySession {

    private java.util.Collection<org.osid.control.SystemNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root system <code> Ids </code> in this hierarchy.
     *
     *  @return the root system <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootSystemIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.control.systemnode.SystemNodeToIdList(this.roots));
    }


    /**
     *  Gets the root systems in the system hierarchy. A node
     *  with no parents is an orphan. While all system <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root systems 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.SystemList getRootSystems()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.control.systemnode.SystemNodeToSystemList(new net.okapia.osid.jamocha.control.systemnode.ArraySystemNodeList(this.roots)));
    }


    /**
     *  Adds a root system node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootSystem(org.osid.control.SystemNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root system nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootSystems(java.util.Collection<org.osid.control.SystemNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root system node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootSystem(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.control.SystemNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> System </code> has any parents. 
     *
     *  @param  systemId a system <code> Id </code> 
     *  @return <code> true </code> if the system has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> systemId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> systemId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentSystems(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getSystemNode(systemId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of a
     *  system.
     *
     *  @param  id an <code> Id </code> 
     *  @param  systemId the <code> Id </code> of a system 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> systemId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> systemId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> systemId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfSystem(org.osid.id.Id id, org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.control.SystemNodeList parents = getSystemNode(systemId).getParentSystemNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextSystemNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given system. 
     *
     *  @param  systemId a system <code> Id </code> 
     *  @return the parent <code> Ids </code> of the system 
     *  @throws org.osid.NotFoundException <code> systemId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> systemId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentSystemIds(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.control.system.SystemToIdList(getParentSystems(systemId)));
    }


    /**
     *  Gets the parents of the given system. 
     *
     *  @param  systemId the <code> Id </code> to query 
     *  @return the parents of the system 
     *  @throws org.osid.NotFoundException <code> systemId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> systemId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.SystemList getParentSystems(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.control.systemnode.SystemNodeToSystemList(getSystemNode(systemId).getParentSystemNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of a
     *  system.
     *
     *  @param  id an <code> Id </code> 
     *  @param  systemId the Id of a system 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> systemId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> systemId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> systemId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfSystem(org.osid.id.Id id, org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfSystem(id, systemId)) {
            return (true);
        }

        try (org.osid.control.SystemList parents = getParentSystems(systemId)) {
            while (parents.hasNext()) {
                if (isAncestorOfSystem(id, parents.getNextSystem().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if a system has any children. 
     *
     *  @param  systemId a system <code> Id </code> 
     *  @return <code> true </code> if the <code> systemId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> systemId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> systemId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildSystems(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getSystemNode(systemId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of a
     *  system.
     *
     *  @param  id an <code> Id </code> 
     *  @param systemId the <code> Id </code> of a 
     *         system
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> systemId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> systemId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> systemId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfSystem(org.osid.id.Id id, org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfSystem(systemId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  system.
     *
     *  @param  systemId the <code> Id </code> to query 
     *  @return the children of the system 
     *  @throws org.osid.NotFoundException <code> systemId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> systemId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildSystemIds(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.control.system.SystemToIdList(getChildSystems(systemId)));
    }


    /**
     *  Gets the children of the given system. 
     *
     *  @param  systemId the <code> Id </code> to query 
     *  @return the children of the system 
     *  @throws org.osid.NotFoundException <code> systemId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> systemId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.SystemList getChildSystems(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.control.systemnode.SystemNodeToSystemList(getSystemNode(systemId).getChildSystemNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of a
     *  system.
     *
     *  @param  id an <code> Id </code> 
     *  @param systemId the <code> Id </code> of a 
     *         system
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> systemId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> systemId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> systemId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfSystem(org.osid.id.Id id, org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfSystem(systemId, id)) {
            return (true);
        }

        try (org.osid.control.SystemList children = getChildSystems(systemId)) {
            while (children.hasNext()) {
                if (isDescendantOfSystem(id, children.getNextSystem().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  system.
     *
     *  @param  systemId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified system node 
     *  @throws org.osid.NotFoundException <code> systemId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> systemId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getSystemNodeIds(org.osid.id.Id systemId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.control.systemnode.SystemNodeToNode(getSystemNode(systemId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given system.
     *
     *  @param  systemId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified system node 
     *  @throws org.osid.NotFoundException <code> systemId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> systemId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.SystemNode getSystemNodes(org.osid.id.Id systemId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getSystemNode(systemId));
    }


    /**
     *  Closes this <code>SystemHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets a system node.
     *
     *  @param systemId the id of the system node
     *  @throws org.osid.NotFoundException <code>systemId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>systemId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.control.SystemNode getSystemNode(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(systemId, "system Id");
        for (org.osid.control.SystemNode system : this.roots) {
            if (system.getId().equals(systemId)) {
                return (system);
            }

            org.osid.control.SystemNode r = findSystem(system, systemId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(systemId + " is not found");
    }


    protected org.osid.control.SystemNode findSystem(org.osid.control.SystemNode node, 
                                                     org.osid.id.Id systemId) 
        throws org.osid.OperationFailedException {

        try (org.osid.control.SystemNodeList children = node.getChildSystemNodes()) {
            while (children.hasNext()) {
                org.osid.control.SystemNode system = children.getNextSystemNode();
                if (system.getId().equals(systemId)) {
                    return (system);
                }
                
                system = findSystem(system, systemId);
                if (system != null) {
                    return (system);
                }
            }
        }

        return (null);
    }
}
