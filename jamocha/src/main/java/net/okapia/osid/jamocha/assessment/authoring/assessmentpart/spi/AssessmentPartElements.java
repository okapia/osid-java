//
// AssessmentPartElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.authoring.assessmentpart.spi;

/**
 *  Ids for object elements for use in forms and queries.
 */

public class AssessmentPartElements
    extends net.okapia.osid.jamocha.spi.OperableOsidObjectElements {


    /**
     *  Gets the sequestered element Id.
     *
     *  @return the sequestered element Id
     */

    public static org.osid.id.Id getSequestered() {
        return (net.okapia.osid.jamocha.spi.ContainableElements.getSequestered());
    }


    /**
     *  Gets the AssessmentPartElement Id.
     *
     *  @return the assessment part element Id
     */

    public static org.osid.id.Id getAssessmentPartEntityId() {
        return (makeEntityId("osid.assessment.authoring.AssessmentPart"));
    }


    /**
     *  Gets the AssessmentId element Id.
     *
     *  @return the AssessmentId element Id
     */

    public static org.osid.id.Id getAssessmentId() {
        return (makeElementId("osid.assessment.authoring.assessmentpart.AssessmentId"));
    }


    /**
     *  Gets the Assessment element Id.
     *
     *  @return the Assessment element Id
     */

    public static org.osid.id.Id getAssessment() {
        return (makeElementId("osid.assessment.authoring.assessmentpart.Assessment"));
    }


    /**
     *  Gets the AssessmentPartId element Id.
     *
     *  @return the AssessmentPartId element Id
     */

    public static org.osid.id.Id getAssessmentPartId() {
        return (makeElementId("osid.assessment.authoring.assessmentpart.AssessmentPartId"));
    }


    /**
     *  Gets the AssessmentPart element Id.
     *
     *  @return the AssessmentPart element Id
     */

    public static org.osid.id.Id getAssessmentPart() {
        return (makeElementId("osid.assessment.authoring.assessmentpart.AssessmentPart"));
    }


    /**
     *  Gets the Weight element Id.
     *
     *  @return the Weight element Id
     */

    public static org.osid.id.Id getWeight() {
        return (makeElementId("osid.assessment.authoring.assessmentpart.Weight"));
    }


    /**
     *  Gets the AllocatedTime element Id.
     *
     *  @return the AllocatedTime element Id
     */

    public static org.osid.id.Id getAllocatedTime() {
        return (makeElementId("osid.assessment.authoring.assessmentpart.AllocatedTime"));
    }


    /**
     *  Gets the ChildAssessmentPartIds element Id.
     *
     *  @return the ChildAssessmentPartIds element Id
     */

    public static org.osid.id.Id getChildAssessmentPartIds() {
        return (makeElementId("osid.assessment.authoring.assessmentpart.ChildAssessmentPartIds"));
    }


    /**
     *  Gets the ChildAssessmentParts element Id.
     *
     *  @return the ChildAssessmentParts element Id
     */

    public static org.osid.id.Id getChildAssessmentParts() {
        return (makeElementId("osid.assessment.authoring.assessmentpart.ChildAssessmentParts"));
    }


    /**
     *  Gets the ParentAssessmentPartId element Id.
     *
     *  @return the ParentAssessmentPartId element Id
     */

    public static org.osid.id.Id getParentAssessmentPartId() {
        return (makeQueryElementId("osid.assessment.authoring.assessmentpart.ParentAssessmentPartId"));
    }


    /**
     *  Gets the ParentAssessmentPart element Id.
     *
     *  @return the ParentAssessmentPart element Id
     */

    public static org.osid.id.Id getParentAssessmentPart() {
        return (makeQueryElementId("osid.assessment.authoring.assessmentpart.ParentAssessmentPart"));
    }


    /**
     *  Gets the Section element Id.
     *
     *  @return the Section element Id
     */

    public static org.osid.id.Id getSection() {
        return (makeElementId("osid.assessment.authoring.assessmentpart.Section"));
    }


    /**
     *  Gets the BankId element Id.
     *
     *  @return the BankId element Id
     */

    public static org.osid.id.Id getBankId() {
        return (makeQueryElementId("osid.assessment.authoring.assessmentpart.BankId"));
    }


    /**
     *  Gets the Bank element Id.
     *
     *  @return the Bank element Id
     */

    public static org.osid.id.Id getBank() {
        return (makeQueryElementId("osid.assessment.authoring.assessmentpart.Bank"));
    }
}
