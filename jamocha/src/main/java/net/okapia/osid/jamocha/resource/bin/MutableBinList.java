//
// MutableBinList.java
//
//     Implements a BinList. This list allows Bins to be
//     added after this list has been created.
//
//
// Tom Coppeto
// OnTapSolutions
// 29 June 2008
//
//
// Copyright (c) 2008, 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resource.bin;


/**
 *  <p>Implements a BinList. This list allows Bins to be
 *  added after this bin has been created. One this list has been
 *  returned to the consumer, all subsequent additions occur in a
 *  separate processing thread.  The creator of this bin must
 *  invoke <code>eol()</code> when there are no more bins to be
 *  added.</p>
 *
 *  <p> If the consumer of the <code>BinList</code> interface
 *  reaches the end of the internal buffer before <code>eol()</code>,
 *  then methods will block until more bins are added or
 *  <code>eol()</code> is invoked.</p>
 *
 *  <p><code>available()</code> never blocks but may return
 *  <code>0</code> if waiting for more bins to be added.</p>
 */

public final class MutableBinList
    extends net.okapia.osid.jamocha.resource.bin.spi.AbstractMutableBinList
    implements org.osid.resource.BinList {


    /**
     *  Creates a new empty <code>MutableBinList</code>.
     */

    public MutableBinList() {
        super();
    }


    /**
     *  Creates a new <code>MutableBinList</code>.
     *
     *  @param bin a <code>Bin</code>
     *  @throws org.osid.NullArgumentException <code>bin</code>
     *          is <code>null</code>
     */

    public MutableBinList(org.osid.resource.Bin bin) {
        super(bin);
        return;
    }


    /**
     *  Creates a new <code>MutableBinList</code>.
     *
     *  @param array an array of bins
     *  @throws org.osid.NullArgumentException <code>array</code>
     *          is <code>null</code>
     */

    public MutableBinList(org.osid.resource.Bin[] array) {
        super(array);
        return;
    }


    /**
     *  Creates a new <code>MutableBinList</code>.
     *
     *  @param collection a java.util.Collection of bins
     *  @throws org.osid.NullArgumentException <code>collection</code>
     *          is <code>null</code>
     */

    public MutableBinList(java.util.Collection<org.osid.resource.Bin> collection) {
        super(collection);
        return;
    }
}
