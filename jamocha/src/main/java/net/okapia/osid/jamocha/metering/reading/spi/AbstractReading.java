//
// AbstractReading.java
//
//     Defines a Reading.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.metering.reading.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Reading</code>.
 */

public abstract class AbstractReading
    implements org.osid.metering.Reading {

    private org.osid.metering.Meter meter;
    private org.osid.id.Id objectId;
    private java.math.BigDecimal amount;

    
    /**
     *  Gets the <code> Id </code> of the <code> Meter </code> associated with 
     *  this reading. 
     *
     *  @return gets the <code> Id </code> of the <code> Meter </code> 
     */

    @OSID @Override
    public org.osid.id.Id getMeterId() {
        return (this.meter.getId());
    }


    /**
     *  Gets the <code> Meter </code> associated with this reading. 
     *
     *  @return gets the <code> Meter </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.metering.Meter getMeter()
        throws org.osid.OperationFailedException {
        
        return (this.meter);
    }


    /**
     *  Sets the meter.
     *
     *  @param meter the meter
     *  @throws org.osid.NullArgumentException <code>meter</code> is
     *          <code>null</code>
     */

    protected void setMeter(org.osid.metering.Meter meter) {
        nullarg(meter,"meter");
        this.meter = meter;
        return;
    }


    /**
     *  Gets the metered object associated with this reading. 
     *
     *  @return gets the metered object <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getMeteredObjectId() {
        return (this.objectId);
    }


    /**
     *  Sets the metered object Id.
     *
     *  @param objectId the metered object Id
     *  @throws org.osid.NullArgumentException <code>object</code> is
     *          <code>null</code>
     */

    protected void setMeteredObjectId(org.osid.id.Id objectId) {
        nullarg(objectId, "metered object Id");
        this.objectId = objectId;
        return;
    }


    /**
     *  Gets the amount of this reading. 
     *
     *  @return gets the amount of this reading 
     */

    @OSID @Override
    public java.math.BigDecimal getAmount() {
        return (this.amount);
    }


    /**
     *  Sets the amount.
     *
     *  @param amount the amount
     *  @throws org.osid.NullArgumentException <code>amount</code> is
     *          <code>null</code>
     */

    protected void setAmount(java.math.BigDecimal amount) {
        nullarg(amount, "amount");
        this.amount = amount;
        return;
    }
}
