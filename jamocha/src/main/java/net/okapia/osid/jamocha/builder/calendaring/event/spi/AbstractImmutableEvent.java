//
// AbstractImmutableEvent.java
//
//     Wraps a mutable Event to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.calendaring.event.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Event</code> to hide modifiers. This
 *  wrapper provides an immutized Event from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying event whose state changes are visible.
 */

public abstract class AbstractImmutableEvent
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableTemporalOsidObject
    implements org.osid.calendaring.Event {

    private final org.osid.calendaring.Event event;


    /**
     *  Constructs a new <code>AbstractImmutableEvent</code>.
     *
     *  @param event the event to immutablize
     *  @throws org.osid.NullArgumentException <code>event</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableEvent(org.osid.calendaring.Event event) {
        super(event);
        this.event = event;
        return;
    }


    /**
     *  Tests if this <code> Containable </code> is sequestered in
     *  that it should not appear outside of its aggregated
     *  event.
     *
     *  @return <code> true </code> if this containable is
     *          sequestered, <code> false </code> if this containable
     *          may appear outside its aggregate
     */

    @OSID @Override
    public boolean isSequestered() {
        return (this.event.isSequestered());
    }


    /**
     *  Tests if this event is implicit. An implicit event is one that is 
     *  generated from an offset or recurring series. 
     *
     *  @return <code> true </code> if this event is implicit, <code> false 
     *          </code> if explicitly defined 
     */

    @OSID @Override
    public boolean isImplicit() {
        return (this.event.isImplicit());
    }


    /**
     *  Tests if this event is an implclit event part of a recurring event. If 
     *  true, <code> isImplicit() </code> must also be true. 
     *
     *  @return <code> true </code> if this event is part of a recurring 
     *          series, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean isInRecurringSeries() {
        return (this.event.isInRecurringSeries());
    }


    /**
     *  Tests if this event is a superseding event. If true, <code> 
     *  isImplicit() </code> must also be true. 
     *
     *  @return <code> true </code> if this event is superseding event, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean isSupersedingEvent() {
        return (this.event.isSupersedingEvent());
    }


    /**
     *  Tests if this event is an event calculated from an offset. If true, 
     *  <code> isImplicit() </code> must also be true. 
     *
     *  @return <code> true </code> if this event is an offset event, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean isOffsetEvent() {
        return (this.event.isOffsetEvent());
    }


    /**
     *  Gets the duration of the event. 
     *
     *  @param  units the units of the duration 
     *  @return the duration 
     *  @throws org.osid.NullArgumentException <code> units </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getDuration(org.osid.calendaring.DateTimeResolution units) {
        return (this.event.getDuration(units));
    }


    /**
     *  Gets a descriptive location. 
     *
     *  @return the location 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getLocationDescription() {
        return (this.event.getLocationDescription());
    }


    /**
     *  Tests if a location is associated with this event. 
     *
     *  @return <code> true </code> if there is an associated location, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasLocation() {
        return (this.event.hasLocation());
    }


    /**
     *  Gets the location <code> Id </code> . 
     *
     *  @return a location <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasLocation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getLocationId() {
        return (this.event.getLocationId());
    }


    /**
     *  Gets the <code> Location. </code> 
     *
     *  @return a location 
     *  @throws org.osid.IllegalStateException <code> hasLocation() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.mapping.Location getLocation()
        throws org.osid.OperationFailedException {

        return (this.event.getLocation());
    }


    /**
     *  Tests if a sponsor is associated with this event. 
     *
     *  @return <code> true </code> if there is an associated sponsor. <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasSponsors() {
        return (this.event.hasSponsors());
    }


    /**
     *  Gets the <code> Id </code> of the event sponsors. 
     *
     *  @return the sponsor <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> hasSponsors() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getSponsorIds() {
        return (this.event.getSponsorIds());
    }


    /**
     *  Gets the <code> Sponsors. </code> 
     *
     *  @return the sponsors 
     *  @throws org.osid.IllegalStateException <code> hasSponsors() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getSponsors()
        throws org.osid.OperationFailedException {

        return (this.event.getSponsors());
    }


    /**
     *  Gets the event record corresponding to the given <code> Event </code> 
     *  record <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record. The <code> eventRecordType </code> 
     *  may be the <code> Type </code> returned in <code> getRecordTypes() 
     *  </code> or any of its parents in a <code> Type </code> hierarchy where 
     *  <code> hasRecordType(eventRecordType) </code> is <code> true </code> . 
     *
     *  @param  eventRecordType the type of the record to retrieve 
     *  @return the event record 
     *  @throws org.osid.NullArgumentException <code> eventRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(eventRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.records.EventRecord getEventRecord(org.osid.type.Type eventRecordType)
        throws org.osid.OperationFailedException {

        return (this.event.getEventRecord(eventRecordType));
    }
}

