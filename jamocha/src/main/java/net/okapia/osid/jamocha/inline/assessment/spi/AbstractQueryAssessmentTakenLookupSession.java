//
// AbstractQueryAssessmentTakenLookupSession.java
//
//    An inline adapter that maps an AssessmentTakenLookupSession to
//    an AssessmentTakenQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.assessment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps an AssessmentTakenLookupSession to
 *  an AssessmentTakenQuerySession.
 */

public abstract class AbstractQueryAssessmentTakenLookupSession
    extends net.okapia.osid.jamocha.assessment.spi.AbstractAssessmentTakenLookupSession
    implements org.osid.assessment.AssessmentTakenLookupSession {

    private final org.osid.assessment.AssessmentTakenQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryAssessmentTakenLookupSession.
     *
     *  @param querySession the underlying assessment taken query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryAssessmentTakenLookupSession(org.osid.assessment.AssessmentTakenQuerySession querySession) {
        nullarg(querySession, "assessment taken query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Bank</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Bank Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBankId() {
        return (this.session.getBankId());
    }


    /**
     *  Gets the <code>Bank</code> associated with this 
     *  session.
     *
     *  @return the <code>Bank</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.Bank getBank()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getBank());
    }


    /**
     *  Tests if this user can perform <code>AssessmentTaken</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAssessmentsTaken() {
        return (this.session.canSearchAssessmentsTaken());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include assessments taken in banks which are children
     *  of this bank in the bank hierarchy.
     */

    @OSID @Override
    public void useFederatedBankView() {
        this.session.useFederatedBankView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this bank only.
     */

    @OSID @Override
    public void useIsolatedBankView() {
        this.session.useIsolatedBankView();
        return;
    }
    
     
    /**
     *  Gets the <code>AssessmentTaken</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>AssessmentTaken</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>AssessmentTaken</code> and
     *  retained for compatibility.
     *
     *  @param  assessmentTakenId <code>Id</code> of the
     *          <code>AssessmentTaken</code>
     *  @return the assessment taken
     *  @throws org.osid.NotFoundException <code>assessmentTakenId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>assessmentTakenId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTaken getAssessmentTaken(org.osid.id.Id assessmentTakenId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.assessment.AssessmentTakenQuery query = getQuery();
        query.matchId(assessmentTakenId, true);
        org.osid.assessment.AssessmentTakenList assessmentsTaken = this.session.getAssessmentsTakenByQuery(query);
        if (assessmentsTaken.hasNext()) {
            return (assessmentsTaken.getNextAssessmentTaken());
        } 
        
        throw new org.osid.NotFoundException(assessmentTakenId + " not found");
    }


    /**
     *  Gets an <code>AssessmentTakenList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  assessmentsTaken specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>AssessmentsTaken</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  assessmentTakenIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>AssessmentTaken</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentTakenIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenByIds(org.osid.id.IdList assessmentTakenIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.assessment.AssessmentTakenQuery query = getQuery();

        try (org.osid.id.IdList ids = assessmentTakenIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getAssessmentsTakenByQuery(query));
    }


    /**
     *  Gets an <code>AssessmentTakenList</code> corresponding to the given
     *  assessment taken genus <code>Type</code> which does not include
     *  assessments taken of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  assessments taken or an error results. Otherwise, the returned list
     *  may contain only those assessments taken that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  assessmentTakenGenusType an assessmentTaken genus type 
     *  @return the returned <code>AssessmentTaken</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentTakenGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenByGenusType(org.osid.type.Type assessmentTakenGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.assessment.AssessmentTakenQuery query = getQuery();
        query.matchGenusType(assessmentTakenGenusType, true);
        return (this.session.getAssessmentsTakenByQuery(query));
    }


    /**
     *  Gets an <code>AssessmentTakenList</code> corresponding to the given
     *  assessment taken genus <code>Type</code> and include any additional
     *  assessments taken with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  assessments taken or an error results. Otherwise, the returned list
     *  may contain only those assessments taken that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  assessmentTakenGenusType an assessmentTaken genus type 
     *  @return the returned <code>AssessmentTaken</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentTakenGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenByParentGenusType(org.osid.type.Type assessmentTakenGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.assessment.AssessmentTakenQuery query = getQuery();
        query.matchParentGenusType(assessmentTakenGenusType, true);
        return (this.session.getAssessmentsTakenByQuery(query));
    }


    /**
     *  Gets an <code>AssessmentTakenList</code> containing the given
     *  assessment taken record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  assessments taken or an error results. Otherwise, the returned list
     *  may contain only those assessments taken that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  assessmentTakenRecordType an assessmentTaken record type 
     *  @return the returned <code>AssessmentTaken</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentTakenRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenByRecordType(org.osid.type.Type assessmentTakenRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.assessment.AssessmentTakenQuery query = getQuery();
        query.matchRecordType(assessmentTakenRecordType, true);
        return (this.session.getAssessmentsTakenByQuery(query));
    }

    
    /**
     *  Gets an <code> AssessmentTakenList </code> started in the
     *  given date range inclusive. In plenary mode, the returned list
     *  contains all known assessments taken or an error
     *  results. Otherwise, the returned list may contain only those
     *  assessments taken that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  @param  from start date 
     *  @param  to end date 
     *  @return the returned <code> AssessmentTaken </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or
     *          <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization
     *          failure occurred
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenByDate(org.osid.calendaring.DateTime from, 
                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.assessment.AssessmentTakenQuery query = getQuery();
        query.matchActualStartTime(from, to, true);
        return (this.session.getAssessmentsTakenByQuery(query));
    }


    /**
     *  Gets an <code> AssessmentTakenList </code> for the given resource. In 
     *  plenary mode, the returned list contains all known assessments taken 
     *  or an error results. Otherwise, the returned list may contain only 
     *  those assessments taken that are accessible through this session. 
     *
     *  @param  resourceId <code> Id </code> of a <code> Resource </code> 
     *  @return the returned <code> AssessmentTaken </code> list 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenForTaker(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.assessment.AssessmentTakenQuery query = getQuery();
        query.matchTakerId(resourceId, true);
        return (this.session.getAssessmentsTakenByQuery(query));
    }


    /**
     *  Gets an <code> AssessmentTakenList </code> started in the given date 
     *  range inclusive for the given resource. In plenary mode, the returned 
     *  list contains all known assessments taken or an error results. 
     *  Otherwise, the returned list may contain only those assessments taken 
     *  that are accessible through this session. 
     *
     *  @param  resourceId <code> Id </code> of a <code> Resource </code> 
     *  @param  from start date 
     *  @param  to end date 
     *  @return the returned <code> AssessmentTaken </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> resourceId, from </code> 
     *          or <code> to </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenByDateForTaker(org.osid.id.Id resourceId, 
                                                                                     org.osid.calendaring.DateTime from, 
                                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.assessment.AssessmentTakenQuery query = getQuery();
        query.matchTakerId(resourceId, true);
        query.matchActualStartTime(from, to, true);
        return (this.session.getAssessmentsTakenByQuery(query));
    }


    /**
     *  Gets an <code> AssessmentTakenList </code> for the given
     *  assessment.  In plenary mode, the returned list contains all
     *  known assessments taken or an error results. Otherwise, the
     *  returned list may contain only those assessments taken that
     *  are accessible through this session.
     *
     *  @param  assessmentId <code> Id </code> of an <code> Assessment </code> 
     *  @return the returned <code> AssessmentTaken </code> list 
     *  @throws org.osid.NullArgumentException <code> assessmentId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.PermissionDeniedException authorization
     *          failure occurred
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenForAssessment(org.osid.id.Id assessmentId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.assessment.AssessmentTakenQuery query = getQuery();

        if (query.supportsAssessmentOfferedQuery()) {
            query.getAssessmentOfferedQuery().matchAssessmentId(assessmentId, true);
            return (this.session.getAssessmentsTakenByQuery(query));
        } else {
            return (super.getAssessmentsTakenForAssessment(assessmentId));
        }
    }


    /**
     *  Gets an <code> AssessmentTakenList </code> started in the given date 
     *  range inclusive for the given assessment. In plenary mode, the 
     *  returned list contains all known assessments taken or an error 
     *  results. Otherwise, the returned list may contain only those 
     *  assessments taken that are accessible through this session. 
     *
     *  @param  assessmentId <code> Id </code> of an <code> Assessment </code> 
     *  @param  from start date 
     *  @param  to end date 
     *  @return the returned <code> AssessmentTaken </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> assessmentId, from 
     *          </code> or <code> to </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenByDateForAssessment(org.osid.id.Id assessmentId, 
                                                                                          org.osid.calendaring.DateTime from, 
                                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.assessment.AssessmentTakenQuery query = getQuery();

        if (query.supportsAssessmentOfferedQuery()) {
            query.getAssessmentOfferedQuery().matchAssessmentId(assessmentId, true);
            query.matchActualStartTime(from, to, true);
            return (this.session.getAssessmentsTakenByQuery(query));
        } else {
            return (super.getAssessmentsTakenForAssessment(assessmentId));
        }
    }


    /**
     *  Gets an <code> AssessmentTakenList </code> for the given
     *  resource and assessment. In plenary mode, the returned list
     *  contains all known assessments taken or an error
     *  results. Otherwise, the returned list may contain only those
     *  assessments taken that are accessible through this session.
     *
     *  @param  resourceId <code> Id </code> of a <code> Resource </code> 
     *  @param  assessmentId <code> Id </code> of an <code> Assessment </code> 
     *  @return the returned <code> AssessmentTaken </code> list 
     *  @throws org.osid.NullArgumentException <code> resourceId
     *          </code> or <code> assessmentId </code> is <code> null
     *          </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization
     *          failure occurred
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenForTakerAndAssessment(org.osid.id.Id resourceId, 
                                                                                            org.osid.id.Id assessmentId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.assessment.AssessmentTakenQuery query = getQuery();

        if (query.supportsAssessmentOfferedQuery()) {
            query.getAssessmentOfferedQuery().matchAssessmentId(assessmentId, true);
            query.matchTakerId(resourceId, true);
            return (this.session.getAssessmentsTakenByQuery(query));
        } else {
            return (super.getAssessmentsTakenForAssessment(assessmentId));
        }
    }

    
    /**
     *  Gets an <code> AssessmentTakenList </code> started in the
     *  given date range inclusive for the given resource and
     *  assessment. In plenary mode, the returned list contains all
     *  known assessments taken or an error results. Otherwise, the
     *  returned list may contain only those assessments taken that
     *  are accessible through this session.
     *
     *  @param  resourceId <code> Id </code> of a <code> Resource </code> 
     *  @param  assessmentId <code> Id </code> of an <code> Assessment </code> 
     *  @param  from start date 
     *  @param  to end date 
     *  @return the returned <code> AssessmentTaken </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> resourceId,
     *          assessmentId, from </code> or <code> to </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization
     *          failure occurred
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenByDateForTakerAndAssessment(org.osid.id.Id resourceId, 
                                                                                                  org.osid.id.Id assessmentId, 
                                                                                                  org.osid.calendaring.DateTime from, 
                                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.assessment.AssessmentTakenQuery query = getQuery();

        if (query.supportsAssessmentOfferedQuery()) {
            query.getAssessmentOfferedQuery().matchAssessmentId(assessmentId, true);
            query.matchTakerId(resourceId, true);
            query.matchActualStartTime(from, to, true);
            return (this.session.getAssessmentsTakenByQuery(query));
        } else {
            return (super.getAssessmentsTakenForAssessment(assessmentId));
        }
    }

    
    /**
     *  Gets an <code> AssessmentTakenList </code> by the given
     *  assessment offered. In plenary mode, the returned list
     *  contains all known assessments taken or an error
     *  results. Otherwise, the returned list may contain only those
     *  assessments taken that are accessible through this session.
     *
     *  @param  assessmentOfferedId <code> Id </code> of an <code> 
     *          AssessmentOffered </code> 
     *  @return the returned <code> AssessmentTaken </code> list 
     *  @throws org.osid.NullArgumentException <code> assessmentOfferedId 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenForAssessmentOffered(org.osid.id.Id assessmentOfferedId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.assessment.AssessmentTakenQuery query = getQuery();
        query.matchAssessmentOfferedId(assessmentOfferedId, true);
        return (this.session.getAssessmentsTakenByQuery(query));
    }


    /**
     *  Gets an <code> AssessmentTakenList </code> started in the
     *  given date range inclusive for the given assessment
     *  offered. In plenary mode, the returned list contains all known
     *  assessments taken or an error results. Otherwise, the returned
     *  list may contain only those assessments taken that are
     *  accessible through this session.
     *
     *  @param assessmentOfferedId <code> Id </code> of an <code>
     *          AssessmentOffered </code>
     *  @param  from start date 
     *  @param  to end date 
     *  @return the returned <code> AssessmentTaken </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code>
     *          assessmentOfferedId, from, </code> or <code> to
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenByDateForAssessmentOffered(org.osid.id.Id assessmentOfferedId, 
                                                                                                 org.osid.calendaring.DateTime from, 
                                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.assessment.AssessmentTakenQuery query = getQuery();
        query.matchAssessmentOfferedId(assessmentOfferedId, true);
        query.matchActualStartTime(from, to, true);
        return (this.session.getAssessmentsTakenByQuery(query));
    }


    /**
     *  Gets an <code> AssessmentTakenList </code> for the given resource and 
     *  assessment offered. In plenary mode, the returned list contains all 
     *  known assessments taken or an error results. Otherwise, the returned 
     *  list may contain only those assessments taken that are accessible 
     *  through this session. 
     *
     *  @param  resourceId <code> Id </code> of a <code> Resource </code> 
     *  @param  assessmentOfferedId <code> Id </code> of an <code> 
     *          AssessmentOffered </code> 
     *  @return the returned <code> AssessmentTaken </code> list 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> or 
     *          <code> assessmenOfferedtId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenForTakerAndAssessmentOffered(org.osid.id.Id resourceId, 
                                                                                                   org.osid.id.Id assessmentOfferedId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.assessment.AssessmentTakenQuery query = getQuery();
        query.matchTakerId(resourceId, true);
        query.matchAssessmentOfferedId(assessmentOfferedId, true);
        return (this.session.getAssessmentsTakenByQuery(query));
    }


    /**
     *  Gets an <code> AssessmentTakenList </code> started in the given date 
     *  range inclusive for the given resource and assessment offered. In 
     *  plenary mode, the returned list contains all known assessments taken 
     *  or an error results. Otherwise, the returned list may contain only 
     *  those assessments taken that are accessible through this session. 
     *
     *  @param  resourceId <code> Id </code> of a <code> Resource </code> 
     *  @param  assessmentOfferedId <code> Id </code> of an <code> 
     *          AssessmentOffered </code> 
     *  @param  from start date 
     *  @param  to end date 
     *  @return the returned <code> AssessmentTaken </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> resourceId, 
     *          assessmentOfferedId, from, </code> or <code> to </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenByDateForTakerAndAssessmentOffered(org.osid.id.Id resourceId, 
                                                                                                         org.osid.id.Id assessmentOfferedId, 
                                                                                                         org.osid.calendaring.DateTime from, 
                                                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.assessment.AssessmentTakenQuery query = getQuery();
        query.matchTakerId(resourceId, true);
        query.matchAssessmentOfferedId(assessmentOfferedId, true);
        query.matchActualStartTime(from, to, true);
        return (this.session.getAssessmentsTakenByQuery(query));
    }


    /**
     *  Gets all <code>AssessmentsTaken</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  assessments taken or an error results. Otherwise, the returned list
     *  may contain only those assessments taken that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>AssessmentsTaken</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTaken()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.assessment.AssessmentTakenQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getAssessmentsTakenByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.assessment.AssessmentTakenQuery getQuery() {
        org.osid.assessment.AssessmentTakenQuery query = this.session.getAssessmentTakenQuery();
        return (query);
    }
}
