//
// AbstractLocationQueryInspector.java
//
//     A template for making a LocationQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.location.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for locations.
 */

public abstract class AbstractLocationQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.mapping.LocationQueryInspector {

    private final java.util.Collection<org.osid.mapping.records.LocationQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the coordinate query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CoordinateTerm[] getCoordinateTerms() {
        return (new org.osid.search.terms.CoordinateTerm[0]);
    }


    /**
     *  Gets the contained spatial unit query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.SpatialUnitTerm[] getContainedSpatialUnitTerms() {
        return (new org.osid.search.terms.SpatialUnitTerm[0]);
    }


    /**
     *  Gets the overlapping spatial unit query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.SpatialUnitTerm[] getOverlappingSpatialUnitTerms() {
        return (new org.osid.search.terms.SpatialUnitTerm[0]);
    }


    /**
     *  Gets the spatial unit query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.SpatialUnitTerm[] getSpatialUnitTerms() {
        return (new org.osid.search.terms.SpatialUnitTerm[0]);
    }


    /**
     *  Gets the route <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRouteIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the route query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteQueryInspector[] getRouteTerms() {
        return (new org.osid.mapping.route.RouteQueryInspector[0]);
    }


    /**
     *  Gets the path <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPathIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the path query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.path.PathQueryInspector[] getPathTerms() {
        return (new org.osid.mapping.path.PathQueryInspector[0]);
    }


    /**
     *  Gets the containing location <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getContainingLocationIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the containing location query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.LocationQueryInspector[] getContainingLocationTerms() {
        return (new org.osid.mapping.LocationQueryInspector[0]);
    }


    /**
     *  Gets the contained location <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getContainedLocationIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the contained location query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.LocationQueryInspector[] getContainedLocationTerms() {
        return (new org.osid.mapping.LocationQueryInspector[0]);
    }


    /**
     *  Gets the map <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getMapIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the map query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.MapQueryInspector[] getMapTerms() {
        return (new org.osid.mapping.MapQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given location query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a location implementing the requested record.
     *
     *  @param locationRecordType a location record type
     *  @return the location query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>locationRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(locationRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.records.LocationQueryInspectorRecord getLocationQueryInspectorRecord(org.osid.type.Type locationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.records.LocationQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(locationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(locationRecordType + " is not supported");
    }


    /**
     *  Adds a record to this location query. 
     *
     *  @param locationQueryInspectorRecord location query inspector
     *         record
     *  @param locationRecordType location record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addLocationQueryInspectorRecord(org.osid.mapping.records.LocationQueryInspectorRecord locationQueryInspectorRecord, 
                                                   org.osid.type.Type locationRecordType) {

        addRecordType(locationRecordType);
        nullarg(locationRecordType, "location record type");
        this.records.add(locationQueryInspectorRecord);        
        return;
    }
}
