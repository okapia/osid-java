//
// AbstractSpeedZoneEnabler.java
//
//     Defines a SpeedZoneEnabler builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.mapping.path.rules.speedzoneenabler.spi;


/**
 *  Defines a <code>SpeedZoneEnabler</code> builder.
 */

public abstract class AbstractSpeedZoneEnablerBuilder<T extends AbstractSpeedZoneEnablerBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidEnablerBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.mapping.path.rules.speedzoneenabler.SpeedZoneEnablerMiter speedZoneEnabler;


    /**
     *  Constructs a new <code>AbstractSpeedZoneEnablerBuilder</code>.
     *
     *  @param speedZoneEnabler the speed zone enabler to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractSpeedZoneEnablerBuilder(net.okapia.osid.jamocha.builder.mapping.path.rules.speedzoneenabler.SpeedZoneEnablerMiter speedZoneEnabler) {
        super(speedZoneEnabler);
        this.speedZoneEnabler = speedZoneEnabler;
        return;
    }


    /**
     *  Builds the speed zone enabler.
     *
     *  @return the new speed zone enabler
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.mapping.path.rules.SpeedZoneEnabler build() {
        (new net.okapia.osid.jamocha.builder.validator.mapping.path.rules.speedzoneenabler.SpeedZoneEnablerValidator(getValidations())).validate(this.speedZoneEnabler);
        return (new net.okapia.osid.jamocha.builder.mapping.path.rules.speedzoneenabler.ImmutableSpeedZoneEnabler(this.speedZoneEnabler));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the speed zone enabler miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.mapping.path.rules.speedzoneenabler.SpeedZoneEnablerMiter getMiter() {
        return (this.speedZoneEnabler);
    }


    /**
     *  Adds a SpeedZoneEnabler record.
     *
     *  @param record a speed zone enabler record
     *  @param recordType the type of speed zone enabler record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.mapping.path.rules.records.SpeedZoneEnablerRecord record, org.osid.type.Type recordType) {
        getMiter().addSpeedZoneEnablerRecord(record, recordType);
        return (self());
    }
}       


