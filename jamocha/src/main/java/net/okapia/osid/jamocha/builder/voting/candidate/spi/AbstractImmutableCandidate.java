//
// AbstractImmutableCandidate.java
//
//     Wraps a mutable Candidate to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.voting.candidate.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Candidate</code> to hide modifiers. This
 *  wrapper provides an immutized Candidate from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying candidate whose state changes are visible.
 */

public abstract class AbstractImmutableCandidate
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRelationship
    implements org.osid.voting.Candidate {

    private final org.osid.voting.Candidate candidate;


    /**
     *  Constructs a new <code>AbstractImmutableCandidate</code>.
     *
     *  @param candidate the candidate to immutablize
     *  @throws org.osid.NullArgumentException <code>candidate</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableCandidate(org.osid.voting.Candidate candidate) {
        super(candidate);
        this.candidate = candidate;
        return;
    }


    /**
     *  Gets the race <code> Id </code> of the candidate. 
     *
     *  @return the candidate <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getRaceId() {
        return (this.candidate.getRaceId());
    }


    /**
     *  Gets the race of the candidate. 
     *
     *  @return the race 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.voting.Race getRace()
        throws org.osid.OperationFailedException {

        return (this.candidate.getRace());
    }


    /**
     *  Gets the resource <code> Id </code> related to the candidate. 
     *
     *  @return the resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getResourceId() {
        return (this.candidate.getResourceId());
    }


    /**
     *  Gets the resource related to the candidate. 
     *
     *  @return the resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getResource()
        throws org.osid.OperationFailedException {

        return (this.candidate.getResource());
    }


    /**
     *  Gets the candidate record corresponding to the given <code> Candidate 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record. The <code> 
     *  candidateRecordType </code> may be the <code> Type </code> returned in 
     *  <code> getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(candidateRecordType) 
     *  </code> is <code> true </code> . 
     *
     *  @param  candidateRecordType the candidate record type 
     *  @return the candidate record 
     *  @throws org.osid.NullArgumentException <code> candidateRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(candidateRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.records.CandidateRecord getCandidateRecord(org.osid.type.Type candidateRecordType)
        throws org.osid.OperationFailedException {

        return (this.candidate.getCandidateRecord(candidateRecordType));
    }
}

