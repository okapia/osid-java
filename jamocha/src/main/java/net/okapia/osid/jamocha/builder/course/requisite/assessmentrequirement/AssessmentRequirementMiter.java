//
// AssessmentRequirementMiter.java
//
//     Defines an AssessmentRequirement miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.requisite.assessmentrequirement;


/**
 *  Defines an <code>AssessmentRequirement</code> miter for use with the builders.
 */

public interface AssessmentRequirementMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidRuleMiter,
            org.osid.course.requisite.AssessmentRequirement {


    /**
     *  Adds an alternative requisite.
     *
     *  @param requisite an alternative requisite
     *  @throws org.osid.NullArgumentException <code>requisite</code>
     *          is <code>null</code>
     */

    public void addAltRequisite(org.osid.course.requisite.Requisite requisite);


    /**
     *  Sets all the alternative requisites.
     *
     *  @param requisites a collection of alternative requisites
     *  @throws org.osid.NullArgumentException <code>requisites</code>
     *          is <code>null</code>
     */

    public void setAltRequisites(java.util.Collection<org.osid.course.requisite.Requisite> requisites);


    /**
     *  Sets the assessment.
     *
     *  @param assessment an assessment
     *  @throws org.osid.NullArgumentException
     *          <code>assessment</code> is <code>null</code>
     */

    public void setAssessment(org.osid.assessment.Assessment assessment);


    /**
     *  Sets the timeframe.
     *
     *  @param timeframe a timeframe
     *  @throws org.osid.NullArgumentException
     *          <code>timeframe</code> is <code>null</code>
     */

    public void setTimeframe(org.osid.calendaring.Duration timeframe);


    /**
     *  Sets the minimum grade.
     *
     *  @param grade a minimum grade
     *  @throws org.osid.NullArgumentException <code>grade</code> is
     *          <code>null</code>
     */

    public void setMinimumGrade(org.osid.grading.Grade grade);


    /**
     *  Sets the minimum score system.
     *
     *  @param system a minimum score system
     *  @throws org.osid.NullArgumentException <code>system</code> is
     *          <code>null</code>
     */

    public void setMinimumScoreSystem(org.osid.grading.GradeSystem system);


    /**
     *  Sets the minimum score.
     *
     *  @param score a minimum score
     *  @throws org.osid.NullArgumentException <code>score</code> is
     *          <code>null</code>
     */

    public void setMinimumScore(java.math.BigDecimal score);


    /**
     *  Adds an AssessmentRequirement record.
     *
     *  @param record an assessmentRequirement record
     *  @param recordType the type of assessmentRequirement record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addAssessmentRequirementRecord(org.osid.course.requisite.records.AssessmentRequirementRecord record, org.osid.type.Type recordType);
}       


