//
// LessonFilterList.java
//
//     Implements a filtering LessonList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.course.plan.lesson;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filtering LessonList.
 */

public final class LessonFilterList
    extends net.okapia.osid.jamocha.inline.filter.course.plan.lesson.spi.AbstractLessonFilterList
    implements org.osid.course.plan.LessonList,
               LessonFilter {

    private final LessonFilter filter;


    /**
     *  Creates a new <code>LessonFilterList</code>.
     *
     *  @param filter an inline query filter
     *  @param list a <code>LessonList</code>
     *  @throws org.osid.NullArgumentException <code>filter</code> or
     *          <code>list</code> is <code>null</code>
     */

    public LessonFilterList(LessonFilter filter, org.osid.course.plan.LessonList list) {
        super(list);

        nullarg(filter, "inline query filter");
        this.filter = filter;

        return;
    }    

    
    /**
     *  Filters Lessons.
     *
     *  @param lesson the lesson to filter
     *  @return <code>true</code> if the lesson passes the filter,
     *          <code>false</code> if the lesson should be filtered
     */

    @Override
    public boolean pass(org.osid.course.plan.Lesson lesson) {
        return (this.filter.pass(lesson));
    }
}
