//
// AbstractSceneSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.scene.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractSceneSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.control.SceneSearchResults {

    private org.osid.control.SceneList scenes;
    private final org.osid.control.SceneQueryInspector inspector;
    private final java.util.Collection<org.osid.control.records.SceneSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractSceneSearchResults.
     *
     *  @param scenes the result set
     *  @param sceneQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>scenes</code>
     *          or <code>sceneQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractSceneSearchResults(org.osid.control.SceneList scenes,
                                            org.osid.control.SceneQueryInspector sceneQueryInspector) {
        nullarg(scenes, "scenes");
        nullarg(sceneQueryInspector, "scene query inspectpr");

        this.scenes = scenes;
        this.inspector = sceneQueryInspector;

        return;
    }


    /**
     *  Gets the scene list resulting from a search.
     *
     *  @return a scene list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.control.SceneList getScenes() {
        if (this.scenes == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.control.SceneList scenes = this.scenes;
        this.scenes = null;
	return (scenes);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.control.SceneQueryInspector getSceneQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  scene search record <code> Type. </code> This method must
     *  be used to retrieve a scene implementing the requested
     *  record.
     *
     *  @param sceneSearchRecordType a scene search 
     *         record type 
     *  @return the scene search
     *  @throws org.osid.NullArgumentException
     *          <code>sceneSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(sceneSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.SceneSearchResultsRecord getSceneSearchResultsRecord(org.osid.type.Type sceneSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.control.records.SceneSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(sceneSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(sceneSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record scene search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addSceneRecord(org.osid.control.records.SceneSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "scene record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
