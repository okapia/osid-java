//
// AbstractMessage.java
//
//     Defines a Message builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.messaging.message.spi;


/**
 *  Defines a <code>Message</code> builder.
 */

public abstract class AbstractMessageBuilder<T extends AbstractMessageBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.messaging.message.MessageMiter message;


    /**
     *  Constructs a new <code>AbstractMessageBuilder</code>.
     *
     *  @param message the message to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractMessageBuilder(net.okapia.osid.jamocha.builder.messaging.message.MessageMiter message) {
        super(message);
        this.message = message;
        return;
    }


    /**
     *  Builds the message.
     *
     *  @return the new message
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.messaging.Message build() {
        (new net.okapia.osid.jamocha.builder.validator.messaging.message.MessageValidator(getValidations())).validate(this.message);
        return (new net.okapia.osid.jamocha.builder.messaging.message.ImmutableMessage(this.message));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the message miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.messaging.message.MessageMiter getMiter() {
        return (this.message);
    }


    /**
     *  Sets the subject line.
     *
     *  @param subjectLine a subject line
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>subjectLine</code> is <code>null</code>
     */

    public T subjectLine(org.osid.locale.DisplayText subjectLine) {
        getMiter().setSubjectLine(subjectLine);
        return (self());
    }


    /**
     *  Sets the text.
     *
     *  @param text a text
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>text</code> is
     *          <code>null</code>
     */

    public T text(org.osid.locale.DisplayText text) {
        getMiter().setText(text);
        return (self());
    }


    /**
     *  Sets the sent time.
     *
     *  @param time a sent time
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    public T sentTime(org.osid.calendaring.DateTime time) {
        getMiter().setSentTime(time);
        return (self());
    }


    /**
     *  Sets the sender.
     *
     *  @param sender a sender
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>sender</code> is <code>null</code>
     */

    public T sender(org.osid.resource.Resource sender) {
        getMiter().setSender(sender);
        return (self());
    }


    /**
     *  Sets the sending agent.
     *
     *  @param agent a sending agent
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    public T sendingAgent(org.osid.authentication.Agent agent) {
        getMiter().setSendingAgent(agent);
        return (self());
    }


    /**
     *  Sets the received time.
     *
     *  @param time a received time
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    public T receivedTime(org.osid.calendaring.DateTime time) {
        getMiter().setReceivedTime(time);
        return (self());
    }


    /**
     *  Adds a recipient.
     *
     *  @param recipient a recipient
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>recipient</code> is <code>null</code>
     */

    public T recipient(org.osid.resource.Resource recipient) {
        getMiter().addRecipient(recipient);
        return (self());
    }


    /**
     *  Sets all the recipients.
     *
     *  @param recipients a collection of recipients
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>recipients</code> is <code>null</code>
     */

    public T recipients(java.util.Collection<org.osid.resource.Resource> recipients) {
        getMiter().setRecipients(recipients);
        return (self());
    }


    /**
     *  Sets the receipt.
     *
     *  @param receipt a receipt
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>receipt</code> is <code>null</code>
     */

    public T receipt(org.osid.messaging.Receipt receipt) {
        getMiter().setReceipt(receipt);
        return (self());
    }


    /**
     *  Adds a Message record.
     *
     *  @param record a message record
     *  @param recordType the type of message record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.messaging.records.MessageRecord record, org.osid.type.Type recordType) {
        getMiter().addMessageRecord(record, recordType);
        return (self());
    }
}       


