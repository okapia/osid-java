//
// AbstractMapAddressBookLookupSession
//
//    A simple framework for providing an AddressBook lookup service
//    backed by a fixed collection of address books.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.contact.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an AddressBook lookup service backed by a
 *  fixed collection of address books. The address books are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>AddressBooks</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapAddressBookLookupSession
    extends net.okapia.osid.jamocha.contact.spi.AbstractAddressBookLookupSession
    implements org.osid.contact.AddressBookLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.contact.AddressBook> addressBooks = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.contact.AddressBook>());


    /**
     *  Makes an <code>AddressBook</code> available in this session.
     *
     *  @param  addressBook an address book
     *  @throws org.osid.NullArgumentException <code>addressBook<code>
     *          is <code>null</code>
     */

    protected void putAddressBook(org.osid.contact.AddressBook addressBook) {
        this.addressBooks.put(addressBook.getId(), addressBook);
        return;
    }


    /**
     *  Makes an array of address books available in this session.
     *
     *  @param  addressBooks an array of address books
     *  @throws org.osid.NullArgumentException <code>addressBooks<code>
     *          is <code>null</code>
     */

    protected void putAddressBooks(org.osid.contact.AddressBook[] addressBooks) {
        putAddressBooks(java.util.Arrays.asList(addressBooks));
        return;
    }


    /**
     *  Makes a collection of address books available in this session.
     *
     *  @param  addressBooks a collection of address books
     *  @throws org.osid.NullArgumentException <code>addressBooks<code>
     *          is <code>null</code>
     */

    protected void putAddressBooks(java.util.Collection<? extends org.osid.contact.AddressBook> addressBooks) {
        for (org.osid.contact.AddressBook addressBook : addressBooks) {
            this.addressBooks.put(addressBook.getId(), addressBook);
        }

        return;
    }


    /**
     *  Removes an AddressBook from this session.
     *
     *  @param  addressBookId the <code>Id</code> of the address book
     *  @throws org.osid.NullArgumentException <code>addressBookId<code> is
     *          <code>null</code>
     */

    protected void removeAddressBook(org.osid.id.Id addressBookId) {
        this.addressBooks.remove(addressBookId);
        return;
    }


    /**
     *  Gets the <code>AddressBook</code> specified by its <code>Id</code>.
     *
     *  @param  addressBookId <code>Id</code> of the <code>AddressBook</code>
     *  @return the addressBook
     *  @throws org.osid.NotFoundException <code>addressBookId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>addressBookId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressBook getAddressBook(org.osid.id.Id addressBookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.contact.AddressBook addressBook = this.addressBooks.get(addressBookId);
        if (addressBook == null) {
            throw new org.osid.NotFoundException("addressBook not found: " + addressBookId);
        }

        return (addressBook);
    }


    /**
     *  Gets all <code>AddressBooks</code>. In plenary mode, the returned
     *  list contains all known addressBooks or an error
     *  results. Otherwise, the returned list may contain only those
     *  addressBooks that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>AddressBooks</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressBookList getAddressBooks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.contact.addressbook.ArrayAddressBookList(this.addressBooks.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.addressBooks.clear();
        super.close();
        return;
    }
}
