//
// AbstractLessonValidator.java
//
//     Validates a Lesson.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.course.plan.lesson.spi;


/**
 *  Validates a Lesson.
 */

public abstract class AbstractLessonValidator
    extends net.okapia.osid.jamocha.builder.validator.spi.AbstractOsidRelationshipValidator {


    /**
     *  Constructs a new <code>AbstractLessonValidator</code>.
     */

    protected AbstractLessonValidator() {
        return;
    }


    /**
     *  Constructs a new <code>AbstractLessonValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    protected AbstractLessonValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates a Lesson.
     *
     *  @param lesson a lesson to validate
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>lesson</code>
     *          is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in
     *          assembly
     */
    
    public void validate(org.osid.course.plan.Lesson lesson) {
        super.validate(lesson);

        testNestedObject(lesson, "getPlan");
        testNestedObject(lesson, "getDocet");
        testNestedObjects(lesson, "getActivityIds", "getActivities");
        test(lesson.getPlannedStartTime(), "getPlannedStartTime()");

        testConditionalMethod(lesson, "getActualStartTime", lesson.hasBegun(), "hasBegun()");
        testConditionalMethod(lesson, "getActualStartingActivityId", lesson.hasBegun(), "hasBegun()");
        testConditionalMethod(lesson, "getActualStartingActivity", lesson.hasBegun(), "hasBegun()");
        testConditionalMethod(lesson, "getActualTimeSpent", lesson.hasBegun(), "hasBegun()");
        if (lesson.hasBegun()) {
            testNestedObject(lesson, "getActualStartingActivity");
        }
        
        try {
            if (lesson.isComplete() && !lesson.hasBegun()) {
                throw new org.osid.BadLogicException("how can the lesson be over yet?");
            }
        } catch (org.osid.IllegalStateException ise) {}

        if (lesson.hasBegun()) {
            testConditionalMethod(lesson, "getActualEndTime", lesson.isComplete(), "isComplete()");
            testConditionalMethod(lesson, "getActualEndingActivityId", lesson.isComplete(), "isComplete()");
            testConditionalMethod(lesson, "getActualEndingActivity", lesson.isComplete(), "isComplete()");
            if (lesson.isComplete()) {
                testNestedObject(lesson, "getActualStartingActivity");
            }
        }

        return;
    }
}
