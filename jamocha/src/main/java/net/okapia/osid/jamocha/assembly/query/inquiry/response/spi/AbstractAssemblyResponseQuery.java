//
// AbstractAssemblyResponseQuery.java
//
//     A ResponseQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.inquiry.response.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ResponseQuery that stores terms.
 */

public abstract class AbstractAssemblyResponseQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRelationshipQuery
    implements org.osid.inquiry.ResponseQuery,
               org.osid.inquiry.ResponseQueryInspector,
               org.osid.inquiry.ResponseSearchOrder {

    private final java.util.Collection<org.osid.inquiry.records.ResponseQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.inquiry.records.ResponseQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.inquiry.records.ResponseSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyResponseQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyResponseQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the inquiry <code> Id </code> for this query. 
     *
     *  @param  inquiryId the inquiry <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> inquiryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchInquiryId(org.osid.id.Id inquiryId, boolean match) {
        getAssembler().addIdTerm(getInquiryIdColumn(), inquiryId, match);
        return;
    }


    /**
     *  Clears the inquiry Id query terms. 
     */

    @OSID @Override
    public void clearInquiryIdTerms() {
        getAssembler().clearTerms(getInquiryIdColumn());
        return;
    }


    /**
     *  Gets the inquiry resource <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getInquiryIdTerms() {
        return (getAssembler().getIdTerms(getInquiryIdColumn()));
    }


    /**
     *  Gets the InquiryId column name.
     *
     * @return the column name
     */

    protected String getInquiryIdColumn() {
        return ("inquiryt_id");
    }


    /**
     *  Tests if an <code> InquiryQuery </code> is available. 
     *
     *  @return <code> true </code> if an inquiry query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquiryQuery() {
        return (false);
    }


    /**
     *  Gets the query for an inquiry. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the inquiry query 
     *  @throws org.osid.UnimplementedException <code> supportsInquiryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryQuery getInquiryQuery() {
        throw new org.osid.UnimplementedException("supportsInquiryQuery() is false");
    }


    /**
     *  Clears the inquiry query terms. 
     */

    @OSID @Override
    public void clearInquiryTerms() {
        getAssembler().clearTerms(getInquiryColumn());
        return;
    }


    /**
     *  Gets the inquiry query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryQueryInspector[] getInquiryTerms() {
        return (new org.osid.inquiry.InquiryQueryInspector[0]);
    }


    /**
     *  Tests if an inquiry search order is available. 
     *
     *  @return <code> true </code> if an inquiry search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquirySearchOrder() {
        return (false);
    }


    /**
     *  Gets the inquiry search order. 
     *
     *  @return the inquiry search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsInquirySearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquirySearchOrder getInquirySearchOrder() {
        throw new org.osid.UnimplementedException("supportsInquirySearchOrder() is false");
    }


    /**
     *  Orders the results by inquiry resource. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByInquiry(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getInquiryColumn(), style);
        return;
    }


    /**
     *  Gets the Inquiry column name.
     *
     * @return the column name
     */

    protected String getInquiryColumn() {
        return ("inquiry");
    }


    /**
     *  Sets the responder resource <code> Id </code> for this query. 
     *
     *  @param  resourceId the resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResponderId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getResponderIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the responder resource <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearResponderIdTerms() {
        getAssembler().clearTerms(getResponderIdColumn());
        return;
    }


    /**
     *  Gets the responder resource <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResponderIdTerms() {
        return (getAssembler().getIdTerms(getResponderIdColumn()));
    }


    /**
     *  Orders the results by responder resource. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByResponder(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getResponderColumn(), style);
        return;
    }


    /**
     *  Gets the ResponderId column name.
     *
     * @return the column name
     */

    protected String getResponderIdColumn() {
        return ("responder_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResponderQuery() {
        return (false);
    }


    /**
     *  Gets the query for a responder resource. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResponderQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getResponderQuery() {
        throw new org.osid.UnimplementedException("supportsResponderQuery() is false");
    }


    /**
     *  Clears the responder resource query terms. 
     */

    @OSID @Override
    public void clearResponderTerms() {
        getAssembler().clearTerms(getResponderColumn());
        return;
    }


    /**
     *  Gets the responder resource query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getResponderTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a responder resource search order is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResponderSearchOrder() {
        return (false);
    }


    /**
     *  Gets the responder resource search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsResponderSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getResponderSearchOrder() {
        throw new org.osid.UnimplementedException("supportsResponderSearchOrder() is false");
    }


    /**
     *  Gets the Responder column name.
     *
     * @return the column name
     */

    protected String getResponderColumn() {
        return ("responder");
    }


    /**
     *  Sets the responding agent <code> Id </code> for this query. 
     *
     *  @param  agentId the agent <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRespondingAgentId(org.osid.id.Id agentId, boolean match) {
        getAssembler().addIdTerm(getRespondingAgentIdColumn(), agentId, match);
        return;
    }


    /**
     *  Clears the responding agent <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRespondingAgentIdTerms() {
        getAssembler().clearTerms(getRespondingAgentIdColumn());
        return;
    }


    /**
     *  Gets the responding agent <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRespondingAgentIdTerms() {
        return (getAssembler().getIdTerms(getRespondingAgentIdColumn()));
    }


    /**
     *  Orders the results by responding agent. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRespondingAgent(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getRespondingAgentColumn(), style);
        return;
    }


    /**
     *  Gets the RespondingAgentId column name.
     *
     * @return the column name
     */

    protected String getRespondingAgentIdColumn() {
        return ("responding_agent_id");
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRespondingAgentQuery() {
        return (false);
    }


    /**
     *  Gets the query for a responding agent. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRespondingAgentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getRespondingAgentQuery() {
        throw new org.osid.UnimplementedException("supportsRespondingAgentQuery() is false");
    }


    /**
     *  Clears the responding agent query terms. 
     */

    @OSID @Override
    public void clearRespondingAgentTerms() {
        getAssembler().clearTerms(getRespondingAgentColumn());
        return;
    }


    /**
     *  Gets the responding agent query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authentication.AgentQueryInspector[] getRespondingAgentTerms() {
        return (new org.osid.authentication.AgentQueryInspector[0]);
    }


    /**
     *  Tests if a responding agent search order is available. 
     *
     *  @return <code> true </code> if an agent search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRespondingAgentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the responding agent search order. 
     *
     *  @return the agent search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsRespondingAgentSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getRespondingAgentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsRespondingAgentSearchOrder() is false");
    }


    /**
     *  Gets the RespondingAgent column name.
     *
     * @return the column name
     */

    protected String getRespondingAgentColumn() {
        return ("responding_agent");
    }


    /**
     *  Matches affirmative responses. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAffirmative(boolean match) {
        getAssembler().addBooleanTerm(getAffirmativeColumn(), match);
        return;
    }


    /**
     *  Clears the affirmative query terms. 
     */

    @OSID @Override
    public void clearAffirmativeTerms() {
        getAssembler().clearTerms(getAffirmativeColumn());
        return;
    }


    /**
     *  Gets the affirmative response query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getAffirmativeTerms() {
        return (getAssembler().getBooleanTerms(getAffirmativeColumn()));
    }


    /**
     *  Orders the results by affirmative responses. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAffirmative(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getAffirmativeColumn(), style);
        return;
    }


    /**
     *  Gets the Affirmative column name.
     *
     * @return the column name
     */

    protected String getAffirmativeColumn() {
        return ("affirmative");
    }


    /**
     *  Sets the inquest <code> Id </code> for this query to match audits 
     *  assigned to inquests. 
     *
     *  @param  inquestId the inquest <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchInquestId(org.osid.id.Id inquestId, boolean match) {
        getAssembler().addIdTerm(getInquestIdColumn(), inquestId, match);
        return;
    }


    /**
     *  Clears the inquest <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearInquestIdTerms() {
        getAssembler().clearTerms(getInquestIdColumn());
        return;
    }


    /**
     *  Gets the inquest <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getInquestIdTerms() {
        return (getAssembler().getIdTerms(getInquestIdColumn()));
    }


    /**
     *  Gets the InquestId column name.
     *
     * @return the column name
     */

    protected String getInquestIdColumn() {
        return ("inquest_id");
    }


    /**
     *  Tests if an <code> InquestQuery </code> is available. 
     *
     *  @return <code> true </code> if an inquest query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquestQuery() {
        return (false);
    }


    /**
     *  Gets the query for an inquest. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the inquest query 
     *  @throws org.osid.UnimplementedException <code> supportsInquestQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquestQuery getInquestQuery() {
        throw new org.osid.UnimplementedException("supportsInquestQuery() is false");
    }


    /**
     *  Clears the inquest query terms. 
     */

    @OSID @Override
    public void clearInquestTerms() {
        getAssembler().clearTerms(getInquestColumn());
        return;
    }


    /**
     *  Gets the inquest query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.inquiry.InquestQueryInspector[] getInquestTerms() {
        return (new org.osid.inquiry.InquestQueryInspector[0]);
    }


    /**
     *  Gets the Inquest column name.
     *
     * @return the column name
     */

    protected String getInquestColumn() {
        return ("inquest");
    }


    /**
     *  Tests if this response supports the given record
     *  <code>Type</code>.
     *
     *  @param  responseRecordType a response record type 
     *  @return <code>true</code> if the responseRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>responseRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type responseRecordType) {
        for (org.osid.inquiry.records.ResponseQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(responseRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  responseRecordType the response record type 
     *  @return the response query record 
     *  @throws org.osid.NullArgumentException
     *          <code>responseRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(responseRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inquiry.records.ResponseQueryRecord getResponseQueryRecord(org.osid.type.Type responseRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inquiry.records.ResponseQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(responseRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(responseRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  responseRecordType the response record type 
     *  @return the response query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>responseRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(responseRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inquiry.records.ResponseQueryInspectorRecord getResponseQueryInspectorRecord(org.osid.type.Type responseRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inquiry.records.ResponseQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(responseRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(responseRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param responseRecordType the response record type
     *  @return the response search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>responseRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(responseRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inquiry.records.ResponseSearchOrderRecord getResponseSearchOrderRecord(org.osid.type.Type responseRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inquiry.records.ResponseSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(responseRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(responseRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this response. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param responseQueryRecord the response query record
     *  @param responseQueryInspectorRecord the response query inspector
     *         record
     *  @param responseSearchOrderRecord the response search order record
     *  @param responseRecordType response record type
     *  @throws org.osid.NullArgumentException
     *          <code>responseQueryRecord</code>,
     *          <code>responseQueryInspectorRecord</code>,
     *          <code>responseSearchOrderRecord</code> or
     *          <code>responseRecordTyperesponse</code> is
     *          <code>null</code>
     */
            
    protected void addResponseRecords(org.osid.inquiry.records.ResponseQueryRecord responseQueryRecord, 
                                      org.osid.inquiry.records.ResponseQueryInspectorRecord responseQueryInspectorRecord, 
                                      org.osid.inquiry.records.ResponseSearchOrderRecord responseSearchOrderRecord, 
                                      org.osid.type.Type responseRecordType) {

        addRecordType(responseRecordType);

        nullarg(responseQueryRecord, "response query record");
        nullarg(responseQueryInspectorRecord, "response query inspector record");
        nullarg(responseSearchOrderRecord, "response search odrer record");

        this.queryRecords.add(responseQueryRecord);
        this.queryInspectorRecords.add(responseQueryInspectorRecord);
        this.searchOrderRecords.add(responseSearchOrderRecord);
        
        return;
    }
}
