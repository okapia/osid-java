//
// AbstractProvisionSearchOdrer.java
//
//     Defines a ProvisionSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.provision.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code ProvisionSearchOrder}.
 */

public abstract class AbstractProvisionSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipSearchOrder
    implements org.osid.provisioning.ProvisionSearchOrder {

    private final java.util.Collection<org.osid.provisioning.records.ProvisionSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Orders the results by the broker. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByBroker(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a broker search order is available. 
     *
     *  @return <code> true </code> if a broker search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerSearchOrder() {
        return (false);
    }


    /**
     *  Gets the broker search order. 
     *
     *  @return the broker search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsBrokerSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerSearchOrder getBrokerSearchOrder() {
        throw new org.osid.UnimplementedException("supportsBrokerSearchOrder() is false");
    }


    /**
     *  Orders the results by the provisionable. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByProvisionable(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a provisionable search order is available. 
     *
     *  @return <code> true </code> if a provisionable search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisionableSearchOrder() {
        return (false);
    }


    /**
     *  Gets the provisionable search order. 
     *
     *  @return the provisionable search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsProvisionableSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionableSearchOrder getProvisionableSearchOrder() {
        throw new org.osid.UnimplementedException("supportsProvisionableSearchOrder() is false");
    }


    /**
     *  Orders the results by the recipient. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRecipient(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a resource search order is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecipientSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsRecipientSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getRecipientSearchOrder() {
        throw new org.osid.UnimplementedException("supportsRecipientSearchOrder() is false");
    }


    /**
     *  Orders the results by the request. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRequest(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a request search order is available. 
     *
     *  @return <code> true </code> if a request search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequestSearchOrder() {
        return (false);
    }


    /**
     *  Gets the request search order. 
     *
     *  @return the request search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsRequestSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestSearchOrder getRequestSearchOrder() {
        throw new org.osid.UnimplementedException("supportsRequestSearchOrder() is false");
    }


    /**
     *  Orders the results by the provision date. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByProvisionDate(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by the leased flag. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByLeased(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by the must return flag. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByMustReturn(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by the due date. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDueDate(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by the cost. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCost(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by the rate amount and period. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRate(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by returned. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByProvisionReturn(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a provision return search order is available. 
     *
     *  @return <code> true </code> if a provision return search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisionReturnSearchOrder() {
        return (false);
    }


    /**
     *  Gets the provision return search order. 
     *
     *  @return the provision return search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsProvisionReturnSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionReturnSearchOrder getProvisionReturnSearchOrder() {
        throw new org.osid.UnimplementedException("supportsProvisionReturnSearchOrder() is false");
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  provisionRecordType a provision record type 
     *  @return {@code true} if the provisionRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code provisionRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type provisionRecordType) {
        for (org.osid.provisioning.records.ProvisionSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(provisionRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  provisionRecordType the provision record type 
     *  @return the provision search order record
     *  @throws org.osid.NullArgumentException
     *          {@code provisionRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(provisionRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.provisioning.records.ProvisionSearchOrderRecord getProvisionSearchOrderRecord(org.osid.type.Type provisionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.records.ProvisionSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(provisionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(provisionRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this provision. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param provisionRecord the provision search odrer record
     *  @param provisionRecordType provision record type
     *  @throws org.osid.NullArgumentException
     *          {@code provisionRecord} or
     *          {@code provisionRecordTypeprovision} is
     *          {@code null}
     */
            
    protected void addProvisionRecord(org.osid.provisioning.records.ProvisionSearchOrderRecord provisionSearchOrderRecord, 
                                     org.osid.type.Type provisionRecordType) {

        addRecordType(provisionRecordType);
        this.records.add(provisionSearchOrderRecord);
        
        return;
    }
}
