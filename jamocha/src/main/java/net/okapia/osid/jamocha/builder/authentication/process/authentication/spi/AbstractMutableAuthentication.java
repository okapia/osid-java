//
// AbstractMutableAuthentication.java
//
//     Defines a mutable Authentication.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.authentication.process.authentication.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>Authentication</code>.
 */

public abstract class AbstractMutableAuthentication
    extends net.okapia.osid.jamocha.authentication.process.authentication.spi.AbstractAuthentication
    implements org.osid.authentication.process.Authentication,
               net.okapia.osid.jamocha.builder.authentication.process.authentication.AuthenticationMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this authentication. 
     *
     *  @param record authentication record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addAuthenticationRecord(org.osid.authentication.process.records.AuthenticationRecord record, org.osid.type.Type recordType) {
        super.addAuthenticationRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }

    
    /**
     *  Sets the display name for this authentication.
     *
     *  @param displayName the name for this authentication
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this authentication.
     *
     *  @param description the description of this authentication
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException
     *          <code>genusType</code> is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the agent.
     *
     *  @param agent the authenticated agent
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    @Override
    public void setAgent(org.osid.authentication.Agent agent) {
        super.setAgent(agent);
        return;
    }


    /**
     *  Sets the valid flag.
     *
     *  @param valid <code> true </code> if this authentication
     *          credential is valid, <code> false </code> otherwise
     */

    @Override
    public void setValid(boolean valid) {
        super.setValid(valid);
        return;
    }


    /**
     *  Sets the expiration.
     *
     *  @param date the authenticated expiration
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    @Override
    public void setExpiration(java.util.Date date) {
        super.setExpiration(date);
        return;
    }


    /**
     *  Adds a credential.
     *
     *  @param credentialType the tyep of credential
     *  @param credential the authenticated credential
     *  @throws org.osid.NullArgumentException
     *          <code>credentialType</code> or <code>credential</code>
     *          is <code>null</code>
     */

    @Override
    public void addCredential(org.osid.type.Type credentialType, java.lang.Object credential) {
        super.addCredential(credentialType, credential);
        return;
    }
}

