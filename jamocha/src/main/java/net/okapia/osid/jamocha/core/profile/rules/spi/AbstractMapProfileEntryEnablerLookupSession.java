//
// AbstractMapProfileEntryEnablerLookupSession
//
//    A simple framework for providing a ProfileEntryEnabler lookup service
//    backed by a fixed collection of profile entry enablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.profile.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a ProfileEntryEnabler lookup service backed by a
 *  fixed collection of profile entry enablers. The profile entry enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>ProfileEntryEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapProfileEntryEnablerLookupSession
    extends net.okapia.osid.jamocha.profile.rules.spi.AbstractProfileEntryEnablerLookupSession
    implements org.osid.profile.rules.ProfileEntryEnablerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.profile.rules.ProfileEntryEnabler> profileEntryEnablers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.profile.rules.ProfileEntryEnabler>());


    /**
     *  Makes a <code>ProfileEntryEnabler</code> available in this session.
     *
     *  @param  profileEntryEnabler a profile entry enabler
     *  @throws org.osid.NullArgumentException <code>profileEntryEnabler<code>
     *          is <code>null</code>
     */

    protected void putProfileEntryEnabler(org.osid.profile.rules.ProfileEntryEnabler profileEntryEnabler) {
        this.profileEntryEnablers.put(profileEntryEnabler.getId(), profileEntryEnabler);
        return;
    }


    /**
     *  Makes an array of profile entry enablers available in this session.
     *
     *  @param  profileEntryEnablers an array of profile entry enablers
     *  @throws org.osid.NullArgumentException <code>profileEntryEnablers<code>
     *          is <code>null</code>
     */

    protected void putProfileEntryEnablers(org.osid.profile.rules.ProfileEntryEnabler[] profileEntryEnablers) {
        putProfileEntryEnablers(java.util.Arrays.asList(profileEntryEnablers));
        return;
    }


    /**
     *  Makes a collection of profile entry enablers available in this session.
     *
     *  @param  profileEntryEnablers a collection of profile entry enablers
     *  @throws org.osid.NullArgumentException <code>profileEntryEnablers<code>
     *          is <code>null</code>
     */

    protected void putProfileEntryEnablers(java.util.Collection<? extends org.osid.profile.rules.ProfileEntryEnabler> profileEntryEnablers) {
        for (org.osid.profile.rules.ProfileEntryEnabler profileEntryEnabler : profileEntryEnablers) {
            this.profileEntryEnablers.put(profileEntryEnabler.getId(), profileEntryEnabler);
        }

        return;
    }


    /**
     *  Removes a ProfileEntryEnabler from this session.
     *
     *  @param  profileEntryEnablerId the <code>Id</code> of the profile entry enabler
     *  @throws org.osid.NullArgumentException <code>profileEntryEnablerId<code> is
     *          <code>null</code>
     */

    protected void removeProfileEntryEnabler(org.osid.id.Id profileEntryEnablerId) {
        this.profileEntryEnablers.remove(profileEntryEnablerId);
        return;
    }


    /**
     *  Gets the <code>ProfileEntryEnabler</code> specified by its <code>Id</code>.
     *
     *  @param  profileEntryEnablerId <code>Id</code> of the <code>ProfileEntryEnabler</code>
     *  @return the profileEntryEnabler
     *  @throws org.osid.NotFoundException <code>profileEntryEnablerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>profileEntryEnablerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.rules.ProfileEntryEnabler getProfileEntryEnabler(org.osid.id.Id profileEntryEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.profile.rules.ProfileEntryEnabler profileEntryEnabler = this.profileEntryEnablers.get(profileEntryEnablerId);
        if (profileEntryEnabler == null) {
            throw new org.osid.NotFoundException("profileEntryEnabler not found: " + profileEntryEnablerId);
        }

        return (profileEntryEnabler);
    }


    /**
     *  Gets all <code>ProfileEntryEnablers</code>. In plenary mode, the returned
     *  list contains all known profileEntryEnablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  profileEntryEnablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>ProfileEntryEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.rules.ProfileEntryEnablerList getProfileEntryEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.profile.rules.profileentryenabler.ArrayProfileEntryEnablerList(this.profileEntryEnablers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.profileEntryEnablers.clear();
        super.close();
        return;
    }
}
