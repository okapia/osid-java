//
// RaceProcessorElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.rules.raceprocessor.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class RaceProcessorElements
    extends net.okapia.osid.jamocha.spi.OsidProcessorElements {


    /**
     *  Gets the RaceProcessorElement Id.
     *
     *  @return the race processor element Id
     */

    public static org.osid.id.Id getRaceProcessorEntityId() {
        return (makeEntityId("osid.voting.rules.RaceProcessor"));
    }


    /**
     *  Gets the MaximumWinners element Id.
     *
     *  @return the MaximumWinners element Id
     */

    public static org.osid.id.Id getMaximumWinners() {
        return (makeElementId("osid.voting.rules.raceprocessor.MaximumWinners"));
    }


    /**
     *  Gets the MinimumPercentageToWin element Id.
     *
     *  @return the MinimumPercentageToWin element Id
     */

    public static org.osid.id.Id getMinimumPercentageToWin() {
        return (makeElementId("osid.voting.rules.raceprocessor.MinimumPercentageToWin"));
    }


    /**
     *  Gets the MinimumVotesToWin element Id.
     *
     *  @return the MinimumVotesToWin element Id
     */

    public static org.osid.id.Id getMinimumVotesToWin() {
        return (makeElementId("osid.voting.rules.raceprocessor.MinimumVotesToWin"));
    }


    /**
     *  Gets the RuledRaceId element Id.
     *
     *  @return the RuledRaceId element Id
     */

    public static org.osid.id.Id getRuledRaceId() {
        return (makeQueryElementId("osid.voting.rules.raceprocessor.RuledRaceId"));
    }


    /**
     *  Gets the RuledRace element Id.
     *
     *  @return the RuledRace element Id
     */

    public static org.osid.id.Id getRuledRace() {
        return (makeQueryElementId("osid.voting.rules.raceprocessor.RuledRace"));
    }


    /**
     *  Gets the PollsId element Id.
     *
     *  @return the PollsId element Id
     */

    public static org.osid.id.Id getPollsId() {
        return (makeQueryElementId("osid.voting.rules.raceprocessor.PollsId"));
    }


    /**
     *  Gets the Polls element Id.
     *
     *  @return the Polls element Id
     */

    public static org.osid.id.Id getPolls() {
        return (makeQueryElementId("osid.voting.rules.raceprocessor.Polls"));
    }
}
