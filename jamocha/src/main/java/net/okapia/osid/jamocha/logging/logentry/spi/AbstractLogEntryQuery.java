//
// AbstractLogEntryQuery.java
//
//     A template for making a LogEntry Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.logging.logentry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for log entries.
 */

public abstract class AbstractLogEntryQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.logging.LogEntryQuery {

    private final java.util.Collection<org.osid.logging.records.LogEntryQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches a priority <code> Type </code> for the log entry. 
     *
     *  @param  priorityType <code> Type </code> to match 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> priorityType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPriority(org.osid.type.Type priorityType, boolean match) {
        return;
    }


    /**
     *  Matches log entries with any priority. 
     *
     *  @param  match <code> true </code> to match log entries with any 
     *          priority, <code> false </code> to match log entries with no 
     *          priority 
     */

    @OSID @Override
    public void matchAnyPriority(boolean match) {
        return;
    }


    /**
     *  Clears the priority terms. 
     */

    @OSID @Override
    public void clearPriorityTerms() {
        return;
    }


    /**
     *  Matches a log entries including and above the given priority type. 
     *
     *  @param  priorityType <code> Type </code> to match 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> priorityType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchMinimumPriority(org.osid.type.Type priorityType, 
                                     boolean match) {
        return;
    }


    /**
     *  Clears the minimum priority terms. 
     */

    @OSID @Override
    public void clearMinimumPriorityTerms() {
        return;
    }


    /**
     *  Matches the time of this log entry. 
     *
     *  @param  startTime start time 
     *  @param  endTime end time 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> startTime </code> is 
     *          greater than <code> endTime </code> 
     *  @throws org.osid.NullArgumentException <code> startTime </code> or 
     *          <code> endTime </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchTimestamp(org.osid.calendaring.DateTime startTime, 
                               org.osid.calendaring.DateTime endTime, 
                               boolean match) {
        return;
    }


    /**
     *  Clears the timestamp terms. 
     */

    @OSID @Override
    public void clearTimestampTerms() {
        return;
    }


    /**
     *  Matches a resource in this log entry. 
     *
     *  @param  resourceId <code> Id </code> to match 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResourceId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearResourceIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available for querying 
     *  agents. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsResourceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getResourceQuery() {
        throw new org.osid.UnimplementedException("supportsResourceQuery() is false");
    }


    /**
     *  Clears the resource terms. 
     */

    @OSID @Override
    public void clearResourceTerms() {
        return;
    }


    /**
     *  Matches an agent in this log entry. 
     *
     *  @param  agentId <code> Id </code> to match 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAgentId(org.osid.id.Id agentId, boolean match) {
        return;
    }


    /**
     *  Clears the agent <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAgentIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available for querying 
     *  agents. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgentQuery() {
        return (false);
    }


    /**
     *  Gets the query for an agent. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> supportsAgentQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getAgentQuery() {
        throw new org.osid.UnimplementedException("supportsAgentQuery() is false");
    }


    /**
     *  Clears the agent terms. 
     */

    @OSID @Override
    public void clearAgentTerms() {
        return;
    }


    /**
     *  Matches a log. 
     *
     *  @param  logId <code> Id </code> to match 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> logId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchLogId(org.osid.id.Id logId, boolean match) {
        return;
    }


    /**
     *  Clears the log <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearLogIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> LogQuery </code> is available for querying logs. 
     *
     *  @return <code> true </code> if a log query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a log. 
     *
     *  @return the log query 
     *  @throws org.osid.UnimplementedException <code> supportsLogQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogQuery getLogQuery() {
        throw new org.osid.UnimplementedException("supportsLogQuery() is false");
    }


    /**
     *  Clears the log terms. 
     */

    @OSID @Override
    public void clearLogTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given log entry query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a log entry implementing the requested record.
     *
     *  @param logEntryRecordType a log entry record type
     *  @return the log entry query record
     *  @throws org.osid.NullArgumentException
     *          <code>logEntryRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(logEntryRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.logging.records.LogEntryQueryRecord getLogEntryQueryRecord(org.osid.type.Type logEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.logging.records.LogEntryQueryRecord record : this.records) {
            if (record.implementsRecordType(logEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(logEntryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this log entry query. 
     *
     *  @param logEntryQueryRecord log entry query record
     *  @param logEntryRecordType logEntry record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addLogEntryQueryRecord(org.osid.logging.records.LogEntryQueryRecord logEntryQueryRecord, 
                                          org.osid.type.Type logEntryRecordType) {

        addRecordType(logEntryRecordType);
        nullarg(logEntryQueryRecord, "log entry query record");
        this.records.add(logEntryQueryRecord);        
        return;
    }
}
