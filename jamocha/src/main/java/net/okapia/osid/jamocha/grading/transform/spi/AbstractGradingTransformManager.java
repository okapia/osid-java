//
// AbstractGradingTransformManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.transform.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractGradingTransformManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.grading.transform.GradingTransformManager,
               org.osid.grading.transform.GradingTransformProxyManager {

    private final Types gradeSystemTransformRecordTypes    = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractGradingTransformManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractGradingTransformManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if a grade system transformation service is supported. 
     *
     *  @return true if grade system transformation is supported, false 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsGradeSystemTransformation() {
        return (false);
    }


    /**
     *  Tests if a grade system transform lookup service is supported. 
     *
     *  @return true if grade system transform lookup is supported, false 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsGradeSystemTransformLookup() {
        return (false);
    }


    /**
     *  Tests if a grade system transform admin service is supported. 
     *
     *  @return <code> true </code> if grade system transform admin is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeSystemTransformAdmin() {
        return (false);
    }


    /**
     *  Tests if a grade system transform notification service is supported. 
     *
     *  @return <code> true </code> if grade system transform notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeSystemTransformNotification() {
        return (false);
    }


    /**
     *  Gets the supported <code> GradeSystemTransform </code> record types. 
     *
     *  @return a list containing the supported <code> GradeSystemTransform 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getGradeSystemTransformRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.gradeSystemTransformRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> GradeSystemTransform </code> record type is 
     *  supported. 
     *
     *  @param  gradeSystemTransformRecordType a <code> Type </code> 
     *          indicating a <code> GradeSystemTransform </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          gradeSystemTransformRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsGradeSystemTransformRecordType(org.osid.type.Type gradeSystemTransformRecordType) {
        return (this.gradeSystemTransformRecordTypes.contains(gradeSystemTransformRecordType));
    }


    /**
     *  Adds support for a grade system transform record type.
     *
     *  @param gradeSystemTransformRecordType a grade system transform record type
     *  @throws org.osid.NullArgumentException
     *  <code>gradeSystemTransformRecordType</code> is <code>null</code>
     */

    protected void addGradeSystemTransformRecordType(org.osid.type.Type gradeSystemTransformRecordType) {
        this.gradeSystemTransformRecordTypes.add(gradeSystemTransformRecordType);
        return;
    }


    /**
     *  Removes support for a grade system transform record type.
     *
     *  @param gradeSystemTransformRecordType a grade system transform record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>gradeSystemTransformRecordType</code> is <code>null</code>
     */

    protected void removeGradeSystemTransformRecordType(org.osid.type.Type gradeSystemTransformRecordType) {
        this.gradeSystemTransformRecordTypes.remove(gradeSystemTransformRecordType);
        return;
    }


    /**
     *  Gets the session for transforming grades among grade systems. The 
     *  available transformations can be examined through the <code> 
     *  GradeSystemTransformLookupSession. </code> 
     *
     *  @param  sourceGradeSystemId the <code> Id </code> of the source grade 
     *          system 
     *  @param  targetGradeSystemId the <code> Id </code> of the target grade 
     *          system 
     *  @return a <code> GradeSystemTransformationSession </code> 
     *  @throws org.osid.NotFoundException no transform exists between <code> 
     *          souceGradebookId </code> and <code> targetGradeSystemId 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> sourceGradeSystemId 
     *          </code> or <code> targetGradeSystemIdId </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemTransformation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.transform.GradeSystemTransformationSession getGradeSystemTransformationSession(org.osid.id.Id sourceGradeSystemId, 
                                                                                                           org.osid.id.Id targetGradeSystemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.transform.GradingTransformManager.getGradeSystemTransformationSession not implemented");
    }


    /**
     *  Gets the session for transforming grades among grade systems. The 
     *  available transformations can be examined through the <code> 
     *  GradeSystemTransformLookupSession. </code> 
     *
     *  @param  sourceGradeSystemId the <code> Id </code> of the source grade 
     *          system 
     *  @param  targetGradeSystemId the <code> Id </code> of the target grade 
     *          system 
     *  @param  proxy a proxy 
     *  @return a <code> GradeSystemTransformationSession </code> 
     *  @throws org.osid.NotFoundException no transform exists between <code> 
     *          souceGradebookId </code> and <code> targetGradeSystemId 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> sourceGradeSystemId, 
     *          targetGradeSystemIdId </code> or <code> proxy </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemTransformation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.transform.GradeSystemTransformationSession getGradeSystemTransformationSession(org.osid.id.Id sourceGradeSystemId, 
                                                                                                           org.osid.id.Id targetGradeSystemId, 
                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.transform.GradingTransformProxyManager.getGradeSystemTransformationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade system 
     *  transform lookup service. 
     *
     *  @return a <code> GradeSystemTransformLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemTransformLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.transform.GradeSystemTransformLookupSession getGradeSystemTransformLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.transform.GradingTransformManager.getGradeSystemTransformLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade system 
     *  transform lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> GradeSystemTransformLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemTransformLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.transform.GradeSystemTransformLookupSession getGradeSystemTransformLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.transform.GradingTransformProxyManager.getGradeSystemTransformLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade system 
     *  transform lookup service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @return <code> a GradeSystemTransformLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemTransformLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.transform.GradeSystemTransformLookupSession getGradeSystemTransformLookupSessionForGradebook(org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.transform.GradingTransformManager.getGradeSystemTransformLookupSessionForGradebook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade system 
     *  transform lookup service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @param  proxy a proxy 
     *  @return <code> a GradeSystemTransformLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemTransformLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.transform.GradeSystemTransformLookupSession getGradeSystemTransformLookupSessionForGradebook(org.osid.id.Id gradebookId, 
                                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.transform.GradingTransformProxyManager.getGradeSystemTransformLookupSessionForGradebook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade system 
     *  transform admin service. 
     *
     *  @return a <code> GradeSystemTransformAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemTransformAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.transform.GradeSystemTransformAdminSession getGradeSystemTransformAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.transform.GradingTransformManager.getGradeSystemTransformAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade system 
     *  transform admin service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> GradeSystemTransformAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemTransformAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.transform.GradeSystemTransformAdminSession getGradeSystemTransformAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.transform.GradingTransformProxyManager.getGradeSystemTransformAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade system 
     *  transform admin service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @return <code> a GradeSystemTransformAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemTransformAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.transform.GradeSystemTransformAdminSession getGradeSystemTransformAdminSessionForGradebook(org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.transform.GradingTransformManager.getGradeSystemTransformAdminSessionForGradebook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade system 
     *  transform admin service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @param  proxy a proxy 
     *  @return <code> a GradeSystemTransformAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemTransformAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.transform.GradeSystemTransformAdminSession getGradeSystemTransformAdminSessionForGradebook(org.osid.id.Id gradebookId, 
                                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.transform.GradingTransformProxyManager.getGradeSystemTransformAdminSessionForGradebook not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to grade 
     *  system transform changes. 
     *
     *  @param  gradeSystemTransformReceiver the grade system transform 
     *          receiver interface 
     *  @return a <code> GradeSystemTransformNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> gradeSystemReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemTransformNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.grading.transform.GradeSystemTransformNotificationSession getGradeSystemTransformNotificationSession(org.osid.grading.transform.GradeSystemTransformReceiver gradeSystemTransformReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.transform.GradingTransformManager.getGradeSystemTransformNotificationSession not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to grade 
     *  system transform changes. 
     *
     *  @param  gradeSystemTransformReceiver the grade system transform 
     *          receiver interface 
     *  @param  proxy a proxy 
     *  @return a <code> GradeSystemTransformNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> gradeSystemReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemTransformNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.grading.transform.GradeSystemTransformNotificationSession getGradeSystemTransformNotificationSession(org.osid.grading.transform.GradeSystemTransformReceiver gradeSystemTransformReceiver, 
                                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.transform.GradingTransformProxyManager.getGradeSystemTransformNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade system 
     *  transform notification service for the given gradebook. 
     *
     *  @param  gradeSystemTransformReceiver the grade system transform 
     *          receiver interface 
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @return <code> a GradeSystemTransformNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradeSystemReceiver 
     *          </code> or <code> gradebookId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemTransformNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.transform.GradeSystemTransformNotificationSession getGradeSystemTransformNotificationSessionForGradebook(org.osid.grading.transform.GradeSystemTransformReceiver gradeSystemTransformReceiver, 
                                                                                                                                     org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.transform.GradingTransformManager.getGradeSystemTransformNotificationSessionForGradebook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade system 
     *  transform notification service for the given gradebook. 
     *
     *  @param  gradeSystemTransformReceiver the grade system transform 
     *          receiver interface 
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @param  proxy a proxy 
     *  @return <code> a GradeSystemTransformNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradeSystemReceiver, 
     *          gradebookId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemTransformNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.transform.GradeSystemTransformNotificationSession getGradeSystemTransformNotificationSessionForGradebook(org.osid.grading.transform.GradeSystemTransformReceiver gradeSystemTransformReceiver, 
                                                                                                                                     org.osid.id.Id gradebookId, 
                                                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.transform.GradingTransformProxyManager.getGradeSystemTransformNotificationSessionForGradebook not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.gradeSystemTransformRecordTypes.clear();
        this.gradeSystemTransformRecordTypes.clear();

        return;
    }
}
