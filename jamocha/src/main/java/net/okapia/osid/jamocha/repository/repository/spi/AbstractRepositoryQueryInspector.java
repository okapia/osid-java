//
// AbstractRepositoryQueryInspector.java
//
//     A template for making a RepositoryQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.repository.repository.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for repositories.
 */

public abstract class AbstractRepositoryQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQueryInspector
    implements org.osid.repository.RepositoryQueryInspector {

    private final java.util.Collection<org.osid.repository.records.RepositoryQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the asset <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAssetIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the asset query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.repository.AssetQueryInspector[] getAssetTerms() {
        return (new org.osid.repository.AssetQueryInspector[0]);
    }


    /**
     *  Gets the composition <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCompositionIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the composition query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.repository.CompositionQueryInspector[] getCompositionTerms() {
        return (new org.osid.repository.CompositionQueryInspector[0]);
    }


    /**
     *  Gets the ancestor repository <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorRepositoryIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor repository query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.repository.RepositoryQueryInspector[] getAncestorRepositoryTerms() {
        return (new org.osid.repository.RepositoryQueryInspector[0]);
    }


    /**
     *  Gets the descendant repository <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantRepositoryIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant repository query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.repository.RepositoryQueryInspector[] getDescendantRepositoryTerms() {
        return (new org.osid.repository.RepositoryQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given repository query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a repository implementing the requested record.
     *
     *  @param repositoryRecordType a repository record type
     *  @return the repository query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>repositoryRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(repositoryRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.repository.records.RepositoryQueryInspectorRecord getRepositoryQueryInspectorRecord(org.osid.type.Type repositoryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.repository.records.RepositoryQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(repositoryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(repositoryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this repository query. 
     *
     *  @param repositoryQueryInspectorRecord repository query inspector
     *         record
     *  @param repositoryRecordType repository record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRepositoryQueryInspectorRecord(org.osid.repository.records.RepositoryQueryInspectorRecord repositoryQueryInspectorRecord, 
                                                   org.osid.type.Type repositoryRecordType) {

        addRecordType(repositoryRecordType);
        nullarg(repositoryRecordType, "repository record type");
        this.records.add(repositoryQueryInspectorRecord);        
        return;
    }
}
