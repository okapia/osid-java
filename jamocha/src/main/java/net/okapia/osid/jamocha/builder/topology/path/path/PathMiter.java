//
// PathMiter.java
//
//     Defines a Path miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.topology.path.path;


/**
 *  Defines a <code>Path</code> miter for use with the builders.
 */

public interface PathMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidRelationshipMiter,
            org.osid.topology.path.Path {


    /**
     *  Sets the starting node.
     *
     *  @param node a starting node
     *  @throws org.osid.NullArgumentException <code>node</code> is
     *          <code>null</code>
     */

    public void setStartingNode(org.osid.topology.Node node);


    /**
     *  Sets the ending node.
     *
     *  @param node an ending node
     *  @throws org.osid.NullArgumentException <code>node</code> is
     *          <code>null</code>
     */

    public void setEndingNode(org.osid.topology.Node node);


    /**
     *  Sets the hops.
     *
     *  @param hop a hops
     *  @throws org.osid.InvalidArgumentException <code>hop</code> is
     *          negative
     */

    public void setHops(long hop);


    /**
     *  Sets the distance.
     *
     *  @param distance a distance
     *  @throws org.osid.NullArgumentException <code>distance</code>
     *          is <code>null</code>
     */

    public void setDistance(java.math.BigDecimal distance);


    /**
     *  Sets the cost.
     *
     *  @param cost a cost
     *  @throws org.osid.NullArgumentException <code>cost</code> is
     *          <code>null</code>
     */

    public void setCost(java.math.BigDecimal cost);


    /**
     *  Adds an edge.
     *
     *  @param edge an edge
     *  @throws org.osid.NullArgumentException <code>edge</code> is
     *          <code>null</code>
     */

    public void addEdge(org.osid.topology.Edge edge);


    /**
     *  Sets all the edges.
     *
     *  @param edges a collection of edges
     *  @throws org.osid.NullArgumentException <code>edges</code> is
     *          <code>null</code>
     */

    public void setEdges(java.util.Collection<org.osid.topology.Edge> edges);


    /**
     *  Adds a Path record.
     *
     *  @param record a path record
     *  @param recordType the type of path record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addPathRecord(org.osid.topology.path.records.PathRecord record, org.osid.type.Type recordType);
}       


