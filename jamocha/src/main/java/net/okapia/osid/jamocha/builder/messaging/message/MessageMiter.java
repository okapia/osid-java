//
// MessageMiter.java
//
//     Defines a Message miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.messaging.message;


/**
 *  Defines a <code>Message</code> miter for use with the builders.
 */

public interface MessageMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidObjectMiter,
            org.osid.messaging.Message {


    /**
     *  Sets the subject line.
     *
     *  @param subjectLine a subject line
     *  @throws org.osid.NullArgumentException
     *          <code>subjectLine</code> is <code>null</code>
     */

    public void setSubjectLine(org.osid.locale.DisplayText subjectLine);


    /**
     *  Sets the text.
     *
     *  @param text a text
     *  @throws org.osid.NullArgumentException <code>text</code> is
     *          <code>null</code>
     */

    public void setText(org.osid.locale.DisplayText text);


    /**
     *  Sets the sent time.
     *
     *  @param time a sent time
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    public void setSentTime(org.osid.calendaring.DateTime time);


    /**
     *  Sets the sender.
     *
     *  @param sender a sender
     *  @throws org.osid.NullArgumentException <code>sender</code> is
     *          <code>null</code>
     */

    public void setSender(org.osid.resource.Resource sender);


    /**
     *  Sets the sending agent.
     *
     *  @param agent a sending agent
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    public void setSendingAgent(org.osid.authentication.Agent agent);


    /**
     *  Sets the received time.
     *
     *  @param time a received time
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    public void setReceivedTime(org.osid.calendaring.DateTime time);


    /**
     *  Adds a recipient.
     *
     *  @param recipient a recipient
     *  @throws org.osid.NullArgumentException <code>recipient</code>
     *          is <code>null</code>
     */

    public void addRecipient(org.osid.resource.Resource recipient);


    /**
     *  Sets all the recipients.
     *
     *  @param recipients a collection of recipients
     *  @throws org.osid.NullArgumentException <code>recipients</code>
     *          is <code>null</code>
     */

    public void setRecipients(java.util.Collection<org.osid.resource.Resource> recipients);


    /**
     *  Sets the receipt.
     *
     *  @param receipt a receipt
     *  @throws org.osid.NullArgumentException <code>receipt</code> is
     *          <code>null</code>
     */

    public void setReceipt(org.osid.messaging.Receipt receipt);


    /**
     *  Adds a Message record.
     *
     *  @param record a message record
     *  @param recordType the type of message record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addMessageRecord(org.osid.messaging.records.MessageRecord record, org.osid.type.Type recordType);
}       


