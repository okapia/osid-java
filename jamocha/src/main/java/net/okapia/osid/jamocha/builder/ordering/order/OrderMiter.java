//
// OrderMiter.java
//
//     Defines an Order miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.ordering.order;


/**
 *  Defines an <code>Order</code> miter for use with the builders.
 */

public interface OrderMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidObjectMiter,
            org.osid.ordering.Order {


    /**
     *  Sets the customer.
     *
     *  @param customer a customer
     *  @throws org.osid.NullArgumentException <code>customer</code>
     *          is <code>null</code>
     */

    public void setCustomer(org.osid.resource.Resource customer);


    /**
     *  Adds an item.
     *
     *  @param item an item
     *  @throws org.osid.NullArgumentException <code>item</code> is
     *          <code>null</code>
     */

    public void addItem(org.osid.ordering.Item item);


    /**
     *  Sets all the items.
     *
     *  @param items a collection of items
     *  @throws org.osid.NullArgumentException <code>items</code> is
     *          <code>null</code>
     */

    public void setItems(java.util.Collection<org.osid.ordering.Item> items);


    /**
     *  Sets the total cost.
     *
     *  @param cost a total cost
     *  @throws org.osid.NullArgumentException <code>cost</code> is
     *          <code>null</code>
     */

    public void setTotalCost(org.osid.financials.Currency cost);


    /**
     *  Sets the atomic flag.
     *
     *  @param atomic <code> true </code> if the order is atomic,
     *         <code> false </code> otherwise
     */

    public void setAtomic(boolean atomic);


    /**
     *  Sets the submit date.
     *
     *  @param date a submit date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public void setSubmitDate(org.osid.calendaring.DateTime date);


    /**
     *  Sets the submitter.
     *
     *  @param resource the submitter
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    public void setSubmitter(org.osid.resource.Resource resource);


    /**
     *  Sets the submitting agent.
     *
     *  @param agent the submitting agent
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    public void setSubmittingAgent(org.osid.authentication.Agent agent);


    /**
     *  Sets the closed date.
     *
     *  @param date a closed date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public void setClosedDate(org.osid.calendaring.DateTime date);


    /**
     *  Sets the closer.
     *
     *  @param resource the closer
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    public void setCloser(org.osid.resource.Resource resource);


    /**
     *  Sets the closing agent.
     *
     *  @param agent the closing agent
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    public void setClosingAgent(org.osid.authentication.Agent agent);


    /**
     *  Adds an Order record.
     *
     *  @param record an order record
     *  @param recordType the type of order record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addOrderRecord(org.osid.ordering.records.OrderRecord record, org.osid.type.Type recordType);
}       


