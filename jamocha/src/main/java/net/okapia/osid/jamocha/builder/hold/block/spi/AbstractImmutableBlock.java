//
// AbstractImmutableBlock.java
//
//     Wraps a mutable Block to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.hold.block.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Block</code> to hide modifiers. This
 *  wrapper provides an immutized Block from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying block whose state changes are visible.
 */

public abstract class AbstractImmutableBlock
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.hold.Block {

    private final org.osid.hold.Block block;


    /**
     *  Constructs a new <code>AbstractImmutableBlock</code>.
     *
     *  @param block the block to immutablize
     *  @throws org.osid.NullArgumentException <code>block</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableBlock(org.osid.hold.Block block) {
        super(block);
        this.block = block;
        return;
    }


    /**
     *  Gets the issue <code> Ids </code> in this system ranked from
     *  highest to lowest.
     *
     *  @return the list of issues <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> isBasedOnIssues() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getIssueIds() {
        return (this.block.getIssueIds());
    }


    /**
     *  Gets the issues in this system ranked from highest to lowest. 
     *
     *  @return the list of issues 
     *  @throws org.osid.IllegalStateException <code> isBasedOnIssues() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.hold.IssueList getIssues()
        throws org.osid.OperationFailedException {

        return (this.block.getIssues());
    }


    /**
     *  Gets the block record corresponding to the given <code> Block </code> 
     *  record <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record. The <code> blockRecordType </code> 
     *  may be the <code> Type </code> returned in <code> getRecordTypes() 
     *  </code> or any of its parents in a <code> Type </code> hierarchy where 
     *  <code> hasRecordType(blockRecordType) </code> is <code> true </code> . 
     *
     *  @param  blockRecordType the type of block record to retrieve 
     *  @return the block record 
     *  @throws org.osid.NullArgumentException <code> blockRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(blockRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.records.BlockRecord getBlockRecord(org.osid.type.Type blockRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.block.getBlockRecord(blockRecordType));
    }
}

