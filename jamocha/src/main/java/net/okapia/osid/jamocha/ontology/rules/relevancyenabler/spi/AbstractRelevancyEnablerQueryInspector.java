//
// AbstractRelevancyEnablerQueryInspector.java
//
//     A template for making a RelevancyEnablerQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ontology.rules.relevancyenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for relevancy enablers.
 */

public abstract class AbstractRelevancyEnablerQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQueryInspector
    implements org.osid.ontology.rules.RelevancyEnablerQueryInspector {

    private final java.util.Collection<org.osid.ontology.rules.records.RelevancyEnablerQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the relevancy <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledRelevancyIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the relevancy query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyQueryInspector[] getRuledRelevancyTerms() {
        return (new org.osid.ontology.RelevancyQueryInspector[0]);
    }


    /**
     *  Gets the ontology <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOntologyIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ontology query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.ontology.OntologyQueryInspector[] getOntologyTerms() {
        return (new org.osid.ontology.OntologyQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given relevancy enabler query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a relevancy enabler implementing the requested record.
     *
     *  @param relevancyEnablerRecordType a relevancy enabler record type
     *  @return the relevancy enabler query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>relevancyEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(relevancyEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.ontology.rules.records.RelevancyEnablerQueryInspectorRecord getRelevancyEnablerQueryInspectorRecord(org.osid.type.Type relevancyEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ontology.rules.records.RelevancyEnablerQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(relevancyEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(relevancyEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this relevancy enabler query. 
     *
     *  @param relevancyEnablerQueryInspectorRecord relevancy enabler query inspector
     *         record
     *  @param relevancyEnablerRecordType relevancyEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRelevancyEnablerQueryInspectorRecord(org.osid.ontology.rules.records.RelevancyEnablerQueryInspectorRecord relevancyEnablerQueryInspectorRecord, 
                                                   org.osid.type.Type relevancyEnablerRecordType) {

        addRecordType(relevancyEnablerRecordType);
        nullarg(relevancyEnablerRecordType, "relevancy enabler record type");
        this.records.add(relevancyEnablerQueryInspectorRecord);        
        return;
    }
}
