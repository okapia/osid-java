//
// ImmutableComposition.java
//
//     Wraps a mutable Composition to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.repository.composition;


/**
 *  Wraps a mutable <code>Composition</code> to hide modifiers. This
 *  wrapper provides an immutized Composition from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying composition whose state changes are visible.
 */

public final class ImmutableComposition
    extends net.okapia.osid.jamocha.builder.repository.composition.spi.AbstractImmutableComposition
    implements org.osid.repository.Composition {


    /**
     *  Constructs a new <code>ImmutableComposition</code>.
     *
     *  @param composition the composition
     *  @throws org.osid.NullArgumentException <code>composition</code>
     *          is <code>null</code>
     */

    public ImmutableComposition(org.osid.repository.Composition composition) {
        super(composition);
        return;
    }
}

