//
// AbstractLogEntryNotificationSession.java
//
//     A template for making LogEntryNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.logging.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code LogEntry} objects. This session is intended
 *  for consumers needing to synchronize their state with this service
 *  without the use of polling. Notifications are cancelled when this
 *  session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code LogEntry} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for log entry entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractLogEntryNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.logging.LogEntryNotificationSession {

    private boolean federated = false;
    private org.osid.logging.Log log = new net.okapia.osid.jamocha.nil.logging.log.UnknownLog();


    /**
     *  Gets the {@code Log/code> {@code Id} associated with this
     *  session.
     *
     *  @return the {@code Log Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getLogId() {
        return (this.log.getId());
    }

    
    /**
     *  Gets the {@code Log} associated with this session.
     *
     *  @return the {@code Log} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.logging.Log getLog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.log);
    }


    /**
     *  Sets the {@code Log}.
     *
     *  @param log the log for this session
     *  @throws org.osid.NullArgumentException {@code log}
     *          is {@code null}
     */

    protected void setLog(org.osid.logging.Log log) {
        nullarg(log, "log");
        this.log = log;
        return;
    }


    /**
     *  Tests if this user can register for {@code LogEntry}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForLogEntryNotifications() {
        return (true);
    }


    /**
     *  Reliable notifications are desired. In reliable mode,
     *  notifications are to be acknowledged using <code>
     *  acknowledgeLogEntryNotification() </code>.
     */

    @OSID @Override
    public void reliableLogEntryNotifications() {
        return;
    }


    /**
     *  Unreliable notifications are desired. In unreliable mode,
     *  notifications do not need to be acknowledged.
     */

    @OSID @Override
    public void unreliableLogEntryNotifications() {
        return;
    }


    /**
     *  Acknowledge a log entry notification.
     *
     *  @param  notificationId the <code> Id </code> of the notification
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void acknowledgeLogEntryNotification(org.osid.id.Id notificationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include entries in logs which are children of this
     *  log in the log hierarchy.
     */

    @OSID @Override
    public void useFederatedLogView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this log only.
     */

    @OSID @Override
    public void useIsolatedLogView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return {@code true} if federated view, {@code false}
     *          otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications of new log entries. {@code
     *  LogEntryReceiver.newLogEntry()} is invoked when a new {@code
     *  LogEntry} is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewLogEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new log entries at or above the
     *  given priority type. {@code LogEntryReceiver.newLogEntry()} is
     *  invoked when a new {@code LogEntry} is created.
     *
     *  @param  priorityType a priority type 
     *  @throws org.osid.NullArgumentException {@code priorityType} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForNewLogEntriesAtPriority(org.osid.type.Type priorityType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of updated log entries logged by an
     *  agent associated with the given resource. {@code
     *  LogEntryReceiver.changedLogEntry()} is invoked when a {@code
     *  LogEntry} in this log is changed.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @throws org.osid.NullArgumentException {@code resourceId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForNewLogEntriesForResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of updated log entries. {@code
     *  LogEntryReceiver.changedLogEntry()} is invoked when a log
     *  entry is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedLogEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of updated log entries at or above
     *  the given priority type. {@code
     *  LogEntryReceiver.changedLogEntry()} is invoked when a {@code
     *  LogEntry} in this log is changed.
     *
     *  @param  priorityType a priority type 
     *  @throws org.osid.NullArgumentException {@code priorityType} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForChangedEntriesAtPriority(org.osid.type.Type priorityType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of updated log entries logged by an
     *  agent associated with the given resource. {@code
     *  LogEntryReceiver.changedLogEntry()} is invoked when a {@code
     *  LogEntry} in this log is changed.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForChangedEntriesForResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of an updated log entry. {@code
     *  LogEntryReceiver.changedLogEntry()} is invoked when the
     *  specified log entry is changed.
     *
     *  @param logEntryId the {@code Id} of the {@code LogEntry} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code logEntryId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedLogEntry(org.osid.id.Id logEntryId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted log entries. {@code
     *  LogEntryReceiver.deletedLogEntry()} is invoked when a log
     *  entry is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedLogEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of deleted log entries at or above
     *  the given priority type. {@code
     *  LogEntryReceiver.deletedLogEntry()} is invoked when a {@code
     *  LogEntry} is deleted or removed from this log.
     *
     *  @param  priorityType a priority type 
     *  @throws org.osid.NullArgumentException {@code priorityType} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForDeletedLogEntriesAtPriority(org.osid.type.Type priorityType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of deleted log entries logged by an
     *  agent associated with the given resource. {@code
     *  LogEntryReceiver.deletedLogEntry()} is invoked when a {@code
     *  LogEntry} is deleted or removed from this log.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @throws org.osid.NullArgumentException {@code resourceId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForDeletedLogEntriesForResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of a deleted log entry. {@code
     *  LogEntryReceiver.deletedLogEntry()} is invoked when the
     *  specified log entry is deleted.
     *
     *  @param logEntryId the {@code Id} of the
     *          {@code LogEntry} to monitor
     *  @throws org.osid.NullArgumentException {@code logEntryId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedLogEntry(org.osid.id.Id logEntryId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
