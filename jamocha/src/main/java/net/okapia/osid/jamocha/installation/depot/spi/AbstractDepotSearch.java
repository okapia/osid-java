//
// AbstractDepotSearch.java
//
//     A template for making a Depot Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.installation.depot.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing depot searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractDepotSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.installation.DepotSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.installation.records.DepotSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.installation.DepotSearchOrder depotSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of depots. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  depotIds list of depots
     *  @throws org.osid.NullArgumentException
     *          <code>depotIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongDepots(org.osid.id.IdList depotIds) {
        while (depotIds.hasNext()) {
            try {
                this.ids.add(depotIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongDepots</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of depot Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getDepotIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  depotSearchOrder depot search order 
     *  @throws org.osid.NullArgumentException
     *          <code>depotSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>depotSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderDepotResults(org.osid.installation.DepotSearchOrder depotSearchOrder) {
	this.depotSearchOrder = depotSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.installation.DepotSearchOrder getDepotSearchOrder() {
	return (this.depotSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given depot search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a depot implementing the requested record.
     *
     *  @param depotSearchRecordType a depot search record
     *         type
     *  @return the depot search record
     *  @throws org.osid.NullArgumentException
     *          <code>depotSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(depotSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.installation.records.DepotSearchRecord getDepotSearchRecord(org.osid.type.Type depotSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.installation.records.DepotSearchRecord record : this.records) {
            if (record.implementsRecordType(depotSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(depotSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this depot search. 
     *
     *  @param depotSearchRecord depot search record
     *  @param depotSearchRecordType depot search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addDepotSearchRecord(org.osid.installation.records.DepotSearchRecord depotSearchRecord, 
                                           org.osid.type.Type depotSearchRecordType) {

        addRecordType(depotSearchRecordType);
        this.records.add(depotSearchRecord);        
        return;
    }
}
