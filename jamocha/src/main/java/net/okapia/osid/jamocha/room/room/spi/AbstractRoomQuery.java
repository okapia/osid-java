//
// AbstractRoomQuery.java
//
//     A template for making a Room Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.room.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for rooms.
 */

public abstract class AbstractRoomQuery    
    extends net.okapia.osid.jamocha.spi.AbstractTemporalOsidObjectQuery
    implements org.osid.room.RoomQuery {

    private final java.util.Collection<org.osid.room.records.RoomQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the building <code> Id </code> for this query to match rooms 
     *  assigned to buildings. 
     *
     *  @param  buildingId a building <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> buildingId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBuildingId(org.osid.id.Id buildingId, boolean match) {
        return;
    }


    /**
     *  Clears the building <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBuildingIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BuildingQuery </code> is available. 
     *
     *  @return <code> true </code> if a building query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBuildingQuery() {
        return (false);
    }


    /**
     *  Gets the query for a building query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the building query 
     *  @throws org.osid.UnimplementedException <code> supportsBuildingQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingQuery getBuildingQuery() {
        throw new org.osid.UnimplementedException("supportsBuildingQuery() is false");
    }


    /**
     *  Clears the building terms. 
     */

    @OSID @Override
    public void clearBuildingTerms() {
        return;
    }


    /**
     *  Sets the floor <code> Id </code> for this query to match rooms 
     *  assigned to campuses. 
     *
     *  @param  floorId a floor <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> floorId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchFloorId(org.osid.id.Id floorId, boolean match) {
        return;
    }


    /**
     *  Clears the floor <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearFloorIdTerms() {
        return;
    }


    /**
     *  Tests if a floor query is available. 
     *
     *  @return <code> true </code> if a room query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFloorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a floor. 
     *
     *  @return the floor query 
     *  @throws org.osid.UnimplementedException <code> supportsFloorQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.FloorQuery getFloorQuery() {
        throw new org.osid.UnimplementedException("supportsFloorQuery() is false");
    }


    /**
     *  Clears the floor terms. 
     */

    @OSID @Override
    public void clearFloorTerms() {
        return;
    }


    /**
     *  Sets an enclosing room <code> Id. </code> 
     *
     *  @param  roomId an enclosing room <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> roomId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchEnclosingRoomId(org.osid.id.Id roomId, boolean match) {
        return;
    }


    /**
     *  Clears the enclosing room <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearEnclosingRoomIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> EnclosingRoomQuery </code> is available. 
     *
     *  @return <code> true </code> if an enclosing room query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEnclosingRoomQuery() {
        return (false);
    }


    /**
     *  Gets the query for an enclosing room query. Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the room query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEnclosingRoomQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomQuery getEnclosingRoomQuery() {
        throw new org.osid.UnimplementedException("supportsEnclosingRoomQuery() is false");
    }


    /**
     *  Matches any enclosing room. 
     *
     *  @param  match <code> true </code> for a to match any room enclosed in 
     *          another room,, <code> false </code> otherwise 
     */

    @OSID @Override
    public void matchAnyEnclosingRoom(boolean match) {
        return;
    }


    /**
     *  Clears the enclosing room terms. 
     */

    @OSID @Override
    public void clearEnclosingRoomTerms() {
        return;
    }


    /**
     *  Sets a subdivision room <code> Id. </code> 
     *
     *  @param  roomId a subdivision room <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> roomId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchSubdivisionId(org.osid.id.Id roomId, boolean match) {
        return;
    }


    /**
     *  Clears the subdivision room <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSubdivisionIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> RoomQuery </code> is available. 
     *
     *  @return <code> true </code> if a subdivision room query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubdivisionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a subdivision room query. Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the room query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubdivisionRoomQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomQuery getSubdivisionQuery() {
        throw new org.osid.UnimplementedException("supportsSubdivisionQuery() is false");
    }


    /**
     *  Matches any subdivision room. 
     *
     *  @param  match <code> true </code> for a to match any room containg 
     *          another room,, <code> false </code> otherwise 
     */

    @OSID @Override
    public void matchAnySubdivision(boolean match) {
        return;
    }


    /**
     *  Clears the subdivision room terms. 
     */

    @OSID @Override
    public void clearSubdivisionTerms() {
        return;
    }


    /**
     *  Sets a name. 
     *
     *  @param  name an official name 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> name </code> is not 
     *          of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> name </code> is <code> 
     *          null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchDesignatedName(String name, 
                                    org.osid.type.Type stringMatchType, 
                                    boolean match) {
        return;
    }


    /**
     *  Matches any designated name. 
     *
     *  @param  match <code> true </code> to match rooms with any designated 
     *          name, <code> false </code> to match rooms with no designated 
     *          name 
     */

    @OSID @Override
    public void matchAnyDesignatedName(boolean match) {
        return;
    }


    /**
     *  Clears the designated name terms. 
     */

    @OSID @Override
    public void clearDesignatedNameTerms() {
        return;
    }


    /**
     *  Sets a room number. 
     *
     *  @param  number a number 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> number </code> is not 
     *          of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> number </code> is <code> 
     *          null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchRoomNumber(String number, 
                                org.osid.type.Type stringMatchType, 
                                boolean match) {
        return;
    }


    /**
     *  Matches any room number. 
     *
     *  @param  match <code> true </code> to match rooms with any number, 
     *          <code> false </code> to match rooms with no number 
     */

    @OSID @Override
    public void matchAnyRoomNumber(boolean match) {
        return;
    }


    /**
     *  Clears the room number terms. 
     */

    @OSID @Override
    public void clearRoomNumberTerms() {
        return;
    }


    /**
     *  Sets a room code. 
     *
     *  @param  code a room code 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> code </code> is not 
     *          of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> code </code> is <code> 
     *          null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchCode(String code, org.osid.type.Type stringMatchType, 
                          boolean match) {
        return;
    }


    /**
     *  Matches any room code. 
     *
     *  @param  match <code> true </code> to match rooms with any code, <code> 
     *          false </code> to match rooms with no code 
     */

    @OSID @Override
    public void matchAnyCode(boolean match) {
        return;
    }


    /**
     *  Clears the code terms. 
     */

    @OSID @Override
    public void clearCodeTerms() {
        return;
    }


    /**
     *  Matches an area within the given range inclusive. 
     *
     *  @param  low start of range 
     *  @param  high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     *  @throws org.osid.NullArgumentException <code> low </code> or <code> 
     *          high </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchArea(java.math.BigDecimal low, java.math.BigDecimal high, 
                          boolean match) {
        return;
    }


    /**
     *  Matches any area. 
     *
     *  @param  match <code> true </code> to match rooms with any area, <code> 
     *          false </code> to match rooms with no area assigned 
     */

    @OSID @Override
    public void matchAnyArea(boolean match) {
        return;
    }


    /**
     *  Clears the area terms. 
     */

    @OSID @Override
    public void clearAreaTerms() {
        return;
    }


    /**
     *  Matches an occupancy limit within the given range inclusive. 
     *
     *  @param  low start of range 
     *  @param  high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     */

    @OSID @Override
    public void matchOccupancyLimit(long low, long high, boolean match) {
        return;
    }


    /**
     *  Matches rooms with any occupancy limit. 
     *
     *  @param  match <code> true </code> to match rooms with any occupancy 
     *          limit, <code> false </code> to match rooms with no limit 
     *          assigned 
     */

    @OSID @Override
    public void matchAnyOccupancyLimit(boolean match) {
        return;
    }


    /**
     *  Clears the occupancy limit terms. 
     */

    @OSID @Override
    public void clearOccupancyLimitTerms() {
        return;
    }


    /**
     *  Sets the room <code> Id </code> for this query to match resources 
     *  assigned to rooms. 
     *
     *  @param  roomId a room <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> roomId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchResourceId(org.osid.id.Id roomId, boolean match) {
        return;
    }


    /**
     *  Clears the resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearResourceIdTerms() {
        return;
    }


    /**
     *  Tests if a resource query is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a room resource. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsResourceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getResourceQuery() {
        throw new org.osid.UnimplementedException("supportsResourceQuery() is false");
    }


    /**
     *  Matches rooms with any resource. 
     *
     *  @param  match <code> true </code> to match rooms with any resource, 
     *          <code> false </code> to match rooms with no resources 
     */

    @OSID @Override
    public void matchAnyResource(boolean match) {
        return;
    }


    /**
     *  Clears the resource terms. 
     */

    @OSID @Override
    public void clearResourceTerms() {
        return;
    }


    /**
     *  Sets the building <code> Id </code> for this query to match rooms 
     *  assigned to campuses. 
     *
     *  @param  campusId a campus <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCampusId(org.osid.id.Id campusId, boolean match) {
        return;
    }


    /**
     *  Clears the campus <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCampusIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CampusQuery </code> is available. 
     *
     *  @return <code> true </code> if a campus query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCampusQuery() {
        return (false);
    }


    /**
     *  Gets the query for a campus query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the campus query 
     *  @throws org.osid.UnimplementedException <code> supportsCampusQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.CampusQuery getCampusQuery() {
        throw new org.osid.UnimplementedException("supportsCampusQuery() is false");
    }


    /**
     *  Clears the campus terms. 
     */

    @OSID @Override
    public void clearCampusTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given room query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a room implementing the requested record.
     *
     *  @param roomRecordType a room record type
     *  @return the room query record
     *  @throws org.osid.NullArgumentException
     *          <code>roomRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(roomRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.room.records.RoomQueryRecord getRoomQueryRecord(org.osid.type.Type roomRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.room.records.RoomQueryRecord record : this.records) {
            if (record.implementsRecordType(roomRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(roomRecordType + " is not supported");
    }


    /**
     *  Adds a record to this room query. 
     *
     *  @param roomQueryRecord room query record
     *  @param roomRecordType room record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRoomQueryRecord(org.osid.room.records.RoomQueryRecord roomQueryRecord, 
                                          org.osid.type.Type roomRecordType) {

        addRecordType(roomRecordType);
        nullarg(roomQueryRecord, "room query record");
        this.records.add(roomQueryRecord);        
        return;
    }
}
