//
// AbstractAdapterProgramLookupSession.java
//
//    A Program lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.course.program.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Program lookup session adapter.
 */

public abstract class AbstractAdapterProgramLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.course.program.ProgramLookupSession {

    private final org.osid.course.program.ProgramLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterProgramLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterProgramLookupSession(org.osid.course.program.ProgramLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code CourseCatalog/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code CourseCatalog Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.session.getCourseCatalogId());
    }


    /**
     *  Gets the {@code CourseCatalog} associated with this session.
     *
     *  @return the {@code CourseCatalog} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCourseCatalog());
    }


    /**
     *  Tests if this user can perform {@code Program} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupPrograms() {
        return (this.session.canLookupPrograms());
    }


    /**
     *  A complete view of the {@code Program} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeProgramView() {
        this.session.useComparativeProgramView();
        return;
    }


    /**
     *  A complete view of the {@code Program} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryProgramView() {
        this.session.usePlenaryProgramView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include programs in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.session.useFederatedCourseCatalogView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.session.useIsolatedCourseCatalogView();
        return;
    }
    

    /**
     *  Only active programs are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveProgramView() {
        this.session.useActiveProgramView();
        return;
    }


    /**
     *  Active and inactive programs are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusProgramView() {
        this.session.useAnyStatusProgramView();
        return;
    }
    
     
    /**
     *  Gets the {@code Program} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Program} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Program} and
     *  retained for compatibility.
     *
     *  In active mode, programs are returned that are currently
     *  active. In any status mode, active and inactive programs
     *  are returned.
     *
     *  @param programId {@code Id} of the {@code Program}
     *  @return the program
     *  @throws org.osid.NotFoundException {@code programId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code programId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.Program getProgram(org.osid.id.Id programId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProgram(programId));
    }


    /**
     *  Gets a {@code ProgramList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  programs specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Programs} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, programs are returned that are currently
     *  active. In any status mode, active and inactive programs
     *  are returned.
     *
     *  @param  programIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Program} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code programIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.ProgramList getProgramsByIds(org.osid.id.IdList programIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProgramsByIds(programIds));
    }


    /**
     *  Gets a {@code ProgramList} corresponding to the given
     *  program genus {@code Type} which does not include
     *  programs of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  programs or an error results. Otherwise, the returned list
     *  may contain only those programs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, programs are returned that are currently
     *  active. In any status mode, active and inactive programs
     *  are returned.
     *
     *  @param  programGenusType a program genus type 
     *  @return the returned {@code Program} list
     *  @throws org.osid.NullArgumentException
     *          {@code programGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.ProgramList getProgramsByGenusType(org.osid.type.Type programGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProgramsByGenusType(programGenusType));
    }


    /**
     *  Gets a {@code ProgramList} corresponding to the given
     *  program genus {@code Type} and include any additional
     *  programs with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  programs or an error results. Otherwise, the returned list
     *  may contain only those programs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, programs are returned that are currently
     *  active. In any status mode, active and inactive programs
     *  are returned.
     *
     *  @param  programGenusType a program genus type 
     *  @return the returned {@code Program} list
     *  @throws org.osid.NullArgumentException
     *          {@code programGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.ProgramList getProgramsByParentGenusType(org.osid.type.Type programGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProgramsByParentGenusType(programGenusType));
    }


    /**
     *  Gets a {@code ProgramList} containing the given
     *  program record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  programs or an error results. Otherwise, the returned list
     *  may contain only those programs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, programs are returned that are currently
     *  active. In any status mode, active and inactive programs
     *  are returned.
     *
     *  @param  programRecordType a program record type 
     *  @return the returned {@code Program} list
     *  @throws org.osid.NullArgumentException
     *          {@code programRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.ProgramList getProgramsByRecordType(org.osid.type.Type programRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProgramsByRecordType(programRecordType));
    }


    /**
     *  Gets a {@code ProgramList} containing the given creential. 
     *  
     *  In plenary mode, the returned list contains all known programs or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  programs that are accessible through this session. 
     *  
     *  In active mode, active programs are returned. In any status mode, 
     *  active and inactive programs are returned. 
     *
     *  @param  credentialId a credential {@code Id} 
     *  @return the returned {@code ProgramList} list 
     *  @throws org.osid.NullArgumentException {@code credentialId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.program.ProgramList getProgramsByCredential(org.osid.id.Id credentialId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProgramsByCredential(credentialId));
    }


    /**
     *  Gets all {@code Programs}. 
     *
     *  In plenary mode, the returned list contains all known
     *  programs or an error results. Otherwise, the returned list
     *  may contain only those programs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, programs are returned that are currently
     *  active. In any status mode, active and inactive programs
     *  are returned.
     *
     *  @return a list of {@code Programs} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.ProgramList getPrograms()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPrograms());
    }
}
