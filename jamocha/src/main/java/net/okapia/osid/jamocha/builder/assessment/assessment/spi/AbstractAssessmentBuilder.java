//
// AbstractAssessment.java
//
//     Defines an Assessment builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.assessment.assessment.spi;


/**
 *  Defines an <code>Assessment</code> builder.
 */

public abstract class AbstractAssessmentBuilder<T extends AbstractAssessmentBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.assessment.assessment.AssessmentMiter assessment;


    /**
     *  Constructs a new <code>AbstractAssessmentBuilder</code>.
     *
     *  @param assessment the assessment to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractAssessmentBuilder(net.okapia.osid.jamocha.builder.assessment.assessment.AssessmentMiter assessment) {
        super(assessment);
        this.assessment = assessment;
        return;
    }


    /**
     *  Builds the assessment.
     *
     *  @return the new assessment
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.assessment.Assessment build() {
        (new net.okapia.osid.jamocha.builder.validator.assessment.assessment.AssessmentValidator(getValidations())).validate(this.assessment);
        return (new net.okapia.osid.jamocha.builder.assessment.assessment.ImmutableAssessment(this.assessment));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the assessment miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.assessment.assessment.AssessmentMiter getMiter() {
        return (this.assessment);
    }


    /**
     *  Sets the level.
     *
     *  @param level a level
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>level</code> is <code>null</code>
     */

    public T level(org.osid.grading.Grade level) {
        getMiter().setLevel(level);
        return (self());
    }


    /**
     *  Sets the rubric.
     *
     *  @param assessment the rubric assessment
     *  @throws org.osid.NullArgumentException
     *          <code>assessment</code> is <code>null</code>
     */

    public T rubric(org.osid.assessment.Assessment assessment) {
        getMiter().setRubric(assessment);
        return (self());
    }


    /**
     *  Adds an Assessment record.
     *
     *  @param record an assessment record
     *  @param recordType the type of assessment record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.assessment.records.AssessmentRecord record, org.osid.type.Type recordType) {
        getMiter().addAssessmentRecord(record, recordType);
        return (self());
    }
}       


