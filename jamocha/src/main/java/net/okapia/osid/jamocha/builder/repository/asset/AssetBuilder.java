//
// Asset.java
//
//     Defines an Asset builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.repository.asset;


/**
 *  Defines an <code>Asset</code> builder.
 */

public final class AssetBuilder
    extends net.okapia.osid.jamocha.builder.repository.asset.spi.AbstractAssetBuilder<AssetBuilder> {
    

    /**
     *  Constructs a new <code>AssetBuilder</code> using a
     *  <code>MutableAsset</code>.
     */

    public AssetBuilder() {
        super(new MutableAsset());
        return;
    }


    /**
     *  Constructs a new <code>AssetBuilder</code> using the given
     *  mutable asset.
     * 
     *  @param asset
     */

    public AssetBuilder(AssetMiter asset) {
        super(asset);
        return;
    }


    /**
     *  Gets the reference to this instance of the builder.
     *
     *  @return AssetBuilder
     */

    @Override
    protected AssetBuilder self() {
        return (this);
    }
}       


