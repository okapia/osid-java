//
// AbstractAction.java
//
//     Defines an Action builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.control.action.spi;


/**
 *  Defines an <code>Action</code> builder.
 */

public abstract class AbstractActionBuilder<T extends AbstractActionBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRuleBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.control.action.ActionMiter action;


    /**
     *  Constructs a new <code>AbstractActionBuilder</code>.
     *
     *  @param action the action to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractActionBuilder(net.okapia.osid.jamocha.builder.control.action.ActionMiter action) {
        super(action);
        this.action = action;
        return;
    }


    /**
     *  Builds the action.
     *
     *  @return the new action
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.NullArgumentException <code>action</code>
     *          is <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.control.Action build() {
        (new net.okapia.osid.jamocha.builder.validator.control.action.ActionValidator(getValidations())).validate(this.action);
        return (new net.okapia.osid.jamocha.builder.control.action.ImmutableAction(this.action));
    }


    /**
     *  Gets the action. This method is used to get the miter
     *  interface for further updates. Use <code>build()</code> to
     *  finalize and validate construction.
     *
     *  @return the new action
     */

    @Override
    public net.okapia.osid.jamocha.builder.control.action.ActionMiter getMiter() {
        return (this.action);
    }


    /**
     *  Sets the action group.
     *
     *  @param actionGroup an action group
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>actionGroup</code> is <code>null</code>
     */

    public T actionGroup(org.osid.control.ActionGroup actionGroup) {
        getMiter().setActionGroup(actionGroup);
        return (self());
    }


    /**
     *  Sets the delay.
     *
     *  @param delay a delay
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>delay</code> is <code>null</code>
     */

    public T delay(org.osid.calendaring.Duration delay) {
        getMiter().setDelay(delay);
        return (self());
    }


    /**
     *  Sets the next action group.
     *
     *  @param actionGroup a next action group
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>nextActionGroup</code> is <code>null</code>
     */

    public T nextActionGroup(org.osid.control.ActionGroup actionGroup) {
        getMiter().setNextActionGroup(actionGroup);
        return (self());
    }


    /**
     *  Sets the scene.
     *
     *  @param scene a scene
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>scene</code> is
     *          <code>null</code>
     */

    public T scene(org.osid.control.Scene scene) {
        getMiter().setScene(scene);
        return (self());
    }


    /**
     *  Sets the setting.
     *
     *  @param setting a setting
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>setting</code> is
     *          <code>null</code>
     */

    public T setting(org.osid.control.Setting setting) {
        getMiter().setSetting(setting);
        return (self());
    }


    /**
     *  Sets the matching controller.
     *
     *  @param controller a matching controller
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>controller</code>
     *          is <code>null</code>
     */

    public T matchingController(org.osid.control.Controller controller) {
        getMiter().setMatchingController(controller);
        return (self());
    }


    /**
     *  Sets the matching amount factor.
     *
     *  @param factor a matching amount factor
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>factor</code> is
     *          <code>null</code>
     */

    public T matchingAmountFactor(java.math.BigDecimal factor) {
        getMiter().setMatchingAmountFactor(factor);
        return (self());
    }


    /**
     *  Sets the matching rate factor.
     *
     *  @param factor a matching rate factor
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>factor</code> is
     *          <code>null</code>
     */

    public T matchingRateFactor(java.math.BigDecimal factor) {
        getMiter().setMatchingRateFactor(factor);
        return (self());
    }


    /**
     *  Adds an Action record.
     *
     *  @param record an action record
     *  @param recordType the type of action record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.control.records.ActionRecord record, org.osid.type.Type recordType) {
        getMiter().addActionRecord(record, recordType);
        return (self());
    }
}       


