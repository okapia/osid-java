//
// AbstractPathQuery.java
//
//     A template for making a Path Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.path.path.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for paths.
 */

public abstract class AbstractPathQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.mapping.path.PathQuery {

    private final java.util.Collection<org.osid.mapping.path.records.PathQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches paths containing the specified <code> Coordinate. </code> 
     *
     *  @param  coordinate a coordinate 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> coordinate </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCoordinate(org.osid.mapping.Coordinate coordinate, 
                                boolean match) {
        return;
    }


    /**
     *  Clears the coordinate query terms. 
     */

    @OSID @Override
    public void clearCoordinateTerms() {
        return;
    }


    /**
     *  Matches paths overlapping with the specified <code> SpatialUnit. 
     *  </code> 
     *
     *  @param  spatialUnit a spatial unit 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> spatialUnit </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchOverlappingSpatialUnit(org.osid.mapping.SpatialUnit spatialUnit, 
                                            boolean match) {
        return;
    }


    /**
     *  Matches paths that have any spatial unit assignment. 
     *
     *  @param  match <code> true </code> to match paths with any spatial 
     *          dimension, <code> false </code> to match paths with no spatial 
     *          dimensions 
     */

    @OSID @Override
    public void matchAnyOverlappingSpatialUnit(boolean match) {
        return;
    }


    /**
     *  Clears the spatial unit query terms. 
     */

    @OSID @Override
    public void clearOverlappingSpatialUnitTerms() {
        return;
    }


    /**
     *  Sets the location <code> Ids </code> for this query to match paths 
     *  including all the given locations. 
     *
     *  @param  locationIds the location <code> Ids </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> locationIds </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAlongLocationIds(org.osid.id.Id[] locationIds, boolean match) {
        return;
    }


    /**
     *  Clears the along location <code> Ids </code> query terms. 
     */

    @OSID @Override
    public void clearAlongLocationIdsTerms() {
        return;
    }


    /**
     *  Sets the path <code> Id </code> for this query to match paths 
     *  intersecting with another path, 
     *
     *  @param  pathId the path <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> pathId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchIntersectingPathId(org.osid.id.Id pathId, boolean match) {
        return;
    }


    /**
     *  Clears the intersecting path <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearIntersectingPathIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> PathQuery </code> is available for intersecting 
     *  paths, 
     *
     *  @return <code> true </code> if a path query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIntersectingPathQuery() {
        return (false);
    }


    /**
     *  Gets the query for an intersecting path, Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the path query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIntersectingPathQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathQuery getIntersectingPathQuery() {
        throw new org.osid.UnimplementedException("supportsIntersectingPathQuery() is false");
    }


    /**
     *  Matches paths with any intersecting path, 
     *
     *  @param  match <code> true </code> to match paths with any intersecting 
     *          path, <code> false </code> to match paths with no intersecting 
     *          path 
     */

    @OSID @Override
    public void matchAnyIntersectingPath(boolean match) {
        return;
    }


    /**
     *  Clears the intersecting path query terms. 
     */

    @OSID @Override
    public void clearIntersectingPathTerms() {
        return;
    }


    /**
     *  Sets the location <code> Id </code> for this query to match paths that 
     *  pass through locations. 
     *
     *  @param  locationId the location <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> locationId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchLocationId(org.osid.id.Id locationId, boolean match) {
        return;
    }


    /**
     *  Clears the location <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearLocationIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> LocationQuery </code> is available. 
     *
     *  @return <code> true </code> if a location query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocationQuery() {
        return (false);
    }


    /**
     *  Gets the query for a location. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the location query 
     *  @throws org.osid.UnimplementedException <code> supportsLocationQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationQuery getLocationQuery() {
        throw new org.osid.UnimplementedException("supportsLocationQuery() is false");
    }


    /**
     *  Matches routes that go through any location. 
     *
     *  @param  match <code> true </code> to match routes with any location, 
     *          <code> false </code> to match routes with no location 
     */

    @OSID @Override
    public void matchAnyLocation(boolean match) {
        return;
    }


    /**
     *  Clears the location query terms. 
     */

    @OSID @Override
    public void clearLocationTerms() {
        return;
    }


    /**
     *  Sets the route <code> Id </code> for this query to match paths used in 
     *  a route. 
     *
     *  @param  routeId a route <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> routeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRouteId(org.osid.id.Id routeId, boolean match) {
        return;
    }


    /**
     *  Clears the route <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRouteIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> RouteQuery </code> is available. 
     *
     *  @return <code> true </code> if a route query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRouteQuery() {
        return (false);
    }


    /**
     *  Gets the query for a route. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the route query 
     *  @throws org.osid.UnimplementedException <code> supportsRouteQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteQuery getRouteQuery() {
        throw new org.osid.UnimplementedException("supportsRouteQuery() is false");
    }


    /**
     *  Matches paths that are used in any route. 
     *
     *  @param  match <code> true </code> to match paths in any route, <code> 
     *          false </code> to match paths used in no route 
     */

    @OSID @Override
    public void matchAnyRoute(boolean match) {
        return;
    }


    /**
     *  Clears the route query terms. 
     */

    @OSID @Override
    public void clearRouteTerms() {
        return;
    }


    /**
     *  Sets the map <code> Id </code> for this query to match paths assigned 
     *  to maps. 
     *
     *  @param  mapId the map <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchMapId(org.osid.id.Id mapId, boolean match) {
        return;
    }


    /**
     *  Clears the map <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearMapIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> MapQuery </code> is available. 
     *
     *  @return <code> true </code> if a map query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMapQuery() {
        return (false);
    }


    /**
     *  Gets the query for a map. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the map query 
     *  @throws org.osid.UnimplementedException <code> supportsMapQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.MapQuery getMapQuery() {
        throw new org.osid.UnimplementedException("supportsMapQuery() is false");
    }


    /**
     *  Clears the map query terms. 
     */

    @OSID @Override
    public void clearMapTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given path query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a path implementing the requested record.
     *
     *  @param pathRecordType a path record type
     *  @return the path query record
     *  @throws org.osid.NullArgumentException
     *          <code>pathRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(pathRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.path.records.PathQueryRecord getPathQueryRecord(org.osid.type.Type pathRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.path.records.PathQueryRecord record : this.records) {
            if (record.implementsRecordType(pathRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(pathRecordType + " is not supported");
    }


    /**
     *  Adds a record to this path query. 
     *
     *  @param pathQueryRecord path query record
     *  @param pathRecordType path record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addPathQueryRecord(org.osid.mapping.path.records.PathQueryRecord pathQueryRecord, 
                                          org.osid.type.Type pathRecordType) {

        addRecordType(pathRecordType);
        nullarg(pathQueryRecord, "path query record");
        this.records.add(pathQueryRecord);        
        return;
    }
}
