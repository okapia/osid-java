//
// AbstractRoutingCategoryLookupSession
//
//     A routing federating adapter for a CategoryLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.billing.spi;

import org.osid.binding.java.annotation.OSID;

import net.okapia.osid.torrefacto.collect.IdHashMap;
import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A federating adapter for a CategoryLookupSession. Sessions are
 *  added to this session through <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 *
 *  The routing for Ids and Types are manually set by subclasses of
 *  this session that add child sessions for federation.
 *
 *  Routing for Ids is based on the existince of an Id embedded in the
 *  authority of a category Id. The embedded Id is specified when
 *  adding child sessions. Optionally, a list of types for genus and
 *  record may also be mapped to a child session. A service Id for
 *  routing must be unique across all federated sessions while a Type
 *  may map to more than one session.
 *
 *  Lookup methods consult the mapping tables for Ids and Types, and
 *  will fallback to searching all sessions if the category is not
 *  found and fallback is true. In the case of record and genus Type
 *  lookups, a fallback will search all child sessions regardless if
 *  any are found.
 */

public abstract class AbstractRoutingCategoryLookupSession
    extends AbstractFederatingCategoryLookupSession
    implements org.osid.billing.CategoryLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.billing.CategoryLookupSession> sessionsById = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.billing.CategoryLookupSession>());
    private final MultiMap<org.osid.type.Type, org.osid.billing.CategoryLookupSession> sessionsByType = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.billing.CategoryLookupSession>());
    private final boolean idFallback;
    private final boolean typeFallback;

    
    /**
     *  Constructs a new
     *  <code>AbstractRoutingCategoryLookupSession</code> with
     *  fallbacks for both Types and Ids.
     */

    protected AbstractRoutingCategoryLookupSession() {
        this.idFallback   = true;
        this.typeFallback = true;
        selectAll();
        return;
    }


    /**
     *  Constructs a new
     *  <code>AbstractRoutingCategoryLookupSession</code>.
     *
     *  @param idFallback <code>true</code> to fall back to searching
     *         all sessions if an Id is not in routing table
     *  @param typeFallback <code>true</code> to fall back to
     *         searching all sessions if a Type is not in routing
     *         table
     */

    protected AbstractRoutingCategoryLookupSession(boolean idFallback, boolean typeFallback) {
        this.idFallback   = idFallback;
        this.typeFallback = typeFallback;
        selectAll();
        return;
    }


    /**
     *  Maps a session to a service Id. Use <code>addSession()</code>
     *  to add a session to this federation.
     *
     *  @param session a session to map
     *  @param serviceId 
     *  @throws org.osid.NullArgumentException <code>session</code> or
     *          <code>serviceId</code> is <code>null</code>
     */

    protected void mapSession(org.osid.billing.CategoryLookupSession session, org.osid.id.Id serviceId) {
        nullarg(serviceId, "service Id");
        this.sessionsById.put(serviceId, session);
        return;
    }


    /**
     *  Maps a session to a record or genus Type. Use
     *  <code>addSession()</code> to add a session to this federation.
     *
     *  @param session a session to add
     *  @param type
     *  @throws org.osid.NullArgumentException <code>session</code> or
     *          <code>type</code> is <code>null</code>
     */

    protected void mapSession(org.osid.billing.CategoryLookupSession session, org.osid.type.Type type) {
        nullarg(type, "type");
        this.sessionsByType.put(type, session);
        return;
    }


    /**
     *  Unmaps a session from a serviceId.
     *
     *  @param serviceId 
     *  @throws org.osid.NullArgumentException 
     *          <code>serviceId</code> is <code>null</code>
     */

    protected void unmapSession(org.osid.id.Id serviceId) {
        nullarg(serviceId, "service Id");
        this.sessionsById.remove(serviceId);
        return;
    }


    /**
     *  Unmaps a session from both the Id and Type indices.
     *
     *  @param session
     *  @throws org.osid.NullArgumentException 
     *          <code>session</code> is <code>null</code>
     */

    protected void unmapSession(org.osid.billing.CategoryLookupSession session) {
        nullarg(session, "session");

        for (org.osid.id.Id id : this.sessionsById.keySet()) {
            if (session.equals(this.sessionsById.get(id))) {
                this.sessionsById.remove(id);
            }
        }

        this.sessionsByType.removeValue(session);

        return;
    }


    /**
     *  Removes a session from this federation.
     *
     *  @param session
     *  @throws org.osid.NullArgumentException 
     *          <code>session</code> is <code>null</code>
     */

    protected void removeSession(org.osid.billing.CategoryLookupSession session) {
        unmapSession(session);
        super.removeSession(session);
        return;
    }


    /**
     *  Gets the <code>Category</code> specified by its <code>Id</code>. 
     *
     *  @param  categoryId <code>Id</code> of the
     *          <code>Category</code>
     *  @return the category
     *  @throws org.osid.NotFoundException <code>categoryId</code> not 
     *          found in any session
     *  @throws org.osid.NullArgumentException <code>categoryId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.Category getCategory(org.osid.id.Id categoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.billing.CategoryLookupSession session = this.sessionsById.get(categoryId.getAuthority());
        if (session != null) {
            return (session.getCategory(categoryId));
        }
        
        if (this.idFallback) {
            return (super.getCategory(categoryId));
        }

        throw new org.osid.NotFoundException(categoryId + " not found");
    }


    /**
     *  Gets a <code>CategoryList</code> corresponding to the given
     *  category genus <code>Type</code> which does not include
     *  categories of types derived from the specified
     *  <code>Type</code>.
     *  
     *
     *  @param  categoryGenusType a category genus type 
     *  @return the returned <code>Category</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>categoryGenusType</code> is
     *          <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.CategoryList getCategoriesByGenusType(org.osid.type.Type categoryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.billing.category.FederatingCategoryList ret = getCategoryList();
        java.util.Collection<org.osid.billing.CategoryLookupSession> sessions = this.sessionsByType.get(categoryGenusType);

        if (sessions.size() == 0) {
            if (this.typeFallback) {
                return (super.getCategoriesByGenusType(categoryGenusType));
            } else {
                return (new net.okapia.osid.jamocha.nil.billing.category.EmptyCategoryList());
            }
        }


        for (org.osid.billing.CategoryLookupSession session : sessions) {
            ret.addCategoryList(session.getCategoriesByGenusType(categoryGenusType));
        }
        
        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CategoryList</code> corresponding to the given
     *  category genus <code>Type</code> and include any additional
     *  categories with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  @param  categoryGenusType a category genus type 
     *  @return the returned <code>Category</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>categoryGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.CategoryList getCategoriesByParentGenusType(org.osid.type.Type categoryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.billing.category.FederatingCategoryList ret = getCategoryList();
        java.util.Collection<org.osid.billing.CategoryLookupSession> sessions = this.sessionsByType.get(categoryGenusType);

        if (sessions.size() == 0) {
            if (this.typeFallback) {
                return (super.getCategoriesByParentGenusType(categoryGenusType));
            } else {
                return (new net.okapia.osid.jamocha.nil.billing.category.EmptyCategoryList());
            }
        }


        for (org.osid.billing.CategoryLookupSession session : sessions) {
            ret.addCategoryList(session.getCategoriesByParentGenusType(categoryGenusType));
        }
        
        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CategoryList</code> containing the given
     *  category record <code>Type</code>.
     *
     *  @param  categoryRecordType a category record type 
     *  @return the returned <code>Category</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>categoryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.CategoryList getCategoriesByRecordType(org.osid.type.Type categoryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.billing.category.FederatingCategoryList ret = getCategoryList();
        java.util.Collection<org.osid.billing.CategoryLookupSession> sessions = this.sessionsByType.get(categoryRecordType);

        if (sessions.size() == 0) {
            if (this.typeFallback) {
                return (super.getCategoriesByRecordType(categoryRecordType));
            } else {
                return (new net.okapia.osid.jamocha.nil.billing.category.EmptyCategoryList());
            }
        }

        for (org.osid.billing.CategoryLookupSession session : sessions) {
            ret.addCategoryList(session.getCategoriesByRecordType(categoryRecordType));
        }
        
        ret.noMore();
        return (ret);
    }
}
