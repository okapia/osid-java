//
// InvariantMapProxySignalLookupSession
//
//    Implements a Signal lookup service backed by a fixed
//    collection of signals. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping.path;


/**
 *  Implements a Signal lookup service backed by a fixed
 *  collection of signals. The signals are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxySignalLookupSession
    extends net.okapia.osid.jamocha.core.mapping.path.spi.AbstractMapSignalLookupSession
    implements org.osid.mapping.path.SignalLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapProxySignalLookupSession</code> with no
     *  signals.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException <code>proxy</code> is
     *          <code>null</code>
     */

    public InvariantMapProxySignalLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new <code>InvariantMapProxySignalLookupSession</code> with a
     *  single signal.
     *
     *  @param  signal a single signal
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException <code>signal</code> or
     *          <code>proxy</code> is <code>null</code>
     */

    public InvariantMapProxySignalLookupSession(org.osid.mapping.path.Signal signal, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putSignal(signal);
        return;
    }


    /**
     *  Constructs a new <code>InvariantMapProxySignalLookupSession</code> using
     *  an array of signals.
     *
     *  @param  signals an array of signals
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException <code>signals</code> or
     *          <code>proxy</code> is <code>null</code>
     */

    public InvariantMapProxySignalLookupSession(org.osid.mapping.path.Signal[] signals, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putSignals(signals);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapProxySignalLookupSession</code> using a
     *  collection of signals.
     *
     *  @param  signals a collection of signals
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException <code>signals</code> or
     *          <code>proxy</code> is <code>null</code>
     */

    public InvariantMapProxySignalLookupSession(java.util.Collection<? extends org.osid.mapping.path.Signal> signals,
                                                  org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putSignals(signals);
        return;
    }
}
