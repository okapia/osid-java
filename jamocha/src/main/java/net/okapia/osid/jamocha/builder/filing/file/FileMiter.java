//
// FileMiter.java
//
//     Defines a File miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.filing.file;


/**
 *  Defines a <code>File</code> miter for use with the builders.
 */

public interface FileMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidObjectMiter,
            org.osid.filing.File {


    /**
     *  Sets the name.
     *  
     *  @param name the file name
     *  @throws org.osid.NullArgumentException <code>name</code> is
     *          <code>null</code>
     */

    public void setName(String name);


    /**
     *  Sets the alias flag.
     *  
     *  @param alias <code>true</code> if an alias, <code>false</code>
     *         otherwise
     */

    public void setAlias(boolean alias);


    /**
     *  Sets the path.
     *  
     *  @param path the file path
     *  @throws org.osid.NullArgumentException <code>path</code> is
     *          <code>null</code>
     */

    public void setPath(String path);


    /**
     *  Sets the real path.
     *  
     *  @param path the file real path
     *  @throws org.osid.NullArgumentException <code>path</code> is
     *          <code>null</code>
     */

    public void setRealPath(String path);


    /**
     *  Sets the owner.
     *  
     *  @param agent the file owner
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    public void setOwner(org.osid.authentication.Agent agent);


    /**
     *  Sets the created time.
     *  
     *  @param time the file created time
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    public void setCreatedTime(org.osid.calendaring.DateTime time);


    /**
     *  Sets the modified time.
     *  
     *  @param time the file modified time
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    public void setLastModifiedTime(org.osid.calendaring.DateTime time);


    /**
     *  Sets the accessed time.
     *  
     *  @param time the file accessed time
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    public void setLastAccessTime(org.osid.calendaring.DateTime time);


    /**
     *  Sets the size.
     *
     *  @param size the file size
     *  @throws org.osid.InvalidArgumentException <code>size</code> is
     *          negative
     */

    public void setSize(long size);



    /**
     *  Adds a record to this file. 
     *
     *  @param record file record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    public void addFileRecord(org.osid.filing.records.FileRecord record, org.osid.type.Type recordType);
}       


