//
// AbstractAdapterCatalogueLookupSession.java
//
//    A Catalogue lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.offering.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Catalogue lookup session adapter.
 */

public abstract class AbstractAdapterCatalogueLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.offering.CatalogueLookupSession {

    private final org.osid.offering.CatalogueLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterCatalogueLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterCatalogueLookupSession(org.osid.offering.CatalogueLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@code Catalogue} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupCatalogues() {
        return (this.session.canLookupCatalogues());
    }


    /**
     *  A complete view of the {@code Catalogue} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCatalogueView() {
        this.session.useComparativeCatalogueView();
        return;
    }


    /**
     *  A complete view of the {@code Catalogue} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCatalogueView() {
        this.session.usePlenaryCatalogueView();
        return;
    }

     
    /**
     *  Gets the {@code Catalogue} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Catalogue} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Catalogue} and
     *  retained for compatibility.
     *
     *  @param catalogueId {@code Id} of the {@code Catalogue}
     *  @return the catalogue
     *  @throws org.osid.NotFoundException {@code catalogueId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code catalogueId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.Catalogue getCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCatalogue(catalogueId));
    }


    /**
     *  Gets a {@code CatalogueList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  catalogues specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Catalogues} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  catalogueIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Catalogue} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code catalogueIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.CatalogueList getCataloguesByIds(org.osid.id.IdList catalogueIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCataloguesByIds(catalogueIds));
    }


    /**
     *  Gets a {@code CatalogueList} corresponding to the given
     *  catalogue genus {@code Type} which does not include
     *  catalogues of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  catalogues or an error results. Otherwise, the returned list
     *  may contain only those catalogues that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  catalogueGenusType a catalogue genus type 
     *  @return the returned {@code Catalogue} list
     *  @throws org.osid.NullArgumentException
     *          {@code catalogueGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.CatalogueList getCataloguesByGenusType(org.osid.type.Type catalogueGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCataloguesByGenusType(catalogueGenusType));
    }


    /**
     *  Gets a {@code CatalogueList} corresponding to the given
     *  catalogue genus {@code Type} and include any additional
     *  catalogues with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  catalogues or an error results. Otherwise, the returned list
     *  may contain only those catalogues that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  catalogueGenusType a catalogue genus type 
     *  @return the returned {@code Catalogue} list
     *  @throws org.osid.NullArgumentException
     *          {@code catalogueGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.CatalogueList getCataloguesByParentGenusType(org.osid.type.Type catalogueGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCataloguesByParentGenusType(catalogueGenusType));
    }


    /**
     *  Gets a {@code CatalogueList} containing the given
     *  catalogue record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  catalogues or an error results. Otherwise, the returned list
     *  may contain only those catalogues that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  catalogueRecordType a catalogue record type 
     *  @return the returned {@code Catalogue} list
     *  @throws org.osid.NullArgumentException
     *          {@code catalogueRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.CatalogueList getCataloguesByRecordType(org.osid.type.Type catalogueRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCataloguesByRecordType(catalogueRecordType));
    }


    /**
     *  Gets a {@code CatalogueList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  catalogues or an error results. Otherwise, the returned list
     *  may contain only those catalogues that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Catalogue} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.CatalogueList getCataloguesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCataloguesByProvider(resourceId));
    }


    /**
     *  Gets all {@code Catalogues}. 
     *
     *  In plenary mode, the returned list contains all known
     *  catalogues or an error results. Otherwise, the returned list
     *  may contain only those catalogues that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Catalogues} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.CatalogueList getCatalogues()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCatalogues());
    }
}
