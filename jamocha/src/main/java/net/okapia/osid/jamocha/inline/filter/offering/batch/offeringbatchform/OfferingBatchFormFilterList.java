//
// OfferingBatchFormFilterList.java
//
//     Implements a filtering OfferingBatchFormList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.offering.batch.offeringbatchform;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filtering OfferingBatchFormList.
 */

public final class OfferingBatchFormFilterList
    extends net.okapia.osid.jamocha.inline.filter.offering.batch.offeringbatchform.spi.AbstractOfferingBatchFormFilterList
    implements org.osid.offering.batch.OfferingBatchFormList,
               OfferingBatchFormFilter {

    private final OfferingBatchFormFilter filter;


    /**
     *  Creates a new <code>OfferingBatchFormFilterList</code>.
     *
     *  @param filter an inline query filter
     *  @param list an <code>OfferingBatchFormList</code>
     *  @throws org.osid.NullArgumentException <code>filter</code> or
     *          <code>list</code> is <code>null</code>
     */

    public OfferingBatchFormFilterList(OfferingBatchFormFilter filter, org.osid.offering.batch.OfferingBatchFormList list) {
        super(list);

        nullarg(filter, "inline query filter");
        this.filter = filter;

        return;
    }    

    
    /**
     *  Filters OfferingBatchForms.
     *
     *  @param offeringBatchForm the offering batch form to filter
     *  @return <code>true</code> if the offering batch form passes the filter,
     *          <code>false</code> if the offering batch form should be filtered
     */

    @Override
    public boolean pass(org.osid.offering.batch.OfferingBatchForm offeringBatchForm) {
        return (this.filter.pass(offeringBatchForm));
    }
}
