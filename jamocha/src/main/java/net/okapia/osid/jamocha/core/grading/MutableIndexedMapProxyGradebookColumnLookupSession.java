//
// MutableIndexedMapProxyGradebookColumnLookupSession
//
//    Implements a GradebookColumn lookup service backed by a collection of
//    gradebookColumns indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.grading;


/**
 *  Implements a GradebookColumn lookup service backed by a collection of
 *  gradebookColumns. The gradebook columns are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some gradebookColumns may be compatible
 *  with more types than are indicated through these gradebookColumn
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of gradebook columns can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyGradebookColumnLookupSession
    extends net.okapia.osid.jamocha.core.grading.spi.AbstractIndexedMapGradebookColumnLookupSession
    implements org.osid.grading.GradebookColumnLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyGradebookColumnLookupSession} with
     *  no gradebook column.
     *
     *  @param gradebook the gradebook
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code gradebook} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyGradebookColumnLookupSession(org.osid.grading.Gradebook gradebook,
                                                       org.osid.proxy.Proxy proxy) {
        setGradebook(gradebook);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyGradebookColumnLookupSession} with
     *  a single gradebook column.
     *
     *  @param gradebook the gradebook
     *  @param  gradebookColumn an gradebook column
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code gradebook},
     *          {@code gradebookColumn}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyGradebookColumnLookupSession(org.osid.grading.Gradebook gradebook,
                                                       org.osid.grading.GradebookColumn gradebookColumn, org.osid.proxy.Proxy proxy) {

        this(gradebook, proxy);
        putGradebookColumn(gradebookColumn);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyGradebookColumnLookupSession} using
     *  an array of gradebook columns.
     *
     *  @param gradebook the gradebook
     *  @param  gradebookColumns an array of gradebook columns
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code gradebook},
     *          {@code gradebookColumns}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyGradebookColumnLookupSession(org.osid.grading.Gradebook gradebook,
                                                       org.osid.grading.GradebookColumn[] gradebookColumns, org.osid.proxy.Proxy proxy) {

        this(gradebook, proxy);
        putGradebookColumns(gradebookColumns);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyGradebookColumnLookupSession} using
     *  a collection of gradebook columns.
     *
     *  @param gradebook the gradebook
     *  @param  gradebookColumns a collection of gradebook columns
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code gradebook},
     *          {@code gradebookColumns}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyGradebookColumnLookupSession(org.osid.grading.Gradebook gradebook,
                                                       java.util.Collection<? extends org.osid.grading.GradebookColumn> gradebookColumns,
                                                       org.osid.proxy.Proxy proxy) {
        this(gradebook, proxy);
        putGradebookColumns(gradebookColumns);
        return;
    }

    
    /**
     *  Makes a {@code GradebookColumn} available in this session.
     *
     *  @param  gradebookColumn a gradebook column
     *  @throws org.osid.NullArgumentException {@code gradebookColumn{@code 
     *          is {@code null}
     */

    @Override
    public void putGradebookColumn(org.osid.grading.GradebookColumn gradebookColumn) {
        super.putGradebookColumn(gradebookColumn);
        return;
    }


    /**
     *  Makes an array of gradebook columns available in this session.
     *
     *  @param  gradebookColumns an array of gradebook columns
     *  @throws org.osid.NullArgumentException {@code gradebookColumns{@code 
     *          is {@code null}
     */

    @Override
    public void putGradebookColumns(org.osid.grading.GradebookColumn[] gradebookColumns) {
        super.putGradebookColumns(gradebookColumns);
        return;
    }


    /**
     *  Makes collection of gradebook columns available in this session.
     *
     *  @param  gradebookColumns a collection of gradebook columns
     *  @throws org.osid.NullArgumentException {@code gradebookColumn{@code 
     *          is {@code null}
     */

    @Override
    public void putGradebookColumns(java.util.Collection<? extends org.osid.grading.GradebookColumn> gradebookColumns) {
        super.putGradebookColumns(gradebookColumns);
        return;
    }


    /**
     *  Removes a GradebookColumn from this session.
     *
     *  @param gradebookColumnId the {@code Id} of the gradebook column
     *  @throws org.osid.NullArgumentException {@code gradebookColumnId{@code  is
     *          {@code null}
     */

    @Override
    public void removeGradebookColumn(org.osid.id.Id gradebookColumnId) {
        super.removeGradebookColumn(gradebookColumnId);
        return;
    }    
}
