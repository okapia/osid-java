//
// AbstractCourseEntryQuery.java
//
//     A template for making a CourseEntry Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.chronicle.courseentry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for course entries.
 */

public abstract class AbstractCourseEntryQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQuery
    implements org.osid.course.chronicle.CourseEntryQuery {

    private final java.util.Collection<org.osid.course.chronicle.records.CourseEntryQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the student <code> Id </code> for this query. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchStudentId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the student <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearStudentIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> StudentQuery </code> is available. 
     *
     *  @return <code> true </code> if a student query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStudentQuery() {
        return (false);
    }


    /**
     *  Gets the query for a student option. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return a student query 
     *  @throws org.osid.UnimplementedException <code> supportsStudentQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getStudentQuery() {
        throw new org.osid.UnimplementedException("supportsStudentQuery() is false");
    }


    /**
     *  Clears the student option terms. 
     */

    @OSID @Override
    public void clearStudentTerms() {
        return;
    }


    /**
     *  Sets the course <code> Id </code> for this query to match entries that 
     *  have an entry for the given course. 
     *
     *  @param  courseId a course <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCourseId(org.osid.id.Id courseId, boolean match) {
        return;
    }


    /**
     *  Clears the course <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CourseQuery </code> is available. 
     *
     *  @return <code> true </code> if a course query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course entry. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return a course query 
     *  @throws org.osid.UnimplementedException <code> supportsCourseQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseQuery getCourseQuery() {
        throw new org.osid.UnimplementedException("supportsCourseQuery() is false");
    }


    /**
     *  Clears the course terms. 
     */

    @OSID @Override
    public void clearCourseTerms() {
        return;
    }


    /**
     *  Sets the term <code> Id </code> for this query. 
     *
     *  @param  termId a term <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> termId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchTermId(org.osid.id.Id termId, boolean match) {
        return;
    }


    /**
     *  Clears the term <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearTermIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> TermQuery </code> is available. 
     *
     *  @return <code> true </code> if a term query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTermQuery() {
        return (false);
    }


    /**
     *  Gets the query for a term entry. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return a term query 
     *  @throws org.osid.UnimplementedException <code> supportsTermQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermQuery getTermQuery() {
        throw new org.osid.UnimplementedException("supportsTermQuery() is false");
    }


    /**
     *  Clears the term terms. 
     */

    @OSID @Override
    public void clearTermTerms() {
        return;
    }


    /**
     *  Matches completed courses. 
     *
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchComplete(boolean match) {
        return;
    }


    /**
     *  Clears the complete terms. 
     */

    @OSID @Override
    public void clearCompleteTerms() {
        return;
    }


    /**
     *  Matches a credit scale <code> Id. </code> 
     *
     *  @param  gradeSystemId a grade system <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeSystemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCreditScaleId(org.osid.id.Id gradeSystemId, boolean match) {
        return;
    }


    /**
     *  Clears the grade system <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCreditScaleIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> GradeSystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade system query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCreditScaleQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grade system. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return a grade system query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCreditScaleQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQuery getCreditScaleQuery() {
        throw new org.osid.UnimplementedException("supportsCreditScaleQuery() is false");
    }


    /**
     *  Matches entries that have any credit scale. 
     *
     *  @param  match <code> true </code> to match entries with any credit 
     *          scale, <code> false </code> to match entries with no credit 
     *          scale 
     */

    @OSID @Override
    public void matchAnyCreditScale(boolean match) {
        return;
    }


    /**
     *  Clears the credit scale terms. 
     */

    @OSID @Override
    public void clearCreditScaleTerms() {
        return;
    }


    /**
     *  Matches earned credits between the given range inclusive. 
     *
     *  @param  from starting value 
     *  @param  to ending value 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchCreditsEarned(java.math.BigDecimal from, 
                                   java.math.BigDecimal to, boolean match) {
        return;
    }


    /**
     *  Matches entries that have any earned credits. 
     *
     *  @param  match <code> true </code> to match entries with any earned 
     *          credits, <code> false </code> to match entries with no earned 
     *          credits 
     */

    @OSID @Override
    public void matchAnyCreditsEarned(boolean match) {
        return;
    }


    /**
     *  Clears the earned credits terms. 
     */

    @OSID @Override
    public void clearCreditsEarnedTerms() {
        return;
    }


    /**
     *  Matches a grade <code> Id. </code> 
     *
     *  @param  gradeId a grade <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGradeId(org.osid.id.Id gradeId, boolean match) {
        return;
    }


    /**
     *  Clears the grade <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGradeIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> GradeQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grade. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return a grade system query 
     *  @throws org.osid.UnimplementedException <code> supportsGradeQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeQuery getGradeQuery() {
        throw new org.osid.UnimplementedException("supportsGradeQuery() is false");
    }


    /**
     *  Matches entries that have any grade. 
     *
     *  @param  match <code> true </code> to match entries with any grade, 
     *          <code> false </code> to match entries with no grade 
     */

    @OSID @Override
    public void matchAnyGrade(boolean match) {
        return;
    }


    /**
     *  Clears the grade terms. 
     */

    @OSID @Override
    public void clearGradeTerms() {
        return;
    }


    /**
     *  Matches a score scale <code> Id. </code> 
     *
     *  @param  gradeSystemId a grade system <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeSystemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchScoreScaleId(org.osid.id.Id gradeSystemId, boolean match) {
        return;
    }


    /**
     *  Clears the grade system <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearScoreScaleIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> GradeSystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade system query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScoreScaleQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grade system. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return a grade system query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScoreScaleQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQuery getScoreScaleQuery() {
        throw new org.osid.UnimplementedException("supportsScoreScaleQuery() is false");
    }


    /**
     *  Matches entries that have any score scale. 
     *
     *  @param  match <code> true </code> to match entries with any score 
     *          scale, <code> false </code> to match entries with no score 
     *          scale 
     */

    @OSID @Override
    public void matchAnyScoreScale(boolean match) {
        return;
    }


    /**
     *  Clears the score scale terms. 
     */

    @OSID @Override
    public void clearScoreScaleTerms() {
        return;
    }


    /**
     *  Matches scores between the given range inclusive. 
     *
     *  @param  from starting value 
     *  @param  to ending value 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchScore(java.math.BigDecimal from, java.math.BigDecimal to, 
                           boolean match) {
        return;
    }


    /**
     *  Matches entries that have any score. 
     *
     *  @param  match <code> true </code> to match entries with any score, 
     *          <code> false </code> to match entries with no score 
     */

    @OSID @Override
    public void matchAnyScore(boolean match) {
        return;
    }


    /**
     *  Clears the score terms. 
     */

    @OSID @Override
    public void clearScoreTerms() {
        return;
    }


    /**
     *  Sets the enrollment <code> Id </code> for this query. 
     *
     *  @param  enrollmentId an enrollment <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> enrollmentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRegistrationId(org.osid.id.Id enrollmentId, boolean match) {
        return;
    }


    /**
     *  Clears the registration <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRegistrationIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> RegistrationQuery </code> is available. 
     *
     *  @return <code> true </code> if a registration query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRegistrationQuery() {
        return (false);
    }


    /**
     *  Gets the query for a registration entry. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return a registration query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRegistrationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationQuery getRegistrationQuery() {
        throw new org.osid.UnimplementedException("supportsRegistrationQuery() is false");
    }


    /**
     *  Matches entries that have any registration. 
     *
     *  @param  match <code> true </code> to match enries with any 
     *          registration, <code> false </code> to match enries with no 
     *          registration 
     */

    @OSID @Override
    public void matchAnyRegistration(boolean match) {
        return;
    }


    /**
     *  Clears the registration terms. 
     */

    @OSID @Override
    public void clearRegistrationTerms() {
        return;
    }


    /**
     *  Sets the course catalog <code> Id </code> for this query to match 
     *  entries assigned to course catalogs. 
     *
     *  @param  courseCatalogId the course catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseCatalogId(org.osid.id.Id courseCatalogId, 
                                     boolean match) {
        return;
    }


    /**
     *  Clears the course catalog <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseCatalogIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CourseCatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a course catalog query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course catalog. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the course catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQuery getCourseCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsCourseCatalogQuery() is false");
    }


    /**
     *  Clears the course catalog terms. 
     */

    @OSID @Override
    public void clearCourseCatalogTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given course entry query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a course entry implementing the requested record.
     *
     *  @param courseEntryRecordType a course entry record type
     *  @return the course entry query record
     *  @throws org.osid.NullArgumentException
     *          <code>courseEntryRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(courseEntryRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.chronicle.records.CourseEntryQueryRecord getCourseEntryQueryRecord(org.osid.type.Type courseEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.chronicle.records.CourseEntryQueryRecord record : this.records) {
            if (record.implementsRecordType(courseEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(courseEntryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this course entry query. 
     *
     *  @param courseEntryQueryRecord course entry query record
     *  @param courseEntryRecordType courseEntry record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCourseEntryQueryRecord(org.osid.course.chronicle.records.CourseEntryQueryRecord courseEntryQueryRecord, 
                                          org.osid.type.Type courseEntryRecordType) {

        addRecordType(courseEntryRecordType);
        nullarg(courseEntryQueryRecord, "course entry query record");
        this.records.add(courseEntryQueryRecord);        
        return;
    }
}
