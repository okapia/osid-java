//
// AbstractFederatingPostLookupSession.java
//
//     An abstract federating adapter for a PostLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.forum.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  PostLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingPostLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.forum.PostLookupSession>
    implements org.osid.forum.PostLookupSession {

    private boolean parallel = false;
    private org.osid.forum.Forum forum = new net.okapia.osid.jamocha.nil.forum.forum.UnknownForum();


    /**
     *  Constructs a new <code>AbstractFederatingPostLookupSession</code>.
     */

    protected AbstractFederatingPostLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.forum.PostLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Forum/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Forum Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getForumId() {
        return (this.forum.getId());
    }


    /**
     *  Gets the <code>Forum</code> associated with this 
     *  session.
     *
     *  @return the <code>Forum</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.Forum getForum()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.forum);
    }


    /**
     *  Sets the <code>Forum</code>.
     *
     *  @param  forum the forum for this session
     *  @throws org.osid.NullArgumentException <code>forum</code>
     *          is <code>null</code>
     */

    protected void setForum(org.osid.forum.Forum forum) {
        nullarg(forum, "forum");
        this.forum = forum;
        return;
    }


    /**
     *  Tests if this user can perform <code>Post</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupPosts() {
        for (org.osid.forum.PostLookupSession session : getSessions()) {
            if (session.canLookupPosts()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Post</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativePostView() {
        for (org.osid.forum.PostLookupSession session : getSessions()) {
            session.useComparativePostView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Post</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryPostView() {
        for (org.osid.forum.PostLookupSession session : getSessions()) {
            session.usePlenaryPostView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include posts in forums which are children
     *  of this forum in the forum hierarchy.
     */

    @OSID @Override
    public void useFederatedForumView() {
        for (org.osid.forum.PostLookupSession session : getSessions()) {
            session.useFederatedForumView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this forum only.
     */

    @OSID @Override
    public void useIsolatedForumView() {
        for (org.osid.forum.PostLookupSession session : getSessions()) {
            session.useIsolatedForumView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Post</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Post</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Post</code> and
     *  retained for compatibility.
     *
     *  @param  postId <code>Id</code> of the
     *          <code>Post</code>
     *  @return the post
     *  @throws org.osid.NotFoundException <code>postId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>postId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.Post getPost(org.osid.id.Id postId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.forum.PostLookupSession session : getSessions()) {
            try {
                return (session.getPost(postId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(postId + " not found");
    }


    /**
     *  Gets a <code>PostList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  posts specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Posts</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  postIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Post</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>postIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.PostList getPostsByIds(org.osid.id.IdList postIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.forum.post.MutablePostList ret = new net.okapia.osid.jamocha.forum.post.MutablePostList();

        try (org.osid.id.IdList ids = postIds) {
            while (ids.hasNext()) {
                ret.addPost(getPost(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>PostList</code> corresponding to the given
     *  post genus <code>Type</code> which does not include
     *  posts of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  posts or an error results. Otherwise, the returned list
     *  may contain only those posts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  postGenusType a post genus type 
     *  @return the returned <code>Post</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>postGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.PostList getPostsByGenusType(org.osid.type.Type postGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.forum.post.FederatingPostList ret = getPostList();

        for (org.osid.forum.PostLookupSession session : getSessions()) {
            ret.addPostList(session.getPostsByGenusType(postGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>PostList</code> corresponding to the given
     *  post genus <code>Type</code> and include any additional
     *  posts with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  posts or an error results. Otherwise, the returned list
     *  may contain only those posts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  postGenusType a post genus type 
     *  @return the returned <code>Post</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>postGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.PostList getPostsByParentGenusType(org.osid.type.Type postGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.forum.post.FederatingPostList ret = getPostList();

        for (org.osid.forum.PostLookupSession session : getSessions()) {
            ret.addPostList(session.getPostsByParentGenusType(postGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>PostList</code> containing the given
     *  post record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  posts or an error results. Otherwise, the returned list
     *  may contain only those posts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  postRecordType a post record type 
     *  @return the returned <code>Post</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>postRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.PostList getPostsByRecordType(org.osid.type.Type postRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.forum.post.FederatingPostList ret = getPostList();

        for (org.osid.forum.PostLookupSession session : getSessions()) {
            ret.addPostList(session.getPostsByRecordType(postRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>PostList</code> in the given date range
     *  inclusive.  In plenary mode, the returned list contains all
     *  known posts or an error results. Otherwise, the returned list
     *  may contain only those posts that are accessible through this
     *  session.
     *
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code>Post</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.forum.PostList getPostsByDate(org.osid.calendaring.DateTime from,
                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.forum.post.FederatingPostList ret = getPostList();

        for (org.osid.forum.PostLookupSession session : getSessions()) {
            ret.addPostList(session.getPostsByDate(from, to));
        }

        ret.noMore();
        return (ret);
    }

    
    /**
     *  Gets a <code>PostList</code> for the given poster. In plenary
     *  mode, the returned list contains all known posts or an error
     *  results. Otherwise, the returned list may contain only those
     *  posts that are accessible through this session.
     *
     *  @param  resourceId a resource <code>Id</code>
     *  @return the returned <code>Post</code> list
     *  @throws org.osid.NullArgumentException <code>resourceId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.forum.PostList getPostsForPoster(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.forum.post.FederatingPostList ret = getPostList();

        for (org.osid.forum.PostLookupSession session : getSessions()) {
            ret.addPostList(session.getPostsForPoster(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>PostList</code> by the given poster and in the
     *  given date range inclusive. In plenary mode, the returned list
     *  contains all known posts or an error results. Otherwise, the
     *  returned list may contain only those posts that are accessible
     *  through this session.
     *
     *  @param  resourceId a resource <code>Id</code>
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code>Post</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.forum.PostList getPostsByDateForPoster(org.osid.id.Id resourceId,
                                                           org.osid.calendaring.DateTime from,
                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
  
        net.okapia.osid.jamocha.adapter.federator.forum.post.FederatingPostList ret = getPostList();

        for (org.osid.forum.PostLookupSession session : getSessions()) {
            ret.addPostList(session.getPostsByDateForPoster(resourceId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Posts</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  posts or an error results. Otherwise, the returned list
     *  may contain only those posts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Posts</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.PostList getPosts()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.forum.post.FederatingPostList ret = getPostList();

        for (org.osid.forum.PostLookupSession session : getSessions()) {
            ret.addPostList(session.getPosts());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.forum.post.FederatingPostList getPostList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.forum.post.ParallelPostList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.forum.post.CompositePostList());
        }
    }
}
