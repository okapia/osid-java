//
// AbstractDepotQueryInspector.java
//
//     A template for making a DepotQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.installation.depot.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for depots.
 */

public abstract class AbstractDepotQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQueryInspector
    implements org.osid.installation.DepotQueryInspector {

    private final java.util.Collection<org.osid.installation.records.DepotQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the package <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPackageIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the package query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.installation.PackageQueryInspector[] getPackageTerms() {
        return (new org.osid.installation.PackageQueryInspector[0]);
    }


    /**
     *  Gets the ancestor depot <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorDepotIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor depot query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.installation.DepotQueryInspector[] getAncestorDepotTerms() {
        return (new org.osid.installation.DepotQueryInspector[0]);
    }


    /**
     *  Gets the descendant depot <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantDepotIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant depot query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.installation.DepotQueryInspector[] getDescendantDepotTerms() {
        return (new org.osid.installation.DepotQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given depot query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a depot implementing the requested record.
     *
     *  @param depotRecordType a depot record type
     *  @return the depot query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>depotRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(depotRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.installation.records.DepotQueryInspectorRecord getDepotQueryInspectorRecord(org.osid.type.Type depotRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.installation.records.DepotQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(depotRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(depotRecordType + " is not supported");
    }


    /**
     *  Adds a record to this depot query. 
     *
     *  @param depotQueryInspectorRecord depot query inspector
     *         record
     *  @param depotRecordType depot record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addDepotQueryInspectorRecord(org.osid.installation.records.DepotQueryInspectorRecord depotQueryInspectorRecord, 
                                                   org.osid.type.Type depotRecordType) {

        addRecordType(depotRecordType);
        nullarg(depotRecordType, "depot record type");
        this.records.add(depotQueryInspectorRecord);        
        return;
    }
}
