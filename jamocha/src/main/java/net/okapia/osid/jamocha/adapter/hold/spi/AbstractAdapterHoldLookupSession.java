//
// AbstractAdapterHoldLookupSession.java
//
//    A Hold lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.hold.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Hold lookup session adapter.
 */

public abstract class AbstractAdapterHoldLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.hold.HoldLookupSession {

    private final org.osid.hold.HoldLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterHoldLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterHoldLookupSession(org.osid.hold.HoldLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Oubliette/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Oubliette Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getOublietteId() {
        return (this.session.getOublietteId());
    }


    /**
     *  Gets the {@code Oubliette} associated with this session.
     *
     *  @return the {@code Oubliette} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.Oubliette getOubliette()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getOubliette());
    }


    /**
     *  Tests if this user can perform {@code Hold} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupHolds() {
        return (this.session.canLookupHolds());
    }


    /**
     *  A complete view of the {@code Hold} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeHoldView() {
        this.session.useComparativeHoldView();
        return;
    }


    /**
     *  A complete view of the {@code Hold} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryHoldView() {
        this.session.usePlenaryHoldView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include holds in oubliettes which are children
     *  of this oubliette in the oubliette hierarchy.
     */

    @OSID @Override
    public void useFederatedOublietteView() {
        this.session.useFederatedOublietteView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this oubliette only.
     */

    @OSID @Override
    public void useIsolatedOublietteView() {
        this.session.useIsolatedOublietteView();
        return;
    }
    

    /**
     *  Only holds whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveHoldView() {
        this.session.useEffectiveHoldView();
        return;
    }
    

    /**
     *  All holds of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveHoldView() {
        this.session.useAnyEffectiveHoldView();
        return;
    }

     
    /**
     *  Gets the {@code Hold} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Hold} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Hold} and
     *  retained for compatibility.
     *
     *  In effective mode, holds are returned that are currently
     *  effective.  In any effective mode, effective holds and
     *  those currently expired are returned.
     *
     *  @param holdId {@code Id} of the {@code Hold}
     *  @return the hold
     *  @throws org.osid.NotFoundException {@code holdId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code holdId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.Hold getHold(org.osid.id.Id holdId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getHold(holdId));
    }


    /**
     *  Gets a {@code HoldList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  holds specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Holds} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, holds are returned that are currently
     *  effective.  In any effective mode, effective holds and
     *  those currently expired are returned.
     *
     *  @param  holdIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Hold} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code holdIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.HoldList getHoldsByIds(org.osid.id.IdList holdIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getHoldsByIds(holdIds));
    }


    /**
     *  Gets a {@code HoldList} corresponding to the given
     *  hold genus {@code Type} which does not include
     *  holds of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  holds or an error results. Otherwise, the returned list
     *  may contain only those holds that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, holds are returned that are currently
     *  effective.  In any effective mode, effective holds and
     *  those currently expired are returned.
     *
     *  @param  holdGenusType a hold genus type 
     *  @return the returned {@code Hold} list
     *  @throws org.osid.NullArgumentException
     *          {@code holdGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.HoldList getHoldsByGenusType(org.osid.type.Type holdGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getHoldsByGenusType(holdGenusType));
    }


    /**
     *  Gets a {@code HoldList} corresponding to the given
     *  hold genus {@code Type} and include any additional
     *  holds with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  holds or an error results. Otherwise, the returned list
     *  may contain only those holds that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, holds are returned that are currently
     *  effective.  In any effective mode, effective holds and
     *  those currently expired are returned.
     *
     *  @param  holdGenusType a hold genus type 
     *  @return the returned {@code Hold} list
     *  @throws org.osid.NullArgumentException
     *          {@code holdGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.HoldList getHoldsByParentGenusType(org.osid.type.Type holdGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getHoldsByParentGenusType(holdGenusType));
    }


    /**
     *  Gets a {@code HoldList} containing the given
     *  hold record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  holds or an error results. Otherwise, the returned list
     *  may contain only those holds that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, holds are returned that are currently
     *  effective.  In any effective mode, effective holds and
     *  those currently expired are returned.
     *
     *  @param  holdRecordType a hold record type 
     *  @return the returned {@code Hold} list
     *  @throws org.osid.NullArgumentException
     *          {@code holdRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.HoldList getHoldsByRecordType(org.osid.type.Type holdRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getHoldsByRecordType(holdRecordType));
    }


    /**
     *  Gets a {@code HoldList} effective during the entire given date
     *  range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known holds or
     *  an error results. Otherwise, the returned list may contain
     *  only those holds that are accessible through this session.
     *  
     *  In active mode, holds are returned that are currently
     *  active. In any status mode, active and inactive holds are
     *  returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Hold} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.hold.HoldList getHoldsOnDate(org.osid.calendaring.DateTime from, 
                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getHoldsOnDate(from, to));
    }
        

    /**
     *  Gets a list of holds corresponding to a resource {@code Id}.
     *
     *  In plenary mode, the returned list contains all known holds or
     *  an error results. Otherwise, the returned list may contain
     *  only those holds that are accessible through this session.
     *
     *  In effective mode, holds are returned that are currently
     *  effective.  In any effective mode, effective holds and those
     *  currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the resource
     *  @return the returned {@code HoldList}
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.hold.HoldList getHoldsForResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getHoldsForResource(resourceId));
    }


    /**
     *  Gets a list of holds corresponding to a resource {@code Id}
     *  and effective during the entire given date range inclusive but
     *  not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known holds or
     *  an error results. Otherwise, the returned list may contain
     *  only those holds that are accessible through this session.
     *
     *  In effective mode, holds are returned that are currently
     *  effective.  In any effective mode, effective holds and those
     *  currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code HoldList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.hold.HoldList getHoldsForResourceOnDate(org.osid.id.Id resourceId,
                                                            org.osid.calendaring.DateTime from,
                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getHoldsForResourceOnDate(resourceId, from, to));
    }


    /**
     *  Gets a list of holds corresponding to an agent {@code Id}.
     *
     *  In plenary mode, the returned list contains all known holds or
     *  an error results. Otherwise, the returned list may contain
     *  only those holds that are accessible through this session.
     *
     *  In effective mode, holds are returned that are currently
     *  effective.  In any effective mode, effective holds and those
     *  currently expired are returned.
     *
     *  @param  agentId the {@code Id} of the agent
     *  @return the returned {@code HoldList}
     *  @throws org.osid.NullArgumentException {@code agentId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.hold.HoldList getHoldsForAgent(org.osid.id.Id agentId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getHoldsForAgent(agentId));
    }


    /**
     *  Gets a list of holds corresponding to an agent {@code Id}
     *  and effective during the entire given date range inclusive but
     *  not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known holds or
     *  an error results. Otherwise, the returned list may contain
     *  only those holds that are accessible through this session.
     *
     *  In effective mode, holds are returned that are currently
     *  effective.  In any effective mode, effective holds and those
     *  currently expired are returned.
     *
     *  @param  agentId the {@code Id} of the agent
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code HoldList}
     *  @throws org.osid.NullArgumentException {@code agentId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.hold.HoldList getHoldsForAgentOnDate(org.osid.id.Id agentId,
                                                         org.osid.calendaring.DateTime from,
                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getHoldsForAgentOnDate(agentId, from, to));
    }


    /**
     *  Gets a list of holds corresponding to an issue {@code Id}.
     *
     *  In plenary mode, the returned list contains all known holds or
     *  an error results. Otherwise, the returned list may contain
     *  only those holds that are accessible through this session.
     *
     *  In effective mode, holds are returned that are currently
     *  effective.  In any effective mode, effective holds and those
     *  currently expired are returned.
     *
     *  @param  issueId the {@code Id} of the issue
     *  @return the returned {@code HoldList}
     *  @throws org.osid.NullArgumentException {@code issueId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.hold.HoldList getHoldsForIssue(org.osid.id.Id issueId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getHoldsForIssue(issueId));
    }


    /**
     *  Gets a list of holds corresponding to an issue {@code Id} and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *
     *  In plenary mode, the returned list contains all known holds or
     *  an error results. Otherwise, the returned list may contain
     *  only those holds that are accessible through this session.
     *
     *  In effective mode, holds are returned that are currently
     *  effective.  In any effective mode, effective holds and those
     *  currently expired are returned.
     *
     *  @param  issueId the {@code Id} of the issue
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code HoldList}
     *  @throws org.osid.NullArgumentException {@code issueId}, {@code
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.hold.HoldList getHoldsForIssueOnDate(org.osid.id.Id issueId,
                                                         org.osid.calendaring.DateTime from,
                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getHoldsForIssueOnDate(issueId, from, to));
    }


    /**
     *  Gets a list of holds corresponding to resource and issue
     *  {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known holds or
     *  an error results. Otherwise, the returned list may contain
     *  only those holds that are accessible through this session.
     *
     *  In effective mode, holds are returned that are currently
     *  effective.  In any effective mode, effective holds and those
     *  currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the resource
     *  @param  issueId the {@code Id} of the issue
     *  @return the returned {@code HoldList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code issueId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.hold.HoldList getHoldsForResourceAndIssue(org.osid.id.Id resourceId,
                                                              org.osid.id.Id issueId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getHoldsForResourceAndIssue(resourceId, issueId));
    }


    /**
     *  Gets a list of holds corresponding to resource and issue
     *  {@code Ids} and effective during the entire given date range
     *  inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known holds or
     *  an error results. Otherwise, the returned list may contain
     *  only those holds that are accessible through this session.
     *
     *  In effective mode, holds are returned that are currently
     *  effective. In any effective mode, effective holds and those
     *  currently expired are returned.
     *
     *  @param  issueId the {@code Id} of the issue
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code HoldList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code issueId}, {@code from} or {@code to} is {@code
     *          null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.hold.HoldList getHoldsForResourceAndIssueOnDate(org.osid.id.Id resourceId,
                                                                    org.osid.id.Id issueId,
                                                                    org.osid.calendaring.DateTime from,
                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getHoldsForResourceAndIssueOnDate(resourceId, issueId, from, to));
    }


    /**
     *  Gets a list of holds corresponding to agent and issue
     *  {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known holds or
     *  an error results. Otherwise, the returned list may contain
     *  only those holds that are accessible through this session.
     *
     *  In effective mode, holds are returned that are currently
     *  effective.  In any effective mode, effective holds and those
     *  currently expired are returned.
     *
     *  @param  agentId the {@code Id} of the agent
     *  @param  issueId the {@code Id} of the issue
     *  @return the returned {@code HoldList}
     *  @throws org.osid.NullArgumentException {@code agentId},
     *          {@code issueId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.hold.HoldList getHoldsForAgentAndIssue(org.osid.id.Id agentId,
                                                           org.osid.id.Id issueId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getHoldsForAgentAndIssue(agentId, issueId));
    }


    /**
     *  Gets a list of holds corresponding to an agent and issue
     *  {@code Ids} and effective during the entire given date range
     *  inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known holds or
     *  an error results. Otherwise, the returned list may contain
     *  only those holds that are accessible through this session.
     *
     *  In effective mode, holds are returned that are currently
     *  effective. In any effective mode, effective holds and those
     *  currently expired are returned.
     *
     *  @param  issueId the {@code Id} of the issue
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code HoldList}
     *  @throws org.osid.NullArgumentException {@code agentId},
     *          {@code issueId}, {@code from} or {@code to} is {@code
     *          null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.hold.HoldList getHoldsForAgentAndIssueOnDate(org.osid.id.Id agentId,
                                                                 org.osid.id.Id issueId,
                                                                 org.osid.calendaring.DateTime from,
                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getHoldsForAgentAndIssueOnDate(agentId, issueId, from, to));
    }

    
    /**
     *  Gets all {@code Holds}. 
     *
     *  In plenary mode, the returned list contains all known holds or
     *  an error results. Otherwise, the returned list may contain
     *  only those holds that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In effective mode, holds are returned that are currently
     *  effective.  In any effective mode, effective holds and those
     *  currently expired are returned.
     *
     *  @return a list of {@code Holds} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.HoldList getHolds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getHolds());
    }
}
