//
// InvariantMapStepProcessorLookupSession
//
//    Implements a StepProcessor lookup service backed by a fixed collection of
//    stepProcessors.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.workflow.rules;


/**
 *  Implements a StepProcessor lookup service backed by a fixed
 *  collection of step processors. The step processors are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapStepProcessorLookupSession
    extends net.okapia.osid.jamocha.core.workflow.rules.spi.AbstractMapStepProcessorLookupSession
    implements org.osid.workflow.rules.StepProcessorLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapStepProcessorLookupSession</code> with no
     *  step processors.
     *  
     *  @param office the office
     *  @throws org.osid.NullArgumnetException {@code office} is
     *          {@code null}
     */

    public InvariantMapStepProcessorLookupSession(org.osid.workflow.Office office) {
        setOffice(office);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapStepProcessorLookupSession</code> with a single
     *  step processor.
     *  
     *  @param office the office
     *  @param stepProcessor a single step processor
     *  @throws org.osid.NullArgumentException {@code office} or
     *          {@code stepProcessor} is <code>null</code>
     */

      public InvariantMapStepProcessorLookupSession(org.osid.workflow.Office office,
                                               org.osid.workflow.rules.StepProcessor stepProcessor) {
        this(office);
        putStepProcessor(stepProcessor);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapStepProcessorLookupSession</code> using an array
     *  of step processors.
     *  
     *  @param office the office
     *  @param stepProcessors an array of step processors
     *  @throws org.osid.NullArgumentException {@code office} or
     *          {@code stepProcessors} is <code>null</code>
     */

      public InvariantMapStepProcessorLookupSession(org.osid.workflow.Office office,
                                               org.osid.workflow.rules.StepProcessor[] stepProcessors) {
        this(office);
        putStepProcessors(stepProcessors);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapStepProcessorLookupSession</code> using a
     *  collection of step processors.
     *
     *  @param office the office
     *  @param stepProcessors a collection of step processors
     *  @throws org.osid.NullArgumentException {@code office} or
     *          {@code stepProcessors} is <code>null</code>
     */

      public InvariantMapStepProcessorLookupSession(org.osid.workflow.Office office,
                                               java.util.Collection<? extends org.osid.workflow.rules.StepProcessor> stepProcessors) {
        this(office);
        putStepProcessors(stepProcessors);
        return;
    }
}
