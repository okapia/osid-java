//
// AbstractFinancialsBudgetingBatchManager.java
//
//     An adapter for a FinancialsBudgetingBatchManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.financials.budgeting.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a FinancialsBudgetingBatchManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterFinancialsBudgetingBatchManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.financials.budgeting.batch.FinancialsBudgetingBatchManager>
    implements org.osid.financials.budgeting.batch.FinancialsBudgetingBatchManager {


    /**
     *  Constructs a new {@code AbstractAdapterFinancialsBudgetingBatchManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterFinancialsBudgetingBatchManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterFinancialsBudgetingBatchManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterFinancialsBudgetingBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if bulk administration of budgets is available. 
     *
     *  @return <code> true </code> if a budget bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBudgetBatchAdmin() {
        return (getAdapteeManager().supportsBudgetBatchAdmin());
    }


    /**
     *  Tests if bulk administration of budget entries is available. 
     *
     *  @return <code> true </code> if a budget entry bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBudgetEntryBatchAdmin() {
        return (getAdapteeManager().supportsBudgetEntryBatchAdmin());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk budget 
     *  administration service. 
     *
     *  @return a <code> BudgetBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.batch.BudgetBatchAdminSession getBudgetBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBudgetBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk budget 
     *  administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> BudgetBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.batch.BudgetBatchAdminSession getBudgetBatchAdminSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBudgetBatchAdminSessionForBusiness(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk budget 
     *  entry administration service. 
     *
     *  @return a <code> BudgetEntryBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetEntryBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.batch.BudgetEntryBatchAdminSession getBudgetEntryBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBudgetEntryBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk budget 
     *  entry administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> BudgetEntryBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetEntryBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.batch.BudgetEntryBatchAdminSession getBudgetEntryBatchAdminSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBudgetEntryBatchAdminSessionForBusiness(businessId));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
