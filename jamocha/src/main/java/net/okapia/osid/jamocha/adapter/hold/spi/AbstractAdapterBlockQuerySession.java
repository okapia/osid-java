//
// AbstractQueryBlockLookupSession.java
//
//    A BlockQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.hold.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A BlockQuerySession adapter.
 */

public abstract class AbstractAdapterBlockQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.hold.BlockQuerySession {

    private final org.osid.hold.BlockQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterBlockQuerySession.
     *
     *  @param session the underlying block query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterBlockQuerySession(org.osid.hold.BlockQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeOubliette</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeOubliette Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getOublietteId() {
        return (this.session.getOublietteId());
    }


    /**
     *  Gets the {@codeOubliette</code> associated with this 
     *  session.
     *
     *  @return the {@codeOubliette</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.Oubliette getOubliette()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getOubliette());
    }


    /**
     *  Tests if this user can perform {@codeBlock</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchBlocks() {
        return (this.session.canSearchBlocks());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include blocks in oubliettes which are children
     *  of this oubliette in the oubliette hierarchy.
     */

    @OSID @Override
    public void useFederatedOublietteView() {
        this.session.useFederatedOublietteView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this oubliette only.
     */
    
    @OSID @Override
    public void useIsolatedOublietteView() {
        this.session.useIsolatedOublietteView();
        return;
    }
    
      
    /**
     *  Gets a block query. The returned query will not have an
     *  extension query.
     *
     *  @return the block query 
     */
      
    @OSID @Override
    public org.osid.hold.BlockQuery getBlockQuery() {
        return (this.session.getBlockQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  blockQuery the block query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code blockQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code blockQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.hold.BlockList getBlocksByQuery(org.osid.hold.BlockQuery blockQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getBlocksByQuery(blockQuery));
    }
}
